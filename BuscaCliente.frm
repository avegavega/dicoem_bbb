VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form BuscaCliente 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Buscar Cliente"
   ClientHeight    =   6105
   ClientLeft      =   3195
   ClientTop       =   3165
   ClientWidth     =   10665
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6105
   ScaleWidth      =   10665
   Begin VB.CommandButton CmdSalir 
      Cancel          =   -1  'True
      Caption         =   "&Retornar"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   9270
      TabIndex        =   6
      ToolTipText     =   "Retorna a la pantalla anterior"
      Top             =   5520
      Width           =   1095
   End
   Begin VB.Frame Frame1 
      Height          =   5310
      Left            =   180
      TabIndex        =   0
      Top             =   150
      Width           =   10200
      Begin VB.CommandButton CmdEliminarCliente 
         Caption         =   "Eliminar Cliente"
         Height          =   495
         Left            =   1515
         TabIndex        =   8
         Top             =   4725
         Width           =   1575
      End
      Begin VB.CommandButton CmdEditarCliente 
         Caption         =   "Editar Cliente"
         Height          =   495
         Left            =   4695
         TabIndex        =   7
         TabStop         =   0   'False
         Top             =   4725
         Width           =   1935
      End
      Begin VB.CommandButton CmdCrear 
         Caption         =   "Crear Nuevo Cliente"
         Height          =   495
         Left            =   6690
         TabIndex        =   3
         TabStop         =   0   'False
         Top             =   4725
         Width           =   1935
      End
      Begin VB.CommandButton CmdSeleccionar 
         Caption         =   "&Seleccionar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   3195
         TabIndex        =   2
         Top             =   4725
         Width           =   1455
      End
      Begin VB.TextBox TxtBusqueda 
         Height          =   375
         Left            =   450
         TabIndex        =   1
         Top             =   720
         Width           =   7095
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   375
         Left            =   435
         OleObjectBlob   =   "BuscaCliente.frx":0000
         TabIndex        =   4
         Top             =   360
         Width           =   8055
      End
      Begin MSComctlLib.ListView LvDetalle 
         Height          =   3435
         Left            =   240
         TabIndex        =   5
         Top             =   1200
         Width           =   9810
         _ExtentX        =   17304
         _ExtentY        =   6059
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   3
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "T1000"
            Text            =   "Rut Cliente"
            Object.Width           =   2646
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "T1500"
            Text            =   "Nombre"
            Object.Width           =   7056
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "T3000"
            Text            =   "Direccion"
            Object.Width           =   7056
         EndProperty
      End
   End
   Begin VB.Timer Timer1 
      Interval        =   20
      Left            =   435
      Top             =   6000
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   -150
      OleObjectBlob   =   "BuscaCliente.frx":00BA
      Top             =   6060
   End
End
Attribute VB_Name = "BuscaCliente"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False






Private Sub CmdCrear_Click()
    AccionCliente = 3
    SG_codigo = Empty
    ClienteEncontrado = False
    AgregoCliente.Timer1.Enabled = True
    AgregoCliente.Show 1
    CargaClientes
End Sub

Private Sub CmdEditarCliente_Click()
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    SG_codigo = LvDetalle.SelectedItem
    AgregoCliente.Show 1
    CargaClientes
End Sub

Private Sub CmdEliminarCliente_Click()
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    SG_codigo = LvDetalle.SelectedItem
    
  If MsgBox("Seguro que desea Eliminar a Cliente, Rut: " & SG_codigo & "", vbOKCancel + vbQuestion) = vbCancel Then Exit Sub
        Sql = "SELECT rut_cliente " & _
          "FROM ven_doc_venta " & _
          "WHERE rut_cliente='" & SG_codigo & "' "
    
          Consulta RsTmp, Sql
    
            If RsTmp.RecordCount = 0 Then
                        cn.Execute "DELETE FROM maestro_clientes  " & _
                        "WHERE rut_cliente='" & SG_codigo & "'"
            Else
                        MsgBox "Este Cliente posee ventas, no es posible elimiarlo...", vbExclamation
            
            
            End If
            CargaClientes
End Sub

Private Sub CmdSalir_Click()
    SG_codigo = Empty
    Unload Me
End Sub

Private Sub CmdSeleccionar_Click()
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    SG_codigo = LvDetalle.SelectedItem
    Unload Me
End Sub
Private Sub CargaClientes()
    Sql = "SELECT rut_cliente,nombre_rsocial,direccion " & _
          "FROM maestro_clientes m INNER JOIN par_asociacion_ruts a ON m.rut_cliente=a.rut_cli " & _
          "WHERE habilitado='SI' AND a.rut_emp='" & SP_Rut_Activo & "' " & _
          "ORDER BY nombre_rsocial "
    Consulta RsTmp, Sql
    
    LLenar_Grilla RsTmp, Me, LvDetalle, False, True, True, False
    
End Sub
Private Sub Form_Load()
    Centrar Me
    Aplicar_skin Me
    CargaClientes
End Sub


Private Sub LvDetalle_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    ordListView ColumnHeader, Me, LvDetalle
End Sub
Private Sub LvDetalle_DblClick()
    CmdSeleccionar_Click
End Sub

Private Sub LvDetalle_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 13 Then CmdSeleccionar_Click
End Sub

Private Sub Timer1_Timer()
    On Error Resume Next
    TxtBusqueda.SetFocus
    Timer1.Enabled = False
End Sub

Private Sub TxtBusqueda_Change()
    If Len(Me.TxtBusqueda.Text) = 0 Then
        Sql = "SELECT rut_cliente,nombre_rsocial,direccion " & _
              "FROM maestro_clientes  m /*INNER JOIN par_asociacion_ruts a  ON m.rut_cliente=a.rut_cli  */ " & _
                "WHERE habilitado='SI' /* AND rut_emp='" & SP_Rut_Activo & "' */ " & _
                "ORDER BY nombre_rsocial"
    Else
        Sql = "SELECT rut_cliente,nombre_rsocial,direccion " & _
              "FROM maestro_clientes  m /* INNER JOIN par_asociacion_ruts a  ON m.rut_cliente=a.rut_cli */ " & _
              "WHERE habilitado='SI' AND /* a.rut_emp='" & SP_Rut_Activo & "' AND */  (nombre_rsocial LIKE '%" & TxtBusqueda & "%' " & _
              "OR direccion LIKE '%" & TxtBusqueda & "%'  OR cli_nombre_fantasia LIKE '%" & TxtBusqueda & "%') " & _
              "ORDER BY nombre_rsocial"
    End If
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvDetalle, False, True, True, False
    
    
End Sub

Private Sub TxtBusqueda_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
    If KeyAscii = 13 Then LvDetalle.SetFocus
     If KeyAscii = 39 Then KeyAscii = 0
End Sub
