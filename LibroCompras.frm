VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "MSDATGRD.OCX"
Begin VB.Form LibroCompras 
   Caption         =   "Libro Compra"
   ClientHeight    =   9690
   ClientLeft      =   -3315
   ClientTop       =   225
   ClientWidth     =   14790
   LinkTopic       =   "Form1"
   ScaleHeight     =   9690
   ScaleWidth      =   14790
   Begin VB.Frame Frame3 
      Caption         =   "Dctos. de Compra"
      Height          =   1695
      Left            =   4350
      TabIndex        =   48
      Top             =   120
      Width           =   2145
      Begin VB.CheckBox Check4 
         Caption         =   "Gu�as"
         Enabled         =   0   'False
         Height          =   255
         Left            =   360
         TabIndex        =   53
         Top             =   1080
         Width           =   975
      End
      Begin VB.CheckBox Check3 
         Caption         =   "Boletas"
         Enabled         =   0   'False
         Height          =   255
         Left            =   360
         TabIndex        =   52
         Top             =   840
         Width           =   1215
      End
      Begin VB.CheckBox Check2 
         Caption         =   "Facturas"
         Enabled         =   0   'False
         Height          =   255
         Left            =   360
         TabIndex        =   51
         Top             =   600
         Width           =   1215
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Todos"
         Height          =   255
         Left            =   360
         TabIndex        =   50
         Top             =   360
         Value           =   1  'Checked
         Width           =   975
      End
      Begin VB.CheckBox chkOrdenDe 
         Caption         =   "Orden de Entrada"
         Enabled         =   0   'False
         Height          =   195
         Left            =   360
         TabIndex        =   49
         Top             =   1320
         Width           =   1575
      End
   End
   Begin VB.Frame FraTipoMovimiento 
      Caption         =   "Tipo Movimiento"
      Height          =   1695
      Left            =   6480
      TabIndex        =   43
      Top             =   120
      Width           =   2175
      Begin VB.CheckBox chkTodos 
         Caption         =   "Todos"
         Height          =   195
         Left            =   360
         TabIndex        =   47
         Top             =   360
         Value           =   1  'Checked
         Width           =   1335
      End
      Begin VB.CheckBox chkCompraRepuesto 
         Caption         =   "Compra Repuesto"
         Enabled         =   0   'False
         Height          =   255
         Left            =   360
         TabIndex        =   46
         Top             =   600
         Width           =   1575
      End
      Begin VB.CheckBox chkVehiculoNuevo 
         Caption         =   "Vehiculo Nuevo"
         Enabled         =   0   'False
         Height          =   195
         Left            =   360
         TabIndex        =   45
         Top             =   840
         Width           =   1455
      End
      Begin VB.CheckBox chkVehiculoUsado 
         Caption         =   "Vehiculo Usado"
         Enabled         =   0   'False
         Height          =   195
         Left            =   360
         TabIndex        =   44
         Top             =   1080
         Width           =   1695
      End
   End
   Begin VB.Frame FraClasificacion 
      Caption         =   "Clasificacion"
      Height          =   1695
      Left            =   8655
      TabIndex        =   38
      Top             =   120
      Visible         =   0   'False
      Width           =   1815
      Begin VB.CheckBox ChkTodosRepuestos 
         Caption         =   "Todos"
         Height          =   255
         Left            =   240
         TabIndex        =   42
         Top             =   600
         Value           =   1  'Checked
         Width           =   1335
      End
      Begin VB.CheckBox chkCITROEN 
         Caption         =   "CITROEN"
         Enabled         =   0   'False
         Height          =   255
         Left            =   240
         TabIndex        =   40
         Top             =   840
         Width           =   1095
      End
      Begin VB.CheckBox chkMultimarca 
         Caption         =   "Multimarca"
         Enabled         =   0   'False
         Height          =   255
         Left            =   240
         TabIndex        =   39
         Top             =   1080
         Width           =   1215
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkiRepuestos 
         Height          =   255
         Left            =   360
         OleObjectBlob   =   "LibroCompras.frx":0000
         TabIndex        =   41
         Top             =   240
         Width           =   975
      End
   End
   Begin VB.Frame FraOrdenarPor 
      Caption         =   "Ordenar Por"
      Height          =   1695
      Left            =   10455
      TabIndex        =   36
      Top             =   120
      Width           =   4170
      Begin VB.ComboBox ComOrdenar 
         Height          =   315
         Left            =   120
         Style           =   2  'Dropdown List
         TabIndex        =   37
         Top             =   720
         Width           =   1455
      End
   End
   Begin VB.Frame FraProgreso 
      Height          =   735
      Left            =   240
      TabIndex        =   32
      Top             =   8880
      Visible         =   0   'False
      Width           =   4575
      Begin VB.PictureBox Picture1 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         ForeColor       =   &H80000008&
         Height          =   375
         Left            =   3720
         ScaleHeight     =   345
         ScaleWidth      =   705
         TabIndex        =   33
         Top             =   240
         Width           =   735
         Begin ACTIVESKINLibCtl.SkinLabel SkProgreso 
            Height          =   255
            Left            =   -120
            OleObjectBlob   =   "LibroCompras.frx":0070
            TabIndex        =   34
            Top             =   45
            Width           =   735
         End
      End
      Begin MSComctlLib.ProgressBar BarraProgreso 
         Height          =   375
         Left            =   120
         TabIndex        =   35
         Top             =   240
         Width           =   3495
         _ExtentX        =   6165
         _ExtentY        =   661
         _Version        =   393216
         Appearance      =   1
      End
   End
   Begin VB.TextBox txtNetoTotal 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00E0E0E0&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   8760
      Locked          =   -1  'True
      TabIndex        =   28
      Top             =   9060
      Width           =   1575
   End
   Begin VB.TextBox txtBrutototal 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00E0E0E0&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   12120
      Locked          =   -1  'True
      TabIndex        =   27
      Top             =   9060
      Width           =   1575
   End
   Begin VB.TextBox txtIvaTotal 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00E0E0E0&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   10440
      Locked          =   -1  'True
      TabIndex        =   26
      Top             =   9060
      Width           =   1575
   End
   Begin VB.Frame Frame 
      Caption         =   "Notas de Credito"
      Height          =   2940
      Left            =   195
      TabIndex        =   16
      Top             =   5730
      Width           =   14415
      Begin VB.TextBox txtIvaNC 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   10245
         Locked          =   -1  'True
         TabIndex        =   19
         Top             =   2565
         Width           =   1575
      End
      Begin VB.TextBox txtBrutoNC 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   11925
         Locked          =   -1  'True
         TabIndex        =   18
         Top             =   2565
         Width           =   1575
      End
      Begin VB.TextBox txtNetoNC 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   8565
         Locked          =   -1  'True
         TabIndex        =   17
         Top             =   2565
         Width           =   1575
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel8 
         Height          =   255
         Index           =   0
         Left            =   9165
         OleObjectBlob   =   "LibroCompras.frx":00D2
         TabIndex        =   21
         Top             =   2325
         Width           =   975
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel 
         Height          =   255
         Left            =   12645
         OleObjectBlob   =   "LibroCompras.frx":0130
         TabIndex        =   22
         Top             =   2325
         Width           =   855
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   255
         Index           =   1
         Left            =   11085
         OleObjectBlob   =   "LibroCompras.frx":0190
         TabIndex        =   23
         Top             =   2325
         Width           =   735
      End
      Begin MSDataGridLib.DataGrid GridNC 
         Height          =   1290
         Left            =   105
         TabIndex        =   20
         Top             =   2880
         Visible         =   0   'False
         Width           =   14055
         _ExtentX        =   24791
         _ExtentY        =   2275
         _Version        =   393216
         HeadLines       =   1
         RowHeight       =   15
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Caption         =   "Notas de Cr�dito"
         ColumnCount     =   2
         BeginProperty Column00 
            DataField       =   ""
            Caption         =   ""
            BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
               Type            =   0
               Format          =   ""
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   3082
               SubFormatType   =   0
            EndProperty
         EndProperty
         BeginProperty Column01 
            DataField       =   ""
            Caption         =   ""
            BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
               Type            =   0
               Format          =   ""
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   3082
               SubFormatType   =   0
            EndProperty
         EndProperty
         SplitCount      =   1
         BeginProperty Split0 
            BeginProperty Column00 
            EndProperty
            BeginProperty Column01 
            EndProperty
         EndProperty
      End
      Begin MSComctlLib.ListView LvNotaCredito 
         Height          =   2100
         Left            =   180
         TabIndex        =   54
         Top             =   210
         Width           =   14040
         _ExtentX        =   24765
         _ExtentY        =   3704
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   15
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Fecha"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Documento"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Nro Doc"
            Object.Width           =   1940
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "RUT Cliente"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Text            =   "R.Social"
            Object.Width           =   5292
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Text            =   "Movimiento"
            Object.Width           =   3528
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   6
            Object.Tag             =   "N100"
            Text            =   "Neto"
            Object.Width           =   1587
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   7
            Object.Tag             =   "N100"
            Text            =   "IVA"
            Object.Width           =   1587
         EndProperty
         BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   8
            Object.Tag             =   "N100"
            Text            =   "Total"
            Object.Width           =   1587
         EndProperty
         BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   9
            Text            =   "Forma de pago"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   10
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(12) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   11
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(13) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   12
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(14) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   13
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(15) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   14
            Object.Width           =   2540
         EndProperty
      End
   End
   Begin MSComDlg.CommonDialog Dialogo 
      Left            =   240
      Top             =   8040
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.Frame Frame1 
      Caption         =   "Opciones del libro"
      Height          =   1695
      Left            =   240
      TabIndex        =   7
      Top             =   120
      Width           =   4125
      Begin VB.Frame FraProveedor 
         Caption         =   "Nombre Proveedor"
         Height          =   735
         Left            =   0
         TabIndex        =   14
         Top             =   960
         Width           =   4095
         Begin VB.TextBox txtNombreProveedor 
            Height          =   375
            Left            =   240
            TabIndex        =   15
            Top             =   240
            Width           =   3015
         End
      End
      Begin VB.CommandButton CmdFiltro 
         Caption         =   "Filtrar"
         Height          =   375
         Left            =   2895
         TabIndex        =   8
         Top             =   480
         Width           =   1095
      End
      Begin MSComCtl2.DTPicker Dtfin 
         Height          =   375
         Left            =   1200
         TabIndex        =   9
         Top             =   600
         Width           =   1455
         _ExtentX        =   2566
         _ExtentY        =   661
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   58982401
         CurrentDate     =   39916
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   255
         Left            =   480
         OleObjectBlob   =   "LibroCompras.frx":01EC
         TabIndex        =   10
         Top             =   600
         Width           =   615
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   255
         Index           =   0
         Left            =   360
         OleObjectBlob   =   "LibroCompras.frx":024C
         TabIndex        =   11
         Top             =   360
         Width           =   735
      End
      Begin MSComCtl2.DTPicker DtInicio 
         Height          =   375
         Left            =   1200
         TabIndex        =   12
         Top             =   240
         Width           =   1455
         _ExtentX        =   2566
         _ExtentY        =   661
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   58982401
         CurrentDate     =   39916
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Detalle"
      Height          =   3735
      Left            =   240
      TabIndex        =   1
      Top             =   1920
      Width           =   14415
      Begin VB.CommandButton cmdEliminarDocumento 
         Caption         =   "Eliminar Documento Seleccionado"
         Height          =   372
         Left            =   240
         TabIndex        =   25
         Top             =   3240
         Width           =   3015
      End
      Begin VB.CommandButton CmdHistorial 
         Caption         =   "Historial"
         Height          =   372
         Left            =   3480
         TabIndex        =   24
         Top             =   3240
         Width           =   1212
      End
      Begin VB.PictureBox PicExcel 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         ForeColor       =   &H80000008&
         Height          =   495
         Left            =   13560
         Picture         =   "LibroCompras.frx":02AE
         ScaleHeight     =   465
         ScaleWidth      =   615
         TabIndex        =   13
         Top             =   3120
         Visible         =   0   'False
         Width           =   645
      End
      Begin VB.TextBox TxtNeto 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   8835
         Locked          =   -1  'True
         TabIndex        =   4
         Top             =   3375
         Width           =   1575
      End
      Begin VB.TextBox TxtBruto 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   12015
         Locked          =   -1  'True
         TabIndex        =   3
         Top             =   3375
         Width           =   1575
      End
      Begin VB.TextBox TxtIva 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   10425
         Locked          =   -1  'True
         TabIndex        =   2
         Top             =   3375
         Width           =   1575
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
         Height          =   255
         Left            =   8970
         OleObjectBlob   =   "LibroCompras.frx":0A75
         TabIndex        =   5
         Top             =   3135
         Width           =   4455
      End
      Begin MSDataGridLib.DataGrid GridVentas 
         Height          =   735
         Left            =   735
         TabIndex        =   6
         Top             =   855
         Visible         =   0   'False
         Width           =   12615
         _ExtentX        =   22251
         _ExtentY        =   1296
         _Version        =   393216
         AllowUpdate     =   0   'False
         HeadLines       =   1
         RowHeight       =   15
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnCount     =   2
         BeginProperty Column00 
            DataField       =   ""
            Caption         =   ""
            BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
               Type            =   0
               Format          =   ""
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   13322
               SubFormatType   =   0
            EndProperty
         EndProperty
         BeginProperty Column01 
            DataField       =   ""
            Caption         =   ""
            BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
               Type            =   0
               Format          =   ""
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   13322
               SubFormatType   =   0
            EndProperty
         EndProperty
         SplitCount      =   1
         BeginProperty Split0 
            BeginProperty Column00 
            EndProperty
            BeginProperty Column01 
            EndProperty
         EndProperty
      End
      Begin MSComctlLib.ListView LvCompras 
         Height          =   2655
         Left            =   120
         TabIndex        =   30
         Top             =   360
         Width           =   14085
         _ExtentX        =   24844
         _ExtentY        =   4683
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   15
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Fecha"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Documento"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Nro Doc"
            Object.Width           =   1940
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "RUT Cliente"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Text            =   "R.Social"
            Object.Width           =   5292
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Text            =   "Movimiento"
            Object.Width           =   3528
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   6
            Object.Tag             =   "N100"
            Text            =   "Neto"
            Object.Width           =   1587
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   7
            Object.Tag             =   "N100"
            Text            =   "IVA"
            Object.Width           =   1587
         EndProperty
         BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   8
            Object.Tag             =   "N100"
            Text            =   "Total"
            Object.Width           =   1587
         EndProperty
         BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   9
            Text            =   "Forma de pago"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   10
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(12) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   11
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(13) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   12
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(14) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   13
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(15) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   14
            Object.Width           =   2540
         EndProperty
      End
   End
   Begin VB.CommandButton CmdSalir 
      Caption         =   "&Salir"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   13770
      TabIndex        =   0
      Top             =   9030
      Width           =   855
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   360
      OleObjectBlob   =   "LibroCompras.frx":0B35
      Top             =   6720
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel8 
      Height          =   225
      Index           =   1
      Left            =   7320
      OleObjectBlob   =   "LibroCompras.frx":0D69
      TabIndex        =   29
      Top             =   9060
      Width           =   1335
   End
   Begin MSComctlLib.ListView LvExport 
      Height          =   2655
      Left            =   0
      TabIndex        =   31
      Top             =   0
      Visible         =   0   'False
      Width           =   14085
      _ExtentX        =   24844
      _ExtentY        =   4683
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   14
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Fecha"
         Object.Width           =   1764
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Documento"
         Object.Width           =   2117
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "Nro Doc"
         Object.Width           =   1940
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Text            =   "RUT Cliente"
         Object.Width           =   2117
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Text            =   "R.Social"
         Object.Width           =   5292
      EndProperty
      BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   5
         Text            =   "Movimiento"
         Object.Width           =   3528
      EndProperty
      BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   6
         Object.Tag             =   "N100"
         Text            =   "Neto"
         Object.Width           =   1587
      EndProperty
      BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   7
         Object.Tag             =   "N100"
         Text            =   "IVA"
         Object.Width           =   1587
      EndProperty
      BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   8
         Object.Tag             =   "N100"
         Text            =   "Total"
         Object.Width           =   1587
      EndProperty
      BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   9
         Text            =   "Forma de pago"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   10
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(12) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   11
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(13) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   12
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(14) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   13
         Object.Width           =   0
      EndProperty
   End
End
Attribute VB_Name = "LibroCompras"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim RsCompras As Recordset
Dim Motivo As String
Dim SqlNC As String
Private Sub cmdEliminarDocumento_Click()
    If RsCompras.RecordCount = 0 Then Exit Sub
    Dim Documento As String
    Dim RutPrv As String
    Dim NumDoc As Double
    Dim Tipo_Mov As String
    Dim ValorDoc As Double
    Dim NombreProveedor As String
    Dim fPago As String
    Dim VehRecDoc As String
    Dim VehRecNum As Double
    Dim RsComprasRep As Recordset
    Dim Lfecha As String
   
   
    Lfecha = Format(Date, "yyyy-mm-dd")
    
    If LvCompras.SelectedItem Is Nothing Then Exit Sub
    
    With LvCompras.SelectedItem 'S    on compras
        
        Documento = .SubItems(1)
        NumDoc = .SubItems(2)
        RutPrv = .SubItems(3)
        NombreProveedor = .SubItems(4)
        Tipo_Mov = .SubItems(5)
        ValorDoc = .SubItems(8)
        fPago = .SubItems(9)
        VehRecDoc = .SubItems(10)
        VehRecNum = 0 & .SubItems(11)
    End With
    
    If MsgBox("Esta a punto de eliminar " & Documento & " N� " & NumDoc & vbNewLine & _
              "Proveedor " & NombreProveedor & vbNewLine & _
              "Por $" & NumFormat(ValorDoc) & " Neto" & vbNewLine & _
              "Forma de pago:" & fPago & vbNewLine & _
              "Por concepto de " & Tipo_Mov & vbNewLine & _
              "�Confirma eliminaci�n?", vbYesNo) = vbYes Then
              
                Motivo = InputBox("Describa el motivo de la eliminacion")
                If Len(Motivo) = 0 Then
                    MsgBox "Debe ingresar motivo de eliminaci�n"
                    Exit Sub
                End If
              
                Dim RsTmp As Recordset
              
                Sql = "INSERT INTO eliminacion (tipo_movimiento,fecha,tipo_doc,no_documento,nombre,valor_neto,usuario,motivo,concepto) VALUES(" & _
                "'COMPRA','" & Lfecha & "','" & Documento & "'," & NumDoc & ",'" & NombreProveedor & "'," & ValorDoc & ",'" & LogUsuario & "','" & Motivo & "','" & Tipo_Mov & "')"
              
                Debug.Print Sql
                
                cn.Execute Sql
                
                
                
                
                If Tipo_Mov = "COMPRA REPUESTO" Then
                        Filtro = "RUT_PROVEEDOR = '" & RutPrv & "' AND DOCUMENTO_INGRESO ='" & Documento & "' AND NUMERO_DOCUMENTO = " & NumDoc
                        Sql = "SELECT cod_producto,unidades FROM compra_stock_repuestos WHERE " & Filtro
                        Call AbreConsulta(RsComprasRep, Sql)
                        
                        If RsComprasRep.RecordCount > 0 Then
                            With RsComprasRep
                                .MoveFirst
                                Do While Not .EOF
                                    'Actualizamos Stock
                                    Sql = "UPDATE maestro_productos SET stock_actual = stock_actual -" & Replace$(Str(!Unidades), ",", ".") & " WHERE codigo = '" & !cod_producto & "'"
                                    Call AbreConsulta(RsTmp, Sql)
                                    .MoveNext
                                Loop
                            End With
                        End If
                        Sql = "DELETE FROM compra_stock_repuestos WHERE " & Filtro
                        Call AbreConsulta(RsComprasRep, Sql)
                End If
                If Tipo_Mov = "VEHICULO NUEVO" Or Tipo_Mov = "VEHICULO USADO" Then
                        'Eliminamos el vehiculo del maestro vehiculos
                        Filtro = "PROVEEDOR = '" & RutPrv & "' AND tipo_documento ='" & Documento & "' AND NO_DOCUMENTO = " & NumDoc
                        
                        If Tipo_Mov = "VEHICULO USADO" Then
                            If fPago = "Vehiculo Rec. P.pago" Then
                               'Eliminar pago en el doc de compra
                                Sql = "DELETE FROM pagos_clientes WHERE forma_pago ='VEHICULO EN PARTE PAGO' AND tipo_doc ='" & VehRecDoc & "' AND no_doc = " & VehRecNum
                                Debug.Print Sql
                                Call AbreConsulta(RsTmp, Sql)
                                
                                Sql = "DELETE FROM ctacte_clientes WHERE tipo_doc ='" & VehRecDoc & "' AND no_doc = " & VehRecNum & " AND HABER = " & ValorDoc
                                Debug.Print Sql
                                Call AbreConsulta(RsTmp, Sql)
                            End If
                        
                            Sql = "SELECT patente FROM vehiculos WHERE " & Filtro
                            Call AbreConsulta(RsTmp, Sql)
                            If RsTmp.RecordCount > 0 Then
                                Lpatente = RsTmp!patente
                                RsTmp.close 'eliminammos la cta cte del vehiculo
                                Sql = "DELETE FROM cta_cte_vehiculos WHERE patente_chasis ='" & Lpatente & "'"
                                Call AbreConsulta(RsTmp, Sql)
                            End If
                        End If
                        
                        Sql = "DELETE FROM vehiculos WHERE " & Filtro
                        Call AbreConsulta(RsTmp, Sql)
                        
                End If
                'Ahora eliminar el Doc de compra
                Filtro = "RUT = '" & RutPrv & "' AND documento ='" & Documento & "' AND NO_DOCUMENTO = " & NumDoc
                Sql = "DELETE FROM compra_repuesto_documento WHERE " & Filtro
                Call AbreConsulta(RsTmp, Sql)
                
                'Ahora eliminamos Cta Cte. del proveedor
                Filtro = "RUT = '" & RutPrv & "' AND tipo_doc ='" & Documento & "' AND NO_DOC = " & NumDoc
                Sql = "DELETE FROM ctacte_proveedores WHERE " & Filtro
                Call AbreConsulta(RsTmp, Sql)
                     
                'tambien eliminamos los pagos
                Sql = "DELETE FROM pagos_proveedores WHERE " & Filtro
                Call AbreConsulta(RsTmp, Sql)
                
                If MsgBox("�Imprimir combrante de anulaci�n?", vbYesNo + vbQuestion) = vbYes Then
                    'Imprime combrobante anulacion
                    Me.EmiteComprobanteEliminacion
                End If
    End If
    
    Filtrar
   
End Sub

Private Sub Check1_Click()
    If Check1.Value = 1 Then
        Check2.Value = 0
        Check3.Value = 0
        Check4.Value = 0
        Check2.Enabled = False
        Check3.Enabled = False
        Check4.Enabled = False
        chkOrdenDe.Enabled = False
    Else
        Check2.Enabled = True
        Check3.Enabled = True
        Check4.Enabled = True
        chkOrdenDe.Enabled = True
    End If
End Sub
Private Sub CmdFiltro_Click()
    Filtrar
End Sub

Private Sub CmdHistorial_Click()
    HistorialEliminacion.Show
End Sub

Private Sub CmdSalir_Click()
    Unload Me
End Sub


Private Sub chkCompraRepuesto_Click()
    If chkCompraRepuesto.Value = 1 Then
        Me.FraClasificacion.Visible = True
    Else
        Me.FraClasificacion.Visible = False
    End If
        
End Sub

Private Sub chkTodos_Click()
    If chkTodos.Value = 1 Then
        Me.chkCompraRepuesto.Enabled = False
        Me.chkVehiculoNuevo.Enabled = False
        Me.chkVehiculoUsado.Enabled = False
    Else
        Me.chkCompraRepuesto.Enabled = True
        Me.chkVehiculoNuevo.Enabled = True
        Me.chkVehiculoUsado.Enabled = True
    End If
End Sub

Private Sub ChkTodosRepuestos_Click()
    If ChkTodosRepuestos.Value = 1 Then
        Me.chkMultimarca.Enabled = False
        Me.chkCITROEN.Enabled = False
    Else
        Me.chkMultimarca.Enabled = True
        Me.chkCITROEN.Enabled = True
    End If
    
End Sub

Private Sub Form_Load()
    Aplicar_skin Me
    Me.DtInicio.Value = Date
    Me.Dtfin.Value = Date
    Exit Sub
    
    
    

End Sub



Private Sub txtNombreProveedor_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Public Sub Filtrar()
    Dim FiltroDoc As String
    TxtNeto = 0
    TxtIva = 0
    TxtBruto = 0
    LfINI = Format(DtInicio.Value, "yyyy-mm-dd")
    LfFIN = Format(Dtfin.Value, "yyyy-mm-dd")
   
  
    

        Filtro = "1=1"
        If Check1.Value = 1 Then
            'no hacemos filtro de documento
        Else
            FiltroDoc = ""
            If Check2.Value = 1 Then FiltroDoc = "'FACTURA'"
            If Check3.Value = 1 Then If Len(FiltroDoc) > 0 Then FiltroDoc = FiltroDoc & ",'BOLETA'" Else FiltroDoc = "'BOLETA'"
            If Check4.Value = 1 Then If Len(FiltroDoc) > 0 Then FiltroDoc = FiltroDoc & ",'GUIA'" Else FiltroDoc = "'GUIA'"
            If chkOrdenDe.Value = 1 Then If Len(FiltroDoc) > 0 Then FiltroDoc = FiltroDoc & ",'ORDEN DE INGRESO'" Else FiltroDoc = "'ORDEN DE INGRESO'"
            If Len(FiltroDoc) > 0 Then FiltroDoc = " AND DOCUMENTO IN(" & FiltroDoc & ") " Else FiltroDoc = " AND 1=1 "
            'If chkOrdenDe.Value = 1 Then Filtro = Filtro & " Documento = 'ORDEN DE INGRESO' AND"
        End If
        
        
        
        If chkTodos.Value = 1 Then
            'No filtramos por tipo movimiento"
        Else
           
            If chkCompraRepuesto.Value = 1 Then Filtro = Filtro & " TIPO_MOVIMIENTO = 'COMPRA REPUESTO' AND"
            If chkVehiculoNuevo.Value = 1 Then Filtro = Filtro & " TIPO_MOVIMIENTO = 'VEHICULO NUEVO' AND"
            If chkVehiculoUsado.Value = 1 Then Filtro = Filtro & " TIPO_MOVIMIENTO = 'VEHICULO USADO' AND"
        End If
        
        
        If chkCompraRepuesto.Value = 1 Then
            If ChkTodosRepuestos.Value = 1 Then
                'No filtramos clasificacion repuestos
            Else
                If chkCITROEN.Value = 1 Then Filtro = Filtro & " Clasificacion = 1 AND"
                If chkMultimarca.Value = 1 Then Filtro = Filtro & " Clasificacion = 2 AND"
            End If
        
        
        End If
        If Check1.Value <> 1 Then
            If Len(Filtro) > 0 Then Filtro = Mid$(Filtro, 1, Len(Filtro) - 3)
        End If
    
  '   Sql = "SELECT Fecha,documento,No_Documento as No,Rut,Nombre_proveedor,Tipo_Movimiento as Accion,Neto,Iva,total,pago,partepagodoc,partepagondoc,Clasificacion FROM compra_repuesto_documento" & _
          " WHERE (FECHA BETWEEN #" & LfIni & "# AND #" & LfFin & "#) " & "AND pago <> 'Actualizar Inventario' " & _
            IIf(Filtro = "", "", "AND " & Filtro & " ") & IIf(Len(txtNombreProveedor) = 0, "", "AND Nombre_Proveedor Like '" & txtNombreProveedor & "%' ") & _
          " ORDER BY " & IIf(ComOrdenar.ListIndex > -1, ComOrdenar.Text, "Fecha Desc")
    
      Sql = "SELECT fecha,documento,no_documento as No,rut,nombre_proveedor,tipo_movimiento as Accion,Neto,Iva,total,pago,partepagodoc,partepagondoc,Clasificacion " & _
            "FROM compra_repuesto_documento c,sis_documentos d" & _
          " WHERE d.doc_id=c.doc_id AND " & _
          " c.doc_id NOT IN(9,10,15,16) AND (fecha BETWEEN '" & LfINI & "' AND '" & LfFIN & "') " & "  " & _
            IIf(Filtro = "", "", "AND " & Filtro & " ") & IIf(Len(txtNombreProveedor) = 0, "", "AND Nombre_Proveedor Like '" & txtNombreProveedor & "%' ") & _
             FiltroDoc & _
          " ORDER BY " & IIf(ComOrdenar.ListIndex > -1, ComOrdenar.Text, "Fecha ")
    
    paso = AbreConsulta(RsCompras, Sql)
    
    LLenar_Grilla RsCompras, Me, LvCompras, False, True, True, False
    LLenar_Grilla RsCompras, Me, LvExport, False, True, True, False
    
    
    
    SqlNC = "SELECT fecha,documento,no_documento as No,rut,nombre_proveedor,tipo_movimiento as Accion,Neto,Iva,total,pago,partepagodoc,partepagondoc,Clasificacion " & _
            "FROM compra_repuesto_documento c,sis_documentos d" & _
          " WHERE d.doc_id=c.doc_id AND " & _
          " c.doc_id  IN(9,10,15,16) AND (fecha BETWEEN '" & LfINI & "' AND '" & LfFIN & "') " & "  " & _
            IIf(Filtro = "", "", "AND " & Filtro & " ") & IIf(Len(txtNombreProveedor) = 0, "", "AND Nombre_Proveedor Like '" & txtNombreProveedor & "%' ") & _
             FiltroDoc & _
          " ORDER BY " & IIf(ComOrdenar.ListIndex > -1, ComOrdenar.Text, "Fecha ")
    
    AbreConsulta RsTmp, SqlNC
    
    LLenar_Grilla RsTmp, Me, LvNotaCredito, False, True, True, False
    
    
    TxtNeto = 0
    Me.TxtIva = 0
    Me.TxtBruto = 0
    For I = 1 To LvCompras.ListItems.Count
        TxtNeto = Val(TxtNeto) + CDbl(LvCompras.ListItems(I).SubItems(6))
        TxtIva = Val(TxtIva) + CDbl(LvCompras.ListItems(I).SubItems(7))
        TxtBruto = Val(TxtBruto) + CDbl(LvCompras.ListItems(I).SubItems(8))
    Next
    
    txtNetoNC = 0
    Me.txtIvaNC = 0
    Me.txtBrutoNC = 0
    For I = 1 To LvNotaCredito.ListItems.Count
        txtNetoNC = Val(txtNetoNC) + CDbl(LvNotaCredito.ListItems(I).SubItems(6))
        txtIvaNC = Val(txtIvaNC) + CDbl(LvNotaCredito.ListItems(I).SubItems(7))
        txtBrutoNC = Val(txtBrutoNC) + CDbl(LvNotaCredito.ListItems(I).SubItems(8))
    Next
    

    
    txtNetoTotal = NumFormat(TxtNeto - txtNetoNC)
    txtIvaTotal = NumFormat(TxtIva - txtIvaNC)
    txtBrutototal = NumFormat(TxtBruto - txtBrutoNC)
    
    TxtNeto = NumFormat(TxtNeto)
    TxtIva = NumFormat(TxtIva)
    TxtBruto = NumFormat(TxtBruto)
    
    txtNetoNC = NumFormat(txtNetoNC)
    txtIvaNC = NumFormat(txtIvaNC)
    txtBrutoNC = NumFormat(txtBrutoNC)
    
    
 '   SqlNC = "SELECT fecha,no_nc as Nro_Nc,tipo_doc,no_doc,rut,razon_social,neto,bruto-neto as IVA,bruto " & _
           "FROM nc_proveedores " & _
           "WHERE (fecha BETWEEN '" & LfINI & "' AND '" & LfFIN & "') ORDER BY Fecha" ' Desc
    
    
    
  '  ElGrid
End Sub
Public Sub EmiteComprobanteEliminacion()
    Dim Documento As String
    Dim RutPrv As String
    Dim NumDoc As Double
    Dim Tipo_Mov As String
    Dim ValorDoc As Double
    Dim NombreProveedor As String
    Dim fPago As String
    Dim RsComprasRep As Recordset
    Dim Lfecha As String
    
   
    Lfecha = Format(Date, "yyyy-mm-dd")
    
    With GridVentas 'S    on compras
        If .Row = -1 Then Exit Sub
        .Col = 1: Documento = .Text
        .Col = 2: NumDoc = Val(.Text)
        .Col = 3: RutPrv = .Text
        .Col = 4: NombreProveedor = .Text
        .Col = 5: Tipo_Mov = .Text
        .Col = 6: ValorDoc = Val(.Text)
        .Col = 9: fPago = .Text
    End With

    Dim CantCopias As Integer
    Dialogo.Copies = 1
    
    
    Dialogo.ShowPrinter
    
    'llamamos al dialogo de impresion
    CantCopias = Dialogo.Copies
    
    Dim PosicionMo, PosicionMA, PosicionOT
    Dim cEspacios As Integer
    cEspacios = 10
    F = FreeFile '
    Dim Linea As String
   
        Printer.FontName = "Courier New"
        Printer.CurrentY = Printer.CurrentY + (1000 - Printer.CurrentY Mod 1000)
        Call TipoLetra(8, True, False, False)
        
        
        Printer.Print Space(cEspacios) & RsEmpresa  'aqui la enviamos a la impresora
        Printer.Print Space(cEspacios) & DireccionEmpresa  ' direccion
        Printer.Print Space(cEspacios) & GiroEmpresa  'giro
        Printer.Print Space(cEspacios) & RutEmpresa 'rut
        
        Call TipoLetra(14, True, False, False)
        Printer.Print Space(cEspacios) & Space(15) & "COMPROBANTE ELIMINACION" & Space(20)  'titulo
        Call TipoLetra(12, True, False, False)
        Printer.Print Space(cEspacios) & Space(23) & "DOCUMENTO DE INGRESO" & Space(20)  'titulo
        Call TipoLetra(10, True, False, False)
        Printer.Print Space(cEspacios) & Space(30) & Now
        
        paso = TipoLetra(8, False, False, False)
        Printer.CurrentY = Printer.CurrentY + 80
    
        Printer.Print Space(cEspacios) & "PROVEEDOR    :"; NombreProveedor
        Printer.Print Space(cEspacios) & "RUT PROVEEDOR:"; RutPrv
        Printer.Print Space(cEspacios) & "DOCUMENTO    :"; Documento
        Printer.Print Space(cEspacios) & "N� DOCUMENTO :"; NumDoc
        Printer.Print Space(cEspacios) & "VALOR NETO   :"; NumFormat(ValorDoc)
        Printer.Print Space(cEspacios) & "CONCEPTO     :"; Tipo_Mov
        Printer.Print Space(cEspacios) & "USUARIO      :"; LogUsuario
        Call TipoLetra(8, True, False, False)
        Printer.Print ""
        Printer.Print ""
        Printer.Print Space(cEspacios) & "MOTIVO ELIMINACION:"; Motivo
        
        Printer.CurrentY = Printer.CurrentY + 100
        
        
    
        paso = TipoLetra(10, True, False, False)
        
        
        Printer.CurrentY = Printer.CurrentY + 100
        
        
       Printer.Print Space(cEspacios) & "La eliminacion actualiza el stock y elimina todos los documentos"
       Printer.Print Space(cEspacios) & "asociados a la compra incluidos los pagos."
                                                                      
    
    
    
    
    Printer.DrawWidth = 1
    
    Printer.Line (400, 700)-(11000, 2550), , B 'Rectangulo Encabezado y N� Orden
    Printer.Line (400, 2550)-(11000, 3900), , B 'Rectangulo Datos del cliente
    Printer.Line (400, 3900)-(11000, 6150), , B ' Rectangulo Datos del vehiculo
    Printer.DrawWidth = 3
    Printer.NewPage 'activar lo enviado a la
    Printer.EndDoc 'impresora, o sea , que imprima lo que le hemos enviado mediante los printer
    
    
    
    
    
    Exit Sub
    
    


ErrHandler:
    ' El usuario ha hecho clic en el bot�n Cancelar
    MsgBox "La impresion ha sido cancelada"

    Exit Sub


End Sub

