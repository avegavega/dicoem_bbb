VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "MSDATGRD.OCX"
Begin VB.Form HistorialEliminacion 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Eliminaciones"
   ClientHeight    =   5475
   ClientLeft      =   30
   ClientTop       =   300
   ClientWidth     =   11130
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5475
   ScaleWidth      =   11130
   StartUpPosition =   2  'CenterScreen
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   360
      OleObjectBlob   =   "HistorialEliminacion.frx":0000
      Top             =   4800
   End
   Begin VB.Frame Frame1 
      Caption         =   "Historial de eliminaciones"
      Height          =   5172
      Left            =   120
      TabIndex        =   0
      Top             =   240
      Width           =   10932
      Begin VB.CommandButton Command1 
         Cancel          =   -1  'True
         Caption         =   "&Retornar"
         Height          =   372
         Left            =   9720
         TabIndex        =   2
         Top             =   4680
         Width           =   972
      End
      Begin MSDataGridLib.DataGrid GridEliminados 
         Height          =   4092
         Left            =   120
         TabIndex        =   1
         Top             =   480
         Width           =   10572
         _ExtentX        =   18653
         _ExtentY        =   7223
         _Version        =   393216
         AllowUpdate     =   0   'False
         AllowArrows     =   0   'False
         HeadLines       =   1
         RowHeight       =   15
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnCount     =   2
         BeginProperty Column00 
            DataField       =   ""
            Caption         =   ""
            BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
               Type            =   0
               Format          =   ""
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   3082
               SubFormatType   =   0
            EndProperty
         EndProperty
         BeginProperty Column01 
            DataField       =   ""
            Caption         =   ""
            BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
               Type            =   0
               Format          =   ""
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   3082
               SubFormatType   =   0
            EndProperty
         EndProperty
         SplitCount      =   1
         BeginProperty Split0 
            SizeMode        =   1
            BeginProperty Column00 
            EndProperty
            BeginProperty Column01 
            EndProperty
         EndProperty
      End
   End
End
Attribute VB_Name = "HistorialEliminacion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim RsEliminacion As Recordset

Private Sub Command1_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    Aplicar_skin Me
    Call Consulta(RsEliminacion, "SELECT * FROM eliminacion ORDER BY fecha DESC")
    With GridEliminados
        Set .DataSource = RsEliminacion
        
        .Columns(1).Width = 1000
        .Columns(3).Alignment = dbgRight
        .Columns(5).Alignment = dbgRight
    End With
End Sub
