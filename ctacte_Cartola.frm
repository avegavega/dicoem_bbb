VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Object = "{DA729E34-689F-49EA-A856-B57046630B73}#1.0#0"; "progressbar-xp.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form ctacte_Cartola 
   Caption         =   "Cartola Cuenta Corriente"
   ClientHeight    =   10560
   ClientLeft      =   3885
   ClientTop       =   855
   ClientWidth     =   14055
   LinkTopic       =   "Form1"
   ScaleHeight     =   10560
   ScaleWidth      =   14055
   Begin VB.TextBox TxtCiudad 
      Height          =   450
      Left            =   13050
      TabIndex        =   47
      Text            =   "Text1"
      Top             =   1350
      Visible         =   0   'False
      Width           =   705
   End
   Begin VB.TextBox txtDireccion 
      Height          =   570
      Left            =   13035
      TabIndex        =   46
      Text            =   "Text1"
      Top             =   645
      Visible         =   0   'False
      Width           =   840
   End
   Begin MSComctlLib.ListView LvEmpresa 
      Height          =   1785
      Left            =   12300
      TabIndex        =   45
      Top             =   2505
      Visible         =   0   'False
      Width           =   5535
      _ExtentX        =   9763
      _ExtentY        =   3149
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   0   'False
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   5
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Object.Tag             =   "T1000"
         Text            =   "RUT"
         Object.Width           =   1499
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   1
         Object.Tag             =   "T1000"
         Text            =   "nombre"
         Object.Width           =   1676
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Object.Tag             =   "T1000"
         Text            =   "direccion"
         Object.Width           =   1940
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Object.Tag             =   "T1000"
         Text            =   "giro"
         Object.Width           =   1940
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   4
         Key             =   "saldito"
         Object.Tag             =   "T1000"
         Text            =   "ciudad"
         Object.Width           =   2117
      EndProperty
   End
   Begin VB.Frame FraProgreso 
      Caption         =   "Progreso exportaci�n"
      Height          =   795
      Left            =   510
      TabIndex        =   11
      Top             =   6045
      Visible         =   0   'False
      Width           =   11385
      Begin Proyecto2.XP_ProgressBar BarraProgreso 
         Height          =   330
         Left            =   150
         TabIndex        =   12
         Top             =   315
         Width           =   11130
         _ExtentX        =   19632
         _ExtentY        =   582
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BrushStyle      =   0
         Color           =   16777088
         Scrolling       =   1
         ShowText        =   -1  'True
      End
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   5880
      Left            =   150
      TabIndex        =   30
      Top             =   4005
      Width           =   12885
      _ExtentX        =   22728
      _ExtentY        =   10372
      _Version        =   393216
      Tabs            =   2
      Tab             =   1
      TabsPerRow      =   2
      TabHeight       =   520
      TabCaption(0)   =   "Detalle por documentols"
      TabPicture(0)   =   "ctacte_Cartola.frx":0000
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "Frame4"
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "Cartola Historica"
      TabPicture(1)   =   "ctacte_Cartola.frx":001C
      Tab(1).ControlEnabled=   -1  'True
      Tab(1).Control(0)=   "SkinLabel9(1)"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).Control(1)=   "SkinLabel10"
      Tab(1).Control(1).Enabled=   0   'False
      Tab(1).Control(2)=   "DtDesdeCartola"
      Tab(1).Control(2).Enabled=   0   'False
      Tab(1).Control(3)=   "LvCartolaHistorica"
      Tab(1).Control(3).Enabled=   0   'False
      Tab(1).Control(4)=   "Frame6"
      Tab(1).Control(4).Enabled=   0   'False
      Tab(1).Control(5)=   "CmdLaserTinta"
      Tab(1).Control(5).Enabled=   0   'False
      Tab(1).Control(6)=   "DtHastaCartola"
      Tab(1).Control(6).Enabled=   0   'False
      Tab(1).Control(7)=   "CmdFiltroHasta"
      Tab(1).Control(7).Enabled=   0   'False
      Tab(1).Control(8)=   "SkinLabel9(0)"
      Tab(1).Control(8).Enabled=   0   'False
      Tab(1).ControlCount=   9
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel9 
         Height          =   165
         Index           =   0
         Left            =   2310
         OleObjectBlob   =   "ctacte_Cartola.frx":0038
         TabIndex        =   51
         Top             =   5175
         Width           =   780
      End
      Begin VB.CommandButton CmdFiltroHasta 
         Caption         =   "Filtrar hasta"
         Height          =   375
         Left            =   4755
         TabIndex        =   49
         Top             =   5175
         Width           =   1290
      End
      Begin MSComCtl2.DTPicker DtHastaCartola 
         Height          =   285
         Left            =   3180
         TabIndex        =   48
         Top             =   5520
         Width           =   1440
         _ExtentX        =   2540
         _ExtentY        =   503
         _Version        =   393216
         Format          =   273416193
         CurrentDate     =   42462
      End
      Begin VB.CommandButton CmdLaserTinta 
         BackColor       =   &H00FFFFFF&
         Height          =   705
         Left            =   450
         Picture         =   "ctacte_Cartola.frx":00A0
         Style           =   1  'Graphical
         TabIndex        =   44
         Top             =   5040
         Width           =   1245
      End
      Begin VB.Frame Frame6 
         Caption         =   "Totales"
         Height          =   765
         Left            =   6060
         TabIndex        =   37
         Top             =   5025
         Width           =   5700
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
            Height          =   195
            Left            =   255
            OleObjectBlob   =   "ctacte_Cartola.frx":053D
            TabIndex        =   41
            Top             =   225
            Width           =   1425
         End
         Begin VB.TextBox TxtCtSaldo 
            Alignment       =   1  'Right Justify
            BackColor       =   &H8000000F&
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   4140
            Locked          =   -1  'True
            TabIndex        =   40
            Text            =   "0"
            ToolTipText     =   "Puede utilizar este saldo para pagar documentos pendientes"
            Top             =   435
            Width           =   1500
         End
         Begin VB.TextBox TxtCtAbonos 
            Alignment       =   1  'Right Justify
            BackColor       =   &H8000000F&
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   1740
            Locked          =   -1  'True
            TabIndex        =   39
            Text            =   "0"
            ToolTipText     =   "Puede utilizar este saldo para pagar documentos pendientes"
            Top             =   420
            Width           =   1500
         End
         Begin VB.TextBox TxtCtVentas 
            Alignment       =   1  'Right Justify
            BackColor       =   &H8000000F&
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   225
            Locked          =   -1  'True
            TabIndex        =   38
            Text            =   "0"
            ToolTipText     =   "Puede utilizar este saldo para pagar documentos pendientes"
            Top             =   420
            Width           =   1500
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel5 
            Height          =   195
            Left            =   1800
            OleObjectBlob   =   "ctacte_Cartola.frx":05A5
            TabIndex        =   42
            Top             =   225
            Width           =   1425
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel8 
            Height          =   195
            Left            =   4200
            OleObjectBlob   =   "ctacte_Cartola.frx":060D
            TabIndex        =   43
            Top             =   240
            Width           =   1425
         End
      End
      Begin VB.Frame Frame4 
         Caption         =   "DETALLE POR DOCUMENTOS"
         Height          =   5190
         Left            =   -74760
         TabIndex        =   31
         Top             =   330
         Width           =   12450
         Begin VB.CommandButton cmdExportar 
            Caption         =   "Exportar Lista a Excel"
            Height          =   330
            Left            =   270
            TabIndex        =   34
            ToolTipText     =   "Exportar"
            Top             =   4665
            Width           =   1650
         End
         Begin VB.Frame Frame5 
            Caption         =   "Saldo a favor"
            Height          =   570
            Left            =   2670
            TabIndex        =   32
            Top             =   4635
            Width           =   3885
            Begin VB.TextBox TxtSaldoAfavor 
               Alignment       =   1  'Right Justify
               Height          =   285
               Left            =   225
               Locked          =   -1  'True
               TabIndex        =   33
               Text            =   "0"
               ToolTipText     =   "Puede utilizar este saldo para pagar documentos pendientes"
               Top             =   195
               Width           =   3570
            End
         End
         Begin MSComctlLib.ListView LvDetalle 
            Height          =   4335
            Left            =   270
            TabIndex        =   35
            Top             =   315
            Width           =   12030
            _ExtentX        =   21220
            _ExtentY        =   7646
            View            =   3
            Arrange         =   1
            LabelEdit       =   1
            LabelWrap       =   0   'False
            HideSelection   =   0   'False
            FullRowSelect   =   -1  'True
            GridLines       =   -1  'True
            _Version        =   393217
            Icons           =   "ImageList1"
            SmallIcons      =   "ImageList1"
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   15
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Object.Tag             =   "N100"
               Text            =   "Id Unico"
               Object.Width           =   0
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Key             =   "vinculo"
               Object.Tag             =   "N109"
               Text            =   "Tipo y Detalle de Documento"
               Object.Width           =   4939
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   2
               Object.Tag             =   "N109"
               Text            =   "Numero"
               Object.Width           =   1676
            EndProperty
            BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   3
               Object.Tag             =   "F1000"
               Text            =   "Fecha Emision"
               Object.Width           =   1764
            EndProperty
            BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   4
               Object.Tag             =   "F1000"
               Text            =   "Fecha Ven."
               Object.Width           =   1764
            EndProperty
            BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   5
               Object.Tag             =   "T1000"
               Text            =   "Correlativo"
               Object.Width           =   1499
            EndProperty
            BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   6
               Key             =   "debitos"
               Object.Tag             =   "N100"
               Text            =   "Debe"
               Object.Width           =   2293
            EndProperty
            BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   7
               Key             =   "creditos"
               Object.Tag             =   "N100"
               Text            =   "Haber"
               Object.Width           =   2293
            EndProperty
            BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   8
               Key             =   "debitosn"
               Object.Tag             =   "N100"
               Text            =   "Saldo Documento"
               Object.Width           =   2117
            EndProperty
            BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   9
               Object.Tag             =   "N100"
               Text            =   "Saldo General"
               Object.Width           =   2117
            EndProperty
            BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   10
               Key             =   "creditosn"
               Object.Tag             =   "N109"
               Text            =   "Che Numero"
               Object.Width           =   2293
            EndProperty
            BeginProperty ColumnHeader(12) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   11
               Object.Tag             =   "T100"
               Text            =   "Banco"
               Object.Width           =   2646
            EndProperty
            BeginProperty ColumnHeader(13) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   12
               Text            =   "mpa_id (3= cheque"
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(14) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   13
               Text            =   "abo_id_unioco"
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(15) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   14
               Text            =   "Doc ID"
               Object.Width           =   2540
            EndProperty
         End
      End
      Begin MSComctlLib.ListView LvCartolaHistorica 
         Height          =   4335
         Left            =   450
         TabIndex        =   36
         Top             =   675
         Width           =   12030
         _ExtentX        =   21220
         _ExtentY        =   7646
         View            =   3
         Arrange         =   1
         LabelEdit       =   1
         LabelWrap       =   0   'False
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         Icons           =   "ImageList1"
         SmallIcons      =   "ImageList1"
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   11
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "F1000"
            Text            =   "Fecha"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Key             =   "vinculo"
            Object.Tag             =   "T1000"
            Text            =   "Documento"
            Object.Width           =   4939
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   2
            Object.Tag             =   "N109"
            Text            =   "Numero"
            Object.Width           =   1940
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Object.Tag             =   "F1000"
            Text            =   "Vencimiento"
            Object.Width           =   1940
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   4
            Key             =   "venta"
            Object.Tag             =   "N100"
            Text            =   "Monto Venta"
            Object.Width           =   2293
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   5
            Key             =   "abono"
            Object.Tag             =   "N100"
            Text            =   "Monto Abono "
            Object.Width           =   2293
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   6
            Key             =   "debitos"
            Object.Tag             =   "N100"
            Text            =   "Ref."
            Object.Width           =   2293
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   7
            Key             =   "saldo"
            Object.Tag             =   "N100"
            Text            =   "Total Saldo"
            Object.Width           =   2293
         EndProperty
         BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   8
            Key             =   "debitosn"
            Object.Tag             =   "N100"
            Text            =   "id doc"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   9
            Object.Tag             =   "N100"
            Text            =   "doc_id"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   10
            Object.Tag             =   "N109"
            Text            =   "tipo pago"
            Object.Width           =   2540
         EndProperty
      End
      Begin MSComCtl2.DTPicker DtDesdeCartola 
         Height          =   285
         Left            =   3195
         TabIndex        =   50
         Top             =   5145
         Width           =   1440
         _ExtentX        =   2540
         _ExtentY        =   503
         _Version        =   393216
         Format          =   273416193
         CurrentDate     =   42462
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel10 
         Height          =   165
         Left            =   0
         OleObjectBlob   =   "ctacte_Cartola.frx":0675
         TabIndex        =   52
         Top             =   0
         Width           =   780
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel9 
         Height          =   165
         Index           =   1
         Left            =   2325
         OleObjectBlob   =   "ctacte_Cartola.frx":06DD
         TabIndex        =   53
         Top             =   5535
         Width           =   780
      End
   End
   Begin VB.Frame FraFiltros 
      Caption         =   "Filtros"
      Height          =   2415
      Left            =   345
      TabIndex        =   13
      Top             =   1230
      Width           =   6255
      Begin VB.CheckBox ChkFecha 
         Height          =   225
         Left            =   240
         TabIndex        =   24
         Top             =   270
         Width           =   225
      End
      Begin VB.CommandButton CmdFiltro 
         Caption         =   "Filtrar"
         Height          =   375
         Left            =   5160
         TabIndex        =   23
         Top             =   1680
         Width           =   855
      End
      Begin VB.Frame Frame3 
         Caption         =   "Documento"
         Height          =   1455
         Left            =   1800
         TabIndex        =   18
         Top             =   600
         Width           =   3135
         Begin VB.ComboBox CboDoc 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   135
            Style           =   2  'Dropdown List
            TabIndex        =   22
            Top             =   600
            Width           =   2580
         End
         Begin VB.TextBox txtNumero 
            Alignment       =   1  'Right Justify
            Enabled         =   0   'False
            Height          =   285
            Left            =   1080
            TabIndex        =   20
            Top             =   975
            Width           =   1695
         End
         Begin VB.CheckBox ChkDocumento 
            Caption         =   "Todos"
            Height          =   255
            Left            =   1080
            TabIndex        =   19
            Top             =   240
            Value           =   1  'Checked
            Width           =   1095
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel7 
            Height          =   270
            Left            =   120
            OleObjectBlob   =   "ctacte_Cartola.frx":0745
            TabIndex        =   21
            Top             =   960
            Width           =   855
         End
      End
      Begin VB.Frame Frame2 
         Caption         =   "Saldo"
         Height          =   1455
         Left            =   120
         TabIndex        =   14
         Top             =   600
         Width           =   1575
         Begin VB.OptionButton Option1 
            Caption         =   "Todos"
            Height          =   255
            Index           =   2
            Left            =   120
            TabIndex        =   17
            Top             =   1080
            Width           =   1695
         End
         Begin VB.OptionButton Option1 
            Caption         =   "Solo sin saldo"
            Height          =   255
            Index           =   1
            Left            =   120
            TabIndex        =   16
            Top             =   720
            Width           =   1815
         End
         Begin VB.OptionButton Option1 
            Caption         =   "Solo con saldo"
            Height          =   255
            Index           =   0
            Left            =   120
            TabIndex        =   15
            Top             =   360
            Value           =   -1  'True
            Width           =   2055
         End
      End
      Begin MSComCtl2.DTPicker DtDesde 
         CausesValidation=   0   'False
         Height          =   285
         Left            =   510
         TabIndex        =   25
         Top             =   240
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   503
         _Version        =   393216
         Enabled         =   0   'False
         Format          =   273416193
         CurrentDate     =   41001
      End
      Begin MSComCtl2.DTPicker DtHasta 
         Height          =   285
         Left            =   1710
         TabIndex        =   26
         Top             =   240
         Width           =   1200
         _ExtentX        =   2117
         _ExtentY        =   503
         _Version        =   393216
         Enabled         =   0   'False
         Format          =   273416193
         CurrentDate     =   41001
      End
   End
   Begin VB.Timer TimEspera 
      Enabled         =   0   'False
      Interval        =   5
      Left            =   7320
      Top             =   8760
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Retornar"
      Height          =   345
      Left            =   11400
      TabIndex        =   10
      Top             =   10065
      Width           =   1650
   End
   Begin VB.Frame FraCliPro 
      Caption         =   "Identificacion"
      Height          =   960
      Left            =   360
      TabIndex        =   0
      Top             =   150
      Width           =   12615
      Begin VB.TextBox TxtRut 
         BackColor       =   &H0080FF80&
         Height          =   315
         Left            =   825
         TabIndex        =   4
         ToolTipText     =   "Ingrese el RUT sin puntos ni guion."
         Top             =   480
         Width           =   1560
      End
      Begin VB.TextBox txtCliente 
         BackColor       =   &H00E0E0E0&
         Height          =   315
         Left            =   2385
         Locked          =   -1  'True
         TabIndex        =   3
         Top             =   480
         Width           =   5145
      End
      Begin VB.TextBox TxtFono 
         BackColor       =   &H00E0E0E0&
         Height          =   315
         Left            =   10425
         Locked          =   -1  'True
         TabIndex        =   2
         Top             =   480
         Width           =   1350
      End
      Begin VB.TextBox TxtMail 
         BackColor       =   &H00E0E0E0&
         Height          =   315
         Left            =   7530
         Locked          =   -1  'True
         TabIndex        =   1
         Top             =   480
         Width           =   2895
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel16 
         Height          =   270
         Left            =   2415
         OleObjectBlob   =   "ctacte_Cartola.frx":07C3
         TabIndex        =   5
         Top             =   300
         Width           =   540
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   195
         Left            =   10410
         OleObjectBlob   =   "ctacte_Cartola.frx":082D
         TabIndex        =   6
         Top             =   300
         Width           =   540
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   225
         Left            =   7530
         OleObjectBlob   =   "ctacte_Cartola.frx":0893
         TabIndex        =   7
         Top             =   300
         Width           =   540
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   165
         Left            =   840
         OleObjectBlob   =   "ctacte_Cartola.frx":08FD
         TabIndex        =   8
         Top             =   300
         Width           =   540
      End
   End
   Begin VB.Timer Timer1 
      Interval        =   10
      Left            =   -195
      Top             =   795
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   -45
      OleObjectBlob   =   "ctacte_Cartola.frx":0967
      Top             =   480
   End
   Begin MSComctlLib.ListView LvResumen 
      Height          =   1785
      Left            =   7320
      TabIndex        =   9
      Top             =   1320
      Width           =   5535
      _ExtentX        =   9763
      _ExtentY        =   3149
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   0   'False
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   7
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Object.Tag             =   "N109"
         Text            =   "Id Unico"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Object.Tag             =   "N109"
         Text            =   "Tipo Doc"
         Object.Width           =   1499
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   2
         Object.Tag             =   "N109"
         Text            =   "Numero"
         Object.Width           =   1676
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Object.Tag             =   "F1000"
         Text            =   "Fecha Emision"
         Object.Width           =   1940
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Object.Tag             =   "F1000"
         Text            =   "Fecha Ven."
         Object.Width           =   1940
      EndProperty
      BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   5
         Key             =   "saldito"
         Object.Tag             =   "N100"
         Text            =   "Saldo"
         Object.Width           =   2117
      EndProperty
      BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   6
         Object.Tag             =   "N109"
         Text            =   "Id Asiento"
         Object.Width           =   0
      EndProperty
   End
   Begin VB.Frame Frame1 
      Caption         =   "TOTALES POR DOCUMENTOS"
      Height          =   2415
      Left            =   7200
      TabIndex        =   27
      Top             =   1080
      Width           =   5775
      Begin VB.TextBox TxtResumen 
         Alignment       =   1  'Right Justify
         BackColor       =   &H0080FF80&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00C00000&
         Height          =   285
         Left            =   4200
         TabIndex        =   29
         Top             =   2040
         Width           =   1200
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel6 
         Height          =   285
         Left            =   2400
         OleObjectBlob   =   "ctacte_Cartola.frx":0B9B
         TabIndex        =   28
         Top             =   2040
         Width           =   1785
      End
   End
   Begin MSComDlg.CommonDialog Dialogo 
      Left            =   1260
      Top             =   -120
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
End
Attribute VB_Name = "ctacte_Cartola"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public Sm_Rut As String
Public Sm_Cliente As String
Public Sm_Mail As String
Public Sm_Fono As String
Public Sm_CliPro As String
Public Sm_Direccion As String
Public Sm_Ciudad As String
Dim Im_C As Integer
Dim Sm_Saldo As String
Dim Sm_Fecha As String
Dim Sm_Documento As String
Dim Cy As Double
Dim Cx As Double
Dim Dp As Double
Dim P_Fecha As String * 10
Dim P_Documento As String * 25
Dim P_Numero As String * 11
Dim P_Vence As String * 10
Dim P_Venta As String * 12
Dim P_Pago As String * 12
Dim P_Saldo As String * 12
Dim Im_Paginas As Integer
Dim Ip_Paginas As Integer


Dim Sp_Filtro As String
Dim Sp_FiltroAbonos As String



Private Sub ChkDocumento_Click()
    If ChkDocumento.Value = 0 Then
        CboDoc.Enabled = True
        txtNumero.Enabled = True
    Else
        CboDoc.Enabled = False
        txtNumero.Enabled = False
    
    End If
    
End Sub

Private Sub ChkFecha_Click()
    If ChkFecha.Value = 1 Then
        DtDesde.Enabled = True
        DtHasta.Enabled = True
    Else
        DtDesde.Enabled = False
        DtHasta.Enabled = False
    End If
End Sub

Private Sub CmdExportar_Click()
 Dim tit(2) As String
    If LvDetalle.ListItems.Count = 0 Then Exit Sub
    FraProgreso.Visible = True
    tit(0) = "CARTOLA DE CUENTA CORRIENTE"
    tit(1) = " RUT CLIENTE   : " & TxtRut
    tit(2) = " CLIENTE          : " & txtCliente
    ExportarNuevo LvDetalle, tit, Me, BarraProgreso
    FraProgreso.Visible = False
End Sub

Private Sub CmdFiltro_Click()
    TxtResumen = "0"
    Sm_Saldo = ""
    Sm_Fecha = ""
    Sm_Documento = ""
    If Option1(0) Then Sm_Saldo = " HAVING saldo>0"
    If Option1(1) Then Sm_Saldo = " HAVING saldo=0"
    If ChkFecha.Value = 1 Then Sm_Fecha = " AND fecha BETWEEN '" & Fql(DtDesde) & "' AND '" & Fql(DtHasta) & "' "
    If ChkDocumento.Value = 0 Then
        If Val(txtNumero) = 0 Then
            MsgBox "Falta Nro de documento..."
            Exit Sub
        End If
        Sm_Documento = " AND (v.doc_id=" & CboDoc.ItemData(CboDoc.ListIndex) & " AND no_documento=" & txtNumero & ") "
        
    End If
    LvResumen.ListItems.Clear
    LvDetalle.ListItems.Clear
    Carga
End Sub

Private Sub CmdFiltroHasta_Click()
    Sp_Filtro = " AND (fecha BETWEEN '" & Fql(Me.DtDesdeCartola) & "' AND '" & Fql(Me.DtHastaCartola) & "')  "
    
    Sp_FiltroAbonos = " AND (abo_fecha BETWEEN '" & Fql(Me.DtDesdeCartola) & "' AND '" & Fql(DtHastaCartola) & "')  "
    LvCartolaHistorica.ListItems.Clear
    CartolaHistorica
    
End Sub

Private Sub CmdLaserTinta_Click()
    
    Dialogo.CancelError = True
    On Error GoTo CancelaImpesion
    Dialogo.ShowPrinter
    
    
    Sql = "SELECT rut,nombre_empresa,direccion,ciudad,giro " & _
            "FROM sis_empresas " & _
            "WHERE rut='" & SP_Rut_Activo & "'"
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvEmpresa, True, True, False, False
    
    Printer.Orientation = vbPRORPortrait
    Printer.ScaleMode = vbCentimeters
    ImprimeCartola
    
    Exit Sub
CancelaImpesion:
    'NO QUISO IMPRIMIR

    
 End Sub

Private Sub Command1_Click()
    Unload Me
End Sub



Private Sub Form_Load()
    Centrar Me
    Skin2 Me, , 7
    TxtRut = Sm_Rut
    Sp_Filtro = ""
    Sp_FiltroAbonos = ""
    Sql = "SELECT nombre_rsocial,email,fono,ciudad,direccion " & _
            "FROM maestro_clientes " & _
            "WHERE rut_cliente='" & TxtRut & "'"
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        Sm_Cliente = "" & RsTmp!nombre_rsocial
        Sm_Mail = "" & RsTmp!Email
        Sm_Fono = "" & RsTmp!fono
        Sm_Direccion = "" & RsTmp!direccion
        Sm_Ciudad = "" & RsTmp!ciudad
    End If
        
    txtCliente = Sm_Cliente
    TxtMail = Sm_Mail
    txtFono = Sm_Fono
    TxtDireccion = Sm_Direccion
    TxtCiudad = Sm_Ciudad
    LLenarCombo Me.CboDoc, "doc_nombre", "doc_id", "sis_documentos", "doc_activo='SI' AND doc_documento=" & IIf(Sm_CliPro = "CLI", "'VENTA'", "'COMPRA'"), "doc_id"
    CboDoc.ListIndex = 0
    DtDesde = Date - 30
    DtHasta = Date
    Sql = "SELECT IFNULL(SUM(pzo_abono)- SUM(pzo_cargo),0) saldo " & _
                "FROM cta_pozo " & _
                "WHERE rut_emp = '" & SP_Rut_Activo & "' AND pzo_rut = '" & TxtRut & "' AND pzo_cli_pro = '" & Sm_CliPro & "'"
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then TxtSaldoAfavor = NumFormat(RsTmp!saldo)
        
        
    DtHastaCartola = Date
    
    CmdFiltro_Click
    If Sm_CliPro = "CLI" Then CartolaHistoricaClientes
    If Sm_CliPro = "PRO" Then CartolaHistoricaProveedores
    
End Sub
Private Sub CartolaHistoricaProveedores()

        Dim Lp_I As Long
    Dim Lp_SuperSaldo As Long
    Dim Rp_Cartola As Recordset
    Dim Rp_Docs As Recordset
    Dim Sp_AbonosId As String
    Dim Sp_DocumentosID As String
    Dim Lp_SaldoAnterior As Long

    
                    If 1 = 2 Then 'Excluyamos esto por ahora
    
    
                            If Len(Sp_FiltroAbonos) > 0 Then
                                Lp_SaldoAnterior = ConsultaSaldoClienteConFecha(TxtRut, Fql(Me.DtDesdeCartola))
                                If Lp_SaldoAnterior <> 0 Then
                                    LvCartolaHistorica.ListItems.Add , , ""
                                    Lp_I = LvCartolaHistorica.ListItems.Count
                                    LvCartolaHistorica.ListItems(Lp_I).SubItems(1) = "SALDO ANTERIOR"
                                    LvCartolaHistorica.ListItems(Lp_I).SubItems(2) = ""
                                    LvCartolaHistorica.ListItems(Lp_I).SubItems(3) = ""
                                    LvCartolaHistorica.ListItems(Lp_I).SubItems(4) = NumFormat(Lp_SaldoAnterior)
                                    LvCartolaHistorica.ListItems(Lp_I).SubItems(5) = 0
                                    LvCartolaHistorica.ListItems(Lp_I).SubItems(6) = 0
                                    LvCartolaHistorica.ListItems(Lp_I).SubItems(8) = 0
                                    LvCartolaHistorica.ListItems(Lp_I).SubItems(9) = 0
                                
                                End If
                            
                            End If
                    End If
    
                
    
    
    
                    Sql = "SELECT id " & _
                        "FROM com_doc_compra v " & _
                        "WHERE v.rut_emp='" & SP_Rut_Activo & "' AND rut='" & TxtRut & "' " & Sp_Filtro & _
                        "ORDER BY fecha "
                    Consulta RsTmp, Sql
                   ' LLenar_Grilla RsTmp, Me, LvCartolita, False, True, True, False
                    
                    
                    If RsTmp.RecordCount > 0 Then
                        RsTmp.MoveFirst
                        Do While Not RsTmp.EOF
                                Sp_AbonosId = "" 'abonos realizados
                                Sql = "SELECT a.abo_id " & _
                                        "FROM cta_abonos a " & _
                                        "JOIN cta_abono_documentos d ON a.abo_id=d.abo_id " & _
                                        "WHERE abo_cli_pro = 'PRO' AND abo_rut = '" & TxtRut & "' " & _
                                                "AND a.rut_emp = '" & SP_Rut_Activo & "' AND d.rut_emp='" & SP_Rut_Activo & "' " & Sp_FiltroAbonos & " " & _
                                                "AND d.id=" & RsTmp!Id
                            
                                Consulta Rp_Cartola, Sql
                                
                                If Rp_Cartola.RecordCount > 0 Then 'Detectamos los abonos realizados
                                    Rp_Cartola.MoveFirst
                                    Do While Not Rp_Cartola.EOF
                                        Sp_AbonosId = Sp_AbonosId & Rp_Cartola!abo_id & ","
                                        Rp_Cartola.MoveNext
                                    Loop
                                    Sp_AbonosId = Mid(Sp_AbonosId, 1, Len(Sp_AbonosId) - 1)
                                End If
                                Sp_DocumentosID = ""
                                If Len(Sp_AbonosId) > 0 Then
                                    'Ahora se deberan buscar los documento(s) que se incluyeron en este pago(s)
                                    Sql = "SELECT d.id /*doc_nombre,no_documento,fecha,bruto */ " & _
                                            "FROM cta_abono_documentos d " & _
                                            "JOIN cta_abonos a ON d.abo_id=a.abo_id " & _
                                            "JOIN com_doc_compra v ON d.id=v.id " & _
                                            "JOIN sis_documentos x ON v.doc_id=x.doc_id " & _
                                            "WHERE a.abo_cli_pro='PRO' AND d.abo_id IN(" & Sp_AbonosId & ") " & _
                                            "AND v.rut_emp='" & SP_Rut_Activo & "' AND d.rut_emp='" & SP_Rut_Activo & "' " & Sp_Filtro & Sp_FiltroAbonos & "  " & _
                                            "GROUP BY d.id " & _
                                            "ORDER BY v.fecha"
                                    Consulta Rp_Docs, Sql
                                    If Rp_Docs.RecordCount > 0 Then
                                        Rp_Docs.MoveFirst
                                        Do While Not Rp_Docs.EOF
                                                Sp_DocumentosID = Sp_DocumentosID & Rp_Docs!Id & ","
                                        
                                        
                                            Rp_Docs.MoveNext
                                        Loop
                                        Sp_DocumentosID = Mid(Sp_DocumentosID, 1, Len(Sp_DocumentosID) - 1)
                                    End If
                                End If
                                
                                If Len(Sp_DocumentosID) > 0 Then 'Documentos
                                        Sql = "SELECT fecha,doc_nombre,no_documento,doc_fecha_vencimiento vence,IF(doc_signo_libro='+',total,total*-1) bruto, 0 pago, 0 saldo,id,v.doc_id " & _
                                                "FROM com_doc_compra v " & _
                                                "JOIN sis_documentos d ON v.doc_id=d.doc_id " & _
                                                "WHERE v.id IN(" & Sp_DocumentosID & ") " & Sp_Filtro & " " & _
                                                "ORDER BY fecha "
                                        Consulta RsTmp2, Sql
                                        
                                        If RsTmp2.RecordCount > 0 Then
                                                    With RsTmp2
                                                        .MoveFirst
                                                        Do While Not .EOF
                                                            yaesta = False
                                                            For i = 1 To LvCartolaHistorica.ListItems.Count
                                                                If LvCartolaHistorica.ListItems(i).SubItems(8) = !Id Then
                                                                    yaesta = True
                                                                    Exit For
                                                                End If
                                                            
                                                            
                                                            Next
                                                            If yaesta = False Then
                                                            
                                                                LvCartolaHistorica.ListItems.Add , , !Fecha
                                                                Lp_I = LvCartolaHistorica.ListItems.Count
                                                                LvCartolaHistorica.ListItems(Lp_I).SubItems(1) = !doc_nombre
                                                                LvCartolaHistorica.ListItems(Lp_I).SubItems(2) = !no_documento
                                                                LvCartolaHistorica.ListItems(Lp_I).SubItems(3) = !vence
                                                                LvCartolaHistorica.ListItems(Lp_I).SubItems(4) = NumFormat(!bruto)
                                                                LvCartolaHistorica.ListItems(Lp_I).SubItems(5) = 0
                                                                LvCartolaHistorica.ListItems(Lp_I).SubItems(6) = 0
                                                                LvCartolaHistorica.ListItems(Lp_I).SubItems(8) = !Id
                                                                LvCartolaHistorica.ListItems(Lp_I).SubItems(9) = !doc_id
                                                            End If
                                                            .MoveNext
                                                        Loop
                                                    End With
                                                    'Aqui Agregaremos la informacion de los abonos
                                                    Sql = "SELECT a.abo_fecha fecha,mpa_nombre doc_nombre,abo_nro_comprobante no_documento, '' vence,0 bruto,t.pad_valor pago, 0 saldo,t.pad_id " & _
                                                    "FROM abo_tipos_de_pagos t " & _
                                                    "JOIN cta_abonos a  ON a.abo_id=t.abo_id " & _
                                                    "/* JOIN cta_abono_documentos d ON a.abo_id=d.abo_id */ " & _
                                                    "JOIN par_medios_de_pago m ON t.mpa_id=m.mpa_id " & _
                                                    "WHERE t.abo_id IN(" & Sp_AbonosId & ") AND abo_cli_pro='PRO' AND abo_rut='" & TxtRut & "' AND a.rut_emp='" & SP_Rut_Activo & "' " & Sp_FiltroAbonos & " " & _
                                                    "GROUP BY t.pad_id"
                                                    Consulta RsTmp2, Sql
                                                    If RsTmp2.RecordCount > 0 Then
                                                            RsTmp2.MoveFirst
                                                            Do While Not RsTmp2.EOF
                                                                yaesta = "no"
                                                                For n = 1 To LvCartolaHistorica.ListItems.Count
                                                                    If Val(LvCartolaHistorica.ListItems(n).SubItems(10)) = Val(RsTmp2!pad_id) Then
                                                                        yaesta = SI
                                                                        Exit For
                                                                    End If
                                                                Next
                                                                
                                                                If yaesta = "no" Then
                                                                    LvCartolaHistorica.ListItems.Add , , RsTmp2!Fecha
                                                                    Lp_I = LvCartolaHistorica.ListItems.Count
                                                                    LvCartolaHistorica.ListItems(Lp_I).SubItems(1) = "--->" & RsTmp2!doc_nombre
                                                                    LvCartolaHistorica.ListItems(Lp_I).SubItems(2) = RsTmp2!no_documento
                                                                    LvCartolaHistorica.ListItems(Lp_I).SubItems(3) = RsTmp2!vence
                                                                    LvCartolaHistorica.ListItems(Lp_I).SubItems(4) = NumFormat(RsTmp2!bruto)
                                                                    LvCartolaHistorica.ListItems(Lp_I).SubItems(5) = NumFormat(RsTmp2!pago)
                                                                    LvCartolaHistorica.ListItems(Lp_I).SubItems(6) = 0
                                                                    LvCartolaHistorica.ListItems(Lp_I).SubItems(10) = RsTmp2!pad_id
                                                                End If
                                                                RsTmp2.MoveNext
                                                            Loop
                                                    End If
                                                    
                                                    
                                                    
                                                    
                                                    
                                        End If
                                Else
                                    'Si el documento no tiene abonos, de igual forma agregarlo en la lista
                                     Sql = "SELECT fecha,doc_nombre,no_documento,doc_fecha_vencimiento vence,IF(doc_signo_libro='+',total,total*-1) bruto, 0 pago, 0 saldo,id,v.doc_id " & _
                                                "FROM com_doc_compra v " & _
                                                "JOIN sis_documentos d ON v.doc_id=d.doc_id " & _
                                                "WHERE v.id =" & RsTmp!Id & " " & Sp_Filtro & " " & _
                                                "ORDER BY fecha "
                                        Consulta RsTmp2, Sql
                                        
                                        If RsTmp2.RecordCount > 0 Then
                                                    With RsTmp2
                                                        .MoveFirst
                                                        Do While Not .EOF
                                                            yaesta = False
                                                            For i = 1 To LvCartolaHistorica.ListItems.Count
                                                                If LvCartolaHistorica.ListItems(i).SubItems(8) = !Id Then
                                                                    yaesta = True
                                                                    Exit For
                                                                End If
                                                            
                                                            
                                                            Next
                                                            If yaesta = False Then
                                                            
                                                                LvCartolaHistorica.ListItems.Add , , !Fecha
                                                                Lp_I = LvCartolaHistorica.ListItems.Count
                                                                LvCartolaHistorica.ListItems(Lp_I).SubItems(1) = !doc_nombre
                                                                LvCartolaHistorica.ListItems(Lp_I).SubItems(2) = !no_documento
                                                                LvCartolaHistorica.ListItems(Lp_I).SubItems(3) = !vence
                                                                LvCartolaHistorica.ListItems(Lp_I).SubItems(4) = NumFormat(!bruto)
                                                                LvCartolaHistorica.ListItems(Lp_I).SubItems(5) = 0
                                                                LvCartolaHistorica.ListItems(Lp_I).SubItems(6) = 0
                                                                LvCartolaHistorica.ListItems(Lp_I).SubItems(8) = !Id
                                                                LvCartolaHistorica.ListItems(Lp_I).SubItems(9) = !doc_id
                                                            End If
                                                            .MoveNext
                                                        Loop
                                                    End With
                                        End If
                                
                                End If
                                
                                
                                
                                
                                
                            
                            
                            RsTmp.MoveNext
                        Loop
                    
                    End If
                    
                    
                    
                            'Buscaremos si el rut tiene saldo a favor y lo mostraremos
                    '7 4 2016
                    Sql = "SELECT IFNULL(SUM(pzo_abono)- SUM(pzo_cargo),0) saldo,pzo_fecha " & _
                            "FROM cta_pozo " & _
                            "WHERE rut_emp = '" & SP_Rut_Activo & "' AND pzo_rut = '" & TxtRut & "' AND pzo_cli_pro = 'PRO'"
                    Consulta RsTmp, Sql
                    If RsTmp.RecordCount > 0 Then
                        If (RsTmp!saldo) > 0 Then
                                 LvCartolaHistorica.ListItems.Add , , RsTmp!pzo_fecha
                                 Lp_I = LvCartolaHistorica.ListItems.Count
                                 LvCartolaHistorica.ListItems(Lp_I).SubItems(1) = "POZO CON SALDO"
                                 LvCartolaHistorica.ListItems(Lp_I).SubItems(2) = ""
                                 LvCartolaHistorica.ListItems(Lp_I).SubItems(3) = ""
                                 LvCartolaHistorica.ListItems(Lp_I).SubItems(4) = 0
                                 LvCartolaHistorica.ListItems(Lp_I).SubItems(5) = NumFormat(RsTmp!saldo)
                                 LvCartolaHistorica.ListItems(Lp_I).SubItems(6) = 0
                                 LvCartolaHistorica.ListItems(Lp_I).SubItems(8) = 0
                                 LvCartolaHistorica.ListItems(Lp_I).SubItems(9) = 0
                     '
                      
                            'SkSaldo = NumFormat(RsTmp!saldo)
                      
                        End If
                    End If
                    
                    
                    
                    
                    Lp_SuperSaldo = 0
                    For i = 1 To LvCartolaHistorica.ListItems.Count
                        Lp_SuperSaldo = Lp_SuperSaldo + CDbl(LvCartolaHistorica.ListItems(i).SubItems(4)) - CDbl(LvCartolaHistorica.ListItems(i).SubItems(5))
                        LvCartolaHistorica.ListItems(i).SubItems(7) = NumFormat(Lp_SuperSaldo)


                    Next

                    TxtCtVentas = NumFormat(TotalizaColumna(Me.LvCartolaHistorica, "venta"))
                    TxtCtAbonos = NumFormat(TotalizaColumna(Me.LvCartolaHistorica, "abono"))
                    TxtCtSaldo = NumFormat(CDbl(TxtCtVentas) - CDbl(TxtCtAbonos))

End Sub
Private Sub CartolaHistoricaClientes()
    Dim Lp_I As Long
    Dim Lp_SuperSaldo As Long
    Dim Rp_Cartola As Recordset
    Dim Rp_Docs As Recordset
    Dim Sp_AbonosId As String
    Dim Sp_DocumentosID As String
    Dim Lp_SaldoAnterior As Long

    
    
    
                    
                    If Len(Sp_FiltroAbonos) > 0 Then
                        Lp_SaldoAnterior = ConsultaSaldoClienteConFecha(TxtRut, Fql(Me.DtDesdeCartola))
                        If Lp_SaldoAnterior <> 0 Then
                            LvCartolaHistorica.ListItems.Add , , ""
                            Lp_I = LvCartolaHistorica.ListItems.Count
                            LvCartolaHistorica.ListItems(Lp_I).SubItems(1) = "SALDO ANTERIOR"
                            LvCartolaHistorica.ListItems(Lp_I).SubItems(2) = ""
                            LvCartolaHistorica.ListItems(Lp_I).SubItems(3) = ""
                            LvCartolaHistorica.ListItems(Lp_I).SubItems(4) = NumFormat(Lp_SaldoAnterior)
                            LvCartolaHistorica.ListItems(Lp_I).SubItems(5) = 0
                            LvCartolaHistorica.ListItems(Lp_I).SubItems(6) = 0
                            LvCartolaHistorica.ListItems(Lp_I).SubItems(8) = 0
                            LvCartolaHistorica.ListItems(Lp_I).SubItems(9) = 0
                        
                        End If
                    
                    End If
                   
    
    
    
    
                   Sql = "SELECT id " & _
                        "FROM ven_doc_venta v " & _
                        "WHERE ven_nc_utilizada='NO' AND v.rut_emp='" & SP_Rut_Activo & "' AND rut_cliente='" & TxtRut & "' " & Sp_Filtro & _
                        "ORDER BY fecha "
                    Consulta RsTmp, Sql
                   ' LLenar_Grilla RsTmp, Me, LvCartolita, False, True, True, False
                    
                    
                    If RsTmp.RecordCount > 0 Then
                        RsTmp.MoveFirst
                        Do While Not RsTmp.EOF
                                Sp_AbonosId = "" 'abonos realizados
                                Sql = "SELECT a.abo_id " & _
                                        "FROM cta_abonos a " & _
                                        "JOIN cta_abono_documentos d ON a.abo_id=d.abo_id " & _
                                        "WHERE abo_cli_pro = 'CLI' AND abo_rut = '" & TxtRut & "' " & _
                                                "AND a.rut_emp = '" & SP_Rut_Activo & "' AND d.rut_emp='" & SP_Rut_Activo & "' " & Sp_FiltroAbonos & " " & _
                                                "AND d.id=" & RsTmp!Id
                            
                                Consulta Rp_Cartola, Sql
                                
                                If Rp_Cartola.RecordCount > 0 Then 'Detectamos los abonos realizados
                                    Rp_Cartola.MoveFirst
                                    Do While Not Rp_Cartola.EOF
                                        Sp_AbonosId = Sp_AbonosId & Rp_Cartola!abo_id & ","
                                        Rp_Cartola.MoveNext
                                    Loop
                                    Sp_AbonosId = Mid(Sp_AbonosId, 1, Len(Sp_AbonosId) - 1)
                                End If
                                Sp_DocumentosID = ""
                                If Len(Sp_AbonosId) > 0 Then
                                    'Ahora se deberan buscar los documento(s) que se incluyeron en este pago(s)
                                    Sql = "SELECT d.id /*doc_nombre,no_documento,fecha,bruto */ " & _
                                            "FROM cta_abono_documentos d " & _
                                            "JOIN cta_abonos a ON d.abo_id=a.abo_id " & _
                                            "JOIN ven_doc_venta v ON d.id=v.id " & _
                                            "JOIN sis_documentos x ON v.doc_id=x.doc_id " & _
                                            "WHERE d.abo_id IN(" & Sp_AbonosId & ") " & _
                                            "AND v.rut_emp='" & SP_Rut_Activo & "' AND d.rut_emp='" & SP_Rut_Activo & "' " & Sp_Filtro & Sp_FiltroAbonos & "  " & _
                                            "GROUP BY d.id " & _
                                            "ORDER BY v.fecha"
                                    Consulta Rp_Docs, Sql
                                    If Rp_Docs.RecordCount > 0 Then
                                        Rp_Docs.MoveFirst
                                        Do While Not Rp_Docs.EOF
                                                Sp_DocumentosID = Sp_DocumentosID & Rp_Docs!Id & ","
                                        
                                        
                                            Rp_Docs.MoveNext
                                        Loop
                                        Sp_DocumentosID = Mid(Sp_DocumentosID, 1, Len(Sp_DocumentosID) - 1)
                                    End If
                                End If
                                
                                If Len(Sp_DocumentosID) > 0 Then 'Documentos
                                        Sql = "SELECT fecha,doc_nombre,no_documento,DATE_ADD(fecha, INTERVAL ven_plazo DAY) vence,IF(doc_signo_libro='+',bruto,bruto*-1) bruto, 0 pago, 0 saldo,id,v.doc_id " & _
                                                "FROM ven_doc_venta v " & _
                                                "JOIN sis_documentos d ON v.doc_id=d.doc_id " & _
                                                "WHERE v.id IN(" & Sp_DocumentosID & ") " & Sp_Filtro & " " & _
                                                "ORDER BY fecha "
                                        Consulta RsTmp2, Sql
                                        
                                        If RsTmp2.RecordCount > 0 Then
                                                    With RsTmp2
                                                        .MoveFirst
                                                        Do While Not .EOF
                                                            yaesta = False
                                                            For i = 1 To LvCartolaHistorica.ListItems.Count
                                                                If LvCartolaHistorica.ListItems(i).SubItems(8) = !Id Then
                                                                    yaesta = True
                                                                    Exit For
                                                                End If
                                                            
                                                            
                                                            Next
                                                            If yaesta = False Then
                                                            
                                                                LvCartolaHistorica.ListItems.Add , , !Fecha
                                                                Lp_I = LvCartolaHistorica.ListItems.Count
                                                                LvCartolaHistorica.ListItems(Lp_I).SubItems(1) = !doc_nombre
                                                                LvCartolaHistorica.ListItems(Lp_I).SubItems(2) = !no_documento
                                                                LvCartolaHistorica.ListItems(Lp_I).SubItems(3) = !vence
                                                                LvCartolaHistorica.ListItems(Lp_I).SubItems(4) = NumFormat(!bruto)
                                                                LvCartolaHistorica.ListItems(Lp_I).SubItems(5) = 0
                                                                LvCartolaHistorica.ListItems(Lp_I).SubItems(6) = 0
                                                                LvCartolaHistorica.ListItems(Lp_I).SubItems(8) = !Id
                                                                LvCartolaHistorica.ListItems(Lp_I).SubItems(9) = !doc_id
                                                            End If
                                                            .MoveNext
                                                        Loop
                                                    End With
                                                    'Aqui Agregaremos la informacion de los abonos
                                                    Sql = "SELECT a.abo_fecha fecha,mpa_nombre doc_nombre,abo_nro_comprobante no_documento, '' vence,0 bruto,t.pad_valor pago, 0 saldo,t.pad_id " & _
                                                    "FROM abo_tipos_de_pagos t " & _
                                                    "JOIN cta_abonos a  ON a.abo_id=t.abo_id " & _
                                                    "/* JOIN cta_abono_documentos d ON a.abo_id=d.abo_id */ " & _
                                                    "JOIN par_medios_de_pago m ON t.mpa_id=m.mpa_id " & _
                                                    "WHERE t.abo_id IN(" & Sp_AbonosId & ") AND abo_cli_pro='CLI' AND abo_rut='" & TxtRut & "' AND a.rut_emp='" & SP_Rut_Activo & "' " & Sp_FiltroAbonos & " " & _
                                                    "GROUP BY t.pad_id"
                                                    Consulta RsTmp2, Sql
                                                    If RsTmp2.RecordCount > 0 Then
                                                            RsTmp2.MoveFirst
                                                            Do While Not RsTmp2.EOF
                                                                yaesta = "no"
                                                                For n = 1 To LvCartolaHistorica.ListItems.Count
                                                                    If Val(LvCartolaHistorica.ListItems(n).SubItems(10)) = Val(RsTmp2!pad_id) Then
                                                                        yaesta = SI
                                                                        Exit For
                                                                    End If
                                                                Next
                                                                
                                                                If yaesta = "no" Then
                                                                    LvCartolaHistorica.ListItems.Add , , RsTmp2!Fecha
                                                                    Lp_I = LvCartolaHistorica.ListItems.Count
                                                                    LvCartolaHistorica.ListItems(Lp_I).SubItems(1) = "--->" & RsTmp2!doc_nombre
                                                                    LvCartolaHistorica.ListItems(Lp_I).SubItems(2) = RsTmp2!no_documento
                                                                    LvCartolaHistorica.ListItems(Lp_I).SubItems(3) = RsTmp2!vence
                                                                    LvCartolaHistorica.ListItems(Lp_I).SubItems(4) = NumFormat(RsTmp2!bruto)
                                                                    LvCartolaHistorica.ListItems(Lp_I).SubItems(5) = NumFormat(RsTmp2!pago)
                                                                    LvCartolaHistorica.ListItems(Lp_I).SubItems(6) = 0
                                                                    LvCartolaHistorica.ListItems(Lp_I).SubItems(10) = RsTmp2!pad_id
                                                                End If
                                                                RsTmp2.MoveNext
                                                            Loop
                                                    End If
                                                    
                                                    
                                                    
                                                    
                                                    
                                        End If
                                Else
                                    'Si el documento no tiene abonos, de igual forma agregarlo en la lista
                                     Sql = "SELECT fecha,doc_nombre,no_documento,DATE_ADD(fecha, INTERVAL ven_plazo DAY) vence,IF(doc_signo_libro='+',bruto,bruto*-1) bruto, 0 pago, 0 saldo,id,v.doc_id " & _
                                                "FROM ven_doc_venta v " & _
                                                "JOIN sis_documentos d ON v.doc_id=d.doc_id " & _
                                                "WHERE v.id =" & RsTmp!Id & " " & Sp_Filtro & " " & _
                                                "ORDER BY fecha "
                                        Consulta RsTmp2, Sql
                                        
                                        If RsTmp2.RecordCount > 0 Then
                                                    With RsTmp2
                                                        .MoveFirst
                                                        Do While Not .EOF
                                                            yaesta = False
                                                            For i = 1 To LvCartolaHistorica.ListItems.Count
                                                                If LvCartolaHistorica.ListItems(i).SubItems(8) = !Id Then
                                                                    yaesta = True
                                                                    Exit For
                                                                End If
                                                            
                                                            
                                                            Next
                                                            If yaesta = False Then
                                                            
                                                                LvCartolaHistorica.ListItems.Add , , !Fecha
                                                                Lp_I = LvCartolaHistorica.ListItems.Count
                                                                LvCartolaHistorica.ListItems(Lp_I).SubItems(1) = !doc_nombre
                                                                LvCartolaHistorica.ListItems(Lp_I).SubItems(2) = !no_documento
                                                                LvCartolaHistorica.ListItems(Lp_I).SubItems(3) = !vence
                                                                LvCartolaHistorica.ListItems(Lp_I).SubItems(4) = NumFormat(!bruto)
                                                                LvCartolaHistorica.ListItems(Lp_I).SubItems(5) = 0
                                                                LvCartolaHistorica.ListItems(Lp_I).SubItems(6) = 0
                                                                LvCartolaHistorica.ListItems(Lp_I).SubItems(8) = !Id
                                                                LvCartolaHistorica.ListItems(Lp_I).SubItems(9) = !doc_id
                                                            End If
                                                            .MoveNext
                                                        Loop
                                                    End With
                                        End If
                                
                                End If
                                
                                
                                
                                
                                
                            
                            
                            RsTmp.MoveNext
                        Loop
                    
                    End If
                    
                    
                    
                            'Buscaremos si el rut tiene saldo a favor y lo mostraremos
                    '7 4 2016
                    Sql = "SELECT IFNULL(SUM(pzo_abono)- SUM(pzo_cargo),0) saldo,pzo_fecha " & _
                            "FROM cta_pozo " & _
                            "WHERE rut_emp = '" & SP_Rut_Activo & "' AND pzo_rut = '" & TxtRut & "' AND pzo_cli_pro = 'CLI'"
                    Consulta RsTmp, Sql
                    If RsTmp.RecordCount > 0 Then
                        If (RsTmp!saldo) > 0 Then
                                 LvCartolaHistorica.ListItems.Add , , RsTmp!pzo_fecha
                                 Lp_I = LvCartolaHistorica.ListItems.Count
                                 LvCartolaHistorica.ListItems(Lp_I).SubItems(1) = "POZO CON SALDO"
                                 LvCartolaHistorica.ListItems(Lp_I).SubItems(2) = ""
                                 LvCartolaHistorica.ListItems(Lp_I).SubItems(3) = ""
                                 LvCartolaHistorica.ListItems(Lp_I).SubItems(4) = 0
                                 LvCartolaHistorica.ListItems(Lp_I).SubItems(5) = NumFormat(RsTmp!saldo)
                                 LvCartolaHistorica.ListItems(Lp_I).SubItems(6) = 0
                                 LvCartolaHistorica.ListItems(Lp_I).SubItems(8) = 0
                                 LvCartolaHistorica.ListItems(Lp_I).SubItems(9) = 0
                     '
                      
                            'SkSaldo = NumFormat(RsTmp!saldo)
                      
                        End If
                    End If
                    
                    
                    
                    
                    Lp_SuperSaldo = 0
                    For i = 1 To LvCartolaHistorica.ListItems.Count
                        Lp_SuperSaldo = Lp_SuperSaldo + CDbl(LvCartolaHistorica.ListItems(i).SubItems(4)) - CDbl(LvCartolaHistorica.ListItems(i).SubItems(5))
                        LvCartolaHistorica.ListItems(i).SubItems(7) = NumFormat(Lp_SuperSaldo)


                    Next

                    TxtCtVentas = NumFormat(TotalizaColumna(Me.LvCartolaHistorica, "venta"))
                    TxtCtAbonos = NumFormat(TotalizaColumna(Me.LvCartolaHistorica, "abono"))
                    TxtCtSaldo = NumFormat(CDbl(TxtCtVentas) - CDbl(TxtCtAbonos))
End Sub
Private Sub CartolaHistorica()
    
                    Sql = "SELECT fecha,doc_nombre,no_documento,doc_fecha_vencimiento vence,IF(doc_signo_libro='+',total,total*-1) bruto, 0 pago, 0 saldo,id,v.doc_id " & _
                            "FROM com_doc_compra v " & _
                            "JOIN sis_documentos d ON v.doc_id=d.doc_id " & _
                            "WHERE  v.rut_emp='" & SP_Rut_Activo & "' AND rut='" & TxtRut & "' " & Sp_Filtro & _
                            "ORDER BY fecha "
                    Consulta RsTmp, Sql
                    LvCartolaHistorica.ListItems.Clear
                    If RsTmp.RecordCount > 0 Then
                        With RsTmp
                            .MoveFirst
                            Do While Not .EOF
                                LvCartolaHistorica.ListItems.Add , , !Fecha
                                Lp_I = LvCartolaHistorica.ListItems.Count
                                LvCartolaHistorica.ListItems(Lp_I).SubItems(1) = !doc_nombre
                                LvCartolaHistorica.ListItems(Lp_I).SubItems(2) = !no_documento
                                LvCartolaHistorica.ListItems(Lp_I).SubItems(3) = !vence
                                LvCartolaHistorica.ListItems(Lp_I).SubItems(4) = NumFormat(!bruto)
                                LvCartolaHistorica.ListItems(Lp_I).SubItems(5) = 0
                                LvCartolaHistorica.ListItems(Lp_I).SubItems(6) = 0
                                LvCartolaHistorica.ListItems(Lp_I).SubItems(7) = !Id
                                LvCartolaHistorica.ListItems(Lp_I).SubItems(8) = !doc_id
                                
                                
                                Sql = "SELECT a.abo_fecha_pago fecha,mpa_nombre doc_nombre,abo_nro_comprobante no_documento, '' vence,0 bruto,d.ctd_monto pago, 0 saldo,t.pad_ " & _
                                        "FROM abo_tipos_de_pagos t " & _
                                        "JOIN cta_abonos a  ON a.abo_id=t.abo_id " & _
                                        "JOIN cta_abono_documentos d ON a.abo_id=d.abo_id " & _
                                        "JOIN par_medios_de_pago m ON t.mpa_id=m.mpa_id " & _
                                        "WHERE abo_cli_pro='PRO' AND abo_rut='" & TxtRut & "' AND a.rut_emp='" & SP_Rut_Activo & "' AND d.rut_emp='" & SP_Rut_Activo & "' AND d.id=" & !Id & " " & Sp_FiltroAbonos
                                Consulta RsTmp2, Sql
                                If RsTmp2.RecordCount > 0 Then
                                        RsTmp2.MoveFirst
                                        Do While Not RsTmp2.EOF
                                            LvCartolaHistorica.ListItems.Add , , RsTmp2!Fecha
                                            Lp_I = LvCartolaHistorica.ListItems.Count
                                            LvCartolaHistorica.ListItems(Lp_I).SubItems(1) = "--->" & RsTmp2!doc_nombre
                                            LvCartolaHistorica.ListItems(Lp_I).SubItems(2) = RsTmp2!no_documento
                                            LvCartolaHistorica.ListItems(Lp_I).SubItems(3) = RsTmp2!vence
                                            LvCartolaHistorica.ListItems(Lp_I).SubItems(4) = NumFormat(RsTmp2!bruto)
                                            LvCartolaHistorica.ListItems(Lp_I).SubItems(5) = NumFormat(RsTmp2!pago)
                                            LvCartolaHistorica.ListItems(Lp_I).SubItems(6) = 0
                                            RsTmp2.MoveNext
                                        Loop
                                End If
                                .MoveNext
                            Loop
                            Lp_SuperSaldo = 0
                            For i = 1 To LvCartolaHistorica.ListItems.Count
                                Lp_SuperSaldo = Lp_SuperSaldo + CDbl(LvCartolaHistorica.ListItems(i).SubItems(4)) - CDbl(LvCartolaHistorica.ListItems(i).SubItems(5))
                                LvCartolaHistorica.ListItems(i).SubItems(7) = NumFormat(Lp_SuperSaldo)
                                
                            
                            Next
                            
                            TxtCtVentas = NumFormat(TotalizaColumna(Me.LvCartolaHistorica, "venta"))
                            TxtCtAbonos = NumFormat(TotalizaColumna(Me.LvCartolaHistorica, "abono"))
                            TxtCtSaldo = NumFormat(CDbl(TxtCtVentas) - CDbl(TxtCtAbonos))
                            
                        End With
                    End If
    

End Sub


Private Sub LvDetalle_DblClick()
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    If InStr(LvDetalle.SelectedItem.SubItems(1), "MULTIPLE") > 0 Then
        'Debemos buscar el detalle de  este abono
        ctacta_VerPagoMultiple.Lm_IdAbono = LvDetalle.SelectedItem.SubItems(13)
        ctacta_VerPagoMultiple.Show 1
        
    End If
End Sub



Private Sub Timer1_Timer()
    On Error Resume Next
    LvDetalle.SetFocus
    Timer1.Enabled = False
  
    

End Sub
Private Sub Carga()
    Dim Sp_Nombre As String
    Dim Sp_Ids As String
    DoEvents
    If Sm_CliPro = "PRO" Then 'PROVEEDORES  PROVEEDORES  PROVEEDORES  PROVEEDORES  PROVEEDORES  PROVEEDORES
        Sp_Nombre = "SELECT nombre_empresa nombre, email, fono,direccion,ciudad " & _
                "FROM maestro_proveedores " & _
                "WHERE rut_proveedor='" & Sm_Rut & "'"
        
        
        
        
        '15 Nov 2013
       Sql = "SELECT  v.id,doc_nombre,no_documento,fecha,v.doc_fecha_vencimiento, " & _
                    "total - (IFNULL((SELECT SUM(t.ctd_monto) " & _
                            "FROM cta_abono_documentos t,cta_abonos a " & _
                            "WHERE t.abo_id = a.abo_id   AND a.abo_cli_pro = 'PRO' " & _
                            "AND t.id = v.id),0) + IFNULL( " & _
                        "(SELECT SUM(ctd_monto) " & _
                            "FROM cta_abonos z " & _
                            "INNER JOIN cta_abono_documentos y USING (abo_id) " & _
                            "WHERE abo_cli_pro='PRO' AND z.rut_emp = '" & SP_Rut_Activo & "' " & _
                            "AND y.rut_emp = '" & SP_Rut_Activo & "' AND y.id IN ( " & _
                                "SELECT id " & _
                                "FROM com_doc_compra w " & _
                                "WHERE w.id_ref = v.id)),0)) saldo " & _
            "FROM com_doc_compra v,sis_documentos d " & _
            "WHERE   v.com_nc_utilizada = 'NO' AND v.rut_emp = '" & SP_Rut_Activo & "' " & _
            "AND id_ref = 0 AND v.doc_id_factura = 0 AND v.doc_id = d.doc_id AND v.rut='" & Sm_Rut & "' " & _
             Sm_Fecha & Sm_Documento & " " & _
             Sm_Saldo & _
            " ORDER BY    id"
      '  Sql = "SELECT id,doc_abreviado,no_documento,fecha,'' ven_fecha_vencimiento,saldo " & _
                "FROM vi_saldo_documentos_proveedores " & _
                "WHERE rut_emp='" & SP_Rut_Activo & "' AND rut='" & Sm_Rut & "' " & Sm_Fecha & Sm_Documento & _
                " " & Sm_Saldo
        
        'Consulta RsTmp, Sql
        Sis_BarraProgreso.Show 1
        LLenar_Grilla RsTmp, Me, LvResumen, False, True, True, False
        'Aqui obtendremos los id de los documentos para procesarlos
        Sp_Ids = ""
        For i = 1 To LvResumen.ListItems.Count
            Sp_Ids = Sp_Ids & LvResumen.ListItems(i) & ","
        
        Next
        If Len(Sp_Ids) > 0 Then
            Sp_Ids = Mid(Sp_Ids, 1, Len(Sp_Ids) - 1)
        Else
            Exit Sub
        End If
        
        TxtResumen = NumFormat(TotalizaColumna(LvResumen, "saldito"))
       ' SkResumen = NumFormat(TotalizaColumna(LvResumen, "saldito"))
        Sql = "SELECT id,d.doc_abreviado,no_documento,fecha,doc_fecha_vencimiento fecha_vence,com_folio_texto,0 f1,total f2,total saldo,c.doc_id  " & _
                "FROM com_doc_compra c " & _
                "JOIN sis_documentos d ON c.doc_id=d.doc_id " & _
                "WHERE rut_emp='" & SP_Rut_Activo & "'  AND id IN(" & Sp_Ids & ")  AND rut='" & Sm_Rut & "' AND " & _
                "doc_contable='SI' AND doc_nota_de_credito='NO' AND doc_nota_de_debito='NO' AND " & _
                "total- " & _
                "IFNULL((SELECT SUM(ctd_monto) FROM cta_abono_documentos ad " & _
                "JOIN cta_abonos a ON ad.abo_id=a.abo_id " & _
                "WHERE a.rut_emp='" & SP_Rut_Activo & "'AND a.abo_cli_pro='PRO' AND c.id=ad.id),0)-" & _
                "(IFNULL((SELECT SUM(o.total) " & _
                        "FROM com_doc_compra o " & _
                        "JOIN sis_documentos doc ON o.doc_id=doc.doc_id " & _
                        "WHERE doc.doc_nota_de_credito='SI' AND o.id_ref=c.id),0))+" & _
                        "(IFNULL((SELECT SUM(o.total) " & _
                        "FROM com_doc_compra o " & _
                        "JOIN sis_documentos doc ON o.doc_id=doc.doc_id " & _
                        "WHERE doc.doc_nota_de_debito='SI' AND o.com_id_ref_de_nota_debito=c.id),0))<>0  " & _
                "ORDER BY no_documento;"
    Else
        'Clientes CLIENTES CLIENTES  CLIENTES CLIENTES  CLIENTES CLIENTES  CLIENTES CLIENTES
        Sp_Nombre = "SELECT nombre_rsocial nombre, email, fono,ciudad,direccion " & _
                "FROM maestro_clientes " & _
                "WHERE rut_cliente='" & Sm_Rut & "'"
                
      ' 11-02-2014  Sql = "SELECT id,doc_nombre,no_documento,fecha,ven_fecha_vencimiento,saldo  " & _
                "FROM vi_saldo_de_documentos_clientes v " & _
                "WHERE rut_emp='" & SP_Rut_Activo & "' AND rut_cliente ='" & Sm_Rut & "' " & Sm_Fecha & Sm_Documento & _
                " " & Sm_Saldo
        Sql = "SELECT  `c`.`id` AS `id`, " & _
    "`d`.`doc_abreviado` AS `doc_abreviado`, " & _
    "`c`.`no_documento` AS `no_documento`, " & _
    "`c`.`fecha` AS `fecha`, " & _
    "`c`.`ven_fecha_vencimiento` AS `ven_fecha_vencimiento`, " & _
    "(((`c`.`bruto` - ifnull(( " & _
                        "SELECT sum(`ad`.`ctd_monto`) " & _
                        "FROM (`cta_abono_documentos` `ad` " & _
                                "JOIN `cta_abonos` `a` ON(" & _
                                    "(`ad`.`abo_id` = `a`.`abo_id`))) " & _
                        "WHERE ((`a`.`rut_emp` = CONVERT(`c`.`rut_emp` USING utf8)) " & _
                                "AND(`a`.`abo_cli_pro` = 'CLI') " & _
                                "AND(`c`.`id` = `ad`.`id`))),0))- ifnull( " & _
                "(SELECT SUM(`o`.`bruto`) " & _
                    "FROM (`ven_doc_venta` `o` " & _
                            "JOIN `sis_documentos` `doc` ON( " & _
                                "(`o`.`doc_id` = `doc`.`doc_id`))) " & _
                    "WHERE ((`doc`.`doc_nota_de_credito` = 'SI') " & _
                            "AND(`o`.`id_ref` = `c`.`id`))),0))+( " & _
                            "IFNULL((SELECT SUM(`e`.`bruto`) " & _
                            "FROM (`ven_doc_venta` `e` " & _
                           "JOIN `sis_documentos` `doc` ON((`e`.`doc_id` = `doc`.`doc_id`))) " & _
                    "WHERE ((`doc`.`doc_nota_de_debito` = 'SI') "
        Sql = Sql & "AND(`e`.`id_ref` = `c`.`id`))),0)- IFNULL( " & _
                            "(SELECT SUM(`x`.`ctd_monto`) " & _
                     "FROM (`cta_abono_documentos` `x` " & _
                            "JOIN `cta_abonos` `z` ON((`x`.`abo_id` = `z`.`abo_id`))) " & _
                    "WHERE ((`z`.`abo_cli_pro` = 'CLI') AND `x`.`id` IN( " & _
                                    "SELECT `ve`.`id` " & _
                                    "FROM `ven_doc_venta` `ve` " & _
                                    "WHERE (`ve`.`id_ref` = `c`.`id`)))),0)))AS `saldo` " & _
"FROM (`ven_doc_venta` `c` " & _
        "JOIN `sis_documentos` `d` ON((`c`.`doc_id` = `d`.`doc_id`))) " & _
"WHERE   ((`d`.`doc_contable` = 'SI') AND (`d`.`doc_nota_de_credito` = 'NO') AND(`d`.`doc_nota_de_debito` = 'NO')) AND  rut_emp='" & SP_Rut_Activo & "' AND rut_cliente='" & Sm_Rut & "' " & Sm_Fecha & Sm_Documento & " " & Sm_Saldo & _
"/* ORDER BY `c`.`no_documento` */"
       
        
        
     '   Sql = "SELECT id,doc_abreviado,no_documento,fecha,ven_fecha_vencimiento,saldo " & _
                "FROM vi_saldo_documentos_clientes " & _
                "WHERE rut_emp='" & SP_Rut_Activo & "' AND rut_cliente='" & Sm_Rut & "' " & Sm_Fecha & Sm_Documento & _
                " " & Sm_Saldo
                
         Sis_BarraProgreso.Show 1
        'Consulta RsTmp, Sql
        LLenar_Grilla RsTmp, Me, LvResumen, False, True, True, False
        TxtResumen = NumFormat(TotalizaColumna(LvResumen, "saldito"))
       ' SkResumen = NumFormat(TotalizaColumna(LvResumen, "saldito"))
        'Fin grilla resumen
        'buscaremos solo los ducumentos que esten en la grila inferior
        '8 mayo 2013
        
        Sp_Ids = ""
        For i = 1 To LvResumen.ListItems.Count
            Sp_Ids = Sp_Ids & LvResumen.ListItems(i) & ","
        
        Next
        If Len(Sp_Ids) > 0 Then
            Sp_Ids = Mid(Sp_Ids, 1, Len(Sp_Ids) - 1)
        Else
            Exit Sub
        End If
        Sql = "SELECT id,d.doc_nombre doc_abreviado,no_documento,fecha,ven_fecha_vencimiento fecha_vence,0 com_folio_texto,bruto  f1,0 f2,bruto saldo,c.doc_id  " & _
                "FROM ven_doc_venta c " & _
                "JOIN sis_documentos d ON c.doc_id=d.doc_id " & _
                "WHERE  rut_emp='" & SP_Rut_Activo & "' AND id IN(" & Sp_Ids & ") AND rut_cliente='" & Sm_Rut & "' AND doc_contable='SI' AND doc_nota_de_credito='NO' AND doc_nota_de_debito='NO'  AND " & _
                "bruto- " & _
                        "IFNULL((SELECT SUM(ctd_monto) FROM cta_abono_documentos ad " & _
                        "JOIN cta_abonos a ON ad.abo_id=a.abo_id " & _
                        "WHERE a.rut_emp='" & SP_Rut_Activo & "' AND a.abo_cli_pro='CLI' AND c.id=ad.id),0)-" & _
                        "(IFNULL((SELECT SUM(o.bruto) " & _
                        "FROM ven_doc_venta o " & _
                        "JOIN sis_documentos doc ON o.doc_id=doc.doc_id " & _
                        "WHERE doc.doc_nota_de_credito='SI' AND o.id_ref=c.id),0)) +" & _
                        "(IFNULL((SELECT SUM(o.bruto) " & _
                        "FROM ven_doc_venta o " & _
                        "JOIN sis_documentos doc ON o.doc_id=doc.doc_id " & _
                        "WHERE doc.doc_nota_de_debito='SI' AND o.id_ref=c.id),0))<>0 " & _
                        "ORDER BY no_documento "
                
    End If
    Consulta RsTmp3, Sp_Nombre
    If RsTmp3.RecordCount > 0 Then
        Me.txtCliente = RsTmp3!nombre
        Me.txtFono = "" & RsTmp3!fono
        Me.TxtMail = "" & RsTmp3!Email
        TxtDireccion = "" & RsTmp3!direccion
        TxtCiudad = "" & RsTmp3!ciudad
    End If
    

        
        Sis_BarraProgreso.Show 1
        'Consulta RsTmp, Sql
        'LLenar_Grilla RsTmp, Me, LvDetalle, False, True, True, False
        If RsTmp.RecordCount > 0 Then
            With RsTmp
                .MoveFirst
                Do While Not .EOF
                    LvDetalle.ListItems.Add , , !Id
                    X = LvDetalle.ListItems.Count
                    LvDetalle.ListItems(X).SubItems(1) = IIf(IsNull(!doc_abreviado), "", !doc_abreviado)
                    LvDetalle.ListItems(X).SubItems(2) = !no_documento
                    LvDetalle.ListItems(X).SubItems(3) = !Fecha
                    LvDetalle.ListItems(X).SubItems(4) = "" & !fecha_vence
                    LvDetalle.ListItems(X).SubItems(5) = !com_folio_texto
                    LvDetalle.ListItems(X).SubItems(6) = !f1
                    LvDetalle.ListItems(X).SubItems(7) = !f2
                    LvDetalle.ListItems(X).SubItems(8) = !saldo
                    LvDetalle.ListItems(X).SubItems(14) = !doc_id
                    For p = 1 To LvDetalle.ColumnHeaders.Count - 1
                        LvDetalle.ListItems(X).ListSubItems(p).Bold = True
                    Next
                    
                    
                    'Aqui consultamos los abonos del documento
                    Sql = "SELECT '',abo_fecha_pago,nombredoc,0 ref,ctd_monto bruto,0 abonos, " & _
                                "0 pad_nro_transaccion,'' pad_tipo_deposito,0 mpa_id,abo_id,'' doc_nota_de_debito,0 id " & _
                            "FROM vi_fpagos WHERE id=" & !Id & " AND abo_cli_pro='" & Sm_CliPro & "' UNION "
                            
                            If Sm_CliPro = "CLI" Then
                                'PAGOS REALIZADOS POR CLIENTES
                                Sql = Sql & "SELECT d.doc_abreviado,fecha,doc_abreviado nombredoc,no_documento ref," & _
                                            "bruto , " & _
                                        "IFNULL((SELECT SUM(ctd_monto) FROM cta_abono_documentos ad " & _
                                        "JOIN cta_abonos ax ON ad.abo_id=ax.abo_id " & _
                                        "WHERE ax.rut_emp='" & SP_Rut_Activo & "' AND  " & _
                                        "ax.abo_cli_pro='CLI' AND c.id=ad.id),0) abonos, " & _
                                "'' com_folio_texto,'',0, 0,doc_nota_de_debito,id " & _
                                "FROM ven_doc_venta c " & _
                                "JOIN sis_documentos d ON c.doc_id = d.doc_id " & _
                                "WHERE c.ven_nc_utilizada='NO' AND rut_emp = '" & SP_Rut_Activo & "' AND rut_cliente = '" & Sm_Rut & "' AND id_ref=" & !Id & " AND doc_contable='SI'  "
                            Else
                                'PAGOS RELACIONADOS DOCUMENTOS PROVEEDORES
                                Sql = Sql & "SELECT d.doc_abreviado,fecha,doc_abreviado nombredoc,no_documento ref," & _
                                                "total bruto , " & _
                                            "IFNULL((SELECT SUM(ctd_monto) FROM cta_abono_documentos ad " & _
                                            "JOIN cta_abonos ax ON ad.abo_id=ax.abo_id " & _
                                            "WHERE ax.rut_emp='" & SP_Rut_Activo & "' AND  " & _
                                            "ax.abo_cli_pro='PRO' AND c.id=ad.id),0) abonos, " & _
                                    "'' com_folio_texto,'',0, 0,doc_nota_de_debito,id " & _
                                    "FROM com_doc_compra c " & _
                                    "JOIN sis_documentos d ON c.doc_id = d.doc_id " & _
                                    "WHERE d.doc_nota_de_debito='SI' AND rut_emp = '" & SP_Rut_Activo & "' AND rut = '" & Sm_Rut & "' AND com_id_ref_de_nota_debito=" & !Id & " AND doc_contable='SI' "
                                
                            
                            End If
                            
                    Consulta RsTmp2, Sql
                    If RsTmp2.RecordCount > 0 Then
                        RsTmp2.MoveFirst
                        Do While Not RsTmp2.EOF
                            'Id del propio abono o documento Nota Debito
                            If Val(RsTmp2!Id) = 0 Then
                                LvDetalle.ListItems.Add , , !Id
                            Else
                                LvDetalle.ListItems.Add , , RsTmp2!Id
                            End If
                            
                            X = LvDetalle.ListItems.Count
                            LvDetalle.ListItems(X).SubItems(1) = "--->" & RsTmp2!NombreDoc '& " Nro " & RsTmp2!ref
                            LvDetalle.ListItems(X).SubItems(2) = RsTmp2!ref ' !no_documento
                            LvDetalle.ListItems(X).SubItems(3) = "" & RsTmp2!abo_fecha_pago
                            If Sm_CliPro = "PRO" Then
                                LvDetalle.ListItems(X).SubItems(6) = RsTmp2!bruto
                                LvDetalle.ListItems(X).SubItems(7) = 0
                                If RsTmp2!doc_nota_de_debito = "SI" Then
                                    'LvDetalle.ListItems(X).SubItems(6) = RsTmp2!abonos
                                    LvDetalle.ListItems(X).SubItems(6) = 0
                                    LvDetalle.ListItems(X).SubItems(7) = RsTmp2!bruto
                                End If
                            Else
                                LvDetalle.ListItems(X).SubItems(6) = 0
                                LvDetalle.ListItems(X).SubItems(7) = RsTmp2!bruto
                                
                                If RsTmp2!doc_nota_de_debito = "SI" Then
                                     LvDetalle.ListItems(X).SubItems(6) = RsTmp2!bruto
                                   '  LvDetalle.ListItems(X).SubItems(7) = RsTmp2!abonos
                                     LvDetalle.ListItems(X).SubItems(7) = 0
                                Else
                                   ' LvDetalle.ListItems(X).SubItems(6) = RsTmp2!bruto
                                   ' LvDetalle.ListItems(X).SubItems(7) = RsTmp2!abonos
                                
                                End If
                                
                            End If
                            LvDetalle.ListItems(X).SubItems(10) = RsTmp2!ref
                            'Aqui vamos restando (pagos, NC) y sumando NDebitos
                            If Sm_CliPro = "PRO" Then
                                valorarestar = RsTmp2!bruto
                                'If RsTmp2!doc_nota_de_debito = "SI" Then valorarestar = (RsTmp2!bruto - RsTmp2!abonos) * -1
                                If RsTmp2!doc_nota_de_debito = "SI" Then valorarestar = (RsTmp2!bruto) * -1
                                LvDetalle.ListItems(X).SubItems(8) = Val(LvDetalle.ListItems(X - 1).SubItems(8)) - valorarestar
                            Else
                                valorarestar = RsTmp2!bruto
                               ' If RsTmp2!doc_nota_de_debito = "SI" Then valorarestar = (RsTmp2!bruto - RsTmp2!abonos) * -1
                                If RsTmp2!doc_nota_de_debito = "SI" Then valorarestar = (RsTmp2!bruto) * -1
                                LvDetalle.ListItems(X).SubItems(8) = Val(LvDetalle.ListItems(X - 1).SubItems(8)) - valorarestar
                            End If
                            LvDetalle.ListItems(X).SubItems(12) = RsTmp2!mpa_id
                            LvDetalle.ListItems(X).SubItems(13) = RsTmp2!abo_id
                            
                            
                            If RsTmp2!doc_nota_de_debito = "SI" Then
                                'Debe ser con el id propio de la nota de debito
                                ' de lo contrario se repitara en cada ND
                                ObtenerPagosNotaDebito RsTmp2!Id
                            End If
                            
                            RsTmp2.MoveNext
                        Loop
                    End If
                    .MoveNext
                    'Agregamos un linea en blancoo para separar los ducumentos uno de otro
                    LvDetalle.ListItems.Add , , ""
                Loop
                'Vamos a rrecorrer la grilla, los que tengas tipo de pago 3=cheque
                'buscaremos los nro's de cheques y el banco
                TGENERAL = 0
                For i = 1 To LvDetalle.ListItems.Count
                     If Val(LvDetalle.ListItems(i).SubItems(12)) = 2 Then
                        
                        Sql = "SELECT che_numero,che_ban_nombre " & _
                                "FROM abo_cheques " & _
                                "WHERE abo_id=" & LvDetalle.ListItems(i).SubItems(13)
                        Consulta RsTmp3, Sql
                        If RsTmp3.RecordCount > 0 Then
                            With RsTmp3
                                .MoveFirst
                                Do While Not .EOF
                                    LvDetalle.ListItems(i).SubItems(11) = !che_ban_nombre
                                    LvDetalle.ListItems(i).SubItems(9) = LvDetalle.ListItems(i).SubItems(9) & !che_numero & ","
                                    .MoveNext
                                Loop
                                LvDetalle.ListItems(i).SubItems(9) = Mid(LvDetalle.ListItems(i).SubItems(9), 1, Len(LvDetalle.ListItems(i).SubItems(9)) - 1)
                                
                            End With
                        End If
                    End If
                    'Aprovechamos el recorrido de la grilla para obtenerr el saldo general
                    If Val(LvDetalle.ListItems(i)) > 0 And Sm_CliPro = "CLI" Then
                            TGENERAL = TGENERAL + (LvDetalle.ListItems(i).SubItems(6) - LvDetalle.ListItems(i).SubItems(7))
                            LvDetalle.ListItems(i).SubItems(9) = TGENERAL
                    
                    
                    ElseIf Val(LvDetalle.ListItems(i)) > 0 And Sm_CliPro = "PRO" Then
                            TGENERAL = TGENERAL + (LvDetalle.ListItems(i).SubItems(7) - LvDetalle.ListItems(i).SubItems(6))
                            LvDetalle.ListItems(i).SubItems(9) = TGENERAL
                    End If
                Next
                'lvdetalle.Icons=
                For i = 1 To LvDetalle.ListItems.Count
                    If Val(LvDetalle.ListItems(i)) > 0 Then
                        LvDetalle.ListItems(i).SubItems(6) = NumFormat(LvDetalle.ListItems(i).SubItems(6))
                        LvDetalle.ListItems(i).SubItems(7) = NumFormat(LvDetalle.ListItems(i).SubItems(7))
                        LvDetalle.ListItems(i).SubItems(9) = NumFormat(LvDetalle.ListItems(i).SubItems(9))
                        LvDetalle.ListItems(i).SubItems(8) = NumFormat(LvDetalle.ListItems(i).SubItems(8))
                        If InStr(LvDetalle.ListItems(i).SubItems(1), "MULTIPLE") > 0 Then
                            LvDetalle.ListItems(i).ListSubItems(1).ForeColor = vbBlue
                            LvDetalle.ListItems(i).ListSubItems(1).Bold = True
                        End If
                        
                    End If
                Next
                               
            End With
        End If
End Sub

Private Sub ObtenerPagosNotaDebito(Nro_Unico As Long)
    Dim Rp_PagoND As Recordset, sql2 As String, Sp_Fpago As String
    
                    'Aqui consultamos los abonos del documento
                            
    sql2 = "SELECT '',abo_fecha_pago,nombredoc,0 ref,ctd_monto bruto,0 abonos, " & _
                   "0 pad_nro_transaccion,'' pad_tipo_deposito,0 mpa_id,abo_id,'' doc_nota_de_debito,0 id " & _
               "FROM vi_fpagos WHERE id=" & Nro_Unico & " AND abo_cli_pro='" & Sm_CliPro & "'  "

    Consulta Rp_PagoND, sql2
    If Rp_PagoND.RecordCount > 0 Then
        Rp_PagoND.MoveFirst
        Do While Not Rp_PagoND.EOF
            Sp_Fpago = Rp_PagoND!pad_nro_transaccion
            If Rp_PagoND!mpa_id = 2 Then Sp_Fpago = Rp_PagoND!chequito
                
            LvDetalle.ListItems.Add , , Nro_Unico
            X = LvDetalle.ListItems.Count
            LvDetalle.ListItems(X).SubItems(1) = "-->" & Rp_PagoND!NombreDoc '& " " & Sp_Fpago
            LvDetalle.ListItems(X).SubItems(2) = Rp_PagoND!ref
            LvDetalle.ListItems(X).SubItems(3) = Rp_PagoND!abo_fecha_pago
             If Sm_CliPro = "PRO" Then
                LvDetalle.ListItems(X).SubItems(6) = Rp_PagoND!bruto
                LvDetalle.ListItems(X).SubItems(7) = 0
               ' If RsTmp2!doc_nota_de_debito = "SI" Then
               '     LvDetalle.ListItems(X).SubItems(6) = RsTmp2!abonos
               '     LvDetalle.ListItems(X).SubItems(7) = RsTmp2!bruto
               ' End If
            Else
                LvDetalle.ListItems(X).SubItems(6) = 0
                LvDetalle.ListItems(X).SubItems(7) = Rp_PagoND!bruto
                
               ' If RsTmp2!doc_nota_de_debito = "SI" Then
               '      LvDetalle.ListItems(X).SubItems(6) = RsTmp2!bruto
               '      LvDetalle.ListItems(X).SubItems(7) = RsTmp2!abonos
               ' Else
                   ' LvDetalle.ListItems(X).SubItems(6) = RsTmp2!bruto
                   ' LvDetalle.ListItems(X).SubItems(7) = RsTmp2!abonos
                
               ' End If
                
            End If
            LvDetalle.ListItems(X).SubItems(12) = Rp_PagoND!mpa_id
            LvDetalle.ListItems(X).SubItems(13) = Rp_PagoND!abo_id
            
            If Sm_CliPro = "PRO" Then
                 valorarestar = Rp_PagoND!bruto
                 'If RsTmp2!doc_nota_de_debito = "SI" Then valorarestar = (RsTmp2!bruto - RsTmp2!abonos) * -1
                '   valorarestar = (Rp_PagoND!bruto) * -1
                 LvDetalle.ListItems(X).SubItems(8) = Val(LvDetalle.ListItems(X - 1).SubItems(8)) - valorarestar
             Else
                 valorarestar = Rp_PagoND!bruto
                ' If RsTmp2!doc_nota_de_debito = "SI" Then valorarestar = (RsTmp2!bruto - RsTmp2!abonos) * -1
                ' valorarestar = (Rp_PagoND!bruto) * -1
                 LvDetalle.ListItems(X).SubItems(8) = Val(LvDetalle.ListItems(X - 1).SubItems(8)) - valorarestar
             End If
            Rp_PagoND.MoveNext
        Loop
    End If
End Sub
Private Sub ImprimeCartola()
    Dim Sp_Texto As String
    Dim Sp_Lh As String
    Dim Lp_Contador As Long
    Dim Ip_LineasPorPaginas As Integer
    
    On Error GoTo ErrorP
    Ip_LineasPorPaginas = 50
    Cx = 2
    Cy = 1
    For i = 1 To 147
        Sp_Lh = Sp_Lh & "="
    Next
    
 
        
        Lp_Contador = 1
        Im_Paginas = 1
        Ip_Paginas = Int(LvCartolaHistorica.ListItems.Count / Ip_LineasPorPaginas)
        If (LvCartolaHistorica.ListItems.Count Mod Ip_LineasPorPaginas) > 0 Then
            Ip_Paginas = Ip_Paginas + 1
        End If
        ResumenEncabezadoLaser
        Cy = Cy + Dp
        
        For i = 1 To Me.LvCartolaHistorica.ListItems.Count
            P_Fecha = LvCartolaHistorica.ListItems(i)
            P_Documento = LvCartolaHistorica.ListItems(i).SubItems(1)
            P_Numero = LvCartolaHistorica.ListItems(i).SubItems(2)
            P_Vence = LvCartolaHistorica.ListItems(i).SubItems(3)
            RSet P_Venta = LvCartolaHistorica.ListItems(i).SubItems(4)
            RSet P_Pago = LvCartolaHistorica.ListItems(i).SubItems(5)
            RSet P_Saldo = LvCartolaHistorica.ListItems(i).SubItems(7)
            EnviaLineasC False
            Cy = Cy + Dp
            Lp_Contador = Lp_Contador + 1
            If Lp_Contador = Ip_LineasPorPaginas Then
                Printer.NewPage
                Im_Paginas = Im_Paginas + 1
                ResumenEncabezadoLaser
                Cy = Cy + Dp
                Lp_Contador = 1
            End If
        Next
        PieDeCartola
        Printer.NewPage
        Printer.EndDoc
        'FIN DEL DOCUMETNO"
    Exit Sub
ErrorP:
    MsgBox "Problema al imprimir..." & vbNewLine & Err.Number & vbNewLine & Err.Description & vbNewLine & Err.Source
  

End Sub

Private Sub ResumenEncabezadoLaser()
    Dim Sp_Lh As String, Sp_Tit As String
    Dim Sp_Cetras As String
    
    
    For i = 1 To 100
        Sp_Lh = Sp_Lh & "_"
    Next
    Printer.FontName = "Courier New"
    

    Dp = 0.35
    Cx = 2
    Cy = 1.5
    Coloca Cx + 15, Cy, "P�gina:" & Im_Paginas & " de " & Ip_Paginas, False, 8
    Cy = Cy + Dp
    Coloca Cx + 15, Cy, "Fecha :" & Date, False, 8
    
    Cx = 2
    Cy = 2
   
    
    Cy = Cy + Dp
    'Datos de la empresa
    '2-Abril-2015
    Coloca Cx, Cy, LvEmpresa.ListItems(1).SubItems(1), True, 10 'Nombre empresa
    Cy = Cy + Dp
    Coloca Cx, Cy, LvEmpresa.ListItems(1), False, 10 'rut
    Cy = Cy + Dp
    Coloca Cx, Cy, LvEmpresa.ListItems(1).SubItems(2) & " " & LvEmpresa.ListItems(1).SubItems(4), False, 10 'direccion
    Cy = Cy + Dp
    Coloca Cx, Cy, LvEmpresa.ListItems(1).SubItems(3), False, 10 'giro
    Cy = Cy + Dp
    Coloca Cx, Cy, Sp_Lh, False, 8
    Cy = Cy + Dp
    Coloca Cx, Cy, "                " & "CARTOLA CLIENTE", True, 14 ' Titulo
    Cy = Cy + Dp
    Coloca Cx, Cy, Sp_Lh, False, 8
    
 
    'Ahora datos del cliente
    Cy = Cy + (Dp * 2)
    Coloca Cx, Cy, "R.U.T.:" & TxtRut & "       NOMBRE:" & Me.txtCliente, True, 10 'Nombre empresa
    Cy = Cy + Dp + Dp
    Coloca Cx, Cy, "DIRECCION:" & TxtDireccion & "       CIUDAD:" & TxtCiudad, False, 10 'rut
    Cy = Cy + Dp + Dp
    Coloca Cx, Cy, Sp_Lh, False, 8
  
    P_Fecha = "Fecha"
    P_Documento = "Documento"
    P_Numero = "Numero"
    P_Vence = "Venc."
    RSet P_Venta = "Monto Vta"
    RSet P_Pago = "Abono"
    RSet P_Saldo = "Saldo"
    Cy = Cy + Dp
    EnviaLineasC True
    Cy = Cy + Dp
     Printer.FontName = "Courier New"
    Coloca Cx, Cy, Sp_Lh, False, 8
     
    
    
    
    
End Sub

Private Sub PieDeCartola()
    Dim Sp_Lh As String, Sp_Tit As String
    Dim Sp_Cetras As String
    For i = 1 To 100
        Sp_Lh = Sp_Lh & "_"
    Next
    Printer.FontName = "Courier New"
    Cy = Cy + Dp
    P_Fecha = ""
    P_Documento = "TOTALES:"
    P_Numero = ""
    P_Vence = ""
    RSet P_Venta = Me.TxtCtVentas
    RSet P_Pago = TxtCtAbonos
    RSet P_Saldo = TxtCtSaldo
    Coloca Cx, Cy, Sp_Lh, False, 8
    Cy = Cy + Dp
    EnviaLineasC True
    Cy = Cy + Dp
     Printer.FontName = "Courier New"
    Coloca Cx, Cy, Sp_Lh, False, 8
End Sub


Private Function EnviaLineasC(Negrita As Boolean)
        Dim avance As Double
        avance = 1.5
        Printer.FontSize = 10
        Printer.FontName = "Arial Narrow"
        
        Coloca Cx, Cy, P_Fecha, Negrita, 10
        Coloca Cx + 1.7, Cy, P_Documento, Negrita, 10
        Coloca Cx + 7, Cy, P_Numero, Negrita, 10
        Coloca Cx + 8.6, Cy, P_Vence, Negrita, 10
        Coloca (Cx + 12) - Printer.TextWidth(P_Venta), Cy, P_Venta, Negrita, 10
        Coloca (Cx + 14.2) - Printer.TextWidth(P_Pago), Cy, P_Pago, Negrita, 10
        Coloca (Cx + 17) - Printer.TextWidth(P_Saldo), Cy, P_Saldo, Negrita, 10
       
       
End Function


