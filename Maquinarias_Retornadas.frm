VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form Maquinarias_Retornadas 
   Caption         =   "Maquinarias Retornadas"
   ClientHeight    =   4260
   ClientLeft      =   1605
   ClientTop       =   1545
   ClientWidth     =   12585
   LinkTopic       =   "Form1"
   ScaleHeight     =   4260
   ScaleWidth      =   12585
   Begin VB.Frame Maquinarias_Retornadas 
      Caption         =   "Maquinarias_"
      Height          =   3900
      Left            =   60
      TabIndex        =   0
      Top             =   45
      Width           =   12330
      Begin MSComctlLib.ListView LvProductos 
         Height          =   1890
         Left            =   180
         TabIndex        =   1
         Top             =   1035
         Width           =   11970
         _ExtentX        =   21114
         _ExtentY        =   3334
         View            =   3
         LabelWrap       =   0   'False
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   7
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Id Detalle"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Numero Contrato"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Codigo Producto"
            Object.Width           =   2822
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "Codigo Empresa"
            Object.Width           =   2822
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Text            =   "Descripcion"
            Object.Width           =   7056
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Object.Tag             =   "N100"
            Text            =   "Valor"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   6
            Text            =   "Estado Producto"
            Object.Width           =   2540
         EndProperty
      End
   End
End
Attribute VB_Name = "Maquinarias_Retornadas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Form_Load()
              Sql = "SELECT arr_id,arr_rut_cliente,arr_nombre_cliente,DATE_FORMAT(arr_fecha_contrato,'%d-%m-%Y')arr_fecha_contrato,arr_direccion_obra,arr_valor,arr_estado " & _
               "FROM ven_arriendo " & _
               "WHERE arr_fecha_contrato BETWEEN '" & Format(DTInicio.Value, "YYYY-MM-DD") & "' AND '" & Format(DtHasta.Value, "YYYY-MM-DD") & "' and arr_id =   TxtConsulta  "
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvProductos, False, True, True, False
End Sub

