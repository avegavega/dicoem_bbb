VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "MSDATGRD.OCX"
Begin VB.Form CompraProducto 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Ingreso de Productos y Veh�culos"
   ClientHeight    =   9315
   ClientLeft      =   -225
   ClientTop       =   930
   ClientWidth     =   11955
   ClipControls    =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   9315
   ScaleWidth      =   11955
   StartUpPosition =   2  'CenterScreen
   Begin MSComDlg.CommonDialog dialogo 
      Left            =   5520
      Top             =   7680
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.Frame FramAc 
      Height          =   855
      Left            =   120
      TabIndex        =   92
      Top             =   8040
      Width           =   5895
      Begin VB.CommandButton CmdSalir 
         Caption         =   "&Salir"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   4560
         TabIndex        =   96
         Top             =   240
         Width           =   1215
      End
      Begin VB.CommandButton CmdGuardar 
         Caption         =   "&Guardar"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   120
         TabIndex        =   95
         Top             =   240
         Width           =   1215
      End
      Begin VB.CommandButton CmdCancelAct 
         Caption         =   "No Modificar"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   2880
         TabIndex        =   94
         Top             =   240
         Visible         =   0   'False
         Width           =   1635
      End
      Begin VB.CommandButton CmdModificar 
         Caption         =   "&Modificar"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   1440
         TabIndex        =   93
         Top             =   240
         Width           =   1395
      End
   End
   Begin VB.FileListBox ListaMarcas 
      Height          =   480
      Left            =   1920
      TabIndex        =   85
      Top             =   8400
      Visible         =   0   'False
      Width           =   1695
   End
   Begin TabDlg.SSTab SuperTab 
      Height          =   6015
      Left            =   120
      TabIndex        =   24
      Top             =   1320
      Width           =   11535
      _ExtentX        =   20346
      _ExtentY        =   10610
      _Version        =   393216
      Tab             =   1
      TabHeight       =   520
      ShowFocusRect   =   0   'False
      TabCaption(0)   =   "REPUESTOS"
      TabPicture(0)   =   "CompraProducto.frx":0000
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "Frame3"
      Tab(0).Control(1)=   "Frame5"
      Tab(0).Control(2)=   "Frame6"
      Tab(0).ControlCount=   3
      TabCaption(1)   =   "VEHICULOS NUEVOS"
      TabPicture(1)   =   "CompraProducto.frx":001C
      Tab(1).ControlEnabled=   -1  'True
      Tab(1).Control(0)=   "FrameNuevo"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).ControlCount=   1
      TabCaption(2)   =   "VEHICULOS USADOS"
      TabPicture(2)   =   "CompraProducto.frx":0038
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "FrameUsado"
      Tab(2).ControlCount=   1
      Begin VB.Frame Frame6 
         Caption         =   "Otros"
         Height          =   855
         Left            =   -70800
         TabIndex        =   133
         Top             =   480
         Width           =   6975
         Begin VB.TextBox TxOS 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   1440
            TabIndex        =   135
            Top             =   480
            Visible         =   0   'False
            Width           =   735
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel13 
            Height          =   255
            Left            =   240
            OleObjectBlob   =   "CompraProducto.frx":0054
            TabIndex        =   137
            Top             =   480
            Visible         =   0   'False
            Width           =   3375
         End
         Begin VB.TextBox TxValorFacArriendo 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   3480
            TabIndex        =   136
            Top             =   480
            Visible         =   0   'False
            Width           =   1215
         End
         Begin VB.CheckBox Check2 
            Caption         =   "Factura por reparacion vehiculo en Arriendo"
            Height          =   255
            Left            =   240
            TabIndex        =   134
            Top             =   240
            Width           =   3495
         End
      End
      Begin VB.Frame Frame5 
         Caption         =   "Clasificacion"
         Height          =   615
         Left            =   -74880
         TabIndex        =   128
         Top             =   480
         Width           =   3975
         Begin VB.OptionButton Option2 
            Caption         =   "Multimarca"
            Height          =   255
            Left            =   2400
            TabIndex        =   130
            Top             =   240
            Width           =   1455
         End
         Begin VB.OptionButton Option1 
            Caption         =   "Citroen"
            Height          =   255
            Left            =   240
            TabIndex        =   129
            Top             =   240
            Value           =   -1  'True
            Width           =   1455
         End
      End
      Begin VB.Frame FrameUsado 
         Height          =   4575
         Left            =   -74640
         TabIndex        =   77
         Top             =   360
         Width           =   10815
         Begin VB.CommandButton CmdMM 
            Caption         =   "M"
            Height          =   345
            Index           =   1
            Left            =   4800
            TabIndex        =   139
            ToolTipText     =   "Mantenedor de Marcas y Modelos"
            Top             =   975
            Width           =   285
         End
         Begin VB.ComboBox CboTransferido 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            ItemData        =   "CompraProducto.frx":0120
            Left            =   4800
            List            =   "CompraProducto.frx":012A
            Style           =   2  'Dropdown List
            TabIndex        =   132
            Top             =   3120
            Width           =   1335
         End
         Begin VB.TextBox TxtUbicacion 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   405
            Left            =   1920
            TabIndex        =   124
            Top             =   3120
            Width           =   1575
         End
         Begin VB.TextBox TxtComentario 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   405
            Left            =   1920
            TabIndex        =   123
            Top             =   3960
            Width           =   4935
         End
         Begin VB.TextBox TxtCombus 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   405
            Left            =   1920
            TabIndex        =   120
            Top             =   2760
            Width           =   1575
         End
         Begin VB.TextBox Txtmotor 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   405
            Left            =   1920
            TabIndex        =   119
            Top             =   2400
            Width           =   1575
         End
         Begin VB.TextBox TxtColor 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   405
            Left            =   1920
            TabIndex        =   118
            Top             =   2040
            Width           =   1575
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel12 
            Height          =   255
            Index           =   0
            Left            =   600
            OleObjectBlob   =   "CompraProducto.frx":0136
            TabIndex        =   110
            Top             =   2880
            Width           =   1095
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel11 
            Height          =   255
            Left            =   720
            OleObjectBlob   =   "CompraProducto.frx":01A3
            TabIndex        =   109
            Top             =   2520
            Width           =   975
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel10 
            Height          =   255
            Left            =   720
            OleObjectBlob   =   "CompraProducto.frx":0204
            TabIndex        =   108
            Top             =   2160
            Width           =   975
         End
         Begin VB.TextBox TxtPVenta 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   4800
            TabIndex        =   122
            Top             =   3600
            Width           =   1335
         End
         Begin VB.Frame Frame4 
            Height          =   2055
            Left            =   6240
            TabIndex        =   97
            Top             =   360
            Width           =   4215
            Begin VB.ComboBox CboDocVenta 
               Height          =   315
               ItemData        =   "CompraProducto.frx":0265
               Left            =   2280
               List            =   "CompraProducto.frx":0272
               Style           =   2  'Dropdown List
               TabIndex        =   103
               Top             =   1080
               Visible         =   0   'False
               Width           =   1575
            End
            Begin VB.TextBox TxtNFacVenta 
               Alignment       =   1  'Right Justify
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   375
               Left            =   2280
               TabIndex        =   102
               Top             =   1440
               Visible         =   0   'False
               Width           =   1575
            End
            Begin ACTIVESKINLibCtl.SkinLabel SkFventa 
               Height          =   255
               Left            =   1080
               OleObjectBlob   =   "CompraProducto.frx":0296
               TabIndex        =   101
               Top             =   1440
               Visible         =   0   'False
               Width           =   1335
            End
            Begin VB.OptionButton Op2 
               Caption         =   "Parte de pago"
               Enabled         =   0   'False
               Height          =   255
               Left            =   2280
               TabIndex        =   100
               Top             =   600
               Width           =   1695
            End
            Begin VB.OptionButton Op1 
               Caption         =   "Consignacion"
               Enabled         =   0   'False
               Height          =   255
               Left            =   360
               TabIndex        =   99
               Top             =   600
               Value           =   -1  'True
               Width           =   1815
            End
            Begin VB.CheckBox CheckConsignacion 
               Caption         =   "Veh�culo recibido en:"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   375
               Left            =   240
               TabIndex        =   98
               Top             =   240
               Width           =   2415
            End
            Begin ACTIVESKINLibCtl.SkinLabel SkDventa 
               Height          =   255
               Left            =   840
               OleObjectBlob   =   "CompraProducto.frx":030E
               TabIndex        =   104
               Top             =   1080
               Visible         =   0   'False
               Width           =   1335
            End
         End
         Begin VB.Frame Frame2 
            Caption         =   "I.V.A."
            Height          =   1455
            Left            =   7200
            TabIndex        =   86
            Top             =   2880
            Width           =   3255
            Begin ACTIVESKINLibCtl.SkinLabel SkinLabel8 
               Height          =   375
               Index           =   0
               Left            =   1200
               OleObjectBlob   =   "CompraProducto.frx":038A
               TabIndex        =   90
               Top             =   600
               Width           =   495
            End
            Begin VB.TextBox TxtABruto 
               Alignment       =   1  'Right Justify
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   12
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   375
               Left            =   1800
               Locked          =   -1  'True
               TabIndex        =   89
               Top             =   840
               Width           =   1335
            End
            Begin VB.TextBox TxtAIVA 
               Alignment       =   1  'Right Justify
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   12
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   375
               Left            =   1800
               Locked          =   -1  'True
               TabIndex        =   88
               Top             =   480
               Width           =   1335
            End
            Begin VB.CheckBox Check1 
               Caption         =   "AGREGAR IVA"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Left            =   120
               TabIndex        =   87
               Top             =   240
               Width           =   1455
            End
            Begin ACTIVESKINLibCtl.SkinLabel SkinLabel8 
               Height          =   255
               Index           =   1
               Left            =   480
               OleObjectBlob   =   "CompraProducto.frx":03F4
               TabIndex        =   91
               Top             =   960
               Width           =   1215
            End
         End
         Begin VB.TextBox TxtPatente 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   1920
            TabIndex        =   112
            Top             =   240
            Width           =   2895
         End
         Begin VB.ComboBox cboano 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   405
            Left            =   1920
            Style           =   2  'Dropdown List
            TabIndex        =   116
            Top             =   1680
            Width           =   1215
         End
         Begin VB.TextBox TxtCtaKm 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   4800
            TabIndex        =   117
            Top             =   2760
            Width           =   1335
         End
         Begin VB.TextBox TxtPrecioCpra 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   1920
            TabIndex        =   121
            Top             =   3600
            Width           =   1575
         End
         Begin VB.ComboBox CboMarca 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Left            =   1920
            Style           =   2  'Dropdown List
            TabIndex        =   114
            Top             =   960
            Width           =   2895
         End
         Begin VB.ComboBox CboModelo 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Left            =   1920
            Style           =   2  'Dropdown List
            TabIndex        =   115
            Top             =   1320
            Width           =   2895
         End
         Begin VB.TextBox TxtTipoVehiculo 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   1920
            TabIndex        =   113
            Top             =   600
            Width           =   2895
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
            Height          =   255
            Index           =   1
            Left            =   720
            OleObjectBlob   =   "CompraProducto.frx":0468
            TabIndex        =   78
            Top             =   240
            Width           =   975
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
            Height          =   255
            Index           =   2
            Left            =   720
            OleObjectBlob   =   "CompraProducto.frx":04CD
            TabIndex        =   79
            Top             =   960
            Width           =   975
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
            Height          =   255
            Index           =   3
            Left            =   720
            OleObjectBlob   =   "CompraProducto.frx":052E
            TabIndex        =   80
            Top             =   1320
            Width           =   975
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
            Height          =   375
            Index           =   4
            Left            =   480
            OleObjectBlob   =   "CompraProducto.frx":0591
            TabIndex        =   81
            Top             =   1680
            Width           =   1215
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
            Height          =   375
            Index           =   5
            Left            =   3720
            OleObjectBlob   =   "CompraProducto.frx":05EE
            TabIndex        =   82
            Top             =   2760
            Width           =   855
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
            Height          =   255
            Index           =   6
            Left            =   480
            OleObjectBlob   =   "CompraProducto.frx":0672
            TabIndex        =   83
            Top             =   3600
            Width           =   1215
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
            Height          =   255
            Index           =   23
            Left            =   480
            OleObjectBlob   =   "CompraProducto.frx":06E1
            TabIndex        =   84
            Top             =   600
            Width           =   1215
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel9 
            Height          =   255
            Left            =   3600
            OleObjectBlob   =   "CompraProducto.frx":0752
            TabIndex        =   105
            Top             =   3600
            Width           =   975
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
            Height          =   255
            Index           =   25
            Left            =   480
            OleObjectBlob   =   "CompraProducto.frx":07C8
            TabIndex        =   111
            Top             =   4080
            Width           =   1215
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel12 
            Height          =   255
            Index           =   1
            Left            =   600
            OleObjectBlob   =   "CompraProducto.frx":0833
            TabIndex        =   125
            Top             =   3240
            Width           =   1095
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel12 
            Height          =   255
            Index           =   3
            Left            =   3480
            OleObjectBlob   =   "CompraProducto.frx":089C
            TabIndex        =   131
            Top             =   3240
            Width           =   1215
         End
      End
      Begin VB.Frame FrameNuevo 
         Height          =   3735
         Left            =   2280
         TabIndex        =   43
         Top             =   480
         Width           =   8175
         Begin VB.CommandButton CmdMM 
            Caption         =   "M"
            Height          =   360
            Index           =   0
            Left            =   3615
            TabIndex        =   138
            ToolTipText     =   "Mantenedor de Marcas y Modelos"
            Top             =   1095
            Width           =   285
         End
         Begin VB.TextBox NTxtUbicacion 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   405
            Left            =   1440
            TabIndex        =   126
            Top             =   3240
            Width           =   2175
         End
         Begin VB.TextBox NtxtPventa 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   5760
            TabIndex        =   106
            Top             =   3240
            Width           =   2175
         End
         Begin VB.TextBox NTxtTipo 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   1440
            TabIndex        =   46
            Top             =   720
            Width           =   2175
         End
         Begin VB.ComboBox NcboModelo 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   405
            Left            =   1440
            Style           =   2  'Dropdown List
            TabIndex        =   48
            Top             =   1440
            Width           =   2175
         End
         Begin VB.ComboBox NcboMarca 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   405
            Left            =   1440
            Style           =   2  'Dropdown List
            TabIndex        =   47
            Top             =   1080
            Width           =   2175
         End
         Begin VB.TextBox NTXTPrecioCompra 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   5760
            TabIndex        =   68
            Top             =   2880
            Width           =   2175
         End
         Begin VB.TextBox NTxtColor 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   1440
            TabIndex        =   49
            Top             =   2160
            Width           =   2175
         End
         Begin VB.ComboBox NcboAno 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   405
            Left            =   1440
            Style           =   2  'Dropdown List
            TabIndex        =   50
            Top             =   1800
            Width           =   2175
         End
         Begin VB.TextBox NTxtPatente 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   1440
            TabIndex        =   45
            Text            =   "0"
            Top             =   360
            Width           =   2175
         End
         Begin VB.TextBox NTxtPuertas 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   1440
            TabIndex        =   51
            Top             =   2520
            Width           =   2175
         End
         Begin VB.TextBox NTxtAsientos 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   1440
            TabIndex        =   52
            Top             =   2880
            Width           =   2175
         End
         Begin VB.TextBox NTxtCarga 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   5760
            TabIndex        =   54
            Top             =   360
            Width           =   2175
         End
         Begin VB.TextBox NTxtPeso 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   5760
            TabIndex        =   56
            Top             =   720
            Width           =   2175
         End
         Begin VB.TextBox NTxtCombustibl 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   5760
            TabIndex        =   58
            Top             =   1080
            Width           =   2175
         End
         Begin VB.TextBox NTxtMotor 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   5760
            TabIndex        =   60
            Top             =   1440
            Width           =   2175
         End
         Begin VB.TextBox NTxtChasis 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   5760
            TabIndex        =   62
            Top             =   1800
            Width           =   2175
         End
         Begin VB.TextBox NTxtSerie 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   5760
            TabIndex        =   64
            Top             =   2160
            Width           =   2175
         End
         Begin VB.TextBox TxtNVIN 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   5760
            TabIndex        =   66
            Top             =   2520
            Width           =   2175
         End
         Begin VB.CommandButton CmdLimpiaNew 
            Caption         =   "Limpia cajas"
            Height          =   255
            Left            =   240
            TabIndex        =   44
            Top             =   5520
            Width           =   1575
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
            Height          =   255
            Index           =   7
            Left            =   300
            OleObjectBlob   =   "CompraProducto.frx":0910
            TabIndex        =   53
            Top             =   360
            Width           =   1000
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
            Height          =   255
            Index           =   8
            Left            =   300
            OleObjectBlob   =   "CompraProducto.frx":0975
            TabIndex        =   55
            Top             =   1080
            Width           =   1000
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
            Height          =   375
            Index           =   9
            Left            =   300
            OleObjectBlob   =   "CompraProducto.frx":09D6
            TabIndex        =   57
            Top             =   1440
            Width           =   1000
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
            Height          =   375
            Index           =   10
            Left            =   300
            OleObjectBlob   =   "CompraProducto.frx":0A39
            TabIndex        =   59
            Top             =   1800
            Width           =   1000
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
            Height          =   375
            Index           =   12
            Left            =   4500
            OleObjectBlob   =   "CompraProducto.frx":0A96
            TabIndex        =   61
            Top             =   2880
            Width           =   1200
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
            Height          =   255
            Index           =   13
            Left            =   300
            OleObjectBlob   =   "CompraProducto.frx":0B05
            TabIndex        =   63
            Top             =   720
            Width           =   1000
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
            Height          =   375
            Index           =   11
            Left            =   300
            OleObjectBlob   =   "CompraProducto.frx":0B76
            TabIndex        =   65
            Top             =   2160
            Width           =   1000
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
            Height          =   375
            Index           =   14
            Left            =   300
            OleObjectBlob   =   "CompraProducto.frx":0BD7
            TabIndex        =   67
            Top             =   2520
            Width           =   1000
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
            Height          =   375
            Index           =   15
            Left            =   300
            OleObjectBlob   =   "CompraProducto.frx":0C3C
            TabIndex        =   69
            Top             =   2880
            Width           =   1000
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
            Height          =   375
            Index           =   16
            Left            =   4500
            OleObjectBlob   =   "CompraProducto.frx":0CA3
            TabIndex        =   70
            Top             =   360
            Width           =   1200
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
            Height          =   375
            Index           =   17
            Left            =   4500
            OleObjectBlob   =   "CompraProducto.frx":0D04
            TabIndex        =   71
            Top             =   720
            Width           =   1200
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
            Height          =   375
            Index           =   18
            Left            =   4500
            OleObjectBlob   =   "CompraProducto.frx":0D63
            TabIndex        =   72
            Top             =   1080
            Width           =   1200
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
            Height          =   375
            Index           =   19
            Left            =   4500
            OleObjectBlob   =   "CompraProducto.frx":0DD0
            TabIndex        =   73
            Top             =   1440
            Width           =   1200
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
            Height          =   375
            Index           =   20
            Left            =   4500
            OleObjectBlob   =   "CompraProducto.frx":0E31
            TabIndex        =   74
            Top             =   1800
            Width           =   1200
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
            Height          =   375
            Index           =   21
            Left            =   4500
            OleObjectBlob   =   "CompraProducto.frx":0E94
            TabIndex        =   75
            Top             =   2160
            Width           =   1200
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
            Height          =   375
            Index           =   22
            Left            =   4500
            OleObjectBlob   =   "CompraProducto.frx":0EF5
            TabIndex        =   76
            Top             =   2520
            Width           =   1200
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
            Height          =   375
            Index           =   24
            Left            =   4500
            OleObjectBlob   =   "CompraProducto.frx":0F52
            TabIndex        =   107
            Top             =   3240
            Width           =   1200
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel12 
            Height          =   255
            Index           =   2
            Left            =   300
            OleObjectBlob   =   "CompraProducto.frx":0FC1
            TabIndex        =   127
            Top             =   3360
            Width           =   1000
         End
      End
      Begin VB.Frame Frame3 
         Caption         =   "Repuestos del documento"
         Height          =   4455
         Left            =   -74880
         TabIndex        =   25
         Top             =   1320
         Width           =   11175
         Begin VB.TextBox TxtCodigo 
            Appearance      =   0  'Flat
            Height          =   285
            Left            =   960
            TabIndex        =   30
            Top             =   480
            Width           =   1335
         End
         Begin VB.TextBox TxtPrecio 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "0,00"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   13322
               SubFormatType   =   1
            EndProperty
            Height          =   285
            Left            =   6120
            TabIndex        =   31
            Top             =   480
            Width           =   1215
         End
         Begin VB.TextBox TxtMarca 
            Appearance      =   0  'Flat
            Height          =   285
            Left            =   2280
            Locked          =   -1  'True
            TabIndex        =   29
            Top             =   480
            Width           =   1335
         End
         Begin VB.TextBox TxtDescripcion 
            Appearance      =   0  'Flat
            Height          =   285
            Left            =   3600
            Locked          =   -1  'True
            TabIndex        =   28
            Top             =   480
            Width           =   2535
         End
         Begin VB.TextBox TxtUnidades 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            Height          =   285
            Left            =   8280
            TabIndex        =   32
            Top             =   480
            Width           =   975
         End
         Begin VB.TextBox TxtSubStotal 
            Appearance      =   0  'Flat
            Height          =   285
            Left            =   9240
            Locked          =   -1  'True
            TabIndex        =   27
            Top             =   480
            Width           =   1095
         End
         Begin VB.CommandButton CmdBuscaProducto 
            Caption         =   "Buscar"
            Height          =   375
            Left            =   120
            TabIndex        =   26
            Top             =   360
            Width           =   735
         End
         Begin VB.CommandButton CmdAgrega 
            Caption         =   "Agrega"
            Height          =   350
            Left            =   10400
            TabIndex        =   33
            Top             =   450
            Width           =   735
         End
         Begin VB.TextBox TxtStockActual 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H000000FF&
            Height          =   285
            Left            =   7320
            Locked          =   -1  'True
            TabIndex        =   36
            Top             =   480
            Width           =   975
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel6 
            Height          =   255
            Left            =   960
            OleObjectBlob   =   "CompraProducto.frx":102A
            TabIndex        =   34
            Top             =   240
            Width           =   9615
         End
         Begin MSDataGridLib.DataGrid GridRepuestos 
            Bindings        =   "CompraProducto.frx":1192
            Height          =   3495
            Left            =   120
            TabIndex        =   35
            Top             =   840
            Width           =   10695
            _ExtentX        =   18865
            _ExtentY        =   6165
            _Version        =   393216
            AllowUpdate     =   0   'False
            AllowArrows     =   -1  'True
            Enabled         =   -1  'True
            HeadLines       =   1
            RowHeight       =   15
            TabAction       =   1
            FormatLocked    =   -1  'True
            AllowDelete     =   -1  'True
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ColumnCount     =   7
            BeginProperty Column00 
               DataField       =   "COD_PRODUCTO"
               Caption         =   "COD_PRODUCTO"
               BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
                  Type            =   0
                  Format          =   ""
                  HaveTrueFalseNull=   0
                  FirstDayOfWeek  =   0
                  FirstWeekOfYear =   0
                  LCID            =   13322
                  SubFormatType   =   0
               EndProperty
            EndProperty
            BeginProperty Column01 
               DataField       =   "MARCA_REPUESTO"
               Caption         =   "MARCA"
               BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
                  Type            =   0
                  Format          =   ""
                  HaveTrueFalseNull=   0
                  FirstDayOfWeek  =   0
                  FirstWeekOfYear =   0
                  LCID            =   13322
                  SubFormatType   =   0
               EndProperty
            EndProperty
            BeginProperty Column02 
               DataField       =   "DESCRIPCION"
               Caption         =   "DESCRIPCION"
               BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
                  Type            =   0
                  Format          =   ""
                  HaveTrueFalseNull=   0
                  FirstDayOfWeek  =   0
                  FirstWeekOfYear =   0
                  LCID            =   13322
                  SubFormatType   =   0
               EndProperty
            EndProperty
            BeginProperty Column03 
               DataField       =   "UNIDADES"
               Caption         =   "UNIDADES"
               BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
                  Type            =   0
                  Format          =   ""
                  HaveTrueFalseNull=   0
                  FirstDayOfWeek  =   0
                  FirstWeekOfYear =   0
                  LCID            =   13322
                  SubFormatType   =   0
               EndProperty
            EndProperty
            BeginProperty Column04 
               DataField       =   "PRECIO_COMPRA"
               Caption         =   "PRECIO"
               BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
                  Type            =   0
                  Format          =   ""
                  HaveTrueFalseNull=   0
                  FirstDayOfWeek  =   0
                  FirstWeekOfYear =   0
                  LCID            =   13322
                  SubFormatType   =   0
               EndProperty
            EndProperty
            BeginProperty Column05 
               DataField       =   "TOTAL"
               Caption         =   "TOTAL"
               BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
                  Type            =   0
                  Format          =   ""
                  HaveTrueFalseNull=   0
                  FirstDayOfWeek  =   0
                  FirstWeekOfYear =   0
                  LCID            =   13322
                  SubFormatType   =   0
               EndProperty
            EndProperty
            BeginProperty Column06 
               DataField       =   "Stock_Actual"
               Caption         =   "Nuevo Stock"
               BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
                  Type            =   0
                  Format          =   ""
                  HaveTrueFalseNull=   0
                  FirstDayOfWeek  =   0
                  FirstWeekOfYear =   0
                  LCID            =   13322
                  SubFormatType   =   0
               EndProperty
            EndProperty
            SplitCount      =   1
            BeginProperty Split0 
               BeginProperty Column00 
                  ColumnWidth     =   1454,74
               EndProperty
               BeginProperty Column01 
                  ColumnWidth     =   1635,024
               EndProperty
               BeginProperty Column02 
                  ColumnWidth     =   2310,236
               EndProperty
               BeginProperty Column03 
                  ColumnWidth     =   915,024
               EndProperty
               BeginProperty Column04 
                  ColumnWidth     =   1094,74
               EndProperty
               BeginProperty Column05 
                  ColumnWidth     =   1005,165
               EndProperty
               BeginProperty Column06 
                  ColumnWidth     =   1065,26
               EndProperty
            EndProperty
         End
         Begin VB.Label LbStockActual 
            Caption         =   "stock actual"
            Height          =   255
            Left            =   9960
            TabIndex        =   37
            Top             =   120
            Width           =   1095
         End
      End
   End
   Begin VB.Frame FrameTotales 
      Caption         =   "Totales"
      Height          =   1725
      Left            =   6240
      TabIndex        =   11
      Top             =   7425
      Width           =   5415
      Begin VB.TextBox TxtExento 
         Alignment       =   1  'Right Justify
         BeginProperty DataFormat 
            Type            =   0
            Format          =   "0,00"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   13322
            SubFormatType   =   0
         EndProperty
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   4080
         MaxLength       =   5
         TabIndex        =   140
         Text            =   "0"
         Top             =   210
         Width           =   1215
      End
      Begin VB.TextBox txtTotalBruto 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   10.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   3825
         Locked          =   -1  'True
         TabIndex        =   14
         Top             =   1200
         Width           =   1470
      End
      Begin VB.TextBox TxtDescuento 
         Alignment       =   1  'Right Justify
         BeginProperty DataFormat 
            Type            =   0
            Format          =   "0,00"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   13322
            SubFormatType   =   0
         EndProperty
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1320
         MaxLength       =   5
         TabIndex        =   13
         Top             =   585
         Width           =   1215
      End
      Begin VB.TextBox TxtDescuentoPesos 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1320
         TabIndex        =   12
         Top             =   960
         Width           =   1215
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkTotalAfecto 
         Height          =   255
         Left            =   3960
         OleObjectBlob   =   "CompraProducto.frx":11AC
         TabIndex        =   15
         Top             =   585
         Width           =   1335
      End
      Begin ACTIVESKINLibCtl.SkinLabel skkk 
         Height          =   255
         Index           =   0
         Left            =   2640
         OleObjectBlob   =   "CompraProducto.frx":1205
         TabIndex        =   16
         Top             =   585
         Width           =   1215
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel5 
         Height          =   255
         Left            =   3060
         OleObjectBlob   =   "CompraProducto.frx":127B
         TabIndex        =   17
         Top             =   915
         Width           =   735
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel7 
         Height          =   255
         Left            =   3045
         OleObjectBlob   =   "CompraProducto.frx":12DF
         TabIndex        =   18
         Top             =   1290
         Width           =   735
      End
      Begin ACTIVESKINLibCtl.SkinLabel SKIVA 
         Height          =   255
         Left            =   3480
         OleObjectBlob   =   "CompraProducto.frx":1347
         TabIndex        =   19
         Top             =   885
         Width           =   1815
      End
      Begin ACTIVESKINLibCtl.SkinLabel skkk 
         Height          =   255
         Index           =   1
         Left            =   240
         OleObjectBlob   =   "CompraProducto.frx":13A0
         TabIndex        =   20
         Top             =   600
         Width           =   975
      End
      Begin ACTIVESKINLibCtl.SkinLabel skkk 
         Height          =   255
         Index           =   2
         Left            =   240
         OleObjectBlob   =   "CompraProducto.frx":140C
         TabIndex        =   21
         Top             =   1020
         Width           =   975
      End
      Begin ACTIVESKINLibCtl.SkinLabel skkk 
         Height          =   255
         Index           =   3
         Left            =   240
         OleObjectBlob   =   "CompraProducto.frx":1476
         TabIndex        =   22
         Top             =   240
         Width           =   1095
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkTotal 
         Height          =   255
         Left            =   1200
         OleObjectBlob   =   "CompraProducto.frx":14E8
         TabIndex        =   23
         Top             =   240
         Width           =   1335
      End
      Begin ACTIVESKINLibCtl.SkinLabel skkk 
         Height          =   255
         Index           =   4
         Left            =   2640
         OleObjectBlob   =   "CompraProducto.frx":1541
         TabIndex        =   141
         Top             =   225
         Width           =   1320
      End
   End
   Begin VB.CommandButton CmdHistorico 
      Caption         =   "Ver Historico de Compras"
      Enabled         =   0   'False
      Height          =   375
      Left            =   12405
      TabIndex        =   10
      Top             =   525
      Width           =   2175
   End
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Interval        =   100
      Left            =   5520
      Top             =   7200
   End
   Begin VB.CommandButton CmdFocusCdgo 
      Caption         =   "Command1"
      Height          =   195
      Left            =   0
      TabIndex        =   8
      Top             =   3480
      Visible         =   0   'False
      Width           =   255
   End
   Begin VB.Frame Frame1 
      Caption         =   "Datos del documento"
      Height          =   1095
      Left            =   120
      TabIndex        =   3
      Top             =   120
      Width           =   11415
      Begin VB.TextBox TxtRutProveedor 
         Alignment       =   1  'Right Justify
         Height          =   315
         Left            =   6000
         TabIndex        =   42
         Top             =   480
         Width           =   1815
      End
      Begin VB.CommandButton CmdBuscaProv 
         Caption         =   "Buscar"
         Height          =   255
         Left            =   6000
         TabIndex        =   41
         Top             =   780
         Width           =   855
      End
      Begin VB.TextBox TxtRsocial 
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   7920
         Locked          =   -1  'True
         TabIndex        =   40
         Top             =   480
         Width           =   3375
      End
      Begin VB.CommandButton CmdNuevoProveedor 
         Caption         =   "Crear"
         Height          =   255
         Left            =   6960
         TabIndex        =   39
         Top             =   780
         Width           =   855
      End
      Begin VB.TextBox TxtNDoc 
         Alignment       =   1  'Right Justify
         Height          =   315
         Left            =   4320
         TabIndex        =   2
         Text            =   "0"
         Top             =   480
         Width           =   1335
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   255
         Index           =   0
         Left            =   4320
         OleObjectBlob   =   "CompraProducto.frx":15BB
         TabIndex        =   6
         Top             =   240
         Width           =   1335
      End
      Begin VB.ComboBox CboTipoDoc 
         Height          =   315
         ItemData        =   "CompraProducto.frx":1631
         Left            =   2160
         List            =   "CompraProducto.frx":163E
         Style           =   2  'Dropdown List
         TabIndex        =   1
         Top             =   480
         Width           =   1935
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   255
         Index           =   0
         Left            =   2175
         OleObjectBlob   =   "CompraProducto.frx":1663
         TabIndex        =   5
         Top             =   240
         Width           =   1575
      End
      Begin MSComCtl2.DTPicker DtFecha 
         Height          =   315
         Left            =   120
         TabIndex        =   0
         Top             =   480
         Width           =   1935
         _ExtentX        =   3413
         _ExtentY        =   556
         _Version        =   393216
         CustomFormat    =   "dd-mm-yyyy hh:mm:ss"
         Format          =   57933825
         CurrentDate     =   39855
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   375
         Left            =   120
         OleObjectBlob   =   "CompraProducto.frx":16DD
         TabIndex        =   4
         Top             =   240
         Width           =   1575
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
         Height          =   255
         Left            =   6000
         OleObjectBlob   =   "CompraProducto.frx":1753
         TabIndex        =   38
         Top             =   240
         Width           =   3975
      End
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   120
      OleObjectBlob   =   "CompraProducto.frx":17FB
      Top             =   7920
   End
   Begin VB.Label Label2 
      Caption         =   "Label2"
      DataField       =   "CODIGO"
      DataSource      =   "AdoHistorial"
      Height          =   255
      Left            =   720
      TabIndex        =   9
      Top             =   8160
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.Label Label1 
      Caption         =   "Label1"
      DataField       =   "RUT_PROVEEDOR"
      DataSource      =   "AdoCompras"
      Height          =   135
      Left            =   240
      TabIndex        =   7
      Top             =   8520
      Visible         =   0   'False
      Width           =   975
   End
End
Attribute VB_Name = "CompraProducto"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim VdDesc As Integer
Dim NoModXcien As Boolean
Dim NoModPesos As Boolean
Dim NuevoVehiculo As ADODB.Recordset
Dim NuevaCtaCte As ADODB.Recordset
Dim RS_ConsultaDoc As Recordset
Dim AdoCtaCte As ADODB.Recordset
Dim RS_Proveedor As Recordset
Dim Patente_Registrada As String
Dim CtaCliente As Boolean
Dim ElCliente As String
Dim ElRutCliente As String
Dim SaldoCtaCte As Double

Dim Uos As Double

Private Sub CboMarca_Click()
    CboModelo.Clear
    If CboMarca.ListIndex = -1 Then Exit Sub
        LLenarCombo CboModelo, "mod_nombre", "mod_id", "par_modelos", "mod_activo='SI' AND mar_id=" & CboMarca.ItemData(CboMarca.ListIndex)
End Sub

Private Sub CboMarca_KeyPress(KeyAscii As Integer)
  '  KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub CboMarca_Validate(Cancel As Boolean)
    Exit Sub
  If CboMarca.Locked = True Then Exit Sub
    
    Dim Archivo As String
    Dim F As Integer
    Dim MODELO As String
    
    If Len(Me.CboMarca.Text) = 0 Then Exit Sub
    Archivo = Ruta & "\modelos\" & Me.CboMarca.Text & ".txt"
    Me.CboModelo.Visible = False 'Lo hago invisible por
    If Existe(Archivo) > 0 Then
        F = FreeFile
        Me.CboModelo.Clear
        
        ' un problema neto de Visual Basic, en el redibujo
        'del control combobox (modelo)
        Open Archivo For Input As #F
            Do While Not EOF(F)
                Line Input #F, MODELO
                Me.CboModelo.AddItem MODELO
            Loop
        Close #F
        
        
        'el combobox pero redibujado correctamente con los datos nuevos
    Else
        CboModelo.Clear
    End If
    Me.CboModelo.Visible = True 'Vuelve a aparecer
End Sub

Private Sub CboModelo_KeyPress(KeyAscii As Integer)
KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub




Private Sub CboTipoDoc_Click()
    If CboTipoDoc.Text = "ORDEN DE INGRESO" Then TxtNDoc = Uos
End Sub


Private Sub CmdBuscaProducto_Click()
  '  BuscaProducto.AdoProducto.Recordset.Filter = 0
  '  Set BuscaProducto.MshProducto.DataSource = BuscaProducto.AdoProducto.Recordset
    RegSeleccionado = False
    DesdeCompra = True
    BuscaProducto.Show 1
    If RegSeleccionado = True Then
        Me.TxtPRecio.SetFocus
    End If
    
    
End Sub

Private Sub CmdBuscaProv_Click()
    AccionProveedor = 0
    BuscaProveedor.Show 1
    TxtRutProveedor = RutBuscado
    If TxtCodigo.Enabled Then If Len(TxtRutProveedor) > 0 Then TxtCodigo.SetFocus
    TxtRutProveedor_Validate True


  '  BuscaProveedor.AdoProveedor.Recordset.Filter = 0
  '  Set BuscaProveedor.MshProveedor.DataSource = BuscaProveedor.AdoProveedor.Recordset
  '  BuscaProveedor.Show 1
  '  Me.ValidaDocumentoCompra
  '  TxtNFacVenta = ""
End Sub



Private Sub CmdEliminar_Click()

End Sub

Private Sub CmdCancelAct_Click()
    Unload Me
End Sub

Private Sub CmdFocusCdgo_Click()
     TxtCodigo.SetFocus
End Sub

Private Sub CmdGuardar_Click()
    Dim RSConsignado As Recordset
    Dim SoloActInv As Boolean
    Dim Nro_Id_Unico As Long
    SoloActInv = False
    'Comenzar la validacion antes de guardar la compra
    If Val(Me.TxtNDoc.Text) = 0 Then
        MsgBox "Debe especificar N� de " & Me.CboTipoDoc.Text, vbExclamation
        Beep
        Me.TxtNDoc.SetFocus
        Exit Sub
    End If
    If Len(Me.TxtRutProveedor.Text) = 0 Or rutProveedor = False Then
        MsgBox "Debe especificar un Proveedor...", vbExclamation
        Beep
        Me.TxtRutProveedor.SetFocus
        Exit Sub
    End If
    If SuperTab.Tab = 0 Then
        'If Me.AdoTemporal.Recordset.RecordCount = 0 Then
        '    If Me.Check2.Value = 0 Then
        '        MsgBox "A�n no ha agregado productos...", vbExclamation
        '        Beep
        '        Me.TxtCodigo.SetFocus
        '        Exit Sub
       ''     Else
               ' If Len(TxOS) = 0 Or Val(Me.TxValorFacArriendo) = 0 Then
       '             MsgBox "Debe ingresar un N� de orden v�lido y un valor neto mayor que 0", vbOKOnly + vbInformation
       '             Exit Sub
       '         Else
       '             'grabamos la ol
       '         End If
       '     End If
       ' End If
    End If
    
    ClienteOproveedor = "Proveedor"
    
    If Me.CheckConsignacion.Value <> 1 Then 'si no es consignacion o parte de pago
                                          'no pasa por la forma de pago
                                         
        If SuperTab.Tab <> 0 Then FormaDePago.FramAct.Visible = True
        
        FormaDePago.Caption = "FORMA DE PAGO A PROVEEDOR"
        FormaDePago.CmdPago(0).Caption = "EFECTIVO"
        FormaDePago.Show 1
        
        If FormaDelPago = "Anular Operacion" Then Exit Sub
        
        If FormaDelPago = "Actualizar Inventario" Then SoloActInv = True
            
    Else
    
        If Op2.Value = True Then
            If CtaCliente = False Then
                MsgBox "Debe especificar un documento de venta EMITIDO ...", vbExclamation + vbOKOnly, "Para vehiculo recibido en parte pago"
                TxtNFacVenta.SetFocus
                Exit Sub
            End If
        End If
        
    End If
    
     txtTotalBruto = NumReal(txtTotalBruto)
    
    Dim Transaccion As String
    Dim Lfecha As String
    Lfecha = Format(DtFecha.Value, "yyyy-mm-dd")
        
        
        'Ahora validar el documento
    'Verificar si el tipo documento ha sido usado con este proveedor
    
         Sql = "SELECT rut,documento,no_documento,pago FROM compra_repuesto_documento WHERE " & _
        "rut = '" & Me.TxtRutProveedor & "' and Documento = '" & Me.CboTipoDoc.Text & "' and no_documento = " & Val(Me.TxtNDoc)
        paso = AbreConsulta(RS_ConsultaDoc, Sql)
    
    If RS_ConsultaDoc.RecordCount = 0 Or (EstadoCompra = 2 And RS_ConsultaDoc.RecordCount > 0) Then
       
          
        If EstadoCompra = 2 Then
           Me.CmdCancelAct.Visible = False
            'If Me.CheckConsignacion.Value <> 1 Then
                If RS_ConsultaDoc.RecordCount > 0 Then
                    
                    If RS_ConsultaDoc!PAGO = "CHEQUE(S)" Or RS_ConsultaDoc!PAGO = "CREDITO" Then
                        'ELIMINAR PAGOS Y CTAS CTES DEL DOCUMENTO
                        
                        If MsgBox("La forma de pago de este documento fue:" & vbNewLine & RS_ConsultaDoc!PAGO & vbNewLine & "los movimientos asociados a este document ser�n eliminados" & vbNewLine & "�Esta seguro de continuar?", vbQuestion + vbYesNo) = vbNo Then Exit Sub
                        
                        
                        '`'Sql = "SELECT * FROM CTACTE_Proveedores WHERE RUT = '" & Me.TxtRutProveedor.Text & "' AND TIPO_DOC = '" & Me.CboTipoDoc.Text & "' AND NO_DOC = " & Me.TxtNdoc
                        Sql = "DELETE FROM ctacte_proveedores WHERE rut = '" & Me.TxtRutProveedor.Text & "' AND tipo_doc = '" & Me.CboTipoDoc.Text & "' AND no_doc = " & Me.TxtNDoc
                        Call AbreConsulta(AdoCtaCte, Sql)
                        'If AdoCtaCte.RecordCount > 0 Then
                        '    With AdoCtaCte
                        '        CantRegistros = .RecordCount
                        '        For I = 1 To CantRegistros
                        '            .Delete
                        '            .MoveFirst
                        '        Next
                        '       ' .UPDATE
                        ''    End With
                        'End If
                        Dim rS_Pagos As Recordset
                        Sql = "SELECT * FROM pagos_proveedores WHERE rut = '" & Me.TxtRutProveedor.Text & "' AND tipo_doc = '" & Me.CboTipoDoc.Text & "' AND no_doc like '%" & Me.TxtNDoc & "%'"
                        paso = AbreConsulta(rS_Pagos, Sql)
                        If rS_Pagos.RecordCount > 0 Then
                            
                            With rS_Pagos
                                CantRegistros = .RecordCount
                                For I = 1 To CantRegistros
                                    If !FORMA_PAGO = "DE PAGO ANTICIPADO" Then
                                        Dim ActAnticipoProveedor As Recordset
                                        Sql = "UPDATE maestro_proveedores " & _
                                              "SET anticipo= anticipo+" & rS_Pagos!monto
                                        AbreConsulta RsTmp3, Sql
                                    End If
                                    .Delete
                                    .MoveFirst
                                Next
                             '   .UPDATE
                            End With
                        End If
                    End If 'fin consulta doc
                    
                    
                    If RS_ConsultaDoc!PAGO = "Vehiculo Rec. P.pago" Then
                        'si el vehiculo fue recibido en parte de pago
                        ' eliminar el abono de la cta cte del cliente
                        If MsgBox("La forma de pago de este documento fue:" & vbNewLine _
                        & RS_ConsultaDoc!PAGO & vbNewLine & "los movimientos en la Cta. Cte. asociados a " & vbNewLine & _
                        CboTipoDoc.Text & " N� " & TxtNDoc & " del cliente " & TxtRsocial & " ser�n eliminados" & vbNewLine & _
                        "�Esta seguro de continuar?", vbQuestion + vbYesNo) = vbNo Then Exit Sub
                        Sql = "Delete FROM ctacte_clientes WHERE tipo_doc = '" & Me.CboDocVenta.Text & "' AND no_doc = " & Me.TxtNFacVenta & " AND debe = 0"
                        paso = AbreConsulta(AdoCtaCte, Sql)
                        
                        Sql = "Delete FROM pagos_clientes WHERE tipo_doc = '" & Me.CboDocVenta.Text & "' and no_doc = " & Me.TxtNFacVenta
                        paso = AbreConsulta(AdoCtaCte, Sql)
                        
                        
                    
                    End If
                    
                    
                End If
              
        End If
        
        
        If SuperTab.Tab = 0 Then
            Transaccion = "COMPRA REPUESTO"
            'Si no se ha usado esta numeracion con este documento
            'en el proveedor entonces procedo a la grabacion
            Dim Unidades As Double
            Dim Codigo As String
            Dim Marcas As String
            Dim Descripcion As String
            Dim Precio As Double
            Dim SubTotal As Double
            Dim NuevoStock As Double
            
            
            If Me.Check2.Value = 0 Then 'Compra repuestos
            End If
        
        End If 'fin tab = 0
        
        '************************************************************
        '**  COMPRA     DE     VEHICULO      NUEVO
        '************************************************************
        If SuperTab.Tab = 1 Then 'vehiculos nuevos
            Transaccion = "VEHICULO NUEVO"
            If EstadoCompra = 2 Then
                Sql = "SELECT * FROM vehiculos WHERE proveedor = '" & Me.TxtRutProveedor & "' and tipo_documento = '" & Me.CboTipoDoc.Text & "' and no_documento = " & Me.TxtNDoc
            Else
                Sql = "SELECT * FROM vehiculos LIMIT 1 "
            End If
             paso = AbreConsulta(NuevoVehiculo, Sql)
             With NuevoVehiculo
                If EstadoCompra <> 2 Then
                    .AddNew
                End If
                 !patente = IIf(Len(Me.NTxtPatente) = 0, "-", Me.NTxtPatente)
                 !tipo = Me.NTxtTipo
                 !MARCA = Me.NcboMarca
                 !MODELO = Me.NcboModelo
                 !mar_id = NcboMarca.ItemData(NcboMarca.ListIndex) 'nuevo
                 !mod_id = NcboModelo.ItemData(NcboModelo.ListIndex) 'nuevo
                 !ano = Me.NcboAno
                 !Color = Me.NTxtColor
                 !puertas = Me.NTxtPuertas
                 !carga = Me.NTxtCarga
                 !peso = Me.NTxtPeso
                 !gasolina = Me.NTxtCombustibl
                 !chasis = Me.NTxtChasis
                 !SERIE = IIf(Len(Me.NTxtSerie) = 0, "-", Me.NTxtSerie)
                 !asientos = Me.NTxtAsientos
                 !MOTOR = Me.NTxtMotor
                 !nuevo = 1
                 !VIN = Me.TxtNVIN
                 !fecha_ingreso = Format(DtFecha.Value, "dd-mm-yyyy")
                 !estado = "EN STOCK"
                 !precio_compra = Val(Replace(Me.NTXTPrecioCompra, ".", ""))
                 !proveedor = Me.TxtRutProveedor
                 !nombre_proveedor = Me.TxtRsocial
                 !tipo_documento = Me.CboTipoDoc.Text
                 !No_Documento = Val(Me.TxtNDoc)
                 !precio_venta = 0 & Me.NtxtPventa
                 !ubicacion = "" & nTxtubicacion
                 If !no_doc_venta = 0 Then
                    !tipo_doc_venta = "-"
                
                 End If
                 .Update
             End With
             Me.SkTotalAfecto = Replace(Me.NTXTPrecioCompra, ".", "")
             Me.txtTotalBruto = Round(AgregarIVA(Val(Me.SkTotalAfecto), 19), 0)
             SKIVA = Val(Me.txtTotalBruto) - Val(Me.SkTotalAfecto)
             
             If NuevoVehiculo!no_doc_venta > 0 Then
                Sql = "UPDATE venta_vehiculos SET " & _
                     "precio_costo=" & Val(Replace(Me.NTXTPrecioCompra, ".", "")) & _
                     " WHERE no_documento=" & NuevoVehiculo!no_doc_venta & " AND tipo_documento='" & NuevoVehiculo!tipo_doc_venta & "'"
                 Call AbreConsulta(RsTmp, Sql)
             End If
             
        End If 'vehiculos nuevos
        
        If SuperTab.Tab = 2 Then 'vehiculos usados
            '************************************
            Transaccion = "VEHICULO USADO"
            '***************************************
            Me.SkTotalAfecto = Replace(Me.TxtPrecioCpra, ".", "")
             
             If Me.Check1.Value = 1 Then
                
                Me.txtTotalBruto = Round(AgregarIVA(Val(Me.SkTotalAfecto), 19), 0)
                SKIVA = Val(Me.txtTotalBruto) - Val(Me.SkTotalAfecto)
            
            Else
                SKIVA = 0
                Me.txtTotalBruto = Me.SkTotalAfecto
            End If
            
            If EstadoCompra = 2 Then '' actualizar
                    Dim ActVehiculo As Recordset
                    'If Me.CheckConsignacion.Value <> 1 Then
                    
                    'Sql = "UPDATE Vehiculos SET Patente = '" & Me.TxtPatente & "', tipo = '" & Me.TxtTipoVehiculo & "', marca  = '" & Me.CboMarca.Text & "', modelo = '" & CboModelo.Text & "', ano = '" & Me.cboano.Text & "', km_cta_kilometros = " & Me.TxtCtaKm & ", precio_compra = " & Me.TxtPrecioCpra & ",Fecha_Ingreso = '" & Format(DtFecha.Value, "dd-mm-yyyy") & "', consignacion = " & IIf(CheckConsignacion.Value = 1, 1, 0) & " WHERE Proveedor = '" & Me.TxtRutProveedor & "' and Tipo_Documento = '" & Me.CboTipoDoc.Text & "' and no_documento = " & Me.TxtNDoc
                    Sql = "SELECT * FROM vehiculos WHERE proveedor = '" & Me.TxtRutProveedor & "' and tipo_documento = '" & Me.CboTipoDoc.Text & "' and no_documento = " & Me.TxtNDoc
                    paso = AbreConsulta(ActVehiculo, Sql)
                    If ActVehiculo.RecordCount > 0 Then 'Actualiza Vehiculo Usado
                        With ActVehiculo
                         !patente = Me.txtPatente
                         !tipo = Me.TxtTipoVehiculo
                         !MARCA = Me.CboMarca.Text
                         !MODELO = Me.CboModelo
                         !mar_id = CboMarca.ItemData(CboMarca.ListIndex) 'nuevo
                         !mod_id = CboModelo.ItemData(CboModelo.ListIndex) 'nuevo
                         !ano = Me.cboano
                         !Color = TxtColor
                         !MOTOR = Txtmotor
                         !gasolina = Me.TxtCombus
                         !comentario = Me.TxtComentario
                         !km_cta_kilometros = Me.TxtCtaKm
                         !precio_compra = Me.TxtPrecioCpra
                         !nuevo = False
                         !fecha_ingreso = Format(DtFecha.Value, "dd-mm-yyyy")
                         !estado = "EN STOCK"
                         !proveedor = Me.TxtRutProveedor
                         !nombre_proveedor = Me.TxtRsocial
                         !tipo_documento = Me.CboTipoDoc.Text
                         !No_Documento = Me.TxtNDoc
                         !ubicacion = "" & tXTuBICACION
                         !transferido = Me.CboTransferido.Text
                         If Me.CheckConsignacion.Value = 1 Then
                            !CONSIGNACION = IIf(Op1.Value = True, 1, 0)
                            !partepago = IIf(Op2.Value = True, 1, 0)
                            
                         Else
                            !CONSIGNACION = 0
                            !partepago = 0
                            
                         End If
                         !precio_venta = Me.TxtPVenta
                         
                         .Update
                        End With
                        
                        'Verificar si el vehiculo era consignado,
                        'si era consignado eliminar de la tabla consignado
                        Sql = "Delete FROM consignados WHERE rut = '" & Me.TxtRutProveedor & "' and orden_ingreso = " & Me.TxtNDoc
                        paso = AbreConsulta(RSConsignado, Sql)
                    End If
                    
               ' Else
                    
                    'aqui si es consignado o en parte de pago
                    'If Op1.Value = True Then 'consignado
                    
               '      Sql = "UPDATE Vehiculos SET Patente = '" & Me.TxtPatente & "', tipo = '" & Me.TxtTipoVehiculo & "', marca  = '" & Me.CboMarca.Text & "', modelo = '" & CboModelo.Text & "', ano = '" & Me.cboano.Text & "', km_cta_kilometros = " & Me.TxtCtaKm & ",precio_compra = " & Me.TxtPrecioCpra & " , precio_venta = " & Me.TxtPVenta & ",Fecha_Ingreso = '" & Format(DtFecha.Value, "dd-mm-yyyy") & "', consignacion = " & IIf(Op1.Value = True, 1, 0) & ", partepago = " & IIf(Op2.Value = True, 1, 0) & " WHERE Proveedor = '" & Me.TxtRutProveedor & "' and Tipo_Documento = '" & Me.CboTipoDoc.Text & "' and no_documento = " & Me.TxtNDoc
                    'Else 'parte de pago
                     '   Sql = "UPDATE Vehiculos SET Patente = '" & Me.TxtPatente & "', tipo = '" & Me.TxtTipoVehiculo & "', marca  = '" & Me.CboMarca.Text & "', modelo = '" & CboModelo.Text & "', ano = '" & Me.cboano.Text & "', km_cta_kilometros = " & Me.TxtCtaKm & ", precio_venta = " & Me.TxtPVenta & ",Fecha_Ingreso = '" & Me.DtFecha.Value & "', consignacion = " & IIf(CheckConsignacion.Value = 1, 1, 0) & " WHERE Proveedor = '" & Me.TxtRutProveedor & "' and Tipo_Documento = '" & Me.CboTipoDoc.Text & "' and no_documento = " & Me.TxtNDoc
                    'End If
                    
              '  End If
                
            Else
                If Me.CheckConsignacion.Value <> 1 Then
                    
                    Sql = "select max(id_unico)+1 as nuevoid from vehiculos "
                          
                    paso = AbreConsulta(NuevoVehiculo, Sql)
                    
                    If NuevoVehiculo.RecordCount > 0 Then
                        Nro_Id_Unico = NuevoVehiculo!nuevoid
                    Else
                        Nro_Id_Unico = 1
                    End If
                    Sql = "SELECT * FROM vehiculos WHERE estado='EN STOCK'"
                    Call AbreConsulta(NuevoVehiculo, Sql)
                    
                    
                    With NuevoVehiculo
                        .AddNew
                        !patente = Me.txtPatente
                        !tipo = Me.TxtTipoVehiculo
                        !MARCA = Me.CboMarca.Text
                        !MODELO = Me.CboModelo
                        !mar_id = CboMarca.ItemData(CboMarca.ListIndex) 'nuevo
                        !mod_id = CboModelo.ItemData(CboModelo.ListIndex) 'nuevo
                        !ano = Me.cboano
                        !Color = TxtColor
                        !MOTOR = Txtmotor
                        !gasolina = Me.TxtCombus
                        !comentario = Me.TxtComentario
                        !km_cta_kilometros = Me.TxtCtaKm
                        !precio_compra = Me.TxtPrecioCpra
                        !nuevo = 0
                        !fecha_ingreso = Format(DtFecha.Value, "dd-mm-yyyy")
                        !estado = "EN STOCK"
                        !proveedor = Me.TxtRutProveedor
                        !nombre_proveedor = Me.TxtRsocial
                        !tipo_documento = Me.CboTipoDoc.Text
                        !No_Documento = Me.TxtNDoc
                        !No_Documento = Me.TxtNDoc
                        !ubicacion = "" & tXTuBICACION
                        !transferido = Me.CboTransferido.Text
                       ' !CONSIGNACION = IIf(CheckConsignacion.Value = 1, 1, 0)
                        !precio_venta = 0 & Me.TxtPVenta
                        !id_unico = Nro_Id_Unico
                        .Update
                    End With
                 '
                 '    Sql = "INSERT INTO Vehiculos (patente, tipo, marca, modelo,ano, km_cta_kilometros,precio_compra,nuevo,fecha_ingreso,estado,proveedor,nombre_proveedor,Tipo_Documento,No_documento,Consignacion) " & _
                 '   "VALUES ('" & Me.TxtPatente & "','" & Me.TxtTipoVehiculo & "','" & Me.CboMarca.Text & "','" & Me.CboModelo.Text & "','" & Me.cboano.Text & "','" & Me.TxtCtaKm & "'," & Val(Me.TxtPrecioCpra) & ",false,'" & Lfecha & "','EN STOCK','" & Me.TxtRutProveedor & "','" & Me.TxtRsocial & "','" & Me.CboTipoDoc.Text & "'," & Val(Me.TxtNDoc) & "," & IIf(CheckConsignacion.Value = 1, 1, 0) & ")"
                Else
                    Sql = "select max(id_unico)+1 as nuevoid from vehiculos "
                    Call AbreConsulta(NuevoVehiculo, Sql)
                    If NuevoVehiculo.RecordCount > 0 Then
                        Nro_Id_Unico = NuevoVehiculo!nuevoid
                    Else
                        Nro_Id_Unico = 1
                    End If
                    Sql = "SELECT * FROM vehiculos WHERE estado='NN'"
                    paso = AbreConsulta(NuevoVehiculo, Sql)
                    With NuevoVehiculo
                        .AddNew
                        !patente = Me.txtPatente
                        !tipo = Me.TxtTipoVehiculo
                        !MARCA = Me.CboMarca.Text
                        !MODELO = Me.CboModelo
                        !mar_id = CboMarca.ItemData(CboMarca.ListIndex) 'nuevo
                        !mod_id = CboModelo.ItemData(CboModelo.ListIndex) 'nuevo
                        !ano = Me.cboano
                        
                        !Color = TxtColor
                        !MOTOR = Txtmotor
                        !gasolina = Me.TxtCombus
                        !comentario = Me.TxtComentario
                        
                        !km_cta_kilometros = Me.TxtCtaKm
                        !precio_compra = Me.TxtPrecioCpra
                        !nuevo = 0
                        !fecha_ingreso = Format(DtFecha.Value, "dd-mm-yyyy")
                        !estado = "EN STOCK"
                        !proveedor = Me.TxtRutProveedor
                        !nombre_proveedor = Me.TxtRsocial
                        !tipo_documento = Me.CboTipoDoc.Text
                        !No_Documento = Me.TxtNDoc
                        !No_Documento = Me.TxtNDoc
                        !transferido = CboTransferido.Text
                        If Me.CheckConsignacion.Value = 1 Then
                            !CONSIGNACION = IIf(Op1.Value = True, 1, 0)
                            !partepago = IIf(Op2.Value = True, 1, 0)
                         Else
                            !CONSIGNACION = 0
                            !partepago = 0
                         End If
                         !id_unico = Nro_Id_Unico
                        !ubicacion = "" & tXTuBICACION
                        !precio_venta = 0 & Me.TxtPVenta
                        .Update
                    End With
                
                
                    
                End If
            End If
            
            If Me.CheckConsignacion.Value <> 1 Or (Me.CheckConsignacion.Value = 1 And Op2.Value = True) Then
                If EstadoCompra = 2 Then
                    '******************************************
                    'Actulizamos la patente en las demas tablas
                    '30 Septeimbre 2009
                    '******************************************
                    Sql = "UPDATE ot SET patente = '" & txtPatente & "' WHERE patente = '" & Patente_Registrada & "'"
                    Call AbreConsulta(NuevaCtaCte, Sql)
                    
                    Sql = "UPDATE cta_cte_vehiculos  SET  patente_chasis = '" & Me.txtPatente & "', fecha = '" & Format(DtFecha.Value, "dd-mm-yyyy") & "'  WHERE patente_chasis = '" & Patente_Registrada & "' AND transaccion <> 'INGRESO USADOS'"
                    Call AbreConsulta(NuevaCtaCte, Sql)
                    Sql = "UPDATE cta_cte_vehiculos  SET  patente_chasis = '" & Me.txtPatente & "', fecha = '" & Format(DtFecha.Value, "dd-mm-yyyy") & "', debe = " & Me.TxtPrecioCpra & " WHERE patente_chasis = '" & Patente_Registrada & "' AND transaccion = 'INGRESO USADOS'"
                    
                Else
                     Sql = "INSERT INTO cta_cte_vehiculos (patente_chasis,fecha, transaccion, nuevo,debe) " & _
                     "VALUES ('" & Me.txtPatente & "','" & Lfecha & "','INGRESO USADOS',FALSE," & Val(Me.TxtPrecioCpra) & ")"
                End If
                PAS0 = AbreConsulta(NuevaCtaCte, Sql)
            End If
         
            
              'Marcas y modelos
           '5 DICIEMBRE 2010
           
           
           
           
           ' Dim Archivo As String
           ' Dim F As Integer
           ' Archivo = Ruta & "\modelos\" & Me.CboMarca.Text & ".txt"
           ' F = FreeFile
           ' If Existe(Archivo) > 0 Then
           '     If CboModelo.ListIndex = -1 Then
           '         Open Archivo For Append As #F
           '             Print #F, Me.CboModelo.Text
           '         Close
           '     End If
           ' Else 'El archivo no existe por lo tanto lo crearemos
           '     Open Archivo For Output As #F
           '         Print #F, Me.CboModelo.Text
           '     Close #F
           ' End If
                
            ''''**** EMITE ORDEN ENTRADA
            'si corresponede
            If Me.CboTipoDoc.Text = "ORDEN DE INGRESO" Then
                If MsgBox("Desea imprimir Orden de Ingreso N� " & Me.TxtNDoc, vbYesNo + vbQuestion) = vbYes Then
                    Me.EmiteOE
                Else
                    'no emite orden de entrada
                End If
            End If
                
                
                
           ' MsgBox "Vehiculo patente " & Me.TxtPatente & " guardado correctamente...", vbInformation
            

            
        
                
        
        End If 'vehiculos usados
            
            
            
        Dim NuevoDocCompra As Recordset
        If Me.CheckConsignacion.Value = 1 Then
            If Op1.Value = True Then 'vehiculo en consignacion
                
                Sql = "SELECT * FROM consignados WHERE rut = '" & Me.TxtRutProveedor & "' and orden_ingreso = " & Me.TxtNDoc & " and patente = '" & Patente_Registrada & "'"
                paso = AbreConsulta(RSConsignado, Sql)
                With RSConsignado
                    If EstadoCompra <> 2 Then
                        .AddNew
                    Else
                        If .RecordCount = 0 Then .AddNew
                    End If
                    !Rut = Me.TxtRutProveedor
                    !Nombre = Me.TxtRsocial
                    !Orden_ingreso = Me.TxtNDoc
                    !FECHA = Format(DtFecha.Value, "dd-mm-yyyy")
                    !patente = Me.txtPatente
                    .Update
                End With
                Me.TxtPVenta = ""
                Me.CheckConsignacion.Value = 0
                Me.Check1.Enabled = True
                
                'crea un documento de compra pero cn valores "0"
                If EstadoCompra = 2 Then 'Actualiza
                    Sql = "SELECT * FROM compra_repuesto_documento WHERE no_documento = " & Me.TxtNDoc & " and documento = '" & Me.CboTipoDoc.Text & "'"
                    paso = AbreConsulta(NuevoDocCompra, Sql)
                    With NuevoDocCompra
                        !clasificacion = IIf(Option1.Value = True, 1, 2)
                        !FECHA = Me.DtFecha.Value
                        !PAGO = "Vehiculo en Consignacion"
                        .Update
                    End With
                
                
                Else 'Crea nuevo registro
                    
                    Sql = "INSERT INTO compra_repuesto_documento " & _
                    "(rut, nombre_proveedor,documento,no_documento,pago,neto,iva,total,fecha, tipo_movimiento,clasificacion) " & _
                    "VALUES ('" & Me.TxtRutProveedor.Text & "','" & Me.TxtRsocial.Text & "','" & Me.CboTipoDoc.Text & _
                    "'," & Val(Me.TxtNDoc) & ",'Vehiculo en Consignacion'," & 0 & "," & 0 & "," & 0 & ",'" & Lfecha & "','" & Transaccion & "'," & IIf(Option1.Value = True, 1, 2) & ")"
                    paso = AbreConsulta(NuevoDocCompra, Sql)
                    
                End If
                
            
            Else 'vehiculo en parte de pago
                Dim RsAbonoEnAuto As Recordset
                paso = AbreConsulta(RsAbonoEnAuto, "SELECT * FROM Pagos_Clientes")
                
                With RsAbonoEnAuto
                    .AddNew
                
                    !Rut = Me.TxtRutProveedor
                    !Nombre = Me.TxtRsocial
                    !tipo_doc = Me.CboDocVenta.Text
                    !no_doc = Me.TxtNFacVenta
                    !FECHA_PAGO = Format(DtFecha.Value, "dd-mm-yyyy")
                    !FORMA_PAGO = "VEHICULO EN PARTE PAGO"
                    If TxtPrecioCpra > SaldoCtaCte Then
                        !monto = SaldoCtaCte
                    Else
                        !monto = NumReal(Me.TxtPrecioCpra)
                    End If
                    .Update
                End With
                
                
                'SI EL VALOR DEL VEHICULO RECIBIO ES MAYOR QUE EL SALDO, LA DIFERENCIA SE DEBE SUMAR AL PAGO ANTICIPADO
                Dim NUEVOPAGO As Recordset
                If NumReal(Me.TxtPrecioCpra) > SaldoCtaCte Then
                    MONTOHABER = SaldoCtaCte
                    anticipo = NumReal(Me.TxtPrecioCpra) - SaldoCtaCte
                    Sql = "UPDATE maestro_clientes SET anticipo = anticipo+" & anticipo & " WHERE rut_cliente = '" & Me.TxtRutProveedor & "'"
                    cn.Execute Sql
                Else
                    MONTOHABER = NumReal(Me.TxtPrecioCpra)
                End If
                
                    Sql = "INSERT INTO ctacte_clientes (nombre,rut,fecha,tipo_doc,no_doc,haber) " & _
                                   "VALUES ('" & ElCliente & "','" & Me.TxtRutProveedor & "','" & Format(DtFecha.Value, "dd-mm-yyyy") & "','" & Me.CboDocVenta.Text & "'," & Me.TxtNFacVenta & "," & MONTOHABER & ")"
                paso = AbreConsulta(NUEVOPAGO, Sql)
            
                If EstadoCompra = 2 Then 'Actualiza
                    Sql = "SELECT * FROM compra_repuesto_documento WHERE no_documento = " & Me.TxtNDoc & " and documento = '" & Me.CboTipoDoc.Text & "'"
                    paso = AbreConsulta(NuevoDocCompra, Sql)
                    With NuevoDocCompra
                        !FECHA = Me.DtFecha.Value
                        !PAGO = "Vehiculo Rec. P.pago"
                        !partepagodoc = Me.CboDocVenta.Text
                        !partepagondoc = Me.TxtNFacVenta
                        !Neto = Me.TxtPrecioCpra
                        !total = Me.TxtPrecioCpra
                        
                        .Update
                    End With
                
                
                Else 'Crea nuevo registro de compra de vehiculo (Rec. parte pago)
                    
                    Sql = "INSERT INTO compra_repuesto_documento " & _
                    "(rut, nombre_proveedor,documento,no_documento,pago,neto,iva,total,fecha, tipo_movimiento,partepagodoc,partepagondoc) " & _
                    "VALUES ('" & Me.TxtRutProveedor.Text & "','" & Me.TxtRsocial.Text & "','" & Me.CboTipoDoc.Text & _
                    "'," & Val(Me.TxtNDoc) & ",'Vehiculo Rec. P.pago'," & Me.TxtPrecioCpra & "," & 0 & "," & Me.TxtPrecioCpra & ",'" & Lfecha & "','" & Transaccion & "','" & Me.CboDocVenta.Text & "'," & Me.TxtNFacVenta & ")"
                    paso = AbreConsulta(NuevoDocCompra, Sql)
                    
                End If
            
            
            
            
            
            
            
            
            End If
            
            
        Else
            'el vehiculo no es consignado ni en parte de pago
           
           If EstadoCompra = 2 Then
                If RS_ConsultaDoc.RecordCount > 0 Then
                    RS_ConsultaDoc.Delete
                    RS_ConsultaDoc.Update
                End If
           End If
           
           
           ''''
           '' aqui se graba la compra
           If Val(TxOS) = 0 Then TxOS = 0
           If SoloActInv = False Then
                mexento = 0
                mafecto = 0
                If Val(SKIVA) = 0 Then
                    mexento = txtTotalBruto
                Else
                    mafecto = SkTotalAfecto
                End If
                
                Sql = "INSERT INTO compra_repuesto_documento " & _
                "(rut, nombre_proveedor,documento,no_documento,pago,neto," & _
                "iva,total,fecha, tipo_movimiento,clasificacion,os_arriendo," & _
                "mes_contable,ano_contable,inventario,doc_id,com_exe_otros_imp) " & _
                "VALUES ('" & Me.TxtRutProveedor.Text & "','" & Me.TxtRsocial.Text & "','" & Me.CboTipoDoc.Text & _
                "'," & Val(Me.TxtNDoc) & ",'" & FormaDelPago & "'," & mafecto & "," & _
                Me.SKIVA.Caption & "," & Me.txtTotalBruto.Text & ",'" & Lfecha & "','" & _
                Transaccion & "'," & IIf(Option1.Value = True, 1, 2) & "," & TxOS & "," & _
                Month(DtFecha.Value) & "," & Year(DtFecha.Value) & ",'SUMA'," & CboTipoDoc.ItemData(CboTipoDoc.ListIndex) & "," & mexento & ")"
            Else
                
                Sql = "INSERT INTO compra_repuesto_documento " & _
                "(rut, nombre_proveedor,documento,no_documento,pago,neto,iva,total,fecha, tipo_movimiento,clasificacion,os_arriendo) " & _
                "VALUES ('" & Me.TxtRutProveedor.Text & "','" & Me.TxtRsocial.Text & "','" & Me.CboTipoDoc.Text & _
                "'," & Val(Me.TxtNDoc) & ",'" & FormaDelPago & "'," & 0 & "," & 0 & "," & 0 & ",'" & Lfecha & "','" & Transaccion & "'," & IIf(Option1.Value = True, 1, 2) & "," & TxOS & ")"
            End If
            AbreConsulta NuevoDocCompra, Sql
            
            If FormaDelPago = "CREDITO" Then
                
                 ' abre el recordset
                Sql = "SELECT * FROM ctacte_proveedores WHERE rut='" & txtRut & "'"
                paso = AbreConsulta(AdoCtaCte, Sql)
                                
                With AdoCtaCte
                    .Filter = 0
                    Dim SaldoActual As Double
                    SaldoActual = 0
                    
                    If .RecordCount > 0 Then
                        .MoveLast
                        SaldoActual = !saldo
                    End If
                    
                    .AddNew
                  
                    !Rut = Me.TxtRutProveedor.Text
                    !Nombre = Me.TxtRsocial.Text
                    !FECHA = Format(DtFecha.Value, "dd-mm-yyyy")
                    !tipo_doc = Me.CboTipoDoc.Text
                    !no_doc = Me.TxtNDoc.Text
                    !HABER = Me.txtTotalBruto.Text
                    !saldo = Val(SaldoActual) + Val(NumReal(Me.txtTotalBruto.Text))
                    .Update
                    .close
                End With
            End If
            
         End If 'fin tipo ingreso
              
            MsgBox "Documento grabado correctamente..."
            BorraTemporal
            LimpiaCajas
            Me.TxtNDoc.Text = ""
            'Me.DtFecha.SetFocus
            Limpia
            NLimpia
    Else
        'El n�  y doc ya ha sido utilizado antes con este proveedor
        MsgBox "El Documento de Ingreso con su respectivo N� ya ha sido utilizado...", vbExclamation
        Beep
        Me.CboTipoDoc.SetFocus
       ' Me.AdoCompras.Recordset.Filter = 0
    
    End If
End Sub



Private Sub CmdHistorico_Click()
    HistoricoCompras.Show 1
End Sub



Private Sub CmdMM_Click(Index As Integer)
    veh_Marcas_Modelos.Show 1
End Sub

Private Sub CmdModificar_Click()
    Frame1.Visible = False
    EstadoCompra = 2
    If SuperTab.Tab = 0 Then
        Me.GridRepuestos.Columns(6).Visible = False
        SuperTab.Enabled = True
        SuperTab.TabEnabled(0) = True
        SuperTab.TabEnabled(1) = False
        SuperTab.TabEnabled(2) = False
        Me.CmdGuardar.Enabled = True
       
       ' Me.CmdCancelAct.Enabled = False
    ElseIf SuperTab.Tab = 1 Then
        
    
    
    
        SuperTab.Enabled = True
        SuperTab.TabEnabled(0) = False
        SuperTab.TabEnabled(1) = True
        SuperTab.TabEnabled(2) = False
        Me.CmdGuardar.Enabled = True
    ElseIf SuperTab.Tab = 2 Then
        txtPatente.Enabled = True
        Patente_Registrada = Me.txtPatente
        
        SuperTab.Enabled = True
        SuperTab.TabEnabled(0) = False
        SuperTab.TabEnabled(1) = False
        SuperTab.TabEnabled(2) = True
        Me.CmdGuardar.Enabled = True
    End If
    Me.CmdCancelAct.Visible = True
     Me.CmdModificar.Enabled = False
End Sub

Private Sub CmdNuevoProveedor_Click()
    rutProveedor = False
    AgregoProveedor.PicNew.Visible = True
    AgregoProveedor.txtRut.Text = ""
    AgregoProveedor.Show 1
    
    If rutProveedor Then
        Me.TxtCodigo.SetFocus
    End If
   
        
End Sub

Private Sub CmdSalir_Click()
    Me.Timer1.Enabled = False
    
    Unload Me
End Sub


Private Sub Command1_Click()
  '  MsgBox Timer1.Enabled
End Sub

Private Sub Check1_Click()
    If Me.Check1.Value = 1 Then
        If Val(Me.TxtPrecioCpra) > 0 Then
            Me.TxtABruto = Round(AgregarIVA(Replace(Me.TxtPrecioCpra, ".", ""), 19), 0)
            Me.TxtAIVA = Val(Me.TxtABruto) - Val(Replace(Me.TxtPrecioCpra, ".", ""))
            Me.txtTotalBruto = Me.TxtABruto
            
        End If
    Else
        Me.txtTotalBruto = Me.SkTotalAfecto
        Me.TxtABruto = ""
        Me.TxtAIVA = ""
    End If
End Sub

Private Sub Check2_Click()
    If Check2.Value = 1 Then
        SkinLabel13.Visible = True
        Me.TxOS.Visible = True
        Me.TxValorFacArriendo.Visible = True
        Frame3.Visible = False
        Frame5.Visible = False
        SkTotalAfecto = Me.TxValorFacArriendo
        CalculaFac
        
    Else
        SkinLabel13.Visible = False
        Me.TxOS.Visible = False
        Me.TxValorFacArriendo.Visible = False
        Frame3.Visible = True
        Frame5.Visible = True
        SumaElGrid
    End If
End Sub

Private Sub CheckConsignacion_Click()
    If Me.CheckConsignacion.Value = 1 Then
        'Me.TxtPVenta.Locked = False
        'Me.TxtPVenta.SetFocus
        Me.TxtPrecioCpra = 0
        Check1.Enabled = False
        Op1.Enabled = True
        Op2.Enabled = True
        If Op1.Value = True Then Me.TxtPrecioCpra.Visible = False
        If Op2.Value = True Then Me.TxtPrecioCpra.Visible = True
    Else
        Check1.Enabled = True
        'Me.TxtPVenta.Locked = True
        Op1.Enabled = False
        Op2.Enabled = False
        'Me.TxtPrecioCpra.SetFocus
        Me.SkFventa.Visible = False
        Me.TxtNFacVenta.Visible = False
        Me.SkDventa.Visible = False
        Me.CboDocVenta.Visible = False
        Me.TxtPrecioCpra.Visible = True
    End If
        
End Sub

Private Sub Form_Load()
    Me.CboTransferido.ListIndex = 0
    SuperTab.TabEnabled(0) = False
    SuperTab.TabEnabled(1) = False
    SuperTab.TabEnabled(2) = False

    Aplicar_skin Me
    
    LLenarCombo CboTipoDoc, "doc_nombre", "doc_id", "sis_documentos", "doc_movimiento='ENTRADA' and doc_activo='SI' AND doc_venta_vehiculos='SI'"
    Me.CboTipoDoc.ListIndex = 0
    Me.DtFecha.Value = Date
    BorraTemporal
    CargaMarcas
    For I = Year(Date) + 1 To 1980 Step -1
        Me.cboano.AddItem I 'Cargamos a�os, hasta el actual  + 1
        Me.NcboAno.AddItem I 'Cargamos a�os, hasta el actual  + 1
    Next
    CtaCliente = False
    Me.CboDocVenta.ListIndex = 0
    EstadoCompra = 1
    Timer1.Enabled = True
    
    With RSUsuarios
        If !INGRESA_FACTURAS = True Or !TODO = True Then
            SuperTab.TabEnabled(0) = True
        End If
        If !INGRESO_VEHICULOS = True Or !TODO = True Then
            SuperTab.Tab = 1
            SuperTab.TabEnabled(1) = True
            SuperTab.TabEnabled(2) = True
        End If
        
        
    End With
    Timer1.Enabled = True
    

    Sql = "SELECT Max(no_documento) FROM compra_repuesto_documento WHERE documento = 'ORDEN DE INGRESO'"
    paso = AbreConsulta(RS_ConsultaDoc, Sql)
    If IsNull(RS_ConsultaDoc.Fields(0)) Then Uos = 1 Else Uos = RS_ConsultaDoc.Fields(0) + 1
    SuperTab.TabVisible(0) = False
End Sub







Private Sub Form_Unload(Cancel As Integer)
    Timer1.Enabled = False
    Unload Me
End Sub




Private Sub GridRepuestos_AfterUPDATE()
        NoModXcien = False
    NoModPesos = False

    SumaElGrid 'suma la grilla cada vez que agrego o elimino un registro
    
        NoModXcien = True
    NoModPesos = True
End Sub







Private Sub NcboMarca_Click()
    NcboModelo.Clear
    If NcboMarca.ListIndex = -1 Then Exit Sub
    LLenarCombo NcboModelo, "mod_nombre", "mod_id", "par_modelos", "mod_activo='SI' AND mar_id=" & NcboMarca.ItemData(NcboMarca.ListIndex)

End Sub

Private Sub NcboMarca_KeyPress(KeyAscii As Integer)
'KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub NcboMarca_Validate(Cancel As Boolean)
    Exit Sub
  Dim Archivo As String
    Dim F As Integer
    Dim MODELO As String
    
    If Len(Me.NcboMarca.Text) = 0 Then Exit Sub
    Archivo = Ruta & "\modelos\" & Me.NcboMarca.Text & ".txt"
    Me.NcboModelo.Visible = False 'Lo hago invisible por
    If Existe(Archivo) > 0 Then
        F = FreeFile
        Me.NcboModelo.Clear
        
        ' un problema neto de Visual Basic, en el redibujo
        'del control combobox (modelo)
        Open Archivo For Input As #F
            Do While Not EOF(F)
                Line Input #F, MODELO
                Me.NcboModelo.AddItem MODELO
            Loop
        Close #F
        
        
        'el combobox pero redibujado correctamente con los datos nuevos
    Else
        NcboModelo.Clear
    End If
    Me.NcboModelo.Visible = True 'Vuelve a aparecer
End Sub


Private Sub NcboModelo_KeyPress(KeyAscii As Integer)
KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub


Private Sub NTxtChasis_LostFocus()
    Dim RsCompChasis As Recordset
    Sql = "SELECT chasis,estado FROM vehiculos WHERE chasis = '" & NTxtChasis & "' and (estado = 'EN STOCK')"
    paso = AbreConsulta(RsCompChasis, Sql)
    If RsCompChasis.RecordCount > 0 Then
        MsgBox "Este Chasis se encuentra en el stock"
        NTxtChasis.Text = ""
        NTxtChasis.SetFocus
    End If
End Sub

Private Sub NTXTPrecioCompra_Change()
    Me.SkTotalAfecto.Caption = Val(Replace(Me.NTXTPrecioCompra.Text, ".", ""))
    Me.txtTotalBruto = Round(AgregarIVA(Val(Me.SkTotalAfecto.Caption), 19))
    'Me.SkTotal = Me.NTXTPrecioCompra
End Sub

Private Sub NtxtPventa_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
End Sub



Private Sub NTxtUbicacion_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub Op1_Click()
    Me.SkFventa.Visible = False
    Me.TxtNFacVenta.Visible = False
    Me.SkDventa.Visible = False
    Me.CboDocVenta.Visible = False
    Me.TxtPrecioCpra = 0
    Me.TxtPrecioCpra.Visible = False
    
End Sub

Private Sub Op2_Click()
    Me.SkFventa.Visible = True
    Me.TxtNFacVenta.Visible = True
    Me.SkDventa.Visible = True
    Me.CboDocVenta.Visible = True
    Me.TxtPrecioCpra.Visible = True
End Sub




Private Sub Timer1_Timer()
    
    For Each txtS In Controls
        If (TypeOf txtS Is TextBox) Then
            If Me.ActiveControl.Name = txtS.Name Then 'Foco activo
                txtS.BackColor = IIf(txtS.Locked, ClrDesha, ClrCfoco)
            Else
                txtS.BackColor = IIf(txtS.Locked, ClrDesha, ClrSfoco)
            End If
        End If
    Next
    
    
     
    If SuperTab.Tab = 1 Then
      '  FrameTotales.Visible = False
        If Len(Me.TxtNVIN) > 0 And _
            Len(Me.NTxtAsientos) > 0 And _
            Len(Me.NTxtCarga) > 0 And _
            Len(Me.NcboAno) > 0 And _
            Len(Me.NcboMarca) > 0 And _
            Len(Me.NcboModelo) > 0 And _
            Len(Me.NTxtChasis) > 0 And _
            Len(Me.NTxtPeso) > 0 And _
            Len(Me.NTxtColor) > 0 And _
            Len(Me.NTxtMotor) > 0 And _
            Len(Me.NTxtPuertas) > 0 And _
            Len(Me.NTxtCombustibl) > 0 And _
            Len(Me.NTxtTipo) > 0 And _
            Val(Me.NTXTPrecioCompra) > 0 Then
            If Me.CmdModificar.Enabled = True Then
                Me.CmdGuardar.Enabled = False
            Else
                Me.CmdGuardar.Enabled = True
            End If
                
            'Me.CmdGuardar.Enabled = True
        Else
        
            Me.CmdGuardar.Enabled = False
            
        End If
    
    ElseIf SuperTab.Tab = 2 Then
     '   FrameTotales.Visible = False
        If Len(Trim(Me.txtPatente)) > 0 And _
            Len(Me.TxtCtaKm) > 0 And _
            Val(Me.TxtPrecioCpra) > 0 And _
            Len(Me.CboMarca.Text) > 0 And _
            Len(Me.CboModelo.Text) > 0 And _
            Len(Me.TxtTipoVehiculo) > 0 And _
            Me.cboano.ListIndex > -1 Then
            
            If Me.CmdModificar.Enabled = True Then
                Me.CmdGuardar.Enabled = False
            Else
                Me.CmdGuardar.Enabled = True
            End If
            
           
                
               ' Me.CmdGuardar.Enabled = True
        Else
             If Len(Me.txtPatente) > 0 And _
                    Len(Me.TxtCtaKm) > 0 And _
                    Len(Me.CboMarca.Text) > 0 And _
                    Len(Me.CboModelo.Text) > 0 And _
                    Len(Me.TxtTipoVehiculo) > 0 And _
                    Me.cboano.ListIndex > -1 And _
                    Me.CheckConsignacion.Value = 1 And Val(Me.TxtPVenta) > 0 Then
                        
                    Me.CmdGuardar.Enabled = True
            Else
                    Me.CmdGuardar.Enabled = False
            End If
                
           ' If Me.CmdModificar.Enabled = True Then
           '     Me.CmdGuardar.Enabled = False
           ' Else
           '     Me.CmdGuardar.Enabled = True
           ' End If
        
        
                
        
        
            
        End If
    ElseIf SuperTab.Tab = 0 Then
        
          
        If Me.CmdModificar.Enabled = True Then
            Me.CmdGuardar.Enabled = False
        Else
            Me.CmdGuardar.Enabled = True
        End If
        
        FrameTotales.Visible = True
        
    End If
    
    
    
    
End Sub

Private Sub TxOS_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
End Sub

Private Sub TxOS_Validate(Cancel As Boolean)

    If Val(TxOS) = 0 Then Exit Sub

    Dim RsConsultaDV As Recordset
    Sql = "SELECT * FROM doc_venta WHERE tipo_doc = 'ORDEN DE SALIDA' AND no_documento = " & TxOS
    paso = AbreConsulta(RsConsultaDV, Sql)
    
    If RsConsultaDV.RecordCount = 0 Then
        MsgBox "ORDEN DE SALIDA NO ENCONTRADO...", vbInformation + vbOKOnly
        TxOS = ""
        Exit Sub
       
    Else
        MsgBox "ORDEN DE SALIDA N� " & TxOS & " corresponde a la patente " & RsConsultaDV!patente
    End If
    
    
End Sub

Private Sub TxtCodigo_GotFocus()
    Me.TxtCodigo.BackColor = ClrCfoco
    Me.Timer1.Enabled = True
End Sub

Private Sub TxtCodigo_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 And Len(TxtCodigo.Text) > 0 Then SendKeys "{Tab}"
        
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub TxtCodigo_LostFocus()
    Me.TxtCodigo.BackColor = ClrSfoco
End Sub

Private Sub TxtCodigo_Validate(Cancel As Boolean)
    If Len(TxtCodigo.Text) = 0 Then Exit Sub
    Sql = "SELECT descripcion,marca,precio_compra,stock_actual " & _
          "FROM maestro_productos " & _
          "WHERE codigo='" & TxtCodigo & "'"
    Call AbreConsulta(RsTmp, Sql)
    If RsTmp.RecordCount > 0 Then
                'Codigo ingresado ha sido encontrado
            With RsTmp
                Me.txtDescripcion.Text = !Descripcion
                Me.TxtMarca.Text = !MARCA
                Me.TxtPRecio.Text = !precio_compra
                Me.LbStockActual.Caption = !stock_actual
                Me.TxtStockActual.Text = !stock_actual
                Me.TxtPRecio.SetFocus
            End With
    Else
            Respuesta = MsgBox("Codigo no encontrado..." & Chr(13) & "�Crear nuevo producto?", vbYesNo + vbQuestion)
            If Respuesta = 6 Then
                'Crear nuevo
                AccionProducto = 5
                AgregarProducto.Caption = "Nuevo Producto"
                AgregarProducto.TxtCodigo.Text = Me.TxtCodigo.Text
                AgregarProducto.Show 1
            Else
                TxtCodigo.Text = ""
                Me.txtDescripcion.Text = ""
                Me.TxtMarca.Text = ""
                Me.TxtPRecio.Text = ""
                Me.TxtUnidades.Text = ""
                Me.TxtCodigo.SetFocus
            End If
    End If
    
End Sub







Private Sub TxtColor_KeyPress(KeyAscii As Integer)
KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub


Private Sub TxtCombus_KeyPress(KeyAscii As Integer)
  KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub


Private Sub TxtComentario_KeyPress(KeyAscii As Integer)
  KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub


Private Sub TxtCtaKm_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
End Sub

Private Sub TxtDescuento_Change()
    If Val(Me.TxtDescuento.Text) > 99 Then
        MsgBox "El descuento no puede superar el 99%"
        Me.TxtDescuento.Text = ""
        Exit Sub
    End If
    If NoModXcien = False Then Exit Sub
    VdDesc = 1
    Me.SumaElGrid
End Sub


Private Sub TxtDescuento_KeyPress(KeyAscii As Integer)
 KeyAscii = AceptaSoloNumeros(KeyAscii)
End Sub

Private Sub TxtDescuentoPesos_Change()
    If NoModPesos = False Then Exit Sub
    VdDesc = 2
    Me.SumaElGrid
End Sub

Private Sub TxtDescuentoPesos_KeyPress(KeyAscii As Integer)
    KeyAscii = AceptaSoloNumeros(KeyAscii)
End Sub

Private Sub TxtExento_Change()
    txtTotalBruto = CDbl(SKIVA) + CDbl(SkTotalAfecto) + CDbl(TxtExento)
End Sub

Private Sub TxtExento_KeyPress(KeyAscii As Integer)
     KeyAscii = AceptaSoloNumeros(KeyAscii)
End Sub

Private Sub Txtmotor_KeyPress(KeyAscii As Integer)
 KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub


Private Sub TxtNDoc_GotFocus()
    En_Foco TxtPRecio
End Sub

Private Sub TxtNdoc_KeyPress(KeyAscii As Integer)
    KeyAscii = AceptaSoloNumeros(KeyAscii)
    If KeyAscii = 46 Then KeyAscii = 0
End Sub




Private Sub TxtNDoc_LostFocus()
    If Len(Me.TxtNDoc.Text) = 0 Then
        MsgBox "Debe especificar N� de Documento", vbOKOnly + vbInformation
        TxtNDoc.SetFocus
    End If
End Sub

Private Sub TxtNDoc_Validate(Cancel As Boolean)
    Me.ValidaDocumentoCompra
    SaldoCtaCte = 0
End Sub

Private Sub TxtNFacVenta_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
End Sub


Private Sub TxtNFacVenta_Validate(Cancel As Boolean)
    'verificar si existe una cta corriente asociada
    'entre el documento y numero de venta
    
    If Val(Me.TxtNFacVenta) = 0 Then
        MsgBox "Debe especificar un N� de " & Me.CboDocVenta.Text
           
        Exit Sub
    End If
    
    Dim RsCtaCliente As Recordset
    
    Sql = "SELECT DISTINCTROW ctacte_clientes.rut, ctacte_clientes.nombre, ctacte_clientes.tipo_doc, ctacte_clientes.no_doc, Sum(ctacte_clientes.debe) AS DEBE_, Sum(ctacte_clientes.haber) AS HABER_, sum(ctacte_clientes.debe)-  SUM(ctacte_clientes.haber  ) as SALDO_, pagar " & _
            "FROM ctacte_clientes WHERE tipo_doc = '" & Me.CboDocVenta.Text & "' and no_doc = " & Me.TxtNFacVenta & _
           " GROUP BY ctacte_clientes.rut, ctacte_clientes.nombre, ctacte_clientes.tipo_doc, ctacte_clientes.no_doc, pagar"
           
    paso = AbreConsulta(RsCtaCliente, Sql)
    
    CtaCliente = False
    
    With RsCtaCliente
        If .RecordCount > 0 Then
            
            If !saldo_ > 0 Then
                SaldoCtaCte = 0
                MsgBox "Documento tiene un saldo de $ " & NumFormat(!saldo_)
                SaldoCtaCte = !saldo_
                ElCliente = !Nombre
                ElRutCliente = !Rut
                
                If ElRutCliente <> Me.TxtRutProveedor Then
                    If MsgBox("RUT no corresponde al ingresado en " & !tipo_doc & " N� " & !no_doc & vbNewLine & "�Cambiar RUT del documento de ingreso?", vbYesNo + vbQuestion, "Rut no coincide") = vbYes Then
                        Dim RsCliente As Recordset
                        Dim RsProveedor As Recordset
                        Sql = "SELECT * FROM maestro_clientes WHERE rut_cliente = '" & ElRutCliente & "'"
                        paso = AbreConsulta(RsCliente, Sql)
                        With RsCliente
                            Sql = "SELECT rut_proveedor FROM maestro_proveedores WHERE rut_proveedor = '" & !rut_cliente & "'"
                            paso = AbreConsulta(RsProveedor, Sql)
                            
                            If RsProveedor.RecordCount = 0 Then
                                Sql = "INSERT INTO maestro_proveedores (rut_proveedor,nombre_empresa,direccion,comuna,ciudad,fono,email) values('" & !rut_cliente & "','" & !nombre_rsocial & "','" & !DIRECCION & "','" & !ciudad & "','" & !comuna & "','" & !fono & "','" & !email & "')"
                                paso = AbreConsulta(RsProveedor, Sql)
                            End If
                            Me.TxtRsocial = !nombre_rsocial
                            Me.TxtRutProveedor = !rut_cliente
                        End With
                        rutProveedor = True
                    Else
                    
                        TxtNFacVenta.Text = ""
                        
                    End If
                Else
                    CtaCliente = True
                End If
            Else
                MsgBox "El documento de venta ingresado no registra saldo" & vbNewLine & "No puede ser usado para abonar un vehiculo en parte de pago."
            End If
        Else
                MsgBox "El documento de venta ingresado no registra saldo" & vbNewLine & "No puede ser usado para abonar un vehiculo en parte de pago."
        End If
    End With
    
    
    
End Sub


Private Sub txtPatente_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub TxtPatente_LostFocus()
    Dim RsCompPatente As Recordset
    Sql = "SELECT patente,estado FROM vehiculos WHERE patente = '" & Me.txtPatente & "' and (estado = 'EN STOCK' or estado = 'ARRENDADO')"
    paso = AbreConsulta(RsCompPatente, Sql)
    If RsCompPatente.RecordCount > 0 Then
        MsgBox "Esta patente se encuentra en el stock"
        txtPatente.Text = ""
        txtPatente.SetFocus
    End If
End Sub

Private Sub TxtPRecio_Change()
    Me.TxtSubStotal.Text = Round(Val(Me.TxtPRecio.Text) * Val(Me.TxtUnidades.Text), 0)
End Sub


Private Sub TxtPrecio_KeyPress(KeyAscii As Integer)
    KeyAscii = AceptaSoloNumeros(KeyAscii)
End Sub



Private Sub TxtPrecioCpra_Change()
    Me.SkTotalAfecto = Replace(Me.TxtPrecioCpra, ".", "")
    If Me.Check1.Value = 1 Then
        Me.txtTotalBruto = Round(AgregarIVA(Val(Me.SkTotalAfecto.Caption), 19))
    Else
        Me.txtTotalBruto = Me.SkTotalAfecto
    End If
    Check1_Click
End Sub

Private Sub TxtPrecioCpra_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
End Sub

Private Sub TxtPVenta_KeyPress(KeyAscii As Integer)
    KeyPress = SoloNumeros(KeyAscii)
End Sub

Private Sub TxtRutProveedor_KeyPress(KeyAscii As Integer)
KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub





Private Sub TxtRutProveedor_Validate(Cancel As Boolean)
    If Len(TxtRutProveedor) > 0 Then
        Sql = "SELECT nombre_empresa " & _
              "FROM maestro_proveedores " & _
              "WHERE rut_proveedor='" & TxtRutProveedor & "' "
        AbreConsulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
            TxtRsocial = RsTmp!nombre_empresa
        Else
            MsgBox "PROVEEDOR NO ENCONTRADO... ", vbOKOnly + vbInformation
            TxtRsocial = ""
            
            Exit Sub
        End If
    End If
   ' ComprobarSiDocumento DG_ID_Unico
End Sub



Private Sub TxtTipoVehiculo_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub txtTotalBruto_Change()
    Me.SKIVA.Caption = Val(Me.txtTotalBruto) - Val(Replace(Me.SkTotalAfecto.Caption, ".", ""))
End Sub

Private Sub txtTotalBruto_KeyPress(KeyAscii As Integer)
    KeyAscii = AceptaSoloNumeros(KeyAscii)
End Sub

Private Sub TxtUnidades_Change()
    Me.TxtSubStotal.Text = Round(Val(Me.TxtPRecio.Text) * Val(Me.TxtUnidades.Text), 0)
End Sub



Private Sub TxtUnidades_KeyPress(KeyAscii As Integer)
    Dim Respuesta2 As Integer
    KeyAscii = AceptaSoloNumeros(KeyAscii)
    Respuesta = InStr(Me.TxtUnidades.Text, ".")
    
    If Respuesta > 0 Then
        If KeyAscii = 46 Then KeyAscii = 0
        'Respuesta2 = InStr(Respuesta + 1, Me.TxtUnidades.Text, ".")
       '
       ' If Respuesta2 <> Respuesta Then KeyAscii = 0
        
    End If
End Sub

Public Sub LimpiaCajas()

    Me.TxtCodigo.Text = ""
    Me.TxtMarca.Text = ""
    Me.txtDescripcion.Text = ""
    Me.TxtUnidades.Text = ""
    Me.TxtPRecio.Text = ""
    Me.TxtSubStotal.Text = ""
    Me.LbStockActual.Caption = ""
    Me.TxtStockActual.Text = ""
    
End Sub

Public Sub SumaElGrid()
  
End Sub

Public Sub BorraTemporal()
    NoModXcien = False
    NoModPesos = False
     Me.SkTotal.Caption = 0
    Me.TxtDescuento = ""
    Me.TxtDescuentoPesos = ""
    Me.SkTotalAfecto = 0
    Me.SKIVA.Caption = 0
    Me.txtTotalBruto.Text = 0
    
    
    NoModXcien = True
    NoModPesos = True
   
    
End Sub
Public Sub ValidaDocumentoCompra()
    EstadoCompra = 1
    Sql = "SELECT rut,documento,no_documento, tipo_movimiento,partepagodoc,partepagondoc,clasificacion FROM compra_repuesto_documento WHERE "
    If CboTipoDoc.Text = "ORDEN DE INGRESO" Then
        Sql = Sql & "documento = '" & Me.CboTipoDoc.Text & "' and no_documento = " & Val(Me.TxtNDoc)
    Else
        Sql = Sql & "rut = '" & Me.TxtRutProveedor & "' and documento = '" & Me.CboTipoDoc.Text & "' and no_documento = " & Val(Me.TxtNDoc)
    End If
    paso = AbreConsulta(RS_ConsultaDoc, Sql)
    
    If Len(Me.TxtNDoc.Text) = 0 Then
        MsgBox "Debe especificar un N� de documento", vbOKOnly + vbInformation
        Me.TxtNDoc.SetFocus
        Exit Sub
    End If
    If RS_ConsultaDoc.RecordCount > 0 Then
        Respuesta = MsgBox("El documento de compra ya ha sido utilizado" + Chr(13) + "�Desea Mostrar?", vbYesNo + vbQuestion)
        
        If Respuesta = 6 Then
            Me.BorraTemporal
            Me.CmdModificar.Enabled = True
            Me.CmdCancelAct.Enabled = True
            Me.CmdGuardar.Enabled = False
            SuperTab.Enabled = False
            
            If RS_ConsultaDoc!Tipo_Movimiento = "COMPRA REPUESTO" Then
                 SuperTab.Tab = 0
                'cargar repuestos
                If Not IsNull(RS_ConsultaDoc!clasificacion) Then
                    Option1.Value = IIf(RS_ConsultaDoc!clasificacion = 1, True, False)
                    Option2.Value = IIf(RS_ConsultaDoc!clasificacion = 2, True, False)
                End If
                Dim rs_REPUESTOS As Recordset
                Sql = "SELECT * FROM compra_stock_repuestos " & _
                "WHERE rut_proveedor = '" & Me.TxtRutProveedor & "' AND documento_ingreso = '" & Me.CboTipoDoc.Text & "' AND numero_documento = " & Me.TxtNDoc
                paso = AbreConsulta(rs_REPUESTOS, Sql)
                With rs_REPUESTOS
                    If .RecordCount > 0 Then
                       
                        .MoveFirst
                        Do While Not .EOF
                            AdoTemporal.Recordset.AddNew
                            AdoTemporal.Recordset.Fields("cod_producto") = !cod_producto
                            AdoTemporal.Recordset.Fields("marca_repuesto") = !marca_Repuesto
                            AdoTemporal.Recordset.Fields("descripcion") = !Descripcion
                            AdoTemporal.Recordset.Fields("unidades") = !Unidades
                            AdoTemporal.Recordset.Fields("precio_compra") = !precio_compra
                            AdoTemporal.Recordset.Fields("total") = !total
                            .MoveNext
                        Loop
                        AdoTemporal.Recordset.Update
                    End If
                End With
                Me.CmdGuardar.Enabled = False
                
            Else
                 
                
                Dim Rs_Vehiculo As Recordset
                
                Sql = "SELECT * FROM vehiculos WHERE " ' proveedor = '" & Me.TxtRutProveedor & "' and tipo_documento = '" & Me.CboTipoDoc.Text & "' and no_documento = " & Me.TxtNdoc
                If CboTipoDoc.Text = "ORDEN DE INGRESO" Then
                    Sql = Sql & "tipo_documento  = 'ORDEN DE INGRESO' AND  no_documento = " & Me.TxtNDoc
                Else
                    Sql = Sql & "proveedor = '" & Me.TxtRutProveedor & " ' and tipo_documento = '" & Me.CboTipoDoc.Text & "' and no_documento = " & Me.TxtNDoc
                End If
                paso = AbreConsulta(Rs_Vehiculo, Sql)
                
                If RS_ConsultaDoc!Tipo_Movimiento = "VEHICULO NUEVO" Then
                    'cargar vehiculo NUEVO
                    SuperTab.Tab = 1
                    With Rs_Vehiculo
                        TxtRutProveedor = !proveedor
                        TxtRsocial = !nombre_proveedor
                        NTxtPatente = !patente
                        rutProveedor = True
                        NTxtTipo = !tipo
                        Me.NcboMarca.Text = !MARCA
                        Me.NcboModelo.Text = !MODELO
                        Me.NcboAno.Text = !ano
                        Me.NTxtColor = !Color
                        Me.NTxtPuertas = !puertas
                        Me.NTxtAsientos = !asientos
                        Me.NTxtCarga = " " & !carga
                        Me.NTxtPeso = " " & !peso
                        Me.NTxtCombustibl = !gasolina
                        Me.NTxtMotor = !MOTOR
                        Me.NTxtChasis = " " & !chasis
                        Me.NTxtSerie = " " & !SERIE
                        Me.TxtNVIN = " " & !VIN
                        nTxtubicacion = "" & !ubicacion
                        Me.NTXTPrecioCompra = !precio_compra
                    End With
                    
                Else
                     SuperTab.Tab = 2
                    'CARGAR VEHICULO USADO
                    With Rs_Vehiculo
                        TxtRutProveedor = "" & !proveedor
                        TxtRsocial = !nombre_proveedor
                        rutProveedor = True
                        txtPatente = !patente
                        TxtTipoVehiculo = !tipo
                        Me.CboMarca.Text = !MARCA
                        Me.CboModelo.Text = !MODELO
                        Me.cboano.Text = !ano
                        
                        Me.TxtCtaKm = !km_cta_kilometros
                        TxtColor = !Color
                        TxtCombus = !gasolina
                        TxtComentario = !comentario
                        Txtmotor = !MOTOR
                        tXTuBICACION = "" & !ubicacion
                        Me.TxtPVenta = !precio_venta
                        If IsNull(!transferido) Then
                            Me.CboTransferido.Text = "No"
                        Else
                            If !transferido = "SI" Then Me.CboTransferido.Text = "SI" Else Me.CboTransferido.Text = "NO"
                        End If
                        
                        
                        If !CONSIGNACION = 1 Or !partepago = 1 Then
                            Me.CheckConsignacion.Value = 1
                            Op1.Value = IIf(!CONSIGNACION = 1, True, False)
                            Op2.Value = IIf(!partepago = 1, True, False)
                            
                            If !partepago = 1 Then
'                                TxtNFacVenta.SetFocus
                                Me.CboDocVenta.Text = RS_ConsultaDoc!partepagodoc
                                Me.TxtNFacVenta = RS_ConsultaDoc!partepagondoc
                            End If
                                                      
                        End If
                        Me.TxtPrecioCpra = !precio_compra
                    End With
                End If
            End If
            
        Else
                
            Me.TxtRutProveedor.Text = ""
            Me.TxtRutProveedor.SetFocus
        End If
        
    Else
    
          Sql = "SELECT * FROM consignados WHERE rut = '" & Me.TxtRutProveedor & "'and orden_ingreso = " & Me.TxtNDoc
            paso = AbreConsulta(RS_ConsultaDoc, Sql)
            With RS_ConsultaDoc
                If .RecordCount > 0 Then
                    Respuesta = MsgBox("El documento de compra ya ha sido utilizado" + Chr(13) + "�Desea Mostrar?", vbYesNo + vbQuestion)
                    
                    If Respuesta = 6 Then
                        Me.TxtRsocial = RS_ConsultaDoc!Nombre
                        
                        Sql = "SELECT * FROM vehiculos WHERE proveedor = '" & Me.TxtRutProveedor & "' and tipo_documento = '" & Me.CboTipoDoc.Text & "' and no_documento = " & Me.TxtNDoc
                        paso = AbreConsulta(Rs_Vehiculo, Sql)
                        With Rs_Vehiculo
                            txtPatente = !patente
                            TxtTipoVehiculo = !tipo
                            Me.CboMarca.Text = !MARCA
                            Me.CboModelo.Text = !MODELO
                            Me.cboano.Text = !ano
                            Me.TxtPrecioCpra = !precio_compra
                            Me.TxtCtaKm = !km_cta_kilometros
                            Me.TxtPVenta = !precio_venta
                            
                             TxtColor = !Color
                            TxtCombus = !gasolina
                            TxtComentario = !comentario
                            Txtmotor = !MOTOR
                            tXTuBICACION = "" & !ubicacion
                            Me.CheckConsignacion.Value = 1
                            Me.Check1.Enabled = False
                            SuperTab.Tab = 2
                      End With
                        Me.CmdModificar.Enabled = True
                       ' Me.CmdCancelAct.Enabled = True
                        Me.CmdGuardar.Enabled = False
                        SuperTab.Enabled = False
                    End If
                    
                Else
                   ' Me.TxtRutProveedor.Text = ""
                   ' Me.TxtRutProveedor.SetFocus
                        
                
                End If
                
            End With
        
    End If
    
End Sub

Public Sub CargaMarcas()
    CboMarca.Clear
    LLenarCombo CboMarca, "mar_nombre", "mar_id", "par_marcas", "mar_activo='SI'"
    LLenarCombo NcboMarca, "mar_nombre", "mar_id", "par_marcas", "mar_activo='SI'"
    Exit Sub
    
    
    listamarcas.Path = Ruta & "\MODELOS"
    If listamarcas.ListCount > 0 Then
        If listamarcas.ListCount > 1 Then
            For I = 0 To listamarcas.ListCount - 1
                CboMarca.AddItem Replace(listamarcas.List(I), ".txt", "")
                NcboMarca.AddItem Replace(listamarcas.List(I), ".txt", "")
            Next
        Else
            CboMarca.AddItem Replace(listamarcas.List(0), ".txt", "")
            NcboMarca.AddItem Replace(listamarcas.List(0), ".txt", "")
        End If
    End If
End Sub


Private Sub NTxtAsientos_KeyPress(KeyAscii As Integer)
 KeyAscii = SoloNumeros(KeyAscii)
End Sub

Private Sub NTxtCarga_KeyPress(KeyAscii As Integer)
' KeyAscii = SoloNumeros(KeyAscii)
KEYASCi = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub NTxtColor_KeyPress(KeyAscii As Integer)
  KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub NTxtCombustibl_KeyPress(KeyAscii As Integer)
  KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub NTxtChasis_KeyPress(KeyAscii As Integer)
  KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub NTxtMotor_KeyPress(KeyAscii As Integer)
  KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub NTxtPatente_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub



Private Sub NTXTPrecioCompra_KeyPress(KeyAscii As Integer)
 KeyAscii = SoloNumeros(KeyAscii)
End Sub

Private Sub NTXTPrecioCompra_LostFocus()
    If Val(Me.NTXTPrecioCompra) > 0 Then Me.NTXTPrecioCompra = Format(Me.NTXTPrecioCompra, "###,##")
End Sub

Private Sub NTxtPuertas_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
End Sub

Private Sub NTxtSerie_KeyPress(KeyAscii As Integer)
  KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub NTxtTipo_KeyPress(KeyAscii As Integer)
  KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub
Public Sub Limpia()
        
    
    Me.txtPatente = ""
    Me.TxtCtaKm = ""
    Me.TxtPrecioCpra = ""
    Me.CboMarca.Clear
    Me.CboModelo.Clear
    Me.TxtTipoVehiculo = ""
    Me.TxtPVenta = ""
    Me.TxtColor = ""
    Me.Txtmotor = ""
    TxtCombus = ""
    Me.TxtComentario = ""
End Sub

Public Sub NLimpia()
    Me.NTxtPatente = ""
    Me.NcboMarca.Clear
    Me.NcboModelo.Clear
    Me.NTxtTipo = ""
    Me.NTxtAsientos = ""
    Me.NTxtCarga = ""
    Me.TxtNVIN = ""
    Me.NTxtColor = ""
    Me.NTxtPeso = ""
    Me.NTxtPuertas = ""
    Me.NTxtSerie = ""
    Me.NTxtCombustibl = ""
    Me.NTxtMotor = ""
    Me.NTxtChasis = ""
    Me.NTXTPrecioCompra = ""
End Sub

Public Sub RebajaProductos()

        'cargar repuestos
        Dim rs_REPUESTOS As Recordset
        Sql = "SELECT * FROM compra_stock_repuestos " & _
        "WHERE rut_proveedor = '" & Me.TxtRutProveedor & "' AND documento_ingreso = '" & Me.CboTipoDoc.Text & "' AND numero_documento = " & Me.TxtNDoc
        paso = AbreConsulta(rs_REPUESTOS, Sql)
        
        With rs_REPUESTOS
                If .RecordCount > 0 Then
                    .MoveFirst
                    Do While Not .EOF
                       'Actualizo el stock del producto
                        Sql = "UPDATE maestro_productos SET stock_actual=stock_actual -" & Replace(Str(!Unidades), ",", ".") & _
                              " WHERE codigo='" & !cod_producto & "'"
                        Call AbreConsulta(RsTmp, Sql)
                        .MoveNext
                    Loop
                End If
        End With
        rs_REPUESTOS.close
        Sql = "DELETE FROM compra_stock_repuestos " & _
              "WHERE Rut_Proveedor = '" & Me.TxtRutProveedor & "' AND DOCUMENTO_INGRESO = '" & Me.CboTipoDoc.Text & "' AND NUMERO_DOCUMENTO = " & Me.TxtNDoc
        Call AbreConsulta(rs_REPUESTOS, Sql)
End Sub
Public Sub EmiteOE()
    Dim RsProv As Recordset
    Dim Rsocial As String * 40
    Dim Giro As String * 50
    Dim DIRECCION As String * 45
    Dim MARCA As String * 15
    Dim MODELO As String * 15
    Dim SERIE As String * 20
    Dim MOTOR As String * 20
    Dim FECHA As String * 101
    Dim Ulinea As String * 70
    
    
    Sql = "SELECT * FROM maestro_proveedores WHERE rut_proveedor = '" & Me.TxtRutProveedor & "'"
    paso = AbreConsulta(RsProv, Sql)
'' Generamos un archivo de texto con los datos de la OT
    Dim Fot As Integer

    Fot = FreeFile
    Archivo = App.Path & "\OS\" & Me.TxtNDoc.Text & ".os"
    Open Archivo For Output As #Fot 'output se crea el archivo
        Print #Fot, RsEmpresa
        Print #Fot, DireccionEmpresa
        Print #Fot, GiroEmpresa
        Print #Fot, RutEmpresa

        Print #Fot, "ORDEN DE ENTRADA N� " & Me.TxtNDoc.Text
        
            RSet FECHA = "Fecha:" & Format(DtFecha.Value, "dd-mm-yyyy")
            Print #Fot, FECHA
           
            
            Print #Fot, "Datos del proveedor"
            Rsocial = Me.TxtRsocial
            DIRECCION = RsProv!DIRECCION
         '   Giro = RsProv!Giro
            Print #Fot, "R.U.T.: " & Me.TxtRutProveedor & Space(10) & "Raz�n Social :" & Rsocial
            Print #Fot, "Direcci�n: " & DIRECCION & Space(5) & "Comuna:" & RsProv!comuna & Space(10) & "Ciudad:" & RsProv!ciudad
          '  Print #Fot, "Giro:" & Giro & "   "
             Print #Fot, ""
            Print #Fot, ""
            Print #Fot, ""
            
            Print #Fot, "Datos del Vehiculo"
            Print #Fot, "PATENTE             : " & txtPatente.Text
            MARCA = Me.CboMarca.Text
            MODELO = Me.CboModelo.Text
            Print #Fot, "TIPO VEHICULO       : " & Me.TxtTipoVehiculo
            Print #Fot, "MARCA               : " & MARCA & Space(10)
            Print #Fot, "MODELO              : " & MODELO & Space(10)
            Print #Fot, "A�O                 : " & Me.cboano.Text
            Print #Fot, "KM. CTA. KILOMETROS : " & Me.TxtCtaKm
            
            RSet Ulinea = "POR CONCEPTO COMPRA VEHICULO       TOTAL $ " & NumFormat(Me.TxtPrecioCpra)
            
            Print #Fot, ""
            Print #Fot, Ulinea
          
            Print #Fot, ""
            For I = 1 To 15
                Print #Fot, ""
            Next
    Close #Fot
    
    
    ''' Fin de la creacion del archivo de texto con extension .ot
    
    
  '  DIALOGO.CancelError = True
  '  On Error GoTo ErrHandler

    Dim CantCopias As Integer
    Dialogo.Copies = 1
    
    Me.Timer1.Enabled = False
    Dialogo.ShowPrinter
    
    'llamamos al dialogo de impresion
    CantCopias = Dialogo.Copies
    
    Dim PosicionMo, PosicionMA, PosicionOT
    Dim cEspacios As Integer
    cEspacios = 10
    F = FreeFile '
    Dim Linea As String
    Open Archivo For Input As #F  'Abrimos el archivos
        Printer.FontName = "Courier New"
        Printer.CurrentY = Printer.CurrentY + (1000 - Printer.CurrentY Mod 1000)
       'Printer.CurrentX = Printer.CurrentX + 200
        paso = TipoLetra(8, True, False, False)
        
        Line Input #F, Linea 'Extraemos linea
        Printer.Print Space(cEspacios) & Linea  'aqui la enviamos a la impresora
        Line Input #F, Linea
        Printer.Print Space(cEspacios) & Linea  ' direccion
        Line Input #F, Linea
        Printer.Print Space(cEspacios) & Linea  'giro
        Line Input #F, Linea
        Printer.Print Space(cEspacios) & Linea  'rut
        
        paso = TipoLetra(14, True, False, False)
        Line Input #F, Linea
        Printer.Print Space(cEspacios) & Space(15) & Linea & Space(20)  'titulo
        paso = TipoLetra(8, True, False, False)
        Line Input #F, Linea
        Printer.Print Linea   ' FECHA
        Printer.Print ""
        Printer.Print ""
        Line Input #F, Linea
        Printer.Print Space(20) & Linea ' FECHA
        Printer.Print ""
       paso = TipoLetra(8, False, False, False)
        For I = 1 To 5 'DATOS DEL CLIENTE
            Line Input #F, Linea
            Printer.Print Space(cEspacios) & Linea
            Printer.CurrentY = Printer.CurrentY + 80
        Next
        
        paso = TipoLetra(8, True, False, False)
        Line Input #F, Linea  'DATOS DEL VEHICULO
        Printer.Print Space(20) & Linea
        paso = TipoLetra(8, False, False, False)
        Printer.CurrentY = Printer.CurrentY + 100
        For I = 1 To 6
            Line Input #F, Linea
            Printer.Print Space(cEspacios) & Linea
            Printer.CurrentY = Printer.CurrentY + 80
        Next
        Printer.Print Space(cEspacios) & ""
        'Line Input #F, Linea
        Line Input #F, Linea
    
        paso = TipoLetra(10, True, False, False)
        Line Input #F, Linea
        Printer.Print Space(cEspacios) & Linea  ' TOTAL
        Printer.CurrentY = Printer.CurrentY + 100
        
        For I = 1 To 4
            Line Input #F, Linea 'detalle del clinete
            Printer.Print Space(cEspacios) & Linea
            Printer.CurrentY = Printer.CurrentY + 80
        Next
    '    Printer.Print Space(cEspacios) & ""
    '
    ''
     '
     '   Printer.CurrentY = Printer.CurrentY + 80
     '   Line Input #F, Linea 'total $ lista materiales
    ''    PosicionMA = Printer.CurrentY + 200 'Posicion para rectangulo Repuestos
     '   Printer.Print Space(cEspacios) & Linea
     '
    '    Printer.Print Space(cEspacios) & ""
     '   PASO = TipoLetra(8, True, False, False)
     '   Line Input #F, Linea 'IMPRIME TOTAL OT
    ''    Printer.Print Space(cEspacios) & Linea
      '  Line Input #F, Linea 'IVA
    '    Printer.Print Space(cEspacios) & Linea
    ''    Line Input #F, Linea 'BRUTO
    '    Printer.Print Space(cEspacios) & Linea
    '    PosicionOT = Printer.CurrentY + 300 'Posicion rectangulo total
    '    For i = 1 To 4: Printer.Print "": Next
    '
    '
    '    Printer.Print "                                                                           Fecha:____/____/_____Hora:___:___"
    Close #F
    
    
    
    Printer.DrawWidth = 1
    
    Printer.Line (400, 700)-(11000, 2400), , B 'Rectangulo Encabezado y N� Orden
    
 '   Printer.Line (400, 2400)-(11000, 2920), , B 'Rectangulo centro costo y administrativo
    
   Printer.Line (400, 2400)-(11000, 3900), , B 'Rectangulo Datos del cliente

    Printer.Line (400, 3900)-(11000, 6150), , B ' Rectangulo Datos del vehiculo
    
    Printer.Line (400, 6150)-(11000, 6500), , B 'Rectangulo tOTAL
    
    'Printer.Line (400, PosicionMo)-(11000, PosicionMA), , B 'Rectangulo repuestos
    
    Printer.DrawWidth = 3
   ' Printer.Line (400, PosicionMA)-(11000, PosicionOT), , B 'Rectangulo total
    
    Printer.NewPage 'activar lo enviado a la
    Printer.EndDoc 'impresora, o sea , que imprima lo que le hemos enviado mediante los printer
    
    
    Me.Timer1.Enabled = True
    
    
    Exit Sub
    
    


ErrHandler:
    ' El usuario ha hecho clic en el bot�n Cancelar
    MsgBox "La impresion ha sido cancelada"

    Exit Sub


End Sub

Private Sub TxValorFacArriendo_Change()
    SkTotal = TxValorFacArriendo
    SkTotalAfecto = TxValorFacArriendo
    CalculaFac
End Sub

Private Sub TxValorFacArriendo_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
End Sub

Public Sub CalculaFac()

    txtTotalBruto = NumFormat(Round(AgregarIVA(Val(SkTotalAfecto), 19), 0))
    SKIVA = NumFormat(NumReal(txtTotalBruto) - NumReal(SkTotalAfecto))
    
End Sub
