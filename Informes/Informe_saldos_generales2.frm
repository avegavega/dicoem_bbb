VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{DA729E34-689F-49EA-A856-B57046630B73}#1.0#0"; "progressbar-xp.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form Informe_saldos_generales 
   Caption         =   "Saldos Generales"
   ClientHeight    =   8700
   ClientLeft      =   2010
   ClientTop       =   795
   ClientWidth     =   15105
   LinkTopic       =   "Form1"
   ScaleHeight     =   8700
   ScaleWidth      =   15105
   Begin MSComCtl2.DTPicker DtHastaCartola 
      Height          =   285
      Left            =   12300
      TabIndex        =   23
      Top             =   495
      Width           =   1440
      _ExtentX        =   2540
      _ExtentY        =   503
      _Version        =   393216
      Format          =   95354881
      CurrentDate     =   42462
   End
   Begin VB.Frame FraProgreso 
      Height          =   795
      Left            =   1620
      TabIndex        =   15
      Top             =   4815
      Visible         =   0   'False
      Width           =   12435
      Begin Proyecto2.XP_ProgressBar BarraProgreso 
         Height          =   450
         Left            =   105
         TabIndex        =   16
         Top             =   225
         Width           =   12150
         _ExtentX        =   21431
         _ExtentY        =   794
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BrushStyle      =   0
         Color           =   16777088
         Scrolling       =   1
         ShowText        =   -1  'True
      End
   End
   Begin VB.Frame Frame3 
      Caption         =   "Filtro"
      Height          =   1335
      Left            =   6210
      TabIndex        =   7
      Top             =   240
      Width           =   8505
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   300
         Left            =   5430
         OleObjectBlob   =   "Informe_saldos_generales2.frx":0000
         TabIndex        =   24
         Top             =   270
         Width           =   600
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Omitir Guias"
         Height          =   270
         Left            =   570
         TabIndex        =   22
         Top             =   990
         Width           =   2580
      End
      Begin VB.TextBox TxtBusqueda 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   555
         TabIndex        =   8
         Top             =   615
         Width           =   4710
      End
      Begin VB.CommandButton CmdBusca 
         Caption         =   "Buscar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   5415
         TabIndex        =   10
         ToolTipText     =   "Busca texto ingresado"
         Top             =   585
         Width           =   1095
      End
      Begin VB.CommandButton CmdTodos 
         Caption         =   "Todos"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   6450
         TabIndex        =   12
         ToolTipText     =   "Muestra todas las compras"
         Top             =   585
         Width           =   1095
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   255
         Left            =   555
         OleObjectBlob   =   "Informe_saldos_generales2.frx":0068
         TabIndex        =   9
         Top             =   360
         Width           =   2295
      End
   End
   Begin VB.Frame Frame4 
      Caption         =   "Mes y A�o"
      Height          =   1335
      Left            =   5115
      TabIndex        =   2
      Top             =   8610
      Visible         =   0   'False
      Width           =   3480
      Begin VB.CheckBox ChkPorMes 
         Caption         =   "Por mes"
         Height          =   270
         Left            =   1020
         TabIndex        =   11
         Top             =   390
         Width           =   1455
      End
      Begin VB.ComboBox comMes 
         Enabled         =   0   'False
         Height          =   315
         ItemData        =   "Informe_saldos_generales2.frx":00CD
         Left            =   420
         List            =   "Informe_saldos_generales2.frx":00CF
         Style           =   2  'Dropdown List
         TabIndex        =   4
         Top             =   900
         Width           =   1575
      End
      Begin VB.ComboBox ComAno 
         Enabled         =   0   'False
         Height          =   315
         Left            =   1995
         Style           =   2  'Dropdown List
         TabIndex        =   3
         Top             =   900
         Width           =   1215
      End
      Begin ACTIVESKINLibCtl.SkinLabel Skmes 
         Height          =   255
         Left            =   420
         OleObjectBlob   =   "Informe_saldos_generales2.frx":00D1
         TabIndex        =   5
         Top             =   675
         Width           =   375
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkAno 
         Height          =   255
         Left            =   1995
         OleObjectBlob   =   "Informe_saldos_generales2.frx":0135
         TabIndex        =   6
         Top             =   675
         Width           =   375
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Saldos Generalales"
      Height          =   6855
      Left            =   435
      TabIndex        =   0
      Top             =   1605
      Width           =   14265
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   240
         Left            =   270
         OleObjectBlob   =   "Informe_saldos_generales2.frx":0199
         TabIndex        =   21
         Top             =   5670
         Width           =   4680
      End
      Begin VB.Frame Frame2 
         Caption         =   "Totales"
         Height          =   600
         Left            =   9225
         TabIndex        =   17
         Top             =   5640
         Width           =   4530
         Begin VB.TextBox TxtTotales 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00E0E0E0&
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   255
            Locked          =   -1  'True
            TabIndex        =   20
            Top             =   195
            Width           =   1350
         End
         Begin VB.TextBox TxtAbonos 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00E0E0E0&
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   1605
            Locked          =   -1  'True
            TabIndex        =   19
            Top             =   195
            Width           =   1350
         End
         Begin VB.TextBox txtSaldos 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00E0E0E0&
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   2910
            Locked          =   -1  'True
            TabIndex        =   18
            Top             =   195
            Width           =   1350
         End
      End
      Begin VB.CommandButton cmdSalir 
         Cancel          =   -1  'True
         Caption         =   "&Salir"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   11880
         TabIndex        =   14
         ToolTipText     =   "Salir"
         Top             =   6285
         Width           =   2055
      End
      Begin VB.CommandButton cmdExportar 
         Caption         =   "Exportar Lista a Excel"
         Height          =   495
         Left            =   75
         TabIndex        =   13
         ToolTipText     =   "Exportar"
         Top             =   6285
         Width           =   2055
      End
      Begin MSComctlLib.ListView LvDetalle 
         Height          =   5220
         Left            =   240
         TabIndex        =   1
         Top             =   390
         Width           =   13770
         _ExtentX        =   24289
         _ExtentY        =   9208
         View            =   3
         LabelWrap       =   0   'False
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   8
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "N109"
            Text            =   "Nro"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "N109"
            Text            =   "RUT"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "T1000"
            Text            =   "Nombre"
            Object.Width           =   7056
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   3
            Object.Tag             =   "N100"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   6
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   7
            Key             =   "saldos"
            Object.Tag             =   "N100"
            Text            =   "Saldo"
            Object.Width           =   2540
         EndProperty
      End
   End
End
Attribute VB_Name = "Informe_saldos_generales"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim Sm_FiltroNombre As String
Dim Sm_FiltroPeriodo As String
Dim Bm_Filtrar As Boolean
Dim Sm_Fguias As String
Private Sub Check1_Click()
    Sm_Fguias = ""
    If Check1.Value = 1 Then Sm_Fguias = " AND doc_nombre<>'GUIA' "
    CargaDatos
    LvDetalle.SetFocus
End Sub

Private Sub ChkPorMes_Click()
    If ChkPorMes.Value = 1 Then
        Skmes.Enabled = True
        SkAno.Enabled = True
        comMes.Enabled = True
        ComAno.Enabled = True
        FiltroPeriodo
    Else
        Skmes.Enabled = False
        SkAno.Enabled = False
        comMes.Enabled = False
        ComAno.Enabled = False
        Sm_FiltroPeriodo = Empty
        CargaDatos
    End If
End Sub
Private Sub FiltroPeriodo()
            Sm_FiltroPeriodo = " AND MONTH(v.fecha)=" & Me.comMes.ItemData(comMes.ListIndex) & " AND YEAR(v.fecha)=" & ComAno.Text & " "
            CargaDatos
End Sub

Private Sub CmdBusca_Click()
    If Len(TxtBusqueda) = 0 Then
        Sm_FiltroNombre = ""
    Else
        If Sp_extra = "CLIENTES" Then
        
            Sm_FiltroNombre = " AND nombre_rsocial LIKE '%" & TxtBusqueda & "%' "
        Else
            Sm_FiltroNombre = " AND nombre_empresa LIKE '%" & TxtBusqueda & "%' "
        End If
    End If
    
    
    CargaDatos
End Sub

Private Sub cmdSalir_Click()
    Unload Me
End Sub




Private Sub CmdExportar_Click()
    Dim tit(2) As String
    If LvDetalle.ListItems.Count = 0 Then Exit Sub
    FraProgreso.Visible = True
    tit(0) = Me.Caption & " " & DtFecha & " - " & Time
    tit(1) = ""
    tit(2) = ""
    ExportarNuevo LvDetalle, tit, Me, BarraProgreso
    FraProgreso.Visible = False
End Sub

Private Sub CmdTodos_Click()
    Sm_FiltroNombre = Empty
    CargaDatos
End Sub

Private Sub ComAno_Click()
    If Bm_Filtrar Then FiltroPeriodo
End Sub



Private Sub comMes_Click()
    If Bm_Filtrar Then FiltroPeriodo
End Sub

Private Sub Form_Load()
    Aplicar_skin Me
    Centrar Me, False
    Sm_Fguias = ""
    Bm_Filtrar = False
    For i = 1 To 12
        comMes.AddItem UCase(MonthName(i, False))
        comMes.ItemData(comMes.ListCount - 1) = i
    Next
    Busca_Id_Combo comMes, Val(IG_Mes_Contable)
    LLenaYears ComAno, 2010
    'For i = Year(Date) To Year(Date) - 1 Step -1
    '    ComAno.AddItem i
    'Next
    For i = 0 To ComAno.ListCount - 1
        If ComAno.List(i) = IG_Ano_Contable Then
            ComAno.ListIndex = i
            Exit For
        End If
    Next
    
    
    Sql = "SELECT men_infoextra " & _
          "FROM sis_menu " & _
          "WHERE men_id=" & IP_IDMenu
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        Sp_extra = RsTmp!men_infoextra
    End If
    
    If Sp_extra = "CLIENTES" Then
        Frame1.Caption = "CLIENTES"
        Me.Caption = "SALDOS GENERALES DE CLIENTES"
    Else
        Frame1.Caption = "PROVEEDORES"
        Me.Caption = "SALDOS GENERALES DE PROVEEDORES"
        
    End If
    
    
    Sm_FiltroNombre = Empty
    Sm_FiltroPeriodo = Empty
    Bm_Filtrar = True
    DtHastaCartola = Date
    CargaDatos
End Sub

Private Sub CargaDatos()
    Dim Sp_Pozo As String
    ' Cartolas de cuentas corrientes CLIENTES-PROVEEDORES
    
    If Sp_extra = "CLIENTES" Then
                If SP_Rut_Activo = "76.244.196-9" Then
                    ' Maq min no tomaremos en cuenta el pozo
                Else
                    Sp_Pozo = "-(SELECT IFNULL(SUM(pzo_abono)- SUM(pzo_cargo),0) saldo " & _
                        "FROM cta_pozo " & _
                        "WHERE rut_emp = '" & SP_Rut_Activo & "' AND pzo_rut = c.rut_cliente AND pzo_cli_pro = 'CLI') "
                End If
                'Ultima mod 15 Nov 2013
                cn.Execute "TRUNCATE TABLE tmp_saldo_temporal"
                Sql = "INSERT INTO tmp_saldo_temporal (tmp_rut,tmp_saldo) " & _
                     " SELECT v.rut_cliente,bruto-" & _
                 "IFNULL((SELECT SUM(t.ctd_monto) FROM cta_abono_documentos t ,cta_abonos a WHERE  a.abo_fecha <='" & Fql(Me.DtHastaCartola) & "' AND t.rut_emp='" & SP_Rut_Activo & "' AND t.abo_id=a.abo_id AND a.abo_cli_pro='CLI' AND t.id=v.id),0)" & _
                    Sp_Pozo & " saldo " & _
          "FROM ven_doc_venta v,maestro_clientes c,sis_documentos d,par_sucursales s " & _
          "WHERE v.fecha <='" & Fql(Me.DtHastaCartola) & "' AND v.rut_cliente<>'11.111.111-1' AND ven_nc_utilizada='NO' AND doc_informa_venta='SI' AND doc_nota_de_venta='NO' AND v.rut_emp='" & SP_Rut_Activo & "' AND  v.doc_id_factura=0 AND v.suc_id = s.suc_id And v.rut_cliente = c.rut_cliente AND v.doc_id = d.doc_id " & Sm_FiltroNombre & " " & _
            Sm_Fguias & _
           " /* HAVING saldo<> 0  */ " & _
          "ORDER BY id"
    
            cn.Execute Sql
            
             Sql = "SELECT @rownum:=@rownum+1 AS rownum,rut_cliente,nombre_rsocial,'','',0,0,SUM(tmp_saldo)  " & _
                    "FROM (SELECT @rownum:=0) r,tmp_saldo_temporal t " & _
                        "JOIN maestro_clientes m ON t.tmp_rut=m.rut_cliente " & _
                        "GROUP BY tmp_rut " & _
                        "ORDER BY @rownum :=@rownum + 1"
           
    
            'Ultima modificacion 18-Mayo 2013
            'probando...
         '   Sql = "SELECT v.rut_cliente,nombre_rsocial cliente,cli_nombre_fantasia,'',0,0," & _
                    "IFNULL((SELECT SUM(bruto) " & _
                    "FROM ven_doc_venta e " & _
                    "JOIN sis_documentos d USING(doc_id) " & _
                            "WHERE doc_nota_de_credito='NO' AND doc_nota_de_venta='NO' AND doc_informa_venta='SI' AND nro_factura=0 " & _
                            "AND e.rut_emp=v.rut_emp     AND e.rut_cliente=v.rut_cliente),0) - " & _
                    "IFNULL((SELECT SUM(a.abo_monto) " & _
                    "FROM cta_abonos a " & _
                    "WHERE a.abo_cli_pro='CLI' AND a.rut_emp=v.rut_emp AND a.abo_rut=v.rut_cliente),0) - " & _
                    "IFNULL((SELECT SUM(bruto) " & _
                    "FROM ven_doc_venta e " & _
                    "JOIN sis_documentos d USING(doc_id) " & _
                            "WHERE e.ven_nc_utilizada='NO' AND doc_nota_de_credito='SI' AND doc_nota_de_venta='NO' AND doc_informa_venta='SI' " & _
                            "AND e.rut_emp=v.rut_emp     AND e.rut_cliente=v.rut_cliente),0) SALDO " & _
                    "FROM ven_doc_venta v " & _
                    "JOIN maestro_clientes m ON v.rut_cliente=m.rut_cliente " & _
                    "WHERE v.rut_emp='" & SP_Rut_Activo & "' " & Sm_FiltroNombre & " " & _
                    "GROUP BY rut_cliente " & _
                    "HAVING SALDO<>0 "
         '   Sql = "SELECT rut_cliente,cliente,cli_nombre_fantasia,'',0,0,SALDO " & _
                "FROM vi_saldos_generales_clientes " & _
                "WHERE rut_emp='" & SP_Rut_Activo & "' " & _
                "HAVING SALDO<>0"
                
    
    Else 'Proveedores
        cn.Execute "TRUNCATE TABLE tmp_saldo_temporal"
        
        Sql = "INSERT INTO tmp_saldo_temporal (tmp_rut,tmp_saldo) " & _
                   "SELECT v.rut, total - (IFNULL((SELECT SUM(t.ctd_monto) FROM cta_abono_documentos t,cta_abonos a " & _
                                        "WHERE t.rut_emp='" & SP_Rut_Activo & "' AND t.abo_id = a.abo_id AND a.abo_cli_pro = 'PRO' AND t.id = v.id ),0)" & _
                    "/*+IFNULL((SELECT SUM(ctd_monto) FROM cta_abonos z " & _
                        "INNER JOIN cta_abono_documentos y USING(abo_id) " & _
                        "WHERE z.rut_emp='" & SP_Rut_Activo & "' AND y.rut_emp='" & SP_Rut_Activo & "' " & _
                        "AND y.id IN(SELECT id FROM com_doc_compra w WHERE w.id_ref=v.id)),0)*/) " & _
                        " saldo " & _
          "FROM    com_doc_compra v,   maestro_proveedores c,  sis_documentos d " & _
          "WHERE v.com_nc_utilizada='NO' AND v.rut_emp='" & SP_Rut_Activo & "' AND id_ref=0 AND  v.doc_id_factura = 0 AND v.Rut = c.rut_proveedor AND v.doc_id = d.doc_id  " & _
           Sm_Fguias & Sm_FiltroNombre & _
           "HAVING saldo<>0 " & _
          " ORDER BY v.id"
        
        cn.Execute Sql
        
            Sql = "SELECT  @rownum:=@rownum+1 AS rownum, rut_proveedor,nombre_empresa,'','',0,0,SUM(tmp_saldo)  " & _
                    "FROM (SELECT @rownum:=0) r, tmp_saldo_temporal t " & _
                        "JOIN maestro_proveedores m ON t.tmp_rut=m.rut_proveedor " & _
                        "GROUP BY tmp_rut " & _
                        "ORDER BY @rownum :=@rownum + 1"
                        
                        
            '15 nov 2013 probando..
           'Sql = "SELECT v.rut,nombre_empresa proveedor,'' cli_nombre_fantasia,'',0,0," & _
                    "IFNULL((SELECT SUM(total) " & _
                    "FROM com_doc_compra e " & _
                    "JOIN sis_documentos d USING(doc_id) " & _
                            "WHERE com_informa_compra='SI' " & Sm_Fguias & " AND doc_nota_de_credito='NO' AND doc_nota_de_venta='NO' AND doc_informa_venta='SI' AND nro_factura=0 " & _
                            "AND e.rut_emp=v.rut_emp     AND e.rut=v.rut),0) - " & _
                    "IFNULL((SELECT SUM(a.abo_monto) " & _
                    "FROM cta_abonos a " & _
                    "WHERE a.abo_cli_pro='PRO' AND a.rut_emp=v.rut_emp AND a.abo_rut=v.rut),0) /* esto es ignorado - " & _
                    "IFNULL((SELECT SUM(total) " & _
                    "FROM com_doc_compra e " & _
                    "JOIN sis_documentos d USING(doc_id) " & _
                            "WHERE doc_nota_de_credito='SI' " & Sm_Fguias & "  AND doc_nota_de_venta='NO' AND doc_informa_venta='SI' " & _
                            "AND e.rut_emp=v.rut_emp     AND e.rut=v.rut),0) */ SALDO " & _
                    "FROM com_doc_compra v " & _
                    "JOIN maestro_proveedores m ON v.rut=m.rut_proveedor " & _
                    "JOIN sis_documentos d ON v.doc_id=d.doc_id " & _
                    "WHERE v.rut_emp='" & SP_Rut_Activo & "' " & Sm_FiltroNombre & " " & Sm_Fguias & " " & _
                    "GROUP BY rut " & _
                    "HAVING SALDO<>0 "
    
    
        'Sql = "SELECT rut,p.nombre_empresa,'','',0,0," & _
                    "IFNULL((SELECT SUM(total) " & _
                    "FROM com_doc_compra e " & _
                    "JOIN sis_documentos d USING(doc_id) " & _
                            "WHERE doc_nota_de_credito='NO' AND doc_orden_de_compra='NO' AND doc_informa_venta='SI' " & _
                            "AND e.rut_emp=v.rut_emp     AND e.rut=v.rut),0) - " & _
                    "IFNULL((SELECT SUM(a.abo_monto) " & _
                    "FROM cta_abonos a " & _
                    "WHERE a.abo_cli_pro='PRO' AND a.rut_emp=v.rut_emp AND a.abo_rut=v.rut),0) - " & _
                    "IFNULL((SELECT SUM(total) " & _
                    "FROM com_doc_compra e " & _
                    "JOIN sis_documentos d USING(doc_id) " & _
                            "WHERE doc_nota_de_credito='SI' AND doc_orden_de_compra='NO' AND doc_informa_venta='SI' " & _
                            "AND e.rut_emp=v.rut_emp AND e.rut=v.rut),0) SALDO " & _
                    "FROM com_doc_compra v " & _
                    "JOIN maestro_proveedores p ON v.rut=p.rut_proveedor " & _
                    "WHERE v.rut_emp='" & SP_Rut_Activo & "' " & _
                    "GROUP BY rut"
    
    End If
    Sis_BarraProgreso.Show 1
   ' Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvDetalle, False, True, True, False
  '  Me.TxtTotales = NumFormat(TotalizaColumna(LvDetalle, "deuda"))
  '  Me.TxtAbonos = NumFormat(TotalizaColumna(LvDetalle, "abonos"))
    txtSaldos = NumFormat(TotalizaColumna(LvDetalle, "saldos"))
End Sub




Private Sub LvDetalle_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    ordListView ColumnHeader, Me, LvDetalle
End Sub

Private Sub LvDetalle_DblClick()
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    If Sp_extra = "CLIENTES" Then ctacte_Cartola.Sm_CliPro = "CLI" Else ctacte_Cartola.Sm_CliPro = "PRO"
    ctacte_Cartola.Sm_Rut = LvDetalle.SelectedItem.SubItems(1)
    ctacte_Cartola.Show 1
End Sub

Private Sub TxtBusqueda_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
    If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtBusqueda_Validate(Cancel As Boolean)
TxtBusqueda = Replace(TxtBusqueda, "'", "")
End Sub
