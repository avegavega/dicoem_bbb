VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{DA729E34-689F-49EA-A856-B57046630B73}#1.0#0"; "progressbar-xp.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form Informes_Productos_Cli_Pro 
   Caption         =   "Form1"
   ClientHeight    =   9810
   ClientLeft      =   2610
   ClientTop       =   1245
   ClientWidth     =   15420
   LinkTopic       =   "Form1"
   ScaleHeight     =   9810
   ScaleWidth      =   15420
   Begin VB.PictureBox PicTipo 
      BackColor       =   &H00FFFF80&
      Height          =   3660
      Left            =   9270
      ScaleHeight     =   3600
      ScaleWidth      =   4275
      TabIndex        =   36
      Top             =   705
      Visible         =   0   'False
      Width           =   4335
      Begin VB.CheckBox ChkTodos 
         Caption         =   "Check1"
         Height          =   195
         Left            =   105
         TabIndex        =   40
         Top             =   315
         Value           =   1  'Checked
         Width           =   180
      End
      Begin VB.CommandButton cmdCierraTipos 
         Caption         =   "x"
         Height          =   195
         Left            =   4005
         TabIndex        =   38
         Top             =   15
         Width           =   270
      End
      Begin MSComctlLib.ListView LvTipoProducto 
         Height          =   3330
         Left            =   15
         TabIndex        =   37
         Top             =   240
         Width           =   4185
         _ExtentX        =   7382
         _ExtentY        =   5874
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   2
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Width           =   1058
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Nombre"
            Object.Width           =   5292
         EndProperty
      End
      Begin VB.Label Label1 
         BackStyle       =   0  'Transparent
         Caption         =   "Tipos de productos"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000E&
         Height          =   195
         Left            =   600
         TabIndex        =   39
         Top             =   0
         Width           =   1650
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H80000002&
         BackStyle       =   1  'Opaque
         FillColor       =   &H00FF0000&
         Height          =   240
         Left            =   -30
         Top             =   -15
         Width           =   4305
      End
   End
   Begin VB.Frame FraArticulos 
      Caption         =   "Articulos Comprados"
      Height          =   4905
      Left            =   150
      TabIndex        =   0
      Top             =   1770
      Width           =   15000
      Begin VB.CommandButton CmdExportaP 
         Caption         =   "Exportar a Excel"
         Height          =   315
         Left            =   105
         TabIndex        =   41
         Top             =   4500
         Width           =   1290
      End
      Begin MSComctlLib.ListView LvDetalle 
         Height          =   4185
         Left            =   90
         TabIndex        =   1
         Top             =   300
         Width           =   14805
         _ExtentX        =   26114
         _ExtentY        =   7382
         View            =   3
         LabelWrap       =   0   'False
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   7
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "N109"
            Text            =   "Codigo"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "T1100"
            Text            =   "Nombre Articulo"
            Object.Width           =   8819
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "T100"
            Text            =   "Marca"
            Object.Width           =   3175
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Object.Tag             =   "T100"
            Text            =   "Tipo Articulo"
            Object.Width           =   3351
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   4
            Object.Tag             =   "N102"
            Text            =   "Cant. "
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   5
            Object.Tag             =   "N102"
            Text            =   "Stk.Actual"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   6
            Object.Tag             =   "T1400"
            Text            =   "Codigo Proveedor"
            Object.Width           =   2540
         EndProperty
      End
   End
   Begin VB.ComboBox CboCentroCosto 
      Height          =   315
      Left            =   1935
      Style           =   2  'Dropdown List
      TabIndex        =   30
      Top             =   2655
      Width           =   3500
   End
   Begin VB.ComboBox CboItemGasto 
      Height          =   315
      Left            =   1935
      Style           =   2  'Dropdown List
      TabIndex        =   29
      Top             =   2985
      Width           =   3500
   End
   Begin VB.ComboBox CboCuenta 
      Height          =   315
      Left            =   1935
      Style           =   2  'Dropdown List
      TabIndex        =   28
      Top             =   2340
      Width           =   3500
   End
   Begin VB.CommandButton CmdIrKardex 
      Caption         =   "Consultar Kardex"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   450
      Left            =   135
      TabIndex        =   23
      Top             =   9375
      Width           =   1950
   End
   Begin VB.Frame FraDetalle 
      Caption         =   "Documentos"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2565
      Left            =   120
      TabIndex        =   17
      Top             =   6735
      Width           =   15000
      Begin VB.CommandButton CmdExportar 
         Caption         =   "Exportar a Excel"
         Height          =   315
         Left            =   195
         TabIndex        =   42
         Top             =   2190
         Width           =   1290
      End
      Begin VB.Frame FraProgreso 
         Height          =   540
         Left            =   1725
         TabIndex        =   21
         Top             =   3240
         Visible         =   0   'False
         Width           =   8835
         Begin Proyecto2.XP_ProgressBar PBar 
            Height          =   255
            Left            =   60
            TabIndex        =   22
            Top             =   180
            Width           =   8565
            _ExtentX        =   15108
            _ExtentY        =   450
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BrushStyle      =   0
            Color           =   16777088
            ShowText        =   -1  'True
         End
      End
      Begin VB.TextBox txtTotal 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   12855
         Locked          =   -1  'True
         TabIndex        =   20
         Top             =   3420
         Width           =   1500
      End
      Begin VB.TextBox TxtCantidad 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   11325
         Locked          =   -1  'True
         TabIndex        =   19
         Top             =   3420
         Width           =   1530
      End
      Begin MSComctlLib.ListView LvDocumentos 
         Height          =   1890
         Left            =   165
         TabIndex        =   18
         Top             =   300
         Width           =   14805
         _ExtentX        =   26114
         _ExtentY        =   3334
         View            =   3
         LabelWrap       =   0   'False
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   7
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "F1200"
            Text            =   "Fecha"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "T1100"
            Text            =   "Nombre "
            Object.Width           =   7056
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "T100"
            Text            =   "Documento"
            Object.Width           =   5292
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Object.Tag             =   "T100"
            Text            =   "Nro Documento"
            Object.Width           =   2646
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   4
            Object.Tag             =   "N102"
            Text            =   "Precio.U."
            Object.Width           =   2646
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   5
            Key             =   "cant"
            Object.Tag             =   "N102"
            Text            =   "Cantidad"
            Object.Width           =   2646
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   6
            Key             =   "total"
            Object.Tag             =   "N100"
            Text            =   "Total"
            Object.Width           =   2646
         EndProperty
      End
   End
   Begin VB.CommandButton CmdSalir 
      Cancel          =   -1  'True
      Caption         =   "&Retornar"
      Height          =   345
      Left            =   13935
      TabIndex        =   9
      Top             =   9435
      Width           =   1155
   End
   Begin VB.Frame Frame4 
      Caption         =   "Informacion"
      Height          =   1470
      Left            =   135
      TabIndex        =   2
      Top             =   165
      Width           =   14970
      Begin VB.CommandButton CmdTipos 
         Caption         =   "Seleccionar Tipos de Productos"
         Height          =   285
         Left            =   9210
         TabIndex        =   35
         Top             =   585
         Width           =   4170
      End
      Begin VB.ComboBox CboSucursal 
         Height          =   315
         Left            =   9900
         Style           =   2  'Dropdown List
         TabIndex        =   34
         Top             =   165
         Width           =   3500
      End
      Begin VB.TextBox txtNombreProveedor 
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2160
         Locked          =   -1  'True
         TabIndex        =   26
         Top             =   915
         Width           =   4395
      End
      Begin VB.CommandButton cmdRutProveedor 
         Caption         =   "Proveedor"
         Height          =   270
         Left            =   120
         TabIndex        =   25
         Top             =   945
         Width           =   900
      End
      Begin VB.TextBox txtRutProveedor 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1050
         TabIndex        =   24
         Top             =   915
         Width           =   1125
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Todas las fechas"
         Height          =   240
         Left            =   6870
         TabIndex        =   4
         Top             =   435
         Value           =   -1  'True
         Width           =   1560
      End
      Begin VB.TextBox TxtCodigo 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1050
         TabIndex        =   14
         ToolTipText     =   "Para ingresar un Gasto digite 0 (cero)"
         Top             =   285
         Width           =   1095
      End
      Begin VB.CommandButton CmdBuscaProducto 
         Caption         =   "Codigo"
         Height          =   270
         Left            =   105
         TabIndex        =   13
         Top             =   315
         Width           =   900
      End
      Begin VB.TextBox TxtMarca 
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   3210
         Locked          =   -1  'True
         TabIndex        =   12
         Top             =   255
         Width           =   3375
      End
      Begin VB.TextBox TxtDescrp 
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1050
         Locked          =   -1  'True
         TabIndex        =   11
         Top             =   600
         Width           =   5520
      End
      Begin VB.CommandButton cmdConsultar 
         Caption         =   "&Consultar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   420
         Left            =   13485
         TabIndex        =   10
         Top             =   930
         Width           =   1365
      End
      Begin MSComCtl2.DTPicker DTInicio 
         Height          =   300
         Left            =   7185
         TabIndex        =   6
         Top             =   705
         Width           =   1305
         _ExtentX        =   2302
         _ExtentY        =   529
         _Version        =   393216
         Enabled         =   0   'False
         Format          =   92078081
         CurrentDate     =   40628
      End
      Begin VB.OptionButton Option2 
         Caption         =   "Seleccionar fechas"
         Height          =   240
         Left            =   6870
         TabIndex        =   3
         Top             =   150
         Width           =   1725
      End
      Begin ACTIVESKINLibCtl.SkinLabel skIni 
         Height          =   240
         Left            =   6345
         OleObjectBlob   =   "Informes_Productos_Cli_Pro.frx":0000
         TabIndex        =   5
         Top             =   720
         Width           =   795
      End
      Begin MSComCtl2.DTPicker DtHasta 
         Height          =   285
         Left            =   7170
         TabIndex        =   7
         Top             =   1035
         Width           =   1305
         _ExtentX        =   2302
         _ExtentY        =   503
         _Version        =   393216
         Enabled         =   0   'False
         Format          =   92078081
         CurrentDate     =   40628
      End
      Begin ACTIVESKINLibCtl.SkinLabel skFin 
         Height          =   210
         Left            =   6345
         OleObjectBlob   =   "Informes_Productos_Cli_Pro.frx":0068
         TabIndex        =   8
         Top             =   1080
         Width           =   795
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
         Height          =   210
         Left            =   2280
         OleObjectBlob   =   "Informes_Productos_Cli_Pro.frx":00D0
         TabIndex        =   15
         Top             =   330
         Width           =   840
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel5 
         Height          =   210
         Left            =   165
         OleObjectBlob   =   "Informes_Productos_Cli_Pro.frx":0138
         TabIndex        =   16
         Top             =   615
         Width           =   840
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkSucursal 
         Height          =   210
         Left            =   8985
         OleObjectBlob   =   "Informes_Productos_Cli_Pro.frx":01A6
         TabIndex        =   33
         Top             =   240
         Width           =   840
      End
   End
   Begin VB.Timer Timer1 
      Left            =   720
      Top             =   8760
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   75
      OleObjectBlob   =   "Informes_Productos_Cli_Pro.frx":0214
      Top             =   8745
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
      Height          =   210
      Left            =   1020
      OleObjectBlob   =   "Informes_Productos_Cli_Pro.frx":0448
      TabIndex        =   27
      Top             =   2415
      Width           =   840
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
      Height          =   210
      Left            =   405
      OleObjectBlob   =   "Informes_Productos_Cli_Pro.frx":04B2
      TabIndex        =   31
      Top             =   2700
      Width           =   1455
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
      Height          =   210
      Left            =   405
      OleObjectBlob   =   "Informes_Productos_Cli_Pro.frx":052E
      TabIndex        =   32
      Top             =   3045
      Width           =   1455
   End
End
Attribute VB_Name = "Informes_Productos_Cli_Pro"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim Sm_FiltroCta As String
Dim Sm_FiltroCosto As String
Dim Sm_FiltroItem As String
Dim sm_FiltroFecha As String
Dim Sm_FiltroCodigo As String
Dim Sm_FiltroSuc As String
Dim Sm_FiltroTipo As String

Private Sub CboCentroCosto_Click()
    If CboCentroCosto.ListIndex > -1 Then
        If Sp_extra = "CLIENTES" Then
        
        Else
            If CboCentroCosto.Text = "TODOS" Then
                Sm_FiltroCosto = Empty
            Else
                Sm_FiltroCosto = " AND  c.cen_id=" & CboCentroCosto.ItemData(CboCentroCosto.ListIndex) & " "
            End If
        End If
        
    End If
  
End Sub



Private Sub CboCuenta_Click()
    If CboCuenta.ListIndex > -1 Then
        If Sp_extra = "CLIENTES" Then
        
        Else
            If CboCuenta.Text = "TODOS" Then
                Sm_FiltroCta = Empty
            Else
                Sm_FiltroCta = " AND c.pla_id=" & CboCuenta.ItemData(CboCuenta.ListIndex) & " "
            End If
        End If
    End If
   
End Sub



Private Sub CboItemGasto_Click()
    If CboItemGasto.ListIndex > -1 Then
        If Sp_extra = "CLIENTES" Then
        
        Else
            If CboItemGasto.Text = "TODOS" Then
                Sm_FiltroItem = Empty
            Else
                Sm_FiltroItem = " AND c.gas_id=" & CboItemGasto.ItemData(CboItemGasto.ListIndex) & " "
            End If
        End If
        
    End If

   
End Sub



Private Sub ChkTodos_Click()
    If ChkTodos.Value = 1 Then
        For i = 1 To LvTipoProducto.ListItems.Count
            LvTipoProducto.ListItems(i).Checked = True
        Next
    Else
        For i = 1 To LvTipoProducto.ListItems.Count
            LvTipoProducto.ListItems(i).Checked = False
        Next
    End If
End Sub

Private Sub CmdBuscaProducto_Click()
    BuscaProducto.Show 1
    If Len(SG_codigo) = 0 Then Exit Sub
    TxtCodigo = SG_codigo
    TxtCodigo_Validate True
End Sub

Private Sub cmdCierraTipos_Click()
    PicTipo.Visible = False
End Sub

Private Sub cmdConsultar_Click()
    If Option1.Value Then
        sm_FiltroFecha = Empty
    Else
         sm_FiltroFecha = " AND (r.fecha BETWEEN '" & Format(DTInicio.Value, "YYYY-MM-DD") & "' AND '" & Format(DtHasta.Value, "YYYY-MM-DD") & "') "
    End If
    
    If Sp_extra = "CLIENTES" Then
        If CboSucursal.ItemData(CboSucursal.ListIndex) = 0 Then
            Sm_FiltroSuc = ""
        Else
            Sm_FiltroSuc = " AND sue_id=" & CboSucursal.ItemData(CboSucursal.ListIndex)
        End If
    Else
        Sm_FiltroSuc = ""
    End If
    
    
    Sm_FiltroTipo = ""
    
    For i = 1 To LvTipoProducto.ListItems.Count
        If LvTipoProducto.ListItems(i).Checked Then
            Sm_FiltroTipo = Sm_FiltroTipo & LvTipoProducto.ListItems(i) & ","
        End If
    Next
    If Len(Sm_FiltroTipo) > 0 Then Sm_FiltroTipo = " AND p.tip_id IN(" & Mid(Sm_FiltroTipo, 1, Len(Sm_FiltroTipo) - 1) & ") "
    
    
    
    
    Sm_FiltroCodigo = Empty
    If Len(TxtCodigo) > 0 Then
        If Sp_extra = "CLIENTES" Then
            Sm_FiltroCodigo = " AND c.codigo='" & TxtCodigo & "' "
        Else
            If TxtCodigo = "0" Then
                Sm_FiltroCodigo = " AND ISNULL(c.pro_codigo) "
            Else
                Sm_FiltroCodigo = " AND c.pro_codigo='" & TxtCodigo & "' "
            End If
        End If
    End If
    LvDocumentos.ListItems.Clear
    CargaArticulos
End Sub

Private Sub CmdExportaP_Click()
    Dim tit(2) As String
    FraProgreso.Visible = True
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    
    tit(0) = "INFORME " & Me.Caption & " " & IIf(Sp_extra = "CLIENTES", "SUCURSAL:" & CboSucursal.Text, "")
    tit(1) = "FECHA INFORME: " & Date & " - " & Time
    tit(2) = "DESDE:" & Me.DTInicio.Value & "   HASTA:" & DtHasta.Value
    ExportarNuevo LvDetalle, tit, Me, PBar
    PBar.Value = 1
    FraProgreso.Visible = False
End Sub

Private Sub CmdExportar_Click()
    Dim tit(2) As String
    FraProgreso.Visible = True
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    
    tit(0) = "INFORME " & Me.Caption & " " & IIf(Sp_extra = "CLIENTES", "SUCURSAL:" & CboSucursal.Text, "")
    tit(1) = "FECHA INFORME: " & Date & " - " & Time
    tit(2) = LvDetalle.SelectedItem & " - " & LvDetalle.SelectedItem.SubItems(1) & " - " & LvDetalle.SelectedItem.SubItems(2)  ' "                                        DESDE:" & DtInicio.Value & "   HASTA:" & Dtfin.Value
    ExportarNuevo LvDocumentos, tit, Me, PBar
    PBar.Value = 1
    FraProgreso.Visible = False
End Sub

Private Sub CmdIrKardex_Click()
    inv_KardexGeneral.Show 1
End Sub

Private Sub cmdRutProveedor_Click()
    AccionProveedor = 0
    BuscaProveedor.Show 1
    TxtRutProveedor = RutBuscado

    txtRutProveedor_Validate True
End Sub

Private Sub CmdSalir_Click()
    Unload Me
End Sub




Private Sub CmdTipos_Click()
    PicTipo.Visible = True
End Sub



Private Sub Form_Load()
    Centrar Me
    Aplicar_skin Me
    Me.DtHasta.Value = Date
    Me.DTInicio.Value = Date - 30
    Sql = "SELECT men_infoextra " & _
          "FROM sis_menu " & _
          "WHERE men_id=" & IP_IDMenu
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        Sp_extra = RsTmp!men_infoextra
    End If
    
    
    '12-07-2014 tipos de products
    Sql = "SELECT tip_id,tip_nombre " & _
            "FROM par_tipos_productos " & _
            "WHERE tip_activo='SI' AND rut_emp='" & SP_Rut_Activo & "'"
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, Me.LvTipoProducto, True, True, True, False
    For i = 1 To LvTipoProducto.ListItems.Count
        LvTipoProducto.ListItems(i).Checked = True
    Next
    '12-07-2014 sucursales
    If Sp_extra = "CLIENTES" Then
        LLenarCombo CboSucursal, "CONCAT(sue_direccion,'-',sue_ciudad)", "sue_id", "sis_empresas_sucursales", "sue_activa='SI' AND emp_id=" & IG_id_Empresa
        CboSucursal.AddItem "TODAS"
        CboSucursal.ItemData(CboSucursal.ListCount - 1) = 0
        CboSucursal.ListIndex = CboSucursal.ListCount - 1
    End If
    
   
    If Sp_extra = "CLIENTES" Then
        CboCuenta.Enabled = False
        CboCentroCosto.Enabled = False
        CboItemGasto.Enabled = False
        FraArticulos.Caption = "Ultimos Productos vendidos"
        Me.Caption = "Informe de Ventas por Productos"
        Me.SkinLabel1.Enabled = False
        Me.SkinLabel2.Enabled = False
        Me.SkinLabel3.Enabled = False
        FraDetalle.Caption = "Documentos en los que se vendi� el articulo"
        Me.cmdRutProveedor.Visible = False
        TxtRutProveedor.Visible = False
        txtNombreProveedor.Visible = False
        LvDetalle.ColumnHeaders(7).Width = 0
     '  CargaArticulos
    Else 'PROVEEDORES
        SkSucursal.Visible = False
        CboSucursal.Visible = False
        LLenarCombo CboCuenta, "pla_nombre", "pla_id", "con_plan_de_cuentas", "pla_activo='SI'"
        CboCuenta.AddItem "TODOS"
        CboCuenta.ListIndex = CboCuenta.ListCount - 1
        LLenarCombo CboCentroCosto, "cen_nombre", "cen_id", "par_centros_de_costo", "cen_activo='SI' AND rut_emp='" & SP_Rut_Activo & "'"
        CboCentroCosto.AddItem "TODOS"
        CboCentroCosto.ListIndex = CboCentroCosto.ListCount - 1
        LLenarCombo Me.CboItemGasto, "gas_nombre", "gas_id", "par_item_gastos", "gas_activo='SI' AND rut_emp='" & SP_Rut_Activo & "'"
        CboItemGasto.AddItem "TODOS"
        CboItemGasto.ListIndex = CboItemGasto.ListCount - 1
        FraArticulos.Caption = "Ultimos Productos comprados"
        Me.Caption = "Informe de Compras por Productos"
        FraDetalle.Caption = "Documentos en los que se compr� el articulo"
       ' CargaArticulos
    End If
End Sub
Private Sub CargaArticulos()
    
    If Sp_extra = "CLIENTES" Then
        Sql = "SELECT c.codigo,IFNULL(c.descripcion,'NO INVENTARIABLE') nombreproducto," & _
                      "c.marca,tip_nombre,sum(unidades)," & _
                     "(SELECT SUM(sto_stock) FROM pro_stock s WHERE s.rut_emp='" & SP_Rut_Activo & "' AND  s.pro_codigo=c.codigo GROUP BY s.pro_codigo) stock " & _
             "FROM ven_detalle c " & _
             "RIGHT JOIN maestro_productos p ON c.codigo=p.codigo " & _
             "RIGHT JOIN par_tipos_productos USING(tip_id), " & _
             "ven_doc_venta r " & _
             "INNER JOIN sis_documentos USING(doc_id) " & _
             "WHERE  doc_informa_venta='SI' AND c.rut_emp='" & SP_Rut_Activo & "' AND  p.rut_emp='" & SP_Rut_Activo & "' AND   doc_nota_de_venta='NO' AND  r.rut_emp='" & SP_Rut_Activo & "' AND " & _
             "c.doc_id=r.doc_id AND c.no_documento=r.no_documento  " & Sm_FiltroCodigo & sm_FiltroFecha & Sm_FiltroCta & Sm_FiltroCosto & Sm_FiltroItem & Sm_FiltroSuc & Sm_FiltroTipo & " " & _
            " GROUP BY c.codigo " & _
             "ORDER BY c.id DESC " & _
             "/*LIMIT 100*/"
    
    Else 'PROVEEDORES
        Dim Sp_Filtro_Codigo_Proveedor As String
        Dim Sp_Solo_Proveedor As String
        Sp_Filtro_Codigo_Proveedor = ""
        Sp_Solo_Proveedor = ""
        If Len(TxtRutProveedor) > 0 Then
            Sp_Filtro_Codigo_Proveedor = ",(SELECT cpv_codigo_proveedor FROM par_codigos_proveedor o WHERE o.pro_codigo=p.codigo AND o.rut_proveedor='" & TxtRutProveedor & "' AND o.rut_emp='" & SP_Rut_Activo & "' LIMIT 1)"
            Sp_Solo_Proveedor = " AND rut='" & TxtRutProveedor & "' "
        End If
        Sql = "SELECT c.pro_codigo codigo,IFNULL(descripcion,'NO INVENTARIABLE') nombreproducto," & _
                      "mar_nombre,tip_nombre,sum(cmd_cantidad)," & _
                     "(SELECT SUM(sto_stock) FROM pro_stock s WHERE s.rut_emp='" & SP_Rut_Activo & "' AND s.pro_codigo=c.pro_codigo GROUP BY s.pro_codigo) stock " & _
                     Sp_Filtro_Codigo_Proveedor & " " & _
               " FROM com_doc_compra_detalle c " & _
                "RIGHT JOIN maestro_productos p ON c.pro_codigo=p.codigo " & _
                "RIGHT JOIN par_tipos_productos USING(tip_id) " & _
                "RIGHT JOIN com_doc_compra r ON c.id=r.id " & _
                "RIGHT JOIN par_marcas m USING(mar_id) " & _
                "INNER JOIN sis_documentos d USING(doc_id) " & _
                "WHERE com_informa_compra='SI' AND p.rut_emp='" & SP_Rut_Activo & "' AND d.doc_orden_de_compra='NO' AND r.rut_emp='" & SP_Rut_Activo & "'  " & Sm_FiltroCodigo & sm_FiltroFecha & Sm_FiltroCta & Sm_FiltroCosto & Sm_FiltroItem & Sp_Solo_Proveedor & Sm_FiltroTipo & _
                " GROUP BY pro_codigo " & _
                "ORDER BY c.id " & _
                "/*LIMIT 100*/"
    End If
    
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvDetalle, False, True, True, False
End Sub

Private Sub LvDetalle_Click()
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    
    If Option1.Value Then
        sm_FiltroFecha = Empty
    Else
        sm_FiltroFecha = " AND (m.fecha BETWEEN '" & Format(DTInicio.Value, "YYYY-MM-DD") & "' AND '" & Format(DtHasta.Value, "YYYY-MM-DD") & "') "
    End If
    
    
    If Sp_extra = "CLIENTES" Then
        
        If CboSucursal.ItemData(CboSucursal.ListIndex) = 0 Then
            Sm_FiltroSuc = ""
        Else
            Sm_FiltroSuc = " AND sue_id=" & CboSucursal.ItemData(CboSucursal.ListIndex)
        End If
    
    
    
    
        Sql = "SELECT m.fecha,nombre_cliente,doc_nombre,m.no_documento,c.precio_final,c.unidades,c.subtotal " & _
              "FROM ven_doc_venta m " & _
              "INNER JOIN sis_documentos d USING(doc_id),ven_detalle c " & _
              "WHERE ven_informa_venta = 'SI' AND c.rut_emp='" & SP_Rut_Activo & "' AND doc_nota_de_venta='NO' AND m.rut_emp='" & SP_Rut_Activo & "' AND c.doc_id=m.doc_id AND c.no_documento=m.no_documento AND codigo ='" & LvDetalle.SelectedItem.Text & "' " & Sm_FiltroCodigo & sm_FiltroFecha & Sm_FiltroCta & Sm_FiltroCosto & Sm_FiltroItem & Sm_FiltroSuc
    
    Else ' P R O V E D O R E S
        Sql = "SELECT m.fecha,nombre_empresa,doc_nombre,no_documento,c.cmd_unitario_neto,c.cmd_cantidad,c.cmd_total_neto " & _
              "FROM com_doc_compra m " & _
              "INNER JOIN com_doc_compra_detalle c ON m.id = c.id " & _
              "INNER JOIN sis_documentos d USING(doc_id) " & _
              "JOIN maestro_proveedores p ON m.rut=p.rut_proveedor " & _
              "WHERE d.doc_orden_de_compra='NO' AND m.rut_emp='" & SP_Rut_Activo & "' AND pro_codigo ='" & LvDetalle.SelectedItem.Text & "' " & Sm_FiltroCodigo & sm_FiltroFecha & Sm_FiltroCta & Sm_FiltroCosto & Sm_FiltroItem
    End If
    
    
    Consulta RsTmp, Sql

    LLenar_Grilla RsTmp, Me, Me.LvDocumentos, False, True, True, False
    TxtTotal = NumFormat(TotalizaColumna(LvDocumentos, "total"))
    TxtCantidad = NumFormat(TotalizaColumna(LvDocumentos, "cant"))
End Sub

Private Sub LvDetalle_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    ordListView ColumnHeader, Me, LvDetalle
    Select Case ColumnHeader
        Case "Cuenta"
            CboCuenta.Visible = True
        Case "Centro costo"
            Me.CboCentroCosto.Visible = True
        Case "Item Gasto"
            Me.CboItemGasto.Visible = True
    End Select
End Sub

Private Sub LvDocumentos_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    ordListView ColumnHeader, Me, LvDocumentos
End Sub

Private Sub Option1_Click()
    DTInicio.Enabled = False
    DtHasta.Enabled = False
End Sub

Private Sub Option2_Click()
    DTInicio.Enabled = True
    DtHasta.Enabled = True
End Sub
Private Sub TxtCodigo_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 And Len(TxtCodigo.Text) > 0 Then SendKeys "{Tab}"
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
    If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtCodigo_Validate(Cancel As Boolean)
    If Len(TxtCodigo) = 0 Then Exit Sub
    Sql = "SELECT descripcion,marca,precio_compra,stock_actual,tip_nombre familia " & _
          "FROM maestro_productos m,par_tipos_productos t " & _
          "WHERE  m.rut_emp='" & SP_Rut_Activo & "' AND  m.tip_id=t.tip_id AND codigo='" & TxtCodigo & "'"
    Call Consulta(RsTmp, Sql)
    If RsTmp.RecordCount > 0 Then
                'Codigo ingresado ha sido encontrado
            With RsTmp
                Me.TxtDescrp = !Descripcion
                Me.TxtMarca.Text = !MARCA
            End With
    Else
        TxtDescrp = Empty
        TxtMarca = Empty
    End If
End Sub



Private Sub txtRutProveedor_Validate(Cancel As Boolean)
    If Len(TxtRutProveedor) = 0 Then Exit Sub
      Sql = "SELECT nombre_empresa " & _
          "FROM maestro_proveedores " & _
          "WHERE rut_proveedor='" & TxtRutProveedor & "'"
      Consulta RsTmp, Sql
      If RsTmp.RecordCount > 0 Then
          txtNombreProveedor = RsTmp!nombre_empresa
      End If

End Sub
