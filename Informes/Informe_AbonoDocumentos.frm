VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form Informe_AbonoDocumentos 
   Appearance      =   0  'Flat
   BackColor       =   &H80000005&
   Caption         =   "Informe de abono a Documentos"
   ClientHeight    =   8085
   ClientLeft      =   135
   ClientTop       =   2085
   ClientWidth     =   14700
   LinkTopic       =   "Form1"
   ScaleHeight     =   8085
   ScaleWidth      =   14700
   Begin VB.Frame Frame5 
      Caption         =   "Frame5"
      Height          =   5145
      Left            =   165
      TabIndex        =   14
      Top             =   2385
      Width           =   14340
      Begin MSComctlLib.ListView LvDetalle 
         Height          =   4530
         Left            =   300
         TabIndex        =   15
         ToolTipText     =   "Ultimos Rut con movimientos"
         Top             =   435
         Width           =   13830
         _ExtentX        =   24395
         _ExtentY        =   7990
         View            =   3
         LabelEdit       =   1
         MultiSelect     =   -1  'True
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   11
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "T1200"
            Text            =   "Abo Id"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "F1000"
            Text            =   "Fecha"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   2
            Object.Tag             =   "N100"
            Text            =   "Monto"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Object.Tag             =   "T1500"
            Text            =   "Nro Comprobante"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Object.Tag             =   "T1000"
            Text            =   "Usuario"
            Object.Width           =   2646
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Object.Tag             =   "T1500"
            Text            =   "Forma pago"
            Object.Width           =   4762
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   6
            Object.Tag             =   "N100"
            Text            =   "Valor"
            Object.Width           =   2646
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   7
            Object.Tag             =   "T2000"
            Text            =   "Nro Transaccio"
            Object.Width           =   2469
         EndProperty
         BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   8
            Object.Tag             =   "T1500"
            Text            =   "Tipo Abono"
            Object.Width           =   2646
         EndProperty
         BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   9
            Object.Tag             =   "N109"
            Text            =   "Id forma pago"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   10
            Text            =   "Nro Comprobante"
            Object.Width           =   2646
         EndProperty
      End
   End
   Begin VB.CommandButton cmdSalir 
      Cancel          =   -1  'True
      Caption         =   "&Retornar"
      Height          =   405
      Left            =   13125
      TabIndex        =   9
      Top             =   7560
      Width           =   1350
   End
   Begin VB.Frame Frame1 
      Caption         =   "Filtro"
      Height          =   1590
      Left            =   225
      TabIndex        =   0
      Top             =   510
      Width           =   14280
      Begin VB.Frame FrameRut 
         Caption         =   "Rut a Consultar"
         Enabled         =   0   'False
         Height          =   1140
         Left            =   2565
         TabIndex        =   10
         Top             =   330
         Width           =   4035
         Begin VB.TextBox TxtRut 
            BackColor       =   &H0080FF80&
            Enabled         =   0   'False
            Height          =   315
            Left            =   870
            TabIndex        =   13
            ToolTipText     =   "Ingrese el RUT sin puntos ni guion."
            Top             =   360
            Width           =   1560
         End
         Begin VB.TextBox txtCliente 
            BackColor       =   &H00E0E0E0&
            Height          =   315
            Left            =   270
            Locked          =   -1  'True
            TabIndex        =   12
            Top             =   675
            Width           =   3570
         End
         Begin VB.CommandButton cmdBtnRut 
            Caption         =   "Rut"
            Enabled         =   0   'False
            Height          =   225
            Left            =   240
            TabIndex        =   11
            Top             =   390
            Width           =   585
         End
      End
      Begin VB.CommandButton CmdOk 
         Caption         =   "Consultar"
         Height          =   420
         Left            =   12930
         TabIndex        =   8
         Top             =   1005
         Width           =   1230
      End
      Begin VB.Frame Frame3 
         Caption         =   "Documento"
         Height          =   1140
         Left            =   6570
         TabIndex        =   4
         Top             =   315
         Width           =   6225
         Begin VB.TextBox TxtSaldo 
            Alignment       =   1  'Right Justify
            BackColor       =   &H0080FF80&
            Height          =   285
            Left            =   4755
            Locked          =   -1  'True
            TabIndex        =   20
            Text            =   "0"
            Top             =   780
            Width           =   1395
         End
         Begin VB.TextBox TxtAbonos 
            Alignment       =   1  'Right Justify
            BackColor       =   &H0080FF80&
            Height          =   285
            Left            =   4755
            Locked          =   -1  'True
            TabIndex        =   18
            Text            =   "0"
            Top             =   495
            Width           =   1395
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
            Height          =   210
            Left            =   3705
            OleObjectBlob   =   "Informe_AbonoDocumentos.frx":0000
            TabIndex        =   17
            Top             =   240
            Width           =   960
         End
         Begin VB.TextBox TxtValor 
            Alignment       =   1  'Right Justify
            BackColor       =   &H0080FF80&
            Height          =   285
            Left            =   4755
            Locked          =   -1  'True
            TabIndex        =   16
            Text            =   "0"
            Top             =   210
            Width           =   1395
         End
         Begin VB.TextBox txtNumero 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   1890
            TabIndex        =   7
            Top             =   720
            Width           =   1335
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
            Height          =   270
            Left            =   165
            OleObjectBlob   =   "Informe_AbonoDocumentos.frx":0072
            TabIndex        =   6
            Top             =   735
            Width           =   1710
         End
         Begin VB.ComboBox CboDoc 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   300
            Style           =   2  'Dropdown List
            TabIndex        =   5
            Top             =   390
            Width           =   2940
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
            Height          =   210
            Left            =   3675
            OleObjectBlob   =   "Informe_AbonoDocumentos.frx":00F0
            TabIndex        =   19
            Top             =   510
            Width           =   960
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
            Height          =   210
            Left            =   3705
            OleObjectBlob   =   "Informe_AbonoDocumentos.frx":015A
            TabIndex        =   21
            Top             =   810
            Width           =   960
         End
      End
      Begin VB.Frame Frame2 
         Caption         =   "Seleccione"
         Height          =   1140
         Left            =   195
         TabIndex        =   1
         Top             =   330
         Width           =   2385
         Begin VB.OptionButton Option2 
            Caption         =   "Proveedores"
            Height          =   195
            Left            =   390
            TabIndex        =   3
            Top             =   780
            Width           =   1530
         End
         Begin VB.OptionButton Option1 
            Caption         =   "Clientes"
            Height          =   195
            Left            =   390
            TabIndex        =   2
            Top             =   330
            Value           =   -1  'True
            Width           =   1395
         End
      End
   End
   Begin VB.Timer Timer1 
      Interval        =   10
      Left            =   960
      Top             =   150
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   0
      OleObjectBlob   =   "Informe_AbonoDocumentos.frx":01C2
      Top             =   0
   End
End
Attribute VB_Name = "Informe_AbonoDocumentos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim Sp_CliPro As String, Lp_Id As Long
Private Sub CboDoc_Validate(Cancel As Boolean)
    VerificaDc
End Sub

Private Sub cmdBtnRut_Click()
    ClienteEncontrado = False
    SG_codigo = Empty
    BuscaProveedor.Show 1
    TxtRut = SG_codigo
    If SG_codigo <> Empty Then
        TxtRut_Validate (True)
    End If
End Sub



Private Sub CmdOk_Click()
    Dim Ic As Integer
    If CboDoc.ListIndex = -1 Then
        MsgBox "Seleccione documento..."
        CboDoc.SetFocus
        Exit Sub
    ElseIf Val(txtNumero) = 0 Then
        MsgBox "Falta numero documento..."
        txtNumero.SetFocus
        Exit Sub
    End If
    

    LvDetalle.ListItems.Clear
    Sql = "SELECT a.abo_id abono,abo_fecha_pago,abo_monto,abo_nro_comprobante,usu_nombre " & _
                        "FROM cta_abonos a " & _
                        "INNER JOIN cta_abono_documentos d USING(abo_id) " & _
                        "WHERE d.rut_emp='" & SP_Rut_Activo & "' AND abo_cli_pro='" & Sp_CliPro & "' AND d.id=" & Lp_Id
    Consulta RsTmp, Sql
    If RsTmp.RecordCount = 0 Then Exit Sub
    With RsTmp
        RsTmp.MoveFirst
        Do While Not RsTmp.EOF
            LvDetalle.ListItems.Add , , !abono
            Ic = LvDetalle.ListItems.Count
            LvDetalle.ListItems(Ic).SubItems(1) = "" & !abo_fecha_pago
            LvDetalle.ListItems(Ic).SubItems(2) = NumFormat(!abo_monto)
            LvDetalle.ListItems(Ic).SubItems(3) = !abo_nro_comprobante
            LvDetalle.ListItems(Ic).SubItems(4) = !usu_nombre
            For i = 1 To 4
                LvDetalle.ListItems(Ic).ListSubItems(i).Bold = True
            Next
            
            Sql = "SELECT abo_id abono,mpa_id,m.mpa_nombre,t.pad_valor,t.pad_nro_transaccion,t.pad_tipo_deposito " & _
                    "FROM abo_tipos_de_pagos t " & _
                    "INNER JOIN par_medios_de_pago m USING(mpa_id) " & _
                    "INNER JOIN cta_abono_documentos c USING(abo_id) " & _
                    "WHERE c.id=" & Lp_Id & " AND t.abo_id=" & !abono
            Consulta RsTmp2, Sql
            If RsTmp2.RecordCount > 0 Then
                RsTmp2.MoveFirst
                Do While Not RsTmp2.EOF
                    LvDetalle.ListItems.Add , , !abono
                    Ic = LvDetalle.ListItems.Count
                    LvDetalle.ListItems(Ic).SubItems(1) = ""
                    LvDetalle.ListItems(Ic).SubItems(2) = ""
                    LvDetalle.ListItems(Ic).SubItems(3) = !abo_nro_comprobante
                    LvDetalle.ListItems(Ic).SubItems(4) = ""
                    LvDetalle.ListItems(Ic).SubItems(5) = RsTmp2!mpa_nombre
                    LvDetalle.ListItems(Ic).SubItems(6) = NumFormat(RsTmp2!pad_valor)
                    LvDetalle.ListItems(Ic).SubItems(7) = RsTmp2!pad_nro_transaccion
                    LvDetalle.ListItems(Ic).SubItems(8) = "" & RsTmp2!pad_tipo_deposito
                    RsTmp2.MoveNext
                Loop
            End If
            .MoveNext
        Loop
    End With
    
   
    
    
End Sub

Private Sub CmdSalir_Click()
    Unload Me
End Sub

Private Sub Command1_Click()

End Sub

Private Sub Form_Load()
    Aplicar_skin Me
    Centrar Me
    LLenarCombo Me.CboDoc, "doc_nombre", "doc_id", "sis_documentos", "doc_activo='SI' AND doc_documento='VENTA'", "doc_id"

End Sub

Private Sub LvDetalle_Click()
    Dim Ip_A As Long
    
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    
    Ip_A = LvDetalle.SelectedItem
    For i = 1 To LvDetalle.ListItems.Count
        LvDetalle.ListItems(i).Selected = False
    
    Next
    
    For i = 1 To LvDetalle.ListItems.Count
        If Val(LvDetalle.ListItems(i)) = Ip_A Then LvDetalle.ListItems(i).Selected = True
    
    Next


End Sub


Private Sub Option1_Click()
    If Option1 Then
        cmdBtnRut.Enabled = False
        TxtRut.Enabled = False
        FrameRut.Enabled = False
        CboDoc.SetFocus
        LLenarCombo Me.CboDoc, "doc_nombre", "doc_id", "sis_documentos", "doc_activo='SI' AND doc_documento='VENTA'", "doc_id"
    End If
    
        
End Sub

Private Sub Option2_Click()
    If Option2 Then
        cmdBtnRut.Enabled = True
        TxtRut.Enabled = True
        LLenarCombo Me.CboDoc, "doc_nombre", "doc_id", "sis_documentos", "doc_activo='SI' AND doc_documento='COMPRA'", "doc_id"
        FrameRut.Enabled = True
        TxtRut.SetFocus
    End If
End Sub

Private Sub Timer1_Timer()
    On Error Resume Next
    Option1.SetFocus
    Timer1.Enabled = False
End Sub

Private Sub txtNumero_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
    If KeyAscii = 39 Then KeyAscii = 0
End Sub
Private Sub VerificaDc()
    
    TxtValor = 0
    TxtAbonos = 0
    TxtSaldo = 0
    
    If CboDoc.ListIndex = -1 Then Exit Sub
    If Val(txtNumero) = 0 Then Exit Sub
    If Option1 Then
        TxtRut = ""
        txtCliente = ""
        Sp_CliPro = "CLI"
        Sql = "SELECT id as idn,bruto monto,rut_cliente,nombre_cliente " & _
             "FROM ven_doc_venta " & _
             "WHERE rut_emp='" & SP_Rut_Activo & "' " & _
             "AND doc_id=" & CboDoc.ItemData(CboDoc.ListIndex) & " AND no_documento=" & Val(txtNumero)
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
            Lp_Id = RsTmp!idn
            TxtRut = RsTmp!rut_cliente
            txtCliente = RsTmp!nombre_cliente
        Else
            MsgBox "Documento no ha sido encontrado..."
            txtNumero.SetFocus
            Exit Sub
        End If
    End If
    If Option2 Then
        Sp_CliPro = "PRO"
        Sql = "SELECT id as idn,total monto " & _
             "FROM com_doc_compra " & _
             "WHERE com_informa_compra='SI' AND rut_emp='" & SP_Rut_Activo & "' " & _
             "AND doc_id=" & CboDoc.ItemData(CboDoc.ListIndex) & " AND no_documento=" & Val(txtNumero)
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
            Lp_Id = RsTmp!idn
        Else
            MsgBox "Documento no ha sido encontrado..."
            txtNumero.SetFocus
            Exit Sub
        End If
    End If
    TxtValor = NumFormat(RsTmp!monto)
    
    Sql = "SELECT SUM(d.ctd_monto) abonos," & RsTmp!monto & "-  SUM(d.ctd_monto) saldo " & _
            "FROM cta_abono_documentos d " & _
            "INNER JOIN cta_abonos a USING(abo_id) " & _
            "WHERE d.rut_emp='" & SP_Rut_Activo & "' AND a.abo_cli_pro='" & Sp_CliPro & "' AND id=" & Lp_Id
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        TxtAbonos = NumFormat(RsTmp!abonos)
        TxtSaldo = NumFormat(RsTmp!saldo)
    
    End If

End Sub


Private Sub txtNumero_Validate(Cancel As Boolean)
    VerificaDc
End Sub

Private Sub TxtRut_Validate(Cancel As Boolean)
    
    If Len(TxtRut) = 0 Then Exit Sub
    
    
    Sql = "SELECT nombre_empresa nombre " & _
            "FROM maestro_proveedores " & _
            "WHERE rut_proveedor='" & TxtRut & "'" ' " & _
            "AND rut_emp='" & SP_Rut_Activo & "'"
            
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        txtCliente = RsTmp!nombre
    Else
        MsgBox "No se encontro proveedor..."
        txtCliente = ""
        
    End If
                    
        
End Sub




