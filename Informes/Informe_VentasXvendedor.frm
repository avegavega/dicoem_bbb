VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{DA729E34-689F-49EA-A856-B57046630B73}#1.0#0"; "progressbar-xp.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form Informe_VentasXvendedor 
   Caption         =   "Resumen de Ventas por Vendedor"
   ClientHeight    =   8265
   ClientLeft      =   1425
   ClientTop       =   2040
   ClientWidth     =   12045
   LinkTopic       =   "Form1"
   ScaleHeight     =   8265
   ScaleWidth      =   12045
   Begin VB.Frame Frame1 
      Caption         =   "Progreso exportación"
      Height          =   795
      Left            =   270
      TabIndex        =   2
      Top             =   3915
      Visible         =   0   'False
      Width           =   11430
      Begin Proyecto2.XP_ProgressBar BarraProgreso 
         Height          =   330
         Left            =   150
         TabIndex        =   3
         Top             =   315
         Width           =   11130
         _ExtentX        =   19632
         _ExtentY        =   582
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BrushStyle      =   0
         Color           =   16777088
         Scrolling       =   1
         ShowText        =   -1  'True
      End
   End
   Begin VB.Frame Frame3 
      Caption         =   "Detalle"
      Height          =   5625
      Left            =   420
      TabIndex        =   12
      Top             =   1740
      Width           =   11220
      Begin VB.TextBox txtComision 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   9345
         Locked          =   -1  'True
         TabIndex        =   17
         TabStop         =   0   'False
         Tag             =   "N"
         Text            =   "0"
         Top             =   5175
         Width           =   1400
      End
      Begin VB.TextBox TxtAfecto 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   6870
         Locked          =   -1  'True
         TabIndex        =   16
         TabStop         =   0   'False
         Tag             =   "N"
         Text            =   "0"
         Top             =   5190
         Width           =   1400
      End
      Begin VB.TextBox txtNC 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   5475
         Locked          =   -1  'True
         TabIndex        =   15
         TabStop         =   0   'False
         Tag             =   "N"
         Text            =   "0"
         Top             =   5190
         Width           =   1400
      End
      Begin VB.TextBox TxtNeto 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   4095
         Locked          =   -1  'True
         TabIndex        =   14
         TabStop         =   0   'False
         Tag             =   "N"
         Text            =   "0"
         Top             =   5190
         Width           =   1400
      End
      Begin MSComctlLib.ListView LvDetalle 
         Height          =   4845
         Left            =   165
         TabIndex        =   13
         Top             =   270
         Width           =   10935
         _ExtentX        =   19288
         _ExtentY        =   8546
         View            =   3
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   7
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Id"
            Object.Width           =   1058
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "T3000"
            Text            =   "Nombre"
            Object.Width           =   5821
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   2
            Key             =   "neto"
            Object.Tag             =   "N100"
            Text            =   "Ventas Netas"
            Object.Width           =   2469
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   3
            Key             =   "nc"
            Object.Tag             =   "N100"
            Text            =   "Notas Cdto."
            Object.Width           =   2469
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   4
            Key             =   "afecto"
            Object.Tag             =   "N100"
            Text            =   "Afecto Comision"
            Object.Width           =   2469
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   2
            SubItemIndex    =   5
            Object.Tag             =   "T1000"
            Text            =   "Factor %"
            Object.Width           =   1940
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   6
            Key             =   "comision"
            Object.Tag             =   "N100"
            Text            =   "Comision"
            Object.Width           =   2469
         EndProperty
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Filtro"
      Height          =   1305
      Left            =   435
      TabIndex        =   4
      Top             =   300
      Width           =   11190
      Begin VB.OptionButton Option1 
         Caption         =   "Todas las fechas"
         Height          =   240
         Left            =   165
         TabIndex        =   7
         Top             =   345
         Value           =   -1  'True
         Width           =   1560
      End
      Begin VB.OptionButton Option2 
         Caption         =   "Seleccionar fechas"
         Height          =   240
         Left            =   2820
         TabIndex        =   6
         Top             =   330
         Width           =   1725
      End
      Begin VB.CommandButton cmdConsultar 
         Caption         =   "&Consultar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   420
         Left            =   8670
         TabIndex        =   5
         Top             =   720
         Width           =   1365
      End
      Begin MSComCtl2.DTPicker DTInicio 
         Height          =   300
         Left            =   2655
         TabIndex        =   8
         Top             =   870
         Width           =   1305
         _ExtentX        =   2302
         _ExtentY        =   529
         _Version        =   393216
         Enabled         =   0   'False
         Format          =   92078081
         CurrentDate     =   40628
      End
      Begin ACTIVESKINLibCtl.SkinLabel skIni 
         Height          =   240
         Left            =   2670
         OleObjectBlob   =   "Informe_VentasXvendedor.frx":0000
         TabIndex        =   9
         Top             =   645
         Width           =   795
      End
      Begin MSComCtl2.DTPicker DtHasta 
         Height          =   285
         Left            =   4050
         TabIndex        =   10
         Top             =   870
         Width           =   1305
         _ExtentX        =   2302
         _ExtentY        =   503
         _Version        =   393216
         Enabled         =   0   'False
         Format          =   92078081
         CurrentDate     =   40628
      End
      Begin ACTIVESKINLibCtl.SkinLabel skFin 
         Height          =   210
         Left            =   4050
         OleObjectBlob   =   "Informe_VentasXvendedor.frx":0068
         TabIndex        =   11
         Top             =   675
         Width           =   795
      End
   End
   Begin VB.CommandButton cmdSalir 
      Cancel          =   -1  'True
      Caption         =   "&Retornar"
      Height          =   405
      Left            =   10260
      TabIndex        =   1
      Top             =   7515
      Width           =   1350
   End
   Begin VB.CommandButton CmdExportar 
      Caption         =   "Exportar a Excel"
      Height          =   315
      Left            =   405
      TabIndex        =   0
      Top             =   7440
      Width           =   1290
   End
   Begin VB.Timer Timer1 
      Interval        =   10
      Left            =   585
      Top             =   90
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   0
      OleObjectBlob   =   "Informe_VentasXvendedor.frx":00D0
      Top             =   0
   End
End
Attribute VB_Name = "Informe_VentasXvendedor"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim sm_FiltroFecha As String
Private Sub cmdConsultar_Click()
    If Option1.Value Then
        sm_FiltroFecha = Empty
    Else
         sm_FiltroFecha = " AND (fecha BETWEEN '" & Format(DTInicio.Value, "YYYY-MM-DD") & "' AND '" & Format(DtHasta.Value, "YYYY-MM-DD") & "') "
    End If
    
    Carga
End Sub

Private Sub CmdExportar_Click()
    Dim tit(2) As String
    If LvDetalle.ListItems.Count = 0 Then Exit Sub
   Me.Frame1.Visible = True
    tit(0) = Me.Caption & " " & DtFecha & " - " & Time
    tit(1) = ""
    tit(2) = ""
    ExportarNuevo LvDetalle, tit, Me, BarraProgreso
       Me.Frame1.Visible = False
End Sub

Private Sub CmdSalir_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    Centrar Me
    Aplicar_skin Me
    sm_FiltroFecha = Empty
    DTInicio = Date
    DtHasta = Date
    Carga
End Sub

Private Sub Option1_Click()
    DTInicio.Enabled = False
    DtHasta.Enabled = False
End Sub

Private Sub Option2_Click()
    DTInicio.Enabled = True
    DtHasta.Enabled = True
End Sub

Private Sub Timer1_Timer()
    On Error Resume Next
    LvDetalle.SetFocus
    Timer1.Enabled = False
End Sub
Private Sub Carga()
    Sql = "SELECT v.ven_id,v.ven_nombre, " & _
            "SUM(IF(d.doc_signo_libro='+',neto,0)) ventas_netas," & _
            "SUM(IF(d.doc_signo_libro='-',neto,0)) notas_credito," & _
            "SUM(IF(d.doc_signo_libro='+',neto,0))-SUM(IF(d.doc_signo_libro='-',neto,0)) afecto_comision, " & _
            "/*CONCAT(CONVERT(*/p.ven_comision /*,CHAR(5)),'%')*/ factor /*SUM(v.ven_comision) comision */ " & _
            "   " & _
            "FROM par_vendedores p " & _
            "INNER JOIN ven_doc_venta v USING(ven_id) " & _
            "INNER JOIN sis_documentos d USING(doc_id) " & _
            "WHERE  doc_nota_de_venta='NO' AND doc_signo_libro<>'$' AND v.rut_emp='" & SP_Rut_Activo & "' AND  p.ven_activo='SI' " & sm_FiltroFecha & _
            "GROUP BY v.ven_id"
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvDetalle, False, True, True, False
    TxtNeto = Format(TotalizaColumna(LvDetalle, "neto"), "#,##0")
    txtNC = Format(TotalizaColumna(LvDetalle, "nc"), "#,##0")
    TxtAfecto = Format(TotalizaColumna(LvDetalle, "afecto"), "#,##0")
    txtComision = Format(TotalizaColumna(LvDetalle, "comision"), "#,##0")
    
    For i = 1 To LvDetalle.ListItems.Count
        LvDetalle.ListItems(i).SubItems(6) = NumFormat(CDbl(LvDetalle.ListItems(i).SubItems(4)) / 100 * CDbl(LvDetalle.ListItems(i).SubItems(5)))
        
    Next
End Sub
