VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{DA729E34-689F-49EA-A856-B57046630B73}#1.0#0"; "progressbar-xp.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form Informe_GuiasSinFacturar 
   Caption         =   "Informe Guias Sin  Facturar"
   ClientHeight    =   8475
   ClientLeft      =   3750
   ClientTop       =   1950
   ClientWidth     =   13095
   ClipControls    =   0   'False
   LinkTopic       =   "Form1"
   ScaleHeight     =   8475
   ScaleWidth      =   13095
   Begin VB.CommandButton CmdExportar 
      Caption         =   "Exportar a Excel"
      Height          =   315
      Left            =   525
      TabIndex        =   9
      Top             =   7095
      Width           =   1290
   End
   Begin VB.Frame Frame1 
      Caption         =   "Progreso exportación"
      Height          =   720
      Left            =   270
      TabIndex        =   7
      Top             =   7500
      Visible         =   0   'False
      Width           =   11430
      Begin Proyecto2.XP_ProgressBar BarraProgreso 
         Height          =   330
         Left            =   150
         TabIndex        =   8
         Top             =   315
         Width           =   11130
         _ExtentX        =   19632
         _ExtentY        =   582
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BrushStyle      =   0
         Color           =   16777088
         Scrolling       =   1
         ShowText        =   -1  'True
      End
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   720
      OleObjectBlob   =   "Informe_GuiasSinFacturar.frx":0000
      Top             =   6960
   End
   Begin VB.Timer Timer1 
      Interval        =   10
      Left            =   255
      Top             =   6915
   End
   Begin VB.CommandButton CmdSalir 
      Cancel          =   -1  'True
      Caption         =   "&Retornar"
      Height          =   375
      Left            =   11535
      TabIndex        =   6
      Top             =   7755
      Width           =   1095
   End
   Begin VB.Frame Frame2 
      Caption         =   "Guias pendientes de facturacion"
      Height          =   6855
      Left            =   420
      TabIndex        =   0
      Top             =   690
      Width           =   12405
      Begin VB.TextBox TxtTotal 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         Height          =   315
         Left            =   10725
         Locked          =   -1  'True
         TabIndex        =   3
         TabStop         =   0   'False
         Tag             =   "N"
         Text            =   "0"
         Top             =   6345
         Width           =   1185
      End
      Begin VB.TextBox TxtIVA 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         Height          =   315
         Left            =   9540
         Locked          =   -1  'True
         TabIndex        =   2
         TabStop         =   0   'False
         Tag             =   "N"
         Text            =   "0"
         Top             =   6345
         Width           =   1185
      End
      Begin VB.TextBox txtNeto 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         Height          =   315
         Left            =   8355
         Locked          =   -1  'True
         TabIndex        =   1
         TabStop         =   0   'False
         Tag             =   "N"
         Text            =   "0"
         Top             =   6345
         Width           =   1185
      End
      Begin MSComctlLib.ListView LvDetalle 
         Height          =   5835
         Left            =   270
         TabIndex        =   4
         Top             =   390
         Width           =   11835
         _ExtentX        =   20876
         _ExtentY        =   10292
         View            =   3
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   8
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "T1000"
            Object.Width           =   529
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "F1300"
            Text            =   "Fecha"
            Object.Width           =   2293
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "N109"
            Text            =   "Nro Guia"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Object.Tag             =   "T800"
            Text            =   "Rut"
            Object.Width           =   2469
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Object.Tag             =   "T3000"
            Text            =   "Nombre Proveedor"
            Object.Width           =   6703
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   5
            Key             =   "neto"
            Object.Tag             =   "N100"
            Text            =   "Neto"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   6
            Key             =   "iva"
            Object.Tag             =   "N100"
            Text            =   "IVA"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   7
            Key             =   "total"
            Object.Tag             =   "N100"
            Text            =   "Total"
            Object.Width           =   2117
         EndProperty
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel14 
         Height          =   180
         Index           =   4
         Left            =   6390
         OleObjectBlob   =   "Informe_GuiasSinFacturar.frx":0234
         TabIndex        =   5
         Top             =   6375
         Width           =   1860
      End
   End
End
Attribute VB_Name = "Informe_GuiasSinFacturar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub CmdExportar_Click()
    Dim tit(2) As String
    If LvDetalle.ListItems.Count = 0 Then Exit Sub
    Me.Frame1.Visible = True
    tit(0) = Me.Caption
    tit(1) = DtFecha
    tit(2) = Time
    ExportarNuevo LvDetalle, tit, Me, BarraProgreso
    Me.Frame1.Visible = False
End Sub


Private Sub CmdSalir_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    Aplicar_skin Me
    Centrar Me
    Sql = "SELECT men_infoextra " & _
          "FROM sis_menu " & _
          "WHERE men_id=" & IP_IDMenu
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        Sp_extra = RsTmp!men_infoextra
    End If
    
    If Sp_extra = "CLIENTES" Then
        'Frame1.Caption = "CLIENTE"
        Me.Caption = "INFORME GUIAS SIN FACTURAR A CLIENTES"
        Sql = "SELECT id,fecha,no_documento,rut_cliente,nombre_cliente,neto,iva,bruto " & _
                "FROM ven_doc_venta v " & _
                "WHERE rut_emp = '" & SP_Rut_Activo & "' AND doc_id IN(" & IG_id_GuiaClientes & ") AND nro_factura=0 AND rut_cliente<>'NULO' " & _
                "ORDER BY rut_cliente,fecha "
        
    Else
       ' Frame1.Caption = "PROVEEDOR"
        Me.Caption = "GUIAS SIN FACTURAR DE PROVEEDORES"
        Sql = "SELECT id,fecha,no_documento,rut,nombre_proveedor,neto,iva,total " & _
                "FROM com_doc_compra " & _
                "WHERE com_informa_compra='SI' AND rut_emp='" & SP_Rut_Activo & "'  AND doc_id=" & IG_id_GuiaProveedor & " AND nro_factura=0 " & _
                "ORDER BY rut,fecha "
        
    End If
    Consulta RsTmp2, Sql
    LLenar_Grilla RsTmp2, Me, LvDetalle, False, True, True, False
    TxtNeto = NumFormat(TotalizaColumna(LvDetalle, "neto"))
    TxtIva = NumFormat(TotalizaColumna(LvDetalle, "iva"))
    txtTotal = NumFormat(TotalizaColumna(LvDetalle, "total"))
    
End Sub

Private Sub Timer1_Timer()
    On Error Resume Next
    LvDetalle.SetFocus
    Timer1.Enabled = False
End Sub
