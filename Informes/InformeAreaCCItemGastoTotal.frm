VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{DA729E34-689F-49EA-A856-B57046630B73}#1.0#0"; "progressbar-xp.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form InformeAreaCCItemGastoTotal 
   Caption         =   "Informe Area, Centro Costo, Item Gasto Resumen"
   ClientHeight    =   7155
   ClientLeft      =   6270
   ClientTop       =   3750
   ClientWidth     =   13665
   LinkTopic       =   "Form1"
   ScaleHeight     =   7155
   ScaleWidth      =   13665
   Begin VB.Frame FraProgreso 
      Height          =   540
      Left            =   1425
      TabIndex        =   32
      Top             =   6360
      Visible         =   0   'False
      Width           =   8835
      Begin Proyecto2.XP_ProgressBar pbar 
         Height          =   255
         Left            =   135
         TabIndex        =   33
         Top             =   195
         Width           =   8565
         _ExtentX        =   15108
         _ExtentY        =   450
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BrushStyle      =   0
         Color           =   16777088
         Scrolling       =   1
         ShowText        =   -1  'True
      End
   End
   Begin VB.CommandButton CmdExportar 
      Caption         =   "&Exportar"
      Height          =   375
      Left            =   300
      TabIndex        =   25
      Top             =   6480
      Width           =   1095
   End
   Begin VB.CommandButton CmdSalir 
      Cancel          =   -1  'True
      Caption         =   "&Retornar"
      Height          =   375
      Left            =   12135
      TabIndex        =   24
      Top             =   6420
      Width           =   1095
   End
   Begin VB.Frame Frame6 
      Caption         =   "Resumen Items de Gastos"
      Height          =   4410
      Left            =   8880
      TabIndex        =   22
      Top             =   1890
      Width           =   4350
      Begin VB.TextBox TxtGasto 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2595
         Locked          =   -1  'True
         TabIndex        =   30
         TabStop         =   0   'False
         Tag             =   "N"
         Text            =   "0"
         Top             =   4020
         Width           =   1215
      End
      Begin MSComctlLib.ListView LvGastos 
         Height          =   3645
         Left            =   150
         TabIndex        =   23
         Top             =   240
         Width           =   3990
         _ExtentX        =   7038
         _ExtentY        =   6429
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   2
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "T1000"
            Text            =   "Item de Gastos"
            Object.Width           =   4762
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   1
            Key             =   "total"
            Object.Tag             =   "N100"
            Text            =   "Total"
            Object.Width           =   2028
         EndProperty
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel5 
         Height          =   195
         Left            =   1485
         OleObjectBlob   =   "InformeAreaCCItemGastoTotal.frx":0000
         TabIndex        =   31
         Top             =   4080
         Width           =   1080
      End
   End
   Begin VB.Frame Frame5 
      Caption         =   "Resumen Centro Costos"
      Height          =   4410
      Left            =   4635
      TabIndex        =   20
      Top             =   1890
      Width           =   4290
      Begin VB.TextBox txtCC 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2625
         Locked          =   -1  'True
         TabIndex        =   28
         TabStop         =   0   'False
         Tag             =   "N"
         Text            =   "0"
         Top             =   4020
         Width           =   1215
      End
      Begin MSComctlLib.ListView LvCC 
         Height          =   3660
         Left            =   105
         TabIndex        =   21
         Top             =   225
         Width           =   4005
         _ExtentX        =   7064
         _ExtentY        =   6456
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   2
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "T1000"
            Text            =   "Centro Costo"
            Object.Width           =   4762
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   1
            Key             =   "total"
            Object.Tag             =   "N100"
            Text            =   "Total"
            Object.Width           =   2028
         EndProperty
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
         Height          =   195
         Left            =   1515
         OleObjectBlob   =   "InformeAreaCCItemGastoTotal.frx":0068
         TabIndex        =   29
         Top             =   4080
         Width           =   1080
      End
   End
   Begin VB.Frame Frame3 
      Caption         =   "Resumen Areas"
      Height          =   4410
      Left            =   285
      TabIndex        =   18
      Top             =   1890
      Width           =   4410
      Begin VB.TextBox TxtTotalArea 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2685
         Locked          =   -1  'True
         TabIndex        =   27
         TabStop         =   0   'False
         Tag             =   "N"
         Text            =   "0"
         Top             =   4005
         Width           =   1215
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   195
         Left            =   1575
         OleObjectBlob   =   "InformeAreaCCItemGastoTotal.frx":00D0
         TabIndex        =   26
         Top             =   4065
         Width           =   1080
      End
      Begin MSComctlLib.ListView LvAreas 
         Height          =   3675
         Left            =   225
         TabIndex        =   19
         Top             =   240
         Width           =   4005
         _ExtentX        =   7064
         _ExtentY        =   6482
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   2
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "T1000"
            Text            =   "Area"
            Object.Width           =   4762
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   1
            Key             =   "total"
            Object.Tag             =   "N100"
            Text            =   "Total"
            Object.Width           =   2028
         EndProperty
      End
   End
   Begin VB.Frame Frame4 
      Caption         =   "Informacion"
      Height          =   1680
      Left            =   285
      TabIndex        =   0
      Top             =   150
      Width           =   12930
      Begin VB.Frame Frame2 
         Caption         =   "Fechas"
         Height          =   1005
         Left            =   9435
         TabIndex        =   13
         Top             =   555
         Width           =   2415
         Begin ACTIVESKINLibCtl.SkinLabel skIni 
            Height          =   240
            Left            =   120
            OleObjectBlob   =   "InformeAreaCCItemGastoTotal.frx":0138
            TabIndex        =   14
            Top             =   270
            Width           =   795
         End
         Begin MSComCtl2.DTPicker DTInicio 
            Height          =   300
            Left            =   960
            TabIndex        =   15
            Top             =   255
            Width           =   1305
            _ExtentX        =   2302
            _ExtentY        =   529
            _Version        =   393216
            Format          =   95354881
            CurrentDate     =   40628
         End
         Begin MSComCtl2.DTPicker DtHasta 
            Height          =   285
            Left            =   960
            TabIndex        =   16
            Top             =   555
            Width           =   1305
            _ExtentX        =   2302
            _ExtentY        =   503
            _Version        =   393216
            Format          =   95354881
            CurrentDate     =   40628
         End
         Begin ACTIVESKINLibCtl.SkinLabel skFin 
            Height          =   240
            Left            =   120
            OleObjectBlob   =   "InformeAreaCCItemGastoTotal.frx":01A0
            TabIndex        =   17
            Top             =   540
            Width           =   795
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "Documento"
         Height          =   1005
         Left            =   210
         TabIndex        =   4
         Top             =   555
         Width           =   9270
         Begin VB.TextBox TxtNDoc 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   2640
            TabIndex        =   9
            Tag             =   "N"
            Text            =   "0"
            Top             =   495
            Width           =   1215
         End
         Begin VB.TextBox TxtRsocial 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   5055
            Locked          =   -1  'True
            TabIndex        =   8
            TabStop         =   0   'False
            Top             =   495
            Width           =   4140
         End
         Begin VB.TextBox TxtRutProveedor 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   3840
            TabIndex        =   7
            Tag             =   "T"
            Top             =   495
            Width           =   1230
         End
         Begin VB.CommandButton CmdBuscaRut 
            Caption         =   "RUT"
            Height          =   240
            Left            =   3825
            TabIndex        =   6
            Top             =   285
            Width           =   1215
         End
         Begin VB.ComboBox CboTipoDoc 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            ItemData        =   "InformeAreaCCItemGastoTotal.frx":0208
            Left            =   225
            List            =   "InformeAreaCCItemGastoTotal.frx":0215
            Style           =   2  'Dropdown List
            TabIndex        =   5
            Top             =   495
            Width           =   2430
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel10 
            Height          =   225
            Left            =   5085
            OleObjectBlob   =   "InformeAreaCCItemGastoTotal.frx":023A
            TabIndex        =   10
            Top             =   285
            Width           =   3180
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
            Height          =   225
            Index           =   0
            Left            =   2505
            OleObjectBlob   =   "InformeAreaCCItemGastoTotal.frx":02A9
            TabIndex        =   11
            Top             =   285
            Width           =   1290
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
            Height          =   225
            Index           =   0
            Left            =   210
            OleObjectBlob   =   "InformeAreaCCItemGastoTotal.frx":031A
            TabIndex        =   12
            Top             =   285
            Width           =   1575
         End
      End
      Begin VB.CommandButton CmdBuscar 
         Caption         =   "Buscar"
         Height          =   345
         Left            =   12135
         TabIndex        =   3
         Top             =   1095
         Width           =   765
      End
      Begin VB.OptionButton Option2 
         Caption         =   "Por documento"
         Height          =   240
         Left            =   330
         TabIndex        =   2
         Top             =   300
         Value           =   -1  'True
         Width           =   1725
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Por Fecha"
         Height          =   240
         Left            =   9555
         TabIndex        =   1
         Top             =   270
         Width           =   1740
      End
   End
   Begin VB.Timer Timer1 
      Interval        =   10
      Left            =   105
      Top             =   7755
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   150
      OleObjectBlob   =   "InformeAreaCCItemGastoTotal.frx":0393
      Top             =   8205
   End
   Begin MSComctlLib.ListView LvDetalle 
      Height          =   2355
      Left            =   255
      TabIndex        =   34
      Top             =   8460
      Width           =   11730
      _ExtentX        =   20690
      _ExtentY        =   4154
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   0
      NumItems        =   8
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Object.Tag             =   "T1000"
         Text            =   "Area"
         Object.Width           =   4762
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   1
         Key             =   "total"
         Object.Tag             =   "N100S"
         Text            =   "Valor"
         Object.Width           =   2028
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Object.Tag             =   "T500"
         Object.Width           =   882
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Object.Tag             =   "T2700"
         Text            =   "Centro Costo"
         Object.Width           =   4762
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Object.Tag             =   "N100"
         Text            =   "Valor"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   5
         Object.Tag             =   "T500"
         Object.Width           =   882
      EndProperty
      BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   6
         Object.Tag             =   "T2700"
         Text            =   "Item de Gasto"
         Object.Width           =   4762
      EndProperty
      BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   7
         Object.Tag             =   "N100"
         Text            =   "Valor"
         Object.Width           =   2540
      EndProperty
   End
End
Attribute VB_Name = "InformeAreaCCItemGastoTotal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub CmdBuscar_Click()
    Filtro = Empty
    If Option2.Value Then

        If CboTipoDoc.ListIndex = -1 Then
            MsgBox "Seleccione documento...", vbInformation
            CboTipoDoc.SetFocus
            Exit Sub
        End If
        If Val(TxtNDoc) = 0 Then
            MsgBox "Falta Nro documento...", vbInformation
            TxtNDoc.SetFocus
            Exit Sub
        End If
        If Len(txtRutProveedor) = 0 Then
            MsgBox "RUT no v�lido...", vbInformation
            txtRutProveedor.SetFocus
            Exit Sub
        End If
        Filtro = " AND c.rut='" & txtRutProveedor & "' AND c.doc_id=" & CboTipoDoc.ItemData(CboTipoDoc.ListIndex) & " AND no_documento=" & TxtNDoc & " "
    Else
        Filtro = " AND fecha BETWEEN '" & Format(DTInicio.Value, "YYYY-MM-DD") & "' AND '" & Format(DtHasta.Value, "YYYY-MM-DD") & "' "
    End If
    Filtro = Filtro & " AND x.doc_orden_de_compra='NO' "

   ' If SP_Control_Inventario = "SI" Then
        Sql = "SELECT are_nombre,SUM(d.cmd_total_neto) " & _
              "FROM com_doc_compra_detalle d " & _
              "INNER JOIN com_doc_compra c  ON c.id=d.id " & _
              "INNER JOIN sis_documentos x ON c.doc_id=x.doc_id " & _
              "INNER JOIN par_areas a ON d.are_id=a.are_id " & _
              "WHERE c.rut_emp='" & SP_Rut_Activo & "' " & Filtro & _
              "GROUP BY d.are_id"
   ' Else
   '     Sql = "SELECT are_nombre,SUM(d.csd_neto)+sum(d.csd_exento) valor " & _
              "FROM com_sin_detalle d " & _
              "INNER JOIN com_doc_compra c USING(id) " & _
              "INNER JOIN par_areas a ON d.are_id=a.are_id " & _
              "WHERE c.rut_emp='" & SP_Rut_Activo & "' " & Filtro & _
              "GROUP BY c.are_id"
   ' End If
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvAreas, False, True, True, False
    TxtTotalArea = NumFormat(TotalizaColumna(LvAreas, "total"))
    
    'Centro costo
   ' If SP_Control_Inventario = "SI" Then
        Sql = "SELECT cen_nombre,SUM(d.cmd_total_neto) " & _
                " FROM com_doc_compra_detalle d " & _
                "INNER JOIN com_doc_compra c  ON c.id=d.id " & _
                "INNER JOIN sis_documentos x ON c.doc_id=x.doc_id " & _
                "INNER JOIN par_centros_de_costo a ON d.cen_id=a.cen_id " & _
                "WHERE  c.rut_emp='" & SP_Rut_Activo & "' " & Filtro & _
                "GROUP BY d.cen_id"
   ' Else
    '    Sql = "SELECT cen_nombre,SUM(d.csd_neto)+sum(d.csd_exento) valor  " & _
                "FROM com_sin_detalle d " & _
                "INNER JOIN com_doc_compra c USING(id) " & _
                "INNER JOIN par_centros_de_costo a ON d.cen_id=a.cen_id  " & _
                 "WHERE c.rut_emp='" & SP_Rut_Activo & "' " & Filtro & _
                 "GROUP BY c.cen_id"
   ' End If
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvCC, False, True, True, False
    txtCC = NumFormat(TotalizaColumna(LvCC, "total"))
    
    'Items de gsatos
   ' If SP_Control_Inventario = "SI" Then
        Sql = "SELECT gas_nombre,SUM(d.cmd_total_neto) " & _
                "FROM com_doc_compra_detalle d " & _
                "INNER JOIN com_doc_compra c  ON c.id=d.id " & _
                "INNER JOIN sis_documentos x ON c.doc_id=x.doc_id " & _
                "INNER JOIN par_item_gastos a ON d.gas_id=a.gas_id " & _
                "WHERE  c.rut_emp='" & SP_Rut_Activo & "' " & Filtro & _
                "GROUP BY d.gas_id"
   ' Else
    '    Sql = "SELECT gas_nombre,SUM(d.csd_neto)+sum(d.csd_exento) valor  " & _
               "FROM com_sin_detalle d " & _
                "INNER JOIN com_doc_compra c USING(id) " & _
                "INNER JOIN par_item_gastos a ON d.gas_id=a.gas_id " & _
                "WHERE c.rut_emp='" & SP_Rut_Activo & "' " & Filtro & _
                "GROUP BY c.gas_id"
    'End If
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, Me.LvGastos, False, True, True, False
    TxtGasto = NumFormat(TotalizaColumna(LvGastos, "total"))
        
End Sub

Private Sub CmdBuscaRut_Click()
    BuscaProveedor.Show 1
    txtRutProveedor = RutBuscado
    txtRutProveedor_Validate True
End Sub






Private Sub CmdExportar_Click()
    Dim tit(2) As String, Lp_Lineas As Long
    LvDetalle.ListItems.Clear
    Lp_Lineas = LvAreas.ListItems.Count
    If LvCC.ListItems.Count > Lp_Lineas Then Lp_Lineas = LvCC.ListItems.Count
    If LvGastos.ListItems.Count > Lp_Lineas Then Lp_Lineas = LvGastos.ListItems.Count
    
    For i = 1 To Lp_Lineas + 1
        LvDetalle.ListItems.Add , , ""
    Next
    For i = 1 To LvAreas.ListItems.Count
        LvDetalle.ListItems(i).Text = LvAreas.ListItems(i)
        LvDetalle.ListItems(i).SubItems(1) = LvAreas.ListItems(i).SubItems(1)
    Next
    LvDetalle.ListItems(LvAreas.ListItems.Count + 1).Text = "Total Areas"
    LvDetalle.ListItems(LvAreas.ListItems.Count + 1).SubItems(1) = TxtTotalArea
    LvDetalle.ListItems(LvAreas.ListItems.Count + 1).Bold = True
    LvDetalle.ListItems(LvAreas.ListItems.Count + 1).ListSubItems(1).Bold = True
    
    For i = 1 To LvCC.ListItems.Count
        LvDetalle.ListItems(i).SubItems(3) = LvCC.ListItems(i)
        LvDetalle.ListItems(i).SubItems(4) = LvCC.ListItems(i).SubItems(1)
    Next
    LvDetalle.ListItems(LvCC.ListItems.Count + 1).SubItems(3) = "Total Centro Costo"
    LvDetalle.ListItems(LvCC.ListItems.Count + 1).SubItems(4) = Me.txtCC
    LvDetalle.ListItems(LvCC.ListItems.Count + 1).ListSubItems(3).Bold = True
    LvDetalle.ListItems(LvCC.ListItems.Count + 1).ListSubItems(4).Bold = True
    
    
    For i = 1 To LvGastos.ListItems.Count
        LvDetalle.ListItems(i).SubItems(6) = LvGastos.ListItems(i)
        LvDetalle.ListItems(i).SubItems(7) = LvGastos.ListItems(i).SubItems(1)
    Next
    LvDetalle.ListItems(LvGastos.ListItems.Count + 1).SubItems(6) = "Total Item Gastos"
    LvDetalle.ListItems(LvGastos.ListItems.Count + 1).SubItems(7) = Me.TxtGasto
    LvDetalle.ListItems(LvGastos.ListItems.Count + 1).ListSubItems(6).Bold = True
    LvDetalle.ListItems(LvGastos.ListItems.Count + 1).ListSubItems(7).Bold = True
    
    
    
    
    
    FraProgreso.Visible = True
    tit(0) = Me.Caption
    tit(1) = "FECHA INFORME: " & Date & " - " & Time
    tit(2) = "" ' "                                        DESDE:" & DtInicio.Value & "   HASTA:" & Dtfin.Value
    ExportarNuevo LvDetalle, tit, Me, PBar
    PBar.Value = 1
    FraProgreso.Visible = False
End Sub

Private Sub cmdSalir_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    Centrar Me
    DTInicio = Date
    DtHasta = Date
    Skin2 Me, , 4
    LLenarCombo CboTipoDoc, "doc_nombre", "doc_id", "sis_documentos", "doc_documento='COMPRA' and doc_activo='SI'", "doc_orden"
End Sub



Private Sub Option1_Click()
    DTInicio.SetFocus
End Sub

Private Sub Option2_Click()
    CboTipoDoc.SetFocus
End Sub

Private Sub Timer1_Timer()
    CboTipoDoc.SetFocus
    Timer1.Enabled = False
End Sub

Private Sub TxtNDoc_GotFocus()
    En_Foco TxtNDoc
End Sub

Private Sub TxtNDoc_KeyPress(KeyAscii As Integer)
    KeyAscii = AceptaSoloNumeros(KeyAscii)
    If KeyAscii = 39 Then KeyAscii = 0
End Sub



Private Sub TxtRutProveedor_GotFocus()
    En_Foco txtRutProveedor
End Sub

Private Sub txtRutProveedor_Validate(Cancel As Boolean)
        Respuesta = VerificaRut(txtRutProveedor, NuevoRut)
        txtRutProveedor = NuevoRut
        Sql = "SELECT nombre_empresa " & _
              "FROM maestro_proveedores " & _
              "WHERE rut_proveedor='" & txtRutProveedor & "' "
        Consulta RsTmp, Sql
        TxtRsocial = ""
        If RsTmp.RecordCount > 0 Then TxtRsocial = RsTmp!nombre_empresa Else MsgBox "Proveedor no encontrado... ", vbOKOnly + vbInformation
End Sub
