VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{DA729E34-689F-49EA-A856-B57046630B73}#1.0#0"; "progressbar-xp.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form Informe_historial_arr_proveedor 
   Caption         =   "Informe Historial de Maquinarias por Cliente"
   ClientHeight    =   7920
   ClientLeft      =   1710
   ClientTop       =   840
   ClientWidth     =   16890
   LinkTopic       =   "Form1"
   ScaleHeight     =   7920
   ScaleWidth      =   16890
   Begin VB.Frame Frame1 
      Caption         =   "Maquinas Arrendadas por Cliente"
      Height          =   7650
      Left            =   30
      TabIndex        =   0
      Top             =   75
      Width           =   16740
      Begin ACTIVESKINLibCtl.SkinLabel SkSucursal 
         Height          =   375
         Left            =   9885
         OleObjectBlob   =   "Informe_historial_arr_proveedor.frx":0000
         TabIndex        =   17
         Top             =   240
         Width           =   6705
      End
      Begin VB.Frame Frame4 
         Caption         =   "Mes y A�o"
         Height          =   1395
         Left            =   6630
         TabIndex        =   9
         Top             =   135
         Width           =   2970
         Begin VB.ComboBox comMes 
            Height          =   315
            ItemData        =   "Informe_historial_arr_proveedor.frx":007E
            Left            =   135
            List            =   "Informe_historial_arr_proveedor.frx":0080
            Style           =   2  'Dropdown List
            TabIndex        =   13
            Top             =   450
            Width           =   1575
         End
         Begin VB.ComboBox ComAno 
            Height          =   315
            Left            =   1710
            Style           =   2  'Dropdown List
            TabIndex        =   12
            Top             =   435
            Width           =   1215
         End
         Begin VB.CheckBox ChkFecha 
            Height          =   225
            Left            =   210
            TabIndex        =   10
            Top             =   1035
            Visible         =   0   'False
            Width           =   225
         End
         Begin MSComCtl2.DTPicker DtDesde 
            CausesValidation=   0   'False
            Height          =   285
            Left            =   480
            TabIndex        =   11
            Top             =   1005
            Visible         =   0   'False
            Width           =   1215
            _ExtentX        =   2143
            _ExtentY        =   503
            _Version        =   393216
            Enabled         =   0   'False
            Format          =   91684865
            CurrentDate     =   41001
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
            Height          =   255
            Left            =   120
            OleObjectBlob   =   "Informe_historial_arr_proveedor.frx":0082
            TabIndex        =   14
            Top             =   255
            Width           =   375
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel5 
            Height          =   255
            Left            =   1725
            OleObjectBlob   =   "Informe_historial_arr_proveedor.frx":00E6
            TabIndex        =   15
            Top             =   255
            Width           =   375
         End
         Begin MSComCtl2.DTPicker DtHasta 
            Height          =   285
            Left            =   1680
            TabIndex        =   16
            Top             =   1005
            Visible         =   0   'False
            Width           =   1200
            _ExtentX        =   2117
            _ExtentY        =   503
            _Version        =   393216
            Enabled         =   0   'False
            Format          =   91684865
            CurrentDate     =   41001
         End
      End
      Begin VB.TextBox TxtRazonSocial 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0C0C0&
         BorderStyle     =   0  'None
         Height          =   285
         Left            =   705
         TabIndex        =   8
         ToolTipText     =   "Ingrese el RUT sin puntos ni guion."
         Top             =   885
         Width           =   5730
      End
      Begin VB.CommandButton CmdBuscaCliente 
         Caption         =   "Buscar Cliente"
         Height          =   345
         Left            =   2505
         TabIndex        =   7
         Top             =   345
         Width           =   1260
      End
      Begin VB.CommandButton CmdRut 
         Caption         =   "Consultar"
         Height          =   480
         Left            =   9810
         TabIndex        =   6
         Top             =   765
         Width           =   1740
      End
      Begin VB.TextBox TxtRut 
         Height          =   345
         Left            =   735
         TabIndex        =   5
         Top             =   345
         Width           =   1605
      End
      Begin VB.CommandButton CmdExportar 
         Caption         =   "Exportar a Excel"
         Height          =   450
         Left            =   13605
         TabIndex        =   2
         Top             =   6960
         Width           =   1395
      End
      Begin VB.CommandButton Command2 
         Caption         =   "Retornar"
         Height          =   450
         Left            =   15120
         TabIndex        =   1
         Top             =   6960
         Width           =   1380
      End
      Begin Proyecto2.XP_ProgressBar BarraProgreso 
         Height          =   495
         Left            =   4425
         TabIndex        =   3
         Top             =   3315
         Visible         =   0   'False
         Width           =   8460
         _ExtentX        =   14923
         _ExtentY        =   873
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BrushStyle      =   0
         Color           =   16777088
         Scrolling       =   1
         ShowText        =   -1  'True
      End
      Begin MSComctlLib.ListView LvMaquinas 
         Height          =   5085
         Left            =   165
         TabIndex        =   4
         Top             =   1710
         Width           =   16350
         _ExtentX        =   28840
         _ExtentY        =   8969
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   11
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Numero Contrato"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Fecha Contrato"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Fecha Termino"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "Dias"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Text            =   "Valor Neto"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Text            =   "Total"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   6
            Text            =   "Codigo Interno"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   7
            Text            =   "Codigo"
            Object.Width           =   2646
         EndProperty
         BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   8
            Text            =   "Obra"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   9
            Text            =   "Descripcion"
            Object.Width           =   8819
         EndProperty
         BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   10
            Text            =   "Estado"
            Object.Width           =   3528
         EndProperty
      End
   End
End
Attribute VB_Name = "Informe_historial_arr_proveedor"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub ChkFecha_Click()
    On Error Resume Next
    If ChkFecha.Value = 1 Then
        comMes.Enabled = False
        ComAno.Enabled = False
        DtDesde.Enabled = True
        DtHasta.Enabled = True
        DtDesde.SetFocus
    Else
        comMes.SetFocus
        DtDesde.Enabled = False
        DtHasta.Enabled = False
        comMes.Enabled = True
        ComAno.Enabled = True
    End If
End Sub

Private Sub CmdBuscaCliente_Click()
    LlamaClienteDe = "VD"
    
    ClienteEncontrado = False
    BuscaCliente.Show 1
    TxtRut = SG_codigo
    TxtRut.SetFocus
End Sub

Private Sub CmdExportar_Click()
 Dim tit(2) As String
    If LvMaquinas.ListItems.Count = 0 Then Exit Sub
    BarraProgreso.Visible = True
    tit(0) = Me.Caption & " " & Date & " - " & Time
    tit(1) = Me.TxtRazonSocial & " " & Me.TxtRut
    tit(2) = ""
    ExportarNuevo LvMaquinas, tit, Me, BarraProgreso
    BarraProgreso.Visible = False
End Sub

Private Sub CmdRut_Click()
    Dim Sp_Fecha As String
    Dim Sp_FiltroSueId As String
    Dim Sp_UltimoDia As Date
    Dim Sp_PrimerDia As Date
   ' ULTIMODIA = Fql(ComAno.Text & "-" & Me.comMes.ItemData(comMes.ListIndex) & "01")
    Sp_UltimoDia = UltimoDiaMes(ComAno.Text & "-" & Me.comMes.ItemData(comMes.ListIndex) & "-01")
    Sp_PrimerDia = ComAno.Text & "-" & comMes.ItemData(comMes.ListIndex) & "-01"
    Sp_FiltroSueId = " AND n.sue_id=" & LogSueID & " AND m.sue_id= " & LogSueID
    If ChkFecha.Value = 1 Then
        Sp_Fecha = " AND m.arr_fecha_contrato BETWEEN '" & Fql(DtDesde.Value) & "' AND '" & Fql(DtHasta.Value) & "' "
    Else
        Sp_Fecha = " AND MONTH(m.arr_fecha_contrato)=" & comMes.ItemData(comMes.ListIndex) & " AND YEAR(m.arr_fecha_contrato)=" & IIf(ComAno.ListCount = 0, Year(Date), ComAno.Text)
    End If



   Sql = "SELECT m.arr_id,m.arr_fecha_contrato,IF(ISNULL(n.det_arr_fecha_recepcion),'" & Fql(Sp_UltimoDia) & "',n.det_arr_fecha_recepcion)" & _
                ",datediff(IF(ISNULL(n.det_arr_fecha_recepcion),'" & Fql(Sp_UltimoDia) & "',n.det_arr_fecha_recepcion),m.arr_fecha_contrato)+1 as dias," & _
                "m.arr_valor,((datediff(IF(ISNULL(n.det_arr_fecha_recepcion),'" & Fql(Sp_UltimoDia) & "',n.det_arr_fecha_recepcion),m.arr_fecha_contrato)+1)    *m.arr_valor) as total," & _
                "det_arr_codigo,det_arr_codigo_interno,m.arr_direccion_obra,det_arr_descripcion,IF(ISNULL(n.det_arr_fecha_recepcion),'VIGENTE','FINALIZADO') " & _
            "FROM ven_arr_detalle n " & _
    " INNER JOIN ven_arriendo m " & _
    " WHERE arr_estado<>'NULO' AND   m.arr_rut_cliente='" & Me.TxtRut & "' AND n.arr_id=m.arr_id " & Sp_Fecha & " " & Sp_FiltroSueId & " " & _
    "  "
  
  
  '01-02-2018 SE AGREGAN LOS REZAGADOS DE OTROS MESES SIN CERRAR
    Sql = Sql & " UNION SELECT m.arr_id,'" & Fql(Sp_PrimerDia) & "','" & Fql(Sp_UltimoDia) & "'," & _
                "datediff('" & Fql(Sp_UltimoDia) & "','" & Fql(Sp_PrimerDia) & "') +1  as dias,m.arr_valor," & _
                "(datediff('" & Fql(Sp_UltimoDia) & "','" & Fql(Sp_PrimerDia) & "') +1 )*m.arr_valor AS total," & _
                "det_arr_codigo,det_arr_codigo_interno,m.arr_direccion_obra,det_arr_descripcion,'VIGENTE' " & _
            "FROM ven_arr_detalle n " & _
    " INNER JOIN ven_arriendo m " & _
    " WHERE arr_estado<>'NULO' AND  ISNULL(n.det_arr_fecha_recepcion) AND m.arr_rut_cliente='" & Me.TxtRut & "' AND n.arr_id=m.arr_id AND m.arr_fecha_contrato< '" & ComAno.Text & "-" & comMes.ItemData(comMes.ListIndex) & "-01' " & Sp_FiltroSueId & " " & _
    "  "
  
    '01-02-2018 SE AGREGAN LOS REZAGADOS DE OTROS CERRADOS EN EL MES ACTUAL
    Sql = Sql & " UNION SELECT m.arr_id,'" & Fql(Sp_PrimerDia) & "',n.det_arr_fecha_recepcion," & _
                "datediff(n.det_arr_fecha_recepcion,'" & Fql(Sp_PrimerDia) & "') +1  as dias,m.arr_valor," & _
                "(datediff(n.det_arr_fecha_recepcion,'" & Fql(Sp_PrimerDia) & "') +1 )*m.arr_valor AS total," & _
                "det_arr_codigo,det_arr_codigo_interno,m.arr_direccion_obra,det_arr_descripcion,'FINALIZADO' " & _
            "FROM ven_arr_detalle n " & _
    " INNER JOIN ven_arriendo m " & _
    " WHERE arr_estado<>'NULO' AND  n.det_arr_fecha_recepcion BETWEEN '" & Fql(Sp_PrimerDia) & "' AND '" & Fql(Sp_UltimoDia) & "' AND m.arr_rut_cliente='" & Me.TxtRut & "' AND n.arr_id=m.arr_id AND m.arr_fecha_contrato< '" & ComAno.Text & "-" & comMes.ItemData(comMes.ListIndex) & "-01' " & Sp_FiltroSueId & " " & _
    "  "
  
   ' cn.Execute Sql
    
    
   Sql = Sql & "UNION SELECT m.arr_id,m.arr_fecha_contrato,IF(ISNULL(n.det_arr_fecha_recepcion),'" & Fql(Sp_UltimoDia) & "',n.det_arr_fecha_recepcion)" & _
                ",0 as dias," & _
                "0,0 as total," & _
                "det_arr_codigo,det_arr_codigo_interno,m.arr_direccion_obra,det_arr_descripcion,m.arr_estado " & _
            "FROM ven_arr_detalle n " & _
    " INNER JOIN ven_arriendo m " & _
    " WHERE arr_estado='NULO' AND   m.arr_rut_cliente='" & Me.TxtRut & "' AND n.arr_id=m.arr_id " & Sp_Fecha & " " & Sp_FiltroSueId & " " & _
    "  "
  
    
    
    
        
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvMaquinas, False, True, True, False
End Sub

Private Sub Command2_Click()
Unload Me
End Sub

Private Sub Form_Load()
Dim nombre As String
    Aplicar_skin Me
    Centrar Me
        DtDesde.Value = Date
    DtHasta.Value = Date
    Centrar Me
    SkEmpresaActiva = SP_Empresa_Activa
    Sp_Condicion = ""
    For i = 1 To 12
        comMes.AddItem UCase(MonthName(i, False))
        comMes.ItemData(comMes.ListCount - 1) = i
    Next
    Busca_Id_Combo comMes, Val(IG_Mes_Contable)
    LLenaYears ComAno, 2010
    
    Sql = "SELECT sue_ciudad,sue_direccion " & _
            "FROM sis_empresas_sucursales " & _
            "WHERE sue_id=" & LogSueID
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        SkSucursal = RsTmp!sue_ciudad & " - " & RsTmp!sue_direccion
        
    
    End If
    
End Sub

Private Sub LvMaquinas_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    ordListView ColumnHeader, Me, LvMaquinas
End Sub

Private Sub TxtRut_Validate(Cancel As Boolean)
    If Len(TxtRut.Text) = 0 Then Exit Sub

    TxtRut.Text = Replace(TxtRut.Text, ".", "")
    TxtRut.Text = Replace(TxtRut.Text, "-", "")
    Respuesta = VerificaRut(TxtRut.Text, NuevoRut)
   
    If Respuesta = True Then
        Me.TxtRut.Text = NuevoRut
        TxtRut.Text = Replace(TxtRut.Text, ",", ".")
        'Buscar el cliente
        Sql = "SELECT rut_cliente,nombre_rsocial,giro,direccion,comuna,fono,email, " & _
                "IFNULL(lst_nombre,'LISTA PRECIO GENERAL') listaprecios,m.lst_id,ven_id,cli_monto_credito " & _
              "FROM maestro_clientes m " & _
              "INNER JOIN par_asociacion_ruts  a ON m.rut_cliente=a.rut_cli " & _
              "LEFT JOIN par_lista_precios l USING(lst_id) " & _
              "WHERE habilitado='SI' AND a.rut_emp='" & SP_Rut_Activo & "' AND rut_cliente = '" & Me.TxtRut.Text & "'"
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
                'Cliente encontrado
            With RsTmp
                TxtMontoCredito = !cli_monto_credito
                TxtListaPrecio = !ListaPrecios
''                TxtListaPrecio.Tag = !lst_id
                TxtRut.Text = !rut_cliente
                TxtGiro = !giro
                TxtDireccion = !direccion
                txtComuna = !comuna
                txtFono = !fono
                TxtEmail = !Email
                TxtRazonSocial.Text = !nombre_rsocial
               
                ClienteEncontrado = True
              
                
                Sql = "SELECT a.lst_id,lst_nombre " & _
                      "FROM par_asociacion_lista_precios  a " & _
                      "INNER JOIN par_lista_precios l USING(lst_id) " & _
                      "WHERE l.rut_emp='" & SP_Rut_Activo & "' AND cli_rut='" & TxtRut & "'"
                Consulta RsTmp2, Sql
           ''     TxtListaPrecio.Tag = !lst_id
        ''        TxtListaPrecio = "LISTA DE PRECIOS PRINCIPAL"
        ''        If RsTmp2.RecordCount > 0 Then
         ''           TxtListaPrecio = RsTmp2!lst_nombre
         ''           TxtListaPrecio.Tag = RsTmp2!lst_id
        ''        End If
                
                If Val(TxtMontoCredito) > 0 Then
                    'Debemos consultar el saldo de credito disponible del cliente.
                    TxtCupoUtilizado = ConsultaSaldoCliente(TxtRut)
                    SkCupoCredito = NumFormat(Val(TxtMontoCredito) - Val(TxtCupoUtilizado))
                End If

                
                
            End With
                
            If bm_SoloVistaDoc Then Exit Sub
                                
        
        Else
                'Cliente no existe aun �crearlo??
                Respuesta = MsgBox("Cliente no encontrado..." & Chr(13) & _
                "     �Desea Crear?    ", vbYesNo + vbQuestion)
                If Respuesta = 6 Then
                    'crear
                    SG_codigo = ""
                    AccionCliente = 4
                    AgregoCliente.TxtRut.Text = Me.TxtRut.Text
                    
                    AgregoCliente.TxtRut.Locked = True
                    ClienteEncontrado = False
                    AgregoCliente.Timer1.Enabled = True
            
                    AgregoCliente.Show 1
                    If ClienteEncontrado = True Then
                        TxtRut_Validate False
                       
                        TxtCodigo.SetFocus
                    Else
                        TxtRut_Validate True
                    End If
                Else
                    'volver al rut
                    ClienteEncontrado = False
                    Me.TxtRut.Text = ""
                    On Error Resume Next
                    SendKeys "+{TAB}" ' Shift TAB retrocede al control anterior segun el orden del tabindex
                End If
        
       TxtRut.Text = Replace(TxtRut.Text, ",", ".")
        End If
    
    Else
        Me.TxtRut.Text = ""
        On Error Resume Next
        SendKeys "+{TAB}" ' Shift TAB retrocede al control anterior segun el orden del tabindex
    End If
    Exit Sub
CancelaImpesionFacturacion:
    'No imprime
    Unload Me
End Sub
