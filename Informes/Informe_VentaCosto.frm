VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{DA729E34-689F-49EA-A856-B57046630B73}#1.0#0"; "progressbar-xp.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form Informe_VentaCosto 
   Caption         =   "Informe Venta V/s Costo - Inmovilizado"
   ClientHeight    =   9960
   ClientLeft      =   1365
   ClientTop       =   2115
   ClientWidth     =   16140
   LinkTopic       =   "Form1"
   ScaleHeight     =   9960
   ScaleWidth      =   16140
   Begin VB.Frame FraProgreso 
      Height          =   540
      Left            =   3045
      TabIndex        =   32
      Top             =   8400
      Visible         =   0   'False
      Width           =   8565
      Begin Proyecto2.XP_ProgressBar PBar 
         Height          =   255
         Left            =   135
         TabIndex        =   33
         Top             =   195
         Width           =   8265
         _ExtentX        =   14579
         _ExtentY        =   450
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BrushStyle      =   0
         Color           =   16777088
         ShowText        =   -1  'True
      End
   End
   Begin VB.Frame Frame4 
      Caption         =   "Informacion"
      Height          =   1365
      Left            =   5820
      TabIndex        =   19
      Top             =   1995
      Width           =   9825
      Begin VB.OptionButton Option2 
         Caption         =   "Seleccionar fechas"
         Height          =   240
         Left            =   4230
         TabIndex        =   25
         Top             =   645
         Width           =   1725
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Todas las fechas"
         Height          =   240
         Left            =   4245
         TabIndex        =   24
         Top             =   315
         Value           =   -1  'True
         Width           =   1560
      End
      Begin VB.TextBox TxtDescrp 
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1050
         Locked          =   -1  'True
         TabIndex        =   23
         Top             =   930
         Width           =   3615
      End
      Begin VB.TextBox TxtMarca 
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1050
         Locked          =   -1  'True
         TabIndex        =   22
         Top             =   600
         Width           =   1455
      End
      Begin VB.CommandButton CmdBuscaProducto 
         Caption         =   "Codigo"
         Height          =   300
         Left            =   75
         TabIndex        =   21
         Top             =   285
         Width           =   975
      End
      Begin VB.TextBox TxtCodigo 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1050
         TabIndex        =   20
         ToolTipText     =   "Para ingresar un Gasto digite 0 (cero)"
         Top             =   285
         Width           =   1095
      End
      Begin MSComCtl2.DTPicker DTInicio 
         Height          =   300
         Left            =   6090
         TabIndex        =   26
         Top             =   435
         Width           =   1305
         _ExtentX        =   2302
         _ExtentY        =   529
         _Version        =   393216
         Enabled         =   0   'False
         Format          =   92078081
         CurrentDate     =   40628
      End
      Begin ACTIVESKINLibCtl.SkinLabel skIni 
         Height          =   240
         Index           =   0
         Left            =   6075
         OleObjectBlob   =   "Informe_VentaCosto.frx":0000
         TabIndex        =   27
         Top             =   195
         Width           =   795
      End
      Begin MSComCtl2.DTPicker DtHasta 
         Height          =   285
         Left            =   6075
         TabIndex        =   28
         Top             =   960
         Width           =   1305
         _ExtentX        =   2302
         _ExtentY        =   503
         _Version        =   393216
         Enabled         =   0   'False
         Format          =   92078081
         CurrentDate     =   40628
      End
      Begin ACTIVESKINLibCtl.SkinLabel skFin 
         Height          =   210
         Index           =   0
         Left            =   6060
         OleObjectBlob   =   "Informe_VentaCosto.frx":0068
         TabIndex        =   29
         Top             =   750
         Width           =   795
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
         Height          =   210
         Left            =   120
         OleObjectBlob   =   "Informe_VentaCosto.frx":00D0
         TabIndex        =   30
         Top             =   660
         Width           =   840
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel5 
         Height          =   210
         Left            =   150
         OleObjectBlob   =   "Informe_VentaCosto.frx":0138
         TabIndex        =   31
         Top             =   960
         Width           =   840
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Filtro"
      Height          =   1710
      Left            =   615
      TabIndex        =   12
      Top             =   1590
      Width           =   4845
      Begin VB.ComboBox CboAgrupa 
         Enabled         =   0   'False
         Height          =   315
         ItemData        =   "Informe_VentaCosto.frx":01A6
         Left            =   2040
         List            =   "Informe_VentaCosto.frx":01B0
         Style           =   2  'Dropdown List
         TabIndex        =   16
         Top             =   1170
         Width           =   2430
      End
      Begin VB.CheckBox ChkTipo 
         Caption         =   "Agrupar"
         Height          =   225
         Left            =   1065
         TabIndex        =   15
         Top             =   1185
         Width           =   1035
      End
      Begin VB.ComboBox CboMarca 
         Height          =   315
         Left            =   1395
         Style           =   2  'Dropdown List
         TabIndex        =   14
         Top             =   765
         Width           =   3210
      End
      Begin VB.ComboBox CboTipo 
         Height          =   315
         Left            =   1380
         Style           =   2  'Dropdown List
         TabIndex        =   13
         Top             =   360
         Width           =   3210
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   240
         Left            =   195
         OleObjectBlob   =   "Informe_VentaCosto.frx":01C1
         TabIndex        =   17
         Top             =   390
         Width           =   1125
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   240
         Left            =   720
         OleObjectBlob   =   "Informe_VentaCosto.frx":0239
         TabIndex        =   18
         Top             =   780
         Width           =   615
      End
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   8655
      Left            =   390
      TabIndex        =   4
      Top             =   1080
      Width           =   15330
      _ExtentX        =   27040
      _ExtentY        =   15266
      _Version        =   393216
      Tabs            =   2
      TabsPerRow      =   2
      TabHeight       =   520
      TabCaption(0)   =   "Costo Venta"
      TabPicture(0)   =   "Informe_VentaCosto.frx":02A1
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "FraArticulos"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "cmdConsultar"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).ControlCount=   2
      TabCaption(1)   =   "Inventario Inmovilizado"
      TabPicture(1)   =   "Informe_VentaCosto.frx":02BD
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "SkResultados"
      Tab(1).Control(1)=   "LvInmo"
      Tab(1).Control(2)=   "Command2"
      Tab(1).Control(3)=   "Command1"
      Tab(1).Control(4)=   "CmdAntes"
      Tab(1).ControlCount=   5
      Begin VB.CommandButton CmdAntes 
         Caption         =   "Command3"
         Height          =   195
         Left            =   -60360
         TabIndex        =   39
         Top             =   360
         Visible         =   0   'False
         Width           =   540
      End
      Begin VB.CommandButton Command1 
         Caption         =   "Consultar"
         Height          =   270
         Left            =   -61620
         TabIndex        =   38
         Top             =   660
         Width           =   1725
      End
      Begin VB.CommandButton Command2 
         Caption         =   "Exportar a Excel"
         Height          =   360
         Left            =   -74670
         TabIndex        =   37
         Top             =   7680
         Width           =   1320
      End
      Begin VB.CommandButton cmdConsultar 
         Caption         =   "&Consultar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   13635
         TabIndex        =   34
         Top             =   435
         Width           =   1365
      End
      Begin VB.Frame FraArticulos 
         Caption         =   "Articulos"
         Height          =   5970
         Left            =   120
         TabIndex        =   5
         Top             =   2250
         Width           =   15000
         Begin VB.CommandButton cmdExportar 
            Caption         =   "Exportar a Excel"
            Height          =   360
            Left            =   105
            TabIndex        =   35
            Top             =   5160
            Width           =   1320
         End
         Begin VB.Frame Frame2 
            Caption         =   "Totales"
            Height          =   600
            Left            =   10200
            TabIndex        =   6
            Top             =   5190
            Width           =   4530
            Begin VB.TextBox txtDiferencia 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00E0E0E0&
               Height          =   285
               Left            =   2955
               Locked          =   -1  'True
               TabIndex        =   9
               Top             =   270
               Width           =   1350
            End
            Begin VB.TextBox TxtCosto 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00E0E0E0&
               Height          =   285
               Left            =   1605
               Locked          =   -1  'True
               TabIndex        =   8
               Top             =   270
               Width           =   1350
            End
            Begin VB.TextBox TxtVenta 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00E0E0E0&
               Height          =   285
               Left            =   255
               Locked          =   -1  'True
               TabIndex        =   7
               Top             =   270
               Width           =   1350
            End
         End
         Begin MSComctlLib.ListView LvDetalle 
            Height          =   4800
            Left            =   75
            TabIndex        =   10
            Top             =   345
            Width           =   14805
            _ExtentX        =   26114
            _ExtentY        =   8467
            View            =   3
            LabelWrap       =   0   'False
            HideSelection   =   0   'False
            FullRowSelect   =   -1  'True
            GridLines       =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   9
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Object.Tag             =   "T1200"
               Text            =   "Codigo"
               Object.Width           =   2117
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Object.Tag             =   "T1100"
               Text            =   "Nombre Articulo"
               Object.Width           =   7056
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   2
               Object.Tag             =   "T100"
               Text            =   "Marca"
               Object.Width           =   3351
            EndProperty
            BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   3
               Object.Tag             =   "T100"
               Text            =   "Tipo Articulo"
               Object.Width           =   3351
            EndProperty
            BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   4
               Object.Tag             =   "N102"
               Text            =   "Un. Vendidas"
               Object.Width           =   2293
            EndProperty
            BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   5
               Key             =   "venta"
               Object.Tag             =   "N102"
               Text            =   "Pre.Venta"
               Object.Width           =   2293
            EndProperty
            BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   6
               Key             =   "costo"
               Object.Tag             =   "N102"
               Text            =   "Pre.Costo"
               Object.Width           =   2293
            EndProperty
            BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   7
               Key             =   "diferencia"
               Object.Tag             =   "N100"
               Text            =   "Utilidad-Perdida"
               Object.Width           =   2646
            EndProperty
            BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   8
               Object.Tag             =   "T1000"
               Text            =   "%"
               Object.Width           =   1764
            EndProperty
         End
      End
      Begin MSComctlLib.ListView LvInmo 
         Height          =   5220
         Left            =   -74700
         TabIndex        =   11
         Top             =   2340
         Width           =   14805
         _ExtentX        =   26114
         _ExtentY        =   9208
         View            =   3
         LabelWrap       =   0   'False
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   10
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "T1200"
            Text            =   "Cod. Sis."
            Object.Width           =   1587
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "T1000"
            Text            =   "Cod Empresa"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "T1100"
            Text            =   "Nombre Articulo"
            Object.Width           =   7056
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Object.Tag             =   "T100"
            Text            =   "Marca"
            Object.Width           =   3351
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Object.Tag             =   "T100"
            Text            =   "Tipo Articulo"
            Object.Width           =   3351
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   5
            Object.Tag             =   "N102"
            Text            =   "Un. Vendidas"
            Object.Width           =   2293
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   6
            Key             =   "venta"
            Object.Tag             =   "F1000"
            Text            =   "Fecha"
            Object.Width           =   2293
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   7
            Object.Tag             =   "N102"
            Text            =   "Stock"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   8
            Object.Tag             =   "F1000"
            Text            =   "1er Kardex"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   9
            Object.Tag             =   "T2000"
            Text            =   "Descripcion Kardex"
            Object.Width           =   3528
         EndProperty
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkResultados 
         Height          =   285
         Left            =   -71280
         OleObjectBlob   =   "Informe_VentaCosto.frx":02D9
         TabIndex        =   36
         Top             =   7620
         Width           =   4605
      End
   End
   Begin VB.Frame Fram 
      Caption         =   "No disponible"
      Height          =   8325
      Left            =   105
      TabIndex        =   2
      Top             =   8085
      Visible         =   0   'False
      Width           =   15045
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   945
         Left            =   1965
         OleObjectBlob   =   "Informe_VentaCosto.frx":035B
         TabIndex        =   3
         Top             =   3090
         Width           =   11715
      End
   End
   Begin VB.CommandButton cmdSalir 
      Cancel          =   -1  'True
      Caption         =   "&Retornar"
      Height          =   405
      Left            =   14490
      TabIndex        =   0
      Top             =   9210
      Width           =   1350
   End
   Begin VB.Timer Timer1 
      Interval        =   10
      Left            =   645
      Top             =   60
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   135
      OleObjectBlob   =   "Informe_VentaCosto.frx":043B
      Top             =   30
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkEmpresaActiva 
      Height          =   285
      Left            =   9330
      OleObjectBlob   =   "Informe_VentaCosto.frx":066F
      TabIndex        =   1
      Top             =   90
      Width           =   5685
   End
End
Attribute VB_Name = "Informe_VentaCosto"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim sm_FiltroFecha  As String
Dim Sm_FiltroCodigo As String
Dim Sm_FiltroMarca  As String
Dim Sm_FiltroTipo   As String
Dim Sm_AgrupaTipo As String

Private Sub CboMarca_Click()
   If CboMarca.ListIndex = -1 Then Exit Sub
    If CboMarca.Text = "TODOS" Then
        Sm_FiltroMarca = Empty
    Else
        Sm_FiltroMarca = " AND p.mar_id=" & CboMarca.ItemData(CboMarca.ListIndex) & " "
    End If
    cmdConsultar_Click
End Sub

Private Sub CboTipo_Click()
    If CboTipo.ListIndex = -1 Then Exit Sub
    If CboTipo.Text = "TODOS" Then
        Sm_FiltroTipo = Empty
    Else
        Sm_FiltroTipo = " AND p.tip_id=" & CboTipo.ItemData(CboTipo.ListIndex) & " "
    End If
    cmdConsultar_Click
End Sub

Private Sub ChkTipo_Click()
    If ChkTipo.Value = 0 Then
        CboAgrupa.Enabled = False
    Else
        CboAgrupa.Enabled = True
    End If
End Sub

Private Sub CmdAntes_Click()
    SG_codigo2 = ""
    For i = 1 To LvInmo.ListItems.Count
        If Fql(LvInmo.ListItems(i).SubItems(8)) > Fql(Me.DTInicio) Then
            SG_codigo2 = SG_codigo2 & LvInmo.ListItems(i) & ","
        
        End If
    Next
    If Len(SG_codigo2) > 0 Then
        SG_codigo2 = Mid(SG_codigo2, 1, Len(SG_codigo2) - 1)
        SG_codigo2 = " AND codigo NOT IN(" & SG_codigo2 & ")"
        Command1_Click
    End If
    
End Sub

Private Sub CmdBuscaProducto_Click()
    BuscaProducto.Show 1
    If Len(SG_codigo) = 0 Then Exit Sub
    TxtCodigo = SG_codigo
    TxtCodigo_Validate True
End Sub

Private Sub cmdConsultar_Click()
    If Option1.Value Then
        sm_FiltroFecha = Empty
    Else
         sm_FiltroFecha = " AND (r.fecha BETWEEN '" & Format(DTInicio.Value, "YYYY-MM-DD") & "' AND '" & Format(DtHasta.Value, "YYYY-MM-DD") & "') "
    End If
    Sm_FiltroCodigo = Empty
    If Len(TxtCodigo) > 0 Then Sm_FiltroCodigo = " AND v.codigo='" & TxtCodigo & "' "
    LvDetalle.ListItems.Clear
    Sm_AgrupaTipo = ""
    If ChkTipo.Value = 1 Then
        Sm_AgrupaTipo = "GROUP BY"
    End If
    CargaArticulos
    If LvDetalle.ListItems.Count > 0 Then
    '6 * 7
        valorcillo = 0
        txtDiferencia = 0
        For i = 1 To LvDetalle.ListItems.Count
            valorcillo = CDbl(LvDetalle.ListItems(i).SubItems(5)) - CDbl(LvDetalle.ListItems(i).SubItems(6))
           ' valorcillo = LvDetalle.ListItems(i).SubItems(7)
            If Val(valorcillo) = 0 Then valorcillo = "0"
            'If Val(LvDetalle.ListItems(i).SubItems(7)) = 0 Then LvDetalle.ListItems(i).SubItems(7) = "0"
            txtDiferencia = CDbl(txtDiferencia) + CDbl(valorcillo)
            LvDetalle.ListItems(i).SubItems(8) = (CDbl(valorcillo) / CDbl(LvDetalle.ListItems(i).SubItems(5)))
            LvDetalle.ListItems(i).SubItems(7) = NumFormat(valorcillo)
            'End If
        Next
        txtDiferencia = NumFormat(txtDiferencia)
    End If
    
End Sub

Private Sub CmdExportar_Click()
    Dim tit(2) As String
    FraProgreso.Visible = True
    tit(0) = Me.Caption
    tit(1) = "FECHA INFORME: " & Date & " - " & Time
    tit(2) = "" ' "                                        DESDE:" & DtInicio.Value & "   HASTA:" & Dtfin.Value
    ExportarNuevo LvDetalle, tit, Me, PBar
    PBar.Value = 1
    FraProgreso.Visible = False
End Sub

Private Sub CmdSalir_Click()
    Unload Me
End Sub

Private Sub Command1_Click()
    If Option1.Value Then
        sm_FiltroFecha = Empty
    Else
         sm_FiltroFecha = " AND (r.fecha BETWEEN '" & Format(DTInicio.Value, "YYYY-MM-DD") & "' AND '" & Format(DtHasta.Value, "YYYY-MM-DD") & "') "
    End If
    Sm_FiltroCodigo = Empty
    If Len(TxtCodigo) > 0 Then Sm_FiltroCodigo = " AND v.codigo='" & TxtCodigo & "' "
    LvInmo.ListItems.Clear

    CargaInmo
    
    SG_codigo2 = ""
    For i = 1 To LvInmo.ListItems.Count
        If Fql(LvInmo.ListItems(i).SubItems(8)) > Fql(Me.DTInicio) Then
            SG_codigo2 = SG_codigo2 & LvInmo.ListItems(i) & ","
        
        End If
    Next
    If Len(SG_codigo2) > 0 Then
        SG_codigo2 = Mid(SG_codigo2, 1, Len(SG_codigo2) - 1)
        SG_codigo2 = " AND codigo NOT IN(" & SG_codigo2 & ")"
        CargaInmo
    End If
    
End Sub

Private Sub Command2_Click()
 '   nombre = InputBox("Escribe tu nombre")
 '   MsgBox "Hola " & nombre
     Dim tit(2) As String
    FraProgreso.Visible = True
    tit(0) = "INMOVILIZADO"
    tit(1) = "FECHA INFORME: " & Date & " - " & Time
    tit(2) = "" ' "                                        DESDE:" & DtInicio.Value & "   HASTA:" & Dtfin.Value
    ExportarNuevo LvInmo, tit, Me, PBar
    PBar.Value = 1
    FraProgreso.Visible = False
End Sub

Private Sub Form_Load()
    If SP_Control_Inventario = "NO" Then
        Fram.Visible = True
    End If
    Aplicar_skin Me
    Centrar Me
    CboAgrupa.ListIndex = 0
    SkEmpresaActiva = "EMPRESA ACTIVA: " & SP_Empresa_Activa
    DTInicio.Value = Date - 30
    DtHasta.Value = Date
       LLenarCombo CboTipo, "tip_nombre", "tip_id", "par_tipos_productos", "tip_activo='SI' AND rut_emp='" & SP_Rut_Activo & "'"
    CboTipo.AddItem "TODOS"
    LLenarCombo CboMarca, "mar_nombre", "mar_id", "par_marcas", "mar_activo='SI' AND rut_emp='" & SP_Rut_Activo & "'"
    CboMarca.AddItem "TODOS"
    SG_codigo2 = ""
End Sub
Private Sub Salir()
   ' Unload Me
End Sub




Private Sub LvDetalle_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
     ordListView ColumnHeader, Me, LvDetalle
End Sub



Private Sub LvInmo_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
 ordListView ColumnHeader, Me, LvInmo
End Sub

Private Sub Option1_Click()
    DTInicio.Enabled = False
    DtHasta.Enabled = False
End Sub
Private Sub Option2_Click()
    DTInicio.Enabled = True
    DtHasta.Enabled = True
End Sub
Private Sub CargaArticulos()


    If Me.ChkTipo.Value = 0 Then
        Sql = "SELECT v.codigo,v.descripcion,v.marca,tip_nombre,sum(unidades) vendidos,SUM(subtotal) pventa," & _
                    "SUM(precio_costo) compra,sum(subtotal)-sum(precio_costo) diferencia " & _
               "FROM ven_detalle v " & _
               "INNER JOIN maestro_productos p USING(codigo) " & _
               "INNER JOIN sis_documentos d USING(doc_id) " & _
               "INNER JOIN ven_doc_venta r USING(doc_id,no_documento), " & _
                    "par_tipos_productos t " & _
               "WHERE doc_nota_de_credito='NO' AND v.codigo<>0 AND doc_nota_de_venta='NO' AND d.doc_orden_de_compra='NO' AND v.rut_emp='" & SP_Rut_Activo & "' AND p.tip_id = t.tip_id " & Sm_FiltroCodigo & sm_FiltroFecha & Sm_FiltroMarca & Sm_FiltroTipo & " " & _
               "GROUP BY v.codigo"
               
               
               
                  Sql = "SELECT v.codigo,v.descripcion,v.marca,tip_nombre,sum(unidades) vendidos,  sum( subtotal-(" & _
"subtotal*((select (ven_descuento_valor-ven_ajuste_recargo+ven_ajuste_descuento)/bruto *100 xcien " & _
"from ven_doc_venta x " & _
"where x.no_documento=v.no_documento AND v.doc_id=x.doc_id)/100))) pventa," & _
                    "SUM(precio_costo) compra /* sum(subtotal)-sum(precio_costo) diferencia */" & _
               "FROM ven_detalle v " & _
               "INNER JOIN maestro_productos p USING(codigo) " & _
               "INNER JOIN sis_documentos d USING(doc_id) " & _
               "INNER JOIN ven_doc_venta r USING(doc_id,no_documento), " & _
                    "par_tipos_productos t " & _
               "WHERE doc_nota_de_credito='NO' AND v.codigo<>0 AND doc_nota_de_venta='NO' AND d.doc_orden_de_compra='NO' AND v.rut_emp='" & SP_Rut_Activo & "' AND p.tip_id = t.tip_id " & Sm_FiltroCodigo & sm_FiltroFecha & Sm_FiltroMarca & Sm_FiltroTipo & " " & _
               "GROUP BY v.codigo"
               
               
               'DE AQUI EN ADELANTE
               
                Sql = "SELECT v.codigo,v.descripcion,v.marca,tip_nombre,sum(unidades) vendidos,"
                Sql = Sql & " SUM(subtotal - (subtotal /100 * ((SELECT (ven_descuento_valor - ven_ajuste_recargo + ven_ajuste_descuento) " & _
                        "FROM ven_doc_venta x " & _
                        "WHERE   x.no_documento = v.no_documento AND v.doc_id = x.doc_id LIMIT 1) " & _
                        "/ " & _
                        "(SELECT (bruto)+(ven_descuento_valor - ven_ajuste_recargo + ven_ajuste_descuento) " & _
                        "FROM ven_doc_venta x " & _
                        "WHERE x.no_documento = V.no_documento " & _
                        "AND v.doc_id = x.doc_id LIMIT 1 " & _
                        ") *100))) pventa,"
                Sql = Sql & "SUM(precio_costo) compra /* sum(subtotal)-sum(precio_costo) diferencia */" & _
               "FROM ven_detalle v " & _
               "INNER JOIN maestro_productos p USING(codigo) " & _
               "INNER JOIN sis_documentos d USING(doc_id) " & _
               "INNER JOIN ven_doc_venta r USING(doc_id,no_documento), " & _
                    "par_tipos_productos t " & _
               "WHERE doc_informa_venta='SI' AND doc_nota_de_credito='NO' AND v.codigo<>0 AND doc_nota_de_venta='NO' AND d.doc_orden_de_compra='NO' AND v.rut_emp='" & SP_Rut_Activo & "' AND p.tip_id = t.tip_id " & Sm_FiltroCodigo & sm_FiltroFecha & Sm_FiltroMarca & Sm_FiltroTipo & " " & _
               "GROUP BY v.codigo"
                          
               
               
    Else
        'Agrupacion por
        If Me.CboAgrupa.Text = "TIPO" Then
            Sql = "SELECT v.codigo,'AGRUPADO POR TIPO','',tip_nombre,sum(unidades) vendidos,"
            'sum( subtotal-(" & _
"subtotal*((select (ven_descuento_valor-ven_ajuste_recargo+ven_ajuste_descuento)/bruto *100 xcien " & _
"from ven_doc_venta x " & _
"where x.no_documento=v.no_documento AND v.doc_id=x.doc_id)/100))) pventa,"
            Sql = Sql & " SUM(subtotal - (subtotal /100 * ((SELECT (ven_descuento_valor - ven_ajuste_recargo + ven_ajuste_descuento) " & _
                        "FROM ven_doc_venta x " & _
                        "WHERE   x.no_documento = v.no_documento AND v.doc_id = x.doc_id LIMIT 1) " & _
                        "/ " & _
                        "(SELECT (bruto)+(ven_descuento_valor - ven_ajuste_recargo + ven_ajuste_descuento) " & _
                        "FROM ven_doc_venta x " & _
                        "WHERE x.no_documento = V.no_documento " & _
                        "AND v.doc_id = x.doc_id LIMIT 1 " & _
                        ") *100))) pventa,"


            
            
            Sql = Sql & "SUM(precio_costo) compra /*,sum(subtotal)-sum(precio_costo) diferencia */ " & _
               "FROM ven_detalle v " & _
               "INNER JOIN maestro_productos p USING(codigo) " & _
               "INNER JOIN sis_documentos d USING(doc_id) " & _
               "INNER JOIN ven_doc_venta r USING(doc_id,no_documento), " & _
                    "par_tipos_productos t " & _
               "WHERE doc_informa_venta='SI' AND doc_nota_de_credito='NO' AND v.codigo<>0 AND doc_nota_de_venta='NO' AND d.doc_orden_de_compra='NO' AND v.rut_emp='" & SP_Rut_Activo & "' AND p.tip_id = t.tip_id " & Sm_FiltroCodigo & sm_FiltroFecha & Sm_FiltroMarca & Sm_FiltroTipo & " " & _
               "GROUP BY p.tip_id"
        Else
                Sql = "SELECT v.codigo,'AGRUPADO POR MARCA',mar_nombre,'',sum(unidades) vendidos,"
                'sum( subtotal-(" & _
"subtotal*((select (ven_descuento_valor-ven_ajuste_recargo+ven_ajuste_descuento)/bruto *100 xcien " & _
"from ven_doc_venta x " & _
"where x.no_documento=v.no_documento AND v.doc_id=x.doc_id)/100))) pventa,"
            Sql = Sql & " SUM(subtotal - (subtotal /100 * ((SELECT (ven_descuento_valor - ven_ajuste_recargo + ven_ajuste_descuento) " & _
                        "FROM ven_doc_venta x " & _
                        "WHERE   x.no_documento = v.no_documento AND v.doc_id = x.doc_id LIMIT 1) " & _
                        "/ " & _
                        "(SELECT (bruto)+(ven_descuento_valor - ven_ajuste_recargo + ven_ajuste_descuento) " & _
                        "FROM ven_doc_venta x " & _
                        "WHERE x.no_documento = V.no_documento " & _
                        "AND v.doc_id = x.doc_id LIMIT 1 " & _
                        ") *100))) pventa,"

               Sql = Sql & "SUM(precio_costo) compra /* sum(subtotal)-sum(precio_costo) diferencia */ " & _
               "FROM ven_detalle v " & _
               "INNER JOIN maestro_productos p USING(codigo) " & _
               "INNER JOIN sis_documentos d USING(doc_id) " & _
               "JOIN par_marcas m ON p.mar_id=m.mar_id " & _
               "INNER JOIN ven_doc_venta r USING(doc_id,no_documento), " & _
                    "par_tipos_productos t " & _
               "WHERE doc_nota_de_credito='NO' AND v.codigo<>0 AND doc_nota_de_venta='NO' AND d.doc_orden_de_compra='NO' AND v.rut_emp='" & SP_Rut_Activo & "' AND p.tip_id = t.tip_id " & Sm_FiltroCodigo & sm_FiltroFecha & Sm_FiltroMarca & Sm_FiltroTipo & " " & _
               "GROUP BY p.mar_id"
        
        
        End If
    End If
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvDetalle, False, True, True, False


    TxtCosto = NumFormat(TotalizaColumna(LvDetalle, "costo"))
    TxtVenta = NumFormat(TotalizaColumna(LvDetalle, "venta"))
    txtDiferencia = NumFormat(TotalizaColumna(LvDetalle, "diferencia"))
End Sub
Private Sub CargaInmo()
    SkResultados = " .... BUSCANDO  ..."
    DoEvents
    Sql = "SELECT  p.codigo, pro_codigo_interno,  p.descripcion,  m.mar_nombre,   tip_nombre,"
    Sql = Sql & "(SELECT SUM(f.unidades) " & _
                    "FROM ven_detalle f " & _
                    "WHERE f.codigo=p.codigo " & _
                    "GROUP BY f.fecha " & _
                    "ORDER BY f.fecha DESC " & _
                    "LIMIT 1), "
    
    Sql = Sql & "(SELECT f.fecha " & _
                    "FROM ven_detalle f " & _
                    "WHERE f.codigo=p.codigo " & _
                    "ORDER BY f.fecha DESC " & _
                    "LIMIT 1),  "
                    
    Sql = Sql & "(SELECT sto_stock stock " & _
                        "FROM  pro_stock k " & _
                        "WHERE rut_emp='" & SP_Rut_Activo & "' AND k.pro_codigo=p.codigo AND bod_id= 1 " & _
                        "GROUP BY pro_codigo) stk, "
    Sql = Sql & "(SELECT kar_fecha FROM inv_kardex i " & _
                    "WHERE i.pro_codigo=p.codigo AND i.rut_emp='" & SP_Rut_Activo & "' /*AND kar_descripcion='KARDEX INICIAL'  */ LIMIT 1) kar_inicial, " & _
                    "(SELECT kar_descripcion FROM inv_kardex i " & _
                    "WHERE i.pro_codigo=p.codigo AND i.rut_emp='" & SP_Rut_Activo & "' /*AND kar_descripcion='KARDEX INICIAL'  */ LIMIT 1) descripcion_kardex "

     Sql = Sql & " " & _
                "FROM maestro_productos p , par_tipos_productos t,par_marcas m " & _
                "WHERE p.rut_emp = '" & SP_Rut_Activo & "' AND p.tip_id = t.tip_id AND p.mar_id=m.mar_id AND codigo not in("


        Sql = Sql & "SELECT v.codigo " & _
               "FROM ven_detalle v " & _
               "INNER JOIN maestro_productos p USING(codigo) " & _
               "INNER JOIN sis_documentos d USING(doc_id) " & _
               "INNER JOIN ven_doc_venta r USING(doc_id,no_documento) " & _
               "WHERE v.codigo<>0 AND doc_nota_de_venta='NO' AND d.doc_orden_de_compra='NO' AND v.rut_emp='" & SP_Rut_Activo & "' " & Sm_FiltroCodigo & sm_FiltroFecha & Sm_FiltroMarca & Sm_FiltroTipo & " " & _
               "GROUP BY v.codigo) " & SG_codigo2
        Consulta RsTmp, Sql
        LLenar_Grilla RsTmp, Me, LvInmo, False, True, True, False
        SkResultados = RsTmp.RecordCount & " Resultados encontrados..."
End Sub
Private Sub Timer1_Timer()
    cmdConsultar.SetFocus
    Timer1.Enabled = False
End Sub
Private Sub TxtCodigo_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 And Len(TxtCodigo.Text) > 0 Then SendKeys "{Tab}"
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
    If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtCodigo_Validate(Cancel As Boolean)
    If Len(TxtCodigo) = 0 Then
        TxtDescrp = Empty
        TxtMarca = Empty
        Exit Sub
    End If
    
    Sql = "SELECT descripcion,marca,precio_compra,stock_actual,tip_nombre familia " & _
          "FROM maestro_productos m,par_tipos_productos t " & _
          "WHERE m.rut_emp='" & SP_Rut_Activo & "' AND m.tip_id=t.tip_id AND codigo='" & TxtCodigo & "'"
    Call Consulta(RsTmp, Sql)
    If RsTmp.RecordCount > 0 Then
                'Codigo ingresado ha sido encontrado
            With RsTmp
                Me.TxtDescrp = !Descripcion
                Me.TxtMarca.Text = !MARCA
            End With
    Else
        TxtDescrp = Empty
        TxtMarca = Empty
    End If
End Sub
