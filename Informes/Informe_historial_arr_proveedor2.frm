VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{28D47522-CF84-11D1-834C-00A0249F0C28}#1.0#0"; "gif89.dll"
Object = "{DA729E34-689F-49EA-A856-B57046630B73}#1.0#0"; "progressbar-xp.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form Informe_historial_arr_proveedor 
   Caption         =   "Informe Historial de Maquinarias por Cliente"
   ClientHeight    =   8070
   ClientLeft      =   7980
   ClientTop       =   4170
   ClientWidth     =   18075
   LinkTopic       =   "Form1"
   ScaleHeight     =   8070
   ScaleWidth      =   18075
   Begin VB.Timer Timer1 
      Interval        =   500
      Left            =   0
      Top             =   0
   End
   Begin VB.Frame FrmLoad 
      Height          =   1680
      Left            =   5610
      TabIndex        =   24
      Top             =   2865
      Visible         =   0   'False
      Width           =   2805
      Begin GIF89LibCtl.Gif89a Gif89a1 
         Height          =   1275
         Left            =   210
         OleObjectBlob   =   "Informe_historial_arr_proveedor2.frx":0000
         TabIndex        =   25
         Top             =   285
         Width           =   2445
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Maquinas Arrendadas por Cliente"
      Height          =   7650
      Left            =   30
      TabIndex        =   0
      Top             =   75
      Width           =   17700
      Begin ACTIVESKINLibCtl.SkinLabel SkObra 
         Height          =   240
         Left            =   330
         OleObjectBlob   =   "Informe_historial_arr_proveedor2.frx":0086
         TabIndex        =   23
         Top             =   1095
         Width           =   1740
      End
      Begin VB.ComboBox CboObras 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   330
         Style           =   2  'Dropdown List
         TabIndex        =   22
         Top             =   1350
         Visible         =   0   'False
         Width           =   4965
      End
      Begin VB.Frame Frame2 
         Caption         =   "Por rangos de fecha"
         Height          =   1365
         Left            =   8580
         TabIndex        =   16
         Top             =   180
         Width           =   2925
         Begin VB.CommandButton CmdRango 
            Caption         =   "Consultar"
            Height          =   480
            Left            =   660
            TabIndex        =   21
            Top             =   825
            Width           =   1740
         End
         Begin VB.CheckBox ChkFecha 
            Height          =   225
            Left            =   165
            TabIndex        =   17
            Top             =   450
            Width           =   225
         End
         Begin MSComCtl2.DTPicker DtDesde 
            CausesValidation=   0   'False
            Height          =   285
            Left            =   435
            TabIndex        =   18
            Top             =   420
            Width           =   1215
            _ExtentX        =   2143
            _ExtentY        =   503
            _Version        =   393216
            Enabled         =   0   'False
            Format          =   95289345
            CurrentDate     =   41001
         End
         Begin MSComCtl2.DTPicker DtHasta 
            Height          =   285
            Left            =   1635
            TabIndex        =   19
            Top             =   420
            Width           =   1200
            _ExtentX        =   2117
            _ExtentY        =   503
            _Version        =   393216
            Enabled         =   0   'False
            Format          =   95289345
            CurrentDate     =   41001
         End
      End
      Begin VB.CommandButton CmdEstadoDeCuenta 
         Caption         =   "Exportar para cliente"
         Height          =   420
         Left            =   11580
         TabIndex        =   14
         Top             =   6990
         Width           =   1710
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkTotal 
         Height          =   240
         Left            =   6300
         OleObjectBlob   =   "Informe_historial_arr_proveedor2.frx":00E4
         TabIndex        =   13
         Top             =   6915
         Width           =   1260
      End
      Begin VB.TextBox TxtTotal 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   7680
         Locked          =   -1  'True
         TabIndex        =   12
         Top             =   6870
         Width           =   1485
      End
      Begin VB.Frame Frame4 
         Caption         =   "Mes y A�o"
         Height          =   1395
         Left            =   5535
         TabIndex        =   8
         Top             =   165
         Width           =   2970
         Begin VB.CommandButton CmdRut 
            Caption         =   "Consultar"
            Height          =   480
            Left            =   675
            TabIndex        =   20
            Top             =   810
            Width           =   1740
         End
         Begin VB.ComboBox comMes 
            Height          =   315
            ItemData        =   "Informe_historial_arr_proveedor2.frx":014C
            Left            =   135
            List            =   "Informe_historial_arr_proveedor2.frx":014E
            Style           =   2  'Dropdown List
            TabIndex        =   10
            Top             =   450
            Width           =   1575
         End
         Begin VB.ComboBox ComAno 
            Height          =   315
            Left            =   1710
            Style           =   2  'Dropdown List
            TabIndex        =   9
            Top             =   435
            Width           =   1215
         End
      End
      Begin VB.TextBox TxtRazonSocial 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0C0C0&
         BorderStyle     =   0  'None
         Height          =   285
         Left            =   300
         TabIndex        =   7
         ToolTipText     =   "Ingrese el RUT sin puntos ni guion."
         Top             =   735
         Width           =   4980
      End
      Begin VB.CommandButton CmdBuscaCliente 
         Caption         =   "Buscar Cliente"
         Height          =   345
         Left            =   240
         TabIndex        =   6
         Top             =   360
         Width           =   1260
      End
      Begin VB.TextBox TxtRut 
         Height          =   345
         Left            =   1545
         TabIndex        =   5
         Top             =   360
         Width           =   1605
      End
      Begin VB.CommandButton CmdExportar 
         Caption         =   "Exportar a Excel"
         Height          =   450
         Left            =   13605
         TabIndex        =   2
         Top             =   6960
         Width           =   1395
      End
      Begin VB.CommandButton Command2 
         Caption         =   "Retornar"
         Height          =   450
         Left            =   15120
         TabIndex        =   1
         Top             =   6960
         Width           =   1380
      End
      Begin Proyecto2.XP_ProgressBar BarraProgreso 
         Height          =   495
         Left            =   4425
         TabIndex        =   3
         Top             =   3315
         Visible         =   0   'False
         Width           =   8460
         _ExtentX        =   14923
         _ExtentY        =   873
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BrushStyle      =   0
         Color           =   16777088
         Scrolling       =   1
         ShowText        =   -1  'True
      End
      Begin MSComctlLib.ListView LvMaquinas 
         Height          =   5100
         Left            =   165
         TabIndex        =   4
         Top             =   1710
         Width           =   17295
         _ExtentX        =   30506
         _ExtentY        =   8996
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   11
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Numero Contrato"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "T1000"
            Text            =   "Codigo"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "F1000"
            Text            =   "Fecha Inicio"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Object.Tag             =   "F1000"
            Text            =   "Fecha T�rmino"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Object.Tag             =   "N109"
            Text            =   "Dias"
            Object.Width           =   1411
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   5
            Object.Tag             =   "N100"
            Text            =   "Valor Neto"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   6
            Key             =   "total"
            Object.Tag             =   "N100"
            Text            =   "Total"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   7
            Object.Tag             =   "T4000"
            Text            =   "Descripcion"
            Object.Width           =   7056
         EndProperty
         BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   8
            Object.Tag             =   "T1000"
            Text            =   "Estado"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   9
            Object.Tag             =   "T1000"
            Text            =   "Obra"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   2
            SubItemIndex    =   10
            Object.Tag             =   "N109"
            Text            =   "Nro Recepcion"
            Object.Width           =   2540
         EndProperty
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkSucursal 
         Height          =   375
         Left            =   10035
         OleObjectBlob   =   "Informe_historial_arr_proveedor2.frx":0150
         TabIndex        =   11
         Top             =   120
         Width           =   6705
      End
      Begin MSComctlLib.ListView LvEstadoCuenta 
         Height          =   2340
         Left            =   195
         TabIndex        =   15
         Top             =   4230
         Visible         =   0   'False
         Width           =   16350
         _ExtentX        =   28840
         _ExtentY        =   4128
         View            =   3
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   8
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "T1000"
            Text            =   "Cod Producto"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "T4000"
            Text            =   "Desc. Producto"
            Object.Width           =   7056
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Numero"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Object.Tag             =   "F1000"
            Text            =   "Desde"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Object.Tag             =   "F1000"
            Text            =   "Hasta"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Object.Tag             =   "N109"
            Text            =   "Dias"
            Object.Width           =   1411
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   6
            Object.Tag             =   "N100"
            Text            =   "Valor Neto"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   7
            Key             =   "total"
            Object.Tag             =   "N100"
            Text            =   "Total"
            Object.Width           =   2540
         EndProperty
      End
   End
End
Attribute VB_Name = "Informe_historial_arr_proveedor"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim Sm_Filtro_Obra As String
Private Sub ChkFecha_Click()
    On Error Resume Next
    If ChkFecha.Value = 1 Then
        comMes.Enabled = False
        ComAno.Enabled = False
        DtDesde.Enabled = True
        DtHasta.Enabled = True
        DtDesde.SetFocus
    Else
        comMes.SetFocus
        DtDesde.Enabled = False
        DtHasta.Enabled = False
        comMes.Enabled = True
        ComAno.Enabled = True
    End If
End Sub

Private Sub CmdBuscaCliente_Click()
    LlamaClienteDe = "VD"
    
    ClienteEncontrado = False
    BuscaCliente.Show 1
    TxtRut = SG_codigo
    TxtRut.SetFocus
End Sub

Private Sub CmdEstadoDeCuenta_Click()
    If LvMaquinas.ListItems.Count = 0 Then Exit Sub
    
    LvEstadoCuenta.ListItems.Clear

  '  LvEstadoCuenta.ListItems.Add , , "COMERCIAL DICOEM SPA"
  '  LvEstadoCuenta.ListItems.Add , , "PERIODO:" & comMes.Text & " " & ComAno.Text
  '  LvEstadoCuenta.ListItems.Add , , ""
  '  LvEstadoCuenta.ListItems(LvEstadoCuenta.ListItems.Count).SubItems(1) = "ESTADO DE CUENTA"
  '  LvEstadoCuenta.ListItems.Add , , ""
'    LvEstadoCuenta.ListItems.Add , , "Cod. Producto"
'    LvEstadoCuenta.ListItems(LvEstadoCuenta.ListItems.Count).SubItems(1) = "Desc. Producto"
'    LvEstadoCuenta.ListItems(LvEstadoCuenta.ListItems.Count).SubItems(2) = "Numero"
'    LvEstadoCuenta.ListItems(LvEstadoCuenta.ListItems.Count).SubItems(3) = "Desde"
'    LvEstadoCuenta.ListItems(LvEstadoCuenta.ListItems.Count).SubItems(4) = "Hasta"
'    LvEstadoCuenta.ListItems(LvEstadoCuenta.ListItems.Count).SubItems(5) = "Dias"
'    LvEstadoCuenta.ListItems(LvEstadoCuenta.ListItems.Count).SubItems(6) = "Unitario"
'    LvEstadoCuenta.ListItems(LvEstadoCuenta.ListItems.Count).SubItems(7) = "Total"
    For i = 1 To LvMaquinas.ListItems.Count
        LvEstadoCuenta.ListItems.Add , , LvMaquinas.ListItems(i).SubItems(1)
        LvEstadoCuenta.ListItems(LvEstadoCuenta.ListItems.Count).SubItems(1) = LvMaquinas.ListItems(i).SubItems(7)
        LvEstadoCuenta.ListItems(LvEstadoCuenta.ListItems.Count).SubItems(2) = LvMaquinas.ListItems(i)
        LvEstadoCuenta.ListItems(LvEstadoCuenta.ListItems.Count).SubItems(3) = LvMaquinas.ListItems(i).SubItems(2)
        LvEstadoCuenta.ListItems(LvEstadoCuenta.ListItems.Count).SubItems(4) = LvMaquinas.ListItems(i).SubItems(3)
        LvEstadoCuenta.ListItems(LvEstadoCuenta.ListItems.Count).SubItems(5) = LvMaquinas.ListItems(i).SubItems(4)
        LvEstadoCuenta.ListItems(LvEstadoCuenta.ListItems.Count).SubItems(6) = LvMaquinas.ListItems(i).SubItems(5)
        LvEstadoCuenta.ListItems(LvEstadoCuenta.ListItems.Count).SubItems(7) = LvMaquinas.ListItems(i).SubItems(6)
        If LvMaquinas.ListItems(i).SubItems(8) = "NULO" Then
                LvEstadoCuenta.ListItems(LvEstadoCuenta.ListItems.Count).SubItems(3) = "NULO"
                LvEstadoCuenta.ListItems(LvEstadoCuenta.ListItems.Count).SubItems(4) = "NULO"
        End If
        
        
    Next
    
        LvEstadoCuenta.ListItems.Add , , "fn"
    LvEstadoCuenta.ListItems(LvEstadoCuenta.ListItems.Count).SubItems(6) = "TOTAL NETO"
    LvEstadoCuenta.ListItems(LvEstadoCuenta.ListItems.Count).SubItems(7) = txtTotal
    
    bruto = CDbl(txtTotal) * Val("1.19")
    Iva = NumFormat(bruto - CDbl(txtTotal))
    bruto = NumFormat(bruto)
   ' LvEstadoCuenta.ListItems.Add , , "fn"
    'LvEstadoCuenta.ListItems(LvEstadoCuenta.ListItems.Count).SubItems(6) = "TOTAL NTO"
   ' LvEstadoCuenta.ListItems(LvEstadoCuenta.ListItems.Count).SubItems(7) = TxtTotal
    LvEstadoCuenta.ListItems.Add , , "fn"
    LvEstadoCuenta.ListItems(LvEstadoCuenta.ListItems.Count).SubItems(6) = "IVA"
    LvEstadoCuenta.ListItems(LvEstadoCuenta.ListItems.Count).SubItems(7) = Iva
    LvEstadoCuenta.ListItems.Add , , "fn"
    LvEstadoCuenta.ListItems(LvEstadoCuenta.ListItems.Count).SubItems(6) = "TOTAL"
    LvEstadoCuenta.ListItems(LvEstadoCuenta.ListItems.Count).SubItems(7) = bruto
    LvEstadoCuenta.ListItems.Add , , "nd"
    LvEstadoCuenta.ListItems.Add , , "nd"
    LvEstadoCuenta.ListItems.Add , , "nd"
     LvEstadoCuenta.ListItems(LvEstadoCuenta.ListItems.Count).SubItems(1) = "DICOEM SPA"
    LvEstadoCuenta.ListItems.Add , , "fn"
     LvEstadoCuenta.ListItems(LvEstadoCuenta.ListItems.Count).SubItems(1) = SP_Rut_Activo
     
         Dim tit(2) As String
    If LvEstadoCuenta.ListItems.Count = 0 Then Exit Sub
    BarraProgreso.Visible = True
    tit(0) = "COMERCIAL DICOEM SPA"
    tit(1) = comMes.Text & " " & ComAno.Text
    tit(2) = ""

    
    ExportarNuevoDicoem LvEstadoCuenta, tit, Me, BarraProgreso
    BarraProgreso.Visible = False
End Sub

Private Sub CmdExportar_Click()
 Dim tit(2) As String
    If LvMaquinas.ListItems.Count = 0 Then Exit Sub
    BarraProgreso.Visible = True
    tit(0) = Me.Caption & " " & Date & " - " & Time
    tit(1) = Me.TxtRazonSocial & " " & Me.TxtRut
    tit(2) = ""
    LvMaquinas.ListItems.Add , , "fn"
    LvMaquinas.ListItems(LvMaquinas.ListItems.Count).SubItems(5) = "TOTAL"
    LvMaquinas.ListItems(LvMaquinas.ListItems.Count).SubItems(6) = txtTotal
    
    ExportarNuevo LvMaquinas, tit, Me, BarraProgreso
    BarraProgreso.Visible = False
End Sub

Private Sub CmdRango_Click()
    Dim Sp_Fecha As String
    Dim Sp_FiltroSueId As String
    Dim Sp_UltimoDia As Date
    Dim Sp_PrimerDia As Date
    
    If Len(TxtRut) = 0 Then Exit Sub
   ' ULTIMODIA = Fql(ComAno.Text & "-" & Me.comMes.ItemData(comMes.ListIndex) & "01")
    Sp_UltimoDia = UltimoDiaMes(ComAno.Text & "-" & Me.comMes.ItemData(comMes.ListIndex) & "-01")
    Sp_PrimerDia = ComAno.Text & "-" & comMes.ItemData(comMes.ListIndex) & "-01"
    Sp_FiltroSueId = " AND n.sue_id=" & LogSueID & " AND m.sue_id= " & LogSueID
    If ChkFecha.Value = 1 Then
        Sp_Fecha = " AND m.arr_fecha_contrato BETWEEN '" & Fql(DtDesde.Value) & "' AND '" & Fql(DtHasta.Value) & "' "
    Else
        Sp_Fecha = " AND MONTH(m.arr_fecha_contrato)=" & comMes.ItemData(comMes.ListIndex) & " AND YEAR(m.arr_fecha_contrato)=" & IIf(ComAno.ListCount = 0, Year(Date), ComAno.Text)
    End If



   Sql = "SELECT m.arr_id,det_arr_codigo_interno,m.arr_fecha_contrato,n.det_arr_fecha_recepcion termino," & _
                "0 as dias," & _
                 "det_arr_total,0 total," & _
                  " " & _
                "det_arr_descripcion,IF(ISNULL(n.det_arr_fecha_recepcion),'VIGENTE','FINALIZADO'),m.arr_direccion_obra " & _
            "FROM ven_arr_detalle n " & _
    " INNER JOIN ven_arriendo m " & _
    " WHERE arr_estado<>'NULO' AND   m.arr_rut_cliente='" & Me.TxtRut & "' AND n.arr_id=m.arr_id " & Sp_Fecha & " " & Sp_FiltroSueId & " " & _
    "  "
        Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvMaquinas, False, True, True, False
    
End Sub

Private Sub CmdRut_Click()
    Dim Sp_Fecha As String
    Dim Sp_FiltroSueId As String
    Dim Sp_UltimoDia As Date
    Dim Sp_PrimerDia As Date
  
    FrmLoad.Visible = True
   ' ULTIMODIA = Fql(ComAno.Text & "-" & Me.comMes.ItemData(comMes.ListIndex) & "01")
    Sp_UltimoDia = UltimoDiaMes(ComAno.Text & "-" & Me.comMes.ItemData(comMes.ListIndex) & "-01")
    Sp_PrimerDia = ComAno.Text & "-" & comMes.ItemData(comMes.ListIndex) & "-01"
    Sp_FiltroSueId = " AND n.sue_id=" & LogSueID & " AND m.sue_id= " & LogSueID
    If ChkFecha.Value = 1 Then
        Sp_Fecha = " AND m.arr_fecha_contrato BETWEEN '" & Fql(DtDesde.Value) & "' AND '" & Fql(DtHasta.Value) & "' "
    Else
        Sp_Fecha = " AND MONTH(m.arr_fecha_contrato)=" & comMes.ItemData(comMes.ListIndex) & " AND YEAR(m.arr_fecha_contrato)=" & IIf(ComAno.ListCount = 0, Year(Date), ComAno.Text)
    End If
    
    Sm_Filtro_Obra = ""
    If Len(SkObra) > 0 Then
        Sm_Filtro_Obra = " AND arr_direccion_obra= '" & SkObra & "'"
    End If
   ' If CboObras.ListCount > 0 Then
   '     If CboObras.Text = "TODAS" Then
   '         Sm_Filtro_Obra = ""
   '     Else
   '         Sm_Filtro_Obra = " AND arr_direccion_obra= '" & CboObras.Text & "'"
   '     End If
   '
   ' Else
   '     Sm_Filtro_Obra = ""
   ''
  '  End If


   Sql = "SELECT m.arr_id,det_arr_codigo_interno,m.arr_fecha_contrato,IF(ISNULL(n.det_arr_fecha_recepcion),'" & Fql(Sp_UltimoDia) & "',IF(n.det_arr_fecha_recepcion>'" & Fql(Sp_UltimoDia) & "','" & Fql(Sp_UltimoDia) & "',n.det_arr_fecha_recepcion )) termino," & _
                "datediff(IF(ISNULL(n.det_arr_fecha_recepcion),'" & Fql(Sp_UltimoDia) & "',IF(n.det_arr_fecha_recepcion>'" & Fql(Sp_UltimoDia) & "','" & Fql(Sp_UltimoDia) & "',n.det_arr_fecha_recepcion)   ),m.arr_fecha_contrato)+1 as dias," & _
                 "det_arr_total,((datediff(IF(ISNULL(n.det_arr_fecha_recepcion),'" & Fql(Sp_UltimoDia) & "',IF(n.det_arr_fecha_recepcion>'" & Fql(Sp_UltimoDia) & "','" & Fql(Sp_UltimoDia) & "',n.det_arr_fecha_recepcion)),m.arr_fecha_contrato)+1)    *det_arr_total) as total," & _
                  " " & _
                "det_arr_descripcion,IF(ISNULL(n.det_arr_fecha_recepcion),'VIGENTE','FINALIZADO'),m.arr_direccion_obra,det_arr_num_recepcion " & _
            "FROM ven_arr_detalle n " & _
    " INNER JOIN ven_arriendo m " & _
    " WHERE det_arr_codigo_interno<>'OTRO' AND arr_estado<>'NULO' AND   m.arr_rut_cliente='" & Me.TxtRut & "' AND n.arr_id=m.arr_id " & Sp_Fecha & " " & Sp_FiltroSueId & " " & Sm_Filtro_Obra & _
    "  "
  
  
  '01-02-2018 SE AGREGAN LOS REZAGADOS DE OTROS MESES SIN CERRAR
    Sql = Sql & " UNION SELECT m.arr_id,det_arr_codigo_interno,'" & Fql(Sp_PrimerDia) & "','" & Fql(Sp_UltimoDia) & "'," & _
                    "datediff('" & Fql(Sp_UltimoDia) & "','" & Fql(Sp_PrimerDia) & "') +1  as dias,det_arr_total," & _
                    "(datediff('" & Fql(Sp_UltimoDia) & "','" & Fql(Sp_PrimerDia) & "') +1 )*det_arr_total AS total," & _
                    "det_arr_descripcion,'VIGENTE',m.arr_direccion_obra,det_arr_num_recepcion " & _
            "FROM ven_arr_detalle n " & _
    " INNER JOIN ven_arriendo m " & _
    " WHERE  det_arr_codigo_interno<>'OTRO' AND  arr_estado<>'NULO' AND  ISNULL(n.det_arr_fecha_recepcion) AND m.arr_rut_cliente='" & Me.TxtRut & "' AND n.arr_id=m.arr_id AND m.arr_fecha_contrato< '" & ComAno.Text & "-" & comMes.ItemData(comMes.ListIndex) & "-01' " & Sp_FiltroSueId & " " & Sm_Filtro_Obra & _
    "  "
  
    '01-02-2018 SE AGREGAN LOS REZAGADOS DE OTROS CERRADOS EN EL MES ACTUAL
    sql2 = " UNION SELECT m.arr_id,det_arr_codigo_interno,'" & Fql(Sp_PrimerDia) & "',n.det_arr_fecha_recepcion," & _
                        "datediff(n.det_arr_fecha_recepcion,'" & Fql(Sp_PrimerDia) & "') +1  as dias,det_arr_total," & _
                "(datediff(n.det_arr_fecha_recepcion,'" & Fql(Sp_PrimerDia) & "') +1 )*det_arr_total AS total," & _
                "det_arr_descripcion,'FINALIZADO',m.arr_direccion_obra,det_arr_num_recepcion " & _
            "FROM ven_arr_detalle n " & _
    " INNER JOIN ven_arriendo m " & _
    " WHERE  det_arr_codigo_interno<>'OTRO' AND  arr_estado<>'NULO' AND  n.det_arr_fecha_recepcion BETWEEN '" & Fql(Sp_PrimerDia) & "' AND '" & Fql(Sp_UltimoDia) & "' AND m.arr_rut_cliente='" & Me.TxtRut & "' AND n.arr_id=m.arr_id AND m.arr_fecha_contrato< '" & ComAno.Text & "-" & comMes.ItemData(comMes.ListIndex) & "-01' " & Sp_FiltroSueId & " " & Sm_Filtro_Obra & _
    "  "
  
   'Vigente en el mes, pero cerrado mas adelante
   '9 May 2018
    
        SQL4 = " UNION SELECT m.arr_id,det_arr_codigo_interno,'" & Fql(Sp_PrimerDia) & "','" & Fql(Sp_UltimoDia) & "'," & _
                        "datediff('" & Fql(Sp_UltimoDia) & "','" & Fql(Sp_PrimerDia) & "') +1  as dias,det_arr_total," & _
                "(datediff('" & Fql(Sp_UltimoDia) & "','" & Fql(Sp_PrimerDia) & "') +1 )*det_arr_total AS total," & _
                "det_arr_descripcion,'FINALIZADO',m.arr_direccion_obra,det_arr_num_recepcion " & _
            "FROM ven_arr_detalle n " & _
    " INNER JOIN ven_arriendo m " & _
    " WHERE  det_arr_codigo_interno<>'OTRO' AND  arr_estado<>'NULO' AND  n.det_arr_fecha_recepcion > '" & Fql(Sp_UltimoDia) & "' AND m.arr_rut_cliente='" & Me.TxtRut & "' AND n.arr_id=m.arr_id AND m.arr_fecha_contrato< '" & ComAno.Text & "-" & comMes.ItemData(comMes.ListIndex) & "-01' " & Sp_FiltroSueId & " " & Sm_Filtro_Obra & _
    "  "
    
    
    'nulos
    
   Sql3 = "UNION SELECT m.arr_id,det_arr_codigo_interno,'',''," & _
                    "0 as dias,0 valor,0 as total, " & _
                "det_arr_descripcion,m.arr_estado,m.arr_direccion_obra,det_arr_num_recepcion " & _
            "FROM ven_arr_detalle n " & _
    " INNER JOIN ven_arriendo m " & _
    " WHERE arr_estado='NULO' AND   m.arr_rut_cliente='" & Me.TxtRut & "' AND n.arr_id=m.arr_id " & Sp_Fecha & " " & Sp_FiltroSueId & " " & Sm_Filtro_Obra & _
    "  "
  
    Sql = Sql & sql2 & SQL4 & Sql3
    
    
    'Tambien se debe agregar los OTRO, que no se multiplican
    '30 Octubre 2018
    
    '****************************************************************************************************************************************
       Sql = Sql & " UNION SELECT m.arr_id,det_arr_codigo_interno,m.arr_fecha_contrato,IF(ISNULL(n.det_arr_fecha_recepcion),'" & Fql(Sp_UltimoDia) & "',IF(n.det_arr_fecha_recepcion>'" & Fql(Sp_UltimoDia) & "','" & Fql(Sp_UltimoDia) & "',n.det_arr_fecha_recepcion )) termino," & _
                "1 as dias," & _
                 "det_arr_total,det_arr_total as total," & _
                  " " & _
                "det_arr_descripcion,IF(ISNULL(n.det_arr_fecha_recepcion),'VIGENTE','FINALIZADO'),m.arr_direccion_obra,det_arr_num_recepcion " & _
            "FROM ven_arr_detalle n " & _
    " INNER JOIN ven_arriendo m " & _
    " WHERE det_arr_codigo_interno='OTRO' AND arr_estado<>'NULO' AND   m.arr_rut_cliente='" & Me.TxtRut & "' AND n.arr_id=m.arr_id " & Sp_Fecha & " " & Sp_FiltroSueId & " " & Sm_Filtro_Obra & _
    "  "
  
  
  '01-02-2018 SE AGREGAN LOS REZAGADOS DE OTROS MESES SIN CERRAR
    Sql = Sql & " UNION SELECT m.arr_id,det_arr_codigo_interno,'" & Fql(Sp_PrimerDia) & "','" & Fql(Sp_UltimoDia) & "'," & _
                    "1  as dias,det_arr_total," & _
                    "det_arr_total AS total," & _
                    "det_arr_descripcion,'VIGENTE',m.arr_direccion_obra,det_arr_num_recepcion " & _
            "FROM ven_arr_detalle n " & _
    " INNER JOIN ven_arriendo m " & _
    " WHERE  det_arr_codigo_interno='OTRO' AND  arr_estado<>'NULO' AND  ISNULL(n.det_arr_fecha_recepcion) AND m.arr_rut_cliente='" & Me.TxtRut & "' AND n.arr_id=m.arr_id AND m.arr_fecha_contrato< '" & ComAno.Text & "-" & comMes.ItemData(comMes.ListIndex) & "-01' " & Sp_FiltroSueId & " " & Sm_Filtro_Obra & _
    "  "
  
    '01-02-2018 SE AGREGAN LOS REZAGADOS DE OTROS CERRADOS EN EL MES ACTUAL
    sql2 = " UNION SELECT m.arr_id,det_arr_codigo_interno,'" & Fql(Sp_PrimerDia) & "',n.det_arr_fecha_recepcion," & _
                        "1 dias,det_arr_total," & _
                "det_arr_total AS total," & _
                "det_arr_descripcion,'FINALIZADO',m.arr_direccion_obra,det_arr_num_recepcion " & _
            "FROM ven_arr_detalle n " & _
    " INNER JOIN ven_arriendo m " & _
    " WHERE  det_arr_codigo_interno='OTRO' AND  arr_estado<>'NULO' AND  n.det_arr_fecha_recepcion BETWEEN '" & Fql(Sp_PrimerDia) & "' AND '" & Fql(Sp_UltimoDia) & "' AND m.arr_rut_cliente='" & Me.TxtRut & "' AND n.arr_id=m.arr_id AND m.arr_fecha_contrato< '" & ComAno.Text & "-" & comMes.ItemData(comMes.ListIndex) & "-01' " & Sp_FiltroSueId & " " & Sm_Filtro_Obra & _
    "  "
  
   'Vigente en el mes, pero cerrado mas adelante
   '9 May 2018
    
        SQL4 = " UNION SELECT m.arr_id,det_arr_codigo_interno,'" & Fql(Sp_PrimerDia) & "','" & Fql(Sp_UltimoDia) & "'," & _
                        "1 dias,det_arr_total," & _
                "det_arr_total AS total," & _
                "det_arr_descripcion,'FINALIZADO',m.arr_direccion_obra,det_arr_num_recepcion " & _
            "FROM ven_arr_detalle n " & _
    " INNER JOIN ven_arriendo m " & _
    " WHERE  det_arr_codigo_interno='OTRO' AND  arr_estado<>'NULO' AND  n.det_arr_fecha_recepcion > '" & Fql(Sp_UltimoDia) & "' AND m.arr_rut_cliente='" & Me.TxtRut & "' AND n.arr_id=m.arr_id AND m.arr_fecha_contrato< '" & ComAno.Text & "-" & comMes.ItemData(comMes.ListIndex) & "-01' " & Sp_FiltroSueId & " " & Sm_Filtro_Obra & _
    "  "
    
    Sql = Sql & sql2 & SQL4
        
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvMaquinas, False, True, True, False
    txtTotal = NumFormat(TotalizaColumna(LvMaquinas, "total"))
    

   
    
    FrmLoad.Visible = False
End Sub

Private Sub Command1_Click()

End Sub

Private Sub Command2_Click()
Unload Me
End Sub

Private Sub Form_Load()
    Dim nombre As String
    Aplicar_skin Me
    Centrar Me
        DtDesde.Value = Date
    DtHasta.Value = Date
    Centrar Me
    SkEmpresaActiva = SP_Empresa_Activa
    Sp_Condicion = ""
    For i = 1 To 12
        comMes.AddItem UCase(MonthName(i, False))
        comMes.ItemData(comMes.ListCount - 1) = i
    Next
    Busca_Id_Combo comMes, Val(IG_Mes_Contable)
    LLenaYears ComAno, 2010
    
    Sql = "SELECT sue_ciudad,sue_direccion " & _
            "FROM sis_empresas_sucursales " & _
            "WHERE sue_id=" & LogSueID
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        SkSucursal = RsTmp!sue_ciudad & " - " & RsTmp!sue_direccion
        
    
    End If
    
End Sub

Private Sub LvMaquinas_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    ordListView ColumnHeader, Me, LvMaquinas
End Sub

Private Sub LvMaquinas_DblClick()
    If LvMaquinas.SelectedItem Is Nothing Then Exit Sub
    
    SkObra = LvMaquinas.SelectedItem.SubItems(9)
    CmdRut_Click
End Sub

Private Sub Timer1_Timer()
    On Error Resume Next
    TxtRut.SetFocus
    Timer1.Enabled = False
End Sub

Private Sub txtRut_Validate(Cancel As Boolean)
    If Len(TxtRut.Text) = 0 Then Exit Sub

    TxtRut.Text = Replace(TxtRut.Text, ".", "")
    TxtRut.Text = Replace(TxtRut.Text, "-", "")
    Respuesta = VerificaRut(TxtRut.Text, NuevoRut)
    Sm_Filtro_Obra = ""
    If Respuesta = True Then
        SkObra = ""
        Me.TxtRut.Text = NuevoRut
        TxtRut.Text = Replace(TxtRut.Text, ",", ".")
        'Buscar el cliente
        Sql = "SELECT rut_cliente,nombre_rsocial,giro,direccion,comuna,fono,email, " & _
                "IFNULL(lst_nombre,'LISTA PRECIO GENERAL') listaprecios,m.lst_id,ven_id,cli_monto_credito " & _
              "FROM maestro_clientes m " & _
              "INNER JOIN par_asociacion_ruts  a ON m.rut_cliente=a.rut_cli " & _
              "LEFT JOIN par_lista_precios l USING(lst_id) " & _
              "WHERE habilitado='SI' AND a.rut_emp='" & SP_Rut_Activo & "' AND rut_cliente = '" & Me.TxtRut.Text & "'"
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
                'Cliente encontrado
            With RsTmp
                TxtMontoCredito = !cli_monto_credito
                TxtListaPrecio = !ListaPrecios
''                TxtListaPrecio.Tag = !lst_id
                TxtRut.Text = !rut_cliente
                TxtGiro = !giro
                TxtDireccion = !direccion
                txtComuna = !comuna
                txtFono = !fono
                TxtEmail = !Email
                TxtRazonSocial.Text = !nombre_rsocial
               
                ClienteEncontrado = True
              
                
                Sql = "SELECT a.lst_id,lst_nombre " & _
                      "FROM par_asociacion_lista_precios  a " & _
                      "INNER JOIN par_lista_precios l USING(lst_id) " & _
                      "WHERE l.rut_emp='" & SP_Rut_Activo & "' AND cli_rut='" & TxtRut & "'"
                Consulta RsTmp2, Sql
           ''     TxtListaPrecio.Tag = !lst_id
        ''        TxtListaPrecio = "LISTA DE PRECIOS PRINCIPAL"
        ''        If RsTmp2.RecordCount > 0 Then
         ''           TxtListaPrecio = RsTmp2!lst_nombre
         ''           TxtListaPrecio.Tag = RsTmp2!lst_id
        ''        End If
                
                If Val(TxtMontoCredito) > 0 Then
                    'Debemos consultar el saldo de credito disponible del cliente.
                    TxtCupoUtilizado = ConsultaSaldoCliente(TxtRut)
                    SkCupoCredito = NumFormat(Val(TxtMontoCredito) - Val(TxtCupoUtilizado))
                End If

                
                
                
                Sql = "SELECT arr_direccion_obra obra " & _
                        "FROM ven_arriendo " & _
                        "GROUP BY arr_direccion_obra"
                Consulta RsTmp, Sql
                If RsTmp.RecordCount > 0 Then
                    CboObras.Clear
                    RsTmp.MoveFirst
                    Do While Not RsTmp.EOF
                        CboObras.AddItem RsTmp!obra
                    
                    
                        RsTmp.MoveNext
                    Loop
                    CboObras.AddItem "TODAS"
                    CboObras.ListIndex = CboObras.ListCount - 1
                End If
            End With
                
            If bm_SoloVistaDoc Then Exit Sub
                                
        
        Else
                'Cliente no existe aun �crearlo??
                Respuesta = MsgBox("Cliente no encontrado..." & Chr(13) & _
                "     �Desea Crear?    ", vbYesNo + vbQuestion)
                If Respuesta = 6 Then
                    'crear
                    SG_codigo = ""
                    AccionCliente = 4
                    AgregoCliente.TxtRut.Text = Me.TxtRut.Text
                    
                    AgregoCliente.TxtRut.Locked = True
                    ClienteEncontrado = False
                    AgregoCliente.Timer1.Enabled = True
            
                    AgregoCliente.Show 1
                    If ClienteEncontrado = True Then
                        txtRut_Validate False
                       
                        TxtCodigo.SetFocus
                    Else
                        txtRut_Validate True
                    End If
                Else
                    'volver al rut
                    ClienteEncontrado = False
                    Me.TxtRut.Text = ""
                    On Error Resume Next
                    SendKeys "+{TAB}" ' Shift TAB retrocede al control anterior segun el orden del tabindex
                End If
        
       TxtRut.Text = Replace(TxtRut.Text, ",", ".")
        End If
    
    Else
        Me.TxtRut.Text = ""
        On Error Resume Next
        SendKeys "+{TAB}" ' Shift TAB retrocede al control anterior segun el orden del tabindex
    End If
    Exit Sub
CancelaImpesionFacturacion:
    'No imprime
    Unload Me
End Sub
