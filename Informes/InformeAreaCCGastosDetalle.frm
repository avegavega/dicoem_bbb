VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{DA729E34-689F-49EA-A856-B57046630B73}#1.0#0"; "progressbar-xp.ocx"
Object = "{0ECD9B60-23AA-11D0-B351-00A0C9055D8E}#6.0#0"; "MSHFLXGD.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form InformeAreaCCGastosDetalle 
   Caption         =   "Informe Areas - Centro Costo - con Detalle Items Gasto"
   ClientHeight    =   10245
   ClientLeft      =   1185
   ClientTop       =   195
   ClientWidth     =   13005
   LinkTopic       =   "Form1"
   ScaleHeight     =   10245
   ScaleWidth      =   13005
   Begin VB.Frame FraProgreso 
      Caption         =   "Progreso exportaci�n"
      Height          =   795
      Left            =   825
      TabIndex        =   33
      Top             =   6045
      Visible         =   0   'False
      Width           =   11430
      Begin Proyecto2.XP_ProgressBar BarraProgreso 
         Height          =   330
         Left            =   150
         TabIndex        =   34
         Top             =   315
         Width           =   11130
         _ExtentX        =   19632
         _ExtentY        =   582
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BrushStyle      =   0
         Color           =   16777088
         Scrolling       =   1
         ShowText        =   -1  'True
      End
   End
   Begin VB.CommandButton cmdExcel 
      Caption         =   "Exportar a excel"
      Height          =   315
      Left            =   720
      TabIndex        =   32
      Top             =   9360
      Width           =   2430
   End
   Begin VB.Frame Frame3 
      Caption         =   "Filtros"
      Height          =   1110
      Left            =   465
      TabIndex        =   21
      Top             =   60
      Width           =   12300
      Begin VB.ComboBox CboCentroCosto 
         Height          =   315
         Left            =   8655
         Style           =   2  'Dropdown List
         TabIndex        =   23
         Top             =   615
         Width           =   3150
      End
      Begin VB.ComboBox CboArea 
         Height          =   315
         Left            =   5520
         Style           =   2  'Dropdown List
         TabIndex        =   22
         Top             =   615
         Width           =   3150
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkCC 
         Height          =   180
         Left            =   8640
         OleObjectBlob   =   "InformeAreaCCGastosDetalle.frx":0000
         TabIndex        =   24
         Top             =   420
         Width           =   2055
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkArea 
         Height          =   180
         Left            =   5565
         OleObjectBlob   =   "InformeAreaCCGastosDetalle.frx":007C
         TabIndex        =   25
         Top             =   420
         Width           =   2055
      End
   End
   Begin VB.CommandButton cmdSalir 
      Cancel          =   -1  'True
      Caption         =   "&Retornar"
      Height          =   405
      Left            =   11325
      TabIndex        =   20
      Top             =   9780
      Width           =   1350
   End
   Begin VB.Timer Timer1 
      Interval        =   10
      Left            =   555
      Top             =   5595
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   -30
      OleObjectBlob   =   "InformeAreaCCGastosDetalle.frx":00E2
      Top             =   5505
   End
   Begin VB.Frame Frame4 
      Caption         =   "Informacion"
      Height          =   8460
      Left            =   465
      TabIndex        =   9
      Top             =   1275
      Width           =   12210
      Begin VB.OptionButton Option1 
         Caption         =   "Por Fecha"
         Height          =   240
         Left            =   9600
         TabIndex        =   19
         ToolTipText     =   "Incluye todos los documentos de compra"
         Top             =   210
         Width           =   1740
      End
      Begin VB.OptionButton Option2 
         Caption         =   "Por documento"
         Height          =   240
         Left            =   2325
         TabIndex        =   18
         Top             =   195
         Value           =   -1  'True
         Width           =   1725
      End
      Begin VB.CommandButton CmdBuscar 
         Caption         =   "Mostrar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   390
         Left            =   10275
         TabIndex        =   4
         Top             =   1905
         Width           =   1725
      End
      Begin VB.Frame Frame1 
         Caption         =   "Documento"
         Height          =   1410
         Left            =   255
         TabIndex        =   13
         Top             =   450
         Width           =   5280
         Begin VB.ComboBox CboTipoDoc 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            ItemData        =   "InformeAreaCCGastosDetalle.frx":0316
            Left            =   225
            List            =   "InformeAreaCCGastosDetalle.frx":0323
            Style           =   2  'Dropdown List
            TabIndex        =   0
            Top             =   495
            Width           =   2430
         End
         Begin VB.CommandButton CmdBuscaRut 
            Caption         =   "RUT"
            Height          =   240
            Left            =   3825
            TabIndex        =   2
            Top             =   285
            Width           =   1215
         End
         Begin VB.TextBox TxtRutProveedor 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   3825
            TabIndex        =   3
            Tag             =   "T"
            Top             =   480
            Width           =   1230
         End
         Begin VB.TextBox TxtRsocial 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   225
            Locked          =   -1  'True
            TabIndex        =   14
            TabStop         =   0   'False
            Top             =   1035
            Width           =   4845
         End
         Begin VB.TextBox TxtNDoc 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   2640
            TabIndex        =   1
            Tag             =   "N"
            Text            =   "0"
            Top             =   495
            Width           =   1215
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel10 
            Height          =   225
            Left            =   225
            OleObjectBlob   =   "InformeAreaCCGastosDetalle.frx":0348
            TabIndex        =   15
            Top             =   840
            Width           =   3180
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
            Height          =   225
            Index           =   0
            Left            =   2505
            OleObjectBlob   =   "InformeAreaCCGastosDetalle.frx":03B7
            TabIndex        =   16
            Top             =   285
            Width           =   1290
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
            Height          =   225
            Index           =   0
            Left            =   210
            OleObjectBlob   =   "InformeAreaCCGastosDetalle.frx":0428
            TabIndex        =   17
            Top             =   285
            Width           =   1575
         End
      End
      Begin VB.Frame Frame2 
         Caption         =   "Fechas"
         Height          =   1410
         Left            =   5535
         TabIndex        =   10
         Top             =   450
         Width           =   6480
         Begin VB.CommandButton cmdBtnRut 
            Caption         =   "Rut"
            Enabled         =   0   'False
            Height          =   225
            Left            =   2670
            TabIndex        =   29
            Top             =   240
            Width           =   585
         End
         Begin VB.TextBox txtCliente 
            BackColor       =   &H00E0E0E0&
            Enabled         =   0   'False
            Height          =   315
            Left            =   2670
            Locked          =   -1  'True
            TabIndex        =   28
            Top             =   990
            Width           =   3750
         End
         Begin VB.TextBox TxtRut 
            BackColor       =   &H0080FF80&
            Enabled         =   0   'False
            Height          =   315
            Left            =   2670
            TabIndex        =   27
            ToolTipText     =   "Ingrese el RUT sin puntos ni guion."
            Top             =   465
            Width           =   1560
         End
         Begin VB.CommandButton cmdSinRut 
            Caption         =   "X"
            Enabled         =   0   'False
            Height          =   195
            Left            =   3285
            TabIndex        =   26
            ToolTipText     =   "Limpiar RUT"
            Top             =   255
            Width           =   180
         End
         Begin ACTIVESKINLibCtl.SkinLabel skIni 
            Height          =   240
            Left            =   465
            OleObjectBlob   =   "InformeAreaCCGastosDetalle.frx":04A1
            TabIndex        =   11
            Top             =   585
            Width           =   795
         End
         Begin MSComCtl2.DTPicker DTInicio 
            Height          =   300
            Left            =   1305
            TabIndex        =   5
            Top             =   570
            Width           =   1305
            _ExtentX        =   2302
            _ExtentY        =   529
            _Version        =   393216
            Enabled         =   0   'False
            Format          =   95354881
            CurrentDate     =   40628
         End
         Begin MSComCtl2.DTPicker DtHasta 
            Height          =   285
            Left            =   1305
            TabIndex        =   6
            Top             =   870
            Width           =   1305
            _ExtentX        =   2302
            _ExtentY        =   503
            _Version        =   393216
            Enabled         =   0   'False
            Format          =   95354881
            CurrentDate     =   40628
         End
         Begin ACTIVESKINLibCtl.SkinLabel skFin 
            Height          =   240
            Left            =   465
            OleObjectBlob   =   "InformeAreaCCGastosDetalle.frx":0509
            TabIndex        =   12
            Top             =   855
            Width           =   795
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
            Height          =   225
            Left            =   2685
            OleObjectBlob   =   "InformeAreaCCGastosDetalle.frx":0571
            TabIndex        =   30
            Top             =   825
            Width           =   1560
         End
      End
      Begin MSComctlLib.ListView LvDetalle 
         Height          =   5670
         Left            =   240
         TabIndex        =   31
         Top             =   2370
         Width           =   11775
         _ExtentX        =   20770
         _ExtentY        =   10001
         View            =   3
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   5
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "T1000"
            Object.Width           =   529
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "T5000"
            Text            =   "Area /Centro de Costo / Gastos"
            Object.Width           =   8819
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   2
            Object.Tag             =   "N100"
            Text            =   "Valor"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Object.Tag             =   "T1300"
            Text            =   "Rut"
            Object.Width           =   2346
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Object.Tag             =   "T2000"
            Text            =   "Proveedor"
            Object.Width           =   3528
         EndProperty
      End
   End
   Begin MSHierarchicalFlexGridLib.MSHFlexGrid MSHFlexGrid1 
      Height          =   7770
      Left            =   11115
      TabIndex        =   8
      Top             =   8385
      Width           =   14265
      _ExtentX        =   25162
      _ExtentY        =   13705
      _Version        =   393216
      _NumberOfBands  =   1
      _Band(0).Cols   =   2
   End
   Begin MSHierarchicalFlexGridLib.MSHFlexGrid MSHFlexGrid2 
      Height          =   2940
      Left            =   -5205
      TabIndex        =   7
      Top             =   9165
      Width           =   11055
      _ExtentX        =   19500
      _ExtentY        =   5186
      _Version        =   393216
      _NumberOfBands  =   1
      _Band(0).Cols   =   2
   End
End
Attribute VB_Name = "InformeAreaCCGastosDetalle"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmdBtnRut_Click()
    ClienteEncontrado = False
    SG_codigo = Empty
    BuscaProveedor.Show 1
    TxtRut = SG_codigo
    If SG_codigo <> Empty Then
  
        TxtRut_Validate (True)
    End If
End Sub

Private Sub CmdBuscar_Click()
        Dim Sp_MasFiltro As String
        Filtro = Empty
        If Option2.Value Then 'Si es por documento
            If CboTipoDoc.ListIndex = -1 Then
                MsgBox "Seleccione documento...", vbInformation
                CboTipoDoc.SetFocus
                Exit Sub
            End If
            If Val(TxtNDoc) = 0 Then
                MsgBox "Falta Nro documento...", vbInformation
                TxtNDoc.SetFocus
                Exit Sub
            End If
            If Len(txtRutProveedor) = 0 Then
                MsgBox "RUT no v�lido...", vbInformation
                txtRutProveedor.SetFocus
                Exit Sub
            End If
            Filtro = "  c.rut_emp='" & SP_Rut_Activo & "' AND c.rut='" & txtRutProveedor & "' AND c.doc_id=" & CboTipoDoc.ItemData(CboTipoDoc.ListIndex) & " AND no_documento=" & TxtNDoc & " "
        Else
            Sp_MasFiltro = ""
            If Len(TxtRut) > 0 Then
                Sp_MasFiltro = " c.rut='" & TxtRut & "' AND "
            End If
            Filtro = Sp_MasFiltro & "  c.rut_emp='" & SP_Rut_Activo & "' AND  fecha BETWEEN '" & Format(DTInicio.Value, "YYYY-MM-DD") & "' AND '" & Format(DtHasta.Value, "YYYY-MM-DD") & "' "
        End If
        Filtro = Filtro & " AND x.doc_orden_de_compra='NO' "
        
        If CboCentroCosto.Text <> "TODOS" Then Filtro = Filtro & " AND d.cen_id=" & CboCentroCosto.ItemData(CboCentroCosto.ListIndex) & " "
        If CboArea.Text <> "TODOS" Then Filtro = Filtro & " AND d.are_id=" & CboArea.ItemData(CboArea.ListIndex) & " "
        
        If Option2.Value Then CargaReportPorDocumento
        If Option1.Value Then CargaReportPorFecha
    

End Sub

Private Sub CmdBuscaRut_Click()
    BuscaProveedor.Show 1
    txtRutProveedor = RutBuscado
    txtRutProveedor_Validate True
End Sub

Private Sub CmdExportar_Click()
    Dim tit(2) As String
    If LvDetalle.ListItems.Count = 0 Then Exit Sub
    Me.Frame1.Visible = True
    tit(0) = Me.Caption & " " & DtFecha & " - " & Time
    tit(1) = ""
    tit(2) = ""
    ExportarNuevo LvDetalle, tit, Me, BarraProgreso
       Me.Frame1.Visible = False
End Sub

Private Sub cmdExcel_Click()
    Dim tit(2) As String
    If LvDetalle.ListItems.Count = 0 Then Exit Sub
    FraProgreso.Visible = True
    tit(0) = Me.Caption & " " & DtFecha & " - " & Time
    tit(1) = ""
    tit(2) = ""
    ExportarNuevo LvDetalle, tit, Me, BarraProgreso
    FraProgreso.Visible = False
End Sub

Private Sub cmdSalir_Click()
    Unload Me
    
End Sub

Private Sub CargaReportPorFecha()
'       Sql = "SHAPE {SELECT  DISTINCT  d.are_id,are_nombre,c.neto total FROM com_doc_compra_detalle d INNER JOIN com_doc_compra c ON c.id=d.id   INNER JOIN sis_documentos x ON x.doc_id=c.doc_id  INNER JOIN par_areas a ON d.are_id=a.are_id WHERE " & Filtro & " GROUP BY d.are_id  } " & _
            "APPEND ((SHAPE {SELECT  DISTINCT d.are_id, d.cen_id,cen_nombre FROM com_doc_compra_detalle d INNER JOIN com_doc_compra c ON c.id=d.id  INNER JOIN sis_documentos x ON x.doc_id=c.doc_id  INNER JOIN par_centros_de_costo a ON d.cen_id=a.cen_id WHERE " & Filtro & " }  " & _
            "APPEND ({SELECT   d.are_id,d.cen_id,gas_nombre,SUM(cmd_total_neto) total_linea FROM com_doc_compra_detalle d INNER JOIN com_doc_compra c ON c.id=d.id   INNER JOIN sis_documentos x ON x.doc_id=c.doc_id INNER JOIN par_item_gastos a ON d.gas_id=a.gas_id WHERE " & Filtro & " GROUP BY d.are_id,d.cen_id,d.gas_id} AS itemgastos " & _
                "RELATE cen_id TO cen_id, are_id TO are_id)) AS centrocostos " & _
                    "RELATE are_id TO are_id)"


   'Sql = "SHAPE {SELECT  DISTINCT d.are_id,are_nombre FROM com_doc_compra_detalle d INNER JOIN com_doc_compra c ON c.id=d.id INNER JOIN sis_documentos x ON c.doc_id=x.doc_id INNER JOIN par_areas a ON d.are_id=a.are_id WHERE " & Filtro & "} " & _
             "APPEND ((SHAPE {SELECT DISTINCT d.are_id, d.cen_id,cen_nombre FROM com_doc_compra_detalle d INNER JOIN com_doc_compra c ON c.id=d.id INNER JOIN sis_documentos x ON c.doc_id=x.doc_id INNER JOIN par_centros_de_costo a ON d.cen_id=a.cen_id WHERE " & Filtro & "}  " & _
            "APPEND ({SELECT d.are_id,d.cen_id,gas_nombre,SUM(cmd_total_neto) total_linea FROM com_doc_compra_detalle d INNER JOIN com_doc_compra c ON c.id=d.id INNER JOIN sis_documentos x ON c.doc_id=x.doc_id INNER JOIN par_item_gastos a ON d.gas_id=a.gas_id WHERE " & Filtro & "GROUP BY d.cen_id,d.are_id} AS itemgastos " & _
                "RELATE cen_id TO cen_id, are_id TO are_id)) AS centrocostos " & _
                    "RELATE are_id TO are_id)"
 '  ConexionBdShape RsCom, Sql
    
   ' Set Me.MSHFlexGrid1.DataSource = RsCom
   ' For i = 1 To Me.MSHFlexGrid1.Cols - 1
   '      Me.MSHFlexGrid1.ColWidth(i) = 2000
   ' Next
   '
   ' Set DRAreaCCGastoGeneral.DataSource = RsCom
   ' DRAreaCCGastoGeneral.Sections("Cabeza").Controls("titulo").Caption = "AREAS,CENTROS DE COSTOS E ITEMS DE GASTOS DEL " & Me.DTInicio & " AL " & DtHasta.Value
   ' DRAreaCCGastoGeneral.Sections("Cabeza").Controls("etempresa").Caption = Principal.SkEmpresa
    'DRAreaCCGastoGeneral.Sections("Cabeza").Controls("etrut").Caption = SP_Rut_Activo
    'DRAreaCCGastoGeneral.Sections("Encabezado").Controls("EtRutp").Caption = TxtRut
   ' DRAreaCCGastoGeneral.Sections("Encabezado").Controls("Etnombrep").Caption = txtCliente
  '  DRAreaCCGastoGeneral.Show 1
    Dim Rp_Areas As Recordset
    Dim Rp_Centros As Recordset
    Dim Rp_Gastos As Recordset
    Dim L As Integer
    Dim Bp_1 As Boolean
    Sql = "SELECT d.are_id,a.are_nombre,SUM(d.cmd_total_neto) total_area " & _
            "FROM com_doc_compra_detalle d " & _
            "JOIN com_doc_compra c ON d.id=c.id " & _
            "JOIN par_areas a ON d.are_id=a.are_id " & _
            "JOIN sis_documentos x ON c.doc_id=x.doc_id " & _
            "WHERE " & Filtro & " " & _
            "GROUP BY d.are_id"
    Consulta Rp_Areas, Sql
    
    Sql = "SELECT d.are_id,d.cen_id,e.cen_nombre,SUM(d.cmd_total_neto) total_centro " & _
                "FROM com_doc_compra_detalle d " & _
                "JOIN com_doc_compra c ON d.id=c.id " & _
                "JOIN par_centros_de_costo e ON d.cen_id=e.cen_id " & _
                "JOIN sis_documentos x ON c.doc_id=x.doc_id " & _
                "WHERE " & Filtro & " " & _
                "GROUP BY d.are_id,d.cen_id"
    Consulta Rp_Centros, Sql
    
    Sql = "SELECT d.are_id,d.cen_id,d.gas_id,g.gas_nombre,SUM(d.cmd_total_neto) total_gasto  " & _
                "FROM com_doc_compra_detalle d " & _
                "JOIN com_doc_compra c ON d.id=c.id " & _
                "JOIN par_item_gastos g ON d.gas_id=g.gas_id " & _
                "JOIN sis_documentos x ON c.doc_id=x.doc_id " & _
                "WHERE " & Filtro & " " & _
                "GROUP BY d.are_id,d.cen_id,d.gas_id"
    Consulta Rp_Gastos, Sql
    
    Sql = "SELECT d.are_id,d.cen_id,d.gas_id,cmd_detalle,cmd_total_neto,no_documento,fecha,rut,nombre_proveedor " & _
                "FROM com_doc_compra_detalle d " & _
                "JOIN com_doc_compra c ON d.id=c.id " & _
                "JOIN sis_documentos x ON c.doc_id=x.doc_id " & _
                "WHERE " & Filtro & " " & _
                "ORDER BY rut,fecha"
    Consulta RsTmp, Sql
 '   Consulta Rp_Areas, Sql
    
    LvDetalle.ListItems.Clear
    If Rp_Areas.RecordCount > 0 Then
            Rp_Areas.MoveFirst
            Do While Not Rp_Areas.EOF
                LvDetalle.ListItems.Add , , ""
                L = LvDetalle.ListItems.Count
                LvDetalle.ListItems(L).SubItems(1) = "AREA:" & Rp_Areas!are_nombre
                LvDetalle.ListItems(L).SubItems(2) = NumFormat(Rp_Areas!total_area)
                Rp_Centros.MoveFirst
                Do While Not Rp_Centros.EOF
                        If Rp_Areas!are_id = Rp_Centros!are_id Then
                            LvDetalle.ListItems.Add , , ""
                            L = LvDetalle.ListItems.Count
                            LvDetalle.ListItems(L).SubItems(1) = Space(15) & "CENTRO DE COSTO: " & Rp_Centros!cen_nombre
                            LvDetalle.ListItems(L).SubItems(2) = NumFormat(Rp_Centros!total_centro)
                            
                            Rp_Gastos.MoveFirst
                            Do While Not Rp_Gastos.EOF
                                If Rp_Areas!are_id = Rp_Gastos!are_id And Rp_Centros!cen_id = Rp_Gastos!cen_id Then
                                    LvDetalle.ListItems.Add , , ""
                                    L = LvDetalle.ListItems.Count
                                    LvDetalle.ListItems(L).SubItems(1) = Space(25) & "ITEM DE GASTO: " & Rp_Gastos!gas_nombre
                                    LvDetalle.ListItems(L).SubItems(2) = NumFormat(Rp_Gastos!total_gasto)
                                    RsTmp.MoveFirst
                                    Bp_1 = False
                                    Do While Not RsTmp.EOF
                                        If Rp_Areas!are_id = RsTmp!are_id And Rp_Centros!cen_id = RsTmp!cen_id And Rp_Gastos!gas_id = RsTmp!gas_id Then
                                            If Bp_1 Then
                                                LvDetalle.ListItems.Add , , ""
                                                L = LvDetalle.ListItems.Count
                                            End If
                                            LvDetalle.ListItems(L).SubItems(3) = RsTmp!Rut
                                            LvDetalle.ListItems(L).SubItems(4) = RsTmp!nombre_proveedor
                                            Bp_1 = True
                                        
                                        End If
                                        RsTmp.MoveNext
                                    Loop
                                    
                                    
                            
                                End If
                                Rp_Gastos.MoveNext
                            Loop
                        End If
                        
                        
                        
                    
                    Rp_Centros.MoveNext
                Loop
            
                Rp_Areas.MoveNext
            Loop
    End If
    
    
    
    
    
    
    
    
    
        
        
  
  
End Sub
Private Sub CargaReportPorDocumento()
    Sql = "SHAPE {SELECT  DISTINCT d.are_id,are_nombre,c.documento,c.no_documento,nombre_proveedor,c.neto total,rut FROM com_doc_compra_detalle d INNER JOIN com_doc_compra c ON c.id=d.id  INNER JOIN sis_documentos x ON c.doc_id=x.doc_id INNER JOIN par_areas a ON d.are_id=a.are_id WHERE " & Filtro & "} " & _
            "APPEND ((SHAPE {SELECT DISTINCT d.are_id, d.cen_id,cen_nombre FROM com_doc_compra_detalle d INNER JOIN com_doc_compra c ON c.id=d.id  INNER JOIN sis_documentos x ON c.doc_id=x.doc_id INNER JOIN par_centros_de_costo a ON d.cen_id=a.cen_id WHERE " & Filtro & "}  " & _
            "APPEND ({SELECT d.are_id,d.cen_id,gas_nombre, cmd_total_neto total_linea FROM com_doc_compra_detalle d INNER JOIN com_doc_compra c ON c.id=d.id  INNER JOIN sis_documentos x ON c.doc_id=x.doc_id INNER JOIN par_item_gastos a ON d.gas_id=a.gas_id WHERE " & Filtro & "} AS itemgastos " & _
                "RELATE cen_id TO cen_id, are_id TO are_id)) AS centrocostos " & _
                    "RELATE are_id TO are_id)"
    
    ConexionBdShape RsCom, Sql
    
    Set Me.MSHFlexGrid2.DataSource = RsCom
    For i = 1 To Me.MSHFlexGrid2.Cols - 1
         Me.MSHFlexGrid2.ColWidth(i) = 2000
    Next
    If RsCom.RecordCount Then
        DRDetalleAreaCCGastos.Sections("Cabeza").Controls("etempresa").Caption = Principal.SkEmpresa
        DRDetalleAreaCCGastos.Sections("Cabeza").Controls("etrut").Caption = SP_Rut_Activo
        With DRDetalleAreaCCGastos
            .Sections("cabeza").Controls("etrut").Caption = SP_Rut_Activo
            .Sections("cabeza").Controls("etempresa").Caption = Principal.SkEmpresa
            .Sections("SecDocumento").Controls("rut").Caption = txtRutProveedor
            .Sections("SecDocumento").Controls("proveedor").Caption = Me.TxtRsocial
            .Sections("SecDocumento").Controls("documento").Caption = Me.CboTipoDoc.Text
            .Sections("SecDocumento").Controls("numero").Caption = Me.TxtNDoc
            .Sections("SecDocumento").Controls("total").Caption = NumFormat(RsCom!Total)
            
            Set DRDetalleAreaCCGastos.DataSource = RsCom
            DRDetalleAreaCCGastos.Show 1
        End With
    Else
        MsgBox "No se encontr� informaci�n..", vbInformation
    End If
End Sub

Private Sub cmdSinRut_Click()
    TxtRut = ""
    txtCliente = ""
End Sub

Private Sub Form_Load()
    Aplicar_skin Me
    DTInicio = Date
    DtHasta = Date
    LLenarCombo CboTipoDoc, "doc_nombre", "doc_id", "sis_documentos", "doc_documento='COMPRA' and doc_activo='SI'", "doc_orden"
    Centrar Me
    LLenarCombo CboCentroCosto, "cen_nombre", "cen_id", "par_centros_de_costo", "cen_activo='SI' AND rut_emp='" & SP_Rut_Activo & "'"
    LLenarCombo CboArea, "are_nombre", "are_id", "par_areas", "are_activo='SI' AND rut_emp='" & SP_Rut_Activo & "'"
    CboCentroCosto.AddItem "TODOS"
    CboCentroCosto.ListIndex = CboCentroCosto.ListCount - 1
    
    CboArea.AddItem "TODOS"
    CboArea.ListIndex = CboArea.ListCount - 1

End Sub




Private Sub Option1_Click()
    DTInicio.Enabled = True
    DtHasta.Enabled = True
    Me.cmdBtnRut.Enabled = True
    TxtRut.Enabled = True
    skIni.Enabled = True
    skFin.Enabled = True
    SkinLabel1.Enabled = True
    cmdSinRut.Enabled = True
    Frame1.Enabled = False
    DTInicio.SetFocus
End Sub

Private Sub Option2_Click()
    DTInicio.Enabled = False
    DtHasta.Enabled = False
    Me.cmdBtnRut.Enabled = False
    TxtRut.Enabled = False
    skIni.Enabled = False
    skFin.Enabled = False
    SkinLabel1.Enabled = False
    cmdSinRut.Enabled = False
    Frame1.Enabled = True
    
    CboTipoDoc.SetFocus
End Sub

Private Sub Timer1_Timer()
    CboTipoDoc.SetFocus
    Timer1.Enabled = False
End Sub

Private Sub TxtNDoc_GotFocus()
    En_Foco TxtNDoc
End Sub

Private Sub TxtNDoc_KeyPress(KeyAscii As Integer)
    KeyAscii = AceptaSoloNumeros(KeyAscii)
    If KeyAscii = 39 Then KeyAscii = 0
End Sub





Private Sub TxtRut_Validate(Cancel As Boolean)
If Len(TxtRut.Text) = 0 Then Exit Sub
    TxtRut.Text = Replace(TxtRut.Text, ".", "")
    TxtRut.Text = Replace(TxtRut.Text, "-", "")
    Respuesta = VerificaRut(TxtRut.Text, NuevoRut)
    If Respuesta = True Then
        Me.TxtRut.Text = NuevoRut
        'Buscar el cliente
        Sql = "SELECT rut_proveedor rut,nombre_empresa nombre,fono,email " & _
              "FROM maestro_proveedores " & _
              "WHERE rut_proveedor='" & TxtRut & "'"
    
        
                    
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
                'Cliente encontrado
            With RsTmp
                TxtRut.Text = !Rut
                txtCliente = !nombre
                txtFono = !fono
                TxtMail = !Email
            End With
         
            Filtro = " AND v.rut='" & TxtRut & "' "
        Else
            MsgBox "Proveedor no encontrado..."
            txtCliente = ""
        End If
    End If
End Sub

Private Sub TxtRutProveedor_GotFocus()
    En_Foco txtRutProveedor
End Sub



Private Sub txtRutProveedor_Validate(Cancel As Boolean)
        Respuesta = VerificaRut(txtRutProveedor, NuevoRut)
        txtRutProveedor = NuevoRut
        Sql = "SELECT nombre_empresa " & _
              "FROM maestro_proveedores " & _
              "WHERE rut_proveedor='" & txtRutProveedor & "' "
        Consulta RsTmp, Sql
        TxtRsocial = ""
        If RsTmp.RecordCount > 0 Then TxtRsocial = RsTmp!nombre_empresa Else MsgBox "Proveedor no encontrado... ", vbOKOnly + vbInformation
End Sub


