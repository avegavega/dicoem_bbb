VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{DA729E34-689F-49EA-A856-B57046630B73}#1.0#0"; "progressbar-xp.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form Informes_Prov_Clientes 
   Caption         =   "Informe por Cliente - Proveedor"
   ClientHeight    =   10215
   ClientLeft      =   2955
   ClientTop       =   345
   ClientWidth     =   15090
   LinkTopic       =   "Form1"
   ScaleHeight     =   10215
   ScaleWidth      =   15090
   Begin VB.Frame FraProgreso 
      Height          =   540
      Left            =   1365
      TabIndex        =   36
      Top             =   5130
      Visible         =   0   'False
      Width           =   9300
      Begin Proyecto2.XP_ProgressBar PBar 
         Height          =   255
         Left            =   585
         TabIndex        =   37
         Top             =   195
         Width           =   8565
         _ExtentX        =   15108
         _ExtentY        =   450
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BrushStyle      =   0
         Color           =   16777088
         ShowText        =   -1  'True
      End
   End
   Begin VB.CommandButton CmdExportar 
      Caption         =   "Exportar a Excel"
      Height          =   315
      Left            =   585
      TabIndex        =   35
      Top             =   6150
      Width           =   1290
   End
   Begin VB.PictureBox FrmDetalles 
      BackColor       =   &H00C0C000&
      Height          =   2910
      Left            =   6645
      ScaleHeight     =   2850
      ScaleWidth      =   8235
      TabIndex        =   28
      Top             =   3780
      Visible         =   0   'False
      Width           =   8295
      Begin VB.CommandButton CmdCierraDetalleCheques 
         Caption         =   "x"
         Height          =   255
         Left            =   7875
         TabIndex        =   29
         ToolTipText     =   "Cierra detalle de cheques"
         Top             =   -15
         Width           =   255
      End
      Begin MSComctlLib.ListView LVCheques 
         Height          =   2535
         Left            =   90
         TabIndex        =   30
         ToolTipText     =   "Este es el detalle de cheque(s)"
         Top             =   255
         Width           =   8055
         _ExtentX        =   14208
         _ExtentY        =   4471
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   0   'False
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   6
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "T1000"
            Text            =   "Banco"
            Object.Width           =   3528
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "T1000"
            Text            =   "Plaza"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "N109"
            Text            =   "Nro Cheque"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   3
            Object.Tag             =   "T1000"
            Text            =   "Fecha"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   4
            Object.Tag             =   "N100"
            Text            =   "Monto"
            Object.Width           =   1940
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Object.Tag             =   "T1200"
            Text            =   "Estado"
            Object.Width           =   2117
         EndProperty
      End
      Begin VB.Label Label1 
         BackStyle       =   0  'Transparent
         Caption         =   "Detalle de cheques"
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   105
         TabIndex        =   31
         Top             =   45
         Width           =   1515
      End
   End
   Begin VB.Frame Frame6 
      Caption         =   "Pago del documento"
      Height          =   2985
      Left            =   8790
      TabIndex        =   25
      Top             =   6585
      Width           =   6075
      Begin MSComctlLib.ListView LvPagos 
         Height          =   2535
         Left            =   105
         TabIndex        =   27
         Top             =   270
         Width           =   5850
         _ExtentX        =   10319
         _ExtentY        =   4471
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   0   'False
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   5
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "N109"
            Text            =   "id abono"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "T1000"
            Text            =   "Fecha"
            Object.Width           =   2028
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "T1500"
            Text            =   "Forma de pago"
            Object.Width           =   3528
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   3
            Object.Tag             =   "N100"
            Text            =   "Monto"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Object.Tag             =   "T1000"
            Text            =   "Usuario"
            Object.Width           =   1764
         EndProperty
      End
   End
   Begin VB.Frame Frame5 
      Caption         =   "Detalle"
      Height          =   2985
      Left            =   360
      TabIndex        =   24
      Top             =   6585
      Width           =   8385
      Begin MSComctlLib.ListView LvMateriales 
         Height          =   2550
         Left            =   135
         TabIndex        =   26
         Top             =   270
         Width           =   8100
         _ExtentX        =   14288
         _ExtentY        =   4498
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   0   'False
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   6
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "T1000"
            Text            =   "Codigo"
            Object.Width           =   2028
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "T1500"
            Text            =   "Marca"
            Object.Width           =   2205
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "T3000"
            Text            =   "Descripcion"
            Object.Width           =   4674
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   3
            Object.Tag             =   "N102"
            Text            =   "Cant."
            Object.Width           =   1411
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   4
            Object.Tag             =   "N102"
            Text            =   "Precio U."
            Object.Width           =   1587
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   5
            Object.Tag             =   "N100"
            Text            =   "Total"
            Object.Width           =   1940
         EndProperty
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Documentos (precios brutos)"
      Height          =   3315
      Left            =   360
      TabIndex        =   10
      Top             =   3225
      Width           =   14520
      Begin VB.TextBox txtTotal 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00E0E0E0&
         Height          =   315
         Left            =   12510
         Locked          =   -1  'True
         TabIndex        =   22
         Top             =   2940
         Width           =   1560
      End
      Begin MSComctlLib.ListView LvDocumentos 
         Height          =   2535
         Left            =   195
         TabIndex        =   21
         Top             =   360
         Width           =   14100
         _ExtentX        =   24871
         _ExtentY        =   4471
         View            =   3
         LabelWrap       =   0   'False
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   10
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "N100"
            Text            =   "Id Unico"
            Object.Width           =   353
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "F1100"
            Text            =   "Fecha"
            Object.Width           =   1940
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "T1000"
            Text            =   "RUT"
            Object.Width           =   2028
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Object.Tag             =   "T2000"
            Text            =   "Nombre"
            Object.Width           =   7937
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Object.Tag             =   "T2500"
            Text            =   "Tipo Documento"
            Object.Width           =   6879
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   5
            Object.Tag             =   "T800"
            Text            =   "Nro Doc"
            Object.Width           =   2469
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   6
            Object.Tag             =   "N100"
            Text            =   "Total"
            Object.Width           =   2469
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   7
            Object.Tag             =   "T100"
            Text            =   "signo"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   8
            Object.Tag             =   "N109"
            Text            =   "doc_id"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   9
            Object.Tag             =   "T100"
            Text            =   "Por guias"
            Object.Width           =   0
         EndProperty
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   195
         Left            =   11925
         OleObjectBlob   =   "Informes_Prov_Clientes.frx":0000
         TabIndex        =   23
         Top             =   2985
         Width           =   540
      End
   End
   Begin VB.CommandButton cmdSalir 
      Cancel          =   -1  'True
      Caption         =   "&Retornar"
      Height          =   405
      Left            =   13530
      TabIndex        =   9
      Top             =   9645
      Width           =   1350
   End
   Begin VB.Frame Frame1 
      Caption         =   "Frame1"
      Height          =   3090
      Left            =   375
      TabIndex        =   0
      Top             =   105
      Width           =   14520
      Begin VB.CommandButton cmdConsultar 
         Caption         =   "&Consultar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   420
         Left            =   12990
         TabIndex        =   20
         Top             =   2550
         Width           =   1365
      End
      Begin VB.Frame Frame3 
         Caption         =   "Seleccionado"
         Height          =   870
         Left            =   210
         TabIndex        =   11
         Top             =   2190
         Width           =   12585
         Begin VB.CommandButton cmdSinRut 
            Caption         =   "X"
            Height          =   210
            Left            =   720
            TabIndex        =   34
            ToolTipText     =   "Limpiar"
            Top             =   285
            Width           =   180
         End
         Begin VB.ComboBox CboSucursal 
            Height          =   315
            Left            =   5400
            Style           =   2  'Dropdown List
            TabIndex        =   33
            Top             =   495
            Width           =   2910
         End
         Begin VB.TextBox TxtRut 
            BackColor       =   &H0080FF80&
            Height          =   315
            Left            =   75
            TabIndex        =   16
            ToolTipText     =   "Ingrese el RUT sin puntos ni guion."
            Top             =   495
            Width           =   1560
         End
         Begin VB.TextBox txtCliente 
            BackColor       =   &H00E0E0E0&
            Height          =   315
            Left            =   1650
            Locked          =   -1  'True
            TabIndex        =   15
            Top             =   495
            Width           =   3750
         End
         Begin VB.CommandButton cmdBtnRut 
            Caption         =   "Rut"
            Height          =   225
            Left            =   75
            TabIndex        =   14
            Top             =   270
            Width           =   585
         End
         Begin VB.TextBox TxtFono 
            BackColor       =   &H00E0E0E0&
            Height          =   315
            Left            =   8340
            Locked          =   -1  'True
            TabIndex        =   13
            Top             =   495
            Width           =   1245
         End
         Begin VB.TextBox TxtMail 
            BackColor       =   &H00E0E0E0&
            Height          =   315
            Left            =   9600
            Locked          =   -1  'True
            TabIndex        =   12
            Top             =   495
            Width           =   2910
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel16 
            Height          =   195
            Left            =   1665
            OleObjectBlob   =   "Informes_Prov_Clientes.frx":0068
            TabIndex        =   17
            Top             =   300
            Width           =   540
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
            Height          =   195
            Left            =   8310
            OleObjectBlob   =   "Informes_Prov_Clientes.frx":00D2
            TabIndex        =   18
            Top             =   300
            Width           =   540
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
            Height          =   225
            Left            =   9600
            OleObjectBlob   =   "Informes_Prov_Clientes.frx":0138
            TabIndex        =   19
            Top             =   285
            Width           =   540
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
            Height          =   270
            Left            =   5415
            OleObjectBlob   =   "Informes_Prov_Clientes.frx":01A2
            TabIndex        =   32
            Top             =   300
            Width           =   1095
         End
      End
      Begin VB.Frame Frame4 
         Caption         =   "Informacion"
         Height          =   1875
         Left            =   11145
         TabIndex        =   2
         Top             =   285
         Width           =   2790
         Begin VB.OptionButton Option2 
            Caption         =   "Seleccionar fechas"
            Height          =   240
            Left            =   465
            TabIndex        =   4
            Top             =   720
            Width           =   1725
         End
         Begin VB.OptionButton Option1 
            Caption         =   "Todas las fechas"
            Height          =   240
            Left            =   495
            TabIndex        =   3
            Top             =   345
            Value           =   -1  'True
            Width           =   1740
         End
         Begin ACTIVESKINLibCtl.SkinLabel skIni 
            Height          =   240
            Left            =   255
            OleObjectBlob   =   "Informes_Prov_Clientes.frx":0210
            TabIndex        =   5
            Top             =   1125
            Width           =   795
         End
         Begin MSComCtl2.DTPicker DTInicio 
            Height          =   300
            Left            =   1095
            TabIndex        =   6
            Top             =   1110
            Width           =   1305
            _ExtentX        =   2302
            _ExtentY        =   529
            _Version        =   393216
            Enabled         =   0   'False
            Format          =   85590017
            CurrentDate     =   40628
         End
         Begin MSComCtl2.DTPicker DtHasta 
            Height          =   285
            Left            =   1095
            TabIndex        =   7
            Top             =   1410
            Width           =   1305
            _ExtentX        =   2302
            _ExtentY        =   503
            _Version        =   393216
            Enabled         =   0   'False
            Format          =   85590017
            CurrentDate     =   40628
         End
         Begin ACTIVESKINLibCtl.SkinLabel skFin 
            Height          =   240
            Left            =   255
            OleObjectBlob   =   "Informes_Prov_Clientes.frx":0278
            TabIndex        =   8
            Top             =   1395
            Width           =   795
         End
      End
      Begin MSComctlLib.ListView LvDetalle 
         Height          =   1770
         Left            =   210
         TabIndex        =   1
         ToolTipText     =   "Ultimos Rut con movimientos"
         Top             =   360
         Width           =   10485
         _ExtentX        =   18494
         _ExtentY        =   3122
         View            =   3
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   4
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "T1000"
            Text            =   "RUT"
            Object.Width           =   2646
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "T2000"
            Text            =   "Nombre"
            Object.Width           =   7056
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "T1500"
            Text            =   "Sucursal"
            Object.Width           =   5292
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Object.Tag             =   "T1300"
            Text            =   "Ultimo Movimiento"
            Object.Width           =   2646
         EndProperty
      End
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   450
      OleObjectBlob   =   "Informes_Prov_Clientes.frx":02E0
      Top             =   9840
   End
   Begin VB.Timer Timer1 
      Interval        =   10
      Left            =   -15
      Top             =   9870
   End
End
Attribute VB_Name = "Informes_Prov_Clientes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim sm_FiltroFecha As String
Dim Sm_FiltroSucursal As String
Dim Sm_FiltroCliente As String
Private Sub cmdBtnRut_Click()
    ClienteEncontrado = False
    SG_codigo = Empty
    
    If Sp_extra = "CLIENTES" Then
        BuscaCliente.Show 1
    Else
        BuscaProveedor.Show 1
    End If
    
    TxtRut = SG_codigo
    If SG_codigo <> Empty Then
        TxtRut_Validate (True)
    End If
End Sub

Private Sub CmdCierraDetalleCheques_Click()
    FrmDetalles.Visible = False
End Sub

Private Sub cmdConsultar_Click()
    
    
    LvDocumentos.ListItems.Clear
    LvMateriales.ListItems.Clear
    LvPagos.ListItems.Clear
    
    If Option1.Value Then
        sm_FiltroFecha = Empty
    Else
        sm_FiltroFecha = " AND (v.fecha BETWEEN '" & Format(DTInicio.Value, "YYYY-MM-DD") & "' AND '" & Format(DtHasta.Value, "YYYY-MM-DD") & "') "
    End If
    
    If Sp_extra = "CLIENTES" Then
        Filtro = Empty
        If Len(TxtRut) = 0 Then
            Sm_FiltroCliente = Empty
        Else
            Sm_FiltroCliente = " AND rut_cliente='" & TxtRut & "' "
        End If
        If CboSucursal.Text = "TODOS" Then
            Sm_FiltroSucursal = Empty
        Else
            If CboSucursal.ListIndex > -1 Then Sm_FiltroSucursal = " AND v.suc_id=" & CboSucursal.ItemData(CboSucursal.ListIndex)
        End If
        Sql = "SELECT id,DATE_FORMAT(fecha,'%d-%m-%Y')fecha,rut_cliente, nombre_cliente," & _
                "CONCAT(tipo_doc,' ',IF( doc_factura_guias='SI','{ Por Guia(s) }',''),IF(nro_factura>0,' {FACTURADA} ',''))," & _
                "no_documento,bruto,d.doc_signo_libro,doc_id,doc_factura_guias " & _
                "FROM ven_doc_venta v " & _
                "INNER JOIN sis_documentos d USING(doc_id) " & _
                "WHERE doc_informa_venta='SI' AND doc_nota_de_venta='NO' AND  v.rut_emp='" & SP_Rut_Activo & "' AND doc_id_factura=0  " & Sm_FiltroCliente & sm_FiltroFecha & Sm_FiltroSucursal & " " & _
                "ORDER BY v.fecha DESC "
        
        
        
     Else 'PROVEEDORES
        If Len(TxtRut) = 0 Then
            Sm_FiltroCliente = Empty
        Else
            Sm_FiltroCliente = " AND rut='" & TxtRut & "' "
        End If

     
        Sql = "SELECT id,DATE_FORMAT(fecha,'%d-%m-%Y')fecha,rut,nombre_proveedor," & _
                "CONCAT(doc_nombre,' ',IF( doc_factura_guias='SI','{ Por Guia(s) }',''),IF(nro_factura>0,' {FACTURADA} ',''))," & _
                "no_documento,total,d.doc_signo_libro,doc_id,doc_factura_guias " & _
                "FROM com_doc_compra v " & _
                "INNER JOIN sis_documentos d USING(doc_id) " & _
                "WHERE com_informa_compra='SI' AND d.doc_orden_de_compra='NO' AND v.rut_emp='" & SP_Rut_Activo & "' AND doc_id_factura=0 " & Sm_FiltroCliente & sm_FiltroFecha & " " & _
                "ORDER BY v.fecha DESC"
     End If
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvDocumentos, False, True, True, False
    TxtTotal = 0
    For i = 1 To LvDocumentos.ListItems.Count
        If LvDocumentos.ListItems(i).SubItems(7) = "-" Then
            TxtTotal = CDbl(TxtTotal) - CDbl(LvDocumentos.ListItems(i).SubItems(6))
        Else
            TxtTotal = CDbl(TxtTotal) + CDbl(LvDocumentos.ListItems(i).SubItems(6))
        End If
    Next
    TxtTotal = NumFormat(TxtTotal)
        
        
End Sub

Private Sub CmdExportar_Click()
 Dim tit(2) As String
    FraProgreso.Visible = True
    tit(0) = Me.Caption
    tit(1) = "FECHA INFORME: " & Date & " - " & Time
    tit(2) = "" ' "                                        DESDE:" & DtInicio.Value & "   HASTA:" & Dtfin.Value
    ExportarNuevo LvDocumentos, tit, Me, pbar
    pbar.Value = 1
    FraProgreso.Visible = False
End Sub

Private Sub CmdSalir_Click()
    Unload Me
End Sub

Private Sub cmdSinRut_Click()
    TxtRut = Empty
    Me.txtCliente = Empty
    CboSucursal.ListIndex = -1
    txtFono = Empty
    TxtMail = Empty
End Sub

Private Sub Form_Load()
    Centrar Me
    Aplicar_skin Me
    
    Sql = "SELECT men_infoextra " & _
          "FROM sis_menu " & _
          "WHERE men_id=" & IP_IDMenu
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        Sp_extra = RsTmp!men_infoextra
    End If
    
    If Sp_extra = "CLIENTES" Then
        Frame1.Caption = "CLIENTE"
        Me.Caption = "INFORME DE VENTAS POR CLIENTE"
        Sql = "SELECT v.rut_cliente, m.nombre_rsocial,CONCAT(suc_ciudad,' ',suc_direccion) sucursal,v.fecha ultimacompra " & _
                "FROM ven_doc_venta v " & _
                "INNER JOIN maestro_clientes m ON v.rut_cliente=m.rut_cliente " & _
                "INNER JOIN par_sucursales USING(suc_id) " & _
                "WHERE ven_informa_venta='SI' AND v.rut_emp='" & SP_Rut_Activo & "' " & _
                "GROUP BY v.rut_cliente " & _
                "ORDER BY v.fecha DESC"
    Else
    
        Me.Caption = "INFORME DE COMPRAS POR PROVEEDOR"
        Sql = "SELECT v.rut, m.nombre_empresa,'',v.fecha ultimacompra " & _
                "FROM com_doc_compra v " & _
                "INNER JOIN maestro_proveedores m ON v.rut=m.rut_proveedor " & _
                "WHERE  v.rut_emp='" & SP_Rut_Activo & "' " & _
                "GROUP BY v.rut " & _
                "ORDER BY v.fecha DESC"
        Frame1.Caption = "PROVEEDORES"
        
    End If
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvDetalle, False, True, True, False
    
    
    Filtro = Empty
    DTInicio.Value = Date - 30
    DtHasta.Value = Date
    
    
    If Sp_extra = "CLIENTES" Then
    '    CargaClientes
    Else
     '   CargaProveedores
    End If
    
    sm_FiltroFecha = Empty
    
    
    
    
    
End Sub


Private Sub LVCheques_LostFocus()
    Me.FrmDetalles.Visible = False
End Sub

Private Sub LvDetalle_DblClick()
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    TxtRut = LvDetalle.SelectedItem.Text
    TxtRut_Validate (True)
    cmdConsultar_Click
End Sub



Private Sub LvDocumentos_Click()
'
    If LvDocumentos.SelectedItem Is Nothing Then Exit Sub
    
    'DETALLE DEL DOCUMENTO
    DG_ID_Unico = LvDocumentos.SelectedItem.Text
    
    If Sp_extra = "CLIENTES" Then
        If LvDocumentos.SelectedItem.SubItems(9) = "SI" Then
            Sql = "SELECT d.codigo,marca,descripcion,d.unidades,d.precio_final,d.subtotal " & _
                  "FROM ven_doc_venta c " & _
                  "INNER JOIN sis_documentos l USING(doc_id), " & _
                  "ven_detalle d  " & _
                  "WHERE doc_nota_de_venta='NO' AND  c.rut_emp='" & SP_Rut_Activo & "' AND d.doc_id=c.doc_id AND d.no_documento=c.no_documento AND c.doc_id_factura =" & LvDocumentos.SelectedItem.SubItems(8) & _
                  " AND c.nro_factura = " & LvDocumentos.SelectedItem.SubItems(5)
        Else
            Sql = "SELECT codigo,marca,descripcion,d.unidades,d.precio_final,d.subtotal " & _
                  "FROM ven_detalle d " & _
                  "WHERE  d.rut_emp='" & SP_Rut_Activo & "' AND doc_id=" & LvDocumentos.SelectedItem.SubItems(8) & " AND no_documento=" & LvDocumentos.SelectedItem.SubItems(5)
        End If

    Else '`RPVEEDPORES
        If LvDocumentos.SelectedItem.SubItems(9) = "SI" Then ' SI ES POR GUIAS
            Sql = "SELECT codigo,mar_nombre,IFNULL(descripcion,cmd_detalle) descripcion,d.cmd_cantidad,IF(c.com_bruto_neto='BRUTO',d.cmd_unitario_bruto,cmd_unitario_neto)," & _
                  "IF(c.com_bruto_neto='BRUTO',d.cmd_total_bruto,cmd_total_neto) " & _
                  "FROM com_doc_compra c " & _
                  "INNER JOIN com_doc_compra_detalle d ON c.id=d.id " & _
                  "LEFT JOIN maestro_productos m ON d.pro_codigo=m.codigo AND m.rut_emp='" & SP_Rut_Activo & "' " & _
                  "LEFT JOIN par_marcas p ON p.mar_id=m.mar_id AND m.rut_emp='" & SP_Rut_Activo & "' " & _
                  "INNER JOIN sis_documentos l USING(doc_id) " & _
                  "WHERE l.doc_orden_de_compra='NO' AND c.rut_emp='" & SP_Rut_Activo & "' AND c.doc_id_factura =" & LvDocumentos.SelectedItem.SubItems(8) & _
                  " AND c.nro_factura = " & LvDocumentos.SelectedItem.SubItems(5)
        Else
            
            Sql = "SELECT codigo id_compra_detalle,mar_nombre,IFNULL(descripcion,cmd_detalle) descripcion,d.cmd_cantidad," & _
                  "IF(c.com_bruto_neto='BRUTO',d.cmd_unitario_bruto,cmd_unitario_neto),IF(c.com_bruto_neto='BRUTO',d.cmd_total_bruto,cmd_total_neto) " & _
                "FROM com_doc_compra_detalle d " & _
                "INNER JOIN com_doc_compra c USING(id) " & _
                "LEFT JOIN maestro_productos m ON m.codigo=d.pro_codigo AND m.rut_emp='" & SP_Rut_Activo & "' " & _
                "LEFT JOIN  par_marcas x  ON m.mar_id=x.mar_id AND x.rut_emp='" & SP_Rut_Activo & "' " & _
                "WHERE d.id = " & DG_ID_Unico
        End If
    End If
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvMateriales, False, True, True, False
    
    'PAGO DEL DOCUMENTO
    If Sp_extra = "CLIENTES" Then
        Sql = "SELECT a.abo_id,a.abo_fecha,mpa_nombre,ctd_monto,a.usu_nombre " & _
              "FROM cta_abonos a INNER JOIN cta_abono_documentos c USING(abo_id) " & _
              "INNER JOIN par_medios_de_pago p USING(mpa_id),ven_doc_venta m " & _
              "WHERE  c.id=m.id AND a.abo_cli_pro='CLI' AND c.id=" & DG_ID_Unico
    
    Else
        'Sql = "SELECT a.abo_id,a.abo_fecha,mpa_nombre,ctd_monto,usu_nombre " & _
              "FROM cta_abonos a INNER JOIN cta_abono_documentos c USING(abo_id) " & _
              "INNER JOIN par_medios_de_pago p USING(mpa_id),com_doc_compra m " & _
              "WHERE  c.id=m.id AND a.abo_cli_pro='PRO' AND c.id=" & DG_ID_Unico
        Sql = "SELECT a.abo_id,a.abo_fecha,mpa_nombre,pad_valor,usu_nombre " & _
              "FROM cta_abonos a " & _
              "INNER JOIN cta_abono_documentos c ON c.abo_id=a.abo_id  " & _
              "INNER JOIN abo_tipos_de_pagos t ON a.abo_id=t.abo_id " & _
              "INNER JOIN par_medios_de_pago p ON p.mpa_id=t.mpa_id, " & _
              "com_doc_compra m " & _
              "WHERE  c.id=m.id AND a.abo_cli_pro='PRO' AND c.id=" & DG_ID_Unico
              
              
    End If
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvPagos, False, True, True, False
    
    
End Sub



Private Sub LvDocumentos_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    ordListView ColumnHeader, Me, LvDocumentos
End Sub

Private Sub LvMateriales_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    ordListView ColumnHeader, Me, LvMateriales
End Sub

Private Sub LvPagos_Click()
    If LvPagos.SelectedItem Is Nothing Then Exit Sub
    DG_ID_Unico = LvPagos.SelectedItem.Text
    LvCheques.ListItems.Clear
    Sql = "SELECT ban_nombre,che_plaza,che_numero,che_fecha,che_monto,che_estado " & _
          "FROM abo_cheques c INNER JOIN par_bancos b USING(ban_id) " & _
          "WHERE abo_id=" & DG_ID_Unico
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        FrmDetalles.Visible = True
        LLenar_Grilla RsTmp, Me, LvCheques, False, True, True, False
        LvCheques.SetFocus
    End If
End Sub

Private Sub Option1_Click()
    DTInicio.Enabled = False
    DtHasta.Enabled = False
End Sub

Private Sub Option2_Click()
    DTInicio.Enabled = True
    DtHasta.Enabled = True
   
End Sub

Private Sub Timer1_Timer()
    LvDetalle.SetFocus
    Timer1.Enabled = False
End Sub




Private Sub TxtRut_Validate(Cancel As Boolean)
    If Len(TxtRut.Text) = 0 Then Exit Sub
    TxtRut.Text = Replace(TxtRut.Text, ".", "")
    TxtRut.Text = Replace(TxtRut.Text, "-", "")
    Respuesta = VerificaRut(TxtRut.Text, NuevoRut)
    If Respuesta = True Then
        Me.TxtRut.Text = NuevoRut
        'Buscar el cliente
        If Sp_extra = "CLIENTES" Then
            Sql = "SELECT rut_cliente rut,IFNULL(cli_nombre_fantasia,nombre_rsocial) nombre,fono,email " & _
                  "FROM maestro_clientes " & _
                  "WHERE rut_cliente='" & TxtRut & "'"
                  CboSucursal.Clear
        Else
            Sql = "SELECT rut_proveedor rut,nombre_empresa nombre,fono,email " & _
                  "FROM maestro_proveedores " & _
                  "WHERE rut_proveedor='" & TxtRut & "'"
        
        End If
                    
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
                'Cliente encontrado
            With RsTmp
                TxtRut.Text = !Rut
                txtCliente = !nombre
                txtFono = "" & !fono
                TxtMail = "" & !Email
            End With
            If Sp_extra = "CLIENTES" Then
                Filtro = " AND v.rut_cliente='" & TxtRut & "' "
                LLenarCombo CboSucursal, "CONCAT(suc_ciudad,' ',suc_direccion,' ',suc_contacto)", "suc_id", "par_sucursales", "suc_activo='SI' AND rut_cliente='" & TxtRut & "'", "suc_id"
                CboSucursal.AddItem "CASA MATRIZ"
                CboSucursal.ItemData(CboSucursal.ListCount - 1) = 0
                CboSucursal.AddItem "TODOS"
                CboSucursal.ListIndex = CboSucursal.ListCount - 1
            Else
                Filtro = " AND v.rut='" & TxtRut & "' "
            End If
        End If
        If Sp_extra = "CLIENTES" Then
            'CargaClientes
        Else
            'CargaProveedores
        End If
    End If
End Sub
