VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form Informe_DetalleVentasXVendedor 
   Caption         =   "Detalle de Ventas por Vendedor"
   ClientHeight    =   8625
   ClientLeft      =   -105
   ClientTop       =   1140
   ClientWidth     =   15045
   LinkTopic       =   "Form1"
   ScaleHeight     =   8625
   ScaleWidth      =   15045
   Begin VB.CommandButton cmdSalir 
      Cancel          =   -1  'True
      Caption         =   "&Retornar"
      Height          =   405
      Left            =   13470
      TabIndex        =   23
      Top             =   8100
      Width           =   1350
   End
   Begin VB.Frame Frame4 
      Caption         =   "Informacion"
      Height          =   1380
      Left            =   8055
      TabIndex        =   14
      Top             =   105
      Width           =   4440
      Begin VB.OptionButton Option1 
         Caption         =   "Todas las fechas"
         Height          =   240
         Left            =   495
         TabIndex        =   16
         Top             =   345
         Value           =   -1  'True
         Width           =   1740
      End
      Begin VB.OptionButton Option2 
         Caption         =   "Seleccionar fechas"
         Height          =   240
         Left            =   2580
         TabIndex        =   15
         Top             =   360
         Width           =   1725
      End
      Begin ACTIVESKINLibCtl.SkinLabel skIni 
         Height          =   240
         Left            =   2160
         OleObjectBlob   =   "Informe_DetalleVentasXVendedor.frx":0000
         TabIndex        =   17
         Top             =   645
         Width           =   795
      End
      Begin MSComCtl2.DTPicker DTInicio 
         Height          =   300
         Left            =   3000
         TabIndex        =   18
         Top             =   630
         Width           =   1305
         _ExtentX        =   2302
         _ExtentY        =   529
         _Version        =   393216
         Enabled         =   0   'False
         Format          =   93323265
         CurrentDate     =   40628
      End
      Begin MSComCtl2.DTPicker DtHasta 
         Height          =   285
         Left            =   3000
         TabIndex        =   19
         Top             =   930
         Width           =   1305
         _ExtentX        =   2302
         _ExtentY        =   503
         _Version        =   393216
         Enabled         =   0   'False
         Format          =   93323265
         CurrentDate     =   40628
      End
      Begin ACTIVESKINLibCtl.SkinLabel skFin 
         Height          =   240
         Left            =   2160
         OleObjectBlob   =   "Informe_DetalleVentasXVendedor.frx":0068
         TabIndex        =   20
         Top             =   915
         Width           =   795
      End
   End
   Begin VB.Timer Timer1 
      Interval        =   10
      Left            =   1965
      Top             =   8175
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   1005
      OleObjectBlob   =   "Informe_DetalleVentasXVendedor.frx":00D0
      Top             =   8025
   End
   Begin VB.Frame Frame1 
      Caption         =   "Filtro"
      Height          =   1380
      Left            =   210
      TabIndex        =   12
      Top             =   105
      Width           =   14535
      Begin VB.CommandButton cmdConsultar 
         Caption         =   "&Consultar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   420
         Left            =   12885
         TabIndex        =   21
         Top             =   525
         Width           =   1365
      End
      Begin VB.ComboBox CboVendedores 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   480
         Style           =   2  'Dropdown List
         TabIndex        =   13
         Top             =   780
         Width           =   6180
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
         Height          =   180
         Left            =   480
         OleObjectBlob   =   "Informe_DetalleVentasXVendedor.frx":0304
         TabIndex        =   22
         Top             =   555
         Width           =   1095
      End
   End
   Begin VB.Frame Frame6 
      Caption         =   "Pago del documento"
      Height          =   2985
      Left            =   8715
      TabIndex        =   10
      Top             =   5040
      Width           =   6075
      Begin MSComctlLib.ListView LvPagos 
         Height          =   2535
         Left            =   105
         TabIndex        =   11
         Top             =   270
         Width           =   5850
         _ExtentX        =   10319
         _ExtentY        =   4471
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   0   'False
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   5
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "N109"
            Text            =   "id abono"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "T1000"
            Text            =   "Fecha"
            Object.Width           =   2028
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "T1500"
            Text            =   "Forma de pago"
            Object.Width           =   3528
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   3
            Object.Tag             =   "N100"
            Text            =   "Monto"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Object.Tag             =   "T1000"
            Text            =   "Usuario"
            Object.Width           =   1764
         EndProperty
      End
   End
   Begin VB.PictureBox FrmDetalles 
      BackColor       =   &H00C0C000&
      Height          =   2910
      Left            =   6570
      ScaleHeight     =   2850
      ScaleWidth      =   8235
      TabIndex        =   6
      Top             =   2235
      Visible         =   0   'False
      Width           =   8295
      Begin VB.CommandButton CmdCierraDetalleCheques 
         Caption         =   "x"
         Height          =   255
         Left            =   7875
         TabIndex        =   7
         ToolTipText     =   "Cierra detalle de cheques"
         Top             =   -15
         Width           =   255
      End
      Begin MSComctlLib.ListView LVCheques 
         Height          =   2535
         Left            =   90
         TabIndex        =   8
         ToolTipText     =   "Este es el detalle de cheque(s)"
         Top             =   255
         Width           =   8055
         _ExtentX        =   14208
         _ExtentY        =   4471
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   0   'False
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   6
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "T1000"
            Text            =   "Banco"
            Object.Width           =   3528
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "T1000"
            Text            =   "Plaza"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "N109"
            Text            =   "Nro Cheque"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   3
            Object.Tag             =   "T1000"
            Text            =   "Fecha"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   4
            Object.Tag             =   "N100"
            Text            =   "Monto"
            Object.Width           =   1940
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Object.Tag             =   "T1200"
            Text            =   "Estado"
            Object.Width           =   2117
         EndProperty
      End
      Begin VB.Label Label1 
         BackStyle       =   0  'Transparent
         Caption         =   "Detalle de cheques"
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   105
         TabIndex        =   9
         Top             =   45
         Width           =   1515
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Documentos (precios brutos)"
      Height          =   3315
      Left            =   240
      TabIndex        =   2
      Top             =   1635
      Width           =   14520
      Begin VB.TextBox txtTotal 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00E0E0E0&
         Height          =   315
         Left            =   12510
         Locked          =   -1  'True
         TabIndex        =   3
         Top             =   2940
         Width           =   1560
      End
      Begin MSComctlLib.ListView LvDocumentos 
         Height          =   2535
         Left            =   195
         TabIndex        =   4
         Top             =   360
         Width           =   14100
         _ExtentX        =   24871
         _ExtentY        =   4471
         View            =   3
         LabelWrap       =   0   'False
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   10
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "N100"
            Text            =   "Id Unico"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "T1100"
            Text            =   "Fecha"
            Object.Width           =   1940
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "T1000"
            Text            =   "RUT"
            Object.Width           =   2028
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Object.Tag             =   "T2000"
            Text            =   "Nombre"
            Object.Width           =   7937
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Object.Tag             =   "T2500"
            Text            =   "Tipo Documento"
            Object.Width           =   6879
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   5
            Object.Tag             =   "T800"
            Text            =   "Nro Doc"
            Object.Width           =   2469
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   6
            Object.Tag             =   "N100"
            Text            =   "Total"
            Object.Width           =   2469
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   7
            Object.Tag             =   "T100"
            Text            =   "signo"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   8
            Object.Tag             =   "N109"
            Text            =   "doc_id"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   9
            Object.Tag             =   "T100"
            Text            =   "Por guias"
            Object.Width           =   0
         EndProperty
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   195
         Left            =   11925
         OleObjectBlob   =   "Informe_DetalleVentasXVendedor.frx":0388
         TabIndex        =   5
         Top             =   2985
         Width           =   540
      End
   End
   Begin VB.Frame Frame5 
      Caption         =   "Detalle"
      Height          =   2985
      Left            =   255
      TabIndex        =   0
      Top             =   5040
      Width           =   8385
      Begin MSComctlLib.ListView LvMateriales 
         Height          =   2550
         Left            =   135
         TabIndex        =   1
         Top             =   270
         Width           =   8100
         _ExtentX        =   14288
         _ExtentY        =   4498
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   0   'False
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   6
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "T1000"
            Text            =   "Codigo"
            Object.Width           =   2028
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "T1500"
            Text            =   "Marca"
            Object.Width           =   2205
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "T3000"
            Text            =   "Descripcion"
            Object.Width           =   4674
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   3
            Object.Tag             =   "N102"
            Text            =   "Cant."
            Object.Width           =   1411
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   4
            Object.Tag             =   "N102"
            Text            =   "Precio U."
            Object.Width           =   1587
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   5
            Object.Tag             =   "N100"
            Text            =   "Total"
            Object.Width           =   1940
         EndProperty
      End
   End
End
Attribute VB_Name = "Informe_DetalleVentasXVendedor"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim sm_FiltroFecha As String
Private Sub cmdConsultar_Click()
    LvDocumentos.ListItems.Clear
    LvMateriales.ListItems.Clear
    LvPagos.ListItems.Clear
    
    If Option1.Value Then
        sm_FiltroFecha = Empty
    Else
        sm_FiltroFecha = " AND (v.fecha BETWEEN '" & Format(DTInicio.Value, "YYYY-MM-DD") & "' AND '" & Format(DtHasta.Value, "YYYY-MM-DD") & "') "
    End If
    Carga
End Sub
Private Sub Carga()
    If CboVendedores.ListIndex = -1 Then
            MsgBox "Seleccione Vendedor ...", vbInformation
            CboVendedores.SetFocus
            Exit Sub
    End If
    Sql = "SELECT id,DATE_FORMAT(fecha,'%d-%m-%Y')fecha,rut_cliente, nombre_cliente," & _
                "CONCAT(tipo_doc,' ',IF( doc_factura_guias='SI','{ Por Guia(s) }',''),IF(nro_factura>0,' {FACTURADA} ',''))," & _
                "no_documento,bruto,d.doc_signo_libro,doc_id,doc_factura_guias " & _
                "FROM ven_doc_venta v " & _
                "INNER JOIN sis_documentos d USING(doc_id) " & _
                "WHERE doc_informa_venta='SI' AND doc_nota_de_venta='NO' AND ven_id=" & CboVendedores.ItemData(CboVendedores.ListIndex) & " AND v.rut_emp='" & SP_Rut_Activo & "' AND doc_id_factura=0  " & sm_FiltroFecha & " " & _
                "ORDER BY v.fecha DESC "
                
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvDocumentos, False, True, True, False
    txtTotal = 0
    For i = 1 To LvDocumentos.ListItems.Count
        If LvDocumentos.ListItems(i).SubItems(7) = "-" Then
            txtTotal = CDbl(txtTotal) - CDbl(LvDocumentos.ListItems(i).SubItems(6))
        Else
            txtTotal = CDbl(txtTotal) + CDbl(LvDocumentos.ListItems(i).SubItems(6))
        End If
    Next
    txtTotal = NumFormat(txtTotal)
End Sub


Private Sub cmdSalir_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    Aplicar_skin Me
    Centrar Me
    LLenarCombo CboVendedores, "ven_nombre", "ven_id", "par_vendedores", "ven_activo='SI' AND rut_emp='" & SP_Rut_Activo & "'", "ven_nombre"
    DTInicio = Date
    DtHasta = Date
End Sub
Private Sub LvDocumentos_Click()
        
    If LvDocumentos.SelectedItem Is Nothing Then Exit Sub
    
    'DETALLE DEL DOCUMENTO
    DG_ID_Unico = LvDocumentos.SelectedItem.Text
    
    
    If LvDocumentos.SelectedItem.SubItems(9) = "SI" Then
        Sql = "SELECT d.id,marca,descripcion,d.unidades,d.precio_final,d.subtotal " & _
              "FROM ven_doc_venta c " & _
              "INNER JOIN sis_documentos l USING(doc_id), " & _
              "ven_detalle d  " & _
              "WHERE   doc_nota_de_venta='NO' AND c.rut_emp='" & SP_Rut_Activo & "' AND d.doc_id=c.doc_id AND d.no_documento=c.no_documento AND c.doc_id_factura =" & LvDocumentos.SelectedItem.SubItems(8) & _
              " AND c.nro_factura = " & LvDocumentos.SelectedItem.SubItems(5)
    Else
        Sql = "SELECT id,marca,descripcion,d.unidades,d.precio_final,d.subtotal " & _
              "FROM ven_detalle d " & _
              "WHERE  d.rut_emp='" & SP_Rut_Activo & "' AND doc_id=" & LvDocumentos.SelectedItem.SubItems(8) & " AND no_documento=" & LvDocumentos.SelectedItem.SubItems(5)
    End If
    
    
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvMateriales, False, True, True, False
    
    'PAGO DEL DOCUMENTO
    
    Sql = "SELECT a.abo_id,a.abo_fecha,mpa_nombre,ctd_monto,a.usu_nombre " & _
          "FROM cta_abonos a INNER JOIN cta_abono_documentos c USING(abo_id) " & _
          "INNER JOIN par_medios_de_pago p USING(mpa_id),ven_doc_venta m " & _
          "WHERE  c.id=m.id AND a.abo_cli_pro='CLI' AND c.id=" & DG_ID_Unico
    
    
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvPagos, False, True, True, False
    
    
End Sub


Private Sub LvPagos_Click()
    If LvPagos.SelectedItem Is Nothing Then Exit Sub
    DG_ID_Unico = LvPagos.SelectedItem.Text
    LvCheques.ListItems.Clear
    Sql = "SELECT ban_nombre,che_plaza,che_numero,che_fecha,che_monto,che_estado " & _
          "FROM abo_cheques c INNER JOIN par_bancos b USING(ban_id) " & _
          "WHERE abo_id=" & DG_ID_Unico
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        FrmDetalles.Visible = True
        LLenar_Grilla RsTmp, Me, LvCheques, False, True, True, False
        LvCheques.SetFocus
    End If
End Sub

Private Sub Option1_Click()
    DTInicio.Enabled = False
    DtHasta.Enabled = False
End Sub

Private Sub Option2_Click()
    DTInicio.Enabled = True
    DtHasta.Enabled = True
   
End Sub

Private Sub Timer1_Timer()
    On Error Resume Next
    CboVendedores.SetFocus
    Timer1.Enabled = False
End Sub
