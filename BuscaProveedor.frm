VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form BuscaProveedor 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Buscar Proveedor"
   ClientHeight    =   6390
   ClientLeft      =   2460
   ClientTop       =   3150
   ClientWidth     =   10755
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6390
   ScaleWidth      =   10755
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton CmdSalir 
      Cancel          =   -1  'True
      Caption         =   "&Retornar"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   9390
      TabIndex        =   5
      Top             =   5895
      Width           =   1095
   End
   Begin VB.Frame Frame1 
      Height          =   5610
      Left            =   225
      TabIndex        =   0
      Top             =   195
      Width           =   10290
      Begin VB.TextBox TxtBusqueda 
         Height          =   375
         Left            =   150
         TabIndex        =   1
         Top             =   540
         Width           =   7530
      End
      Begin VB.CommandButton CmdSeleccionar 
         Caption         =   "&Seleccionar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   6510
         TabIndex        =   2
         Top             =   5070
         Width           =   1455
      End
      Begin VB.CommandButton CmdCrear 
         Caption         =   "Crear Nuevo Proveedor"
         Height          =   375
         Left            =   7995
         TabIndex        =   3
         Top             =   5085
         Width           =   1935
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   330
         Left            =   150
         OleObjectBlob   =   "BuscaProveedor.frx":0000
         TabIndex        =   4
         Top             =   240
         Width           =   2655
      End
      Begin MSComctlLib.ListView LvDetalle 
         Height          =   3855
         Left            =   165
         TabIndex        =   6
         Top             =   1050
         Width           =   9855
         _ExtentX        =   17383
         _ExtentY        =   6800
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   3
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "T1000"
            Text            =   "Rut Cliente"
            Object.Width           =   2646
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "T1500"
            Text            =   "Nombre"
            Object.Width           =   7056
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "T3000"
            Text            =   "Direccion"
            Object.Width           =   7056
         EndProperty
      End
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   240
      OleObjectBlob   =   "BuscaProveedor.frx":007F
      Top             =   4620
   End
End
Attribute VB_Name = "BuscaProveedor"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim AdoProveedor As Recordset
Private Sub CmdCrear_Click()
    SG_codigo = Empty
    AgregoProveedor.Show 1
    CargaProveedores
End Sub

Private Sub cmdSalir_Click()
    RutBuscado = Empty
    Unload Me
End Sub

Private Sub CmdSeleccionar_Click()
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    SG_codigo = LvDetalle.SelectedItem
    RutBuscado = SG_codigo
    rutProveedor = True
    AccionProveedor = 0
    Unload Me
End Sub



Private Sub Form_Load()
    Aplicar_skin Me
    Centrar Me
    Sql = "SELECT p.rut_proveedor, nombre_empresa " & _
          "FROM maestro_proveedores p " & _
          "INNER JOIN par_asociacion_ruts a ON p.rut_proveedor=a.rut_pro " & _
          "WHERE a.rut_emp='" & SP_Rut_Activo & "' AND habilitado='SI' "
    Consulta AdoProveedor, Sql
    LLenar_Grilla AdoProveedor, Me, LvDetalle, False, True, True, False
    
End Sub
Private Sub CargaProveedores()
       Sql = "SELECT p.rut_proveedor, nombre_empresa " & _
          "FROM maestro_proveedores p " & _
          "INNER JOIN par_asociacion_ruts a ON p.rut_proveedor=a.rut_pro " & _
          "WHERE a.rut_emp='" & SP_Rut_Activo & "'"
    Consulta AdoProveedor, Sql
    LLenar_Grilla AdoProveedor, Me, LvDetalle, False, True, True, False
End Sub
Private Sub MshProveedor_DblClick()
    CmdSeleccionar_Click
End Sub

Private Sub LvDetalle_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    ordListView ColumnHeader, Me, LvDetalle
End Sub

Private Sub LvDetalle_DblClick()
    CmdSeleccionar_Click
End Sub

Private Sub LvDetalle_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 13 Then CmdSeleccionar_Click
End Sub

Private Sub TxtBusqueda_Change()
    If Len(Me.TxtBusqueda.Text) = 0 Then
        AdoProveedor.Filter = 0
    Else
        Filtro = "Nombre_Empresa Like '%" & Me.TxtBusqueda.Text & "*'"
        AdoProveedor.Filter = Filtro
    End If
    LLenar_Grilla AdoProveedor, Me, LvDetalle, False, True, True, False
End Sub

Private Sub TxtBusqueda_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii))) 'Transformo a mayuscula el caracter ingresado
    If KeyAscii = 13 Then Me.LvDetalle.SetFocus
     If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtBusqueda_Validate(Cancel As Boolean)
TxtBusqueda = Replace(TxtBusqueda, "'", "")
End Sub
