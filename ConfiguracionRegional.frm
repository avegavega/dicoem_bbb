VERSION 5.00
Begin VB.Form ConfiguracionRegional 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Optima Configuracion Regional"
   ClientHeight    =   8400
   ClientLeft      =   45
   ClientTop       =   405
   ClientWidth     =   9060
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8400
   ScaleWidth      =   9060
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton CmdSalir 
      Cancel          =   -1  'True
      Caption         =   "&Retornar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   4080
      TabIndex        =   1
      Top             =   7320
      Width           =   1815
   End
   Begin VB.PictureBox Picture1 
      Height          =   6735
      Left            =   1080
      Picture         =   "ConfiguracionRegional.frx":0000
      ScaleHeight     =   6675
      ScaleWidth      =   6315
      TabIndex        =   0
      Top             =   240
      Width           =   6375
   End
End
Attribute VB_Name = "ConfiguracionRegional"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdSalir_Click()
    Unload Me
End Sub

Private Sub Form_Resize()
Dim i As Integer
Dim Y As Integer
    With Me
        .Cls
        .AutoRedraw = True
        .DrawStyle = 1
        .DrawMode = 13
        .DrawWidth = 2
        .ScaleMode = 3
        .ScaleHeight = 512
        Y = 0
        For i = 255 To 0 Step -1
        
            ConfiguracionRegional.Line (0, Y)-(Principal.Width, Y + 2), RGB(0 + 30, i + 50, i + 100), BF
        
            Y = Y + 2
        Next i
    End With
    
End Sub

