VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form Ingreso_Mantenciones 
   Caption         =   "Ingreso de Mantenciones"
   ClientHeight    =   5760
   ClientLeft      =   6495
   ClientTop       =   1395
   ClientWidth     =   12090
   LinkTopic       =   "Form1"
   ScaleHeight     =   5760
   ScaleWidth      =   12090
   Begin VB.CommandButton Command1 
      Cancel          =   -1  'True
      Caption         =   "Grabar"
      Height          =   405
      Left            =   9060
      TabIndex        =   12
      Top             =   5070
      Width           =   1350
   End
   Begin VB.TextBox Text4 
      Height          =   360
      Left            =   2640
      TabIndex        =   9
      Top             =   1875
      Width           =   3030
   End
   Begin VB.TextBox Text3 
      Height          =   360
      Left            =   2640
      TabIndex        =   8
      Top             =   1470
      Width           =   3030
   End
   Begin VB.TextBox Text2 
      Height          =   360
      Left            =   2640
      TabIndex        =   7
      Top             =   1095
      Width           =   3030
   End
   Begin VB.Frame Frame1 
      Caption         =   "Ingreso de Mantenciones"
      Height          =   5535
      Left            =   105
      TabIndex        =   0
      Top             =   105
      Width           =   11865
      Begin VB.CommandButton cmdSalir 
         Caption         =   "&Retornar"
         Height          =   405
         Left            =   10380
         TabIndex        =   11
         Top             =   4965
         Width           =   1350
      End
      Begin VB.TextBox Text1 
         Height          =   360
         Left            =   2535
         TabIndex        =   6
         Top             =   600
         Width           =   3030
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel5 
         Height          =   270
         Left            =   1005
         OleObjectBlob   =   "Ingreso_Mantenciones.frx":0000
         TabIndex        =   5
         Top             =   1875
         Width           =   1320
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
         Height          =   255
         Left            =   1740
         OleObjectBlob   =   "Ingreso_Mantenciones.frx":007A
         TabIndex        =   4
         Top             =   1470
         Width           =   555
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   270
         Left            =   1515
         OleObjectBlob   =   "Ingreso_Mantenciones.frx":00E2
         TabIndex        =   3
         Top             =   1080
         Width           =   930
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   270
         Left            =   840
         OleObjectBlob   =   "Ingreso_Mantenciones.frx":0154
         TabIndex        =   1
         Top             =   705
         Width           =   1530
      End
      Begin MSComctlLib.ListView LvInsumos 
         Height          =   1980
         Left            =   300
         TabIndex        =   10
         Top             =   2685
         Width           =   11310
         _ExtentX        =   19950
         _ExtentY        =   3493
         View            =   3
         LabelWrap       =   0   'False
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   4
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "F1200"
            Text            =   "Codigo"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "T1100"
            Text            =   "Nombre Producto"
            Object.Width           =   7056
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "T100"
            Text            =   "Estado"
            Object.Width           =   5292
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Object.Tag             =   "T100"
            Text            =   "Monto"
            Object.Width           =   2646
         EndProperty
      End
      Begin VB.Frame Frame2 
         Caption         =   "Listado de Insumos"
         Height          =   2460
         Left            =   195
         TabIndex        =   13
         Top             =   2370
         Width           =   11565
      End
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
      Height          =   375
      Left            =   330
      OleObjectBlob   =   "Ingreso_Mantenciones.frx":01D4
      TabIndex        =   2
      Top             =   1230
      Width           =   1740
   End
End
Attribute VB_Name = "Ingreso_Mantenciones"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub CmdSalir_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    Centrar Me
    Aplicar_skin Me
End Sub

