VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Begin VB.Form Principal 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "VINET AUTOMOTRIZ "
   ClientHeight    =   8880
   ClientLeft      =   210
   ClientTop       =   1920
   ClientWidth     =   14865
   DrawMode        =   9  'Not Mask Pen
   DrawStyle       =   2  'Dot
   Icon            =   "Mian�.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   444
   ScaleMode       =   2  'Point
   ScaleWidth      =   743.25
   WhatsThisButton =   -1  'True
   WhatsThisHelp   =   -1  'True
   Begin VB.Frame Frame1 
      Caption         =   "Menu Principal"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5955
      Left            =   1230
      TabIndex        =   5
      Top             =   1845
      Width           =   11475
      Begin VB.CommandButton CmdMenu 
         Caption         =   "&Compras"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   600
         Index           =   0
         Left            =   375
         TabIndex        =   12
         Tag             =   "1"
         Top             =   645
         Width           =   4000
      End
      Begin VB.CommandButton CmdMenu 
         Caption         =   "&Ventas"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   600
         Index           =   1
         Left            =   360
         TabIndex        =   11
         Tag             =   "2"
         Top             =   1725
         Width           =   4000
      End
      Begin VB.CommandButton CmdMenu 
         Caption         =   "&Inventarios"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   600
         Index           =   2
         Left            =   375
         TabIndex        =   10
         Tag             =   "3"
         Top             =   2790
         Width           =   4000
      End
      Begin VB.CommandButton CmdMenu 
         Caption         =   "C&uentas Corrientes"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   600
         Index           =   3
         Left            =   360
         TabIndex        =   9
         Tag             =   "4"
         Top             =   3915
         Width           =   4000
      End
      Begin VB.CommandButton CmdMenu 
         Caption         =   "I&nformes"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   600
         Index           =   4
         Left            =   345
         TabIndex        =   8
         Tag             =   "5"
         Top             =   5085
         Width           =   4000
      End
      Begin VB.Frame FrmSub 
         Caption         =   "Sub-Menu"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   5280
         Left            =   4830
         TabIndex        =   6
         Top             =   630
         Width           =   5715
         Begin VB.CommandButton cmdSub 
            Caption         =   "Command6"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   765
            Index           =   0
            Left            =   900
            TabIndex        =   7
            Top             =   -210
            Visible         =   0   'False
            Width           =   4125
         End
      End
   End
   Begin VB.Frame Frame2 
      BackColor       =   &H80000009&
      Height          =   1035
      Left            =   150
      TabIndex        =   3
      Top             =   225
      Width           =   3645
      Begin VB.PictureBox Picture1 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   795
         Left            =   90
         Picture         =   "Mian�.frx":0442
         ScaleHeight     =   795
         ScaleWidth      =   3495
         TabIndex        =   4
         Top             =   165
         Width           =   3495
      End
   End
   Begin VB.CommandButton CmdSalir 
      Caption         =   "Salir"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   12120
      TabIndex        =   2
      Top             =   8130
      Width           =   2295
   End
   Begin VB.Frame Frame9 
      BackColor       =   &H00E0E0E0&
      Caption         =   "SISTEMA"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   -1  'True
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00404040&
      Height          =   855
      Left            =   4005
      TabIndex        =   0
      Top             =   300
      Width           =   10350
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   375
         Left            =   1470
         OleObjectBlob   =   "Mian�.frx":0DD8
         TabIndex        =   1
         Top             =   315
         Width           =   8190
      End
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   14190
      OleObjectBlob   =   "Mian�.frx":0E6C
      Top             =   1260
   End
   Begin VB.Menu m1 
      Caption         =   "Maestros"
      Visible         =   0   'False
      Begin VB.Menu p2 
         Caption         =   "Productos"
      End
      Begin VB.Menu sep 
         Caption         =   "-"
         Index           =   0
      End
      Begin VB.Menu cp2 
         Caption         =   "Clientes / Proveedores"
      End
      Begin VB.Menu sepx 
         Caption         =   "-"
      End
      Begin VB.Menu p1 
         Caption         =   "Parametros"
      End
   End
   Begin VB.Menu mnCompra 
      Caption         =   "Compra"
      Visible         =   0   'False
      Begin VB.Menu mnIngreseo 
         Caption         =   "Ingreso Repuestos, Vehiculos Nuevos y Usados"
      End
   End
   Begin VB.Menu v1 
      Caption         =   "Ventas"
      Visible         =   0   'False
      Begin VB.Menu v2 
         Caption         =   "Venta Directa"
      End
      Begin VB.Menu sep2 
         Caption         =   "-"
         Index           =   1
      End
      Begin VB.Menu vb2 
         Caption         =   "Ver Ventas Directas"
      End
   End
   Begin VB.Menu o1 
      Caption         =   "Ordenes de Trabajo"
      Visible         =   0   'False
      Begin VB.Menu sepq 
         Caption         =   "Crear Orden de Trabajo"
      End
      Begin VB.Menu sepaa 
         Caption         =   "-"
      End
      Begin VB.Menu editOt 
         Caption         =   "Editar Orden de Trabajo"
      End
      Begin VB.Menu sepo 
         Caption         =   "-"
      End
      Begin VB.Menu his 
         Caption         =   "Historico  Ordenes de Trabajo"
      End
   End
   Begin VB.Menu mnVehi 
      Caption         =   "Vehiculos"
      Visible         =   0   'False
      Begin VB.Menu mnvendevehiculo 
         Caption         =   "Vender Vehiculo"
      End
      Begin VB.Menu sep10 
         Caption         =   "-"
      End
      Begin VB.Menu mnMvehi 
         Caption         =   "Maestro Vehiculos"
      End
      Begin VB.Menu sep12 
         Caption         =   "-"
      End
      Begin VB.Menu mnarr 
         Caption         =   "Arrendar Vehiculo"
      End
   End
   Begin VB.Menu c1 
      Caption         =   "Cuentas Corrientes"
      Visible         =   0   'False
      Begin VB.Menu paycliente 
         Caption         =   "Pago de Clientes"
      End
      Begin VB.Menu seppay 
         Caption         =   "-"
      End
      Begin VB.Menu payprov 
         Caption         =   "Pago a Proveedores"
      End
   End
   Begin VB.Menu nmNotacdto 
      Caption         =   "Nota de Credito"
      Visible         =   0   'False
      Begin VB.Menu mncdito 
         Caption         =   "Nota de Credito"
      End
   End
   Begin VB.Menu i1 
      Caption         =   "Informes"
      Visible         =   0   'False
      Begin VB.Menu sepi 
         Caption         =   "Informes Varios"
      End
   End
   Begin VB.Menu s1 
      Caption         =   "Salir"
      Visible         =   0   'False
      Begin VB.Menu close 
         Caption         =   "Cerrar Sesion"
      End
   End
End
Attribute VB_Name = "Principal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim Formulario As Form
Private Sub close_Click()
    End
End Sub

Private Sub CmdArriendo_Click()
    If CmdArriendo.Enabled = True Then Arriendo.Show
End Sub

Private Sub CmdClientesProveedores_Click()
    If Me.CmdClientesProveedores.Enabled = True Then MaestroClientesProveedores.Show 1
End Sub

Private Sub cmdCommand3_Click()
    InformeDetalleCtroCosto.Show
End Sub

Private Sub cmdCommand4_Click()
    ActualizaVDMateriales.Show
End Sub

Private Sub cmdCommand5_Click()
  Dialogo.ShowOpen
    Skin1.LoadSkin Dialogo.FileName
    Skin1.ApplySkin Principal.hWnd
End Sub

Private Sub CmdCompraRp_Click()
    If Me.CmdCompraRp.Enabled = True Then
     QueActividad = 2
 '    CompraProducto.Timer1.Enabled = True
     CompraProducto.Show 1
    ' CompraProducto.Timer1.Enabled = False
    End If
End Sub

Private Sub CmdCompras_Click()
    Compra_Listado.Show
    
End Sub

Private Sub CmdConfiguraiconRegional_Click()
    ConfiguracionRegional.Show 1
End Sub

Private Sub CmdEditarOT_Click()
    If Me.CmdEditarOT.Enabled = False Then Exit Sub
    
    If FormularioCargado("OrdenTrabajo") = True Then
        MsgBox "Ya tiene una OT en proceso"
        Exit Sub
    End If

    Respuesta = InputBox("Ingrese el N� de OT", "Editar Orden de trabajo")
    If Respuesta = "" Then Exit Sub
    If IsNumeric(Respuesta) Then
        If Respuesta > 0 Then
            
            NumeroOrden = Respuesta
            
            Dim AdoOrden As ADODB.Recordset
            
            Sql = "SELECT * FROM ot WHERE no_orden = " & NumeroOrden
            paso = Consulta(AdoOrden, Sql)
            
            
            Respuesta = AdoOrden.RecordCount
            
           
            
            If Respuesta > 0 Then
                If AdoOrden!Cliente = "OT Nula" Then
                    MsgBox "Esta orden fue Anulada, no se puede editar", vbInformation
                    AdoOrden.close
                    Exit Sub
                End If
                
                
                    
                EstadoOT = "Editar"
                    
                
                OrdenTrabajo.Caption = "Orden de Trabajo N�" & NumeroOrden
                OrdenTrabajo.Show
            Else
                MsgBox "Orden No encontrada...", vbOKOnly + vbExclamation
                 AdoOrden.close
                Exit Sub
            End If
            AdoOrden.close
        End If
    Else
        MsgBox "Datos no v�lidos..", vbInformation
    End If
End Sub

Private Sub CmdHistoricoFacturas_Click()
    If Me.CmdHistoricoFacturas.Enabled = True Then HistoricoFacturas.Show
End Sub



Private Sub cmdHistorioDetalleOts_Click()
    InformeDetalleOTS.Show
End Sub

Private Sub CmdInformes_Click()
    If Me.CmdInformes.Enabled = True Then INFORMES.Show
End Sub

Private Sub CmdLibrocompras_Click()
    LibroCompras.Show
End Sub

Private Sub CmdLibroVentas_Click()
    LibroVentas.Show
End Sub

Private Sub CmdListaOT_Click()
         If Me.CmdListaOT.Enabled = True Then Historico.Show
End Sub

Private Sub CMDMaestroVehiculos_Click()
    If Me.CMDMaestroVehiculos.Enabled = True Then MaestroVehiculo.Show
End Sub

Private Sub CmdMastroProducto_Click()
    If Me.CmdMastroProducto.Enabled = False Then Exit Sub
   ' Unload MasterProductos
    'MasterProductos.AdoProductos.Refresh
    MasterProductos.Show
End Sub

Private Sub CmdNC_Click()
    If CmdNC.Enabled = True Then NotaCdtoVenta.Show
End Sub

Private Sub cmdNcProveedores_Click()
    NcdProveedores.Show
End Sub

Private Sub CmdOT_Click()
    If CmdOT.Enabled = False Then Exit Sub
    QueActividad = 1
    EstadoOT = "Nueva"
    If FormularioCargado("OrdenTrabajo") = True Then
        MsgBox "Ya tiene una OT en proceso"
        Exit Sub
    End If
    OrdenTrabajo.Show
End Sub

Private Sub CmdPagoClientes_Click()
    If Me.CmdPagoClientes.Enabled = False Then Exit Sub
 '   Pago_Clientes.Timer1 = True
    ClienteOproveedor = "Cliente"
    Pago_Clientes.Show
'    Pago_Clientes.Timer1 = False
End Sub

Private Sub cmdPagoPr_Click()
    If cmdPagoPr.Enabled = False Then Exit Sub
    ClienteOproveedor = "Proveedor"
    Pago_Proveedores.Timerx.Enabled = True
    Pago_Proveedores.Show
End Sub

Private Sub CmdParametros_Click()
    If Me.CmdParametros.Enabled = True Then Parametros.Show 1
End Sub

Private Sub cmdPlanCuentas_Click()
    LineasItemsGastos.Show 1
End Sub

Private Sub CmdReporteProductos_Click()
    ReporteRepuestos.Show
End Sub

Private Sub CmdMenu_Click(Index As Integer)
    If cmdSub.Count > 2 Then
        For I = cmdSub.Count - 1 To 1 Step -1
            Unload cmdSub(I)
        Next
    ElseIf cmdSub.Count = 2 Then
        Unload cmdSub(1)
    End If
    

    Sql = "SELECT men_id,men_padre,men_nombre,men_tooltips " & _
          "FROM sis_menu " & _
          "WHERE men_padre=" & CmdMenu(Index).Tag & " AND men_activo='SI'"
    Call Query(Rst_tmp, Sql)
    FrmSub.Caption = "Sub-Menu de " & CmdMenu(Index).Caption
    If Rst_tmp.RecordCount > 0 Then
        With Rst_tmp
            I = 1
            .MoveFirst
            Do While Not .EOF
                
                        
                 Load cmdSub(I)
                 cmdSub(I).Caption = Rst_tmp!men_nombre
                 cmdSub(I).Height = cmdSub(0).Height
                 cmdSub(I).Top = cmdSub(I - 1).Top + cmdSub(0).Height + 100
                 cmdSub(I).ToolTipText = Rst_tmp!men_tooltips
                 cmdSub(I).Visible = True
                 cmdSub(I).Tag = Rst_tmp!men_id
                I = I + 1
                .MoveNext
            Loop
        End With
    End If
    
    Aplicar_skin Me
End Sub

Private Sub CmdSalir_Click()
    End
End Sub

Private Sub CmdVentaDirecta_Click()

    If Me.CmdVentaDirecta.Enabled = True Then
        If FormularioCargado("vtadirecta") = True Then
            MsgBox "Ya est� creando o revisando una " & Chr(34) & " Venta directa" & Chr(34)
            Exit Sub
        End If
        SaldarInmediato = False
        EstadoOT = "VentaDirecta"
        VtaDirecta.Show
    End If
End Sub

Private Sub cMDvTAVehiculo_Click()
    If Me.cMDvTAVehiculo.Enabled = False Then Exit Sub
    VtaVehiculo.Timer1 = True
    VtaVehiculo.Show
    
End Sub

Private Sub Command2_Click()
    Usuarios.Show 1
'DataReport1.Show
End Sub

Private Sub Command3_Click()
    Form1.Show 1
End Sub

Private Sub Command4_Click()
    
End Sub

Private Sub Command5_Click()
   con_librocompra.Show 1
End Sub

Private Sub cmdSub_Click(Index As Integer)
    Sql = "SELECT men_id,men_nombre,men_formulario " & _
          "FROM sis_menu " & _
          "WHERE men_id=" & cmdSub(Index).Tag & " AND men_activo='SI'"
    Call Consulta(Rst_tmp, Sql)
    If Rst_tmp.RecordCount > 0 Then
        If IsNull(Rst_tmp!men_formulario) Or Len(Rst_tmp!men_formulario) = 0 Then Exit Sub
        Set Formulario = Forms.Add(Rst_tmp!men_formulario)
        Formulario.Show vbModal
        
    Else
        MsgBox "Acceso no autorizado", vbInformation
    
    End If
    
End Sub

Private Sub cp2_Click()
If Me.CmdClientesProveedores.Enabled = True Then CmdClientesProveedores_Click
End Sub

Private Sub editOt_Click()
    If editOt.Enabled = True Then CmdEditarOT_Click
End Sub

Private Sub Form_Load()

  '  Acceso.Show 1
    ClrCfoco = &H80FF80
    ClrSfoco = &H80000014
    ClrDesha = &HE0E0E0
    
    Sql = "SELECT * FROM sis_empresas "
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        
        With RsTmp
            RutEmpresa = .Fields(0)
            RsEmpresa = !nombre_empresa
            DireccionEmpresa = !DIRECCION
            GiroEmpresa = !Giro
            
        End With
    End If
    Aplicar_skin Me
   
    
   
  
End Sub

Private Sub Form_Unload(Cancel As Integer)
    End
End Sub




Private Sub Frame1_DragDrop(Source As Control, X As Single, Y As Single)

End Sub

Private Sub his_Click()
CmdListaOT_Click
End Sub

Private Sub mnarr_Click()
CmdArriendo_Click
End Sub

Private Sub mncdito_Click()
    CmdNC_Click
End Sub

Private Sub mnIngreseo_Click()
CmdCompraRp_Click
End Sub

Private Sub mnMvehi_Click()
CMDMaestroVehiculos_Click
End Sub

Private Sub mnvendevehiculo_Click()
    If Me.cMDvTAVehiculo.Enabled = True Then cMDvTAVehiculo_Click
End Sub

Private Sub p1_Click()
CmdParametros_Click
End Sub

Private Sub p2_Click()
    CmdMastroProducto_Click
End Sub

Private Sub paycliente_Click()
CmdPagoClientes_Click
End Sub

Private Sub payprov_Click()
cmdPagoPr_Click
End Sub

Private Sub sepq_Click()
CmdOT_Click
End Sub

Private Sub v2_Click()
CmdVentaDirecta_Click
End Sub

Private Sub vb2_Click()
CmdHistoricoFacturas_Click
End Sub



