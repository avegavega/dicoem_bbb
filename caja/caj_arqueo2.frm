VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Object = "{DA729E34-689F-49EA-A856-B57046630B73}#1.0#0"; "progressbar-xp.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form caj_arqueo 
   Caption         =   "Arqueo de Caja"
   ClientHeight    =   9900
   ClientLeft      =   2070
   ClientTop       =   1740
   ClientWidth     =   15405
   LinkTopic       =   "Form1"
   ScaleHeight     =   9900
   ScaleWidth      =   15405
   Begin MSComctlLib.ListView LvParaImprimir 
      Height          =   3450
      Left            =   20160
      TabIndex        =   86
      Top             =   4950
      Visible         =   0   'False
      Width           =   9630
      _ExtentX        =   16986
      _ExtentY        =   6085
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   12
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Object.Tag             =   "T1000"
         Text            =   "F. pago"
         Object.Width           =   2293
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Object.Tag             =   "F1000"
         Text            =   "Emision"
         Object.Width           =   1764
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Object.Tag             =   "F1000"
         Text            =   "Venc."
         Object.Width           =   1764
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Object.Tag             =   "T2000"
         Text            =   "Documento "
         Object.Width           =   1587
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Object.Tag             =   "T1000"
         Text            =   "Nro"
         Object.Width           =   1764
      EndProperty
      BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   5
         Object.Tag             =   "T1000"
         Text            =   "Cliente"
         Object.Width           =   4762
      EndProperty
      BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   6
         Key             =   "total"
         Object.Tag             =   "N100"
         Text            =   "Monto"
         Object.Width           =   2117
      EndProperty
      BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   7
         Object.Tag             =   "T1000"
         Text            =   "Rut Cliente"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   8
         Object.Tag             =   "N109"
         Text            =   "NC X"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   9
         Object.Tag             =   "N100"
         Text            =   "NETO"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   10
         Object.Tag             =   "N100"
         Text            =   "IVA"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(12) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   11
         Text            =   "NombreDoc"
         Object.Width           =   2540
      EndProperty
   End
   Begin VB.ComboBox CboFpago 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   15690
      Style           =   2  'Dropdown List
      TabIndex        =   83
      Top             =   570
      Visible         =   0   'False
      Width           =   2130
   End
   Begin VB.CommandButton cmdSalir 
      Caption         =   "&Retornar"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   465
      Left            =   13080
      TabIndex        =   36
      Top             =   9030
      Width           =   1635
   End
   Begin VB.Frame Frame4 
      Caption         =   "Accion"
      Height          =   1395
      Left            =   7980
      TabIndex        =   0
      Top             =   8160
      Width           =   4215
      Begin VB.CommandButton CmdDesbloquear 
         Caption         =   "Desbloquear Caja"
         Height          =   510
         Left            =   420
         TabIndex        =   99
         Top             =   765
         Visible         =   0   'False
         Width           =   3345
      End
      Begin VB.CommandButton CmdImprimir 
         Caption         =   "Imprimir"
         Height          =   345
         Left            =   2160
         TabIndex        =   2
         Top             =   285
         Width           =   1620
      End
      Begin VB.CommandButton cmdCerrar 
         Caption         =   "Cerrar Caja"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   360
         TabIndex        =   1
         Top             =   285
         Width           =   1620
      End
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   9510
      Left            =   105
      TabIndex        =   3
      Top             =   180
      Width           =   14805
      _ExtentX        =   26114
      _ExtentY        =   16775
      _Version        =   393216
      Tabs            =   2
      TabsPerRow      =   2
      TabHeight       =   520
      TabCaption(0)   =   "Informe de Caja"
      TabPicture(0)   =   "caj_arqueo2.frx":0000
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Frame3"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Frame5"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "Frame6"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "Frame1"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "Frame2"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "Frame7"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).Control(6)=   "Frame13"
      Tab(0).Control(6).Enabled=   0   'False
      Tab(0).ControlCount=   7
      TabCaption(1)   =   "Arqueo"
      TabPicture(1)   =   "caj_arqueo2.frx":001C
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "FrmMantenedor"
      Tab(1).Control(1)=   "Frame8"
      Tab(1).Control(2)=   "Frame9"
      Tab(1).Control(3)=   "Frame10"
      Tab(1).Control(4)=   "Frame11"
      Tab(1).Control(5)=   "Frame12"
      Tab(1).ControlCount=   6
      Begin VB.Frame Frame13 
         Caption         =   " Reportes de Jornada "
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000C0&
         Height          =   1905
         Left            =   4770
         TabIndex        =   92
         Top             =   7500
         Visible         =   0   'False
         Width           =   2715
         Begin VB.CommandButton CmdInformeX 
            BackColor       =   &H00C0FFC0&
            Caption         =   "Reporte X"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   225
            Style           =   1  'Graphical
            TabIndex        =   97
            ToolTipText     =   " Emite INFORME X del Momento "
            Top             =   675
            Width           =   2220
         End
         Begin VB.CommandButton CmdCierreCajero 
            BackColor       =   &H00C0FFC0&
            Caption         =   "Cierre Turno de Cajero"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   225
            Style           =   1  'Graphical
            TabIndex        =   96
            ToolTipText     =   " Emite CIERRE DE TURNO DE CAJERO "
            Top             =   315
            Width           =   2220
         End
         Begin VB.Frame Frame14 
            Height          =   795
            Left            =   105
            TabIndex        =   93
            Top             =   1020
            Width           =   2445
            Begin VB.CommandButton CmdCierreDiarioZ 
               BackColor       =   &H00C0FFC0&
               Caption         =   "Cierre Diario Z"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   225
               Style           =   1  'Graphical
               TabIndex        =   95
               ToolTipText     =   " Emite CIERRE DE JORNADA DIARIA "
               Top             =   150
               Width           =   1950
            End
            Begin VB.CheckBox ChkImprimeZ 
               Caption         =   "Imprime Reporte"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00C00000&
               Height          =   195
               Left            =   285
               TabIndex        =   94
               TabStop         =   0   'False
               ToolTipText     =   " Activa / Desactiva Impresi�n de Informes "
               Top             =   495
               Value           =   1  'Checked
               Width           =   1740
            End
         End
      End
      Begin VB.Frame Frame12 
         Caption         =   "Deposito"
         Height          =   1035
         Left            =   -66405
         TabIndex        =   87
         Top             =   7185
         Width           =   5910
         Begin VB.TextBox TxtMontoInicial 
            Alignment       =   1  'Right Justify
            Height          =   315
            Left            =   3225
            Locked          =   -1  'True
            TabIndex        =   91
            TabStop         =   0   'False
            Tag             =   "T"
            Text            =   "0"
            Top             =   570
            Width           =   2000
         End
         Begin VB.TextBox TxtMontoDeposito 
            Alignment       =   1  'Right Justify
            Height          =   315
            Left            =   525
            TabIndex        =   90
            TabStop         =   0   'False
            Tag             =   "N"
            Text            =   "0"
            Top             =   585
            Width           =   2000
         End
         Begin ACTIVESKINLibCtl.SkinLabel Valor 
            Height          =   255
            Index           =   0
            Left            =   555
            OleObjectBlob   =   "caj_arqueo2.frx":0038
            TabIndex        =   88
            Top             =   345
            Width           =   1950
         End
         Begin ACTIVESKINLibCtl.SkinLabel Valor 
            Height          =   210
            Index           =   2
            Left            =   3225
            OleObjectBlob   =   "caj_arqueo2.frx":00B2
            TabIndex        =   89
            Top             =   345
            Width           =   1965
         End
      End
      Begin VB.Frame Frame11 
         Caption         =   " INGRESOS POR CAJA"
         Height          =   2835
         Left            =   -74595
         TabIndex        =   72
         Top             =   3465
         Width           =   7290
         Begin VB.TextBox TxtIdI 
            Alignment       =   1  'Right Justify
            BackColor       =   &H8000000F&
            Height          =   315
            Left            =   165
            Locked          =   -1  'True
            TabIndex        =   77
            TabStop         =   0   'False
            Tag             =   "T"
            Top             =   495
            Width           =   615
         End
         Begin VB.TextBox TxtIngreso 
            Height          =   315
            Left            =   780
            TabIndex        =   76
            Tag             =   "T"
            Top             =   495
            Width           =   4560
         End
         Begin VB.CommandButton CmdOkIngreso 
            Caption         =   "Ok"
            Height          =   300
            Left            =   6345
            TabIndex        =   75
            Top             =   495
            Width           =   435
         End
         Begin VB.TextBox txtValorIngreso 
            Alignment       =   1  'Right Justify
            Height          =   315
            Left            =   5340
            TabIndex        =   74
            Tag             =   "T"
            Top             =   495
            Width           =   1050
         End
         Begin VB.TextBox TxtTotalIngresos 
            Alignment       =   1  'Right Justify
            BackColor       =   &H8000000F&
            Height          =   315
            Left            =   5565
            Locked          =   -1  'True
            TabIndex        =   73
            TabStop         =   0   'False
            Tag             =   "T"
            Text            =   "0"
            Top             =   2295
            Width           =   1050
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel10 
            Height          =   195
            Index           =   2
            Left            =   120
            OleObjectBlob   =   "caj_arqueo2.frx":0142
            TabIndex        =   78
            Top             =   2325
            Width           =   5205
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
            Height          =   255
            Index           =   1
            Left            =   765
            OleObjectBlob   =   "caj_arqueo2.frx":01EC
            TabIndex        =   79
            Top             =   285
            Width           =   1095
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel5 
            Height          =   195
            Index           =   4
            Left            =   5655
            OleObjectBlob   =   "caj_arqueo2.frx":0260
            TabIndex        =   80
            Top             =   285
            Width           =   735
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
            Height          =   255
            Index           =   4
            Left            =   165
            OleObjectBlob   =   "caj_arqueo2.frx":02C8
            TabIndex        =   81
            Top             =   285
            Width           =   615
         End
         Begin MSComctlLib.ListView LvIngresos 
            Height          =   1335
            Left            =   150
            TabIndex        =   82
            Top             =   855
            Width           =   6660
            _ExtentX        =   11748
            _ExtentY        =   2355
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            FullRowSelect   =   -1  'True
            GridLines       =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   0
            NumItems        =   4
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Object.Tag             =   "N109"
               Text            =   "Id"
               Object.Width           =   1058
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Object.Tag             =   "T4000"
               Text            =   "Descripcion"
               Object.Width           =   8043
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   2
               Key             =   "total"
               Object.Tag             =   "N100"
               Text            =   "Valor"
               Object.Width           =   1614
            EndProperty
            BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   3
               Text            =   "CAMBIA?"
               Object.Width           =   0
            EndProperty
         End
      End
      Begin VB.Frame Frame10 
         Caption         =   "Resumen Arqueo"
         Height          =   2925
         Left            =   -66420
         TabIndex        =   64
         Top             =   4155
         Width           =   5910
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            Height          =   315
            Left            =   5595
            TabIndex        =   65
            Tag             =   "T"
            Top             =   3690
            Width           =   915
         End
         Begin MSComctlLib.ListView LvResumenArqueo 
            Height          =   2550
            Left            =   240
            TabIndex        =   66
            Top             =   240
            Width           =   5355
            _ExtentX        =   9446
            _ExtentY        =   4498
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            FullRowSelect   =   -1  'True
            GridLines       =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   0
            NumItems        =   3
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Object.Tag             =   "N109"
               Text            =   "Id"
               Object.Width           =   1058
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Object.Tag             =   "T4000"
               Text            =   "Detalle"
               Object.Width           =   5292
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   2
               Object.Tag             =   "N100"
               Text            =   "Valor"
               Object.Width           =   2117
            EndProperty
         End
      End
      Begin VB.Frame Frame9 
         Caption         =   "Detalle de Cheques"
         Height          =   2790
         Left            =   -74670
         TabIndex        =   61
         Top             =   6480
         Width           =   7320
         Begin VB.TextBox TxtTotalCheques 
            Alignment       =   1  'Right Justify
            BackColor       =   &H8000000F&
            Height          =   315
            Left            =   5745
            Locked          =   -1  'True
            TabIndex        =   62
            TabStop         =   0   'False
            Tag             =   "T"
            Text            =   "0"
            Top             =   2325
            Width           =   915
         End
         Begin MSComctlLib.ListView LvCheques 
            Height          =   1905
            Left            =   195
            TabIndex        =   63
            Top             =   300
            Width           =   6585
            _ExtentX        =   11615
            _ExtentY        =   3360
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            FullRowSelect   =   -1  'True
            GridLines       =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   0
            NumItems        =   5
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Object.Tag             =   "N109"
               Text            =   "Id"
               Object.Width           =   1058
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Object.Tag             =   "T4000"
               Text            =   "Nro Cheque"
               Object.Width           =   1940
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   2
               Object.Tag             =   "T3000"
               Text            =   "Banco"
               Object.Width           =   5292
            EndProperty
            BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   3
               Key             =   "total"
               Object.Tag             =   "N100"
               Text            =   "Valor"
               Object.Width           =   1676
            EndProperty
            BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   4
               Object.Tag             =   "F1000"
               Text            =   "Fecha"
               Object.Width           =   2540
            EndProperty
         End
      End
      Begin VB.Frame Frame8 
         Caption         =   "Detalle de Billetes"
         Height          =   3795
         Left            =   -66435
         TabIndex        =   48
         Top             =   300
         Width           =   5880
         Begin VB.TextBox txtCantBillete 
            Alignment       =   1  'Right Justify
            Height          =   315
            Left            =   3105
            TabIndex        =   59
            Tag             =   "T"
            Top             =   615
            Width           =   950
         End
         Begin VB.TextBox TxtIdBillete 
            Alignment       =   1  'Right Justify
            BackColor       =   &H8000000F&
            Height          =   315
            Left            =   495
            Locked          =   -1  'True
            TabIndex        =   53
            TabStop         =   0   'False
            Tag             =   "T"
            Top             =   615
            Width           =   615
         End
         Begin VB.TextBox TxTValorBillete 
            Height          =   315
            Left            =   1110
            Locked          =   -1  'True
            TabIndex        =   52
            TabStop         =   0   'False
            Tag             =   "T"
            Top             =   615
            Width           =   2000
         End
         Begin VB.CommandButton CmdOkDetBillete 
            Caption         =   "Ok"
            Height          =   300
            Left            =   4995
            TabIndex        =   51
            Top             =   615
            Width           =   450
         End
         Begin VB.TextBox txtTotalBillete 
            Alignment       =   1  'Right Justify
            Height          =   315
            Left            =   4050
            Locked          =   -1  'True
            TabIndex        =   50
            TabStop         =   0   'False
            Tag             =   "T"
            Top             =   615
            Width           =   950
         End
         Begin VB.TextBox TxtTBilletes 
            Alignment       =   1  'Right Justify
            BackColor       =   &H8000000F&
            Height          =   315
            Left            =   4245
            Locked          =   -1  'True
            TabIndex        =   49
            TabStop         =   0   'False
            Tag             =   "T"
            Text            =   "0"
            Top             =   3405
            Width           =   915
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel11 
            Height          =   210
            Left            =   510
            OleObjectBlob   =   "caj_arqueo2.frx":032A
            TabIndex        =   54
            Top             =   3420
            Width           =   3420
         End
         Begin ACTIVESKINLibCtl.SkinLabel Valor 
            Height          =   255
            Index           =   1
            Left            =   1095
            OleObjectBlob   =   "caj_arqueo2.frx":03CA
            TabIndex        =   55
            Top             =   405
            Width           =   1095
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel5 
            Height          =   195
            Index           =   1
            Left            =   4215
            OleObjectBlob   =   "caj_arqueo2.frx":0432
            TabIndex        =   56
            Top             =   420
            Width           =   735
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
            Height          =   255
            Index           =   2
            Left            =   495
            OleObjectBlob   =   "caj_arqueo2.frx":049A
            TabIndex        =   57
            Top             =   405
            Width           =   615
         End
         Begin MSComctlLib.ListView LvBilletes 
            Height          =   2385
            Left            =   495
            TabIndex        =   58
            Top             =   945
            Width           =   4965
            _ExtentX        =   8758
            _ExtentY        =   4207
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            FullRowSelect   =   -1  'True
            GridLines       =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   0
            NumItems        =   4
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Object.Tag             =   "N109"
               Text            =   "Id"
               Object.Width           =   1058
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Object.Tag             =   "T4000"
               Text            =   "Valor"
               Object.Width           =   3528
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   2
               Object.Tag             =   "N100"
               Text            =   "Cantidad"
               Object.Width           =   1676
            EndProperty
            BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   3
               Key             =   "totalbillete"
               Text            =   "Total"
               Object.Width           =   1676
            EndProperty
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel5 
            Height          =   195
            Index           =   3
            Left            =   3270
            OleObjectBlob   =   "caj_arqueo2.frx":04FC
            TabIndex        =   60
            Top             =   420
            Width           =   735
         End
      End
      Begin VB.Frame FrmMantenedor 
         Caption         =   "Gastos por caja (Las en EFECTIVO del m�dulo de compras, se reflejan en esta opci�n )"
         Height          =   2835
         Left            =   -74595
         TabIndex        =   37
         Top             =   555
         Width           =   7290
         Begin VB.TextBox txtTotalGasto 
            Alignment       =   1  'Right Justify
            BackColor       =   &H8000000F&
            Height          =   315
            Left            =   5565
            Locked          =   -1  'True
            TabIndex        =   47
            TabStop         =   0   'False
            Tag             =   "T"
            Text            =   "0"
            Top             =   2295
            Width           =   1050
         End
         Begin VB.TextBox txtValorGasto 
            Alignment       =   1  'Right Justify
            Height          =   315
            Left            =   5340
            TabIndex        =   41
            Tag             =   "T"
            Top             =   495
            Width           =   1050
         End
         Begin VB.CommandButton CmdOkGasto 
            Caption         =   "Ok"
            Height          =   300
            Left            =   6345
            TabIndex        =   42
            Top             =   495
            Width           =   435
         End
         Begin VB.TextBox TxtGasto 
            Height          =   315
            Left            =   780
            TabIndex        =   40
            Tag             =   "T"
            Top             =   495
            Width           =   4560
         End
         Begin VB.TextBox TxtId 
            Alignment       =   1  'Right Justify
            BackColor       =   &H8000000F&
            Height          =   315
            Left            =   165
            Locked          =   -1  'True
            TabIndex        =   39
            TabStop         =   0   'False
            Tag             =   "T"
            Top             =   495
            Width           =   615
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel10 
            Height          =   195
            Index           =   0
            Left            =   120
            OleObjectBlob   =   "caj_arqueo2.frx":056A
            TabIndex        =   38
            Top             =   2325
            Width           =   6675
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
            Height          =   255
            Index           =   3
            Left            =   765
            OleObjectBlob   =   "caj_arqueo2.frx":0614
            TabIndex        =   44
            Top             =   285
            Width           =   1095
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel5 
            Height          =   195
            Index           =   2
            Left            =   5655
            OleObjectBlob   =   "caj_arqueo2.frx":0688
            TabIndex        =   45
            Top             =   285
            Width           =   735
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
            Height          =   255
            Index           =   5
            Left            =   165
            OleObjectBlob   =   "caj_arqueo2.frx":06F0
            TabIndex        =   46
            Top             =   285
            Width           =   615
         End
         Begin MSComctlLib.ListView LvGastos 
            Height          =   1335
            Left            =   150
            TabIndex        =   43
            Top             =   855
            Width           =   6660
            _ExtentX        =   11748
            _ExtentY        =   2355
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            FullRowSelect   =   -1  'True
            GridLines       =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   0
            NumItems        =   4
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Object.Tag             =   "N109"
               Text            =   "Id"
               Object.Width           =   1058
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Object.Tag             =   "T4000"
               Text            =   "Descripcion"
               Object.Width           =   8043
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   2
               Key             =   "total"
               Object.Tag             =   "N100"
               Text            =   "Valor"
               Object.Width           =   1614
            EndProperty
            BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   3
               Text            =   "CAMBIA?"
               Object.Width           =   0
            EndProperty
         End
      End
      Begin VB.Frame Frame7 
         Caption         =   "Detalle por pagos de Clientes"
         Height          =   2520
         Left            =   4785
         TabIndex        =   32
         Top             =   4905
         Width           =   9855
         Begin VB.TextBox txtTotalCtaCte 
            Alignment       =   1  'Right Justify
            BackColor       =   &H8000000F&
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Left            =   8265
            Locked          =   -1  'True
            TabIndex        =   33
            Text            =   "0"
            Top             =   2085
            Width           =   1230
         End
         Begin MSComctlLib.ListView LvDetalleCtaCte 
            Height          =   1830
            Left            =   165
            TabIndex        =   34
            Top             =   225
            Width           =   9630
            _ExtentX        =   16986
            _ExtentY        =   3228
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            FullRowSelect   =   -1  'True
            GridLines       =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            NumItems        =   7
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Object.Tag             =   "N109"
               Text            =   "F. pago"
               Object.Width           =   2293
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Object.Tag             =   "F1000"
               Text            =   "Emision"
               Object.Width           =   1764
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   2
               Object.Tag             =   "F1000"
               Text            =   "Venc."
               Object.Width           =   1764
            EndProperty
            BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   3
               Object.Tag             =   "T2000"
               Text            =   "Documento "
               Object.Width           =   1587
            EndProperty
            BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   4
               Object.Tag             =   "T1000"
               Text            =   "Nro"
               Object.Width           =   1764
            EndProperty
            BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   5
               Object.Tag             =   "T1000"
               Text            =   "Cliente"
               Object.Width           =   4939
            EndProperty
            BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   6
               Key             =   "total"
               Object.Tag             =   "N100"
               Text            =   "Monto"
               Object.Width           =   2117
            EndProperty
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel9 
            Height          =   225
            Left            =   6465
            OleObjectBlob   =   "caj_arqueo2.frx":0752
            TabIndex        =   35
            Top             =   2145
            Width           =   1740
         End
      End
      Begin VB.Frame Frame2 
         Caption         =   "Detalle por ventas"
         Height          =   4455
         Left            =   4800
         TabIndex        =   25
         Top             =   315
         Width           =   9840
         Begin VB.CommandButton CmdNoIncluir 
            Caption         =   "No Incluir Marcadas"
            Height          =   195
            Left            =   2985
            TabIndex        =   98
            Top             =   3795
            Visible         =   0   'False
            Width           =   1785
         End
         Begin VB.CommandButton CmdLaserTinta 
            BackColor       =   &H00FFFFFF&
            Height          =   525
            Left            =   1515
            Picture         =   "caj_arqueo2.frx":07BA
            Style           =   1  'Graphical
            TabIndex        =   84
            Top             =   3765
            Width           =   1245
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel14 
            Height          =   300
            Left            =   5250
            OleObjectBlob   =   "caj_arqueo2.frx":0C57
            TabIndex        =   70
            Top             =   4140
            Width           =   4530
         End
         Begin VB.CommandButton CmdExportar 
            Caption         =   "&Exportar"
            Height          =   525
            Left            =   90
            TabIndex        =   31
            Top             =   3765
            Width           =   1350
         End
         Begin VB.TextBox txtTotalDoc 
            Alignment       =   1  'Right Justify
            BackColor       =   &H8000000F&
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Left            =   8205
            Locked          =   -1  'True
            TabIndex        =   29
            Text            =   "0"
            Top             =   3720
            Width           =   1290
         End
         Begin VB.Frame FraProgreso 
            Caption         =   "Progreso exportaci�n"
            Height          =   795
            Left            =   825
            TabIndex        =   26
            Top             =   2205
            Visible         =   0   'False
            Width           =   8265
            Begin Proyecto2.XP_ProgressBar BarraProgreso 
               Height          =   330
               Left            =   -1620
               TabIndex        =   27
               Top             =   270
               Width           =   9750
               _ExtentX        =   17198
               _ExtentY        =   582
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               BrushStyle      =   0
               Color           =   16777088
               Scrolling       =   1
               ShowText        =   -1  'True
            End
         End
         Begin MSComctlLib.ListView LvDetalleDoc 
            Height          =   3450
            Left            =   90
            TabIndex        =   28
            Top             =   255
            Width           =   9630
            _ExtentX        =   16986
            _ExtentY        =   6085
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            Checkboxes      =   -1  'True
            FullRowSelect   =   -1  'True
            GridLines       =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            NumItems        =   13
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Object.Tag             =   "T1000"
               Text            =   "F. pago"
               Object.Width           =   2293
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Object.Tag             =   "F1000"
               Text            =   "Emision"
               Object.Width           =   1764
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   2
               Object.Tag             =   "F1000"
               Text            =   "Venc."
               Object.Width           =   1764
            EndProperty
            BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   3
               Object.Tag             =   "T2000"
               Text            =   "Documento "
               Object.Width           =   1587
            EndProperty
            BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   4
               Object.Tag             =   "T1000"
               Text            =   "Nro"
               Object.Width           =   1764
            EndProperty
            BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   5
               Object.Tag             =   "T1000"
               Text            =   "Cliente"
               Object.Width           =   4762
            EndProperty
            BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   6
               Key             =   "total"
               Object.Tag             =   "N100"
               Text            =   "Monto"
               Object.Width           =   2117
            EndProperty
            BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   7
               Object.Tag             =   "T1000"
               Text            =   "Rut Cliente"
               Object.Width           =   0
            EndProperty
            BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   8
               Object.Tag             =   "N109"
               Text            =   "NC X"
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   9
               Object.Tag             =   "N100"
               Text            =   "NETO"
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   10
               Object.Tag             =   "N100"
               Text            =   "IVA"
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(12) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   11
               Text            =   "NombreDoc"
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(13) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   12
               Object.Tag             =   "N109"
               Text            =   "Nro Tur ID VM"
               Object.Width           =   2540
            EndProperty
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel6 
            Height          =   225
            Left            =   6420
            OleObjectBlob   =   "caj_arqueo2.frx":0D36
            TabIndex        =   30
            Top             =   3795
            Width           =   1740
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "Resumen de Caja"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2505
         Left            =   210
         TabIndex        =   21
         Top             =   6540
         Width           =   4395
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel15 
            Height          =   240
            Left            =   -45
            OleObjectBlob   =   "caj_arqueo2.frx":0D9E
            TabIndex        =   71
            Top             =   2130
            Width           =   4050
         End
         Begin VB.TextBox txtTotal 
            Alignment       =   1  'Right Justify
            BackColor       =   &H8000000F&
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   1965
            Locked          =   -1  'True
            TabIndex        =   23
            Text            =   "0"
            Top             =   1725
            Width           =   2055
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel5 
            Height          =   225
            Index           =   0
            Left            =   525
            OleObjectBlob   =   "caj_arqueo2.frx":0E32
            TabIndex        =   22
            Top             =   1740
            Width           =   1305
         End
         Begin MSComctlLib.ListView LvDetalle 
            Height          =   1365
            Left            =   225
            TabIndex        =   24
            Top             =   345
            Width           =   3975
            _ExtentX        =   7011
            _ExtentY        =   2408
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            FullRowSelect   =   -1  'True
            GridLines       =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            NumItems        =   3
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Object.Tag             =   "N109"
               Text            =   "Id"
               Object.Width           =   0
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Object.Tag             =   "T7000"
               Text            =   "Documento"
               Object.Width           =   4410
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   2
               Key             =   "total"
               Object.Tag             =   "N100"
               Text            =   "Venta"
               Object.Width           =   2117
            EndProperty
         End
      End
      Begin VB.Frame Frame6 
         Caption         =   "Ingresos por Cta. Cte. Clientes"
         Height          =   2115
         Left            =   195
         TabIndex        =   17
         Top             =   4380
         Width           =   4395
         Begin VB.TextBox txtIngCtaCtes 
            Alignment       =   1  'Right Justify
            BackColor       =   &H8000000F&
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   2700
            Locked          =   -1  'True
            TabIndex        =   18
            Text            =   "0"
            Top             =   1725
            Width           =   1230
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel8 
            Height          =   225
            Left            =   930
            OleObjectBlob   =   "caj_arqueo2.frx":0E9A
            TabIndex        =   19
            Top             =   1755
            Width           =   1740
         End
         Begin MSComctlLib.ListView LvCtaCtes 
            Height          =   1365
            Left            =   195
            TabIndex        =   20
            Top             =   315
            Width           =   4035
            _ExtentX        =   7117
            _ExtentY        =   2408
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            FullRowSelect   =   -1  'True
            GridLines       =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            NumItems        =   3
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Object.Tag             =   "N109"
               Text            =   "Id"
               Object.Width           =   0
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Object.Tag             =   "T7000"
               Text            =   "Documento"
               Object.Width           =   4410
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   2
               Key             =   "total"
               Object.Tag             =   "N100"
               Text            =   "Venta"
               Object.Width           =   2117
            EndProperty
         End
      End
      Begin VB.Frame Frame5 
         Caption         =   "Ingresos por ventas"
         Height          =   2370
         Left            =   195
         TabIndex        =   13
         Top             =   1965
         Width           =   4395
         Begin VB.TextBox txtIngVentas 
            Alignment       =   1  'Right Justify
            BackColor       =   &H8000000F&
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   2685
            Locked          =   -1  'True
            TabIndex        =   14
            Text            =   "0"
            Top             =   1905
            Width           =   1230
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel7 
            Height          =   225
            Left            =   915
            OleObjectBlob   =   "caj_arqueo2.frx":0F02
            TabIndex        =   15
            Top             =   1935
            Width           =   1740
         End
         Begin MSComctlLib.ListView LvVentas 
            Height          =   1560
            Left            =   150
            TabIndex        =   16
            Top             =   360
            Width           =   4035
            _ExtentX        =   7117
            _ExtentY        =   2752
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            FullRowSelect   =   -1  'True
            GridLines       =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            NumItems        =   3
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Object.Tag             =   "N109"
               Text            =   "Id"
               Object.Width           =   0
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Object.Tag             =   "T7000"
               Text            =   "Documento"
               Object.Width           =   4410
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   2
               Key             =   "total"
               Object.Tag             =   "N100"
               Text            =   "Venta"
               Object.Width           =   2117
            EndProperty
         End
      End
      Begin VB.Frame Frame3 
         Caption         =   "Datos apertura caja"
         Height          =   1620
         Left            =   195
         TabIndex        =   4
         Top             =   315
         Width           =   4425
         Begin VB.TextBox txtUsuario 
            BackColor       =   &H8000000F&
            Height          =   285
            Left            =   1455
            Locked          =   -1  'True
            TabIndex        =   7
            Text            =   "4"
            Top             =   375
            Width           =   2760
         End
         Begin VB.TextBox txtSucursal 
            Height          =   285
            Left            =   1470
            Locked          =   -1  'True
            TabIndex        =   6
            Text            =   "Text1"
            Top             =   1215
            Width           =   2790
         End
         Begin VB.TextBox txtEfectivoInicial 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   1455
            Locked          =   -1  'True
            TabIndex        =   5
            Text            =   "0"
            Top             =   945
            Width           =   1530
         End
         Begin MSComCtl2.DTPicker DtFecha 
            Height          =   285
            Left            =   1455
            TabIndex        =   8
            Top             =   660
            Width           =   1245
            _ExtentX        =   2196
            _ExtentY        =   503
            _Version        =   393216
            Enabled         =   0   'False
            Format          =   273416193
            CurrentDate     =   41573
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
            Height          =   240
            Index           =   0
            Left            =   285
            OleObjectBlob   =   "caj_arqueo2.frx":0F6A
            TabIndex        =   9
            Top             =   945
            Width           =   1140
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
            Height          =   240
            Left            =   195
            OleObjectBlob   =   "caj_arqueo2.frx":0FE8
            TabIndex        =   10
            Top             =   375
            Width           =   1215
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
            Height          =   270
            Left            =   645
            OleObjectBlob   =   "caj_arqueo2.frx":1066
            TabIndex        =   11
            Top             =   645
            Width           =   765
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
            Height          =   240
            Left            =   405
            OleObjectBlob   =   "caj_arqueo2.frx":10CE
            TabIndex        =   12
            Top             =   1215
            Width           =   1020
         End
      End
   End
   Begin MSComDlg.CommonDialog Dialogo 
      Left            =   -60
      Top             =   1185
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.Timer Timer1 
      Interval        =   10
      Left            =   30
      Top             =   570
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   0
      OleObjectBlob   =   "caj_arqueo2.frx":113C
      Top             =   0
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel12 
      Height          =   210
      Left            =   -135
      OleObjectBlob   =   "caj_arqueo2.frx":1370
      TabIndex        =   67
      Top             =   4830
      Width           =   6675
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel13 
      Height          =   210
      Left            =   540
      OleObjectBlob   =   "caj_arqueo2.frx":146E
      TabIndex        =   68
      Top             =   4530
      Width           =   6675
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel10 
      Height          =   210
      Index           =   1
      Left            =   570
      OleObjectBlob   =   "caj_arqueo2.frx":156C
      TabIndex        =   69
      Top             =   4440
      Width           =   6675
   End
   Begin MSComctlLib.ListView LvEmpresa 
      Height          =   1785
      Left            =   20085
      TabIndex        =   85
      Top             =   2985
      Visible         =   0   'False
      Width           =   5535
      _ExtentX        =   9763
      _ExtentY        =   3149
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   0   'False
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   5
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Object.Tag             =   "T1000"
         Text            =   "RUT"
         Object.Width           =   1499
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   1
         Object.Tag             =   "T1000"
         Text            =   "nombre"
         Object.Width           =   1676
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Object.Tag             =   "T1000"
         Text            =   "direccion"
         Object.Width           =   1940
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Object.Tag             =   "T1000"
         Text            =   "giro"
         Object.Width           =   1940
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   4
         Key             =   "saldito"
         Object.Tag             =   "T1000"
         Text            =   "ciudad"
         Object.Width           =   2117
      EndProperty
   End
End
Attribute VB_Name = "caj_arqueo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim Lp_IdMiCaja As Long
Dim P_Fecha As String * 10
Dim P_Documento As String * 14
Dim P_Numero As String * 11
Dim P_Rut As String * 12
Dim P_Cliente As String * 22
Dim P_Venta As String * 12
Dim P_Neto As String * 12
Dim P_Iva As String * 12
Dim P_Pago As String * 10
Dim Im_Paginas As Integer
Dim Ip_Paginas As Integer
Dim Cy As Double
Dim Cx As Double
Dim Dp As Double
Dim CnV As Connection
Dim Sp_Condicion As String
Private Sub CajaHistorica()
    Dim Lp_Id_CH As Long
    
 '   Lp_Id_CH = UltimoNro("his_caja", "chi_id")
 '
 '   Sql = "INSERT INTO his_caja (chi_id,caj_id,chi_usuario,chi_fecha,chi_efectivo_inicial,chi_sucursal) " & _
 '           "VALUES(" & Lp_Id_CH & "," & Lp_IdMiCaja & ",'" & txtUsuario & "','" & Fql(DtFecha) & "'," & _
 '           CDbl(txtEfectivoInicial) & ",'" & txtSucursal & "')"
 '   cn.Execute Sql
    On Error GoTo ErrorHistorico
    cn.BeginTrans
    If LvVentas.ListItems.Count > 0 Then
        Sql = "INSERT INTO his_caja_ingresos (caj_id,hin_id_fpago,hin_nom_fpago,hin_valor) VALUES"
        For i = 1 To LvVentas.ListItems.Count
            Sql = Sql & " (" & Lp_IdMiCaja & "," & LvVentas.ListItems(i) & ",'" & LvVentas.ListItems(i).SubItems(1) & "'," & CDbl(LvVentas.ListItems(i).SubItems(2)) & "),"
        
        Next
        cn.Execute Mid(Sql, 1, Len(Sql) - 1)
    End If
    
    If LvCtaCtes.ListItems.Count > 0 Then
        Sql = "INSERT INTO his_caja_ingresos_ctacte (caj_id,hic_id_fpago,hic_nom_fpago,hic_valor) VALUES"
        For i = 1 To LvCtaCtes.ListItems.Count
            Sql = Sql & " (" & Lp_IdMiCaja & "," & LvCtaCtes.ListItems(i) & ",'" & LvCtaCtes.ListItems(i).SubItems(1) & "'," & CDbl(LvCtaCtes.ListItems(i).SubItems(2)) & "),"
        
        Next
        cn.Execute Mid(Sql, 1, Len(Sql) - 1)
    End If
    
    
    If LvDetalleDoc.ListItems.Count > 0 Then
        Sql = "INSERT INTO his_caja_detalle_ventas (caj_id,hde_fpago,hde_emision,hde_vencimiento,hde_documento,hde_nro,hde_cliente,hde_monto,hde_rut_cliente,hde_ncx,hde_neto,hde_iva," & _
            "hde_nombre_documento) VALUES"
        With LvDetalleDoc
            Neto = 0
            Iva = 0
            If Val(.ListItems(i).SubItems(9)) > 0 Then Neto = CDbl(.ListItems(i).SubItems(9))
            If Val(.ListItems(i).SubItems(10)) > 0 Then Iva = CDbl(.ListItems(i).SubItems(10))
            For i = 1 To LvDetalleDoc.ListItems.Count
                Sql = Sql & "(" & Lp_IdMiCaja & ",'" & .ListItems(i) & "','" & Fql(.ListItems(i).SubItems(1)) & "','" & Fql(.ListItems(i).SubItems(2)) & "','" & _
                .ListItems(i).SubItems(3) & "'," & .ListItems(i).SubItems(4) & ",'" & .ListItems(i).SubItems(5) & "'," & CDbl(.ListItems(i).SubItems(6)) & ",'" & .ListItems(i).SubItems(7) & "'," & Val(.ListItems(i).SubItems(8)) & "," & _
                Neto & "," & Iva & ",'" & .ListItems(i).SubItems(11) & "'),"
            Next
            cn.Execute Mid(Sql, 1, Len(Sql) - 1)
        End With
    End If
    
    
    If LvDetalleCtaCte.ListItems.Count > 0 Then
        Sql = "INSERT INTO his_caja_detalle_ctacte (caj_id,hcr_fpago,hcr_emision,hcr_vencimiento,hcr_documento,hcr_nro,hcr_cliente,hcr_monto) VALUES"
        With LvDetalleCtaCte
            For i = 1 To LvDetalleCtaCte.ListItems.Count
                Sql = Sql & "(" & Lp_IdMiCaja & ",'" & LvDetalleCtaCte.ListItems(i) & "','" & Fql(.ListItems(i).SubItems(1)) & "','" & Fql(.ListItems(i).SubItems(2)) & "','" & _
                .ListItems(i).SubItems(3) & "'," & .ListItems(i).SubItems(4) & ",'" & .ListItems(i).SubItems(5) & "'," & CDbl(.ListItems(i).SubItems(6)) & "),"
            Next
        
        End With
        cn.Execute Mid(Sql, 1, Len(Sql) - 1)
        
    
    
    End If
    cn.CommitTrans
    
    Exit Sub
ErrorHistorico:
    MsgBox "Problema.." & vbNewLine & Err.Description & vbNewLine & Err.Number, vbExclamation
    cn.RollbackTrans
    
End Sub

Private Sub CargaLVVentas()
    For i = 0 To CboFpago.ListCount - 1
        If CboFpago.List(i) = "CREDITO" Then
            CboFpago.Tag = ">1"
        Else
            Sql = "SELECT pla_dias " & _
                "FROM par_medios_de_pago " & _
                "WHERE mpa_id=" & CboFpago.ItemData(i)
            Consulta RsTmp, Sql
            If RsTmp.RecordCount > 0 Then
                CboFpago.Tag = "=" & RsTmp!pla_dias
            End If
        End If
        
                
        Sql = "SELECT " & CboFpago.ItemData(i) & " id,(SELECT m.mpa_nombre " & _
                    "FROM abo_tipos_de_pagos p " & _
                    "JOIN cta_abono_documentos l ON p.abo_id = l.abo_id " & _
                    "JOIN cta_abonos a ON l.abo_id = a.abo_id " & _
                    "/* JOIN abo_tipos_de_pagos t ON t.abo_id=a.abo_id */" & _
                    "JOIN par_medios_de_pago m ON p.mpa_id=m.mpa_id " & _
                    "WHERE a.abo_cli_pro = 'CLI' AND a.rut_emp = '76.178.895-7'  AND p.mpa_id=" & CboFpago.ItemData(i) & " AND l.id = v.id " & _
                    "LIMIT 1) mpago,"
                    
        Sql = Sql & "SUM( (SELECT p.pad_valor " & _
                    "FROM abo_tipos_de_pagos p " & _
                    "JOIN cta_abono_documentos l ON p.abo_id = l.abo_id " & _
                    "JOIN cta_abonos a ON l.abo_id = a.abo_id " & _
                    "/* JOIN abo_tipos_de_pagos t ON t.abo_id=a.abo_id */" & _
                    "JOIN par_medios_de_pago m ON p.mpa_id=m.mpa_id " & _
                    "WHERE a.abo_cli_pro = 'CLI' AND a.rut_emp = '76.178.895-7'  AND p.mpa_id=" & CboFpago.ItemData(i) & " AND l.id = v.id " & _
                    "LIMIT 1)) venta"
                    
                    
                    
       Sql = Sql & "/*UM(bruto) venta */" & _
                "FROM vi_venta_listado v " & _
                "WHERE rut_emp = '76.178.895-7' " & _
                "AND v.caj_id=" & Lp_IdMiCaja & " " & _
                "AND (SELECT p.mpa_id " & _
                    "FROM abo_tipos_de_pagos p " & _
                    "JOIN cta_abono_documentos l ON p.abo_id = l.abo_id " & _
                    "JOIN cta_abonos a ON l.abo_id = a.abo_id " & _
                    "WHERE a.abo_cli_pro = 'CLI' AND a.rut_emp = '76.178.895-7' AND p.mpa_id=" & CboFpago.ItemData(i) & " AND  l.id = v.id " & _
                    "LIMIT 1) = " & CboFpago.ItemData(i) & " " & _
                "AND doc_nota_de_venta = 'NO'"
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
            If Not IsNull(RsTmp!mpago) Then
                LvVentas.ListItems.Add , , RsTmp!Id
                LvVentas.ListItems(LvVentas.ListItems.Count).SubItems(1) = RsTmp!mpago
                LvVentas.ListItems(LvVentas.ListItems.Count).SubItems(2) = NumFormat(RsTmp!venta)
            End If
        End If
            
    
    Next
    
    Sql = "SELECT 0 id,'CREDITO' mpago,SUM(v.bruto) ventas " & _
                    "FROM ven_doc_venta v " & _
                    "JOIN sis_documentos s ON v.doc_id = s.doc_id " & _
                    "JOIN maestro_clientes r ON v.rut_cliente = r.rut_cliente " & _
                    "WHERE doc_nota_de_venta='NO' AND doc_permite_pago='SI' AND s.doc_nota_de_credito='NO' AND ven_plazo>1 /* (SELECT COUNT(d.abo_id) FROM cta_abonos   a " & _
                            "JOIN cta_abono_documentos d ON a.abo_id=d.abo_id " & _
                            "WHERE a.mpa_id<>8888 AND a.abo_cli_pro='CLI' AND d.id=v.id)=0 */ " & _
                    "AND v.caj_id =" & Lp_IdMiCaja & " " & _
                    "HAVING ventas>0"
        Sql = Sql & " UNION SELECT 8888 id,'NOTAS DE CREDITO' mpago, " & _
                "SUM(if(IF(id_ref>0,(SELECT bruto FROM ven_doc_venta k WHERE k.id=z.id_ref),0)>0,z.bruto*-1,0)) laventa " & _
                "FROM ven_doc_venta z " & _
                "JOIN sis_documentos t ON z.doc_id=t.doc_id " & _
                "WHERE doc_nota_de_credito='SI' AND /*doc_permite_pago='SI' AND*/ caj_id=" & Lp_IdMiCaja & " " & _
                "/*HAVING laventa<>0 */"
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        RsTmp.MoveFirst
        Do While Not RsTmp.EOF
            If Not IsNull(RsTmp!mpago) Then
                If Not IsNull(RsTmp.Fields(2)) Then
                    LvVentas.ListItems.Add , , RsTmp!Id
                    LvVentas.ListItems(LvVentas.ListItems.Count).SubItems(1) = RsTmp!mpago
                    LvVentas.ListItems(LvVentas.ListItems.Count).SubItems(2) = NumFormat(RsTmp.Fields(2))
                End If
            End If
            RsTmp.MoveNext
        Loop
            
    End If
    
        
End Sub
Private Sub CargaLVVentas2()
    For i = 0 To CboFpago.ListCount - 1
        If CboFpago.List(i) = "CREDITO" Then
            CboFpago.Tag = ">1"
        Else
            Sql = "SELECT pla_dias " & _
                "FROM par_medios_de_pago " & _
                "WHERE mpa_id=" & CboFpago.ItemData(i)
            Consulta RsTmp, Sql
            If RsTmp.RecordCount > 0 Then
                CboFpago.Tag = "=" & RsTmp!pla_dias
            End If
        End If
        
                
        Sql = "SELECT " & CboFpago.ItemData(i) & " id,(SELECT m.mpa_nombre " & _
                    "FROM abo_tipos_de_pagos p " & _
                    "JOIN cta_abono_documentos l ON p.abo_id = l.abo_id " & _
                    "JOIN cta_abonos a ON l.abo_id = a.abo_id " & _
                    "/* JOIN abo_tipos_de_pagos t ON t.abo_id=a.abo_id */" & _
                    "JOIN par_medios_de_pago m ON p.mpa_id=m.mpa_id " & _
                    "WHERE  abo_origen='VENTA' AND a.abo_cli_pro = 'CLI' AND a.rut_emp = '" & SP_Rut_Activo & "'  AND p.mpa_id=" & CboFpago.ItemData(i) & " AND l.id = v.id " & _
                    "LIMIT 1) mpago,"
                    
        Sql = Sql & "SUM( (SELECT p.pad_valor " & _
                    "FROM abo_tipos_de_pagos p " & _
                    "JOIN cta_abono_documentos l ON p.abo_id = l.abo_id " & _
                    "JOIN cta_abonos a ON l.abo_id = a.abo_id " & _
                    "/* JOIN abo_tipos_de_pagos t ON t.abo_id=a.abo_id */" & _
                    "JOIN par_medios_de_pago m ON p.mpa_id=m.mpa_id " & _
                    "WHERE abo_origen='VENTA' AND a.abo_cli_pro = 'CLI' AND a.rut_emp =  '" & SP_Rut_Activo & "'   AND p.mpa_id=" & CboFpago.ItemData(i) & " AND l.id = v.id " & _
                    "LIMIT 1)) venta"
                    
                    
                    
       Sql = Sql & "/*UM(bruto) venta */" & _
                "FROM vi_venta_listado v " & _
                "WHERE rut_emp =  '" & SP_Rut_Activo & "'  " & _
                "AND v.caj_id=" & Lp_IdMiCaja & " " & _
                "AND (SELECT p.mpa_id " & _
                    "FROM abo_tipos_de_pagos p " & _
                    "JOIN cta_abono_documentos l ON p.abo_id = l.abo_id " & _
                    "JOIN cta_abonos a ON l.abo_id = a.abo_id " & _
                    "WHERE  abo_origen='VENTA' AND a.abo_cli_pro = 'CLI' AND a.rut_emp =  '" & SP_Rut_Activo & "'  AND p.mpa_id=" & CboFpago.ItemData(i) & " AND  l.id = v.id " & _
                    "LIMIT 1) = " & CboFpago.ItemData(i) & " " & _
                "AND doc_nota_de_venta = 'NO'"
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
            If Not IsNull(RsTmp!mpago) Then
                LvVentas.ListItems.Add , , RsTmp!Id
                LvVentas.ListItems(LvVentas.ListItems.Count).SubItems(1) = RsTmp!mpago
                LvVentas.ListItems(LvVentas.ListItems.Count).SubItems(2) = NumFormat(RsTmp!venta)
            End If
        End If
            
    
    Next
    
    Sql = "SELECT 0 id,'CREDITO' mpago,SUM(v.bruto) ventas " & _
                    "FROM ven_doc_venta v " & _
                    "JOIN sis_documentos s ON v.doc_id = s.doc_id " & _
                    "JOIN maestro_clientes r ON v.rut_cliente = r.rut_cliente " & _
                    "WHERE doc_nota_de_venta='NO' AND doc_permite_pago='SI' AND s.doc_nota_de_credito='NO' AND ven_plazo>1 /* (SELECT COUNT(d.abo_id) FROM cta_abonos   a " & _
                            "JOIN cta_abono_documentos d ON a.abo_id=d.abo_id " & _
                            "WHERE a.mpa_id<>8888 AND a.abo_cli_pro='CLI' AND d.id=v.id)=0 */ " & _
                    "AND v.caj_id =" & Lp_IdMiCaja & " " & _
                    "HAVING ventas>0"
        Sql = Sql & " UNION SELECT 8888 id,'NOTAS DE CREDITO' mpago, " & _
                "SUM(if(IF(id_ref>0,(SELECT bruto FROM ven_doc_venta k WHERE k.id=z.id_ref),0)>0,z.bruto*-1,0)) laventa " & _
                "FROM ven_doc_venta z " & _
                "JOIN sis_documentos t ON z.doc_id=t.doc_id " & _
                "WHERE doc_nota_de_credito='SI' AND /*doc_permite_pago='SI' AND*/ caj_id=" & Lp_IdMiCaja & " " & _
                "/*HAVING laventa<>0 */"
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        RsTmp.MoveFirst
        Do While Not RsTmp.EOF
            If Not IsNull(RsTmp!mpago) Then
                If Not IsNull(RsTmp.Fields(2)) Then
                    LvVentas.ListItems.Add , , RsTmp!Id
                    LvVentas.ListItems(LvVentas.ListItems.Count).SubItems(1) = RsTmp!mpago
                    LvVentas.ListItems(LvVentas.ListItems.Count).SubItems(2) = NumFormat(RsTmp.Fields(2))
                End If
            End If
            RsTmp.MoveNext
        Loop
            
    End If
    
        
End Sub

Private Sub cmdCerrar_Click()
    
    If MsgBox("� Esta seguro de cerrar caja ...?", vbQuestion + vbOKCancel) = vbCancel Then Exit Sub
    On Error GoTo errorCierre
    cn.BeginTrans
  
                
        Sql = ""
        For i = 1 To LvBilletes.ListItems.Count
            Sql = Sql & "(" & Lp_IdMiCaja & "," & LvBilletes.ListItems(i) & "," & Val(LvBilletes.ListItems(i).SubItems(2)) & "),"
        Next
        If Len(Sql) > 0 Then
            sql2 = "INSERT INTO ven_caja_detalle_billete (caj_id,deb_id,cbi_cantidad) VALUES "
            cn.Execute sql2 & " " & Mid(Sql, 1, Len(Sql) - 1)
        End If
  
        cn.Execute "UPDATE ven_caja " & _
            " SET caj_fecha_cierre='" & Fql(Date) & " " & Time & "',caj_estado='CERRADA' " & _
            "WHERE caj_id=" & Lp_IdMiCaja

    cn.CommitTrans
    
    
    
    
    
 '   Public IG_Puerto_Impresora_Fiscal As Integer
    If SG_ImpresoraFiscalBixolon = "SI" Then
    'Lunes 9 Febrero 2015
            
            If SG_Marca_Impresora_Fiscal = "EPSON" Then
                vtaBoletaFiscalEpson.Exentos
                vtaBoletaFiscalEpson.EmiteInformeZ
                 
                 
                

                
                
            Else
    
    
                vtaBoletaFiscalSamsumg.EmitaInformeZ
            'Ahora procedemos a cerrar el turno
            'Debermos emitir una Z
             'Emitimos boleta
            End If
             
            
    End If
    

    
    'Si es vega modelo, actualiza tambien la bd de porteria con la caja cerrada
    If SP_Rut_Activo = "96.803.210-0" Then CerrarCajasPorteriaVegaModelo
    
    
    
    CajaHistorica
    
        
    MsgBox "Caja cerrada correctamente...", vbInformation
    On Error GoTo ImpresoraError

    Dialogo.CancelError = True
    Dialogo.ShowPrinter
    
    If SP_Rut_Activo = "78.967.170-2" Or SP_Rut_Activo = "2.891.054-1" Then
        ImprimeCierreGemppAldunate
    ElseIf SP_Rut_Activo = "76.005.337-6" Then
        ImprimeCierreSoloGempp 'gempp
        
    Else
        ImprimeCierre
    End If
    
    '27 Marzo 2016 _
    Guardaremos caja historica en his_caja ...
 
    
    
    End
    
    Exit Sub
errorCierre:
    MsgBox "Ocurrio un problema al intentar cerrar la caja ..." & vbNewLine & Err.Number & vbNewLine & Err.Description
    cn.RollbackTrans
    Exit Sub
ImpresoraError:
    End
    'NO quiso imprimir
End Sub


Private Sub CmdCierreCajero_Click()
    vtaBoletaFiscalEpson.CierreCajero
End Sub

Private Sub CmdCierreDiarioZ_Click()
    vtaBoletaFiscalEpson.EmiteInformeZ
End Sub

Private Sub CmdDesbloquear_Click()
    If MsgBox("Esta seguro de desbloquear caja?.." & vbNewLine & "PRECAUCION: SOLO USAR ESTA OPCION LUEGO DE HABER CERRADO LA CAJA ANTERIOR Y NO TENER MOVIMEINTOS EN LA CAJA ACTUAL", vbQuestion + vbYesNo) = vbYes Then
        If DG_ID_Unico > 0 Then
            Sql = "UPDATE ven_caja SET caj_estado='ABIERTA' " & _
                    "WHERE caj_id=" & DG_ID_Unico
            cn.Execute Sql
            
            Sql = "UPDATE his_caja_detalle_ctacte SET caj_id=0 " & _
                        "WHERE caj_id=" & DG_ID_Unico
            cn.Execute Sql
            
            Sql = "UPDATE his_caja_detalle_ventas SET caj_id=0 " & _
                        "WHERE caj_id=" & DG_ID_Unico
            cn.Execute Sql
            
            Sql = "UPDATE his_caja_ingresos SET caj_id=0 " & _
                        "WHERE caj_id=" & DG_ID_Unico
            cn.Execute Sql
            
            Sql = "UPDATE his_caja_ingresos_ctacte SET caj_id=0 " & _
                        "WHERE caj_id=" & DG_ID_Unico
            cn.Execute Sql
            
            
            ConexionBDPorteria
            
            Sql = "UPDATE ven_turno SET caj_id=0 " & _
                    "WHERE caj_id=" & DG_ID_Unico
            CnV.Execute Sql
             
            MsgBox "Caja desbloqueada correctamente." & vbNewLine & " El programa se cerrar�, para que vuelva a ingresar con esta caja.", vbInformation
            End
        End If
    End If
End Sub

Private Sub CmdExportar_Click()
 Dim tit(2) As String
    If LvDetalleDoc.ListItems.Count = 0 Then Exit Sub
    FraProgreso.Visible = True
    tit(0) = ".....DETALLE   POR   VENTAS....."
    tit(1) = " "
    tit(2) = " "
    ExportarNuevo LvDetalleDoc, tit, Me, BarraProgreso
    FraProgreso.Visible = False
End Sub

Private Sub CmdImprimir_Click()
    On Error GoTo ImpresoraError
    Dialogo.CancelError = True
    Dialogo.ShowPrinter
    If SP_Rut_Activo = "78.967.170-2" Then
        ImprimeCierrePlasticosAldunate
        'ImprimeCierreGemppAldunate 'aldunate
    ElseIf SP_Rut_Activo = "76.005.337-6" Then
        ImprimeCierreSoloGempp 'gempp
    Else
        ImprimeCierre
    End If
    Exit Sub
ImpresoraError:
              'no quiere imnprir
End Sub

Private Sub CmdInformeX_Click()
vtaBoletaFiscalEpson.EmiteInformeX
End Sub

Private Sub CmdLaserTinta_Click()
    
    Dialogo.CancelError = True
    On Error GoTo CancelaImpesion
    Dialogo.ShowPrinter
    
    
    Sql = "SELECT rut,nombre_empresa,direccion,ciudad,giro " & _
            "FROM sis_empresas " & _
            "WHERE rut='" & SP_Rut_Activo & "'"
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvEmpresa, True, True, False, False
    
    Printer.Orientation = vbPRORPortrait
    Printer.ScaleMode = vbCentimeters
    ImprimeCartola
    
    Exit Sub
CancelaImpesion:
    'NO QUISO IMPRIMIR
End Sub

Private Sub CmdNoIncluir_Click()
    

    For p = 1 To LvDetalleDoc.ListItems.Count
        'If p > LvDetalleDoc.ListItems.Count Then Exit For
        If LvDetalleDoc.ListItems(p).Checked Then
        
            For i = 1 To LvVentas.ListItems.Count
                If LvVentas.ListItems(i) = 1 Then
                    LvVentas.ListItems(i).SubItems(2) = NumFormat(CDbl(LvVentas.ListItems(i).SubItems(2)) - CDbl(LvDetalleDoc.ListItems(p).SubItems(6)))
                    txtIngVentas = NumFormat(TotalizaColumna(LvVentas, "total"))
                    Exit For
                End If
            Next
            
            For i = 1 To LvDetalle.ListItems.Count
                If LvDetalle.ListItems(i) = 1 Then
                    LvDetalle.ListItems(i).SubItems(2) = NumFormat(CDbl(LvDetalle.ListItems(i).SubItems(2)) - CDbl(LvDetalleDoc.ListItems(p).SubItems(6)))
                    txtTotal = NumFormat(TotalizaColumna(LvDetalle, "total"))
                    Exit For
                End If
            Next
        
        
            LvDetalleDoc.ListItems.Remove LvDetalleDoc.ListItems(p).Index
            Exit For
        End If
        
    Next

    TotalizaDoc
End Sub

Private Sub CmdOkDetBillete_Click()
    
    If DG_ID_Unico > 0 Then Exit Sub
    cn.Execute "DELETE FROM ven_caja_detalle_billete " & _
                "WHERE caj_id=" & Lp_IdMiCaja
    Sql = "INSERT INTO ven_caja_detalle_billete (caj_id,deb_id,cbi_cantidad) VALUES "
    sql2 = ""
    For i = 1 To LvBilletes.ListItems.Count
        If Val(TxtIdBillete) = Val(LvBilletes.ListItems(i)) Then
           
            LvBilletes.ListItems(i).SubItems(2) = Val(txtCantBillete)
            LvBilletes.ListItems(i).SubItems(3) = txtTotalBillete
                
            
            Exit For
        End If
    Next
    For i = 1 To LvBilletes.ListItems.Count
         sql2 = sql2 & "(" & Lp_IdMiCaja & "," & LvBilletes.ListItems(i) & "," & Val(LvBilletes.ListItems(i).SubItems(2)) & "),"
    Next
    
    
    TxtTBilletes = NumFormat(TotalizaColumna(LvBilletes, "totalbillete"))
    If Len(sql2) > 0 Then
        cn.Execute Sql & Mid(sql2, 1, Len(sql2) - 1)
    End If
    TxtIdBillete = ""
    TxTValorBillete = ""
    txtCantBillete = ""
    txtTotalBillete = ""
    LvBilletes.SetFocus
    LLenaArqueo
    
End Sub

Private Sub CmdOkGasto_Click()
    If DG_ID_Unico > 0 Then Exit Sub
    If Len(TxtGasto) = 0 Or Val(Me.txtValorGasto) = 0 Then
        MsgBox "Complete los datos...", vbInformation
        TxtGasto.SetFocus
        Exit Sub
    End If
    
    Sql = "INSERT INTO ven_gastos_por_caja (caj_id,vgc_descripcion,vgc_valor) " & _
                "VALUES(" & Lp_IdMiCaja & ",'" & TxtGasto & "'," & CDbl(txtValorGasto) & ")"
    cn.Execute Sql
    TxtId = ""
    TxtGasto = ""
    txtValorGasto = ""
    CargaGastos
    
End Sub
Private Sub CargaGastos()
    Sql = "SELECT vgc_id, vgc_descripcion,vgc_valor,'SI' " & _
            "FROM  ven_gastos_por_caja " & _
            "WHERE caj_id=" & Lp_IdMiCaja & " UNION " & _
              "SELECT no_documento, nombre_empresa,total,'NO' " & _
                "FROM com_doc_compra c " & _
                "JOIN maestro_proveedores p ON c.rut=p.rut_proveedor " & _
                "WHERE caj_id=" & Lp_IdMiCaja
       
            
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, Me.LvGastos, False, True, True, False
    txtTotalGasto = NumFormat(TotalizaColumna(LvGastos, "total"))
    LLenaArqueo
End Sub
Private Sub CargaIngresos()
    Sql = "SELECT vic_id, vic_descripcion,vic_valor,'SI' " & _
            "FROM  ven_ingresos_por_caja " & _
            "WHERE caj_id=" & Lp_IdMiCaja & " "
       
            
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, Me.LvIngresos, False, True, True, False
    TxtTotalIngresos = NumFormat(TotalizaColumna(LvIngresos, "total"))
    LLenaArqueo
End Sub
Private Sub CmdOkIngreso_Click()
    If DG_ID_Unico > 0 Then Exit Sub
    If Len(TxtIngreso) = 0 Or Val(Me.txtValorIngreso) = 0 Then
        MsgBox "Complete los datos...", vbInformation
        TxtIngreso.SetFocus
        Exit Sub
    End If
    
    Sql = "INSERT INTO ven_ingresos_por_caja (caj_id,vic_descripcion,vic_valor) " & _
                "VALUES(" & Lp_IdMiCaja & ",'" & TxtIngreso & "'," & CDbl(txtValorIngreso) & ")"
    cn.Execute Sql
    TxtIdI = ""
    TxtIngreso = ""
    txtValorIngreso = ""
    CargaIngresos
End Sub

Private Sub cmdSalir_Click()
        Unload Me
End Sub





Private Sub Form_Load()
    Aplicar_skin Me
    Centrar Me
    
    '9-10-15
    'Intentando otra forma de resumir la caja, tomando como base la query del listado de ventas.
    LLenarCombo CboFpago, "mpa_nombre", "mpa_id", "par_medios_de_pago", "mpa_activo='SI'"
    CboFpago.AddItem "CREDITO"
    CboFpago.ItemData(CboFpago.ListCount - 1) = 100
    'recorreremos este combo para ir llenando la grilla lvventas _
    en forma manual y no con llena_grilla
    If (SP_Rut_Activo = "11.500.319-4" Or SP_Rut_Activo = "76.370.578-1" Or SP_Rut_Activo = "76.399.144-K" Or SP_Rut_Activo = "76.158.157-0") And SG_Marca_Impresora_Fiscal = "EPSON" Then Frame13.Visible = True
    
    
    If DG_ID_Unico > 0 Then
        Sql = "SELECT caj_id,sue_id,caj_fecha_apertura,caj_estado,caj_monto_inicial,usu_nombre,caj_deposito,caj_saldo " & _
                "FROM ven_caja c " & _
                "JOIN sis_usuarios u ON c.usu_id=u.usu_id " & _
                "WHERE caj_id=" & DG_ID_Unico
        Me.Caption = Me.Caption & " /  No Modificable"
        Me.cmdCerrar.Enabled = False
        If SP_Rut_Activo = "96.803.210-0" Then
            Me.CmdDesbloquear.Visible = True
        End If
        
        
    Else
        Sql = "SELECT caj_id,sue_id,caj_fecha_apertura,caj_estado,caj_monto_inicial,usu_nombre,caj_deposito,caj_saldo " & _
                "FROM ven_caja c " & _
                "JOIN sis_usuarios u ON c.usu_id=u.usu_id " & _
                "WHERE emp_id=" & IG_id_Empresa & " AND sue_id=" & IG_id_Sucursal_Empresa & " AND caj_estado='ABIERTA'"
    End If
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        txtUsuario = RsTmp!usu_nombre
        DtFecha = RsTmp!caj_fecha_apertura
        txtEfectivoInicial = NumFormat(RsTmp!caj_monto_inicial)
        txtSucursal = Principal.txtSucursal
        Lp_IdMiCaja = RsTmp!caj_id
        TxtMontoDeposito = NumFormat(RsTmp!caj_deposito)
        TxtMontoInicial = NumFormat(RsTmp!caj_saldo)
        CargaGastos
        CargaIngresos
        Sql = "SELECT deb_id,deb_valor " & _
                "FROM par_billetes_caja " & _
                "WHERE deb_activo='SI'"
        Consulta RsTmp, Sql
        LLenar_Grilla RsTmp, Me, Me.LvBilletes, False, True, True, False
        
        Sql = "SELECT deb_id,cbi_cantidad,0,0 " & _
                "FROM ven_caja_detalle_billete " & _
                "WHERE caj_id=" & Lp_IdMiCaja
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
            RsTmp.MoveFirst
            Do While Not RsTmp.EOF
                For i = 1 To LvBilletes.ListItems.Count
                    If RsTmp!deb_id = Val(LvBilletes.ListItems(i)) Then
                        LvBilletes.ListItems(i).SubItems(2) = RsTmp!cbi_cantidad
                        LvBilletes.ListItems(i).SubItems(3) = NumFormat(RsTmp!cbi_cantidad * CDbl(LvBilletes.ListItems(i).SubItems(1)))
                        Exit For
                    End If
                Next
            
                RsTmp.MoveNext
            Loop
        End If
    End If
    TxtTBilletes = NumFormat(TotalizaColumna(LvBilletes, "totalbillete"))
   ' If DG_ID_Unico > 0 Then
        VerResumen
   ' Else
   '     CargaResumen
   ' End If
    
End Sub

'Private Sub CargaResumen()
'    'Estas fueron las pagadas desde ventas
'    If SP_Rut_Activo <> "gm" Then
'        Sql = "SELECT t.mpa_id,m.mpa_nombre,SUM(t.pad_valor) ventas " & _
'                "FROM ven_caja c " & _
'                "JOIN cta_abonos a USING(caj_id) " & _
'                "JOIN cta_abono_documentos d ON a.abo_id=d.abo_id " & _
'                "JOIN abo_tipos_de_pagos t ON a.abo_id=t.abo_id " & _
'                "JOIN par_medios_de_pago m ON t.mpa_id=m.mpa_id " & _
'                "WHERE a.abo_origen='VENTA' AND a.abo_cli_pro='CLI' AND caj_estado='ABIERTA' AND c.caj_id=" & LG_id_Caja & _
'                " AND a.rut_emp='" & SP_Rut_Activo & "' AND emp_id=" & IG_id_Empresa & " AND sue_id=" & IG_id_Sucursal_Empresa & " " & _
'                "GROUP BY t.mpa_id " & _
'                "/*ORDER BY t.mpa_id */" & _
'                "UNION SELECT 0,'CREDITO',SUM(v.bruto) ventas " & _
'                        "FROM ven_doc_venta v " & _
'                        "JOIN sis_documentos s ON v.doc_id = s.doc_id " & _
'                        "JOIN maestro_clientes r ON v.rut_cliente = r.rut_cliente " & _
'                        "WHERE (SELECT COUNT(d.abo_id) FROM cta_abonos   a " & _
'                                "JOIN cta_abono_documentos d ON a.abo_id=d.abo_id " & _
'                                "WHERE a.abo_cli_pro='CLI' AND d.id=v.id)=0 " & _
'                        "AND v.rut_emp='" & SP_Rut_Activo & "' AND v.caj_id =" & LG_id_Caja & " " & _
'                        "HAVING ventas>0"
'        Consulta RsTmp, Sql
'        LLenar_Grilla RsTmp, Me, LvVentas, False, True, True, False
'    Else
'        'solo para totalgomas 9-10.2015
'        CargaLVVentas
'
'    End If
'    txtIngVentas = NumFormat(TotalizaColumna(LvVentas, "total"))
'
'
'    'Estas fueron las pagadas desde CTAS CTES
'    Sql = "SELECT t.mpa_id,m.mpa_nombre,SUM(d.ctd_monto) ventas " & _
'            "FROM ven_caja c " & _
'            "JOIN cta_abonos a USING(caj_id) " & _
'            "JOIN cta_abono_documentos d ON a.abo_id=d.abo_id " & _
'            "JOIN abo_tipos_de_pagos t ON a.abo_id=t.abo_id " & _
'            "JOIN par_medios_de_pago m ON t.mpa_id=m.mpa_id " & _
'            "WHERE a.abo_origen='CTACTE' AND a.abo_cli_pro='CLI' AND caj_estado='ABIERTA' AND c.caj_id=" & LG_id_Caja & _
'            " AND emp_id=" & IG_id_Empresa & " AND sue_id=" & IG_id_Sucursal_Empresa & " " & _
'            "GROUP BY t.mpa_id "
'
'    Consulta RsTmp, Sql
'    LLenar_Grilla RsTmp, Me, LvCtaCtes, False, True, True, False
'    txtIngCtaCtes = NumFormat(TotalizaColumna(LvCtaCtes, "total"))
'
'
'    'Estas fueron las resumen de la caja
'    Sql = "SELECT t.mpa_id,m.mpa_nombre,SUM(d.ctd_monto) ventas " & _
'            "FROM ven_caja c " & _
'            "JOIN cta_abonos a USING(caj_id) " & _
'            "JOIN cta_abono_documentos d ON a.abo_id=d.abo_id " & _
'            "JOIN abo_tipos_de_pagos t ON a.abo_id=t.abo_id " & _
'            "JOIN par_medios_de_pago m ON t.mpa_id=m.mpa_id " & _
'            "WHERE  (a.abo_origen='VENTA' or a.abo_origen='CTACTE') AND a.abo_cli_pro='CLI' AND caj_estado='ABIERTA' AND c.caj_id=" & LG_id_Caja & _
'            " AND emp_id=" & IG_id_Empresa & " AND sue_id=" & IG_id_Sucursal_Empresa & " " & _
'            "GROUP BY t.mpa_id " & _
'            "/*ORDER BY t.mpa_id */" & _
'            "UNION SELECT 0,'CREDITO',SUM(v.bruto) ventas " & _
'                    "FROM ven_doc_venta v " & _
'                    "JOIN sis_documentos s ON v.doc_id = s.doc_id " & _
'                    "JOIN maestro_clientes r ON v.rut_cliente = r.rut_cliente " & _
'                    "WHERE (SELECT COUNT(d.abo_id) FROM cta_abonos   a " & _
'                            "JOIN cta_abono_documentos d ON a.abo_id=d.abo_id " & _
'                            "WHERE a.abo_cli_pro='CLI' AND d.id=v.id)=0 " & _
'                    "AND v.caj_id =" & LG_id_Caja & " " & _
'                    "HAVING ventas>0"
'    Consulta RsTmp, Sql
'    LLenar_Grilla RsTmp, Me, LvDetalle, False, True, True, False
'    txtTotal = NumFormat(TotalizaColumna(LvDetalle, "total"))
'
'
'
'
'
'
'
'
'
'    'detalle de documentos
'    'resumen de las ventas de la caja
'
'    Sql = "SELECT IF(v.rut_cliente='NULO','DOC. NULO',p.pla_nombre),fecha,ven_fecha_vencimiento, doc_abreviado,  no_documento,IF(v.rut_cliente='11.111.111-1','',c.nombre_rsocial),bruto,v.rut_cliente " & _
'            "FROM ven_doc_venta v " & _
'            "JOIN sis_documentos d USING(doc_id) " & _
'            "LEFT JOIN maestro_clientes c ON v.rut_cliente=c.rut_cliente " & _
'            "LEFT JOIN par_plazos_vencimiento p ON v.ven_plazo_id=p.pla_id " & _
'            "WHERE v.rut_emp='" & SP_Rut_Activo & "' AND caj_id=" & LG_id_Caja & " " & _
'            "ORDER BY doc_abreviado,v.no_documento "
'    Consulta RsTmp, Sql
'    LLenar_Grilla RsTmp, Me, LvDetalleDoc, False, True, True, False
'    txtTotalDoc = NumFormat(TotalizaColumna(LvDetalleDoc, "total"))
'
'       'detalle de documentos
'    'resumen de los pagos de documentos por CTA CTE clientes
'    Sql = "SELECT m.mpa_nombre,v.fecha emision,v.ven_fecha_vencimiento,s.doc_abreviado,no_documento,r.nombre_rsocial,d.ctd_monto " & _
'                "FROM ven_caja c " & _
'                "JOIN cta_abonos a USING(caj_id) " & _
'                "JOIN cta_abono_documentos d ON a.abo_id=d.abo_id " & _
'                "JOIN abo_tipos_de_pagos t ON a.abo_id=t.abo_id " & _
'                "JOIN par_medios_de_pago m ON t.mpa_id=m.mpa_id  " & _
'                "JOIN ven_doc_venta v ON d.id=v.id " & _
'                "JOIN sis_documentos s ON v.doc_id=s.doc_id " & _
'                "JOIN maestro_clientes r ON v.rut_cliente=r.rut_cliente " & _
'                "WHERE a.abo_origen='CTACTE' AND a.abo_cli_pro='CLI' AND caj_estado='ABIERTA' AND c.caj_id=" & LG_id_Caja & _
'                " AND emp_id=" & IG_id_Empresa & " AND c.sue_id=" & IG_id_Sucursal_Empresa & " " & _
'                 "/*ORDER BY t.mpa_id,v.doc_id,v.no_documento*/ "
'
'    Consulta RsTmp, Sql
'    LLenar_Grilla RsTmp, Me, LvDetalleCtaCte, False, True, True, False
'    Me.txtTotalCtaCte = NumFormat(TotalizaColumna(LvDetalleCtaCte, "total"))
'
'
'    '14 Diciembre 2013
'    'Cargamos tambien el detalle de los cheques
'    Sql = "SELECT che_id,che_numero,ban_nombre,che_monto " & _
'            "FROM abo_cheques c " & _
'            "JOIN par_bancos b ON c.ban_id=b.ban_id " & _
'            "WHERE abo_id IN " & _
'            "(SELECT d.abo_id " & _
'                "FROM    cta_abono_documentos d " & _
'                "JOIN ven_doc_venta m ON d.id=m.id " & _
'                "JOIN cta_abonos a ON d.abo_id=a.abo_id AND a.abo_cli_pro='CLI' " & _
'                "JOIN abo_tipos_de_pagos t ON d.abo_id=t.abo_id " & _
'                "WHERE m.caj_id=" & LG_id_Caja & " AND m.rut_emp='" & SP_Rut_Activo & "' AND t.mpa_id=2)"
'    Consulta RsTmp, Sql
'    LLenar_Grilla RsTmp, Me, LvCheques, False, True, True, False
'    TxtTotalCheques = NumFormat(TotalizaColumna(LvCheques, "total"))
'    '*********************************************
'
'
'
'
'
'    '14 dic 2013
'    'El esperado resumen
'    LLenaArqueo
'
'
'
'
'    '****************************************************************
'End Sub
Private Sub VerResumen()

    'Estas fueron las pagadas desde ventas
    
    If DG_ID_Unico > 0 Then GoTo cargaHistoricos
    
    
    If SP_Rut_Activo = "76.178.895-7" Or SP_Rut_Activo = "76.005.337-6" Or SP_Rut_Activo = "78.967.170-2" Then
        'TotalGomas 'GEMPP
        CargaLVVentas2
    ElseIf SP_Rut_Activo = "76.553.302-3" Then
        'kyr
        CargaLVVentas2
    Else
        If SP_Rut_Activo <> "96.803.210-0" Then
                Sql = "SELECT t.mpa_id,m.mpa_nombre,SUM(t.pad_valor) ventas " & _
                    "FROM ven_caja c " & _
                    "JOIN cta_abonos a USING(caj_id) " & _
                    "JOIN cta_abono_documentos d ON a.abo_id=d.abo_id " & _
                    "JOIN abo_tipos_de_pagos t ON a.abo_id=t.abo_id " & _
                    "JOIN par_medios_de_pago m ON t.mpa_id=m.mpa_id " & _
                    "JOIN ven_doc_venta x ON d.id=x.id " & _
                    "WHERE t.caj_id>0 AND x.caj_id=" & Lp_IdMiCaja & " AND (SELECT doc_nota_de_venta " & _
                            "FROM ven_doc_venta e " & _
                            "JOIN sis_documentos s ON e.doc_id=s.doc_id " & _
                            "WHERE d.id=e.id)='NO' AND " & _
                    " a.abo_origen='VENTA' AND a.abo_cli_pro='CLI' AND c.caj_id=" & Lp_IdMiCaja & _
                    " GROUP BY t.mpa_id " & _
                    "/*ORDER BY t.mpa_id */" & _
                    "UNION SELECT 0,'CREDITO',SUM(v.bruto) ventas " & _
                            "FROM ven_doc_venta v " & _
                            "JOIN sis_documentos s ON v.doc_id = s.doc_id " & _
                            "JOIN maestro_clientes r ON v.rut_cliente = r.rut_cliente " & _
                            "WHERE doc_nota_de_venta='NO' AND doc_permite_pago='SI' AND s.doc_nota_de_credito='NO' AND ven_plazo>1 /* (SELECT COUNT(d.abo_id) FROM cta_abonos   a " & _
                                    "JOIN cta_abono_documentos d ON a.abo_id=d.abo_id " & _
                                    "WHERE a.mpa_id<>8888 AND a.abo_cli_pro='CLI' AND d.id=v.id)=0 */ " & _
                            "AND v.caj_id =" & Lp_IdMiCaja & " " & _
                            "HAVING ventas>0"
                Sql = Sql & " UNION SELECT 0,'NOTAS DE CREDITO', " & _
                        "SUM(if(IF(id_ref>0,(SELECT bruto FROM ven_doc_venta k WHERE k.id=z.id_ref AND k.caj_id=" & Lp_IdMiCaja & "),0)>0,z.bruto*-1,0)) laventa " & _
                        "FROM ven_doc_venta z " & _
                        "JOIN sis_documentos t ON z.doc_id=t.doc_id " & _
                        "WHERE doc_nota_de_venta='NO' AND doc_permite_pago='SI' AND caj_id=" & Lp_IdMiCaja & " " & _
                        "HAVING laventa<>0"
                            
                Consulta RsTmp, Sql
                LLenar_Grilla RsTmp, Me, LvVentas, False, True, True, False
        Else
            'en el caso de ser vega modelo, si cargar los doc de venta de las boeltas ingresadas
                Sql = "SELECT t.mpa_id,m.mpa_nombre,SUM(t.pad_valor) ventas " & _
                    "FROM ven_caja c " & _
                    "JOIN cta_abonos a USING(caj_id) " & _
                    "JOIN cta_abono_documentos d ON a.abo_id=d.abo_id " & _
                    "JOIN abo_tipos_de_pagos t ON a.abo_id=t.abo_id " & _
                    "JOIN par_medios_de_pago m ON t.mpa_id=m.mpa_id " & _
                    "JOIN ven_doc_venta x ON d.id=x.id " & _
                    "WHERE t.caj_id>0 AND x.caj_id=" & Lp_IdMiCaja & " AND (SELECT doc_nota_de_venta " & _
                            "FROM ven_doc_venta e " & _
                            "JOIN sis_documentos s ON e.doc_id=s.doc_id " & _
                            "WHERE d.id=e.id)='NO' AND " & _
                    " a.abo_origen='VENTA' AND a.abo_cli_pro='CLI' AND c.caj_id=" & Lp_IdMiCaja & _
                    " GROUP BY t.mpa_id " & _
                    "/*ORDER BY t.mpa_id */"

               
                            
                Consulta RsTmp, Sql
                LLenar_Grilla RsTmp, Me, LvVentas, False, True, True, False
            
        
        End If
    End If
    txtIngVentas = NumFormat(TotalizaColumna(LvVentas, "total"))


    'Estas fueron las pagadas desde CTAS CTES
    Sql = "SELECT t.mpa_id,m.mpa_nombre,SUM(t.pad_valor) ventas " & _
            "FROM ven_caja c " & _
            "JOIN cta_abonos a USING(caj_id) " & _
            "/*JOIN cta_abono_documentos d ON a.abo_id=d.abo_id */" & _
            "JOIN abo_tipos_de_pagos t ON a.abo_id=t.abo_id " & _
            "JOIN par_medios_de_pago m ON t.mpa_id=m.mpa_id " & _
            "WHERE a.abo_origen='CTACTE' AND a.abo_cli_pro='CLI'  AND c.caj_id=" & Lp_IdMiCaja & _
            " GROUP BY  t.mpa_id"

    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvCtaCtes, False, True, True, False
    txtIngCtaCtes = NumFormat(TotalizaColumna(LvCtaCtes, "total"))


    'Estas fueron las resumen de la caja
    '30 de mayo, eliminamos esta query y reemplazamos con la suma de los ingresos por ventas y cta ctes
    If 1 = 2 Then
            Sql = "SELECT t.mpa_id,m.mpa_nombre,SUM(t.pad_valor) ventas " & _
                    "FROM ven_caja c " & _
                    "JOIN cta_abonos a USING(caj_id) " & _
                    "JOIN cta_abono_documentos d ON a.abo_id=d.abo_id " & _
                    "JOIN abo_tipos_de_pagos t ON a.abo_id=t.abo_id AND c.caj_id=t.caj_id " & _
                    "JOIN par_medios_de_pago m ON t.mpa_id=m.mpa_id " & _
                    "JOIN ven_doc_venta x ON d.id=x.id " & _
                    "WHERE  (SELECT doc_nota_de_venta " & _
                            "FROM ven_doc_venta e " & _
                            "JOIN sis_documentos s ON e.doc_id=s.doc_id " & _
                            "WHERE e.id=(SELECT id FROM cta_abono_documentos m WHERE a.abo_id=m.abo_id LIMIT 1) )='NO' AND " & _
                    "(a.abo_origen='VENTA' or a.abo_origen='CTACTE') AND a.abo_cli_pro='CLI' AND c.caj_id=" & Lp_IdMiCaja & _
                    " GROUP BY t.mpa_id " & _
                    "/*ORDER BY t.mpa_id */" & _
                    "UNION SELECT 0,'CREDITO',SUM(v.bruto) ventas " & _
                            "FROM ven_doc_venta v " & _
                            "JOIN sis_documentos s ON v.doc_id = s.doc_id " & _
                            "JOIN maestro_clientes r ON v.rut_cliente = r.rut_cliente " & _
                            "WHERE doc_nota_de_venta='NO' AND doc_permite_pago='SI' AND s.doc_nota_de_credito='NO' AND  (SELECT COUNT(d.abo_id) FROM cta_abonos   a " & _
                                    "JOIN cta_abono_documentos d ON a.abo_id=d.abo_id " & _
                                    "WHERE a.mpa_id<>8888 AND a.abo_cli_pro='CLI' AND d.id=v.id)=0 " & _
                            "AND v.caj_id =" & Lp_IdMiCaja & " " & _
                            "HAVING ventas>0"
            Sql = Sql & " UNION SELECT 0,'NOTAS DE CREDITO', " & _
                        "SUM(if(IF(id_ref>0,(SELECT bruto FROM ven_doc_venta k WHERE k.id=z.id_ref AND k.caj_id=" & Lp_IdMiCaja & "),0)>0,z.bruto*-1,0)) LAVENTA " & _
                        "FROM ven_doc_venta z " & _
                        "JOIN sis_documentos t ON z.doc_id=t.doc_id " & _
                        "WHERE doc_nota_de_venta='NO' AND doc_permite_pago='SI' AND caj_id=" & Lp_IdMiCaja & " " & _
                        "HAVING LAVENTA<>0"
    
            Consulta RsTmp, Sql
    
    
            LLenar_Grilla RsTmp, Me, LvDetalle, False, True, True, False
    End If
    
    
    
    
    'resumen de caja
    'si el efectivo inicial es > 0 then
    'colocarlo 1ro
    If CDbl(txtEfectivoInicial) > 0 Then
        LvDetalle.ListItems.Add , , 1
        LvDetalle.ListItems(LvDetalle.ListItems.Count).SubItems(1) = "EFECTIVO"
        LvDetalle.ListItems(LvDetalle.ListItems.Count).SubItems(2) = txtEfectivoInicial
    End If
    
    For i = 1 To LvVentas.ListItems.Count
        Bp_Agregado = False
        For X = 1 To LvDetalle.ListItems.Count
            If LvVentas.ListItems(i) = LvDetalle.ListItems(X) Then
                LvDetalle.ListItems(X).SubItems(2) = NumFormat(CDbl(LvDetalle.ListItems(X).SubItems(2)) + CDbl(LvVentas.ListItems(i).SubItems(2)))
                Bp_Agregado = True
                Exit For
            End If
        Next
    
        If Not Bp_Agregado Then
            LvDetalle.ListItems.Add , , LvVentas.ListItems(i)
            LvDetalle.ListItems(LvDetalle.ListItems.Count).SubItems(1) = LvVentas.ListItems(i).SubItems(1)
            LvDetalle.ListItems(LvDetalle.ListItems.Count).SubItems(2) = LvVentas.ListItems(i).SubItems(2)
        End If
    Next
    
    For i = 1 To Me.LvCtaCtes.ListItems.Count
        Bp_Agregado = False
        For X = 1 To LvDetalle.ListItems.Count
            If LvCtaCtes.ListItems(i) = LvDetalle.ListItems(X) Then
                LvDetalle.ListItems(X).SubItems(2) = NumFormat(CDbl(LvDetalle.ListItems(X).SubItems(2)) + CDbl(LvCtaCtes.ListItems(i).SubItems(2)))
                Bp_Agregado = True
                Exit For
            End If
        Next
        If Not Bp_Agregado Then
            LvDetalle.ListItems.Add , , LvCtaCtes.ListItems(i)
            LvDetalle.ListItems(LvDetalle.ListItems.Count).SubItems(1) = LvCtaCtes.ListItems(i).SubItems(1)
            LvDetalle.ListItems(LvDetalle.ListItems.Count).SubItems(2) = LvCtaCtes.ListItems(i).SubItems(2)
        
        End If
    Next
    
    txtTotal = NumFormat(TotalizaColumna(LvDetalle, "total"))
    
    
    
    
    
           'detalle de documentos
    'resumen de los pagos de documentos por CTA CTE clientes
    Sql = "SELECT m.mpa_nombre,v.fecha emision,v.ven_fecha_vencimiento,s.doc_abreviado,no_documento,r.nombre_rsocial,t.pad_valor " & _
                "FROM ven_caja c " & _
                "JOIN cta_abonos a USING(caj_id) " & _
                "JOIN cta_abono_documentos d ON a.abo_id=d.abo_id " & _
                "JOIN abo_tipos_de_pagos t ON a.abo_id=t.abo_id " & _
                "JOIN par_medios_de_pago m ON t.mpa_id=m.mpa_id  " & _
                "JOIN ven_doc_venta v ON d.id=v.id " & _
                "JOIN sis_documentos s ON v.doc_id=s.doc_id " & _
                "JOIN maestro_clientes r ON v.rut_cliente=r.rut_cliente " & _
                "WHERE doc_nota_de_venta='NO' AND a.abo_origen='CTACTE' AND a.abo_cli_pro='CLI' AND c.caj_id=" & Lp_IdMiCaja & _
                 " /*ORDER BY t.mpa_id,v.doc_id,v.no_documento*/ " & _
                 "GROUP BY t.mpa_id,t.pad_id "
                 

    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvDetalleCtaCte, False, True, True, False
    Me.txtTotalCtaCte = NumFormat(TotalizaColumna(LvDetalleCtaCte, "total"))
    
    
    
    If SP_Rut_Activo <> "96.803.210-0" Then
            'cuando no es vega modelo
            'vega modelo no refleja ventas del dia  solo creditos
            '24 Marzo 2016
            'detalle de documentos
            'resumen de las ventas de la caja
            
            'Aqui haremos una UNION, ya que las NC quedaran como no permite pago.
            '25 - 9 - 2015
            'TEMA: NOTA DE CREDITO EN CAJA
            
            Sql = "SELECT  IFNULL((SELECT m.mpa_nombre " & _
                                     "FROM abo_tipos_de_pagos p " & _
                                    "JOIN cta_abono_documentos l ON p.abo_id = l.abo_id " & _
                                    "JOIN cta_abonos a ON l.abo_id = a.abo_id " & _
                                    "JOIN par_medios_de_pago m ON p.mpa_id=m.mpa_id " & _
                                    "WHERE a.abo_cli_pro = 'CLI' AND a.rut_emp = '" & SP_Rut_Activo & "' " & _
                                    "AND l.id = v.id AND abo_origen='VENTA' " & _
                                    "LIMIT 1),'CREDITO')" & _
                                    ",fecha,ven_fecha_vencimiento, doc_abreviado,  no_documento,IF(v.rut_cliente='11.111.111-1','',c.nombre_rsocial),bruto*IF(d.doc_nota_de_credito='SI',-1,1),v.rut_cliente, " & _
                    "IF(IF(id_ref>0,(SELECT bruto FROM ven_doc_venta k WHERE k.id=v.id_ref AND k.caj_id=" & Lp_IdMiCaja & "),0)>0,-1,v.id_ref),neto,iva,doc_nombre " & _
                    "FROM ven_doc_venta v " & _
                    "JOIN sis_documentos d USING(doc_id) " & _
                    "JOIN maestro_clientes c ON v.rut_cliente=c.rut_cliente " & _
                    "/*JOIN par_plazos_vencimiento p ON v.ven_plazo_id=p.pla_id */" & _
                    "WHERE doc_nota_de_credito='NO' AND doc_nota_de_venta='NO' AND doc_permite_pago='SI' AND caj_id=" & Lp_IdMiCaja & " " & _
                    "/* ORDER BY doc_abreviado,no_documento */ "
            
            Sql = Sql & " UNION SELECT  IFNULL((SELECT m.mpa_nombre " & _
                                     "FROM abo_tipos_de_pagos p " & _
                                    "JOIN cta_abono_documentos l ON p.abo_id = l.abo_id " & _
                                    "JOIN cta_abonos a ON l.abo_id = a.abo_id " & _
                                    "JOIN par_medios_de_pago m ON p.mpa_id=m.mpa_id " & _
                                    "WHERE a.abo_cli_pro = 'CLI' AND a.rut_emp = '" & SP_Rut_Activo & "' " & _
                                    "AND l.id = v.id " & _
                                    "LIMIT 1),'CREDITO')" & _
                                    ",fecha,ven_fecha_vencimiento, doc_abreviado,  no_documento,IF(v.rut_cliente='11.111.111-1','',c.nombre_rsocial),bruto*IF(d.doc_nota_de_credito='SI',-1,1),v.rut_cliente, " & _
                    "IF(IF(id_ref>0,(SELECT bruto FROM ven_doc_venta k WHERE k.id=v.id_ref),0)>0,-1,v.id_ref),neto,iva,doc_nombre " & _
                    "FROM ven_doc_venta v " & _
                    "JOIN sis_documentos d USING(doc_id) " & _
                    "JOIN maestro_clientes c ON v.rut_cliente=c.rut_cliente " & _
                    "/*JOIN par_plazos_vencimiento p ON v.ven_plazo_id=p.pla_id */" & _
                    "WHERE doc_nota_de_credito='SI' /*AND doc_permite_pago='SI'*/ AND caj_id=" & Lp_IdMiCaja & " " & _
                    "ORDER BY doc_abreviado,no_documento "
            
            
            
             Consulta RsTmp, Sql
            
            
    Else
                'vega modelo, incluir solo las ventas con boletas 4-4-2016
              
            Sql = "SELECT  IFNULL((SELECT m.mpa_nombre " & _
                                     "FROM abo_tipos_de_pagos p " & _
                                    "JOIN cta_abono_documentos l ON p.abo_id = l.abo_id " & _
                                    "JOIN cta_abonos a ON l.abo_id = a.abo_id " & _
                                    "JOIN par_medios_de_pago m ON p.mpa_id=m.mpa_id " & _
                                    "WHERE a.abo_cli_pro = 'CLI' AND a.rut_emp = '" & SP_Rut_Activo & "' " & _
                                    "AND l.id = v.id " & _
                                    "LIMIT 1),'CREDITO')" & _
                                    ",fecha,fecha, doc_abreviado,  no_documento,IF(v.rut_cliente='11.111.111-1','',c.nombre_rsocial),bruto*IF(d.doc_nota_de_credito='SI',-1,1),v.rut_cliente, " & _
                    "IF(IF(id_ref>0,(SELECT bruto FROM ven_doc_venta k WHERE k.id=v.id_ref AND k.caj_id=" & Lp_IdMiCaja & "),0)>0,-1,v.id_ref),neto,iva,doc_nombre " & _
                    "FROM ven_doc_venta v " & _
                    "JOIN sis_documentos d USING(doc_id) " & _
                    "JOIN maestro_clientes c ON v.rut_cliente=c.rut_cliente " & _
                    "/*JOIN par_plazos_vencimiento p ON v.ven_plazo_id=p.pla_id */" & _
                    "WHERE doc_boleta_venta='SI' AND doc_nota_de_credito='NO' AND doc_nota_de_venta='NO' AND doc_permite_pago='SI' AND caj_id=" & Lp_IdMiCaja & " " & _
                    "/* ORDER BY doc_abreviado,no_documento */ "
            
        '    Sql = Sql & " UNION SELECT  IFNULL((SELECT m.mpa_nombre " & _
                                     "FROM abo_tipos_de_pagos p " & _
                                    "JOIN cta_abono_documentos l ON p.abo_id = l.abo_id " & _
                                    "JOIN cta_abonos a ON l.abo_id = a.abo_id " & _
                                    "JOIN par_medios_de_pago m ON p.mpa_id=m.mpa_id " & _
                                    "WHERE a.abo_cli_pro = 'CLI' AND a.rut_emp = '" & SP_Rut_Activo & "' " & _
                                    "AND l.id = v.id " & _
                                    "LIMIT 1),'CREDITO')" & _
                                    ",fecha,ven_fecha_vencimiento, doc_abreviado,  no_documento,IF(v.rut_cliente='11.111.111-1','',c.nombre_rsocial),bruto*IF(d.doc_nota_de_credito='SI',-1,1),v.rut_cliente, " & _
                    "IF(IF(id_ref>0,(SELECT bruto FROM ven_doc_venta k WHERE k.id=v.id_ref),0)>0,-1,v.id_ref),neto,iva,doc_nombre " & _
                    "FROM ven_doc_venta v " & _
                    "JOIN sis_documentos d USING(doc_id) " & _
                    "JOIN maestro_clientes c ON v.rut_cliente=c.rut_cliente " & _
                    "/*JOIN par_plazos_vencimiento p ON v.ven_plazo_id=p.pla_id */" & _
                    "WHERE doc_nota_de_credito='SI' /*AND doc_permite_pago='SI'*/ AND caj_id=" & Lp_IdMiCaja & " " & _
                    "ORDER BY doc_abreviado,no_documento, v.fecha "
            
              Consulta RsTmp, Sql
    
    End If
    
    If SP_Rut_Activo = "96.803.210-0" Then
        'venga modelo
       LLenar_Grilla RsTmp, Me, LvDetalleDoc, True, True, True, False
        CmdNoIncluir.Visible = True
    Else
        LLenar_Grilla RsTmp, Me, LvDetalleDoc, False, True, True, False
    End If
    
    
    
    If SP_Rut_Activo = "96.803.210-0" Then
        ' **********  V E G A      M O D E L O  *************
        'Hacer conexion con la otra BD
        'para agregar las cajas cerradas que no hhan sido cerradas por adminitracion
        '26 Febrero  2016
        ConexionBDPorteria
        Sql = "SELECT 'EFECTIVO' efectivo,tur_apertura ap1,tur_apertura ape2,'BOF' bof,tur_numero,CONCAT( " & _
                "CAST((SELECT MIN(ven_nro_boleta) FROM ven_registro WHERE ven_registro.tur_id=ven_turno.tur_id) AS CHAR),' HASTA ', " & _
                "CAST((SELECT MAX(ven_nro_boleta) FROM ven_registro WHERE ven_registro.tur_id=ven_turno.tur_id) AS CHAR)) Documento," & _
                "(SELECT SUM(ven_valor) FROM ven_registro WHERE ven_registro.tur_id=ven_turno.tur_id) venta,tur_id " & _
                "From ven_turno " & _
                "WHERE  /* tur_apertura='" & Fql(DtFecha) & "' AND */  caj_id=0 AND tur_estado='CERRADO' " & _
                "ORDER BY tur_apertura,tur_numero"
         ConsultaV RsTmp, Sql
         Dim Bp_Sumado As Boolean
         If RsTmp.RecordCount > 0 Then
            RsTmp.MoveFirst
            Do While Not RsTmp.EOF
                LvDetalleDoc.ListItems.Add , , RsTmp!efectivo
                i = LvDetalleDoc.ListItems.Count
                LvDetalleDoc.ListItems(i).SubItems(1) = RsTmp!ap1
                LvDetalleDoc.ListItems(i).SubItems(2) = RsTmp!ape2
                LvDetalleDoc.ListItems(i).SubItems(3) = RsTmp!BOF
                LvDetalleDoc.ListItems(i).SubItems(4) = RsTmp!tur_numero
                LvDetalleDoc.ListItems(i).SubItems(5) = RsTmp!Documento
                LvDetalleDoc.ListItems(i).SubItems(6) = NumFormat(RsTmp!venta)
                LvDetalleDoc.ListItems(i).SubItems(8) = 0
                LvDetalleDoc.ListItems(i).SubItems(9) = 0
                LvDetalleDoc.ListItems(i).SubItems(10) = 0
                LvDetalleDoc.ListItems(i).SubItems(12) = RsTmp!tur_id
                Bp_Sumado = False
                For i = 1 To LvVentas.ListItems.Count
                    If LvVentas.ListItems(i) = 1 Then
                        LvVentas.ListItems(i).SubItems(2) = NumFormat(CDbl(LvVentas.ListItems(i).SubItems(2)) + RsTmp!venta)
                        Bp_Sumado = True
                    End If
                    
                
                Next
                If Not Bp_Sumado Then
                    LvVentas.ListItems.Add , , 1
                    LvVentas.ListItems(LvVentas.ListItems.Count).SubItems(1) = "EFECTIVO"
                    LvVentas.ListItems(LvVentas.ListItems.Count).SubItems(2) = NumFormat(RsTmp!venta)
                End If
                
                Bp_Sumado = False
                For i = 1 To LvDetalle.ListItems.Count
                    If LvDetalle.ListItems(i) = 1 Then
                        LvDetalle.ListItems(i).SubItems(2) = NumFormat(CDbl(LvDetalle.ListItems(i).SubItems(2)) + RsTmp!venta)
                        Bp_Sumado = True
                    End If
                    
                
                Next
                If Not Bp_Sumado Then
                    LvDetalle.ListItems.Add , , 1
                    LvDetalle.ListItems(LvDetalle.ListItems.Count).SubItems(1) = "EFECTIVO"
                    LvDetalle.ListItems(LvDetalle.ListItems.Count).SubItems(2) = NumFormat(RsTmp!venta)
                End If
                
                
                RsTmp.MoveNext
            Loop
        End If
         
        txtIngVentas = NumFormat(TotalizaColumna(LvVentas, "total"))
        txtTotal = NumFormat(TotalizaColumna(LvDetalle, "total"))
         
    End If
    
    
    TotalizaDoc
    

     
    
    '14 Diciembre
    'Cargamos tambien el detalle de los cheques
    '' se agrega por MSG 22-11-2016 campo che_fecha, che_fecha
    Sql = "SELECT che_id,che_numero,ban_nombre,che_monto " & _
            "FROM abo_cheques c " & _
            "JOIN par_bancos b ON c.ban_id=b.ban_id " & _
            "WHERE abo_id IN " & _
            "(SELECT d.abo_id " & _
                "FROM    cta_abono_documentos d " & _
                "/* JOIN ven_doc_venta m ON d.id=m.id */ " & _
                "JOIN cta_abonos a ON d.abo_id=a.abo_id AND a.abo_cli_pro='CLI' " & _
                "JOIN abo_tipos_de_pagos t ON d.abo_id=t.abo_id " & _
                "WHERE a.caj_id=" & Lp_IdMiCaja & " AND a.rut_emp='" & SP_Rut_Activo & "' AND t.mpa_id=2)"
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvCheques, False, True, True, False
    TxtTotalCheques = NumFormat(TotalizaColumna(LvCheques, "total"))
    '*********************************************
    GoTo Arqueo
cargaHistoricos:
    CargaHistoricas



Arqueo:
    '14 dic 2013
    'El esperado resumen
    LLenaArqueo
    
    
    
    
    '****************************************************************

End Sub

Private Sub TotalizaDoc()
    txtTotalDoc = 0
  For i = 1 To LvDetalleDoc.ListItems.Count
        If Val(LvDetalleDoc.ListItems(i).SubItems(8)) <= 0 Then
            txtTotalDoc = CDbl(txtTotalDoc) + CDbl(LvDetalleDoc.ListItems(i).SubItems(6))
        Else
            For L = 1 To LvDetalleDoc.ColumnHeaders.Count - 2
                LvDetalleDoc.ListItems(i).ListSubItems(L).ForeColor = vbGreen
            Next
        End If
    Next
    
    
    
    txtTotalDoc = NumFormat(txtTotalDoc)
End Sub

Private Sub LLenaArqueo()
    '14 Dic 2013
    'resumen ingresos
      Sql = "SELECT '1','EFECTIVO                                       (+)','0' UNION " & _
            "SELECT '2','CHEQUE                                         (+)','0' UNION " & _
            "SELECT 'G','MENOS GASTOS Y COMPRAS         (-)','0' UNION " & _
            "SELECT 'NC','MENOS NC                      (-)','0' UNION " & _
            "SELECT 'TC','TOTAL CAJA                                    (=)','0' UNION " & _
            "SELECT '','','' UNION " & _
            "SELECT 'TB','BILLETES                                         (+)','0' UNION " & _
            "SELECT 'CH','CHEQUES                                        (+)','0' UNION " & _
            "SELECT 'TA','TOTAL ARQUEO                              (=)','0' UNION " & _
            "SELECT '','','' UNION " & _
            "SELECT 'D','DIFERENCIA','0'"
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvResumenArqueo, False, True, True, False
    
    
    For i = 1 To LvVentas.ListItems.Count
        If LvVentas.ListItems(i).SubItems(1) = "NOTAS DE CREDITO" Then
            For X = 1 To LvResumenArqueo.ListItems.Count
                If LvResumenArqueo.ListItems(X) = "NC" Then
                    LvResumenArqueo.ListItems(X).SubItems(2) = NumFormat(Abs(CDbl(LvVentas.ListItems(i).SubItems(2))))
                    LvResumenArqueo.ListItems(X).ListSubItems(1).ForeColor = vbRed
                    Exit For
                End If
            Next
        
        End If
    Next
    
    
    For i = 1 To LvDetalle.ListItems.Count
        For X = 1 To LvResumenArqueo.ListItems.Count
            If LvDetalle.ListItems(i) = LvResumenArqueo.ListItems(X) Then
                LvResumenArqueo.ListItems(X).SubItems(2) = LvDetalle.ListItems(i).SubItems(2)
                Exit For
            End If
        Next
    Next
    
    For i = 1 To LvResumenArqueo.ListItems.Count
        If LvResumenArqueo.ListItems(i) = "1" Then
            LvResumenArqueo.ListItems(i).SubItems(2) = NumFormat(CDbl(LvResumenArqueo.ListItems(i).SubItems(2)) + CDbl(TxtTotalIngresos))
        End If
    Next
    
    For i = 1 To LvResumenArqueo.ListItems.Count
        If LvResumenArqueo.ListItems(i) = "G" Then LvResumenArqueo.ListItems(i).SubItems(2) = txtTotalGasto
        If LvResumenArqueo.ListItems(i) = "TB" Then LvResumenArqueo.ListItems(i).SubItems(2) = TxtTBilletes
        If LvResumenArqueo.ListItems(i) = "CH" Then LvResumenArqueo.ListItems(i).SubItems(2) = Me.TxtTotalCheques
    Next
    
    TOTALCAJA = 0
    TOTALBILLULLO = 0
    For i = 1 To LvResumenArqueo.ListItems.Count
        If LvResumenArqueo.ListItems(i) = "1" Or LvResumenArqueo.ListItems(i) = "2" Then
            TOTALCAJA = TOTALCAJA + CDbl(LvResumenArqueo.ListItems(i).SubItems(2))
        End If
        If LvResumenArqueo.ListItems(i) = "G" Or LvResumenArqueo.ListItems(i) = "NC" Then
            TOTALCAJA = TOTALCAJA - CDbl(LvResumenArqueo.ListItems(i).SubItems(2))
        End If
        If LvResumenArqueo.ListItems(i) = "TB" Or LvResumenArqueo.ListItems(i) = "CH" Then
            TOTALBILLULLO = TOTALBILLULLO + CDbl(LvResumenArqueo.ListItems(i).SubItems(2))
            
            
            
        End If
        
    Next
    

    
    
    
    
    For i = 1 To LvResumenArqueo.ListItems.Count
        If LvResumenArqueo.ListItems(i) = "TC" Then LvResumenArqueo.ListItems(i).SubItems(2) = NumFormat(TOTALCAJA)
        If LvResumenArqueo.ListItems(i) = "TA" Then LvResumenArqueo.ListItems(i).SubItems(2) = NumFormat(TOTALBILLULLO)
    Next
    diferencia = 0
    For i = 1 To LvResumenArqueo.ListItems.Count
        If LvResumenArqueo.ListItems(i) = "TC" Then diferencia = diferencia + CDbl(LvResumenArqueo.ListItems(i).SubItems(2))
        If LvResumenArqueo.ListItems(i) = "TA" Then diferencia = diferencia - CDbl(LvResumenArqueo.ListItems(i).SubItems(2))
    Next
    For i = 1 To LvResumenArqueo.ListItems.Count
        If LvResumenArqueo.ListItems(i) = "D" Then LvResumenArqueo.ListItems(i).SubItems(2) = NumFormat(diferencia)
    Next
    
    For i = 1 To LvResumenArqueo.ListItems.Count
        If LvResumenArqueo.ListItems(i) = "1" Then
            LvResumenArqueo.ListItems(i).ListSubItems(1).ForeColor = vbGreen
        End If
        If LvResumenArqueo.ListItems(i) = "2" Then
            LvResumenArqueo.ListItems(i).ListSubItems(1).ForeColor = vbGreen
            LvResumenArqueo.ListItems(i).ListSubItems(2).ForeColor = vbGreen
        End If
        If LvResumenArqueo.ListItems(i) = "G" Then
            LvResumenArqueo.ListItems(i).ListSubItems(1).ForeColor = vbRed
           'LvResumenArqueo.ListItems(i).ListSubItems(2).ForeColor = vbRed
        End If
        If LvResumenArqueo.ListItems(i) = "TC" Then
            LvResumenArqueo.ListItems(i).ListSubItems(1).ForeColor = vbBlue
        End If
        If LvResumenArqueo.ListItems(i) = "TA" Then
            LvResumenArqueo.ListItems(i).ListSubItems(1).ForeColor = vbBlue
        End If
        If LvResumenArqueo.ListItems(i) = "D" Then
            LvResumenArqueo.ListItems(i).ListSubItems(1).ForeColor = vbRed
        End If
    Next
    
        
    '2-12'2015 _
    Monto deposito y efectivo inicial caja siguiente.
    If CDbl(TxtMontoDeposito) > 0 Then
         TxtMontoInicial = NumFormat(CDbl(TxtTBilletes) - CDbl(TxtMontoDeposito))
    End If
    
    Sql = "UPDATE ven_caja SET caj_deposito=" & CDbl(TxtMontoDeposito) & ",caj_saldo=" & CDbl(TxtMontoInicial) & " " & _
            "WHERE caj_id=" & Lp_IdMiCaja
    cn.Execute Sql
    
End Sub



Private Sub LvBilletes_DblClick()
     If DG_ID_Unico > 0 Then Exit Sub
    If LvBilletes.SelectedItem Is Nothing Then Exit Sub
    
    TxtIdBillete = LvBilletes.SelectedItem
    TxTValorBillete = LvBilletes.SelectedItem.SubItems(1)
    txtCantBillete = LvBilletes.SelectedItem.SubItems(2)
    txtTotalBillete = LvBilletes.SelectedItem.SubItems(3)
    txtCantBillete.SetFocus
End Sub





Private Sub LvDetalleDoc_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    ordListView ColumnHeader, Me, LvDetalleDoc
End Sub

Private Sub LvGastos_DblClick()
    If DG_ID_Unico > 0 Then Exit Sub
    If LvGastos.SelectedItem Is Nothing Then Exit Sub
    If LvGastos.SelectedItem.SubItems(3) = "NO" Then
        MsgBox "No es posible modificar los gastos por factura..", vbInformation
        Exit Sub
    End If
    TxtId = LvGastos.SelectedItem
    TxtGasto = LvGastos.SelectedItem.SubItems(1)
    txtValorGasto = LvGastos.SelectedItem.SubItems(2)
    
    cn.Execute "DELETE FROM ven_gastos_por_caja " & _
                "WHERE vgc_id=" & TxtId
    
     CargaGastos
     TxtGasto.SetFocus


End Sub



Private Sub LvIngresos_DblClick()
    If DG_ID_Unico > 0 Then Exit Sub
    If LvIngresos.SelectedItem Is Nothing Then Exit Sub
    If LvIngresos.SelectedItem.SubItems(3) = "NO" Then
        MsgBox "No es posible modificar los ingresos por factura..", vbInformation
        Exit Sub
    End If
    TxtIdI = LvIngresos.SelectedItem
    TxtIngreso = LvIngresos.SelectedItem.SubItems(1)
    txtValorIngreso = LvIngresos.SelectedItem.SubItems(2)
    
    cn.Execute "DELETE FROM ven_ingresos_por_caja " & _
                "WHERE vic_id=" & TxtIdI
    
     CargaIngresos
     TxtIngreso.SetFocus
End Sub

Private Sub Timer1_Timer()
    On Error Resume Next
    LvDetalle.SetFocus
    Timer1.Enabled = False
End Sub

Private Sub ImprimeCierre()
    Dim Lp_SoloContado As Long
    Dim Cx As Double, Cy As Double, Sp_Fecha As String, Dp_S As Double, Sp_Letras As String
    Dim Sp_Valor As String * 10, Sp_Fpago As String * 12
    
    Dim Sp_FpagoR As String * 15
    Dim Sp_Femision As String * 11
    Dim Sp_Fvenc As String * 11
    Dim Sp_Doc As String * 4
    Dim Sp_Nro As String * 9
    Dim Sp_Cliente As String * 35
    
    Dim Sp_Empresa As String
    Dim Sp_Giro As String
    Dim Sp_Direccion As String
    Printer.FontName = "Courier New"
    
    Dp_S = 0.3
    For f = 1 To Dialogo.Copies
        On Error GoTo ErroComp
        Sql = "SELECT giro,direccion,ciudad " & _
             "FROM sis_empresas " & _
             "WHERE rut='" & SP_Rut_Activo & "'"
        Consulta RsTmp2, Sql
        If RsTmp2.RecordCount > 0 Then
            Sp_Giro = RsTmp2!giro
            Sp_Direccion = RsTmp2!direccion & " - " & RsTmp2!ciudad
        End If
        Printer.ScaleMode = 7
        Cx = 1.5
        Cy = 1.9
        Dp_S = 0.3
        Printer.CurrentX = Cx
        Printer.CurrentY = Cy
        'Printer.PaintPicture LoadPicture(App.Path & "\IMG\LAFLOR\LAFLOR.jpg"), Cx + 0.62, 1
       ' Printer.PaintPicture LoadPicture(App.Path & "\REDMAROK.jpg"), Cx + 0.62, 1
        Coloca Cx, Cy, SP_Empresa_Activa, False, 10
        Cy = Cy + (Dp_S * 2)
        Coloca Cx + 6, Cy, "ARQUEO DE CAJA", True, 14
        Cy = Cy + 1
        Sp_FpagoR = "SUCURSAL"
        Coloca Cx, Cy, Sp_FpagoR & ":" & Me.txtSucursal, False, 10
        Sp_FpagoR = "APERTURA"
        Coloca Cx, Cy + Dp_S, Sp_FpagoR & ":" & DtFecha, False, 10
        Sp_FpagoR = "CIERRE"
        Coloca Cx, Cy + Dp_S + Dp_S, Sp_FpagoR & ":" & Now, False, 10
        
        Sp_FpagoR = "EFECT. INICIAL"
        Coloca Cx, Cy + Dp_S + Dp_S + Dp_S, Sp_FpagoR & ":" & txtEfectivoInicial, False, 10
        
        Cy = Cy + Dp_S * 5 'interlineas
        fincierra = Cy - 0.2
        Coloca Cx, Cy, "INGRESOS POR VENTAS", True, 8
        Coloca Cx + 6, Cy, "INGRESOS POR CTAS CTES", True, 8
        Coloca Cx + 12.5, Cy, "RESUMEN DE INGRESOS", True, 8
        Cy = Cy + 0.4
        pos1 = Cy
        For i = 1 To Me.LvVentas.ListItems.Count
            Sp_Fpago = LvVentas.ListItems(i).SubItems(1)
            Coloca Cx, Cy, Sp_Fpago, False, 8
            RSet Sp_Valor = LvVentas.ListItems(i).SubItems(2)
            Coloca Cx + 3, Cy, Sp_Valor, False, 8
            Cy = Cy + Dp_S
        Next i
        finr = Cy
        Cy = pos1
        For i = 1 To LvCtaCtes.ListItems.Count
            Sp_Fpago = LvCtaCtes.ListItems(i).SubItems(1)
            Coloca Cx + 6, Cy, Sp_Fpago, False, 8
            RSet Sp_Valor = LvCtaCtes.ListItems(i).SubItems(2)
            Coloca Cx + 6 + 3, Cy, Sp_Valor, False, 8
            Cy = Cy + Dp_S
        Next i
        If Cy > finr Then finr = Cy
        Cy = pos1
        For i = 1 To LvDetalle.ListItems.Count
            Sp_Fpago = LvDetalle.ListItems(i).SubItems(1)
            Coloca Cx + 12.5, Cy, Sp_Fpago, False, 8
            RSet Sp_Valor = LvDetalle.ListItems(i).SubItems(2)
            Coloca Cx + 15.5, Cy, Sp_Valor, False, 8
            Cy = Cy + Dp_S
        Next i
        If Cy > finr Then finr = Cy 'Hasta variable es para el final del cuadro de los resumen
        'Imprimir totales
        Cy = finr
        RSet Sp_Fpago = "TOTAL"
      
        RSet Sp_Valor = txtIngVentas
        Coloca Cx, Cy, Sp_Fpago, True, 8
        Coloca Cx + 3, Cy, Sp_Valor, True, 8
        RSet Sp_Valor = txtIngCtaCtes
        Coloca Cx + 6, Cy, Sp_Fpago, True, 8
        Coloca Cx + 9, Cy, Sp_Valor, True, 8
        RSet Sp_Valor = txtTotal
        Coloca Cx + 12.5, Cy, Sp_Fpago, True, 8
        Coloca Cx + 15.5, Cy, Sp_Valor, True, 8
        Cy = Cy + Dp_S
        finr = Cy + 0.2
        
               
        'Ahora el detalle
        With LvDetalleDoc
            Sp_FpagoR = "F.pago"
            Sp_Femision = "Emision"
            Sp_Fvenc = "Vence"
            Sp_Doc = "Doc"
            Sp_Nro = "Numero"
            Sp_Cliente = "Cliente"
            RSet Sp_Valor = "Valor"
            Cy = finr + 0.4
            Coloca Cx, Cy, "DETALLE DE VENTAS", True, 10
            Cy = Cy + Dp_S
            Coloca Cx, Cy, Sp_FpagoR & " " & Sp_Femision & " " & Sp_Fvenc & " " & Sp_Doc & " " & Sp_Nro & " " & Sp_Cliente & " " & Sp_Valor, True, 8
            Cy = Cy + Dp_S
            For i = 1 To .ListItems.Count
                If SP_Rut_Activo = "76.369.600-6" Then
                    'Para fertiquimica
                    'Imprimimos todos los doc de venta y no sumamos lo que no es contado
                    '18-Ene-2014
                '    If .ListItems(i).SubItems(3) <> "BOM" Then
                        Sp_FpagoR = .ListItems(i)
                        Sp_Femision = .ListItems(i).SubItems(1)
                        Sp_Fvenc = .ListItems(i).SubItems(2)
                        Sp_Doc = .ListItems(i).SubItems(3)
                        Sp_Nro = .ListItems(i).SubItems(4)
                        If .ListItems(i).SubItems(7) = "11.111.111-1" Then
                            Sp_Cliente = ""
                        Else
                            Sp_Cliente = .ListItems(i).SubItems(5)
                        End If
                        If Trim(Sp_FpagoR) = "CONTADO" Then
                            RSet Sp_Valor = .ListItems(i).SubItems(6)
                        Else
                            RSet Sp_Valor = "(" & .ListItems(i).SubItems(6) & ")"
                        End If
                        Coloca Cx, Cy, Sp_FpagoR & " " & Sp_Femision & " " & Sp_Fvenc & " " & Sp_Doc & " " & Sp_Nro & " " & Sp_Cliente & " " & Sp_Valor, False, 8
                        Cy = Cy + Dp_S
               '     End If
                    ''
                Else
                    
                    If .ListItems(i).SubItems(3) <> "BOM" Then
                        Sp_FpagoR = .ListItems(i)
                        Sp_Femision = .ListItems(i).SubItems(1)
                        Sp_Fvenc = .ListItems(i).SubItems(2)
                        Sp_Doc = .ListItems(i).SubItems(3)
                        Sp_Nro = .ListItems(i).SubItems(4)
                        If .ListItems(i).SubItems(7) = "11.111.111-1" Then
                            Sp_Cliente = ""
                        Else
                            Sp_Cliente = .ListItems(i).SubItems(5)
                        End If
                        RSet Sp_Valor = .ListItems(i).SubItems(6)
                        Coloca Cx, Cy, Sp_FpagoR & " " & Sp_Femision & " " & Sp_Fvenc & " " & Sp_Doc & " " & Sp_Nro & " " & Sp_Cliente & " " & Sp_Valor, False, 8
                        Cy = Cy + Dp_S
                    End If
                End If
            Next
            
            If SP_Rut_Activo = "76.369.600-6" Then
            
                ''
            Else
                    
                For i = 1 To .ListItems.Count
                    If .ListItems(i).SubItems(3) = "BOM" Then
                        Sql = "SELECT CONCAT('DE ',CAST(MIN(no_documento) AS CHAR),' A ',CAST(max(no_documento) AS CHAR)) boletas,SUM(bruto) venta " & _
                                "FROM ven_doc_venta " & _
                                "JOIN sis_documentos USING(doc_id) " & _
                                "WHERE caj_id=" & Lp_IdMiCaja & " AND doc_nombre='BOLETA'"
                        Consulta RsTmp, Sql
                        Sp_FpagoR = ""
                        Sp_Femision = ""
                        Sp_Fvenc = ""
                        Sp_Doc = "BOM"
                        Sp_Nro = ""
                        Sp_Cliente = RsTmp!boletas
                        RSet Sp_Valor = NumFormat(RsTmp!venta)
                        Coloca Cx, Cy, Sp_FpagoR & " " & Sp_Femision & " " & Sp_Fvenc & " " & Sp_Doc & " " & Sp_Nro & " " & Sp_Cliente & " " & Sp_Valor, False, 8
                        Cy = Cy + Dp_S
                        Exit For
                    End If
                Next
            End If
            
            Sp_FpagoR = ""
            Sp_Femision = ""
            Sp_Fvenc = ""
            Sp_Doc = ""
            Sp_Nro = ""
            RSet Sp_Cliente = "TOTAL"
            If SP_Rut_Activo = "76.369.600-6" Then
                For i = 1 To Me.LvDetalleDoc.ListItems.Count
                    If Trim(LvDetalleDoc.ListItems(i)) = "CONTADO" Then
                        Lp_SoloContado = Lp_SoloContado + CDbl(LvDetalleDoc.ListItems(i).SubItems(6))
                    End If
                    
                Next
                RSet Sp_Valor = NumFormat(Lp_SoloContado)
                    
                
            Else
                RSet Sp_Valor = txtTotalDoc
            End If
            Coloca Cx, Cy, Sp_FpagoR & " " & Sp_Femision & " " & Sp_Fvenc & " " & Sp_Doc & " " & Sp_Nro & " " & Sp_Cliente & " " & Sp_Valor, True, 8
            
           
            If SP_Rut_Activo = "76.369.600-6" Then
                'Restamos el valor de los gastos a los pagos contado
                If LvGastos.ListItems.Count > 0 Then
                    RSet Sp_Cliente = "GASTOS POR CAJA"
                    RSet Sp_Valor = Me.txtTotalGasto
                    Cy = Cy + Dp_S
                    Coloca Cx, Cy, Sp_FpagoR & " " & Sp_Femision & " " & Sp_Fvenc & " " & Sp_Doc & " " & Sp_Nro & " " & Sp_Cliente & " " & Sp_Valor, True, 8
                    If Val(txtTotalGasto) = 0 Then TxtGasto = 0
                    
                    RSet Sp_Cliente = "LIQUIDO"
                    RSet Sp_Valor = NumFormat(Lp_SoloContado - CDbl(txtTotalGasto))
                    Cy = Cy + Dp_S
                    Coloca Cx, Cy, Sp_FpagoR & " " & Sp_Femision & " " & Sp_Fvenc & " " & Sp_Doc & " " & Sp_Nro & " " & Sp_Cliente & " " & Sp_Valor, True, 8
                End If
            End If
            
            Finde = Cy + Dp_S
        End With
      
        
                'Ahora el detalle DE PAGOS POR CTA CTE
        
        Cy = Finde + Dp_S * 2
        cuantoenabonos = 0
        If LvDetalleCtaCte.ListItems.Count > 0 Then
            
            With LvDetalleCtaCte
                Sp_FpagoR = "F.pago"
                Sp_Femision = "Emision"
                Sp_Fvenc = "Vence"
                Sp_Doc = "Doc"
                Sp_Nro = "Numero"
                Sp_Cliente = "Cliente"
                RSet Sp_Valor = "Valor"
                Cy = Finde + 0.5
                Coloca Cx, Cy, "DETALLE INGRESOS POR CTA CTE", True, 10
                Cy = Cy + Dp_S
                Coloca Cx, Cy, Sp_FpagoR & " " & Sp_Femision & " " & Sp_Fvenc & " " & Sp_Doc & " " & Sp_Nro & " " & Sp_Cliente & " " & Sp_Valor, True, 8
                Cy = Cy + Dp_S
                For i = 1 To .ListItems.Count
                    Sp_FpagoR = .ListItems(i)
                    Sp_Femision = .ListItems(i).SubItems(1)
                    Sp_Fvenc = .ListItems(i).SubItems(2)
                    Sp_Doc = .ListItems(i).SubItems(3)
                    Sp_Nro = .ListItems(i).SubItems(4)
                    Sp_Cliente = .ListItems(i).SubItems(5)
                    RSet Sp_Valor = .ListItems(i).SubItems(6)
                    If Mid(Sp_FpagoR, 1, 9) = "ABONO CON" Then
                        'x
                        '28.12.2016
                        'Separa los abonos de los pagos Vega Modelo
                        
                        cuantoenabonos = cuantoenabonos + CDbl(.ListItems(i).SubItems(6))
                        
                    Else
                    
                        Coloca Cx, Cy, Sp_FpagoR & " " & Sp_Femision & " " & Sp_Fvenc & " " & Sp_Doc & " " & Sp_Nro & " " & Sp_Cliente & " " & Sp_Valor, False, 8
                        Cy = Cy + Dp_S
                    End If
                    
                Next
                Sp_FpagoR = ""
                Sp_Femision = ""
                Sp_Fvenc = ""
                Sp_Doc = ""
                Sp_Nro = ""
                RSet Sp_Cliente = "TOTAL"
                RSet Sp_Valor = NumFormat(CDbl(txtTotalCtaCte) - CDbl(cuantoenabonos))
                Coloca Cx, Cy, Sp_FpagoR & " " & Sp_Femision & " " & Sp_Fvenc & " " & Sp_Doc & " " & Sp_Nro & " " & Sp_Cliente & " " & Sp_Valor, True, 8
                finc = Cy + Dp_S
            End With
        End If
        Cy = Cy + 0.5
        inich = Cy - 0.2
        
        'Ahora en detalle de gastos y el detalle de cheques recibidos
        Dim Sp_ChNro As String * 12
        Dim Sp_ChBanco As String * 12
        Dim Sp_ChValor As String * 12
        Dim Sp_DetGasto As String * 25
        Dim Sp_DetGastoValor As String * 10
        Coloca Cx, Cy, "CHEQUES RECIBIDOS", True, 8
        Coloca Cx + 12.5, Cy, "GASTOS POR CAJA", True, 8
        Cy = Cy + 0.4
        pedeta = Cy
        Sp_ChNro = "Nro"
        Sp_ChBanco = "Banco"
        RSet Sp_ChValor = "Valor"
        Sp_DetGasto = "Detalle"
        RSet Sp_DetGastoValor = "Valor"
        
        Coloca Cx, Cy, Sp_ChNro & " " & Sp_ChBanco & " " & Sp_ChValor, False, 8
        Coloca Cx + 11, Cy, Sp_DetGasto & " " & Sp_DetGastoValor, False, 8
        'Coloca Cx + 12.5, Cy, "GASTOS POR CAJA", True, 8
        
        Cy = Cy + Dp_S
        For i = 1 To LvCheques.ListItems.Count
            Sp_ChNro = LvCheques.ListItems(i).SubItems(1)
            Sp_ChBanco = LvCheques.ListItems(i).SubItems(2)
            RSet Sp_ChValor = LvCheques.ListItems(i).SubItems(3)
            Coloca Cx, Cy, Sp_ChNro & " " & Sp_ChBanco & " " & Sp_ChValor, False, 8
            Cy = Cy + Dp_S
        Next i
        finch = Cy
        Sp_ChNro = ""
        RSet Sp_ChBanco = "TOT. CHEQUES"
        RSet Sp_ChValor = TxtTotalCheques
        Coloca Cx, Cy, Sp_ChNro & " " & Sp_ChBanco & " " & Sp_ChValor, True, 8
        
        Cy = pedeta
                
        Cy = Cy + Dp_S
            
            For i = 1 To LvGastos.ListItems.Count
                Sp_DetGasto = LvGastos.ListItems(i).SubItems(1)
                RSet Sp_DetGastoValor = LvGastos.ListItems(i).SubItems(2)
                Coloca Cx + 11, Cy, Sp_DetGasto & " " & Sp_DetGastoValor, False, 8
                Cy = Cy + Dp_S
            Next i
            RSet Sp_DetGasto = "TOTAL GASTOS"
            RSet Sp_DetGastoValor = txtTotalGasto
            Coloca Cx + 11, Cy, Sp_DetGasto & " " & Sp_DetGastoValor, True, 8
            If Cy > finch Then finch = Cy
            
            
        'Agregar detalle de billets ingresados
        Dim Sp_ValorBillete As String * 6
        Dim Sp_CantidadBilletes As String * 4
        Dim Sp_TotalBilletes As String * 10
        
        finch = finch + Dp_S + Dp_S
        Cy = finch + Dp_S + Dp_S
        iniciobilletes = finch + Dp_S + Dp_S
        finbilletes = Cy
        
        Coloca Cx, Cy, "Detalle de Billetes", True, 8
        Cy = Cy + Dp_S
        RSet Sp_ValorBillete = "Valor"
        RSet Sp_CantidadBilletes = "Cant."
        RSet Sp_TotalBilletes = "Total"
        Coloca Cx, Cy, Sp_ValorBillete & " " & Sp_CantidadBilletes & " " & Sp_TotalBilletes, True, 8
        Cy = Cy + Dp_S
         For i = 1 To LvBilletes.ListItems.Count
                RSet Sp_ValorBillete = LvBilletes.ListItems(i).SubItems(1)
                RSet Sp_CantidadBilletes = LvBilletes.ListItems(i).SubItems(2)
                RSet Sp_TotalBilletes = LvBilletes.ListItems(i).SubItems(3)
                Coloca Cx, Cy, Sp_ValorBillete & " " & Sp_CantidadBilletes & " " & Sp_TotalBilletes, False, 8
                
                Cy = Cy + Dp_S
        Next i
        RSet Sp_ValorBillete = "TOTAL"
        RSet Sp_CantidadBilletes = "BILLETES"
        RSet Sp_TotalBilletes = TxtTBilletes
        Coloca Cx, Cy, Sp_ValorBillete & " " & Sp_CantidadBilletes & " " & Sp_TotalBilletes, True, 8
        If Cy > finbilletes Then finbilletes = Cy
        
        finbilletes = finbilletes + Dp_S + Dp_S
        
            
       ' Printer.DrawMode = 1
        Printer.DrawWidth = 3
        
        Printer.Line (1.4, 1)-(19, fincierra), , B 'Rectangulo Encabezado
        
        Printer.Line (1.4, fincierra)-(19, finr), , B 'ingressos
        Printer.Line (1.4 + 5.8, fincierra)-(12.3, finr), , B 'ingressos
        
    
        If Me.LvDetalleDoc.ListItems.Count > 0 Then Printer.Line (1.4, finr)-(19, Finde), , B       'detalle ventas
        If Me.LvDetalleCtaCte.ListItems.Count > 0 Then Printer.Line (1.4, Finde)-(19, finc), , B        'detalle ingresos ctas ctes
        Printer.Line (1.4, inich)-(19, finch), , B 'Cheques y gastos
        
        Printer.Line (1.4, iniciobilletes)-(19, finbilletes), , B 'Detalle de billets
        
        Printer.NewPage
        Printer.EndDoc
        Sp_Valor = 0
        Lp_SoloContado = 0
        
    Next
    Exit Sub
ErroComp:
    MsgBox "Error :" & Err.Description & vbNewLine & "Nro " & Err.Number & vbNewLine & "Lin:" & Err.Source
End Sub

Private Sub ImprimeCierreGemppAldunate()
    Dim Lp_SoloContado As Long
    Dim Cx As Double, Cy As Double, Sp_Fecha As String, Dp_S As Double, Sp_Letras As String
    Dim Sp_Valor As String * 10, Sp_Fpago As String * 12
    
    Dim Sp_FpagoR As String * 15
    Dim Sp_Femision As String * 11
    Dim Sp_Fvenc As String * 11
    Dim Sp_Doc As String * 4
    Dim Sp_Nro As String * 9
    Dim Sp_Cliente As String * 35
    
    Dim Sp_Empresa As String
    Dim Sp_Giro As String
    Dim Sp_Direccion As String
    Printer.FontName = "Courier New"
    
    Dp_S = 0.3
    For f = 1 To Dialogo.Copies
        On Error GoTo ErroComp
        Sql = "SELECT giro,direccion,ciudad " & _
             "FROM sis_empresas " & _
             "WHERE rut='" & SP_Rut_Activo & "'"
        Consulta RsTmp2, Sql
        If RsTmp2.RecordCount > 0 Then
            Sp_Giro = RsTmp2!giro
            Sp_Direccion = RsTmp2!direccion & " - " & RsTmp2!ciudad
        End If
        Printer.ScaleMode = 7
        Cx = 1.5
        Cy = 1.9
        Dp_S = 0.3
        Printer.CurrentX = Cx
        Printer.CurrentY = Cy
        'Printer.PaintPicture LoadPicture(App.Path & "\IMG\LAFLOR\LAFLOR.jpg"), Cx + 0.62, 1
       ' Printer.PaintPicture LoadPicture(App.Path & "\REDMAROK.jpg"), Cx + 0.62, 1
        Coloca Cx, Cy, SP_Empresa_Activa, False, 10
        Cy = Cy + (Dp_S * 2)
        Coloca Cx + 6, Cy, "ARQUEO DE CAJA", True, 14
        Cy = Cy + 1
        Sp_FpagoR = "SUCURSAL"
        Coloca Cx, Cy, Sp_FpagoR & ":" & Me.txtSucursal, False, 10
        Sp_FpagoR = "APERTURA"
        Coloca Cx, Cy + Dp_S, Sp_FpagoR & ":" & DtFecha, False, 10
        Sp_FpagoR = "CIERRE"
        Coloca Cx, Cy + Dp_S + Dp_S, Sp_FpagoR & ":" & Now, False, 10
        
        Sp_FpagoR = "EFECT. INICIAL"
        Coloca Cx, Cy + Dp_S + Dp_S + Dp_S, Sp_FpagoR & ":" & txtEfectivoInicial, False, 10
        
        Cy = Cy + Dp_S * 5 'interlineas
        fincierra = Cy - 0.2
        Coloca Cx, Cy, "INGRESOS POR VENTAS", True, 8
        Coloca Cx + 6, Cy, "INGRESOS POR CTAS CTES", True, 8
        Coloca Cx + 12.5, Cy, "RESUMEN DE INGRESOS", True, 8
        Cy = Cy + 0.4
        pos1 = Cy
        For i = 1 To Me.LvVentas.ListItems.Count
            Sp_Fpago = LvVentas.ListItems(i).SubItems(1)
            Coloca Cx, Cy, Sp_Fpago, False, 8
            RSet Sp_Valor = LvVentas.ListItems(i).SubItems(2)
            Coloca Cx + 3, Cy, Sp_Valor, False, 8
            Cy = Cy + Dp_S
        Next i
        finr = Cy
        Cy = pos1
        For i = 1 To LvCtaCtes.ListItems.Count
            Sp_Fpago = LvCtaCtes.ListItems(i).SubItems(1)
            Coloca Cx + 6, Cy, Sp_Fpago, False, 8
            RSet Sp_Valor = LvCtaCtes.ListItems(i).SubItems(2)
            Coloca Cx + 6 + 3, Cy, Sp_Valor, False, 8
            Cy = Cy + Dp_S
        Next i
        If Cy > finr Then finr = Cy
        Cy = pos1
        For i = 1 To LvDetalle.ListItems.Count
            Sp_Fpago = LvDetalle.ListItems(i).SubItems(1)
            Coloca Cx + 12.5, Cy, Sp_Fpago, False, 8
            RSet Sp_Valor = LvDetalle.ListItems(i).SubItems(2)
            Coloca Cx + 15.5, Cy, Sp_Valor, False, 8
            Cy = Cy + Dp_S
        Next i
        If Cy > finr Then finr = Cy 'Hasta variable es para el final del cuadro de los resumen
        'Imprimir totales
        Cy = finr
        RSet Sp_Fpago = "TOTAL"
      
        RSet Sp_Valor = txtIngVentas
        Coloca Cx, Cy, Sp_Fpago, True, 8
        Coloca Cx + 3, Cy, Sp_Valor, True, 8
        RSet Sp_Valor = txtIngCtaCtes
        Coloca Cx + 6, Cy, Sp_Fpago, True, 8
        Coloca Cx + 9, Cy, Sp_Valor, True, 8
        RSet Sp_Valor = txtTotal
        Coloca Cx + 12.5, Cy, Sp_Fpago, True, 8
        Coloca Cx + 15.5, Cy, Sp_Valor, True, 8
        Cy = Cy + Dp_S
        finr = Cy + 0.2
        
               
        'Ahora el detalle
        With LvDetalleDoc
            Sp_FpagoR = "F.pago"
            Sp_Femision = "Emision"
            Sp_Fvenc = "Vence"
            Sp_Doc = "Doc"
            Sp_Nro = "Numero"
            Sp_Cliente = "Cliente"
            RSet Sp_Valor = "Valor"
            Cy = finr + 0.4
            Coloca Cx, Cy, "DETALLE DE VENTAS", True, 10
            Cy = Cy + Dp_S
            Coloca Cx, Cy, Sp_FpagoR & " " & Sp_Femision & " " & Sp_Fvenc & " " & Sp_Doc & " " & Sp_Nro & " " & Sp_Cliente & " " & Sp_Valor, True, 8
            Cy = Cy + Dp_S
            For i = 1 To .ListItems.Count
                
                    
                    If .ListItems(i).SubItems(3) <> "BOM" And .ListItems(i).SubItems(3) <> "BOF" Then
                        Sp_FpagoR = .ListItems(i)
                        Sp_Femision = .ListItems(i).SubItems(1)
                        Sp_Fvenc = .ListItems(i).SubItems(2)
                        Sp_Doc = .ListItems(i).SubItems(3)
                        Sp_Nro = .ListItems(i).SubItems(4)
                        If .ListItems(i).SubItems(7) = "11.111.111-1" Then
                            Sp_Cliente = ""
                        Else
                            Sp_Cliente = .ListItems(i).SubItems(5)
                        End If
                        RSet Sp_Valor = .ListItems(i).SubItems(6)
                        Coloca Cx, Cy, Sp_FpagoR & " " & Sp_Femision & " " & Sp_Fvenc & " " & Sp_Doc & " " & Sp_Nro & " " & Sp_Cliente & " " & Sp_Valor, False, 8
                        Cy = Cy + Dp_S
                    End If
               
            Next
            
            If SP_Rut_Activo = "76.369.600-6" Then
            
                ''
            Else
                    
                For i = 1 To .ListItems.Count
                    If .ListItems(i).SubItems(3) = "BOM" Then
                        Sql = "SELECT CONCAT('DE ',CAST(MIN(no_documento) AS CHAR),' A ',CAST(max(no_documento) AS CHAR)) boletas,SUM(bruto) venta " & _
                                "FROM ven_doc_venta " & _
                                "JOIN sis_documentos USING(doc_id) " & _
                                "WHERE caj_id=" & Lp_IdMiCaja & " AND doc_nombre='BOLETA'"
                        Consulta RsTmp, Sql
                        Sp_FpagoR = ""
                        Sp_Femision = ""
                        Sp_Fvenc = ""
                        Sp_Doc = "BOM"
                        Sp_Nro = ""
                        Sp_Cliente = RsTmp!boletas
                        RSet Sp_Valor = NumFormat(RsTmp!venta)
                        Coloca Cx, Cy, Sp_FpagoR & " " & Sp_Femision & " " & Sp_Fvenc & " " & Sp_Doc & " " & Sp_Nro & " " & Sp_Cliente & " " & Sp_Valor, False, 8
                        Cy = Cy + Dp_S
                        Exit For
                    End If
                Next
                
                For i = 1 To .ListItems.Count
                    If .ListItems(i).SubItems(3) = "BOF" Then
                        Sql = "SELECT CONCAT('DE ',CAST(MIN(no_documento) AS CHAR),' A ',CAST(max(no_documento) AS CHAR)) boletas,SUM(bruto) venta " & _
                                "FROM ven_doc_venta " & _
                                "JOIN sis_documentos USING(doc_id) " & _
                                "WHERE caj_id=" & Lp_IdMiCaja & " AND doc_nombre='BOLETA FISCAL'"
                        Consulta RsTmp, Sql
                        Sp_FpagoR = ""
                        Sp_Femision = ""
                        Sp_Fvenc = ""
                        Sp_Doc = "BOF"
                        Sp_Nro = ""
                        Sp_Cliente = RsTmp!boletas
                        RSet Sp_Valor = NumFormat(RsTmp!venta)
                        Coloca Cx, Cy, Sp_FpagoR & " " & Sp_Femision & " " & Sp_Fvenc & " " & Sp_Doc & " " & Sp_Nro & " " & Sp_Cliente & " " & Sp_Valor, False, 8
                        Cy = Cy + Dp_S
                        Exit For
                    End If
                Next
                
            End If
            
            Sp_FpagoR = ""
            Sp_Femision = ""
            Sp_Fvenc = ""
            Sp_Doc = ""
            Sp_Nro = ""
            RSet Sp_Cliente = "TOTAL"
            If SP_Rut_Activo = "76.369.600-6" Then
                For i = 1 To Me.LvDetalleDoc.ListItems.Count
                    If Trim(LvDetalleDoc.ListItems(i)) = "CONTADO" Then
                        Lp_SoloContado = Lp_SoloContado + CDbl(LvDetalleDoc.ListItems(i).SubItems(6))
                    End If
                    
                Next
                RSet Sp_Valor = NumFormat(Lp_SoloContado)
                    
                
            Else
                RSet Sp_Valor = txtTotalDoc
            End If
            Coloca Cx, Cy, Sp_FpagoR & " " & Sp_Femision & " " & Sp_Fvenc & " " & Sp_Doc & " " & Sp_Nro & " " & Sp_Cliente & " " & Sp_Valor, True, 8
            
           
            If SP_Rut_Activo = "76.369.600-6" Then
                'Restamos el valor de los gastos a los pagos contado
                If LvGastos.ListItems.Count > 0 Then
                    RSet Sp_Cliente = "GASTOS POR CAJA"
                    RSet Sp_Valor = Me.txtTotalGasto
                    Cy = Cy + Dp_S
                    Coloca Cx, Cy, Sp_FpagoR & " " & Sp_Femision & " " & Sp_Fvenc & " " & Sp_Doc & " " & Sp_Nro & " " & Sp_Cliente & " " & Sp_Valor, True, 8
                    If Val(txtTotalGasto) = 0 Then TxtGasto = 0
                    
                    RSet Sp_Cliente = "LIQUIDO"
                    RSet Sp_Valor = NumFormat(Lp_SoloContado - CDbl(txtTotalGasto))
                    Cy = Cy + Dp_S
                    Coloca Cx, Cy, Sp_FpagoR & " " & Sp_Femision & " " & Sp_Fvenc & " " & Sp_Doc & " " & Sp_Nro & " " & Sp_Cliente & " " & Sp_Valor, True, 8
                End If
            End If
            
            Finde = Cy + Dp_S
        End With
      
        
                'Ahora el detalle DE PAGOS POR CTA CTE
        
        Cy = Finde + Dp_S * 2
        If LvDetalleCtaCte.ListItems.Count > 0 Then
            With LvDetalleCtaCte
                Sp_FpagoR = "F.pago"
                Sp_Femision = "Emision"
                Sp_Fvenc = "Vence"
                Sp_Doc = "Doc"
                Sp_Nro = "Numero"
                Sp_Cliente = "Cliente"
                RSet Sp_Valor = "Valor"
                Cy = Finde + 0.5
                Coloca Cx, Cy, "DETALLE INGRESOS POR CTA CTE", True, 10
                Cy = Cy + Dp_S
                Coloca Cx, Cy, Sp_FpagoR & " " & Sp_Femision & " " & Sp_Fvenc & " " & Sp_Doc & " " & Sp_Nro & " " & Sp_Cliente & " " & Sp_Valor, True, 8
                Cy = Cy + Dp_S
                For i = 1 To .ListItems.Count
                    Sp_FpagoR = .ListItems(i)
                    Sp_Femision = .ListItems(i).SubItems(1)
                    Sp_Fvenc = .ListItems(i).SubItems(2)
                    Sp_Doc = .ListItems(i).SubItems(3)
                    Sp_Nro = .ListItems(i).SubItems(4)
                    Sp_Cliente = .ListItems(i).SubItems(5)
                    RSet Sp_Valor = .ListItems(i).SubItems(6)
                    Coloca Cx, Cy, Sp_FpagoR & " " & Sp_Femision & " " & Sp_Fvenc & " " & Sp_Doc & " " & Sp_Nro & " " & Sp_Cliente & " " & Sp_Valor, False, 8
                    Cy = Cy + Dp_S
                Next
                Sp_FpagoR = ""
                Sp_Femision = ""
                Sp_Fvenc = ""
                Sp_Doc = ""
                Sp_Nro = ""
                RSet Sp_Cliente = "TOTAL"
                RSet Sp_Valor = txtTotalCtaCte
                Coloca Cx, Cy, Sp_FpagoR & " " & Sp_Femision & " " & Sp_Fvenc & " " & Sp_Doc & " " & Sp_Nro & " " & Sp_Cliente & " " & Sp_Valor, True, 8
                finc = Cy + Dp_S
            End With
        End If
        Cy = Cy + 0.5
        inich = Cy - 0.2
        
        'Ahora en detalle de gastos y el detalle de cheques recibidos
        Dim Sp_ChNro As String * 12
        Dim Sp_ChBanco As String * 12
        Dim Sp_ChValor As String * 12
        Dim Sp_DetGasto As String * 25
        Dim Sp_DetGastoValor As String * 10
        Coloca Cx, Cy, "CHEQUES RECIBIDOS", True, 8
        Coloca Cx + 12.5, Cy, "GASTOS POR CAJA", True, 8
        Cy = Cy + 0.4
        pedeta = Cy
        Sp_ChNro = "Nro"
        Sp_ChBanco = "Banco"
        RSet Sp_ChValor = "Valor"
        Sp_DetGasto = "Detalle"
        RSet Sp_DetGastoValor = "Valor"
        
        Coloca Cx, Cy, Sp_ChNro & " " & Sp_ChBanco & " " & Sp_ChValor, False, 8
        Coloca Cx + 11, Cy, Sp_DetGasto & " " & Sp_DetGastoValor, False, 8
        'Coloca Cx + 12.5, Cy, "GASTOS POR CAJA", True, 8
        
        Cy = Cy + Dp_S
        For i = 1 To LvCheques.ListItems.Count
            Sp_ChNro = LvCheques.ListItems(i).SubItems(1)
            Sp_ChBanco = LvCheques.ListItems(i).SubItems(2)
            RSet Sp_ChValor = LvCheques.ListItems(i).SubItems(3)
            Coloca Cx, Cy, Sp_ChNro & " " & Sp_ChBanco & " " & Sp_ChValor, False, 8
            Cy = Cy + Dp_S
        Next i
        finch = Cy
        Sp_ChNro = ""
        RSet Sp_ChBanco = "TOT. CHEQUES"
        RSet Sp_ChValor = TxtTotalCheques
        Coloca Cx, Cy, Sp_ChNro & " " & Sp_ChBanco & " " & Sp_ChValor, True, 8
        
        Cy = pedeta
                
        Cy = Cy + Dp_S
            
            For i = 1 To LvGastos.ListItems.Count
                Sp_DetGasto = LvGastos.ListItems(i).SubItems(1)
                RSet Sp_DetGastoValor = LvGastos.ListItems(i).SubItems(2)
                Coloca Cx + 11, Cy, Sp_DetGasto & " " & Sp_DetGastoValor, False, 8
                Cy = Cy + Dp_S
            Next i
            RSet Sp_DetGasto = "TOTAL GASTOS"
            RSet Sp_DetGastoValor = txtTotalGasto
            Coloca Cx + 11, Cy, Sp_DetGasto & " " & Sp_DetGastoValor, True, 8
            If Cy > finch Then finch = Cy
            
            
        'Agregar detalle de billets ingresados
        Dim Sp_ValorBillete As String * 6
        Dim Sp_CantidadBilletes As String * 4
        Dim Sp_TotalBilletes As String * 10
        
        finch = finch + Dp_S + Dp_S
        Cy = finch + Dp_S + Dp_S
        iniciobilletes = finch + Dp_S + Dp_S
        finbilletes = Cy
        
        Coloca Cx, Cy, "Detalle de Billetes", True, 8
        Cy = Cy + Dp_S
        RSet Sp_ValorBillete = "Valor"
        RSet Sp_CantidadBilletes = "Cant."
        RSet Sp_TotalBilletes = "Total"
        Coloca Cx, Cy, Sp_ValorBillete & " " & Sp_CantidadBilletes & " " & Sp_TotalBilletes, True, 8
        Cy = Cy + Dp_S
         For i = 1 To LvBilletes.ListItems.Count
                RSet Sp_ValorBillete = LvBilletes.ListItems(i).SubItems(1)
                RSet Sp_CantidadBilletes = LvBilletes.ListItems(i).SubItems(2)
                RSet Sp_TotalBilletes = LvBilletes.ListItems(i).SubItems(3)
                Coloca Cx, Cy, Sp_ValorBillete & " " & Sp_CantidadBilletes & " " & Sp_TotalBilletes, False, 8
                
                Cy = Cy + Dp_S
        Next i
        RSet Sp_ValorBillete = "TOTAL"
        RSet Sp_CantidadBilletes = "BILLETES"
        RSet Sp_TotalBilletes = TxtTBilletes
        Coloca Cx, Cy, Sp_ValorBillete & " " & Sp_CantidadBilletes & " " & Sp_TotalBilletes, True, 8
        If Cy > finbilletes Then finbilletes = Cy
        
        finbilletes = finbilletes + Dp_S + Dp_S
        
            
       ' Printer.DrawMode = 1
        Printer.DrawWidth = 3
        
        Printer.Line (1.4, 1)-(19, fincierra), , B 'Rectangulo Encabezado
        
        Printer.Line (1.4, fincierra)-(19, finr), , B 'ingressos
        Printer.Line (1.4 + 5.8, fincierra)-(12.3, finr), , B 'ingressos
        
    
        If Me.LvDetalleDoc.ListItems.Count > 0 Then Printer.Line (1.4, finr)-(19, Finde), , B       'detalle ventas
        If Me.LvDetalleCtaCte.ListItems.Count > 0 Then Printer.Line (1.4, Finde)-(19, finc), , B        'detalle ingresos ctas ctes
        Printer.Line (1.4, inich)-(19, finch), , B 'Cheques y gastos
        
        Printer.Line (1.4, iniciobilletes)-(19, finbilletes), , B 'Detalle de billets
        
        Printer.NewPage
        Printer.EndDoc
        Sp_Valor = 0
        Lp_SoloContado = 0
        
    Next
    Exit Sub
ErroComp:
    MsgBox "Error :" & Err.Description & vbNewLine & "Nro " & Err.Number & vbNewLine & "Lin:" & Err.Source
End Sub
Private Sub ImprimeCierrePlasticosAldunate()
    Dim Lp_SoloContado As Long
    Dim Cx As Double, Cy As Double, Sp_Fecha As String, Dp_S As Double, Sp_Letras As String
    Dim Sp_Valor As String * 10, Sp_Fpago As String * 12
    Dim Sp_Valor1 As String * 50, Sp_Fpago1 As String * 50
    
    Dim Sp_FpagoR As String * 15
    Dim Sp_Femision As String * 11
    Dim Sp_Fvenc As String * 11
    Dim Sp_Doc As String * 4
    Dim Sp_Nro As String * 9
    Dim Sp_Cliente As String * 35
    
    Dim Sp_Empresa As String
    Dim Sp_Giro As String
    Dim Sp_Direccion As String
    Printer.FontName = "Courier New"
    ''
    Dim Sp_Lh As String
    Dim Lp_Contador As Long
    Dim Ip_LineasPorPaginas As Integer
    
    On Error GoTo ErroComp
    Ip_LineasPorPaginas = 55
    Cx = 2
    Cy = 1
    For i = 1 To 147
        Sp_Lh = Sp_Lh & "="
    Next
    
    Dp_S = 0.3
    For f = 1 To Dialogo.Copies
        On Error GoTo ErroComp
        Sql = "SELECT giro,direccion,ciudad " & _
             "FROM sis_empresas " & _
             "WHERE rut='" & SP_Rut_Activo & "'"
        Consulta RsTmp2, Sql
        If RsTmp2.RecordCount > 0 Then
            Sp_Giro = RsTmp2!giro
            Sp_Direccion = RsTmp2!direccion & " - " & RsTmp2!ciudad
        End If
        Printer.ScaleMode = 7
        Cx = 1.5
        Cy = 1.9
        Dp_S = 0.3
        Printer.CurrentX = Cx
        Printer.CurrentY = Cy
        'Printer.PaintPicture LoadPicture(App.Path & "\IMG\LAFLOR\LAFLOR.jpg"), Cx + 0.62, 1
       ' Printer.PaintPicture LoadPicture(App.Path & "\REDMAROK.jpg"), Cx + 0.62, 1
        Coloca Cx, Cy, SP_Empresa_Activa, False, 10
        Cy = Cy + (Dp_S * 3)
        Coloca Cx + 6, Cy, "ARQUEO DE CAJA", True, 14
        Cy = Cy + 1
        Sp_FpagoR = "SUCURSAL"
        Coloca Cx, Cy, Sp_FpagoR & ":" & Me.txtSucursal, False, 10
        Sp_FpagoR = "APERTURA"
        Coloca Cx, Cy + Dp_S, Sp_FpagoR & ":" & DtFecha, False, 10
        Sp_FpagoR = "CIERRE"
        Coloca Cx, Cy + Dp_S + Dp_S, Sp_FpagoR & ":" & Now, False, 10
        
        Sp_FpagoR = "EFECT. INICIAL"
        Coloca Cx, Cy + Dp_S + Dp_S + Dp_S, Sp_FpagoR & ":" & txtEfectivoInicial, False, 10
        
        Cy = Cy + 3 'interlineas
        fincierra = Cy - 0.2
        Coloca Cx, Cy, "INGRESOS POR VENTAS", True, 8
        Coloca Cx + 6, Cy, "INGRESOS POR CTAS CTES", True, 8
        Coloca Cx + 12.5, Cy, "RESUMEN DE INGRESOS", True, 8
        Cy = Cy + 0.4
        pos1 = Cy
        For i = 1 To Me.LvVentas.ListItems.Count
            Sp_Fpago = LvVentas.ListItems(i).SubItems(1)
            Coloca Cx, Cy, Sp_Fpago, False, 8
            RSet Sp_Valor = LvVentas.ListItems(i).SubItems(2)
            Coloca Cx + 3, Cy, Sp_Valor, False, 8
            Cy = Cy + Dp_S
        Next i
        finr = Cy
        Cy = pos1
        For i = 1 To LvCtaCtes.ListItems.Count
            Sp_Fpago = LvCtaCtes.ListItems(i).SubItems(1)
            Coloca Cx + 6, Cy, Sp_Fpago, False, 8
            RSet Sp_Valor = LvCtaCtes.ListItems(i).SubItems(2)
            Coloca Cx + 6 + 3, Cy, Sp_Valor, False, 8
            Cy = Cy + Dp_S
        Next i
        If Cy > finr Then finr = Cy
        Cy = pos1
        For i = 1 To LvDetalle.ListItems.Count
            Sp_Fpago = LvDetalle.ListItems(i).SubItems(1)
            Coloca Cx + 12.5, Cy, Sp_Fpago, False, 8
            RSet Sp_Valor = LvDetalle.ListItems(i).SubItems(2)
            Coloca Cx + 15.5, Cy, Sp_Valor, False, 8
            Cy = Cy + Dp_S
        Next i
        
        If Cy > finr Then finr = Cy 'Hasta variable es para el final del cuadro de los resumen
        'Imprimir totales
        Cy = finr
        RSet Sp_Fpago = "TOTAL"
      
        RSet Sp_Valor = txtIngVentas
        Coloca Cx, Cy, Sp_Fpago, True, 8
        Coloca Cx + 3, Cy, Sp_Valor, True, 8
        RSet Sp_Valor = txtIngCtaCtes
        Coloca Cx + 6, Cy, Sp_Fpago, True, 8
        Coloca Cx + 9, Cy, Sp_Valor, True, 8
        RSet Sp_Valor = txtTotal
        Coloca Cx + 12.5, Cy, Sp_Fpago, True, 8
        Coloca Cx + 15.5, Cy, Sp_Valor, True, 8
        Cy = Cy + Dp_S
        finr = Cy + 0.2
        
                        Dim sacachequectacte As String
                sacachequectacte = 0
        
       'Ahora el detalle DE PAGOS POR CTA CTE
        ''Cy = finr + Dp_S * 2
        Cy = finr
        If LvDetalleCtaCte.ListItems.Count > 0 Then
            With LvDetalleCtaCte
                Sp_FpagoR = "F.pago"
                Sp_Femision = "Emision"
                Sp_Fvenc = "Vence"
                Sp_Doc = "Doc"
                Sp_Nro = "Numero"
                Sp_Cliente = "Cliente"
                RSet Sp_Valor = "Valor"
                Cy = finr
                Coloca Cx, Cy, "DETALLE INGRESOS POR CTA CTE", True, 10
                Cy = Cy + Dp_S
                Coloca Cx, Cy, Sp_FpagoR & " " & Sp_Femision & " " & Sp_Fvenc & " " & Sp_Doc & " " & Sp_Nro & " " & Sp_Cliente & " " & Sp_Valor, True, 8
                Cy = Cy + Dp_S
                For i = 1 To .ListItems.Count
                    Sp_FpagoR = .ListItems(i)
                    Sp_Femision = .ListItems(i).SubItems(1)
                    Sp_Fvenc = .ListItems(i).SubItems(2)
                    Sp_Doc = .ListItems(i).SubItems(3)
                    Sp_Nro = .ListItems(i).SubItems(4)
                    Sp_Cliente = .ListItems(i).SubItems(5)
                    RSet Sp_Valor = .ListItems(i).SubItems(6)
                    Coloca Cx, Cy, Sp_FpagoR & " " & Sp_Femision & " " & Sp_Fvenc & " " & Sp_Doc & " " & Sp_Nro & " " & Sp_Cliente & " " & Sp_Valor, False, 8
                    Cy = Cy + Dp_S
                Next
'                Dim sacachequectacte As String
'                sacachequectacte = 0
                
                If Trim(Sp_FpagoR) = "CHEQUE" Then
                sacachequectacte = Sp_Valor
                Else
                sacachequectacte = 0
                End If
                
                Sp_FpagoR = ""
                Sp_Femision = ""
                Sp_Fvenc = ""
                Sp_Doc = ""
                Sp_Nro = ""
                RSet Sp_Cliente = "TOTAL"
                RSet Sp_Valor = txtTotalCtaCte
                Dim ctacte As String
                ctacte = 0
                ctacte = Sp_Valor
                Coloca Cx, Cy, Sp_FpagoR & " " & Sp_Femision & " " & Sp_Fvenc & " " & Sp_Doc & " " & Sp_Nro & " " & Sp_Cliente & " " & Sp_Valor, True, 8
                finc = Cy + Dp_S
            End With
        End If
        Cy = Cy + 1
        inich = Cy - 0.2
        
        
        'Ahora en detalle de gastos y el detalle de cheques recibidos
        Dim Sp_ChNro As String * 12
        Dim Sp_ChBanco As String * 12
        Dim Sp_ChValor As String * 12
        Dim Sp_DetGasto As String * 25
        Dim Sp_DetGastoValor As String * 10
        Coloca Cx, Cy, "CHEQUES RECIBIDOS", True, 8
        Coloca Cx + 12.5, Cy, "GASTOS POR CAJA", True, 8
        Cy = Cy + 0.4
        pedeta = Cy
        Sp_ChNro = "Nro"
        Sp_ChBanco = "Banco"
        Sp_ChFecha = "Fecha"
        RSet Sp_ChValor = "Valor"
        
        Sp_DetGasto = "Detalle"
        RSet Sp_DetGastoValor = "Valor"
        
        Coloca Cx, Cy, Sp_ChNro & " " & Sp_ChBanco & " " & Sp_ChValor & " " & Sp_ChFecha, False, 8
        Coloca Cx + 11, Cy, Sp_DetGasto & " " & Sp_DetGastoValor, False, 8
        'Coloca Cx + 12.5, Cy, "GASTOS POR CAJA", True, 8
        
        Cy = Cy + Dp_S
        For i = 1 To LvCheques.ListItems.Count
            Sp_ChNro = LvCheques.ListItems(i).SubItems(1)
            Sp_ChBanco = LvCheques.ListItems(i).SubItems(2)
            RSet Sp_ChValor = LvCheques.ListItems(i).SubItems(3)
            Sp_ChFecha = LvCheques.ListItems(i).SubItems(4)
            Coloca Cx, Cy, Sp_ChNro & " " & Sp_ChBanco & " " & Sp_ChValor & " " & Sp_ChFecha, False, 8
            Cy = Cy + Dp_S
        Next i
        finch = Cy
        Sp_ChNro = ""
        RSet Sp_ChBanco = "TOT. CHEQUES"
        RSet Sp_ChValor = TxtTotalCheques
        Coloca Cx, Cy, Sp_ChNro & " " & Sp_ChBanco & " " & Sp_ChValor, True, 8
        
        Cy = pedeta
                
        Cy = Cy + Dp_S
            
            For i = 1 To LvGastos.ListItems.Count
                Sp_DetGasto = LvGastos.ListItems(i).SubItems(1)
                RSet Sp_DetGastoValor = LvGastos.ListItems(i).SubItems(2)
                Coloca Cx + 11, Cy, Sp_DetGasto & " " & Sp_DetGastoValor, False, 8
                Cy = Cy + Dp_S
            Next i
            RSet Sp_DetGasto = "TOTAL GASTOS"
            RSet Sp_DetGastoValor = txtTotalGasto
            Coloca Cx + 11, Cy, Sp_DetGasto & " " & Sp_DetGastoValor, True, 8
            If Cy > finch Then finch = Cy
            
            
        'Agregar detalle de billets ingresados
        Dim Sp_ValorBillete As String * 6
        Dim Sp_CantidadBilletes As String * 4
        Dim Sp_TotalBilletes As String * 10
        
        finch = finch + Dp_S + Dp_S
        Cy = finch + Dp_S + Dp_S
        iniciobilletes = finch + Dp_S + Dp_S
        finbilletes = Cy
        
        Coloca Cx, Cy, "Detalle de Billetes", True, 8
        Cy = Cy + Dp_S
        RSet Sp_ValorBillete = "Valor"
        RSet Sp_CantidadBilletes = "Cant."
        RSet Sp_TotalBilletes = "Total"
        Coloca Cx, Cy, Sp_ValorBillete & " " & Sp_CantidadBilletes & " " & Sp_TotalBilletes, True, 8
        Cy = Cy + Dp_S
         For i = 1 To LvBilletes.ListItems.Count
                RSet Sp_ValorBillete = LvBilletes.ListItems(i).SubItems(1)
                RSet Sp_CantidadBilletes = LvBilletes.ListItems(i).SubItems(2)
                RSet Sp_TotalBilletes = LvBilletes.ListItems(i).SubItems(3)
                Coloca Cx, Cy, Sp_ValorBillete & " " & Sp_CantidadBilletes & " " & Sp_TotalBilletes, False, 8
                
                Cy = Cy + Dp_S
        Next i
        RSet Sp_ValorBillete = "TOTAL"
        RSet Sp_CantidadBilletes = "BILLETES"
        RSet Sp_TotalBilletes = TxtTBilletes
        Coloca Cx, Cy, Sp_ValorBillete & " " & Sp_CantidadBilletes & " " & Sp_TotalBilletes, True, 8
        If Cy > finbilletes Then finbilletes = Cy
        
        finbilletes = finbilletes + Dp_S + Dp_S
        


        
        
        
        ''Sacar Cheque al dia
        Dim totalx As String * 30
        Dim totald As String * 30
        Dim Sp_ValorEfectivo As String * 20
        Dim Credito As String * 20 'Credito
        
        If LvVentas.ListItems.Count = 5 Then
        ''If LvVentas.ListItems.Count = 3 Then
        Credito = LvVentas.ListItems(5).SubItems(2)
        Else
        Credito = 0
        End If
        
        Dim fechas As String
        Dim fechas1 As String
        Dim valorch As Long
        Dim valoTotalrch As String
        valorch = 0
        valoTotalrch = 0
        Dim valorchFe As Long
        Dim valoTotalrchFe As String
        valorchFe = 0
        valoTotalrchFe = 0
        
    ''    Dim fechas1 As String
        fechas = Trim(Fqls(DtFecha))
        
        For i = 1 To LvCheques.ListItems.Count
            Sp_ChNro = LvCheques.ListItems(i).SubItems(1)
            Sp_ChBanco = LvCheques.ListItems(i).SubItems(2)
            RSet Sp_ChValor = LvCheques.ListItems(i).SubItems(3)
             Sp_ChFecha = LvCheques.ListItems(i).SubItems(4)
        ''    fechas1 = Sp_ChFecha
        ''cheque al dia
            If Sp_ChFecha <= fechas Then
        ''    Cy = Cy + 1
        ''    Coloca Cx, Cy, Sp_ChNro & " " & Sp_ChBanco & " " & Sp_ChValor & " " & Sp_ChFecha, False, 8
            valoTotalrch = NumFormat(CDbl(Sp_ChValor) + valoTotalrch)
        ''    Coloca Cx + 10, Cy + 5, "Total Cheque al Dia", True, 8
        ''    Coloca Cx + 10, Cy + 5, valoTotalrch, True, 8
            Else ''Cheque a Fecha
            valoTotalrchFe = NumFormat(CDbl(Sp_ChValor) + valoTotalrchFe)
          ''  Coloca Cx, Cy, Sp_ChNro & " " & Sp_ChBanco & " " & Sp_ChValor & " " & Sp_ChFecha, False, 8
            End If
        Next i
        finch = Cy
        Sp_ChNro = ""
        RSet Sp_ChBanco = "TOT. CHEQUES"
        RSet Sp_ChValor = TxtTotalCheques
        
        'MSG
        finIe = Cy * 2
        Cy = Cy * 2
        Cy = finbilletes + 1
        Coloca Cx, Cy, "*******************************************************************************************************", True, 8
        Coloca Cx, Cy + 0.2, "INGRESOS EXTRAS", True, 8
        If Cy > finIe Then finIe = Cy
        Cy = Cy + 0.5
        For i = 1 To LvIngresos.ListItems.Count
            Sp_Fpago1 = LvIngresos.ListItems(i).SubItems(1)
            Coloca Cx, Cy, Sp_Fpago1, False, 8
            RSet Sp_Valor1 = LvIngresos.ListItems(i).SubItems(2)
            Coloca Cx + 3, Cy, Sp_Valor1, False, 8
            Cy = Cy + Dp_S
        Next i

      ''  Coloca Cx, Cy, Sp_ChNro & " " & Sp_ChBanco & " " & Sp_ChValor, True, 8
        Coloca Cx, Cy + 0.2, " TOTAL INGRESOS EXTRAS", True, 8
        Coloca Cx + 10, Cy + 0.2, TxtTotalIngresos, True, 8
        
        Coloca Cx, Cy + 0.7, "( Total Ing. + Total Ing. Extras + Total Efectivo - Gastos - Cheque a Fecha - Factura a Credito)", False, 8
        Coloca Cx, Cy + 1, "TOTAL CAJA", True, 8
        ''Termina sacar cheque a fecha
        
        ''Suma el efectivo + tarjeta de creidto + redcompra
    
       
        Dim Credito1 As String * 20 'Credito
        If LvVentas.ListItems.Count > 2 Then
        Credito1 = LvVentas.ListItems(3).SubItems(2)
        Else
        Credito1 = 0
        End If
        
        Dim efectivo As String * 20 'efectivo
        If LvVentas.ListItems.Count > 0 Then
        efectivo = LvVentas.ListItems(1).SubItems(2)
        Else
        efectivo = 0
        End If
        
        Dim tarjeta As String * 20 'tarjeta
        If LvVentas.ListItems.Count >= 4 Then
        tarjeta = LvVentas.ListItems(4).SubItems(2)
        Else
        tarjeta = 0
        End If
                
        
       '' Sp_ValorEfectivo = (CDbl(LvVentas.ListItems(1).SubItems(2))) + (CDbl(Credito1)) + (CDbl(LvVentas.ListItems(4).SubItems(2)))
        Sp_ValorEfectivo = NumFormat(CDbl(efectivo) + CDbl(Credito1) + CDbl(tarjeta))
    '''    totalx = NumFormat(Sp_ValorEfectivo + CDbl(TxtTotalIngresos) - CDbl(Sp_DetGastoValor))
       totalx = NumFormat(Sp_ValorEfectivo + CDbl(TxtTotalIngresos) - CDbl(Sp_DetGastoValor) - CDbl(Credito) - valoTotalrchFe) '' - Credito1 - valoTotalrchFe)
        Coloca Cx + 10, Cy + 1, totalx, True, 8
        'MSG
        ''TOTAL A DEPOSITAR
        Coloca Cx, Cy + 2, "( Ing. Cta. Cte. + Total Ing. Extras + Total Efectivo + Cheque al dia + TC + RedCompra)", False, 8
       ''  Dim xxxx As String
       ''  xxxx = NumFormat(Sp_ValorEfectivo)
         totald = NumFormat(CDbl(Sp_ValorEfectivo) + CDbl(TxtTotalIngresos) + valoTotalrch + txtTotalCtaCte - CDbl(sacachequectacte))
        ''totald = NumFormat(CDbl(Sp_Valor) + CDbl(TxtTotalIngresos) + CDbl(txtTotalCtaCte) + CDbl(txtTotalCtaCte))
        Coloca Cx, Cy + 2.5, "TOTAL A DEPOSITAR", True, 8
        Coloca Cx + 10, Cy + 2.5, totald, True, 8
            
        Printer.DrawMode = 1
        Printer.DrawWidth = 3
        Printer.Line (1.4, 1)-(19, fincierra), , B 'Rectangulo Encabezado
        Printer.Line (1.4, fincierra)-(19, finr), , B 'ingressos
        Printer.Line (1.4 + 5.8, fincierra)-(12.3, finr), , B 'ingressos
   ''     If Me.LvDetalleDoc.ListItems.Count > 0 Then Printer.Line (1.4, finr)-(19, Finde), , B       'detalle ventas
   ''     If Me.LvDetalleCtaCte.ListItems.Count > 0 Then Printer.Line (1.4, Finde)-(19, finc), , B        'detalle ingresos ctas ctes
     Printer.Line (1.4, inich)-(19, finch), , B 'Cheques y gastos
    Printer.Line (1.4, iniciobilletes)-(19, finbilletes), , B 'Detalle de billets
    Printer.Line (1.4, finbilletes + 1)-(19, fincalculototal + 1), , B 'Calculo Final
        fincalculototal = Cy + 1
     Printer.Line (1.4, finbilletes + 1)-(19, fincalculototal + 2), , B 'Calculo Final
     Coloca Cx, Cy + 3, "Informacion Adicional", True, 8
        Printer.Line (1.4, finbilletes + 2)-(19, fincalculototal + 4), , B 'Calculo Final
        
        
        
        Printer.NewPage
                 Cx = 2
         Cy = 1
        Coloca Cx, Cy, SP_Empresa_Activa, False, 10

        'Ahora el detalle
        With LvDetalleDoc
            Sp_FpagoR = "F.pago"
            Sp_Femision = "Emision"
            Sp_Fvenc = "Vence"
            Sp_Doc = "Doc"
            Sp_Nro = "Numero"
            Sp_Cliente = "Cliente"
            RSet Sp_Valor = "Valor"
          ''  Cy = finr + 0.4
           Cy = 5
            Coloca Cx, Cy, "DETALLE DE VENTAS", True, 10
            Cy = Cy + Dp_S
            Coloca Cx, Cy, Sp_FpagoR & " " & Sp_Femision & " " & Sp_Fvenc & " " & Sp_Doc & " " & Sp_Nro & " " & Sp_Cliente & " " & Sp_Valor, True, 8
            Cy = Cy + Dp_S
            Dim TotalFacturas As String * 20 'TotalFacturas
            TotalFacturas = 0
            For i = 1 To .ListItems.Count

''Facturas
                    If .ListItems(i).SubItems(3) <> "BOM" And .ListItems(i).SubItems(3) <> "BOF" Then
                        Sp_FpagoR = .ListItems(i)
                        Sp_Femision = .ListItems(i).SubItems(1)
                        Sp_Fvenc = .ListItems(i).SubItems(2)
                        Sp_Doc = .ListItems(i).SubItems(3)
                        Sp_Nro = .ListItems(i).SubItems(4)
                        If .ListItems(i).SubItems(7) = "11.111.111-1" Then
                            Sp_Cliente = ""
                        Else
                            Sp_Cliente = .ListItems(i).SubItems(5)
                        End If
                        RSet Sp_Valor = .ListItems(i).SubItems(6)
                         TotalFacturas = NumFormat(CDbl(TotalFacturas) + Sp_Valor)
                         Trim (TotalFacturas)
                        Coloca Cx, Cy, Sp_FpagoR & " " & Sp_Femision & " " & Sp_Fvenc & " " & Sp_Doc & " " & Sp_Nro & " " & Sp_Cliente & " " & Sp_Valor, False, 8
                        Cy = Cy + Dp_S
                       
                    End If
                 ''   Coloca Cx, Cy, "Total Facturas 1 :" & " " & TotalFacturas, True, 8
            Next
            Coloca Cx + 12, Cy + 0.1, "Total Facturas : (" & " " & Trim(TotalFacturas) & ")", True, 8
            If SP_Rut_Activo = "76.369.600-6" Then
            Else
             ''  Coloca Cx, Cy, "Total Facturas 3:" & " " & TotalFacturas, True, 8
                For i = 1 To .ListItems.Count
                    If .ListItems(i).SubItems(3) = "BOM" Then
                        Sql = "SELECT CONCAT('DE ',CAST(MIN(no_documento) AS CHAR),' A ',CAST(max(no_documento) AS CHAR)) boletas,SUM(bruto) venta " & _
                                "FROM ven_doc_venta " & _
                                "JOIN sis_documentos USING(doc_id) " & _
                                "WHERE caj_id=" & Lp_IdMiCaja & " AND doc_nombre='BOLETA'"
                        Consulta RsTmp, Sql
                        Sp_FpagoR = ""
                        Sp_Femision = ""
                        Sp_Fvenc = ""
                        Sp_Doc = "BOM"
                        Sp_Nro = ""
                        Sp_Cliente = RsTmp!boletas
                        RSet Sp_Valor = NumFormat(RsTmp!venta)
                        Coloca Cx, Cy + 0.1, Sp_FpagoR & " " & Sp_Femision & " " & Sp_Fvenc & " " & Sp_Doc & " " & Sp_Nro & " " & Sp_Cliente & " " & Sp_Valor, False, 8
                        Cy = Cy + Dp_S
                        Exit For
                    End If
                Next

                For i = 1 To .ListItems.Count
                    If .ListItems(i).SubItems(3) = "BOF" Then
                        Sql = "SELECT CONCAT('DE ',CAST(MIN(no_documento) AS CHAR),' A ',CAST(max(no_documento) AS CHAR)) boletas,SUM(bruto) venta " & _
                                "FROM ven_doc_venta " & _
                                "JOIN sis_documentos USING(doc_id) " & _
                                "WHERE caj_id=" & Lp_IdMiCaja & " AND doc_nombre='BOLETA FISCAL'"
                        Consulta RsTmp, Sql
                        Sp_FpagoR = ""
                        Sp_Femision = ""
                        Sp_Fvenc = ""
                        Sp_Doc = "BOF"
                        Sp_Nro = ""
                        Sp_Cliente = RsTmp!boletas
                        RSet Sp_Valor = NumFormat(RsTmp!venta)
                        Coloca Cx, Cy + 0.6, Sp_FpagoR & " " & Sp_Femision & " " & Sp_Fvenc & " " & Sp_Doc & " " & Sp_Nro & " " & Sp_Cliente & " " & Sp_Valor, False, 8
                        Cy = Cy + Dp_S
                        Exit For
                    End If
                Next

            End If

            Sp_FpagoR = ""
            Sp_Femision = ""
            Sp_Fvenc = ""
            Sp_Doc = ""
            Sp_Nro = ""
            RSet Sp_Cliente = "TOTAL"

            RSet Sp_Valor = txtTotalDoc

            
            Coloca Cx, Cy + 1, Sp_FpagoR & " " & Sp_Femision & " " & Sp_Fvenc & " " & Sp_Doc & " " & Sp_Nro & " " & Sp_Cliente & " " & Sp_Valor, True, 8
            ''Coloca Cx, Cy + 2, Sp_FpagoR & " " & Sp_Femision & " " & Sp_Fvenc & " " & Sp_Doc & " " & Sp_Nro & " " & Factura & " " & TotalFacturas, True, 8
            

            Finde = Cy + Dp_S
        End With
      ''  Coloca Cx, Cy, "Total Facturas :",& TotalFacturas, True, 10
        
       '' Coloca Cx, Cy + 2, Sp_FpagoR & " " & Sp_Femision & " " & Sp_Fvenc & " " & Sp_Doc & " " & Sp_Nro & " " & Factura & " " & TotalFacturas, True, 8
        
    ''    LvParaImprimir.ListItems.Clear
'''        ''MSG
'''    On Error GoTo ErroComp
'''    Ip_LineasPorPaginas = 53
'''    Cx = 1
'''    Cy = 1
'''
'''            Dim avance As Double
'''        avance = 1.5
'''        Printer.FontSize = 10
'''        Printer.FontName = "Arial Narrow"
'''        Cx = 0
'''        Cy = 0
'''
'''
'''    For i = 1 To 147
'''        Sp_Lh = Sp_Lh & "="
'''    Next
'''
'''        Lp_Contador = 1
'''        Im_Paginas = 1
'''        Ip_Paginas = Int(LvDetalleDoc.ListItems.Count / Ip_LineasPorPaginas)
'''        If (LvDetalleDoc.ListItems.Count Mod Ip_LineasPorPaginas) > 0 Then
'''            Ip_Paginas = Ip_Paginas + 1
'''        End If
'''     ''   ResumenEncabezadoLaser
'''        Cy = Cy + Dp + 1
'''
'''        For i = 1 To LvDetalleDoc.ListItems.Count
'''            P_Fecha = LvDetalleDoc.ListItems(i).SubItems(1)
'''            P_Documento = LvDetalleDoc.ListItems(i).SubItems(11)
'''            P_Numero = LvDetalleDoc.ListItems(i).SubItems(4)
'''            P_Cliente = LvDetalleDoc.ListItems(i).SubItems(5)
'''            P_Rut = LvDetalleDoc.ListItems(i).SubItems(7)
'''            RSet P_Venta = LvDetalleDoc.ListItems(i).SubItems(6)
'''            RSet P_Neto = LvDetalleDoc.ListItems(i).SubItems(9)
'''            RSet P_Iva = LvDetalleDoc.ListItems(i).SubItems(10)
'''            P_Pago = LvDetalleDoc.ListItems(i)
'''            If Mid(P_Cliente, 1, 5) = "TOTAL" Then
'''                P_Pago = ""
'''                EnviaLineasC True
'''            Else
'''        Cy = Cy + 0.2
'''        Coloca Cx, Cy, P_Fecha, True, 10
'''        Coloca Cx + 1.7, Cy, P_Documento, True, 10
'''        Coloca Cx + 4.9, Cy, P_Numero, True, 10
'''        Coloca Cx + 6.2, Cy, P_Rut, True, 10
'''        Coloca Cx + 8.4, Cy, P_Cliente, True, 10
'''        Coloca (Cx + 14.2) - Printer.TextWidth(P_Neto), Cy, P_Neto, True, 10
'''        Coloca (Cx + 15.4) - Printer.TextWidth(P_Iva), Cy, P_Iva, True, 10
'''        Coloca (Cx + 17) - Printer.TextWidth(P_Venta), Cy, P_Venta, True, 10
'''        Coloca Cx + 17.5, Cy, P_Pago, True, 10
'''          ''      EnviaLineasC False
'''            End If
'''            Cy = Cy + Dp
'''            Lp_Contador = Lp_Contador + 1
'''            If Lp_Contador = Ip_LineasPorPaginas Then
'''                Printer.NewPage
'''                Im_Paginas = Im_Paginas + 1
'''             ''   ResumenEncabezadoLaser
'''                Cy = Cy + Dp
'''                Lp_Contador = 1
'''            End If
'''        Next
'''        ''MSG
        
        Printer.NewPage
        Printer.EndDoc
        Sp_Valor = 0
        Lp_SoloContado = 0
        
    Next
    Exit Sub
ErroComp:
    MsgBox "Error :" & Err.Description & vbNewLine & "Nro " & Err.Number & vbNewLine & "Lin:" & Err.Source
End Sub



Private Sub txtCantBillete_Change()
    If DG_ID_Unico > 0 Then Exit Sub
        If Val(txtCantBillete) = 0 Then
            txtTotalBillete = 0
            Exit Sub
        End If
       txtTotalBillete = CDbl(TxTValorBillete) * Val(txtCantBillete)
End Sub

Private Sub txtCantBillete_GotFocus()
    En_Foco txtCantBillete
End Sub

Private Sub txtCantBillete_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then CmdOkDetBillete_Click
End Sub

Private Sub TxtGasto_GotFocus()
    En_Foco TxtGasto
End Sub

Private Sub TxtGasto_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub



Private Sub TxtIngreso_GotFocus()
    En_Foco TxtIngreso
End Sub

Private Sub TxtIngreso_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub TxtMontoDeposito_Change()
    If Len(TxtMontoDeposito) > 3 Then
        TxtMontoDeposito = NumFormat(TxtMontoDeposito)
        TxtMontoDeposito.SelStart = Len(TxtMontoDeposito)
    End If
End Sub

Private Sub TxtMontoDeposito_GotFocus()
    En_Foco TxtMontoDeposito
End Sub

Private Sub TxtMontoDeposito_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
End Sub

Private Sub TxtMontoDeposito_Validate(Cancel As Boolean)
    If Val(TxtMontoDeposito) = 0 Then
        TxtMontoDeposito = 0
    Else
        TxtMontoDeposito = NumFormat(TxtMontoDeposito)
    End If
    LLenaArqueo
        
        
End Sub

Private Sub txtSucursal_Validate(Cancel As Boolean)
txtSucursal = Replace(txtSucursal, "'", "")
End Sub

Private Sub txtTotalBillete_GotFocus()
    En_Foco txtTotalBillete
End Sub

Private Sub txtValorGasto_Change()
    If Len(txtValorGasto) > 3 Then
        txtValorGasto = NumFormat(txtValorGasto)
        txtValorGasto.SelStart = Len(txtValorGasto)
    End If
End Sub

Private Sub txtValorGasto_GotFocus()
    En_Foco txtValorGasto
End Sub

Private Sub txtValorGasto_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
  '  If Len(txtValorGasto) > 2 Then
  '
  '      txtValorGasto = NumFormat(txtValorGasto)
   ' End If
End Sub

Private Sub txtValorIngreso_Change()
    If Len(txtValorIngreso) > 3 Then
        txtValorIngreso = NumFormat(txtValorIngreso)
        txtValorIngreso.SelStart = Len(txtValorIngreso)
    End If
End Sub

Private Sub txtValorIngreso_GotFocus()
    En_Foco txtValorIngreso
End Sub

Private Sub txtValorIngreso_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
   ' txtValorIngreso = NumFormat(txtValorIngreso)
End Sub
Private Sub ImprimeCartola()
    Dim Sp_Texto As String
    Dim Sp_Lh As String
    Dim Lp_Contador As Long
    Dim Ip_LineasPorPaginas As Integer
    
    'Antes que nada crearemos una copia de la grilla para / 12-10-15 /
    'debemos separa los documentos de ventas
    If LvDetalleDoc.ListItems.Count = 0 Then Exit Sub
    docinicial = LvDetalleDoc.ListItems(1).SubItems(11)
    montoinicial = 0
    montoinicialiva = 0
    montoinicialneto = 0
    LvParaImprimir.ListItems.Clear
    For i = 1 To LvDetalleDoc.ListItems.Count
        If docinicial <> LvDetalleDoc.ListItems(i).SubItems(11) Then
            P_Cliente = "TOTAL " & docinicial
            RSet P_Venta = NumFormat(montoinicial)
            RSet P_Neto = NumFormat(montoinicialneto)
            RSet P_Iva = NumFormat(montoinicialiva)
          
            
            Me.LvParaImprimir.ListItems.Add , , LvDetalleDoc.ListItems(i)
            LvParaImprimir.ListItems(LvParaImprimir.ListItems.Count).SubItems(5) = P_Cliente
            LvParaImprimir.ListItems(LvParaImprimir.ListItems.Count).SubItems(6) = P_Venta
            LvParaImprimir.ListItems(LvParaImprimir.ListItems.Count).SubItems(9) = P_Neto
            LvParaImprimir.ListItems(LvParaImprimir.ListItems.Count).SubItems(10) = P_Iva
            LvParaImprimir.ListItems(LvParaImprimir.ListItems.Count).SubItems(11) = ""
            
            montoinicial = 0
            montoinicialiva = 0
            montoinicialneto = 0
            
            Me.LvParaImprimir.ListItems.Add , , ""
            docinicial = LvDetalleDoc.ListItems(i).SubItems(11)
            
            
            montoinicial = montoinicial + CDbl(LvDetalleDoc.ListItems(i).SubItems(6))
            montoinicialiva = montoinicialiva + CDbl(LvDetalleDoc.ListItems(i).SubItems(10))
            montoinicialneto = montoinicialneto + CDbl(LvDetalleDoc.ListItems(i).SubItems(9))
            
            Me.LvParaImprimir.ListItems.Add , , LvDetalleDoc.ListItems(i)
            LvParaImprimir.ListItems(LvParaImprimir.ListItems.Count).SubItems(1) = LvDetalleDoc.ListItems(i).SubItems(1)
            LvParaImprimir.ListItems(LvParaImprimir.ListItems.Count).SubItems(2) = LvDetalleDoc.ListItems(i).SubItems(2)
            LvParaImprimir.ListItems(LvParaImprimir.ListItems.Count).SubItems(3) = LvDetalleDoc.ListItems(i).SubItems(3)
            LvParaImprimir.ListItems(LvParaImprimir.ListItems.Count).SubItems(4) = LvDetalleDoc.ListItems(i).SubItems(4)
            LvParaImprimir.ListItems(LvParaImprimir.ListItems.Count).SubItems(5) = LvDetalleDoc.ListItems(i).SubItems(5)
            LvParaImprimir.ListItems(LvParaImprimir.ListItems.Count).SubItems(6) = LvDetalleDoc.ListItems(i).SubItems(6)
            LvParaImprimir.ListItems(LvParaImprimir.ListItems.Count).SubItems(7) = LvDetalleDoc.ListItems(i).SubItems(7)
            LvParaImprimir.ListItems(LvParaImprimir.ListItems.Count).SubItems(8) = LvDetalleDoc.ListItems(i).SubItems(8)
            LvParaImprimir.ListItems(LvParaImprimir.ListItems.Count).SubItems(9) = LvDetalleDoc.ListItems(i).SubItems(9)
            LvParaImprimir.ListItems(LvParaImprimir.ListItems.Count).SubItems(10) = LvDetalleDoc.ListItems(i).SubItems(10)
            LvParaImprimir.ListItems(LvParaImprimir.ListItems.Count).SubItems(11) = LvDetalleDoc.ListItems(i).SubItems(11)
            
            
            'Totalizar tipo documento.
        Else
            montoinicial = montoinicial + CDbl(LvDetalleDoc.ListItems(i).SubItems(6))
            montoinicialiva = montoinicialiva + CDbl(LvDetalleDoc.ListItems(i).SubItems(10))
            montoinicialneto = montoinicialneto + CDbl(LvDetalleDoc.ListItems(i).SubItems(9))
            
            Me.LvParaImprimir.ListItems.Add , , LvDetalleDoc.ListItems(i)
            LvParaImprimir.ListItems(LvParaImprimir.ListItems.Count).SubItems(1) = LvDetalleDoc.ListItems(i).SubItems(1)
            LvParaImprimir.ListItems(LvParaImprimir.ListItems.Count).SubItems(2) = LvDetalleDoc.ListItems(i).SubItems(2)
            LvParaImprimir.ListItems(LvParaImprimir.ListItems.Count).SubItems(3) = LvDetalleDoc.ListItems(i).SubItems(3)
            LvParaImprimir.ListItems(LvParaImprimir.ListItems.Count).SubItems(4) = LvDetalleDoc.ListItems(i).SubItems(4)
            LvParaImprimir.ListItems(LvParaImprimir.ListItems.Count).SubItems(5) = LvDetalleDoc.ListItems(i).SubItems(5)
            LvParaImprimir.ListItems(LvParaImprimir.ListItems.Count).SubItems(6) = LvDetalleDoc.ListItems(i).SubItems(6)
            LvParaImprimir.ListItems(LvParaImprimir.ListItems.Count).SubItems(7) = LvDetalleDoc.ListItems(i).SubItems(7)
            LvParaImprimir.ListItems(LvParaImprimir.ListItems.Count).SubItems(8) = LvDetalleDoc.ListItems(i).SubItems(8)
            LvParaImprimir.ListItems(LvParaImprimir.ListItems.Count).SubItems(9) = LvDetalleDoc.ListItems(i).SubItems(9)
            LvParaImprimir.ListItems(LvParaImprimir.ListItems.Count).SubItems(10) = LvDetalleDoc.ListItems(i).SubItems(10)
            LvParaImprimir.ListItems(LvParaImprimir.ListItems.Count).SubItems(11) = LvDetalleDoc.ListItems(i).SubItems(11)
        End If
    Next

    If Mid(LvParaImprimir.ListItems(LvParaImprimir.ListItems.Count).SubItems(5), 1, 5) <> "TOTAL" Then
        'Si el ultimo tipo documento no esta totalizado lo totalizamos aqui
            P_Cliente = "TOTAL " & docinicial
            RSet P_Venta = NumFormat(Abs(montoinicial))
            RSet P_Neto = NumFormat(montoinicialneto)
            RSet P_Iva = NumFormat(montoinicialiva)
            P_Pago = ""
            
            Me.LvParaImprimir.ListItems.Add , , LvDetalleDoc.ListItems(LvDetalleDoc.ListItems.Count)
            LvParaImprimir.ListItems(LvParaImprimir.ListItems.Count).SubItems(5) = P_Cliente
            LvParaImprimir.ListItems(LvParaImprimir.ListItems.Count).SubItems(6) = P_Venta
            LvParaImprimir.ListItems(LvParaImprimir.ListItems.Count).SubItems(9) = P_Neto
            LvParaImprimir.ListItems(LvParaImprimir.ListItems.Count).SubItems(10) = P_Iva
            
    
    End If
    

    
    
    
    On Error GoTo ErrorP
    Ip_LineasPorPaginas = 53
    Cx = 1
    Cy = 1
    For i = 1 To 147
        Sp_Lh = Sp_Lh & "="
    Next
    
 
        
        Lp_Contador = 1
        Im_Paginas = 1
        Ip_Paginas = Int(LvParaImprimir.ListItems.Count / Ip_LineasPorPaginas)
        If (LvParaImprimir.ListItems.Count Mod Ip_LineasPorPaginas) > 0 Then
            Ip_Paginas = Ip_Paginas + 1
        End If
        ResumenEncabezadoLaser
        Cy = Cy + Dp
        
        For i = 1 To Me.LvParaImprimir.ListItems.Count
            P_Fecha = LvParaImprimir.ListItems(i).SubItems(1)
            P_Documento = LvParaImprimir.ListItems(i).SubItems(11)
            P_Numero = LvParaImprimir.ListItems(i).SubItems(4)
            P_Cliente = LvParaImprimir.ListItems(i).SubItems(5)
            P_Rut = LvParaImprimir.ListItems(i).SubItems(7)
            RSet P_Venta = LvParaImprimir.ListItems(i).SubItems(6)
            RSet P_Neto = LvParaImprimir.ListItems(i).SubItems(9)
            RSet P_Iva = LvParaImprimir.ListItems(i).SubItems(10)
            P_Pago = LvParaImprimir.ListItems(i)
            If Mid(P_Cliente, 1, 5) = "TOTAL" Then
                P_Pago = ""
                EnviaLineasC True
            Else
                EnviaLineasC False
            End If
            Cy = Cy + Dp
            Lp_Contador = Lp_Contador + 1
            If Lp_Contador = Ip_LineasPorPaginas Then
                Printer.NewPage
                Im_Paginas = Im_Paginas + 1
                ResumenEncabezadoLaser
                Cy = Cy + Dp
                Lp_Contador = 1
            End If
        Next
        PieDeCartola
        Printer.NewPage
        Printer.EndDoc
        'FIN DEL DOCUMETNO"
    Exit Sub
ErrorP:
    MsgBox "Problema al imprimir..." & vbNewLine & Err.Number & vbNewLine & Err.Description & vbNewLine & Err.Source
  

End Sub

Private Sub ResumenEncabezadoLaser()
    Dim Sp_Lh As String, Sp_Tit As String
    Dim Sp_Cetras As String
    
    
    For i = 1 To 115
        Sp_Lh = Sp_Lh & "_"
    Next
    Printer.FontName = "Courier New"
    

    Dp = 0.35
    Cx = 1
    Cy = 1.5
    Coloca Cx + 15, Cy, "P�gina:" & Im_Paginas & " de " & Ip_Paginas, False, 8
    Cy = Cy + Dp
    Coloca Cx + 15, Cy, "Fecha :" & Date, False, 8
    
    Cx = 1
    Cy = 2
   
    
    Cy = Cy + Dp
    'Datos de la empresa
    '2-Abril-2015
    Coloca Cx, Cy, LvEmpresa.ListItems(1).SubItems(1), True, 10 'Nombre empresa
    Cy = Cy + Dp
    Coloca Cx, Cy, LvEmpresa.ListItems(1), False, 10 'rut
    Cy = Cy + Dp
    Coloca Cx, Cy, LvEmpresa.ListItems(1).SubItems(2) & " " & LvEmpresa.ListItems(1).SubItems(4), False, 10 'direccion
    Cy = Cy + Dp
    Coloca Cx, Cy, LvEmpresa.ListItems(1).SubItems(3), False, 10 'giro
    Cy = Cy + Dp
    Coloca Cx, Cy, Sp_Lh, False, 8
    Cy = Cy + Dp
    Coloca Cx, Cy, "                " & "INFORME DE VENTAS", True, 14 ' Titulo
    Cy = Cy + Dp
    Coloca Cx, Cy, Sp_Lh, False, 8
    
 
'    'Ahora datos del cliente
'    Cy = Cy + (Dp * 2)
'    Coloca Cx, Cy, "R.U.T.:" & TxtRut & "       NOMBRE:" & Me.txtCliente, True, 10 'Nombre empresa
'    Cy = Cy + Dp + Dp
'    Coloca Cx, Cy, "DIRECCION:" & TxtDireccion & "       CIUDAD:" & TxtCiudad, False, 10 'rut
'    Cy = Cy + Dp + Dp
'    Coloca Cx, Cy, Sp_Lh, False, 8
  
    P_Fecha = "Fecha"
    P_Documento = "Documento"
    P_Numero = "Numero"
    'P_Vence = "Venc."
    RSet P_Venta = "Bruto"
    RSet P_Neto = "Neto"
    RSet P_Iva = "IVA"
    P_Cliente = "CLIENTE"
    P_Rut = "R.U.T."
    RSet P_Pago = "F.PAGO"
    
    Cy = Cy + Dp
    EnviaLineasC True
    Cy = Cy + Dp
     Printer.FontName = "Courier New"
    Coloca Cx, Cy, Sp_Lh, False, 8
     
    
    
    
    
End Sub

Private Sub PieDeCartola()
    Dim Sp_Lh As String, Sp_Tit As String
    Dim Sp_Cetras As String
    For i = 1 To 120
        Sp_Lh = Sp_Lh & "_"
    Next
    Printer.FontName = "Courier New"
    Cy = Cy + Dp
    P_Fecha = ""
    P_Cliente = ""
    P_Documento = "TOTALES:"
    P_Numero = ""
    P_Vence = ""
    Dp_Neto = Round(CDbl(txtTotalDoc) / 1.19, 0)
    RSet P_Venta = Me.txtTotalDoc
    RSet P_Neto = NumFormat(Dp_Neto)
    RSet P_Iva = NumFormat(CDbl(txtTotalDoc) - Round(Dp_Neto, 0))
    Coloca Cx, Cy, Sp_Lh, False, 8
    Cy = Cy + Dp
    EnviaLineasC True
    Cy = Cy + Dp
     Printer.FontName = "Courier New"
    Coloca Cx, Cy, Sp_Lh, False, 8
End Sub


Private Function EnviaLineasC(Negrita As Boolean)
        Dim avance As Double
        avance = 1.5
        Printer.FontSize = 10
        Printer.FontName = "Arial Narrow"
       ' Cx = 0
       ' Cy = 0
        
        Coloca Cx, Cy, P_Fecha, Negrita, 10
        Coloca Cx + 1.7, Cy, P_Documento, Negrita, 10
        Coloca Cx + 4.9, Cy, P_Numero, Negrita, 10
        Coloca Cx + 6.2, Cy, P_Rut, Negrita, 10
        Coloca Cx + 8.4, Cy, P_Cliente, Negrita, 10
        Coloca (Cx + 14.2) - Printer.TextWidth(P_Neto), Cy, P_Neto, Negrita, 10
        Coloca (Cx + 15.4) - Printer.TextWidth(P_Iva), Cy, P_Iva, Negrita, 10
        Coloca (Cx + 17) - Printer.TextWidth(P_Venta), Cy, P_Venta, Negrita, 10
        Coloca Cx + 17.5, Cy, P_Pago, Negrita, 10
       
End Function


Function ConexionBDPorteria()
            On Error GoTo FalloConexion
               ' Sp_Servidor = "vegamodelosa.dyndns.org"
                SG_BaseDato = "db_valley"
                'Connection_String = "PROVIDER=MSDASQL;dsn=citroen;uid=;pwd=;" '   ' le agrega el path de la base de datos al Connectionstring
                Connection_String = "PROVIDER=MSDASQL;dsn=db_redmar;uid=root;pwd=eunice;server=" & Sp_Servidor & ";database=db_valley;"
                '" '   ' le agrega el path de la base de datos al Connectionstring
                
'                Connection_String = "Driver={MySQL ODBC 5.2 Driver Unicode Driver};Server=" & Sp_Servidor & " ;Port=3306;" & _
                                    "Database=" & SG_BaseDato & ";User=root;Password=eunice;Option=3;"
                
                Set CnV = New ADODB.Connection '  ' Nuevo objeto Connection
                CnV.CursorLocation = adUseClient
                'cn.CommandTimeout = 30
                CnV.ConnectionTimeout = 30
                CnV.Open Connection_String '  'Abre la conexi�n
            Exit Function
FalloConexion:
    MsgBox "No fue posible establecer la conecci�n con el Servidor..." & vbNewLine & "Verifique que la Ip ingresada sea la correcta", vbInformation
    Exit Function
End Function

Function ConsultaV(RecSet As ADODB.Recordset, CSQL As String)
InicioConsulta:
            IS_IntentosConexion = IS_IntentosConexion + 1
             Set RecSet = New ADODB.Recordset '  'Nuevo recordset
            'Debug.Print CSQL
            On Error GoTo FalloConsulta
            
            RecSet.Open CSQL, CnV, adOpenStatic, adLockOptimistic '  ' abre
            IS_IntentosConexion = 0
            Exit Function
FalloConsulta:
    If IS_IntentosConexion < 2 Then
        ConexionBD
        GoTo InicioConsulta
    End If
        
    If EjecucionIDE Then
        MsgBox "Fallo la consulta a la base de datos " & vbNewLine & CSQL & vbNewLine & Err.Number & " - " & Err.Description
    Else
        MsgBox "Fallo la consulta a la base de datos " & vbNewLine & vbNewLine & Err.Number & " - " & Err.Description
    End If
    If IS_IntentosConexion = 2 Then End
End Function
Private Sub CerrarCajasPorteriaVegaModelo()
 
        ' **********  V E G A      M O D E L O  *************
        'Hacer conexion con la otra BD
        'para agregar las cajas cerradas que no hhan sido cerradas por adminitracion
        '26 Febrero  2016
        ConexionBDPorteria
        For i = 1 To LvDetalleDoc.ListItems.Count
            If Val(LvDetalleDoc.ListItems(i).SubItems(12)) > 0 Then
                Sql = "UPDATE ven_turno SET caj_id=" & Lp_IdMiCaja & _
                    " WHERE  caj_id=0 AND tur_estado='CERRADO' AND tur_id=" & LvDetalleDoc.ListItems(i).SubItems(12)
                    
                
                CnV.Execute Sql
            End If
        Next
         
End Sub

Private Sub CargaHistoricas()

    '14 Diciembre
    'Cargamos tambien el detalle de los cheques ,che_fecha
    Sql = "SELECT che_id,che_numero,ban_nombre,che_monto,che_fecha " & _
            "FROM abo_cheques c " & _
            "JOIN par_bancos b ON c.ban_id=b.ban_id " & _
            "WHERE abo_id IN " & _
            "(SELECT d.abo_id " & _
                "FROM    cta_abono_documentos d " & _
                "/* JOIN ven_doc_venta m ON d.id=m.id */ " & _
                "JOIN cta_abonos a ON d.abo_id=a.abo_id AND a.abo_cli_pro='CLI' " & _
                "JOIN abo_tipos_de_pagos t ON d.abo_id=t.abo_id " & _
                "WHERE a.caj_id=" & Lp_IdMiCaja & " AND a.rut_emp='" & SP_Rut_Activo & "' AND t.mpa_id=2)"
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvCheques, False, True, True, False
    TxtTotalCheques = NumFormat(TotalizaColumna(LvCheques, "total"))


        Sql = "SELECT hin_id_fpago,hin_nom_fpago,hin_valor " & _
                "FROM his_caja_ingresos " & _
                "WHERE caj_id=" & DG_ID_Unico
        Consulta RsTmp, Sql
        LLenar_Grilla RsTmp, Me, LvVentas, False, True, True, False
        txtIngVentas = NumFormat(TotalizaColumna(LvVentas, "total"))
        
        Sql = "SELECT hic_id_fpago,hic_nom_fpago,hic_valor " & _
                "FROM his_caja_ingresos_ctacte " & _
                "WHERE caj_id=" & DG_ID_Unico
        Consulta RsTmp, Sql
        LLenar_Grilla RsTmp, Me, LvCtaCtes, False, True, True, False
        txtIngCtaCtes = NumFormat(TotalizaColumna(LvCtaCtes, "total"))
       
        Sql = "SELECT hde_fpago,hde_emision,hde_vencimiento,hde_documento,hde_nro,hde_cliente,hde_monto,hde_rut_cliente,hde_ncx,hde_neto,hde_iva," & _
            "hde_nombre_documento " & _
                "FROM his_caja_detalle_ventas " & _
                "WHERE caj_id=" & DG_ID_Unico
        Consulta RsTmp, Sql
        LLenar_Grilla RsTmp, Me, LvDetalleDoc, False, True, True, False
        TotalizaDoc

        Sql = "SELECT hcr_fpago,hcr_emision,hcr_vencimiento,hcr_documento,hcr_nro,hcr_cliente,hcr_monto " & _
                "FROM his_caja_detalle_ctacte " & _
                "WHERE caj_id=" & DG_ID_Unico
        Consulta RsTmp, Sql
        LLenar_Grilla RsTmp, Me, LvDetalleCtaCte, False, True, True, False
        Me.txtTotalCtaCte = NumFormat(TotalizaColumna(LvDetalleCtaCte, "total"))
    
    
        'resumen de caja
    'si el efectivo inicial es > 0 then
    'colocarlo 1ro
    If CDbl(txtEfectivoInicial) > 0 Then
        LvDetalle.ListItems.Add , , 1
        LvDetalle.ListItems(LvDetalle.ListItems.Count).SubItems(1) = "EFECTIVO"
        LvDetalle.ListItems(LvDetalle.ListItems.Count).SubItems(2) = txtEfectivoInicial
    End If
    
    For i = 1 To LvVentas.ListItems.Count
        Bp_Agregado = False
        For X = 1 To LvDetalle.ListItems.Count
            If LvVentas.ListItems(i) = LvDetalle.ListItems(X) Then
                LvDetalle.ListItems(X).SubItems(2) = NumFormat(CDbl(LvDetalle.ListItems(X).SubItems(2)) + CDbl(LvVentas.ListItems(i).SubItems(2)))
                Bp_Agregado = True
                Exit For
            End If
        Next
    
        If Not Bp_Agregado Then
            LvDetalle.ListItems.Add , , LvVentas.ListItems(i)
            LvDetalle.ListItems(LvDetalle.ListItems.Count).SubItems(1) = LvVentas.ListItems(i).SubItems(1)
            LvDetalle.ListItems(LvDetalle.ListItems.Count).SubItems(2) = LvVentas.ListItems(i).SubItems(2)
        End If
    Next
    
    For i = 1 To Me.LvCtaCtes.ListItems.Count
        Bp_Agregado = False
        For X = 1 To LvDetalle.ListItems.Count
            If LvCtaCtes.ListItems(i) = LvDetalle.ListItems(X) Then
                LvDetalle.ListItems(X).SubItems(2) = NumFormat(CDbl(LvDetalle.ListItems(X).SubItems(2)) + CDbl(LvCtaCtes.ListItems(i).SubItems(2)))
                Bp_Agregado = True
                Exit For
            End If
        Next
        If Not Bp_Agregado Then
            LvDetalle.ListItems.Add , , LvCtaCtes.ListItems(i)
            LvDetalle.ListItems(LvDetalle.ListItems.Count).SubItems(1) = LvCtaCtes.ListItems(i).SubItems(1)
            LvDetalle.ListItems(LvDetalle.ListItems.Count).SubItems(2) = LvCtaCtes.ListItems(i).SubItems(2)
        
        End If
    Next
    
    txtTotal = NumFormat(TotalizaColumna(LvDetalle, "total"))
End Sub



Private Sub ImprimeCierreSoloGempp()
    Dim Lp_SoloContado As Long
    Dim Cx As Double, Cy As Double, Sp_Fecha As String, Dp_S As Double, Sp_Letras As String
    Dim Sp_Valor As String * 10, Sp_Fpago As String * 12
    
    Dim Sp_FpagoR As String * 15
    Dim Sp_Femision As String * 11
    Dim Sp_Fvenc As String * 11
    Dim Sp_Doc As String * 4
    Dim Sp_Nro As String * 9
    Dim Sp_Cliente As String * 35
    
    Dim Sp_Empresa As String
    Dim Sp_Giro As String
    Dim Sp_Direccion As String
    
    Dim Lp_ValorSoloFACNC As Long
    Printer.FontName = "Courier New"
    
    Dp_S = 0.3
    For f = 1 To Dialogo.Copies
        On Error GoTo ErroComp
        Sql = "SELECT giro,direccion,ciudad " & _
             "FROM sis_empresas " & _
             "WHERE rut='" & SP_Rut_Activo & "'"
        Consulta RsTmp2, Sql
        If RsTmp2.RecordCount > 0 Then
            Sp_Giro = RsTmp2!giro
            Sp_Direccion = RsTmp2!direccion & " - " & RsTmp2!ciudad
        End If
        Printer.ScaleMode = 7
        Cx = 1.5
        Cy = 1.9
        Dp_S = 0.3
        Printer.CurrentX = Cx
        Printer.CurrentY = Cy
        'Printer.PaintPicture LoadPicture(App.Path & "\IMG\LAFLOR\LAFLOR.jpg"), Cx + 0.62, 1
       ' Printer.PaintPicture LoadPicture(App.Path & "\REDMAROK.jpg"), Cx + 0.62, 1
        Coloca Cx, Cy, SP_Empresa_Activa, False, 10
        Cy = Cy + (Dp_S * 2)
        Coloca Cx + 6, Cy, "ARQUEO DE CAJA", True, 14
        Cy = Cy + 1
        Sp_FpagoR = "SUCURSAL"
        Coloca Cx, Cy, Sp_FpagoR & ":" & Me.txtSucursal, False, 10
        Sp_FpagoR = "APERTURA"
        Coloca Cx, Cy + Dp_S, Sp_FpagoR & ":" & DtFecha, False, 10
        Sp_FpagoR = "CIERRE"
        Coloca Cx, Cy + Dp_S + Dp_S, Sp_FpagoR & ":" & Now, False, 10
        
        Sp_FpagoR = "EFECT. INICIAL"
        Coloca Cx, Cy + Dp_S + Dp_S + Dp_S, Sp_FpagoR & ":" & txtEfectivoInicial, False, 10
        
        Cy = Cy + Dp_S * 5 'interlineas
        fincierra = Cy - 0.2
        Coloca Cx, Cy, "INGRESOS POR VENTAS", True, 8
        Coloca Cx + 6, Cy, "INGRESOS POR CTAS CTES", True, 8
        'Coloca Cx + 12.5, Cy, "RESUMEN DE INGRESOS", True, 8
        Cy = Cy + 0.4
        pos1 = Cy
        For i = 1 To Me.LvVentas.ListItems.Count
            Sp_Fpago = LvVentas.ListItems(i).SubItems(1)
            Coloca Cx, Cy, Sp_Fpago, False, 8
            RSet Sp_Valor = LvVentas.ListItems(i).SubItems(2)
            Coloca Cx + 3, Cy, Sp_Valor, False, 8
            Cy = Cy + Dp_S
        Next i
        finr = Cy
        Cy = pos1
        For i = 1 To LvCtaCtes.ListItems.Count
            Sp_Fpago = LvCtaCtes.ListItems(i).SubItems(1)
            Coloca Cx + 6, Cy, Sp_Fpago, False, 8
            RSet Sp_Valor = LvCtaCtes.ListItems(i).SubItems(2)
            Coloca Cx + 6 + 3, Cy, Sp_Valor, False, 8
            Cy = Cy + Dp_S
        Next i
        If Cy > finr Then finr = Cy
        Cy = pos1
      '  For i = 1 To LvDetalle.ListItems.Count
      '      Sp_Fpago = LvDetalle.ListItems(i).SubItems(1)
      '      Coloca Cx + 12.5, Cy, Sp_Fpago, False, 8
      '      RSet Sp_Valor = LvDetalle.ListItems(i).SubItems(2)
      '      Coloca Cx + 15.5, Cy, Sp_Valor, False, 8
      '      Cy = Cy + Dp_S
      '  Next i
        If Cy > finr Then finr = Cy 'Hasta variable es para el final del cuadro de los resumen
        'Imprimir totales
        Cy = finr
      '  RSet Sp_Fpago = "TOTAL"
      
        RSet Sp_Valor = txtIngVentas
        Coloca Cx, Cy, Sp_Fpago, True, 8
        Coloca Cx + 3, Cy, Sp_Valor, True, 8
        RSet Sp_Valor = txtIngCtaCtes
        Coloca Cx + 6, Cy, Sp_Fpago, True, 8
        Coloca Cx + 9, Cy, Sp_Valor, True, 8
       ' RSet Sp_Valor = txtTotal
       ' Coloca Cx + 12.5, Cy, Sp_Fpago, True, 8
       ' Coloca Cx + 15.5, Cy, Sp_Valor, True, 8
        Cy = Cy + Dp_S
        finr = Cy + 0.2
        
               
        'Ahora el detalle
        With LvDetalleDoc
            Sp_FpagoR = "F.pago"
            Sp_Femision = "Emision"
            Sp_Fvenc = "Vence"
            Sp_Doc = "Doc"
            Sp_Nro = "Numero"
            Sp_Cliente = "Cliente"
            RSet Sp_Valor = "Valor"
            Cy = finr + 0.4
            Coloca Cx, Cy, "DETALLE DE VENTAS", True, 10
            Cy = Cy + Dp_S
            Coloca Cx, Cy, Sp_FpagoR & " " & Sp_Femision & " " & Sp_Fvenc & " " & Sp_Doc & " " & Sp_Nro & " " & Sp_Cliente & " " & Sp_Valor, True, 8
            Cy = Cy + Dp_S
            Lp_ValorSoloFACNC = 0
            i = 0
            cf = 0
            For i = 1 To .ListItems.Count
                
                    
                    If .ListItems(i).SubItems(3) <> "BOM" And .ListItems(i).SubItems(3) <> "BOF" Then
                        Sp_FpagoR = .ListItems(i)
                        Sp_Femision = .ListItems(i).SubItems(1)
                        Sp_Fvenc = .ListItems(i).SubItems(2)
                        Sp_Doc = .ListItems(i).SubItems(3)
                        Sp_Nro = .ListItems(i).SubItems(4)
                        If .ListItems(i).SubItems(7) = "11.111.111-1" Then
                            Sp_Cliente = ""
                        Else
                            Sp_Cliente = .ListItems(i).SubItems(5)
                        End If
                        RSet Sp_Valor = .ListItems(i).SubItems(6)
                        Lp_ValorSoloFACNC = Lp_ValorSoloFACNC + CDbl(.ListItems(i).SubItems(6))
                        Coloca Cx, Cy, Sp_FpagoR & " " & Sp_Femision & " " & Sp_Fvenc & " " & Sp_Doc & " " & Sp_Nro & " " & Sp_Cliente & " " & Sp_Valor, False, 8
                        Cy = Cy + Dp_S
                        cf = cf + 1
                    End If
                    
                    
               
            Next
            
            'Aqui debemos contar la cantidad fac, y totalizar las fac.
            If cf > 0 Then
                        Sp_FpagoR = ""
                        Sp_Femision = ""
                        Sp_Fvenc = ""
                        Sp_Doc = ""
                        Sp_Nro = ""
                        Sp_Cliente = cf & " FACTURAS-NC, POR $" & NumFormat(Lp_ValorSoloFACNC)
                        
                        RSet Sp_Valor = ""
                        Coloca Cx, Cy, Sp_FpagoR & " " & Sp_Femision & " " & Sp_Fvenc & " " & Sp_Doc & " " & Sp_Nro & " " & Sp_Cliente & " " & Sp_Valor, True, 8
                        Cy = Cy + Dp_S
            
            End If
            
            
            
            
            If SP_Rut_Activo = "76.369.600-6" Then
            
                ''
            Else
                    
                For i = 1 To .ListItems.Count
                    If .ListItems(i).SubItems(3) = "BOM" Then
                        Sql = "SELECT CONCAT('DE ',CAST(MIN(no_documento) AS CHAR),' A ',CAST(max(no_documento) AS CHAR)) boletas,SUM(bruto) venta " & _
                                "FROM ven_doc_venta " & _
                                "JOIN sis_documentos USING(doc_id) " & _
                                "WHERE caj_id=" & Lp_IdMiCaja & " AND doc_nombre='BOLETA'"
                        Consulta RsTmp, Sql
                        Sp_FpagoR = ""
                        Sp_Femision = ""
                        Sp_Fvenc = ""
                        Sp_Doc = "BOM"
                        Sp_Nro = ""
                        Sp_Cliente = RsTmp!boletas
                        RSet Sp_Valor = NumFormat(RsTmp!venta)
                        Coloca Cx, Cy, Sp_FpagoR & " " & Sp_Femision & " " & Sp_Fvenc & " " & Sp_Doc & " " & Sp_Nro & " " & Sp_Cliente & " " & Sp_Valor, False, 8
                        Cy = Cy + Dp_S
                        Exit For
                    End If
                Next
                
                For i = 1 To .ListItems.Count
                    If .ListItems(i).SubItems(3) = "BOF" Then
                        Sql = "SELECT CONCAT('DE ',CAST(MIN(no_documento) AS CHAR),' A ',CAST(max(no_documento) AS CHAR)) boletas,SUM(bruto) venta " & _
                                "FROM ven_doc_venta " & _
                                "JOIN sis_documentos USING(doc_id) " & _
                                "WHERE caj_id=" & Lp_IdMiCaja & " AND doc_nombre='BOLETA FISCAL'"
                        Consulta RsTmp, Sql
                        Sp_FpagoR = ""
                        Sp_Femision = ""
                        Sp_Fvenc = ""
                        Sp_Doc = "BOF"
                        Sp_Nro = ""
                        Sp_Cliente = RsTmp!boletas
                        RSet Sp_Valor = NumFormat(RsTmp!venta)
                        Coloca Cx, Cy, Sp_FpagoR & " " & Sp_Femision & " " & Sp_Fvenc & " " & Sp_Doc & " " & Sp_Nro & " " & Sp_Cliente & " " & Sp_Valor, False, 8
                        Cy = Cy + Dp_S
                        Exit For
                    End If
                Next
                
            End If
            
            Sp_FpagoR = ""
            Sp_Femision = ""
            Sp_Fvenc = ""
            Sp_Doc = ""
            Sp_Nro = ""
            RSet Sp_Cliente = "TOTAL"
            If SP_Rut_Activo = "76.369.600-6" Then
                For i = 1 To Me.LvDetalleDoc.ListItems.Count
                    If Trim(LvDetalleDoc.ListItems(i)) = "CONTADO" Then
                        Lp_SoloContado = Lp_SoloContado + CDbl(LvDetalleDoc.ListItems(i).SubItems(6))
                    End If
                    
                Next
                RSet Sp_Valor = NumFormat(Lp_SoloContado)
                    
                
            Else
                RSet Sp_Valor = txtTotalDoc
            End If
            Coloca Cx, Cy, Sp_FpagoR & " " & Sp_Femision & " " & Sp_Fvenc & " " & Sp_Doc & " " & Sp_Nro & " " & Sp_Cliente & " " & Sp_Valor, True, 8
            
           
            If SP_Rut_Activo = "76.369.600-6" Then
                'Restamos el valor de los gastos a los pagos contado
                If LvGastos.ListItems.Count > 0 Then
                    RSet Sp_Cliente = "GASTOS POR CAJA"
                    RSet Sp_Valor = Me.txtTotalGasto
                    Cy = Cy + Dp_S
                    Coloca Cx, Cy, Sp_FpagoR & " " & Sp_Femision & " " & Sp_Fvenc & " " & Sp_Doc & " " & Sp_Nro & " " & Sp_Cliente & " " & Sp_Valor, True, 8
                    If Val(txtTotalGasto) = 0 Then TxtGasto = 0
                    
                    RSet Sp_Cliente = "LIQUIDO"
                    RSet Sp_Valor = NumFormat(Lp_SoloContado - CDbl(txtTotalGasto))
                    Cy = Cy + Dp_S
                    Coloca Cx, Cy, Sp_FpagoR & " " & Sp_Femision & " " & Sp_Fvenc & " " & Sp_Doc & " " & Sp_Nro & " " & Sp_Cliente & " " & Sp_Valor, True, 8
                End If
            End If
            
            Finde = Cy + Dp_S
        End With
      
        
                'Ahora el detalle DE PAGOS POR CTA CTE
        
        Cy = Finde + Dp_S * 2
        If LvDetalleCtaCte.ListItems.Count > 0 Then
            With LvDetalleCtaCte
                Sp_FpagoR = "F.pago"
                Sp_Femision = "Emision"
                Sp_Fvenc = "Vence"
                Sp_Doc = "Doc"
                Sp_Nro = "Numero"
                Sp_Cliente = "Cliente"
                RSet Sp_Valor = "Valor"
                Cy = Finde + 0.5
                Coloca Cx, Cy, "DETALLE INGRESOS POR CTA CTE", True, 10
                Cy = Cy + Dp_S
                Coloca Cx, Cy, Sp_FpagoR & " " & Sp_Femision & " " & Sp_Fvenc & " " & Sp_Doc & " " & Sp_Nro & " " & Sp_Cliente & " " & Sp_Valor, True, 8
                Cy = Cy + Dp_S
                For i = 1 To .ListItems.Count
                    Sp_FpagoR = .ListItems(i)
                    Sp_Femision = .ListItems(i).SubItems(1)
                    Sp_Fvenc = .ListItems(i).SubItems(2)
                    Sp_Doc = .ListItems(i).SubItems(3)
                    Sp_Nro = .ListItems(i).SubItems(4)
                    Sp_Cliente = .ListItems(i).SubItems(5)
                    RSet Sp_Valor = .ListItems(i).SubItems(6)
                    Coloca Cx, Cy, Sp_FpagoR & " " & Sp_Femision & " " & Sp_Fvenc & " " & Sp_Doc & " " & Sp_Nro & " " & Sp_Cliente & " " & Sp_Valor, False, 8
                    Cy = Cy + Dp_S
                Next
                Sp_FpagoR = ""
                Sp_Femision = ""
                Sp_Fvenc = ""
                Sp_Doc = ""
                Sp_Nro = ""
                RSet Sp_Cliente = "TOTAL"
                RSet Sp_Valor = txtTotalCtaCte
                Coloca Cx, Cy, Sp_FpagoR & " " & Sp_Femision & " " & Sp_Fvenc & " " & Sp_Doc & " " & Sp_Nro & " " & Sp_Cliente & " " & Sp_Valor, True, 8
                finc = Cy + Dp_S
            End With
        End If
        Cy = Cy + 0.5
        inich = Cy - 0.2
        
        'Ahora en detalle de gastos y el detalle de cheques recibidos
        Dim Sp_ChNro As String * 12
        Dim Sp_ChBanco As String * 12
        Dim Sp_ChValor As String * 12
        Dim Sp_DetGasto As String * 25
        Dim Sp_DetGastoValor As String * 10
        Coloca Cx, Cy, "CHEQUES RECIBIDOS", True, 8
        'Coloca Cx + 12.5, Cy, "GASTOS POR CAJA", True, 8
        Cy = Cy + 0.4
        pedeta = Cy
        Sp_ChNro = "Nro"
        Sp_ChBanco = "Banco"
        RSet Sp_ChValor = "Valor"
        Sp_DetGasto = "Detalle"
        RSet Sp_DetGastoValor = "Valor"
        
        Coloca Cx, Cy, Sp_ChNro & " " & Sp_ChBanco & " " & Sp_ChValor, False, 8
        Coloca Cx + 11, Cy, Sp_DetGasto & " " & Sp_DetGastoValor, False, 8
        'Coloca Cx + 12.5, Cy, "GASTOS POR CAJA", True, 8
        
        Cy = Cy + Dp_S
        For i = 1 To LvCheques.ListItems.Count
            Sp_ChNro = LvCheques.ListItems(i).SubItems(1)
            Sp_ChBanco = LvCheques.ListItems(i).SubItems(2)
            RSet Sp_ChValor = LvCheques.ListItems(i).SubItems(3)
            Coloca Cx, Cy, Sp_ChNro & " " & Sp_ChBanco & " " & Sp_ChValor, False, 8
            Cy = Cy + Dp_S
        Next i
        finch = Cy
        Sp_ChNro = ""
        RSet Sp_ChBanco = "TOT. CHEQUES"
        RSet Sp_ChValor = TxtTotalCheques
        Coloca Cx, Cy, Sp_ChNro & " " & Sp_ChBanco & " " & Sp_ChValor, True, 8
        
        Cy = pedeta
                
        Cy = Cy + Dp_S
            
            'For i = 1 To LvGastos.ListItems.Count
            '    Sp_DetGasto = LvGastos.ListItems(i).SubItems(1)
            '    RSet Sp_DetGastoValor = LvGastos.ListItems(i).SubItems(2)
            '    Coloca Cx + 11, Cy, Sp_DetGasto & " " & Sp_DetGastoValor, False, 8
            '    Cy = Cy + Dp_S
            'Next i
            'RSet Sp_DetGasto = "TOTAL GASTOS"
            'RSet Sp_DetGastoValor = txtTotalGasto
            'Coloca Cx + 11, Cy, Sp_DetGasto & " " & Sp_DetGastoValor, True, 8
            If Cy > finch Then finch = Cy
            
            
        'Agregar detalle de billets ingresados
        Dim Sp_ValorBillete As String * 6
        Dim Sp_CantidadBilletes As String * 4
        Dim Sp_TotalBilletes As String * 10
        
        
        finch = finch + Dp_S + Dp_S
        Cy = finch + Dp_S + Dp_S
        iniciobilletes = finch + Dp_S + Dp_S
        finbilletes = Cy
        inicioresumen = Cy
        Coloca Cx, Cy, "Detalle de Billetes", True, 8
        Cy = Cy + Dp_S
        RSet Sp_ValorBillete = "Valor"
        RSet Sp_CantidadBilletes = "Cant."
        RSet Sp_TotalBilletes = "Total"
        Coloca Cx, Cy, Sp_ValorBillete & " " & Sp_CantidadBilletes & " " & Sp_TotalBilletes, True, 8
        Cy = Cy + Dp_S
         For i = 1 To LvBilletes.ListItems.Count
                RSet Sp_ValorBillete = LvBilletes.ListItems(i).SubItems(1)
                RSet Sp_CantidadBilletes = LvBilletes.ListItems(i).SubItems(2)
                RSet Sp_TotalBilletes = LvBilletes.ListItems(i).SubItems(3)
                Coloca Cx, Cy, Sp_ValorBillete & " " & Sp_CantidadBilletes & " " & Sp_TotalBilletes, False, 8
                
                Cy = Cy + Dp_S
        Next i
        RSet Sp_ValorBillete = "TOTAL"
        RSet Sp_CantidadBilletes = "BILLETES"
        RSet Sp_TotalBilletes = TxtTBilletes
        Coloca Cx, Cy, Sp_ValorBillete & " " & Sp_CantidadBilletes & " " & Sp_TotalBilletes, True, 8
        
        
        If Cy > finbilletes Then finbilletes = Cy
        'RESUMEN INGRESOS
        
        Coloca Cx + 12.5, inicioresumen, "RESUMEN DE INGRESOS", True, 8
        Cy = inicioresumen + Dp_S
        
        If LvDetalle.ListItems.Count > 1 Then
        
                'si tiene mas de una forma de pago
                'empezamos por la 2da
                For i = 2 To LvDetalle.ListItems.Count
                    Sp_Fpago = LvDetalle.ListItems(i).SubItems(1)
                    Coloca Cx + 12.5, Cy, Sp_Fpago, False, 8
                    RSet Sp_Valor = LvDetalle.ListItems(i).SubItems(2)
                    Coloca Cx + 15.5, Cy, Sp_Valor, False, 8
                    Cy = Cy + Dp_S
                Next i
                
                
        Else
              ''
        
        End If
        
        For i = 1 To 1
                    Sp_Fpago = LvDetalle.ListItems(i).SubItems(1)
                    Coloca Cx + 12.5, Cy, Sp_Fpago, True, 8
                    RSet Sp_Valor = LvDetalle.ListItems(i).SubItems(2)
                    Coloca Cx + 15.5, Cy, Sp_Valor, True, 8
                    Cy = Cy + Dp_S
        Next i
        
        Sp_Fpago = "RETIROS (-)"
        Coloca Cx + 12.5, Cy, Sp_Fpago, False, 8
        RSet Sp_Valor = Me.txtTotalGasto
        Coloca Cx + 15.5, Cy, Sp_Valor, False, 8
        Cy = Cy + Dp_S
        
        
        Sp_Fpago = "-------------"
        Coloca Cx + 12.5, Cy, Sp_Fpago, False, 8
        RSet Sp_Valor = "----------"
        Coloca Cx + 15.5, Cy, Sp_Valor, False, 8
        Cy = Cy + Dp_S
        
        Sp_Fpago = "EFECTIVO ENTREGAR"
        Coloca Cx + 12.5, Cy, Sp_Fpago, True, 8
        'Restar NC tambien,
        restaNc = 0
        If SP_Rut_Activo = "76.005.337-6" Then
            'Gempp restar NC
            For i = 1 To LvVentas.ListItems.Count
                If LvVentas.ListItems(i) = 8888 Then
                      restaNc = Abs(LvVentas.ListItems(i).SubItems(2))
                      Exit For
                End If
            
            Next
            
            RSet Sp_Valor = NumFormat(CDbl(LvDetalle.ListItems(1).SubItems(2)) - CDbl(Me.txtTotalGasto) - restaNc)
        Else
            RSet Sp_Valor = NumFormat(CDbl(LvDetalle.ListItems(1).SubItems(2)) - CDbl(Me.txtTotalGasto))
        End If
        Coloca Cx + 15.5, Cy, Sp_Valor, True, 8
        Cy = Cy + Dp_S
        
        Sp_Fpago = "-------------"
        Coloca Cx + 12.5, Cy, Sp_Fpago, False, 8
        RSet Sp_Valor = "----------"
        Coloca Cx + 15.5, Cy, Sp_Valor, False, 8
        Cy = Cy + Dp_S
        
        
        
        finbilletes = finbilletes + Dp_S + Dp_S
        
            
       ' Printer.DrawMode = 1
        Printer.DrawWidth = 3
        
        Printer.Line (1.4, 1)-(19, fincierra), , B 'Rectangulo Encabezado
        
        Printer.Line (1.4, fincierra)-(19, finr), , B 'ingressos
        Printer.Line (1.4 + 5.8, fincierra)-(12.3, finr), , B 'ingressos
        
    
        If Me.LvDetalleDoc.ListItems.Count > 0 Then Printer.Line (1.4, finr)-(19, Finde), , B       'detalle ventas
        If Me.LvDetalleCtaCte.ListItems.Count > 0 Then Printer.Line (1.4, Finde)-(19, finc), , B        'detalle ingresos ctas ctes
        Printer.Line (1.4, inich)-(19, finch), , B 'Cheques y gastos
        
        Printer.Line (1.4, iniciobilletes)-(19, finbilletes), , B 'Detalle de billets
        
        Printer.NewPage
        Printer.EndDoc
        Sp_Valor = 0
        Lp_SoloContado = 0
        
    Next
    Exit Sub
ErroComp:
    MsgBox "Error :" & Err.Description & vbNewLine & "Nro " & Err.Number & vbNewLine & "Lin:" & Err.Source
End Sub


