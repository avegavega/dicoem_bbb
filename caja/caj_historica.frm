VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form caj_historica 
   Caption         =   "Ver cajas historicas"
   ClientHeight    =   3555
   ClientLeft      =   9435
   ClientTop       =   4890
   ClientWidth     =   3975
   LinkTopic       =   "Form1"
   ScaleHeight     =   3555
   ScaleWidth      =   3975
   Begin VB.Timer Timer1 
      Interval        =   10
      Left            =   45
      Top             =   3045
   End
   Begin VB.CommandButton cmdRetornar 
      Caption         =   "Retornar"
      Height          =   420
      Left            =   1230
      TabIndex        =   6
      Top             =   3030
      Width           =   1410
   End
   Begin VB.Frame Frame1 
      Caption         =   "Datos de la Caja"
      Height          =   2805
      Left            =   195
      TabIndex        =   0
      Top             =   135
      Width           =   3555
      Begin ACTIVESKINLibCtl.SkinLabel SknSucursal 
         Height          =   330
         Left            =   285
         OleObjectBlob   =   "caj_historica.frx":0000
         TabIndex        =   7
         Top             =   1410
         Width           =   3180
      End
      Begin VB.CommandButton cmdVerCaja 
         Caption         =   "Ver Caja"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   480
         Left            =   285
         TabIndex        =   5
         Top             =   1995
         Width           =   3135
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   195
         Left            =   1635
         OleObjectBlob   =   "caj_historica.frx":006E
         TabIndex        =   4
         Top             =   510
         Width           =   1335
      End
      Begin VB.ComboBox CboCajas 
         Height          =   315
         Left            =   1620
         Style           =   2  'Dropdown List
         TabIndex        =   3
         Top             =   720
         Width           =   1815
      End
      Begin MSComCtl2.DTPicker DtFecha 
         Height          =   315
         Left            =   315
         TabIndex        =   2
         Top             =   720
         Width           =   1305
         _ExtentX        =   2302
         _ExtentY        =   556
         _Version        =   393216
         Format          =   273416193
         CurrentDate     =   41626
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   345
         Left            =   315
         OleObjectBlob   =   "caj_historica.frx":00DA
         TabIndex        =   1
         Top             =   495
         Width           =   2655
      End
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   90
      OleObjectBlob   =   "caj_historica.frx":0142
      Top             =   2235
   End
End
Attribute VB_Name = "caj_historica"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub CboCajas_Click()
Dim Id_Caja As Integer


Id_Caja = CboCajas.ItemData(CboCajas.ListIndex)

        Sql = "SELECT sue_id " & _
                "FROM ven_caja " & _
                "WHERE caj_id=" & Id_Caja & " AND caj_estado='CERRADA'"

    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        If RsTmp!sue_id = 1 Then
        SknSucursal = "Casa Matriz"
        End If
        If RsTmp!sue_id = 2 Then
        SknSucursal = "LAUTARO 1324"
        End If
        If RsTmp!sue_id = 3 Then
        SknSucursal = "LAUTARO 1358"
        End If
    End If
    Nom_SUCU = SknSucursal
End Sub

Private Sub CmdRetornar_Click()
    Unload Me
End Sub

Private Sub cmdVerCaja_Click()
    If Me.CboCajas.ListIndex = -1 Then
        MsgBox "Seleccione Id de Caja...", vbInformation
        DtFecha.SetFocus
        Exit Sub
    End If
    
    DG_ID_Unico = CboCajas.ItemData(CboCajas.ListIndex)
   ' Nom_SUCU
   ' DG_ID_Sucursal = Me.CboSucursal.ItemData(CboSucursal.ListIndex)
    caj_arqueo.Show 1
End Sub

Private Sub DtFecha_GotFocus()
    CboCajas.Clear
End Sub

Private Sub DtFecha_Validate(Cancel As Boolean)
    'Llenar combo con las cajas abiertas en la fecha seleccionada...
    LLenarCombo CboCajas, "caj_id", "caj_id", "ven_caja", "caj_estado='CERRADA'" & _
    " AND caj_fecha_apertura BETWEEN '" & Fql(DtFecha) & " 00:00:01' AND '" & Fql(DtFecha) & " 23:59:59'"
    
    'LLenarCombo CboCajas, "caj_id", "caj_id", "ven_caja", "caj_estado='CERRADA' AND sue_id=" & IG_id_Sucursal_Empresa & _
    '" AND caj_fecha_apertura BETWEEN '" & Fql(DtFecha) & " 00:00:01' AND '" & Fql(DtFecha) & " 23:59:59'"
    's_Sql = "SELECT " & Campo & "," & Indice & "  FROM " & Tabla & " WHERE " & Condicion
    '    LLenarCombo CboSucursal, "suc_direccion", "suc_id", "par_sucursales"
    
End Sub

Private Sub Form_Load()
    Aplicar_skin Me
    Centrar Me
    DtFecha = Date
End Sub

Private Sub Timer1_Timer()
    On Error Resume Next
    DtFecha.SetFocus
    Timer1.Enabled = False
End Sub
