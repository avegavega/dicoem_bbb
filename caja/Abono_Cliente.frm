VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form Abono_Cliente 
   Caption         =   "Abono Clientes"
   ClientHeight    =   5025
   ClientLeft      =   3540
   ClientTop       =   4365
   ClientWidth     =   12510
   LinkTopic       =   "Abono Clientes"
   ScaleHeight     =   5025
   ScaleWidth      =   12510
   Begin VB.Timer Timer1 
      Left            =   15
      Top             =   360
   End
   Begin VB.TextBox TxtPOS 
      BackColor       =   &H8000000F&
      Height          =   285
      Left            =   18195
      Locked          =   -1  'True
      TabIndex        =   25
      TabStop         =   0   'False
      Text            =   "POS"
      Top             =   0
      Width           =   1275
   End
   Begin VB.TextBox TxtStock 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C0C0C0&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   18
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   450
      Left            =   6270
      TabIndex        =   24
      Top             =   9840
      Width           =   2760
   End
   Begin VB.TextBox TxtUbicacion 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C0C0C0&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   18
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   450
      Left            =   6285
      TabIndex        =   23
      Top             =   10605
      Width           =   2805
   End
   Begin VB.TextBox TxtBarCode 
      Height          =   285
      Left            =   9165
      TabIndex        =   22
      Text            =   "Text1"
      Top             =   9855
      Width           =   1080
   End
   Begin VB.TextBox TxtEmpDireccion 
      Height          =   315
      Left            =   18285
      TabIndex        =   21
      Text            =   "Text1"
      Top             =   6915
      Width           =   3510
   End
   Begin VB.TextBox TxtEmpFono 
      Height          =   285
      Left            =   18225
      TabIndex        =   20
      Text            =   "Text2"
      Top             =   7500
      Width           =   3255
   End
   Begin VB.TextBox TxtEmpMail 
      Height          =   405
      Left            =   18330
      TabIndex        =   19
      Text            =   "Text3"
      Top             =   8145
      Width           =   3300
   End
   Begin VB.Frame Frame7 
      Caption         =   "Cliente"
      Height          =   7560
      Left            =   0
      TabIndex        =   1
      Top             =   120
      Width           =   12465
      Begin VB.CommandButton CmdConsultaAbonos 
         Caption         =   "Consultar Abonos"
         Height          =   525
         Left            =   7680
         TabIndex        =   41
         Top             =   3360
         Width           =   1680
      End
      Begin VB.CommandButton CmdGuardar 
         Caption         =   "Guardar"
         Height          =   525
         Left            =   9600
         TabIndex        =   40
         Top             =   3360
         Width           =   1680
      End
      Begin VB.Frame AbonoCliente 
         Caption         =   "Lista de Abonos Cliente"
         Height          =   3375
         Left            =   120
         TabIndex        =   35
         Top             =   4080
         Width           =   12255
         Begin VB.CommandButton CmdRetornar 
            Caption         =   "Retornar"
            Height          =   525
            Left            =   10440
            TabIndex        =   39
            Top             =   2760
            Width           =   1680
         End
         Begin VB.TextBox TxtTotalAbonos 
            Height          =   285
            Left            =   9960
            TabIndex        =   38
            Text            =   "0"
            Top             =   1920
            Width           =   2175
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
            Height          =   255
            Left            =   8760
            OleObjectBlob   =   "Abono_Cliente.frx":0000
            TabIndex        =   37
            Top             =   1920
            Width           =   1215
         End
         Begin MSComctlLib.ListView LvListadoAbono 
            Height          =   1335
            Left            =   1440
            TabIndex        =   36
            Top             =   360
            Width           =   10695
            _ExtentX        =   18865
            _ExtentY        =   2355
            LabelWrap       =   -1  'True
            HideSelection   =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   4
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Text            =   "id"
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Text            =   "Descripcion"
               Object.Width           =   10583
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   2
               Text            =   "Fecha"
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   3
               Key             =   "total"
               Text            =   "Valor"
               Object.Width           =   2540
            EndProperty
         End
      End
      Begin ACTIVESKINLibCtl.SkinLabel SKDescAbono 
         Height          =   255
         Left            =   120
         OleObjectBlob   =   "Abono_Cliente.frx":0076
         TabIndex        =   34
         Top             =   3600
         Width           =   1455
      End
      Begin ACTIVESKINLibCtl.SkinLabel SLMontoAbono 
         Height          =   255
         Left            =   360
         OleObjectBlob   =   "Abono_Cliente.frx":00F6
         TabIndex        =   33
         Top             =   3120
         Width           =   1095
      End
      Begin VB.TextBox TxtDescAbono 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   1560
         TabIndex        =   32
         Top             =   3600
         Width           =   5775
      End
      Begin VB.TextBox TxtMontoAbono 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   1560
         TabIndex        =   31
         Top             =   3120
         Width           =   5775
      End
      Begin VB.TextBox TxtRazonSocial 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0C0C0&
         BorderStyle     =   0  'None
         Height          =   285
         Left            =   1560
         TabIndex        =   11
         ToolTipText     =   "Ingrese el RUT sin puntos ni guion."
         Top             =   795
         Width           =   5730
      End
      Begin VB.CommandButton CmdBuscaCliente 
         Caption         =   "F1 - Buscar"
         Height          =   255
         Left            =   2985
         TabIndex        =   10
         Top             =   465
         Width           =   1395
      End
      Begin VB.TextBox TxtRut 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0C0C0&
         BorderStyle     =   0  'None
         Height          =   285
         Left            =   1560
         TabIndex        =   0
         ToolTipText     =   "Ingrese el RUT sin puntos ni guion."
         Top             =   450
         Width           =   1395
      End
      Begin VB.TextBox TxtGiro 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0C0C0&
         BorderStyle     =   0  'None
         Height          =   285
         Left            =   1560
         TabIndex        =   9
         ToolTipText     =   "Ingrese el RUT sin puntos ni guion."
         Top             =   1125
         Width           =   5715
      End
      Begin VB.TextBox TxtDireccion 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0C0C0&
         BorderStyle     =   0  'None
         Height          =   285
         Left            =   7785
         TabIndex        =   8
         ToolTipText     =   "Ingrese el RUT sin puntos ni guion."
         Top             =   1425
         Width           =   5670
      End
      Begin VB.TextBox txtComuna 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0C0C0&
         BorderStyle     =   0  'None
         Height          =   285
         Left            =   1560
         TabIndex        =   7
         ToolTipText     =   "Ingrese el RUT sin puntos ni guion."
         Top             =   1785
         Width           =   5760
      End
      Begin VB.TextBox txtFono 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0C0C0&
         BorderStyle     =   0  'None
         Height          =   285
         Left            =   1545
         TabIndex        =   6
         ToolTipText     =   "Ingrese el RUT sin puntos ni guion."
         Top             =   2115
         Width           =   5775
      End
      Begin VB.TextBox TxtEmail 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0C0C0&
         BorderStyle     =   0  'None
         Height          =   285
         Left            =   1560
         TabIndex        =   5
         ToolTipText     =   "Ingrese el RUT sin puntos ni guion."
         Top             =   2445
         Width           =   5775
      End
      Begin VB.TextBox TxtMontoCredito 
         Height          =   255
         Left            =   8400
         TabIndex        =   4
         Text            =   "Text1"
         Top             =   2040
         Visible         =   0   'False
         Width           =   1995
      End
      Begin VB.TextBox TxtCupoUtilizado 
         Height          =   285
         Left            =   8400
         TabIndex        =   3
         Text            =   "Text1"
         Top             =   2400
         Visible         =   0   'False
         Width           =   2010
      End
      Begin VB.ComboBox CboSucursal 
         Height          =   315
         Left            =   1545
         Style           =   2  'Dropdown List
         TabIndex        =   2
         Top             =   1440
         Width           =   5760
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   240
         Index           =   0
         Left            =   1005
         OleObjectBlob   =   "Abono_Cliente.frx":016A
         TabIndex        =   12
         Top             =   495
         Width           =   495
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   210
         Index           =   2
         Left            =   840
         OleObjectBlob   =   "Abono_Cliente.frx":01D4
         TabIndex        =   13
         Top             =   840
         Width           =   660
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   255
         Index           =   1
         Left            =   435
         OleObjectBlob   =   "Abono_Cliente.frx":023E
         TabIndex        =   14
         Top             =   1470
         Width           =   1065
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel19 
         Height          =   255
         Index           =   0
         Left            =   645
         OleObjectBlob   =   "Abono_Cliente.frx":02AE
         TabIndex        =   15
         Top             =   1815
         Width           =   855
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel18 
         Height          =   255
         Left            =   1080
         OleObjectBlob   =   "Abono_Cliente.frx":0318
         TabIndex        =   16
         Top             =   1155
         Width           =   420
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel19 
         Height          =   255
         Index           =   1
         Left            =   615
         OleObjectBlob   =   "Abono_Cliente.frx":037E
         TabIndex        =   17
         Top             =   2145
         Width           =   855
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel19 
         Height          =   255
         Index           =   2
         Left            =   195
         OleObjectBlob   =   "Abono_Cliente.frx":03E4
         TabIndex        =   18
         Top             =   2445
         Width           =   1320
      End
   End
   Begin MSComDlg.CommonDialog Dialogo 
      Left            =   17820
      Top             =   7110
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
      Height          =   285
      Index           =   1
      Left            =   18270
      OleObjectBlob   =   "Abono_Cliente.frx":0466
      TabIndex        =   26
      Top             =   195
      Width           =   1650
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   30
      OleObjectBlob   =   "Abono_Cliente.frx":04CA
      Top             =   765
   End
   Begin MSComctlLib.ListView LVFpago 
      Height          =   3450
      Left            =   18705
      TabIndex        =   27
      Top             =   495
      Width           =   2940
      _ExtentX        =   5186
      _ExtentY        =   6085
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483646
      BackColor       =   -2147483643
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   3
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Object.Tag             =   "N109"
         Text            =   "Id Interno"
         Object.Width           =   1764
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Object.Tag             =   "N109"
         Text            =   "valor"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Object.Tag             =   "N109"
         Text            =   "id IF"
         Object.Width           =   2540
      EndProperty
   End
   Begin MSComctlLib.ListView LvCheques 
      Height          =   1260
      Left            =   18180
      TabIndex        =   28
      Top             =   5055
      Width           =   8205
      _ExtentX        =   14473
      _ExtentY        =   2223
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   12648384
      BorderStyle     =   1
      Appearance      =   0
      NumItems        =   9
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Object.Tag             =   "N100"
         Text            =   "id doc"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Object.Tag             =   "T1000"
         Text            =   "RUT"
         Object.Width           =   2117
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Object.Tag             =   "N109"
         Text            =   "Numero"
         Object.Width           =   1764
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Object.Tag             =   "T1500"
         Text            =   "Banco"
         Object.Width           =   2646
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Object.Tag             =   "T1000"
         Text            =   "Plaza"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   5
         Object.Tag             =   "F1000"
         Text            =   "Fecha"
         Object.Width           =   2196
      EndProperty
      BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   6
         Object.Tag             =   "N100"
         Text            =   "Monto"
         Object.Width           =   2117
      EndProperty
      BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   7
         Object.Tag             =   "T1000"
         Text            =   "Observacion"
         Object.Width           =   3528
      EndProperty
      BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   8
         Object.Tag             =   "N109"
         Text            =   "banco_id"
         Object.Width           =   2540
      EndProperty
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
      Height          =   225
      Index           =   5
      Left            =   5310
      OleObjectBlob   =   "Abono_Cliente.frx":06FE
      TabIndex        =   29
      Top             =   9795
      Width           =   660
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
      Height          =   225
      Index           =   6
      Left            =   6270
      OleObjectBlob   =   "Abono_Cliente.frx":0766
      TabIndex        =   30
      Top             =   10320
      Width           =   1035
   End
End
Attribute VB_Name = "Abono_Cliente"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim Sm_TipoPos As String
Dim Sm_PermiteDscto As String
Dim Sm_PrecioVtaModificable As String
Dim Sm_VentaRapida As String
Dim Dp_Descuento As Double
Dim Im_Redondear As Integer
Dim Lm_CuentaVentas As Long
Dim Sm_UtilizaCodigoInterno As String * 2
Dim Im_Descuento_Maximo As Double
Dim Im_Recargo_Maximo As Double
Dim Sm_Codigo_Barra_Pesable As String * 2
Dim Lp_Id_Unico_Venta As Long 'id del documento
Dim Lp_Id_Nueva_Venta As Long 'Nro documento
Dim Bm_BoletaEnProceso As Boolean
Dim Sm_ImprimeTicketNv As String * 2
Dim Sp_EmpresaRepuestos As String * 2
Dim Sp_MatrizDescuentos As String * 2
''MSG Formulario Para Ingresar y Consultar los Abonos de Clientes 28-11-2016




















'Private Sub CmdF7_Click()
'    If LvVenta.SelectedItem Is Nothing Then Exit Sub
'
'    LvVenta.ListItems.Remove LvVenta.SelectedItem.Index
'
'    CalculaGrilla
'    TxtCodigo.SetFocus
'End Sub

'Private Sub CmdF8_Click()
'            If Val(TxtCodigo.Tag) = 0 Then
'                    SG_codigo2 = ""
'
'                    If SG_Es_la_Flor = "SI" Then
'                        AgregarProductoFlor.Bm_Nuevo = True
'                        AgregarProductoFlor.TxtCodigoInterno = TxtCodigo
'                        AgregarProductoFlor.Show 1
'                    Else
'                        AgregarProducto.Bm_Nuevo = True
'                        AgregarProducto.Show 1
'                    End If
'                    If Len(SG_codigo2) > 0 Then
'                        TxtCodigo = SG_codigo2
'                    End If
'            Else
'                    SG_codigo2 = ""
'                    SG_codigo = TxtCodigo.Tag
'                    AgregarProducto.Bm_Nuevo = False
'                    If SG_Es_la_Flor = "SI" Then
'                        AgregarProductoFlor.Show 1
'                    Else
'                        AgregarProducto.Show 1
'                    End If
'                    If Len(SG_codigo2) > 0 Then
'                        TxtCodigo = SG_codigo2
'                    End If
'            End If
'            TxtCodigo.SetFocus
'End Sub

'Private Sub CmdF9_Click()
'    If MsgBox("Quitar todos los productos...", vbQuestion + vbOKCancel) = vbOK Then
'        LvVenta.ListItems.Clear
'        TxtValorDescuento = 0
'        CalculaGrilla
'        TxtCodigo.SetFocus
'    End If
'
'End Sub

'Private Sub CmdOk_Click()
'    If Len(TxtPLU) = 0 Then Exit Sub
'    If CDbl(TxtCant) = 0 Then Exit Sub
'
'    Dim Dp_Total As Double
'    Dim Ip_C As Integer
'
'    Dim Dp_Unitario As Double
'    Sql = ""
'    Filtro = "codigo = '" & TxtPLU & "' "
'    If Sm_UtilizaCodigoInterno = "SI" Then
'                Sp_FiltroCI = "pro_codigo_interno='" & TxtPLU & "' "
'                Sql = "SELECT id,pro_inventariable,descripcion,marca,ubicacion_bodega, " & _
'                        "IF((SELECT lst_id FROM maestro_clientes WHERE rut_cliente='" & TxtRut & "')=0 " & _
'                         "OR ISNULL((SELECT lst_id FROM maestro_clientes WHERE rut_cliente='" & TxtRut & "')) ,precio_venta," & _
'                        "IFNULL((SELECT lsd_precio FROM par_lista_precios_detalle d WHERE lst_id=" & Val(TxtListaPrecio.Tag) & " AND m.id=d.id),precio_venta)) precio_venta," & _
'                        "IFNULL((SELECT AVG(pro_ultimo_precio_compra) FROM pro_stock s WHERE s.rut_emp='" & SP_Rut_Activo & "' AND s.pro_codigo=m.codigo),0) precio_compra, " & _
'                        "(SELECT sto_stock FROM pro_stock WHERE rut_emp='" & SP_Rut_Activo & "' AND pro_codigo=m.codigo) stock,ubicacion_bodega " & _
'                    "FROM maestro_productos m " & _
'                    "WHERE pro_activo='SI' AND m.rut_emp='" & SP_Rut_Activo & "' AND " & Sp_FiltroCI & " UNION "
'    End If
'    Sql = Sql & "SELECT id, pro_inventariable,descripcion,marca,ubicacion_bodega, " & _
'            "IF((SELECT lst_id FROM maestro_clientes WHERE rut_cliente='" & TxtRut & "')=0 " & _
'             "OR ISNULL((SELECT lst_id FROM maestro_clientes WHERE rut_cliente='" & TxtRut & "')) ,precio_venta," & _
'            "IFNULL((SELECT lsd_precio FROM par_lista_precios_detalle d WHERE lst_id=" & Val(TxtListaPrecio.Tag) & " AND m.id=d.id),precio_venta)) precio_venta," & _
'            "IFNULL((SELECT AVG(pro_ultimo_precio_compra) FROM pro_stock s WHERE s.rut_emp='" & SP_Rut_Activo & "' AND s.pro_codigo=m.codigo),0) precio_compra, " & _
'            "(SELECT sto_stock FROM pro_stock WHERE rut_emp='" & SP_Rut_Activo & "' AND pro_codigo=m.codigo) stock,ubicacion_bodega " & _
'          "FROM maestro_productos m " & _
'          "WHERE pro_activo='SI' AND  m.rut_emp='" & SP_Rut_Activo & "' AND " & Filtro & " LIMIT 1"
'    Consulta RsTmp, Sql
'    If RsTmp.RecordCount Then
'
'        'If Sm_PermiteDscto = "NO" And Sm_VentaRapida = "SI" Then
'        'probar
'        If Sm_VentaRapida = "SI" Then
'
'                Dp_Descuento = 0
'                Dp_Unitario = RsTmp!precio_venta - Dp_Descuento
'
'                Dp_Total = Dp_Unitario * Val(CxP(TxtCant))
'                LvDetalle.ListItems.Add , , LvDetalle.ListItems.Count + 1
'                Ip_C = LvDetalle.ListItems.Count
'                LvDetalle.ListItems(Ip_C).SubItems(1) = RsTmp!Id
'                LvDetalle.ListItems(Ip_C).SubItems(2) = TxtCant
'                LvDetalle.ListItems(Ip_C).SubItems(3) = RsTmp!Descripcion
'                LvDetalle.ListItems(Ip_C).SubItems(4) = NumFormat(RsTmp!precio_venta) 'unitario
'                LvDetalle.ListItems(Ip_C).SubItems(5) = NumFormat(Dp_Total) ' cant x unitario
'                LvDetalle.ListItems(Ip_C).SubItems(6) = RsTmp!pro_inventariable
'                LvDetalle.ListItems(Ip_C).SubItems(7) = RsTmp!precio_compra
'                LvDetalle.ListItems(Ip_C).SubItems(8) = RsTmp!precio_venta
'                LvDetalle.ListItems(Ip_C).SubItems(9) = Dp_Descuento
'
'                TxtStock = Val("" & RsTmp!stock)
'                TxtUbicacion = "" & RsTmp!ubicacion_bodega
'                TxtDescripcion = RsTmp!Descripcion
'                TxtCant = "1,000"
'                CboDescuento.ListIndex = 0
'                TxtPLU = ""
'
'                If LvDetalle.ListItems.Count > 0 Then
'                    LvDetalle.ListItems(LvDetalle.ListItems.Count).Selected = True
'                End If
'
'
'
'                ElTotal
'                TxtPLU.SetFocus
'        Else
'
'                '
'                TxtDescripcion = RsTmp!Descripcion
'                TxtPrecioOriginal = RsTmp!precio_venta
'                TxtStock = Val("0" & RsTmp!stock)
'                TxtUbicacion = RsTmp!ubicacion_bodega
'                On Error Resume Next
'                CboDescuento.SetFocus
'
'
'        End If
'
'    End If
'
'
'



Private Sub CmdBuscaCliente_Click()
     LlamaClienteDe = "VD"
    
    ClienteEncontrado = False
    BuscaCliente.Show 1
    TxtRut = SG_codigo
    TxtRut.SetFocus
End Sub

'Private Sub CmdVisorCotizaciones_Click()
'    SG_codigo2 = ""
'    ven_visor_cotizaciones.Show 1
'    If Val(SG_codigo) > 0 Then
'        CargaCotizacion Val(SG_codigo)
'        If Val(SG_codigo2) > 0 Then
'            'Imprimir cotizzcion
'           ' MsgBox "Imprimir cotizacion Nro " & SG_codigo2
'            CargaCotizacion Val(SG_codigo)
'            PrevisualizaCotizacion
'            On Error GoTo CancelaImpesionNV
'            If SP_Rut_Activo = "76.553.302-3" Or SP_Rut_Activo = "76.169.962-8" Then
'            'kyr y alcalde
'
'                    If SG_codigo = "print" Then
'
'                            Dialogo.CancelError = True
'                            Dialogo.ShowPrinter
'                            La_Establecer_Impresora Printer.DeviceName
'
'
'                          '  EstableImpresora Printer.DeviceName
'                            ImprimeCOTIZACION
'                    End If
'            ElseIf SP_Rut_Activo = "78.967.170-2" Then
'                'Plasticos aldunate
'                If SG_codigo = "print" Then
'                    Dialogo.CancelError = True
'                    Dialogo.ShowPrinter
'
'                    If Printer.DeviceName = "TERMICA" Then
'                        ProcCOTIZACION
'                    Else
'                        La_Establecer_Impresora Printer.DeviceName
'                        ImprimeCOTIZACION
'                    End If
'                End If
'
'
'
'            Else
'
'                'Termica
'                If SG_codigo = "print" Then ProcCOTIZACION
'            End If
'
'            GoTo CancelaImpesionNV
'
'        End If
'    End If
'    Exit Sub
'
'CancelaImpesionNV:
'    CmdF5.Enabled = True
'    LimpiaTodo
'End Sub

Private Sub CmdConsultaAbonos_Click()
    Sql = "SELECT pzo_id, pzo_comentario,pzo_fecha,pzo_abono " & _
            "FROM  cta_pozo " & _
            "WHERE pzo_rut='" & TxtRut & "' "


    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, Me.LvListadoAbono, False, True, True, False
    TxtTotalAbonos = NumFormat(TotalizaColumna(LvListadoAbono, "total"))
 ''   LLenaArqueo
End Sub

Private Sub CmdGuardar_Click()
    If TxtMontoAbono = "" Then
        MsgBox "Debe Ingresar Monto Abono...", vbInformation
        TxtMontoAbono.SetFocus
        Exit Sub
    End If
    
    If TxtDescAbono = "" Then
        MsgBox "Debe Ingresar Descripcion Abono...", vbInformation
        TxtDescAbono.SetFocus
        Exit Sub
    End If
    
Sql = "INSERT INTO cta_pozo (pzo_fecha,rut_emp,pzo_cli_pro,pzo_rut,pzo_abono,pzo_comentario) " & _
        "VALUES('" & Fql(Date) & "','" & SP_Rut_Activo & "','CLI','" & TxtRut & "','" & TxtMontoAbono & "','" & TxtDescAbono & "')"

cn.Execute Sql
Previsualiza
End Sub
Private Sub Previsualiza()
   Dim Cx As Double, Cy As Double, Sp_Fecha As String, Ip_Mes As Integer, Dp_S As Double, Sp_Letras As String
    Dim Sp_Neto As String * 12, Sp_IVA As String * 12
    
    Dim Sp_Nro_Comprobante As String
    
    Dim Sp_Empresa As String
    Dim Sp_Giro As String
    Dim Sp_Direccion As String
    
    
    Dim Sp_Nombre As String * 45
    Dim Sp_Rut As String * 15
    Dim Sp_Concepto As String
    
    Dim Sp_Nro_Doc As String * 12
    Dim Sp_Documento As String * 35
    Dim Sp_Valor As String * 15
    Dim Sp_Saldo As String * 15
    Dim Sp_Fpago As String * 24
    Dim Sp_Total As String * 12
    
    Dim Sp_Banco As String * 15
    Dim Sp_NroCheque As String * 10
    Dim Sp_FechaCheque As String * 10
    Dim Sp_ValorCheque As String * 15
    Dim Sp_SumaCheque As String * 15
    
    Dim Sp_Observacion As String * 80
    
    
    Sql = "SELECT giro,direccion,ciudad " & _
         "FROM sis_empresas " & _
         "WHERE rut='" & SP_Rut_Activo & "'"
    Consulta RsTmp2, Sql
    If RsTmp2.RecordCount > 0 Then
        Sp_Giro = RsTmp2!giro
        Sp_Direccion = RsTmp2!direccion & " - " & RsTmp2!ciudad
    End If
    
    
    Sis_Previsualizar.Pic.ScaleMode = vbCentimeters
    Sis_Previsualizar.Pic.BackColor = vbWhite
    Sis_Previsualizar.Pic.AutoRedraw = True
    Sis_Previsualizar.Pic.DrawWidth = 1
    Sis_Previsualizar.Pic.DrawMode = 1
    
    Sis_Previsualizar.Pic.FontName = "Courier New"
    Sis_Previsualizar.Pic.FontSize = 10
   
    Cx = 2
    Cy = 1.9
    Dp_S = 0.17
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.CurrentY = Cy
    Sis_Previsualizar.Pic.FontSize = 16  'tama�o de letra
    Sis_Previsualizar.Pic.FontBold = True
    
    '**
    
    'Sis_Previsualizar.Pic.PaintPicture LoadPicture(App.Path & "\IMG\LAFLOR\LAFLOR.jpg"), Cx + 0.62, 1
    Sis_Previsualizar.Pic.PaintPicture LoadPicture(App.Path & "\REDMAROK.jpg"), Cx + 0.62, 1
    'Printer.PaintPicture LoadPicture(App.Path & "\REDMAROK.jpg"), Cx + 0.62, 1
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S + Dp_S
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S + Dp_S
    '**
    
    Sis_Previsualizar.Pic.Print SP_Empresa_Activa
        
    
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY
  
    Sis_Previsualizar.Pic.FontSize = 10
  
    pos = Sis_Previsualizar.Pic.CurrentY
    Sis_Previsualizar.Pic.Print "R.U.T.   :" & SP_Rut_Activo
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY
    

    Sis_Previsualizar.Pic.CurrentY = pos + 0.5
    Sis_Previsualizar.Pic.CurrentX = Cx + 13
'    If Sm_Cli_Pro = "CLI" Then
'         Sis_Previsualizar.Pic.Print "COMPROBANTE DE INGRESO N�: " & LvListadoAbono.ListItems(i).SubItems(1)
'    Else
'         Sis_Previsualizar.Pic.Print "COMPROBANTE DE EGRESO N�: " & LvListadoAbono.ListItems(1).SubItems(1)
'    End If
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY
 
 
    Sis_Previsualizar.Pic.FontBold = False
    Sis_Previsualizar.Pic.FontSize = 10
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.Print "GIRO     :" & Sp_Giro
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY
    
  
    Sis_Previsualizar.Pic.FontSize = 10
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.Print "DIRECCION:"; Sp_Direccion
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
    
    Sis_Previsualizar.Pic.CurrentX = Cx
    If Sm_Cli_Pro = "CLI" Then
         Sis_Previsualizar.Pic.Print "Recibi de:"
    Else
         Sis_Previsualizar.Pic.Print "Pagado a :"
    End If
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
    Sis_Previsualizar.Pic.FontSize = 10
    pos = Sis_Previsualizar.Pic.CurrentY
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.Print "Nombre:" & TxtRazonSocial ''TxtRazonSocial
    
    Sis_Previsualizar.Pic.CurrentY = pos
    Sis_Previsualizar.Pic.CurrentX = Cx + 14
    Sis_Previsualizar.Pic.Print "Fecha:" & Fql(Date)
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
    
    
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.Print "RUT   :" & TxtRut
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
    
'    Sis_Previsualizar.Pic.CurrentX = Cx
'    Sis_Previsualizar.Pic.FontBold = True
'    Sis_Previsualizar.Pic.Print "CONCEPTO :"
'    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
    
       
'''    'Aqui agregamos los documentos a los que se abonara
'''    Sis_Previsualizar.Pic.FontBold = False
'''    Sp_Nro_Doc = "Nro"
'''    Sp_Documento = "Documento"
'''    RSet Sp_Valor = "Valor"
'''    RSet Sp_Saldo = "Saldo"
'''    Sis_Previsualizar.Pic.CurrentX = Cx
'''    Sis_Previsualizar.Pic.Print Sp_Nro_Doc & " " & Sp_Documento & " " & Sp_Valor & " " & Sp_Saldo
'''    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S - 0.1
'''    For i = 1 To LvDetalle.ListItems.Count
'''        Sp_Nro_Doc = LvDetalle.ListItems(i).SubItems(1)
'''        Sp_Documento = LvDetalle.ListItems(i).SubItems(2)
'''        RSet Sp_Valor = LvDetalle.ListItems(i).SubItems(11)
'''        RSet Sp_Saldo = NumFormat(CDbl(LvDetalle.ListItems(i).SubItems(8)) - (CDbl(LvDetalle.ListItems(i).SubItems(9)) + CDbl(LvDetalle.ListItems(i).SubItems(11))))
'''        Sis_Previsualizar.Pic.CurrentX = Cx
'''        Sis_Previsualizar.Pic.Print Sp_Nro_Doc & " " & Sp_Documento & " " & Sp_Valor & " " & Sp_Saldo
'''        Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S - 0.2
'''    Next
'''    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + 0.2
'''    Sis_Previsualizar.Pic.CurrentX = Cx
'''    Sis_Previsualizar.Pic.FontBold = True
'''   ' Sp_Fpago = CboMPago.Text
'''    pos = Sis_Previsualizar.Pic.CurrentY
'''    Sis_Previsualizar.Pic.FontSize = 10
'''    Sis_Previsualizar.Pic.Print "FORMA DE PAGO:" '& Sp_Fpago & " " & IIf(CboTipoDeposito.ListIndex > -1, Me.CboTipoDeposito.Text, "")
'''    Sis_Previsualizar.Pic.CurrentX = Cx
'''    Sis_Previsualizar.Pic.FontBold = False
'''    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
'''    Cy = Sis_Previsualizar.Pic.CurrentY
'''    Sis_Previsualizar.Pic.FontSize = 9
'''    For i = 1 To LvPagos.ListItems.Count
'''        Sis_Previsualizar.Pic.CurrentY = Cy
'''        Sis_Previsualizar.Pic.CurrentX = Cx
'''        If CDbl(LvPagos.ListItems(i).SubItems(2)) > 0 Then
'''            RSet Sp_VPagos = LvPagos.ListItems(i).SubItems(2)
'''            RSet Sm_Mpagos = LvPagos.ListItems(i).SubItems(1) & _
'''            IIf(Val(LvPagos.ListItems(i).SubItems(5)) > 0, " NRO " & LvPagos.ListItems(i).SubItems(5) & IIf(Len(LvPagos.ListItems(i).SubItems(6)) > 0, " " & LvPagos.ListItems(i).SubItems(6), ""), "") & _
'''            " $" & Sp_VPagos
'''            'Coloca Cx, Cy, Sp_Fagos, False, 10
'''
'''            Sis_Previsualizar.Pic.Print Sm_Mpagos
'''            Cy = Cy + (Dp_S * 2)
'''        End If
'''    Next
'    RSet Sm_Mpagos = "--------------------------------------------"

Dim Sp_DescAbo As String * 50
Dim Sp_FechaAbo As String * 12
Dim Sp_MontoAbo As String * 12

    For i = 1 To LvListadoAbono.ListItems.Count
        Sp_DescAbo = LvListadoAbono.ListItems(i).SubItems(1)
        Sp_FechaAbo = LvListadoAbono.ListItems(i).SubItems(2)
        Sp_MontoAbo = LvListadoAbono.ListItems(i).SubItems(3)
        'RSet Sp_Saldo = NumFormat(CDbl(LvDetalle.ListItems(i).SubItems(8)) - (CDbl(LvDetalle.ListItems(i).SubItems(9)) + CDbl(LvDetalle.ListItems(i).SubItems(11))))
        Sis_Previsualizar.Pic.CurrentX = Cx
        Sis_Previsualizar.Pic.Print Sp_DescAbo & " " & Sp_FechaAbo & " " & Sp_MontoAbo  ' & " " & Sp_Saldo
'        Sis_Previsualizar.Pic.CurrentX = Cx
'        Sis_Previsualizar.Pic.Print Sp_DescAbo
'        Sis_Previsualizar.Pic.Print Sp_FechaAbo
        Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S - 0.3
        
    Next

    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.Print Sm_Mpagos
    Sis_Previsualizar.Pic.FontBold = True
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
    Sis_Previsualizar.Pic.CurrentX = Cx + 5
    Sis_Previsualizar.Pic.Print "Monto Abonado:" & TxtMontoAbono
    Sis_Previsualizar.Pic.CurrentX = Cx + 5
    Sis_Previsualizar.Pic.Print "Total Monto Abonado:" & TxtTotalAbonos
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S + Dp_S
    
    
    Sis_Previsualizar.Pic.FontBold = False
    
   ' If CboMPago.ItemData(CboMPago.ListIndex) = 3 Or CboMPago.ItemData(CboMPago.ListIndex) = 4 Then
   ''     Sis_Previsualizar.Pic.CurrentX = Cx
   '     Sis_Previsualizar.Pic.Print "NRO OPERACION:" & txtNroOperacion
   '     Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
   ' End If
   finpago = Sis_Previsualizar.Pic.CurrentY
    
'    'Aqui detallaremos los cheques
'    If LvCheques.ListItems.Count > 0 Then
'        iniciocheque = Sis_Previsualizar.Pic.CurrentY
'        Sis_Previsualizar.Pic.CurrentX = Cx
'        Sis_Previsualizar.Pic.FontBold = True
'        Sis_Previsualizar.Pic.Print "DETALLE DE CHEQUES"
'        Sis_Previsualizar.Pic.FontBold = False
'        Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
'        Sp_FechaCheque = "Fecha"
'        Sp_NroCheque = "Nro"
'        RSet Sp_ValorCheque = "Valor"
'        Sp_Banco = "Banco"
'        Sis_Previsualizar.Pic.CurrentX = Cx
'        Sis_Previsualizar.Pic.Print Sp_Banco & " " & Sp_NroCheque & " " & Sp_FechaCheque & "       " & Sp_ValorCheque
'        Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S - 0.2
'        tcheques = 0
'        For i = 1 To LvCheques.ListItems.Count
'            Sis_Previsualizar.Pic.CurrentX = Cx
'            Sp_FechaCheque = LvCheques.ListItems(i)
'            Sp_NroCheque = LvCheques.ListItems(i).SubItems(1)
'            RSet Sp_ValorCheque = LvCheques.ListItems(i).SubItems(2)
'            Sp_Banco = LvCheques.ListItems(i).SubItems(6)
'            Sis_Previsualizar.Pic.Print Sp_Banco & " " & Sp_NroCheque & " " & Sp_FechaCheque & "       " & Sp_ValorCheque
'            Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S - 0.2
'            tcheques = tcheques + CDbl(LvCheques.ListItems(i).SubItems(2))
'        Next
'        Sp_FechaCheque = ""
'        Sp_NroCheque = ""
'        RSet Sp_ValorCheque = NumFormat(tcheques)
'        Sp_Banco = ""
'        Sis_Previsualizar.Pic.CurrentX = Cx
'        Sis_Previsualizar.Pic.FontBold = True
'        Sis_Previsualizar.Pic.Print Sp_Banco & " " & Sp_NroCheque & " " & Sp_FechaCheque & "       " & Sp_ValorCheque
'        fincheque = Sis_Previsualizar.Pic.CurrentY
'        Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
'    End If
        
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.FontBold = True
    posobs = Sis_Previsualizar.Pic.CurrentY
'    Sis_Previsualizar.Pic.Print "Monto Abonado:" & TxtMontoAbono
   Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S + 0.5
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.Print "OBSERVACIONES:" & TxtDescAbono
        Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
    Sis_Previsualizar.Pic.FontBold = False
     
    Sis_Previsualizar.Pic.CurrentX = Cx + 2
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
    pos = Sis_Previsualizar.Pic.CurrentY
    Sis_Previsualizar.Pic.CurrentY = pos + 1
    Sis_Previsualizar.Pic.CurrentX = Cx
    
    Sis_Previsualizar.Pic.Print "CONTABILIDAD"
        
    Sis_Previsualizar.Pic.CurrentY = pos + 1
    Sis_Previsualizar.Pic.CurrentX = Cx + 6
    Sis_Previsualizar.Pic.Print "V� B� CAJA"
   
   
   
    Sis_Previsualizar.Pic.CurrentY = pos + 1
    Sis_Previsualizar.Pic.CurrentX = Cx + 12
    Sis_Previsualizar.Pic.Print "NOMBRE, RUT Y FIRMA"
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
    
    
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY - (Dp_S * 6)
    Sis_Previsualizar.Pic.CurrentX = Cx + 12
    Sis_Previsualizar.Pic.Print "RECIBI CONFORME"
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
   ' Sis_Previsualizar.pic.DrawMode = 1
    'Sis_Previsualizar.pic.DrawWidth = 3
    
    Sis_Previsualizar.Pic.Line (1.5, 1)-(20, 4.5), , B 'Rectangulo Encabezado y N� Comprobante
    
    Sis_Previsualizar.Pic.Line (1.5, 4.5)-(20, 6.2), , B 'Pagado o Recibido del cliente o proveedor
    
    If LvCheques.ListItems.Count > 0 Then
        Sis_Previsualizar.Pic.Line (1.5, 6.2)-(20, finpago), , B  'Concepto - documentos pagados o recibidos y forma de pago
    Else
        Sis_Previsualizar.Pic.Line (1.5, 6.2)-(20, finpago + 0.15), , B 'Concepto - documentos pagados o recibidos y forma de pago
    End If
    
    Sis_Previsualizar.Pic.Line (13.5, 6.2)-(16.8, posobs), , B  'columnas de los pagos en efectivo
    'Sis_Previsualizar.pic.Line (13.5, 6.2)-(16.8, 9.37), , B  'columnas de los pagos en efectivo
    
    If LvCheques.ListItems.Count > 0 Then
        Sis_Previsualizar.Pic.Line (1.5, iniciocheque)-(20, posobs), , B  'documentos pagados y forma de pago
        Sis_Previsualizar.Pic.Line (13.5, iniciocheque)-(16.8, posobs), , B  'columnas de los pagos con cheques
    End If
    
    Sis_Previsualizar.Pic.Line (1.5, posobs)-(20, posobs + 1.5), , B 'Observaciones
    Sis_Previsualizar.Pic.Line (1.5, posobs + 1.5)-(20, posobs + 1.5 + 2), , B 'Pie del comprobante
        
    pos = Sis_Previsualizar.Pic.CurrentY
    
 '   Sis_Previsualizar.Pic.Height = pos * 0.04

    Sis_Previsualizar.Show 1
End Sub

Private Sub CmdRetornar_Click()
Unload Me
End Sub

'Private Sub Form_KeyUp(KeyCode As Integer, Shift As Integer)
''    If KeyCode = vbKeyF5 Then
''        If CmdF5.Enabled Then CmdF5_Click
''    End If
''
'    If KeyCode = vbKeyF8 Then
'        CmdF8_Click
'    End If
'
'    If KeyCode = vbKeyF7 Then
'        CmdF7_Click
'    End If
'    If KeyCode = vbKeyF9 Then
'        CmdF9_Click
'    End If
'End Sub
Private Sub Form_Load()
    Aplicar_skin Me
    Centrar Me, False
        
    If SG_ImpresoraFiscalBixolon = "SI" Then
                '4-2 2016
                'Cargar Formulario de Impresora Fiscal Epson, pero no mostrar el formulario
                'Pero tenerlo cargado para emitir boletas cuando sea necesario
                If SG_Marca_Impresora_Fiscal = "   xEPSON" Then
                    
                         With Epson.CmbCom
                            .AddItem "Com1"
                    '        .AddItem "Com2"
                    '        .AddItem "Com3"
                    '        .AddItem "Com4"
                    '        .AddItem "Com5"
                    '        .AddItem "Com6"
                    '        .AddItem "Com7"
                    '        .AddItem "Com8"
                    '        .AddItem "Com9"
                    '        .AddItem "Com10"
                    '        .AddItem "Com11"
                    '        .AddItem "Com12"
                    '        .AddItem "Com13"
                    '        .AddItem "Com14"
                    '        .AddItem "Com15"
                    '        .AddItem "Com16"
                        End With
                        
                        '*** DEFINE VELOCIDADES PARA PUERTO ***
                        With Epson.CmbBaudRate
                            
                            .AddItem "1200"
                            .AddItem "2400"
                            .AddItem "4800"
                            .AddItem "9600"
                            .AddItem "19200"
                            .AddItem "38400"
                        End With
                        Epson.CmbCom.ListIndex = 0
                        Epson.CmbBaudRate.ListIndex = 3
                        Epson.AbrePuerto
                       ' MsgBox "Puerto Abierto"
                End If
    End If
    
    If SP_Rut_Activo = "78.967.170-2" Then
        'solo comercial aldunate
        Sp_MatrizDescuentos = "SI"
    End If
    
    
    
End Sub

'
'Private Sub LvVenta_DblClick()
'    If LvVenta.SelectedItem Is Nothing Then Exit Sub
'    With LvVenta
'        TxtCodigo.Tag = LvVenta.SelectedItem
'        TxtCodigo = .SelectedItem.SubItems(1)
'        Me.TxtDescripcion = .SelectedItem.SubItems(2)
'        TxtCantidad = .SelectedItem.SubItems(3)
'        TxtPrecio = .SelectedItem.SubItems(4)
'        TxtTotalLinea = .SelectedItem.SubItems(5)
'        TxtStock = .SelectedItem.SubItems(6)
'        TxtCodigo.SetFocus
'        LvVenta.ListItems.Remove .SelectedItem.Index
'    End With
'    CalculaGrilla
'    NormalizaColores
'    TxtCodigo.SetFocus
'
'End Sub
Private Sub Timer1_Timer()
    On Error Resume Next
    TxtCodigo.SetFocus
    Timer1.Enabled = False
End Sub




Private Sub TxtDescAbono_GotFocus()
    En_Foco TxtDescAbono
End Sub

Private Sub TxtDescAbono_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub TxtMontoAbono_GotFocus()
 En_Foco TxtMontoAbono
End Sub

Private Sub TxtMontoAbono_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
End Sub

Private Sub TxtRut_GotFocus()
    En_Foco TxtRut
End Sub

Private Sub TxtRut_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
    On Error Resume Next
    If KeyAscii = 13 Then SendKeys ("{TAB}")
    If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtRut_KeyUp(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF1 Then CmdBuscaCliente_Click
End Sub

Private Sub TxtRut_LostFocus()
    If Len(TxtRut.Text) = 0 Then
        Me.TxtRazonSocial.Text = ""
        Exit Sub
    End If
    If ClienteEncontrado Then
    Else       ' TxtRut.SetFocus
    End If
End Sub

Private Sub TxtRut_Validate(Cancel As Boolean)
    TxtListaPrecio = "NORMAL"
   '' TxtListaPrecio.Tag = 0
    TxtRazonSocial.Tag = ""
    If Len(TxtRut.Text) = 0 Then Exit Sub
'    If CboDocVenta.ListIndex = -1 Then
'        MsgBox "Seleccione documento...", vbInformation
'        On Error Resume Next
'        CboDocVenta.SetFocus
'        Exit Sub
'    End If
    TxtRut.Text = Replace(TxtRut.Text, ".", "")
    TxtRut.Text = Replace(TxtRut.Text, "-", "")
    Respuesta = VerificaRut(TxtRut.Text, NuevoRut)
   
    If Respuesta = True Then
        Me.TxtRut.Text = NuevoRut
        'Buscar el cliente
        Sql = "SELECT rut_cliente,nombre_rsocial,giro,direccion,comuna,fono,email, " & _
                "IFNULL(lst_nombre,'LISTA PRECIO GENERAL') listaprecios,m.lst_id,ven_id,cli_monto_credito " & _
              "FROM maestro_clientes m " & _
              "INNER JOIN par_asociacion_ruts  a ON m.rut_cliente=a.rut_cli " & _
              "LEFT JOIN par_lista_precios l USING(lst_id) " & _
              "WHERE habilitado='SI' AND a.rut_emp='" & SP_Rut_Activo & "' AND rut_cliente = '" & Me.TxtRut.Text & "'"
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
                'Cliente encontrado
            With RsTmp
                TxtMontoCredito = !cli_monto_credito
                TxtListaPrecio = !ListaPrecios
               '' TxtListaPrecio.Tag = !lst_id
                TxtRut.Text = !rut_cliente
                TxtGiro = !giro
                TxtDireccion = !direccion
                txtComuna = !comuna
                txtFono = !fono
                TxtEmail = !Email
                TxtRazonSocial.Text = !nombre_rsocial
               
                ClienteEncontrado = True
              
                
                Sql = "SELECT a.lst_id,lst_nombre " & _
                      "FROM par_asociacion_lista_precios  a " & _
                      "INNER JOIN par_lista_precios l USING(lst_id) " & _
                      "WHERE l.rut_emp='" & SP_Rut_Activo & "' AND cli_rut='" & TxtRut & "'"
                Consulta RsTmp2, Sql
               '' TxtListaPrecio.Tag = !lst_id
               ''  TxtListaPrecio = "LISTA DE PRECIOS PRINCIPAL"
                If RsTmp2.RecordCount > 0 Then
                    TxtListaPrecio = RsTmp2!lst_nombre
                    TxtListaPrecio.Tag = RsTmp2!lst_id
                End If
                
                If Val(TxtMontoCredito) > 0 Then
                    'Debemos consultar el saldo de credito disponible del cliente.
                    TxtCupoUtilizado = ConsultaSaldoCliente(TxtRut)
                    SkCupoCredito = NumFormat(Val(TxtMontoCredito) - Val(TxtCupoUtilizado))
                End If
                CboSucursal.Clear
                LLenarCombo CboSucursal, "CONCAT(suc_ciudad,' ',suc_direccion,' ',suc_contacto)", "suc_id", "par_sucursales", "suc_activo='SI' AND rut_cliente='" & TxtRut & "'", "suc_id"
                CboSucursal.AddItem "CASA MATRIZ"
                CboSucursal.ItemData(CboSucursal.ListCount - 1) = 0
                CboSucursal.ListIndex = CboSucursal.ListCount - 1
                
                
            End With
                
            If bm_SoloVistaDoc Then Exit Sub
                                
        
        Else
                'Cliente no existe aun �crearlo??
                Respuesta = MsgBox("Cliente no encontrado..." & Chr(13) & _
                "     �Desea Crear?    ", vbYesNo + vbQuestion)
                If Respuesta = 6 Then
                    'crear
                    SG_codigo = ""
                    AccionCliente = 4
                    AgregoCliente.TxtRut.Text = Me.TxtRut.Text
                    
                    AgregoCliente.TxtRut.Locked = True
                    ClienteEncontrado = False
                    AgregoCliente.Timer1.Enabled = True
            
                    AgregoCliente.Show 1
                    If ClienteEncontrado = True Then
                        TxtRut_Validate False
                       
                        TxtCodigo.SetFocus
                    Else
                        TxtRut_Validate True
                    End If
                Else
                    'volver al rut
                    ClienteEncontrado = False
                    Me.TxtRut.Text = ""
                    On Error Resume Next
                    SendKeys "+{TAB}" ' Shift TAB retrocede al control anterior segun el orden del tabindex
                End If
        
          
        End If
       
    Else
        Me.TxtRut.Text = ""
        On Error Resume Next
        SendKeys "+{TAB}" ' Shift TAB retrocede al control anterior segun el orden del tabindex
    End If
    Exit Sub
CancelaImpesionFacturacion:
    'No imprime
    Unload Me
End Sub



Private Sub EstableImpresora(NombreImpresora As String)
   'Seteamos al impresora.
    Dim impresora As Printer
    ' Establece la impresora que se utilizar� para imprimir
    'Recorremos el objeto Printers
    For Each impresora In Printers
        'Si es igual al contenido se�alado en el combobox
        If UCase(impresora.DeviceName) = UCase(NombreImpresora) Then
            'Lo seteamos as�.
            Set Printer = impresora
            Exit For
        End If
    Next

End Sub
Private Function La_Establecer_Impresora(ByVal NamePrinter As String) As Boolean
  
On Error GoTo errSub
  
     
  
    'Variable de referencia
  
    Dim obj_Impresora As Object
  
     
  
    'Creamos la referencia
  
    Set obj_Impresora = CreateObject("WScript.Network")
  
        obj_Impresora.setdefaultprinter NamePrinter
  
     
  
    Set obj_Impresora = Nothing
  
         
  
        'La funci�n devuelve true y se cambi� con �xito
  
       La_Establecer_Impresora = True
  
  '      MsgBox "La impresora se cambi� correctamente", vbInformation
  
    Exit Function
  
     
  
     
  
'Error al cambiar la impresora
  
errSub:
  
If Err.Number = 0 Then Exit Function
  
   La_Establecer_Impresora = False
  
   MsgBox "error: " & Err.Number & Chr(13) & "Description: " & Err.Description
  
   On Error GoTo 0
  
End Function

