VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form caj_apertura 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Apertura de caja"
   ClientHeight    =   3075
   ClientLeft      =   7050
   ClientTop       =   4200
   ClientWidth     =   8070
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3075
   ScaleWidth      =   8070
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox txtSucursal 
      Height          =   285
      Left            =   3285
      Locked          =   -1  'True
      TabIndex        =   8
      Text            =   "Text1"
      Top             =   1695
      Width           =   4170
   End
   Begin VB.TextBox txtEfectivoInicial 
      Alignment       =   1  'Right Justify
      Height          =   285
      Left            =   3285
      Locked          =   -1  'True
      TabIndex        =   7
      Text            =   "0"
      Top             =   1410
      Width           =   1530
   End
   Begin VB.Frame Frame1 
      Caption         =   "Datos apertura caja"
      Height          =   2610
      Left            =   315
      TabIndex        =   0
      Top             =   240
      Width           =   7455
      Begin VB.CommandButton CmdAbrir 
         Caption         =   "Comenzar"
         Height          =   390
         Left            =   5505
         TabIndex        =   9
         Top             =   1800
         Width           =   1665
      End
      Begin MSComCtl2.DTPicker DtFecha 
         Height          =   285
         Left            =   2970
         TabIndex        =   6
         Top             =   885
         Width           =   1245
         _ExtentX        =   2196
         _ExtentY        =   503
         _Version        =   393216
         Format          =   273416193
         CurrentDate     =   41573
      End
      Begin VB.TextBox txtUsuario 
         BackColor       =   &H8000000F&
         Height          =   285
         Left            =   2970
         Locked          =   -1  'True
         TabIndex        =   5
         Text            =   "Text1"
         Top             =   600
         Width           =   4125
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   240
         Left            =   1320
         OleObjectBlob   =   "caj_apertura.frx":0000
         TabIndex        =   3
         Top             =   1185
         Width           =   1575
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   240
         Left            =   1500
         OleObjectBlob   =   "caj_apertura.frx":007E
         TabIndex        =   2
         Top             =   630
         Width           =   1395
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   270
         Left            =   2100
         OleObjectBlob   =   "caj_apertura.frx":00FC
         TabIndex        =   1
         Top             =   900
         Width           =   765
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
         Height          =   240
         Left            =   1485
         OleObjectBlob   =   "caj_apertura.frx":0164
         TabIndex        =   4
         Top             =   1485
         Width           =   1395
      End
   End
   Begin VB.Timer Timer1 
      Interval        =   10
      Left            =   135
      Top             =   615
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   105
      OleObjectBlob   =   "caj_apertura.frx":01D2
      Top             =   45
   End
End
Attribute VB_Name = "caj_apertura"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub CmdAbrir_Click()
    'Comenzar nueva caja
    If LG_id_Caja = 0 Then
    
        '
    
    
    
        'Insertar nuevo registro de caja
        
        '29-10-2015 _
        Verificar si es que se abrio otra caja en este momento
         Sql = "SELECT caj_id,sue_id,caj_fecha_apertura,caj_estado,caj_monto_inicial,usu_nombre " & _
                "FROM ven_caja c " & _
                "JOIN sis_usuarios u ON c.usu_id=u.usu_id " & _
                "WHERE emp_id=" & IG_id_Empresa & " AND sue_id=" & IG_id_Sucursal_Empresa & " AND caj_estado='ABIERTA'"
        
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
            'Identificamos la caja abierta acualmente
            LG_id_Caja = RsTmp!caj_id
            GoTo yaHabiaUnaAbierta
        End If
        '_____________________________________/
        
        
        LG_id_Caja = UltimoNro("ven_caja", "ven_id")
        On Error GoTo AbreCaja
        cn.BeginTrans
            Sql = "INSERT INTO ven_caja (caj_id,emp_id,sue_id,caj_fecha_apertura,caj_monto_inicial,caj_estado,usu_id) VALUES (" & _
                        LG_id_Caja & "," & IG_id_Empresa & "," & IG_id_Sucursal_Empresa & ",'" & Fql(DtFecha) & " " & Time & "'," & CDbl(txtEfectivoInicial) & ",'ABIERTA'," & LogIdUsuario & ")"
            cn.Execute Sql
        cn.CommitTrans
    End If
yaHabiaUnaAbierta:
    
    Unload Me
    Exit Sub
AbreCaja:
    LG_id_Caja = 0
    MsgBox "Error al intentar abrir caja " & newline & Err.Description & vbNewLine & Err.Number
    cn.RollbackTrans
End Sub

Private Sub Form_Load()
    Aplicar_skin Me
    Centrar Me
    If LG_id_Caja = 0 Then
        CmdAbrir.Caption = "CONTINUAR"
        Me.txtEfectivoInicial.Locked = False
    Else
        Me.txtEfectivoInicial.Locked = True
    End If
End Sub

Private Sub Timer1_Timer()
    On Error Resume Next
    CmdAbrir.SetFocus
    Timer1.Enabled = False
End Sub
Private Sub txtEfectivoInicial_GotFocus()
    En_Foco txtEfectivoInicial
End Sub

Private Sub txtEfectivoInicial_Validate(Cancel As Boolean)
    If Val(txtEfectivoInicial) = 0 Then txtEfectivoInicial = "0"
End Sub

Private Sub txtSucursal_Validate(Cancel As Boolean)
txtSucursal = Replace(txtSucursal, "'", "")
End Sub
