VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "MSDATGRD.OCX"
Begin VB.Form Kardec 
   Caption         =   "Kardex"
   ClientHeight    =   8985
   ClientLeft      =   120
   ClientTop       =   450
   ClientWidth     =   12975
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   8985
   ScaleWidth      =   12975
   StartUpPosition =   2  'CenterScreen
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   0
      OleObjectBlob   =   "Kardec.frx":0000
      Top             =   8160
   End
   Begin VB.Frame Frame1 
      Caption         =   "Salidas"
      Height          =   7575
      Left            =   120
      TabIndex        =   6
      Top             =   1200
      Width           =   12375
      Begin MSDataGridLib.DataGrid GridCompras 
         Height          =   2055
         Left            =   240
         TabIndex        =   10
         Top             =   5400
         Width           =   11895
         _ExtentX        =   20981
         _ExtentY        =   3625
         _Version        =   393216
         AllowUpdate     =   -1  'True
         HeadLines       =   1
         RowHeight       =   15
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Caption         =   "Compras"
         ColumnCount     =   2
         BeginProperty Column00 
            DataField       =   ""
            Caption         =   ""
            BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
               Type            =   0
               Format          =   ""
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   13322
               SubFormatType   =   0
            EndProperty
         EndProperty
         BeginProperty Column01 
            DataField       =   ""
            Caption         =   ""
            BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
               Type            =   0
               Format          =   ""
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   13322
               SubFormatType   =   0
            EndProperty
         EndProperty
         SplitCount      =   1
         BeginProperty Split0 
            BeginProperty Column00 
            EndProperty
            BeginProperty Column01 
            EndProperty
         EndProperty
      End
      Begin MSDataGridLib.DataGrid GridOt 
         Height          =   1695
         Left            =   240
         TabIndex        =   9
         Top             =   3600
         Width           =   11895
         _ExtentX        =   20981
         _ExtentY        =   2990
         _Version        =   393216
         AllowUpdate     =   -1  'True
         HeadLines       =   1
         RowHeight       =   15
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Caption         =   "OT's"
         ColumnCount     =   2
         BeginProperty Column00 
            DataField       =   ""
            Caption         =   ""
            BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
               Type            =   0
               Format          =   ""
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   13322
               SubFormatType   =   0
            EndProperty
         EndProperty
         BeginProperty Column01 
            DataField       =   ""
            Caption         =   ""
            BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
               Type            =   0
               Format          =   ""
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   13322
               SubFormatType   =   0
            EndProperty
         EndProperty
         SplitCount      =   1
         BeginProperty Split0 
            BeginProperty Column00 
            EndProperty
            BeginProperty Column01 
            EndProperty
         EndProperty
      End
      Begin MSDataGridLib.DataGrid GridVD 
         Height          =   1695
         Left            =   240
         TabIndex        =   8
         Top             =   240
         Width           =   11895
         _ExtentX        =   20981
         _ExtentY        =   2990
         _Version        =   393216
         AllowUpdate     =   0   'False
         HeadLines       =   1
         RowHeight       =   15
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Caption         =   "Ventas Directas [FACTURAS]"
         ColumnCount     =   2
         BeginProperty Column00 
            DataField       =   ""
            Caption         =   ""
            BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
               Type            =   0
               Format          =   ""
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   13322
               SubFormatType   =   0
            EndProperty
         EndProperty
         BeginProperty Column01 
            DataField       =   ""
            Caption         =   ""
            BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
               Type            =   0
               Format          =   ""
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   13322
               SubFormatType   =   0
            EndProperty
         EndProperty
         SplitCount      =   1
         BeginProperty Split0 
            BeginProperty Column00 
            EndProperty
            BeginProperty Column01 
            EndProperty
         EndProperty
      End
      Begin MSDataGridLib.DataGrid GridVDbol 
         Height          =   1575
         Left            =   240
         TabIndex        =   11
         Top             =   1920
         Width           =   11895
         _ExtentX        =   20981
         _ExtentY        =   2778
         _Version        =   393216
         AllowUpdate     =   0   'False
         HeadLines       =   1
         RowHeight       =   15
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Caption         =   "Ventas Directas [BOLETAS]"
         ColumnCount     =   2
         BeginProperty Column00 
            DataField       =   ""
            Caption         =   ""
            BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
               Type            =   0
               Format          =   ""
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   13322
               SubFormatType   =   0
            EndProperty
         EndProperty
         BeginProperty Column01 
            DataField       =   ""
            Caption         =   ""
            BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
               Type            =   0
               Format          =   ""
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   13322
               SubFormatType   =   0
            EndProperty
         EndProperty
         SplitCount      =   1
         BeginProperty Split0 
            BeginProperty Column00 
            EndProperty
            BeginProperty Column01 
            EndProperty
         EndProperty
      End
   End
   Begin VB.Frame Frame3 
      Caption         =   "Productoso del documento"
      Height          =   975
      Left            =   480
      TabIndex        =   0
      Top             =   120
      Width           =   11175
      Begin VB.CommandButton CmdRevisa 
         Caption         =   "Revisar"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   8280
         TabIndex        =   7
         Top             =   360
         Width           =   1575
      End
      Begin VB.CommandButton CmdBuscaProducto 
         Caption         =   "Buscar"
         Height          =   375
         Left            =   480
         TabIndex        =   4
         Top             =   360
         Width           =   735
      End
      Begin VB.TextBox TxtDescripcion 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   3960
         Locked          =   -1  'True
         TabIndex        =   3
         Top             =   480
         Width           =   3975
      End
      Begin VB.TextBox TxtMarca 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   2640
         Locked          =   -1  'True
         TabIndex        =   2
         Top             =   480
         Width           =   1335
      End
      Begin VB.TextBox TxtCodigo 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   1320
         TabIndex        =   1
         Top             =   480
         Width           =   1335
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel6 
         Height          =   255
         Left            =   1440
         OleObjectBlob   =   "Kardec.frx":0234
         TabIndex        =   5
         Top             =   240
         Width           =   9615
      End
   End
End
Attribute VB_Name = "Kardec"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub CmdBuscaProducto_Click()
    RegSeleccionado = False
    DesdeCompra = False
    EstadoOT = "KARDEC"
    BuscaProducto.Show 1
    If RegSeleccionado = True Then
        'Me.TxtPRecio.SetFocus
    End If
    
End Sub


Private Sub CmdRevisa_Click()
    Dim RsRevisaVD As Recordset
    
    
    Sql = "SELECT doc_venta.nombre_cliente,Vd_Materiales.Fecha,Vd_Materiales.Codigo,Vd_Materiales.No_Factura,Vd_Materiales.Unidades,Vd_Materiales.Precio_final,Vd_Materiales.Subtotal FROM Vd_Materiales Inner Join doc_venta on Vd_Materiales.No_Factura = doc_venta.NO_Documento WHERE Vd_Materiales.Codigo = '" & Me.TxtCodigo & "' AND doc_venta.tipo_doc = 'FACTURA'"
    
    'Sql = "SELECT Fecha,Codigo,Unidades,Precio_Final,Subtotal FROM Vd_Materiales WHERE Codigo = '" & Me.TxtCodigo & "' IN (SELECT"
    paso = AbreConsulta(RsRevisaVD, Sql)
    Set GridVD.DataSource = RsRevisaVD
    
    
    Dim RsRevisaVDBol As Recordset
    Sql = "SELECT doc_venta.nombre_cliente,vd_materiales.fecha,vd_materiales.codigo,vd_materiales.no_boleta,vd_materiales.unidades,vd_materiales.precio_final,vd_materiales.subtotal FROM vd_materiales Inner Join doc_venta on Vd_Materiales.No_Boleta = doc_venta.no_documento WHERE vd_materiales.codigo = '" & Me.TxtCodigo & "' AND doc_venta.tipo_doc = 'BOLETA'"
    paso = AbreConsulta(RsRevisaVDBol, Sql)
    Set GridVDbol.DataSource = RsRevisaVDBol
    
    Dim RsRevisaOT As Recordset
    Sql = "SELECT ot.fecha,ot_materiales.codigo,ot_materiales.ordentrabajo,ot_materiales.unidades,ot_materiales.precio_final,ot_materiales.subtotal,ot.nombre_cliente FROM ot_materiales Inner Join ot on ot_materiales.ordentrabajo = Ot.No_Orden  WHERE ot_materiales.codigo = '" & Me.TxtCodigo & "'"
    paso = AbreConsulta(RsRevisaOT, Sql)
    Set GridOt.DataSource = RsRevisaOT
    
    Dim RsRevisaCpra As Recordset
    Sql = "SELECT fecha_compra, documento_ingreso,numero_documento, cod_producto,unidades,precio_compra,total,nombre_proveedor FROM compra_stock_repuestos WHERE cod_producto = '" & TxtCodigo.Text & "'"
    paso = AbreConsulta(RsRevisaCpra, Sql)
    Set GridCompras.DataSource = RsRevisaCpra
    
    
End Sub


Private Sub Form_Load()
    Aplicar_skin Me
End Sub
