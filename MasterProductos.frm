VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form MasterProductos 
   Appearance      =   0  'Flat
   BackColor       =   &H80000005&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Maestro de Productos y Servicios"
   ClientHeight    =   8535
   ClientLeft      =   2955
   ClientTop       =   1680
   ClientWidth     =   15180
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   8535
   ScaleWidth      =   15180
   Begin VB.ComboBox CboTipo 
      Height          =   315
      ItemData        =   "MasterProductos.frx":0000
      Left            =   13230
      List            =   "MasterProductos.frx":000D
      Style           =   2  'Dropdown List
      TabIndex        =   51
      Top             =   1260
      Width           =   1815
   End
   Begin MSComDlg.CommonDialog Dialogo 
      Left            =   300
      Top             =   165
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.PictureBox PicTipo 
      BackColor       =   &H00FFFF80&
      Height          =   3960
      Left            =   8820
      ScaleHeight     =   3900
      ScaleWidth      =   4275
      TabIndex        =   39
      Top             =   1590
      Visible         =   0   'False
      Width           =   4335
      Begin VB.CommandButton Command2 
         Caption         =   "Por defecto"
         Height          =   210
         Left            =   3255
         TabIndex        =   44
         Top             =   3660
         Width           =   975
      End
      Begin VB.CommandButton CmdAplicaVistaCol 
         Caption         =   "Aplicar"
         Height          =   225
         Left            =   60
         TabIndex        =   43
         Top             =   3660
         Width           =   960
      End
      Begin VB.CommandButton cmdCierraTipos 
         Caption         =   "x"
         Height          =   195
         Left            =   4005
         TabIndex        =   40
         Top             =   15
         Width           =   270
      End
      Begin MSComctlLib.ListView LvColumnasVisibles 
         Height          =   3330
         Left            =   60
         TabIndex        =   41
         Top             =   285
         Width           =   4185
         _ExtentX        =   7382
         _ExtentY        =   5874
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         Checkboxes      =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   3
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Width           =   1058
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Nombre"
            Object.Width           =   5292
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "N109"
            Text            =   "Ancho Por defecto"
            Object.Width           =   0
         EndProperty
      End
      Begin VB.Label Label3 
         BackStyle       =   0  'Transparent
         Caption         =   "Seleccione columnas visibles"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000E&
         Height          =   195
         Left            =   390
         TabIndex        =   42
         Top             =   15
         Width           =   3165
      End
      Begin VB.Shape Shape2 
         BackColor       =   &H80000002&
         BackStyle       =   1  'Opaque
         FillColor       =   &H00FF0000&
         Height          =   240
         Left            =   0
         Top             =   15
         Width           =   4305
      End
   End
   Begin VB.PictureBox FrmDetalles 
      BackColor       =   &H00C0C000&
      Height          =   1170
      Left            =   1215
      ScaleHeight     =   1110
      ScaleWidth      =   8175
      TabIndex        =   29
      Top             =   1335
      Visible         =   0   'False
      Width           =   8235
      Begin VB.CommandButton Command3 
         Caption         =   "+"
         Height          =   210
         Left            =   7530
         TabIndex        =   50
         Top             =   15
         Width           =   270
      End
      Begin VB.CommandButton CmdMinimizaProveedor 
         Caption         =   "__"
         Height          =   210
         Left            =   7170
         TabIndex        =   49
         Top             =   15
         Width           =   270
      End
      Begin VB.CommandButton CmdTodosLosProveedores 
         Caption         =   "Ver Todos los codigos de proveedor"
         Height          =   285
         Left            =   4365
         TabIndex        =   45
         Top             =   750
         Width           =   3390
      End
      Begin VB.TextBox txtRutProveedor 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1020
         TabIndex        =   35
         Top             =   360
         Width           =   1125
      End
      Begin VB.CommandButton cmdRutProveedor 
         Caption         =   "Proveedor"
         Height          =   270
         Left            =   90
         TabIndex        =   34
         Top             =   390
         Width           =   900
      End
      Begin VB.TextBox txtNombreProveedor 
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2130
         Locked          =   -1  'True
         TabIndex        =   33
         Top             =   360
         Width           =   5610
      End
      Begin VB.CommandButton VerConCodigoProveedor 
         Caption         =   "Ver del proveedor seleccionado"
         Height          =   285
         Left            =   1005
         TabIndex        =   32
         Top             =   750
         Width           =   3195
      End
      Begin VB.CommandButton CmdCierraDetalleCheques 
         Caption         =   "x"
         Height          =   255
         Left            =   7875
         TabIndex        =   30
         ToolTipText     =   "Cierra detalle de cheques"
         Top             =   -15
         Width           =   255
      End
      Begin VB.Label Label1 
         BackStyle       =   0  'Transparent
         Caption         =   "Seleccion de proveedor"
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   345
         TabIndex        =   31
         Top             =   30
         Width           =   1815
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H00FF0000&
         BackStyle       =   1  'Opaque
         Height          =   255
         Left            =   0
         Top             =   0
         Width           =   8220
      End
   End
   Begin VB.Frame Frame3 
      Caption         =   "Listado productos"
      Height          =   6405
      Left            =   180
      TabIndex        =   19
      Top             =   1605
      Width           =   14865
      Begin VB.CommandButton CmdMatrix 
         Caption         =   "Matriz Descuentos por volumen"
         Height          =   330
         Left            =   2475
         TabIndex        =   48
         Top             =   5580
         Width           =   2775
      End
      Begin VB.Frame Frame4 
         Caption         =   "Archivo Plano"
         Height          =   675
         Left            =   11100
         TabIndex        =   46
         Top             =   5580
         Width           =   3555
         Begin VB.CommandButton CmdGeneraArchivoPlano 
            Caption         =   "Generar Archivo Plano"
            Height          =   315
            Left            =   525
            TabIndex        =   47
            Top             =   270
            Width           =   2475
         End
      End
      Begin VB.Frame FrmCargando 
         Caption         =   "Cargando"
         Height          =   810
         Left            =   4065
         TabIndex        =   36
         Top             =   1485
         Visible         =   0   'False
         Width           =   7275
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
            Height          =   330
            Left            =   2160
            OleObjectBlob   =   "MasterProductos.frx":0037
            TabIndex        =   37
            Top             =   300
            Width           =   4200
         End
      End
      Begin VB.Frame FraInformacionExtra 
         Caption         =   "Informacion extra"
         Height          =   720
         Left            =   5790
         TabIndex        =   26
         Top             =   5505
         Width           =   2415
         Begin VB.TextBox txtCntidadregistros 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   1500
            Locked          =   -1  'True
            TabIndex        =   27
            Text            =   "0"
            Top             =   315
            Width           =   870
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkiCantidadRegistros 
            Height          =   255
            Left            =   105
            OleObjectBlob   =   "MasterProductos.frx":00BD
            TabIndex        =   28
            Top             =   345
            Width           =   1815
         End
      End
      Begin VB.Frame FraProgreso 
         Caption         =   "Progreso"
         Height          =   735
         Left            =   4755
         TabIndex        =   22
         Top             =   3930
         Visible         =   0   'False
         Width           =   6750
         Begin VB.PictureBox Picture1 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            ForeColor       =   &H80000008&
            Height          =   375
            Left            =   5595
            ScaleHeight     =   345
            ScaleWidth      =   705
            TabIndex        =   23
            Top             =   240
            Width           =   735
            Begin ACTIVESKINLibCtl.SkinLabel SkProgreso 
               Height          =   255
               Left            =   -15
               OleObjectBlob   =   "MasterProductos.frx":013F
               TabIndex        =   24
               Top             =   45
               Width           =   735
            End
         End
         Begin MSComctlLib.ProgressBar BarraProgreso 
            Height          =   375
            Left            =   150
            TabIndex        =   25
            Top             =   255
            Width           =   5415
            _ExtentX        =   9551
            _ExtentY        =   661
            _Version        =   393216
            Appearance      =   0
            Min             =   1e-4
         End
      End
      Begin VB.CommandButton CmdEliminarMarcados 
         Caption         =   "Eliminar Art�culos"
         Height          =   330
         Left            =   150
         TabIndex        =   21
         ToolTipText     =   "Eliminar Articulos Marcados"
         Top             =   5580
         Width           =   2235
      End
      Begin MSComctlLib.ListView LvDetalle 
         Height          =   5250
         Left            =   120
         TabIndex        =   20
         Top             =   255
         Width           =   14625
         _ExtentX        =   25797
         _ExtentY        =   9260
         View            =   3
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   18
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "T1000"
            Text            =   "Codigo"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "T2000"
            Text            =   "Tipo Producto"
            Object.Width           =   3528
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "T2000"
            Text            =   "Marca"
            Object.Width           =   3528
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Object.Tag             =   "T4000"
            Text            =   "Descripcion"
            Object.Width           =   7056
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   4
            Object.Tag             =   "N102"
            Text            =   "Pre. Compra"
            Object.Width           =   1676
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   5
            Object.Tag             =   "N102"
            Text            =   "Margen"
            Object.Width           =   1676
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   6
            Object.Tag             =   "N102"
            Text            =   "Pre. Venta"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   7
            Object.Tag             =   "N102"
            Text            =   "Utilidad"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   8
            Object.Tag             =   "N102"
            Text            =   "Stock Bodega"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   9
            Object.Tag             =   "N102"
            Text            =   "En Arriendo"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   10
            Object.Tag             =   "N102"
            Text            =   "Stock_Total"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(12) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   11
            Object.Tag             =   "N102"
            Text            =   "Stock Minimo"
            Object.Width           =   2293
         EndProperty
         BeginProperty ColumnHeader(13) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   12
            Object.Tag             =   "T2000"
            Text            =   "Bodega"
            Object.Width           =   4586
         EndProperty
         BeginProperty ColumnHeader(14) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   13
            Object.Tag             =   "T1500"
            Text            =   "Ubicacion Bodega"
            Object.Width           =   3528
         EndProperty
         BeginProperty ColumnHeader(15) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   14
            Object.Tag             =   "T2000"
            Text            =   "Obs"
            Object.Width           =   3528
         EndProperty
         BeginProperty ColumnHeader(16) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   15
            Object.Tag             =   "T1000"
            Text            =   "Inventariable"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(17) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   16
            Object.Tag             =   "T1500"
            Text            =   "Codigo Empresa"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(18) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   17
            Object.Tag             =   "T1000"
            Text            =   "Codigo Proveedor"
            Object.Width           =   2540
         EndProperty
      End
   End
   Begin VB.CommandButton CmdCodigoProveedor 
      Caption         =   "Ver c�digos proveedor"
      Height          =   255
      Left            =   150
      TabIndex        =   18
      Top             =   1275
      Width           =   1935
   End
   Begin VB.Frame Frame1 
      Caption         =   "Bodega"
      Height          =   1095
      Left            =   12060
      TabIndex        =   16
      Top             =   135
      Width           =   1980
      Begin VB.ComboBox CboBodega 
         Height          =   315
         Left            =   60
         Style           =   2  'Dropdown List
         TabIndex        =   17
         Top             =   480
         Width           =   1800
      End
   End
   Begin VB.CommandButton CmdExporta 
      Caption         =   "&Exportar"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3105
      TabIndex        =   3
      ToolTipText     =   "Exportar a excel"
      Top             =   630
      Width           =   1215
   End
   Begin VB.CommandButton cmdNuevoKardex 
      Caption         =   "&Kardex"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2010
      TabIndex        =   2
      Top             =   630
      Width           =   1050
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   2250
      OleObjectBlob   =   "MasterProductos.frx":01A1
      Top             =   8100
   End
   Begin VB.Frame Frame2 
      Caption         =   "Busqueda"
      Height          =   1110
      Left            =   4395
      TabIndex        =   11
      Top             =   135
      Width           =   10755
      Begin VB.Frame FraFamilia 
         Caption         =   "Tipo Producto"
         Height          =   1095
         Left            =   5685
         TabIndex        =   15
         Top             =   0
         Width           =   2010
         Begin VB.ComboBox CboTipoProducto 
            Height          =   315
            Left            =   120
            Style           =   2  'Dropdown List
            TabIndex        =   8
            Top             =   480
            Width           =   1815
         End
      End
      Begin VB.Frame FraMarca 
         Caption         =   "Marca"
         Height          =   1095
         Left            =   3060
         TabIndex        =   14
         Top             =   0
         Width           =   2640
         Begin VB.ComboBox CboMarca 
            Height          =   315
            Left            =   90
            Style           =   2  'Dropdown List
            TabIndex        =   7
            Top             =   480
            Width           =   2505
         End
      End
      Begin VB.CommandButton CmdTodosPro 
         Caption         =   "Todos"
         Height          =   375
         Left            =   9690
         TabIndex        =   10
         Top             =   630
         Width           =   975
      End
      Begin VB.CommandButton CmdBuscaProd 
         Caption         =   "&Buscar"
         Height          =   375
         Left            =   9690
         TabIndex        =   9
         Top             =   210
         Width           =   975
      End
      Begin VB.OptionButton OpCodigo 
         Caption         =   "Codigo"
         Height          =   255
         Left            =   1470
         TabIndex        =   6
         Top             =   675
         Width           =   1530
      End
      Begin VB.OptionButton OpDescripcion 
         Caption         =   "Descripcion"
         Height          =   255
         Left            =   135
         TabIndex        =   5
         Top             =   705
         Value           =   -1  'True
         Width           =   1185
      End
      Begin VB.TextBox TxtBusca 
         Height          =   375
         Left            =   120
         TabIndex        =   4
         Top             =   240
         Width           =   2925
      End
   End
   Begin VB.CommandButton CmdSalir 
      Cancel          =   -1  'True
      Caption         =   "&Retornar"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   13755
      TabIndex        =   12
      Top             =   8100
      Width           =   1215
   End
   Begin VB.CommandButton CmdEditar 
      Caption         =   "&Editar"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1080
      TabIndex        =   1
      Top             =   630
      Width           =   900
   End
   Begin VB.CommandButton CmdNuevo 
      Caption         =   "&Nuevo"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   90
      TabIndex        =   0
      Top             =   630
      Width           =   945
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Ver Columnas"
      Height          =   255
      Left            =   2160
      TabIndex        =   38
      Top             =   1275
      Width           =   1935
   End
   Begin VB.Label Label2 
      Caption         =   "Label2"
      DataField       =   "CODIGO"
      DataSource      =   "AdoHistorial"
      Height          =   135
      Left            =   4755
      TabIndex        =   13
      Top             =   8085
      Visible         =   0   'False
      Width           =   855
   End
End
Attribute VB_Name = "MasterProductos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim RsProductos As Recordset
Dim s_Sql As String
Dim sql2 As String
Dim Sm_filtro As String
Dim Sm_FiltroMarca As String
Dim Sm_FiltroTipo  As String
Dim Sm_Filtrobodega As String
Dim Bm_GrillaCargada As Boolean
Dim Sp_CodigoProveedor As String
Dim Sp_SoloRut As String
Dim Sm_CodInternos As String * 2
Dim Sm_FiltroCodInterno As String
Dim Sm_TipoF As String
Private Sub CboBodega_Click()
    If Not Bm_GrillaCargada Then Exit Sub
    If CboBodega.Text = "TODAS" Then
        Sm_Filtrobodega = Empty
    Else
        Sm_Filtrobodega = " s.bod_id=" & CboBodega.ItemData(CboBodega.ListIndex) & " AND "
    End If
    Grilla
End Sub

Private Sub CboMarca_Click()
    If Not Bm_GrillaCargada Then Exit Sub
    If CboMarca.Text = "TODOS" Then
        Sm_FiltroMarca = Empty
    Else
        Sm_FiltroMarca = " AND p.mar_id=" & CboMarca.ItemData(CboMarca.ListIndex) & " "
    End If
    
    Grilla
End Sub

Private Sub CboTipo_Click()
    If CboTipo.ListIndex = -1 Then Exit Sub
    If CboTipo.ListIndex = 0 Then
        Sm_TipoF = " AND pro_tipo='F' "
    ElseIf CboTipo.ListIndex = 1 Then
        Sm_TipoF = " AND pro_tipo='M' "
    Else
        Sm_TipoF = ""
    End If
    Grilla
End Sub

Private Sub CboTipoProducto_Click()
    If Not Bm_GrillaCargada Then Exit Sub
    If CboTipoProducto.Text = "TODOS" Then
        Sm_FiltroTipo = Empty
    Else
        Sm_FiltroTipo = " AND p.tip_id=" & CboTipoProducto.ItemData(CboTipoProducto.ListIndex) & " "
    End If
    Grilla
End Sub

Private Sub CmdAplicaVistaCol_Click()
    For i = 1 To LvColumnasVisibles.ListItems.Count
        If LvColumnasVisibles.ListItems(i).Checked Then
            LvDetalle.ColumnHeaders(i).Width = LvColumnasVisibles.ListItems(i).SubItems(2)
        Else
            LvDetalle.ColumnHeaders(i).Width = 0
        End If
        
    
    Next

    PicTipo.Visible = False
End Sub

Private Sub CmdBuscaProd_Click()
    Sm_filtro = Empty
    If Len(TxtBusca) > 0 Then
        If Me.OpDescripcion.Value Then
            Sm_filtro = "AND descripcion LIKE '%" & TxtBusca & "%' "
        End If
        If Me.OpCodigo Then
            Sm_filtro = "AND codigo LIKE '%" & TxtBusca & "%' "
            If Sm_CodInternos = "SI" Then
                Sm_FiltroCodInterno = " AND pro_codigo_interno ='" & TxtBusca & "' "
            End If
        End If
        Grilla
    End If
        
        
        
End Sub

Private Sub CmdCierraDetalleCheques_Click()
    Sp_CodigoProveedor = ""
    FrmDetalles.Visible = False
End Sub

Private Sub cmdCierraTipos_Click()
    PicTipo.Visible = False
End Sub

Private Sub CmdCodigoProveedor_Click()
    FrmDetalles.Visible = True
End Sub

Private Sub CmdEditar_Click()
    Dim Lp_Cx As Long
    For Lp_Cx = 1 To LvDetalle.ListItems.Count
        If LvDetalle.ListItems(Lp_Cx).Checked Then
            SG_codigo = LvDetalle.ListItems(Lp_Cx)
            AgregarProducto.Bm_Nuevo = False
            If SG_Es_la_Flor = "SI" Then
                AgregarProductoFlor.Show 1
            Else
                AgregarProducto.Show 1
            End If
            
        End If
    Next
    Grilla
End Sub

Private Sub CmdEliminarMarcados_Click()
        Dim Lp_CantidadEliminados As Long
        Lp_CantidadEliminados = 0
        cn.BeginTrans
        For i = 1 To LvDetalle.ListItems.Count
            If LvDetalle.ListItems(i).Checked Then
                SG_codigo = LvDetalle.ListItems(i)
                Sql = "SELECT codigo " & _
                        "FROM ven_detalle " & _
                        "WHERE codigo = '" & SG_codigo & "' AND rut_emp='" & SP_Rut_Activo & "' " & _
                        "UNION " & _
                        "SELECT pro_codigo " & _
                        "FROM com_doc_compra_detalle d " & _
                        "INNER JOIN com_doc_compra c ON d.id=c.id " & _
                        "WHERE pro_codigo = '" & SG_codigo & "' AND rut_emp='" & SP_Rut_Activo & "' "
                Consulta RsTmp, Sql
                
                If RsTmp.RecordCount > 0 Then
                    MsgBox "Este art�culo ha sido utilizado en compras y/o ventas ..." & vbNewLine & _
                           "no es posible eliminarlo ya que se perderian tambien los registros anteriores.", vbInformation
                Else
                    'Eliminar el codigo
                    cn.Execute "DELETE FROM maestro_productos WHERE codigo='" & SG_codigo & "' AND rut_emp='" & SP_Rut_Activo & "'"
                    Lp_CantidadEliminados = Lp_CantidadEliminados + 1
                    
                End If
            
              
                
            
           
            
            End If
        Next
        
        If Lp_CantidadEliminados > 0 Then
            If MsgBox("Se eliminar�n " & Lp_CantidadEliminados & " art�culos... " & vbNewLine & _
                   " �Continuar? ... ", vbQuestion + vbOKCancel) = vbOK Then
                cn.CommitTrans
            Else
                cn.RollbackTrans
                
            End If
        Else
            cn.RollbackTrans
        End If
        Grilla
End Sub

Private Sub CmdExporta_Click()
    Dim tit(2) As String
    FraProgreso.Visible = True
    tit(0) = Me.Caption
    tit(1) = "FECHA INFORME: " & Date & " - " & Time
    tit(2) = "" ' "                                        DESDE:" & DtInicio.Value & "   HASTA:" & Dtfin.Value
    Exportar LvDetalle, tit, Me, BarraProgreso, SkProgreso
    BarraProgreso.Value = 1
    SkProgreso = "0%"
    FraProgreso.Visible = False
End Sub

Private Sub CmdFamilias_Click()
    With Mantenedor_Dependencia
        .S_Id = "p.tip_id"
        .S_Nombre = "tip_nombre"
        .S_Activo = "tip_activo"
        .S_tabla = "par_tipos_productos p"
        .S_RutEmpresa = "SI"
        .S_Consulta = "SELECT tip_id,pla_nombre,tip_nombre,tip_activo " & _
                      "FROM par_tipos_productos p " & _
                      "INNER JOIN con_plan_de_cuentas c ON p.pla_id=c.pla_id " & _
                      "WHERE  rut_emp='" & SP_Rut_Activo & "' "
        .Caption = "Mantenedor Tipos de productos"
        .FrmMantenedor.Caption = "Tipos de productos "
        .S_id_dep = "c.pla_id"
        .S_nombre_dep = "pla_nombre"
        .S_tabla_dep = "con_plan_de_cuentas c"
        .S_Activo_dep = "pla_activo"
        .S_titulo_dep = "Cuenta contable"
        .B_Editable = True
        .Show 1
    End With
    
End Sub



Private Sub CmdMarcas_Click()
    With Mantenedor_Simple
        .S_Id = "mar_id"
        .S_Nombre = "mar_nombre"
        .S_Activo = "mar_activo"
        .S_tabla = "par_marcas"
        .S_RutEmpresa = "SI"
        .S_Consulta = "SELECT " & .S_Id & "," & .S_Nombre & "," & .S_Activo & " " & _
                      "FROM " & .S_tabla & " " & _
                      "WHERE  rut_emp='" & SP_Rut_Activo & "' "
        .Caption = "Mantenedor Marcas de Articulos"
        .FrmMantenedor.Caption = "Marcas "
        .B_Editable = True
        .Show 1
    End With
End Sub

Private Sub CmdGeneraArchivoPlano_Click()
    Dim Sp_NomArchivo As String
    Dim Ip_Cantidad As Integer
    If LvDetalle.ListItems.Count = 0 Then Exit Sub
    
    Ip_Cantidad = 0
    
    For i = 1 To LvDetalle.ListItems.Count
        If LvDetalle.ListItems(i).Checked Then Ip_Cantidad = Ip_Cantidad + 1
    Next
    
    If Ip_Cantidad = 0 Then Exit Sub
    
    On Error GoTo sale
    Dialogo.ShowSave
    
    Sp_NomArchivo = Dialogo.FileName
    X = FreeFile
    Open Sp_NomArchivo For Output As X
        Print #X, Chr(34) & "CODIGO" & Chr(34) & "," & Chr(34) & "NOMBRE" & Chr(34)
        For i = 1 To LvDetalle.ListItems.Count
            If LvDetalle.ListItems(i).Checked Then Print #X, Chr(34) & LvDetalle.ListItems(i) & Chr(34) & "," & Chr(34) & LvDetalle.ListItems(i).SubItems(3) & Chr(34)
        Next
    Close #X
    
    MsgBox Sp_NomArchivo & vbNewLine & "Generado correctamente..."
    
    
    Exit Sub
sale:
    
End Sub

Private Sub CmdMatrix_Click()
    Dim Sp_Codigos As String
    Sp_Codigos = ""
    For i = 1 To LvDetalle.ListItems.Count
        If LvDetalle.ListItems(i).Checked Then
            
            inv_DescuentosPorVolumen.TxtCodSistema = LvDetalle.ListItems(i)
            inv_DescuentosPorVolumen.TxtNombre = LvDetalle.ListItems(i).SubItems(3)
            inv_DescuentosPorVolumen.TxtCodBarra = LvDetalle.ListItems(i).SubItems(16)
            inv_DescuentosPorVolumen.TxtPrecioU = LvDetalle.ListItems(i).SubItems(6)
            inv_DescuentosPorVolumen.CargaMatriz
            inv_DescuentosPorVolumen.Show 1
        End If
    Next
    Exit Sub
    If Len(Sp_Codigos) > 0 Then
        Sp_Codigos = Mid(Sp_Codigos, 1, Len(Sp_Codigos) - 1)
        inv_DescuentosPorVolumen.TxtNombre = Sp_Codigos
        inv_DescuentosPorVolumen.CargaMatriz
        inv_DescuentosPorVolumen.Show 1
    End If
End Sub

Private Sub CmdMinimizaProveedor_Click()
    FrmDetalles.Height = 300
End Sub

Private Sub CmdNuevo_Click()
    SG_codigo = Empty
    AgregarProducto.Bm_Nuevo = True
    
 '   Sql = "SELECT emp_de_repuestos " & _
            "FROM sis_empresas " & _
            "WHERE rut='" & SP_Rut_Activo & "'"
 '   Consulta RsTmp, Sql
 '   If RsTmp!emp_de_repuestos = "SI" Then
        
 '       inv_ficha_respuestos.Show 1
 '   Else

         
        If SG_Es_la_Flor = "SI" Then
                    AgregarProductoFlor.Show 1
        Else
            AgregarProducto.Show 1
        End If
  '  End If
    Grilla
End Sub

Private Sub cmdNuevoKardex_Click()
    'With Me.GridProductos
    '    .Col = 2
    '    Inv_Kardex.TxtCodigo = .Text
        inv_KardexGeneral.Show 1
   ' End With
End Sub

Private Sub cmdRutProveedor_Click()
    AccionProveedor = 0
    BuscaProveedor.Show 1
    txtRutProveedor = RutBuscado

    txtRutProveedor_Validate True
End Sub

Private Sub cmdSalir_Click()
    Unload Me
End Sub

Private Sub CmdTodosLosProveedores_Click()
    Dim Ip_Mayor As Integer
    Sp_CodigoProveedor = ""
    Sp_SoloRut = ""
        
    FrmCargando.Visible = True
    FraProgreso.Visible = True
    DoEvents
    
    BarraProgreso.Value = 1
    BarraProgreso.Max = LvDetalle.ListItems.Count
    
    Ip_Mayor = 1
    Sql = "SELECT COUNT(pro_codigo) codigo " & _
            "FROM par_codigos_proveedor " & _
            "GROUP BY rut_emp,pro_codigo " & _
            "HAVING Codigo > 1 "
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        RsTmp.MoveFirst
        Do While Not RsTmp.EOF
            If RsTmp!Codigo > Ip_Mayor Then Ip_Mayor = RsTmp!Codigo
    
    
            RsTmp.MoveNext
        Loop
    End If
    'Cantidad de columnas normla de la grillal de productos
    If LvDetalle.ColumnHeaders.Count > 18 Then
        For i = LvDetalle.ColumnHeaders.Count To 1 Step -1
            LvDetalle.ColumnHeaders.Remove LvDetalle.ColumnHeaders.Count
            If LvDetalle.ColumnHeaders.Count = 18 Then Exit For
        Next
    End If
    
    If Ip_Mayor > 1 Then
            For i = 2 To Ip_Mayor
                LvDetalle.ColumnHeaders.Add , , "Prov " & i, 1000, 1
            Next
    End If
    
    
    For i = 1 To LvDetalle.ListItems.Count
            BarraProgreso.Value = i
            SkProgreso = i
  '  SELECT cpv_codigo_proveedor FROM par_codigos_proveedor o WHERE o.pro_codigo=p.codigo
            Col = 17
            Sql = "SELECT rut_proveedor,cpv_codigo_proveedor " & _
                    "FROM par_codigos_proveedor " & _
                    "WHERE pro_codigo='" & LvDetalle.ListItems(i) & "' AND rut_emp='" & SP_Rut_Activo & "'"
            Consulta RsTmp, Sql
            If RsTmp.RecordCount > 0 Then
                RsTmp.MoveFirst
                Do While Not RsTmp.EOF
                    LvDetalle.ListItems(i).SubItems(Col) = RsTmp!rut_proveedor & " : " & RsTmp!cpv_codigo_proveedor
            
                    RsTmp.MoveNext
                    Col = Col + 1
                Loop
            End If
                    
    
            
    Next
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    FraProgreso.Visible = False
    
    
    FrmCargando.Visible = False
End Sub

Private Sub CmdTodosPro_Click()
    Me.TxtBusca.Text = ""
    Sm_filtro = Empty
    Grilla
End Sub



Private Sub Grilla()
    s_sql2 = ""
    
    'bod_id= 1, ya que las bodegas principales, son 1
    
    s_Sql = "SELECT codigo,tip_nombre,mar_nombre,descripcion," & _
             "IFNULL((SELECT AVG(pro_ultimo_precio_compra) FROM pro_stock s WHERE s.rut_emp='" & SP_Rut_Activo & "' AND s.pro_codigo=p.codigo AND bod_id=1),precio_compra) precio_compra," & _
             "porciento_utilidad,precio_venta,precio_venta-IFNULL((" & _
                    "SELECT AVG(pro_ultimo_precio_compra) " & _
                    "FROM pro_stock s " & _
                    "WHERE s.rut_emp = '76.005.337-6' " & _
                    "AND s.pro_codigo = p.codigo)," & _
                    "precio_compra) margen," & _
              "IF(pro_inventariable='SI',IFNULL((SELECT SUM(sto_stock) FROM pro_stock s WHERE " & Sm_Filtrobodega & " s.rut_emp='" & SP_Rut_Activo & "' AND s.pro_codigo=p.codigo),0),0) stock_bodega," & _
              "IFNULL((SELECT SUM(arr_cantidad) " & _
                "FROM inv_productos_arrendados WHERE arr_devuelto='NO' AND pro_codigo=p.codigo AND emp_id=" & IG_id_Empresa & "),0) arrendados,0, " & _
              "stock_critico, bod_nombre,ubicacion_bodega,comentario,pro_inventariable,pro_codigo_interno " & Sp_CodigoProveedor & " " & _
          "FROM maestro_productos p,par_tipos_productos t,par_marcas m,par_bodegas b " & _
          "WHERE p.rut_emp='" & SP_Rut_Activo & "' AND  p.tip_id=t.tip_id AND p.mar_id=m.mar_id AND p.bod_id=b.bod_id " & Sm_filtro & Sm_FiltroTipo & Sm_FiltroMarca & Sm_TipoF & _
          Sp_SoloRut
    
    If Sm_CodInternos = "SI" And Bm_GrillaCargada And OpCodigo Then
    
        s_sql2 = " UNION SELECT codigo,tip_nombre,mar_nombre,descripcion," & _
             "IFNULL((SELECT AVG(pro_ultimo_precio_compra) FROM pro_stock s WHERE s.rut_emp='" & SP_Rut_Activo & "' AND s.pro_codigo=p.codigo),0) precio_compra," & _
             "porciento_utilidad,precio_venta,margen," & _
              "IF(pro_inventariable='SI',IFNULL((SELECT SUM(sto_stock) FROM pro_stock s WHERE " & Sm_Filtrobodega & " s.rut_emp='" & SP_Rut_Activo & "' AND s.pro_codigo=p.codigo),0),0) stock_bodega," & _
              "IFNULL((SELECT SUM(arr_cantidad) " & _
                "FROM inv_productos_arrendados WHERE arr_devuelto='NO' AND pro_codigo=p.codigo AND emp_id=" & IG_id_Empresa & "),0) arrendados,0, " & _
              "stock_critico, bod_nombre,ubicacion_bodega,comentario,pro_inventariable,pro_codigo_interno " & Sp_CodigoProveedor & " " & _
          "FROM maestro_productos p,par_tipos_productos t,par_marcas m,par_bodegas b " & _
          "WHERE p.rut_emp='" & SP_Rut_Activo & "' AND  p.tip_id=t.tip_id AND p.mar_id=m.mar_id AND p.bod_id=b.bod_id " & Sm_FiltroCodInterno & Sm_FiltroTipo & Sm_FiltroMarca & Sm_TipoF & _
          Sp_SoloRut
    
    
    
    End If
    
    
    Consulta RsProductos, s_Sql & s_sql2
    
    
    
    
    LLenar_Grilla RsProductos, Me, LvDetalle, True, True, True, False
    txtCntidadregistros = RsProductos.RecordCount
    For i = 1 To LvDetalle.ListItems.Count
        LvDetalle.ListItems(i).SubItems(8) = CDbl(LvDetalle.ListItems(i).SubItems(8)) - CDbl(LvDetalle.ListItems(i).SubItems(9))
        LvDetalle.ListItems(i).SubItems(10) = CDbl(LvDetalle.ListItems(i).SubItems(9)) + CDbl(LvDetalle.ListItems(i).SubItems(8))
    Next
    
End Sub

Private Sub Command1_Click()
    PicTipo.Visible = True
End Sub

Private Sub Command2_Click()
    For i = 1 To LvColumnasVisibles.ListItems.Count
        
        LvDetalle.ColumnHeaders(i).Width = LvColumnasVisibles.ListItems(i).SubItems(2)
        
        If LvDetalle.ColumnHeaders(i).Width > 0 Then
            LvColumnasVisibles.ListItems(i).Checked = True
        End If
    Next
    PicTipo.Visible = False
    
End Sub



Private Sub Command3_Click()
    FrmDetalles.Height = 1170
End Sub

Private Sub Form_Load()
    Centrar Me
    Bm_GrillaCargada = False
    Sp_CodigoProveedor = ""
    Sp_SoloRut = ""
   ' BuscaProducto.AdoProducto.Refresh
    LLenarCombo CboTipoProducto, "tip_nombre", "tip_id", "par_tipos_productos", "tip_activo='SI' AND  rut_emp='" & SP_Rut_Activo & "'", "tip_nombre"
    CboTipoProducto.AddItem "TODOS"
    CboTipoProducto.ListIndex = CboTipoProducto.ListCount - 1
    LLenarCombo CboMarca, "mar_nombre", "mar_id", "par_marcas", "mar_activo='SI' AND  rut_emp='" & SP_Rut_Activo & "'", "mar_nombre"
    CboMarca.AddItem "TODOS"
    CboMarca.ListIndex = CboMarca.ListCount - 1
    
    LLenarCombo CboBodega, "bod_nombre", "bod_id", "par_bodegas", "bod_activo='SI' AND rut_emp='" & SP_Rut_Activo & "'", "bod_nombre"
    CboBodega.AddItem "TODAS"
    CboBodega.ListIndex = CboBodega.ListCount - 1
    
    
    Sql = "SELECT emp_utiliza_codigos_internos_productos codinterno " & _
        "FROM sis_empresas " & _
        "WHERE rut='" & SP_Rut_Activo & "'"
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        Sm_CodInternos = RsTmp!codinterno
    End If
    
    
    If SG_Codigos_Alfanumericos = "NO" Then
        LvDetalle.ColumnHeaders(1).Tag = "N109"
    End If
    
    
    
    Sm_FiltroMarca = Empty
    Sm_FiltroTipo = Empty
    Sm_Filtrobodega = Empty
    Aplicar_skin Me
    Grilla
    Bm_GrillaCargada = True
    
    If SP_Rut_Activo <> "76.244.196-9" Then
        LvDetalle.ColumnHeaders(10).Width = 0
    End If
    
    CargaColumnasVisibles
    
End Sub
Private Sub CargaColumnasVisibles()
    LvColumnasVisibles.ListItems.Clear
    For i = 1 To LvDetalle.ColumnHeaders.Count
        LvColumnasVisibles.ListItems.Add , , i
        LvColumnasVisibles.ListItems(LvColumnasVisibles.ListItems.Count).SubItems(1) = LvDetalle.ColumnHeaders(i).Text
        LvColumnasVisibles.ListItems(LvColumnasVisibles.ListItems.Count).SubItems(2) = LvDetalle.ColumnHeaders(i).Width
        If LvDetalle.ColumnHeaders(i).Width > 0 Then
            LvColumnasVisibles.ListItems(LvColumnasVisibles.ListItems.Count).Checked = True
        End If
    Next
End Sub


Private Sub LvDetalle_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    ordListView ColumnHeader, Me, LvDetalle
End Sub

Private Sub TxtBusca_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii))) 'Transformo a mayuscula el caracter ingresado
    If KeyAscii = 39 Then KeyAscii = 0
End Sub



Private Sub txtRutProveedor_Validate(Cancel As Boolean)
    If Len(txtRutProveedor) = 0 Then Exit Sub
    Sql = "SELECT nombre_empresa " & _
        "FROM maestro_proveedores " & _
        "WHERE rut_proveedor='" & txtRutProveedor & "'"
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        txtNombreProveedor = RsTmp!nombre_empresa
    End If
End Sub

Private Sub VerConCodigoProveedor_Click()
    Sp_CodigoProveedor = ""
    Sp_SoloRut = ""
    If Len(txtRutProveedor) = 0 Then
        MsgBox "Debe ingresar RUT de Proveedor", vbInformation
    
        Exit Sub
        
    End If
    FrmCargando.Visible = True
    DoEvents
     Sp_CodigoProveedor = ",(SELECT cpv_codigo_proveedor FROM par_codigos_proveedor o WHERE o.pro_codigo=p.codigo AND o.rut_proveedor='" & txtRutProveedor & "' AND o.rut_emp='" & SP_Rut_Activo & "' LIMIT 1) codproveedor "
  '   Sp_Solo_Proveedor = " AND rut='" & txtRutProveedor & "' "
   '  = ",(SELECT )"
    Sp_SoloRut = "HAVING LENGTH(codproveedor)>0"
    Grilla
    FrmCargando.Visible = False
End Sub
