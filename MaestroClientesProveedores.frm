VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{DA729E34-689F-49EA-A856-B57046630B73}#1.0#0"; "progressbar-xp.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form MaestroClientesProveedores 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Maestro Clientes/Proveedores"
   ClientHeight    =   8940
   ClientLeft      =   6630
   ClientTop       =   5940
   ClientWidth     =   14685
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   8940
   ScaleWidth      =   14685
   Begin VB.Timer Timer1 
      Interval        =   300
      Left            =   195
      Top             =   60
   End
   Begin VB.Frame FraProgreso 
      Caption         =   "Progreso Exportaci�n"
      Height          =   795
      Left            =   450
      TabIndex        =   20
      Top             =   7245
      Visible         =   0   'False
      Width           =   13680
      Begin Proyecto2.XP_ProgressBar BarraProgreso 
         Height          =   345
         Left            =   105
         TabIndex        =   21
         Top             =   330
         Width           =   13410
         _ExtentX        =   23654
         _ExtentY        =   609
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BrushStyle      =   0
         Color           =   16777088
         Scrolling       =   1
         ShowText        =   -1  'True
      End
   End
   Begin VB.CommandButton CmdSalir 
      Cancel          =   -1  'True
      Caption         =   "&Retornar"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   12630
      TabIndex        =   15
      Top             =   8340
      Width           =   1815
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   0
      OleObjectBlob   =   "MaestroClientesProveedores.frx":0000
      Top             =   8430
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   7935
      Left            =   240
      TabIndex        =   7
      Top             =   360
      Width           =   14325
      _ExtentX        =   25268
      _ExtentY        =   13996
      _Version        =   393216
      Tabs            =   2
      TabsPerRow      =   2
      TabHeight       =   520
      TabCaption(0)   =   "Maestro Clientes"
      TabPicture(0)   =   "MaestroClientesProveedores.frx":0234
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "LvClientes"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Frame1"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "CmdEditarCliente"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "CmdNuevoCliente"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "cmdExportar"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).ControlCount=   5
      TabCaption(1)   =   "Maestro Proveedores"
      TabPicture(1)   =   "MaestroClientesProveedores.frx":0250
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "CmdExportaPro"
      Tab(1).Control(1)=   "EditarProveedor"
      Tab(1).Control(2)=   "CmdNuevoProveedor"
      Tab(1).Control(3)=   "Frame2"
      Tab(1).Control(4)=   "LvProveedores"
      Tab(1).ControlCount=   5
      Begin VB.CommandButton cmdExportar 
         Caption         =   "Exportar a Excel"
         Height          =   360
         Left            =   60
         TabIndex        =   23
         Top             =   7455
         Width           =   1500
      End
      Begin VB.CommandButton CmdExportaPro 
         Caption         =   "Exportar a Excel"
         Height          =   360
         Left            =   -74880
         TabIndex        =   22
         Top             =   7470
         Width           =   1500
      End
      Begin VB.CommandButton CmdNuevoCliente 
         Caption         =   "Crear Nuevo Cliente"
         Height          =   495
         Left            =   360
         TabIndex        =   0
         Top             =   825
         Width           =   1815
      End
      Begin VB.CommandButton CmdEditarCliente 
         Caption         =   "Editar Seleccionado"
         Height          =   495
         Left            =   2280
         TabIndex        =   1
         Top             =   825
         Width           =   1815
      End
      Begin VB.CommandButton EditarProveedor 
         Caption         =   "&Editar Seleccionado"
         Height          =   375
         Left            =   -73050
         TabIndex        =   9
         Top             =   1230
         Width           =   1785
      End
      Begin VB.CommandButton CmdNuevoProveedor 
         Caption         =   "&Crear Nuevo Proveedor"
         Height          =   375
         Left            =   -74895
         TabIndex        =   8
         Top             =   1230
         Width           =   1815
      End
      Begin VB.Frame Frame2 
         Caption         =   "Busqueda"
         Height          =   1095
         Left            =   -67035
         TabIndex        =   17
         Top             =   510
         Width           =   6135
         Begin VB.TextBox TxtBuscaProveedor 
            Height          =   405
            Left            =   120
            TabIndex        =   10
            Top             =   240
            Width           =   2775
         End
         Begin VB.OptionButton OPRsocialProved 
            Caption         =   "Raz�n Social"
            Height          =   255
            Left            =   120
            TabIndex        =   11
            Top             =   720
            Value           =   -1  'True
            Width           =   1575
         End
         Begin VB.OptionButton OpRutProveedor 
            Caption         =   "RUT Proveedor"
            Height          =   255
            Left            =   1800
            TabIndex        =   12
            Top             =   720
            Width           =   1695
         End
         Begin VB.CommandButton CmdBuscaProv 
            Caption         =   "&Buscar"
            Height          =   615
            Left            =   3600
            TabIndex        =   13
            Top             =   240
            Width           =   975
         End
         Begin VB.CommandButton CmdTodosPRov 
            Caption         =   "Todos"
            Height          =   615
            Left            =   4800
            TabIndex        =   14
            Top             =   240
            Width           =   975
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "Busqueda"
         Height          =   1095
         Left            =   8730
         TabIndex        =   16
         Top             =   495
         Width           =   5415
         Begin VB.CommandButton CmdTodos 
            Caption         =   "Todos"
            Height          =   615
            Left            =   4200
            TabIndex        =   6
            Top             =   240
            Width           =   975
         End
         Begin VB.CommandButton CmdBuscaCliente 
            Caption         =   "&Buscar"
            Height          =   615
            Left            =   3120
            TabIndex        =   5
            Top             =   240
            Width           =   975
         End
         Begin VB.OptionButton OpRut 
            Caption         =   "RUT Cliente"
            Height          =   255
            Left            =   1680
            TabIndex        =   4
            Top             =   720
            Width           =   1575
         End
         Begin VB.OptionButton OpRsocial 
            Caption         =   "Raz�n Social"
            Height          =   255
            Left            =   120
            TabIndex        =   3
            Top             =   720
            Value           =   -1  'True
            Width           =   1695
         End
         Begin VB.TextBox TxtBuscaCliente 
            Height          =   405
            Left            =   120
            TabIndex        =   2
            Top             =   240
            Width           =   2775
         End
      End
      Begin MSComctlLib.ListView LvClientes 
         Height          =   5595
         Left            =   75
         TabIndex        =   18
         Top             =   1665
         Width           =   14085
         _ExtentX        =   24844
         _ExtentY        =   9869
         View            =   3
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   13
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "T1000"
            Text            =   "Rut"
            Object.Width           =   2293
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "T1300"
            Text            =   "Razon Social"
            Object.Width           =   4410
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "T2000"
            Text            =   "Nombre Fantasia"
            Object.Width           =   3528
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Object.Tag             =   "T800"
            Text            =   "Direccion"
            Object.Width           =   4410
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Object.Tag             =   "T3000"
            Text            =   "Ciudad"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Object.Tag             =   "T800"
            Text            =   "Comuna"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   6
            Object.Tag             =   "T800"
            Text            =   "Fono"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   7
            Object.Tag             =   "T1500"
            Text            =   "email"
            Object.Width           =   2646
         EndProperty
         BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   8
            Object.Tag             =   "T1500"
            Text            =   "Giro"
            Object.Width           =   3528
         EndProperty
         BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   9
            Object.Tag             =   "N109"
            Text            =   "Descuento"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   10
            Object.Tag             =   "T2000"
            Text            =   "Contacto"
            Object.Width           =   3528
         EndProperty
         BeginProperty ColumnHeader(12) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   11
            Object.Tag             =   "T2000"
            Text            =   "Obs"
            Object.Width           =   3528
         EndProperty
         BeginProperty ColumnHeader(13) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   12
            Object.Tag             =   "N100"
            Text            =   "Monto Credito Aprobado"
            Object.Width           =   3528
         EndProperty
      End
      Begin MSComctlLib.ListView LvProveedores 
         Height          =   5760
         Left            =   -74880
         TabIndex        =   19
         Top             =   1650
         Width           =   14040
         _ExtentX        =   24765
         _ExtentY        =   10160
         View            =   3
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   10
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "T1000"
            Text            =   "Rut"
            Object.Width           =   2293
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "T1300"
            Text            =   "Nombre Proveedor"
            Object.Width           =   4410
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "T2000"
            Text            =   "Direccion"
            Object.Width           =   3528
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Object.Tag             =   "T800"
            Text            =   "Comuna"
            Object.Width           =   4410
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Object.Tag             =   "T3000"
            Text            =   "Ciudad"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Object.Tag             =   "T800"
            Text            =   "Fono"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   6
            Object.Tag             =   "T800"
            Text            =   "Fax"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   7
            Object.Tag             =   "T1500"
            Text            =   "email"
            Object.Width           =   2646
         EndProperty
         BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   8
            Object.Tag             =   "T1000"
            Text            =   "Banco"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   9
            Object.Tag             =   "2000"
            Text            =   "Nro Cuenta"
            Object.Width           =   2540
         EndProperty
      End
   End
End
Attribute VB_Name = "MaestroClientesProveedores"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim FiltroP As String, FiltroC As String, OrdenC As String, OrdenP As String
Private Sub CmdBuscaCliente_Click()
    If Len(Me.TxtBuscaCliente.Text) = 0 Then Exit Sub
    If Me.OpRsocial.Value Then FiltroC = " AND nombre_rsocial LIKE '%" & Me.TxtBuscaCliente.Text & "%'"
    If Me.OpRut.Value Then FiltroC = " AND rut_cliente LIKE '" & Me.TxtBuscaCliente.Text & "%'"
    CargaClientes
End Sub

Private Sub CmdBuscaProv_Click()
    If Len(Me.TxtBuscaProveedor.Text) = 0 Then Exit Sub
    If Me.OPRsocialProved.Value = True Then FiltroP = " AND nombre_empresa LIKE '%" & Me.TxtBuscaProveedor.Text & "%'"
    If Me.OpRutProveedor.Value = True Then FiltroP = " AND rut_proveedor LIKE '" & Me.TxtBuscaProveedor.Text & "%'"
    CargaProveedores
End Sub

Private Sub CmdEditarCliente_Click()
    If LvClientes.SelectedItem Is Nothing Then Exit Sub
    AccionCliente = 2
    SG_codigo = LvClientes.SelectedItem
    AgregoCliente.Show 1
    CargaClientes
End Sub

Private Sub CmdExportaPro_Click()
    Dim tit(2) As String
    If LvProveedores.ListItems.Count = 0 Then Exit Sub
    FraProgreso.Visible = True
    tit(0) = "Maestro de Proveedores "
    tit(1) = DtFecha
    tit(2) = Time
    ExportarNuevo LvProveedores, tit, Me, BarraProgreso
    FraProgreso.Visible = False
End Sub

Private Sub CmdExportar_Click()
    Dim tit(2) As String
    If LvClientes.ListItems.Count = 0 Then Exit Sub
    FraProgreso.Visible = True
    tit(0) = "Maestro de Clientes "
    tit(1) = DtFecha
    tit(2) = Time
    ExportarNuevo LvClientes, tit, Me, BarraProgreso
    FraProgreso.Visible = False
End Sub

Private Sub CmdNuevoCliente_Click()
    SG_codigo = Empty
    AccionCliente = 1
    'With AgregoCliente
'        .Timer2.Enabled = True
        '.txtRut.Text = ""
    '    .CmdGuarda.Caption = "Guarda Nuevo Cliente"
      '  .PicNew.Visible = True
        AgregoCliente.Show 1
  '  End With
   
    CargaClientes
End Sub

Private Sub CmdNuevoProveedor_Click()
    SG_codigo = Empty
    AgregoProveedor.Show 1
    CargaProveedores
End Sub

Private Sub CmdSalir_Click()
    Unload Me
End Sub

Private Sub CmdTodos_Click()
    Me.TxtBuscaCliente.Text = ""
    FiltroC = Empty
    CargaClientes
    
End Sub


Private Sub CmdTodosPRov_Click()
    Me.TxtBuscaProveedor.Text = ""
    FiltroP = Empty
    CargaProveedores
    
End Sub

Private Sub EditarProveedor_Click()
    If LvProveedores.SelectedItem Is Nothing Then Exit Sub
    SG_codigo = LvProveedores.SelectedItem
    AgregoProveedor.Show 1
    CargaProveedores
End Sub

Private Sub Form_Load()
    Centrar Me
    Aplicar_skin Me
    FiltroC = Empty
    FiltroP = Empty
    OrdenC = Empty
    OrdenP = Empty
   
    CargaProveedores
    CargaClientes
End Sub

Private Sub CargaProveedores()
    Sql = "SELECT rut_proveedor,nombre_empresa,direccion,comuna,ciudad,fono,fax,email,ban_nombre,p.prv_ctacte " & _
          "FROM maestro_proveedores p " & _
          "INNER JOIN par_asociacion_ruts a ON p.rut_proveedor=a.rut_pro " & _
          "LEFT JOIN par_bancos USING(ban_id) " & _
          "WHERE a.rut_emp='" & SP_Rut_Activo & "'" & FiltroP & OrdenP
    Consulta RsTmp3, Sql
    LLenar_Grilla RsTmp3, Me, LvProveedores, False, True, True, False
End Sub
Private Sub CargaClientes()
    'Sql = "SELECT rut_cliente,nombre_rsocial,cli_nombre_fantasia,direccion,ciudad,comuna,fono,email,giro," & _
                  "descuento , cli_contacto, comentario,cli_monto_credito " & _
          "FROM maestro_clientes p " & _
          "INNER JOIN par_asociacion_ruts a ON p.rut_cliente=a.rut_cli " & _
          "WHERE a.rut_emp='" & SP_Rut_Activo & "' " & FiltroC & OrdenC
          
    Sql = "SELECT rut_cliente,nombre_rsocial,cli_nombre_fantasia,direccion,ciudad,comuna,fono,email,giro," & _
                  "descuento , cli_contacto, comentario,cli_monto_credito " & _
          "FROM maestro_clientes p " & _
          "WHERE 1=1 " & FiltroC & OrdenC & " " & _
          "GROUP BY rut_cliente"
    Consulta RsTmp2, Sql
    LLenar_Grilla RsTmp2, Me, LvClientes, False, True, True, False
End Sub

Private Sub Form_Unload(Cancel As Integer)
 
    DesdeCompra = False
End Sub



Private Sub GridClientes_HeadClick(ByVal ColIndex As Integer)
   OrdenC = " ORDER BY " & GridClientes.Columns(ColIndex).DataField
   CargaClientes
End Sub

Private Sub GridClientes_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii))) 'Transformo a mayuscula el caracter ingresado
End Sub



Private Sub GridProveedores_HeadClick(ByVal ColIndex As Integer)
   OrdenP = " ORDER BY " & GridProveedores.Columns(ColIndex).DataField
   CargaProveedores
End Sub

Private Sub GridProveedores_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii))) 'Transformo a mayuscula el caracter ingresado
End Sub

'Instrucci�n que ordena columnas
Private Sub Lvclientes_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    ordListView ColumnHeader, Me, LvClientes
End Sub
'Instrucci�n que ordena columnas
Private Sub Lvproveedores_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    ordListView ColumnHeader, Me, LvProveedores
End Sub

Private Sub Timer1_Timer()
    On Error Resume Next
    TxtBuscaCliente.SetFocus
    Timer1.Enabled = False
End Sub

Private Sub TxtBuscaCliente_Change()
    If TxtBuscaCliente = "" Then Exit Sub
    If Me.OpRsocial.Value = True Then
        FiltroC = " AND nombre_rsocial LIKE '" & Me.TxtBuscaCliente.Text & "%'"
    Else
        FiltroC = " AND rut_cliente LIKE '" & Me.TxtBuscaCliente.Text & "%'"
    End If
    CargaClientes
End Sub

Private Sub TxtBuscaCliente_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii))) 'Transformo a mayuscula el caracter ingresado
    If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtBuscaCliente_Validate(Cancel As Boolean)
TxtBuscaCliente = Replace(TxtBuscaCliente, "'", "")
End Sub

Private Sub TxtBuscaProveedor_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii))) 'Transformo a mayuscula el caracter ingresado
    If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtBuscaProveedor_Validate(Cancel As Boolean)
TxtBuscaProveedor = Replace(TxtBuscaProveedor, "'", "")
End Sub
