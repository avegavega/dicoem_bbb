VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Class1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit


'Eventos
Public Event InicioDeExportacion(ByVal ExportingFormat As DatabaseExportEnum)
Public Event ErrorExportacion(Error As ErrObject, ByVal ExportingFormat As DatabaseExportEnum)
Public Event ExportacionCompleta(ByVal Success As Boolean, ByVal ExportingFormat As DatabaseExportEnum)


Private Progress As ProgressBar
Private PathArchivoSalida As String
Private ADODBRecordset As ADODB.Recordset

Public Enum DatabaseExportEnum
    [HTML] = 1
    [Excel] = 2
End Enum


Private Declare Function GetQueueStatus Lib "user32" (ByVal qsFlags As Long) As Long

Public Property Set RecordsetAdo(ByVal vADORec As ADODB.Recordset)
    Set ADODBRecordset = vADORec
End Property

Public Property Set ProgressBar(ByVal vProgress As ProgressBar)
    Set Progress = vProgress
End Property

Public Property Get pathArchivo() As String
    pathArchivo = PathArchivoSalida
End Property

Public Property Let pathArchivo(ByVal sNewValue As String)
    PathArchivoSalida = sNewValue
End Property

Private Function DoEventsEx() As Long


    On Local Error Resume Next
        DoEventsEx = GetQueueStatus(&H80 Or &H1 Or &H4 Or &H20 Or &H10)
        If DoEventsEx <> 0 Then
            DoEvents
        End If

End Function



Public Sub ExportarHTML(ByVal TitleOfHTML As String, _
                        Optional TitleFont As String = "Tahoma", _
                        Optional HeaderFont As String = "Tahoma", _
                        Optional TitleFontSize As Byte = 5, _
                        Optional HeaderFontSize As Byte = 3, _
                        Optional TableBorder As Integer = 0, _
                        Optional CellPadding As Integer = 0, _
                        Optional CellSpacing As Integer = 5, _
                        Optional hexBodyBackground As String = "FFFFFF", _
                        Optional hexTitleBackground As String = "800000", _
                        Optional hexTitleForeground As String = "FFFFFF", _
                        Optional hexHeaderBackground As String = "FFFFEF", _
                        Optional hexHeaderForeground As String = "111111", _
                        Optional hexRecordsForeground As String = "111111", _
                        Optional hexTableBackground = "FFFFEF", _
                        Optional hexTableForeground = "111111", _
                        Optional hexBorderColor As String = "111111")

Dim TotalRecords As Long, I As Integer, NumberOfFields As Integer
Dim ErrorOccured As Boolean
Const Quote As String = """"

    On Error GoTo errSub
    RaiseEvent InicioDeExportacion(HTML)

    With Progress
        .Min = 0
        .Max = ADODBRecordset.RecordCount
        .Value = 0
    End With

    Open PathArchivoSalida For Output Access Write As #1

    With ADODBRecordset
        .MoveFirst
        NumberOfFields = .Fields.Count - 1

        Print #1, "<HTML><HEAD><TITLE>" & TitleOfHTML & "</TITLE></HEAD>"
        Print #1, "<meta name=""GENERATEDBY"" content="" [HME] ADO Recordset Export Class "">"
        Print #1, "<meta name=""GENERATEDINFO"" content = "" www.elvista.cjb.net "">"
        Print #1, "<BODY BGCOLOR= " & Quote & hexBodyBackground & Quote & " Text = " & Quote & hexRecordsForeground & Quote & ">"
        Print #1, "<TABLE BORDER= " & Quote & TableBorder & Quote & " CellPadding = " & Quote & CellPadding & Quote & " CellSpacing = " & Quote & CellSpacing & Quote & " BODERCOLOR = " & hexBorderColor & " BGCOLOR = " & Quote & hexTableBackground & Quote & " Width = " & Quote & "100%" & Quote & ">"
        Print #1, "<TR><TD WIDTH=""100%"" COLSPAN=" & Quote & NumberOfFields + 1 & Quote & " BGCOLOR=" & Quote & hexTitleBackground & Quote & ">"
        Print #1, "<FONT COLOR = " & Quote & hexTitleForeground & Quote & "FACE=" & TitleFont & " SIZE=" & Quote & TitleFontSize & Quote & "><B>" & TitleOfHTML & "</B></FONT></TD></TR>"

        Print #1,
        Print #1, "<!-- Database Headers are are listed below -->"
        Print #1,

        Print #1, "     <TR>"        'First, add the Usual HTML Tags ^^^
        For I = 0 To NumberOfFields  'Now, add the titles to the file
            Print #1, "          <TD BGCOLOR=" & hexHeaderBackground & "><B>"
            Print #1, "          <FONT COLOR=" & hexTableForeground & Quote & " FACE=" & Quote & HeaderFont & Quote & " SIZE=" & Quote & HeaderFontSize & Quote & ">" & .Fields(I).Name & "</FONT></B></TD>"
        Next I
        Print #1, "     </TR>"

        Print #1,
        Print #1, "<!-- Database Records are are listed below -->"
        Print #1,

        Do While Not .EOF
            Print #1, "  <TR>"  'Add database records in HTML Format
            For I = 0 To NumberOfFields
                Print #1, "    <TD>" & .Fields(I) & "</TD>"
            Next I
            Print #1, "  </TR>"
            Progress.Value = Progress.Value + 1
            .MoveNext
            DoEventsEx
        Loop

    End With

    Print #1, "</TABLE></BODY></HTML>"
    Close #1

    RaiseEvent ExportacionCompleta(Not ErrorOccured, HTML)

Exit Sub

errSub:
    RaiseEvent ErrorExportacion(Err, HTML)
    If Err.number = 0 Then
        Resume Next
        ErrorOccured = True
    End If

End Sub

Public Sub ExportarExcel(Optional SaveFile As Boolean = False, _
                         Optional VisibleInstance As Boolean = True, _
                         Optional Password As String = "", _
                         Optional WriteResPassword As String = "", _
                         Optional ReadOnlyRecommended As Boolean = False, _
                         Optional HeaderFont As String = "Tahoma", _
                         Optional HeaderFontSize As Integer = 9)

Dim iRowIndex As Integer, avRows As Variant, ErrorOccured As Boolean
Dim iFieldCount As Integer, objExcel As Object, objTemp As Object
Dim iColIndex As Integer, iRecordCount As Integer



    On Error GoTo errSub

    RaiseEvent InicioDeExportacion(Excel)

    With ADODBRecordset
        .MoveFirst
        avRows = .GetRows()
        iRecordCount = UBound(avRows, 2) + 1
        iFieldCount = UBound(avRows, 1) + 1
        Set objExcel = CreateObject("Excel.Application")
        objExcel.Visible = VisibleInstance
        objExcel.Workbooks.Add

        Set objTemp = objExcel           'Ensure excel remains visible

        If Val(objExcel.Application.Version) >= 8 Then
            Set objExcel = objExcel.ActiveSheet
        End If

        iRowIndex = 1

        objExcel.Visible = False

        'Place Name of the fields
        For iColIndex = 1 To iFieldCount
            With objExcel.Cells(iRowIndex, iColIndex)
                .Value = ADODBRecordset.Fields(iColIndex - 1).Name
                With .Font
                    .Name = HeaderFont      'Make the headers stand out
                    .Size = HeaderFontSize
                    .Bold = True
                End With
            End With
        Next iColIndex

    End With

    With Progress
        .Min = 0
        .Max = ADODBRecordset.RecordCount
        .Value = 0
    End With

    With objExcel
        
        For iRowIndex = 2 To iRecordCount + 1
            For iColIndex = 1 To iFieldCount
                .Cells(iRowIndex, iColIndex) = avRows(iColIndex - 1, iRowIndex - 2)
            Next iColIndex
            Progress.Value = Progress.Value + 1
            DoEventsEx
        Next iRowIndex
        .Cells(1, 1).CurrentRegion.EntireColumn.AutoFit
        If SaveFile Then
            .SaveAs PathArchivoSalida, , Password, WriteResPassword, ReadOnlyRecommended
        End If
    End With
    
    If Not VisibleInstance Then objExcel.Application.Quit
    Set objTemp = Nothing
    Set objExcel = Nothing
    
    RaiseEvent ExportacionCompleta(Not ErrorOccured, Excel)

Exit Sub

errSub:
    RaiseEvent ErrorExportacion(Err, Excel)
    If Err.number = 0 Then
        Resume Next
        ErrorOccured = True
    End If

End Sub

'Abre el recordset
Public Sub AbrirRecordset(ByRef rs As ADODB.Recordset, ByVal _
           NombreTabla As String, ByVal Conexion As ADODB.Connection, _
           Optional ByVal QueryParameter As String = "")

     
        rs.CursorLocation = adUseClient
        rs.Open Trim$("SELECT * FROM [" & NombreTabla & "] " & QueryParameter), Conexion, adOpenKeyset, adLockOptimistic
    

End Sub



Public Function Guardar(ByVal cmdlg As CommonDialog, ByVal ExportMode As _
                DatabaseExportEnum, Optional DialogTitle As String = _
                "Export Recordset to...") As Boolean

Dim Ext As String
    
    
    If ExportMode = HTML Then
        Ext = "Archivos (*.htm,*.html)|*.htm;*.html|"
    ElseIf ExportMode = Excel Then
        Ext = "Archivos Microsoft Excel (*.xls)|*.xls|"
    End If
        Ext = Ext & "All Files (*.*)|*.*"

    With cmdlg
        .CancelError = True
        .DialogTitle = DialogTitle
        .FileName = ""
        .Filter = Ext
        .FilterIndex = 0
        .ShowSave
        PathArchivoSalida = .FileName
    End With

    If PathArchivoSalida <> "" Then Guardar = True
    
End Function
