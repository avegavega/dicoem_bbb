Attribute VB_Name = "mod_querys_asientos"
Public Function AsientoContable(FiltroFecha As String, Id_Asiento As Integer, Optional ByVal Ip_Mes As Integer, Optional ByVal Ip_Ano As Integer, Optional ByVal IdBodega As String) As Recordset
    Dim Rs_A As Recordset, Sp_SwitchNC As String, Sp_Bodega As String
    Select Case Id_Asiento
        Case 1, 11
        
                '****************************************************
                'COMPRAS
                '****************************************************
                
                'Identificaremos si correosponde a NC o no NC
                If Id_Asiento = 1 Then Sp_SwitchNC = "NO" Else Sp_SwitchNC = "SI"
                '--------------------------------------------
                
                'If Len(IdBodega) > 0 Then
                
                        
                
                
                 Sql = "SELECT d.pla_id,pla_nombre,SUM(d.cmd_total_neto+d.cmd_exento) debe,0 iva " & _
                      "FROM com_doc_compra_detalle d " & _
                      "INNER JOIN com_doc_compra c ON d.id=c.id " & _
                      "INNER JOIN con_plan_de_cuentas p ON d.pla_id=p.pla_id " & _
                      "INNER JOIN sis_documentos x ON c.doc_id=x.doc_id " & _
                      "WHERE  com_informa_compra='SI' " & IdBodega & " AND  c.rut_emp='" & SP_Rut_Activo & "' AND ((doc_contable = 'SI' " & _
                      " AND  doc_nota_de_credito='" & Sp_SwitchNC & "'" & FiltroFecha & ") OR " & _
                      "(doc_id_factura>0 AND (SELECT mes_contable FROM com_doc_compra p JOIN sis_documentos k WHERE  doc_nota_de_credito='" & Sp_SwitchNC & "' " & IdBodega & " AND p.no_documento=c.nro_factura AND p.doc_id=c.doc_id_factura LIMIT 1)=" & Ip_Mes & " " & _
                      " AND (SELECT ano_contable FROM com_doc_compra p  JOIN sis_documentos k USING(doc_id) WHERE doc_nota_de_credito='" & Sp_SwitchNC & "' " & IdBodega & " AND p.no_documento=c.nro_factura AND p.doc_id=c.doc_id_factura LIMIT 1)=" & Ip_Ano & ")) " & _
                      "GROUP BY d.pla_id " & _
                      "UNION SELECT " & IG_Id_CuentaCreditoFiscal & ",'IVA CREDITO FISCAL',SUM(iva),0  " & _
                      "FROM com_doc_compra c " & _
                      "INNER JOIN sis_documentos d USING(doc_id) " & _
                      "WHERE c.rut_emp = '" & SP_Rut_Activo & "' " & IdBodega & " AND doc_contable = 'SI' AND doc_nota_de_credito = '" & Sp_SwitchNC & "' " & FiltroFecha & " " & _
                      "/*UNION SELECT IF(e.ime_costo_credito = 'COSTO',imp_id_cuenta_costo,imp_id_cuenta_credito)pla_id," & _
                      "IF(e.ime_costo_credito = 'COSTO',(SELECT pla_nombre FROM con_plan_de_cuentas a WHERE a.pla_id=imp_id_cuenta_costo)," & _
                        "(SELECT pla_nombre FROM con_plan_de_cuentas a WHERE a.pla_id=imp_id_cuenta_credito)) pla_nombre,SUM(coi_valor) debe,0 iva " & _
                       " FROM com_impuestos i " & _
                        "INNER JOIN par_impuestos_empresas e USING(imp_id) " & _
                        "INNER JOIN com_doc_compra c ON i.id = c.id " & _
                        "INNER JOIN sis_documentos x ON c.doc_id = x.doc_id " & _
                        "JOIN par_impuestos p ON i.imp_id=p.imp_id " & _
                        "WHERE ime_costo_credito<>'COSTO' AND com_informa_compra='SI' " & IdBodega & " AND  e.rut='" & SP_Rut_Activo & "' AND c.rut_emp='" & SP_Rut_Activo & "' AND (doc_contable = 'SI' OR c.doc_id_factura>0)  AND doc_nota_de_credito='" & Sp_SwitchNC & "' " & FiltroFecha & " " & _
                        "GROUP BY e.ime_id */ "
                Sql = Sql & "UNION SELECT " & IG_Id_CuentaIvaRetenido & ",'IVA RETENIDO',0,SUM(iva_retenido)ivaretenido  " & _
                      "FROM com_doc_compra c " & _
                      "INNER JOIN sis_documentos d USING(doc_id) " & _
                      "WHERE  com_informa_compra='SI' " & IdBodega & " AND  c.rut_emp = '" & SP_Rut_Activo & "' AND doc_contable = 'SI'  AND doc_nota_de_credito = '" & Sp_SwitchNC & "' " & FiltroFecha & " HAVING ivaretenido>0 "
                      
                 Sql = Sql & " UNION SELECT p.imp_id_cuenta_costo,p.imp_nombre,SUM(coi_valor) debe,0 haber " & _
                                        "FROM com_impuestos i " & _
                                        "JOIN par_impuestos p ON i.imp_id=p.imp_id " & _
                                        "WHERE i.id IN( " & _
                                            "SELECT id " & _
                                            "FROM com_doc_compra c " & _
                                            "JOIN sis_documentos d ON c.doc_id=d.doc_id " & _
                                            "WHERE rut_emp='" & SP_Rut_Activo & "' AND doc_contable = 'SI' " & _
                                                     "AND doc_nota_de_credito = '" & Sp_SwitchNC & "' " & FiltroFecha & _
                                            ") GROUP BY i.imp_id"
                      
                      
                      
                      
                Sql = Sql & " UNION SELECT  " & _
                                "CASE c.pago WHEN 'CONTADO' THEN " & IG_Id_CuentaCaja & " WHEN 'CREDITO' THEN " & IG_IdCtaProveedores & " WHEN 'FONDOS POR RENDIR' THEN " & IG_IdCtaFondo & " WHEN 'CON ANTICIPO PROVEEDOR' THEN " & IG_Id_CuentaAnticipoProveedores & " END pla_id," & _
                                "CASE c.pago WHEN 'CONTADO' THEN 'CAJA' WHEN 'CREDITO' THEN 'PROVEEDORES' WHEN 'FONDOS POR RENDIR' THEN  'FONDOS POR RENDIR' WHEN 'CON ANTICIPO PROVEEDOR' THEN 'ANTICIPO PROVEEDORES' END pla_nombre," & _
                                "0 debe, SUM(c.total)haber " & _
                               " FROM    com_doc_compra c " & _
                                "INNER JOIN sis_documentos x ON c.doc_id = x.doc_id " & _
                                "WHERE  com_informa_compra='SI' " & IdBodega & " AND   c.rut_emp = '" & SP_Rut_Activo & "' " & _
                                "AND doc_contable = 'SI' " & _
                                "AND doc_nota_de_credito = '" & Sp_SwitchNC & "' " & FiltroFecha & _
                                " GROUP BY c.pago "
                      
          
            
        
        
        Case 2, 10
                '*************************
                'VENTAS
                '*************************
                If Id_Asiento = 2 Then Sp_SwitchNC = "NO" Else Sp_SwitchNC = "SI"
                If SP_Control_Inventario = "NO" Then
                
                                        'Venta SIN con control de inventario
               '     Sql = "SELECT " & _
                                    "CASE v.condicionpago WHEN 'CONTADO' THEN " & IG_Id_CuentaCaja & " WHEN 'CREDITO' THEN " & IG_IdCtaClientes & " END pla_id," & _
                                    "CASE v.condicionpago " & _
                                    "WHEN 'CONTADO' THEN 'CAJA' " & _
                                    "WHEN 'CREDITO' THEN 'CLIENTES' " & _
                                    "END pla_nombre," & _
                                    "SUM(v.bruto) debe,0 haber " & _
                                "FROM ven_doc_venta v " & _
                                "INNER JOIN sis_documentos x ON v.doc_id = x.doc_id " & _
                                "WHERE  ven_informa_venta='SI' " & IdBodega & " AND  rut_cliente<>'NULO' AND v.rut_emp = '" & SP_Rut_Activo & "' AND(doc_contable = 'SI') AND doc_nota_de_credito = '" & Sp_SwitchNC & "' " & FiltroFecha & " " & _
                                "GROUP BY pla_nombre "
                                
                          Sql = "SELECT " & _
                                    "CASE v.condicionpago WHEN 'CONTADO' THEN " & _
                                        "IF(" & _
                                    "(SELECT count(d.id) FROM cta_abonos a " & _
                                        "JOIN cta_abono_documentos d ON a.abo_id=d.abo_id " & _
                                        "JOIN abo_tipos_de_pagos t ON a.abo_id=t.abo_id " & _
                                        "WHERE a.abo_cli_pro='CLI' AND a.rut_emp='" & SP_Rut_Activo & "' AND d.rut_emp='" & SP_Rut_Activo & "' AND abo_origen='VENTA' " & _
                                        "AND v.id=d.id AND t.mpa_id IN (7,8) " & _
                                    ")>0," & IG_Id_CtaCuentasPorCobrar & "," & IG_Id_CuentaCaja & ") "
                                Sql = Sql & " WHEN 'CREDITO' THEN " & _
                                    "IF((SELECT pla_nombre FROM par_plazos_vencimiento ven WHERE v.ven_plazo_id=ven.pla_id)='CHEQUE'," & IG_Id_CtaDocumentosCartera & "," & IG_IdCtaClientes & ") " & _
                                    " END pla_id," & _
                                    "CASE v.condicionpago " & _
                                    "WHEN 'CONTADO' THEN " & _
                                      "IF((SELECT count(d.id) FROM cta_abonos a " & _
                                        "JOIN cta_abono_documentos d ON a.abo_id=d.abo_id " & _
                                        "JOIN abo_tipos_de_pagos t ON a.abo_id=t.abo_id " & _
                                        "WHERE a.abo_cli_pro='CLI' AND a.rut_emp='" & SP_Rut_Activo & "' AND d.rut_emp='" & SP_Rut_Activo & "' AND abo_origen='VENTA' " & _
                                        "AND v.id=d.id AND t.mpa_id IN (7,8) " & _
                                    ")>0,'CUENTAS POR COBRAR','CAJA') "
                                    Sql = Sql & _
                                    "WHEN 'CREDITO' THEN " & _
                                    "IF( (SELECT pla_nombre FROM par_plazos_vencimiento ven WHERE v.ven_plazo_id=ven.pla_id)='CHEQUE','DCTOS. EN CARTERA','CLIENTES') " & _
                                    "END pla_nombre," & _
                                    "SUM(v.bruto) debe,0 haber " & _
                                "FROM ven_doc_venta v " & _
                                "INNER JOIN sis_documentos x ON v.doc_id = x.doc_id " & _
                                "WHERE   ven_solo_activo_inmobilizado ='NO' AND  ven_informa_venta='SI' " & IdBodega & "  AND  rut_cliente<>'NULO' AND v.rut_emp = '" & SP_Rut_Activo & "' AND(doc_contable = 'SI') AND doc_nota_de_credito = '" & Sp_SwitchNC & "' " & FiltroFecha & " " & _
                                "GROUP BY pla_nombre "
                                
                                
                                
                                
                    Sql = Sql & " UNION SELECT d.pla_id,pla_nombre,0,SUM(d.vsd_exento)+SUM(d.vsd_neto) haber  " & _
                            "FROM ven_sin_detalle d " & _
                            "INNER JOIN ven_doc_venta v USING(id) " & _
                            "INNER JOIN con_plan_de_cuentas p ON d.pla_id=p.pla_id " & _
                            "INNER JOIN sis_documentos x ON v.doc_id=x.doc_id " & _
                            "WHERE   ven_solo_activo_inmobilizado ='NO' AND  ven_informa_venta='SI' AND v.id IN(" & _
                                "SELECT v.id " & _
                                "FROM ven_doc_venta v " & _
                                "WHERE  ven_informa_venta='SI'  " & IdBodega & " AND v.nro_factura > 0 AND rut_emp = '" & SP_Rut_Activo & "' AND MONTH(" & _
                                    "(SELECT x.fecha " & _
                                        "FROM ven_doc_venta x " & _
                                        "WHERE   ven_solo_activo_inmobilizado ='NO' AND  ven_informa_venta='SI'  " & IdBodega & " AND   x.no_documento = v.nro_factura AND x.doc_id=v.doc_id_factura AND rut_emp='" & SP_Rut_Activo & "'))= " & Ip_Mes & _
                                        " AND YEAR((SELECT x.fecha " & _
                                                            "FROM ven_doc_venta x " & _
                                                            "JOIN sis_documentos h USING(doc_id) " & _
                                                            "WHERE   ven_informa_venta='SI' " & IdBodega & "  AND h.doc_nota_de_credito='" & Sp_SwitchNC & "' AND x.no_documento = v.nro_factura AND x.doc_id=v.doc_id_factura AND rut_emp='" & SP_Rut_Activo & "'))= " & Ip_Ano & _
                                " UNION " & _
                                "SELECT k.id " & _
                                "FROM ven_doc_venta k " & _
                                "INNER JOIN sis_documentos r USING(doc_id) " & _
                                "WHERE   ven_solo_activo_inmobilizado ='NO' AND  ven_informa_venta='SI'  " & IdBodega & "  AND r.doc_contable='SI' and k.rut_emp = '" & SP_Rut_Activo & "' " & _
                                "AND MONTH(k.fecha)=" & Ip_Mes & _
                                " AND YEAR(k.fecha)=" & Ip_Ano & " AND r.doc_nota_de_credito = '" & Sp_SwitchNC & "') " & _
                                "GROUP BY d.pla_id "
                    Sql = Sql & "UNION SELECT " & IG_Id_CuentaDebitoFiscal & ",'IVA DEBITO FISCAL',0,SUM(iva) " & _
                            "FROM ven_doc_venta v " & _
                            "INNER JOIN sis_documentos x ON v.doc_id=x.doc_id " & _
                            "WHERE   ven_solo_activo_inmobilizado ='NO' AND  ven_informa_venta='SI'  " & IdBodega & " AND v.rut_emp='" & SP_Rut_Activo & "' AND doc_contable='SI'  AND doc_nota_de_credito='" & Sp_SwitchNC & "' " & FiltroFecha & " " & _
                            "UNION SELECT " & IG_Id_CuentaIvaRetenido & ",'IVA RETENIDO',SUM(ven_iva_retenido)ivaretenido,0 " & _
                            "FROM ven_doc_venta v " & _
                            "INNER JOIN sis_documentos x ON v.doc_id=x.doc_id " & _
                            "WHERE   ven_solo_activo_inmobilizado ='NO' AND  ven_informa_venta='SI'  " & IdBodega & "  AND v.rut_emp='" & SP_Rut_Activo & "' AND doc_contable='SI'  AND doc_nota_de_credito='" & Sp_SwitchNC & "' " & FiltroFecha & " "
                    Sql = Sql & " " & _
                                "GROUP BY v.condicionpago " & _
                                "HAVING ivaretenido>0 "
        
              
                Else
                    
                    'Venta con control de inventario
                    Sql = "SELECT " & _
                                    "CASE v.condicionpago WHEN 'CONTADO' THEN " & _
                                        "IF(" & _
                                    "(SELECT count(d.id) FROM cta_abonos a " & _
                                        "JOIN cta_abono_documentos d ON a.abo_id=d.abo_id " & _
                                        "JOIN abo_tipos_de_pagos t ON a.abo_id=t.abo_id " & _
                                        "WHERE a.abo_cli_pro='CLI' AND a.rut_emp='" & SP_Rut_Activo & "' AND d.rut_emp='" & SP_Rut_Activo & "' AND abo_origen='VENTA' " & _
                                        "AND v.id=d.id AND t.mpa_id IN (7,8) " & _
                                    ")>0," & IG_Id_CtaCuentasPorCobrar & "," & IG_Id_CuentaCaja & ") "
                                Sql = Sql & " WHEN 'CREDITO' THEN " & _
                                    "IF((SELECT pla_nombre FROM par_plazos_vencimiento ven WHERE v.ven_plazo_id=ven.pla_id)='CHEQUE'," & IG_Id_CtaDocumentosCartera & "," & IG_IdCtaClientes & ") " & _
                                    " END pla_id," & _
                                    "CASE v.condicionpago " & _
                                    "WHEN 'CONTADO' THEN " & _
                                      "IF((SELECT count(d.id) FROM cta_abonos a " & _
                                        "JOIN cta_abono_documentos d ON a.abo_id=d.abo_id " & _
                                        "JOIN abo_tipos_de_pagos t ON a.abo_id=t.abo_id " & _
                                        "WHERE a.abo_cli_pro='CLI' AND a.rut_emp='" & SP_Rut_Activo & "' AND d.rut_emp='" & SP_Rut_Activo & "' AND abo_origen='VENTA' " & _
                                        "AND v.id=d.id AND t.mpa_id IN (7,8) " & _
                                    ")>0,'CUENTAS POR COBRAR','CAJA') "
                                    Sql = Sql & _
                                    "WHEN 'CREDITO' THEN " & _
                                    "IF( (SELECT pla_nombre FROM par_plazos_vencimiento ven WHERE v.ven_plazo_id=ven.pla_id)='CHEQUE','DCTOS. EN CARTERA','CLIENTES') " & _
                                    "END pla_nombre," & _
                                    "SUM(v.bruto) debe,0 haber " & _
                                "FROM ven_doc_venta v " & _
                                "INNER JOIN sis_documentos x ON v.doc_id = x.doc_id " & _
                                "WHERE   ven_solo_activo_inmobilizado ='NO' AND  ven_informa_venta='SI' " & IdBodega & "  AND  rut_cliente<>'NULO' AND v.rut_emp = '" & SP_Rut_Activo & "' AND(doc_contable = 'SI') AND doc_nota_de_credito = '" & Sp_SwitchNC & "' " & FiltroFecha & " " & _
                                "GROUP BY pla_nombre"
                                
                                
                    Sql = Sql & " UNION SELECT d.pla_id,pla_nombre,0,SUM(d.ved_precio_venta_neto) haber  " & _
                            "FROM ven_detalle d " & _
                            "INNER JOIN ven_doc_venta v ON (v.no_documento=d.no_documento AND v.doc_id=d.doc_id AND d.rut_emp=v.rut_emp) " & _
                            "INNER JOIN con_plan_de_cuentas p ON d.pla_id=p.pla_id " & _
                            "INNER JOIN sis_documentos x ON v.doc_id=x.doc_id " & _
                            "WHERE   ven_solo_activo_inmobilizado ='NO' AND  ven_informa_venta='SI'  " & IdBodega & " AND v.id IN(" & _
                                "SELECT v.id " & _
                                "FROM ven_doc_venta v " & _
                                "WHERE   ven_solo_activo_inmobilizado ='NO' AND  ven_informa_venta='SI'  " & IdBodega & "  AND v.nro_factura > 0 AND rut_emp = '" & SP_Rut_Activo & "' AND MONTH(v.fecha)=" & Ip_Mes & " " & _
                                    "AND YEAR(v.fecha)=" & Ip_Ano & "  " & _
                                " UNION " & _
                                "SELECT k.id " & _
                                "FROM ven_doc_venta k " & _
                                "INNER JOIN sis_documentos r USING(doc_id) " & _
                                "WHERE   ven_solo_activo_inmobilizado ='NO' AND  ven_informa_venta='SI'  " & IdBodega & "  AND r.doc_contable='SI' and k.rut_emp = '" & SP_Rut_Activo & "' " & _
                                "AND MONTH(k.fecha)=" & Ip_Mes & _
                                " AND YEAR(k.fecha)=" & Ip_Ano & " AND r.doc_nota_de_credito = '" & Sp_SwitchNC & "') " & _
                                "GROUP BY d.pla_id  "
                    Sql = Sql & "UNION SELECT " & IG_Id_CuentaDebitoFiscal & ",'IVA DEBITO FISCAL',0,SUM(iva) " & _
                            "FROM ven_doc_venta v " & _
                            "INNER JOIN sis_documentos x ON v.doc_id=x.doc_id " & _
                            "WHERE  ven_solo_activo_inmobilizado ='NO' AND ven_informa_venta='SI'  " & IdBodega & " AND v.rut_emp='" & SP_Rut_Activo & "' AND doc_contable='SI'  AND doc_nota_de_credito='" & Sp_SwitchNC & "' " & FiltroFecha & " " & _
                            "UNION SELECT " & IG_Id_CuentaIvaRetenido & ",'IVA RETENIDO',SUM(ven_iva_retenido) ivaretenido,0 " & _
                            "FROM ven_doc_venta v " & _
                            "INNER JOIN sis_documentos x ON v.doc_id=x.doc_id " & _
                            "WHERE  ven_informa_venta='SI'  " & IdBodega & " AND v.rut_emp='" & SP_Rut_Activo & "' AND doc_contable='SI'  AND doc_nota_de_credito='" & Sp_SwitchNC & "' " & FiltroFecha & " GROUP BY v.condicionpago  HAVING ivaretenido>0 "
                   
                    
                End If
         
        Case 4
                '***************************
                '   GASTOS
                '***************************
                
        
                Sql = " SELECT p.pla_id,p.pla_nombre,SUM(total) debe, 0 haber " & _
                        "FROM com_doc_compra c " & _
                         "INNER JOIN con_plan_de_cuentas p USING(pla_id) " & _
                         "INNER JOIN sis_documentos USING(doc_id) " & _
                         "WHERE rut_emp='" & SP_Rut_Activo & "' AND doc_libro_gastos='SI'  " & _
                         FiltroFecha & " " & _
                         "GROUP BY c.pla_id " & _
                         "UNION " & _
                         "SELECT " & _
                                "CASE c.pago WHEN 'CONTADO' THEN " & IG_Id_CuentaCaja & " WHEN 'CREDITO' THEN " & IG_IdCtaProveedores & " WHEN 'FONDOS POR RENDIR' THEN " & IG_IdCtaFondo & " END pla_id," & _
                                "CASE c.pago WHEN 'CONTADO' THEN 'CAJA' WHEN 'CREDITO' THEN 'PROVEEDORES' WHEN 'FONDOS POR RENDIR' THEN  'FONDOS POR RENDIR' END pla_nombre," & _
                                 "0 debe, SUM(total) haber " & _
                         "FROM com_doc_compra c " & _
                         "INNER JOIN con_plan_de_cuentas p USING(pla_id) " & _
                         "INNER JOIN sis_documentos USING(doc_id) " & _
                         "WHERE rut_emp = '" & SP_Rut_Activo & "' AND doc_libro_gastos = 'SI' " & FiltroFecha & " " & _
                         "GROUP BY pago"
        
        
        
        Case 5 'Caja clientes
            
            
                Sql = "SELECT " & IG_Id_CuentaCaja & ",'CAJA',SUM(pad_valor),0 " & _
                      "FROM cta_abonos a " & _
                      "INNER JOIN abo_tipos_de_pagos t USING(abo_id) " & _
                      "WHERE " & _
                      "(SELECT COUNT(ctd_id) " & _
                            "FROM cta_abono_documentos d " & _
                            "JOIN ven_doc_venta v ON d.id=v.id " & _
                            "WHERE d.abo_id=a.abo_id AND condicionpago='CONTADO' ) =0 " & _
                      "AND abo_cli_pro='CLI' AND rut_emp='" & SP_Rut_Activo & "' AND t.mpa_id = 1 " & FiltroFecha & " " & _
                      "UNION " & _
                      "SELECT " & IG_IdCtaClientes & ",'CLIENTES',0,SUM(pad_valor) " & _
                      "FROM cta_abonos a " & _
                      "INNER JOIN abo_tipos_de_pagos t USING(abo_id) " & _
                      "WHERE (SELECT COUNT(ctd_id) " & _
                            "FROM cta_abono_documentos d " & _
                            "JOIN ven_doc_venta v ON d.id=v.id " & _
                            "WHERE d.abo_id=a.abo_id AND condicionpago='CONTADO' ) =0 " & _
                      "AND abo_cli_pro='CLI' AND rut_emp='" & SP_Rut_Activo & "' AND t.mpa_id=1 " & FiltroFecha
                    
        
        
        Case 6 'Proveedores Caja
        
               Sql = "SELECT " & IG_IdCtaProveedores & ",'PROVEEDORES',SUM(pad_valor),0 " & _
                    "FROM cta_abonos a " & _
                    "INNER JOIN abo_tipos_de_pagos t USING(abo_id) " & _
                    "WHERE (SELECT COUNT(ctd_id) " & _
                            "FROM cta_abono_documentos d " & _
                            "JOIN com_doc_compra v ON d.id=v.id " & _
                            "WHERE d.abo_id=a.abo_id AND pago='CONTADO' ) =0 " & _
                      "AND  abo_cli_pro='PRO' AND rut_emp='" & SP_Rut_Activo & "' AND t.mpa_id = 1 " & FiltroFecha & " " & _
                    "UNION " & _
                    "SELECT " & IG_Id_CuentaCaja & ",'CAJA',0,SUM(pad_valor) " & _
                    "FROM cta_abonos a " & _
                    "INNER JOIN abo_tipos_de_pagos t USING(abo_id) " & _
                    "WHERE  (SELECT COUNT(ctd_id) " & _
                            "FROM cta_abono_documentos d " & _
                            "JOIN com_doc_compra v ON d.id=v.id " & _
                            "WHERE d.abo_id=a.abo_id AND pago='CONTADO' ) =0 " & _
                      "AND abo_cli_pro='PRO' AND rut_emp='" & SP_Rut_Activo & "' AND t.mpa_id=1 " & FiltroFecha
        Case 7
        
        
                            'ASIENTOS GIROS-CARGOS
                    Sql = "SELECT  com_con_multicuenta.pla_id,pla_nombre,SUM(dcu_valor) debe, 0 haber " & _
                          "FROM com_con_multicuenta " & _
                          "INNER JOIN con_plan_de_cuentas USING(pla_id) " & _
                            "WHERE id_unico IN(" & _
                            "SELECT che_id " & _
                            "FROM ban_cheques q " & _
                            "INNER JOIN ban_movimientos m ON m.mov_id=q.mov_id " & _
                            "INNER JOIN ban_cta_cte c USING(cte_id) " & _
                            "WHERE  mov_apertura='NO' AND /*mov_tipo = 'CARGO' */ " & _
                            "  mov_identificacion='CHEQUE' AND com_con_multicuenta.dcu_tipo='CHEQUE' " & _
                            "AND c.rut_emp = '" & SP_Rut_Activo & "' " & FiltroFecha & " " & _
                                "UNION " & _
                                "SELECT mov_id " & _
                                "FROM ban_movimientos m " & _
                                "WHERE mov_apertura='NO' AND mov_tipo = 'CARGO'   AND com_con_multicuenta.dcu_tipo<>'CHEQUE' /*AND mov_identificacion<>'TRANSFERENCIA'*/ " & _
                                FiltroFecha & ") " & _
                                "GROUP BY com_con_multicuenta.pla_id " & _
                            "UNION "
                
                            Sql = Sql & " " & _
                            "SELECT c.ban_id,cte_nombre_banco,0 debe,SUM(mov_valor) haber " & _
                            "FROM ban_movimientos m " & _
                            "INNER JOIN ban_cta_cte c USING(cte_id) " & _
                            "WHERE mov_apertura='NO' AND mov_tipo='CARGO' " & FiltroFecha & _
                            "GROUP BY cte_numero"
        
        
     '    Sql = "SELECT  com_con_multicuenta.pla_id,pla_nombre,SUM(dcu_valor) debe, 0 haber " & _
                      "FROM com_con_multicuenta " & _
                      "INNER JOIN con_plan_de_cuentas USING(pla_id) " & _
                        "WHERE id_unico IN(" & _
                        "SELECT che_id " & _
                        "FROM ban_cheques q " & _
                        "INNER JOIN ban_movimientos m ON m.mov_id=q.mov_id " & _
                        "INNER JOIN ban_cta_cte c USING(cte_id) " & _
                        "WHERE mov_tipo = 'CARGO' " & _
                        "/* AND mov_identificacion='CHEQUE' */" & _
                        "AND c.rut_emp = '" & SP_Rut_Activo & "' " & Sp_Fechas & " " & _
                            "UNION " & _
                            "SELECT mov_id " & _
                            "FROM ban_movimientos m " & _
                            "WHERE mov_tipo = 'CARGO'  /*AND mov_identificacion<>'TRANSFERENCIA'*/ " & _
                            Sp_Fechas & ") " & _
                            "GROUP BY com_con_multicuenta.pla_id " & _
                        "UNION "
            
                     '  Sql = Sql & " " & _
                        "SELECT c.ban_id,cte_nombre_banco,0 debe,SUM(mov_valor) haber " & _
                        "FROM ban_movimientos m " & _
                        "INNER JOIN ban_cta_cte c USING(cte_id) " & _
                        "WHERE mov_tipo='CARGO' " & Sp_Fechas & _
                        "GROUP BY cte_numero"
                'BANCOS GIROS
                 'ASIENTOS GIROS-CARGOS
                 
                 
                  '  Sql = "SELECT  com_con_multicuenta.pla_id,pla_nombre,SUM(dcu_valor) debe, 0 haber " & _
                          "FROM com_con_multicuenta " & _
                          "INNER JOIN con_plan_de_cuentas USING(pla_id) " & _
                            "WHERE id_unico IN(" & _
                            "SELECT che_id " & _
                            "FROM ban_cheques q " & _
                            "INNER JOIN ban_movimientos m ON m.mov_id=q.mov_id " & _
                            "INNER JOIN ban_cta_cte c USING(cte_id) " & _
                            "WHERE mov_apertura='NO' AND mov_tipo = 'CARGO' " & _
                            "AND mov_identificacion='CHEQUE' " & _
                            "AND c.rut_emp = '" & SP_Rut_Activo & "' " & FiltroFecha & ") " & _
                            "GROUP BY com_con_multicuenta.pla_id " & _
                            "UNION "
                   ' Sql = Sql & _
                    "SELECT s.pla_id, s.pla_nombre,SUM(mov_valor) debe,0 haber " & _
                            "FROM ban_movimientos m  " & _
                            "INNER JOIN ban_cta_cte c USING(cte_id) " & _
                            "INNER JOIN con_plan_de_cuentas s USING(pla_id) " & _
                            "WHERE mov_apertura='NO' AND  mov_identificacion<>'CHEQUE' AND mov_tipo='CARGO' AND c.rut_emp='" & SP_Rut_Activo & "' " & FiltroFecha & _
                            "GROUP BY m.pla_id " & _
                            "UNION " & _
                            "SELECT c.ban_id,cte_nombre_banco,0 debe,SUM(mov_valor) haber " & _
                            "FROM ban_movimientos m " & _
                            "INNER JOIN ban_cta_cte c USING(cte_id) " & _
                            "INNER JOIN con_plan_de_cuentas s USING(pla_id) " & _
                            "WHERE mov_apertura='NO' AND  mov_tipo='CARGO' AND c.rut_emp='" & SP_Rut_Activo & "' " & FiltroFecha & _
                            "GROUP BY cte_numero"
                            
                            
        Case 8
        
                                
                'ASIENTO DEPOSITOS
                Sql = "SELECT ban_id,cte_nombre_banco,SUM(mov_valor) DEBE,0 HABER " & _
                        "FROM ban_movimientos m " & _
                        "INNER JOIN ban_cta_cte c USING(cte_id) " & _
                        "/*INNER JOIN par_bancos b USING(ban_id) */" & _
                        "INNER JOIN con_plan_de_cuentas s USING(pla_id) " & _
                        "WHERE mov_apertura='NO' AND mov_tipo='INGRESO' AND c.rut_emp='" & SP_Rut_Activo & "' " & FiltroFecha & _
                        "UNION " & _
                        "SELECT com_con_multicuenta.pla_id,pla_nombre,0 debe,SUM(dcu_valor) haber " & _
                            "FROM com_con_multicuenta " & _
                            "INNER JOIN con_plan_de_cuentas USING (pla_id) " & _
                            "WHERE id_unico IN ( " & _
                                "SELECT mov_id " & _
                                "FROM ban_movimientos m " & _
                                "WHERE mov_apertura='NO' AND  mov_tipo = 'INGRESO' /* AND mov_identificacion='CHEQUE' */ " & _
                                "/* AND  MID(mov_identificacion,1,13)<>'TRANSFERENCIA' */" & _
                                "AND (com_con_multicuenta.dcu_tipo = 'DEPOSITO' OR  com_con_multicuenta.dcu_tipo='TRANSFERENCIA') " & _
                                FiltroFecha & ") " & _
                            "GROUP BY com_con_multicuenta.pla_id "
                
               ' Consulta RsTmp, Sql
        
                'BANCOS DEPOSITOS
                     'Sql = "SELECT c.ban_id,cte_nombre_banco,SUM(mov_valor) DEBE,0 HABER " & _
                            "FROM ban_movimientos m " & _
                            "INNER JOIN ban_cta_cte c USING(cte_id) " & _
                            "INNER JOIN con_plan_de_cuentas s USING(pla_id) " & _
                            "WHERE mov_apertura='NO' AND mov_tipo='INGRESO' AND c.rut_emp='" & SP_Rut_Activo & "' " & FiltroFecha & _
                            " HAVING DEBE>0  " & _
                            "UNION " & _
                            "SELECT m.pla_id,s.pla_nombre,0 DEBE,SUM(mov_valor) HABER " & _
                            "FROM ban_movimientos m " & _
                            "INNER JOIN ban_cta_cte c USING(cte_id) " & _
                            "/*INNER JOIN par_bancos b USING(ban_id) */" & _
                            "INNER JOIN con_plan_de_cuentas s USING(pla_id) " & _
                            "WHERE mov_apertura='NO' AND mov_tipo='INGRESO' AND c.rut_emp='" & SP_Rut_Activo & "' " & FiltroFecha & _
                            "GROUP BY m.pla_id "
                            
                    
                
                
                
        
        Case 9 'Fondos por rendir
                
                 Sql = "SELECT  " & IG_Id_CuentaCaja & ",  'CAJA', SUM(pad_valor), 0 " & _
                        "FROM    cta_abonos a " & _
                        "INNER JOIN abo_tipos_de_pagos t USING(abo_id) " & _
                        "WHERE abo_rut='" & SP_Rut_Activo & "' AND  abo_cli_pro = 'PRO' AND rut_emp = '" & SP_Rut_Activo & "'AND t.mpa_id = 9999 " & _
                        FiltroFecha & " " & _
                        "UNION " & _
                        "SELECT " & IG_IdCtaFondo & ",'FONDOS POR RENDIR',0,SUM(pad_valor) " & _
                        "FROM cta_abonos a " & _
                        "INNER JOIN abo_tipos_de_pagos t USING(abo_id) " & _
                        "WHERE abo_rut='" & SP_Rut_Activo & "' AND abo_cli_pro = 'PRO' AND rut_emp = '" & SP_Rut_Activo & "'AND t.mpa_id = 9999 " & _
                        FiltroFecha
        
                
          Case 12
                    Sql = "SELECT  t.pla_id,   p.pla_nombre,   SUM(pad_valor), 0 " & _
                            "FROM    cta_abonos a " & _
                            "INNER JOIN abo_tipos_de_pagos t USING(abo_id) " & _
                            "JOIN con_plan_de_cuentas p ON t.pla_id=p.pla_id " & _
                            "WHERE    abo_cli_pro = " & IIf(Id_Asiento = 12, "'CLI'", "'PRO'") & " AND rut_emp = '" & SP_Rut_Activo & "' AND t.mpa_id = 5555 " & FiltroFecha & _
                           " GROUP BY t.pla_id " & _
                            "UNION " & _
                                "SELECT " & IIf(Id_Asiento = 12, IG_IdCtaClientes, IG_IdCtaProveedores) & "," & IIf(Id_Asiento = 12, "'CLIENTES'", "'PROVEEDORES'") & ",0,SUM(pad_valor) " & _
                                "FROM cta_abonos a " & _
                                "INNER JOIN abo_tipos_de_pagos t USING(abo_id) " & _
                                "WHERE   abo_cli_pro = " & IIf(Id_Asiento = 12, "'CLI'", "'PRO'") & " AND rut_emp = '" & SP_Rut_Activo & "' AND t.mpa_id = 5555 " & FiltroFecha
                                
          Case 13
                    Sql = " SELECT " & IIf(Id_Asiento = 12, IG_IdCtaClientes, IG_IdCtaProveedores) & "," & IIf(Id_Asiento = 12, "'CLIENTES'", "'PROVEEDORES'") & ",SUM(pad_valor),0 " & _
                                "FROM cta_abonos a " & _
                                "INNER JOIN abo_tipos_de_pagos t USING(abo_id) " & _
                                "WHERE   abo_cli_pro = " & IIf(Id_Asiento = 12, "'CLI'", "'PRO'") & " AND rut_emp = '" & SP_Rut_Activo & "' AND t.mpa_id = 5555 " & FiltroFecha
                    Sql = Sql & "UNION SELECT  t.pla_id,   p.pla_nombre,0,SUM(pad_valor) " & _
                            "FROM    cta_abonos a " & _
                            "INNER JOIN abo_tipos_de_pagos t USING(abo_id) " & _
                            "JOIN con_plan_de_cuentas p ON t.pla_id=p.pla_id " & _
                            "WHERE    abo_cli_pro = " & IIf(Id_Asiento = 12, "'CLI'", "'PRO'") & " AND rut_emp = '" & SP_Rut_Activo & "' AND t.mpa_id = 5555 " & FiltroFecha & _
                           " GROUP BY t.pla_id "
                           
            Case 15 'CHEQUES EN CARTERA
                    Sql = "SELECT  " & IG_Id_CtaDocumentosCartera & ",'DOCUMENTOS EN CARTERA',SUM(pad_valor),0 " & _
                            "FROM cta_abonos a " & _
                            "INNER JOIN abo_tipos_de_pagos t USING(abo_id) " & _
                            "WHERE abo_origen='CTACTE'  AND abo_cli_pro='CLI' AND rut_emp='" & SP_Rut_Activo & "' AND t.mpa_id = 2 " & FiltroFecha & " " & _
                            "UNION " & _
                                "SELECT " & IG_IdCtaClientes & ",'CLIENTES',0,SUM(pad_valor) " & _
                                "FROM cta_abonos a " & _
                                "INNER JOIN abo_tipos_de_pagos t USING(abo_id) " & _
                                "WHERE abo_origen='CTACTE'  AND abo_cli_pro='CLI' AND rut_emp='" & SP_Rut_Activo & "' AND t.mpa_id = 2 " & FiltroFecha

                    
            Case 16 'PAGOS DE CLIENTES CON TARJETA CREDITO/DEBITO Y VALES VISTA
            
                        
                Sql = "SELECT " & IG_Id_CtaCuentasPorCobrar & ",'CUENTAS POR COBRAR',SUM(pad_valor),0 " & _
                      "FROM cta_abonos a " & _
                      "INNER JOIN abo_tipos_de_pagos t USING(abo_id) " & _
                      "WHERE " & _
                      "/*(SELECT COUNT(ctd_id) " & _
                            "FROM cta_abono_documentos d " & _
                            "JOIN ven_doc_venta v ON d.id=v.id " & _
                            "WHERE d.abo_id=a.abo_id AND condicionpago='CONTADO' ) =0 " & _
                      "AND */ abo_cli_pro='CLI' AND rut_emp='" & SP_Rut_Activo & "' AND t.mpa_id IN (8,9,10) " & FiltroFecha & " " & _
                      "UNION " & _
                      "SELECT " & IG_IdCtaClientes & ",'CLIENTES',0,SUM(pad_valor) " & _
                      "FROM cta_abonos a " & _
                      "INNER JOIN abo_tipos_de_pagos t USING(abo_id) " & _
                      "WHERE /* (SELECT COUNT(ctd_id) " & _
                            "FROM cta_abono_documentos d " & _
                            "JOIN ven_doc_venta v ON d.id=v.id " & _
                            "WHERE d.abo_id=a.abo_id AND condicionpago='CONTADO' ) =0 " & _
                      "AND */ abo_cli_pro='CLI' AND rut_emp='" & SP_Rut_Activo & "' AND t.mpa_id IN (8,9,10) " & FiltroFecha
                    
        
            
            
            
            
            
            
            Case 17 'PAGO A PROVEEDORES CON TARJETA CREDITO/DEBITO
                        
            

                Sql = "SELECT " & IG_IdCtaProveedores & ",'PROVEEDORES',SUM(pad_valor),0 " & _
                      "FROM cta_abonos a " & _
                      "INNER JOIN abo_tipos_de_pagos t USING(abo_id) " & _
                      "WHERE (SELECT COUNT(ctd_id) " & _
                            "FROM cta_abono_documentos d " & _
                            "JOIN com_doc_compra v ON d.id=v.id " & _
                            "WHERE d.abo_id=a.abo_id AND pago='CONTADO' ) =0 " & _
                      "AND abo_cli_pro='PRO' AND rut_emp='" & SP_Rut_Activo & "' AND t.mpa_id IN (8,9) " & FiltroFecha & " " & _
                        "UNION " & _
                        "SELECT " & IG_Id_CtaCuentasPorPagar & ",'CUENTAS POR PAGAR',0,SUM(pad_valor) " & _
                      "FROM cta_abonos a " & _
                      "INNER JOIN abo_tipos_de_pagos t USING(abo_id) " & _
                      "WHERE " & _
                      "(SELECT COUNT(ctd_id) " & _
                            "FROM cta_abono_documentos d " & _
                            "JOIN com_doc_compra v ON d.id=v.id " & _
                            "WHERE d.abo_id=a.abo_id AND pago='CONTADO' ) =0 " & _
                      "AND abo_cli_pro='PRO' AND rut_emp='" & SP_Rut_Activo & "' AND t.mpa_id IN (8,9) " & FiltroFecha

                       
            Case 18 'Asiento por boletas de honorarios
                Sql = "SELECT h.pla_id,pla_nombre,SUM(deh_debe) debe,SUM(deh_haber) haber " & _
                        "FROM com_doc_compra c " & _
                        "JOIN sis_documentos d USING(doc_id) " & _
                        "JOIN com_detalle_honorarios h ON c.id=h.com_id " & _
                        "JOIN con_plan_de_cuentas p ON h.pla_id=p.pla_id " & _
                        "WHERE doc_honorarios='SI' AND doc_documento='COMPRA' " & _
                        FiltroFecha & " " & _
                        "GROUP BY h.pla_id " & _
                        "ORDER BY SUM(deh_debe) DESC"
                
            
            
            
            
            
    End Select
    Consulta Rs_A, Sql
    Set AsientoContable = Rs_A



End Function
Public Sub TotalAsiento(LV As ListView)
    Dim Ip_C As Integer
    With LV
        .ListItems.Add , , ""
        Ip_C = .ListItems.Count
        .ListItems(Ip_C).SubItems(1) = "TOTALES"
        .ListItems(Ip_C).SubItems(2) = NumFormat(TotalizaColumna(LV, "debe"))
        .ListItems(Ip_C).SubItems(3) = NumFormat(TotalizaColumna(LV, "haber"))
        For i = 1 To 3
            LV.ListItems(Ip_C).ListSubItems(i).Bold = True
        Next
    End With
End Sub
Public Function ConsultaCentralizado(IdAsiento As String, Mes As Integer, Ano As Integer) As Boolean
    Dim Rp_Cen As Recordset
    Sql = "SELECT ctl_id " & _
            "FROM con_centralizaciones " & _
            "WHERE asi_id IN(" & IdAsiento & ") AND ctl_mes=" & Mes & _
            " AND ctl_ano=" & Ano & " AND rut_emp='" & SP_Rut_Activo & "'"
    Consulta Rp_Cen, Sql
    If Rp_Cen.RecordCount > 0 Then
        ConsultaCentralizado = True
    Else
        ConsultaCentralizado = False
    End If
End Function


