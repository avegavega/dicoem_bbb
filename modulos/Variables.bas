Attribute VB_Name = "Variables"
Public IS_IntentosConexion As Integer
Public DM_Comision_Boton_Especial As Boolean
Public paso As Variant
Public LogUsuario As String
Public LogSueID As Integer
Public LogPass As String
Public LogPerfil As Integer
Public LogIdUsuario As Integer
Public RSUsuarios As Recordset
Public PosicionCombo As Integer
Public RutEmpresa As String
Public RsEmpresa As String
Public DireccionEmpresa As String
Public GiroEmpresa As String
Public AccionProducto As Integer
Public AccionProveedor As Integer
Public AccionCliente As Integer
Public Filtro As String
Public DesdeCompra As Boolean
Public Respuesta As Variant
Public RegSeleccionado As Boolean
Public rutProveedor As Boolean
Public RutBuscado As String
Public RutVerdadero As String
Public NuevoRut As String
Public ClrCfoco As String
Public ClrSfoco As String
Public ClrDesha As String
Public ClienteEncontrado As Boolean
Public RealizaPago As Boolean
Public VDdoc As String
Public Sql As String
Public sql2 As String
Public Sp_Servidor As String
Public BG_FalloConexion As Boolean

Public ConexionEspecial As String

Public UserDb As String
Public PwdDb As String
Public UsuarioGerencia As Boolean

Public QueImpresora As Integer
Public ImpresoraFacturas As String
Public ImpresoraBoletas As String
Public ImpresoraInformes As String
Public Connection_String As String
Public cn As ADODB.Connection 'variable para la base de datos y el record
Public RutAnalizar As String
Public ClienteOproveedor As String
Public TipoDocumentoVenta As String
Public SaldarInmediato As Boolean
Public ElNumeroDoc As Double
Public CargaEnseguida As Boolean
Public TextoDelCredito As String
Public ElDetalleCheque As String
Public VentaEspecial As Boolean
Public Archivo As String
'Recordset
Public RsTmp As Recordset
Public RsTmp2 As Recordset
Public RsCom As Recordset
Public RsTmp3 As Recordset
Public objExcel As Excel.Application
'boton espcial
Public EsCosto As String
Public EsDescp As String
Public EsPrecio As String
Public EsCodigo As String
Public EsComision As String
Public Graba As Boolean

Public IP_familia As Integer
Public IP_Doc_ID As Integer
Public lp_Recargo As Double
Public SG_codigo As String
Public SG_codigo2 As String
Public SG_codigo3 As String
Public G_Filtro_Fecha As String
Public DG_IVA As Double
Public IG_documentoIvaRetenido As Integer ' id del documento que lleva iva retenido
Public DG_ID_Unico As Double ' id unico multiples tabblas
Public IG_id_OT As Integer ' id del documento OT
Public IG_id_Empresa As Integer ' Id de la empresa activa
Public IG_id_Sucursal_Empresa As Integer
Public IG_id_Bodega_Ventas As Integer


Public IG_id_Nota_Credito As Integer ' Id del documento Nota de Credito
Public IG_id_Nota_Debito As Integer ' Id del documento Nota de debito
Public IG_id_Nota_Credito_Electronica As Integer ' Id del documento Nota de Credito
Public IG_id_Nota_Debito_Electronica As Integer ' Id del documento Nota de debito
Public IG_id_Factura_Manual As Integer ' Id de la factura
Public IG_id_Boleta_Manual As Integer ' Id de boleta
Public IG_id_Nota_Credito_Clientes As Integer ' Nota de Credito a clientes
Public IG_id_Nota_Debito_Clientes As Integer ' Nota de DEBITO a clientes
Public IG_id_GuiaProveedor As Integer ' Id Unico de Guias Proveedores
Public IG_id_GuiaClientes As String ' Id Unico de Guias Clientes
Public l_Pcompra As Double ' precio compra kardex - compras
Public IG_id_OrdenCompra As Long 'id de orden de compra
Public IG_Id_DocFondosRendir As Integer
Public IG_Id_CuentaCaja As Integer
Public IG_Id_CuentaCreditoFiscal As Integer
Public IG_Id_CuentaDebitoFiscal As Integer
Public IG_Id_CuentaIvaRetenido As Integer
Public IG_id_CorrecionPerdida As Long
Public IG_id_CorrecionGanancia As Long
Public IG_Id_Depreciaciones As Long
Public IG_Id_CtaDocumentosCartera As Long
Public IG_Id_CtaCuentasPorCobrar As Long
Public IG_Id_CtaCuentasPorPagar As Long
Public IG_Id_CuentaDepAcumulada As Long
Public IG_Id_CuentaAnticipoProveedores As Long
Public bm_NoTimer As Boolean

Public IG_Ano_Contable As Integer
Public IG_Mes_Contable As Integer

Public Sp_extra As String
Public IP_IDMenu As Integer
Public LP_FactorValorFuturo As Long
Public SP_Rut_Activo As String
Public SP_Empresa_Activa As String
Public SP_Control_Inventario As String
Public SP_Nombre_Equipo As String
Public SP_Cuentas_Reservadas As String
Public SP_Facturas_De_Ventas As String
Public SG_Funcion_Equipo As String
Public IG_Version As Integer
Public Sm_ImpresoraSeleccionada As String
Public Sp_RutaDescarga As String
Public IG_IdCtaProveedores As Integer
Public IG_IdCtaClientes As Integer
Public IG_IdCtaFondo As Long
Public IG_idCuentaActivoFijoNuevos As Long
Public IG_IdCuentaHonorariosPagar As Long
Public IG_IdRetencionImpHonorarios As Long
Public IG_IdUtilidadVtaActivoInmovilizado As Long
Public IG_IdPerdidaVtaActivoInmovilizado As Long


Public SG_Presentacion_Factura As String
Public SG_Codigos_Alfanumericos As String * 2
Public SG_Activos_Inmovilizados As String
Public SG_Modulo_Caja As String * 2
Public LG_id_Caja As Long
Public SG_Escritorio_Remoto As String * 2
Public SG_Sistema_MultiEmpresa As String * 2
Public SG_Es_la_Flor As String * 2

Public SG_BaseDato As String
Public Ip_CantLineas_Factura As Integer
Public SG_Url_Factura_Electronica As String
Public SG_Oficina_Contable As String

'16 Enero 2015
Public SG_Equipo_Solo_Nota_de_Venta As String * 2
'6 Feb 2015
Public IG_Puerto_Impresora_Fiscal As Integer
Public SG_ImpresoraFiscalBixolon As String * 2
'25 Abril 2015
Public SG_Usuario_Ve_Pcosto As String
'16 Mayo 2015
Public SG_Usuario_Indivual_Empresa As String * 2
'15 Agosto 2015
'Esta se usara en el POS, cuando pasa a Caja, si pago correctametne, devulve SI, entonces graba la venta.
Public SG_Pago_Correcto As String * 2
'23 Agosto 2015
Public SG_Marca_Impresora_Fiscal As String
'28 Agosto 2015
Public Lp_Nro_Boleta_Obtenida_IF As Long

'5 sep 2015
Public SG_Busqueda_codigo_Interno As String * 2

'16 sep 2015
Public IG_Posicion_Inicial_Cheques As String
Public SG_Ver_Boton_Subir_Libros As String * 2 'para todos debiera ser si, excepto para redmar


'10-10-15
Public SG_UtilizaImpresoraTermicaDTE As String * 2



'17-10-15
Public LG_Monto_Pagado_BF As Long

'21-10-15 ' Numero de forma de pago, 0=efectivo, 1=cheque, depende la empresa _
esto es aplicable a las impresoras fiscales samsung
Public IG_NFpago_IF As Long


'19-11-15 (molinos visualizar pdf)
Public SG_LlevaIVAAnticipado As String * 2

'01-04-2016
Public BG_ModoProductivoDTE As Boolean



'7-4-2016 'Archivo pdf generado, obetener el nombre con rut, para poder impirmirlo
Public SG_NombrePDF As String

'04-10-2016 'empresa por sistema vende o no vende sin stock
Public SG_VenderSinStock As String * 2

Public SG_CondicionaVentaSinStock As String * 2

Public SG_Sucursal_Activa As String

Public SG_Formulario_Ficha_Producto  As String ' = RsTmp!par_valor 'nombre fomulario ficha de producto
Public SG_PriorizaCodigoInterno As String


' Declaraci�n de la Funci�n Api SendMessage, para odenar el listview asc o desc
Public Declare Function SendMessage Lib "user32" Alias "SendMessageA" ( _
    ByVal hWnd As Long, _
    ByVal wMsg As Long, _
    ByVal wParam As Long, _
    lParam As Any) As Long
  
Public Const WM_SETREDRAW As Long = &HB& 'necesesario para odenar el listview asc o desc
Public Declare Function GetSystemMenu Lib "user32" (ByVal hWnd As Long, ByVal bRevert As Long) As Long
Public Declare Function GetMenuItemCount Lib "user32" (ByVal hMenu As Long) As Long

Public Declare Function RemoveMenu Lib "user32" (ByVal hMenu As Long, _
       ByVal nPosition As Long, _
       ByVal wFlags As Long) As Long '--end block--'
Public Declare Function AppendMenu Lib "user32" Alias "AppendMenuA" (ByVal hMenu As Long, ByVal wFlags As Long, ByVal wIDNewItem As Long, ByVal lpNewItem As Any) As Long
    Const MF_STRING = &H0&
    Public Const MF_BYPOSITION = &H400
Public Const MF_REMOVE = &H1000
Public Declare Function DrawMenuBar Lib "user32" (ByVal hWnd As Long) As Long


' Declara las rutinas API necesarias:
Declare Function FindWindow Lib "user32" Alias _
"FindWindowA" (ByVal lpClassName As String, _
               ByVal lpWindowName As Long) As Long



Public Declare Function CreateFile Lib "kernel32" Alias "CreateFileA" (ByVal lpFileName As String, ByVal dwDesiredAccess As Long, ByVal dwShareMode As Long, lpSecurityAttributes As SECURITY_ATTRIBUTES, ByVal dwCreationDisposition As Long, ByVal dwFlagsAndAttributes As Long, ByVal hTemplateFile As Long) As Long
Public Declare Function CloseHandle Lib "kernel32" (ByVal hObject As Long) As Long

Public Type SECURITY_ATTRIBUTES
    nLength As Long
    lpSecurityDescriptor As Long
    bInheritHandle As Long
End Type
Public Im_Nv As Long



Function EsLetra(Tecla As Integer) As Boolean
    Select Case Tecla
        Case 8, 13, 32, 65 To 90, 97 To 122, 209, 241
            EsLetra = True
        Case Else
            EsLetra = False
    End Select
End Function

Function EsNumero(Tecla As Integer) As Boolean
    Select Case Tecla
        Case 8, 13, 48 To 57
            EsNumero = True
        Case Else
            EsNumero = False
    End Select
End Function

Public Sub PressEnter(Tecla As Integer)
    If Tecla = 13 Then
        SendKeys "{TAB}"
    End If
End Sub

Sub Main()
    'If Crear_DsN("Citroen", "190.121.87.42", "db_citroen", "MySQL ODBC " & "5.1" & " Driver", "citroen") Then
    '    Mdrv = "MySQL ODBC " & mODBC & " Driver"
    'Else
    '    MsgBox "No se pudo instalar el DNS, verificar que esta instalado el MyOdbc " & mODBC, vbCritical
    '    End
    'End If
'
    'Modo productivo por defecto en verdadero
    BG_ModoProductivoDTE = True
  '  SG_Sucursal_Activa = "TEMUCO"
    Sp_Archivo = Dir(App.Path & "\cnx\suc.ini")
    If Sp_Archivo <> "" Then
        Sp_Archivo = App.Path & "\cnx\suc.ini"
        X = FreeFile
        On Error Resume Next
        
        Open Sp_Archivo For Input As X
           Line Input #X, SG_Sucursal_Activa
        Close X
    End If
    
    ConexionBD
   
    
    On Error GoTo Fallo
    Consulta RsTmp, "SELECT par_id,par_valor FROM tabla_parametros WHERE par_activo='SI'"
    'Consulta RsTmp, "SELECT par_id,par_valor FROM tabla_parametros WHERE par_activo='SI'"
    If RsTmp.RecordCount > 0 Then
        RsTmp.MoveFirst
        Do While Not RsTmp.EOF
            If RsTmp!par_id = 1 Then DG_IVA = RsTmp!par_valor
            If RsTmp!par_id = 2 Then IG_documentoIvaRetenido = RsTmp!par_valor
            If RsTmp!par_id = 3 Then IG_id_OT = RsTmp!par_valor
            If RsTmp!par_id = 4 Then IG_id_Nota_Credito = RsTmp!par_valor
            If RsTmp!par_id = 5 Then IG_id_Nota_Debito = RsTmp!par_valor
            If RsTmp!par_id = 6 Then IG_id_Nota_Credito_Electronica = RsTmp!par_valor
            If RsTmp!par_id = 7 Then IG_id_Nota_Debito_Electronica = RsTmp!par_valor
            If RsTmp!par_id = 8 Then IG_id_Factura_Manual = RsTmp!par_valor
            If RsTmp!par_id = 9 Then IG_id_Boleta_Manual = RsTmp!par_valor
            If RsTmp!par_id = 10 Then IG_id_Nota_Credito_Clientes = RsTmp!par_valor
            If RsTmp!par_id = 11 Then IG_id_GuiaProveedor = RsTmp!par_valor
            If RsTmp!par_id = 12 Then IG_id_GuiaClientes = RsTmp!par_valor
            If RsTmp!par_id = 13 Then IG_Ano_Contable = RsTmp!par_valor
            If RsTmp!par_id = 14 Then IG_Mes_Contable = RsTmp!par_valor
            If RsTmp!par_id = 15 Then IG_id_Nota_Debito_Clientes = RsTmp!par_valor
            If RsTmp!par_id = 16 Then LP_FactorValorFuturo = RsTmp!par_valor
            If RsTmp!par_id = 17 Then SP_Rut_Activo = RsTmp!par_valor
            If RsTmp!par_id = 18 Then SP_Empresa_Activa = RsTmp!par_valor
            If RsTmp!par_id = 19 Then SP_Control_Inventario = RsTmp!par_valor
            If RsTmp!par_id = 20 Then SP_Cuentas_Reservadas = RsTmp!par_valor
            If RsTmp!par_id = 21 Then SP_Facturas_De_Ventas = RsTmp!par_valor
            If RsTmp!par_id = 22 Then IG_id_OrdenCompra = RsTmp!par_valor
            If RsTmp!par_id = 100 Then IG_Version = RsTmp!par_valor
            If RsTmp!par_id = 300 Then Sp_RutaDescarga = RsTmp!par_valor
            If RsTmp!par_id = 400 Then IG_IdCtaProveedores = RsTmp!par_valor 'ID Cta Proveedores
            If RsTmp!par_id = 401 Then IG_IdCtaClientes = RsTmp!par_valor 'ID Cta CLIENTES
            If RsTmp!par_id = 402 Then IG_IdCtaFondo = RsTmp!par_valor 'ID Cta FONDO POR RENDIR
            If RsTmp!par_id = 403 Then IG_Id_CuentaCaja = RsTmp!par_valor 'ID Cta CAJA
            
            If RsTmp!par_id = 404 Then IG_Id_CuentaCreditoFiscal = RsTmp!par_valor 'ID Cta CREDITO FISCAL
            If RsTmp!par_id = 405 Then IG_Id_CuentaDebitoFiscal = RsTmp!par_valor 'ID Cta DEBITO FISCAL
            If RsTmp!par_id = 406 Then IG_Id_CuentaIvaRetenido = RsTmp!par_valor 'ID Cta IVA RETENIDO
  
            If RsTmp!par_id = 407 Then IG_id_CorrecionPerdida = RsTmp!par_valor 'correccion monetaria perdida
            If RsTmp!par_id = 408 Then IG_id_CorrecionGanancia = RsTmp!par_valor 'correccion monetaria ganancia
            If RsTmp!par_id = 409 Then IG_Id_Depreciaciones = RsTmp!par_valor 'id depreciaciones
            If RsTmp!par_id = 410 Then IG_idCuentaActivoFijoNuevos = RsTmp!par_valor                'id IG_idCuentaActivoFijo nuevos
            If RsTmp!par_id = 411 Then IG_Id_CuentaDepAcumulada = RsTmp!par_valor                'id dep acumulada
            If RsTmp!par_id = 412 Then IG_Id_CuentaAnticipoProveedores = RsTmp!par_valor                'id ANTICIPO PROVEEDORES
            
            If RsTmp!par_id = 500 Then SG_Codigos_Alfanumericos = RsTmp!par_valor 'ESTO ES SI EL CODIGO DE PRODUCTOS SERA ALFANUMERICO
            If RsTmp!par_id = 600 Then IG_Id_DocFondosRendir = RsTmp!par_valor 'ESTE ES EL DOCUMENTO DE FONDOS POR RENDIR
            If RsTmp!par_id = 700 Then SG_Escritorio_Remoto = RsTmp!par_valor                'Trabajamos con escritorio remoto
            If RsTmp!par_id = 800 Then SG_Sistema_MultiEmpresa = RsTmp!par_valor
            If RsTmp!par_id = 900 Then SG_Es_la_Flor = RsTmp!par_valor
            If RsTmp!par_id = 1100 Then Ip_CantLineas_Factura = RsTmp!par_valor ' CANTIDAD DE LINEAS POR FACTURA
            If RsTmp!par_id = 1200 Then IG_Id_CtaDocumentosCartera = RsTmp!par_valor                 ' id cta documentos en cartera
            If RsTmp!par_id = 1300 Then IG_Id_CtaCuentasPorCobrar = RsTmp!par_valor                 ' id CUENTAS POR COBRAR
            If RsTmp!par_id = 1400 Then IG_Id_CtaCuentasPorPagar = RsTmp!par_valor                 ' id CUENTAS POR PAGAR
            If RsTmp!par_id = 1500 Then IG_IdCuentaHonorariosPagar = RsTmp!par_valor
            If RsTmp!par_id = 1600 Then IG_IdRetencionImpHonorarios = RsTmp!par_valor
            If RsTmp!par_id = 1700 Then IG_IdUtilidadVtaActivoInmovilizado = RsTmp!par_valor
            If RsTmp!par_id = 1800 Then IG_IdPerdidaVtaActivoInmovilizado = RsTmp!par_valor
            If RsTmp!par_id = 1900 Then SG_Url_Factura_Electronica = RsTmp!par_valor
            If RsTmp!par_id = 2000 Then IG_Puerto_Impresora_Fiscal = RsTmp!par_valor
            If RsTmp!par_id = 2100 Then SG_ImpresoraFiscalBixolon = RsTmp!par_valor
            If RsTmp!par_id = 2200 Then SG_Usuario_Indivual_Empresa = RsTmp!par_valor
            If RsTmp!par_id = 2300 Then SG_Marca_Impresora_Fiscal = RsTmp!par_valor
            If RsTmp!par_id = 2400 Then IG_Posicion_Inicial_Cheques = RsTmp!par_valor
            If RsTmp!par_id = 2500 Then SG_Ver_Boton_Subir_Libros = RsTmp!par_valor
            If RsTmp!par_id = 3000 Then
                If RsTmp!par_valor = "NO" Then
                    BG_ModoProductivoDTE = False
                End If
            End If
            If RsTmp!par_id = 3100 Then SG_VenderSinStock = RsTmp!par_valor
            If RsTmp!par_id = 3200 Then SG_CondicionaVentaSinStock = RsTmp!par_valor
            
            If RsTmp!par_id = 27000 Then SG_Formulario_Ficha_Producto = RsTmp!par_valor 'nombre fomulario ficha de producto
            If RsTmp!par_id = 50000 Then SG_PriorizaCodigoInterno = RsTmp!par_valor
            
            
            RsTmp.MoveNext
        Loop
    End If
    
    Exit Sub
Fallo:
BG_FalloConexion = True
MsgBox "Ocurrio un problema al intentar conectar con la Base de Datos..." & vbNewLine & Err.Description & " Nro:" & Err.Number, vbInformation

End Sub
Public Sub LLenarCombo(Cbo As ComboBox, Campo As String, Indice As String, Tabla As String, Optional ByVal Condicion As String, Optional ByVal Ordena As String)
    Dim RsCombo As Recordset, s_Sql As String
    If Condicion = Empty Then
        s_Sql = "SELECT " & Campo & "," & Indice & "  FROM " & Tabla
    Else
        s_Sql = "SELECT " & Campo & "," & Indice & "  FROM " & Tabla & " WHERE " & Condicion
    End If
    If Ordena <> Empty Then s_Sql = s_Sql & " ORDER BY " & Ordena
    Consulta RsCombo, s_Sql
    If RsCombo.RecordCount > 0 Then
        RsCombo.MoveFirst
        Cbo.Clear
        Do While Not RsCombo.EOF
            Cbo.AddItem RsCombo.Fields(0)
            Cbo.ItemData(Cbo.ListCount - 1) = RsCombo.Fields(1)
            RsCombo.MoveNext
        Loop
    End If

End Sub
Public Sub Aplicar_skin(ByVal Formulario As Form, Optional ByVal RutaSkin As String)
  '  If RutaSkin = Empty Then RutaSkin =
  '  Cambiar Skin
    Acceso.Skin1.LoadSkin App.Path & "\skins\Stxx.skn"
    Acceso.Skin1.ApplySkin Formulario.hWnd
End Sub
Public Function SinDesborde(ElValor As Variant) As Boolean
    If Abs(Val(ElValor)) > 2147483647 Then
        SinDesborde = False
        MsgBox "Valor Ingresado fuera de limite...", vbInformation
    Else
        SinDesborde = True
    End If
End Function

Public Sub Skin2(ByVal Formulario As Form, Optional ByVal RutaSkin As String, Optional ByVal MSkin As Integer)
  '  If RutaSkin = Empty Then RutaSkin =
  '  Cambiar Skin
    Select Case MSkin
        Case 1
            Formulario.Skin1.LoadSkin App.Path & "\skins\media.skn"
        Case 2
            Formulario.Skin1.LoadSkin App.Path & "\skins\stmac.skn"
        Case 3
            Formulario.Skin1.LoadSkin App.Path & "\skins\sst.skn"
        Case 4
            Formulario.Skin1.LoadSkin App.Path & "\skins\stx.skn"
        Case 5
            Formulario.Skin1.LoadSkin App.Path & "\skins\VistaBlueNew.skn"
        Case 6
            Formulario.Skin1.LoadSkin App.Path & "\skins\MagnificBlue.skn"
        Case 7
            Formulario.Skin1.LoadSkin App.Path & "\skins\Winter.skn"
    End Select
    Formulario.Skin1.ApplySkin Formulario.hWnd
End Sub
Public Function AceptaSoloNumeros(CodAscii As Integer)
    If IsNumeric(Chr(CodAscii)) Or CodAscii = 8 Or CodAscii = 44 Then  'pas
    Else
        If CodAscii = 13 Then
            On Error Resume Next
            SendKeys "{TAB}" 'envia un tab
        ElseIf CodAscii = 46 Then 'pase
        Else
            CodAscii = 0 'No acepta el caracter ingresado
        End If
    End If
    AceptaSoloNumeros = CodAscii
End Function
Public Function SoloNumeros(CodAscii As Integer)
    If IsNumeric(Chr(CodAscii)) Or CodAscii = 8 Then 'pas
    Else
        If CodAscii = 13 Then
            On Error Resume Next
            SendKeys "{TAB}" 'envia un tab
        ElseIf CodAscii = 46 Or CodAscii = 44 Then
            CodAscii = 0 'no acepta punto 'ni coma
        Else
            CodAscii = 0 'No acepta el caracter ingresado
        End If
    End If
    SoloNumeros = CodAscii
End Function
Public Function VerificaRut_V2(TextoRut As String, DevolverRut As String) As Boolean
    Dim LargoRut As Integer
    Dim SumaDelRut As Double
    Dim DigitoVerificador As String * 1
    Dim DigitoReal As String
    Dim DigitoResultado As Integer
    TextoRut = Replace(Trim(TextoRut), ".", "")
    TextoRut = Replace(TextoRut, "-", "")
    LargoRut = Len(TextoRut)
    If LargoRut < 6 Or Val(TextoRut) < 100000 Or LargoRut > 9 Then
        MsgBox "RUT no v�lido ... ", vbOKOnly + vbExclamation
        VerificaRut_V2 = False
    Else
        DigitoVerificador = Right(TextoRut, 1)
        SumaDelRut = SumaDelRut + (Val(Mid(TextoRut, LargoRut, 1)) * 9)       '9
        SumaDelRut = SumaDelRut + (Val(Mid(TextoRut, LargoRut - 1, 1)) * 8) '8
        SumaDelRut = SumaDelRut + (Val(Mid(TextoRut, LargoRut - 2, 1)) * 7) '7
        SumaDelRut = SumaDelRut + (Val(Mid(TextoRut, LargoRut - 3, 1)) * 6) '6
        SumaDelRut = SumaDelRut + (Val(Mid(TextoRut, LargoRut - 4, 1)) * 5) '5
        If LargoRut > 5 Then SumaDelRut = SumaDelRut + (Val(Mid(TextoRut, LargoRut - 5, 1)) * 4) '4
        If LargoRut > 6 Then SumaDelRut = SumaDelRut + (Val(Mid(TextoRut, LargoRut - 6, 1)) * 9)
        If LargoRut > 7 Then SumaDelRut = SumaDelRut + (Val(Mid(TextoRut, LargoRut - 7, 1)) * 8)
        If SumaDelRut Mod 11 = 10 Then
            DigitoReal = "K"
        Else
          DigitoResultado = SumaDelRut Mod 11
          DigitoReal = Trim(Str(DigitoResultado))
        End If
        VerificaRut_V2 = True        'rut correcto
        DevolverRut = Format(Mid(TextoRut, 1, LargoRut), "##,###") & "-" & DigitoReal

    End If
End Function
Public Function VerificaRut(TextoRut As String, DevolverRut As String) As Boolean
    Dim LargoRut As Integer
    Dim SumaDelRut As Double
    Dim DigitoVerificador As String * 1
    Dim DigitoReal As String
    Dim DigitoResultado As Integer
    TextoRut = Replace(Trim(TextoRut), ".", "")
    TextoRut = Replace(TextoRut, "-", "")
    LargoRut = Len(TextoRut)
    If LargoRut < 6 Or Val(TextoRut) < 100000 Or LargoRut > 9 Then
        MsgBox "RUT no v�lido ... ", vbOKOnly + vbExclamation
        VerificaRut = False
    Else
        DigitoVerificador = Right(TextoRut, 1)
        LargoRut = LargoRut - 1
        SumaDelRut = SumaDelRut + (Val(Mid(TextoRut, LargoRut, 1)) * 9)       '9
        SumaDelRut = SumaDelRut + (Val(Mid(TextoRut, LargoRut - 1, 1)) * 8) '8
        SumaDelRut = SumaDelRut + (Val(Mid(TextoRut, LargoRut - 2, 1)) * 7) '7
        SumaDelRut = SumaDelRut + (Val(Mid(TextoRut, LargoRut - 3, 1)) * 6) '6
        SumaDelRut = SumaDelRut + (Val(Mid(TextoRut, LargoRut - 4, 1)) * 5) '5
        If LargoRut > 5 Then SumaDelRut = SumaDelRut + (Val(Mid(TextoRut, LargoRut - 5, 1)) * 4) '4
        If LargoRut > 6 Then SumaDelRut = SumaDelRut + (Val(Mid(TextoRut, LargoRut - 6, 1)) * 9)
        If LargoRut > 7 Then SumaDelRut = SumaDelRut + (Val(Mid(TextoRut, LargoRut - 7, 1)) * 8)
        If SumaDelRut Mod 11 = 10 Then
            DigitoReal = "K"
        Else
          DigitoResultado = SumaDelRut Mod 11
          DigitoReal = Trim(Str(DigitoResultado))
        End If
        If DigitoReal = DigitoVerificador Then
            VerificaRut = True        'rut correcto
            DevolverRut = Format(Mid(TextoRut, 1, LargoRut), "##,###") & "-" & DigitoReal
            DevolverRut = Replace(DevolverRut, ",", ".")
        Else
            MsgBox "RUT no v�lido ... " & Chr(13) & _
                   " El digito verificador es " & DigitoReal, vbOKOnly + vbExclamation
                VerificaRut = False
        End If
    End If
End Function


Function BrutoAletras(ByVal Number As Double, ByVal masculino As Boolean) As String
' By Francisco Castillo - september 2000.
' Devuelve la representaci�n textual del valor del n�-
' mero ENTERO que se pasa como argumento, (100 =
' "Cien"). El par�metro "masculino" ser� True cuando deban
' ponerse terminaciones masculinas,(210 = doscientOs
' diez), y False en caso contrario, (320 = trescientAs
' veinte). El valor m�ximo del n�mero es de
'                      999.999.999


    If Number = 0 Then
        NumberToString = "Cero"
        Exit Function
    End If
    If Number < 0 Then             ' Hacerlo positivo,
        Number = Number * -1
    End If
    X = CStr(Fix(Number))          ' ...entero,
    Do While Len(X) < 9         ' ...y de 9 cifras.
        X = "0" & X
    Loop
    a = ""
' Grupos de 3 cifras, de atr�s hacia adelante:
    For n = 7 To 1 Step -3
' �El grupo actual es cero?:
        If CInt(Mid(X, n, 3)) <> 0 Then
' No.
' Tratar casos especiales decena:
            Select Case CInt(Mid(X, n + 1, 2))
                Case 10
                    a = "diez " & a
                Case 11
                    a = "once " & a
                Case 11
                    a = "once " & a
                Case 12
                    a = "doce " & a
                Case 13
                    a = "trece " & a
                Case 14
                    a = "catorce " & a
                Case 15
                    a = "quince " & a
                Case 16
                    a = "dieciseis " & a
                Case 17
                    a = "diecisiete " & a
                Case 18
                    a = "dieciocho " & a
                Case 19
                    a = "diecinueve " & a
                Case 20
                    a = "veinte " & a
                Case 21
                    If n > 1 Then
                        a = "veintiun$ " & a
                    Else
                        a = "veintiun " & a
                    End If
                Case 22
                    a = "veintidos " & a
                Case 23
                    a = "veintitr�s " & a
                Case 24
                    a = "veinticuatro " & a
                Case 25
                    a = "veinticinco " & a
                Case 26
                    a = "veintiseis " & a
                Case 27
                    a = "veintisiete " & a
                Case 28
                    a = "veintiocho " & a
                Case 29
                    a = "veintinueve " & a
                Case Else
' Restantes casos; traducir unidad:
                    Select Case CInt(Mid(X, n + 2, 1))
                        Case 0
                        Case 1
                            Select Case n
                                Case 7
                                    a = "y un$ " & a
                                Case 4
                                    If masculino Then
                                        a = "y un " & a
                                    Else
                                        a = "y una " & a
                                    End If
                                Case 1
                                    a = "y un " & a
                            End Select
                        Case 2
                            a = "y dos " & a
                        Case 3
                            a = "y tres " & a
                        Case 4
                            a = "y cuatro " & a
                        Case 5
                            a = "y cinco " & a
                        Case 6
                            a = "y seis " & a
                        Case 7
                            a = "y siete " & a
                        Case 8
                            a = "y ocho " & a
                        Case 9
                            a = "y nueve " & a
                    End Select
' Traducir decena:
                    Select Case CInt(Mid(X, n + 1, 1))
                        Case 0
                        Case 3
                            a = "treinta " & a
                        Case 4
                            a = "cuarenta " & a
                        Case 5
                            a = "cincuenta " & a
                        Case 6
                            a = "sesenta " & a
                        Case 7
                            a = "setenta " & a
                        Case 8
                            a = "ochenta " & a
                        Case 9
                            a = "noventa " & a
                    End Select
            End Select
' Prever caso "ciento y tres":
            If Left(a, 1) = "y" Then
                a = Right(a, Len(a) - 2)
            End If
' Traducir centena:
            Select Case CInt(Mid(X, n, 1))
                Case 0
                Case 1
                    If CInt(Mid(X, n + 1, 2)) = 0 Then
                        a = "cien " & a
                    Else
                        a = "ciento " & a
                    End If
                Case 2
                    a = "doscient$s " & a
                Case 3
                    a = "trescient$s " & a
                Case 4
                    a = "cuatrocient$s " & a
                Case 5
                    a = "quinient$s " & a
                Case 6
                    a = "seiscient$s " & a
                Case 7
                    a = "setecient$s " & a
                Case 8
                    a = "ochocient$s " & a
                Case 9
                    a = "novecient$s " & a
                
            End Select
        End If
' Poner terminaci�n del grupo anterior:
' Puede haber quedado "y tres":
        If Left(a, 1) = "y" Then
            a = Right(a, Len(a) - 2)
        End If
' Millones:
        If n = 4 Then
            If CInt(Left(X, 3)) = 1 Then
                a = "mill�n " & a
            Else
                If CInt(Left(X, 3)) <> 0 Then
                    a = "millones " & a
                End If
            End If
        Else
            If n = 7 Then
' Miles:
                If CInt(Mid(X, 4, 3)) = 1 Then
                    a = "mil " & a
                Else
                    If CInt(Mid(X, 4, 3)) <> 0 Then
                        a = "mil " & a
                    End If
                End If
            End If
        End If
' Traducir g�nero, "$" en el texto. Para el grupo de los
' millones, se traduce siempre por masculino:
        If n = 1 Then
            masculino = True
        End If
        posic = 1
        Do While posic <> 0
            posic = InStr(a, "$")
            If posic <> 0 Then
                ante = Left(a, posic - 1)
                Post = Right(a, Len(a) - posic)
                If masculino Then
                    a = ante & "o" & Post
                Else
                    a = ante & "a" & Post
                End If
            End If
        Loop
    Next n
' Caso especial: puede haber quedado "unx mil "
    If Left(a, 7) = "un mil " Then
        a = Right(a, Len(a) - 3)
    Else
        If Left(a, 8) = "una mil " Then
            a = Right(a, Len(a) - 4)
        End If
    End If
' Inicial en may�sculas:
    If val_Dec = "" Then
        BrutoAletras = Trim(UCase(Left(a, 1)) & Right(a, Len(a) - 1))
    Else
        BrutoAletras = Trim(Left(a, 1) & Right(a, Len(a) - 1))
    End If
End Function


Function Establecer_Impresora(ByVal NamePrinter As String) As Boolean
On Error GoTo errSub
       
    'Variable de referencia
    Dim obj_Impresora As Object
       
    'Creamos la referencia
    Set obj_Impresora = CreateObject("WScript.Network")
        obj_Impresora.setdefaultprinter NamePrinter
       
    Set obj_Impresora = Nothing
           
        'La funci�n devuelve true y se cambi� con �xito
        Establecer_Impresora = True
       ' MsgBox "La impresora se cambi� correctamente", vbInformation
    Exit Function
       
       
'Error al cambiar la impresora
errSub:
If Err.Number = 0 Then Exit Function
   Establecer_Impresora = False
   MsgBox "error: " & Err.Number & Chr(13) & "Description: " & Err.Description
   On Error GoTo 0
End Function
Public Function AgregarIVA(VNeto As Double, Iva As Integer) As Double
    AgregarIVA = VNeto + Round(VNeto * Iva / 100, 1)
End Function
Public Function ExtraeNeto(Vbruto As Double) As Double
    ExtraeNeto = Round((Vbruto * 100 / 119), 0)
End Function
Function Consulta(RecSet As ADODB.Recordset, CSQL As String)
InicioConsulta:
            IS_IntentosConexion = IS_IntentosConexion + 1
             Set RecSet = New ADODB.Recordset '  'Nuevo recordset
            'Debug.Print CSQL
            On Error GoTo FalloConsulta
            
            RecSet.Open CSQL, cn, adOpenStatic, adLockOptimistic, adAsyncFetch  '  ' abre
            
            
            If SP_Rut_Activo = "76.005.337-6-X" Then
                X = FreeFile
                Open App.Path & "\sql.sql" For Append As #X
                    Print #X, " "
                    Print #X, Time & " " & Date
                    Print #X, CSQL
                    Print #X, " "
                Close #X
            End If
            
 IS_IntentosConexion = 0
            Exit Function
FalloConsulta:
    If IS_IntentosConexion < 2 Then
        ConexionBD
        GoTo InicioConsulta
    End If
        
    If EjecucionIDE Then
        MsgBox "Fallo la consulta a la base de datos " & vbNewLine & CSQL & vbNewLine & Err.Number & " - " & Err.Description
    Else
        MsgBox "Fallo la consulta a la base de datos " & vbNewLine & vbNewLine & Err.Number & " - " & Err.Description
    End If
    
    If Err.Number = 1146 Then
        MsgBox "tabla no exitse"
    End If
    
    If IS_IntentosConexion = 2 Then End
End Function

Function ConsultaNew(RecSet As ADODB.Recordset, CSQL As String)
InicioConsulta:
            IS_IntentosConexion = IS_IntentosConexion + 1
            On Error GoTo FalloConsulta
             Set RecSet = New ADODB.Recordset '  'Nuevo recordset
            'Debug.Print CSQL
           
            With RecSet
                   .CursorLocation = adUseClient
                   
                .Properties("Initial Fetch Size") = 0
                  
                ' Se ejecuta el evento FetchProgress cada ves que se traen dos registros
                .Properties("Background Fetch Size") = 2
                  
                'Abre el recordset y se disparan el evento FetchProgress
                .Open CSQL, cn, , , adAsyncFetch
            End With
           ' RecSet.Open CSQL, cn, adOpenStatic, adLockOptimistic, adAsyncFetch  '  ' abre
            IS_IntentosConexion = 0
            Exit Function
FalloConsulta:
    If IS_IntentosConexion < 2 Then
        ConexionBD
        GoTo InicioConsulta
    End If
        
    If EjecucionIDE Then
        MsgBox "Fallo la consulta a la base de datos " & vbNewLine & CSQL & vbNewLine & Err.Number & " - " & Err.Description
    Else
        MsgBox "Fallo la consulta a la base de datos " & vbNewLine & vbNewLine & Err.Number & " - " & Err.Description
    End If
    If IS_IntentosConexion = 2 Then End
End Function


Function ConsultaDO(RecSet As ADODB.Recordset, CSQL As String)
             Set RecSet = New ADODB.Recordset '  'Nuevo recordset
            'Debug.Print CSQL
            On Error GoTo FalloConsulta
            DoEvents
            RecSet.Open CSQL, cn, adOpenStatic, adLockOptimistic '  ' abre
            
            Exit Function
FalloConsulta:
    If EjecucionIDE Then
        MsgBox "Fallo la consulta a la base de datos " & vbNewLine & CSQL & vbNewLine & Err.Number & " - " & Err.Description
    Else
        MsgBox "Fallo la consulta a la base de datos " & vbNewLine & vbNewLine & Err.Number & " - " & Err.Description
    End If
End Function

Function ConexionBD()
            On Error GoTo FalloConexion
                'Connection_String = "PROVIDER=MSDASQL;dsn=citroen;uid=;pwd=;" '   ' le agrega el path de la base de datos al Connectionstring
                
                If SG_Sucursal_Activa = "TEMUCO" Then
                    SG_BaseDato = "alvaro_dicoem"
                    Connection_String = "PROVIDER=MSDASQL;dsn=db_redmar;uid=" & UserDb & ";pwd=" & PwdDb & ";server=" & Sp_Servidor & ";database=" & "alvaro_dicoem" & ";"
                ElseIf SG_Sucursal_Activa = "TALCA" Then
                    SG_BaseDato = "alvaro_redic"
                    Connection_String = "PROVIDER=MSDASQL;dsn=db_redmar;uid=" & UserDb & ";pwd=" & PwdDb & ";server=" & Sp_Servidor & ";database=" & "alvaro_redic" & ";"
                End If
                
                '" '   ' le agrega el path de la base de datos al Connectionstring
                
'                Connection_String = "Driver={MySQL ODBC 5.2 Driver Unicode Driver};Server=" & Sp_Servidor & " ;Port=3306;" & _
                                    "Database=" & SG_BaseDato & ";User=root;Password=eunice;Option=3;"
                
                Set cn = New ADODB.Connection '  ' Nuevo objeto Connection
                cn.CursorLocation = adUseClient
                'cn.CommandTimeout = 30
                cn.ConnectionTimeout = 30
                cn.Open Connection_String '  'Abre la conexi�n
            Exit Function
FalloConexion:
    MsgBox "No fue posible establecer la conecci�n con el Servidor..." & vbNewLine & "Verifique que la Ip ingresada sea la correcta", vbInformation
    Exit Function
End Function





Function ConexionBD_DellMaggio()
            On Error GoTo FalloConexion
                Connection_String = "PROVIDER=MSDASQL;dsn=db_redmar;uid=helpcom_db;pwd=helpcom211;server=" & Sp_Servidor & "; "
                Set cn = New ADODB.Connection '  ' Nuevo objeto Connection
                cn.CursorLocation = adUseClient
                cn.Open Connection_String '  'Abre la conexi�n
            Exit Function
FalloConexion:
    MsgBox "No fue posible establecer la conecci�n con el Servidor..." & vbNewLine & "Verifique que la Ip ingresada sea la correcta", vbInformation
    Exit Function
End Function
Function ConexionBdShape(RecSetSh As ADODB.Recordset, CSQL As String)
    Dim Sp_CadenaConexion As String
    On Error GoTo FalloConexion
    Sp_CadenaConexion = "PROVIDER=MSDataShape;Data PROVIDER=MSDASQL;dsn=db_redmar;uid=root;pwd=eunice;server=" & Sp_Servidor & "; "
    Set CnShape = New ADODB.Connection
    CnShape.CursorLocation = adUseClient
    CnShape.Open Sp_CadenaConexion
    Set RecSetSh = New ADODB.Recordset
    RecSetSh.Open CSQL, CnShape, adOpenStatic, adLockOptimistic
    Exit Function
FalloConexion:
    MsgBox "No fue posible establecer la conecci�n con el Servidor..." & vbNewLine & "Verifique que la Ip ingresada sea la correcta", vbInformation
End Function
Public Function Existe(sArchivo As String) As Integer
    Existe = Len(Dir$(sArchivo))
End Function
Public Function NumReal(Numtexto As String) As Double
    NumReal = Val(Replace(Numtexto, ".", ""))
   
End Function
Public Function NumFormat(Numdtexto As Variant) As String
   ' If IsNull(Numdtexto) = 0 Then Numdtexto = 0
    NumFormat = Format(Numdtexto, "#,##0")
    If Len(NumFormat) = 0 Then NumFormat = "0"
End Function
Function TipoLetra(Tamano As Integer, Negrita As Boolean, Cursiva As Boolean, Subrayada As Boolean)
    Printer.FontSize = Tamano
    Printer.FontBold = Negrita
    Printer.FontItalic = Cursiva
    Printer.FontUnderline = Subrayada
End Function
Function FormularioCargado(NombreFormulario As String) As Boolean
    Dim Formulario As Form
    FormularioCargado = False
    For Each Formulario In Forms
       If (UCase(Formulario.Name) = UCase(NombreFormulario)) Then
        FormularioCargado = True
        Exit For
        End If
    Next
End Function
'FECHA DE CRECION: 01/11/2008
'CREADA POR: ALDO NAVARRETE
'FECHA Y NOMBRE DE ACTUALIZACION: 14/01/2009 - ALDO NAVARRETE
'OBSERVACION TAG LISTVIEW : 1 CARACTER: TIPO DATO (T-F-N); 2 Y 3 CARACTER: LONGITUD IMPRESION; 4 CARACTER: NRO DECIMALES
'******************************************************
Public Sub Exportar(LV As ListView, tit() As String, Formulario As Form, Progreso As ProgressBar, LbProgreso As SkinLabel)
On Error GoTo Genera_Error

Dim Tipo_Dato As String, Cantidad_Decimales As Integer, Tipo_Formato As String, mDato As String

If LV.ListItems.Count = 0 Then Exit Sub
Dim J As Integer
Dim V As Integer
Dim H As Integer
Dim mFec As Date
Call Inicio_Excel
Formulario.MousePointer = 11
objExcel.Visible = False
With objExcel.ActiveSheet
    .Range(.Cells(1, 1), .Cells(3, 1)).Font.Bold = True
    .Range(.Cells(1, 1), .Cells(1, 1)).Font.Size = 14
    .Range(.Cells(2, 1), .Cells(3, 1)).Font.Size = 12
    .Range(.Cells(5, 1), .Cells(5, LV.ColumnHeaders.Count)).Font.Bold = True
    .Range(.Cells(5, 1), .Cells(5, LV.ColumnHeaders.Count)).Borders.LineStyle = xlContinuous
    .Cells(1, 1) = UCase(tit(0))
    .Cells(2, 1) = UCase(tit(1))
    .Cells(3, 1) = UCase(tit(2))
End With
For J = 1 To LV.ColumnHeaders.Count
    objExcel.ActiveSheet.Cells(5, J) = LV.ColumnHeaders(J).Text
    objExcel.ActiveSheet.Columns(J).ColumnWidth = (LV.ColumnHeaders(J).Width) / 100
Next J
Progreso.Min = 1
If LV.ListItems.Count = 1 Then Progreso.Max = 10 Else Progreso.Max = LV.ListItems.Count

For V = 1 To LV.ListItems.Count
    LbProgreso = Format(V / Progreso.Max * 100, "###") & "%"
    Progreso.Value = V
    H = 1
    If Mid(LV.ColumnHeaders(H).Tag, 1, 1) = "F" Then
        mFec = LV.ListItems(V)
        objExcel.ActiveSheet.Cells(V + 5, H) = mFec
    ElseIf Mid(LV.ColumnHeaders(H).Tag, 1, 1) = "N" Then
        objExcel.ActiveSheet.Cells(V + 5, H) = CDbl(LV.ListItems(V))
    Else
        objExcel.ActiveSheet.Cells(V + 5, H) = LV.ListItems(V)
    End If
    For H = 1 To LV.ColumnHeaders.Count - 1
        If Mid(LV.ColumnHeaders(H + 1).Tag, 1, 1) = "F" Then
            mFec = LV.ListItems(V).SubItems(H)
            objExcel.ActiveSheet.Cells(V + 5, H + 1) = mFec
        ElseIf Mid(LV.ColumnHeaders(H + 1).Tag, 1, 1) = "N" Then
            If LV.ListItems(V).SubItems(H) = "" Then
                objExcel.ActiveSheet.Cells(V + 5, H + 1) = 0
            Else
                If Not IsNumeric(LV.ListItems(V).SubItems(H)) Then
                    objExcel.ActiveSheet.Cells(V + 5, H + 1) = 0
                Else
                    objExcel.ActiveSheet.Cells(V + 5, H + 1) = CDbl(LV.ListItems(V).SubItems(H))
                End If
            End If
        Else
            objExcel.ActiveSheet.Cells(V + 5, H + 1) = LV.ListItems(V).SubItems(H)
        End If
    Next H
Next V
'FORMATO COLUMNAS
For H = 1 To LV.ColumnHeaders.Count
    Tipo_Dato = Mid(LV.ColumnHeaders(H).Tag, 1, 1)
    If Len(LV.ColumnHeaders(H).Tag) = 4 Then Cantidad_Decimales = Mid(LV.ColumnHeaders(H).Tag, 4, 1)
    If Tipo_Dato = "N" Then
        Tipo_Formato = "#,##0"
        If Cantidad_Decimales > 0 Then Tipo_Formato = Tipo_Formato & "." & Mid("0000000000", 1, Cantidad_Decimales)
        If Cantidad_Decimales = 9 Then Tipo_Formato = "###0"
        With objExcel.ActiveSheet
            .Range(.Cells(5, H), .Cells(V + 8, H)).NumberFormat = Tipo_Formato
        End With
    End If
Next H

objExcel.Visible = True
Formulario.MousePointer = 0
Exit Sub

Genera_Error:
Formulario.MousePointer = 0
MsgBox "SE GENERO EL SIGUIENTE ERROR:" & Chr(13) & _
       UCase(Err.Description) & Chr(13) & _
       "NUMERO: " & Err.Number, vbCritical, mSis

End Sub
Public Sub ExportarNuevo(LV As ListView, tit() As String, Formulario As Form, Progreso As XP_ProgressBar)
On Error GoTo Genera_Error

Dim Tipo_Dato As String, Cantidad_Decimales As Integer, Tipo_Formato As String, mDato As String

If LV.ListItems.Count = 0 Then Exit Sub
Dim J As Integer
Dim V As Integer
Dim H As Integer
Dim mFec As Date
Call Inicio_Excel
Formulario.MousePointer = 11
objExcel.Visible = False
With objExcel.ActiveSheet
    .Range(.Cells(1, 1), .Cells(3, 1)).Font.Bold = True
    .Range(.Cells(1, 1), .Cells(1, 1)).Font.Size = 14
    .Range(.Cells(2, 1), .Cells(3, 1)).Font.Size = 12
    .Range(.Cells(5, 1), .Cells(5, LV.ColumnHeaders.Count)).Font.Bold = True
    .Range(.Cells(5, 1), .Cells(5, LV.ColumnHeaders.Count)).Borders.LineStyle = xlContinuous
    .Cells(1, 1) = UCase(tit(0))
    .Cells(2, 1) = UCase(tit(1))
    .Cells(3, 1) = UCase(tit(2))
   ' MsgBox UBound(tit)
End With
For J = 1 To LV.ColumnHeaders.Count
    objExcel.ActiveSheet.Cells(5, J) = LV.ColumnHeaders(J).Text
    objExcel.ActiveSheet.Columns(J).ColumnWidth = (LV.ColumnHeaders(J).Width) / 100
Next J
Progreso.Min = 1
If LV.ListItems.Count = 1 Then Progreso.Max = 10 Else Progreso.Max = LV.ListItems.Count

For V = 1 To LV.ListItems.Count
    LbProgreso = Format(V / Progreso.Max * 100, "###") & "%"
    Progreso.Value = V
    H = 1
    If Mid(LV.ColumnHeaders(H).Tag, 1, 1) = "F" Then
        If Len(LV.ListItems(V)) = 0 Then
            'ignoramos si no tiene valor la fecha
        Else
            mFec = LV.ListItems(V)
            objExcel.ActiveSheet.Cells(V + 5, H) = mFec
        End If
    ElseIf Mid(LV.ColumnHeaders(H).Tag, 1, 1) = "N" Then
        If Val(LV.ListItems(V)) > 0 Then objExcel.ActiveSheet.Cells(V + 5, H) = CDbl(LV.ListItems(V))
    Else
        objExcel.ActiveSheet.Cells(V + 5, H) = LV.ListItems(V)
    End If
    For H = 1 To LV.ColumnHeaders.Count - 1
        If Mid(LV.ColumnHeaders(H + 1).Tag, 1, 1) = "F" Then
            If Len(LV.ListItems(V).SubItems(H)) > 0 Then
              '  If Not IsDate(Len(LV.ListItems(V).SubItems(H))) Then
                    'ignoramos si no tiene valor la fecha
              '  Else
                    On Error Resume Next
                    mFec = LV.ListItems(V).SubItems(H)
                    objExcel.ActiveSheet.Cells(V + 5, H + 1) = mFec
              '  End If
            End If
        ElseIf Mid(LV.ColumnHeaders(H + 1).Tag, 1, 1) = "N" Then
            If LV.ListItems(V).SubItems(H) = "" Then
                If LV.ListItems(V) = "fn" Then
                    objExcel.ActiveSheet.Cells(V + 5, H + 1) = ""
                Else
                    objExcel.ActiveSheet.Cells(V + 5, H + 1) = 0
                End If
            Else
                If Not IsNumeric(LV.ListItems(V).SubItems(H)) Then
                    If LV.ListItems(V).SubItems(H) = "TOTAL" Then
                        objExcel.ActiveSheet.Cells(V + 5, H + 1) = "TOTAL"
                    Else
                        objExcel.ActiveSheet.Cells(V + 5, H + 1) = 0
                    End If
                Else
                    
                    objExcel.ActiveSheet.Cells(V + 5, H + 1) = CDbl(LV.ListItems(V).SubItems(H))
                    If LV.ListItems(V) = "fn" Then
                            'ponemos negrita al total
                            With objExcel.ActiveSheet
                                .Range(.Cells(V + 5, H + 1), .Cells(V + 5, H + 1)).Font.Bold = True
                              '  .Range(.Cells(1, 1), .Cells(1, 1)).Font.Size = 14
                              '  .Range(.Cells(2, 1), .Cells(3, 1)).Font.Size = 12
                              ''  .Range(.Cells(5, 1), .Cells(5, LV.ColumnHeaders.Count)).Font.Bold = True
                               ' .Range(.Cells(5, 1), .Cells(5, LV.ColumnHeaders.Count)).Borders.LineStyle = xlContinuous
                                
                               ' MsgBox UBound(tit)
                         End With
                    End If
                End If
            End If
        Else
            objExcel.ActiveSheet.Cells(V + 5, H + 1) = LV.ListItems(V).SubItems(H)
        End If
    Next H
Next V
'FORMATO COLUMNAS
For H = 1 To LV.ColumnHeaders.Count
    Tipo_Dato = Mid(LV.ColumnHeaders(H).Tag, 1, 1)
    If Len(LV.ColumnHeaders(H).Tag) = 4 Then Cantidad_Decimales = Mid(LV.ColumnHeaders(H).Tag, 4, 1)
    If Tipo_Dato = "N" Then
        Tipo_Formato = "#,##0"
        If Cantidad_Decimales > 0 Then Tipo_Formato = Tipo_Formato & "." & Mid("0000000000", 1, Cantidad_Decimales)
        If Cantidad_Decimales = 9 Then Tipo_Formato = "###0"
        With objExcel.ActiveSheet
            .Range(.Cells(5, H), .Cells(V + 8, H)).NumberFormat = Tipo_Formato
        End With
    End If
Next H

objExcel.Visible = True
Formulario.MousePointer = 0

Exit Sub

Genera_Error:
Formulario.MousePointer = 0
MsgBox "SE GENERO EL SIGUIENTE ERROR:" & Chr(13) & _
       UCase(Err.Description) & Chr(13) & _
       "NUMERO: " & Err.Number, vbCritical, mSis

End Sub

Public Sub ExportarNuevoDicoem(LV As ListView, tit() As String, Formulario As Form, Progreso As XP_ProgressBar)
On Error GoTo Genera_Error

Dim Tipo_Dato As String, Cantidad_Decimales As Integer, Tipo_Formato As String, mDato As String

If LV.ListItems.Count = 0 Then Exit Sub
Dim J As Integer
Dim V As Integer
Dim H As Integer
Dim mFec As Date
Call Inicio_Excel
Formulario.MousePointer = 11
objExcel.Visible = False
With objExcel.ActiveSheet
    .Range(.Cells(1, 1), .Cells(3, 1)).Font.Bold = True
    .Range(.Cells(1, 1), .Cells(1, 1)).Font.Size = 14
    .Range(.Cells(2, 1), .Cells(3, 1)).Font.Size = 12
    .Range(.Cells(5, 1), .Cells(5, LV.ColumnHeaders.Count)).Font.Bold = True
    .Range(.Cells(5, 1), .Cells(5, LV.ColumnHeaders.Count)).Borders.LineStyle = xlContinuous
    .Range(.Cells(1, 1), .Cells(1, LV.ColumnHeaders.Count)).Borders.LineStyle = xlContinuous
    .Range(.Cells(2, 1), .Cells(2, LV.ColumnHeaders.Count)).Borders.LineStyle = xlContinuous
    .Range(.Cells(2, 1), .Cells(2, LV.ColumnHeaders.Count)).Interior.ColorIndex = 12
    .Range(.Cells(1, 1), .Cells(1, LV.ColumnHeaders.Count)).Interior.ColorIndex = 12
        
    .Cells(1, 1) = UCase(tit(0))
    .Cells(2, 1) = UCase(tit(1))
    .Cells(3, 1) = UCase(tit(2))
   ' MsgBox UBound(tit)
End With
For J = 1 To LV.ColumnHeaders.Count
    objExcel.ActiveSheet.Cells(5, J) = LV.ColumnHeaders(J).Text
    objExcel.ActiveSheet.Columns(J).ColumnWidth = (LV.ColumnHeaders(J).Width) / 100
Next J
Progreso.Min = 1
If LV.ListItems.Count = 1 Then Progreso.Max = 10 Else Progreso.Max = LV.ListItems.Count

For V = 1 To LV.ListItems.Count
    LbProgreso = Format(V / Progreso.Max * 100, "###") & "%"
    Progreso.Value = V
    H = 1
    If Mid(LV.ColumnHeaders(H).Tag, 1, 1) = "F" Then
        If Len(LV.ListItems(V)) = 0 Then
            'ignoramos si no tiene valor la fecha
        Else
            mFec = LV.ListItems(V)
            objExcel.ActiveSheet.Cells(V + 5, H) = mFec
        End If
    ElseIf Mid(LV.ColumnHeaders(H).Tag, 1, 1) = "N" Then
        If Val(LV.ListItems(V)) > 0 Then objExcel.ActiveSheet.Cells(V + 5, H) = CDbl(LV.ListItems(V))
    Else
        objExcel.ActiveSheet.Cells(V + 5, H) = LV.ListItems(V)
    End If
    For H = 1 To LV.ColumnHeaders.Count - 1
        If Mid(LV.ColumnHeaders(H + 1).Tag, 1, 1) = "F" Then
            If Len(LV.ListItems(V).SubItems(H)) > 0 Then
              '  If Not IsDate(Len(LV.ListItems(V).SubItems(H))) Then
                    'ignoramos si no tiene valor la fecha
              '  Else
                    'On Error Resume Next
                    If LV.ListItems(V).SubItems(H) = "NULO" Then
                        objExcel.ActiveSheet.Cells(V + 5, H + 1) = "NULO"
                    Else
                    
                        mFec = LV.ListItems(V).SubItems(H)
                       objExcel.ActiveSheet.Cells(V + 5, H + 1) = mFec
                    End If
                    

              '  End If
            End If
        ElseIf Mid(LV.ColumnHeaders(H + 1).Tag, 1, 1) = "N" Then
            If LV.ListItems(V).SubItems(H) = "" Then
                If LV.ListItems(V) = "fn" Or LV.ListItems(V) = "nd" Then
                    objExcel.ActiveSheet.Cells(V + 5, H + 1) = ""
                Else
                    objExcel.ActiveSheet.Cells(V + 5, H + 1) = 0
                End If
            Else
                If Not IsNumeric(LV.ListItems(V).SubItems(H)) Then
                    If LV.ListItems(V).SubItems(H) = "TOTAL" Then
                        objExcel.ActiveSheet.Cells(V + 5, H + 1) = "TOTAL"
                    ElseIf LV.ListItems(V).SubItems(H) = "IVA" Then
                        objExcel.ActiveSheet.Cells(V + 5, H + 1) = "IVA"
                    ElseIf LV.ListItems(V).SubItems(H) = "TOTAL NETO" Then
                        objExcel.ActiveSheet.Cells(V + 5, H + 1) = "TOTAL NETO"
                    Else
                        objExcel.ActiveSheet.Cells(V + 5, H + 1) = 0
                    End If
                Else
                    
                    objExcel.ActiveSheet.Cells(V + 5, H + 1) = CDbl(LV.ListItems(V).SubItems(H))
                    If LV.ListItems(V) = "fn" Then
                            'ponemos negrita al total
                            With objExcel.ActiveSheet
                                .Range(.Cells(V + 5, H + 1), .Cells(V + 5, H + 1)).Font.Bold = True
                                .Range(.Cells(V + 5, H), .Cells(V + 5, H)).Font.Bold = True
                              '  .Range(.Cells(1, 1), .Cells(1, 1)).Font.Size = 14
                              '  .Range(.Cells(2, 1), .Cells(3, 1)).Font.Size = 12
                              ''  .Range(.Cells(5, 1), .Cells(5, LV.ColumnHeaders.Count)).Font.Bold = True
                               ' .Range(.Cells(5, 1), .Cells(5, LV.ColumnHeaders.Count)).Borders.LineStyle = xlContinuous
                                
                               ' MsgBox UBound(tit)
                         End With
                    End If
                End If
            End If

        Else
            objExcel.ActiveSheet.Cells(V + 5, H + 1) = LV.ListItems(V).SubItems(H)
            'If LV.ListItems(V).SubItems(H) = "fn" Then objExcel.ActiveSheet.Cells(V + 5, H + 1) = ""
        End If
       ' If LV.ListItems(V + 5) <> "nd" Then
            With objExcel.ActiveSheet
            .Range(.Cells(V + 5, 1), .Cells(V + 5, LV.ColumnHeaders.Count)).Borders.LineStyle = xlContinuous
            End With
       ' End If
         If LV.ListItems(V) = "fn" Then
                    objExcel.ActiveSheet.Cells(V + 5, 1) = ""
        End If
        
        If LV.ListItems(V) = "nd" Then
                    objExcel.ActiveSheet.Cells(V + 5, 1) = ""
        End If
    Next H
Next V
'FORMATO COLUMNAS
For H = 1 To LV.ColumnHeaders.Count
    Tipo_Dato = Mid(LV.ColumnHeaders(H).Tag, 1, 1)
    If Len(LV.ColumnHeaders(H).Tag) = 4 Then Cantidad_Decimales = Mid(LV.ColumnHeaders(H).Tag, 4, 1)
    If Tipo_Dato = "N" Then
        Tipo_Formato = "#,##0"
        If Cantidad_Decimales > 0 Then Tipo_Formato = Tipo_Formato & "." & Mid("0000000000", 1, Cantidad_Decimales)
        If Cantidad_Decimales = 9 Then Tipo_Formato = "###0"
        With objExcel.ActiveSheet
            .Range(.Cells(5, H), .Cells(V + 8, H)).NumberFormat = Tipo_Formato
        End With
    End If
Next H

objExcel.Visible = True
Formulario.MousePointer = 0

Exit Sub

Genera_Error:
Formulario.MousePointer = 0
MsgBox "SE GENERO EL SIGUIENTE ERROR:" & Chr(13) & _
       UCase(Err.Description) & Chr(13) & _
       "NUMERO: " & Err.Number, vbCritical, mSis

End Sub


Public Sub LLenar_Grilla(Record_Set As ADODB.Recordset, Name_Formulario As Form, Name_ListView As ListView, Con_ChekBox As Boolean, Full_Row_Select As Boolean, Grid_line As Boolean, Mensaje_Grilla_Vacia As Boolean)
On Error GoTo Genera_Error
Dim Contador_i As Integer, Tipo_Dato As Variant, Cantidad_Decimales As Integer, Tipo_Formato As String, mDato As String
Name_Formulario.MousePointer = 11
Name_ListView.ListItems.Clear
Name_ListView.CheckBoxes = Con_ChekBox
If Record_Set.RecordCount > 0 Then
    Record_Set.MoveFirst
    While Not Record_Set.EOF
        For Contador_i = 1 To Record_Set.Fields.Count
            'DEFINE TIPO DE DATO Y FORMATO
            Tipo_Dato = Mid(Name_ListView.ColumnHeaders(Contador_i).Tag, 1, 1)
            Cantidad_Decimales = 0
            If Name_ListView.ColumnHeaders(Contador_i).Tag <> Empty Then Cantidad_Decimales = Mid(Name_ListView.ColumnHeaders(Contador_i).Tag, 4, 1)
            
            If Tipo_Dato = "T" Then
                mDato = ""
                Tipo_Formato = ">"
            End If
            If Tipo_Dato = "N" Then
                mDato = 0
                Tipo_Formato = "#,###0"
            End If
            If Tipo_Dato = "F" Then
                mDato = ""
                Tipo_Formato = "DD-MM-YYYY"
            End If
            If Tipo_Dato = "" Then
                mDato = ""
                Tipo_Formato = ">"
            End If
            
            If Cantidad_Decimales > 0 Then Tipo_Formato = Tipo_Formato & "." & Mid("0000000000", 1, Cantidad_Decimales)
            If Cantidad_Decimales = 9 Then Tipo_Formato = "##0"
            If Not IsNull(Record_Set.Fields(Contador_i - 1)) Then
                mDato = Format(Record_Set.Fields(Contador_i - 1), Tipo_Formato)
            End If
            
            If Contador_i = 1 Then
                Set itmx = Name_ListView.ListItems.Add(, , mDato)
            Else
                itmx.SubItems(Contador_i - 1) = mDato
            End If
        Next Contador_i
        Record_Set.MoveNext
    Wend
Else
    If Mensaje_Grilla_Vacia Then MsgBox "NO EXISTEN REGISTROS PARA VISUALIZAR !!!", vbCritical, mSis
End If
Name_ListView.View = lvwReport
Name_ListView.LabelEdit = lvwManual
Name_ListView.FullRowSelect = Full_Row_Select
Name_ListView.Gridlines = Grid_line
Name_Formulario.MousePointer = 0
Name_Formulario.Refresh
Exit Sub

Genera_Error:
Name_Formulario.MousePointer = 0
'MsgBox "SE GENERO EL SIGUIENTE ERROR:" & Chr(13) & _
'       UCase(Err.Description) & Chr(13) & _
'       "NUMERO: " & Err.Number, vbCritical, mSis
End Sub
Public Function Inicio_Excel() As Boolean
    Dim J As Integer
    Set objExcel = New Excel.Application
    objExcel.Visible = True
    objExcel.SheetsInNewWorkbook = 1
    objExcel.Workbooks.Add
End Function

Public Function Formato_Excel(Num_Campos As Integer, Nombre_Campos() As String) As Boolean
    With objExcel.ActiveSheet
            .Range(.Cells(3, 1), .Cells(3, Num_Campos + 1)).Borders.LineStyle = xlContinuous
            .Range(.Cells(3, 1), .Cells(3, Num_Campos + 1)).Font.Bold = True
        For IG_I = 0 To Num_Campos Step 1
            .Cells(3, IG_I + 1) = Nombre_Campos(IG_I)
        Next IG_I
    End With
End Function

Sub Busca_Id_Combo(Combo As ComboBox, Codigo As Double)
For IG_I = 0 To Combo.ListCount - 1
    If Combo.ItemData(IG_I) = Codigo Then
       Combo.ListIndex = IG_I
       Exit Sub
    End If
Next IG_I
On Error Resume Next
Combo.ListIndex = -1
End Sub
Sub Kardex(FechaKardex As Date, Movimiento As String, Id_Documento As Integer, _
           Nro_Documento As Double, Id_Bodega As Integer, elCodigo As String, _
           LaCantidad As Double, DescripcionM As String, PrecioNeto As Double, _
           ValorCompra As Double, Optional ByVal ElRutProveedor As String, _
           Optional ByVal LaRazon As String, Optional ByVal ActualizaPcompra As String, _
           Optional ByVal ElPrecioCosto As Double, Optional ByVal IpDocID As Integer, _
           Optional ByVal SoloUnidades As String, Optional ByVal SoloValores As String, _
           Optional ByVal MovFinal As String, Optional ByVal LPidVenta)
           
           
    Dim l_Nuevo As Double, l_Anterior As Double, Existe As Boolean, lp_UPcompra As Double
    If IpDocID = Empty Then IpDocID = 0
    If Val(LPidVenta) = 0 Then LPidVenta = 0
    If SoloUnidades = Empty Then SoloUnidades = "NO"
    If Len(SoloValores) = 0 Then
        SoloValores = "NO"
    Else
        If SoloValores = "SI" Then Movimiento = "NN"
    End If
    
    'Lo primero es saber el saldo anterior y cual sera el nuevo
    'si se encuentra el registro se actualiza, de lo contrario se agrega
    If ElRutProveedor = Empty Then ElRutProveedor = Space(1)
    If LaRazon = Empty Then LaRazon = Space(1)
    Sql = "SELECT sto_stock,pro_ultimo_precio_compra pcompra " & _
          "FROM pro_stock s " & _
          "WHERE rut_emp='" & SP_Rut_Activo & "' AND pro_codigo='" & elCodigo & "' AND bod_id=" & Id_Bodega
    Consulta RsTmp2, Sql
    If RsTmp2.RecordCount > 0 Then
        Existe = True
        l_Anterior = RsTmp2!sto_stock
        lp_UPcompra = RsTmp2!pcompra
        If Movimiento = "SALIDA" Then
            l_Nuevo = l_Anterior - LaCantidad
        ElseIf Movimiento = "ENTRADA" Then
            lp_UPcompra = PrecioNeto
            l_Nuevo = l_Anterior + LaCantidad
        ElseIf Movimiento = "NN" Then
            l_Nuevo = l_Anterior
        End If
        
        Sql = "UPDATE pro_stock " & _
              "SET sto_stock=" & CxP(Str(l_Nuevo)) & _
              ",pro_ultimo_precio_compra=" & CxP(lp_UPcompra) & _
              " WHERE rut_emp='" & SP_Rut_Activo & "' AND  pro_codigo='" & elCodigo & "' AND bod_id=" & Id_Bodega
    Else
        'Nuevo registro
        If Movimiento = "SALIDA" Then
            l_Nuevo = 0 - LaCantidad
        ElseIf Movimiento = "ENTRADA" Then
            l_Nuevo = LaCantidad
            lp_UPcompra = PrecioNeto
     '   ElseIf Movimiento = "NN" Then
      '      l_Nuevo = LaCantidad
        End If
        Sql = "INSERT INTO pro_stock " & _
                          "(pro_codigo,bod_id,sto_stock,pro_ultimo_precio_compra,rut_emp) " & _
                   "VALUES('" & elCodigo & "'," & Id_Bodega & "," & CxP(l_Nuevo) & "," & CxP(lp_UPcompra) & ",'" & SP_Rut_Activo & "')"
                           
        Existe = False
        l_Anterior = 0
        ' �If Movimiento = "SALIDA" Then
        '    l_Nuevo = l_Anterior - LaCantidad
        'ElseIf Movimiento = "ENTRADA" Then
        '    l_Nuevo = l_Anterior + LaCantidad
        'End If
        
    End If
    cn.Execute Sql
    
    If Movimiento = "ENTRADA" And ActualizaPcompra = "SI" Then
        
        Sql = "UPDATE maestro_productos " & _
              "SET precio_compra=" & CxP(CDbl(lp_UPcompra)) & " " & _
              "WHERE rut_emp='" & SP_Rut_Activo & "' AND  codigo='" & elCodigo & "'"
        cn.Execute Sql
    End If
    
    
    'Ahora insertamos el Kardex
    Dim dp_SaldoValor As Double
    Dim dp_NuevoSaldoValor As Double
    Dim dp_CostoPromedioActual As Double
    
    Sql = "SELECT kar_nuevo_saldo_valor,IFNULL(ROUND(kar_nuevo_saldo_valor/kar_nuevo_saldo,0),0) costo_promedio " & _
          "FROM inv_kardex " & _
          "WHERE rut_emp='" & SP_Rut_Activo & "' AND  pro_codigo ='" & elCodigo & "' AND bod_id=" & Id_Bodega & " " & _
          "ORDER BY kar_id DESC " & _
          "LIMIT 1"
    Consulta RsTmp2, Sql
    
    dp_NuevoSaldoValor = ValorCompra
    If RsTmp2.RecordCount > 0 Then
        dp_SaldoValor = RsTmp2!kar_nuevo_saldo_valor
        dp_CostoPromedioActual = Abs(RsTmp2!costo_promedio)
        If dp_CostoPromedioActual = 0 Then dp_CostoPromedioActual = ObtenerPromedio(Val(elCodigo))
        If Movimiento = "ENTRADA" Then
            dp_NuevoSaldoValor = dp_SaldoValor + ValorCompra
        ElseIf Movimiento = "SALIDA" Then
            If ElPrecioCosto = 0 Then
            
              
                    If SoloUnidades = "NO" Then
                        PrecioNeto = dp_CostoPromedioActual
                        ValorCompra = dp_CostoPromedioActual * LaCantidad
                        dp_NuevoSaldoValor = dp_SaldoValor - (dp_CostoPromedioActual * LaCantidad)
                    Else
                        dp_NuevoSaldoValor = dp_SaldoValor
                    End If
              
                
            Else
                ValorCompra = ElPrecioCosto * LaCantidad
                dp_NuevoSaldoValor = dp_SaldoValor - ValorCompra
            
            End If
            If ValorCompra = 0 Then
                If SoloUnidades = "NO" Then
                    ValorCompra = lp_UPcompra
                    dp_NuevoSaldoValor = lp_UPcompra * LaCantidad
                End If
                
            End If
            'dp_NuevoSaldoValor = dp_CostoPromedioActual
        ElseIf Movimiento = "NN" Then
            dp_NuevoSaldoValor = dp_SaldoValor - ValorCompra
        End If
    End If
    
          
    If Len(MovFinal) > 0 Then Movimiento = MovFinal
          
          
    
    Sql = "INSERT INTO inv_kardex " & _
          "(kar_fecha,kar_movimiento,kar_documento,kar_numero,bod_id,pro_codigo,kar_saldo_anterior," & _
          "kar_cantidad,kar_nuevo_saldo,kar_usuario,kar_descripcion,pro_precio_neto,kar_saldo_anterior_valor, " & _
          "kar_cantidad_valor,kar_nuevo_saldo_valor,rut,rsocial,rut_emp,doc_id,id) " & _
          "VALUES('" & Format(FechaKardex, "YYYY-MM-DD") & "','" & Movimiento & "'," & Id_Documento & "," & _
          Nro_Documento & "," & Id_Bodega & ",'" & elCodigo & "'," & CxP(Str(l_Anterior)) & "," & CxP(Str(LaCantidad)) & _
          "," & CxP(Str(l_Nuevo)) & ",'" & LogUsuario & "','" & DescripcionM & "'," & CxP(Str(PrecioNeto)) & "," & _
          CxP(Str(dp_SaldoValor)) & "," & CxP(Str(ValorCompra)) & "," & CxP(Str(dp_NuevoSaldoValor)) & ",'" & ElRutProveedor & "','" & LaRazon & "','" & SP_Rut_Activo & "'," & IpDocID & "," & LPidVenta & ") "
    cn.Execute Sql
End Sub

Sub KardexVenta(FechaKardex As Date, Movimiento As String, Id_Documento As Integer, _
           Nro_Documento As Double, Id_Bodega As Integer, elCodigo As String, _
           LaCantidad As Double, DescripcionM As String, PrecioNeto As Double, _
           ValorCompra As Double, Optional ByVal ElRutProveedor As String, _
           Optional ByVal LaRazon As String, Optional ByVal ActualizaPcompra As String, _
           Optional ByVal ElPrecioCosto As Double, Optional ByVal IpDocID As Integer, _
           Optional ByVal SoloUnidades As String, Optional ByVal SoloValores As String, _
           Optional ByVal MovFinal As String, Optional ByVal LPidVenta)
           
           
    Dim l_Nuevo As Double, l_Anterior As Double, Existe As Boolean, lp_UPcompra As Double
    If IpDocID = Empty Then IpDocID = 0
    If Val(LPidVenta) = 0 Then LPidVenta = 0
    If SoloUnidades = Empty Then SoloUnidades = "NO"
    If Len(SoloValores) = 0 Then
        SoloValores = "NO"
    Else
        If SoloValores = "SI" Then Movimiento = "NN"
    End If
    
    'Lo primero es saber el saldo anterior y cual sera el nuevo
    'si se encuentra el registro se actualiza, de lo contrario se agrega
    If ElRutProveedor = Empty Then ElRutProveedor = Space(1)
    If LaRazon = Empty Then LaRazon = Space(1)
    Sql = "SELECT sto_stock,pro_ultimo_precio_compra pcompra " & _
          "FROM pro_stock s " & _
          "WHERE rut_emp='" & SP_Rut_Activo & "' AND pro_codigo='" & elCodigo & "' AND bod_id=" & Id_Bodega
    Consulta RsTmp2, Sql
    If RsTmp2.RecordCount > 0 Then
        Existe = True
        l_Anterior = RsTmp2!sto_stock
        lp_UPcompra = RsTmp2!pcompra
        If Movimiento = "SALIDA" Then
            l_Nuevo = l_Anterior - LaCantidad
        ElseIf Movimiento = "ENTRADA" Then
            lp_UPcompra = PrecioNeto
            l_Nuevo = l_Anterior + LaCantidad
        ElseIf Movimiento = "NN" Then
            l_Nuevo = l_Anterior
        End If
        
        Sql = "UPDATE pro_stock " & _
              "SET sto_stock=" & CxP(Str(l_Nuevo)) & _
              ",pro_ultimo_precio_compra=" & CxP(lp_UPcompra) & _
              " WHERE rut_emp='" & SP_Rut_Activo & "' AND  pro_codigo='" & elCodigo & "' AND bod_id=" & Id_Bodega
    Else
        'Nuevo registro
        If Movimiento = "SALIDA" Then
            l_Nuevo = 0 - LaCantidad
        ElseIf Movimiento = "ENTRADA" Then
            l_Nuevo = LaCantidad
            lp_UPcompra = PrecioNeto
     '   ElseIf Movimiento = "NN" Then
      '      l_Nuevo = LaCantidad
        End If
        Sql = "INSERT INTO pro_stock " & _
                          "(pro_codigo,bod_id,sto_stock,pro_ultimo_precio_compra,rut_emp) " & _
                   "VALUES('" & elCodigo & "'," & Id_Bodega & "," & CxP(l_Nuevo) & "," & CxP(lp_UPcompra) & ",'" & SP_Rut_Activo & "')"
                           
        Existe = False
        l_Anterior = 0
        ' �If Movimiento = "SALIDA" Then
        '    l_Nuevo = l_Anterior - LaCantidad
        'ElseIf Movimiento = "ENTRADA" Then
        '    l_Nuevo = l_Anterior + LaCantidad
        'End If
        
    End If
    cn.Execute Sql
    
    
    
    
    'Ahora insertamos el Kardex
    Dim dp_SaldoValor As Double
    Dim dp_NuevoSaldoValor As Double
    Dim dp_CostoPromedioActual As Double
    
    Sql = "SELECT pro_precio_neto,kar_nuevo_saldo_valor,IFNULL(ROUND(kar_nuevo_saldo_valor/kar_nuevo_saldo,0),0) costo_promedio " & _
          "FROM inv_kardex " & _
          "WHERE rut_emp='" & SP_Rut_Activo & "' AND  pro_codigo ='" & elCodigo & "' AND bod_id=" & Id_Bodega & " " & _
          "ORDER BY kar_id DESC " & _
          "LIMIT 1"
    Consulta RsTmp2, Sql
    
    dp_NuevoSaldoValor = ValorCompra
    If RsTmp2.RecordCount > 0 Then
        dp_SaldoValor = RsTmp2!kar_nuevo_saldo_valor
        dp_CostoPromedioActual = Abs(RsTmp2!pro_precio_neto)
        'If dp_CostoPromedioActual = 0 Then dp_CostoPromedioActual = ObtenerPromedio(Val(ElCodigo))
        If Movimiento = "ENTRADA" Then
            dp_NuevoSaldoValor = dp_SaldoValor + ValorCompra
        ElseIf Movimiento = "SALIDA" Then
            If ElPrecioCosto = 0 Then
            
              
                    If SoloUnidades = "NO" Then
                        PrecioNeto = dp_CostoPromedioActual
                        ValorCompra = dp_CostoPromedioActual * LaCantidad
                        dp_NuevoSaldoValor = dp_SaldoValor - (dp_CostoPromedioActual * LaCantidad)
                    Else
                        dp_NuevoSaldoValor = dp_SaldoValor
                    End If
              
                
            Else
                ValorCompra = ElPrecioCosto * LaCantidad
                dp_NuevoSaldoValor = dp_SaldoValor - ValorCompra
            
            End If
            If ValorCompra = 0 Then
                If SoloUnidades = "NO" Then
                    ValorCompra = lp_UPcompra
                    dp_NuevoSaldoValor = lp_UPcompra * LaCantidad
                End If
                
            End If
            'dp_NuevoSaldoValor = dp_CostoPromedioActual
        ElseIf Movimiento = "NN" Then
            dp_NuevoSaldoValor = dp_SaldoValor - ValorCompra
        End If
    End If
    
          
    If Len(MovFinal) > 0 Then Movimiento = MovFinal
          
          
    
    Sql = "INSERT INTO inv_kardex " & _
          "(kar_fecha,kar_movimiento,kar_documento,kar_numero,bod_id,pro_codigo,kar_saldo_anterior," & _
          "kar_cantidad,kar_nuevo_saldo,kar_usuario,kar_descripcion,pro_precio_neto,kar_saldo_anterior_valor, " & _
          "kar_cantidad_valor,kar_nuevo_saldo_valor,rut,rsocial,rut_emp,doc_id,id) " & _
          "VALUES('" & Format(FechaKardex, "YYYY-MM-DD") & "','" & Movimiento & "'," & Id_Documento & "," & _
          Nro_Documento & "," & Id_Bodega & ",'" & elCodigo & "'," & CxP(Str(l_Anterior)) & "," & CxP(Str(LaCantidad)) & _
          "," & CxP(Str(l_Nuevo)) & ",'" & LogUsuario & "','" & DescripcionM & "'," & CxP(Str(PrecioNeto)) & "," & _
          CxP(Str(dp_SaldoValor)) & "," & CxP(Str(ValorCompra)) & "," & CxP(Str(dp_NuevoSaldoValor)) & ",'" & ElRutProveedor & "','" & LaRazon & "','" & SP_Rut_Activo & "'," & IpDocID & "," & LPidVenta & ") "
    cn.Execute Sql
End Sub





Public Sub KardexNN_NotaDebito(FechaKardex As Date, Movimiento As String, Id_Documento As Integer, _
           Nro_Documento As Double, Id_Bodega As Integer, elCodigo As String, _
           DescripcionM As String, ValorEntrada As Long, Optional ByVal ElPrecioCosto As Double, _
           Optional ByVal ElRutProveedor As String, _
           Optional ByVal LaRazon As String, Optional ByVal Signo As String, Optional ByVal IpDocID As Integer)
           
    Dim pl_SaldoAnteriorCantidad As Long
    Dim pl_SaldoAnteriorValor As Long
    Dim pl_PrecioNetoAnterior As Long
    Dim pl_Nuevo_PrecioCompra As Long
    Sql = "SELECT kar_nuevo_saldo cantidad,kar_nuevo_saldo_valor valor,pro_precio_neto neto " & _
          "FROM inv_kardex " & _
          "WHERE rut_emp='" & SP_Rut_Activo & "' AND  pro_codigo ='" & elCodigo & "' " & _
          "ORDER BY kar_id DESC " & _
          "LIMIT 1"
    Consulta RsTmp2, Sql
    If IpDocID = Empty Then IpDocID = 0
    If RsTmp2.RecordCount = 0 Then Exit Sub 'si no se encuentra ningun registro salimos, pero no debiera ocurrir.
    pl_SaldoAnteriorCantidad = RsTmp2!cantidad
    pl_SaldoAnteriorValor = RsTmp2!Valor
    pl_PrecioNetoAnterior = RsTmp2!Neto
    Sql = "INSERT INTO inv_kardex " & _
          "(kar_fecha,kar_movimiento,kar_documento,kar_numero,bod_id,pro_codigo,kar_saldo_anterior," & _
          "kar_cantidad,kar_nuevo_saldo,kar_usuario,kar_descripcion,pro_precio_neto,kar_saldo_anterior_valor, " & _
          "kar_cantidad_valor,kar_nuevo_saldo_valor,rut,rsocial,rut_emp,doc_id) " & _
          "VALUES('" & Format(FechaKardex, "YYYY-MM-DD") & "','" & Movimiento & "'," & Id_Documento & "," & _
          Nro_Documento & "," & Id_Bodega & ",'" & elCodigo & "'," & CDbl(pl_SaldoAnteriorCantidad) & "," & 0 & _
          "," & pl_SaldoAnteriorCantidad & ",'" & LogUsuario & "','" & DescripcionM & "'," & Round(pl_PrecioNetoAnterior + (ValorEntrada / IIf(pl_SaldoAnteriorCantidad = 0, 1, pl_SaldoAnteriorCantidad))) & "," & _
          CDbl(pl_SaldoAnteriorValor) & "," & ValorEntrada & "," & CDbl(pl_SaldoAnteriorValor) & Signo & ValorEntrada & ",'" & ElRutProveedor & "','" & LaRazon & "','" & SP_Rut_Activo & "'," & IpDocID & ")"
    Call Consulta(RsTmp2, Sql)
    If Signo = "+" Then
        pl_Nuevo_PrecioCompra = (CDbl(pl_SaldoAnteriorValor) + ValorEntrada) / pl_SaldoAnteriorCantidad
    ElseIf Signo = "-" Then
        pl_Nuevo_PrecioCompra = (CDbl(pl_SaldoAnteriorValor) - ValorEntrada) / IIf(pl_SaldoAnteriorCantidad = 0, 1, pl_SaldoAnteriorCantidad)
    End If
    Sql = "UPDATE pro_stock SET pro_ultimo_precio_compra= " & pl_Nuevo_PrecioCompra & "  " & _
          "WHERE rut_emp='" & SP_Rut_Activo & "' AND  pro_codigo='" & elCodigo & "' AND bod_id=1"
    Call Consulta(RsTmp2, Sql)
          
End Sub
Public Function UltimoNro(S_tabla As String, S_campo As String) As Long
    Dim s_Sql As String, rg_Rst As Recordset
    's_Sql = "SELECT MAX(" & S_campo & ") numero " & _
            "FROM " & S_tabla
            
    '17 Enero 2012 Toma
    s_Sql = "SHOW TABLE STATUS LIKE '" & S_tabla & "'"
    Consulta rg_Rst, s_Sql
    If rg_Rst.RecordCount > 0 Then
        UltimoNro = (0 & rg_Rst!Auto_increment)
    Else
        UltimoNro = 1
    End If
End Function
Public Function UltimoMayor(S_tabla As String, S_campo As String, Optional ByVal rut_Empresa) As Long
    Dim s_Sql As String, rg_Rst As Recordset, Sp_Empresa As String
    If Len(rut_Empresa) > 0 Then Sp_Empresa = " WHERE rut_emp='" & rut_Empresa & "' "
    s_Sql = "SELECT MAX(" & S_campo & ") mayor " & _
            "FROM " & S_tabla & _
            Sp_Empresa
    Consulta rg_Rst, s_Sql
    If rg_Rst.RecordCount > 0 Then
        UltimoMayor = (0 & rg_Rst!mayor) + 1
    Else
        UltimoMayor = 1
    End If
End Function

Public Sub En_Foco(ctl As TextBox)
    On Error Resume Next
    If Not IsNull(ctl.Text) Then
       ctl.SelStart = 0
      ctl.SelLength = Len(ctl.Text)
    End If
End Sub

Public Sub Centrar(fr As Form, Optional Tiene_Banner As Boolean)
fr.Move (Screen.Width - fr.Width) / 2, (Screen.Height - fr.Height) / 2
On Error Resume Next
fr.BackColor = &HFFFFFF
fr.KeyPreview = True

If Tiene_Banner Then
    fr.Logo = LoadPicture(App.Path & "\REDMAROK.jpg")
    '
End If
End Sub
Public Function TotalizaColumna(LV As ListView, Clave As String) As Double
    Dim ip_Columna As Integer, Lp_Total As Double, TColumna As Double
    Dim J As Long
    If LV.ListItems.Count = 0 Then
        TotalizaColumna = 0
        Exit Function
    End If
    For J = 2 To LV.ColumnHeaders.Count
        If LV.ColumnHeaders(J).Key = Clave Then
            ip_Columna = J
            Exit For
        End If
    Next
    Lp_Total = 0
    For J = 1 To LV.ListItems.Count
        If LV.ListItems(J).SubItems(ip_Columna - 1) = "" Then LV.ListItems(J).SubItems(ip_Columna - 1) = "0"
        If Val(LV.ListItems(J).SubItems(ip_Columna - 1)) = 0 Then
             '' LV.ListItems(J).SubItems(ip_Columna - 1) = "0"
        Else
            Lp_Total = Lp_Total + CDbl(LV.ListItems(J).SubItems(ip_Columna - 1))
        End If
    Next
    TotalizaColumna = Lp_Total
End Function
Public Function ctacte(TxtRut As String, CliPro As String, Ip_IdDocumento As Integer, FechaDoc As String, obs As String, Lp_Nro As Long, Lp_Monto As Long, Anular As Boolean)
    Dim Lp_SaldoCtaCte As Long
    Dim Lp_Abono As Long, Lp_Cargo As Long, Lp_Saldo As Long
     'CONSULTAMOS SALDO ACTUAL DE LA CTA CTE
    Sql = "SELECT cta_saldo " & _
          "FROM cta_corriente " & _
          "WHERE cta_cli_pro='" & CliPro & "' AND cta_rut='" & TxtRut & "' AND rut_emp='" & SP_Rut_Activo & "' " & _
          "ORDER BY cta_id DESC " & _
          "LIMIT 1 "
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then Lp_SaldoCtaCte = RsTmp!cta_saldo Else Lp_SaldoCtaCte = 0
    Lp_Abono = 0
    Lp_Cargo = 0
    If IG_id_Nota_Credito_Clientes = Ip_IdDocumento Or IG_id_Nota_Credito_Electronica = Ip_IdDocumento Or IG_id_Nota_Credito = Ip_IdDocumento Then
        Lp_Abono = CDbl(Lp_Monto)
        If Anular Then 'Solo si anula Nota de Credito (cliente o provedor)
            Lp_Cargo = CDbl(Lp_Monto)
            Lp_Abono = 0
        End If
            
    Else
        Lp_Cargo = CDbl(Lp_Monto)
        If Anular Then
            Lp_Abono = CDbl(Lp_Monto)
            Lp_Cargo = 0
        End If
        
    End If
    Lp_SaldoCtaCte = Lp_SaldoCtaCte + Lp_Cargo - Lp_Abono
    Sql = "INSERT INTO cta_corriente (cta_cli_pro,cta_fecha,cta_rut,cta_observacion,cta_cargo,cta_abono,cta_saldo,rut_emp) VALUES( " & _
          "'" & CliPro & "','" & FechaDoc & "','" & TxtRut & "','" & obs & " " & lpnro & "'," & Lp_Cargo & "," & Lp_Abono & "," & Lp_SaldoCtaCte & ",'" & SP_Rut_Activo & "')"
    cn.Execute Sql
    'Fin cta corriente
End Function
Public Function PagoDocumento(Lp_IdAbo As Long, Sm_Cli_Pro As String, AboRut As String, Fecha As Date, _
    TxtAbonos As Long, FormaPagoID As Long, FormaPagoTexto As String, Sp_EC As String, lUsuario As String, NroComprobante As Long, Optional ByVal Referencia, Optional ByVal Origen)
    Dim Lp_Refef As Long
    Lp_Refef = Referencia
    'If Referencia <> Empty Then Lp_Refef = 0
    Sql = "INSERT INTO cta_abonos (abo_id,abo_cli_pro,abo_rut,abo_fecha,abo_monto,mpa_id,abo_observacion,usu_nombre,rut_emp,abo_fecha_pago,abo_nro_comprobante,id_ref,abo_origen) " & _
          "VALUES(" & Lp_IdAbo & ",'" & Sm_Cli_Pro & "','" & AboRut & "','" & _
          Format(Fecha, "YYYY-MM-DD") & "'," & CDbl(TxtAbonos) & "," & FormaPagoID & _
          ",'" & FormaPagoTexto & " " & Sp_EC & "','" & lUsuario & "','" & SP_Rut_Activo & "','" & Fql(Fecha) & "'," & NroComprobante & "," & Lp_Refef & ",'" & Origen & "')"
    cn.Execute Sql
    
    Sql = "INSERT INTO abo_tipos_de_pagos (abo_id,mpa_id,pad_valor) " & _
                         "VALUES(" & Lp_IdAbo & "," & FormaPagoID & "," & CDbl(TxtAbonos) & ")"
    cn.Execute Sql
        
End Function

Public Function Fql(fp_Fecha As Date) As String
    Fql = Format(fp_Fecha, "YYYY-MM-DD")
End Function
Public Function Fqls(fp_Fecha As Date) As String
    Fqls = Format(fp_Fecha, "DD-MM-YYYY")
End Function
Public Function CxP(Valor As Variant) As String
    CxP = Replace(Valor, ",", ".")
End Function
Public Function PxC(Valor As Variant) As String
    PxC = Replace(Valor, ".", ",")
End Function
Public Function CostoAVG(CodPro As String, BodId As Integer) As Double
    Dim Qlc As String, RgCP As Recordset
    'Correccion 16 Febrero 2013
    
    CostoAVG = 0
   ' Qlc = "SELECT IFNULL(SUM(kar_cantidad_valor)/SUM(kar_cantidad),0) promedio " & _
            "FROM inv_kardex k " & _
            "WHERE pro_codigo='" & CodPro & "' AND rut_emp='" & SP_Rut_Activo & " 'AND k.kar_movimiento='ENTRADA' " & _
            "GROUP BY k.pro_codigo "
      Qlc = "SELECT IFNULL(kar_cantidad_valor/kar_cantidad,0) promedio " & _
            "FROM inv_kardex k " & _
            "WHERE pro_codigo='" & CodPro & "' AND rut_emp='" & SP_Rut_Activo & "' AND bod_id=" & BodId & " " & _
            "ORDER BY kar_id DESC " & _
             "LIMIT 1 "
    
    Consulta RgCP, Qlc
    If RgCP.RecordCount > 0 Then CostoAVG = RgCP!promedio
End Function
Public Function BoldColuna(LV As ListView, Col As Integer)
    If LV.ListItems.Count > 0 Then
        For i = 1 To LV.ListItems.Count
            LV.ListItems(i).ListSubItems(5).Bold = True
        Next
    End If
End Function
Public Function ObtID(Cbo As ComboBox) As Long
    If Cbo.ListIndex > -1 Then
        ObtID = Cbo.ItemData(Cbo.ListIndex)
    Else
        ObtID = 0
    End If
End Function
Private Function ObtenerPromedio(Cod As Long) As Long
    Dim Sqp As String, Rsp As Recordset
    Sqp = "SELECT SUM(k.pro_precio_neto)/COUNT(kar_id) promedio " & _
          "FROM inv_kardex k " & _
          "WHERE rut_emp='" & SP_Rut_Activo & "' AND pro_precio_neto > 0 And pro_codigo =" & Cod
    Consulta Rsp, Sqp
    If Rsp.RecordCount > 0 Then
        ObtenerPromedio = 0 & Rsp!promedio
    Else
        ObtenerPromedio = 0
    End If
End Function
'Ordenar el ListView pinchando en el ancabezado de la columna
'jueves 13 de octubre 2011 23:21

Public Function ordListView(ByVal ColumnHeaderX As MSComctlLib.ColumnHeader, elFormulario As Form, listviewx As ListView)
      
    With listviewx
      
        Dim i As Long
        Dim Formato As String
        Dim strData() As String
          
        Dim Columna As Long
          
        Call SendMessage(elFormulario.hWnd, WM_SETREDRAW, 0&, 0&)
          
          
        Columna = ColumnHeaderX.Index - 1
          
        '''''''''''''''''''''''''''''''''''''''''''''
        ' Tipo de dato a ordenar
        ''''''''''''''''''''''''''''''''''''''''''''''
        
        
        Select Case UCase$(Mid((ColumnHeaderX.Tag), 1, 1))
      
          
        ' Fecha
        '''''''''''''''''''''''''''''''''''''''''''''
        Case "F"
          
            Formato = "YYYYMMDDHhNnSs"
          
            ' Ordena alfab�ticamente la columna con Fechas _
              ( es la columna que tiene en el tag el valor FECHA )
          
            With .ListItems
                If (Columna > 0) Then
                    For i = 1 To .Count
                        With .Item(i).ListSubItems(Columna)
                            .Tag = .Text & Chr$(0) & .Tag
                            If IsDate(.Text) Then
                                .Text = Format(CDate(.Text), _
                                                    Formato)
                            Else
                                .Text = ""
                            End If
                        End With
                    Next i
                Else
                    For i = 1 To .Count
                        With .Item(i)
                            .Tag = .Text & Chr$(0) & .Tag
                            If IsDate(.Text) Then
                                .Text = Format(CDate(.Text), _
                                                    Formato)
                            Else
                                .Text = ""
                            End If
                        End With
                    Next i
                End If
            End With
              
            ' Ordena alfab�ticamente
              
            .SortOrder = (.SortOrder + 1) Mod 2
            .SortKey = ColumnHeaderX.Index - 1
            .Sorted = True
              
            With .ListItems
                If (Columna > 0) Then
                    For i = 1 To .Count
                        With .Item(i).ListSubItems(Columna)
                            strData = Split(.Tag, Chr$(0))
                            .Text = strData(0)
                            .Tag = strData(1)
                        End With
                    Next i
                Else
                    For i = 1 To .Count
                        With .Item(i)
                            strData = Split(.Tag, Chr$(0))
                            .Text = strData(0)
                            .Tag = strData(1)
                        End With
                    Next i
                End If
            End With
              
        ' Datos de num�ricos
        '''''''''''''''''''''''''''''''''''''''''''''
        Case "N"
          
            ' Ordena alfab�ticamente la columna con n�meros _
              ( es la columna que tiene en el tag el valor NUMBER )
          
            Formato = String(30, "0") & "." & String(30, "0")
                  
            With .ListItems
                If (Columna > 0) Then
                    For i = 1 To .Count
                        With .Item(i).ListSubItems(Columna)
                            .Tag = .Text & Chr$(0) & .Tag
                            If IsNumeric(.Text) Then
                                If CDbl(.Text) >= 0 Then
                                    .Text = Format(CDbl(.Text), _
                                        Formato)
                                Else
                                    .Text = "&" & invNumeros( _
                                        Format(0 - CDbl(.Text), _
                                        Formato))
                                End If
                            Else
                                .Text = ""
                            End If
                        End With
                    Next i
                Else
                    For i = 1 To .Count
                        With .Item(i)
                            .Tag = .Text & Chr$(0) & .Tag
                            If IsNumeric(.Text) Then
                                If CDbl(.Text) >= 0 Then
                                    .Text = Format(CDbl(.Text), _
                                        Formato)
                                Else
                                    .Text = "&" & invNumeros( _
                                        Format(0 - CDbl(.Text), _
                                        Formato))
                                End If
                            Else
                                .Text = ""
                            End If
                        End With
                    Next i
                End If
            End With
              
            ' Ordena alfab�ticamente
              
            .SortOrder = (.SortOrder + 1) Mod 2
            .SortKey = ColumnHeaderX.Index - 1
            .Sorted = True
              
            With .ListItems
                If (Columna > 0) Then
                    For i = 1 To .Count
                        With .Item(i).ListSubItems(Columna)
                            strData = Split(.Tag, Chr$(0))
                            .Text = strData(0)
                            .Tag = strData(1)
                        End With
                    Next i
                Else
                    For i = 1 To .Count
                        With .Item(i)
                            strData = Split(.Tag, Chr$(0))
                            .Text = strData(0)
                            .Tag = strData(1)
                        End With
                    Next i
                End If
            End With
          
        Case Else
                      
            .SortOrder = (.SortOrder + 1) Mod 2
            .SortKey = ColumnHeaderX.Index - 1
            .Sorted = True
              
        End Select
      
    End With
      
    Call SendMessage(elFormulario.hWnd, WM_SETREDRAW, 1&, 0&)
    listviewx.Refresh

End Function
'invNumeros necesasios para ordenar listview asc o desc
Public Function invNumeros(ByVal Number As String) As String
    Static i As Integer
    For i = 1 To Len(Number)
        Select Case Mid$(Number, i, 1)
        Case "-": Mid$(Number, i, 1) = " "
        Case "0": Mid$(Number, i, 1) = "9"
        Case "1": Mid$(Number, i, 1) = "8"
        Case "2": Mid$(Number, i, 1) = "7"
        Case "3": Mid$(Number, i, 1) = "6"
        Case "4": Mid$(Number, i, 1) = "5"
        Case "5": Mid$(Number, i, 1) = "4"
        Case "6": Mid$(Number, i, 1) = "3"
        Case "7": Mid$(Number, i, 1) = "2"
        Case "8": Mid$(Number, i, 1) = "1"
        Case "9": Mid$(Number, i, 1) = "0"
        End Select
    Next
    invNumeros = Number
End Function


Public Function AutoIncremento(Accion As String, CodDoc As Integer, Optional ByVal ElNumero, Optional ByVal Sucursal) As Long
    Dim aSql As String, aRs As Recordset, Ip_autoID As Integer
    
    Dim Ip_Suc As Variant
    Ip_Suc = 0
    If Sucursal <> Empty Then
        Ip_Suc = Val(Sucursal)
    End If
        
    If Ip_Suc = 0 Then
        aSql = "SELECT tau_id,tau_autoincremento numerito " & _
               "FROM sis_autoincrementos " & _
               "WHERE rut_emp='" & SP_Rut_Activo & "' AND doc_id=" & CodDoc
    Else
        aSql = "SELECT tau_id,tau_autoincremento numerito " & _
               "FROM sis_autoincrementos " & _
               "WHERE sue_id=" & Sucursal & " AND  rut_emp='" & SP_Rut_Activo & "' AND doc_id=" & CodDoc
    
    End If
    Consulta aRs, aSql
    
    If Accion = "GUARDA" Then
        If aRs.RecordCount > 0 Then
            Ip_autoID = aRs!tau_id
            If Ip_Suc = 0 Then
                aSql = "UPDATE sis_autoincrementos SET tau_autoincremento=" & ElNumero & " " & _
                       "WHERE tau_id=" & Ip_autoID
            Else
                'con sucursal
                aSql = "UPDATE sis_autoincrementos SET tau_autoincremento=" & ElNumero & " " & _
                       "WHERE tau_id=" & Ip_autoID & " AND sue_id=" & Ip_Suc
            
            End If
        Else
            aSql = "INSERT INTO sis_autoincrementos (doc_id,tau_autoincremento,rut_emp,sue_id) " & _
                   "VALUES(" & CodDoc & "," & ElNumero & ",'" & SP_Rut_Activo & "'," & Ip_Suc & ")"
        End If
        cn.Execute aSql
        AutoIncremento = 0
    Else 'ACCION DE LECTURA SOLAMENTE
        If aRs.RecordCount > 0 Then
            AutoIncremento = aRs!numerito + 1
        Else
            AutoIncremento = 1
        End If
    End If
End Function

Public Sub Coloca(H As Variant, V As Variant, Tx As String, Ng As Boolean, Tm As Integer)
    'Coloca impresion con todos los parametros
    '5 Febrero 2012
    Printer.CurrentX = H
    Printer.CurrentY = V
    Printer.FontSize = Tm
    Printer.FontBold = Ng
    Printer.Print Tx
End Sub
Public Function PrimerDiaMes(DtFecha As Date) As Date
   PrimerDiaMes = DateSerial(Year(DtFecha), Month(DtFecha) + 0, 1)
End Function

Public Function UltimoDiaMes(DtFecha As Date) As Date
   UltimoDiaMes = DateSerial(Year(DtFecha), Month(DtFecha) + 1, 0)
End Function
Private Function EjecucionIDE() As Boolean

   On Error GoTo errHandler

   ' Ocasionar un error (los m�todos del objeto Debug solo funcionan
   ' en tiempo de dise�o
   Debug.Print 1 / 0

   ' Si no hubo error, el programa se est� ejecutando a partir del ejecutable
   EjecucionIDE = False
   Exit Function

errHandler:
   ' Si hubo error, estamos en modo de dise�o
   EjecucionIDE = True
   Err.Clear
End Function
Public Function SaldoDocumento(IdDoc As String, CliePro As String) As Long
    Dim Sp_Ql As String, Rs_Saldo As Recordset
    SaldoDocumento = 0
    If IdDoc = 0 Then
        '
    Else
        If CliePro = "PRO" Then
            Sp_Ql = "SELECT total -(IFNULL((SELECT SUM(t.ctd_monto) " & _
                                            "FROM cta_abono_documentos t,cta_abonos a " & _
                                            "WHERE t.abo_id = a.abo_id AND a.abo_cli_pro = 'PRO' AND t.id = v.id),0) " & _
                        " + IFNULL((SELECT SUM(ctd_monto) " & _
                                    "FROM cta_abonos z " & _
                                    "INNER JOIN cta_abono_documentos y ON z.abo_id=y.abo_id " & _
                                    "WHERE z.rut_emp = '" & SP_Rut_Activo & "' AND y.rut_emp = '" & SP_Rut_Activo & "' AND y.id IN(" & _
                                            "SELECT id " & _
                                            "FROM com_doc_compra w " & _
                                            "WHERE w.id_ref = v.id)),0)) saldo " & _
                    "FROM com_doc_compra v,maestro_proveedores c " & _
                    "WHERE v.id IN(" & IdDoc & ") AND v.rut_emp = '" & SP_Rut_Activo & "' AND id_ref = 0 AND v.doc_id_factura = 0 AND v.Rut = c.rut_proveedor"
            
            Consulta Rs_Saldo, Sp_Ql
            If Rs_Saldo.RecordCount > 0 Then
                Rs_Saldo.MoveFirst
                Do While Not Rs_Saldo.EOF
                    SaldoDocumento = SaldoDocumento + Rs_Saldo!saldo
                    Rs_Saldo.MoveNext
                Loop
            End If
        Else
            Sp_Ql = "SELECT bruto -(IFNULL((SELECT SUM(t.ctd_monto) " & _
                                            "FROM cta_abono_documentos t,cta_abonos a " & _
                                            "WHERE t.abo_id = a.abo_id AND a.abo_cli_pro = 'CLI' AND t.id = v.id),0) " & _
                        " + IFNULL((SELECT SUM(ctd_monto) " & _
                                    "FROM cta_abonos z " & _
                                    "INNER JOIN cta_abono_documentos y ON z.abo_id=y.abo_id " & _
                                    "WHERE z.rut_emp = '" & SP_Rut_Activo & "' AND y.rut_emp = '" & SP_Rut_Activo & "' AND y.id IN(" & _
                                            "SELECT id " & _
                                            "FROM ven_doc_venta w " & _
                                            "WHERE w.id_ref = v.id)),0)) saldo " & _
                    "FROM ven_doc_venta v " & _
                    "WHERE v.id =" & IdDoc & " AND v.rut_emp = '" & SP_Rut_Activo & "'"
            
            Consulta Rs_Saldo, Sp_Ql
            If Rs_Saldo.RecordCount > 0 Then SaldoDocumento = Rs_Saldo!saldo
        
        
        End If
    End If
End Function
Public Function SaldoDocumentoOC(IdDoc As String, CliePro As String) As Long
    Dim Sp_Ql As String, Rs_Saldo As Recordset
    SaldoDocumentoOC = 0
    If IdDoc = 0 Then
        '
    Else
        If CliePro = "PRO" Then
            Sp_Ql = "SELECT IFNULL((SELECT SUM(t.ctd_monto) " & _
                                            "FROM cta_abono_documentos t,cta_abonos a " & _
                                            "WHERE t.abo_id = a.abo_id AND a.abo_cli_pro = 'PRO' AND t.id = v.id),0) " & _
                        " + IFNULL((SELECT SUM(ctd_monto) " & _
                                    "FROM cta_abonos z " & _
                                    "INNER JOIN cta_abono_documentos y ON z.abo_id=y.abo_id " & _
                                    "WHERE z.rut_emp = '" & SP_Rut_Activo & "' AND y.rut_emp = '" & SP_Rut_Activo & "' AND y.id IN(" & _
                                            "SELECT id " & _
                                            "FROM com_doc_compra w " & _
                                            "WHERE w.id_ref = v.id)),0) saldo " & _
                    "FROM com_doc_compra v,maestro_proveedores c " & _
                    "WHERE v.id IN(" & IdDoc & ") AND v.rut_emp = '" & SP_Rut_Activo & "' AND id_ref = 0 AND v.doc_id_factura = 0 AND v.Rut = c.rut_proveedor"
            
            Consulta Rs_Saldo, Sp_Ql
            If Rs_Saldo.RecordCount > 0 Then
                Rs_Saldo.MoveFirst
                Do While Not Rs_Saldo.EOF
                    SaldoDocumentoOC = SaldoDocumentoOC + Rs_Saldo!saldo
                    Rs_Saldo.MoveNext
                Loop
            End If
        Else
            Sp_Ql = "SELECT bruto -(IFNULL((SELECT SUM(t.ctd_monto) " & _
                                            "FROM cta_abono_documentos t,cta_abonos a " & _
                                            "WHERE t.abo_id = a.abo_id AND a.abo_cli_pro = 'CLI' AND t.id = v.id),0) " & _
                        " + IFNULL((SELECT SUM(ctd_monto) " & _
                                    "FROM cta_abonos z " & _
                                    "INNER JOIN cta_abono_documentos y ON z.abo_id=y.abo_id " & _
                                    "WHERE z.rut_emp = '" & SP_Rut_Activo & "' AND y.rut_emp = '" & SP_Rut_Activo & "' AND y.id IN(" & _
                                            "SELECT id " & _
                                            "FROM ven_doc_venta w " & _
                                            "WHERE w.id_ref = v.id)),0)) saldo " & _
                    "FROM ven_doc_venta v " & _
                    "WHERE v.id =" & IdDoc & " AND v.rut_emp = '" & SP_Rut_Activo & "'"
            
            Consulta Rs_Saldo, Sp_Ql
            If Rs_Saldo.RecordCount > 0 Then SaldoDocumentoOC = Rs_Saldo!saldo
        
        
        End If
    End If
End Function

Public Sub EliminaAbonosDeNC(CliPro As String, IdUniko As Long)
    Dim Lp_AboBorrar As Long
    Sql = "SELECT abo_id " & _
            "FROM cta_abonos " & _
            "WHERE abo_cli_pro='" & CliPro & "' AND rut_emp='" & SP_Rut_Activo & "' AND  id_ref=" & IdUniko
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        Lp_AboBorrar = RsTmp!abo_id
        cn.Execute "DELETE FROM cta_abonos " & _
                        "WHERE abo_id=" & Lp_AboBorrar
        cn.Execute "DELETE FROM cta_abono_documentos " & _
                        "WHERE abo_id=" & Lp_AboBorrar
        cn.Execute "DELETE FROM abo_tipos_de_pagos " & _
                        "WHERE abo_id=" & Lp_AboBorrar
    End If
    cn.Execute "DELETE FROM cta_pozo " & _
                        "WHERE rut_emp='" & SP_Rut_Activo & "' AND pzo_cli_pro='" & CliPro & "' AND id_ref=" & IdUniko
End Sub
Public Sub LLenaYears(Combo As ComboBox, Desde As Integer)
    For Z = Year(Date) To Desde Step -1
        Combo.AddItem Z
        Combo.ItemData(Combo.ListCount - 1) = Z
    Next
    Busca_Id_Combo Combo, Val(IG_Ano_Contable)
End Sub

Public Function ConsultaSaldoCliente(RutCliente As String) As Long
    '22 Agosto 2015
    'Consultar saldo cliente
    Dim Rp_ConsultaSaldo As Recordset
    Sql = "SELECT SUM(bruto -IFNULL((" & _
                "SELECT SUM(t.ctd_monto) " & _
                "FROM cta_abono_documentos t,cta_abonos a " & _
                "WHERE t.rut_emp = '" & SP_Rut_Activo & "' " & _
                "AND t.abo_id = a.abo_id " & _
                "AND a.abo_cli_pro = 'CLI' " & _
                "AND t.id = v.id),0)) saldo " & _
            "FROM ven_doc_venta v,maestro_clientes c,sis_documentos d,par_sucursales s " & _
            "WHERE ven_nc_utilizada = 'NO' /*AND doc_informa_venta='SI'*/ AND doc_nota_de_venta = 'NO' AND v.rut_emp = '" & SP_Rut_Activo & "' AND v.doc_id_factura = 0 " & _
                "AND v.suc_id = s.suc_id AND v.rut_cliente = c.rut_cliente AND v.doc_id = d.doc_id AND v.rut_cliente = '" & RutCliente & "' " & _
            "HAVING saldo > 0 " & _
            "ORDER BY id "
      Consulta Rp_ConsultaSaldo, Sql
      If Rp_ConsultaSaldo.RecordCount > 0 Then
            ConsultaSaldoCliente = Rp_ConsultaSaldo!saldo
      Else
            ConsultaSaldoCliente = 0
      End If
       
End Function
Public Function ConsultaSaldoProveedor(RutPro As String) As Long
    '22 Agosto 2015
    'Consultar saldo cliente
    Dim Rp_ConsultaSaldo As Recordset
    Sql = "SELECT SUM(total -IFNULL((" & _
                "SELECT SUM(t.ctd_monto) " & _
                "FROM cta_abono_documentos t,cta_abonos a " & _
                "WHERE t.rut_emp = '" & SP_Rut_Activo & "' " & _
                "AND t.abo_id = a.abo_id " & _
                "AND a.abo_cli_pro = 'PRO' " & _
                "AND t.id = v.id),0)) saldo " & _
            "FROM com_doc_compra v,maestro_proveedores c,sis_documentos d " & _
            "WHERE v.rut_emp = '" & SP_Rut_Activo & "' AND v.rut_proveedor = c.rut_proveedor AND v.doc_id = d.doc_id AND v.rut_proveedor = '" & RutPro & "' " & _
            "HAVING saldo > 0 " & _
            "ORDER BY id "
      Consulta Rp_ConsultaSaldo, Sql
      If Rp_ConsultaSaldo.RecordCount > 0 Then
            ConsultaSaldoProveedor = Rp_ConsultaSaldo!saldo
      Else
            ConsultaSaldoProveedor = 0
      End If
       
End Function

Public Function ConsultaSaldoClienteConFecha(RutCliente As String, Fecha As String) As Long
    '22 Agosto 2015
    'Consultar saldo cliente, DESDE FECHA X
    Dim Rp_ConsultaSaldo As Recordset
    Sql = "SELECT SUM(bruto -IFNULL((" & _
                "SELECT SUM(t.ctd_monto) " & _
                "FROM cta_abono_documentos t,cta_abonos a " & _
                "WHERE abo_fecha<'" & Fecha & "' AND t.rut_emp = '" & SP_Rut_Activo & "' " & _
                "AND t.abo_id = a.abo_id " & _
                "AND a.abo_cli_pro = 'CLI' " & _
                "AND t.id = v.id),0)) saldo " & _
            "FROM ven_doc_venta v,maestro_clientes c,sis_documentos d,par_sucursales s " & _
            "WHERE v.fecha<'" & Fecha & "' AND ven_nc_utilizada = 'NO' /*AND doc_informa_venta='SI'*/ AND doc_nota_de_venta = 'NO' AND v.rut_emp = '" & SP_Rut_Activo & "' AND v.doc_id_factura = 0 " & _
                "AND v.suc_id = s.suc_id AND v.rut_cliente = c.rut_cliente AND v.doc_id = d.doc_id AND v.rut_cliente = '" & RutCliente & "' " & _
            "/* HAVING saldo > 0 */ " & _
            "ORDER BY id "
      Consulta Rp_ConsultaSaldo, Sql
      If Rp_ConsultaSaldo.RecordCount > 0 Then
            ConsultaSaldoClienteConFecha = Val("" & Rp_ConsultaSaldo!saldo)
      Else
            ConsultaSaldoClienteConFecha = 0
      End If
      'Buscaremos si el rut tiene saldo a favor y lo mostraremos
        '10 Nov 2012
        Sql = "SELECT IFNULL(SUM(pzo_abono)- SUM(pzo_cargo),0) saldo " & _
                "FROM cta_pozo " & _
                "WHERE pzo_fecha<'" & Fecha & "' AND rut_emp = '" & SP_Rut_Activo & "' AND pzo_rut = '" & RutCliente & "' AND pzo_cli_pro = 'CLI' "
                
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
            If (RsTmp!saldo) <> 0 Then
                ConsultaSaldoClienteConFecha = ConsultaSaldoClienteConFecha + RsTmp!saldo
            End If
        End If
End Function

Public Function ConsultarDTESII(Tipo As String, Nro As String) As Boolean

    Dim Bp_Cedible As Boolean
    Bp_Cedible = False
    'Modulo: ver pdfs, ya sea local o en la nube
    'fecha : 11 octubre 2014
    Dim resp As Boolean
    
        'Consulta electronica
    Dim obj As DTECloud.Integracion
    Set obj = New DTECloud.Integracion
        
    obj.Productivo = True
    'obj.UrlServicioFacturacion = "http://wsdtedesa.dyndns.biz/WSDTE/Service.asmx"
    obj.UrlServicioFacturacion = SG_Url_Factura_Electronica ' "http://wsdte.dyndns.biz/WSDTE/Service.asmx?WSDL"
    obj.HabilitarDescarga = True
    obj.CarpetaDestino = "C:\FACTURAELECTRONICA\PDF\" & Replace(SP_Rut_Activo, ".", "")
    
    obj.Password = "1234"
    On Error GoTo ErrorConexion
    resp = obj.DescargarPDF(Replace(SP_Rut_Activo, ".", ""), Replace(SP_Rut_Activo, ".", ""), Trim(Tipo), Trim(Nro), "E", Bp_Cedible, True)
        
    Archivo = obj.URLPDF
    If Mid(Archivo, 1, 12) = "El documento" Then
        ConsultarDTESII = False
     '   MsgBox "Documento no se encontr� en SII..."
    Else
         ConsultarDTESII = True
    End If
    
    
    Exit Function

ErrorConexion:
    MsgBox "Error:" & Err.Description & vbNewLine & "Nro:" & Err.Number & " " & Err.Source
End Function


Public Sub ActualizaStockVenta(NroDoc As Long, IdDoc As Integer, NombreDoc As String, RutASV As String, NombreASV As String)
    ' 28 Mayo 2016
    ' Actualiza stock con venta, esto para ser usado en POS Epson, POS Bixolon, Pos Caja (independiente de tipo de documento de venta)
    
        Dim Dp_Promedio As Double
        Dim Rp_Stock As Recordset
        Sql = "SELECT d.codigo,unidades,pro_inventariable,d.doc_id,v.id " & _
                "FROM ven_detalle d " & _
                "JOIN ven_doc_venta v ON d.doc_id=v.doc_id AND d.no_documento=v.no_documento " & _
                "JOIN maestro_productos p ON p.codigo=d.codigo " & _
                "WHERE  d.rut_emp='" & SP_Rut_Activo & "' AND p.rut_emp='" & SP_Rut_Activo & "' AND d.no_documento=" & NroDoc & " AND d.doc_id=" & IdDoc
        Consulta Rp_Stock, Sql
       If Rp_Stock.RecordCount > 0 Then
            Rp_Stock.MoveFirst
            Do While Not Rp_Stock.EOF
                If Rp_Stock!pro_inventariable = "SI" Then 'inventariable
                              Dp_Promedio = 0 '
                              Dp_Promedio = CostoAVG(Str(Rp_Stock!Codigo), 1) '
                               Sql = "SELECT pro_precio_neto promedio " & _
                                        "FROM inv_kardex k " & _
                                        "WHERE pro_codigo='" & Rp_Stock!Codigo & "' AND rut_emp='" & SP_Rut_Activo & "' AND bod_id=" & 1 & " " & _
                                        "ORDER BY kar_id DESC " & _
                                         "LIMIT 1 "
                              Consulta RsTmp, Sql
                              If RsTmp.RecordCount > 0 Then Dp_Promedio = RsTmp!promedio '
                              Kardex Fql(Date), "SALIDA", _
                             Rp_Stock!doc_id, Val(NroDoc), IG_id_Bodega_Ventas, Rp_Stock!Codigo, _
                              Rp_Stock!Unidades, "VENTA " & NombreDoc & " " & NroDoc, _
                              0, 0, RutASV, NombreASV, , , Rp_Stock!doc_id, , , , Rp_Stock!ID
                End If
                Rp_Stock.MoveNext
          Loop '
       End If
End Sub

Public Function Super_Impresora(ByVal NamePrinter As String) As Boolean
On Error GoTo errSub
      'Variable de referencia
  
    Dim obj_Impresora As Object
    'Creamos la referencia
    Set obj_Impresora = CreateObject("WScript.Network")
        obj_Impresora.setdefaultprinter NamePrinter
    Set obj_Impresora = Nothing
  
        'La funci�n devuelve true y se cambi� con �xito
  
       La_Establecer_Impresora = True
    Exit Function
'Error al cambiar la impresora
errSub:
If Err.Number = 0 Then Exit Function
   La_Establecer_Impresora = False
   MsgBox "error: " & Err.Number & Chr(13) & "Description: " & Err.Description
   On Error GoTo 0
End Function
Public Function QuitarAcentos(Palabra As String) As String
    Palabra = Replace(Palabra, "�", "N")
    Palabra = Replace(Palabra, "�", "A")
    Palabra = Replace(Palabra, "�", "E")
    Palabra = Replace(Palabra, "�", "I")
    Palabra = Replace(Palabra, "�", "O")
    Palabra = Replace(Palabra, "�", "U")
    Palabra = Replace(Palabra, "&", " Y ")
    Palabra = Replace(Palabra, "%", " PORC. ")
    Palabra = Replace(Palabra, "�", "")
    Palabra = Replace(Palabra, "�", "")
    Palabra = Replace(Palabra, "�", "")
    Palabra = Replace(Palabra, "'", ".")
    Palabra = Replace(Palabra, "�", "")
    Palabra = Replace(Palabra, "~", "")
    Palabra = Replace(Palabra, "`", "")
    Palabra = Replace(Palabra, "^", "")
    Palabra = Replace(Palabra, "|", "")
    Palabra = Replace(Palabra, "�", "")
    Palabra = Replace(Palabra, "�", "")
    Palabra = Replace(Palabra, "�", "")
    Palabra = Replace(Palabra, "�", "")
    Palabra = Replace(Palabra, "�", "")
    Palabra = Replace(Palabra, "�", "")
    Palabra = Replace(Palabra, "�", "")
    Palabra = Replace(Palabra, "�", "")
    Palabra = Replace(Palabra, "�", "")
    Palabra = Replace(Palabra, "�", "")
    Palabra = Replace(Palabra, "�", "")
    Palabra = Replace(Palabra, "�", "")
    Palabra = Replace(Palabra, "�", "")
    Palabra = Replace(Palabra, "�", "")
    Palabra = Replace(Palabra, "�", "")
    Palabra = Replace(Palabra, "'", "")
    Palabra = Replace(Palabra, "�", "")
   
    
    
    QuitarAcentos = Palabra
End Function

'Private Function EnviaLineasC(Negrita As Boolean)
'        Dim avance As Double
'        avance = 1.5
'        Printer.FontSize = 10
'        Printer.FontName = "Arial Narrow"
'
'        Coloca Cx, Cy, P_Fecha, Negrita, 10
'        Coloca Cx + 1.7, Cy, P_Documento, Negrita, 10
'        Coloca Cx + 7, Cy, P_Numero, Negrita, 10
'        Coloca Cx + 8.6, Cy, P_Vence, Negrita, 10
'        Coloca (Cx + 12) - Printer.TextWidth(P_Venta), Cy, P_Venta, Negrita, 10
'        Coloca (Cx + 14.2) - Printer.TextWidth(P_Pago), Cy, P_Pago, Negrita, 10
'        Coloca (Cx + 17) - Printer.TextWidth(P_Saldo), Cy, P_Saldo, Negrita, 10
'
'
'End Function
