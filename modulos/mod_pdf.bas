Attribute VB_Name = "mod_pdf"
Private Declare Function ShellExecute Lib "shell32.dll" Alias "ShellExecuteA" (ByVal hWnd As Long, ByVal lpOperation As String, ByVal lpFile As String, ByVal lpParameters As String, ByVal lpDirectory As String, ByVal nShowCmd As Long) As Long '
Public Sub ComprobantePDF(Obs_Comprobante As String, LvCheques As ListView, Abonos_Comprobante, LvPagos As ListView, LvDetalle As ListView, Fecha_Comprobante As String, Nro_Comprobante As Long, Nom_Comprobante As String, Rut_Comprobante As String, CliPro As String)
    Dim Cx As Double, Cy As Double, Sp_Fecha As String, Ip_Mes As Integer, Dp_S As Double, Sp_Letras As String
    Dim Sp_Neto As String * 12, Sp_IVA As String * 12
    Dim Sp_Nro_Comprobante As String, Sp_Imprime_Cheque As String * 2
    Dim Sp_Empresa As String
    Dim Sp_Giro As String
    Dim Sp_Direccion As String
    Dim Sp_Nombre As String * 45
    Dim Sp_Rut As String * 15
    Dim Sp_Concepto As String
    Dim Sp_Nro_Doc As String * 12
    Dim Sp_Documento As String * 35
    Dim Sp_Valor As String * 15
    Dim Sp_Saldo As String * 15
    Dim Sp_Fpago As String * 24
    Dim Sp_Total As String * 12
    Dim Sp_Banco As String * 15
    Dim Sp_NroCheque As String * 10
    Dim Sp_FechaCheque As String * 10
    Dim Sp_ValorCheque As String * 15
    Dim Sp_SumaCheque As String * 15
    Dim Sp_Observacion As String * 80
    Dim Sp_PlazaFechas As String * 50
    Dim Sp_PosicionValor As String
    Dim Sp_PosicionFechaPlaza As String
    Dim Sp_PosicionBenefeciario As String
    Dim Sp_PosicionMontoLetras As String
    Dim Px As Double, Py As Double, pI As Double
    Dim Sm_Mpagos As String
    Sp_Imprime_Cheque = "NO"
    
    Dim pdf As PdfComLib.PdfDoc
    Set pdf = New PdfDoc
    pdf.AddPage (1)
    pdf.SetMargins 10, 10, 20
    pdf.SetTitle "COMPROBANTE DE PAGO " & Nro_Comprobante & " " & Nom_Comprobante
    pdf.AliasNbPages "{nb}"
    pdf.FOOTER True
    pdf.SetY -15
    pdf.SetFont "Arial", "I", 8
    pdf.SetTextColor 128, 0, 128
    pdf.Cell 0, 10, "Pagina {pg}/{nb}", 0, 0, PDF_ALIGN_CENTER, 0, ""
    pdf.FOOTER False
    
    'For F = 1 To dialogo.Copies
    On Error GoTo ErroComp
    Sql = "SELECT giro,direccion,ciudad,emp_emite_cheque_en_comprobante_pago cheque " & _
         "FROM sis_empresas " & _
         "WHERE rut='" & SP_Rut_Activo & "'"
    Consulta RsTmp2, Sql
    If RsTmp2.RecordCount > 0 Then
        Sp_Giro = RsTmp2!giro
        Sp_Direccion = RsTmp2!direccion & " - " & RsTmp2!ciudad
        Sp_Imprime_Cheque = RsTmp2!cheque
    End If
    Py = 30
    Px = 20
    pI = 3
    ColocaPdf pdf, Px, Py, Principal.SkEmpresa, 12, "B"  'NOMBRE EMPRESA
    ColocaPdf pdf, Px, Py + 5, "RUT:" & SP_Rut_Activo, 10, "B"
    ColocaPdf pdf, Px, Py + 10, "GIRO:" & Sp_Giro, 10, ""
    ColocaPdf pdf, Px, Py + 15, "DIRECCION:" & Sp_Direccion, 10, ""
    ColocaPdf pdf, Px + 100, Py + 5, "COMPROBANTE DE PAGO " & Nro_Comprobante, 10, "BU"
    pdf.RECT Px - 4, Py - 5, 175, 22, PDF_STYLE_DRAW
    Py = Py + 25
    
    'pdf.RECT 70, 35, 65, 7, PDF_STYLE_DRAW
    ColocaPdf pdf, Px, Py, "Recib� de:", 10, "B"
    ColocaPdf pdf, Px, Py + 5, "NOMBRE:" & Nom_Comprobante, 9, ""
    ColocaPdf pdf, Px, Py + 10, "RUT   :" & Rut_Comprobante, 9, "B"
    ColocaPdf pdf, Px + 120, Py, "Fecha Pago " & Fecha_Comprobante, 9, ""
    pdf.RECT Px - 4, Py - 5, 175, 16, PDF_STYLE_DRAW
    Py = Py + 17
    desdey = Py - 4
    ColocaPdf pdf, Px + 15, Py, "CONCEPTO", 10, "B"
    ColocaPdf pdf, Px + 130, Py, "Valor                     Saldo", 9, ""
    Py = Py + 3
    ColocaPdf pdf, Px, Py, "Nro                 Documento ", 9, ""
    Py = Py + 4
    For i = 1 To LvDetalle.ListItems.Count
            Sp_Nro_Doc = LvDetalle.ListItems(i).SubItems(1)
            Sp_Documento = LvDetalle.ListItems(i).SubItems(2)
            RSet Sp_Valor = LvDetalle.ListItems(i).SubItems(11)
            RSet Sp_Saldo = NumFormat(CDbl(LvDetalle.ListItems(i).SubItems(8)) - (CDbl(LvDetalle.ListItems(i).SubItems(9)) + CDbl(LvDetalle.ListItems(i).SubItems(11))))
            ColocaPdf pdf, Px, Py, Sp_Nro_Doc, 9, ""
            ColocaPdf pdf, Px + 20, Py, Sp_Documento, 9, ""
            ColocaPdf pdf, Px + 120, Py, Sp_Valor, 9, ""
            ColocaPdf pdf, Px + 150, Py, Sp_Saldo, 9, ""
            Py = PdfNewPag(pdf, Int(Py))
            Py = PdfNewPag(pdf, Int(Py))
            Py = PdfNewPag(pdf, Int(Py))
    Next
    For F = 1 To 6
        Py = PdfNewPag(pdf, Int(Py))
    Next
    ColocaPdf pdf, Px + 15, Py, "FORMAS DE PAGO", 10, "B"
    For F = 1 To 4
        Py = PdfNewPag(pdf, Int(Py))
    Next
    For i = 1 To LvPagos.ListItems.Count
        If CDbl(LvPagos.ListItems(i).SubItems(2)) > 0 Then
            Sp_VPagos = LvPagos.ListItems(i).SubItems(2)
            Sm_Mpagos = LvPagos.ListItems(i).SubItems(1) & _
            IIf(Val(LvPagos.ListItems(i).SubItems(5)) > 0, " NRO " & LvPagos.ListItems(i).SubItems(5) & IIf(Len(LvPagos.ListItems(i).SubItems(6)) > 0, " " & LvPagos.ListItems(i).SubItems(6), ""), "") & _
            " $"
            ColocaPdf pdf, Px, Py, Sm_Mpagos, 9, ""
            ColocaPdf pdf, Px + 60, Py, NumFormat(Str(Sp_VPagos)), 9, ""
            For F = 1 To 4
                Py = PdfNewPag(pdf, Int(Py))
            Next
        End If
    Next
   ' Printer.CurrentX = Cx
    Sm_Mpagos = "-------------------------------------------------------"
    ColocaPdf pdf, Px, Py, Sm_Mpagos, 9, ""
    For F = 1 To 4
        Py = PdfNewPag(pdf, Int(Py))
    Next
    Sm_Mpagos = "TOTAL PAGO   :$"
    ColocaPdf pdf, Px, Py, Sm_Mpagos, 9, "B"
    ColocaPdf pdf, Px + 60, Py, NumFormat(Str(Abonos_Comprobante)), 9, "B"

    If LvCheques.ListItems.Count > 0 Then
            For F = 1 To 6
                Py = PdfNewPag(pdf, Int(Py))
            Next
            ColocaPdf pdf, Px + 15, Py, "Detalle de cheques", 9, "B"
            For F = 1 To 4
                Py = PdfNewPag(pdf, Int(Py))
            Next
            Sp_FechaCheque = "Fecha"
            Sp_NroCheque = "Nro"
            Sp_ValorCheque = "Valor"
            Sp_Banco = "Banco"
            ColocaPdf pdf, Px, Py, Sp_Banco, 9, ""
            ColocaPdf pdf, Px + 40, Py, Sp_NroCheque, 9, ""
            ColocaPdf pdf, Px + 60, Py, Sp_FechaCheque, 9, ""
            ColocaPdf pdf, Px + 80, Py, Sp_ValorCheque, 9, ""
            For F = 1 To 3
                Py = PdfNewPag(pdf, Int(Py))
            Next
            tcheques = 0
            For i = 1 To LvCheques.ListItems.Count
                If p = i Then Printer.FontBold = True
                Printer.CurrentX = Cx
                Sp_FechaCheque = LvCheques.ListItems(i)
                Sp_NroCheque = LvCheques.ListItems(i).SubItems(1)
                Sp_ValorCheque = LvCheques.ListItems(i).SubItems(2)
                Sp_Banco = LvCheques.ListItems(i).SubItems(6)
                ColocaPdf pdf, Px, Py, Sp_Banco, 9, ""
                ColocaPdf pdf, Px + 40, Py, Sp_NroCheque, 9, ""
                ColocaPdf pdf, Px + 60, Py, Sp_FechaCheque, 9, ""
                ColocaPdf pdf, Px + 80, Py, Sp_ValorCheque, 9, ""
                For F = 1 To 4
                    Py = PdfNewPag(pdf, Int(Py))
                Next
                
               ' Printer.Print Sp_Banco & " " & Sp_NroCheque & " " & Sp_FechaCheque & "       " & Sp_ValorCheque
                'Printer.CurrentY = Printer.CurrentY + Dp_S - 0.2
                tcheques = tcheques + CDbl(LvCheques.ListItems(i).SubItems(2))
                'Printer.FontBold = False
            Next
            Sp_FechaCheque = ""
            Sp_NroCheque = ""
            Sp_ValorCheque = NumFormat(tcheques)
            Sp_Banco = ""
            ColocaPdf pdf, Px, Py, Sp_Banco, 9, "B"
            ColocaPdf pdf, Px + 40, Py, Sp_NroCheque, 9, "B"
            ColocaPdf pdf, Px + 60, Py, Sp_FechaCheque, 9, "B"
            ColocaPdf pdf, Px + 80, Py, Sp_ValorCheque, 9, "B"
            For F = 1 To 3
                Py = PdfNewPag(pdf, Int(Py))
            Next
    End If
    hastay = (Py - desdey) + 2
    pdf.RECT Px - 4, desdey, 175, hastay, PDF_STYLE_DRAW
    pdf.RECT Px - 4, desdey, 120, hastay, PDF_STYLE_DRAW
    pdf.RECT Px - 4, desdey, 150, hastay, PDF_STYLE_DRAW
    hastay = desdey + hastay
    'Observaciones
    For F = 1 To 6
        Py = PdfNewPag(pdf, Int(Py))
    Next
    ColocaPdf pdf, Px, Py, "Observaciones", 10, "B"
    For F = 1 To 5
        Py = PdfNewPag(pdf, Int(Py))
    Next
    ColocaPdf pdf, Px + 15, Py, Obs_Comprobante, 10, ""
    pdf.RECT Px - 4, hastay, 175, 10, PDF_STYLE_DRAW
    
    'firmas
    For F = 1 To 6
        Py = PdfNewPag(pdf, Int(Py))
    Next
    ColocaPdf pdf, Px, Py, "CONTABILIDAD", 9, "B"
    ColocaPdf pdf, Px + 65, Py, "V� B� CAJA", 9, "B"
    ColocaPdf pdf, Px + 130, Py, "RECIBI CONFORME", 9, "B"
    ColocaPdf pdf, Px + 130, Py + 6, "NOMBRE, RUT Y FIRMA", 9, ""
    pdf.RECT Px - 4, hastay + 10, 175, 17, PDF_STYLE_DRAW
        'genera archivo
    Fp_Archivo = "c:\facturaelectronica\GENPDF\COMPROBANTE-" & Nro_Comprobante & "-" & Nom_Comprobante & ".pdf"
    pdf.SaveAsFile (Fp_Archivo)
    ShellExecute hWnd, "open", Fp_Archivo, "", "", 4
    
    Exit Sub
ErroComp:
    MsgBox "Error :" & Err.Description & vbNewLine & "Nro " & Err.Number & vbNewLine & "Lin:" & Err.Source
End Sub

Public Sub CartolaPDF(Rut_Cartola As String, Nom_Cartola As String, Ciu_Cartola As String, Dir_Cartola As String, Fon_Cartola As String, LvDetalle As ListView, Tot_Cartola As String, Abo_Cartola As String, Sal_Cartola As String, CliPro As String)
    Dim pdf As PdfComLib.PdfDoc
    Dim Px As Double, Py As Double, pI As Double
    Dim TLet As Double
    Dim Sp_Giro As String
    Dim Sp_Direccion As String
    Dim Sp_Imprime_Cheque As String
    Set pdf = New PdfDoc
    pdf.AddPage (1)
    pdf.SetMargins 10, 10, 20
    pdf.SetTitle "CARTOLA CLIENTE "
    pdf.AliasNbPages "{nb}"
    pdf.FOOTER True
    pdf.SetY -15
    pdf.SetFont "Arial", "I", 8
    pdf.SetTextColor 128, 0, 128
    pdf.Cell 0, 10, "Pagina {pg}/{nb}", 0, 0, PDF_ALIGN_CENTER, 0, ""
    pdf.FOOTER False
      On Error GoTo ErroComp
    Sql = "SELECT giro,direccion,ciudad,emp_emite_cheque_en_comprobante_pago cheque " & _
         "FROM sis_empresas " & _
         "WHERE rut='" & SP_Rut_Activo & "'"
    Consulta RsTmp2, Sql
    If RsTmp2.RecordCount > 0 Then
        Sp_Giro = RsTmp2!giro
        Sp_Direccion = RsTmp2!direccion & " - " & RsTmp2!ciudad
        Sp_Imprime_Cheque = RsTmp2!cheque
    End If
    Py = 20
    Px = 20
    pI = 3
    ColocaPdf pdf, Px, Py, Principal.SkEmpresa, 11, "B"  'NOMBRE EMPRESA
    TLet = 9
    ColocaPdf pdf, Px, Py + 5, "RUT", TLet, "B"
    ColocaPdf pdf, Px + 22, Py + 5, ": " & SP_Rut_Activo, TLet, "B"
    ColocaPdf pdf, Px, Py + 10, "GIRO", TLet, ""
    ColocaPdf pdf, Px + 22, Py + 10, ": " & Sp_Giro, TLet, ""
    ColocaPdf pdf, Px, Py + 15, "DIRECCION:", TLet, ""
    ColocaPdf pdf, Px + 22, Py + 15, ": " & Sp_Direccion, TLet, ""
    pdf.RECT Px - 4, Py - 5, 175, 24, PDF_STYLE_DRAW
    If CliPro = "CLIENTES" Then
        ColocaPdf pdf, Px + 43, Py + 26, "C A R T O L A     C L I E N T E", 14, "BU"
    Else
        ColocaPdf pdf, Px + 42, Py + 26, "C A R T O L A     P R O V E E D O R", 14, "BU"
    End If
    pdf.SetFont "Arial Narrow", "I", 8
    Py = Py + 28
    ColocaPdf pdf, Px, Py + 5, "R.U.T.", TLet, ""
    ColocaPdf pdf, Px, Py + 9, "NOMBRE", TLet, "B"   'NOMBRE EMPRESA
    ColocaPdf pdf, Px, Py + 13, "CIUDAD", TLet, ""
    ColocaPdf pdf, Px, Py + 17, "DIRECCION", TLet, ""
    ColocaPdf pdf, Px, Py + 21, "FONO", 10, ""
    ColocaPdf pdf, Px + 22, Py + 5, ":  " & Rut_Cartola, TLet, ""
    ColocaPdf pdf, Px + 22, Py + 9, ":  " & Nom_Cartola, TLet, "B" 'NOMBRE EMPRESA
    ColocaPdf pdf, Px + 22, Py + 13, ":  " & Ciu_Cartola, TLet, ""
    ColocaPdf pdf, Px + 22, Py + 17, ":  " & Dir_Cartola, TLet, ""
    ColocaPdf pdf, Px + 22, Py + 21, ":  " & Fon_Cartola, TLet, ""
    pdf.RECT Px - 4, Py, 175, 24, PDF_STYLE_DRAW
    
    pdf.RECT Px - 4, Py + 25, 175, 4.7, PDF_STYLE_DRAW
    Py = Py + 25
    Px = Px - 3
    
    TLet = 8
    ColocaPdfm pdf, Px, Py, "Fecha", TLet, "B", 0
    ColocaPdfm pdf, Px + 16, Py, "Documento", TLet, "B", 0
    ColocaPdfm pdf, Px + 81, Py, "Numero", TLet, "B", 0
    ColocaPdfm pdf, Px + 102, Py, "Vencimiento", TLet, "B", 0
    ColocaPdfm pdf, Px + 127, Py, "Venta", TLet, "B", 2
    ColocaPdfm pdf, Px + 145, Py, "Abono", TLet, "B", 2
    ColocaPdfm pdf, Px + 163, Py, "Saldo", TLet, "B", 2
    
    Py = Py + 4
    For i = 1 To LvDetalle.ListItems.Count
        ColocaPdfm pdf, Px, Py, LvDetalle.ListItems(i).SubItems(6), TLet, "", 0
        ColocaPdfm pdf, Px + 16, Py, LvDetalle.ListItems(i).SubItems(2), TLet, "", 0
        ColocaPdfm pdf, Px + 81, Py, LvDetalle.ListItems(i).SubItems(1), TLet, "", 0
        ColocaPdfm pdf, Px + 102, Py, LvDetalle.ListItems(i).SubItems(7), TLet, "", 0
        ColocaPdfm pdf, Px + 127, Py, LvDetalle.ListItems(i).SubItems(8), TLet, "", 2
        ColocaPdfm pdf, Px + 145, Py, LvDetalle.ListItems(i).SubItems(9), TLet, "", 2
        ColocaPdfm pdf, Px + 163, Py, LvDetalle.ListItems(i).SubItems(10), TLet, "", 2
        Py = Py + 4
    Next
    Py = Py + 5
    Px = Px + 3
    ColocaPdfm pdf, Px + 76, Py, "TOTALES", TLet, "B", 0
    ColocaPdfm pdf, Px + 124, Py, Tot_Cartola, TLet, "B", 2
    ColocaPdfm pdf, Px + 142, Py, Abo_Cartola, TLet, "B", 2
    ColocaPdfm pdf, Px + 160, Py, Sal_Cartola, TLet, "B", 2
    pdf.RECT Px - 4, Py - 1, 175, 6, PDF_STYLE_DRAW
    
     If SP_Rut_Activo = "12.072.366-9" Then
        'mario acu�a
        Py = Py + 6
        ColocaPdfm pdf, Px + 10, Py, "S�rvase transferir a la cta. cte. ", TLet + 1, "B", 0
        ColocaPdfm pdf, Px + 10, Py + 5, "BancoEstado Nro 62500036830 email: curacavet@gmail.com", TLet + 1, "B", 0
       
        
    
    End If
    
    
    Fp_Archivo = "c:\facturaelectronica\GENPDF\CARTOLA " & Nom_Cartola & ".pdf"
    pdf.SaveAsFile (Fp_Archivo)
    ShellExecute hWnd, "open", Fp_Archivo, "", "", 4
    Exit Sub
ErroComp:
    MsgBox "Error :" & Err.Description & vbNewLine & "Nro " & Err.Number & vbNewLine & "Lin:" & Err.Source
End Sub


Public Function PdfNewPag(ElPdf As PdfDoc, NroLinea As Integer) As Integer
    NroLinea = NroLinea + 1
    If NroLinea > 260 Then
        ElPdf.AddPage 1
        PdfNewPag = 20
        Exit Function
    End If
    PdfNewPag = NroLinea
End Function

