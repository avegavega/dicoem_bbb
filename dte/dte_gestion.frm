VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{28D47522-CF84-11D1-834C-00A0249F0C28}#1.0#0"; "gif89.dll"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form dte_gestion 
   Caption         =   "Gestion de Facturacion Electronica"
   ClientHeight    =   5820
   ClientLeft      =   4215
   ClientTop       =   4140
   ClientWidth     =   13485
   LinkTopic       =   "Form1"
   ScaleHeight     =   5820
   ScaleWidth      =   13485
   Begin VB.Frame FrmLoad 
      Height          =   1680
      Left            =   6915
      TabIndex        =   60
      Top             =   435
      Visible         =   0   'False
      Width           =   2805
      Begin GIF89LibCtl.Gif89a Gif89a1 
         Height          =   1275
         Left            =   210
         OleObjectBlob   =   "dte_gestion.frx":0000
         TabIndex        =   61
         Top             =   285
         Width           =   2445
      End
   End
   Begin VB.TextBox TxtFinal 
      Height          =   285
      Left            =   1380
      TabIndex        =   59
      Text            =   "Text1"
      Top             =   7065
      Width           =   630
   End
   Begin VB.TextBox TxtInicial 
      Height          =   285
      Left            =   570
      TabIndex        =   58
      Text            =   "Text1"
      Top             =   7065
      Width           =   630
   End
   Begin VB.CommandButton Command6 
      Caption         =   "Command6"
      Height          =   390
      Left            =   4260
      TabIndex        =   57
      Top             =   6900
      Width           =   1185
   End
   Begin VB.CommandButton Command5 
      Caption         =   "Command5"
      Height          =   540
      Left            =   2700
      TabIndex        =   56
      Top             =   6435
      Width           =   1155
   End
   Begin VB.CommandButton Command4 
      Caption         =   "Command4"
      Height          =   870
      Left            =   5715
      TabIndex        =   52
      Top             =   6525
      Width           =   2895
   End
   Begin VB.CommandButton Command3 
      Caption         =   "PDF Masivo"
      Height          =   405
      Left            =   510
      TabIndex        =   51
      Top             =   6525
      Width           =   1515
   End
   Begin VB.CommandButton CmdSalir 
      Caption         =   "Retornar"
      Height          =   390
      Left            =   11865
      TabIndex        =   50
      Top             =   5280
      Width           =   1365
   End
   Begin VB.Timer Timer1 
      Interval        =   50
      Left            =   75
      Top             =   90
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   135
      OleObjectBlob   =   "dte_gestion.frx":0086
      Top             =   5310
   End
   Begin TabDlg.SSTab Sst_Dte 
      Height          =   4995
      Left            =   360
      TabIndex        =   0
      Top             =   225
      Width           =   12930
      _ExtentX        =   22807
      _ExtentY        =   8811
      _Version        =   393216
      TabHeight       =   520
      TabCaption(0)   =   "Subir folios"
      TabPicture(0)   =   "dte_gestion.frx":02BA
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "nada"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "Consultar Documentos"
      TabPicture(1)   =   "dte_gestion.frx":02D6
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Frame3"
      Tab(1).Control(1)=   "Frame1(1)"
      Tab(1).Control(2)=   "Frame1(0)"
      Tab(1).ControlCount=   3
      TabCaption(2)   =   "Consultar Libros Contables"
      TabPicture(2)   =   "dte_gestion.frx":02F2
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "Frame1(2)"
      Tab(2).ControlCount=   1
      Begin VB.Frame Frame3 
         Caption         =   "Consultar Folios Disponibles"
         Height          =   3210
         Left            =   -68625
         TabIndex        =   44
         Top             =   870
         Width           =   5010
         Begin ACTIVESKINLibCtl.SkinLabel SkCantDisponibles 
            Height          =   240
            Left            =   270
            OleObjectBlob   =   "dte_gestion.frx":030E
            TabIndex        =   49
            Top             =   2520
            Width           =   2130
         End
         Begin MSComctlLib.ListView LvFDisponible 
            Height          =   1410
            Left            =   315
            TabIndex        =   48
            Top             =   1065
            Width           =   3435
            _ExtentX        =   6059
            _ExtentY        =   2487
            View            =   3
            LabelWrap       =   -1  'True
            HideSelection   =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   2
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Text            =   "Folio"
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Text            =   "Disponible"
               Object.Width           =   2540
            EndProperty
         End
         Begin VB.CommandButton cmdMostrarFolios 
            Caption         =   "Mostrar"
            Height          =   300
            Left            =   3840
            TabIndex        =   47
            Top             =   675
            Width           =   885
         End
         Begin VB.ComboBox CboDocVenta2 
            Height          =   315
            Left            =   330
            Style           =   2  'Dropdown List
            TabIndex        =   46
            Top             =   675
            Width           =   3465
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
            Height          =   195
            Index           =   6
            Left            =   345
            OleObjectBlob   =   "dte_gestion.frx":0398
            TabIndex        =   45
            Top             =   465
            Width           =   1935
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "Documento de Venta"
         Height          =   3210
         Index           =   2
         Left            =   -73935
         TabIndex        =   30
         Top             =   885
         Width           =   5475
         Begin VB.ComboBox CboAnoContable 
            Height          =   315
            ItemData        =   "dte_gestion.frx":0408
            Left            =   3210
            List            =   "dte_gestion.frx":040A
            Style           =   2  'Dropdown List
            TabIndex        =   36
            Top             =   1650
            Width           =   1650
         End
         Begin VB.ComboBox CboMesContable 
            Height          =   315
            ItemData        =   "dte_gestion.frx":040C
            Left            =   510
            List            =   "dte_gestion.frx":040E
            Style           =   2  'Dropdown List
            TabIndex        =   35
            Top             =   1650
            Width           =   2715
         End
         Begin VB.ComboBox CboTipoLibro 
            Height          =   315
            ItemData        =   "dte_gestion.frx":0410
            Left            =   480
            List            =   "dte_gestion.frx":041A
            Style           =   2  'Dropdown List
            TabIndex        =   33
            Top             =   945
            Width           =   4395
         End
         Begin VB.CommandButton CmdConsultaStatus 
            Caption         =   "Consultar"
            Height          =   570
            Left            =   1620
            TabIndex        =   31
            Top             =   2310
            Width           =   2505
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
            Height          =   195
            Index           =   5
            Left            =   495
            OleObjectBlob   =   "dte_gestion.frx":042D
            TabIndex        =   32
            Top             =   735
            Width           =   1935
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
            Height          =   195
            Index           =   1
            Left            =   495
            OleObjectBlob   =   "dte_gestion.frx":0493
            TabIndex        =   34
            Top             =   1455
            Width           =   1320
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
            Height          =   195
            Index           =   1
            Left            =   3240
            OleObjectBlob   =   "dte_gestion.frx":04F7
            TabIndex        =   37
            Top             =   1470
            Width           =   1320
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "Documento de Compra"
         Height          =   3210
         Index           =   1
         Left            =   -63345
         TabIndex        =   20
         Top             =   825
         Visible         =   0   'False
         Width           =   5310
         Begin VB.TextBox TxtRutProveedor 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   540
            TabIndex        =   28
            Text            =   "0"
            Top             =   2430
            Width           =   1650
         End
         Begin VB.ComboBox CboDocCompra 
            Height          =   315
            Left            =   480
            Style           =   2  'Dropdown List
            TabIndex        =   26
            Top             =   945
            Width           =   4395
         End
         Begin VB.CommandButton CmdConsultaDTECompra 
            Caption         =   "Consultar"
            Height          =   570
            Left            =   2685
            TabIndex        =   24
            Top             =   2475
            Width           =   2505
         End
         Begin VB.TextBox TxtFolioCompra 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   510
            TabIndex        =   23
            Text            =   "0"
            Top             =   1740
            Width           =   1650
         End
         Begin VB.Frame Frame2 
            Caption         =   "Ubicaci�n"
            Height          =   810
            Index           =   1
            Left            =   2490
            TabIndex        =   21
            Top             =   1440
            Width           =   2685
            Begin VB.OptionButton Option2 
               Caption         =   "Nube"
               Height          =   270
               Left            =   750
               TabIndex        =   22
               Top             =   360
               Value           =   -1  'True
               Width           =   855
            End
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
            Height          =   195
            Index           =   2
            Left            =   495
            OleObjectBlob   =   "dte_gestion.frx":055B
            TabIndex        =   25
            Top             =   735
            Width           =   1935
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
            Height          =   195
            Index           =   3
            Left            =   495
            OleObjectBlob   =   "dte_gestion.frx":05CB
            TabIndex        =   27
            Top             =   1515
            Width           =   1935
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
            Height          =   195
            Index           =   4
            Left            =   525
            OleObjectBlob   =   "dte_gestion.frx":0633
            TabIndex        =   29
            Top             =   2205
            Width           =   1935
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "Documento de Venta"
         Height          =   3210
         Index           =   0
         Left            =   -74550
         TabIndex        =   11
         Top             =   885
         Width           =   5310
         Begin VB.Frame Frame4 
            Caption         =   "Cedible"
            Height          =   720
            Left            =   225
            TabIndex        =   53
            Top             =   2295
            Width           =   2070
            Begin VB.OptionButton Option4 
               Caption         =   "Si"
               Height          =   360
               Left            =   1215
               TabIndex        =   55
               Top             =   255
               Width           =   585
            End
            Begin VB.OptionButton Option3 
               Caption         =   "No"
               Height          =   195
               Left            =   465
               TabIndex        =   54
               Top             =   330
               Value           =   -1  'True
               Width           =   600
            End
         End
         Begin VB.Frame Frame2 
            Caption         =   "Ubicaci�n"
            Height          =   810
            Index           =   0
            Left            =   2400
            TabIndex        =   17
            Top             =   1440
            Width           =   2685
            Begin VB.OptionButton OptNube 
               Caption         =   "Nube"
               Height          =   270
               Left            =   1680
               TabIndex        =   19
               Top             =   315
               Width           =   855
            End
            Begin VB.OptionButton OptLocal 
               Caption         =   "Local"
               Height          =   270
               Left            =   45
               TabIndex        =   18
               Top             =   315
               Value           =   -1  'True
               Width           =   1035
            End
         End
         Begin VB.TextBox TxtNroDocVenta 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   510
            TabIndex        =   15
            Text            =   "0"
            Top             =   1740
            Width           =   1650
         End
         Begin VB.CommandButton CmdConsultaDEV 
            Caption         =   "Consultar"
            Height          =   570
            Left            =   2370
            TabIndex        =   14
            Top             =   2415
            Width           =   2505
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
            Height          =   195
            Index           =   0
            Left            =   495
            OleObjectBlob   =   "dte_gestion.frx":06AB
            TabIndex        =   13
            Top             =   735
            Width           =   1935
         End
         Begin VB.ComboBox CboDocVenta 
            Height          =   315
            Left            =   480
            Style           =   2  'Dropdown List
            TabIndex        =   12
            Top             =   945
            Width           =   4395
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
            Height          =   195
            Index           =   1
            Left            =   495
            OleObjectBlob   =   "dte_gestion.frx":071B
            TabIndex        =   16
            Top             =   1515
            Width           =   1935
         End
      End
      Begin VB.Frame nada 
         Caption         =   "Subir Folios"
         Height          =   3255
         Left            =   1290
         TabIndex        =   1
         Top             =   945
         Width           =   10350
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
            Height          =   210
            Left            =   3645
            OleObjectBlob   =   "dte_gestion.frx":0783
            TabIndex        =   63
            Top             =   2925
            Width           =   1140
         End
         Begin VB.ComboBox CboCajaDestino 
            Height          =   315
            ItemData        =   "dte_gestion.frx":07F3
            Left            =   4860
            List            =   "dte_gestion.frx":0821
            Style           =   2  'Dropdown List
            TabIndex        =   62
            Top             =   2880
            Width           =   1635
         End
         Begin VB.TextBox TxtRutFolio 
            Height          =   345
            Left            =   1515
            TabIndex        =   43
            Text            =   "rut"
            Top             =   2835
            Width           =   1365
         End
         Begin VB.TextBox txtRangoHasta 
            BackColor       =   &H8000000F&
            Height          =   285
            Left            =   2940
            Locked          =   -1  'True
            TabIndex        =   40
            TabStop         =   0   'False
            Text            =   "336"
            Top             =   2190
            Width           =   1620
         End
         Begin VB.TextBox txtIdDocumento 
            Height          =   300
            Left            =   1515
            TabIndex        =   39
            Text            =   "TIPO DOC"
            Top             =   2505
            Width           =   3135
         End
         Begin VB.TextBox txtRango 
            BackColor       =   &H8000000F&
            Height          =   285
            Left            =   1530
            Locked          =   -1  'True
            TabIndex        =   38
            TabStop         =   0   'False
            Text            =   "201"
            Top             =   2190
            Width           =   1395
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
            Height          =   225
            Index           =   0
            Left            =   1560
            OleObjectBlob   =   "dte_gestion.frx":0854
            TabIndex        =   10
            Top             =   1410
            Width           =   1170
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
            Height          =   270
            Index           =   0
            Left            =   1560
            OleObjectBlob   =   "dte_gestion.frx":08CE
            TabIndex        =   9
            Top             =   810
            Width           =   2115
         End
         Begin VB.ComboBox CboTipoDTE 
            Height          =   315
            Left            =   1560
            Style           =   2  'Dropdown List
            TabIndex        =   6
            Top             =   1650
            Width           =   3090
         End
         Begin VB.CommandButton Command2 
            Caption         =   "Buscar XML Folio"
            Height          =   615
            Left            =   6720
            TabIndex        =   5
            Top             =   960
            Width           =   2925
         End
         Begin VB.OptionButton Option1 
            Caption         =   "Productivo"
            Height          =   375
            Left            =   6840
            TabIndex        =   4
            Top             =   360
            Value           =   -1  'True
            Width           =   1095
         End
         Begin VB.TextBox txtXmlBasico 
            Height          =   285
            Left            =   1560
            TabIndex        =   3
            Top             =   1080
            Width           =   4695
         End
         Begin VB.CommandButton Command1 
            Caption         =   "Proceder"
            Height          =   615
            Left            =   6720
            TabIndex        =   2
            Top             =   2160
            Width           =   2895
         End
         Begin MSComDlg.CommonDialog CommonDialog1 
            Left            =   4455
            Top             =   195
            _ExtentX        =   847
            _ExtentY        =   847
            _Version        =   393216
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
            Height          =   225
            Index           =   2
            Left            =   1530
            OleObjectBlob   =   "dte_gestion.frx":0958
            TabIndex        =   41
            Top             =   1995
            Width           =   1170
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
            Height          =   225
            Index           =   3
            Left            =   2940
            OleObjectBlob   =   "dte_gestion.frx":09C0
            TabIndex        =   42
            Top             =   1995
            Width           =   1170
         End
         Begin VB.Label Label2 
            Caption         =   "Tipo"
            Height          =   375
            Left            =   240
            TabIndex        =   8
            Top             =   1560
            Width           =   1095
         End
         Begin VB.Label Label1 
            Caption         =   "XML Folio"
            Height          =   255
            Left            =   330
            TabIndex        =   7
            Top             =   735
            Width           =   1215
         End
      End
   End
End
Attribute VB_Name = "dte_gestion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
 Option Explicit
      Private Declare Function ShellExecute Lib "shell32.dll" Alias _
      "ShellExecuteA" (ByVal hWnd As Long, ByVal lpszOp As _
      String, ByVal lpszFile As String, ByVal lpszParams As String, _
      ByVal lpszDir As String, ByVal FsShowCmd As Long) As Long

      Private Declare Function GetDesktopWindow Lib "user32" () As Long
     Const SW_SHOWNORMAL = 1

      Const SE_ERR_FNF = 2&
      Const SE_ERR_PNF = 3&
      Const SE_ERR_ACCESSDENIED = 5&
      Const SE_ERR_OOM = 8&
      Const SE_ERR_DLLNOTFOUND = 32&
      Const SE_ERR_SHARE = 26&
      Const SE_ERR_ASSOCINCOMPLETE = 27&
      Const SE_ERR_DDETIMEOUT = 28&
      Const SE_ERR_DDEFAIL = 29&
      Const SE_ERR_DDEBUSY = 30&
      Const SE_ERR_NOASSOC = 31&
      Const ERROR_BAD_FORMAT = 11&

      Function StartDoc(DocName As String) As Long
          Dim Scr_hDC As Long
          Scr_hDC = GetDesktopWindow()
          StartDoc = ShellExecute(Scr_hDC, "Open", DocName, _
          "", "C:\", SW_SHOWNORMAL)
      End Function

Private Sub CmdConsultaDEV_Click()
    Dim Bp_Cedible As Boolean
    Dim Sp_TipoImpreso As String
    Dim Sp_Archivo2 As String
    Bp_Cedible = False
    Sp_TipoImpreso = 6
    'Modulo: ver pdfs, ya sea local o en la nube
    'fecha : 11 octubre 2014
    Dim resp As Boolean
    If CboDocVenta.ListIndex = -1 Then
        MsgBox "Seleccione documento de venta...", vbInformation
        CboDocVenta.SetFocus
        Exit Sub
    End If
    
    If Val(Me.TxtNroDocVenta) = 0 Then
        MsgBox "Ingrese Folio...", vbInformation
        TxtNroDocVenta.SetFocus
        Exit Sub
    End If
    FrmLoad.Visible = True
    DoEvents
        
    If Option4 Then Bp_Cedible = True
    If OptLocal.Value Then
        
        Archivo = "C:\FACTURAELECTRONICA\PDF\" & Replace(SP_Rut_Activo, ".", "") & "\T" & CboDocVenta.ItemData(CboDocVenta.ListIndex) & "F" & Me.TxtNroDocVenta & ".PDF"
        VerDteLocal
    Else
        'Consulta electronica
        Dim obj As DTECloud.Integracion
        Set obj = New DTECloud.Integracion

        obj.Productivo = False
        'obj.UrlServicioFacturacion = "http://wsdtedesa.dyndns.biz/WSDTE/Service.asmx"
        obj.UrlServicioFacturacion = SG_Url_Factura_Electronica ' "http://wsdte.dyndns.biz/WSDTE/Service.asmx?WSDL"
        obj.HabilitarDescarga = True
        obj.CarpetaDestino = "C:\FACTURAELECTRONICA\PDF\" & Replace(SP_Rut_Activo, ".", "")
      
        obj.Password = "1234"
        obj.Productivo = BG_ModoProductivoDTE
         Sp_TipoImpreso = "1"
        '////////////////////////////
        If SG_LlevaIVAAnticipado = "SI" Then
            Sp_TipoImpreso = 6
            'obj.TipoImpresionRDL = "6"
        End If
        obj.TipoImpresionRDL = Sp_TipoImpreso
        'resp = obj.DescargarPDF(rutempresa.Text, rutempresa.Text, Combo1.Text, folio.Text, "E", False, False)
        On Error GoTo ErrorConexion
        If Val(Sp_TipoImpreso) = 1 Then
            resp = obj.DescargarPDF(Replace(SP_Rut_Activo, ".", ""), Replace(SP_Rut_Activo, ".", ""), Trim(CboDocVenta.ItemData(CboDocVenta.ListIndex)), Trim(Me.TxtNroDocVenta), "E", Bp_Cedible, BG_ModoProductivoDTE)
        Else
            resp = obj.DescargarPDF2(Replace(SP_Rut_Activo, ".", ""), Replace(SP_Rut_Activo, ".", ""), Trim(CboDocVenta.ItemData(CboDocVenta.ListIndex)), Trim(Me.TxtNroDocVenta), "E", Sp_TipoImpreso, BG_ModoProductivoDTE)
        End If

        Archivo = obj.URLPDF
      '  Sp_Archivo2 = obj.URLXMLFirmado
        
        If Mid(Archivo, 1, 12) = "El documento" Then
            MsgBox "Documento no se encontr� en SII..."
        Else
            ShellExecute Me.hWnd, "open", Archivo, "", "", 4
        End If

    End If
    FrmLoad.Visible = False
    Exit Sub

ErrorConexion:
    FrmLoad.Visible = False
    MsgBox "Error:" & Err.Description & vbNewLine & "Nro:" & Err.Number & " " & Err.Source
    
    
 End Sub

Private Sub CmdConsultaDTECompra_Click()
    Dim Bp_Cedible As Boolean
    Bp_Cedible = False
    'Modulo: ver pdfs, ya sea local o en la nube
    'fecha : 11 octubre 2014
    Dim resp As Boolean
    If CboDocCompra.ListIndex = -1 Then
        MsgBox "Seleccione documento de venta...", vbInformation
        CboDocCompra.SetFocus
        Exit Sub
    End If
    
    If Val(Me.TxtFolioCompra) = 0 Then
        MsgBox "Ingrese Folio...", vbInformation
        TxtFolioCompra.SetFocus
        Exit Sub
    End If
        
    If Option4 Then Bp_Cedible = True
      'Consulta electronica
      Dim obj As DTECloud.Integracion
      Set obj = New DTECloud.Integracion
      obj.Productivo = True
      'obj.UrlServicioFacturacion = "http://wsdtedesa.dyndns.biz/WSDTE/Service.asmx"
      obj.UrlServicioFacturacion = SG_Url_Factura_Electronica
      obj.HabilitarDescarga = True
      obj.CarpetaDestino = "C:\FACTURAELECTRONICA\PDF"
      obj.Password = "1234"
      'resp = obj.DescargarPDF(rutempresa.Text, rutempresa.Text, Combo1.Text, folio.Text, "E", False, False)
      resp = obj.DescargarPDF(Replace(SP_Rut_Activo, ".", ""), Replace(TxtRutProveedor, ".", ""), CboDocCompra.ItemData(CboDocCompra.ListIndex), Me.TxtFolioCompra, "R", Bp_Cedible, True)
      MsgBox obj.URLPDF
'      Call Shell(obj.RutaPDF)
      Archivo = obj.URLPDF
      VerDteLocal
End Sub

Private Sub CmdConsultaStatus_Click()
    Dim obj As DTECloud.Integracion, resp  As Variant
    Set obj = New DTECloud.Integracion
       

        obj.UrlServicioFacturacion = SG_Url_Factura_Electronica
        obj.Password = "1234"
        resp = obj.ConsultaEstadoLibroElectronico(Replace(SP_Rut_Activo, ".", ""), CboAnoContable.Text & "-" & Right("00" & CboMesContable.ItemData(CboMesContable.ListIndex), 2), CboTipoLibro.Text, True)
        
        MsgBox obj.DescripcionResultado
        

End Sub

Private Sub cmdMostrarFolios_Click()
    Sql = "SELECT IFNULL(COUNT(dte_folio),0) disponibles " & _
            "FROM dte_folios " & _
            "WHERE rut_emp='" & SP_Rut_Activo & "' AND doc_id=" & CboDocVenta2.ItemData(CboDocVenta2.ListIndex) & " AND dte_disponible='SI'"
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        Me.SkCantDisponibles = "Cant. Disponibles:" & RsTmp!disponibles
    End If
    Sql = "SELECT dte_folio,dte_disponible " & _
            "FROM dte_folios " & _
            "WHERE rut_emp='" & SP_Rut_Activo & "' AND doc_id=" & CboDocVenta2.ItemData(CboDocVenta2.ListIndex) & " AND dte_disponible='SI'"
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, Me.LvFDisponible, False, True, True, False
    
    

End Sub

Private Sub CmdSalir_Click()
    Unload Me
End Sub

Private Sub Command1_Click()
    Dim Sp_CpraVta As String
  Dim obj As DTECloud.Integracion, i As Long
  Set obj = New DTECloud.Integracion
  Dim resp As Boolean
  'Validaciones
  If SP_Rut_Activo <> TxtRutFolio Then
        MsgBox "El archivo que intenta cargar, no corresponde a la empresa...", vbInformation
        txtXmlBasico.SetFocus
        Exit Sub
  End If
  
  If txtIdDocumento <> CboTipoDTE.ItemData(CboTipoDTE.ListIndex) Then
    MsgBox "El archivo que intenta cargar corresponde a otro tipo de documento..."
    CboTipoDTE.SetFocus
    Exit Sub
  End If
  FrmLoad.Visible = True
  DoEvents
  Sql = "SELECT dte_id " & _
        "FROM dte_folios " & _
        "WHERE rut_emp='" & SP_Rut_Activo & "' AND doc_id=" & txtIdDocumento & _
        "  AND (dte_folio BETWEEN " & txtRango & " AND " & txtRangoHasta & ")"
  Consulta RsTmp, Sql
  If RsTmp.RecordCount > 0 Then
        FrmLoad.Visible = False
        MsgBox "Folios ya se encuentran cargados...", vbExclamation
        Exit Sub
  End If
  Sp_CpraVta = "VENTA"
    If CboTipoDTE.ItemData(CboTipoDTE.ListIndex) = 46 Then
        Sp_CpraVta = "COMPRA"
    End If
    If txtIdDocumento <> 39 And txtIdDocumento <> 41 Then
         'no cargamos folios de boletas en abastrahre
         obj.UrlServicioFacturacion = SG_Url_Factura_Electronica
         obj.Password = "1234"
        ' resp = obj.SubirAutoriacionDeFolios(Replace(SP_Rut_Activo, ".", ""), CboTipoDTE.ItemData(CboTipoDTE.ListIndex), txtXmlBasico.Text, True)
         resp = obj.SubirAutoriacionDeFolios(Replace(SP_Rut_Activo, ".", ""), CboTipoDTE.ItemData(CboTipoDTE.ListIndex), txtXmlBasico.Text, True)
         FrmLoad.Visible = False
         MsgBox (obj.DescripcionResultado & " Inicial: " & obj.FolioInicialCAF & ", Final: " & obj.FolioFinalCAF)
    End If
    FrmLoad.Visible = True
    DoEvents
    Sql = "INSERT INTO dte_folios (rut_emp,dte_venta_compra,doc_id,dte_folio,dte_disponible,dte_fecha,dte_nro_caja) VALUES"
    For i = txtRango To txtRangoHasta
        Sql = Sql & "('" & SP_Rut_Activo & "','" & Sp_CpraVta & "'," & txtIdDocumento & "," & i & ",'SI'" & ",'" & Fql(Date) & "'," & CboCajaDestino.Text & "),"
    Next
    If txtIdDocumento = 39 Or txtIdDocumento = 41 Then
        MsgBox "Folios cargados de Boletas ...", vbInformation
    End If
    On Error GoTo fallagrabarFolio
    cn.Execute Mid(Sql, 1, Len(Sql) - 1)
    FrmLoad.Visible = False
    Exit Sub
fallagrabarFolio:
    FrmLoad.Visible = False
    MsgBox "Error en BD... " & Err.Description, vbExclamation
    
End Sub

Private Sub Command2_Click()
    Dim ff As Integer
    Dim Sp_infoLinea As String, Sp_RutFormat As String
    ff = FreeFile 'Sets to next available file number
    With CommonDialog1
        .FileName = ""
        .Filter = "All files (*.*) |*.*|" 'Sets the filter
        .ShowOpen
    End With
    txtXmlBasico.Text = CommonDialog1.FileName
    If Len(txtXmlBasico.Text) > 0 Then
        Open txtXmlBasico For Input As ff
            Do While Not EOF(ff)
                Line Input #ff, Sp_infoLinea
                If InStr(1, Sp_infoLinea, "<TD>") > 0 Then
                    txtIdDocumento = Mid(Sp_infoLinea, InStr(1, Sp_infoLinea, "<TD>") + 4, 2)
                    Busca_Id_Combo CboTipoDTE, Val(txtIdDocumento)
                End If
                '<RNG><D>17</D><H>36</H></RNG>
                If InStr(1, Sp_infoLinea, "<RNG>") > 0 Then
                'If Mid(Sp_infoLinea, 1, 5) = "<RNG>" Then
                    txtRango = Mid(Sp_infoLinea, InStr(1, Sp_infoLinea, "<RNG><D>") + 8, InStr(1, Sp_infoLinea, "</H></RNG>") - (InStr(1, Sp_infoLinea, "<RNG><D>") + 8))
                    txtRangoHasta = Mid(txtRango, InStr(1, txtRango, "<H>") + 3)
                    txtRango = Mid(txtRango, 1, InStr(1, txtRango, "</D>") - 1)
                End If
                'rut
                If InStr(1, Sp_infoLinea, "<RE>") > 0 Then
                'If Mid(Sp_infoLinea, 1, 4) = "<RE>" Then
                    TxtRutFolio = Mid(Sp_infoLinea, InStr(1, Sp_infoLinea, "<RE>") + 4, InStr(1, Sp_infoLinea, "</RE>") - (InStr(1, Sp_infoLinea, "<RE>") + 4))
                    VerificaRut Me.TxtRutFolio, Sp_RutFormat
                    TxtRutFolio = Sp_RutFormat
                End If
                
            Loop
        
        Close #ff
    End If
End Sub

Private Sub VerDteLocal()
        Dim r As Long, Msg As String
          r = StartDoc(Archivo)
          If r <= 32 Then
              'There was an error
              Select Case r
                  Case SE_ERR_FNF
                      Msg = "File not found"
                  Case SE_ERR_PNF
                      Msg = "Path not found"
                  Case SE_ERR_ACCESSDENIED
                      Msg = "Access denied"
                  Case SE_ERR_OOM
                      Msg = "Out of memory"
                  Case SE_ERR_DLLNOTFOUND
                      Msg = "DLL not found"
                  Case SE_ERR_SHARE
                      Msg = "A sharing violation occurred"
                  Case SE_ERR_ASSOCINCOMPLETE
                      Msg = "Incomplete or invalid file association"
                  Case SE_ERR_DDETIMEOUT
                      Msg = "DDE Time out"
                  Case SE_ERR_DDEFAIL
                      Msg = "DDE transaction failed"
                  Case SE_ERR_DDEBUSY
                      Msg = "DDE busy"
                  Case SE_ERR_NOASSOC
                      Msg = "No association for file extension"
                  Case ERROR_BAD_FORMAT
                      Msg = "Invalid EXE file or error in EXE image"
                  Case Else
                      Msg = "Unknown error"
              End Select
              MsgBox Msg
          End If
End Sub

Private Sub Command3_Click()
  'Consulta electronica
        Dim obj As DTECloud.Integracion
        Set obj = New DTECloud.Integracion
    Dim resp As Variant
       
        obj.Productivo = BG_ModoProductivoDTE
        'obj.UrlServicioFacturacion = "http://wsdtedesa.dyndns.biz/WSDTE/Service.asmx"
        obj.UrlServicioFacturacion = SG_Url_Factura_Electronica
        obj.HabilitarDescarga = True
        obj.CarpetaDestino = "C:\FACTURAELECTRONICA\PDF\" & Trim(Replace(SP_Rut_Activo, ".", ""))
        obj.Password = "1234"
        resp = obj.DescargarPDFMasivo(Replace(SP_Rut_Activo, ".", ""), Str(CboDocVenta.ItemData(CboDocVenta.ListIndex)), TxtInicial, TxtFinal, "NO", BG_ModoProductivoDTE)
        Archivo = obj.URLPDF
        VerDteLocal
End Sub

Private Sub Command5_Click()
    Dim ObjDteCloud As New DTECloud.Integracion
Dim resp As Boolean
            ObjDteCloud.Password = "Rz,.370kT"

            ObjDteCloud.UrlServicioFacturacion = SG_Url_Factura_Electronica
            
            resp = ObjDteCloud.CederDTE("76035172-5", "33", "10", "20230000", "99575550-5", _
              "Bolsa de Productos de Chile Bolsa de Productos Agropecuarios S.A.", "Huerfanos N�770 Piso 14, Santiago", "dsuarez@contempora.com", _
                 "Marcelo Pizarro", "mpizarro@contempora.cl", "76035172-5", "marcelo sabugo", "2016-05-10", "ventas@alvamar.cl", True)
            If resp Then
                MsgBox ObjDteCloud.ConsultaEstadoCesion("76035172-5", "33", "10", True)
            End If
            
           'respuesta = ObjDteCloud.CederDTE(ByVal rutEmpresa As String, ByVal tipoDTE As String, ByVal folioDTE As String, ByVal montoCesion As Long, ByVal rutcesionario As String, ByVal rznsocialcesionario As String, ByVal direccioncesionario As String, ByVal emailcesionario As String, ByVal contactoempresa As String, ByVal mailcontacto As String, ByVal rutautorizado As String, ByVal nombreautorizado As String, ByVal ultimovencimiento As String, ByVal emaildeudor As String, ByVal productivo As Boolean)
End Sub
Private Sub Form_Load()
    Skin2 Me, , 5
    Centrar Me
    Dim i As Integer
    LLenarCombo CboTipoDTE, "doc_nombre", "doc_cod_sii", "sis_documentos", "doc_activo='SI' AND doc_dte='SI' /*AND doc_documento='VENTA'*/ AND doc_honorarios='NO'"
    CboTipoDTE.ListIndex = 0
    LLenarCombo CboDocVenta, "doc_nombre", "doc_cod_sii", "sis_documentos", "doc_activo='SI' AND doc_dte='SI' AND /*doc_documento='VENTA' AND*/ doc_honorarios='NO'"
    CboDocVenta.ListIndex = 0
    
    LLenarCombo CboDocVenta2, "doc_nombre", "doc_cod_sii", "sis_documentos", "doc_activo='SI' AND doc_dte='SI' AND /*doc_documento='VENTA' AND*/ doc_honorarios='NO'"
    CboDocVenta2.ListIndex = 0
    
    LLenarCombo CboDocCompra, "doc_nombre", "doc_cod_sii", "sis_documentos", "doc_activo='SI' AND doc_dte='SI' AND doc_documento='COMPRA' AND doc_honorarios='NO'"
'    CboDocCompra.ListIndex = 0
     For i = 1 To 12
        CboMesContable.AddItem UCase(MonthName(i, False))
        CboMesContable.ItemData(CboMesContable.ListCount - 1) = i
    Next
    Busca_Id_Combo CboMesContable, Val(IG_Mes_Contable)
    LLenaYears Me.CboAnoContable, 2010
    CboCajaDestino.ListIndex = 0
End Sub

Private Sub Timer1_Timer()
    On Error Resume Next
    txtXmlBasico.SetFocus
    Timer1.Enabled = False
End Sub
Private Sub TxtFolioCompra_GotFocus()
    En_Foco TxtFolioCompra
End Sub

Private Sub TxtNroDocVenta_GotFocus()
    En_Foco TxtNroDocVenta
End Sub

Private Sub TxtNroDocVenta_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
    If KeyAscii = 39 Then KeyAscii = 0
End Sub
