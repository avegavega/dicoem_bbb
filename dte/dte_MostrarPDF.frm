VERSION 5.00
Begin VB.Form dte_MostrarPDF 
   Caption         =   "Form1"
   ClientHeight    =   10215
   ClientLeft      =   8190
   ClientTop       =   1350
   ClientWidth     =   7200
   LinkTopic       =   "Form1"
   ScaleHeight     =   10215
   ScaleWidth      =   7200
   Begin VB.CommandButton Command1 
      Caption         =   "Command1"
      Height          =   255
      Left            =   2160
      TabIndex        =   0
      Top             =   1260
      Width           =   1410
   End
End
Attribute VB_Name = "dte_MostrarPDF"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
      Private Declare Function ShellExecute Lib "shell32.dll" Alias _
      "ShellExecuteA" (ByVal hWnd As Long, ByVal lpszOp As _
      String, ByVal lpszFile As String, ByVal lpszParams As String, _
      ByVal lpszDir As String, ByVal FsShowCmd As Long) As Long
'ver DTE
      Private Declare Function GetDesktopWindow Lib "user32" () As Long
     Const SW_SHOWNORMAL = 1

      Const SE_ERR_FNF = 2&
      Const SE_ERR_PNF = 3&
      Const SE_ERR_ACCESSDENIED = 5&
      Const SE_ERR_OOM = 8&
      Const SE_ERR_DLLNOTFOUND = 32&
      Const SE_ERR_SHARE = 26&
      Const SE_ERR_ASSOCINCOMPLETE = 27&
      Const SE_ERR_DDETIMEOUT = 28&
      Const SE_ERR_DDEFAIL = 29&
      Const SE_ERR_DDEBUSY = 30&
      Const SE_ERR_NOASSOC = 31&
      Const ERROR_BAD_FORMAT = 11&


      Function StartDoc(DocName As String) As Long
          Dim Scr_hDC As Long
          Scr_hDC = GetDesktopWindow()
          StartDoc = ShellExecute(Scr_hDC, "Open", DocName, _
          "", "C:\", SW_SHOWNORMAL)
      End Function
Public Sub XML(doc_sii As String, nro_Doc As String)
    Dim Sp_TipoImpreso As String
    Dim Sp_Archivo2 As String
    Dim resp As Boolean

    'Consulta electronica
    Dim obj As DTECloud.Integracion
    Set obj = New DTECloud.Integracion
    obj.Productivo = BG_ModoProductivoDTE
    obj.UrlServicioFacturacion = SG_Url_Factura_Electronica '"http://wsdte.dyndns.biz/WSDTE/Service.asmx?WSDL"
    obj.HabilitarDescarga = True
    If Dir("C:\FACTURAELECTRONICA\PDF\" & Replace(SP_Rut_Activo, ".", ""), vbDirectory) = "" Then
        'MsgBox "La carpeta no existe"
        MkDir ("C:\FACTURAELECTRONICA\PDF\" & Replace(SP_Rut_Activo, ".", ""))
    End If
    obj.CarpetaDestino = "C:\FACTURAELECTRONICA\PDF\" & Replace(SP_Rut_Activo, ".", "")
    obj.Password = "1234"
    Sp_TipoImpreso = "1"
    '////////////////////////////

    '///////////////////////////
    obj.TipoImpresionRDL = Str(Sp_TipoImpreso)

        resp = obj.DescargarPDF(Replace(SP_Rut_Activo, ".", ""), Replace(SP_Rut_Activo, ".", ""), Trim(doc_sii), Trim(nro_Doc), "E", False, BG_ModoProductivoDTE)

    Archivo = obj.URLXMLSIIFirmado
    'Archivo = obj.URLXMLFirmado
    ShellExecute Me.hWnd, "open", Archivo, "", "", 4
    '   Sp_Archivo2 = obj.URLXMLSIIFirmado
    'VerDteLocal
    
    SG_LlevaIVAAnticipado = "NO"
End Sub

Public Sub Dte(doc_sii As String, nro_Doc As String, Cedible As Boolean)
    Dim Sp_TipoImpreso As String
    Dim Sp_Archivo2 As String
    Dim resp As Boolean
    If Not Cedible Then
        If VerDteLocal Then Exit Sub
    End If
    Dim InfoXml As String
    'Consulta electronica
    Dim obj As DTECloud.Integracion
    Set obj = New DTECloud.Integracion
    obj.Productivo = BG_ModoProductivoDTE
    obj.UrlServicioFacturacion = SG_Url_Factura_Electronica '"http://wsdte.dyndns.biz/WSDTE/Service.asmx?WSDL"
    obj.HabilitarDescarga = True
    If Dir("C:\FACTURAELECTRONICA\PDF\" & Replace(SP_Rut_Activo, ".", ""), vbDirectory) = "" Then
        'MsgBox "La carpeta no existe"
        MkDir ("C:\FACTURAELECTRONICA\PDF\" & Replace(SP_Rut_Activo, ".", ""))
    End If
    obj.CarpetaDestino = "C:\FACTURAELECTRONICA\PDF\" & Replace(SP_Rut_Activo, ".", "")
    obj.Password = "1234"
    Sp_TipoImpreso = "1"
    '////////////////////////////
    If SG_LlevaIVAAnticipado = "SI" Then
        If Not Cedible Then
            Sp_TipoImpreso = "6"
        Else
            Sp_TipoImpreso = "7"
        End If
        'obj.TipoImpresionRDL = "6"
    End If
    '///////////////////////////
    obj.TipoImpresionRDL = Str(Sp_TipoImpreso)
    'resp = obj.DescargarPDF(Replace(SP_Rut_Activo, ".", ""), Replace(SP_Rut_Activo, ".", ""), Trim(doc_sii), Trim(nro_Doc), "E", Cedible, BG_ModoProductivoDTE)
    If Val(Sp_TipoImpreso) > 1 Then
    
        resp = obj.DescargarPDF2(Replace(SP_Rut_Activo, ".", ""), Replace(SP_Rut_Activo, ".", ""), Trim(doc_sii), Trim(nro_Doc), "E", Sp_TipoImpreso, Cedible)
    Else
        resp = obj.DescargarPDF(Replace(SP_Rut_Activo, ".", ""), Replace(SP_Rut_Activo, ".", ""), Trim(doc_sii), Trim(nro_Doc), "E", Cedible, BG_ModoProductivoDTE)
    End If
    Archivo = obj.URLPDF
   ' InfoXml = obj.URLXMLFirmado
    '   Sp_Archivo2 = obj.URLXMLSIIFirmado
    VerDteLocal
    
    SG_LlevaIVAAnticipado = "NO"
End Sub

Function VerDteLocal() As Boolean
        Dim r As Long, Msg As String
          r = StartDoc(Archivo)
          If r <= 32 Then
                VerDteLocal = False
              'There was an error
              Select Case r
                  Case SE_ERR_FNF
                      Msg = "Buscando..."
                  Case SE_ERR_PNF
                      Msg = "Path not found"
                  Case SE_ERR_ACCESSDENIED
                      Msg = "Access denied"
                  Case SE_ERR_OOM
                      Msg = "Out of memory"
                  Case SE_ERR_DLLNOTFOUND
                      Msg = "DLL not found"
                  Case SE_ERR_SHARE
                      Msg = "A sharing violation occurred"
                  Case SE_ERR_ASSOCINCOMPLETE
                      Msg = "Incomplete or invalid file association"
                  Case SE_ERR_DDETIMEOUT
                      Msg = "DDE Time out"
                  Case SE_ERR_DDEFAIL
                      Msg = "DDE transaction failed"
                  Case SE_ERR_DDEBUSY
                      Msg = "DDE busy"
                  Case SE_ERR_NOASSOC
                      Msg = "No association for file extension"
                  Case ERROR_BAD_FORMAT
                      Msg = "Invalid EXE file or error in EXE image"
                  Case Else
                      Msg = "Unknown error"
              End Select
              'MsgBox Msg
            Else
            
                VerDteLocal = True
          End If
End Function

