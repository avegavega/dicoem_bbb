VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{28D47522-CF84-11D1-834C-00A0249F0C28}#1.0#0"; "gif89.dll"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form dte_contratos 
   Caption         =   "Cargar Folios de Contratos"
   ClientHeight    =   7035
   ClientLeft      =   10860
   ClientTop       =   1725
   ClientWidth     =   8655
   LinkTopic       =   "Form1"
   ScaleHeight     =   7035
   ScaleWidth      =   8655
   Begin VB.Frame FrmLoad 
      BackColor       =   &H00FF0000&
      Height          =   3720
      Left            =   2865
      TabIndex        =   12
      Top             =   2145
      Visible         =   0   'False
      Width           =   3300
      Begin GIF89LibCtl.Gif89a Gif89a1 
         Height          =   3345
         Left            =   165
         OleObjectBlob   =   "dte_contratos.frx":0000
         TabIndex        =   13
         Top             =   195
         Width           =   2940
      End
   End
   Begin VB.CommandButton CmdRetornar 
      Caption         =   "Retornar"
      Height          =   420
      Left            =   7290
      TabIndex        =   11
      Top             =   6435
      Width           =   1230
   End
   Begin VB.Timer Timer1 
      Interval        =   1000
      Left            =   5025
      Top             =   6435
   End
   Begin VB.Frame Frame1 
      Caption         =   "Historial de carga de folios"
      Height          =   3600
      Left            =   210
      TabIndex        =   9
      Top             =   2745
      Width           =   8295
      Begin MSComctlLib.ListView LvDetalle 
         Height          =   2730
         Left            =   120
         TabIndex        =   10
         Top             =   450
         Width           =   7920
         _ExtentX        =   13970
         _ExtentY        =   4815
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   7
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Id"
            Object.Width           =   1058
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "T1000"
            Text            =   "Sucursal"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   2
            Object.Tag             =   "N109"
            Text            =   "Desde"
            Object.Width           =   1614
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   3
            Object.Tag             =   "N109"
            Text            =   "Hasta"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   2
            SubItemIndex    =   4
            Object.Tag             =   "N109"
            Text            =   "Cantidad"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Object.Tag             =   "F1000"
            Text            =   "Fecha"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   6
            Object.Tag             =   "T1000"
            Text            =   "Usuario"
            Object.Width           =   2540
         EndProperty
      End
   End
   Begin VB.TextBox TxtHasta 
      Height          =   300
      Left            =   3015
      TabIndex        =   8
      Top             =   1425
      Width           =   1260
   End
   Begin VB.Frame nada 
      Caption         =   "Subir Folios"
      Height          =   2370
      Left            =   180
      TabIndex        =   0
      Top             =   120
      Width           =   8340
      Begin VB.TextBox TxtDesde 
         Height          =   300
         Left            =   1350
         TabIndex        =   7
         Top             =   1320
         Width           =   1260
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   285
         Left            =   420
         OleObjectBlob   =   "dte_contratos.frx":0088
         TabIndex        =   6
         Top             =   660
         Width           =   720
      End
      Begin VB.ComboBox CmbSucursal 
         Height          =   315
         ItemData        =   "dte_contratos.frx":00F6
         Left            =   1335
         List            =   "dte_contratos.frx":0100
         Style           =   2  'Dropdown List
         TabIndex        =   5
         Top             =   615
         Width           =   2790
      End
      Begin VB.CommandButton Command1 
         Caption         =   "Proceder"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   975
         Left            =   4725
         TabIndex        =   1
         Top             =   585
         Width           =   2895
      End
      Begin MSComDlg.CommonDialog CommonDialog1 
         Left            =   4455
         Top             =   195
         _ExtentX        =   847
         _ExtentY        =   847
         _Version        =   393216
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   225
         Index           =   2
         Left            =   1395
         OleObjectBlob   =   "dte_contratos.frx":0116
         TabIndex        =   2
         Top             =   1095
         Width           =   1170
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   225
         Index           =   3
         Left            =   2910
         OleObjectBlob   =   "dte_contratos.frx":017E
         TabIndex        =   3
         Top             =   1095
         Width           =   1170
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         Caption         =   "Tipo"
         Height          =   360
         Left            =   600
         TabIndex        =   4
         Top             =   1350
         Width           =   540
      End
   End
End
Attribute VB_Name = "dte_contratos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub CmdRetornar_Click()
    Unload Me
End Sub

Private Sub Command1_Click()

    Dim Sp_CpraVta As String
    Dim resp As Boolean
  'Validaciones
  
    If Me.TxtDesde = "" Then
        MsgBox "Debe Ingresar Valor Desde..."
       '' CboTipoDTE.SetFocus
        Exit Sub
    End If
    If Me.TxtHasta = "" Then
        MsgBox "Debe Ingresar Valor Hasta..."
        '' CboTipoDTE.SetFocus
        Exit Sub
    End If
    FrmLoad.Visible = True
    DoEvents
    Sql = "SELECT contrato_numero " & _
            "FROM dte_contrato " & _
            "WHERE contrato_numero BETWEEN " & TxtDesde & " AND " & TxtHasta
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        FrmLoad.Visible = False
        MsgBox "Folios ya se encuentran cargados...", vbExclamation
        Exit Sub
    End If
    i = 0
    Sql = "INSERT INTO dte_contrato (contrato_numero,contrato_sucursal,contrato_disponible) VALUES"
    For i = TxtDesde To TxtHasta
        Sql = Sql & "(" & i & ",'" & CmbSucursal.ItemData(CmbSucursal.ListIndex) & "','SI'" & "),"
    Next
    
 
    On Error GoTo fallagrabarFolio
    cn.Execute Mid(Sql, 1, Len(Sql) - 1)
    Sql = "INSERT INTO sis_registro_folios (sue_id,fol_inicio,fol_fin,fol_cantidad,fol_fecha,fol_usuario) " & _
            "VALUES(" & CmbSucursal.ItemData(CmbSucursal.ListIndex) & "," & TxtDesde & "," & TxtHasta & "," & Val(TxtHasta) - Val(TxtDesde) & ",'" & Fql(Date) & "','" & LogUsuario & "')"
    cn.Execute Sql
    CargaHistorico
    
    FrmLoad.Visible = False
    MsgBox "Folios Cargados Correctamente..."
    Me.TxtDesde = ""
    Me.TxtHasta = ""
    
    Exit Sub
fallagrabarFolio:
    FrmLoad.Visible = False
    MsgBox "Error en BD..."
End Sub

Private Sub Form_Load()
    Aplicar_skin Me
    Centrar Me, False
    CargaHistorico
End Sub
Private Sub CargaHistorico()
    Sql = "SELECT fol_id,sue_ciudad,fol_inicio,fol_fin,fol_cantidad,fol_fecha,fol_usuario " & _
            "FROM sis_registro_folios r " & _
            "JOIN sis_empresas_sucursales s ON r.sue_id=s.sue_id"
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvDetalle, False, True, True, False
End Sub

Private Sub LvDetalle_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
     ordListView ColumnHeader, Me, LvDetalle
End Sub

Private Sub Timer1_Timer()
    On Error Resume Next
    CmbSucursal.SetFocus
    Timer1.Enabled = False
End Sub
