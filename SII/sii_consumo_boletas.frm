VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{28D47522-CF84-11D1-834C-00A0249F0C28}#1.0#0"; "gif89.dll"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{DE4917CD-D9B7-45E2-B0C7-DA1DB0A76109}#1.0#0"; "jhTextBoxM.ocx"
Begin VB.Form sii_consumo_boletas 
   Caption         =   "Consumo de boletas al SII"
   ClientHeight    =   8580
   ClientLeft      =   3225
   ClientTop       =   2385
   ClientWidth     =   14880
   LinkTopic       =   "Form1"
   ScaleHeight     =   8580
   ScaleWidth      =   14880
   Begin VB.CommandButton CmdRevisar 
      Caption         =   "Revisar"
      Height          =   330
      Left            =   3600
      TabIndex        =   53
      Top             =   3960
      Width           =   1980
   End
   Begin VB.ComboBox CboDesde 
      Height          =   315
      Left            =   765
      Style           =   2  'Dropdown List
      TabIndex        =   45
      Top             =   720
      Width           =   885
   End
   Begin VB.ListBox LstLog 
      Height          =   5325
      Left            =   16005
      TabIndex        =   43
      Top             =   1305
      Width           =   5745
   End
   Begin VB.CommandButton CmdComienzo 
      Caption         =   "Comienzo"
      Height          =   540
      Left            =   8145
      TabIndex        =   40
      Top             =   285
      Width           =   2400
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel15 
      Height          =   270
      Left            =   435
      OleObjectBlob   =   "sii_consumo_boletas.frx":0000
      TabIndex        =   39
      Top             =   7260
      Width           =   1650
   End
   Begin MSComCtl2.DTPicker DtComienzo 
      Height          =   300
      Left            =   6330
      TabIndex        =   37
      Top             =   675
      Width           =   1125
      _ExtentX        =   1984
      _ExtentY        =   529
      _Version        =   393216
      CalendarBackColor=   16777215
      Format          =   95223810
      CurrentDate     =   44006.0416666667
   End
   Begin VB.Frame Frame4 
      Caption         =   "Programacion"
      Height          =   1095
      Left            =   465
      TabIndex        =   32
      Top             =   15
      Width           =   7140
      Begin VB.ComboBox CboNroCaja 
         Height          =   315
         ItemData        =   "sii_consumo_boletas.frx":0084
         Left            =   4305
         List            =   "sii_consumo_boletas.frx":0097
         Style           =   2  'Dropdown List
         TabIndex        =   50
         Top             =   375
         Width           =   885
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel17 
         Height          =   195
         Left            =   2865
         OleObjectBlob   =   "sii_consumo_boletas.frx":00AA
         TabIndex        =   48
         Top             =   165
         Width           =   1335
      End
      Begin VB.ComboBox CboSecuencia 
         Height          =   315
         ItemData        =   "sii_consumo_boletas.frx":011A
         Left            =   3420
         List            =   "sii_consumo_boletas.frx":012D
         Style           =   2  'Dropdown List
         TabIndex        =   47
         Top             =   375
         Width           =   885
      End
      Begin VB.ComboBox CboHasta 
         Height          =   315
         Left            =   1260
         Style           =   2  'Dropdown List
         TabIndex        =   46
         Top             =   720
         Width           =   885
      End
      Begin VB.ComboBox ComAno 
         Height          =   315
         Left            =   2175
         Style           =   2  'Dropdown List
         TabIndex        =   34
         Top             =   375
         Width           =   1215
      End
      Begin VB.ComboBox comMes 
         Height          =   315
         ItemData        =   "sii_consumo_boletas.frx":0140
         Left            =   285
         List            =   "sii_consumo_boletas.frx":0142
         Style           =   2  'Dropdown List
         TabIndex        =   33
         Top             =   360
         Width           =   1890
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel13 
         Height          =   255
         Left            =   285
         OleObjectBlob   =   "sii_consumo_boletas.frx":0144
         TabIndex        =   35
         Top             =   165
         Width           =   375
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel14 
         Height          =   255
         Left            =   2205
         OleObjectBlob   =   "sii_consumo_boletas.frx":01A8
         TabIndex        =   36
         Top             =   180
         Width           =   375
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel18 
         Height          =   195
         Left            =   4410
         OleObjectBlob   =   "sii_consumo_boletas.frx":020C
         TabIndex        =   49
         Top             =   165
         Width           =   675
      End
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel12 
      Height          =   225
      Left            =   480
      OleObjectBlob   =   "sii_consumo_boletas.frx":0272
      TabIndex        =   31
      Top             =   1065
      Width           =   3450
   End
   Begin VB.Frame FrmLoad 
      Height          =   1680
      Left            =   3330
      TabIndex        =   10
      Top             =   5970
      Visible         =   0   'False
      Width           =   2805
      Begin GIF89LibCtl.Gif89a Gif89a1 
         Height          =   1275
         Left            =   210
         OleObjectBlob   =   "sii_consumo_boletas.frx":0306
         TabIndex        =   11
         Top             =   285
         Width           =   2445
      End
   End
   Begin VB.CommandButton CmdRetornar 
      Cancel          =   -1  'True
      Caption         =   "Retornar"
      Height          =   405
      Left            =   16230
      TabIndex        =   3
      Top             =   9885
      Width           =   1665
   End
   Begin VB.Frame Frame1 
      Caption         =   "Detalle"
      Height          =   9105
      Left            =   9630
      TabIndex        =   0
      Top             =   1245
      Width           =   6075
      Begin jhTextBoxM.TextBoxM TxtNombreCertificado 
         Height          =   285
         Left            =   315
         TabIndex        =   44
         Top             =   270
         Width           =   5520
         _ExtentX        =   9737
         _ExtentY        =   503
         Text            =   ""
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   7.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Locked          =   -1  'True
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel10 
         Height          =   240
         Left            =   315
         OleObjectBlob   =   "sii_consumo_boletas.frx":038C
         TabIndex        =   26
         Top             =   570
         Width           =   1545
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel9 
         Height          =   195
         Left            =   150
         OleObjectBlob   =   "sii_consumo_boletas.frx":0402
         TabIndex        =   25
         Top             =   7560
         Width           =   1860
      End
      Begin jhTextBoxM.TextBoxM TxtTrackID 
         Height          =   330
         Left            =   705
         TabIndex        =   24
         Top             =   7770
         Width           =   1800
         _ExtentX        =   3175
         _ExtentY        =   582
         Text            =   ""
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Locked          =   -1  'True
      End
      Begin jhTextBoxM.TextBoxM TxtIva 
         Height          =   285
         Left            =   1650
         TabIndex        =   13
         TabStop         =   0   'False
         Top             =   4575
         Width           =   1320
         _ExtentX        =   2328
         _ExtentY        =   503
         TipoDeDato      =   1
         Text            =   ""
         FormatoDeMiles  =   -1  'True
         Estilo          =   1
         Alignment       =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   7.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Locked          =   -1  'True
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   225
         Left            =   585
         OleObjectBlob   =   "sii_consumo_boletas.frx":048E
         TabIndex        =   12
         Top             =   4875
         Width           =   960
      End
      Begin VB.ComboBox CboTipo 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         ItemData        =   "sii_consumo_boletas.frx":04F6
         Left            =   2295
         List            =   "sii_consumo_boletas.frx":0500
         Style           =   2  'Dropdown List
         TabIndex        =   9
         Top             =   1365
         Width           =   1125
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   240
         Left            =   150
         OleObjectBlob   =   "sii_consumo_boletas.frx":050C
         TabIndex        =   8
         Top             =   5250
         Width           =   1665
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   480
         Left            =   255
         OleObjectBlob   =   "sii_consumo_boletas.frx":0578
         TabIndex        =   6
         Top             =   8385
         Width           =   6495
      End
      Begin VB.CommandButton CmdInformaConsumo 
         Caption         =   "Informar Consumo al SII"
         Height          =   540
         Left            =   2955
         TabIndex        =   4
         Top             =   7665
         Width           =   2790
      End
      Begin VB.CommandButton CmdConsultar 
         Caption         =   "Consultar"
         Height          =   315
         Left            =   3720
         TabIndex        =   2
         Top             =   1380
         Width           =   1785
      End
      Begin MSComCtl2.DTPicker DtDesde 
         CausesValidation=   0   'False
         Height          =   375
         Left            =   300
         TabIndex        =   1
         Top             =   1365
         Width           =   1950
         _ExtentX        =   3440
         _ExtentY        =   661
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         CalendarBackColor=   16777215
         CalendarForeColor=   -2147483640
         CalendarTitleBackColor=   8388608
         CalendarTitleForeColor=   16777088
         CalendarTrailingForeColor=   8421376
         Format          =   95223809
         CurrentDate     =   41001
      End
      Begin MSComctlLib.ListView LvDetalle 
         Height          =   2145
         Left            =   225
         TabIndex        =   5
         Top             =   1785
         Width           =   5655
         _ExtentX        =   9975
         _ExtentY        =   3784
         View            =   3
         LabelWrap       =   0   'False
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   4
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "N109"
            Text            =   "Nro Boleta"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   1
            Key             =   "valor"
            Object.Tag             =   "N100"
            Text            =   "Valor"
            Object.Width           =   2646
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "N109"
            Text            =   "Cod SII"
            Object.Width           =   1411
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Object.Tag             =   "T1000"
            Text            =   "Estado"
            Object.Width           =   2540
         EndProperty
      End
      Begin MSComctlLib.ListView LvResumen 
         Height          =   2130
         Left            =   135
         TabIndex        =   7
         Top             =   5445
         Width           =   5625
         _ExtentX        =   9922
         _ExtentY        =   3757
         View            =   3
         LabelWrap       =   0   'False
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   2
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "N109"
            Text            =   "Descripcion"
            Object.Width           =   4410
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   1
            Object.Tag             =   "N100"
            Text            =   "Cantidad/Valor"
            Object.Width           =   3528
         EndProperty
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
         Height          =   225
         Left            =   585
         OleObjectBlob   =   "sii_consumo_boletas.frx":0700
         TabIndex        =   14
         Top             =   4590
         Width           =   960
      End
      Begin jhTextBoxM.TextBoxM TxtNeto 
         Height          =   285
         Left            =   1650
         TabIndex        =   15
         TabStop         =   0   'False
         Top             =   4290
         Width           =   1320
         _ExtentX        =   2328
         _ExtentY        =   503
         TipoDeDato      =   1
         Text            =   ""
         FormatoDeMiles  =   -1  'True
         Estilo          =   1
         Alignment       =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   7.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Locked          =   -1  'True
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel5 
         Height          =   225
         Left            =   600
         OleObjectBlob   =   "sii_consumo_boletas.frx":0764
         TabIndex        =   16
         Top             =   4320
         Width           =   960
      End
      Begin jhTextBoxM.TextBoxM TxtExento 
         Height          =   285
         Left            =   1650
         TabIndex        =   17
         TabStop         =   0   'False
         Top             =   4005
         Width           =   1320
         _ExtentX        =   2328
         _ExtentY        =   503
         TipoDeDato      =   1
         Text            =   ""
         FormatoDeMiles  =   -1  'True
         Estilo          =   1
         Alignment       =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   7.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Locked          =   -1  'True
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel6 
         Height          =   225
         Left            =   600
         OleObjectBlob   =   "sii_consumo_boletas.frx":07CA
         TabIndex        =   18
         Top             =   4035
         Width           =   960
      End
      Begin jhTextBoxM.TextBoxM TxtBoletas 
         Height          =   285
         Left            =   1650
         TabIndex        =   19
         TabStop         =   0   'False
         Top             =   4860
         Width           =   1320
         _ExtentX        =   2328
         _ExtentY        =   503
         TipoDeDato      =   1
         Text            =   ""
         FormatoDeMiles  =   -1  'True
         Estilo          =   1
         Alignment       =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   7.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Locked          =   -1  'True
      End
      Begin jhTextBoxM.TextBoxM TxtFolInicial 
         Height          =   285
         Left            =   4290
         TabIndex        =   20
         TabStop         =   0   'False
         Top             =   4335
         Width           =   1320
         _ExtentX        =   2328
         _ExtentY        =   503
         TipoDeDato      =   1
         Text            =   ""
         FormatoDeMiles  =   -1  'True
         Estilo          =   1
         Alignment       =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   7.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Locked          =   -1  'True
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel7 
         Height          =   225
         Left            =   3270
         OleObjectBlob   =   "sii_consumo_boletas.frx":0834
         TabIndex        =   21
         Top             =   4620
         Width           =   960
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel8 
         Height          =   225
         Left            =   3270
         OleObjectBlob   =   "sii_consumo_boletas.frx":08A8
         TabIndex        =   22
         Top             =   4395
         Width           =   960
      End
      Begin jhTextBoxM.TextBoxM TxtFolFinal 
         Height          =   285
         Left            =   4290
         TabIndex        =   23
         TabStop         =   0   'False
         Top             =   4620
         Width           =   1320
         _ExtentX        =   2328
         _ExtentY        =   503
         TipoDeDato      =   1
         Text            =   ""
         FormatoDeMiles  =   -1  'True
         Estilo          =   1
         Alignment       =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   7.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Locked          =   -1  'True
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel11 
         Height          =   240
         Left            =   4065
         OleObjectBlob   =   "sii_consumo_boletas.frx":0920
         TabIndex        =   27
         Top             =   630
         Width           =   1545
      End
      Begin jhTextBoxM.TextBoxM TxtRUTEnvia 
         Height          =   285
         Left            =   300
         TabIndex        =   28
         Top             =   840
         Width           =   1800
         _ExtentX        =   3175
         _ExtentY        =   503
         Text            =   ""
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Locked          =   -1  'True
      End
      Begin jhTextBoxM.TextBoxM TxtRutEmisor 
         Height          =   285
         Left            =   4065
         TabIndex        =   29
         Top             =   870
         Width           =   1800
         _ExtentX        =   3175
         _ExtentY        =   503
         Text            =   ""
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Locked          =   -1  'True
      End
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   0
      OleObjectBlob   =   "sii_consumo_boletas.frx":0992
      Top             =   0
   End
   Begin MSComctlLib.ListView LvEmpresas 
      Height          =   2685
      Left            =   450
      TabIndex        =   30
      Top             =   1260
      Width           =   9105
      _ExtentX        =   16060
      _ExtentY        =   4736
      View            =   3
      LabelWrap       =   0   'False
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   7
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Object.Tag             =   "N109"
         Object.Width           =   882
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Object.Tag             =   "T3000"
         Text            =   "Nombre Empresa"
         Object.Width           =   5292
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Object.Tag             =   "T1000"
         Text            =   "RUT"
         Object.Width           =   2646
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Object.Tag             =   "T1000"
         Text            =   "Host"
         Object.Width           =   3528
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Object.Tag             =   "T1000"
         Text            =   "Nombre BD"
         Object.Width           =   3528
      EndProperty
      BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   5
         Object.Tag             =   "T1000"
         Text            =   "Usuario BD"
         Object.Width           =   3528
      EndProperty
      BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   6
         Object.Tag             =   "T1000"
         Text            =   "Pwd"
         Object.Width           =   2540
      EndProperty
   End
   Begin MSComctlLib.ListView LvDocumentosDia 
      Height          =   2670
      Left            =   465
      TabIndex        =   38
      Top             =   7575
      Width           =   9015
      _ExtentX        =   15901
      _ExtentY        =   4710
      View            =   3
      LabelWrap       =   0   'False
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   7
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Object.Tag             =   "N109"
         Text            =   "Id"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Object.Tag             =   "T3000"
         Text            =   "Nombre Empresa"
         Object.Width           =   5292
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Object.Tag             =   "T1000"
         Text            =   "RUT"
         Object.Width           =   2646
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Object.Tag             =   "T1000"
         Text            =   "Host"
         Object.Width           =   3528
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Object.Tag             =   "T1000"
         Text            =   "Nombre BD"
         Object.Width           =   3528
      EndProperty
      BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   5
         Object.Tag             =   "T1000"
         Text            =   "Usuario BD"
         Object.Width           =   3528
      EndProperty
      BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   6
         Object.Tag             =   "T1000"
         Text            =   "Pwd"
         Object.Width           =   2540
      EndProperty
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel16 
      Height          =   270
      Left            =   525
      OleObjectBlob   =   "sii_consumo_boletas.frx":0BC6
      TabIndex        =   41
      Top             =   4200
      Width           =   1650
   End
   Begin MSComctlLib.ListView LvEnvios 
      Height          =   2670
      Left            =   510
      TabIndex        =   42
      Top             =   4425
      Width           =   9015
      _ExtentX        =   15901
      _ExtentY        =   4710
      View            =   3
      LabelWrap       =   0   'False
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   9
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Object.Tag             =   "N109"
         Text            =   "Dia"
         Object.Width           =   1323
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Object.Tag             =   "N109"
         Text            =   "Tipo Doc"
         Object.Width           =   1764
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Object.Tag             =   "T1000"
         Text            =   "RUT"
         Object.Width           =   2646
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Object.Tag             =   "F1000"
         Text            =   "Fecha"
         Object.Width           =   2469
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Object.Tag             =   "F1000"
         Text            =   "Publicacion"
         Object.Width           =   2469
      EndProperty
      BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   5
         Object.Tag             =   "T1000"
         Text            =   "Hora"
         Object.Width           =   2117
      EndProperty
      BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   6
         Object.Tag             =   "T1000"
         Text            =   "Track ID"
         Object.Width           =   2293
      EndProperty
      BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   7
         Text            =   "Secuencia"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   8
         Object.Tag             =   "N109"
         Text            =   "Nro Caja"
         Object.Width           =   2540
      EndProperty
   End
   Begin MSComctlLib.ListView LvRangos 
      Height          =   2145
      Left            =   16050
      TabIndex        =   51
      Top             =   7320
      Width           =   5655
      _ExtentX        =   9975
      _ExtentY        =   3784
      View            =   3
      LabelWrap       =   0   'False
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   3
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Object.Tag             =   "N109"
         Text            =   "Caja"
         Object.Width           =   2117
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   1
         Key             =   "valor"
         Object.Tag             =   "N109"
         Text            =   "Desde"
         Object.Width           =   2646
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Object.Tag             =   "N109"
         Text            =   "Hasta"
         Object.Width           =   1411
      EndProperty
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel19 
      Height          =   240
      Left            =   16155
      OleObjectBlob   =   "sii_consumo_boletas.frx":0C32
      TabIndex        =   52
      Top             =   7050
      Width           =   1545
   End
End
Attribute VB_Name = "sii_consumo_boletas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim Sm_RUT_Envia As String
Dim Sm_Fecha_Res As Date
Dim Sm_Nro_Res As String
Dim CnE As ADODB.Connection
Private Sub CmdComienzo_Click()
    If LvEmpresas.ListItems.Count = 0 Then Exit Sub
    Dim Fp_Ultimodia As Date
    Dim Sp_TrackID As String
    Fp_Ultimodia = ComAno.Text & "-" & Right("0" & comMes.ItemData(comMes.ListIndex), 2) & "-" & Right("0" & CboHasta, 2)
    If Fp_Ultimodia >= Date Then
    
        MsgBox "El ultima dia que pretende informar no puede ser igual o mayor a la fecha actual ...", vbExclamation + vbOKOnly
        Exit Sub
    End If
    FrmLoad.Visible = True
    DoEvents
    LstLog.Clear
    LstLog.AddItem Time & " ... " & "Comenzando " & comMes.Text & " " & ComAno.Text
    
    
    For p = 1 To LvEmpresas.ListItems.Count
        If Not LvEmpresas.ListItems(p).Checked Then GoTo Siguiente
        LstLog.AddItem Time & " ... " & "Conectando bd remota"
        
        '1ro recorremos las empresas
        LstLog.AddItem Time & " ... " & "Cargando emp. " & LvEmpresas.ListItems(p).SubItems(2) & " " & LvEmpresas.ListItems(p).SubItems(1)
        DatosParaEnviar LvEmpresas.ListItems(p).SubItems(2) 'llena los datos de envio, rut enviador rur emisor
        ConexionBDRcof LCase(LvEmpresas.ListItems(p).SubItems(3)), LCase(LvEmpresas.ListItems(p).SubItems(5)), LCase(LvEmpresas.ListItems(p).SubItems(4)), LCase(LvEmpresas.ListItems(p).SubItems(6))
        CargaDiasEnvios LvEmpresas.ListItems(p).SubItems(2) 'carga dia a dia y consulta track id
        LstLog.AddItem Time & " ... Cargando los dias"
        
        For a = 1 To LvEnvios.ListItems.Count
            LstLog.AddItem Time & " ... Buscando dia:" & LvEnvios.ListItems(a)
            DtDesde = LvEnvios.ListItems(a).SubItems(3)
            Sql = "SELECT doc_cod_sii,rco_fecha_publicacion,rco_hora,rco_trackid,rco_secuencia,rco_nro_caja " & _
                    "FROM sii_rcofs " & _
                    "WHERE rco_secuencia=" & CboSecuencia.Text & " AND rut_emp='" & LvEmpresas.ListItems(p).SubItems(2) & "' AND rco_fecha='" & Fql(LvEnvios.ListItems(a).SubItems(3)) & "'"
            ConsultaRcof RsTmp, Sql
            If RsTmp.RecordCount > 0 Then 'consulta dia a dia el trackid
                LstLog.List(LstLog.ListCount - 1) = LstLog.List(LstLog.ListCount - 1) & " track id " & RsTmp!rco_trackid
                LvEnvios.ListItems(a).SubItems(1) = RsTmp!doc_cod_sii
                LvEnvios.ListItems(a).SubItems(4) = RsTmp!rco_fecha_publicacion
                LvEnvios.ListItems(a).SubItems(5) = RsTmp!rco_hora
                LvEnvios.ListItems(a).SubItems(6) = RsTmp!rco_trackid
                LvEnvios.ListItems(a).SubItems(7) = RsTmp!rco_secuencia
                LvEnvios.ListItems(a).SubItems(8) = RsTmp!rco_nro_caja
            Else
                '28 Junio 2020 No hay informacior, por lo que se proceder� a al env�o
                LstLog.List(LstLog.ListCount - 1) = LstLog.List(LstLog.ListCount - 1) & " " & Time & " Preparando envio"
                
               ' If LvEnvios.ListItems(a).SubItems(1) = "39" Then CboTipo.ListIndex = 0 Else CboTipo.ListIndex = 1
                CmdConsultar_Click
                LstLog.AddItem Time & " ... Informar consumo"
                CmdInformaConsumo_Click
                LvEnvios.ListItems(a).SubItems(6) = TxtTrackID
                LstLog.AddItem Time & " ... Envio finalizado " & TxtTrackID
               ' FrmLoad.Visible = False
               ' Exit Sub
            End If
        Next
Siguiente:
    
    Next
    FrmLoad.Visible = False
    
    MsgBox "Si llegamos hasta aqui, podriamos decir que se public� todo, " & vbNewLine & " Favor verifique en el SII el registro de consumos", vbInformation
    
End Sub
Private Sub CargaDiasEnvios(Rut_Emp As String)
    Dim ii As Integer
    contt = 1
    LvEnvios.ListItems.Clear
    For ii = Val(CboDesde.Text) To Val(CboHasta.Text)
        LvEnvios.ListItems.Add contt, , ii
        LvEnvios.ListItems(LvEnvios.ListItems.Count).SubItems(3) = Right("0" & ii, 2) & "-" & Right("0" & comMes.ItemData(comMes.ListIndex), 2) & "-" & ComAno.Text
        LvEnvios.ListItems(LvEnvios.ListItems.Count).SubItems(2) = Rut_Emp
        contt = contt + 1
    Next
End Sub

Private Sub CmdConsultar_Click()
    Dim Lp_Emitidos As Long
    Dim Lp_Anulados As Long
    Dim Lp_Utilizados As Long
    FrmLoad.Visible = True
    DoEvents
    TxtTrackID = ""
    Sql = "SELECT rco_trackid " & _
            "FROM sii_rcofs " & _
            "WHERE rco_secuencia=" & CboSecuencia.Text & " AND rut_emp='" & TxtRutEmisor & "' AND doc_cod_sii=" & CboTipo.Text & " AND rco_fecha='" & Fql(DtDesde) & "'"
    ConsultaRcof RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        TxtTrackID = RsTmp!rco_trackid
    End If
    LstLog.AddItem Time & " ... Consultando dia " & DtDesde
    TxtFolInicial = 0
    TxtFolFinal = 0
    Sql = "SELECT no_documento, bruto, " & _
            "(select doc_cod_sii from sis_documentos d where v.doc_id=d.doc_id) cod_sii, " & _
            "IF(tipo_movimiento='VD','Emitida','Anulada') estado " & _
            "FROM ven_doc_venta v " & _
            "WHERE  fecha='" & Fql(DtDesde) & "' AND rut_emp='" & TxtRutEmisor & "' " & _
            "HAVING cod_sii = " & CboTipo.Text & " " & _
            "ORDER BY no_documento"
    ConsultaRcof RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvDetalle, False, True, True, False
    If RsTmp.RecordCount > 0 Then
        RsTmp.MoveFirst
        TxtFolInicial = RsTmp!no_documento
        RsTmp.MoveLast
        TxtFolFinal = RsTmp!no_documento
    End If
    
    Sql = "SELECT caj_nro_caja,    min(no_documento) desde,    max(no_documento) hasta " & _
                "FROM ven_doc_venta v " & _
                "WHERE (select doc_cod_sii from sis_documentos d where v.doc_id=d.doc_id)=" & CboTipo.Text & " AND " & _
                "fecha='" & Fql(DtDesde) & "' " & _
                "GROUP BY  caj_nro_caja"
    ConsultaRcof RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvRangos, False, True, True, False
    
    txtBoletas = NumFormat(TotalizaColumna(LvDetalle, "valor"))
    TxtNeto = NumFormat(Round(CDbl(txtBoletas) / Val("1." & DG_IVA)))
    TxtIVA = NumFormat(CDbl(txtBoletas) - CDbl(TxtNeto))
    LvResumen.ListItems.Clear
    Lp_Emitidos = 0
    Lp_Anulados = 0
    If RsTmp.RecordCount > 0 Then
        For i = 1 To LvDetalle.ListItems.Count
            If LvDetalle.ListItems(i).SubItems(3) = "EMITIDA" Then Lp_Emitidos = Lp_Emitidos + 1
            If LvDetalle.ListItems(i).SubItems(3) = "ANULADA" Then Lp_Anulados = Lp_Anulados + 1
        Next
    End If
    LvResumen.ListItems.Add , , "EMITIDOS"
    LvResumen.ListItems(LvResumen.ListItems.Count).SubItems(1) = Lp_Emitidos
    LvResumen.ListItems.Add , , "ANULADOS"
    LvResumen.ListItems(LvResumen.ListItems.Count).SubItems(1) = Lp_Anulados
    LvResumen.ListItems.Add , , "UTILIZADOS"
    LvResumen.ListItems(LvResumen.ListItems.Count).SubItems(1) = LvDetalle.ListItems.Count
    LvResumen.ListItems.Add , , "TOTAL"
    LvResumen.ListItems(LvResumen.ListItems.Count).SubItems(1) = txtBoletas
    LvResumen.ListItems.Add , , "NETO"
    LvResumen.ListItems(LvResumen.ListItems.Count).SubItems(1) = NumFormat(Round(CDbl(txtBoletas) / Val("1." & DG_IVA)))
    LvResumen.ListItems.Add , , "IVA" '5
    LvResumen.ListItems(LvResumen.ListItems.Count).SubItems(1) = TxtIVA
    LvResumen.ListItems.Add , , "TASA IVA" '6
    LvResumen.ListItems(LvResumen.ListItems.Count).SubItems(1) = DG_IVA
    LstLog.AddItem Time & " ... dia cargado"
    XML_ArchivoEnvio CboTipo.Text
    LstLog.AddItem Time & " ... XML generado"
    FrmLoad.Visible = False
End Sub

Private Sub CmdInformaConsumo_Click()
    Dim Sp_ArchivoRoc As String
    Dim g As Integer
    Dim Sp_Bat As String
    If Val(TxtTrackID) > 0 Then
        FrmLoad.Visible = False
        MsgBox "Ya se encuentra un envio de este dia, " & vbNewLine & "favor Verificar el Track ID en el SII", vbInformation + vbOKOnly, "Ya existe un envio"
        TxtTrackID.SetFocus
        Exit Sub
    End If
    Frame1.Enabled = False
    FrmLoad.Visible = True
    DoEvents
    dia = Format(Date, "YYYY-MM-DD")
    Sp_ArchivoRoc = "C:\resultados_cliente\rcofs\" & Replace(TxtRutEmisor, ".", "") & "\" & dia & "\rcof.log"
    If Dir(Sp_ArchivoRoc, vbArchive) <> "" Then
        Kill Sp_ArchivoRoc
    End If
    LstLog.AddItem Time & " ... Creando archivo Ejecutar.bat"
    
    TxtNombreCertificado = Replace(TxtNombreCertificado, "�", Chr(164))
    TxtNombreCertificado = Replace(TxtNombreCertificado, "�", Chr(165))
    
    Sp_Bat = "C:\Publicador\ejecutar.bat"
    g = FreeFile
    Open Sp_Bat For Output As g
        Print #g, "c:"
        Print #g, "cd\"
        Print #g, "cd publicador"
        Print #g, "HefPublicador.exe /re:" & Replace(TxtRutEmisor, ".", "") & " /cn:" & Chr(34) & TxtNombreCertificado & Chr(34) & " /in:" & Chr(34) & "Data\RCOF.xml" & Chr(34) & " /out:" & Chr(34) & "C:\Resultados_Cliente\RCOFS" & Chr(34); ""
        
        Print #g, "pause"
    Close g
   '     Print #X, "<?xml version=" & Chr(34) & "1.0" & Chr(34) & " encoding=" & Chr(34) & "ISO-8859-1" & Chr(34) & "?>"
   '     Print #1, "<ConsumoFolios version=" & Chr(34) & "1.0" & Chr(34) & ">"
    
    Shell "c:\publicador\ejecutar.bat"
    Do While Dir(Sp_ArchivoRoc, vbArchive) = ""
    Loop
    leerXML Sp_ArchivoRoc
    'Grabamos
    Sql = "INSERT INTO sii_rcofs (rut_emp,doc_cod_sii,rco_fecha,rco_hora,rco_trackid,rco_fecha_publicacion,rco_secuencia) " & _
                "VALUES('" & TxtRutEmisor & "'," & CboTipo.Text & ",'" & Fql(DtDesde) & "','" & Time & "','" & Me.TxtTrackID & "','" & Fql(Date) & "'," & CboSecuencia.Text & ")"
    CnE.Execute Sql
    
    Sql = "UPDATE ven_doc_venta  SET ven_informado_rcof ='SI' " & _
            "WHERE rut_emp='" & TxtRutEmisor & "' AND fecha='" & Fql(DtDesde) & "'"
    CnE.Execute Sql
    
   ' FrmLoad.Visible = False
    LstLog.AddItem Time & " ... Publicacion finalizada... " & Me.TxtTrackID
    'MsgBox "Consumo del dia publicado correctamente... " & vbNewLine & DtDesde & vbNewLine & "Tipo:" & CboTipo.Text & vbNewLine & "Track ID" & vbNewLine & Me.TxtTrackID, vbInformation, "Publicacion Exitosa:" & SP_Rut_Activo
    Frame1.Enabled = True
End Sub
Private Sub CmdRetornar_Click()
    Unload Me
End Sub
Private Function leerXML(xmlARchivo As String) As String
    Dim oParser As MSXML2.DOMDocument
    Set oParser = New MSXML2.DOMDocument
    If oParser.Load(xmlARchivo) Then
        MuestraNodos oParser.childNodes
    Else
        MsgBox "Ha ocurrido un error."
    End If
End Function
Public Sub MuestraNodos(ByRef Nodos As MSXML2.IXMLDOMNodeList)
    Dim oNodo As MSXML2.IXMLDOMNode
    For Each oNodo In Nodos
        If oNodo.nodeType = NODE_TEXT Then
            If oNodo.parentNode.nodeName = "Trackid_SII" Then
                TxtTrackID = oNodo.nodeValue
                Exit Sub
            End If
        End If
        If oNodo.hasChildNodes Then
            MuestraNodos oNodo.childNodes
        End If
    Next oNodo
End Sub

Private Sub CmdRevisar_Click()
    If LvEmpresas.ListItems.Count = 0 Then Exit Sub
    Dim Sp_TrackID As String
    FrmLoad.Visible = True
    DoEvents
    LstLog.Clear
    LstLog.AddItem Time & " ... " & "Comenzando " & comMes.Text & " " & ComAno.Text
    
    For p = 1 To LvEmpresas.ListItems.Count
        If Not LvEmpresas.ListItems(p).Checked Then GoTo Siguiente
        LstLog.AddItem Time & " ... " & "Conectando bd remota"
        
        '1ro recorremos las empresas
        LstLog.AddItem Time & " ... " & "Cargando emp. " & LvEmpresas.ListItems(p).SubItems(2) & " " & LvEmpresas.ListItems(p).SubItems(1)
        DatosParaEnviar LvEmpresas.ListItems(p).SubItems(2) 'llena los datos de envio, rut enviador rur emisor
        ConexionBDRcof LCase(LvEmpresas.ListItems(p).SubItems(3)), LCase(LvEmpresas.ListItems(p).SubItems(5)), LCase(LvEmpresas.ListItems(p).SubItems(4)), LCase(LvEmpresas.ListItems(p).SubItems(6))
        CargaDiasEnvios LvEmpresas.ListItems(p).SubItems(2) 'carga dia a dia y consulta track id
        LstLog.AddItem Time & " ... Cargando los dias"
        
        For a = 1 To LvEnvios.ListItems.Count
            LstLog.AddItem Time & " ... Buscando dia:" & LvEnvios.ListItems(a)
            DtDesde = LvEnvios.ListItems(a).SubItems(3)
            Sql = "SELECT doc_cod_sii,rco_fecha_publicacion,rco_hora,rco_trackid,rco_secuencia,rco_nro_caja " & _
                    "FROM sii_rcofs " & _
                    "WHERE rco_secuencia=" & CboSecuencia.Text & " AND rut_emp='" & LvEmpresas.ListItems(p).SubItems(2) & "' AND rco_fecha='" & Fql(LvEnvios.ListItems(a).SubItems(3)) & "'"
            ConsultaRcof RsTmp, Sql
            If RsTmp.RecordCount > 0 Then 'consulta dia a dia el trackid
                LstLog.List(LstLog.ListCount - 1) = LstLog.List(LstLog.ListCount - 1) & " track id " & RsTmp!rco_trackid
                LvEnvios.ListItems(a).SubItems(1) = RsTmp!doc_cod_sii
                LvEnvios.ListItems(a).SubItems(4) = RsTmp!rco_fecha_publicacion
                LvEnvios.ListItems(a).SubItems(5) = RsTmp!rco_hora
                LvEnvios.ListItems(a).SubItems(6) = RsTmp!rco_trackid
                LvEnvios.ListItems(a).SubItems(7) = RsTmp!rco_secuencia
                LvEnvios.ListItems(a).SubItems(8) = RsTmp!rco_nro_caja
            End If
        Next
Siguiente:
        
    Next
    FrmLoad.Visible = False
        
End Sub


Private Sub comMes_Validate(Cancel As Boolean)
    CargaDiasAConsumir
End Sub

Private Sub Form_Load()
    DtDesde = Date
   ' Me.Show
    Centrar Me
    Skin2 Me, , 4
    For i = 1 To 12
        comMes.AddItem UCase(MonthName(i, False))
        comMes.ItemData(comMes.ListCount - 1) = i
    Next
    'if ig_mes_contable =
    Busca_Id_Combo comMes, Val(IG_Mes_Contable)
    
    If comMes.ListIndex = -1 Then
        comMes.ListIndex = Val(IG_Mes_Contable) - 1
    End If
    LLenaYears ComAno, 2010
    CboTipo.ListIndex = 0
    CargaDiasAConsumir

    
    Sql = "SELECT irc_id,irc_nombre_empresa,rut_emp,irc_host,irc_bd,irc_user,irc_pwd " & _
            "FROM sii_empresas_informar_consumo " & _
            "WHERE irc_activo='SI'"
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvEmpresas, True, True, True, False
    CboSecuencia.ListIndex = 0
End Sub
Private Sub CargaDiasAConsumir()
    CboDesde.Clear
    CboHasta.Clear
    For i = 1 To Day(UltimoDiaMes(ComAno.Text & "-" & comMes.ItemData(comMes.ListIndex) & "-01"))
        CboDesde.AddItem i
        CboHasta.AddItem i
    Next
End Sub
Private Sub DatosParaEnviar(Rut_Emp As String)
    LstLog.AddItem Time & " ... Cargando datos de envio " & Rut_Emp
    Sql = "SELECT * " & _
            "FROM sii_datos_empresa " & _
            "WHERE sii_rut_emisor='" & Replace(Rut_Emp, ".", "") & "'"
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
         Sm_RUT_Envia = Trim(RsTmp!sii_rut_envia)
         Sm_Fecha_Res = RsTmp!sii_fecha_res
         Sm_Nro_Res = RsTmp!sii_nro_res
         TxtRUTEnvia = Sm_RUT_Envia
         TxtRutEmisor = Rut_Emp
         Me.TxtNombreCertificado = RsTmp!sii_nombre_certificado
    End If
End Sub
Private Sub XML_ArchivoEnvio(Tipo As String)
    Dim Sp_Xml As String
    Dim Lp_Sangria As Long
    Dim Sp_Sql As String
    Dim Dp_Total As Long
    Dim dsctoGeneral As Long
    On Error GoTo ErrorGenerarXML
    '22 Mayo 2019 Genera XML para procesar boleta
    'Modulo genera xml boleta electronica
    'Autor: AVV
    '1ro Crear xml
    Lp_Sangria = 4
    X = FreeFile
    dsctoGeneral = 0
    Sp_Xml = "C:\Publicador\data\RCOF.xml"
    Open Sp_Xml For Output As X
        Print #X, "<?xml version=" & Chr(34) & "1.0" & Chr(34) & " encoding=" & Chr(34) & "ISO-8859-1" & Chr(34) & "?>"
        Print #1, "<ConsumoFolios version=" & Chr(34) & "1.0" & Chr(34) & ">"
        Print #X, "" & Space(Lp_Sangria) & "<DocumentoConsumoFolios ID=" & Chr(34) & "HEF_CF" & Chr(34) & ">"
        Print #X, "" & Space(Lp_Sangria * 2) & "<Caratula version=" & Chr(34) & "1.0" & Chr(34) & ">"
                Print #X, "" & Space(Lp_Sangria * 3) & "<RutEmisor>" & Replace(TxtRutEmisor, ".", "") & "</RutEmisor>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<RutEnvia>" & Sm_RUT_Envia & "</RutEnvia>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<FchResol>" & Fql(Sm_Fecha_Res) & "</FchResol>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<NroResol>" & Trim(Sm_Nro_Res) & "</NroResol>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<FchInicio>" & Fql(DtDesde) & "</FchInicio>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<FchFinal>" & Fql(DtDesde) & "</FchFinal>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<Correlativo>1</Correlativo>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<SecEnvio>" & CboSecuencia.Text & "</SecEnvio>"
                hora = Time
                If InStr(1, hora, ":") = 2 Then
                    hora = "0" & hora
                End If
                hora = Mid(hora, 1, 8)
                Print #X, "" & Space(Lp_Sangria * 3) & "<TmstFirmaEnv>" & Fql(Date) & "T" & hora & "</TmstFirmaEnv>"
        Print #X, "" & Space(Lp_Sangria * 2) & "</Caratula>"
        Print #X, "" & Space(Lp_Sangria * 2) & "<Resumen>"
            Print #X, "" & Space(Lp_Sangria * 3) & "<TipoDocumento>" & Trim(Tipo) & "</TipoDocumento>"
            Print #X, "" & Space(Lp_Sangria * 3) & "<MntNeto>" & CDbl(LvResumen.ListItems(5).SubItems(1)) & "</MntNeto>"
            Print #X, "" & Space(Lp_Sangria * 3) & "<MntIva>" & CDbl(LvResumen.ListItems(6).SubItems(1)) & "</MntIva>"
            'Print #X, "" & Space(Lp_Sangria * 3) & "<TasaIva>" & CDbl(LvResumen.ListItems(7).SubItems(1)) & "</TasaIva>"
            Print #X, "" & Space(Lp_Sangria * 3) & "<MntExento>0</MntExento>"
            Print #X, "" & Space(Lp_Sangria * 3) & "<MntTotal>" & CDbl(LvResumen.ListItems(4).SubItems(1)) & "</MntTotal>"
            Print #X, "" & Space(Lp_Sangria * 3) & "<FoliosEmitidos>" & CDbl(LvResumen.ListItems(1).SubItems(1)) & "</FoliosEmitidos>"
            Print #X, "" & Space(Lp_Sangria * 3) & "<FoliosAnulados>" & CDbl(LvResumen.ListItems(2).SubItems(1)) & "</FoliosAnulados>"
            Print #X, "" & Space(Lp_Sangria * 3) & "<FoliosUtilizados>" & CDbl(LvResumen.ListItems(3).SubItems(1)) & "</FoliosUtilizados>"
            If Val(TxtFolInicial) > 0 Then
                'esto soluciona las multiples cajas.
                '12 agosot 2020
                For w = 1 To Me.LvRangos.ListItems.Count
                    Print #X, "" & Space(Lp_Sangria * 3) & "<RangoUtilizados>"
                    Print #X, "" & Space(Lp_Sangria * 4) & "<Inicial>" & Me.LvRangos.ListItems(w).SubItems(1) & "</Inicial>"
                    Print #X, "" & Space(Lp_Sangria * 4) & "<Final>" & Me.LvRangos.ListItems(w).SubItems(2) & "</Final>"
                    Print #X, "" & Space(Lp_Sangria * 3) & "</RangoUtilizados>"
                
                Next
            
                'Print #x, "" & Space(Lp_Sangria * 3) & "<RangoUtilizados>"
                'Print #x, "" & Space(Lp_Sangria * 4) & "<Inicial>" & Me.TxtFolInicial & "</Inicial>"
                'Print #x, "" & Space(Lp_Sangria * 4) & "<Final>" & Me.TxtFolFinal & "</Final>"
                'Print #x, "" & Space(Lp_Sangria * 3) & "</RangoUtilizados>"
            End If
        Print #X, "" & Space(Lp_Sangria * 2) & "</Resumen>"
        Print #X, "" & Space(Lp_Sangria) & "</DocumentoConsumoFolios>"
        Print #X, "</ConsumoFolios>"
    Close #X
   ' MsgBox "Archivo generado ok", vbInformation
    Exit Sub
ErrorGenerarXML:
    MsgBox "Se produjo un error al generar XML de boleta..." & vbNewLine & Err.Description & vbNewLine & Err.Number, vbCritical
End Sub
Function ConexionBDRcof(Host As String, User As String, Bd As String, Pwd As String)
            On Error GoTo FalloConexion
                Connection_String = "PROVIDER=MSDASQL;dsn=db_redmar;uid=" & User & ";pwd=" & Pwd & ";server=" & Host & ";database=" & Bd & ";port=" & Sp_PuertoSql
                Set CnE = New ADODB.Connection '  ' Nuevo objeto Connection
                CnE.CursorLocation = adUseClient
                CnE.ConnectionTimeout = 30
                CnE.Open Connection_String '  'Abre la conexi�n
            Exit Function
FalloConexion:
    MsgBox "No fue posible establecer la conecci�n con el Servidor..." & vbNewLine & "Verifique que la Ip ingresada sea la correcta" & vbNewLine & PwdDb & "." & UserDb & "." & SG_BaseDato & "." & Sp_Servidor, vbInformation
    Exit Function
End Function
Function ConsultaRcof(RecSet As ADODB.Recordset, CSQL As String)
InicioConsulta:
            IS_IntentosConexion = IS_IntentosConexion + 1
             Set RecSet = New ADODB.Recordset '  'Nuevo recordset
            On Error GoTo FalloConsulta
            RecSet.Open CSQL, CnE, adOpenStatic, adLockOptimistic, adAsyncFetch  '  ' abre
 IS_IntentosConexion = 0
            Exit Function
FalloConsulta:
 '   If IS_IntentosConexion < 2 Then
 '       ConexionBDRcof
 '       GoTo InicioConsulta
 '   End If
    If EjecucionIDE Then
        MsgBox "Fallo la consulta a la base de datos " & vbNewLine & CSQL & vbNewLine & Err.Number & " - " & Err.Description
    Else
        MsgBox "Fallo la consulta a la base de datos " & vbNewLine & vbNewLine & Err.Number & " - " & Err.Description
    End If
    If Err.Number = 1146 Then
        MsgBox "tabla no exitse"
    End If
    If IS_IntentosConexion = 2 Then End
End Function


