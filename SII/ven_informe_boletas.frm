VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{28D47522-CF84-11D1-834C-00A0249F0C28}#1.0#0"; "gif89.dll"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form ven_informe_boletas 
   Caption         =   "Informe Boletas"
   ClientHeight    =   11190
   ClientLeft      =   3810
   ClientTop       =   2370
   ClientWidth     =   16245
   LinkTopic       =   "Form1"
   ScaleHeight     =   11190
   ScaleWidth      =   16245
   Begin VB.CommandButton Command1 
      Appearance      =   0  'Flat
      Caption         =   "Retornar"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   13770
      TabIndex        =   17
      Top             =   9765
      Width           =   2010
   End
   Begin VB.Frame FrmLoad 
      Height          =   1680
      Left            =   5505
      TabIndex        =   14
      Top             =   4515
      Visible         =   0   'False
      Width           =   2805
      Begin GIF89LibCtl.Gif89a Gif89a1 
         Height          =   1275
         Left            =   210
         OleObjectBlob   =   "ven_informe_boletas.frx":0000
         TabIndex        =   15
         Top             =   285
         Width           =   2445
      End
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   390
      OleObjectBlob   =   "ven_informe_boletas.frx":0086
      Top             =   10980
   End
   Begin VB.CommandButton CmdInforme 
      Caption         =   "Ver Informe"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1365
      Left            =   12300
      TabIndex        =   10
      Top             =   900
      Width           =   3705
   End
   Begin VB.Frame frmOts 
      Caption         =   "Listado historico"
      Height          =   7260
      Left            =   300
      TabIndex        =   7
      Top             =   2340
      Width           =   15765
      Begin VB.TextBox TxtCant 
         Alignment       =   1  'Right Justify
         BackColor       =   &H0000FF00&
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   405
         Left            =   12450
         TabIndex        =   19
         Text            =   "0"
         Top             =   6780
         Width           =   975
      End
      Begin VB.TextBox txtTotal 
         Alignment       =   1  'Right Justify
         BackColor       =   &H0000FF00&
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   405
         Left            =   13545
         TabIndex        =   18
         Text            =   "0"
         Top             =   6780
         Width           =   1920
      End
      Begin MSComctlLib.ListView LvDetalle 
         Height          =   6420
         Left            =   90
         TabIndex        =   8
         Top             =   345
         Width           =   11925
         _ExtentX        =   21034
         _ExtentY        =   11324
         View            =   3
         LabelWrap       =   0   'False
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   13
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "N109"
            Text            =   "Dia"
            Object.Width           =   1587
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   2
            SubItemIndex    =   1
            Object.Tag             =   "N109"
            Text            =   "Nro Caja"
            Object.Width           =   1587
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "T1000"
            Text            =   "Caja"
            Object.Width           =   4057
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   3
            Object.Tag             =   "N109"
            Text            =   "Desde"
            Object.Width           =   1587
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   4
            Object.Tag             =   "N109"
            Text            =   "hasta"
            Object.Width           =   1587
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Object.Tag             =   "N100"
            Text            =   "Cant"
            Object.Width           =   1411
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   6
            Key             =   "total"
            Object.Tag             =   "N100"
            Text            =   "Total"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   7
            Object.Tag             =   "N109"
            Text            =   "Nro Caja"
            Object.Width           =   1587
         EndProperty
         BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   8
            Object.Tag             =   "T2000"
            Text            =   "Descripcion"
            Object.Width           =   3528
         EndProperty
         BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   9
            Object.Tag             =   "N109"
            Text            =   "Inicio"
            Object.Width           =   1499
         EndProperty
         BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   10
            Object.Tag             =   "N109"
            Text            =   "Fin"
            Object.Width           =   1587
         EndProperty
         BeginProperty ColumnHeader(12) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   2
            SubItemIndex    =   11
            Object.Tag             =   "N109"
            Text            =   "Cant"
            Object.Width           =   1499
         EndProperty
         BeginProperty ColumnHeader(13) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   12
            Object.Tag             =   "N100"
            Text            =   "Venta"
            Object.Width           =   1764
         EndProperty
      End
      Begin MSComctlLib.ListView LvResumen 
         Height          =   6405
         Left            =   12090
         TabIndex        =   16
         Top             =   345
         Width           =   3375
         _ExtentX        =   5953
         _ExtentY        =   11298
         View            =   3
         LabelWrap       =   0   'False
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   3
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "N109"
            Text            =   "Dia"
            Object.Width           =   1587
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Key             =   "cant"
            Object.Tag             =   "N100"
            Text            =   "Cant"
            Object.Width           =   1411
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   2
            Key             =   "total"
            Object.Tag             =   "N100"
            Text            =   "Total"
            Object.Width           =   2117
         EndProperty
      End
   End
   Begin VB.Frame Frame6 
      Caption         =   "Documento"
      Height          =   750
      Left            =   3705
      TabIndex        =   5
      Top             =   900
      Width           =   2355
      Begin VB.ComboBox CboDocVenta 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   105
         Style           =   2  'Dropdown List
         TabIndex        =   6
         Top             =   315
         Width           =   2130
      End
   End
   Begin VB.Frame Frame4 
      Caption         =   "Mes y A�o"
      Height          =   1395
      Left            =   315
      TabIndex        =   0
      Top             =   855
      Width           =   2970
      Begin VB.ComboBox CboHasta 
         Height          =   315
         Left            =   1740
         Style           =   2  'Dropdown List
         TabIndex        =   13
         Top             =   915
         Width           =   885
      End
      Begin VB.ComboBox CboDesde 
         Height          =   315
         Left            =   345
         Style           =   2  'Dropdown List
         TabIndex        =   12
         Top             =   900
         Width           =   885
      End
      Begin VB.ComboBox comMes 
         Height          =   315
         ItemData        =   "ven_informe_boletas.frx":02BA
         Left            =   135
         List            =   "ven_informe_boletas.frx":02BC
         Style           =   2  'Dropdown List
         TabIndex        =   2
         Top             =   450
         Width           =   1575
      End
      Begin VB.ComboBox ComAno 
         Height          =   315
         Left            =   1710
         Style           =   2  'Dropdown List
         TabIndex        =   1
         Top             =   435
         Width           =   1215
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
         Height          =   255
         Left            =   120
         OleObjectBlob   =   "ven_informe_boletas.frx":02BE
         TabIndex        =   3
         Top             =   255
         Width           =   375
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel5 
         Height          =   255
         Left            =   1725
         OleObjectBlob   =   "ven_informe_boletas.frx":0322
         TabIndex        =   4
         Top             =   255
         Width           =   375
      End
   End
   Begin MSComctlLib.ListView LvCajas 
      Height          =   1365
      Left            =   6195
      TabIndex        =   9
      ToolTipText     =   "Estas son las formas de pago"
      Top             =   900
      Width           =   6030
      _ExtentX        =   10636
      _ExtentY        =   2408
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   0   'False
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   2
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Object.Tag             =   "N100"
         Text            =   "Nro"
         Object.Width           =   1764
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Object.Tag             =   "T3000"
         Text            =   "Descripcion"
         Object.Width           =   8819
      EndProperty
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkEmpresaActiva 
      Height          =   510
      Left            =   0
      OleObjectBlob   =   "ven_informe_boletas.frx":0386
      TabIndex        =   11
      Top             =   0
      Width           =   14940
   End
End
Attribute VB_Name = "ven_informe_boletas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub CmdInforme_Click()
  
    FrmLoad.Visible = True
    DoEvents
    LvDetalle.ListItems.Clear
    LvResumen.ListItems.Clear
    For i = Me.CboDesde.Text To CboHasta.Text
        LvDetalle.ListItems.Add , , i
        LvDetalle.ListItems(i).SubItems(1) = LvCajas.ListItems(1)
        LvDetalle.ListItems(i).SubItems(2) = LvCajas.ListItems(1).SubItems(1)
        LvDetalle.ListItems(i).SubItems(3) = 0
        LvDetalle.ListItems(i).SubItems(4) = 0
        LvDetalle.ListItems(i).SubItems(5) = 0
        LvDetalle.ListItems(i).SubItems(6) = 0
    Next
    For i = Me.CboDesde.Text To CboHasta.Text
        LvResumen.ListItems.Add , , i
        LvResumen.ListItems(i).SubItems(1) = 0
        LvResumen.ListItems(i).SubItems(2) = 0
    Next
    For c = 1 To LvCajas.ListItems.Count
        If c = 1 Then
                 Sql = "SELECT DAY(FECHA) dia, MIN(no_documento) inicio,MAX(no_documento) fin, COUNT(no_documento) cant,SUM(bruto) venta " & _
                                 "FROM ven_doc_venta " & _
                                 "WHERE  doc_id = " & CboDocVenta.ItemData(CboDocVenta.ListIndex) & " AND caj_nro_caja = 1 AND Month(Fecha) = " & comMes.ItemData(comMes.ListIndex) & " AND Year(Fecha) = " & ComAno.Text & " " & _
                                 "GROUP BY fecha " & _
                                 "ORDER BY DAY(FECHA)"
                 Consulta RsTmp, Sql
                 If RsTmp.RecordCount > 0 Then
                     RsTmp.MoveFirst
                     Do While Not RsTmp.EOF
                         Bp_Found = False
                         For i = 1 To LvDetalle.ListItems.Count
                             If Val(LvDetalle.ListItems(i)) = Val(RsTmp!dia) Then
                                 LvDetalle.ListItems(i).SubItems(3) = RsTmp!inicio
                                 LvDetalle.ListItems(i).SubItems(4) = RsTmp!fin
                                 LvDetalle.ListItems(i).SubItems(5) = RsTmp!cant
                                 LvDetalle.ListItems(i).SubItems(6) = NumFormat(RsTmp!venta)
                                 LvResumen.ListItems(i).SubItems(1) = RsTmp!cant
                                 LvResumen.ListItems(i).SubItems(2) = NumFormat(RsTmp!venta)
                                 Exit For
                             End If
                         Next
                         RsTmp.MoveNext
                     Loop
                 End If
        ElseIf c = 2 Then
                For i = Me.CboDesde.Text To CboHasta.Text
               '     LvDetalle.ListItems.Add , , i
                    LvDetalle.ListItems(i).SubItems(7) = LvCajas.ListItems(2)
                    LvDetalle.ListItems(i).SubItems(8) = LvCajas.ListItems(2).SubItems(1)
                    LvDetalle.ListItems(i).SubItems(9) = 0
                    LvDetalle.ListItems(i).SubItems(10) = 0
                    LvDetalle.ListItems(i).SubItems(11) = 0
                    LvDetalle.ListItems(i).SubItems(12) = 0
                    
                Next
            
            
                  Sql = "SELECT DAY(FECHA) dia, MIN(no_documento) inicio,MAX(no_documento) fin, COUNT(no_documento) cant,SUM(bruto) venta " & _
                                "FROM ven_doc_venta " & _
                                "WHERE  doc_id = " & CboDocVenta.ItemData(CboDocVenta.ListIndex) & " AND caj_nro_caja = 2 AND Month(Fecha) = " & comMes.ItemData(comMes.ListIndex) & " AND Year(Fecha) = " & ComAno.Text & " " & _
                                "GROUP BY fecha " & _
                                "ORDER BY DAY(FECHA)"
                Consulta RsTmp, Sql
                If RsTmp.RecordCount > 0 Then
                    RsTmp.MoveFirst
                    Do While Not RsTmp.EOF
                        Bp_Found = False
                        For i = 1 To LvDetalle.ListItems.Count
                            If Val(LvDetalle.ListItems(i)) = Val(RsTmp!dia) Then
                                LvDetalle.ListItems(i).SubItems(9) = RsTmp!inicio
                                LvDetalle.ListItems(i).SubItems(10) = RsTmp!fin
                                LvDetalle.ListItems(i).SubItems(11) = RsTmp!cant
                                LvDetalle.ListItems(i).SubItems(12) = NumFormat(RsTmp!venta)
                                LvResumen.ListItems(i).SubItems(1) = Val(LvResumen.ListItems(i).SubItems(1)) + RsTmp!cant
                                LvResumen.ListItems(i).SubItems(2) = NumFormat(CDbl(LvResumen.ListItems(i).SubItems(2)) + RsTmp!venta)
                                Exit For
                            End If
                        Next
                        RsTmp.MoveNext
                    Loop
                End If
         End If
        
        
   Next
   
   
   txtTotal = NumFormat(TotalizaColumna(LvResumen, "total"))
   TxtCant = NumFormat(TotalizaColumna(LvResumen, "cant"))
   
    FrmLoad.Visible = False
End Sub

Private Sub Command1_Click()
    Unload Me
End Sub

Private Sub comMes_Validate(Cancel As Boolean)
    CargaLosDias
End Sub

Private Sub Form_Load()
    Centrar Me
    Skin2 Me, , 6
    'FILTRO ESTADO DE PAGOS DE DOCUMENTOS
    Me.Height = 10605
    Me.Width = 16500
    For i = 1 To 12
        comMes.AddItem UCase(MonthName(i, False))
        comMes.ItemData(comMes.ListCount - 1) = i
    Next
    Busca_Id_Combo comMes, Val(IG_Mes_Contable)
    LLenaYears ComAno, 2010
   
   CargaLosDias
    Sql = "SELECT caj_nro,caj_nombre " & _
            "FROM par_cajas " & _
            "WHERE caj_activo = 'SI'"
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvCajas, False, True, True, False
    SkEmpresaActiva = Principal.SkEmpresa
      LLenarCombo CboDocVenta, "doc_nombre", "doc_id", "sis_documentos", "doc_activo='SI' AND doc_documento='VENTA' AND doc_utiliza_en_caja='" & SG_Funcion_Equipo & "'", "doc_orden"
    CboDocVenta.ListIndex = 0
End Sub
Private Sub CargaLosDias()
  For i = 1 To Day(UltimoDiaMes(ComAno.Text & "-" & comMes.ItemData(comMes.ListIndex) & "-01"))
        CboDesde.AddItem i
        CboHasta.AddItem i
    Next
    CboDesde.ListIndex = 0
    CboHasta.ListIndex = CboHasta.ListCount - 1
End Sub
