VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{DA729E34-689F-49EA-A856-B57046630B73}#1.0#0"; "progressbar-xp.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form Mantenedor_Empresas 
   Caption         =   "Mantenedor de Empresas"
   ClientHeight    =   8910
   ClientLeft      =   3525
   ClientTop       =   1620
   ClientWidth     =   15000
   LinkTopic       =   "Form1"
   ScaleHeight     =   8910
   ScaleWidth      =   15000
   Begin VB.Frame FraProgreso 
      Height          =   540
      Left            =   2490
      TabIndex        =   12
      Top             =   8115
      Visible         =   0   'False
      Width           =   8835
      Begin Proyecto2.XP_ProgressBar pbar 
         Height          =   255
         Left            =   105
         TabIndex        =   13
         Top             =   180
         Width           =   8565
         _ExtentX        =   15108
         _ExtentY        =   450
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BrushStyle      =   0
         Color           =   16777088
         Scrolling       =   1
         ShowText        =   -1  'True
      End
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Exportar a Excel"
      Height          =   495
      Left            =   450
      TabIndex        =   11
      Top             =   8145
      Width           =   1815
   End
   Begin VB.Frame Frame2 
      Caption         =   "Empresas"
      Height          =   6780
      Left            =   195
      TabIndex        =   7
      Top             =   1245
      Width           =   14520
      Begin VB.CommandButton CmdEditarCliente 
         Caption         =   "Editar Seleccionada"
         Height          =   495
         Left            =   2100
         TabIndex        =   9
         Top             =   570
         Width           =   1815
      End
      Begin VB.CommandButton CmdNuevoCliente 
         Caption         =   "Crear Nueva Empresa"
         Height          =   495
         Left            =   255
         TabIndex        =   8
         Top             =   570
         Width           =   1815
      End
      Begin MSComctlLib.ListView LvDetalle 
         Height          =   4995
         Left            =   300
         TabIndex        =   10
         Top             =   1095
         Width           =   14085
         _ExtentX        =   24844
         _ExtentY        =   8811
         View            =   3
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   13
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "T1000"
            Text            =   "Rut"
            Object.Width           =   2293
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "T1300"
            Text            =   "Razon Social"
            Object.Width           =   4410
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "T800"
            Text            =   "Direccion"
            Object.Width           =   4410
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Object.Tag             =   "T3000"
            Text            =   "Comuna"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Object.Tag             =   "T800"
            Text            =   "Ciudad"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Object.Tag             =   "T800"
            Text            =   "Fono"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   6
            Object.Tag             =   "T1500"
            Text            =   "fax"
            Object.Width           =   2646
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   7
            Object.Tag             =   "T1500"
            Text            =   "email"
            Object.Width           =   3528
         EndProperty
         BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   8
            Object.Tag             =   "N109"
            Text            =   "Giro"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   9
            Object.Tag             =   "T2000"
            Text            =   "Rut RL"
            Object.Width           =   3528
         EndProperty
         BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   10
            Object.Tag             =   "T2000"
            Text            =   "Nombre RL"
            Object.Width           =   3528
         EndProperty
         BeginProperty ColumnHeader(12) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   11
            Object.Tag             =   "T1200"
            Text            =   "Ctrl inventario"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(13) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   12
            Object.Tag             =   "T500"
            Text            =   "Activo"
            Object.Width           =   1411
         EndProperty
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Busqueda"
      Height          =   1095
      Left            =   9255
      TabIndex        =   1
      Top             =   135
      Width           =   5415
      Begin VB.CommandButton CmdTodos 
         Caption         =   "Todos"
         Height          =   480
         Left            =   4185
         TabIndex        =   6
         Top             =   315
         Width           =   975
      End
      Begin VB.CommandButton CmdBuscaCliente 
         Caption         =   "&Buscar"
         Height          =   480
         Left            =   3120
         TabIndex        =   5
         Top             =   345
         Width           =   975
      End
      Begin VB.OptionButton OpRut 
         Caption         =   "RUT Empresa"
         Height          =   255
         Left            =   1680
         TabIndex        =   4
         Top             =   720
         Width           =   1575
      End
      Begin VB.OptionButton OpRsocial 
         Caption         =   "Raz�n Social"
         Height          =   255
         Left            =   120
         TabIndex        =   3
         Top             =   720
         Value           =   -1  'True
         Width           =   1695
      End
      Begin VB.TextBox TxtBusqueda 
         Height          =   405
         Left            =   120
         TabIndex        =   2
         Top             =   240
         Width           =   2775
      End
   End
   Begin VB.CommandButton CmdSalir 
      Cancel          =   -1  'True
      Caption         =   "&Retornar"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   12855
      TabIndex        =   0
      Top             =   8205
      Width           =   1815
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   615
      OleObjectBlob   =   "Mantenedor_Empresas.frx":0000
      Top             =   7485
   End
End
Attribute VB_Name = "Mantenedor_Empresas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub CmdEditarCliente_Click()
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    SG_codigo = LvDetalle.SelectedItem.Text
    AgregoEmpresa.Show 1
    CargaEmpresas
End Sub

Private Sub CmdNuevoCliente_Click()
    SG_codigo = Empty
    AgregoEmpresa.Show 1
    CargaEmpresas
End Sub

Private Sub CmdSalir_Click()
    Unload Me
End Sub

Private Sub CargaEmpresas()
    Sql = "SELECT a.rut,a.nombre_empresa,a.direccion,a.comuna,a.ciudad,a.fono,a.fax,a.email,a.giro,a.rut_rl,a.nombre_rl,a.control_inventario,a.activo " & _
          "FROM sis_empresas AS a"
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvDetalle, False, True, True, False
End Sub

Private Sub CmdTodos_Click()
    Sql = "SELECT a.rut,a.nombre_empresa,a.direccion,a.comuna,a.ciudad,a.fono,a.fax,a.email,a.giro,a.rut_rl,a.nombre_rl,a.control_inventario,a.activo " & _
          "FROM sis_empresas AS a"
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvDetalle, False, True, True, False
End Sub

Private Sub Command1_Click()
    Dim tit(2) As String
    FraProgreso.Visible = True
    tit(0) = Me.Caption
    tit(1) = "FECHA INFORME: " & Date & " - " & Time
    tit(2) = "" ' "                                        DESDE:" & DtInicio.Value & "   HASTA:" & Dtfin.Value
    ExportarNuevo LvDetalle, tit, Me, PBar
    PBar.Value = 1
    FraProgreso.Visible = False
End Sub

Private Sub Form_Load()
    Centrar Me
    Aplicar_skin Me
    CargaEmpresas
End Sub




Private Sub TxtBusqueda_Change()
    Sql = "SELECT a.rut,a.nombre_empresa,a.direccion,a.comuna,a.ciudad,a.fono,a.fax,a.email,a.giro,a.rut_rl,a.nombre_rl,a.control_inventario,a.activo " & _
             "FROM sis_empresas AS a " & _
             "WHERE a.nombre_empresa LIKE '" & TxtBusqueda & "%'"
        Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvDetalle, False, True, True, False
End Sub

Private Sub TxtBusqueda_KeyPress(KeyAscii As Integer)
     KeyAscii = Asc(UCase(Chr(KeyAscii)))
     If KeyAscii = 39 Then KeyAscii = 0
End Sub
Private Sub LvDetalle_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    ordListView ColumnHeader, Me, LvDetalle
End Sub

Private Sub TxtBusqueda_Validate(Cancel As Boolean)
TxtBusqueda = Replace(TxtBusqueda, "'", "")
End Sub
