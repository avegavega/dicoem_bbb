VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form mantenedor_Usuarios 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Usuarios"
   ClientHeight    =   4800
   ClientLeft      =   2445
   ClientTop       =   3330
   ClientWidth     =   11490
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4800
   ScaleWidth      =   11490
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton cmdSalir 
      Cancel          =   -1  'True
      Caption         =   "&Retornar"
      Height          =   465
      Left            =   10155
      TabIndex        =   7
      Top             =   4140
      Width           =   1185
   End
   Begin VB.Timer Timer1 
      Interval        =   10
      Left            =   750
      Top             =   4185
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   90
      OleObjectBlob   =   "mantenedor_Usuarios.frx":0000
      Top             =   4215
   End
   Begin VB.Frame Frame5 
      Caption         =   "Usuarios"
      Height          =   3375
      Left            =   285
      TabIndex        =   8
      Top             =   645
      Width           =   11100
      Begin VB.ComboBox CboVePCosto 
         Appearance      =   0  'Flat
         Height          =   315
         ItemData        =   "mantenedor_Usuarios.frx":0234
         Left            =   8775
         List            =   "mantenedor_Usuarios.frx":023E
         Style           =   2  'Dropdown List
         TabIndex        =   17
         Top             =   720
         Width           =   1560
      End
      Begin VB.TextBox TxtNombre 
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   840
         TabIndex        =   0
         Tag             =   "T"
         Top             =   720
         Width           =   1815
      End
      Begin VB.TextBox TxtLogin 
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   2670
         TabIndex        =   1
         Tag             =   "T"
         Top             =   720
         Width           =   1290
      End
      Begin VB.TextBox TxtPassword 
         Appearance      =   0  'Flat
         Height          =   315
         IMEMode         =   3  'DISABLE
         Left            =   3975
         PasswordChar    =   "*"
         TabIndex        =   2
         Tag             =   "T"
         Top             =   720
         Width           =   1170
      End
      Begin VB.ComboBox ComPermisos 
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   5160
         Style           =   2  'Dropdown List
         TabIndex        =   3
         Top             =   720
         Width           =   2535
      End
      Begin VB.ComboBox ComActivo 
         Appearance      =   0  'Flat
         Height          =   315
         ItemData        =   "mantenedor_Usuarios.frx":024A
         Left            =   7680
         List            =   "mantenedor_Usuarios.frx":0254
         Style           =   2  'Dropdown List
         TabIndex        =   4
         Top             =   720
         Width           =   1095
      End
      Begin VB.CommandButton CmdOk 
         Caption         =   "Ok"
         Height          =   315
         Left            =   10350
         TabIndex        =   5
         Top             =   720
         Width           =   495
      End
      Begin VB.TextBox TxtIdUsuario 
         Appearance      =   0  'Flat
         BackColor       =   &H80000004&
         Height          =   315
         Left            =   240
         Locked          =   -1  'True
         TabIndex        =   10
         Tag             =   "T"
         Top             =   720
         Width           =   585
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel6 
         Height          =   255
         Left            =   240
         OleObjectBlob   =   "mantenedor_Usuarios.frx":0260
         TabIndex        =   9
         Top             =   3000
         Width           =   4095
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
         Height          =   255
         Left            =   3960
         OleObjectBlob   =   "mantenedor_Usuarios.frx":02E5
         TabIndex        =   11
         Top             =   480
         Width           =   975
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   255
         Index           =   0
         Left            =   840
         OleObjectBlob   =   "mantenedor_Usuarios.frx":0353
         TabIndex        =   12
         Top             =   480
         Width           =   1095
      End
      Begin MSComctlLib.ListView LvUsuarios 
         Height          =   1815
         Left            =   240
         TabIndex        =   6
         Top             =   1080
         Width           =   10635
         _ExtentX        =   18759
         _ExtentY        =   3201
         View            =   3
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   8
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Id"
            Object.Width           =   1058
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Nombre"
            Object.Width           =   3193
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Login"
            Object.Width           =   2328
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "Password"
            Object.Width           =   2081
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Text            =   "Permisos"
            Object.Width           =   4427
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Text            =   "Activo"
            Object.Width           =   1940
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   6
            Object.Tag             =   "N109"
            Text            =   "Perfil"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   7
            Object.Tag             =   "T1000"
            Text            =   "Ve P.Costo"
            Object.Width           =   2752
         EndProperty
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   255
         Index           =   1
         Left            =   2640
         OleObjectBlob   =   "mantenedor_Usuarios.frx":03BD
         TabIndex        =   13
         Top             =   480
         Width           =   1095
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel5 
         Height          =   255
         Index           =   0
         Left            =   5160
         OleObjectBlob   =   "mantenedor_Usuarios.frx":0425
         TabIndex        =   14
         Top             =   480
         Width           =   975
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel5 
         Height          =   255
         Index           =   1
         Left            =   7680
         OleObjectBlob   =   "mantenedor_Usuarios.frx":0493
         TabIndex        =   15
         Top             =   480
         Width           =   1125
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   255
         Index           =   2
         Left            =   240
         OleObjectBlob   =   "mantenedor_Usuarios.frx":050D
         TabIndex        =   16
         Top             =   480
         Width           =   615
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel5 
         Height          =   255
         Index           =   2
         Left            =   8790
         OleObjectBlob   =   "mantenedor_Usuarios.frx":056F
         TabIndex        =   18
         Top             =   495
         Width           =   1125
      End
   End
End
Attribute VB_Name = "mantenedor_Usuarios"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub CmdOk_Click()

    Dim b_Agrega As Boolean
    Dim s_Mensaje As String
    b_Agrega = True
    s_Mensaje = ""
    If Len(TxtNombre) = 0 Then s_Mensaje = "-Debe especificar nombre" & vbNewLine
    If Len(TxtLogin) = 0 Then s_Mensaje = s_Mensaje & "-Debe especificar login" & vbNewLine
    If Len(txtPassword) = 0 Then s_Mensaje = s_Mensaje & "-Debe especificar password" & vbNewLine
    
    If Len(s_Mensaje) > 0 Then
        MsgBox s_Mensaje, vbExclamation + vbOKOnly
        Exit Sub
    End If
    If Len(TxtIdUsuario) = 0 Then
        For i = 1 To LvUsuarios.ListItems.Count
            If TxtNombre = LvUsuarios.ListItems(i).SubItems(1) Or TxtLogin = LvUsuarios.ListItems(i).SubItems(2) Then
                MsgBox "Ya existe un usuario o login ingresado !!! ", vbExclamation + vbOKOnly
                Exit Sub
            End If
        Next
    End If
    If Val(TxtIdUsuario) > 0 Then
        Sql = "UPDATE sis_usuarios SET " & _
              "usu_nombre='" & TxtNombre & "'," & _
              "usu_login='" & TxtLogin & "'," & _
              "usu_pwd=md5('" & txtPassword & "')," & _
              "per_id=" & ComPermisos.ItemData(ComPermisos.ListIndex) & "," & _
              "usu_activo='" & ComActivo.Text & "', " & _
              "usu_ver_precios_de_costo='" & Me.CboVePCosto.Text & "' " & _
              "WHERE usu_id=" & TxtIdUsuario
    Else
        Sql = "INSERT INTO sis_usuarios " & _
              "(usu_nombre,usu_login,usu_pwd,per_id,usu_activo,usu_ver_precios_de_costo) " & _
              "VALUES ('" & _
              TxtNombre & "','" & TxtLogin & "',md5('" & txtPassword & "')," & ComPermisos.ItemData(ComPermisos.ListIndex) & ",'" & ComActivo.Text & "','" & CboVePCosto.Text & "')"
    End If
    
    Call Consulta(RsTmp, Sql)
    TxtNombre = "": TxtLogin = "": txtPassword = ""
    TxtIdUsuario = ""
    CargaUsuarios
End Sub

Private Sub CmdSalir_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    Centrar Me
    Aplicar_skin Me
    CargaUsuarios
    LLenarCombo Me.ComPermisos, "per_nombre", "per_id", "sis_perfiles", "per_activo='SI'"
End Sub
Private Sub LvUsuarios_DblClick()
 '   ordListView ColumnHeader, Me, LvUsuarios
    If LvUsuarios.SelectedItem Is Nothing Then Exit Sub
    TxtIdUsuario = LvUsuarios.SelectedItem
    TxtNombre = LvUsuarios.SelectedItem.SubItems(1)
    TxtLogin = LvUsuarios.SelectedItem.SubItems(2)
    Busca_Id_Combo ComPermisos, LvUsuarios.SelectedItem.SubItems(6)
    If LvUsuarios.SelectedItem.SubItems(5) = "SI" Then
        ComActivo.ListIndex = 0
    Else
        ComActivo.ListIndex = 1
    End If
    
    If LvUsuarios.SelectedItem.SubItems(7) = "SI" Then
        CboVePCosto.ListIndex = 0
    Else
        CboVePCosto.ListIndex = 1
    End If
        

End Sub



Private Sub Timer1_Timer()
    LvUsuarios.SetFocus
    Timer1.Enabled = False
End Sub
Private Sub CargaUsuarios()
    Sql = "SELECT u.usu_id,u.usu_nombre,u.usu_login,md5(u.usu_pwd),p.per_nombre,u.usu_activo,u.per_id,usu_ver_precios_de_costo " & _
          "FROM sis_usuarios u, sis_perfiles p " & _
          "WHERE u.per_id=p.per_id "
    Call Consulta(RsTmp, Sql)
    If RsTmp.RecordCount > 0 Then
        LLenar_Grilla RsTmp, Me, LvUsuarios, False, True, True, False
    End If
End Sub

Private Sub TxtLogin_KeyPress(KeyAscii As Integer)
  KeyAscii = Asc(UCase(Chr(KeyAscii)))
  If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtNombre_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
    If KeyAscii = 39 Then KeyAscii = 0
End Sub


Private Sub TxtPassword_KeyPress(KeyAscii As Integer)
  KeyAscii = Asc(UCase(Chr(KeyAscii)))
  If KeyAscii = 39 Then KeyAscii = 0
End Sub
