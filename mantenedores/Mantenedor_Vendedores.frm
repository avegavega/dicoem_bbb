VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form Mantenedor_Vendedores 
   Caption         =   "Mantenedor Vendedores"
   ClientHeight    =   7080
   ClientLeft      =   810
   ClientTop       =   1980
   ClientWidth     =   12165
   LinkTopic       =   "Form1"
   ScaleHeight     =   7080
   ScaleWidth      =   12165
   Begin VB.Frame Frame5 
      Caption         =   "Vendedores"
      Height          =   5430
      Left            =   630
      TabIndex        =   7
      Top             =   735
      Width           =   11025
      Begin VB.TextBox txtComision 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   7935
         TabIndex        =   3
         Tag             =   "N"
         Text            =   "0.00"
         Top             =   720
         Width           =   1200
      End
      Begin VB.TextBox SkId 
         Appearance      =   0  'Flat
         BackColor       =   &H80000004&
         Height          =   315
         Left            =   240
         Locked          =   -1  'True
         TabIndex        =   8
         TabStop         =   0   'False
         Tag             =   "T"
         Top             =   720
         Width           =   585
      End
      Begin VB.CommandButton CmdOk 
         Caption         =   "Ok"
         Height          =   315
         Left            =   10230
         TabIndex        =   5
         Top             =   705
         Width           =   495
      End
      Begin VB.ComboBox ComActivo 
         Appearance      =   0  'Flat
         Height          =   315
         ItemData        =   "Mantenedor_Vendedores.frx":0000
         Left            =   9135
         List            =   "Mantenedor_Vendedores.frx":000A
         Style           =   2  'Dropdown List
         TabIndex        =   4
         Top             =   720
         Width           =   1095
      End
      Begin VB.TextBox TxtMail 
         Appearance      =   0  'Flat
         Height          =   315
         IMEMode         =   3  'DISABLE
         Left            =   5430
         TabIndex        =   2
         Tag             =   "t"
         Top             =   720
         Width           =   2500
      End
      Begin VB.TextBox TxtFono 
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   4125
         TabIndex        =   1
         Tag             =   "T"
         Top             =   720
         Width           =   1300
      End
      Begin VB.TextBox TxtNombre 
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   825
         TabIndex        =   0
         Tag             =   "T"
         Top             =   720
         Width           =   3300
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel6 
         Height          =   255
         Left            =   270
         OleObjectBlob   =   "Mantenedor_Vendedores.frx":0016
         TabIndex        =   9
         Top             =   5085
         Width           =   4095
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
         Height          =   255
         Left            =   5415
         OleObjectBlob   =   "Mantenedor_Vendedores.frx":009B
         TabIndex        =   10
         Top             =   510
         Width           =   975
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   255
         Index           =   0
         Left            =   840
         OleObjectBlob   =   "Mantenedor_Vendedores.frx":0105
         TabIndex        =   11
         Top             =   510
         Width           =   1095
      End
      Begin MSComctlLib.ListView LvUsuarios 
         Height          =   4020
         Left            =   240
         TabIndex        =   12
         Top             =   1050
         Width           =   10500
         _ExtentX        =   18521
         _ExtentY        =   7091
         View            =   3
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   7
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Id"
            Object.Width           =   1058
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "T3000"
            Text            =   "Nombre"
            Object.Width           =   5821
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "T1000"
            Text            =   "Fono"
            Object.Width           =   2293
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Object.Tag             =   "T2000"
            Text            =   "email"
            Object.Width           =   4410
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   4
            Object.Tag             =   "N102"
            Text            =   "Comision"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Object.Tag             =   "T1000"
            Text            =   "Activo"
            Object.Width           =   1940
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   6
            Object.Tag             =   "N109"
            Text            =   "Perfil"
            Object.Width           =   0
         EndProperty
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   255
         Index           =   1
         Left            =   4125
         OleObjectBlob   =   "Mantenedor_Vendedores.frx":016F
         TabIndex        =   13
         Top             =   510
         Width           =   1095
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel5 
         Height          =   255
         Index           =   0
         Left            =   8010
         OleObjectBlob   =   "Mantenedor_Vendedores.frx":01D5
         TabIndex        =   14
         Top             =   510
         Width           =   765
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel5 
         Height          =   255
         Index           =   1
         Left            =   9135
         OleObjectBlob   =   "Mantenedor_Vendedores.frx":0243
         TabIndex        =   15
         Top             =   510
         Width           =   1095
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   255
         Index           =   2
         Left            =   240
         OleObjectBlob   =   "Mantenedor_Vendedores.frx":02BD
         TabIndex        =   16
         Top             =   510
         Width           =   615
      End
   End
   Begin VB.Timer Timer1 
      Interval        =   10
      Left            =   1125
      Top             =   6720
   End
   Begin VB.CommandButton cmdSalir 
      Cancel          =   -1  'True
      Caption         =   "&Retornar"
      Height          =   465
      Left            =   10485
      TabIndex        =   6
      Top             =   6225
      Width           =   1185
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   465
      OleObjectBlob   =   "Mantenedor_Vendedores.frx":031F
      Top             =   6750
   End
End
Attribute VB_Name = "Mantenedor_Vendedores"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False


Private Sub CmdOk_Click()
    If Len(TxtNombre) = 0 Then
        MsgBox "Debe ingresar nombre...", vbInformation
        TxtNombre.SetFocus
        Exit Sub
    End If
    If CDbl(txtComision) = 0 Then
        If MsgBox("�Vendedor sin comisi�n? ...", vbQuestion + vbYesNo) = vbNo Then
            txtComision.SetFocus
            Exit Sub
        End If
    End If
    
    If Val(SkId) = 0 Then 'Nuevo vendedor
        Sql = "INSERT INTO par_vendedores (ven_nombre,ven_fono,ven_email,ven_comision,rut_emp) VALUES('" & _
                TxtNombre & "','" & TxtFono & "','" & TxtMail & "'," & CxP(txtComision) & ",'" & SP_Rut_Activo & "')"
    Else 'Actualiza
        Sql = "UPDATE par_vendedores SET ven_nombre='" & TxtNombre & "'," & _
                                      "ven_email='" & TxtMail & "'," & _
                                      "ven_fono ='" & TxtFono & "'," & _
                                      "ven_comision=" & CxP(txtComision) & ", " & _
                                      "ven_activo='" & Me.ComActivo.Text & "' " & _
                                      "WHERE ven_id=" & SkId
    End If
    cn.Execute Sql
    SkId = Empty
    TxtNombre = Empty
    TxtFono = Empty
    TxtMail = Empty
    txtComision = "0,00"
    ComActivo.ListIndex = 0
    Carga
    TxtNombre.SetFocus
End Sub

Private Sub CmdSalir_Click()
    Unload Me
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = 8 Or KeyAscii = 44 Or keysacii = 46 Then 'pas
    Else
        If KeyAscii = 13 Then
            SendKeys "{TAB}" 'envia un tab
        Else
            If Me.ActiveControl.Tag = "T" Then KeyAscii = Asc(UCase(Chr(KeyAscii)))
            If Me.ActiveControl.Tag = "t" Then KeyAscii = Asc(LCase(Chr(KeyAscii)))
            If Me.ActiveControl.Tag = "N" Then
                KeyAscii = SoloNumeros(KeyAscii)
            End If
        End If
    End If
End Sub
Private Sub Form_Load()
    Centrar Me
    Aplicar_skin Me
    Carga
End Sub

Private Sub LvUsuarios_DblClick()
    If LvUsuarios.SelectedItem Is Nothing Then Exit Sub
    SkId = LvUsuarios.SelectedItem
    TxtNombre = LvUsuarios.SelectedItem.SubItems(1)
    TxtFono = LvUsuarios.SelectedItem.SubItems(2)
    TxtMail = LvUsuarios.SelectedItem.SubItems(3)
    txtComision = LvUsuarios.SelectedItem.SubItems(4)
    If LvUsuarios.SelectedItem.SubItems(5) = "SI" Then ComActivo.ListIndex = 0 Else ComActivo.ListIndex = 1
    TxtNombre.SetFocus
    
End Sub

Private Sub Timer1_Timer()
    TxtNombre.SetFocus
    Timer1.Enabled = False
End Sub
Private Sub Carga()
    Sql = "SELECT * " & _
          "FROM par_vendedores " & _
          "WHERE rut_emp='" & SP_Rut_Activo & "'"
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvUsuarios, False, True, True, False
End Sub
Private Sub txtComision_GotFocus()
    En_Foco txtComision
End Sub

Private Sub txtComision_Validate(Cancel As Boolean)
    If Len(txtComision) = 0 Then txtComision = "0"
    txtComision = Format(txtComision, "#0.#0")
End Sub

Private Sub TxtFono_GotFocus()
    En_Foco TxtFono
End Sub



Private Sub TxtMail_GotFocus()
    En_Foco TxtMail
End Sub

Private Sub TxtNombre_GotFocus()
    En_Foco TxtNombre
End Sub

