VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form Mantenedor_Simple 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Mantenedor"
   ClientHeight    =   5100
   ClientLeft      =   3720
   ClientTop       =   2940
   ClientWidth     =   7530
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5100
   ScaleWidth      =   7530
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   150
      OleObjectBlob   =   "Mantenedor_Simple.frx":0000
      Top             =   4560
   End
   Begin VB.CommandButton cmdSalir 
      Caption         =   "&Salir"
      Height          =   465
      Left            =   6225
      TabIndex        =   9
      Top             =   4455
      Width           =   1185
   End
   Begin VB.Frame FrmMantenedor 
      Caption         =   "MARCAS"
      Height          =   4125
      Left            =   135
      TabIndex        =   3
      Top             =   315
      Width           =   7275
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   210
         Left            =   480
         OleObjectBlob   =   "Mantenedor_Simple.frx":0234
         TabIndex        =   10
         Top             =   3720
         Width           =   6675
      End
      Begin VB.TextBox TxtId 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         Height          =   315
         Left            =   495
         Locked          =   -1  'True
         TabIndex        =   4
         TabStop         =   0   'False
         Tag             =   "T"
         Top             =   615
         Width           =   600
      End
      Begin VB.ComboBox CboActivo 
         Appearance      =   0  'Flat
         Height          =   315
         ItemData        =   "Mantenedor_Simple.frx":0332
         Left            =   5670
         List            =   "Mantenedor_Simple.frx":033C
         Style           =   2  'Dropdown List
         TabIndex        =   1
         Top             =   615
         Width           =   915
      End
      Begin VB.TextBox TxtNombre 
         Height          =   315
         Left            =   1110
         TabIndex        =   0
         Tag             =   "T"
         Top             =   615
         Width           =   4560
      End
      Begin VB.CommandButton CmdOk 
         Caption         =   "Ok"
         Height          =   300
         Left            =   6615
         TabIndex        =   2
         Top             =   600
         Width           =   495
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   255
         Index           =   3
         Left            =   1095
         OleObjectBlob   =   "Mantenedor_Simple.frx":0348
         TabIndex        =   5
         Top             =   405
         Width           =   1095
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel5 
         Height          =   255
         Index           =   2
         Left            =   5670
         OleObjectBlob   =   "Mantenedor_Simple.frx":03B2
         TabIndex        =   6
         Top             =   405
         Width           =   735
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   255
         Index           =   5
         Left            =   495
         OleObjectBlob   =   "Mantenedor_Simple.frx":041C
         TabIndex        =   7
         Top             =   405
         Width           =   615
      End
      Begin MSComctlLib.ListView LvDetalle 
         Height          =   2730
         Left            =   495
         TabIndex        =   8
         Top             =   945
         Width           =   6660
         _ExtentX        =   11748
         _ExtentY        =   4815
         View            =   3
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   3
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Id"
            Object.Width           =   1058
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Nombre"
            Object.Width           =   8043
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Activo"
            Object.Width           =   1614
         EndProperty
      End
   End
End
Attribute VB_Name = "mantenedor_simple"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public S_Tabla As String
Public S_Id As String
Public S_Nombre As String
Public S_Activo As String
Public S_Consulta As String
Public B_Editable As Boolean
Dim rs_Mantenedor As Recordset, S_sql As String

Private Sub CmdOk_Click()
    If Val(TxtId) = 0 Then
        S_sql = "INSERT INTO " & S_Tabla & _
              " (" & S_Nombre & "," & S_Activo & ") " & _
              "VALUES ('" & TxtNombre & "','" & CboActivo.Text & "')"
    Else
        S_sql = "UPDATE " & S_Tabla & _
              " SET " & S_Nombre & "='" & TxtNombre & "'," & S_Activo & "='" & CboActivo.Text & "' " & _
              " WHERE " & S_Id & "=" & Val(TxtId)
    End If
    AbreConsulta rs_Mantenedor, S_sql
    
    TxtId = ""
    TxtNombre = Empty
    CboActivo.ListIndex = 0
    CargaDatos
    LvDetalle.SetFocus
End Sub

Private Sub cmdSalir_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    Aplicar_skin Me
    CboActivo.ListIndex = 0
    If Not B_Editable Then
        CmdOk.Enabled = False
        Me.Caption = Me.Caption & " (Solo lectura) "
    End If
    CargaDatos
End Sub
Private Sub CargaDatos()
    AbreConsulta rs_Mantenedor, S_Consulta
    LLenar_Grilla rs_Mantenedor, Me, LvDetalle, False, True, True, False
End Sub

Private Sub LvDetalle_DblClick()
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    TxtId = LvDetalle.SelectedItem.Text
    TxtNombre = LvDetalle.SelectedItem.SubItems(1)
    CboActivo.ListIndex = IIf(LvDetalle.SelectedItem.SubItems(2) = "SI", 0, 1)
End Sub
Private Sub TxtNombre_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub
