VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form BuscarSimple 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Buscar"
   ClientHeight    =   5040
   ClientLeft      =   6885
   ClientTop       =   1170
   ClientWidth     =   7215
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5040
   ScaleWidth      =   7215
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton CmdSalir 
      Cancel          =   -1  'True
      Caption         =   "&Retornar"
      Height          =   375
      Left            =   5955
      TabIndex        =   6
      Top             =   4530
      Width           =   1095
   End
   Begin VB.Timer Timer1 
      Interval        =   10
      Left            =   705
      Top             =   4410
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   30
      OleObjectBlob   =   "BuscarSimple.frx":0000
      Top             =   4395
   End
   Begin VB.Frame FraBusqueda 
      Caption         =   "Busqueda Simple"
      Height          =   4095
      Left            =   465
      TabIndex        =   0
      Top             =   375
      Width           =   6585
      Begin VB.CommandButton CmdCuentasContables 
         Caption         =   "Crear cuenta nueva"
         Height          =   400
         Left            =   1605
         TabIndex        =   7
         Top             =   3630
         Visible         =   0   'False
         Width           =   1905
      End
      Begin VB.CommandButton CmdSelecciona 
         Caption         =   "&Seleccionar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   400
         Left            =   195
         TabIndex        =   5
         Top             =   3630
         Width           =   1400
      End
      Begin VB.CommandButton CmdTodos 
         Caption         =   "Todos"
         Height          =   375
         Left            =   5160
         TabIndex        =   2
         Top             =   675
         Width           =   1215
      End
      Begin VB.TextBox TxtBusqueda 
         Height          =   375
         Left            =   165
         TabIndex        =   1
         Top             =   690
         Width           =   4980
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkBuscar 
         Height          =   315
         Left            =   165
         OleObjectBlob   =   "BuscarSimple.frx":0234
         TabIndex        =   3
         Top             =   390
         Width           =   3525
      End
      Begin MSComctlLib.ListView LvDetalle 
         Height          =   2535
         Left            =   180
         TabIndex        =   4
         Top             =   1065
         Width           =   6210
         _ExtentX        =   10954
         _ExtentY        =   4471
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   2
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "T1000"
            Text            =   "Id"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "T1500"
            Text            =   "Descripcion"
            Object.Width           =   7937
         EndProperty
      End
   End
End
Attribute VB_Name = "BuscarSimple"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public Sm_Consulta As String
Public Sm_CampoLike As String
Public Im_Columnas As Integer
Public Sm_Order As String
Public Sm_Group As String
Public Sm_Busqueda_por_Defecto As String
Dim Sp_Filtro As String
Private Sub CmdCuentasContables_Click()
    con_PlanDeCuentas.Show 1
    CargaLista
End Sub

Private Sub cmdSalir_Click()
    SG_codigo = Empty
    Unload Me
End Sub

Private Sub CmdSelecciona_Click()
    LvDetalle_DblClick
End Sub

Private Sub CmdTodos_Click()
    Sql = Sm_Consulta
    CargaLista
End Sub



Private Sub Form_Load()
    Dim Sp_Ancho As Long
    Aplicar_skin Me
    'Sql = Sm_Consulta
    If Im_Columnas > 0 Then
        For i = 1 To Im_Columnas
            LvDetalle.ColumnHeaders.Add , , "", 2000
            LvDetalle.ColumnHeaders(LvDetalle.ColumnHeaders.Count - 1).Tag = "T2000"
        Next
    End If
    For i = 1 To LvDetalle.ColumnHeaders.Count - 1
        Sp_Ancho = Sp_Ancho + LvDetalle.ColumnHeaders(i).Width
    Next
    Me.Width = Sp_Ancho + 3500
    Me.FraBusqueda.Width = Sp_Ancho + 3000
    LvDetalle.Width = Sp_Ancho + 2500
    cmdSalir.Left = Me.Width - 1500
    Centrar Me
    TxtBusqueda = Sm_Busqueda_por_Defecto
    If Len(TxtBusqueda) > 0 Then
        Sp_Filtro = " AND " & Sm_CampoLike & " LIKE '" & Me.TxtBusqueda.Text & "%'"
    End If
    CargaLista
End Sub
Private Sub CargaLista()
    
    Consulta RsTmp3, Sm_Consulta & Sp_Filtro & Sm_Group & Sm_Order
    LLenar_Grilla RsTmp3, Me, LvDetalle, False, True, True, False
End Sub
Private Sub LvDetalle_DblClick()
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    SG_codigo = LvDetalle.SelectedItem
    Unload Me
End Sub

Private Sub LvDetalle_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 13 Then CmdSelecciona_Click
End Sub

Private Sub Timer1_Timer()
    TxtBusqueda.SetFocus
    Timer1.Enabled = False
End Sub
Private Sub TxtBusqueda_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
     If KeyAscii = 39 Then KeyAscii = 0
    Sp_Filtro = ""
    If Len(Me.TxtBusqueda.Text) = 0 Then
        Sp_Filtro = ""
        Sm_Consulta = Sm_Consulta
    Else
        Sp_Filtro = " AND " & Sm_CampoLike & " LIKE '" & Me.TxtBusqueda.Text & "%'"
        'Sm_Consulta = Sm_Consulta & " AND " & Sm_CampoLike & " LIKE '" & Me.TxtBusqueda.Text & "%'"
    End If
   ' Sm_Consulta = Sm_Consulta & Filtro
    CargaLista
    If KeyAscii = 13 Then
        LvDetalle.SetFocus
    End If
End Sub

Private Sub TxtBusqueda_Validate(Cancel As Boolean)
TxtBusqueda = Replace(TxtBusqueda, "'", "")
End Sub
