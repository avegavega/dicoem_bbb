VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{DA729E34-689F-49EA-A856-B57046630B73}#1.0#0"; "progressbar-xp.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form AgregoEmpresa 
   Caption         =   "Empresa"
   ClientHeight    =   8385
   ClientLeft      =   1380
   ClientTop       =   2415
   ClientWidth     =   14820
   LinkTopic       =   "Form1"
   ScaleHeight     =   8385
   ScaleWidth      =   14820
   Begin VB.Frame FraProgreso 
      Caption         =   "Creando Empresa"
      Height          =   645
      Left            =   150
      TabIndex        =   46
      Top             =   7500
      Visible         =   0   'False
      Width           =   11430
      Begin Proyecto2.XP_ProgressBar BarraProgreso 
         Height          =   330
         Left            =   165
         TabIndex        =   47
         Top             =   255
         Width           =   11130
         _ExtentX        =   19632
         _ExtentY        =   582
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BrushStyle      =   0
         Color           =   16777088
         Scrolling       =   1
         ShowText        =   -1  'True
      End
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Command1"
      Height          =   240
      Left            =   4455
      TabIndex        =   38
      Top             =   8715
      Width           =   270
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   2025
      OleObjectBlob   =   "AgregoEmpresa.frx":0000
      Top             =   8655
   End
   Begin VB.Timer Timer2 
      Interval        =   10
      Left            =   1260
      Top             =   8445
   End
   Begin VB.Timer Timer1 
      Interval        =   20
      Left            =   285
      Top             =   8430
   End
   Begin VB.CommandButton CmdSale 
      Cancel          =   -1  'True
      Caption         =   "&Retornar"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Left            =   13185
      TabIndex        =   18
      Top             =   7245
      Width           =   1530
   End
   Begin VB.Frame Frame1 
      Caption         =   "Datos"
      Height          =   6705
      Left            =   390
      TabIndex        =   19
      Top             =   360
      Width           =   14280
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel18 
         Height          =   630
         Left            =   8835
         OleObjectBlob   =   "AgregoEmpresa.frx":0234
         TabIndex        =   56
         Top             =   4695
         Width           =   5160
      End
      Begin VB.ComboBox CboDepreciacion 
         Appearance      =   0  'Flat
         Height          =   315
         ItemData        =   "AgregoEmpresa.frx":032A
         Left            =   2550
         List            =   "AgregoEmpresa.frx":0334
         Style           =   2  'Dropdown List
         TabIndex        =   55
         Top             =   5445
         Width           =   1785
      End
      Begin VB.ComboBox CboFueraPeriodoContable 
         Appearance      =   0  'Flat
         Height          =   315
         ItemData        =   "AgregoEmpresa.frx":034C
         Left            =   2565
         List            =   "AgregoEmpresa.frx":0356
         Style           =   2  'Dropdown List
         TabIndex        =   52
         Top             =   5100
         Width           =   1110
      End
      Begin VB.ComboBox CboImpCompPago 
         Appearance      =   0  'Flat
         Height          =   315
         ItemData        =   "AgregoEmpresa.frx":0362
         Left            =   2580
         List            =   "AgregoEmpresa.frx":036C
         Style           =   2  'Dropdown List
         TabIndex        =   50
         Top             =   4785
         Width           =   1110
      End
      Begin VB.ComboBox CboUsaBruto 
         Appearance      =   0  'Flat
         Height          =   315
         ItemData        =   "AgregoEmpresa.frx":0378
         Left            =   2580
         List            =   "AgregoEmpresa.frx":0382
         Style           =   2  'Dropdown List
         TabIndex        =   48
         Top             =   4470
         Width           =   1110
      End
      Begin VB.Frame Frame4 
         Caption         =   "Impuestos utilizados por esta empresa"
         Height          =   3585
         Left            =   8715
         TabIndex        =   43
         Top             =   1110
         Width           =   5265
         Begin MSComctlLib.ListView LvImpuestos 
            Height          =   2970
            Left            =   255
            TabIndex        =   44
            Top             =   360
            Width           =   4830
            _ExtentX        =   8520
            _ExtentY        =   5239
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            FullRowSelect   =   -1  'True
            GridLines       =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   0
            NumItems        =   3
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Object.Tag             =   "N109"
               Text            =   "Id"
               Object.Width           =   1058
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Object.Tag             =   "T5000"
               Text            =   "Nombre"
               Object.Width           =   4939
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   2
               Object.Tag             =   "T1500"
               Text            =   "Costo/Credito"
               Object.Width           =   2646
            EndProperty
         End
      End
      Begin VB.Frame Frame3 
         Caption         =   "Opcionales por Empresa"
         Height          =   2505
         Left            =   6375
         TabIndex        =   39
         Top             =   1125
         Width           =   2145
         Begin VB.CheckBox ChkCuenta 
            Caption         =   "Plan de Cuentas"
            Height          =   225
            Left            =   315
            TabIndex        =   45
            Top             =   690
            Value           =   1  'Checked
            Width           =   1605
         End
         Begin VB.CheckBox ChkItem 
            Caption         =   "Item de Gastos"
            Height          =   240
            Left            =   315
            TabIndex        =   42
            Top             =   2025
            Value           =   1  'Checked
            Width           =   1575
         End
         Begin VB.CheckBox ChkAreas 
            Caption         =   "Areas"
            Height          =   225
            Left            =   315
            TabIndex        =   41
            Top             =   1590
            Value           =   1  'Checked
            Width           =   1545
         End
         Begin VB.CheckBox ChkCC 
            Caption         =   "Centro de costo"
            Height          =   225
            Left            =   300
            TabIndex        =   40
            Top             =   1155
            Value           =   1  'Checked
            Width           =   1605
         End
      End
      Begin VB.ComboBox CboActivo 
         Appearance      =   0  'Flat
         Height          =   315
         ItemData        =   "AgregoEmpresa.frx":0393
         Left            =   2580
         List            =   "AgregoEmpresa.frx":039D
         Style           =   2  'Dropdown List
         TabIndex        =   37
         Top             =   4155
         Width           =   915
      End
      Begin VB.ComboBox CboInventario 
         Appearance      =   0  'Flat
         Height          =   315
         ItemData        =   "AgregoEmpresa.frx":03A9
         Left            =   2580
         List            =   "AgregoEmpresa.frx":03B3
         Style           =   2  'Dropdown List
         TabIndex        =   11
         Top             =   3825
         Width           =   915
      End
      Begin VB.TextBox txtRut 
         Alignment       =   1  'Right Justify
         Height          =   315
         Left            =   2580
         TabIndex        =   0
         Text            =   " "
         Top             =   270
         Width           =   2055
      End
      Begin VB.TextBox txtRl 
         Height          =   315
         Left            =   2580
         TabIndex        =   9
         Top             =   3180
         Width           =   2055
      End
      Begin VB.TextBox txtFax 
         Height          =   315
         Left            =   2580
         MaxLength       =   15
         TabIndex        =   7
         Top             =   2520
         Width           =   3375
      End
      Begin VB.TextBox TxtGiro 
         Height          =   315
         Left            =   2580
         TabIndex        =   5
         Top             =   1860
         Width           =   3375
      End
      Begin VB.TextBox TxtRz 
         Height          =   315
         Left            =   2595
         MaxLength       =   100
         TabIndex        =   1
         Top             =   585
         Width           =   4350
      End
      Begin VB.TextBox TxtDirec 
         Height          =   315
         Left            =   2580
         MaxLength       =   50
         TabIndex        =   2
         Top             =   900
         Width           =   3375
      End
      Begin VB.TextBox TxtComuna 
         Height          =   315
         Left            =   2580
         MaxLength       =   25
         TabIndex        =   3
         Top             =   1215
         Width           =   3375
      End
      Begin VB.TextBox TxtCiudad 
         Height          =   315
         Left            =   2580
         MaxLength       =   25
         TabIndex        =   4
         Top             =   1545
         Width           =   3375
      End
      Begin VB.TextBox TxtFono 
         Height          =   315
         Left            =   2580
         MaxLength       =   15
         TabIndex        =   6
         Top             =   2190
         Width           =   3375
      End
      Begin VB.TextBox TxtMail 
         Height          =   315
         Left            =   2580
         MaxLength       =   50
         TabIndex        =   8
         Top             =   2850
         Width           =   3405
      End
      Begin VB.TextBox TxtRecargo 
         Alignment       =   1  'Right Justify
         Height          =   315
         Left            =   10620
         TabIndex        =   23
         Text            =   "0"
         Top             =   390
         Visible         =   0   'False
         Width           =   1095
      End
      Begin VB.TextBox txtNl 
         Height          =   315
         Left            =   2580
         MaxLength       =   50
         TabIndex        =   10
         Top             =   3495
         Width           =   3375
      End
      Begin VB.Frame Frame2 
         Caption         =   "Sucursales"
         Height          =   2475
         Left            =   -15
         TabIndex        =   20
         Top             =   6825
         Visible         =   0   'False
         Width           =   8205
         Begin VB.TextBox TxtSucCiudad 
            Height          =   315
            Left            =   240
            TabIndex        =   12
            Tag             =   "T"
            Top             =   315
            Width           =   1900
         End
         Begin VB.TextBox TxtSucDireccion 
            Height          =   315
            Left            =   2145
            TabIndex        =   13
            Tag             =   "T"
            Top             =   315
            Width           =   2900
         End
         Begin VB.TextBox TxtSucContacto 
            Height          =   315
            Left            =   5040
            TabIndex        =   14
            Tag             =   "T"
            Top             =   315
            Width           =   1800
         End
         Begin VB.ComboBox CboSucActivo 
            Appearance      =   0  'Flat
            Height          =   315
            ItemData        =   "AgregoEmpresa.frx":03BF
            Left            =   6855
            List            =   "AgregoEmpresa.frx":03C9
            Style           =   2  'Dropdown List
            TabIndex        =   15
            Top             =   315
            Width           =   750
         End
         Begin VB.CommandButton CmdOk 
            Caption         =   "Ok"
            Height          =   285
            Left            =   7605
            TabIndex        =   16
            Top             =   315
            Width           =   330
         End
         Begin MSComctlLib.ListView LvDetalle 
            Height          =   1860
            Left            =   240
            TabIndex        =   21
            Top             =   630
            Width           =   7695
            _ExtentX        =   13573
            _ExtentY        =   3281
            View            =   3
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            FullRowSelect   =   -1  'True
            GridLines       =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   0
            NumItems        =   5
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Object.Tag             =   "N109"
               Text            =   "cli_id"
               Object.Width           =   0
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Object.Tag             =   "T1300"
               Text            =   "Ciudad"
               Object.Width           =   3351
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   2
               Object.Tag             =   "T3000"
               Text            =   "Direccion"
               Object.Width           =   5115
            EndProperty
            BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   3
               Object.Tag             =   "T1900"
               Text            =   "Contacto"
               Object.Width           =   3175
            EndProperty
            BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   4
               Object.Tag             =   "T800"
               Text            =   "Activo"
               Object.Width           =   1323
            EndProperty
         End
         Begin VB.Label LBIdSuc 
            Height          =   225
            Left            =   15
            TabIndex        =   22
            Top             =   360
            Width           =   255
         End
      End
      Begin VB.CommandButton CmdGuarda 
         Appearance      =   0  'Flat
         Caption         =   "&Guardar"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   465
         Left            =   180
         TabIndex        =   17
         ToolTipText     =   "Graba empresa"
         Top             =   6105
         Width           =   1905
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel8 
         Height          =   210
         Left            =   870
         OleObjectBlob   =   "AgregoEmpresa.frx":03D5
         TabIndex        =   24
         Top             =   2910
         Width           =   1605
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel6 
         Height          =   210
         Left            =   870
         OleObjectBlob   =   "AgregoEmpresa.frx":0438
         TabIndex        =   25
         Top             =   2250
         Width           =   1605
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel5 
         Height          =   255
         Left            =   885
         OleObjectBlob   =   "AgregoEmpresa.frx":0497
         TabIndex        =   26
         Top             =   1560
         Width           =   1605
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
         Height          =   375
         Left            =   885
         OleObjectBlob   =   "AgregoEmpresa.frx":04FA
         TabIndex        =   27
         Top             =   1245
         Width           =   1605
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   255
         Left            =   870
         OleObjectBlob   =   "AgregoEmpresa.frx":055D
         TabIndex        =   28
         Top             =   915
         Width           =   1605
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   255
         Left            =   900
         OleObjectBlob   =   "AgregoEmpresa.frx":05C6
         TabIndex        =   29
         Top             =   585
         Width           =   1605
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel9 
         Height          =   255
         Left            =   855
         OleObjectBlob   =   "AgregoEmpresa.frx":0635
         TabIndex        =   30
         Top             =   1905
         Width           =   1605
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel10 
         Height          =   210
         Left            =   900
         OleObjectBlob   =   "AgregoEmpresa.frx":0694
         TabIndex        =   31
         Top             =   2580
         Width           =   1545
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   375
         Left            =   525
         OleObjectBlob   =   "AgregoEmpresa.frx":06F1
         TabIndex        =   32
         Top             =   3210
         Width           =   1950
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel11 
         Height          =   240
         Left            =   510
         OleObjectBlob   =   "AgregoEmpresa.frx":0760
         TabIndex        =   33
         Top             =   3540
         Width           =   1950
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel12 
         Height          =   375
         Left            =   555
         OleObjectBlob   =   "AgregoEmpresa.frx":07D7
         TabIndex        =   34
         Top             =   300
         Width           =   1950
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel13 
         Height          =   375
         Left            =   120
         OleObjectBlob   =   "AgregoEmpresa.frx":0844
         TabIndex        =   35
         Top             =   3855
         Width           =   2355
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel14 
         Height          =   270
         Left            =   135
         OleObjectBlob   =   "AgregoEmpresa.frx":08C5
         TabIndex        =   36
         Top             =   4215
         Width           =   2355
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel7 
         Height          =   195
         Left            =   120
         OleObjectBlob   =   "AgregoEmpresa.frx":0932
         TabIndex        =   49
         Top             =   4485
         Width           =   2355
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel15 
         Height          =   390
         Left            =   60
         OleObjectBlob   =   "AgregoEmpresa.frx":09B1
         TabIndex        =   51
         Top             =   4695
         Width           =   2415
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel16 
         Height          =   390
         Left            =   45
         OleObjectBlob   =   "AgregoEmpresa.frx":0A40
         TabIndex        =   53
         Top             =   5070
         Width           =   2415
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel17 
         Height          =   390
         Left            =   45
         OleObjectBlob   =   "AgregoEmpresa.frx":0ADF
         TabIndex        =   54
         Top             =   5505
         Width           =   2415
      End
   End
End
Attribute VB_Name = "AgregoEmpresa"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim Bp_Nuevo As Boolean








Private Sub ChkAreas_Click()
    On Error Resume Next
    Me.CmdGuarda.SetFocus
End Sub

Private Sub ChkCC_Click()
    On Error Resume Next
    Me.CmdGuarda.SetFocus
End Sub

Private Sub ChkCuenta_Click()
    On Error Resume Next
    Me.CmdGuarda.SetFocus
End Sub

Private Sub ChkItem_Click()
    On Error Resume Next
    Me.CmdGuarda.SetFocus
End Sub

Private Sub CmdGuarda_Click()
    Dim sp_Mensaje_Error As String, sp_CC As String, sp_Area As String
    Dim sp_Item As String, sp_Impuestos As String, sp_Cuenta As String
    Dim Lp_VP As Long
    sp_Mensaje_Error = Empty
    If Len(TxtRut) = 0 Then sp_Mensaje_Error = "Falta RUT" & vbNewLine
    If Len(TxtRz) = 0 Then sp_Mensaje_Error = sp_Mensaje_Error & "Falta raz�n social" & vbNewLine
    If Len(TxtDirec) = 0 Then sp_Mensaje_Error = sp_Mensaje_Error & "Falta direcci�n" & vbNewLine
    If Len(txtComuna) = 0 Then sp_Mensaje_Error = sp_Mensaje_Error & "Falta Comuna" & vbNewLine
    If Len(TxtCiudad) = 0 Then sp_Mensaje_Error = sp_Mensaje_Error & "Falta Ciudad" & vbNewLine
    If Len(TxtGiro) = 0 Then sp_Mensaje_Error = sp_Mensaje_Error & "Falta Giro" & vbNewLine
   '� If Len(txtRl) = 0 Then sp_Mensaje_Error = sp_Mensaje_Error & "Falta Rut Representante legal" & vbNewLine
   ' If Len(txtNl) = 0 Then sp_Mensaje_Error = sp_Mensaje_Error & "Falta Nombre Representante legal" & vbNewLine
    If CboInventario.ListIndex = -1 Then sp_Mensaje_Error = sp_Mensaje_Error & "Seleccione si lleva control de inventario " & vbNewLine
    
    
    If sp_Mensaje_Error <> Empty Then
        MsgBox sp_Mensaje_Error, vbExclamation 'Se econtraron errores
        Exit Sub
    End If
    sp_CC = IIf(ChkCC.Value = 1, "SI", "NO")
    sp_Area = IIf(ChkAreas.Value = 1, "SI", "NO")
    sp_Item = IIf(ChkItem.Value = 1, "SI", "NO")
    sp_Cuenta = IIf(ChkCuenta.Value = 1, "SI", "NO")
    On Error GoTo Fallo
    cn.BeginTrans
        If SG_codigo = Empty Then
            Me.FraProgreso.Visible = True
            BarraProgreso.Min = 1
            BarraProgreso.Max = 350
            DoEvents
            'Nuevo
            Lp_VP = 1
            Filtro = "WHERE rut_emp='" & TxtRut & "'"
            '1ro Borraremos datos Previos si existiesen por rut de prueba por ejemplo
            cn.Execute "DELETE FROM par_bodegas " & Filtro
            Lp_VP = Lp_VP + 10
            BarraProgreso.Value = Lp_VP
            cn.Execute "DELETE FROM par_areas " & Filtro
            Lp_VP = Lp_VP + 10
            BarraProgreso.Value = Lp_VP
            cn.Execute "DELETE FROM par_centros_de_costo " & Filtro
            Lp_VP = Lp_VP + 10
            BarraProgreso.Value = Lp_VP
            cn.Execute "DELETE FROM par_item_gastos  " & Filtro
            Lp_VP = Lp_VP + 10
            BarraProgreso.Value = Lp_VP
            cn.Execute "DELETE FROM par_tipos_productos " & Filtro
            Lp_VP = Lp_VP + 10
            BarraProgreso.Value = Lp_VP
            cn.Execute "DELETE FROM par_marcas " & Filtro
            Lp_VP = Lp_VP + 10
            BarraProgreso.Value = Lp_VP
            cn.Execute "DELETE FROM sis_unidad_medida " & Filtro
            Lp_VP = Lp_VP + 10
            BarraProgreso.Value = Lp_VP
            cn.Execute "DELETE FROM maestro_productos " & Filtro
            Lp_VP = Lp_VP + 10
            BarraProgreso.Value = Lp_VP
            cn.Execute "DELETE FROM pro_stock " & Filtro
            Lp_VP = Lp_VP + 10
            BarraProgreso.Value = Lp_VP
            cn.Execute "DELETE FROM par_asociacion_ruts " & Filtro
            Lp_VP = Lp_VP + 10
            BarraProgreso.Value = Lp_VP
            'cn.Execute "DELETE FROM par_impuestos_empresas " & Filtro
            cn.Execute "DELETE FROM  par_vendedores " & Filtro
            Lp_VP = Lp_VP + 10
            BarraProgreso.Value = Lp_VP
            cn.Execute "DELETE FROM ven_doc_venta " & Filtro
            Lp_VP = Lp_VP + 10
            BarraProgreso.Value = Lp_VP
            cn.Execute "DELETE FROM ven_detalle " & Filtro
            Lp_VP = Lp_VP + 10
            BarraProgreso.Value = Lp_VP
            cn.Execute "DELETE FROM inv_kardex " & Filtro
            Lp_VP = Lp_VP + 10
            BarraProgreso.Value = Lp_VP
             Sql = "DELETE FROM " & _
                      "com_doc_compra, " & _
                      "com_doc_compra_detalle " & _
                      "USING com_doc_compra " & _
                      "LEFT JOIN com_doc_compra_detalle ON (com_doc_compra.id=com_doc_compra_detalle.id) " & Filtro
            cn.Execute Sql
            Lp_VP = Lp_VP + 10
            BarraProgreso.Value = Lp_VP
            cn.Execute "DELETE FROM " & _
                      "inv_toma_inventario, " & _
                      "inv_toma_inventario_detalle " & _
                      "USING inv_toma_inventario " & _
                      "LEFT JOIN inv_toma_inventario_detalle ON (inv_toma_inventario.tmi_id=inv_toma_inventario_detalle.tmi_id) " & Filtro
                      
            Lp_VP = Lp_VP + 10
            BarraProgreso.Value = Lp_VP
            cn.Execute "DELETE FROM " & _
                      "inv_actualizacion_precios, " & _
                      "inv_actualizacion_precios_detalle " & _
                      "USING inv_actualizacion_precios " & _
                      "LEFT JOIN inv_actualizacion_precios_detalle ON (inv_actualizacion_precios.act_id=inv_actualizacion_precios_detalle.act_id) " & Filtro
            Lp_VP = Lp_VP + 10
            BarraProgreso.Value = Lp_VP
            cn.Execute "DELETE FROM cta_corriente " & Filtro
            Lp_VP = Lp_VP + 10
            BarraProgreso.Value = Lp_VP
            cn.Execute "DELETE FROM cta_abonos " & Filtro
            Lp_VP = Lp_VP + 10
            BarraProgreso.Value = Lp_VP
            cn.Execute "DELETE FROM abo_cheques " & Filtro
            Lp_VP = Lp_VP + 10
            BarraProgreso.Value = Lp_VP
            cn.Execute "DELETE FROM cta_abono_documentos " & Filtro
            Lp_VP = Lp_VP + 10
            BarraProgreso.Value = Lp_VP
            cn.Execute "DELETE FROM abo_transferencia " & Filtro
            Lp_VP = Lp_VP + 10
            BarraProgreso.Value = Lp_VP
            
            Sql = "INSERT INTO par_bodegas (bod_nombre,rut_emp) VALUES('PRINCIPAL','" & TxtRut & "')"
            cn.Execute Sql
            Lp_VP = Lp_VP + 10
            BarraProgreso.Value = Lp_VP
            Sql = "INSERT INTO par_areas (are_nombre,rut_emp) VALUES('GENERICA','" & TxtRut & "')"
            cn.Execute Sql
            Lp_VP = Lp_VP + 10
            BarraProgreso.Value = Lp_VP
            Sql = "INSERT INTO par_centros_de_costo (cen_nombre,rut_emp) VALUES('GENERICA','" & TxtRut & "')"
            cn.Execute Sql
            Lp_VP = Lp_VP + 10
            BarraProgreso.Value = Lp_VP
            Sql = "INSERT INTO par_item_gastos (gas_nombre,rut_emp) VALUES('GENERICA','" & TxtRut & "')"
            cn.Execute Sql
            Lp_VP = Lp_VP + 10
            BarraProgreso.Value = Lp_VP
            Sql = "INSERT INTO par_tipos_productos (tip_nombre,rut_emp,pla_id) VALUES('GENERICA','" & TxtRut & "',99999)"
            cn.Execute Sql
            Lp_VP = Lp_VP + 10
            BarraProgreso.Value = Lp_VP
            Sql = "INSERT INTO par_marcas (mar_nombre,rut_emp) VALUES('GENERICA','" & TxtRut & "')"
            cn.Execute Sql
            Lp_VP = Lp_VP + 10
            BarraProgreso.Value = Lp_VP
            Sql = "INSERT INTO sis_unidad_medida (ume_nombre,rut_emp) VALUES('GENERICA','" & TxtRut & "')"
            cn.Execute Sql
            Lp_VP = Lp_VP + 10
            BarraProgreso.Value = Lp_VP
            Sql = "INSERT INTO sis_empresas (rut,nombre_empresa,direccion,comuna,ciudad,fono,fax,email," & _
                                             "giro,rut_rl,nombre_rl,control_inventario,activo," & _
                                             "emp_centro_de_costos,emp_areas,emp_item_de_gastos," & _
                                             "emp_cuenta,emp_venta_precios_brutos,emp_comprobantes_pagos_contado," & _
                                             "emp_permite_venta_fuera_periodo,emp_tipo_depreciacion) " & _
                 "VALUES('" & TxtRut & "','" & TxtRz & "','" & TxtDirec & "','" & txtComuna & "','" & _
                 TxtCiudad & "','" & txtFono & "','" & txtFax & "','" & TxtMail & "','" & TxtGiro & "','" & txtRl & "','" & _
                 txtNl & "','" & CboInventario.Text & "','" & CboActivo.Text & "','" & sp_CC & "','" & sp_Area & "','" & sp_Item & "','" & _
                 sp_Cuenta & "','" & CboUsaBruto.Text & "','" & Me.CboImpCompPago.Text & "','" & Me.CboFueraPeriodoContable.Text & "','" & CboDepreciacion.Text & "')"
            Lp_VP = Lp_VP + 10
            BarraProgreso.Value = 349
        Else
            'Actualiza
            Sql = "UPDATE sis_empresas SET nombre_empresa='" & TxtRz & "'," & _
                                                  "direccion='" & TxtDirec & "'," & _
                                                  "comuna='" & txtComuna & "'," & _
                                                  "ciudad='" & TxtCiudad & "'," & _
                                                  "fono='" & txtFono & "'," & _
                                                  "email='" & TxtMail & "'," & _
                                                  "giro='" & TxtGiro & "'," & _
                                                  "rut_rl='" & txtRl & "'," & _
                                                  "nombre_rl='" & txtNl & "'," & _
                                                  "control_inventario='" & CboInventario.Text & "', " & _
                                                  "emp_centro_de_costos='" & sp_CC & "'," & _
                                                  "emp_areas='" & sp_Area & "'," & _
                                                  "emp_item_de_gastos='" & sp_Item & "'," & _
                                                  "activo='" & CboActivo.Text & "', " & _
                                                  "emp_cuenta='" & sp_Cuenta & "', " & _
                                                  "emp_venta_precios_brutos='" & CboUsaBruto.Text & "', " & _
                                                  "emp_comprobantes_pagos_contado='" & Me.CboImpCompPago.Text & "', " & _
                                                  "emp_permite_venta_fuera_periodo='" & Me.CboFueraPeriodoContable.Text & "', " & _
                                                  "emp_tipo_depreciacion='" & CboDepreciacion.Text & "' " & _
                    "WHERE rut='" & TxtRut & "'"
                    
            cn.Execute "UPDATE con_activo_inmovilizado SET aim_tipo_depreciacion='" & CboDepreciacion.Text & "' " & _
                    "WHERE rut_emp='" & TxtRut & "'"
            
        End If
        cn.Execute Sql
        
        
        Sql = "DELETE FROM par_impuestos_empresas " & _
              "WHERE rut='" & TxtRut & "'"
        cn.Execute Sql
        sp_Impuestos = ""
        For i = 1 To LvImpuestos.ListItems.Count
            If LvImpuestos.ListItems(i).Checked Then
                sp_Impuestos = sp_Impuestos & "('" & TxtRut & "'," & LvImpuestos.ListItems(i) & ",'" & LvImpuestos.ListItems(i).SubItems(2) & "'),"
            End If
        Next
        If Len(sp_Impuestos) > 0 Then
            sp_Impuestos = Mid(sp_Impuestos, 1, Len(sp_Impuestos) - 1)
            Sql = "INSERT INTO par_impuestos_empresas (rut,imp_id,ime_costo_credito) " & _
                  "VALUES " & sp_Impuestos
            cn.Execute Sql
        End If
    cn.CommitTrans
    Me.FraProgreso.Visible = True
    MsgBox "RECUERDE INGRESAR EN MANTENEDORES" & vbNewLine & "-AREAS" & vbNewLine & "-CENTROS DE COSTOS" & vbNewLine & "ITEM DE GASTOS", , "EMPRESA CREADA"
    Unload Me
    Exit Sub
Fallo:
    cn.RollbackTrans
    MsgBox "Ocurrio un error al intentar grabar los datos..." & vbNewLine & Err.Number & vbNewLine & Err.Description, vbInformation
    
End Sub

Private Sub CmdOk_Click()
If Len(TxtRut) = 0 Then
        MsgBox "Falta RUT cliente ...", vbInformation
        TxtRut.SetFocus
        Exit Sub
    End If
    If Len(TxtSucCiudad) = 0 Or Len(TxtSucDireccion) = 0 Or Len(Me.TxtSucContacto) = 0 Then
        MsgBox "Faltan datos para Sucursal...", vbInformation
        TxtSucCiudad.SetFocus
        Exit Sub
    End If
    If Val(LBIdSuc) = 0 Then
        'Nueva sucursal
        Sql = "INSERT INTO par_sucursales (rut_cliente,suc_ciudad,suc_direccion,suc_contacto,suc_activo) " & _
              "VALUES('" & TxtRut & "','" & TxtSucCiudad & "','" & TxtSucDireccion & "','" & TxtSucContacto & "','" & Me.CboSucActivo.Text & "')"
    Else
        'Actualiza sucursal
        Sql = "UPDATE par_sucursales SET suc_ciudad='" & TxtSucCiudad & "'," & _
                                        "suc_direccion='" & TxtSucDireccion & "'," & _
                                        "suc_contacto='" & TxtSucContacto & "'," & _
                                        "suc_activo='" & CboSucActivo.Text & "' " & _
               "WHERE suc_id=" & LBIdSuc
    End If
    TxtSucCiudad = Empty
    TxtSucDireccion = Empty
    TxtSucContacto = Empty
    Consulta RsTmp, Sql
    CargaSucursales
End Sub

Private Sub CmdSale_Click()
    Unload Me
End Sub

Private Sub Command1_Click()
    Timer1.Enabled = False
End Sub

Private Sub Form_Load()
    Centrar Me
    Aplicar_skin Me
    CboUsaBruto.ListIndex = 0
    Me.CboImpCompPago.ListIndex = 0
    CboFueraPeriodoContable.ListIndex = 0
    CboDepreciacion.ListIndex = 0
    If SG_codigo <> Empty Then
        Bp_Nuevo = False
        'Llenamos datos de la empresa
        Sql = "SELECT a.rut,a.nombre_empresa,a.direccion,a.comuna,a.ciudad,a.fono," & _
                     "a.fax,a.email,a.giro,a.rut_rl,a.nombre_rl,a.control_inventario,a.activo, " & _
                     "emp_centro_de_costos,emp_areas,emp_item_de_gastos,emp_cuenta," & _
                     "emp_venta_precios_brutos brutoneto,emp_comprobantes_pagos_contado impcom, " & _
                     "emp_permite_venta_fuera_periodo fueraperiodo,emp_tipo_depreciacion depre " & _
              "FROM sis_empresas AS a " & _
              "WHERE rut='" & SG_codigo & "'"
        Consulta RsTmp3, Sql
        If RsTmp3.RecordCount > 0 Then
            With RsTmp3
                TxtRut = !Rut
                TxtRz = !nombre_empresa
                TxtDirec = !direccion
                txtComuna = !comuna
                TxtCiudad = !ciudad
                txtFono = !fono
                txtFax = "" & !fax
                TxtMail = !Email
                TxtGiro = !giro
                txtRl = !rut_rl
                txtNl = !nombre_rl
                CboInventario.ListIndex = IIf(!control_inventario = "SI", 0, 1)
                CboActivo.ListIndex = IIf(!activo = "SI", 0, 1)
                If !emp_centro_de_costos = "NO" Then ChkCC.Value = 0
                If !emp_areas = "NO" Then ChkAreas.Value = 0
                If !emp_item_de_gastos = "NO" Then ChkItem.Value = 0
                If !emp_cuenta = "NO" Then ChkCuenta.Value = 0
                If !brutoneto = "BRUTO" Then Me.CboUsaBruto.ListIndex = 0 Else CboUsaBruto.ListIndex = 1
                If !impcom = "SI" Then Me.CboImpCompPago.ListIndex = 0 Else Me.CboImpCompPago.ListIndex = 1
                If !fueraperiodo = "SI" Then Me.CboFueraPeriodoContable.ListIndex = 0 Else Me.CboFueraPeriodoContable.ListIndex = 1
                If !depre = "INDIRECTA" Then Me.CboDepreciacion.ListIndex = 0 Else Me.CboDepreciacion.ListIndex = 1
            End With
        End If
        TxtRut.Locked = True
        
    Else
        Bp_Nuevo = True
    End If
    CboActivo.ListIndex = 0
    CboSucActivo.ListIndex = 0
    CargaSucursales
    
    Sql = "SELECT imp_id,imp_nombre " & _
          "FROM par_impuestos " & _
          "WHERE imp_activo='SI'"
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvImpuestos, True, True, True, False
    
    Sql = "SELECT imp_id,ime_costo_credito " & _
          "FROM par_impuestos_empresas " & _
          "WHERE rut='" & TxtRut & "' "
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        RsTmp.MoveFirst
        Do While Not RsTmp.EOF
            For i = 1 To LvImpuestos.ListItems.Count
                If RsTmp!imp_id = Val(LvImpuestos.ListItems(i)) Then
                    LvImpuestos.ListItems(i).Checked = True
                    LvImpuestos.ListItems(i).SubItems(2) = RsTmp!ime_costo_credito
                End If
            Next
            RsTmp.MoveNext
        Loop
    End If
End Sub
Private Sub CargaSucursales()
        Sql = "SELECT suc_id,suc_ciudad,suc_direccion,suc_contacto,suc_activo " & _
              "FROM par_sucursales s " & _
              "WHERE rut_cliente='" & SG_codigo & "'"
        Consulta RsTmp, Sql
        LLenar_Grilla RsTmp, Me, LvDetalle, False, True, True, False

End Sub
Public Sub InhabilitaCajas(SI As Boolean)
    
    For Each CTxt In Controls
            If (TypeOf CTxt Is TextBox) Then
                If SI = True Then
                    If CTxt.Name <> "TxtRut" Then CTxt.Locked = True
                Else
                  '  If CTxt.Name <> "TxtRut" Then CTxt.Text = ""
                    CTxt.Locked = False
                    
                End If
            End If
    Next
    If SI Then
        CboInventario.Enabled = False
        CboActivo.Enabled = False
        Frame2.Visible = False
    Else
        CboInventario.Enabled = True
        CboActivo.Enabled = True
        'Frame2.Visible = True'
    End If
    
    
End Sub

Private Sub LvDetalle_DblClick()
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    
    LBIdSuc = LvDetalle.SelectedItem
    TxtSucCiudad = LvDetalle.SelectedItem.SubItems(1)
    TxtSucDireccion = LvDetalle.SelectedItem.SubItems(2)
    TxtSucContacto = LvDetalle.SelectedItem.SubItems(3)
    CboSucActivo.ListIndex = IIf(LvDetalle.SelectedItem.SubItems(4) = "SI", 0, 1)
    TxtSucCiudad.SetFocus
End Sub

Private Sub LvImpuestos_ItemCheck(ByVal Item As MSComctlLib.ListItem)
    If Not Item.Checked Then
        Item.SubItems(2) = ""
    Else
        If MsgBox("�Impuesto da derecho a credito?" & vbNewLine & "SI: da derecho a credito" & vbNewLine & "NO: es costo", vbYesNo) = vbNo Then
            Item.SubItems(2) = "COSTO"
        Else
            Item.SubItems(2) = "CREDITO"
        End If
    End If
        
End Sub

Private Sub Timer1_Timer()
    On Error GoTo ElError
    For Each CTextos In Controls
        If (TypeOf CTextos Is TextBox) Then
            If Me.ActiveControl.Name = CTextos.Name Then 'Foco activo
                CTextos.BackColor = IIf(CTextos.Locked, ClrDesha, ClrCfoco)
            Else
                CTextos.BackColor = IIf(CTextos.Locked, ClrDesha, ClrSfoco)
            End If
        End If
    Next
    Exit Sub
ElError:
End Sub

Private Sub Timer2_Timer()
    If SG_codigo = Empty Then TxtRut.SetFocus Else Me.TxtRz.SetFocus
    Timer2.Enabled = False
End Sub


Private Sub TxtCiudad_Validate(Cancel As Boolean)
TxtCiudad = Replace(TxtCiudad, "'", "")
End Sub

Private Sub TxtComuna_Validate(Cancel As Boolean)
txtComuna = Replace(txtComuna, "'", "")
End Sub

Private Sub TxtDirec_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
     If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtDirec_Validate(Cancel As Boolean)
TxtDirec = Replace(TxtDirec, "'", "")
End Sub

Private Sub txtFax_GotFocus()
    En_Foco txtFax
End Sub

Private Sub txtFax_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
     If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtFax_Validate(Cancel As Boolean)
txtFax = Replace(txtFax, "'", "")
End Sub

Private Sub TxtFono_GotFocus()
    En_Foco txtFono
End Sub

Private Sub TxtFono_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
     If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtFono_Validate(Cancel As Boolean)
txtFono = Replace(txtFono, "'", "")
End Sub

Private Sub TxtGiro_GotFocus()
    En_Foco TxtGiro
End Sub

Private Sub TxtGiro_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
     If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtGiro_Validate(Cancel As Boolean)
TxtGiro = Replace(TxtGiro, "'", "")
End Sub

Private Sub TxtMail_GotFocus()
    En_Foco TxtMail
End Sub
Private Sub TxtMail_KeyPress(KeyAscii As Integer)
KeyAscii = Asc(LCase(Chr(KeyAscii)))
 If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtMail_Validate(Cancel As Boolean)
TxtMail = Replace(TxtMail, "'", "")
End Sub

Private Sub txtNl_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
     If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub txtNl_Validate(Cancel As Boolean)
txtNl = Replace(txtNl, "'", "")
End Sub

Private Sub TxtRut_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
     If KeyAscii = 39 Then KeyAscii = 0
End Sub
Private Sub TxtCiudad_GotFocus()
    En_Foco TxtCiudad
End Sub

Private Sub TxtCiudad_KeyPress(KeyAscii As Integer)
KeyAscii = Asc(UCase(Chr(KeyAscii)))
 If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtComuna_GotFocus()
    En_Foco txtComuna
   
End Sub

Private Sub txtComuna_KeyPress(KeyAscii As Integer)
KeyAscii = Asc(UCase(Chr(KeyAscii)))
 If KeyAscii = 39 Then KeyAscii = 0
End Sub
Private Sub TxtRut_Validate(Cancel As Boolean)
    If Len(TxtRut.Text) = 0 Then Exit Sub
    If TxtRut.Locked = True Then Exit Sub
   
    Respuesta = VerificaRut(Me.TxtRut.Text, NuevoRut)
    If Respuesta = True Then
        Me.TxtRut.Text = NuevoRut
        Dim VeriRut As Recordset
        Consulta VeriRut, "SELECT * FROM sis_empresas WHERE rut = '" & NuevoRut & "'"
        
        
        
        If VeriRut.RecordCount > 0 Then
            With VeriRut
                 Me.TxtRz = "" & !nombre_empresa
                 TxtDirec = "" & !direccion
                 TxtCiudad = "" & !ciudad
                 txtComuna = "" & !comuna
                 txtFono = "" & !fono
                 TxtGiro = "" & !giro
                 TxtMail = "" & !Email
                
                 Me.InhabilitaCajas (True)
                 NoGuardar = True
            End With
            If Bp_Nuevo Then
                MsgBox "Cliente ya existe", vbInformation
                CmdGuarda.Enabled = False
            End If
        Else
            Me.InhabilitaCajas (False)
            NoGuardar = False
        End If
        
    Else
        TxtRut.Text = ""
        NoGuardar = False
    End If
End Sub

Private Sub TxtRz_GotFocus()
    En_Foco TxtRz
End Sub

Private Sub TxtRz_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
     If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtRz_Validate(Cancel As Boolean)
TxtRz = Replace(TxtRz, "'", "")
End Sub

Private Sub TxtSucCiudad_KeyPress(KeyAscii As Integer)
 KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub TxtSucContacto_KeyPress(KeyAscii As Integer)
 KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub TxtSucDireccion_KeyPress(KeyAscii As Integer)
 KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub
