VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form Mantenedor_Simple 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Mantenedor"
   ClientHeight    =   5505
   ClientLeft      =   5415
   ClientTop       =   1575
   ClientWidth     =   8475
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5505
   ScaleWidth      =   8475
   ShowInTaskbar   =   0   'False
   Begin VB.Timer Timer1 
      Interval        =   100
      Left            =   675
      Top             =   4575
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   150
      OleObjectBlob   =   "Mantenedor_Simple_.frx":0000
      Top             =   4560
   End
   Begin VB.CommandButton cmdSalir 
      Cancel          =   -1  'True
      Caption         =   "&Retornar"
      Height          =   465
      Left            =   6225
      TabIndex        =   9
      Top             =   4455
      Width           =   1185
   End
   Begin VB.Frame FrmMantenedor 
      Caption         =   "Sub-Categorias"
      Height          =   4125
      Left            =   135
      TabIndex        =   3
      Top             =   315
      Width           =   7275
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   210
         Left            =   480
         OleObjectBlob   =   "Mantenedor_Simple_.frx":0234
         TabIndex        =   10
         Top             =   3720
         Width           =   6675
      End
      Begin VB.TextBox TxtId 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         Height          =   315
         Left            =   495
         Locked          =   -1  'True
         TabIndex        =   4
         TabStop         =   0   'False
         Tag             =   "T"
         Top             =   615
         Width           =   615
      End
      Begin VB.ComboBox CboActivo 
         Appearance      =   0  'Flat
         Height          =   315
         ItemData        =   "Mantenedor_Simple_.frx":0332
         Left            =   5670
         List            =   "Mantenedor_Simple_.frx":033C
         Style           =   2  'Dropdown List
         TabIndex        =   1
         Top             =   615
         Width           =   960
      End
      Begin VB.TextBox TxtNombre 
         Height          =   315
         Left            =   1110
         TabIndex        =   0
         Tag             =   "T"
         Top             =   615
         Width           =   4560
      End
      Begin VB.CommandButton CmdOk 
         Caption         =   "Ok"
         Height          =   300
         Left            =   6615
         TabIndex        =   2
         Top             =   600
         Width           =   495
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   255
         Index           =   3
         Left            =   1095
         OleObjectBlob   =   "Mantenedor_Simple_.frx":0348
         TabIndex        =   5
         Top             =   405
         Width           =   1095
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel5 
         Height          =   255
         Index           =   2
         Left            =   5580
         OleObjectBlob   =   "Mantenedor_Simple_.frx":03B2
         TabIndex        =   6
         Top             =   420
         Width           =   1065
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   255
         Index           =   5
         Left            =   495
         OleObjectBlob   =   "Mantenedor_Simple_.frx":042C
         TabIndex        =   7
         Top             =   405
         Width           =   615
      End
      Begin MSComctlLib.ListView LvDetalle 
         Height          =   2730
         Left            =   495
         TabIndex        =   8
         Top             =   960
         Width           =   6660
         _ExtentX        =   11748
         _ExtentY        =   4815
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   3
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Id"
            Object.Width           =   1058
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Nombre"
            Object.Width           =   8043
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Habilitado"
            Object.Width           =   1614
         EndProperty
      End
   End
End
Attribute VB_Name = "Mantenedor_Simple"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public S_tabla As String
Public S_Id As String
Public S_Nombre As String
Public S_Activo As String
Public S_Consulta As String
Public B_Editable As Boolean
Public S_RutEmpresa As String
Dim rs_Mantenedor As Recordset, s_Sql As String
Dim Sm_LP As String


Private Sub CmdOk_Click()
    Dim Bp_Crear_LP As Boolean, Lp_NuevoID As Long
    Bp_Crear_LP = False
    If Len(TxtNombre) = 0 Then
        MsgBox "Faltan datos...", vbInformation
        TxtNombre.SetFocus
        Exit Sub
    End If
    
    
    Sql = "SELECT " & S_Id & " " & _
          "FROM " & S_tabla & " " & _
          "WHERE " & S_Nombre & "='" & TxtNombre & "' " & IIf(S_RutEmpresa = "SI", " AND rut_emp='" & SP_Rut_Activo & "'", "")
          
    Consulta RsTmp2, Sql
    If RsTmp2.RecordCount > 0 Then
        If Val(TxtId) > 0 Then
        
        Else
            MsgBox "Ya existe un registro con este nombre...", vbInformation
            TxtNombre.SetFocus
            Exit Sub
        End If
    End If
    If Val(TxtId) = 0 Then
        Lp_NuevoID = UltimoNro(S_tabla, S_Id)
        If S_RutEmpresa = "NO" Then
            s_Sql = "INSERT INTO " & S_tabla & _
                  " (" & S_Id & "," & S_Nombre & "," & S_Activo & ") " & _
                  "VALUES (" & Lp_NuevoID & ",'" & TxtNombre & "','" & CboActivo.Text & "')"
        Else
            s_Sql = "INSERT INTO " & S_tabla & _
                  " (" & S_Id & "," & S_Nombre & "," & S_Activo & ",rut_emp) " & _
                     "VALUES (" & Lp_NuevoID & ",'" & TxtNombre & "','" & CboActivo.Text & "','" & SP_Rut_Activo & "')"
                  
           Bp_Crear_LP = True
                  
        End If
    Else
            s_Sql = "UPDATE " & S_tabla & _
                  " SET " & S_Nombre & "='" & TxtNombre & "'," & S_Activo & "='" & CboActivo.Text & "' " & _
                  " WHERE " & S_Id & "=" & Val(TxtId)
    End If
    cn.Execute s_Sql
    
    
    If Sm_LP = "LISTAP" And Bp_Crear_LP Then
        'Mantenedor corresponde a lista de precios"
        s_Sql = "INSERT INTO par_lista_precios_detalle (lst_id,id,lsd_precio) " & _
                "SELECT " & Lp_NuevoID & ",m.id,precio_venta " & _
                "FROM maestro_productos m " & _
                "WHERE m.rut_emp='" & SP_Rut_Activo & "'"
        cn.Execute s_Sql
        MsgBox "Lista de precios creada...", vbInformation
        
    End If
    
    TxtId = ""
    TxtNombre = Empty
    CboActivo.ListIndex = 0
    CargaDatos
    LvDetalle.SetFocus
End Sub

Private Sub CmdSalir_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    Centrar Me
    Aplicar_skin Me
    CboActivo.ListIndex = 0
    If Not B_Editable Then
        CmdOk.Enabled = False
        Me.Caption = Me.Caption & " (Solo lectura) "
    Else
        CmdOk.Enabled = True
    End If
    Sm_LP = ""
    Sql = "SELECT men_infoextra " & _
          "FROM sis_menu " & _
          "WHERE men_id=" & IP_IDMenu
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then Sm_LP = RsTmp!men_infoextra
    CargaDatos
    
End Sub
Private Sub CargaDatos()
On Error GoTo elrrr
    Consulta rs_Mantenedor, S_Consulta & IIf(S_RutEmpresa = "SI", " AND rut_emp='" & SP_Rut_Activo & "'", "")
    LLenar_Grilla rs_Mantenedor, Me, LvDetalle, False, True, True, False
    TxtNombre.SetFocus
    Exit Sub
elrrr:
' nanda
End Sub

Private Sub LvDetalle_DblClick()
  '  ordListView ColumnHeader, Me, LvDetalles
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    TxtId = LvDetalle.SelectedItem.Text
    TxtNombre = LvDetalle.SelectedItem.SubItems(1)
    CboActivo.ListIndex = IIf(LvDetalle.SelectedItem.SubItems(2) = "SI", 0, 1)
End Sub

Private Sub Timer1_Timer()
    If B_Editable Then
        CmdOk.Enabled = True
    Else
        CmdOk.Enabled = False
    End If
    TxtNombre.SetFocus
    Timer1.Enabled = False
End Sub

Private Sub TxtNombre_GotFocus()
    En_Foco TxtNombre
End Sub

Private Sub TxtNombre_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
    If KeyAscii = 39 Then KeyAscii = 0
End Sub
