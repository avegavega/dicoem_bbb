VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form Mantenedor_Impuestos 
   Caption         =   "Mantenedor de Impuestos"
   ClientHeight    =   5985
   ClientLeft      =   6555
   ClientTop       =   3330
   ClientWidth     =   6945
   LinkTopic       =   "Form1"
   ScaleHeight     =   5985
   ScaleWidth      =   6945
   Begin VB.Timer Timer1 
      Interval        =   50
      Left            =   75
      Top             =   5745
   End
   Begin VB.CommandButton CmdSalir 
      Cancel          =   -1  'True
      Caption         =   "&Retornar"
      Height          =   330
      Left            =   5550
      TabIndex        =   11
      Top             =   5520
      Width           =   1155
   End
   Begin VB.Frame Frame6 
      Caption         =   "Impuestos"
      Height          =   4770
      Index           =   0
      Left            =   480
      TabIndex        =   0
      Top             =   645
      Width           =   6225
      Begin VB.CommandButton CmdOk 
         Caption         =   "Ok"
         Height          =   315
         Left            =   5385
         TabIndex        =   5
         Top             =   840
         Width           =   495
      End
      Begin VB.TextBox txtNombre 
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   1200
         TabIndex        =   2
         Tag             =   "T"
         Top             =   840
         Width           =   2415
      End
      Begin VB.TextBox TxtFactor 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   3600
         TabIndex        =   3
         Tag             =   "N"
         Top             =   840
         Width           =   975
      End
      Begin VB.ComboBox ComActivo 
         Height          =   315
         ItemData        =   "Mantenedor_Impuestos.frx":0000
         Left            =   4590
         List            =   "Mantenedor_Impuestos.frx":000A
         Style           =   2  'Dropdown List
         TabIndex        =   4
         Top             =   840
         Width           =   800
      End
      Begin VB.TextBox TxtId 
         Appearance      =   0  'Flat
         BackColor       =   &H80000004&
         Height          =   315
         Left            =   600
         Locked          =   -1  'True
         TabIndex        =   1
         Tag             =   "T"
         Top             =   840
         Width           =   615
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   255
         Index           =   3
         Left            =   1200
         OleObjectBlob   =   "Mantenedor_Impuestos.frx":0016
         TabIndex        =   6
         Top             =   630
         Width           =   1095
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   255
         Index           =   4
         Left            =   3660
         OleObjectBlob   =   "Mantenedor_Impuestos.frx":0080
         TabIndex        =   7
         Top             =   645
         Width           =   615
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel5 
         Height          =   255
         Index           =   2
         Left            =   4485
         OleObjectBlob   =   "Mantenedor_Impuestos.frx":00EA
         TabIndex        =   8
         Top             =   630
         Width           =   1050
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   255
         Index           =   5
         Left            =   600
         OleObjectBlob   =   "Mantenedor_Impuestos.frx":0164
         TabIndex        =   9
         Top             =   630
         Width           =   615
      End
      Begin MSComctlLib.ListView LVDetalle 
         Height          =   3180
         Left            =   585
         TabIndex        =   10
         Top             =   1185
         Width           =   5325
         _ExtentX        =   9393
         _ExtentY        =   5609
         View            =   3
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   4
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Id"
            Object.Width           =   1058
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Descripcion"
            Object.Width           =   4260
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "N102"
            Text            =   "Factor"
            Object.Width           =   1720
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "Activo"
            Object.Width           =   1411
         EndProperty
      End
   End
End
Attribute VB_Name = "Mantenedor_Impuestos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub CmdOk_Click()
    If Len(TxtNombre) = 0 Then
        MsgBox "Debe ingresar Descripcion...", vbInformation
        Exit Sub
    ElseIf Val(txtFactor) = 0 Then
        MsgBox "Debe ingresar factor...", vbInformation
        Exit Sub
    End If
    
    If Val(TxtId) = 0 Then
        Sql = "INSERT INTO par_impuestos (imp_nombre,imp_adicional) VALUES (" & _
               "'" & TxtNombre & "'," & txtFactor & "   )"
    Else
        Sql = "UPDATE par_impuestos SET imp_nombre='" & TxtNombre & "',imp_adicional=" & txtFactor & ",imp_activo='" & ComActivo.Text & "' " & _
              "WHERE imp_id=" & TxtId
    End If
    TxtId = ""
    TxtNombre = ""
    ComActivo.ListIndex = 0
    txtFactor = ""
    Consulta RsTmp, Sql
    CargaImpuestos
    
End Sub

Private Sub CmdSalir_Click()
    Unload Me
End Sub
Private Sub Form_Load()
        
    Aplicar_skin Me
    ComActivo.ListIndex = 0
    CargaImpuestos
          
End Sub
Private Sub CargaImpuestos()
    Sql = "SELECT imp_id,imp_nombre,imp_adicional,imp_activo " & _
          "FROM par_impuestos "
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvDetalle, False, True, True, False

End Sub
Private Sub LvDetalle_DblClick()
  '  ordListView ColumnHeader, Me, LvDetalle
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    TxtId = LvDetalle.SelectedItem.Text
    TxtNombre = LvDetalle.SelectedItem.SubItems(1)
    txtFactor = Replace(LvDetalle.SelectedItem.SubItems(2), ",", ".")
    ComActivo.ListIndex = IIf(LvDetalle.SelectedItem.SubItems(3) = "SI", 0, 1)
    TxtNombre.SetFocus
End Sub

Private Sub Timer1_Timer()
    TxtNombre.SetFocus
    Timer1.Enabled = False
End Sub

Private Sub TxtFactor_KeyPress(KeyAscii As Integer)
    KeyAscii = AceptaSoloNumeros(KeyAscii)
    If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtNombre_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
    If KeyAscii = 39 Then KeyAscii = 0
    
End Sub
