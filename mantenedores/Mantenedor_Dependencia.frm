VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form Mantenedor_Dependencia 
   Caption         =   "Mantenedor con dependencias"
   ClientHeight    =   5400
   ClientLeft      =   2445
   ClientTop       =   4170
   ClientWidth     =   10530
   LinkTopic       =   "Form1"
   ScaleHeight     =   5400
   ScaleWidth      =   10530
   Begin VB.Frame FrmMantenedor 
      Caption         =   "Categorias"
      Height          =   4125
      Left            =   345
      TabIndex        =   5
      Top             =   525
      Width           =   9915
      Begin VB.CommandButton CmdBusca 
         Caption         =   "Buscar"
         Height          =   195
         Left            =   3405
         TabIndex        =   13
         ToolTipText     =   "Buscar plan de cuenta"
         Top             =   390
         Visible         =   0   'False
         Width           =   645
      End
      Begin VB.ComboBox CboDep 
         Appearance      =   0  'Flat
         Height          =   315
         ItemData        =   "Mantenedor_Dependencia.frx":0000
         Left            =   1110
         List            =   "Mantenedor_Dependencia.frx":0002
         Style           =   2  'Dropdown List
         TabIndex        =   0
         Top             =   600
         Width           =   2985
      End
      Begin VB.CommandButton CmdOk 
         Caption         =   "Ok"
         Height          =   300
         Left            =   9165
         TabIndex        =   3
         Top             =   600
         Width           =   495
      End
      Begin VB.TextBox TxtNombre 
         Height          =   315
         Left            =   4095
         TabIndex        =   1
         Tag             =   "T"
         Top             =   600
         Width           =   4125
      End
      Begin VB.ComboBox CboActivo 
         Appearance      =   0  'Flat
         Height          =   315
         ItemData        =   "Mantenedor_Dependencia.frx":0004
         Left            =   8220
         List            =   "Mantenedor_Dependencia.frx":000E
         Style           =   2  'Dropdown List
         TabIndex        =   2
         Top             =   600
         Width           =   915
      End
      Begin VB.TextBox TxtId 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         Height          =   315
         Left            =   495
         Locked          =   -1  'True
         TabIndex        =   7
         TabStop         =   0   'False
         Tag             =   "T"
         Top             =   600
         Width           =   600
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   210
         Left            =   480
         OleObjectBlob   =   "Mantenedor_Dependencia.frx":001A
         TabIndex        =   6
         Top             =   3720
         Width           =   6675
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   255
         Index           =   3
         Left            =   4095
         OleObjectBlob   =   "Mantenedor_Dependencia.frx":0118
         TabIndex        =   8
         Top             =   390
         Width           =   1095
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel5 
         Height          =   255
         Index           =   2
         Left            =   8115
         OleObjectBlob   =   "Mantenedor_Dependencia.frx":0182
         TabIndex        =   9
         Top             =   405
         Width           =   1065
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   255
         Index           =   5
         Left            =   495
         OleObjectBlob   =   "Mantenedor_Dependencia.frx":01FC
         TabIndex        =   10
         Top             =   390
         Width           =   615
      End
      Begin MSComctlLib.ListView LvDetalle 
         Height          =   2730
         Left            =   525
         TabIndex        =   11
         Top             =   930
         Width           =   9135
         _ExtentX        =   16113
         _ExtentY        =   4815
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   5
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Id"
            Object.Width           =   1058
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "T3000"
            Text            =   "Dependencia"
            Object.Width           =   5265
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "T4000"
            Text            =   "Nombre"
            Object.Width           =   7276
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "Activo"
            Object.Width           =   1614
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Object.Tag             =   "N109"
            Text            =   "id_combo"
            Object.Width           =   0
         EndProperty
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkCombo 
         Height          =   255
         Left            =   1125
         OleObjectBlob   =   "Mantenedor_Dependencia.frx":025E
         TabIndex        =   12
         Top             =   405
         Width           =   2580
      End
   End
   Begin VB.CommandButton cmdSalir 
      Cancel          =   -1  'True
      Caption         =   "&Retornar"
      Height          =   465
      Left            =   9060
      TabIndex        =   4
      Top             =   4755
      Width           =   1185
   End
   Begin VB.Timer Timer1 
      Interval        =   20
      Left            =   885
      Top             =   4785
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   360
      OleObjectBlob   =   "Mantenedor_Dependencia.frx":02C8
      Top             =   4770
   End
End
Attribute VB_Name = "Mantenedor_Dependencia"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public S_tabla As String
Public S_Id As String
Public S_Nombre As String
Public S_Activo As String
Public S_Consulta As String
Public B_Editable As Boolean
Public S_id_dep As String
Public S_nombre_dep As String
Public S_tabla_dep As String
Public S_Activo_dep As String
Public S_titulo_dep As String
Public S_RutEmpresa As String
Dim rs_Mantenedor As Recordset, s_Sql As String



Private Sub CmdBusca_Click()
    With BuscarSimple
        SG_codigo = Empty
        .Sm_Consulta = "SELECT pla_id, pla_nombre,tpo_nombre,det_nombre " & _
                       "FROM    con_plan_de_cuentas p,  con_tipo_de_cuenta t,   con_detalle_tipo_cuenta d " & _
                       "WHERE   p.tpo_id = t.tpo_id AND p.det_id = d.det_id "
        .Sm_CampoLike = "pla_nombre"
        .Im_Columnas = 2
        .Show 1
        If SG_codigo = Empty Then Exit Sub
        Busca_Id_Combo CboDep, Val(SG_codigo)
        CboDep.SetFocus
    End With
End Sub

Private Sub CmdOk_Click()
    If Len(TxtNombre) = 0 Then
        MsgBox "Faltan datos...", vbInformation
        TxtNombre.SetFocus
        Exit Sub
    End If
    If CboDep.ListIndex = -1 Then
        MsgBox "Debe seleccionar " & S_titulo_dep
        CboDep.SetFocus
        Exit Sub
    End If
    
    Sql = "SELECT " & S_Id & " " & _
          "FROM " & S_tabla & " " & _
          "WHERE " & S_id_dep & "=" & Me.CboDep.ItemData(CboDep.ListIndex) & " AND " & S_Nombre & "='" & TxtNombre & "' AND rut_emp='" & SP_Rut_Activo & "'"
    Consulta RsTmp2, Sql
    If RsTmp2.RecordCount > 0 Then
        If Val(TxtId) > 0 Then
        
        Else
            MsgBox "Ya existe un registro con este nombre...", vbInformation
            TxtNombre.SetFocus
            Exit Sub
        End If
    End If
    If Val(TxtId) = 0 Then
        s_Sql = "INSERT INTO " & S_tabla & _
              " (" & S_id_dep & "," & S_Nombre & "," & S_Activo & ",rut_emp) " & _
              "VALUES (" & CboDep.ItemData(CboDep.ListIndex) & ",'" & TxtNombre & "','" & CboActivo.Text & "','" & SP_Rut_Activo & "')"
    Else
        s_Sql = "UPDATE " & S_tabla & _
              " SET " & S_id_dep & "=" & CboDep.ItemData(CboDep.ListIndex) & "," & S_Nombre & "='" & TxtNombre & "'," & S_Activo & "='" & CboActivo.Text & "' " & _
              " WHERE " & S_Id & "=" & Val(TxtId)
    End If
    Consulta rs_Mantenedor, s_Sql
    
    TxtId = ""
    TxtNombre = Empty
    CboActivo.ListIndex = 0
    CargaDatos
    LvDetalle.SetFocus
End Sub

Private Sub CmdSalir_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    Centrar Me
    Aplicar_skin Me
    CboActivo.ListIndex = 0
    If Not B_Editable Then
        CmdOk.Enabled = False
        Me.Caption = Me.Caption & " (Solo lectura) "
    Else
        CmdOk.Enabled = True
    End If
   
    Sql = "SELECT men_infoextra " & _
          "FROM sis_menu " & _
          "WHERE men_id=" & IP_IDMenu
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        Sp_extra = RsTmp!men_infoextra
    End If
    
    If Sp_extra = "BOTON" Then
        CmdBusca.Visible = True
    End If
   
   
   
   
   
    CargaDatos
    
    
    If Sp_extra <> "OTROS" Then
        Sql = "SELECT emp_cuenta " & _
              "FROM sis_empresas " & _
              "WHERE rut='" & SP_Rut_Activo & "'"
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
            
                'If RsTmp!emp_cuenta = "NO" Then S_Activo_dep = S_Activo_dep & " AND pla_id=99999 "
            If RsTmp!emp_cuenta = "NO" Then S_Activo_dep = S_Activo_dep & " AND pla_id=99999 "
        End If
    End If
    
    
    LLenarCombo CboDep, Me.S_nombre_dep, Me.S_id_dep, Me.S_tabla_dep, Me.S_Activo_dep

End Sub

Private Sub CargaDatos()
     Me.SkCombo = Me.S_titulo_dep
    LvDetalle.ColumnHeaders(2).Text = S_titulo_dep


    Consulta rs_Mantenedor, S_Consulta & " WHERE rut_emp='" & SP_Rut_Activo & "'"
    LLenar_Grilla rs_Mantenedor, Me, LvDetalle, False, True, True, False
 '   Aplicar_skin Me
End Sub

Private Sub LvDetalle_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    ordListView ColumnHeader, Me, LvDetalle
End Sub

Private Sub LvDetalle_DblClick()
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    TxtId = LvDetalle.SelectedItem.Text
    TxtNombre = LvDetalle.SelectedItem.SubItems(2)
    CboActivo.ListIndex = IIf(LvDetalle.SelectedItem.SubItems(3) = "SI", 0, 1)
    Busca_Id_Combo CboDep, LvDetalle.SelectedItem.SubItems(4)
End Sub

Private Sub Timer1_Timer()
    If B_Editable Then
        CmdOk.Enabled = True
    Else
        CmdOk.Enabled = False
    End If
    TxtNombre.SetFocus
    Timer1.Enabled = False
End Sub

Private Sub TxtNombre_GotFocus()
    En_Foco TxtNombre
End Sub

Private Sub TxtNombre_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
    If KeyAscii = 39 Then KeyAscii = 0
End Sub


