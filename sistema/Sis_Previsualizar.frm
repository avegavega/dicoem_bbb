VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Begin VB.Form Sis_Previsualizar 
   AutoRedraw      =   -1  'True
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Comprobante"
   ClientHeight    =   10350
   ClientLeft      =   3870
   ClientTop       =   450
   ClientWidth     =   12090
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   10350
   ScaleWidth      =   12090
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton CmdSalir 
      Caption         =   "No Imprimir"
      Height          =   435
      Left            =   6405
      TabIndex        =   2
      Top             =   9810
      Width           =   2115
   End
   Begin VB.CommandButton CmdPrint 
      Caption         =   "Imprimir"
      Height          =   435
      Left            =   4230
      TabIndex        =   1
      Top             =   9810
      Width           =   2115
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   615
      OleObjectBlob   =   "Sis_Previsualizar.frx":0000
      Top             =   10230
   End
   Begin VB.Timer Timer1 
      Interval        =   10
      Left            =   60
      Top             =   10185
   End
   Begin VB.PictureBox Pic 
      Height          =   10380
      Left            =   -30
      ScaleHeight     =   10320
      ScaleWidth      =   12015
      TabIndex        =   0
      Top             =   -45
      Width           =   12075
   End
End
Attribute VB_Name = "Sis_Previsualizar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub CmdPrint_Click()
    SG_codigo = "print"
    Unload Me
End Sub

Private Sub CmdSalir_Click()
    SG_codigo = Empty
    Unload Me
End Sub

Private Sub Form_Load()
    Aplicar_skin Me
    Centrar Me
End Sub

Private Sub Timer1_Timer()
    On Error Resume Next
    Me.CmdPrint.SetFocus
    Timer1.Enabled = False
End Sub
