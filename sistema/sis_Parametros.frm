VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Begin VB.Form sis_Parametros 
   Caption         =   "Parametros"
   ClientHeight    =   6045
   ClientLeft      =   4935
   ClientTop       =   2685
   ClientWidth     =   6930
   LinkTopic       =   "Form1"
   ScaleHeight     =   6045
   ScaleWidth      =   6930
   Begin VB.PictureBox PicLogo 
      Height          =   1080
      Left            =   -15
      ScaleHeight     =   1020
      ScaleWidth      =   6675
      TabIndex        =   18
      Top             =   4935
      Width           =   6735
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   15
      OleObjectBlob   =   "sis_Parametros.frx":0000
      Top             =   3270
   End
   Begin VB.CommandButton cmdSalir 
      Cancel          =   -1  'True
      Caption         =   "&Retornar"
      Height          =   465
      Left            =   5550
      TabIndex        =   7
      ToolTipText     =   "Salir sin hacer cambios"
      Top             =   4290
      Width           =   1050
   End
   Begin VB.Frame sis_Parametros 
      Caption         =   "Parametros"
      Height          =   4860
      Left            =   -60
      TabIndex        =   0
      Top             =   90
      Width           =   6855
      Begin VB.ComboBox CboOficinaContable 
         Height          =   315
         ItemData        =   "sis_Parametros.frx":0234
         Left            =   1995
         List            =   "sis_Parametros.frx":023E
         Style           =   2  'Dropdown List
         TabIndex        =   21
         ToolTipText     =   "Solo registrar� contabilidad sin detalle de productos"
         Top             =   2940
         Width           =   885
      End
      Begin VB.ComboBox CboSucursales 
         Height          =   315
         Left            =   1995
         Style           =   2  'Dropdown List
         TabIndex        =   20
         ToolTipText     =   "Sucursal de la empresa"
         Top             =   2295
         Width           =   3765
      End
      Begin VB.PictureBox Picture1 
         Height          =   30
         Left            =   75
         ScaleHeight     =   30
         ScaleWidth      =   5340
         TabIndex        =   17
         Top             =   4035
         Width           =   5340
      End
      Begin VB.ComboBox CboDatosContables 
         Height          =   315
         ItemData        =   "sis_Parametros.frx":024A
         Left            =   3270
         List            =   "sis_Parametros.frx":0254
         Style           =   2  'Dropdown List
         TabIndex        =   16
         ToolTipText     =   "No... significa que no aparecen en Ventas Datos Contables.."
         Top             =   4245
         Visible         =   0   'False
         Width           =   780
      End
      Begin VB.ComboBox CboArea 
         Height          =   315
         Left            =   1995
         Style           =   2  'Dropdown List
         TabIndex        =   13
         ToolTipText     =   "Debe crear Areas si desea cambiar esta opci�n"
         Top             =   2610
         Width           =   2800
      End
      Begin VB.CommandButton CmdCambiarEmpresa 
         Caption         =   "&Activa otra Empresa"
         Height          =   345
         Left            =   3420
         TabIndex        =   12
         Top             =   1575
         Width           =   1905
      End
      Begin VB.TextBox skEmpresa 
         BackColor       =   &H00FFFFC0&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   1995
         Locked          =   -1  'True
         TabIndex        =   11
         TabStop         =   0   'False
         Text            =   "Text1"
         Top             =   1920
         Width           =   3735
      End
      Begin VB.TextBox SkRut 
         BackColor       =   &H00FFFFC0&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   1995
         Locked          =   -1  'True
         TabIndex        =   10
         TabStop         =   0   'False
         Text            =   "Text1"
         Top             =   1575
         Width           =   1395
      End
      Begin VB.CommandButton CmdGuardaParametros 
         Caption         =   "&Guardar Parametros"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   465
         Left            =   165
         TabIndex        =   8
         Top             =   4185
         Width           =   2040
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   240
         Left            =   300
         OleObjectBlob   =   "sis_Parametros.frx":0260
         TabIndex        =   4
         Top             =   570
         Width           =   1650
      End
      Begin VB.TextBox TxtIVA 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1995
         TabIndex        =   3
         Tag             =   "N"
         Text            =   "0"
         Top             =   1230
         Width           =   975
      End
      Begin VB.ComboBox CboAno 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         ItemData        =   "sis_Parametros.frx":02D6
         Left            =   1995
         List            =   "sis_Parametros.frx":02D8
         Style           =   2  'Dropdown List
         TabIndex        =   2
         Top             =   900
         Width           =   1260
      End
      Begin VB.ComboBox CboMes 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         ItemData        =   "sis_Parametros.frx":02DA
         Left            =   1995
         List            =   "sis_Parametros.frx":02DC
         Style           =   2  'Dropdown List
         TabIndex        =   1
         Top             =   510
         Width           =   1725
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   240
         Left            =   180
         OleObjectBlob   =   "sis_Parametros.frx":02DE
         TabIndex        =   5
         Top             =   960
         Width           =   1770
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   240
         Left            =   825
         OleObjectBlob   =   "sis_Parametros.frx":0354
         TabIndex        =   6
         Top             =   1305
         Width           =   1125
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
         Height          =   240
         Left            =   300
         OleObjectBlob   =   "sis_Parametros.frx":03C6
         TabIndex        =   9
         Top             =   1635
         Width           =   1650
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel14 
         Height          =   180
         Index           =   2
         Left            =   405
         OleObjectBlob   =   "sis_Parametros.frx":0440
         TabIndex        =   14
         Top             =   2640
         Width           =   1515
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel14 
         Height          =   270
         Index           =   1
         Left            =   1485
         OleObjectBlob   =   "sis_Parametros.frx":04BE
         TabIndex        =   15
         Top             =   3990
         Visible         =   0   'False
         Width           =   3810
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel5 
         Height          =   240
         Left            =   225
         OleObjectBlob   =   "sis_Parametros.frx":056C
         TabIndex        =   19
         Top             =   2325
         Width           =   1650
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel14 
         Height          =   180
         Index           =   0
         Left            =   345
         OleObjectBlob   =   "sis_Parametros.frx":05DA
         TabIndex        =   22
         Top             =   2970
         Width           =   1515
      End
   End
End
Attribute VB_Name = "sis_Parametros"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False










Private Sub CmdCambiarEmpresa_Click()
    SG_codigo = Empty
    BuscaEmpresa.Show 1
    If SG_codigo = Empty Then Exit Sub
    Sql = "SELECT emp_id, rut,nombre_empresa,control_inventario,are_id,emp_seleciona_datos_contables_venta datos " & _
          "FROM sis_empresas " & _
          "WHERE emp_id=" & SG_codigo
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        Me.Tag = RsTmp!emp_id
        SkRut = RsTmp!Rut
        SkEmpresa = RsTmp!nombre_empresa
        SkRut.Tag = RsTmp!control_inventario
        LLenarCombo CboArea, "are_nombre", "are_id", "par_areas", "are_activo='SI' AND rut_emp='" & SkRut & "'"
        Busca_Id_Combo CboArea, RsTmp!are_id
        Me.CboDatosContables.ListIndex = IIf(RsTmp!datos = "SI", 0, 1)
        
    End If
    
End Sub

Private Sub CmdGuardaParametros_Click()
    If Val(TxtIva) = 0 Then
        MsgBox "Valor de FACTOR IVA no es valido...", vbOKOnly + vbInformation
        TxtIva.SetFocus
        Exit Sub
    End If
    
    If CboArea.ListIndex = -1 Then
        MsgBox "Seleccione area para ventas...", vbInformation + vbOKOnly
        CboArea.SetFocus
        Exit Sub
    End If
    'Factor IVA
    Sql = "UPDATE tabla_parametros " & _
          "SET par_valor='" & TxtIva & "' " & _
          "WHERE par_id=1"
          DG_IVA = TxtIva
    Consulta RsTmp, Sql
    'A�� Contable
    Sql = "UPDATE tabla_parametros " & _
          "SET par_valor='" & CboAno.Text & "' " & _
          "WHERE par_id=13"
          IG_Ano_Contable = CboAno.Text
    Consulta RsTmp, Sql
    'Mes Contable
    Sql = "UPDATE tabla_parametros " & _
          "SET par_valor='" & CboMes.ListIndex + 1 & "' " & _
          "WHERE par_id=14"
          IG_Mes_Contable = CboMes.ListIndex + 1
    Consulta RsTmp, Sql
    
    'Empresa activa
    Sql = "UPDATE tabla_parametros " & _
          "SET par_valor='" & SkRut & "' " & _
          "WHERE par_id=17"
    Consulta RsTmp, Sql
    
    'Empresa activa
    Sql = "UPDATE tabla_parametros " & _
          "SET par_valor='" & SkEmpresa & "' " & _
          "WHERE par_id=18"
    Consulta RsTmp, Sql
    
    'Empresa activa
    Sql = "UPDATE tabla_parametros " & _
          "SET par_valor='" & SkRut.Tag & "' " & _
          "WHERE par_id=19"
    cn.Execute Sql
    
    
    'Area de venta empresa
    Sql = "UPDATE sis_empresas SET are_id=" & CboArea.ItemData(CboArea.ListIndex) & "," & _
                "emp_seleciona_datos_contables_venta='" & Me.CboDatosContables.Text & "' " & _
          "WHERE rut='" & SkRut & "'"
    cn.Execute Sql
    
    
    
    Sql = "SELECT rut FROM sis_empresa_activa " & _
          "WHERE UPPER(ema_nombre_equipo)='" & UCase(SP_Nombre_Equipo) & "'"
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        Sql = "UPDATE sis_empresa_activa " & _
             "SET rut='" & SkRut & "', " & _
             "ema_id_empresa=" & Me.Tag & "," & _
             "mes_contable=" & CboMes.ListIndex + 1 & ",ano_contable=" & Me.CboAno.Text & ",sue_id=" & CboSucursales.ItemData(CboSucursales.ListIndex) & " " & _
             ",ema_oficina_contable='" & CboOficinaContable.Text & "' " & _
             "WHERE ema_nombre_equipo='" & SP_Nombre_Equipo & "'"
    Else
        Sql = "INSERT INTO sis_empresa_activa (ema_nombre_equipo,rut,ema_id_empresa,sue_id,ema_oficina_contable) " & _
             "VALUES('" & SP_Nombre_Equipo & "','" & SkRut & "'," & Me.Tag & "," & CboSucursales.ItemData(CboSucursales.ListIndex) & ",'" & CboOficinaContable.Text & "')"
    End If
    cn.Execute Sql
    
    
    If SP_Rut_Activo <> SkRut Then MsgBox "Ha cambiado la Empresa Activa...", vbInformation
    IG_id_Empresa = Me.Tag
    SP_Rut_Activo = SkRut
    SP_Empresa_Activa = SkEmpresa
    SP_Control_Inventario = SkRut.Tag
    Principal.SkEmpresa = SkEmpresa
    Principal.SkRut = SkRut
    Principal.CboAno = IG_Ano_Contable
    Principal.CboMes = UCase(MonthName(IG_Mes_Contable))
    Principal.Skconsininventario = "CONTROL DE EXISTENCIAS: " & SP_Control_Inventario
    Principal.txtSucursal = CboSucursales.Text
    Principal.txtSucursal.Tag = CboSucursales.ItemData(CboSucursales.ListIndex)
    
    IG_Ano_Contable = CboAno.Text
    IG_Mes_Contable = CboMes.ListIndex + 1
    IG_id_Sucursal_Empresa = CboSucursales.ItemData(CboSucursales.ListIndex)
    SG_Oficina_Contable = CboOficinaContable.Text
    
     
    Unload Me
    
    
End Sub

Private Sub CmdSalir_Click()
    Unload Me
End Sub



Private Sub Form_Load()
    Centrar Me
    Aplicar_skin Me
    On Error Resume Next
    PicLogo.Picture = LoadPicture(App.Path & "\REDMAROK.jpg")
    Me.CboOficinaContable.ListIndex = 1
    
    
    SkRut = SP_Rut_Activo
    SkEmpresa = SP_Empresa_Activa
    SkRut.Tag = SP_Control_Inventario
    Me.Tag = IG_id_Empresa
    For i = 1 To 12
        CboMes.AddItem UCase(MonthName(i))
    Next
    
   'For i = (Year(Date) - 1) To Year(Date) + 1
   LLenaYears CboAno, 2010
   ' For i = Year(Date) To 2010 Step -1
   '     CboAno.AddItem i
   ' Next
    LLenarCombo CboArea, "are_nombre", "are_id", "par_areas", "are_activo='SI' AND rut_emp='" & SP_Rut_Activo & "'"

    Sql = "SELECT are_id,emp_seleciona_datos_contables_venta datoscontables " & _
          "FROM sis_empresas " & _
          "WHERE rut='" & SP_Rut_Activo & "'"
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        Busca_Id_Combo CboArea, RsTmp!are_id
        Me.CboDatosContables.ListIndex = IIf(RsTmp!datoscontables = "SI", 0, 1)
    End If
    
    

    Sql = "SELECT par_id,par_valor " & _
          "FROM tabla_parametros " & _
          "WHERE par_id IN(1,13,14)"
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        RsTmp.MoveFirst
        Do While Not RsTmp.EOF
            If RsTmp!par_id = 1 Then TxtIva = RsTmp!par_valor
            
            If RsTmp!par_id = 14 Then
                For i = 0 To 11
                    If UCase(MonthName(i + 1)) = UCase(MonthName(RsTmp!par_valor)) Then
                        CboMes.ListIndex = i
                        Exit For
                    End If
                Next
            End If
            
            If RsTmp!par_id = 13 Then
                For i = 0 To CboAno.ListCount - 1
                    If CboAno.List(i) = RsTmp!par_valor Then
                        CboAno.ListIndex = i
                        Exit For
                    End If
                Next
            End If
            
            
            RsTmp.MoveNext
        Loop
    End If
    LLenarCombo CboSucursales, "CONCAT(sue_direccion,'-',sue_ciudad)", "sue_id", "sis_empresas_sucursales", "sue_activa='SI' AND emp_id=" & IG_id_Empresa
    CboSucursales.AddItem "CASA MATRIZ"
    If IG_id_Sucursal_Empresa = 1 Then
        CboSucursales.ItemData(CboSucursales.ListCount - 1) = 1
        CboSucursales.ListIndex = CboSucursales.ListCount - 1
        
    Else
        Busca_Id_Combo CboSucursales, Val(IG_id_Sucursal_Empresa)
    End If
    
    If SG_Oficina_Contable = "SI" Then
        Me.CboOficinaContable.ListIndex = 0
    Else: Me.CboOficinaContable.ListIndex = 1
    End If
        
    
    
End Sub



