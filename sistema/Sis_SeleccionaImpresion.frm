VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form Sis_SeleccionaImpresion 
   Caption         =   "Seleccione tipo de impresion"
   ClientHeight    =   4470
   ClientLeft      =   795
   ClientTop       =   1725
   ClientWidth     =   11415
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   ScaleHeight     =   4470
   ScaleWidth      =   11415
   Begin MSComDlg.CommonDialog Dialogo 
      Left            =   1230
      Top             =   4035
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   720
      OleObjectBlob   =   "Sis_SeleccionaImpresion.frx":0000
      Top             =   3990
   End
   Begin VB.Timer Timer1 
      Interval        =   5
      Left            =   165
      Top             =   4050
   End
   Begin VB.CommandButton CmdCancela 
      Cancel          =   -1  'True
      Caption         =   "No imprimir"
      Height          =   435
      Left            =   9375
      TabIndex        =   2
      Top             =   3915
      Width           =   1455
   End
   Begin VB.Frame Frame1 
      Caption         =   "Opciones"
      Height          =   3285
      Left            =   525
      TabIndex        =   0
      Top             =   480
      Width           =   10350
      Begin VB.ComboBox CboPuertos 
         Height          =   315
         Left            =   5400
         Style           =   2  'Dropdown List
         TabIndex        =   6
         Top             =   615
         Width           =   4815
      End
      Begin VB.ComboBox CboPrinters 
         Height          =   315
         Left            =   255
         Style           =   2  'Dropdown List
         TabIndex        =   4
         Top             =   600
         Width           =   4935
      End
      Begin VB.CommandButton CmdMatriz 
         BackColor       =   &H00FFFFFF&
         Caption         =   "Matriz de punto"
         Height          =   1965
         Left            =   6450
         Picture         =   "Sis_SeleccionaImpresion.frx":0234
         Style           =   1  'Graphical
         TabIndex        =   3
         Top             =   1110
         Width           =   2655
      End
      Begin VB.CommandButton CmdLaserTinta 
         BackColor       =   &H00FFFFFF&
         Caption         =   "Laser / Tinta"
         Height          =   1965
         Left            =   1260
         Picture         =   "Sis_SeleccionaImpresion.frx":1138
         Style           =   1  'Graphical
         TabIndex        =   1
         Top             =   1080
         Width           =   2760
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   240
         Left            =   240
         OleObjectBlob   =   "Sis_SeleccionaImpresion.frx":47DB
         TabIndex        =   5
         Top             =   390
         Width           =   2535
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   240
         Left            =   5370
         OleObjectBlob   =   "Sis_SeleccionaImpresion.frx":4865
         TabIndex        =   7
         Top             =   405
         Width           =   2535
      End
   End
End
Attribute VB_Name = "Sis_SeleccionaImpresion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub CmdCancela_Click()
    SG_codigo = Empty
    Unload Me
End Sub

Private Sub CmdLaserTinta_Click()
    SG_codigo = "laser"
   '  On Error GoTo CancelaImp
   ' Dialogo.CancelError = True
   ' Dialogo.ShowPrinter
   ' La_Establecer_Impresora CboPrinters.Text
'    MsgBox Dialogo.PrinterDefault
  '  Establecer_Impresora CboPrinters.Text
    Sm_ImpresoraSeleccionada = CboPrinters.Text
    Unload Me
    Exit Sub
CancelaImp:
    'nada
End Sub

Private Sub CmdMatriz_Click()
'    If MsgBox("Para utilizar este tipo de impresi�n debe tener una impresora " & vbNewLine & _
              "matriz de punto conectada al puerto LPT1..." & vbNewLine & "�Continuar ...?", vbQuestion + vbYesNo) = vbNo Then Exit Sub
   
    SG_codigo = "matriz"
    SG_codigo2 = CboPuertos.Text
    Unload Me
    
End Sub

Private Function La_Establecer_Impresora(ByVal NamePrinter As String) As Boolean
  
On Error GoTo errSub
  
     
  
    'Variable de referencia
  
    Dim obj_Impresora As Object
  
     
  
    'Creamos la referencia
  
    Set obj_Impresora = CreateObject("WScript.Network")
  
        obj_Impresora.setdefaultprinter NamePrinter
  
     
  
    Set obj_Impresora = Nothing
  
         
  
        'La funci�n devuelve true y se cambi� con �xito
  
       La_Establecer_Impresora = True
  
        MsgBox "La impresora se cambi� correctamente", vbInformation
  
    Exit Function
  
     
  
     
  
'Error al cambiar la impresora
  
errSub:
  
If Err.Number = 0 Then Exit Function
  
   La_Establecer_Impresora = False
  
   MsgBox "error: " & Err.Number & Chr(13) & "Description: " & Err.Description
  
   On Error GoTo 0
  
End Function

Private Sub Form_Load()
    Aplicar_skin Me
    Centrar Me
    
    For i = 0 To Printers.Count - 1
        CboPrinters.AddItem Printers(i).DeviceName
    Next
    If CboPrinters.ListCount = 0 Then
        Me.CmdLaserTinta.Enabled = False
    Else
        CboPrinters.ListIndex = 0
    End If
    LLenarCombo CboPuertos, "prt_nombre", "prt_id", "sis_puertos_lpt", "prt_activo='SI'"
    If CboPuertos.ListCount = 0 Then
        Me.CmdMatriz.Enabled = False
    Else
        CboPuertos.ListIndex = 0
    End If
End Sub

Private Sub Timer1_Timer()
    On Error Resume Next
    CmdLaserTinta.SetFocus
    Timer1.Enabled = False
End Sub
