VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{DA729E34-689F-49EA-A856-B57046630B73}#1.0#0"; "progressbar-xp.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form Busca_Producto_Repuestos 
   Caption         =   "Buscar Productos"
   ClientHeight    =   9270
   ClientLeft      =   -195
   ClientTop       =   930
   ClientWidth     =   18765
   LinkTopic       =   "Form1"
   ScaleHeight     =   9270
   ScaleWidth      =   18765
   Begin VB.Frame Frame4 
      Caption         =   "Inventario Inmovilizado"
      Height          =   1005
      Left            =   3225
      TabIndex        =   35
      Top             =   8220
      Width           =   6315
      Begin VB.CheckBox Check2 
         Caption         =   "Check2"
         Height          =   195
         Left            =   75
         TabIndex        =   41
         Top             =   360
         Width           =   300
      End
      Begin VB.CommandButton CmdInvInmo 
         Caption         =   "Consultar"
         Enabled         =   0   'False
         Height          =   525
         Left            =   5085
         TabIndex        =   40
         Top             =   390
         Width           =   900
      End
      Begin MSComCtl2.DTPicker DTInicio 
         Height          =   300
         Left            =   1395
         TabIndex        =   36
         Top             =   315
         Width           =   1305
         _ExtentX        =   2302
         _ExtentY        =   529
         _Version        =   393216
         Enabled         =   0   'False
         Format          =   273416193
         CurrentDate     =   40628
      End
      Begin ACTIVESKINLibCtl.SkinLabel skIni 
         Height          =   240
         Index           =   0
         Left            =   825
         OleObjectBlob   =   "Busca_Producto_Repuestos2.frx":0000
         TabIndex        =   37
         Top             =   345
         Width           =   795
      End
      Begin MSComCtl2.DTPicker DtHasta 
         Height          =   285
         Left            =   3555
         TabIndex        =   38
         Top             =   330
         Width           =   1305
         _ExtentX        =   2302
         _ExtentY        =   503
         _Version        =   393216
         Enabled         =   0   'False
         Format          =   273416193
         CurrentDate     =   40628
      End
      Begin ACTIVESKINLibCtl.SkinLabel skFin 
         Height          =   210
         Index           =   0
         Left            =   3075
         OleObjectBlob   =   "Busca_Producto_Repuestos2.frx":0068
         TabIndex        =   39
         Top             =   360
         Width           =   795
      End
      Begin MSComCtl2.DTPicker DtCrecacionInicio 
         Height          =   300
         Left            =   1395
         TabIndex        =   45
         Top             =   645
         Width           =   1305
         _ExtentX        =   2302
         _ExtentY        =   529
         _Version        =   393216
         Enabled         =   0   'False
         Format          =   273416193
         CurrentDate     =   40628
      End
      Begin ACTIVESKINLibCtl.SkinLabel skIni 
         Height          =   240
         Index           =   1
         Left            =   510
         OleObjectBlob   =   "Busca_Producto_Repuestos2.frx":00D0
         TabIndex        =   46
         Top             =   675
         Width           =   795
      End
      Begin MSComCtl2.DTPicker DtCreacionFin 
         Height          =   285
         Left            =   3555
         TabIndex        =   47
         Top             =   660
         Width           =   1305
         _ExtentX        =   2302
         _ExtentY        =   503
         _Version        =   393216
         Enabled         =   0   'False
         Format          =   273416193
         CurrentDate     =   40628
      End
      Begin ACTIVESKINLibCtl.SkinLabel skFin 
         Height          =   210
         Index           =   1
         Left            =   3075
         OleObjectBlob   =   "Busca_Producto_Repuestos2.frx":013E
         TabIndex        =   48
         Top             =   690
         Width           =   795
      End
   End
   Begin VB.Frame FrmFijaMinimo 
      Caption         =   "Fijar Stock Minimo"
      Height          =   660
      Left            =   420
      TabIndex        =   31
      Top             =   8220
      Visible         =   0   'False
      Width           =   2595
      Begin VB.CommandButton CmdFijarSM 
         Caption         =   "Ok"
         Height          =   270
         Left            =   1290
         TabIndex        =   33
         Top             =   255
         Width           =   735
      End
      Begin VB.TextBox TxtFijaStockMInimo 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   435
         MaxLength       =   3
         TabIndex        =   32
         Text            =   "0"
         Top             =   240
         Width           =   840
      End
   End
   Begin VB.CheckBox ChkStockMinimos 
      Caption         =   "Fijas stock Minimo"
      Height          =   285
      Left            =   14775
      TabIndex        =   30
      Top             =   45
      Width           =   2040
   End
   Begin VB.Frame FraProgreso 
      Caption         =   "Progreso exportaci�n"
      Height          =   795
      Left            =   2610
      TabIndex        =   25
      Top             =   4545
      Visible         =   0   'False
      Width           =   11430
      Begin Proyecto2.XP_ProgressBar BarraProgreso 
         Height          =   330
         Left            =   150
         TabIndex        =   26
         Top             =   315
         Width           =   11130
         _ExtentX        =   19632
         _ExtentY        =   582
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BrushStyle      =   0
         Color           =   16777088
         Scrolling       =   1
         ShowText        =   -1  'True
      End
   End
   Begin MSComctlLib.ListView LvDetalle 
      Height          =   4095
      Left            =   525
      TabIndex        =   1
      Top             =   3555
      Width           =   17925
      _ExtentX        =   31618
      _ExtentY        =   7223
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   0
      NumItems        =   12
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Object.Tag             =   "T1000"
         Text            =   "Codigo"
         Object.Width           =   1764
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Object.Tag             =   "T1500"
         Text            =   "Marca"
         Object.Width           =   1940
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Object.Tag             =   "T3000"
         Text            =   "Descripcion"
         Object.Width           =   10760
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   3
         Object.Tag             =   "N100"
         Text            =   "Precio Venta"
         Object.Width           =   1764
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Text            =   "Stock"
         Object.Width           =   1411
      EndProperty
      BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   5
         Text            =   "Proveedor"
         Object.Width           =   2117
      EndProperty
      BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   6
         Text            =   "Codigo Proveedor"
         Object.Width           =   2117
      EndProperty
      BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   7
         Object.Tag             =   "2000"
         Text            =   "Codigo Empresa"
         Object.Width           =   1940
      EndProperty
      BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   8
         Object.Tag             =   "N109"
         Text            =   "Stk Minimo"
         Object.Width           =   1587
      EndProperty
      BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   9
         Text            =   "Original"
         Object.Width           =   2293
      EndProperty
      BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   10
         Text            =   "Procedencia"
         Object.Width           =   1940
      EndProperty
      BeginProperty ColumnHeader(12) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   11
         Text            =   "Marca2"
         Object.Width           =   1940
      EndProperty
   End
   Begin VB.Frame Frame2 
      Caption         =   "Buscando Producto"
      Height          =   7890
      Left            =   315
      TabIndex        =   2
      Top             =   330
      Width           =   18285
      Begin ACTIVESKINLibCtl.SkinLabel SkResultados 
         Height          =   285
         Left            =   6765
         OleObjectBlob   =   "Busca_Producto_Repuestos2.frx":01A0
         TabIndex        =   34
         Top             =   7515
         Width           =   2055
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel5 
         Height          =   240
         Left            =   10125
         OleObjectBlob   =   "Busca_Producto_Repuestos2.frx":0222
         TabIndex        =   29
         Top             =   7500
         Width           =   1410
      End
      Begin VB.ComboBox CboCodigosProveedor 
         Height          =   315
         Left            =   11595
         Style           =   2  'Dropdown List
         TabIndex        =   28
         Top             =   7425
         Width           =   4890
      End
      Begin VB.CommandButton Command1 
         Caption         =   "Exportar Lista "
         Height          =   375
         Left            =   5175
         TabIndex        =   24
         Top             =   7410
         Width           =   1410
      End
      Begin VB.CommandButton CmdEditar 
         Caption         =   "Editar/Modificar"
         Height          =   375
         Left            =   3645
         TabIndex        =   11
         Top             =   7410
         Width           =   1515
      End
      Begin VB.CommandButton CmdCrear 
         Caption         =   "Crear Nuevo Producto"
         Height          =   375
         Left            =   1815
         TabIndex        =   9
         Top             =   7410
         Width           =   1815
      End
      Begin VB.CommandButton CmdSeleccionar 
         Caption         =   "&Seleccionar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   225
         TabIndex        =   8
         Top             =   7410
         Width           =   1575
      End
      Begin VB.TextBox TxtBusqueda 
         Height          =   375
         Left            =   135
         TabIndex        =   7
         Top             =   735
         Width           =   3300
      End
      Begin VB.CommandButton CmdTodos 
         Caption         =   "Todos"
         Height          =   390
         Left            =   3435
         TabIndex        =   6
         Top             =   750
         Width           =   1215
      End
      Begin VB.Frame Frame1 
         Caption         =   "Opciones de busqueda"
         Height          =   2865
         Left            =   6465
         TabIndex        =   3
         Top             =   180
         Width           =   9975
         Begin VB.OptionButton ChkOriginal 
            Caption         =   "Solo Codigo Original"
            Height          =   195
            Left            =   300
            TabIndex        =   49
            Top             =   1155
            Width           =   3465
         End
         Begin VB.TextBox txtNombreProveedor 
            BackColor       =   &H8000000F&
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   5820
            Locked          =   -1  'True
            TabIndex        =   44
            Top             =   1095
            Width           =   3930
         End
         Begin VB.CommandButton cmdRutProveedor 
            Caption         =   "Proveedor"
            Enabled         =   0   'False
            Height          =   270
            Left            =   3735
            TabIndex        =   43
            Top             =   1125
            Width           =   900
         End
         Begin VB.TextBox txtRutProveedor 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   4665
            TabIndex        =   42
            Top             =   1095
            Width           =   1125
         End
         Begin VB.OptionButton Option3 
            Caption         =   "Filtro por codigo Proveedor"
            Height          =   195
            Left            =   3765
            TabIndex        =   27
            Top             =   825
            Width           =   3240
         End
         Begin VB.Frame Frame3 
            Caption         =   "Aplicaciones"
            Height          =   1215
            Left            =   195
            TabIndex        =   15
            Top             =   1455
            Width           =   9165
            Begin VB.CheckBox Check1 
               Caption         =   "Busca por Marca, Modelo y A�o"
               Height          =   195
               Left            =   150
               TabIndex        =   20
               Top             =   345
               Width           =   3195
            End
            Begin VB.ComboBox CboModelos 
               Enabled         =   0   'False
               Height          =   315
               Left            =   2700
               Style           =   2  'Dropdown List
               TabIndex        =   19
               Top             =   780
               Width           =   3810
            End
            Begin VB.ComboBox CboMarcas 
               Enabled         =   0   'False
               Height          =   315
               Left            =   120
               Style           =   2  'Dropdown List
               TabIndex        =   18
               Top             =   780
               Width           =   2595
            End
            Begin VB.TextBox TxtAnoDesde 
               Enabled         =   0   'False
               Height          =   300
               Left            =   6525
               TabIndex        =   17
               Text            =   "0"
               Top             =   780
               Width           =   885
            End
            Begin VB.CommandButton CmdAgregaAplicacion 
               Caption         =   "Buscar"
               Enabled         =   0   'False
               Height          =   390
               Left            =   7440
               TabIndex        =   16
               Top             =   735
               Width           =   1515
            End
            Begin ACTIVESKINLibCtl.SkinLabel SkinLabel18 
               Height          =   240
               Index           =   0
               Left            =   6540
               OleObjectBlob   =   "Busca_Producto_Repuestos2.frx":02A2
               TabIndex        =   21
               Top             =   600
               Width           =   840
            End
            Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
               Height          =   195
               Left            =   120
               OleObjectBlob   =   "Busca_Producto_Repuestos2.frx":0306
               TabIndex        =   22
               Top             =   585
               Width           =   1740
            End
            Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
               Height          =   195
               Left            =   2715
               OleObjectBlob   =   "Busca_Producto_Repuestos2.frx":036E
               TabIndex        =   23
               Top             =   585
               Width           =   1740
            End
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
            Height          =   210
            Left            =   7545
            OleObjectBlob   =   "Busca_Producto_Repuestos2.frx":03D8
            TabIndex        =   14
            Top             =   420
            Width           =   1500
         End
         Begin VB.ComboBox CboLista 
            Height          =   315
            ItemData        =   "Busca_Producto_Repuestos2.frx":0454
            Left            =   7545
            List            =   "Busca_Producto_Repuestos2.frx":046A
            Style           =   2  'Dropdown List
            TabIndex        =   13
            Top             =   630
            Width           =   1590
         End
         Begin VB.OptionButton Option1 
            Caption         =   "Filtro por codigo Barra/int (empresa)"
            Height          =   195
            Left            =   285
            TabIndex        =   12
            Top             =   795
            Width           =   3465
         End
         Begin VB.OptionButton OptBusca 
            Caption         =   "Que la descripcion comience con el texto"
            Height          =   300
            Left            =   285
            TabIndex        =   5
            Top             =   375
            Width           =   3315
         End
         Begin VB.OptionButton Option2 
            Caption         =   "Que contenga el texto dentro de la descripcion"
            Height          =   270
            Left            =   3720
            TabIndex        =   4
            Top             =   390
            Value           =   -1  'True
            Width           =   3630
         End
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   375
         Left            =   120
         OleObjectBlob   =   "Busca_Producto_Repuestos2.frx":048F
         TabIndex        =   10
         Top             =   345
         Width           =   2655
      End
   End
   Begin VB.CommandButton CmdSalir 
      Cancel          =   -1  'True
      Caption         =   "&Retornar"
      Height          =   375
      Left            =   15675
      TabIndex        =   0
      Top             =   8805
      Width           =   1095
   End
   Begin VB.Timer Timer1 
      Interval        =   30
      Left            =   300
      Top             =   5580
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   195
      OleObjectBlob   =   "Busca_Producto_Repuestos2.frx":050C
      Top             =   6540
   End
End
Attribute VB_Name = "Busca_Producto_Repuestos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim RsProductos As Recordset




Private Sub CboMarcas_Validate(Cancel As Boolean)
       
            LLenarCombo CboModelos, "mob_nombre", "mob_id", "par_buscador_modelos", "mob_activo='SI' AND mab_id=" & CboMarcas.ItemData(CboMarcas.ListIndex), "mob_nombre"
            CboModelos.ListIndex = 0
End Sub

Private Sub Check1_Click()
    If Check1.Value = 1 Then
        CboMarcas.Enabled = True
        CboModelos.Enabled = True
        TxtAnoDesde.Enabled = True
        CmdAgregaAplicacion.Enabled = True
    Else
        CboMarcas.Enabled = False
        CboModelos.Enabled = False
        TxtAnoDesde.Enabled = False
        CmdAgregaAplicacion.Enabled = False
    End If
End Sub

Private Sub Check2_Click()
    If Check2.Value = 1 Then
        DTInicio.Enabled = True
        DtHasta.Enabled = True
        Me.DtCreacionFin.Enabled = True
        Me.DtCrecacionInicio.Enabled = True
        Me.CmdInvInmo.Enabled = True
        skIni(0).Enabled = True
        skFin(0).Enabled = True
    Else
        DTInicio.Enabled = False
        DtHasta.Enabled = False
        Me.CmdInvInmo.Enabled = False
        Me.DtCreacionFin.Enabled = False
        Me.DtCrecacionInicio.Enabled = False
        skIni(0).Enabled = False
        skFin(0).Enabled = False
    End If
End Sub

Private Sub ChkStockMinimos_Click()
    If ChkStockMinimos.Value = 1 Then
        FrmFijaMinimo.Visible = True
        CargaTodosConCodigosProveedor
    Else
        FrmFijaMinimo.Visible = False
    End If
End Sub
Private Sub CargaTodosConCodigosProveedor()
        Sp_Like = ""

        If OptBusca.Value Then
            Sp_Like = " AND descripcion LIKE '" & TxtBusqueda & "%' "
        End If
        If Option2.Value Then
            Sp_Like = " AND  descripcion LIKE '%" & TxtBusqueda & "%' "
        End If
        If Option1.Value Then
            Sp_Like = " AND pro_codigo_interno LIKE '" & TxtBusqueda & "%'"
        End If
        
        If Option3.Value Then
            '08-01-2016 _
            filtro por codigo proveedor
            Sp_Like = " AND (SELECT cpv_codigo_proveedor " & _
                        "FROM par_codigos_proveedor o " & _
                        "WHERE  o.pro_codigo = p.codigo AND o.rut_emp = '" & SP_Rut_Activo & "' " & _
                        "LIMIT 1) LIKE '" & TxtBusqueda & "%' "
        End If

      Sql = "SELECT codigo,mar_nombre,descripcion,precio_venta, " & _
                        "(SELECT sto_stock stock " & _
                        "FROM  pro_stock k " & _
                        "WHERE rut_emp='" & SP_Rut_Activo & "' AND k.pro_codigo=p.codigo AND bod_id= 1 " & _
                        "GROUP BY pro_codigo) stk, " & _
                    "(SELECT nombre_empresa " & _
                        "FROM par_codigos_proveedor o " & _
                        "JOIN maestro_proveedores s ON o.rut_proveedor=s.rut_proveedor " & _
                    "WHERE   o.pro_codigo = p.codigo AND o.rut_emp = '" & SP_Rut_Activo & "' " & _
                    "LIMIT 1) nombreproveedor, " & _
                    "(SELECT cpv_codigo_proveedor " & _
                        "FROM par_codigos_proveedor o " & _
                        "WHERE  o.pro_codigo = p.codigo AND o.rut_emp = '" & SP_Rut_Activo & "' " & _
                        "LIMIT 1) codproveedor,pro_codigo_interno,stock_critico  " & _
              "FROM maestro_productos p " & _
              "JOIN par_marcas m ON p.mar_id=m.mar_id  " & _
              "WHERE pro_activo='SI' AND p.rut_emp='" & SP_Rut_Activo & "'" & Sp_Like & Sp_Like2 & _
              " ORDER BY descripcion "

        CargaLista

End Sub



Private Sub CmdAgregaAplicacion_Click()
    Dim Sp_Anos As String
    If CboModelos.ListIndex = -1 Then
        MsgBox "Seleccione marca y modelo...", vbInformation
        CboModelos.SetFocus
        Exit Sub
    End If
 '   If Val(TxtAnoDesde) = 0 Then
 '       MsgBox "Ingrese a�o para la b�squeda...", vbInformation
 '       TxtAnoDesde.SetFocus
 '       Exit Sub
 '   End If
    Sp_Anos = ")"
    If Val(TxtAnoDesde) > 0 Then
        Sp_Anos = " AND (" & TxtAnoDesde & " BETWEEN bcm_desde AND IF(bcm_hasta=0,5000,bcm_hasta))) "
    End If
    Sql = "SELECT codigo,mar_nombre,descripcion,precio_venta, " & _
                        "(SELECT sto_stock stock " & _
                        "FROM  pro_stock k " & _
                        "WHERE rut_emp='" & SP_Rut_Activo & "' AND k.pro_codigo=p.codigo AND bod_id= 1 " & _
                        "GROUP BY pro_codigo) stk, " & _
                    "(SELECT nombre_empresa " & _
                        "FROM par_codigos_proveedor o " & _
                        "JOIN maestro_proveedores s ON o.rut_proveedor=s.rut_proveedor " & _
                    "WHERE   o.pro_codigo = p.codigo AND o.rut_emp = '" & SP_Rut_Activo & "' " & _
                    "LIMIT 1) nombreproveedor, " & _
                    "(SELECT cpv_codigo_proveedor " & _
                        "FROM par_codigos_proveedor o " & _
                        "WHERE  o.pro_codigo = p.codigo AND o.rut_emp = '" & SP_Rut_Activo & "' " & _
                        "LIMIT 1) codproveedor " & _
              "FROM maestro_productos p " & _
              "JOIN par_marcas m ON p.mar_id=m.mar_id  " & _
              "WHERE codigo IN (SELECT pro_codigo FROM inv_relaciona_codigo_marca_modelo WHERE " & _
                    "mob_id=" & CboModelos.ItemData(CboModelos.ListIndex) & Sp_Anos & " " & _
              " ORDER BY descripcion "

    CargaLista
End Sub

Private Sub CmdCrear_Click()
    SG_codigo = Empty
    AgregarProducto.Bm_Nuevo = True
    If SG_Es_la_Flor = "SI" Then
                AgregarProductoFlor.Show 1
    Else
        AgregarProducto.Show 1
    End If
    Sql = "SELECT codigo,marca,descripcion " & _
          "FROM maestro_productos " & _
          "WHERE  rut_emp='" & SP_Rut_Activo & "' " & _
          "ORDER BY descripcion"
    CargaLista
End Sub

Private Sub CmdEditar_Click()
            If LvDetalle.SelectedItem Is Nothing Then Exit Sub
            SG_codigo = LvDetalle.SelectedItem.Text
            AgregarProducto.Bm_Nuevo = False
            If SG_Es_la_Flor = "SI" Then
                AgregarProductoFlor.Show 1
            Else
                AgregarProducto.Show 1
            End If
         '   CmdSeleccionar_Click
End Sub

Private Sub CmdFiltra_Click()
    Sm_filtro = " AND pro_codigo_interno LIKE '%" & TxtBusca & "%'"
End Sub

Private Sub CmdFijarSM_Click()
    Dim Sp_CodMarcados As String
    Dim Sp_UltimoCodMarcado As String
    Dim ret As Long
    Sp_CodMarcados = ""
    If Val(TxtFijaStockMInimo) = 0 Then
        MsgBox "Stock Minimo debe ser mayor a cero...", vbInformation
        TxtFijaStockMInimo.SetFocus
        Exit Sub
    End If
    For L = 1 To LvDetalle.ListItems.Count
        If LvDetalle.ListItems(L).Checked Then
            Sp_CodMarcados = Sp_CodMarcados & LvDetalle.ListItems(L) & ","
            Sp_UltimoCodMarcado = LvDetalle.ListItems(L)
        End If
    Next
    If Len(Sp_CodMarcados) > 0 Then
        Sp_CodMarcados = Mid(Sp_CodMarcados, 1, Len(Sp_CodMarcados) - 1)
        cn.Execute "UPDATE maestro_productos SET stock_critico=" & TxtFijaStockMInimo & " " & _
                    "WHERE rut_emp='" & SP_Rut_Activo & "' AND codigo IN(" & Sp_CodMarcados & ")"
        CargaTodosConCodigosProveedor
    End If
    
    
   
     ret = Buscar(LvDetalle, Sp_UltimoCodMarcado, 0, True)
    
     If ret <> 0 Then
        LvDetalle.ListItems(ret).Selected = True
        LvDetalle.SetFocus
        'LvProductos_KeyDown 13, 1
    ' Else
     '   MsgBox "Dato no encontrado", vbExclamation
     End If
    
End Sub

Private Sub CmdInvInmo_Click()
    TxtBusqueda_Change
End Sub

Private Sub cmdRutProveedor_Click()
    AccionProveedor = 0
    BuscaProveedor.Show 1
    txtRutProveedor = RutBuscado

    txtRutProveedor_Validate True
    txtRutProveedor.SetFocus
End Sub

Private Sub cmdSalir_Click()
    SG_codigo = Empty
    Unload Me
End Sub

Private Sub CmdSeleccionar_Click()
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    SG_codigo = LvDetalle.SelectedItem.Text
        If Len(LvDetalle.SelectedItem.SubItems(7)) > 0 Then
        SG_codigo2 = LvDetalle.SelectedItem.SubItems(7)
    Else
        SG_codigo2 = SG_codigo
    End If
    Unload Me
End Sub

Private Sub CmdTodos_Click()
    Sql = "SELECT codigo,mar_nombre,descripcion,precio_venta " & _
          "FROM maestro_productos p " & _
             "JOIN par_marcas m ON p.mar_id=m.mar_id  " & _
          "WHERE pro_activo='SI' AND p.rut_emp='" & SP_Rut_Activo & "' " & _
          "ORDER BY descripcion"
    CargaLista
End Sub

Private Sub Command1_Click()
    Dim tit(2) As String
    If LvDetalle.ListItems.Count = 0 Then Exit Sub
    FraProgreso.Visible = True
    tit(0) = Me.Caption & " " & DtFecha & " - " & Time
    tit(1) = ""
    tit(2) = ""
    ExportarNuevo LvDetalle, tit, Me, BarraProgreso
    FraProgreso.Visible = False
End Sub

Private Sub Form_Load()
    Centrar Me
    Aplicar_skin Me, App.Path
    CboLista.ListIndex = 0
    LLenarCombo CboMarcas, "mab_nombre", "mab_id", "par_buscador_marcas", "mab_activo='SI'", "mab_nombre"
    CboMarcas.ListIndex = 0
    DTInicio = Date - 30
    DtHasta = Date
    LLenarCombo CboModelos, "mob_nombre", "mob_id", "par_buscador_modelos", "mob_activo='SI' AND mab_id=" & CboMarcas.ItemData(CboMarcas.ListIndex), "mob_nombre"
    CboModelos.ListIndex = 0
    
    If SP_Rut_Activo = "76.553.302-3" Then
    '    Me.Check1.Value = 1
        'SOLO KYR
        OptBusca.Value = True
        CboLista.ListIndex = CboLista.ListCount - 1
        LvDetalle.ColumnHeaders(8).Width = 0
        
    End If
        
    
    
    Sql = "SELECT codigo,mar_nombre,descripcion,precio_venta,'','','',pro_codigo_interno /*, " & _
      "(SELECT sto_stock stock " & _
                        "FROM  pro_stock k " & _
                        "WHERE rut_emp='" & SP_Rut_Activo & "' AND k.pro_codigo=p.codigo AND bod_id= 1 " & _
                        "GROUP BY pro_codigo) stk, " & _
                    "(SELECT nombre_empresa " & _
                        "FROM par_codigos_proveedor o " & _
                        "JOIN maestro_proveedores s ON o.rut_proveedor=s.rut_proveedor " & _
                    "WHERE   o.pro_codigo = p.codigo AND o.rut_emp = '" & SP_Rut_Activo & "' " & _
                    "LIMIT 1) nombreproveedor, " & _
                    "(SELECT cpv_codigo_proveedor " & _
                        "FROM par_codigos_proveedor o " & _
                        "WHERE  o.pro_codigo = p.codigo AND o.rut_emp = '" & SP_Rut_Activo & "' " & _
                        "LIMIT 1) codproveedor */ " & _
          "FROM maestro_productos p " & _
          "JOIN par_marcas m ON p.mar_id=m.mar_id  " & _
          "WHERE pro_activo='SI' AND p.rut_emp='" & SP_Rut_Activo & "' " & _
          "ORDER BY descripcion " & _
          "LIMIT 20"
    
    
    CargaLista
    
        'Solo para alcalde
    '21 Agosto 2015
    If SP_Rut_Activo = "76.169.962-8" Then
        OptBusca.Value = True
    End If
End Sub

Private Sub CargaLista()
    Consulta RsProductos, Sql
    If ChkStockMinimos.Value = 1 Then
        LLenar_Grilla RsProductos, Me, LvDetalle, True, True, True, False
    Else
        LLenar_Grilla RsProductos, Me, LvDetalle, False, True, True, False
    End If
    Me.SkResultados = LvDetalle.ListItems.Count & " Encontrados..."
End Sub



Private Sub LvDetalle_Click()
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    Me.CboCodigosProveedor.Clear
    Sql = "SELECT CONCAT(cpv_codigo_proveedor,' - ',nombre_empresa) cod_proveedor  " & _
                "FROM par_codigos_proveedor c " & _
                "JOIN maestro_proveedores m ON c.rut_proveedor=m.rut_proveedor " & _
                "WHERE pro_codigo='" & LvDetalle.SelectedItem & "' AND c.rut_emp='" & SP_Rut_Activo & "'"
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        RsTmp.MoveFirst
        Do While Not RsTmp.EOF
            Me.CboCodigosProveedor.AddItem RsTmp!cod_proveedor
        
            RsTmp.MoveNext
        Loop
        'LLenar_Grilla RsTmp, Me, LvProveedor, False, True, True, False
    End If
End Sub

Private Sub LvDetalle_DblClick()
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    CmdSeleccionar_Click
    'SG_codigo = LvDetalle.SelectedItem.Text
    'Unload Me
End Sub

Private Sub LvDetalle_KeyDown(KeyCode As Integer, Shift As Integer)
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    If KeyCode = 13 Then CmdSeleccionar_Click
        
End Sub
'ordena columnas
Private Sub LvDetalle_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    ordListView ColumnHeader, Me, LvDetalle
End Sub

Private Sub OptBusca_Click()
    TxtBusqueda_Change
            Me.cmdRutProveedor.Enabled = False
        txtRutProveedor.Enabled = False
   ' DeshabilitaMMA
End Sub

Private Sub DeshabilitaMMA()
    CboMarcas.Enabled = False
    CboModelos.Enabled = False
    TxtAnoDesde.Enabled = False
    CmdAgregaAplicacion.Enabled = False

End Sub

Private Sub Option1_Click()
    TxtBusqueda_Change
    Me.cmdRutProveedor.Enabled = False
    txtRutProveedor.Enabled = False
End Sub

Private Sub Option2_Click()
    TxtBusqueda_Change
    Me.cmdRutProveedor.Enabled = False
    txtRutProveedor.Enabled = False
   ' DeshabilitaMMA
End Sub

Private Sub Option3_Click()
    If Option3.Value Then
        Me.cmdRutProveedor.Enabled = True
        txtRutProveedor.Enabled = True
    End If
End Sub

Private Sub Timer1_Timer()
    TxtBusqueda.SetFocus
    Timer1.Enabled = False
End Sub

Private Sub TxtAnoDesde_GotFocus()
    En_Foco TxtAnoDesde
End Sub

Private Sub TxtAnoDesde_KeyPress(KeyAscii As Integer)
    KeyAscii = AceptaSoloNumeros(KeyAscii)
End Sub



Private Sub TxtBusqueda_Change()
    Dim Sp_Like As String, Sp_Like2 As String, Sp_Limit As String, Sp_LikeNomProv As String, Sp_LikeCodProv As String
    Sp_Like = ""
    Sp_Like2 = ""
    Sp_LikeNomProv = ""
    Sp_LikeCodProv = ""
    sp_likeinmo = ""
    
    If Me.ChkOriginal.Value = True Then
        Sp_Like = " AND pro_codigo_original LIKE '%" & TxtBusqueda & "%'"
        
        Sp_LikeNomProv = "0"
        Sp_LikeCodProv = "0"
        GoTo soloOriginal
        
    End If
    
    
    If Check2.Value = 1 Then
        'sp_likeinmo = " AND (SELECT kar_fecha FROM inv_kardex i WHERE i.pro_codigo=p.codigo AND i.rut_emp='" & SP_Rut_Activo & "' AND kar_descripcion='KARDEX INICIAL' LIMIT 1)<'" & Fql(DTInicio) & "' "
        sp_likeinmo = " AND (SELECT kar_fecha FROM inv_kardex i WHERE i.pro_codigo=p.codigo AND i.rut_emp='" & SP_Rut_Activo & "' AND kar_descripcion='KARDEX INICIAL' LIMIT 1)  BETWEEN '" & Fql(Me.DtCrecacionInicio) & " ' AND '" & Fql(Me.DtCreacionFin) & "' "
        
        
        sm_FiltroFecha = " AND (r.fecha BETWEEN '" & Format(DTInicio.Value, "YYYY-MM-DD") & "' AND '" & Format(DtHasta.Value, "YYYY-MM-DD") & "') "
         sp_likeinmo = sp_likeinmo & " AND codigo NOT IN(SELECT v.codigo " & _
               "FROM ven_detalle v " & _
               "INNER JOIN maestro_productos p USING(codigo) " & _
               "INNER JOIN sis_documentos d USING(doc_id) " & _
               "INNER JOIN ven_doc_venta r USING(doc_id,no_documento) " & _
               "WHERE v.codigo<>0 AND doc_nota_de_venta='NO' AND d.doc_orden_de_compra='NO' AND v.rut_emp='" & SP_Rut_Activo & "' " & Sm_FiltroCodigo & sm_FiltroFecha & Sm_FiltroMarca & Sm_FiltroTipo & " " & _
               "GROUP BY v.codigo)"
    Else
        sm_FiltroFecha = ""
    
    End If
    '31-10-2015
    'HAREMOS LA QUERY DE BUSQUEDA MAS RAPIDA LIMITANDO LA CANTIDAD DE RESULTADOS
    If CboLista.Text = "TODOS" Then
        Sp_Limit = ""
    Else
        Sp_Limit = " LIMIT " & CboLista.Text
    End If
    
    
    If Len(Me.TxtBusqueda.Text) = 0 And Me.Check2.Value = 0 Then
        'Me.AdoProducto.Recordset.Filter = 0
    Else
    
        If Len(TxtBusqueda) > 0 Then
            If OptBusca.Value Then
                Sp_Like = " AND descripcion LIKE '" & TxtBusqueda & "%' "
            End If
            If Option2.Value Then
                Sp_Like = " AND  descripcion LIKE '%" & TxtBusqueda & "%' "
            End If
            If Option1.Value Then
                Sp_Like = " AND pro_codigo_interno LIKE '" & TxtBusqueda & "%'"
            End If
        End If
        
      
        
        
        
        Sp_LikeNomProv = "(SELECT nombre_empresa " & _
                        "FROM par_codigos_proveedor o " & _
                        "JOIN maestro_proveedores s ON o.rut_proveedor=s.rut_proveedor " & _
                    "WHERE o.pro_codigo = p.codigo AND o.rut_emp = '" & SP_Rut_Activo & "' " & _
                    "LIMIT 1) nombreproveedor"
        Sp_LikeCodProv = "(SELECT cpv_codigo_proveedor " & _
                        "FROM par_codigos_proveedor o " & _
                        "WHERE o.pro_codigo = p.codigo AND o.rut_emp = '" & SP_Rut_Activo & "' " & _
                        "LIMIT 1) codproveedor"
        
        
        If Option3.Value Then
            '08-01-2016 _
            filtro por codigo proveedor
            Sp_RutP = ""
            If Len(txtRutProveedor) > 0 Then
                Sp_RutP = "AND o.rut_proveedor='" & txtRutProveedor & "' "
            End If
            
            
            Sp_Like = " AND (SELECT cpv_codigo_proveedor " & _
                        "FROM par_codigos_proveedor o " & _
                        "WHERE  cpv_codigo_proveedor LIKE '%" & TxtBusqueda & "%' AND o.pro_codigo = p.codigo AND o.rut_emp = '" & SP_Rut_Activo & "' " & Sp_RutP & _
                        "LIMIT 1) LIKE '" & TxtBusqueda & "%' "
            Sp_LikeNomProv = "(SELECT nombre_empresa " & _
                        "FROM par_codigos_proveedor o " & _
                        "JOIN maestro_proveedores s ON o.rut_proveedor=s.rut_proveedor " & _
                    "WHERE cpv_codigo_proveedor LIKE '%" & TxtBusqueda & "%' AND  o.pro_codigo = p.codigo AND o.rut_emp = '" & SP_Rut_Activo & "' " & Sp_RutP & _
                    "LIMIT 1) nombreproveedor"
            Sp_LikeCodProv = "(SELECT cpv_codigo_proveedor " & _
                        "FROM par_codigos_proveedor o " & _
                        "WHERE cpv_codigo_proveedor LIKE '%" & TxtBusqueda & "%' AND o.pro_codigo = p.codigo AND o.rut_emp = '" & SP_Rut_Activo & "' " & Sp_RutP & _
                        "LIMIT 1) codproveedor"
                        
        End If
        
        
        If Check1.Value = 1 Then
            
            Sp_Like2 = " AND codigo IN (SELECT pro_codigo FROM inv_relaciona_codigo_marca_modelo WHERE " & _
                    "mob_id=" & CboModelos.ItemData(CboModelos.ListIndex)
            If Val(TxtAnoDesde) = 0 Then
                Sp_Like2 = Sp_Like2 & ")" 'no tomatoms en cuenta el a�o para la busqueda
            Else
                Sp_Like2 = Sp_Like2 & " AND (" & TxtAnoDesde & " BETWEEN bcm_desde AND bcm_hasta)) "
            End If
        End If
        
soloOriginal:
        
        Sql = "SELECT codigo,mar_nombre,descripcion,precio_venta, " & _
                        "(SELECT sto_stock stock " & _
                        "FROM  pro_stock k " & _
                        "WHERE rut_emp='" & SP_Rut_Activo & "' AND k.pro_codigo=p.codigo AND bod_id= 1 " & _
                        "GROUP BY pro_codigo) stk, " & _
                     Sp_LikeNomProv & "," & Sp_LikeCodProv & _
                    ",pro_codigo_interno,stock_critico,pro_codigo_original,pcd_nombre,ma2_nombre " & _
              "FROM maestro_productos p " & _
              "JOIN par_marcas m ON p.mar_id=m.mar_id  " & _
              "LEFT JOIN par_procedencias s ON p.pcd_id=s.pcd_id " & _
              "LEFT JOIN par_marcas2 m2 ON m2.ma2_id=p.ma2_id " & _
              "WHERE pro_activo='SI' AND p.rut_emp='" & SP_Rut_Activo & "'" & Sp_Like & Sp_Like2 & sp_likeinmo & " " & _
              " ORDER BY descripcion " & Sp_Limit

        CargaLista
    End If
End Sub

Private Sub TxtBusqueda_GotFocus()
    En_Foco TxtBusqueda
End Sub
Private Sub CargaListilla()
     If Option3.Value Then
            '08-01-2016 _
            filtro por codigo proveedor
            Sp_RutP = ""
            If Len(txtRutProveedor) > 0 Then
                Sp_RutP = "AND o.rut_proveedor='" & txtRutProveedor & "' "
            End If
            
            
            Sp_Like = " AND (SELECT cpv_codigo_proveedor " & _
                        "FROM par_codigos_proveedor o " & _
                        "WHERE  cpv_codigo_proveedor LIKE '%" & TxtBusqueda & "%' AND o.pro_codigo = p.codigo AND o.rut_emp = '" & SP_Rut_Activo & "' " & Sp_RutP & _
                        "LIMIT 1) LIKE '" & TxtBusqueda & "%' "
            Sp_LikeNomProv = "(SELECT nombre_empresa " & _
                        "FROM par_codigos_proveedor o " & _
                        "JOIN maestro_proveedores s ON o.rut_proveedor=s.rut_proveedor " & _
                    "WHERE cpv_codigo_proveedor LIKE '%" & TxtBusqueda & "%' AND  o.pro_codigo = p.codigo AND o.rut_emp = '" & SP_Rut_Activo & "' " & Sp_RutP & _
                    "LIMIT 1) nombreproveedor"
            Sp_LikeCodProv = "(SELECT cpv_codigo_proveedor " & _
                        "FROM par_codigos_proveedor o " & _
                        "WHERE cpv_codigo_proveedor LIKE '%" & TxtBusqueda & "%' AND o.pro_codigo = p.codigo AND o.rut_emp = '" & SP_Rut_Activo & "' " & Sp_RutP & _
                        "LIMIT 1) codproveedor"
                        
        End If
        
          Sql = "SELECT codigo,mar_nombre,descripcion,precio_venta, " & _
                        "(SELECT sto_stock stock " & _
                        "FROM  pro_stock k " & _
                        "WHERE rut_emp='" & SP_Rut_Activo & "' AND k.pro_codigo=p.codigo AND bod_id= 1 " & _
                        "GROUP BY pro_codigo) stk, " & _
                     Sp_LikeNomProv & "," & Sp_LikeCodProv & _
                    ",pro_codigo_interno,stock_critico " & _
              "FROM maestro_productos p " & _
              "JOIN par_marcas m ON p.mar_id=m.mar_id  " & _
              "WHERE pro_activo='SI' AND p.rut_emp='" & SP_Rut_Activo & "'" & Sp_Like & Sp_Like2 & sp_likeinmo & " " & _
              " ORDER BY descripcion " & Sp_Limit

        CargaLista
End Sub


Private Sub TxtBusqueda_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 13 Then LvDetalle.SetFocus
End Sub

Private Sub TxtBusqueda_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii))) 'Transformo a mayuscula el caracter ingresado
     If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtFijaStockMInimo_GotFocus()
    En_Foco TxtFijaStockMInimo
End Sub

Private Sub TxtFijaStockMInimo_KeyPress(KeyAscii As Integer)
    KeyAscii = AceptaSoloNumeros(KeyAscii)
End Sub

Private Sub TxtRutProveedor_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then CargaListilla
End Sub

Private Sub txtRutProveedor_Validate(Cancel As Boolean)
    If Len(txtRutProveedor) = 0 Then
        txtNombreProveedor = ""
        Exit Sub
    End If
    Sql = "SELECT nombre_empresa " & _
        "FROM maestro_proveedores " & _
        "WHERE rut_proveedor='" & txtRutProveedor & "'"
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        txtNombreProveedor = RsTmp!nombre_empresa
    End If
End Sub
Function Buscar(LV As ListView, _
                Cadena As String, _
                nCol As Integer, _
                bFraseCompleta As Boolean) As Long
   
   
    Dim i As Long
    Dim iStart As Long
    Dim oItem As ListItem
    
    
    'If Lv.SelectedItem Is Nothing Then
    '   i = 1
    If Not LV.SelectedItem Is Nothing Then
       iStart = LV.SelectedItem.Index + 1
    Else
       iStart = 1
    End If
    
    With LV
    For i = 1 To LV.ListItems.Count
        
        Set oItem = LV.ListItems(i)
        
        Dim sItem As String
        
        If nCol = 0 Then
            sItem = LV.ListItems(i)
        Else
            sItem = oItem.SubItems(nCol)
        End If
        If bFraseCompleta = False Then
           Dim nPos As Integer
           
           nPos = InStr(LCase(sItem), LCase(Cadena))
           If nPos <> 0 Then
              Buscar = oItem.Index
              oItem.EnsureVisible
              Exit For
           End If
        ElseIf bFraseCompleta = True Then
             If LCase(sItem) = LCase(Cadena) Then
              Buscar = oItem.Index
              oItem.EnsureVisible
              Exit For
             End If
        End If
    Next
    End With
    
    Set LV.SelectedItem = Nothing
    
End Function

