VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Begin VB.Form Sis_NecesarioActualizar 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Actualizacion"
   ClientHeight    =   2430
   ClientLeft      =   1905
   ClientTop       =   3285
   ClientWidth     =   8685
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2430
   ScaleWidth      =   8685
   Begin VB.Frame Frame1 
      Caption         =   "Informacion"
      Height          =   1650
      Left            =   810
      TabIndex        =   0
      Top             =   585
      Width           =   7170
      Begin VB.CommandButton CmdDescarga 
         Caption         =   "Presione este boton para descargar"
         Height          =   465
         Left            =   615
         MouseIcon       =   "Sis_NecesarioActualizar.frx":0000
         MousePointer    =   99  'Custom
         TabIndex        =   2
         Top             =   960
         Width           =   5865
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   495
         Left            =   615
         OleObjectBlob   =   "Sis_NecesarioActualizar.frx":0442
         TabIndex        =   1
         Top             =   570
         Width           =   6315
      End
   End
   Begin VB.Timer Timer1 
      Interval        =   10
      Left            =   0
      Top             =   525
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   0
      OleObjectBlob   =   "Sis_NecesarioActualizar.frx":0502
      Top             =   0
   End
End
Attribute VB_Name = "Sis_NecesarioActualizar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Declare Function ShellExecute Lib "shell32.dll" Alias "ShellExecuteA" (ByVal hWnd As Long, ByVal lpOperation As String, ByVal lpFile As String, ByVal lpParameters As String, ByVal lpDirectory As String, ByVal nShowCmd As Long) As Long
Const SW_NORMAL = 1

Private Sub CmdDescarga_Click()
    X = ShellExecute(Me.hWnd, "Open", Sp_RutaDescarga, &O0, &O0, SW_NORMAL)
    End
End Sub

Private Sub Form_Load()
    Centrar Me
    Aplicar_skin Me
End Sub

Private Sub Timer1_Timer()
    On Error Resume Next
    Me.SetFocus
    Timer1.Enabled = False
End Sub
