VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Begin VB.Form sis_InputBox 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Requiere autorización"
   ClientHeight    =   1875
   ClientLeft      =   7065
   ClientTop       =   2685
   ClientWidth     =   8310
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1875
   ScaleWidth      =   8310
   ShowInTaskbar   =   0   'False
   Begin VB.Frame FramBox 
      Caption         =   "Frame1"
      Height          =   1500
      Left            =   315
      TabIndex        =   0
      Top             =   240
      Width           =   7725
      Begin VB.TextBox texto 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   405
         IMEMode         =   3  'DISABLE
         Left            =   765
         PasswordChar    =   "*"
         TabIndex        =   3
         Top             =   450
         Width           =   6435
      End
      Begin VB.CommandButton CmdCancelar 
         Cancel          =   -1  'True
         Caption         =   "&Cancelar"
         Height          =   300
         Left            =   6480
         TabIndex        =   2
         Top             =   1080
         Width           =   1080
      End
      Begin VB.CommandButton CmdOk 
         Caption         =   "&Aceptar"
         Height          =   300
         Left            =   5280
         TabIndex        =   1
         Top             =   1095
         Width           =   1080
      End
   End
   Begin VB.Timer Timer1 
      Interval        =   10
      Left            =   30
      Top             =   1605
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   0
      OleObjectBlob   =   "sis_InputBox.frx":0000
      Top             =   1065
   End
End
Attribute VB_Name = "sis_InputBox"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public Sm_TipoDato As String, SM_Texto As String
Private Sub CmdCancelar_Click()
    SG_codigo2 = Empty
    Unload Me
End Sub

Private Sub CmdCancelar_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then CmdOk_Click
End Sub

Private Sub CmdOk_Click()
    If Len(texto) = 0 Then
        MsgBox "No ha ingresado datos...", vbInformation
        texto.SetFocus
        Exit Sub
    End If
    SG_codigo2 = texto
    Me.Sm_TipoDato = ""
    Unload Me
End Sub

Private Sub Form_Load()
    Aplicar_skin Me
    Centrar Me
    If Len(SM_Texto) > 0 Then
        texto = SM_Texto
    End If
End Sub


Private Sub texto_GotFocus()
    On Error Resume Next
    En_Foco texto
End Sub

Private Sub texto_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then CmdOk_Click
    If Sm_TipoDato = "N" Then KeyAscii = SoloNumeros(KeyAscii)
    If KeyAscii = 39 Then KeyAscii = 0
End Sub


Private Sub Timer1_Timer()
    On Error Resume Next
    texto.SetFocus
    Timer1.Enabled = False
    If Len(SM_Texto) > 0 Then
        texto = SM_Texto
    End If
End Sub
