VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{DA729E34-689F-49EA-A856-B57046630B73}#1.0#0"; "progressbar-xp.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form Sis_BarraProgreso 
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   3735
   ClientLeft      =   6945
   ClientTop       =   3525
   ClientWidth     =   7860
   DrawStyle       =   6  'Inside Solid
   LinkTopic       =   "Form1"
   ScaleHeight     =   3735
   ScaleWidth      =   7860
   ShowInTaskbar   =   0   'False
   Begin VB.Timer Timer1 
      Interval        =   250
      Left            =   3720
      Top             =   1680
   End
   Begin VB.Frame Frame1 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      Caption         =   "Cargando... Espere ... "
      ForeColor       =   &H80000008&
      Height          =   1575
      Left            =   105
      MouseIcon       =   "Sis_BarraProgreso.frx":0000
      TabIndex        =   1
      Top             =   135
      Width           =   7725
      Begin Proyecto2.XP_ProgressBar ProgBar 
         Height          =   1275
         Left            =   135
         TabIndex        =   2
         Top             =   225
         Width           =   7305
         _ExtentX        =   12885
         _ExtentY        =   2249
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BrushStyle      =   0
         Color           =   8421376
         Image           =   "Sis_BarraProgreso.frx":0442
         Orientation     =   1
         Scrolling       =   6
      End
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   0
      OleObjectBlob   =   "Sis_BarraProgreso.frx":0894
      Top             =   0
   End
   Begin VB.Timer TimEspera 
      Enabled         =   0   'False
      Interval        =   10
      Left            =   120
      Top             =   1560
   End
   Begin MSComctlLib.ProgressBar ProgressBar1 
      Height          =   495
      Left            =   2445
      TabIndex        =   3
      Top             =   540
      Width           =   3855
      _ExtentX        =   6800
      _ExtentY        =   873
      _Version        =   393216
      Appearance      =   1
      Scrolling       =   1
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Iniciar"
      Enabled         =   0   'False
      Height          =   375
      Left            =   3600
      TabIndex        =   0
      Top             =   540
      Width           =   1335
   End
End
Attribute VB_Name = "Sis_BarraProgreso"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim Im_C As Integer
Dim Bm_eje As Boolean
Dim Bm_Listo As Boolean
Private Sub Command1_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    Aplicar_skin Me
    Centrar Me
    Im_C = 0
    Bm_Listo = False
    Bm_eje = False
    Me.TimEspera.Enabled = True
    ProgBar.Color = QBColor(Aleatorio(0, 15))
    
    
    
  '  Proceso
End Sub

  
Private Function Aleatorio(Minimo As Long, Maximo As Long) As Long
    Randomize
    Aleatorio = CLng((Minimo - Maximo) * Rnd + Maximo)
    
End Function

Private Sub Proceso()
     
    DoEvents
    ConsultaDO RsTmp, Sql
    Bm_Listo = True

End Sub

Private Sub Timer1_Timer()

Proceso
Timer1.Enabled = False
End Sub

Private Sub TimEspera_Timer()
        If Im_C > 99 Then Im_C = 1
        ProgBar.Value = Im_C
        Me.ProgressBar1.Value = Im_C
        Im_C = Im_C + 5
        If Bm_eje And Im_C > 30 Then
           ' Proceso
           Timer1.Enabled = True
            Bm_eje = False
        End If
        
        If Bm_Listo Then Unload Me
End Sub
