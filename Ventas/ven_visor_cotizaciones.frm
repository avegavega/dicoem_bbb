VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form ven_visor_cotizaciones 
   Caption         =   "Visor de Cotizaciones"
   ClientHeight    =   8220
   ClientLeft      =   2100
   ClientTop       =   1500
   ClientWidth     =   13335
   LinkTopic       =   "Form1"
   ScaleHeight     =   8220
   ScaleWidth      =   13335
   Begin VB.Frame Frame1 
      Caption         =   "Cotizaciones"
      Height          =   6225
      Left            =   255
      TabIndex        =   10
      Top             =   1380
      Width           =   12960
      Begin VB.CommandButton CmdEliminar 
         Caption         =   "Eliminar"
         Height          =   435
         Left            =   5655
         TabIndex        =   15
         Top             =   5580
         Width           =   2025
      End
      Begin VB.CommandButton CmdReimprime 
         Caption         =   "Re Imprimir"
         Height          =   435
         Left            =   3390
         TabIndex        =   14
         Top             =   5580
         Width           =   2025
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   225
         Left            =   240
         OleObjectBlob   =   "ven_visor_cotizaciones.frx":0000
         TabIndex        =   12
         Top             =   5550
         Width           =   3315
      End
      Begin MSComctlLib.ListView LvDetalle 
         Height          =   5085
         Left            =   150
         TabIndex        =   11
         Top             =   390
         Width           =   12540
         _ExtentX        =   22119
         _ExtentY        =   8969
         View            =   3
         LabelWrap       =   0   'False
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   13
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "N109"
            Text            =   "Id Unico"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "N109"
            Text            =   "Nro"
            Object.Width           =   1587
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "F1000"
            Text            =   "Fecha"
            Object.Width           =   1852
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Object.Tag             =   "T1000"
            Text            =   "Usuario"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Object.Tag             =   "T2500"
            Text            =   "Tipo Documento"
            Object.Width           =   3528
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   5
            Object.Tag             =   "N109"
            Text            =   "Nro Doc"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   6
            Object.Tag             =   "T1500"
            Text            =   "Hora"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   7
            Object.Tag             =   "N100"
            Text            =   "Total"
            Object.Width           =   1940
         EndProperty
         BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   8
            Object.Tag             =   "T1000"
            Text            =   "Rut Cliente"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   9
            Object.Tag             =   "T1500"
            Text            =   "Nombre Cliente"
            Object.Width           =   2646
         EndProperty
         BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   10
            Object.Tag             =   "T2000"
            Text            =   "Documento"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(12) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   11
            Object.Tag             =   "N109"
            Text            =   "Numero"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(13) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   12
            Object.Tag             =   "N109"
            Text            =   "Id de venta"
            Object.Width           =   2540
         EndProperty
      End
   End
   Begin VB.Timer Timer1 
      Interval        =   50
      Left            =   45
      Top             =   6810
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   705
      OleObjectBlob   =   "ven_visor_cotizaciones.frx":00A2
      Top             =   6000
   End
   Begin VB.CommandButton cmdSalir 
      Cancel          =   -1  'True
      Caption         =   "Retornar"
      Height          =   390
      Left            =   11820
      TabIndex        =   9
      Top             =   7740
      Width           =   1440
   End
   Begin VB.Frame Frame4 
      Caption         =   "Mes y A�o"
      Height          =   1170
      Left            =   225
      TabIndex        =   0
      Top             =   120
      Width           =   12930
      Begin VB.CheckBox ChkTerminadas 
         Caption         =   "Ver NV terminadas"
         Height          =   255
         Left            =   10485
         TabIndex        =   13
         Top             =   570
         Width           =   2175
      End
      Begin VB.CheckBox ChkFecha 
         Height          =   225
         Left            =   3210
         TabIndex        =   4
         Top             =   480
         Width           =   225
      End
      Begin VB.ComboBox ComAno 
         Height          =   315
         Left            =   1710
         Style           =   2  'Dropdown List
         TabIndex        =   3
         Top             =   435
         Width           =   1215
      End
      Begin VB.ComboBox comMes 
         Height          =   315
         ItemData        =   "ven_visor_cotizaciones.frx":02D6
         Left            =   135
         List            =   "ven_visor_cotizaciones.frx":02D8
         Style           =   2  'Dropdown List
         TabIndex        =   2
         Top             =   450
         Width           =   1575
      End
      Begin VB.CommandButton CmdBuscar 
         Caption         =   "Buscar"
         Height          =   435
         Left            =   6945
         TabIndex        =   1
         Top             =   435
         Width           =   2310
      End
      Begin MSComCtl2.DTPicker DtDesde 
         CausesValidation=   0   'False
         Height          =   285
         Left            =   3480
         TabIndex        =   5
         Top             =   450
         Width           =   1290
         _ExtentX        =   2275
         _ExtentY        =   503
         _Version        =   393216
         Enabled         =   0   'False
         Format          =   91684865
         CurrentDate     =   41001
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
         Height          =   255
         Left            =   120
         OleObjectBlob   =   "ven_visor_cotizaciones.frx":02DA
         TabIndex        =   6
         Top             =   255
         Width           =   375
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel5 
         Height          =   255
         Left            =   1725
         OleObjectBlob   =   "ven_visor_cotizaciones.frx":033E
         TabIndex        =   7
         Top             =   255
         Width           =   375
      End
      Begin MSComCtl2.DTPicker DtHasta 
         Height          =   285
         Left            =   4755
         TabIndex        =   8
         Top             =   450
         Width           =   1275
         _ExtentX        =   2249
         _ExtentY        =   503
         _Version        =   393216
         Enabled         =   0   'False
         Format          =   91684865
         CurrentDate     =   41001
      End
   End
End
Attribute VB_Name = "ven_visor_cotizaciones"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim Sp_Condicion As String

Private Sub ChkFecha_Click()
    On Error Resume Next
    If ChkFecha.Value = 1 Then
        comMes.Enabled = False
        ComAno.Enabled = False
        DtDesde.Enabled = True
        DtHasta.Enabled = True
        DtDesde.SetFocus
    Else
        comMes.SetFocus
        DtDesde.Enabled = False
        DtHasta.Enabled = False
        comMes.Enabled = True
        ComAno.Enabled = True
    End If
End Sub

Private Sub CmdBuscar_Click()
    LvDetalle.ListItems.Clear
   
    If ChkFecha.Value = 1 Then
        Sp_Condicion = " AND c.nve_fecha BETWEEN '" & Fql(DtDesde.Value) & "'  AND '" & Fql(DtHasta.Value) & " 23:59' "
    Else
        Sp_Condicion = " AND MONTH(c.nve_fecha)=" & comMes.ItemData(comMes.ListIndex) & " AND YEAR(c.nve_fecha)=" & IIf(ComAno.ListCount = 0, Year(Date), ComAno.Text) & " "
    End If
    CargaCambios
End Sub

Private Sub CmdEliminar_Click()
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    
    If LvDetalle.SelectedItem.SubItems(12) > 0 Then
        MsgBox "Nota de venta corresponde a una venta, no es posible eliminarla...", vbExclamation
        Exit Sub
    End If
    If MsgBox(LogUsuario & " ... �Confirma eliminacion de Nota de venta Nro:" & LvDetalle.SelectedItem & "?", vbQuestion + vbOKCancel) = vbOK Then
        Sql = "UPDATE ven_nota_venta " & _
                "SET nve_anulada='SI',nve_anulada_usuario='" & LogUsuario & "' " & _
                "WHERE nve_id=" & LvDetalle.SelectedItem
        cn.Execute Sql
        CargaCambios
    
    End If
End Sub

Private Sub CmdReimprime_Click()
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    SG_codigo = LvDetalle.SelectedItem.SubItems(5)
    SG_codigo2 = LvDetalle.SelectedItem
    Unload Me
End Sub

Private Sub CmdSalir_Click()
    SG_codigo = ""
    Unload Me
End Sub

Private Sub Form_Load()
    SG_codigo = ""
    Centrar Me
    Aplicar_skin Me
    For i = 1 To 12
        comMes.AddItem UCase(MonthName(i, False))
        comMes.ItemData(comMes.ListCount - 1) = i
    Next
    Busca_Id_Combo comMes, Val(Month(Date))
    LLenaYears ComAno, 2010
    ComAno.ListIndex = 0
    DtDesde = Date
    DtHasta = Date
    CmdBuscar_Click
End Sub


Private Sub CargaCambios()
    If Me.ChkTerminadas.Value = 0 Then
        Sql = "SELECT nve_id id, @rownum:=@rownum+1 AS rownum,nve_fecha,nve_login,'COTIZACION',nve_id,DATE_FORMAT(nve_hora,'%h:%i %p') hora,nve_total,rut_cliente,(SELECT nombre_rsocial FROM maestro_clientes m WHERE m.rut_cliente=c.rut_cliente LIMIT 1),'',0,id " & _
                    "FROM (SELECT @rownum:=0) r,ven_nota_venta c " & _
                    "WHERE nve_anulada='NO' AND nve_cotizacion='SI' " & Sp_Condicion & _
                    "ORDER BY @rownum :=@rownum + 1"
    Else
        Sql = "SELECT nve_id id, @rownum:=@rownum+1 AS rownum,nve_fecha,nve_login,'COTIZACION',nve_id,DATE_FORMAT(nve_hora,'%h:%i %p') hora,nve_total,c.rut_cliente,(SELECT nombre_rsocial FROM maestro_clientes m WHERE m.rut_cliente=c.rut_cliente LIMIT 1)," & _
                        "(SELECT doc_nombre FROM ven_doc_venta v  JOIN sis_documentos d ON v.doc_id=d.doc_id WHERE v.id=c.id LIMIT 1) documento, " & _
                        "(SELECT no_documento FROM ven_doc_venta v WHERE v.id=c.id LIMIT 1) numero,id " & _
                    "FROM (SELECT @rownum:=0) r,ven_nota_venta c " & _
                    "WHERE nve_anulada='NO' AND nve_terminada='SI' " & Sp_Condicion & _
                    "ORDER BY @rownum :=@rownum + 1"
        
    
    End If
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvDetalle, False, True, True, False
End Sub

Private Sub LvDetalle_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    ordListView ColumnHeader, Me, LvDetalle
End Sub

Private Sub LvDetalle_DblClick()
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    SG_codigo = LvDetalle.SelectedItem.SubItems(5)
    Unload Me
End Sub
