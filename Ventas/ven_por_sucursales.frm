VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form ven_por_sucursales 
   Caption         =   "Ventas Sucursales"
   ClientHeight    =   8940
   ClientLeft      =   1590
   ClientTop       =   2295
   ClientWidth     =   16545
   LinkTopic       =   "Form1"
   ScaleHeight     =   8940
   ScaleWidth      =   16545
   Begin ACTIVESKINLibCtl.SkinLabel LbTelefono 
      Height          =   255
      Left            =   5685
      OleObjectBlob   =   "ven_por_sucursales.frx":0000
      TabIndex        =   41
      Top             =   30
      Width           =   1725
   End
   Begin VB.Frame Frame4 
      Caption         =   "Cantidad de productos por sucursal"
      Height          =   5910
      Left            =   180
      TabIndex        =   35
      Top             =   2820
      Width           =   16260
      Begin VB.TextBox TxtCantFacturas 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   1365
         TabIndex        =   45
         Text            =   "0"
         Top             =   5145
         Width           =   1140
      End
      Begin VB.TextBox TxtTotal 
         Alignment       =   1  'Right Justify
         Height          =   285
         Index           =   0
         Left            =   6015
         TabIndex        =   44
         Text            =   "0"
         Top             =   5205
         Width           =   1800
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel6 
         Height          =   240
         Left            =   2655
         OleObjectBlob   =   "ven_por_sucursales.frx":007A
         TabIndex        =   43
         Top             =   5265
         Width           =   3270
      End
      Begin VB.CommandButton CmdOk 
         Caption         =   "Ok"
         Height          =   330
         Left            =   15030
         TabIndex        =   42
         Top             =   315
         Width           =   1035
      End
      Begin VB.TextBox TxtCant 
         Alignment       =   1  'Right Justify
         Height          =   285
         Index           =   0
         Left            =   6060
         TabIndex        =   38
         Text            =   "0"
         Top             =   375
         Width           =   1800
      End
      Begin VB.TextBox TxtPrecio 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   4770
         TabIndex        =   37
         Text            =   "0"
         Top             =   375
         Width           =   1275
      End
      Begin VB.TextBox TxtDescrp 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1260
         Locked          =   -1  'True
         TabIndex        =   39
         TabStop         =   0   'False
         Top             =   360
         Width           =   3495
      End
      Begin VB.TextBox TxtCodigo 
         Height          =   285
         Left            =   150
         TabIndex        =   36
         Top             =   360
         Width           =   1110
      End
      Begin MSComctlLib.ListView LvDetalle 
         Height          =   4335
         Left            =   165
         TabIndex        =   40
         Top             =   675
         Width           =   15915
         _ExtentX        =   28072
         _ExtentY        =   7646
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   5
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Id"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Codigo"
            Object.Width           =   1958
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Nombre"
            Object.Width           =   6174
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Object.Tag             =   "N100"
            Text            =   "Precio"
            Object.Width           =   2249
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   4
            Object.Tag             =   "N100"
            Text            =   "Casa Matriz"
            Object.Width           =   3175
         EndProperty
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel7 
         Height          =   240
         Left            =   180
         OleObjectBlob   =   "ven_por_sucursales.frx":00FC
         TabIndex        =   46
         Top             =   5190
         Width           =   1125
      End
   End
   Begin VB.Frame FrameCliente 
      Caption         =   "Datos del cliente"
      Height          =   1230
      Left            =   195
      TabIndex        =   14
      Top             =   1395
      Width           =   16185
      Begin VB.TextBox TxtDscto 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   11910
         MaxLength       =   6
         TabIndex        =   26
         Top             =   525
         Width           =   615
      End
      Begin VB.TextBox TxtGiro 
         Height          =   285
         Left            =   16200
         Locked          =   -1  'True
         TabIndex        =   25
         TabStop         =   0   'False
         Top             =   780
         Width           =   1680
      End
      Begin VB.CommandButton CmdBuscaCliente 
         Caption         =   "F1 - Buscar"
         Height          =   255
         Left            =   600
         TabIndex        =   24
         Top             =   510
         Width           =   1125
      End
      Begin VB.TextBox TxtCiudad 
         Height          =   285
         Left            =   9795
         Locked          =   -1  'True
         TabIndex        =   23
         TabStop         =   0   'False
         Text            =   " "
         Top             =   540
         Width           =   1335
      End
      Begin VB.TextBox TxtComuna 
         Height          =   285
         Left            =   7680
         Locked          =   -1  'True
         TabIndex        =   22
         TabStop         =   0   'False
         Text            =   " "
         Top             =   540
         Width           =   1425
      End
      Begin VB.TextBox TxtDireccion 
         Height          =   285
         Left            =   3735
         Locked          =   -1  'True
         TabIndex        =   21
         TabStop         =   0   'False
         Top             =   510
         Width           =   2775
      End
      Begin VB.TextBox TxtRazonSocial 
         BackColor       =   &H00E0E0E0&
         Height          =   285
         Left            =   3720
         Locked          =   -1  'True
         TabIndex        =   20
         TabStop         =   0   'False
         Top             =   210
         Width           =   3015
      End
      Begin VB.TextBox TxtRut 
         BackColor       =   &H0080FF80&
         Height          =   285
         Left            =   630
         TabIndex        =   19
         ToolTipText     =   "Ingrese el RUT sin puntos ni guion."
         Top             =   240
         Width           =   1575
      End
      Begin VB.ComboBox CboSucursal 
         Height          =   315
         Left            =   7680
         Style           =   2  'Dropdown List
         TabIndex        =   18
         Top             =   210
         Width           =   6105
      End
      Begin VB.TextBox TxtListaPrecio 
         BackColor       =   &H00E0E0E0&
         Height          =   285
         Left            =   135
         Locked          =   -1  'True
         TabIndex        =   17
         TabStop         =   0   'False
         Top             =   795
         Width           =   2775
      End
      Begin VB.ComboBox CboGiros 
         Height          =   315
         Left            =   3735
         Style           =   2  'Dropdown List
         TabIndex        =   16
         Top             =   810
         Width           =   10020
      End
      Begin VB.ComboBox CboRs 
         Height          =   315
         Left            =   3870
         Style           =   2  'Dropdown List
         TabIndex        =   15
         Top             =   195
         Visible         =   0   'False
         Width           =   3060
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   255
         Left            =   2025
         OleObjectBlob   =   "ven_por_sucursales.frx":0174
         TabIndex        =   27
         Top             =   540
         Width           =   1605
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   255
         Index           =   0
         Left            =   120
         OleObjectBlob   =   "ven_por_sucursales.frx":01E4
         TabIndex        =   28
         Top             =   240
         Width           =   495
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel16 
         Height          =   255
         Left            =   1935
         OleObjectBlob   =   "ven_por_sucursales.frx":0248
         TabIndex        =   29
         Top             =   240
         Width           =   1695
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel18 
         Height          =   255
         Left            =   3210
         OleObjectBlob   =   "ven_por_sucursales.frx":02B2
         TabIndex        =   30
         Top             =   840
         Width           =   420
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel19 
         Height          =   255
         Left            =   6765
         OleObjectBlob   =   "ven_por_sucursales.frx":0318
         TabIndex        =   31
         Top             =   555
         Width           =   855
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel20 
         Height          =   255
         Left            =   9135
         OleObjectBlob   =   "ven_por_sucursales.frx":0382
         TabIndex        =   32
         Top             =   570
         Width           =   735
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel21 
         Height          =   255
         Left            =   11280
         OleObjectBlob   =   "ven_por_sucursales.frx":03EC
         TabIndex        =   33
         Top             =   540
         Width           =   1395
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel5 
         Height          =   255
         Left            =   6750
         OleObjectBlob   =   "ven_por_sucursales.frx":0476
         TabIndex        =   34
         Top             =   210
         Width           =   900
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Datos Venta"
      Height          =   1380
      Left            =   195
      TabIndex        =   0
      Top             =   135
      Width           =   16200
      Begin VB.ComboBox CboVendedores 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   3450
         Style           =   2  'Dropdown List
         TabIndex        =   9
         Top             =   615
         Width           =   2880
      End
      Begin VB.Frame Frame1 
         Caption         =   "Documento de Venta"
         Height          =   1110
         Left            =   11655
         TabIndex        =   4
         Top             =   15
         Width           =   4440
         Begin VB.ComboBox CboDocVenta 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            ItemData        =   "ven_por_sucursales.frx":04E4
            Left            =   210
            List            =   "ven_por_sucursales.frx":04E6
            Style           =   2  'Dropdown List
            TabIndex        =   7
            ToolTipText     =   "Seleccione Documento de  venta. Ventas con Retenci�n, cuando el contribuyente recibe factura de terceros "
            Top             =   525
            Width           =   2370
         End
         Begin VB.TextBox TxtNroDocumento 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   2565
            TabIndex        =   6
            ToolTipText     =   "Nro de Documento"
            Top             =   525
            Width           =   1320
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkFoliosDisponibles 
            Height          =   195
            Left            =   195
            OleObjectBlob   =   "ven_por_sucursales.frx":04E8
            TabIndex        =   5
            Top             =   855
            Width           =   3660
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
            Height          =   210
            Left            =   2565
            OleObjectBlob   =   "ven_por_sucursales.frx":054A
            TabIndex        =   8
            Top             =   300
            Width           =   1260
         End
      End
      Begin VB.Frame Frame3 
         Caption         =   "Bodega Entrega"
         Height          =   1110
         Left            =   16230
         TabIndex        =   2
         Top             =   285
         Visible         =   0   'False
         Width           =   2490
         Begin VB.ComboBox CboBodega 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            ItemData        =   "ven_por_sucursales.frx":05C4
            Left            =   405
            List            =   "ven_por_sucursales.frx":05D1
            Style           =   2  'Dropdown List
            TabIndex        =   3
            ToolTipText     =   "Seleccione Bodega"
            Top             =   600
            Width           =   2355
         End
      End
      Begin VB.ComboBox CboPlazos 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         ItemData        =   "ven_por_sucursales.frx":05F6
         Left            =   1575
         List            =   "ven_por_sucursales.frx":05F8
         Style           =   2  'Dropdown List
         TabIndex        =   1
         Top             =   615
         Width           =   1860
      End
      Begin ACTIVESKINLibCtl.SkinLabel Skvendedor 
         Height          =   180
         Left            =   3435
         OleObjectBlob   =   "ven_por_sucursales.frx":05FA
         TabIndex        =   10
         Top             =   450
         Width           =   810
      End
      Begin MSComCtl2.DTPicker DtFecha 
         Height          =   315
         Left            =   225
         TabIndex        =   11
         Top             =   615
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   556
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   273416193
         CurrentDate     =   39857
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   255
         Left            =   255
         OleObjectBlob   =   "ven_por_sucursales.frx":0661
         TabIndex        =   12
         Top             =   420
         Width           =   1035
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel9 
         Height          =   285
         Left            =   1605
         OleObjectBlob   =   "ven_por_sucursales.frx":06CE
         TabIndex        =   13
         Top             =   420
         Width           =   420
      End
   End
   Begin VB.Timer Timer1 
      Interval        =   50
      Left            =   -195
      Top             =   5775
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   0
      OleObjectBlob   =   "ven_por_sucursales.frx":072F
      Top             =   0
   End
End
Attribute VB_Name = "ven_por_sucursales"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim Im_CantSucursales As Integer
Dim Bm_Cuentas As Boolean
Dim Sm_UtilizaCodigoInterno As String



Private Sub CboDocVenta_Click()
    Dim Rp_DocVenta As Recordset
    Dim Ip_DocIdDocumento As Integer
    If CboDocVenta.ListIndex = -1 Then Exit Sub
    
    Ip_DocIdDocumento = CboDocVenta.ItemData(CboDocVenta.ListIndex)
    Sql = "SELECT doc_dte,doc_cod_sii " & _
            "FROM sis_documentos " & _
            "WHERE doc_id=" & Ip_DocIdDocumento
    Consulta Rp_DocVenta, Sql
    If Rp_DocVenta.RecordCount > 0 Then
        If Rp_DocVenta!doc_dte = "SI" Then
            Sql = "SELECT dte_id,dte_folio numero,doc_id " & _
                "FROM dte_folios " & _
                "WHERE rut_emp='" & SP_Rut_Activo & "' AND doc_id=" & Rp_DocVenta!doc_cod_sii & " AND dte_venta_compra='VENTA' AND dte_disponible='SI' " & _
                "ORDER BY dte_folio " & _
                "LIMIT 1"
            Consulta Rp_DocVenta, Sql
            TxtNroDocumento = 0
            If Rp_DocVenta.RecordCount > 0 Then
                TxtNroDocumento = Rp_DocVenta!Numero
            End If
            Sql = "SELECT count(dte_id) dte " & _
                        "FROM dte_folios " & _
                        "WHERE rut_emp='" & SP_Rut_Activo & "' AND doc_id=" & Rp_DocVenta!doc_id & " AND dte_disponible='SI' " & _
                        "GROUP BY doc_id"
            Consulta RsTmp3, Sql
            If RsTmp3.RecordCount > 0 Then
                SkFoliosDisponibles = "Folios disponibles:" & RsTmp3!Dte
            End If
            
        Else
            TxtNroDocumento = AutoIncremento("LEE", Ip_DocIdDocumento, , IG_id_Sucursal_Empresa)
        End If
    Else
    
        TxtNroDocumento = AutoIncremento("LEE", Ip_DocIdDocumento, , IG_id_Sucursal_Empresa)
    End If
    
    
    
End Sub

Private Sub CmdBuscaCliente_Click()
    LlamaClienteDe = "VD"

    ClienteEncontrado = False
    BuscaCliente.Show 1
    TxtRut = SG_codigo

    TxtRut.SetFocus
End Sub

Private Sub CmdOk_Click()
    Dim Dp_Cantidad As Double
    If Len(TxtCodigo) = 0 Then
        MsgBox "Debe ingresar codigo de producto...", vbInformation
        TxtCodigo.SetFocus
        Exit Sub
    End If
    If Val(TxtPrecio) = 0 Then
        MsgBox "Es necesarios ingresar precio del producto...", vbInformation
        TxtPrecio.SetFocus
        Exit Sub
    End If
    Dp_Cantidad = 0
    For i = 0 To TxtCant.Count - 1
        Dp_Cantidad = Dp_Cantidad + TxtCant(i)
    Next
    If TxtCant.Count - 1 = 0 Then
        Dp_Cantidad = TxtCant(0)
    End If
    
    If Dp_Cantidad = 0 Then
        MsgBox "No ha ingresado cantidades...", vbAbortRetryIgnore
        TxtCant(0).SetFocus
        Exit Sub
    End If
    
    LvDetalle.ListItems.Add , , ""  'TxtCodigo
    i = LvDetalle.ListItems.Count
    LvDetalle.ListItems(i).SubItems(1) = TxtCodigo
    LvDetalle.ListItems(i).SubItems(2) = Me.TxtDescrp
    LvDetalle.ListItems(i).SubItems(3) = TxtPrecio
    c = 4
    If TxtCant.Count > 1 Then
        For p = 0 To TxtCant.Count - 1
            LvDetalle.ListItems(i).SubItems(c) = TxtCant(p)
            c = c + 1
    
        Next
    Else
        LvDetalle.ListItems(i).SubItems(c) = TxtCant(p)
    End If
    
    'Limpiar
    TxtCodigo = ""
    TxtDescrp = ""
    TxtPrecio = ""
    c = 4
    If TxtCant.Count > 1 Then
        For p = 0 To TxtCant.Count - 1
            TxtCant(p) = 0
            c = c + 1
        Next
    End If
    
    '22 Enero 2016
    'Calcular Total Por Factura
  
    TotalesSucursales
    
    
    
    TxtCodigo.SetFocus
    
    
    
End Sub
Private Sub TotalesSucursales()
    c = 4
    suma_columna = 0
    TxtCantFacturas = 0
    If TxtCant.Count > 1 Then
        
        For p = 0 To TxtCant.Count - 1
            
            For i = 1 To LvDetalle.ListItems.Count
                 suma_columna = suma_columna + CDbl(LvDetalle.ListItems(i).SubItems(c)) * CDbl(LvDetalle.ListItems(i).SubItems(3))
                
            Next
            c = c + 1
            txtTotal(p) = NumFormat(suma_columna)
            
            suma_columna = 0
        Next
    Else
        LvDetalle.ListItems(i).SubItems(c) = TxtCant(p)
        
        
    End If
    
    If txtTotal.Count > 1 Then
        For i = 0 To txtTotal.Count - 1
            If Val(txtTotal(i)) > 0 Then
                Me.TxtCantFacturas = Val(TxtCantFacturas) + 1
            End If
        Next
    Else
        If Val(txtTotal(0)) > 1 Then
                Me.TxtCantFacturas = Val(TxtCantFacturas) + 1
        End If
    End If
    
End Sub



Private Sub DescargaSuc()
    For i = TxtCant.Count - 1 To 1 Step -1
          TxtCant(i).Visible = False
        '  Set TxtCant(i) = Nothing
          Unload TxtCant(i)
          
          txtTotal(i).Visible = False
          Unload txtTotal(i)
          
          LvDetalle.ColumnHeaders.Remove LvDetalle.ColumnHeaders.Count
    Next

End Sub

Private Sub Command2_Click()

End Sub

Private Sub Form_Load()
    Aplicar_skin Me
    Centrar Me
      'Consultamos si daremos acceso a a modificar combos.
    Sql = "SELECT are_id,emp_seleciona_datos_contables_venta datos,emp_bitacora_productos bitacora," & _
                     "emp_venta_precios_brutos,emp_avisa_sin_stock,emp_permite_venta_fuera_periodo, " & _
                     "emp_solo_vendedor_asignado venasignado,emp_precio_venta_modificable modificapv, " & _
                     "emp_permite_crear_producto_form_venta crea_productos,emp_actualiza_precio_venta," & _
                     "emp_arrienda_productos arrienda,emp_permite_ajustar_descuento ajustadscto," & _
                     "emp_factura_electronica dte,emp_utiliza_codigos_internos_productos cod_interno,  " & _
                     "emp_de_repuestos,emp_iva_anticipado_ventas " & _
              "FROM sis_empresas " & _
              "WHERE rut='" & SP_Rut_Activo & "'"
              
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        Sm_UtilizaCodigoInterno = RsTmp!cod_interno
    End If
    

    LLenarCombo CboDocVenta, "doc_nombre", "doc_id", "sis_documentos", "doc_activo='SI' AND doc_documento='VENTA' AND doc_utiliza_en_caja='" & SG_Funcion_Equipo & "' " & Sm_FiltroDocVentas, "doc_id"
    LLenarCombo CboPlazos, "CONCAT(pla_nombre,'                                    ',CAST(pla_id AS CHAR))", "pla_dias", "par_plazos_vencimiento", "pla_activo='SI'", "pla_orden"
    LLenarCombo CboVendedores, "ven_nombre", "ven_id", "par_vendedores", "ven_activo='SI' AND rut_emp='" & SP_Rut_Activo & "'", "ven_nombre"
    CboDocVenta.ListIndex = 0
    CboPlazos.ListIndex = 0
    CboVendedores.ListIndex = 0
    
    
End Sub

Private Sub LvDetalle_DblClick()
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    c = 1
    For i = Im_CantSucursales - 1 To 0 Step -1
        TxtCant(i) = LvDetalle.SelectedItem.SubItems(LvDetalle.ColumnHeaders.Count - c)
        c = c + 1
    Next
    TxtCodigo = LvDetalle.SelectedItem.SubItems(1)
    TxtDescrp = LvDetalle.SelectedItem.SubItems(2)
    TxtPrecio = LvDetalle.SelectedItem.SubItems(3)
    
    LvDetalle.ListItems.Remove LvDetalle.SelectedItem.Index
    TotalesSucursales
    TxtPrecio.SetFocus
End Sub

Private Sub Timer1_Timer()
    DtFecha.SetFocus
    Timer1.Enabled = False
End Sub
Private Sub CargaSuc()
    'Si solo es casa matriz, no cargamos nada mas
    If CboSucursal.ListCount = 1 Then Exit Sub
  
    'Cargar textbox para cantidad de cada sucursal
    i = 0
    Im_CantSucursales = CboSucursal.ListCount
    For p = 0 To CboSucursal.ListCount - 1
        If CboSucursal.List(p) <> "CASA MATRIZ" Then
                 i = i + 1
                 Load TxtCant(i)
                 TxtCant(i) = 0 'CboSucursal.List(P)
                 TxtCant(i).Height = TxtCant(0).Height
                 TxtCant(i).Width = TxtCant(0).Width
                 TxtCant(i).Top = TxtCant(0).Top
                 TxtCant(i).ToolTipText = CboSucursal.List(p)
                 TxtCant(i).Left = TxtCant(i - 1).Left + TxtCant(i - 1).Width + 5
                 TxtCant(i).Visible = True
                 TxtCant(i).Tag = CboSucursal.ItemData(p)
                 TxtCant(i).TabIndex = TxtCant(i - 1).TabIndex + 1
                 
                 
                  Load txtTotal(i)
                 txtTotal(i) = 0 'CboSucursal.List(P)
                 txtTotal(i).Height = txtTotal(0).Height
                 txtTotal(i).Width = txtTotal(0).Width
                 txtTotal(i).Top = txtTotal(0).Top
                 txtTotal(i).ToolTipText = CboSucursal.List(p)
                 txtTotal(i).Left = txtTotal(i - 1).Left + txtTotal(i - 1).Width + 5
                 txtTotal(i).Visible = True
                 txtTotal(i).Locked = True
                 txtTotal(i).Tag = CboSucursal.ItemData(p)
                 
                 LvDetalle.ColumnHeaders.Add , , CboSucursal.List(p), TxtCant(i).Width, 1
        End If
    Next
    CmdOk.TabIndex = TxtCant(TxtCant.Count - 1).TabIndex + 1
End Sub

Private Sub TxtCant_GotFocus(Index As Integer)
    En_Foco TxtCant(Index)
End Sub

Private Sub TxtCant_Validate(Index As Integer, Cancel As Boolean)
    If Val(TxtCant(Index)) = 0 Then TxtCant(Index) = 0
End Sub



Private Sub TxtCodigo_KeyPress(KeyAscii As Integer)
    On Error Resume Next
    If KeyAscii = 13 Then SendKeys ("{TAB}")
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
    If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtCodigo_KeyUp(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF1 Then
                If Sp_EmpresaRepuestos = "SI" Then
                    Busca_Producto_Repuestos.Show 1
                    If Sm_UtilizaCodigoInterno = "SI" Then
                        TxtCodigo = SG_codigo2
                    Else
                        TxtCodigo = SG_codigo
                    End If
                    TxtCodigo_Validate True
                    TxtCantidad.SetFocus
                Else
                    BuscaProducto.Show 1
                    TxtCodigo = SG_codigo
                    TxtCodigo_Validate True
                End If
               
    Else
        If KeyCode = 107 Then
            TxtCantidad = Val(TxtCantidad) + 1
            
        ElseIf KeyCode = 109 Then
            TxtCantidad = Val(TxtCantidad) - 1
            If Val(TxtCantidad) = 0 Then TxtCantidad = 1
        End If
    End If
End Sub

Private Sub TxtCodigo_Validate(Cancel As Boolean)
    Dim Sp_FiltroCI As String
    If Len(TxtCodigo.Text) = 0 Then Exit Sub
    CodigoEncontrado = False
   ' Me.FrmDescripcion.Visible = False
    Filtro = "codigo = '" & Me.TxtCodigo.Text & "' "
    Sql = ""
    If Bm_Cuenta Then
        
        If Sm_UtilizaCodigoInterno = "SI" Then
                Sp_FiltroCI = "pro_codigo_interno='" & TxtCodigo & "' "
                Sql = "SELECT codigo, pro_inventariable,descripcion,marca,ubicacion_bodega, " & _
                        "IF((SELECT lst_id FROM maestro_clientes WHERE rut_cliente='" & TxtRut & "')=0 " & _
                         "OR ISNULL((SELECT lst_id FROM maestro_clientes WHERE rut_cliente='" & TxtRut & "')) ,precio_venta," & _
                        "IFNULL((SELECT lsd_precio FROM par_lista_precios_detalle d WHERE lst_id=" & Val(TxtListaPrecio.Tag) & " AND m.id=d.id),precio_venta)) precio_venta," & _
                        "0,'',p.pla_id, " & _
                        "IFNULL((SELECT AVG(pro_ultimo_precio_compra) FROM pro_stock s WHERE s.rut_emp='" & SP_Rut_Activo & "' AND s.pro_codigo=m.codigo),0) precio_compra,ume_nombre,pro_iva_anticipado " & _
                    "FROM maestro_productos m " & _
                    "INNER JOIN par_tipos_productos t ON m.tip_id=t.tip_id " & _
                    "INNER JOIN con_plan_de_cuentas p USING(pla_id) " & _
                    "INNER JOIN sis_unidad_medida d ON m.ume_id=d.ume_id " & _
                    "WHERE pro_activo='SI' AND t.rut_emp='" & SP_Rut_Activo & "' AND  m.rut_emp='" & SP_Rut_Activo & "' AND " & Sp_FiltroCI & " UNION "
         
                    
            
        End If
        
        Sql = Sql & "SELECT codigo, pro_inventariable,descripcion,marca,ubicacion_bodega, " & _
                "IF((SELECT lst_id FROM maestro_clientes WHERE rut_cliente='" & TxtRut & "')=0 " & _
                 "OR ISNULL((SELECT lst_id FROM maestro_clientes WHERE rut_cliente='" & TxtRut & "')) ,precio_venta," & _
                "IFNULL((SELECT lsd_precio FROM par_lista_precios_detalle d WHERE lst_id=" & Val(TxtListaPrecio.Tag) & " AND m.id=d.id),precio_venta)) precio_venta," & _
                "0,'',p.pla_id, " & _
                "IFNULL((SELECT AVG(pro_ultimo_precio_compra) FROM pro_stock s WHERE s.rut_emp='" & SP_Rut_Activo & "' AND s.pro_codigo=m.codigo),0) precio_compra,ume_nombre,pro_iva_anticipado  " & _
              "FROM maestro_productos m " & _
              "INNER JOIN par_tipos_productos t USING(tip_id) " & _
              "INNER JOIN con_plan_de_cuentas p USING(pla_id) " & _
              "INNER JOIN sis_unidad_medida d ON m.ume_id=d.ume_id " & _
              "WHERE pro_activo='SI' AND t.rut_emp='" & SP_Rut_Activo & "' AND  m.rut_emp='" & SP_Rut_Activo & "' AND " & Filtro & " LIMIT 1"
    Else
    
        If Sm_UtilizaCodigoInterno = "SI" Then
            Sp_FiltroCI = "pro_codigo_interno='" & TxtCodigo & "' "
            Sql = "SELECT codigo,pro_inventariable,descripcion,marca,ubicacion_bodega," & _
                    "IFNULL((SELECT lst_id FROM par_asociacion_lista_precios WHERE rut_emp='" & SP_Rut_Activo & "' AND cli_rut='" & TxtRut & "'),0)=0,precio_venta," & _
                    "IFNULL((SELECT lsd_precio FROM par_lista_precios_detalle d WHERE lst_id=" & Val(TxtListaPrecio.Tag) & " AND m.id=d.id),precio_venta) precio_venta," & _
                    "0,'',99999 pla_id," & _
                    "IFNULL((SELECT AVG(pro_ultimo_precio_compra) FROM pro_stock s WHERE s.rut_emp='" & SP_Rut_Activo & "' AND s.pro_codigo=m.codigo),0) precio_compra,ume_nombre,pro_iva_anticipado  " & _
              "FROM maestro_productos m " & _
              "INNER JOIN par_tipos_productos t USING(tip_id) " & _
              "INNER JOIN sis_unidad_medida d ON m.ume_id=d.ume_id " & _
              "WHERE pro_activo='SI' AND m.rut_emp='" & SP_Rut_Activo & "' AND " & Sp_FiltroCI & " UNION "
        End If
            
        Sql = Sql & "SELECT codigo,pro_inventariable,descripcion,marca,ubicacion_bodega," & _
                "IFNULL((SELECT lst_id FROM par_asociacion_lista_precios WHERE rut_emp='" & SP_Rut_Activo & "' AND cli_rut='" & TxtRut & "'),0)=0,precio_venta," & _
                "IFNULL((SELECT lsd_precio FROM par_lista_precios_detalle d WHERE lst_id=" & Val(TxtListaPrecio.Tag) & " AND m.id=d.id),precio_venta) precio_venta," & _
                "0,'',99999 pla_id," & _
                "IFNULL((SELECT AVG(pro_ultimo_precio_compra) FROM pro_stock s WHERE s.rut_emp='" & SP_Rut_Activo & "' AND s.pro_codigo=m.codigo),0) precio_compra,ume_nombre,pro_iva_anticipado  " & _
          "FROM maestro_productos m " & _
          "INNER JOIN par_tipos_productos t USING(tip_id) " & _
          "INNER JOIN sis_unidad_medida d ON m.ume_id=d.ume_id " & _
          "WHERE pro_activo='SI' AND m.rut_emp='" & SP_Rut_Activo & "' AND " & Filtro & " LIMIT 1"

    End If
    
    
    Consulta RsTmp, Sql
    
    With RsTmp
        'DtSalidaMaterial.Value = Date
        If .RecordCount = 0 Then
            If Sm_PermiteCrearProductos = "SI" Then
                    Respuesta = MsgBox("C�digo no encontrado..." & Chr(13) & "�Crear nuevo producto?", vbYesNo + vbQuestion)
                    If Respuesta = 6 Then                'Crear nuevo
                        Fococantidad = False
                        Fococodigo = False
                        AccionProducto = 8
                        AgregarProducto.Bm_Nuevo = True
                        SG_codigo = Empty
                        AgregarProducto.Caption = "Nuevo Producto"
                        AgregarProducto.TxtCodigo.Text = Me.TxtCodigo.Text
                        
                        
                        If SG_Es_la_Flor = "SI" Then
                            AgregarProductoFlor.Show 1
                        Else
                             AgregarProducto.Show 1
                        End If
'                         If Len(Me.TxtMarca) = 0 Then
'                            Fococodigo = True
'                            Fococantidad = False
'                        Else
'                            Fococodigo = False
'                            Fococantidad = True
'                        End If
                    Else
                        TxtCodigo.Text = "": TxtDescrp.Text = "":: TxtPrecio.Text = ""
                        'Me.TxtCantidad.Text = "": TxtIDFamilia.Text = "": TxtNombreFamilia.Text = ""
                        Me.TxtCodigo.SetFocus
                        Fococodigo = True
                        SendKeys "+{TAB}" ' Shift TAB retrocede al control anterior segun el orden del tabindex
        
                    End If
            Else
                    MsgBox "Codigo No encontrado..." & vbNewLine & "Presione F1 para buscar ayuda...", vbInformation + vbOKOnly
                    TxtCodigo.Text = "": TxtDescrp.Text = "":  TxtPrecio.Text = ""
                  '  Me.TxtCantidad.Text = "": TxtIDFamilia.Text = "": TxtNombreFamilia.Text = ""
                    Me.TxtCodigo.SetFocus
                    Fococodigo = True
                    
                    SendKeys "+{TAB}" ' Shift TAB retrocede al control anterior segun el orden del tabindex
            End If
        Else
            'Codigo ingresado  encontrado
            TxtCodigo = !Codigo
          '  If CboCuenta.Locked = True Then Busca_Id_Combo CboCuenta, IIf(Bm_Cuenta, Lp_IdCuentaVentasPorDefecto, 99999)
            TxtCodigo.Tag = !pro_inventariable
           ' Me.FrmDescripcion.Visible = True
           ' If SG_Usuario_Ve_Pcosto = "SI" Then
           '     Me.skDescripcionLarga = !Descripcion & " / ($" & NumFormat(!precio_compra) & ".-)"
           ' Else
           '     Me.skDescripcionLarga = !Descripcion & " .-)"
           ' End If
            SkUbicacion = !ubicacion_bodega
            Me.TxtDescrp.Text = !Descripcion
            'Me.TxtMarca.Text = "" & !MARCA
            'TxtMarca.Tag = !ume_nombre
            '''Comrobar centro costo para definir
            '''si es precio venta o precio compra
            'Verificamos el combro Centro de cstos
            '0 y 2 son precio venta -  1 precio compra
          '  TxtStock = "Stock Actual  : " & .Fields(8)
            TxtPrecio = "" & !precio_venta
            Sm_Precio_Original = "" & !precio_venta
            'TxtCantidad.ToolTipText = .Fields(8)
            'Me.TxtIDFamilia.Text = "" & !familia
            'Me.TxtNombreFamilia = "" & !nombre_familia
            'Me.TxtPcosto = !precio_compra
            'TxtProIvaAnticipado = !pro_iva_anticipado
            
            Fococantidad = True
            CodigoEncontrado = True
            'TxtStock = 0
           
'            Sql = "SELECT sto_stock stock " & _
'                  "FROM  pro_stock " & _
'                  "WHERE rut_emp='" & SP_Rut_Activo & "' AND pro_codigo='" & TxtCodigo & "' AND bod_id=" & IG_id_Bodega_Ventas & " " & _
'                  "GROUP BY pro_codigo"
'            Consulta RsTmp2, Sql
'            If RsTmp2.RecordCount > 0 Then
'                TxtStock = IIf(IsNull(RsTmp2!stock), 0, RsTmp2!stock)
'                If Sm_Avisa_Sin_Stock = "SI" And RsTmp!pro_inventariable = "SI" Then
'                    If Val(TxtStock) < 1 Then
'                        If MsgBox("Sin stock (" & TxtStock & ")" & vbNewLine & " �Continuar?...", vbQuestion + vbYesNo) = vbNo Then
'                            TxtCodigo = Empty
'                            TxtCodigo.Tag = Empty
'                            TxtDescrp = Empty
'                            TxtMarca = Empty
'                            TxtPrecio = 0
'                            TxtPcosto = 0
'                            TxtIDFamilia = Empty
'                            TxtNombreFamilia = Empty
'                            Exit Sub
'                        End If
'                    End If
'                End If
'            End If
'
          '  Sql = "SELECT pro_ultimo_precio_compra costo " & _
          '        "FROM  pro_stock " & _
          '        "WHERE rut_emp='" & SP_Rut_Activo & "' AND  pro_codigo='" & TxtCodigo & "' " & _
          '        "LIMIT 1"
          ''  Consulta RsTmp, Sql
           ' If RsTmp.RecordCount > 0 Then TxtPcosto = IIf(IsNull(RsTmp!costo), 0, RsTmp!costo)
            
            
        End If
    
    End With
End Sub












Private Sub TxtPrecio_GotFocus()
    En_Foco TxtPrecio
End Sub

Private Sub TxtPrecio_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
    If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtPrecio_Validate(Cancel As Boolean)
    If Val(TxtPrecio) = 0 Then TxtPrecio = 0
End Sub



Private Sub TxtRut_GotFocus()
    En_Foco TxtRut
    DescargaSuc
End Sub

Private Sub TxtRut_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
    If KeyAscii = 13 Then SendKeys ("{TAB}")
    If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtRut_KeyUp(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF1 Then CmdBuscaCliente_Click
End Sub

Private Sub TxtRut_LostFocus()
    If Len(TxtRut.Text) = 0 Then
        Me.TxtRazonSocial.Text = ""
        Me.TxtDireccion.Text = ""
        Me.TxtCiudad.Text = ""
        Me.TxtGiro.Text = ""
        Me.txtComuna.Text = ""
        Me.TxtDscto.Text = ""
        Exit Sub
    End If
    If ClienteEncontrado Then
        
    Else
       ' TxtRut.SetFocus
    End If
End Sub

Private Sub TxtRut_Validate(Cancel As Boolean)
    Dim iP_RS As Integer
    TxtListaPrecio = "NORMAL"
    TxtListaPrecio.Tag = 0
    TxtRazonSocial.Tag = ""
    If Len(TxtRut.Text) = 0 Then Exit Sub
    If CboDocVenta.ListIndex = -1 Then
        MsgBox "Seleccione documento...", vbInformation
        On Error Resume Next
        CboDocVenta.SetFocus
        Exit Sub
    End If
    TxtRut.Text = Replace(TxtRut.Text, ".", "")
    TxtRut.Text = Replace(TxtRut.Text, "-", "")
    Respuesta = VerificaRut(TxtRut.Text, NuevoRut)
    CboSucursal.Clear

    If Respuesta = True Then
        Me.TxtRut.Text = NuevoRut
        'Buscar el cliente
        Sql = "SELECT rut_cliente,nombre_rsocial,direccion direccion_,ciudad,comuna,giro,descuento descuento_,fono,email," & _
                "IFNULL(lst_nombre,'LISTA PRECIO GENERAL') listaprecios,m.lst_id,ven_id,cli_monto_credito,cli_bloqueado,cli_motivo_bloqueo " & _
              "FROM maestro_clientes m " & _
              "INNER JOIN par_asociacion_ruts  a ON m.rut_cliente=a.rut_cli " & _
              "LEFT JOIN par_lista_precios l USING(lst_id) " & _
              "WHERE habilitado='SI' AND a.rut_emp='" & SP_Rut_Activo & "' AND rut_cliente = '" & Me.TxtRut.Text & "'"
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
                'Cliente encontrado
            With RsTmp
                Lp_MontoCreditoAprobado = !cli_monto_credito
                sql2 = "SELECT pla_requiere_validacion valida " & _
                        "FROM par_plazos_vencimiento " & _
                        "WHERE pla_id=" & Val(Right(CboPlazos.Text, 10))
                Consulta RsTmp2, sql2
                If RsTmp2!valida = "SI" Then
                    If !cli_monto_credito = 0 Then
                        If MsgBox("El cliente seleccionado no esta habilitado para ventas al credito..." & vbNewLine & _
                            "�Ver ficha ahora...?", vbQuestion + vbYesNo) = vbNo Then
                            
                            TxtRut = ""
                            Exit Sub
                        Else
                            SG_codigo = TxtRut
                            AgregoCliente.Show 1
                            Sql = "SELECT cli_monto_credito " & _
                                    "FROM maestro_clientes " & _
                                    "WHERE rut_cliente='" & TxtRut & "'"
                            Consulta RsTmp3, Sql
                            Lp_MontoCreditoAprobado = RsTmp3!cli_monto_credito
                        End If
                    End If
                    
                    If !cli_bloqueado = "SI" Then
                        
                       MsgBox "El cliente seleccionado esta bloqueado por:" & vbNewLine & vbnewlien & !cli_motivo_bloqueo & "..." & vbNewLine & _
                            ".....", vbInformation
                            
                            TxtRut = ""
                            Exit Sub
                        
                            
                    End If
                    
                    
                    
                End If
                TxtListaPrecio = !listaprecios
                TxtListaPrecio.Tag = !lst_id
                TxtRut.Text = !rut_cliente
                
                TxtRazonSocial.Text = !nombre_rsocial
                TxtRazonSocial.Tag = !Email
                TxtDireccion.Text = "" & !direccion_
                TxtCiudad.Text = "" & !ciudad
                TxtCiudad.Tag = "" & !ciudad
                txtComuna.Text = "" & !comuna
                TxtGiro.Text = "" & !giro
                TxtDscto.Text = "" & !Descuento_
                LbTelefono.Caption = "" & !fono
                ClienteEncontrado = True
               
                
                Sql = "SELECT a.lst_id,lst_nombre " & _
                      "FROM par_asociacion_lista_precios  a " & _
                      "INNER JOIN par_lista_precios l USING(lst_id) " & _
                      "WHERE l.rut_emp='" & SP_Rut_Activo & "' AND cli_rut='" & TxtRut & "'"
                Consulta RsTmp2, Sql
                TxtListaPrecio.Tag = !lst_id
                TxtListaPrecio = "LISTA DE PRECIOS PRINCIPAL"
                If RsTmp2.RecordCount > 0 Then
                    TxtListaPrecio = RsTmp2!lst_nombre
                    TxtListaPrecio.Tag = RsTmp2!lst_id
                End If
                
                
                If Sm_SoloVendedorAsignado = "SI" Then
                    Busca_Id_Combo CboVendedores, Val(!ven_id)
                End If
                    
                     
                
                
                
                LLenarCombo CboSucursal, "CONCAT(suc_ciudad,' ',suc_direccion,' ',suc_contacto)", "suc_id", "par_sucursales", "suc_activo='SI' AND rut_cliente='" & TxtRut & "'", "suc_id"
                CboSucursal.AddItem "CASA MATRIZ"
                CboSucursal.ItemData(CboSucursal.ListCount - 1) = 0
                CboSucursal.ListIndex = CboSucursal.ListCount - 1
                

                
                CboGiros.Clear
                LLenarCombo CboGiros, "gir_nombre", "gir_id", "par_clientes_giros", "cli_rut='" & TxtRut & "'"
                CboGiros.AddItem !giro, 0
                Busca_Id_Combo CboGiros, 0
        '        CboGiros.AddItem TxtGiro
        '        CboGiros.ItemData(CboGiros.ListCount - 1) = 0
                
                            CboRs.Clear
                        LLenarCombo CboRs, "rso_nombre", "rso_id", "par_razon_social_clientes", "rut_cliente='" & Me.TxtRut & "'"
                        If CboRs.ListCount > 0 Then
                           CboRs.Visible = True
                           CboRs.ListIndex = 0
                        Else
                            CboRs.Visible = False
                        End If
                
                CargaSuc
                If bm_SoloVistaDoc Then Exit Sub
                
            End With
        Else
                'Cliente no existe aun �crearlo??
                Respuesta = MsgBox("Cliente no encontrado..." & Chr(13) & _
                "     �Desea Crear?    ", vbYesNo + vbQuestion)
               
        
        End If
       
    Else
        Me.TxtRut.Text = ""
        SendKeys "+{TAB}" ' Shift TAB retrocede al control anterior segun el orden del tabindex
    End If
    Exit Sub
CancelaImpesionFacturacion:
    'No imprime
    Unload Me
End Sub



