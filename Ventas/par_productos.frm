VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{28D47522-CF84-11D1-834C-00A0249F0C28}#1.0#0"; "gif89.dll"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form par_productos 
   Caption         =   "Mantenedor de Productos"
   ClientHeight    =   10710
   ClientLeft      =   2415
   ClientTop       =   1575
   ClientWidth     =   18405
   LinkTopic       =   "Form1"
   ScaleHeight     =   10710
   ScaleWidth      =   18405
   Begin VB.Frame FrmLoad 
      BorderStyle     =   0  'None
      Height          =   1665
      Left            =   6690
      TabIndex        =   56
      Top             =   1965
      Visible         =   0   'False
      Width           =   2805
      Begin GIF89LibCtl.Gif89a Gif89a1 
         Height          =   1245
         Left            =   165
         OleObjectBlob   =   "par_productos.frx":0000
         TabIndex        =   57
         Top             =   240
         Width           =   2445
      End
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkCantidadProductos 
      Height          =   225
      Left            =   285
      OleObjectBlob   =   "par_productos.frx":0086
      TabIndex        =   55
      Top             =   6015
      Width           =   3405
   End
   Begin VB.Frame Frame7 
      Caption         =   "Ingreso de Productos"
      Height          =   6060
      Left            =   10965
      TabIndex        =   14
      Top             =   30
      Width           =   6870
      Begin ACTIVESKINLibCtl.SkinLabel SkCreando 
         Height          =   240
         Left            =   3705
         OleObjectBlob   =   "par_productos.frx":0100
         TabIndex        =   59
         Top             =   495
         Visible         =   0   'False
         Width           =   2085
      End
      Begin VB.TextBox TxtPrecioVta 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   1515
         TabIndex        =   58
         Text            =   "0"
         Top             =   2130
         Visible         =   0   'False
         Width           =   975
      End
      Begin VB.CommandButton CmdNuevo 
         Caption         =   "Nuevo"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   480
         Left            =   1020
         TabIndex        =   43
         Top             =   5370
         Width           =   2205
      End
      Begin VB.CommandButton CmdGrabar 
         Caption         =   "Guardar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   480
         Left            =   3510
         TabIndex        =   42
         Top             =   5370
         Width           =   2205
      End
      Begin VB.Frame Frame6 
         Caption         =   "�Cuando ingreso?"
         Height          =   810
         Left            =   345
         TabIndex        =   38
         Top             =   3135
         Width           =   6270
         Begin VB.TextBox TxtDetalleIngreso 
            Height          =   345
            Left            =   1815
            TabIndex        =   41
            Text            =   "FACTURA 3232389 - CASE"
            Top             =   375
            Width           =   4200
         End
         Begin MSComCtl2.DTPicker DtFechaIngreso 
            Height          =   360
            Left            =   435
            TabIndex        =   40
            Top             =   375
            Width           =   1380
            _ExtentX        =   2434
            _ExtentY        =   635
            _Version        =   393216
            Format          =   96796673
            CurrentDate     =   42779
         End
      End
      Begin VB.Frame Frame4 
         Caption         =   "Estado actual"
         Height          =   1305
         Left            =   345
         TabIndex        =   37
         Top             =   3990
         Width           =   6255
         Begin VB.ComboBox CboEstado 
            Height          =   315
            ItemData        =   "par_productos.frx":017C
            Left            =   645
            List            =   "par_productos.frx":0186
            Style           =   2  'Dropdown List
            TabIndex        =   46
            Top             =   840
            Width           =   5130
         End
         Begin VB.TextBox txtProEstado 
            Height          =   345
            Left            =   630
            TabIndex        =   39
            Text            =   "EN BODEGA TEMUCO"
            Top             =   390
            Width           =   5130
         End
      End
      Begin VB.TextBox TxtCodigoInterno 
         Height          =   285
         Left            =   1470
         TabIndex        =   26
         Top             =   2775
         Width           =   2655
      End
      Begin VB.ComboBox CboHabilitado 
         Height          =   315
         ItemData        =   "par_productos.frx":01A4
         Left            =   3150
         List            =   "par_productos.frx":01AE
         Style           =   2  'Dropdown List
         TabIndex        =   25
         Top             =   2400
         Width           =   765
      End
      Begin VB.TextBox TxtPrecioCostoSFlete 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   1485
         TabIndex        =   24
         Text            =   "0"
         Top             =   1785
         Width           =   975
      End
      Begin VB.CommandButton CmdFamilias 
         Caption         =   "Categoria"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   4725
         TabIndex        =   23
         ToolTipText     =   "Ingrese al Mantenedor de Tipos de Art�culos"
         Top             =   825
         Width           =   1290
      End
      Begin VB.ComboBox CboInventariable 
         Height          =   315
         ItemData        =   "par_productos.frx":01BA
         Left            =   1485
         List            =   "par_productos.frx":01C4
         Style           =   2  'Dropdown List
         TabIndex        =   22
         Top             =   2415
         Width           =   765
      End
      Begin VB.Frame Frame8 
         Caption         =   "Impuestos del Producto"
         Enabled         =   0   'False
         Height          =   3210
         Left            =   7095
         TabIndex        =   20
         Top             =   1035
         Width           =   4545
         Begin MSComctlLib.ListView ListView1 
            Height          =   2670
            Left            =   255
            TabIndex        =   21
            Top             =   390
            Width           =   4110
            _ExtentX        =   7250
            _ExtentY        =   4710
            View            =   3
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            FullRowSelect   =   -1  'True
            GridLines       =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   0
            NumItems        =   3
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Text            =   "Id"
               Object.Width           =   1058
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Text            =   "Descripcion"
               Object.Width           =   4260
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   2
               Object.Tag             =   "N102"
               Text            =   "Factor"
               Object.Width           =   1720
            EndProperty
         End
      End
      Begin VB.TextBox TxtCodigo 
         BackColor       =   &H80000016&
         Height          =   285
         Left            =   1485
         MaxLength       =   50
         TabIndex        =   19
         ToolTipText     =   "Aqui se ingresa el codigo unico"
         Top             =   510
         Width           =   2055
      End
      Begin VB.TextBox TxtDescripcion 
         Height          =   315
         Left            =   1485
         TabIndex        =   18
         Top             =   1455
         Width           =   5220
      End
      Begin VB.ComboBox CboFtipo 
         Height          =   315
         Left            =   1485
         Style           =   2  'Dropdown List
         TabIndex        =   17
         Top             =   825
         Width           =   3255
      End
      Begin VB.ComboBox CboFMarca 
         Height          =   315
         ItemData        =   "par_productos.frx":01D0
         Left            =   1485
         List            =   "par_productos.frx":01D2
         Style           =   2  'Dropdown List
         TabIndex        =   16
         Top             =   1155
         Width           =   3255
      End
      Begin VB.CommandButton CmdMarcas 
         Caption         =   "Sub Categoria"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   4740
         TabIndex        =   15
         ToolTipText     =   "Ingrese al Mantenedor de Marcas de Art�culos"
         Top             =   1155
         Width           =   1290
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel16 
         Height          =   240
         Index           =   0
         Left            =   390
         OleObjectBlob   =   "par_productos.frx":01D4
         TabIndex        =   27
         Top             =   840
         Width           =   960
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel11 
         Height          =   225
         Left            =   2910
         OleObjectBlob   =   "par_productos.frx":0244
         TabIndex        =   28
         Top             =   2670
         Width           =   300
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
         Height          =   315
         Left            =   75
         OleObjectBlob   =   "par_productos.frx":02A2
         TabIndex        =   29
         Top             =   1185
         Width           =   1335
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   255
         Left            =   120
         OleObjectBlob   =   "par_productos.frx":031A
         TabIndex        =   30
         Top             =   510
         Width           =   1290
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   255
         Index           =   0
         Left            =   180
         OleObjectBlob   =   "par_productos.frx":0394
         TabIndex        =   31
         Top             =   1485
         Width           =   1215
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel12 
         Height          =   195
         Left            =   240
         OleObjectBlob   =   "par_productos.frx":0408
         TabIndex        =   32
         Top             =   2430
         Width           =   1095
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel13 
         Height          =   255
         Left            =   210
         OleObjectBlob   =   "par_productos.frx":0480
         TabIndex        =   33
         Top             =   1800
         Width           =   1170
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel15 
         Height          =   195
         Left            =   1935
         OleObjectBlob   =   "par_productos.frx":04F6
         TabIndex        =   34
         Top             =   2430
         Width           =   1095
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   255
         Index           =   3
         Left            =   -330
         OleObjectBlob   =   "par_productos.frx":0568
         TabIndex        =   35
         Top             =   2775
         Width           =   1695
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkCreando2 
         Height          =   240
         Left            =   4215
         OleObjectBlob   =   "par_productos.frx":05E2
         TabIndex        =   60
         Top             =   2790
         Visible         =   0   'False
         Width           =   2085
      End
      Begin VB.Label Label4 
         Alignment       =   2  'Center
         Caption         =   "Para poder grabar el registro debe completar todos los campos"
         Height          =   780
         Left            =   4890
         TabIndex        =   36
         Top             =   2055
         Width           =   1935
      End
   End
   Begin VB.Timer Timer1 
      Interval        =   100
      Left            =   18510
      Top             =   6195
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   18555
      OleObjectBlob   =   "par_productos.frx":065E
      Top             =   7275
   End
   Begin VB.Frame Frame3 
      Caption         =   "Bitacora"
      Height          =   3735
      Left            =   225
      TabIndex        =   10
      Top             =   6360
      Width           =   18060
      Begin TabDlg.SSTab SSTab1 
         Height          =   3225
         Left            =   240
         TabIndex        =   11
         Top             =   345
         Width           =   17595
         _ExtentX        =   31036
         _ExtentY        =   5689
         _Version        =   393216
         Tab             =   2
         TabHeight       =   520
         TabCaption(0)   =   "Arriendos/Contratos"
         TabPicture(0)   =   "par_productos.frx":0892
         Tab(0).ControlEnabled=   0   'False
         Tab(0).Control(0)=   "LvArriendos"
         Tab(0).ControlCount=   1
         TabCaption(1)   =   "Mantenciones Internas"
         TabPicture(1)   =   "par_productos.frx":08AE
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "ListView2"
         Tab(1).ControlCount=   1
         TabCaption(2)   =   "Traslados"
         TabPicture(2)   =   "par_productos.frx":08CA
         Tab(2).ControlEnabled=   -1  'True
         Tab(2).Control(0)=   "ListView3"
         Tab(2).Control(0).Enabled=   0   'False
         Tab(2).Control(1)=   "Text1"
         Tab(2).Control(1).Enabled=   0   'False
         Tab(2).Control(2)=   "Text3"
         Tab(2).Control(2).Enabled=   0   'False
         Tab(2).Control(3)=   "Text4"
         Tab(2).Control(3).Enabled=   0   'False
         Tab(2).Control(4)=   "DtFechaTraslado"
         Tab(2).Control(4).Enabled=   0   'False
         Tab(2).Control(5)=   "CboOrigen"
         Tab(2).Control(5).Enabled=   0   'False
         Tab(2).Control(6)=   "CboDestino"
         Tab(2).Control(6).Enabled=   0   'False
         Tab(2).Control(7)=   "CmdRegistrarTraslado"
         Tab(2).Control(7).Enabled=   0   'False
         Tab(2).ControlCount=   8
         Begin VB.CommandButton CmdRegistrarTraslado 
            Caption         =   "Registrar"
            Height          =   285
            Left            =   15255
            TabIndex        =   54
            Top             =   615
            Width           =   1830
         End
         Begin VB.ComboBox CboDestino 
            Height          =   315
            Left            =   12840
            Style           =   2  'Dropdown List
            TabIndex        =   53
            Top             =   630
            Width           =   2400
         End
         Begin VB.ComboBox CboOrigen 
            Height          =   315
            Left            =   10440
            Locked          =   -1  'True
            Style           =   2  'Dropdown List
            TabIndex        =   52
            Top             =   630
            Width           =   2400
         End
         Begin MSComCtl2.DTPicker DtFechaTraslado 
            Height          =   300
            Left            =   8445
            TabIndex        =   51
            Top             =   630
            Width           =   2000
            _ExtentX        =   3519
            _ExtentY        =   529
            _Version        =   393216
            Format          =   96796673
            CurrentDate     =   42793
         End
         Begin VB.TextBox Text4 
            Height          =   315
            Left            =   4650
            TabIndex        =   50
            Top             =   630
            Width           =   3800
         End
         Begin VB.TextBox Text3 
            Height          =   315
            Left            =   2265
            TabIndex        =   49
            Top             =   630
            Width           =   2400
         End
         Begin VB.TextBox Text1 
            BackColor       =   &H80000016&
            Height          =   285
            Left            =   255
            MaxLength       =   50
            TabIndex        =   48
            ToolTipText     =   "Aqui se ingresa el codigo unico"
            Top             =   645
            Width           =   2000
         End
         Begin MSComctlLib.ListView LvArriendos 
            Height          =   2085
            Left            =   -74730
            TabIndex        =   44
            Top             =   825
            Width           =   16995
            _ExtentX        =   29977
            _ExtentY        =   3678
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            FullRowSelect   =   -1  'True
            GridLines       =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483646
            BackColor       =   -2147483643
            Appearance      =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   14.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            NumItems        =   6
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Object.Tag             =   "N109"
               Text            =   "Id Interno"
               Object.Width           =   0
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Object.Tag             =   "T1000"
               Text            =   "Nro Contrato"
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   2
               Object.Tag             =   "F1000"
               Text            =   "Fecha "
               Object.Width           =   3528
            EndProperty
            BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   3
               Object.Tag             =   "T1000"
               Text            =   "Rut"
               Object.Width           =   3528
            EndProperty
            BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   4
               Object.Tag             =   "T3000"
               Text            =   "Nombre"
               Object.Width           =   12347
            EndProperty
            BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   5
               Object.Tag             =   "T3000"
               Text            =   "Obra"
               Object.Width           =   8819
            EndProperty
         End
         Begin MSComctlLib.ListView ListView2 
            Height          =   1980
            Left            =   -73485
            TabIndex        =   45
            Top             =   540
            Width           =   15540
            _ExtentX        =   27411
            _ExtentY        =   3493
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            FullRowSelect   =   -1  'True
            GridLines       =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483646
            BackColor       =   -2147483643
            Appearance      =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   14.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            NumItems        =   9
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Object.Tag             =   "N109"
               Text            =   "Id Interno"
               Object.Width           =   0
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Object.Tag             =   "T1000"
               Text            =   "Codigo"
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   2
               Object.Tag             =   "T3000"
               Text            =   "Descripci�n"
               Object.Width           =   5292
            EndProperty
            BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   3
               Object.Tag             =   "N102"
               Text            =   "Cantidad"
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   4
               Object.Tag             =   "N100"
               Text            =   "Precio"
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   5
               Object.Tag             =   "N100"
               Text            =   "Total"
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   6
               Object.Tag             =   "N109"
               Text            =   "Stock"
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   7
               Object.Tag             =   "T1000"
               Text            =   "Inventario"
               Object.Width           =   0
            EndProperty
            BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   8
               Text            =   "Precio Costo"
               Object.Width           =   0
            EndProperty
         End
         Begin MSComctlLib.ListView ListView3 
            Height          =   2010
            Left            =   240
            TabIndex        =   47
            Top             =   990
            Width           =   16905
            _ExtentX        =   29819
            _ExtentY        =   3545
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            FullRowSelect   =   -1  'True
            GridLines       =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483646
            BackColor       =   -2147483643
            Appearance      =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   14.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            NumItems        =   6
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Object.Tag             =   "N109"
               Text            =   "Id Traslado"
               Object.Width           =   3528
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Object.Tag             =   "T1000"
               Text            =   "Numero de Guia"
               Object.Width           =   4233
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   2
               Object.Tag             =   "T3000"
               Text            =   "Nombre de usuario"
               Object.Width           =   6703
            EndProperty
            BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   3
               Object.Tag             =   "N102"
               Text            =   "Fecha Traslado"
               Object.Width           =   3528
            EndProperty
            BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   4
               Object.Tag             =   "N100"
               Text            =   "Sucursal Origen"
               Object.Width           =   4233
            EndProperty
            BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   5
               Object.Tag             =   "N100"
               Text            =   "Sucursal Destino"
               Object.Width           =   4233
            EndProperty
         End
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Busqueda"
      Height          =   1110
      Left            =   180
      TabIndex        =   1
      Top             =   0
      Width           =   10590
      Begin VB.Frame Frame5 
         Caption         =   "Bodega"
         Height          =   1095
         Left            =   8685
         TabIndex        =   12
         Top             =   0
         Width           =   1905
         Begin VB.ComboBox CboBodega 
            Height          =   315
            Left            =   60
            Style           =   2  'Dropdown List
            TabIndex        =   13
            Top             =   480
            Width           =   1800
         End
      End
      Begin VB.TextBox TxtBusca 
         Height          =   375
         Left            =   120
         TabIndex        =   9
         Top             =   240
         Width           =   2925
      End
      Begin VB.OptionButton OpDescripcion 
         Caption         =   "Descripcion"
         Height          =   255
         Left            =   135
         TabIndex        =   8
         Top             =   705
         Value           =   -1  'True
         Width           =   1185
      End
      Begin VB.OptionButton OpCodigo 
         Caption         =   "Codigo"
         Height          =   255
         Left            =   1470
         TabIndex        =   7
         Top             =   675
         Width           =   1530
      End
      Begin VB.CommandButton CmdTodosPro 
         Caption         =   "Todos"
         Height          =   375
         Left            =   3060
         TabIndex        =   6
         Top             =   390
         Width           =   900
      End
      Begin VB.Frame FraMarca 
         Caption         =   "Sub-Categoria"
         Height          =   1095
         Left            =   6060
         TabIndex        =   4
         Top             =   0
         Width           =   2640
         Begin VB.ComboBox CboMarca 
            Height          =   315
            Left            =   90
            Style           =   2  'Dropdown List
            TabIndex        =   5
            Top             =   480
            Width           =   2505
         End
      End
      Begin VB.Frame FraFamilia 
         Caption         =   "Categoria"
         Height          =   1095
         Left            =   4080
         TabIndex        =   2
         Top             =   0
         Width           =   2010
         Begin VB.ComboBox CboTipoProducto 
            Height          =   315
            Left            =   120
            Style           =   2  'Dropdown List
            TabIndex        =   3
            Top             =   480
            Width           =   1815
         End
      End
   End
   Begin MSComctlLib.ListView LvDetalle 
      Height          =   4740
      Left            =   210
      TabIndex        =   0
      Top             =   1215
      Width           =   10575
      _ExtentX        =   18653
      _ExtentY        =   8361
      View            =   3
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   19
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Object.Tag             =   "T1000"
         Object.Width           =   529
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Object.Tag             =   "T1000"
         Text            =   "Codigo Empresa"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Object.Tag             =   "T2000"
         Text            =   "Categoria"
         Object.Width           =   3528
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Object.Tag             =   "T2000"
         Text            =   "Sub-Categoria"
         Object.Width           =   3528
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Object.Tag             =   "T4000"
         Text            =   "Descripcion"
         Object.Width           =   7056
      EndProperty
      BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   5
         Object.Tag             =   "N102"
         Text            =   "Pre. Compra"
         Object.Width           =   1676
      EndProperty
      BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   6
         Object.Tag             =   "N102"
         Text            =   "Margen"
         Object.Width           =   1676
      EndProperty
      BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   7
         Object.Tag             =   "N102"
         Text            =   "Pre. Venta"
         Object.Width           =   2117
      EndProperty
      BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   8
         Object.Tag             =   "N102"
         Text            =   "Utilidad"
         Object.Width           =   2117
      EndProperty
      BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   9
         Object.Tag             =   "N102"
         Text            =   "Stock Bodega"
         Object.Width           =   2117
      EndProperty
      BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   10
         Object.Tag             =   "N102"
         Text            =   "En Arriendo"
         Object.Width           =   2117
      EndProperty
      BeginProperty ColumnHeader(12) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   11
         Object.Tag             =   "N102"
         Text            =   "Stock_Total"
         Object.Width           =   2117
      EndProperty
      BeginProperty ColumnHeader(13) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   12
         Object.Tag             =   "N102"
         Text            =   "Stock Minimo"
         Object.Width           =   2293
      EndProperty
      BeginProperty ColumnHeader(14) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   13
         Object.Tag             =   "T2000"
         Text            =   "Bodega"
         Object.Width           =   4586
      EndProperty
      BeginProperty ColumnHeader(15) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   14
         Object.Tag             =   "T1500"
         Text            =   "Ubicacion Bodega"
         Object.Width           =   3528
      EndProperty
      BeginProperty ColumnHeader(16) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   15
         Object.Tag             =   "T2000"
         Text            =   "Obs"
         Object.Width           =   3528
      EndProperty
      BeginProperty ColumnHeader(17) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   16
         Object.Tag             =   "T1000"
         Text            =   "Inventariable"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(18) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   17
         Object.Tag             =   "T1500"
         Text            =   "Codigo Empresa"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(19) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   18
         Object.Tag             =   "T1000"
         Text            =   "Codigo Proveedor"
         Object.Width           =   2540
      EndProperty
   End
End
Attribute VB_Name = "par_productos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim RsProductos As Recordset
Dim s_Sql As String
Dim sql2 As String
Dim Sm_filtro As String
Dim Sm_FiltroMarca As String
Dim Sm_FiltroTipo  As String
Dim Sm_Filtrobodega As String
Dim Bm_GrillaCargada As Boolean
Dim Sp_CodigoProveedor As String
Dim Sp_SoloRut As String
Dim Sm_CodInternos As String * 2
Dim Sm_FiltroCodInterno As String
Dim Sm_TipoF As String



Private Sub CboMarca_Click()
    If Not Bm_GrillaCargada Then Exit Sub
    If CboMarca.Text = "TODOS" Then
        Sm_FiltroMarca = Empty
    Else
        Sm_FiltroMarca = " AND p.mar_id=" & CboMarca.ItemData(CboMarca.ListIndex) & " "
    End If
    
    Grilla
End Sub

Private Sub CboTipoProducto_Click()
    If Not Bm_GrillaCargada Then Exit Sub
    If CboTipoProducto.Text = "TODOS" Then
        Sm_FiltroTipo = Empty
        CboMarca.Clear
        CboMarca.AddItem "TODOS"
    Else
         LLenarCombo CboMarca, "mar_nombre", "mar_id", "par_marcas", "tip_id=" & CboTipoProducto.ItemData(CboTipoProducto.ListIndex) & " AND mar_activo='SI' AND  rut_emp='" & SP_Rut_Activo & "'", "mar_nombre"
         Sm_FiltroMarca = ""
        Sm_FiltroTipo = " AND p.tip_id=" & CboTipoProducto.ItemData(CboTipoProducto.ListIndex) & " "
        
    End If
    Grilla
End Sub

Private Sub BuscarTexto()
    Sm_filtro = Empty
    Sm_FiltroCodInterno = Empty
    If Len(TxtBusca) > 0 Then
        If Me.OpDescripcion.Value Then
            Sm_filtro = "AND descripcion LIKE '%" & TxtBusca & "%' "
        End If
        If Me.OpCodigo Then
            Sm_filtro = "AND codigo LIKE '%" & TxtBusca & "%' "
            If Sm_CodInternos = "SI" Then
                Sm_FiltroCodInterno = " AND pro_codigo_interno LIKE '%" & TxtBusca & "%' "
            End If
        End If
        Grilla
    Else
        Grilla
    End If
End Sub

Private Sub CmdFamilias_Click()
      'Aqui llamamos a un mantenedor
    Sql = "SELECT mav_id,mav_nombre,mav_activo,mav_tabla,mav_consulta,mav_caption,mav_caption_frm,man_editable, " & _
                        "man_dep_id,man_dep_nombre,man_dep_tabla,man_dep_activo,man_dep_titulo " & _
                  "FROM sis_mantenedor_simple " & _
                  "WHERE man_activo='SI' AND man_id=8"
            Consulta RsTmp2, Sql
            If RsTmp2.RecordCount > 0 Then
                With Mantenedor_Dependencia
                    .S_Id = RsTmp2!mav_id
                    .S_Nombre = RsTmp2!mav_nombre
                    .S_Activo = RsTmp2!mav_activo
                    .S_tabla = RsTmp2!mav_tabla
                    .S_Consulta = RsTmp2!mav_consulta
                    .B_Editable = IIf(RsTmp2!man_editable = "SI", True, False)
                    .S_id_dep = RsTmp2!man_dep_id
                    .S_nombre_dep = RsTmp2!man_dep_nombre
                    .S_Activo_dep = RsTmp2!man_dep_activo
                    .S_tabla_dep = RsTmp2!man_dep_tabla
                    .S_titulo_dep = RsTmp2!man_dep_titulo
                    .CmdBusca.Visible = True
                    .Caption = "SubCategoria"
'                    .FrmMantenedor.Caption = RsTmp2!mav_caption_frm

                    .Show 1
                End With
            End If
    LLenarCombo CboTipoProducto, "tip_nombre", "tip_id", "par_tipos_productos", "tip_activo='SI' AND rut_emp='" & SP_Rut_Activo & "'", "tip_nombre"
End Sub


Private Sub CmdGrabar_Click()
    If CboFMarca.ListIndex = -1 Then
        MsgBox "Seleccione Categoria...", vbInformation + vbOKOnly
        CboFMarca.SetFocus
        Exit Sub
    End If
    
    If CboFtipo.ListIndex = -1 Then
        MsgBox "Seleccione Sub-Categoria...", vbInformation + vbOKOnly
        CboFtipo.SetFocus
        Exit Sub
    End If
    
    If Len(TxtDescripcion) = 0 Then
        MsgBox "Ingrese descripcion...", vbInformation + vbOKOnly
        TxtDescripcion.SetFocus
        Exit Sub
    End If
    
   ' If Val(TxtPrecioVta) = 0 Then
    '    MsgBox "Ingrese precio de Venta...", vbInformation + vbOKOnly
    ''    TxtPrecioVta.SetFocus
      '  Exit Sub
    
  '  End If
    
    If SkCreando.Visible Then
        'Nuevo
             sp_Actualiza = "INSERT INTO maestro_productos SET codigo='" & TxtCodigo & "', marca='" & CboMarca.Text & "'," & "descripcion='" & TxtDescripcion & "'," & _
                                                "precio_compra=" & CxP(TxtPrecioCostoSFlete) & "," & _
                                                "porciento_utilidad=0," & _
                                                "/* precio_venta=" & CxP(TxtPrecioVta) & ",*/" & _
                                                "stock_actual=1," & _
                                                "stock_critico=1," & _
                                                "ubicacion_bodega='" & txtUbicacionBodega & "'," & _
                                                "comentario='" & TxtComentario & "', " & _
                                                "bod_id=1," & _
                                                "mar_id=" & CboFMarca.ItemData(CboFMarca.ListIndex) & "," & _
                                                "pro_inventariable='" & Me.CboInventariable.Text & "'," & _
                                                "rut_emp='" & SP_Rut_Activo & "'," & _
                                                "tip_id=" & CboFtipo.ItemData(CboFtipo.ListIndex) & ", " & _
                                                "ume_id=1," & _
                                                "pro_precio_sin_flete=0," & _
                                                "pro_precio_flete=0," & _
                                                "pro_activo='" & CboHabilitado.Text & "', " & _
                                                "pro_codigo_interno='" & TxtCodigoInterno & "', " & _
                                                "pro_tipo='F'," & _
                                                "pro_estado='" & Me.CboEstado & "'," & _
                                                "pro_fecha_ingreso='" & Fql(Me.DtFechaIngreso) & "'," & _
                                                "pro_info_adicional='" & Me.TxtDetalleIngreso & "'"
                                                

                                                SkCreando.Visible = False
                                                SkCreando2.Visible = False
                    '"WHERE codigo='" & TxtCodigo & "' AND rut_emp='" & SP_Rut_Activo & "'"
    
    Else
            If CboEstado.ListIndex = -1 Then
                MsgBox "Seleccione estado...", vbInformation
                CboEstado.SetFocus
                Exit Sub
            End If
    

            sp_Actualiza = "UPDATE maestro_productos SET marca='" & CboMarca.Text & "'," & "descripcion='" & TxtDescripcion & "'," & _
                                                "precio_compra=" & CxP(TxtPrecioCostoSFlete) & "," & _
                                                "porciento_utilidad=0," & _
                                                "/* precio_venta=" & CxP(TxtPrecioVta) & ",*/" & _
                                                "stock_actual=1," & _
                                                "stock_critico=1," & _
                                                "ubicacion_bodega='" & txtUbicacionBodega & "'," & _
                                                "comentario='" & TxtComentario & "', " & _
                                                "bod_id=1," & _
                                                "mar_id=" & CboFMarca.ItemData(CboFMarca.ListIndex) & "," & _
                                                "pro_inventariable='" & Me.CboInventariable.Text & "'," & _
                                                "rut_emp='" & SP_Rut_Activo & "'," & _
                                                "tip_id=" & CboFtipo.ItemData(CboFtipo.ListIndex) & ", " & _
                                                "ume_id=1," & _
                                                "pro_precio_sin_flete=0," & _
                                                "pro_precio_flete=0," & _
                                                "pro_activo='" & CboHabilitado.Text & "', " & _
                                                "pro_codigo_interno='" & TxtCodigoInterno & "', " & _
                                                "pro_tipo='F', " & _
                                                "pro_estado='" & Me.CboEstado.Text & "' " & _
                    "WHERE codigo='" & TxtCodigo & "' AND rut_emp='" & SP_Rut_Activo & "'"
            Debug.Print sp_Actualiza
    End If
            
    cn.Execute sp_Actualiza
            
    MsgBox "Guardado ...", vbInformation
End Sub

Private Sub CmdMarcas_Click()
    With Mantenedor_Simple
        .S_Id = "mar_id"
        .S_Nombre = "mar_nombre"
        .S_Activo = "mar_activo"
        .S_tabla = "par_marcas"
        .S_RutEmpresa = "SI"
        .S_Consulta = "SELECT " & .S_Id & "," & .S_Nombre & "," & .S_Activo & " " & _
                      "FROM " & .S_tabla & " " & _
                      "WHERE  rut_emp='" & SP_Rut_Activo & "' "
        .Caption = "Mantenedor Marcas de Articulos"
        .FrmMantenedor.Caption = "Categorias "
        .B_Editable = True
        .Show 1
    End With
    LLenarCombo CboMarca, "mar_nombre", "mar_id", "par_marcas", "mar_activo='SI' AND rut_emp='" & SP_Rut_Activo & "'", "mar_nombre"

End Sub



Private Sub CmdNuevo_Click()
    SkCreando.Visible = True
    SkCreando2.Visible = True
    Sql = "SELECT MAX(codigo)+1 nuevocodigo " & _
            "FROM maestro_productos " & _
            "WHERE rut_emp='" & SP_Rut_Activo & "'"
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        TxtCodigo = RsTmp!nuevocodigo
        TxtCodigoInterno = Mid(TxtCodigoInterno, 1, 2) & Trim(Str(RsTmp!nuevocodigo))
    End If
End Sub

Private Sub CmdRegistrarTraslado_Click()
    If CboOrigen.ListIndex = -1 Then
        MsgBox "Debe seleccionar Origen...", vbInformation
        CboOrigen.SetFocus
        Exit Sub
    End If
    If CboDestino.ListIndex = -1 Then
        MsgBox "Debe seleccionar Destino...", vbInformation
        CboDestino.SetFocus
        Exit Sub
    End If
    If CboDestino.ListIndex = CboOrigen.ListIndex Then
        MsgBox "Seleccione Destino distino al origen...", vbInformation
        CboDestino.SetFocus
        Exit Sub
    End If
    
    Sql = "UPDATE maestro_productos SET bod_id=" & CboDestino.ItemData(CboDestino.ListIndex) & " " & _
            "WHERE codigo=" & TxtCodigo
    cn.Execute Sql
    CargaDatosProducto
    MsgBox "Traslado Registrado..."
    
    
End Sub

Private Sub Form_Load()
    Centrar Me
    DtFechaIngreso = Date
    DtFechaTraslado = Date
    LLenarCombo CboTipoProducto, "tip_nombre", "tip_id", "par_tipos_productos", "tip_activo='SI' AND  rut_emp='" & SP_Rut_Activo & "'", "tip_nombre"
    LLenarCombo CboFtipo, "tip_nombre", "tip_id", "par_tipos_productos", "tip_activo='SI' AND  rut_emp='" & SP_Rut_Activo & "'", "tip_nombre"
    CboTipoProducto.AddItem "TODOS"
    CboTipoProducto.ListIndex = CboTipoProducto.ListCount - 1
    LLenarCombo CboFMarca, "mar_nombre", "mar_id", "par_marcas", "mar_activo='SI' AND  rut_emp='" & SP_Rut_Activo & "'", "mar_nombre"
   ' LLenarCombo CboMarca, "mar_nombre", "mar_id", "par_marcas", "mar_activo='SI' AND  rut_emp='" & SP_Rut_Activo & "'", "mar_nombre"
    CboMarca.AddItem "TODOS"
    CboMarca.ListIndex = CboMarca.ListCount - 1
    
    LLenarCombo CboBodega, "bod_nombre", "bod_id", "par_bodegas", "bod_activo='SI' AND rut_emp='" & SP_Rut_Activo & "'", "bod_nombre"
    
    LLenarCombo CboOrigen, "bod_nombre", "bod_id", "par_bodegas", "bod_activo='SI' AND rut_emp='" & SP_Rut_Activo & "'", "bod_nombre"
    LLenarCombo CboDestino, "bod_nombre", "bod_id", "par_bodegas", "bod_activo='SI' AND rut_emp='" & SP_Rut_Activo & "'", "bod_nombre"
    
    CboBodega.AddItem "TODAS"
    CboBodega.ListIndex = CboBodega.ListCount - 1
    Sm_CodInternos = "SI"
        
    Sm_FiltroMarca = Empty
    Sm_FiltroTipo = Empty
    Sm_Filtrobodega = Empty
    Skin2 Me, , 4
    Grilla
    
End Sub
Private Sub Grilla()
    s_sql2 = ""
    FrmLoad.Visible = True
    DoEvents
        'bod_id= 1, ya que las bodegas principales, son 1
    
 '   s_Sql = "SELECT codigo,pro_codigo_interno,tip_nombre,mar_nombre,descripcion," & _
             "IFNULL((SELECT AVG(pro_ultimo_precio_compra) FROM pro_stock s WHERE s.rut_emp='" & SP_Rut_Activo & "' AND s.pro_codigo=p.codigo AND bod_id=1),precio_compra) precio_compra," & _
             "porciento_utilidad,precio_venta,precio_venta-IFNULL((" & _
                    "SELECT AVG(pro_ultimo_precio_compra) " & _
                    "FROM pro_stock s " & _
                    "WHERE s.rut_emp = '" & SP_Rut_Activo & "' " & _
                    "AND s.pro_codigo = p.codigo)," & _
                    "precio_compra) margen," & _
              "IF(pro_inventariable='SI',IFNULL((SELECT SUM(sto_stock) FROM pro_stock s WHERE " & Sm_Filtrobodega & " s.rut_emp='" & SP_Rut_Activo & "' AND s.pro_codigo=p.codigo),0),0) stock_bodega," & _
              "IFNULL((SELECT SUM(arr_cantidad) " & _
                "FROM inv_productos_arrendados WHERE arr_devuelto='NO' AND pro_codigo=p.codigo AND emp_id=" & IG_id_Empresa & "),0) arrendados,0, " & _
              "stock_critico, bod_nombre,ubicacion_bodega,comentario,pro_inventariable " & Sp_CodigoProveedor & " " & _
          "FROM maestro_productos p,par_tipos_productos t,par_marcas m,par_bodegas b " & _
          "WHERE p.rut_emp='" & SP_Rut_Activo & "' AND  p.tip_id=t.tip_id AND p.mar_id=m.mar_id AND p.bod_id=b.bod_id " & Sm_filtro & Sm_FiltroTipo & Sm_FiltroMarca & Sm_TipoF & _
          Sp_SoloRut
    
  '  If Sm_CodInternos = "SI" And Bm_GrillaCargada And OpCodigo Then
    
        s_Sql = " SELECT codigo,pro_codigo_interno,tip_nombre,mar_nombre,descripcion," & _
             "IFNULL((SELECT AVG(pro_ultimo_precio_compra) FROM pro_stock s WHERE s.rut_emp='" & SP_Rut_Activo & "' AND s.pro_codigo=p.codigo),0) precio_compra," & _
             "porciento_utilidad,precio_venta,margen," & _
              "IF(pro_inventariable='SI',IFNULL((SELECT SUM(sto_stock) FROM pro_stock s WHERE " & Sm_Filtrobodega & " s.rut_emp='" & SP_Rut_Activo & "' AND s.pro_codigo=p.codigo),0),0) stock_bodega," & _
              "IFNULL((SELECT SUM(arr_cantidad) " & _
                "FROM inv_productos_arrendados WHERE arr_devuelto='NO' AND pro_codigo=p.codigo AND emp_id=" & IG_id_Empresa & "),0) arrendados,0, " & _
              "stock_critico, bod_nombre,ubicacion_bodega,comentario,pro_inventariable " & Sp_CodigoProveedor & " " & _
          "FROM maestro_productos p,par_tipos_productos t,par_marcas m,par_bodegas b " & _
          "WHERE p.rut_emp='" & SP_Rut_Activo & "' AND  p.tip_id=t.tip_id AND p.mar_id=m.mar_id AND p.bod_id=b.bod_id " & Sm_FiltroCodInterno & Sm_FiltroTipo & Sm_FiltroMarca & Sm_TipoF & _
          Sp_SoloRut
    
    
    
   ' End If
    
    
    Consulta RsProductos, s_Sql
    
    
    
    
    LLenar_Grilla RsProductos, Me, LvDetalle, True, True, True, False
    txtCntidadregistros = RsProductos.RecordCount
    For i = 1 To LvDetalle.ListItems.Count
        LvDetalle.ListItems(i).SubItems(8) = CDbl(LvDetalle.ListItems(i).SubItems(8)) - CDbl(LvDetalle.ListItems(i).SubItems(9))
        LvDetalle.ListItems(i).SubItems(10) = CDbl(LvDetalle.ListItems(i).SubItems(9)) + CDbl(LvDetalle.ListItems(i).SubItems(8))
    Next
    Bm_GrillaCargada = True
    Me.SkCantidadProductos = LvDetalle.ListItems.Count
    FrmLoad.Visible = False
End Sub



Private Sub LvDetalle_Click()
    CargaDatosProducto
    
End Sub
Private Sub CargaDatosProducto()
    SkCreando.Visible = False
    SkCreando2.Visible = False
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    FrmLoad.Visible = True
    DoEvents
    Me.SkCantidadProductos = "Registro " & LvDetalle.SelectedItem.Index & " de " & LvDetalle.ListItems.Count
    
    Sql = "SELECT * " & _
            "FROM maestro_productos " & _
            "WHERE codigo=" & LvDetalle.SelectedItem
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        TxtCodigo = RsTmp!Codigo
        TxtDescripcion = RsTmp!Descripcion
        TxtPrecioCostoSFlete = RsTmp!precio_compra
        If Not IsNull(RsTmp!pro_fecha_ingreso) Then Me.DtFechaIngreso = RsTmp!pro_fecha_ingreso
        Me.TxtDetalleIngreso = RsTmp!pro_info_adicional
    '    TxtPrecioVta = RsTmp!precio_venta
        txtProEstado = RsTmp!pro_estado
        If RsTmp!pro_inventariable = "SI" Then
            CboInventariable.ListIndex = 0
        Else
            CboInventariable.ListIndex = 1
        End If
        If RsTmp!pro_activo = "SI" Then
            CboHabilitado.ListIndex = 0
        Else
            CboHabilitado.ListIndex = 1
        End If
        TxtCodigoInterno = RsTmp!pro_codigo_interno
        Busca_Id_Combo CboFtipo, Val(RsTmp!tip_id)
        Busca_Id_Combo CboFMarca, Val(RsTmp!mar_id)
        Busca_Id_Combo CboOrigen, Val(RsTmp!bod_id)
        For i = 0 To CboBodega.ListCount - 1
            If CboBodega.ItemData(i) = RsTmp!bod_id Then
                txtProEstado = UCase(RsTmp!pro_estado) & " EN BODEGA " & UCase(CboBodega.List(i))
            End If
        Next
        
    End If
    
    
    Sql = "SELECT a.arr_id,a.arr_id,arr_fecha_contrato,arr_rut_cliente,arr_nombre_cliente,arr_direccion_obra " & _
            "FROM ven_arriendo a " & _
            "JOIN ven_arr_detalle d ON a.arr_id=d.arr_id " & _
            "WHERE d.det_arr_codigo=" & LvDetalle.SelectedItem
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, Me.LvArriendos, False, True, True, False
    
    FrmLoad.Visible = False
    
End Sub


Private Sub LvDetalle_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    ordListView ColumnHeader, Me, LvDetalle
End Sub
Private Sub LvDetalle_KeyUp(KeyCode As Integer, Shift As Integer)
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    CargaDatosProducto
End Sub

Private Sub TxtBusca_Change()
    BuscarTexto
End Sub

Private Sub TxtBusca_GotFocus()
    En_Foco TxtBusca
End Sub
Private Sub TxtBusca_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub
