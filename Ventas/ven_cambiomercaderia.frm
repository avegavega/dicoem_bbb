VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form ven_cambiomercaderia 
   Caption         =   "Cambio de mercaderia"
   ClientHeight    =   10140
   ClientLeft      =   3780
   ClientTop       =   450
   ClientWidth     =   13500
   LinkTopic       =   "Form1"
   ScaleHeight     =   10140
   ScaleWidth      =   13500
   Begin VB.Frame Frame7 
      Caption         =   "Cambio mercader�a"
      Height          =   9795
      Left            =   285
      TabIndex        =   20
      Top             =   255
      Width           =   13080
      Begin VB.CommandButton CmdSaslir 
         Caption         =   "Retornar"
         Height          =   330
         Left            =   11355
         TabIndex        =   53
         Top             =   9375
         Width           =   1410
      End
      Begin VB.Frame Frame4 
         Caption         =   "Producto(s) a cambiar"
         Height          =   3555
         Left            =   180
         TabIndex        =   37
         Top             =   5760
         Width           =   12645
         Begin VB.CommandButton CmdProcede 
            Caption         =   "Proceder con el Cambio"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   255
            TabIndex        =   52
            Top             =   2910
            Width           =   2820
         End
         Begin VB.TextBox txtTotalCambio 
            Alignment       =   1  'Right Justify
            BackColor       =   &H8000000F&
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   10875
            Locked          =   -1  'True
            TabIndex        =   44
            TabStop         =   0   'False
            Top             =   2805
            Width           =   855
         End
         Begin VB.CommandButton CmdOk 
            Caption         =   "ok"
            Height          =   270
            Left            =   11625
            TabIndex        =   43
            Top             =   555
            Width           =   375
         End
         Begin VB.TextBox txtTotalC 
            Alignment       =   1  'Right Justify
            BackColor       =   &H8000000F&
            Height          =   285
            Left            =   10260
            Locked          =   -1  'True
            TabIndex        =   42
            TabStop         =   0   'False
            Text            =   "0"
            Top             =   570
            Width           =   1320
         End
         Begin VB.TextBox txtCantC 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   9195
            TabIndex        =   41
            Top             =   570
            Width           =   1065
         End
         Begin VB.TextBox txtPventaC 
            Alignment       =   1  'Right Justify
            BackColor       =   &H8000000F&
            Height          =   285
            Left            =   7545
            Locked          =   -1  'True
            TabIndex        =   40
            TabStop         =   0   'False
            Text            =   "0"
            Top             =   570
            Width           =   1650
         End
         Begin VB.TextBox txtCodigoC 
            Height          =   285
            Left            =   255
            TabIndex        =   39
            ToolTipText     =   "F1 buscar productos"
            Top             =   570
            Width           =   855
         End
         Begin VB.TextBox txtDescripcionC 
            BackColor       =   &H8000000F&
            Height          =   285
            Left            =   1110
            Locked          =   -1  'True
            TabIndex        =   38
            TabStop         =   0   'False
            Top             =   570
            Width           =   6435
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel5 
            Height          =   240
            Left            =   1110
            OleObjectBlob   =   "ven_cambiomercaderia.frx":0000
            TabIndex        =   45
            Top             =   360
            Width           =   2880
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel6 
            Height          =   195
            Left            =   240
            OleObjectBlob   =   "ven_cambiomercaderia.frx":0084
            TabIndex        =   46
            ToolTipText     =   "F1 Buscar Producto"
            Top             =   360
            Width           =   840
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
            Height          =   210
            Index           =   4
            Left            =   8400
            OleObjectBlob   =   "ven_cambiomercaderia.frx":00EE
            TabIndex        =   47
            Top             =   360
            Width           =   810
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
            Height          =   210
            Index           =   5
            Left            =   9450
            OleObjectBlob   =   "ven_cambiomercaderia.frx":0164
            TabIndex        =   48
            Top             =   375
            Width           =   810
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
            Height          =   210
            Index           =   6
            Left            =   10740
            OleObjectBlob   =   "ven_cambiomercaderia.frx":01CC
            TabIndex        =   49
            Top             =   360
            Width           =   810
         End
         Begin MSComctlLib.ListView LvProductos 
            Height          =   1890
            Left            =   255
            TabIndex        =   50
            Top             =   870
            Width           =   11760
            _ExtentX        =   20743
            _ExtentY        =   3334
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            FullRowSelect   =   -1  'True
            GridLines       =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            NumItems        =   6
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Object.Tag             =   "N109"
               Text            =   "Codigo"
               Object.Width           =   1508
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Object.Tag             =   "T4500"
               Text            =   "Producto"
               Object.Width           =   11351
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   2
               Object.Tag             =   "N100"
               Text            =   "P.Venta"
               Object.Width           =   2910
            EndProperty
            BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   3
               Object.Tag             =   "N100"
               Text            =   "Cant"
               Object.Width           =   1879
            EndProperty
            BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   4
               Key             =   "total"
               Object.Tag             =   "N100"
               Text            =   "Total"
               Object.Width           =   2328
            EndProperty
            BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   5
               Object.Tag             =   "N109"
               Text            =   "Codigo Sistema"
               Object.Width           =   2540
            EndProperty
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
            Height          =   210
            Index           =   7
            Left            =   9450
            OleObjectBlob   =   "ven_cambiomercaderia.frx":0234
            TabIndex        =   51
            Top             =   2850
            Width           =   1380
         End
      End
      Begin VB.Frame Frame8 
         Caption         =   "Productos del documento"
         Height          =   4185
         Left            =   195
         TabIndex        =   30
         Top             =   1515
         Width           =   12585
         Begin VB.CheckBox ChkTodo 
            Caption         =   "Check1"
            Height          =   195
            Left            =   105
            TabIndex        =   54
            Top             =   360
            Value           =   1  'Checked
            Width           =   180
         End
         Begin VB.TextBox txtCantACambiar 
            BackColor       =   &H8000000F&
            Height          =   285
            Left            =   10725
            Locked          =   -1  'True
            TabIndex        =   35
            TabStop         =   0   'False
            Top             =   3810
            Width           =   1305
         End
         Begin VB.TextBox TxtTotalSeleccionados 
            BackColor       =   &H8000000F&
            Height          =   285
            Left            =   9540
            Locked          =   -1  'True
            TabIndex        =   34
            TabStop         =   0   'False
            Top             =   3525
            Width           =   1185
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel7 
            Height          =   210
            Left            =   7245
            OleObjectBlob   =   "ven_cambiomercaderia.frx":02AA
            TabIndex        =   33
            Top             =   3570
            Width           =   2280
         End
         Begin MSComctlLib.ListView LvProductosDocumento 
            Height          =   3135
            Left            =   75
            TabIndex        =   31
            ToolTipText     =   "Marque los productos que cambiar�"
            Top             =   315
            Width           =   12240
            _ExtentX        =   21590
            _ExtentY        =   5530
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            FullRowSelect   =   -1  'True
            GridLines       =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            NumItems        =   7
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Object.Tag             =   "N109"
               Object.Width           =   529
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Object.Tag             =   "N109"
               Text            =   "Codigo"
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   2
               Object.Tag             =   "T4500"
               Text            =   "Descripcion"
               Object.Width           =   9525
            EndProperty
            BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   3
               Object.Tag             =   "N109"
               Text            =   "Cant."
               Object.Width           =   2117
            EndProperty
            BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   4
               Object.Tag             =   "N100"
               Text            =   "Unitario"
               Object.Width           =   1940
            EndProperty
            BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   5
               Object.Tag             =   "N100"
               Text            =   "SubTotal"
               Object.Width           =   2117
            EndProperty
            BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   6
               Object.Tag             =   "N109"
               Text            =   "Cant. a Cambiar"
               Object.Width           =   2540
            EndProperty
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel8 
            Height          =   210
            Left            =   8415
            OleObjectBlob   =   "ven_cambiomercaderia.frx":0342
            TabIndex        =   36
            Top             =   3855
            Width           =   2280
         End
      End
      Begin VB.Frame Frame6 
         Caption         =   "Documento Referencia"
         Height          =   960
         Left            =   210
         TabIndex        =   21
         Top             =   450
         Width           =   12630
         Begin VB.CommandButton cmdBuscar 
            Caption         =   "Buscar"
            Height          =   285
            Left            =   11265
            TabIndex        =   32
            Top             =   480
            Width           =   1245
         End
         Begin VB.TextBox txtTotalDocBuscado 
            Alignment       =   1  'Right Justify
            BackColor       =   &H8000000F&
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   9990
            Locked          =   -1  'True
            TabIndex        =   25
            TabStop         =   0   'False
            Top             =   495
            Width           =   1290
         End
         Begin VB.TextBox txtFechaDocBuscado 
            BackColor       =   &H8000000F&
            Height          =   285
            Left            =   8910
            Locked          =   -1  'True
            TabIndex        =   24
            TabStop         =   0   'False
            Top             =   495
            Width           =   1065
         End
         Begin VB.TextBox txtNroDocumentoBuscar 
            BackColor       =   &H8000000E&
            Height          =   285
            Left            =   7170
            TabIndex        =   23
            Top             =   495
            Width           =   1740
         End
         Begin VB.ComboBox CboDocVenta 
            Height          =   315
            Left            =   180
            Style           =   2  'Dropdown List
            TabIndex        =   22
            Top             =   495
            Width           =   6945
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
            Height          =   210
            Index           =   9
            Left            =   180
            OleObjectBlob   =   "ven_cambiomercaderia.frx":03D2
            TabIndex        =   26
            Top             =   315
            Width           =   1980
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
            Height          =   210
            Index           =   10
            Left            =   7185
            OleObjectBlob   =   "ven_cambiomercaderia.frx":0442
            TabIndex        =   27
            Top             =   315
            Width           =   1020
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
            Height          =   195
            Index           =   11
            Left            =   8910
            OleObjectBlob   =   "ven_cambiomercaderia.frx":04A6
            TabIndex        =   28
            Top             =   285
            Width           =   870
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
            Height          =   210
            Index           =   12
            Left            =   10035
            OleObjectBlob   =   "ven_cambiomercaderia.frx":050E
            TabIndex        =   29
            Top             =   285
            Width           =   1185
         End
      End
   End
   Begin VB.Timer Timer1 
      Interval        =   50
      Left            =   1005
      Top             =   5835
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   180
      OleObjectBlob   =   "ven_cambiomercaderia.frx":0580
      Top             =   5865
   End
   Begin VB.Frame Frame5 
      Caption         =   "Cambio de mercaderia"
      Height          =   5190
      Left            =   465
      TabIndex        =   0
      Top             =   4860
      Width           =   12930
      Begin VB.Frame Frame3 
         Caption         =   "Documento Referencia"
         Height          =   885
         Left            =   5700
         TabIndex        =   3
         Top             =   405
         Width           =   7140
         Begin VB.TextBox txtDevuelve 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   4845
            TabIndex        =   18
            ToolTipText     =   "Ingrese cantidad que se devuelve"
            Top             =   525
            Width           =   945
         End
         Begin VB.TextBox txtPVendido 
            Alignment       =   1  'Right Justify
            BackColor       =   &H8000000F&
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   5790
            Locked          =   -1  'True
            TabIndex        =   16
            TabStop         =   0   'False
            Top             =   525
            Width           =   1290
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
            Height          =   210
            Index           =   0
            Left            =   180
            OleObjectBlob   =   "ven_cambiomercaderia.frx":07B4
            TabIndex        =   13
            Top             =   315
            Width           =   1980
         End
         Begin VB.TextBox txtFecha 
            BackColor       =   &H8000000F&
            Height          =   285
            Left            =   3780
            Locked          =   -1  'True
            TabIndex        =   12
            TabStop         =   0   'False
            Top             =   525
            Width           =   1065
         End
         Begin VB.TextBox txtNro 
            BackColor       =   &H8000000F&
            Height          =   285
            Left            =   2595
            Locked          =   -1  'True
            TabIndex        =   11
            TabStop         =   0   'False
            Top             =   525
            Width           =   1185
         End
         Begin VB.TextBox txtDocumento 
            BackColor       =   &H8000000F&
            Height          =   285
            Left            =   180
            Locked          =   -1  'True
            TabIndex        =   10
            TabStop         =   0   'False
            Top             =   525
            Width           =   2415
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
            Height          =   210
            Index           =   1
            Left            =   2595
            OleObjectBlob   =   "ven_cambiomercaderia.frx":0824
            TabIndex        =   14
            Top             =   345
            Width           =   1020
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
            Height          =   195
            Index           =   2
            Left            =   3765
            OleObjectBlob   =   "ven_cambiomercaderia.frx":0888
            TabIndex        =   15
            Top             =   345
            Width           =   870
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
            Height          =   210
            Index           =   3
            Left            =   5760
            OleObjectBlob   =   "ven_cambiomercaderia.frx":08F0
            TabIndex        =   17
            Top             =   330
            Width           =   1185
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
            Height          =   210
            Index           =   8
            Left            =   4710
            OleObjectBlob   =   "ven_cambiomercaderia.frx":095C
            TabIndex        =   19
            Top             =   315
            Width           =   1095
         End
      End
      Begin VB.Frame Frame2 
         Caption         =   "Documentos"
         Height          =   3225
         Left            =   225
         TabIndex        =   2
         Top             =   1395
         Width           =   5340
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
            Height          =   180
            Left            =   195
            OleObjectBlob   =   "ven_cambiomercaderia.frx":09D4
            TabIndex        =   9
            Top             =   2805
            Width           =   3240
         End
         Begin MSComctlLib.ListView LvDetalle 
            Height          =   2460
            Left            =   195
            TabIndex        =   8
            Top             =   285
            Width           =   5025
            _ExtentX        =   8864
            _ExtentY        =   4339
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            FullRowSelect   =   -1  'True
            GridLines       =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            NumItems        =   6
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Object.Tag             =   "N109"
               Text            =   "Id"
               Object.Width           =   0
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Object.Tag             =   "T4500"
               Text            =   "Documento"
               Object.Width           =   5292
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   2
               Object.Tag             =   "N109"
               Text            =   "Nro"
               Object.Width           =   1587
            EndProperty
            BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   3
               Object.Tag             =   "F1000"
               Text            =   "Fecha"
               Object.Width           =   1940
            EndProperty
            BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   4
               Text            =   "Precio. Venta"
               Object.Width           =   0
            EndProperty
            BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   5
               Object.Tag             =   "N100"
               Text            =   "Unidades"
               Object.Width           =   2540
            EndProperty
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "Producto a cambiar"
         Height          =   870
         Left            =   240
         TabIndex        =   1
         Top             =   405
         Width           =   5310
         Begin VB.TextBox txtDescripcion 
            BackColor       =   &H8000000F&
            Height          =   285
            Left            =   930
            Locked          =   -1  'True
            TabIndex        =   7
            TabStop         =   0   'False
            Top             =   525
            Width           =   4245
         End
         Begin VB.TextBox txtCodigo 
            Height          =   285
            Left            =   90
            TabIndex        =   6
            Top             =   525
            Width           =   855
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
            Height          =   240
            Left            =   930
            OleObjectBlob   =   "ven_cambiomercaderia.frx":0A68
            TabIndex        =   5
            Top             =   330
            Width           =   3585
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
            Height          =   195
            Left            =   90
            OleObjectBlob   =   "ven_cambiomercaderia.frx":0AEC
            TabIndex        =   4
            Top             =   330
            Width           =   840
         End
      End
   End
End
Attribute VB_Name = "ven_cambiomercaderia"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim Sm_CodigoInterno As String * 2
Private Sub ChkTodo_Click()
    If ChkTodo.Value = 1 Then
            For i = 1 To Me.LvProductosDocumento.ListItems.Count
                Me.LvProductosDocumento.ListItems(i).Checked = False
            Next
    Else
            For i = 1 To Me.LvProductosDocumento.ListItems.Count
                Me.LvProductosDocumento.ListItems(i).Checked = True
            Next
    End If
End Sub

Private Sub CmdBuscar_Click()
    txtFechaDocBuscado = ""
    txtTotalDocBuscado = ""
    If Val(txtNroDocumentoBuscar) = 0 Then
        MsgBox "Ingrese Nro del documento...", vbInformation
        txtNroDocumentoBuscar.SetFocus
        Exit Sub
    End If
    Sql = "SELECT fecha,bruto,id " & _
            "FROM ven_doc_venta " & _
            "WHERE rut_emp='" & SP_Rut_Activo & "' AND no_documento=" & Me.txtNroDocumentoBuscar & " AND doc_id=" & CboDocVenta.ItemData(CboDocVenta.ListIndex)
    Consulta RsTmp, Sql
    If RsTmp.RecordCount = 0 Then
        MsgBox "Documento no encontrado...", vbInformation
        Me.txtNroDocumentoBuscar.SetFocus
        Exit Sub
    End If
    CboDocVenta.Tag = RsTmp!Id
    txtFechaDocBuscado = Format(RsTmp!Fecha, "DD-MM-YYYY")
    txtTotalDocBuscado = NumFormat(RsTmp!bruto)
    
    Sql = "SELECT id,codigo,descripcion,unidades,precio_final,subtotal,unidades " & _
            "FROM ven_detalle " & _
            "WHERE rut_emp='" & SP_Rut_Activo & "' AND no_documento=" & Me.txtNroDocumentoBuscar & " AND doc_id=" & CboDocVenta.ItemData(CboDocVenta.ListIndex)
    Consulta RsTmp, Sql
    
    LLenar_Grilla RsTmp, Me, LvProductosDocumento, True, True, True, False
    For i = 1 To LvProductosDocumento.ListItems.Count
        LvProductosDocumento.ListItems(i).Checked = True
    Next
    CuentaProductosMarcados
End Sub
Private Sub CuentaProductosMarcados()
    txtCantACambiar = 0
    TxtTotalSeleccionados = 0
    For i = 1 To LvProductosDocumento.ListItems.Count
        If LvProductosDocumento.ListItems(i).Checked Then
            txtCantACambiar = Val(txtCantACambiar) + Val(LvProductosDocumento.ListItems(i).SubItems(6))
            TxtTotalSeleccionados = NumFormat(CDbl(TxtTotalSeleccionados) + CDbl(LvProductosDocumento.ListItems(i).SubItems(5)))
        End If
    Next
End Sub
Private Sub CmdOk_Click()
    If Len(txtCodigoC) = 0 Then
        MsgBox "Ingrese codigo...", vbInformation
        txtCodigoC.SetFocus
        Exit Sub
    End If
    If Val(txtCantC) = 0 Then
        MsgBox "Ingrese Cantidad...", vbInformation
        txtCantC.SetFocus
        Exit Sub
    End If
    If Val(txtTotalC) = 0 Then
        MsgBox "Faltan datos...", vbInformation
        txtCodigoC.SetFocus
        Exit Sub
    End If
    
    'Comprobar si el codigo ya esta en la lista
    For i = 1 To LvProductos.ListItems.Count
        If LvProductos.ListItems(i) = txtCodigoC Then
            MsgBox "Ya est� ingresado este codigo...", vbInformation
            Exit Sub
        End If
    Next
    
    
    LvProductos.ListItems.Add , , txtCodigoC
    i = LvProductos.ListItems.Count
    LvProductos.ListItems(i).SubItems(1) = txtDescripcionC
    LvProductos.ListItems(i).SubItems(2) = txtPventaC
    LvProductos.ListItems(i).SubItems(3) = txtCantC
    LvProductos.ListItems(i).SubItems(4) = txtTotalC
    LvProductos.ListItems(i).SubItems(5) = txtCodigoC.Tag
    
    txtCodigoC = ""
    txtDescripcionC = ""
    txtPventaC = 0
    txtCantC = ""
    txtTotalC = 0
    txtCodigoC.Tag = 0
    txtTotalCambio = NumFormat(TotalizaColumna(LvProductos, "total"))
    
    txtCodigoC.SetFocus
End Sub

Private Sub CmdProcede_Click()
    Dim Lp_IdCambio As Long, Lp_Id_inv As Long
'    If Len(TxtCodigo) = 0 Then
'        MsgBox "Debe seleccionar producto que va a cambiar...", vbInformation
'        TxtCodigo.SetFocus
'        Exit Sub
'    End If
    If Val(txtCantACambiar) = 0 Then
        MsgBox "No ha seleccionado productos para cambiar...", vbInformation
        LvProductosDocumento.SetFocus
        Exit Sub
    End If


    If LvProductos.ListItems.Count = 0 Then
        MsgBox "Debe seleccionar productos para el cambio...", vbInformation
        txtCodigoC.SetFocus
        Exit Sub
    End If
'    If Len(txtDocumento) = 0 Then
'        MsgBox "No ha seleccionado documento de referencia...", vbInformation
'        LvDetalle.SetFocus
'        Exit Sub
'    End If
'    If Val(txtDevuelve) = 0 Then
'        MsgBox "Debe ingresar cantidad a devolver...", vbInformation
'        txtDevuelve.SetFocus
'        Exit Sub
'    End If
    
    
    '31-10-2015 _
    Validar que lo que se esta cambiando no sea por el mismo producto
    
    For i = 1 To Me.LvProductosDocumento.ListItems.Count
        If LvProductosDocumento.ListItems(i).Checked Then
            For p = 1 To LvProductos.ListItems.Count
                If LvProductosDocumento.ListItems(i).SubItems(1) = LvProductos.ListItems(p) Then
                    MsgBox "Est� intentando cambiar producto por producto..." & vbNewLine & " Verifique los  datos por favor...", vbExclamation
                    Exit Sub
                End If
            Next
        End If
    Next
    
    
    
    
    
    If MsgBox("Est� seguro de cambiar los productos marcados ...", vbOKCancel + vbQuestion) = vbCancel Then Exit Sub
    
    
    
    On Error GoTo ErrorCambio
    cn.BeginTrans
        Lp_IdCambio = UltimoNro("ven_cambio_mercaderia", "cam_id")
        Sql = "INSERT INTO ven_cambio_mercaderia (cam_id,cam_fecha,id,pro_codigo,cam_usuario,cam_cantidad) " & _
                "VALUES(" & Lp_IdCambio & ",'" & Fql(Date) & " " & Time & "'," & CboDocVenta.Tag & "," & 0 & ",'" & LogUsuario & "'," & Val(Me.txtCantACambiar) & ")"
        cn.Execute Sql
        For i = 1 To LvProductos.ListItems.Count
            'Productos que salen
            cn.Execute "INSERT INTO ven_cambio_mercaderia_detalle (cam_id,pro_codigo,cbd_cantidad,cbd_precio_venta,cbd_tipo) VALUES(" & _
                Lp_IdCambio & "," & LvProductos.ListItems(i).SubItems(5) & "," & LvProductos.ListItems(i).SubItems(3) & "," & CDbl(LvProductos.ListItems(i).SubItems(4)) & ",'SALIDA')"
        Next
        For i = 1 To LvProductosDocumento.ListItems.Count
            'Productos que entran
            If LvProductosDocumento.ListItems(i).Checked And Val(LvProductosDocumento.ListItems(i).SubItems(6)) > 0 Then
                    cn.Execute "INSERT INTO ven_cambio_mercaderia_detalle (cam_id,pro_codigo,cbd_cantidad,cbd_precio_venta,cbd_tipo) VALUES(" & _
                        Lp_IdCambio & "," & LvProductosDocumento.ListItems(i).SubItems(1) & "," & LvProductosDocumento.ListItems(i).SubItems(6) & "," & CDbl(LvProductosDocumento.ListItems(i).SubItems(4)) & ",'ENTRADA')"
            End If
        Next
        

                
        'For i = 1 To LvProductos.ListItems.Count
            'Lp_Id_inv = UltimoNro("inv_kardex", "kar_id")
            'ctd = Val(txtDevuelve) 'LvProductos.ListItems(i).SubItems(3)
            
           ' Sql = " INSERT INTO inv_kardex SELECT " & Lp_Id_inv & ",CURRENT_DATE(),'ENTRADA',kar_documento,kar_numero,bod_id,pro_codigo,/* saldo anterior*/ kar_nuevo_saldo SALDO_ANTERIOR," & _
                    ctd & ", kar_nuevo_saldo + " & Val(ctd) & " NUEVOSALDO,'" & LogUsuario & "'," & _
                    "pro_precio_neto,0,CONCAT('CAMBIO MERCADERIA ',kar_descripcion),rut,rsocial,/*anterior */kar_nuevo_saldo_valor SALDOANTERIOR,pro_precio_neto* " & ctd & ", " & _
                    "kar_nuevo_saldo_valor+(pro_precio_neto*" & ctd & ") NUEVOSALDOVALOR,CURRENT_TIMESTAMP(),kar_mostrar,rut_emp,doc_id,0 " & _
                     "FROM inv_kardex WHERE pro_codigo=" & txtCodigo & " AND id=" & txtDocumento.Tag
            'cn.Execute Sql
       ' Next
        'Ahora viene los movimientos del KARD
                    Dim Dp_SAnterior As Double
                    Dim Dp_SAnteriorvalor As Double
                    Dim Dp_Diferencia As Double
                    Dim Dp_PrecioNeto As Double
                    Dim Ip_IdBodega As Integer
                    Dim Lp_Numero As Long
                
                'Prudcto que vuelve a entrar
                
                
                
                For i = 1 To LvProductosDocumento.ListItems.Count
                        '7 Octubre,2013
                        'Rehacer kardex para ajuste de inventario
                    If LvProductosDocumento.ListItems(i).Checked And Val(LvProductosDocumento.ListItems(i).SubItems(6)) > 0 Then
                            Sql = "SELECT kar_documento,kar_numero,bod_id " & _
                                     "FROM inv_kardex WHERE pro_codigo='" & LvProductosDocumento.ListItems(i).SubItems(1) & "' AND id=" & CboDocVenta.Tag
                            Consulta RsTmp, Sql
                            If RsTmp.RecordCount > 0 Then
                                Ip_IdBodega = RsTmp!bod_id
                                Lp_Numero = RsTmp!kar_numero
                            End If
                    
                            Sql = "SELECT kar_nuevo_saldo,pro_precio_neto,kar_nuevo_saldo_valor " & _
                                            "FROM inv_kardex " & _
                                            "WHERE pro_codigo='" & LvProductosDocumento.ListItems(i).SubItems(1) & "' AND rut_emp='" & SP_Rut_Activo & "' " & _
                                            "ORDER BY kar_id DESC " & _
                                            "LIMIT 1"
                                    Consulta RsTmp, Sql
                                    If RsTmp.RecordCount > 0 Then
                                        Dp_SAnterior = RsTmp!kar_nuevo_saldo
                                        Dp_SAnteriorvalor = RsTmp!kar_nuevo_saldo_valor
                                        Dp_Diferencia = Val(LvProductosDocumento.ListItems(i).SubItems(6))
                                        Dp_PrecioNeto = Round(RsTmp!pro_precio_neto, 0)
                                    End If
                                'Hacer el insert directo sin la funcion Kardex
                             Sql = "INSERT INTO inv_kardex (kar_fecha,kar_movimiento,kar_numero,bod_id,pro_codigo," & _
                                    "kar_saldo_anterior,kar_cantidad,kar_nuevo_saldo,kar_usuario,pro_precio_neto,kar_descripcion," & _
                                    "kar_saldo_anterior_valor,kar_cantidad_valor,kar_nuevo_saldo_valor,rut_emp) VALUES("
                            cn.Execute Sql & "'" & Fql(Date) & "','ENTRADA'," & Lp_Numero & "," & Ip_IdBodega & ",'" & LvProductosDocumento.ListItems(i).SubItems(1) & "'," & _
                                    CxP(Dp_SAnterior) & "," & Abs(Dp_Diferencia) & "," & Val(CxP(Dp_SAnterior)) + Val(LvProductosDocumento.ListItems(i).SubItems(6)) & ",'" & LogUsuario & "'," & CxP(Dp_PrecioNeto) & ",'" & _
                                    "CAMBIO MERCADERIA" & "'," & CxP(Dp_PrecioNeto * CxP(Dp_SAnterior)) & "," & _
                                    CxP(Abs(Dp_Diferencia) * Dp_PrecioNeto) & "," & Dp_SAnteriorvalor + (CxP(Dp_PrecioNeto) * Val(Val(LvProductosDocumento.ListItems(i).SubItems(6)))) & ",'" & SP_Rut_Activo & "')"
                                
                            cn.Execute "UPDATE pro_stock SET sto_stock=" & Dp_SAnterior + Val(LvProductosDocumento.ListItems(i).SubItems(6)) & " " & _
                                                "WHERE bod_id=" & Ip_IdBodega & " AND rut_emp='" & SP_Rut_Activo & "' AND pro_codigo='" & LvProductosDocumento.ListItems(i).SubItems(1) & "'"
                
                            'Hasta aqui el kardex q retira productos.
                              'Prudcto que vuelve a entrar
                    End If
                Next
          
          
          
          'Ahora productos que va a SALIR POR EL CAMBIO
          For i = 1 To LvProductos.ListItems.Count
                'Sql = "SELECT kar_documento,kar_numero,bod_id " & _
                '     "FROM inv_kardex WHERE pro_codigo=" & LvProductos.ListItems(i) & " AND id=" & txtDocumento.Tag
                'Consulta RsTmp, Sql
                'If RsTmp.RecordCount > 0 Then
                '    Ip_IdBodega = RsTmp!bod_id
                '    Lp_Numero = RsTmp!kar_numero
                'End If
                '7 Octubre,2013
                'Rehacer kardex para ajuste de inventario
                    Sql = "SELECT kar_nuevo_saldo,pro_precio_neto,kar_nuevo_saldo_valor " & _
                            "FROM inv_kardex " & _
                            "WHERE pro_codigo='" & LvProductos.ListItems(i).SubItems(5) & "' AND rut_emp='" & SP_Rut_Activo & "' " & _
                            "ORDER BY kar_id DESC " & _
                            "LIMIT 1"
                    Consulta RsTmp, Sql
                    If RsTmp.RecordCount > 0 Then
                        Dp_SAnterior = RsTmp!kar_nuevo_saldo
                        Dp_SAnteriorvalor = RsTmp!kar_nuevo_saldo_valor
                        Dp_Diferencia = LvProductos.ListItems(i).SubItems(3)
                        Dp_PrecioNeto = Round(RsTmp!pro_precio_neto, 0)
                    End If
                'Hacer el insert directo sin la funcion Kardex
             Sql = "INSERT INTO inv_kardex (kar_fecha,kar_movimiento,kar_numero,bod_id,pro_codigo," & _
                    "kar_saldo_anterior,kar_cantidad,kar_nuevo_saldo,kar_usuario,pro_precio_neto,kar_descripcion," & _
                    "kar_saldo_anterior_valor,kar_cantidad_valor,kar_nuevo_saldo_valor,rut_emp) VALUES("
            cn.Execute Sql & "'" & Fql(Date) & "','SALIDA'," & Lp_Numero & "," & Ip_IdBodega & ",'" & LvProductos.ListItems(i).SubItems(5) & "'," & _
                    Dp_SAnterior & "," & Abs(Dp_Diferencia) & "," & CxP(Dp_SAnterior) - Val(LvProductos.ListItems(i).SubItems(3)) & ",'" & LogUsuario & "'," & CxP(Dp_PrecioNeto) & ",'" & _
                    "CAMBIO MERCADERIA" & "'," & CxP(Dp_PrecioNeto * Dp_SAnterior) & "," & _
                    CxP(Abs(Dp_Diferencia) * Dp_PrecioNeto) & "," & Dp_SAnteriorvalor - (CxP(Dp_PrecioNeto) * Val(LvProductos.ListItems(i).SubItems(3))) & ",'" & SP_Rut_Activo & "')"
                
            cn.Execute "UPDATE pro_stock SET sto_stock=" & Dp_SAnterior - Val(LvProductos.ListItems(i).SubItems(3)) & " " & _
                                "WHERE bod_id=" & Ip_IdBodega & " AND rut_emp='" & SP_Rut_Activo & "' AND pro_codigo='" & LvProductos.ListItems(i).SubItems(5) & "'"
        
        
        Next
        
    cn.CommitTrans
    MsgBox "Cambio realizado", vbInformation
    Unload Me
    Exit Sub
ErrorCambio:
    
    MsgBox "Error en el proceso..." & vbNewLine & Err.Description, vbInformation
    cn.RollbackTrans
    
End Sub

Private Sub CmdSaslir_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    Aplicar_skin Me
    Centrar Me
    LLenarCombo Me.CboDocVenta, "doc_nombre", "doc_id", "sis_documentos", "(doc_activo='SI' AND doc_documento='VENTA' AND doc_mueve_inventario='SI') OR doc_abreviado='VTR'", "doc_id"
    CboDocVenta.ListIndex = 0
    Sql = "SELECT emp_utiliza_codigos_internos_productos util " & _
                "FROM sis_empresas " & _
                "WHERE rut='" & SP_Rut_Activo & "'"
    Consulta RsTmp, Sql
    Sm_CodigoInterno = "NO"
    If RsTmp.RecordCount > 0 Then
        Sm_CodigoInterno = RsTmp!util
    End If
    
End Sub

Private Sub LvDetalle_DblClick()
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    TxtDocumento.Tag = LvDetalle.SelectedItem
    TxtDocumento = LvDetalle.SelectedItem.SubItems(1)
    txtNro = LvDetalle.SelectedItem.SubItems(2)
    TxtFecha = LvDetalle.SelectedItem.SubItems(3)
    txtPVendido = LvDetalle.SelectedItem.SubItems(4)
    txtDevuelve = LvDetalle.SelectedItem.SubItems(5)
End Sub

Private Sub LvProductos_DblClick()
    If LvProductos.SelectedItem Is Nothing Then Exit Sub
    
    txtCodigoC = LvProductos.SelectedItem
    txtDescripcionC = LvProductos.SelectedItem.SubItems(1)
    txtPventaC = LvProductos.SelectedItem.SubItems(2)
    txtCantC = LvProductos.SelectedItem.SubItems(3)
    txtTotalC = LvProductos.SelectedItem.SubItems(4)
    LvProductos.ListItems.Remove LvProductos.SelectedItem.Index
    txtCodigoC.SetFocus
End Sub

Private Sub LvProductosDocumento_DblClick()
    Dim Sp_Respuestas As String
    If LvProductosDocumento.SelectedItem Is Nothing Then Exit Sub
    
        
    
    SG_codigo2 = Empty
    sis_InputBox.Caption = "Cambio mercaderia"
    sis_InputBox.FramBox = "Ingrese cantidad a cambiar"
    sis_InputBox.texto.PasswordChar = ""
    sis_InputBox.Sm_TipoDato = "N"
    sis_InputBox.texto = LvProductosDocumento.SelectedItem.SubItems(6)
    sis_InputBox.Show 1
    If Val(SG_codigo2) > 0 Then
        LvProductosDocumento.SelectedItem.SubItems(6) = NumFormat(SG_codigo2)
    End If
    CuentaProductosMarcados
    
End Sub

Private Sub LvProductosDocumento_ItemCheck(ByVal Item As MSComctlLib.ListItem)
    CuentaProductosMarcados
End Sub

Private Sub Timer1_Timer()
    On Error Resume Next
    TxtCodigo.SetFocus
    Timer1.Enabled = False
End Sub

Private Sub txtCantC_GotFocus()
    En_Foco txtCantC
End Sub

Private Sub txtCantC_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
    If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub txtCantC_Validate(Cancel As Boolean)
    If Val(txtPventaC) = 0 Or Val(txtCantC) = 0 Then Exit Sub
    txtTotalC = Val(txtPventaC) * Val(txtCantC)
End Sub

Private Sub TxtCodigo_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub TxtCodigo_KeyUp(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF1 Then
            BuscaProducto.Show 1
            TxtCodigo = SG_codigo
    End If
    If KeyCode = 13 Then
        TxtCodigo_Validate (True)
    End If
End Sub

Private Sub TxtCodigo_Validate(Cancel As Boolean)
    Dim Sp_FilRut As String
    Dim Sp_FilRut2 As String
    Dim Sp_FilRut3 As String
    If Len(TxtCodigo) = 0 Then Exit Sub
    If SG_Sistema_MultiEmpresa = "NO" Then
        Sp_FilRut = ""
    Else
        Sp_FilRut = " AND c.rut_emp='" & SP_Rut_Activo & "' "
        Sp_FilRut2 = " AND d.rut_emp='" & SP_Rut_Activo & "' "
        Sp_FilRut3 = " AND rut_emp='" & SP_Rut_Activo & "' "
    End If
    
    Sql = "SELECT descripcion,precio_venta " & _
            "FROM maestro_productos " & _
            "WHERE codigo='" & TxtCodigo & "' " & Sp_FilRut3
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        TxtDescripcion = RsTmp!Descripcion
        Sql = "SELECT c.id,doc_nombre,c.no_documento,c.fecha,precio_final,unidades " & _
                "FROM ven_doc_venta c " & _
                "JOIN ven_detalle d ON c.no_documento=d.no_documento AND c.doc_id=d.doc_id " & _
                "JOIN sis_documentos s ON c.doc_id=s.doc_id  " & _
                "WHERE d.codigo='" & TxtCodigo & "' " & Sp_FilRut & " " & Sp_FilRut2 & _
                "ORDER BY c.id DESC"
                
 
        
        
       
                
                
        Consulta RsTmp, Sql
        
        LLenar_Grilla RsTmp, Me, LvDetalle, False, True, True, False
        If RsTmp.RecordCount = 0 Then MsgBox "Producto no ha sido vendido...", vbInformation
    Else
        MsgBox "Codigo no encontrado...", vbInformation
        
    End If
    
End Sub


Private Sub txtCodigoC_GotFocus()
    En_Foco txtCodigoC
End Sub

Private Sub txtCodigoC_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
    If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub txtCodigoC_KeyUp(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF1 Then
            BuscaProducto.Show 1
            txtCodigoC = SG_codigo
    End If
    If KeyCode = 13 Then
        txtCodigoC_Validate (True)
       ' SendKeys ("{TAB}")
    End If
End Sub

Private Sub txtCodigoC_Validate(Cancel As Boolean)
    Dim Sp_FilRut As String
    Dim Sp_FilRut2 As String
    Dim Sp_FilRut3 As String
    If Len(txtCodigoC) = 0 Then Exit Sub
    If SG_Sistema_MultiEmpresa = "NO" Then
        Sp_FilRut = ""
    Else
        Sp_FilRut = " AND c.rut_emp='" & SP_Rut_Activo & "' "
        Sp_FilRut2 = " AND d.rut_emp='" & SP_Rut_Activo & "' "
        Sp_FilRut3 = " AND rut_emp='" & SP_Rut_Activo & "' "
    End If
    
    If TxtCodigo = txtCodigoC Then
        MsgBox "No es posible cambiar el mismo producto...", vbInformation
        Exit Sub
    End If
    
    Sql = "SELECT descripcion,precio_venta,codigo " & _
            "FROM maestro_productos " & _
            "WHERE codigo='" & txtCodigoC & "' " & Sp_FilRut3
            
            
     If Sm_CodigoInterno = "SI" Then
        
            Sql = Sql & "union SELECT descripcion,precio_venta,codigo " & _
            "FROM maestro_productos " & _
            "WHERE pro_codigo_interno='" & txtCodigoC & "' " & Sp_FilRut3

    End If
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        txtCodigoC.Tag = RsTmp!Codigo
        txtDescripcionC = RsTmp!Descripcion
        txtPventaC = RsTmp!precio_venta
        txtCantC.SetFocus
      '  Cancel = False
    Else
        MsgBox "Producto no encontrado ...", vbInformation
    End If
End Sub

Private Sub txtDevuelve_GotFocus()
    En_Foco txtDevuelve
End Sub

Private Sub txtDevuelve_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
    If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub txtDevuelve_Validate(Cancel As Boolean)
    Dim Ip_CMax As Integer
    If Val(TxtDocumento.Tag) = 0 Then
        MsgBox "No ha saleccionado documento...", vbInformation
        Exit Sub
    End If
    For i = 1 To LvDetalle.ListItems.Count
        If Val(LvDetalle.ListItems(i)) = Val(TxtDocumento.Tag) Then
            Ip_CMax = LvDetalle.ListItems(i).SubItems(5)
            Exit For
        End If
    Next
    If Val(txtDevuelve) > Ip_CMax Then
        MsgBox "En el documento seleccinado solo se vendieron " & Ip_CMax, vbInformation
        txtDevuelve = Ip_CMax
        Exit Sub
    End If
End Sub
Private Sub txtNroDocumentoBuscar_KeyPress(KeyAscii As Integer)
    KeyAscii = AceptaSoloNumeros(KeyAscii)
    If KeyAscii = 13 Then CmdBuscar_Click
    If KeyAscii = 39 Then KeyAscii = 0
End Sub
