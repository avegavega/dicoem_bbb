VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Object = "{DA729E34-689F-49EA-A856-B57046630B73}#1.0#0"; "progressbar-xp.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form VtaReplicacionFacturacion 
   Caption         =   "Replicar Facturacion"
   ClientHeight    =   8970
   ClientLeft      =   -450
   ClientTop       =   2370
   ClientWidth     =   13920
   LinkTopic       =   "Form1"
   ScaleHeight     =   8970
   ScaleWidth      =   13920
   Begin MSComDlg.CommonDialog Dialogo 
      Left            =   285
      Top             =   3405
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.CommandButton CmdSalir 
      Caption         =   "Retornar"
      Height          =   420
      Left            =   11835
      TabIndex        =   17
      Top             =   8190
      Width           =   1590
   End
   Begin VB.Frame Frame1 
      Caption         =   "Proximo Folio"
      Height          =   1005
      Left            =   8220
      TabIndex        =   14
      Top             =   315
      Width           =   5205
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel6 
         Height          =   165
         Left            =   105
         OleObjectBlob   =   "VtaReplicacionFacturacion.frx":0000
         TabIndex        =   21
         Top             =   375
         Width           =   1110
      End
      Begin MSComCtl2.DTPicker DtFecha 
         Height          =   315
         Left            =   105
         TabIndex        =   20
         Top             =   570
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   556
         _Version        =   393216
         Format          =   273416193
         CurrentDate     =   42052
      End
      Begin VB.CommandButton CmdFacturar 
         Caption         =   "Comenzar Replicaci�n"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   420
         Left            =   3045
         TabIndex        =   16
         Top             =   510
         Width           =   2100
      End
      Begin VB.TextBox TxtProximoFolio 
         Alignment       =   2  'Center
         Height          =   315
         Left            =   1440
         TabIndex        =   15
         Top             =   570
         Width           =   1575
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel7 
         Height          =   165
         Left            =   1440
         OleObjectBlob   =   "VtaReplicacionFacturacion.frx":0068
         TabIndex        =   22
         Top             =   375
         Width           =   1110
      End
   End
   Begin VB.Timer Timer1 
      Interval        =   50
      Left            =   120
      Top             =   2025
   End
   Begin VB.Frame FraProgreso 
      Caption         =   "Progreso exportaci�n"
      Height          =   795
      Left            =   1320
      TabIndex        =   0
      Top             =   3945
      Visible         =   0   'False
      Width           =   11430
      Begin Proyecto2.XP_ProgressBar BarraProgreso 
         Height          =   330
         Left            =   150
         TabIndex        =   1
         Top             =   315
         Width           =   11130
         _ExtentX        =   19632
         _ExtentY        =   582
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BrushStyle      =   0
         Color           =   16777088
         Scrolling       =   1
         ShowText        =   -1  'True
      End
   End
   Begin VB.Frame Frame4 
      Caption         =   "Documento y Periodo a replicar"
      Height          =   1620
      Left            =   615
      TabIndex        =   6
      Top             =   315
      Width           =   7590
      Begin ACTIVESKINLibCtl.SkinLabel SkCantDisponibles 
         Height          =   285
         Left            =   3540
         OleObjectBlob   =   "VtaReplicacionFacturacion.frx":00E0
         TabIndex        =   27
         Top             =   1230
         Visible         =   0   'False
         Width           =   2655
      End
      Begin VB.ComboBox CboDocDestino 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         ItemData        =   "VtaReplicacionFacturacion.frx":015A
         Left            =   210
         List            =   "VtaReplicacionFacturacion.frx":015C
         Style           =   2  'Dropdown List
         TabIndex        =   25
         ToolTipText     =   "Seleccione Documento de  venta. Ventas con Retenci�n, cuando el contribuyente recibe factura de terceros "
         Top             =   1200
         Width           =   3255
      End
      Begin VB.CommandButton CmdBusca 
         Caption         =   "Buscar"
         Height          =   345
         Left            =   6240
         TabIndex        =   13
         Top             =   495
         Width           =   1230
      End
      Begin VB.ComboBox CboDocVenta 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         ItemData        =   "VtaReplicacionFacturacion.frx":015E
         Left            =   210
         List            =   "VtaReplicacionFacturacion.frx":0160
         Style           =   2  'Dropdown List
         TabIndex        =   12
         ToolTipText     =   "Seleccione Documento de  venta. Ventas con Retenci�n, cuando el contribuyente recibe factura de terceros "
         Top             =   555
         Width           =   3255
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   240
         Left            =   210
         OleObjectBlob   =   "VtaReplicacionFacturacion.frx":0162
         TabIndex        =   11
         Top             =   375
         Width           =   1575
      End
      Begin VB.ComboBox comMes 
         Height          =   315
         ItemData        =   "VtaReplicacionFacturacion.frx":01D2
         Left            =   3435
         List            =   "VtaReplicacionFacturacion.frx":01D4
         Style           =   2  'Dropdown List
         TabIndex        =   8
         Top             =   540
         Width           =   1575
      End
      Begin VB.ComboBox ComAno 
         Height          =   315
         Left            =   5010
         Style           =   2  'Dropdown List
         TabIndex        =   7
         Top             =   540
         Width           =   1215
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
         Height          =   255
         Left            =   3435
         OleObjectBlob   =   "VtaReplicacionFacturacion.frx":01D6
         TabIndex        =   9
         Top             =   345
         Width           =   375
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel5 
         Height          =   255
         Left            =   5025
         OleObjectBlob   =   "VtaReplicacionFacturacion.frx":023A
         TabIndex        =   10
         Top             =   345
         Width           =   375
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel8 
         Height          =   225
         Left            =   210
         OleObjectBlob   =   "VtaReplicacionFacturacion.frx":029E
         TabIndex        =   26
         Top             =   1005
         Width           =   2700
      End
   End
   Begin VB.Frame frmOts 
      Caption         =   "Resultado de la busqueda..."
      Height          =   6090
      Left            =   705
      TabIndex        =   2
      Top             =   2100
      Width           =   12750
      Begin VB.CheckBox Chk 
         Caption         =   "Check1"
         Height          =   195
         Left            =   255
         TabIndex        =   24
         Top             =   315
         Width           =   180
      End
      Begin VB.CommandButton CmdImprimir 
         Caption         =   "Imprimir Facturas Seleccionadas"
         Height          =   405
         Left            =   195
         TabIndex        =   23
         Top             =   5595
         Width           =   2760
      End
      Begin VB.Frame Frame2 
         Caption         =   "Trabajando"
         Height          =   1350
         Left            =   2190
         TabIndex        =   18
         Top             =   855
         Visible         =   0   'False
         Width           =   9000
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
            Height          =   435
            Left            =   1110
            OleObjectBlob   =   "VtaReplicacionFacturacion.frx":0324
            TabIndex        =   19
            Top             =   480
            Width           =   7005
         End
      End
      Begin VB.TextBox TxtTotal 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFC0&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   9690
         Locked          =   -1  'True
         TabIndex        =   3
         Text            =   "0"
         Top             =   5655
         Width           =   1560
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   255
         Left            =   8340
         OleObjectBlob   =   "VtaReplicacionFacturacion.frx":039C
         TabIndex        =   4
         Top             =   5760
         Width           =   1185
      End
      Begin MSComctlLib.ListView LvDetalle 
         Height          =   5355
         Left            =   195
         TabIndex        =   5
         Top             =   255
         Width           =   12285
         _ExtentX        =   21669
         _ExtentY        =   9446
         View            =   3
         LabelWrap       =   0   'False
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   12
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "N109"
            Object.Width           =   547
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "N109"
            Text            =   "Nro"
            Object.Width           =   1852
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "N109"
            Text            =   "Nro Documento"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Object.Tag             =   "T1000"
            Text            =   "RUT"
            Object.Width           =   2646
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Object.Tag             =   "T2500"
            Text            =   "Nombre Cliente"
            Object.Width           =   7937
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   5
            Key             =   "valor"
            Object.Tag             =   "N100"
            Text            =   "Valor"
            Object.Width           =   2646
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   6
            Object.Tag             =   "N109"
            Text            =   "iva"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   7
            Object.Tag             =   "N109"
            Text            =   "Neto"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   8
            Object.Tag             =   "F1000"
            Text            =   "Fecha"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   9
            Object.Tag             =   "N109"
            Text            =   "Doc Id"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   10
            Object.Tag             =   "T1000"
            Text            =   "DTE"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(12) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   11
            Object.Tag             =   "T1000"
            Text            =   "Codigo SII"
            Object.Width           =   2540
         EndProperty
      End
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   0
      OleObjectBlob   =   "VtaReplicacionFacturacion.frx":03FD
      Top             =   0
   End
   Begin MSComctlLib.ListView LvMateriales 
      Height          =   2310
      Left            =   900
      TabIndex        =   28
      Top             =   9525
      Width           =   12525
      _ExtentX        =   22093
      _ExtentY        =   4075
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   0   'False
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   0
      NumItems        =   22
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Nro Orden"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Codigo"
         Object.Width           =   2452
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "Marca"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Text            =   "Descripcion"
         Object.Width           =   6174
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   4
         Object.Tag             =   "N100"
         Text            =   "Precio"
         Object.Width           =   1676
      EndProperty
      BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   5
         Text            =   "Dscto."
         Object.Width           =   1676
      EndProperty
      BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   6
         Text            =   "Un."
         Object.Width           =   1235
      EndProperty
      BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   7
         Object.Tag             =   "N100"
         Text            =   "Precio U."
         Object.Width           =   1676
      EndProperty
      BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   8
         Object.Tag             =   "N100"
         Text            =   "Total"
         Object.Width           =   1940
      EndProperty
      BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   9
         Text            =   "Id Familia"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   10
         Text            =   "Costo"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(12) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   11
         Text            =   "Nro de Ot"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(13) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   12
         Text            =   "especial"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(14) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   13
         Text            =   "Comision"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(15) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   14
         Text            =   "Fec. Salida"
         Object.Width           =   2187
      EndProperty
      BeginProperty ColumnHeader(16) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   15
         Text            =   "Utilidad"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(17) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   16
         Object.Tag             =   "T100"
         Text            =   "Inventariable"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(18) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   17
         Object.Tag             =   "N109"
         Text            =   "Cuenta"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(19) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   18
         Object.Tag             =   "N109"
         Text            =   "ARea"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(20) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   19
         Object.Tag             =   "N109"
         Text            =   "CentroCosto"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(21) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   20
         Object.Tag             =   "N109"
         Text            =   "Codigo Activo Inmovilizado"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(22) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   21
         Object.Tag             =   "T1000"
         Text            =   "Unidad Medida"
         Object.Width           =   0
      EndProperty
   End
End
Attribute VB_Name = "VtaReplicacionFacturacion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim Rm_EmpresaEmisora As Recordset
Dim Sm_TipoDocSII As String
Private Declare Function ShellExecute Lib "shell32.dll" Alias "ShellExecuteA" (ByVal hWnd As Long, ByVal lpOperation As String, ByVal lpFile As String, ByVal lpParameters As String, ByVal lpDirectory As String, ByVal nShowCmd As Long) As Long


Private Sub CboDocDestino_Click()
    LvMateriales.Tag = "NO"
    Sm_TipoDocSII = ""
    If CboDocDestino.ListIndex = -1 Then Exit Sub
    
    SkCantDisponibles.Visible = False
    Sql = "SELECT doc_dte,doc_cod_sii " & _
            "FROM sis_documentos " & _
            "WHERE doc_id=" & CboDocDestino.ItemData(CboDocDestino.ListIndex)
    Consulta RsTmp, Sql
    If RsTmp!doc_dte = "SI" Then
        Sm_TipoDocSII = RsTmp!doc_cod_sii
        Sql = "SELECT IFNULL(COUNT(*),0) disponibles " & _
                "FROM dte_folios " & _
                "WHERE rut_emp='" & SP_Rut_Activo & "' AND doc_id=" & RsTmp!doc_cod_sii & " AND dte_disponible='SI'"
        Consulta RsTmp, Sql
        SkCantDisponibles = 0
        If RsTmp.RecordCount > 0 Then
            Me.SkCantDisponibles = "Cant. Disponibles:" & RsTmp!disponibles
            SkCantDisponibles.Tag = RsTmp!disponibles
        End If
        SkCantDisponibles.Visible = True
        LvMateriales.Tag = "SI" ' Es para saber si el documento desitno es electronico
    End If
    
End Sub

Private Sub Chk_Click()
    For i = 1 To LvDetalle.ListItems.Count
        If LvDetalle.ListItems(i).Checked Then LvDetalle.ListItems(i).Checked = False Else LvDetalle.ListItems(i).Checked = True
    
    Next
End Sub

Private Sub CmdBusca_Click()
   
    LvDetalle.ListItems.Clear
    If CboDocVenta.ListIndex = -1 Then
        MsgBox "Seleccionar documento para replicar...", vbInformation
        CboDocVenta.SetFocus
        Exit Sub
    End If
    
    If CboDocDestino.ListIndex = -1 Then
        MsgBox "Seleccionar documento Destino para replicar...", vbInformation
        CboDocDestino.SetFocus
        Exit Sub
    End If
    
    
    Sql = "SELECT  id, @rownum:=@rownum+1 AS rownum,no_documento,rut_cliente,nombre_cliente,bruto,iva,neto,fecha,v.doc_id,d.doc_dte,doc_cod_sii " & _
            "FROM (SELECT @rownum:=0) r,ven_doc_venta v " & _
            "JOIN sis_documentos d ON d.doc_id=v.doc_id " & _
            "WHERE rut_emp='" & SP_Rut_Activo & "' AND rut_cliente<>'NULO' AND v.doc_id=" & CboDocVenta.ItemData(CboDocVenta.ListIndex) & " AND YEAR(fecha)=" & ComAno.Text & " AND MONTH(fecha)=" & comMes.ItemData(comMes.ListIndex) & " " & _
            "ORDER BY @rownum :=@rownum + 1"
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvDetalle, True, True, True, False
    txtTotal = NumFormat(TotalizaColumna(LvDetalle, "valor"))
    Sql = "SELECT IFNULL(MAX(no_documento),0)   +1 folio " & _
            "FROM ven_doc_venta " & _
            "WHERE rut_emp='" & SP_Rut_Activo & "' AND doc_id=" & CboDocDestino.ItemData(CboDocDestino.ListIndex)
    Consulta RsTmp, Sql
    TxtProximoFolio = RsTmp!Folio
    
    
    
    
End Sub

Private Sub CmdFacturar_Click()
     Dim Sp_Facturar As String
     Dim Lp_No_Documento As Long
     Dim Lp_Id_Ventas As Long
     Sp_Facturar = ""
     
     If Year(DtFecha) = ComAno.Text And Month(DtFecha) = comMes.ItemData(comMes.ListIndex) Then
        MsgBox "La fecha para la replicacion no puede ser del mismo mes consultado...", vbInformation
        DtFecha.SetFocus
        Exit Sub
    End If
     
     
     
     For i = 1 To LvDetalle.ListItems.Count
        If LvDetalle.ListItems(i).Checked Then Sp_Facturar = Sp_Facturar = LvDetalle.ListItems(i) & ","
     Next
    If Len(Sp_Facturar) = 0 Then
        MsgBox "No ha seleccionado documentos para replicar...", vbInformation
        LvDetalle.SetFocus
        Exit Sub
    End If
    Sp_Facturar = Mid(Sp_Facturar, 1, Len(Sp_Facturar) - 1)
     
    Frame2.Visible = True
    DoEvents
        
    cn.BeginTrans
    Lp_No_Documento = TxtProximoFolio
    For i = 1 To LvDetalle.ListItems.Count
        If LvDetalle.ListItems(i).Checked Then
            'Obtener nuevo id unico y No documento
            If ComprobarFolio(Lp_No_Documento) Then
                Lp_Id_Ventas = UltimoNro("ven_doc_venta", "id")
                Sql = "INSERT INTO ven_doc_venta (no_documento,fecha,rut_cliente,nombre_cliente,tipo_movimiento," & _
                            "neto,iva,bruto,comentario,condicionpago,ven_id,ven_nombre,forma_pago,tipo_doc, " & _
                            "id,doc_id,exento,usu_nombre,ven_fecha_vencimiento,suc_id,rut_emp,bod_id, " & _
                            "ven_plazo,ven_comentario,sue_id,ven_plazo_id,gir_id,rso_id) " & _
                        "SELECT " & Lp_No_Documento & ",'" & Fql(DtFecha) & "',rut_cliente,nombre_cliente,tipo_movimiento, " & _
                            "neto,iva,bruto,comentario,condicionpago,ven_id,ven_nombre,forma_pago,tipo_doc, " & _
                            Lp_Id_Ventas & "," & CboDocDestino.ItemData(CboDocDestino.ListIndex) & ",exento,usu_nombre,'" & Fql(DtFecha + 30) & "',suc_id,rut_emp,bod_id, " & _
                            "ven_plazo , ven_comentario, sue_id, ven_plazo_id, gir_id, rso_id " & _
                        "FROM ven_doc_venta " & _
                        "WHERE id=" & LvDetalle.ListItems(i)
                cn.Execute Sql
                'Ahora grabar el detalle
                Sql = "INSERT INTO ven_detalle (codigo,marca,descripcion,precio_real,descuento,unidades,precio_final," & _
                            "subtotal,familia,nombre_familia,precio_costo,fecha,doc_id,no_documento," & _
                            "comision,rut_emp,pla_id,are_id,cen_id,ved_precio_venta_bruto,ved_precio_venta_neto, " & _
                            "aim_id,ved_exento,ved_iva) " & _
                        "SELECT codigo,marca,descripcion,precio_real,descuento,unidades,precio_final, " & _
                            "subtotal,familia,nombre_familia,precio_costo,fecha," & CboDocDestino.ItemData(CboDocDestino.ListIndex) & "," & Lp_No_Documento & ", " & _
                            "comision,rut_emp,pla_id,are_id,cen_id,ved_precio_venta_bruto,ved_precio_venta_neto, " & _
                            "aim_id , ved_exento, ved_iva " & _
                        "FROM ven_detalle " & _
                        "WHERE rut_emp='" & SP_Rut_Activo & "' AND no_documento=" & LvDetalle.ListItems(i).SubItems(2) & " AND doc_id=" & CboDocVenta.ItemData(CboDocVenta.ListIndex)
                cn.Execute Sql
            End If
            
            If LvMateriales.Tag = "SI" Then
                'generar DTE
                '4 '4 '2015
                FacturaElectronica Sm_TipoDocSII, Str(Lp_No_Documento), DtFecha, Lp_Id_Ventas, LvDetalle.ListItems(i).SubItems(3), CDbl(LvDetalle.ListItems(i).SubItems(5)), CDbl(LvDetalle.ListItems(i).SubItems(5)), 0, 0, CDbl(LvDetalle.ListItems(i).SubItems(5)), LvDetalle.ListItems(i).SubItems(9), LvDetalle.ListItems(i).SubItems(2)
            
                 cn.Execute "UPDATE dte_folios SET dte_disponible='NO' " & _
                                            "WHERE rut_emp='" & SP_Rut_Activo & "' AND doc_id=" & CboDocDestino.ItemData(CboDocDestino.ListIndex) & " AND dte_folio=" & Lp_No_Documento
            End If
            
            
            
            Lp_No_Documento = Lp_No_Documento + 1
        End If
        
        
        
    Next
    cn.CommitTrans
    
    Frame2.Visible = False
    MsgBox "Replica Generada... Ahora a Imprimir...", vbInformation
    Unload Me
    Exit Sub
ErrorReplica:
    cn.RollbackTrans
    MsgBox "Se detecto un problema al replicar...", vbInformation
End Sub

Private Sub CmdImprimir_Click()
    If MsgBox("Est� seguro de imprimir...?", vbQuestion + vbOKCancel) = vbOK Then
        Dialogo.CancelError = True
        On Error GoTo CancelaImpesionNV
        
        Dialogo.ShowPrinter
    
    
        For i = 1 To LvDetalle.ListItems.Count
            If LvDetalle.ListItems(i).Checked Then
                If LvDetalle.ListItems(i).SubItems(10) = "SI" Then
                    Archivo = "C:\FACTURAELECTRONICA\PDF\T" & LvDetalle.ListItems(i).SubItems(11) & "F" & LvDetalle.ListItems(i).SubItems(2) & ".PDF"
                    ShellExecute Me.hWnd, "print", Archivo, "", "", 4
                    
                Else
            
                    ImprimeFacturaVegaModelo LvDetalle.ListItems(i), LvDetalle.ListItems(i).SubItems(2), LvDetalle.ListItems(i).SubItems(3), LvDetalle.ListItems(i).SubItems(7), LvDetalle.ListItems(i).SubItems(6), LvDetalle.ListItems(i).SubItems(5), LvDetalle.ListItems(i).SubItems(8)
            
                End If
            End If
        Next
    End If
    
    Exit Sub
CancelaImpesionNV:
    'NO QUIZO IMPRIMIR
End Sub

Private Sub cmdSalir_Click()
    Unload Me
End Sub
Function ComprobarFolio(Folio As Long) As Boolean
    Sql = "SELECT id " & _
            "FROM ven_doc_venta " & _
            "WHERE rut_emp='" & SP_Rut_Activo & "' AND doc_id=" & CboDocDestino.ItemData(CboDocDestino.ListIndex) & " AND no_documento=" & Folio
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        ComprobarFolio = False
    Else
        ComprobarFolio = True
    End If

End Function
Private Sub Form_Load()
    Centrar Me
    Aplicar_skin Me
    DtFecha = Date
    For i = 1 To 12
        comMes.AddItem UCase(MonthName(i, False))
        comMes.ItemData(comMes.ListCount - 1) = i
    Next
    Busca_Id_Combo comMes, Val(IG_Mes_Contable)
    LLenaYears ComAno, 2010
    LLenarCombo Me.CboDocVenta, "doc_nombre", "doc_id", "sis_documentos", "doc_activo='SI' AND doc_documento='VENTA' AND doc_utiliza_en_caja='" & SG_Funcion_Equipo & "' " & Sm_FiltroDocVentas, "doc_id"
    LLenarCombo Me.CboDocDestino, "doc_nombre", "doc_id", "sis_documentos", "doc_activo='SI' AND doc_documento='VENTA' AND doc_utiliza_en_caja='" & SG_Funcion_Equipo & "' " & Sm_FiltroDocVentas, "doc_id"
    
    
        'Select empresa para obtener los datos del emisor
    Sql = "SELECT nombre_empresa,direccion,comuna,ciudad,giro,emp_codigo_actividad_economica actividad " & _
            "FROM sis_empresas " & _
            "WHERE rut='" & SP_Rut_Activo & "'"
    Consulta Rm_EmpresaEmisora, Sql
End Sub

Private Sub Timer1_Timer()
    On Error Resume Next
    CboDocVenta.SetFocus
    Timer1.Enabled = False
End Sub
Private Sub TxtProximoFolio_GotFocus()
    En_Foco TxtProximoFolio
End Sub
Private Sub TxtProximoFolio_KeyPress(KeyAscii As Integer)
    KeyAscii = AceptaSoloNumeros(KeyAscii)
    If KeyAscii = 39 Then KeyAscii = 0
End Sub
Private Sub ImprimeFacturaVegaModelo(Id As Long, NoDocumento As Long, Rut As String, Neto As Long, Iva As Long, Total As Long, Fecha As Date)
    Dim Cx As Double, Cy As Double, Sp_Fecha As String, Ip_Mes As Integer, Dp_S As Double, Sp_Letras As String
    Dim Sp_Neto As String * 12, Sp_IVA As String * 12, Sp_Total As String * 12
    Dim Sp_GuiasFacturadas As String
    
    Dim p_Codigo As String * 5
    Dim p_Cantidad As String * 7
    Dim p_UM As String * 4
    Dim p_Detalle As String * 40
    Dim p_Unitario As String * 10
    Dim p_Total As String * 11
    
    
    On Error GoTo ProblemaImpresora
  '  Printer.ScaleMode = vbcentimter
    Printer.FontName = "Courier New"
    Printer.FontSize = 10
    Printer.ScaleMode = 7
        
    'Cx = 2 'horizontal
    'Cy = 5.9 'vertical
    'Dp_S = 0.35
    
    Sql = "SELECT sue_cx,sue_cy " & _
            "FROM sis_empresas_sucursales " & _
            "WHERE sue_id=" & IG_id_Sucursal_Empresa
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        Cx = Val(RsTmp!sue_cx)
        Cy = Val(RsTmp!sue_cy) 'vertical)
    Else
        Cx = 1 'horizontal
        Cy = 0.6
    
    End If
    
    
    'Obtendremos los datos del cliente
    Sql = "SELECT ciudad,comuna,nombre_rsocial,giro,direccion,fono " & _
            "FROM maestro_clientes " & _
            "WHERE rut_cliente='" & Rut & "'"
    Consulta RsTmp, Sql
    If RsTmp.RecordCount = 0 Then
        'esto no debiera passar,
        MsgBox "Cliente no encontrado...", vbInformation
        Exit Sub
    End If
    
    
    
    Dp_S = 0.2
    
    Printer.CurrentX = Cx
    Printer.CurrentY = Cy
    Sp_Fecha = Fecha
    Ip_Mes = Val(Mid(Sp_Fecha, 4, 2))
    
    
    'FECHA
    Printer.CurrentY = Cy + 1.5
    Printer.CurrentX = Cx + 2
    
    
    pos = Printer.CurrentY
    Printer.Print Mid(Sp_Fecha, 1, 2) & "   " & UCase(MonthName(Val(Mid(Sp_Fecha, 4, 2)))) & "   " & Mid(Sp_Fecha, 7, 4)
    pos = pos + 2
   
    'CIUDAD
    Printer.CurrentY = pos
    Printer.CurrentX = Cx + 15
    Printer.Print "Ciudad: " & RsTmp!ciudad


     'comuna
    Printer.CurrentY = pos
    Printer.CurrentX = Cx + 2
    Printer.Print "Comuna   : " & RsTmp!comuna

    'RAZON SOCIAL
    Printer.CurrentX = Cx + 2
    Printer.CurrentY = Cy + 2.9
    pos = Printer.CurrentY
    Printer.Print "Cliente  : " & RsTmp!nombre_rsocial

    
    'RUT
    Printer.CurrentX = Cx + 15
    Printer.CurrentY = pos
    pos = Printer.CurrentY
    Printer.Print "R.U.T.: " & Rut


        
   
    'DIRECCION
    Printer.CurrentX = Cx + 2
    Printer.CurrentY = Cy + 4
    pos = Printer.CurrentY
    posd = Printer.CurrentY
    Printer.Print "Direcci�n: " & RsTmp!direccion
    
    'TELEFONO
    Printer.CurrentY = pos
    Printer.CurrentX = Cx + 15
    Printer.Print "Fono  : " & RsTmp!fono
    
    'GIRO
    Printer.CurrentY = Cy + 4.5
    Printer.CurrentX = Cx + 2
    pos = Printer.CurrentY
    Printer.Print "Giro     : " & RsTmp!giro ' CboGiros.Text ' MARIO ****   TxtGiro
   
    'COND. VENTA
    Printer.CurrentY = pos
    Printer.CurrentX = Cx + 15
   ' Printer.Print "Plazo : " & 'Mid(CboPlazos, 1, 20)
    
    
    '***
   ' Printer.CurrentX = Cx
   ' Printer.CurrentY = Printer.CurrentY + Dp_S - 0.2
  '  Printer.CurrentX = Printer.CurrentX + 10.9
   ' Printer.Print CboVendedores
    
    
    Printer.CurrentY = Printer.CurrentY + Dp_S - 0.3
    '***
    'Printer.CurrentY = Printer.CurrentY + Dp_S
    pos = Printer.CurrentY
    
    Printer.CurrentX = Cx + 8.5
    Printer.CurrentY = Printer.CurrentY + 1
       
    
    
    
    Printer.CurrentY = Printer.CurrentY + 2.8
    
    'Obtener detalle de productos
    '18 Feb 2015
    Sql = "SELECT codigo,unidades,precio_real,descripcion,precio_final,subtotal " & _
            "FROM ven_detalle " & _
            "WHERE rut_emp='" & SP_Rut_Activo & "' AND doc_id=" & CboDocVenta.ItemData(CboDocVenta.ListIndex) & " AND no_documento=" & NoDocumento
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        RsTmp.MoveFirst
        Do While Not RsTmp.EOF
        
        
        
        
    'For i = 1 To LvMateriales.ListItems.Count
        '6 - 3 - 7 - 8
       ' Printer.CurrentX = Printer.CurrentX + 0.07 'INTERLINIA ARTICULOS
       'esperar el interlinedo automtico
       ' Printer.CurrentX = Cx - 2
                Printer.CurrentX = Cx - 0.5
                
                p_Codigo = RsTmp!Codigo ' LvMateriales.ListItems(i).SubItems(1)
                RSet p_Cantidad = RsTmp!Unidades ' LvMateriales.ListItems(i).SubItems(6)
                p_UM = "UN" ' LvMateriales.ListItems(i).SubItems(21)
                p_Detalle = RsTmp!Descripcion ' LvMateriales.ListItems(i).SubItems(3)
                RSet p_Unitario = RsTmp!precio_final 'LvMateriales.ListItems(i).SubItems(7)
                RSet p_Total = RsTmp!SubTotal ' LvMateriales.ListItems(i).SubItems(8)
                
                Printer.Print "     " & "    " & p_Cantidad & "      " & p_Detalle & "    " & p_Unitario & "    " & p_Total
                'Printer.Print p_Codigo & "    " & p_Cantidad & "      " & p_Detalle & "    " & p_Unitario & "    " & p_Total
    'Next
            RsTmp.MoveNext
        Loop
    End If
    'If Bm_Factura_por_Guias Then LvMateriales.ListItems.Clear
    
    Printer.CurrentX = Cx - 1
    Printer.CurrentY = Cy + 14.2
    pos = Printer.CurrentY
        
    Printer.CurrentX = Cx + 1
    Printer.CurrentY = pos - 2.2
    'Printer.Print TxtComentario
    
  '  pos = Printer.CurrentY
  
    Printer.CurrentX = Cx + 1
        
    'Printer.CurrentY = pos
    Printer.CurrentY = pos - 1.4
    
   
    
   ' mitad = Len(Sp_Letras) \ 2
   ' buscaespacio = InStr(mitad, Sp_Letras, " ")
   ' If buscaespacio > 0 Then
   '         Printer.Print Mid(Sp_Letras, 1, buscaespacio)
   '         Printer.CurrentX = Cx - 1
   '         Printer.CurrentY = Cy + 15.3
   '         Printer.Print Mid(Sp_Letras, buscaespacio)
   ' Else
   '           Printer.Print Sp_Letras
   ' End If
    
    
    Cy = 13
    
    RSet Sp_Neto = IIf(CDbl(Neto) = 0, "", Total)
    RSet Sp_IVA = IIf(CDbl(Iva) = 0, "", Iva)
    RSet Sp_Total = Total
    pos = pos - 1.4
    
    
    Printer.CurrentX = Cx + 16.1
    Printer.CurrentY = Cy + 9.3
    Printer.Print Sp_Neto
    
    Printer.CurrentX = Cx + 16.1
    Printer.CurrentY = Cy + 9.8
    Printer.Print Sp_IVA
    
    Printer.CurrentX = Cx + 16.1
    Printer.CurrentY = Cy + 9.3
    Printer.Print Sp_Total
    
    Printer.CurrentX = Cx + 2
    Printer.CurrentY = Cy + 10.1
    Sp_Letras = UCase(BrutoAletras(CDbl(Total), True))
    Printer.Print Sp_Letras
    Printer.NewPage
    Printer.EndDoc
    Exit Sub
ProblemaImpresora:
    MsgBox "Ocurrio un error al intentar imprimir..." & vbNewLine & Err.Description & vbNewLine & Err.Number

End Sub



Private Sub FacturaElectronica(Tipo As String, Folio As String, Fecha As Date, Id As Long, Rut As String, Neto As String, Exento As String, TasaIva As String, Iva As String, Total As String, DodId As String, NoDoc As String)
    Dim Sp_Xml As String
    Dim obj As DTECloud.Integracion
    Dim Lp_Sangria As Long
    Dim Sp_Sql As String
    '13-9-2014
    'Modulo genera factura electronica
    'Autor: alvamar
    
    
    '1ro Crear xml
    Lp_Sangria = 4
    X = FreeFile
    
   ' Sp_Xml = App.Path & "\dte\" & CboDocDestino.ItemData(CboDocDestino.ListIndex) & "_" & TxtNroDocumento & ".xml"
    Sp_Xml = "C:\nueva2.xml"
      'Open Sp_Archivo For Output As X
    Open Sp_Xml For Output As X
        '<?xml version="1.0" encoding="ISO-8859-1"?>
        Print #X, "<DTE version=" & Chr(34) & "1.0" & Chr(34) & ">"
       ' Print #X, "" & Space(Lp_Sangria) & "<Documento ID=" & Chr(34) & "ID" & Id & Chr(34) & ">"
        Print #X, "" & Space(Lp_Sangria) & "<Documento ID=" & Chr(34) & "ID" & Id & Chr(34) & ">"
        'Encabezado
        Print #X, "" & Space(Lp_Sangria) & "<Encabezado>"
            'id documento
            Print #X, "" & Space(Lp_Sangria * 2) & "<IdDoc>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<TipoDTE>" & Tipo & "</TipoDTE>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<Folio>" & Trim(Folio) & "</Folio>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<FchEmis>" & Fql(Fecha) & "</FchEmis>"
           '     Print #X, "" & Space(Lp_Sangria * 3) & "<FchVenc>" & Format(DtFecha.Value + CboPlazos.ItemData(CboPlazos.ListIndex), "; YYYY - MM - DD; ") & "</FchVenc>"
            Print #X, "" & Space(Lp_Sangria * 2) & "</IdDoc>"
            'Emisor

            
            Print #X, "" & Space(Lp_Sangria * 2) & " <Emisor>"
                'Print #X, "" & Space(Lp_Sangria * 3) & "<RUTEmisor>" & Replace(SP_Rut_Activo, ".", "") & "</RUTEmisor>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<RUTEmisor>" & Replace(SP_Rut_Activo, ".", "") & "</RUTEmisor>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<RznSoc>" & Rm_EmpresaEmisora!nombre_empresa & "</RznSoc>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<GiroEmis>" & Rm_EmpresaEmisora!giro & "</GiroEmis>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<Acteco>" & Rm_EmpresaEmisora!actividad & "</Acteco>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<DirOrigen>" & Rm_EmpresaEmisora!direccion & "</DirOrigen>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<CmnaOrigen>" & Rm_EmpresaEmisora!comuna & "</CmnaOrigen>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<CiudadOrigen>" & Rm_EmpresaEmisora!ciudad & "</CiudadOrigen>"
            Print #X, "" & Space(Lp_Sangria * 2) & "</Emisor>"
            
            'Receptor
            Sql = "SELECT nombre_rsocial, giro,direccion,comuna,ciudad " & _
                    "FROM maestro_clientes " & _
                    "WHERE rut_cliente='" & Rut & "'"
            Consulta RsTmp, Sql
            
            Print #X, "" & Space(Lp_Sangria * 2) & " <Receptor>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<RUTRecep>" & Replace(Rut, ".", "") & "</RUTRecep>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<RznSocRecep>" & Mid(Replace(RsTmp!nombre_rsocial, "�", "N"), 1, 100) & "</RznSocRecep>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<GiroRecep>" & Mid(Replace(RsTmp!giro, "�", "N"), 1, 40) & "</GiroRecep>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<Contacto></Contacto>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<DirRecep>" & Mid(Replace(RsTmp!direccion, "�", "N"), 1, 60) & "</DirRecep>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<CmnaRecep>" & Mid(Replace(RsTmp!comuna, "�", "N"), 1, 20) & "</CmnaRecep>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<CiudadRecep>" & Mid(Replace(RsTmp!ciudad, "�", "N"), 1, 20) & "</CiudadRecep>"
            Print #X, "" & Space(Lp_Sangria * 2) & "</Receptor>"
            
             'Totales
            Print #X, "" & Space(Lp_Sangria * 2) & " <Totales>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<MntNeto>0</MntNeto>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<MntExe>" & Exento & "</MntExe>"
                Print #X, "" & Space(Lp_Sangria * 3) & " <TasaIVA>19</TasaIVA>"
                Print #X, "" & Space(Lp_Sangria * 3) & " <IVA>0</IVA>"
                'Antes del monto total debemos verificar si la factura incluye impuestos retenidos
'                                         <ImptoReten>
'                                            <TipoImp>18</TipoImp>
'                                            <TasaImp>5</TasaImp>
'                                            <MontoImp>8887</MontoImp>
'                                        </ImptoReten>
                Print #X, "" & Space(Lp_Sangria * 3) & "<MntTotal>" & Total & "</MntTotal>"
            Print #X, "" & Space(Lp_Sangria * 2) & "</Totales>"
                
            'Cerramos encabezado
            Print #X, "" & Space(Lp_Sangria) & "</Encabezado>"
                
                
           'Comenzamos el detalle
            Sql = "SELECT id,codigo,marca,descripcion,precio_real,descuento,unidades,precio_final, " & _
                            "subtotal," & _
                            "ved_precio_venta_bruto,ved_precio_venta_neto, " & _
                            "ved_exento, ved_iva " & _
                        "FROM ven_detalle " & _
                        "WHERE rut_emp='" & SP_Rut_Activo & "' AND no_documento=" & NoDoc & " AND doc_id=" & DodId
           
            Consulta RsTmp, Sql
           
         
           'Recorremos la grilla con productos
           cont = 1
           If RsTmp.RecordCount > 0 Then
                
                Print #X, "" & Space(Lp_Sangria) & "<Detalle>"
                    Do While Not RsTmp.EOF
                        Print #X, "" & Space(Lp_Sangria * 2) & "<NroLinDet>" & cont & "</NroLinDet>"
                        Print #X, "" & Space(Lp_Sangria * 2) & " <CdgItem>"
                        Print #X, "" & Space(Lp_Sangria * 3) & "<TpoCodigo>PLU</TpoCodigo>"
                        Print #X, "" & Space(Lp_Sangria * 3) & "<VlrCodigo>" & RsTmp!Codigo & "</VlrCodigo>"
                        Print #X, "" & Space(Lp_Sangria * 2) & "</CdgItem>"
                        Print #X, "" & Space(Lp_Sangria * 2) & "<NmbItem>" & RsTmp!Descripcion & "</NmbItem>"
                        Print #X, "" & Space(Lp_Sangria * 2) & "<QtyItem>" & RsTmp!Unidades & "</QtyItem>"
                        Print #X, "" & Space(Lp_Sangria * 2) & "<PrcItem>" & RsTmp!precio_final & "</PrcItem>"
        '                    <DescuentoPct>15</DescuentoPct>
        '                    <DescuentoMonto>4212</DescuentoMonto>
        '                             <!- Solo se indica el c�digo del impuesto adicional '
        '                    <CodImpAdic>17</CodImpAdic>
                        Print #X, "" & Space(Lp_Sangria * 2) & "<MontoItem>" & RsTmp!SubTotal & "</MontoItem>"
                        cont = cont + 1
                        RsTmp.MoveNext
                    Loop
                Print #X, "" & Space(Lp_Sangria) & "</Detalle>"
                    
          End If

            'Cerramos detalle
            
                
        
                'CREAR REFERENCIA
                
            'CERRAR REFERNCIA
        

        Print #X, "" & Space(Lp_Sangria) & "</Documento>"
        Print #X, "" & Space(Lp_Sangria) & "</DTE>"
    Close #X
        
       
        Set obj = New DTECloud.Integracion
   
        obj.UrlServicioFacturacion = SG_Url_Factura_Electronica '"http://wsdtedesa.dyndns.biz/WSDTE/Service.asmx"
        obj.HabilitarDescarga = True
        obj.FormatoNombrePDF = "0"
        obj.CarpetaDestino = "C:\FACTURAELECTRONICA\PDF"
        obj.Password = "1234"
    
        obj.AsignarFolio = False
        obj.IncluirCopiaCesion = True
        
        'MSXML.DOMDocument
        Dim Documento As MSXML2.DOMDocument30
        Set Documento = New DOMDocument30
        
        Documento.preserveWhiteSpace = True
        Documento.Load (Sp_Xml)
        
        '  Dim oXMLAdicional As New xml.XMLDocument
        Dim documento2 As MSXML2.DOMDocument30
        Set documento2 = New DOMDocument30
        
        documento2.preserveWhiteSpace = True
        documento2.Load ("")
        obj.Productivo = True
        If obj.ProcesarXMLBasico(Documento.XML, documento2.XML) Then
             'MsgBox obj.URLPDF
           '  Archivo = "C:\FACTURAELECTRONICA\PDF\T" & Tipo & "F" & TxtNroDocumento & ".PDF"
      '       ShellExecute Me.Hwnd, "open", Archivo, "", "", 4
        Else
            MsgBox (CStr(obj.IDResultado) + " " + obj.DescripcionResultado)
        End If
        
        

        
End Sub


