VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Begin VB.Form venPosModificaNV 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Modifica Nota de Venta"
   ClientHeight    =   6495
   ClientLeft      =   4095
   ClientTop       =   1020
   ClientWidth     =   9345
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6495
   ScaleWidth      =   9345
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton cmdSAlir 
      Caption         =   "&Cancelar"
      Height          =   405
      Left            =   7140
      TabIndex        =   27
      Top             =   5940
      Width           =   1680
   End
   Begin VB.Frame FrmNV 
      Caption         =   "Nota de Venta Nro"
      Height          =   5655
      Left            =   570
      TabIndex        =   0
      Top             =   150
      Width           =   8235
      Begin VB.CommandButton CmdAceptar 
         Caption         =   "&Aceptar"
         Height          =   405
         Left            =   420
         TabIndex        =   26
         Top             =   5085
         Width           =   1680
      End
      Begin VB.Frame Frame1 
         Caption         =   "Documento Actual"
         Height          =   1185
         Left            =   390
         TabIndex        =   21
         Top             =   600
         Width           =   7350
         Begin VB.ComboBox CboDocInidicio 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            ItemData        =   "venPosModificaNV.frx":0000
            Left            =   3120
            List            =   "venPosModificaNV.frx":0002
            Style           =   2  'Dropdown List
            TabIndex        =   23
            ToolTipText     =   "Seleccione Documento de  venta. Ventas con Retenci�n, cuando el contribuyente recibe factura de terceros "
            Top             =   675
            Width           =   3285
         End
         Begin VB.TextBox TxtDocActual 
            Appearance      =   0  'Flat
            BackColor       =   &H00C0C0C0&
            BorderStyle     =   0  'None
            Height          =   285
            Left            =   3120
            TabIndex        =   22
            ToolTipText     =   "Ingrese el RUT sin puntos ni guion."
            Top             =   315
            Width           =   3225
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
            Height          =   300
            Index           =   0
            Left            =   765
            OleObjectBlob   =   "venPosModificaNV.frx":0004
            TabIndex        =   24
            Top             =   705
            Width           =   2250
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
            Height          =   300
            Index           =   2
            Left            =   780
            OleObjectBlob   =   "venPosModificaNV.frx":0084
            TabIndex        =   25
            Top             =   315
            Width           =   2250
         End
      End
      Begin VB.Frame Frame7 
         Caption         =   "Cliente"
         Height          =   2940
         Left            =   375
         TabIndex        =   1
         Top             =   1980
         Width           =   7380
         Begin VB.TextBox TxtCupoUtilizado 
            Height          =   285
            Left            =   6405
            TabIndex        =   11
            Text            =   "Text1"
            Top             =   2370
            Visible         =   0   'False
            Width           =   2010
         End
         Begin VB.TextBox TxtMontoCredito 
            Height          =   255
            Left            =   6420
            TabIndex        =   10
            Text            =   "Text1"
            Top             =   2040
            Visible         =   0   'False
            Width           =   1995
         End
         Begin VB.TextBox TxtEmail 
            Appearance      =   0  'Flat
            BackColor       =   &H00C0C0C0&
            BorderStyle     =   0  'None
            Height          =   285
            Left            =   1560
            TabIndex        =   9
            ToolTipText     =   "Ingrese el RUT sin puntos ni guion."
            Top             =   2445
            Width           =   3855
         End
         Begin VB.TextBox txtFono 
            Appearance      =   0  'Flat
            BackColor       =   &H00C0C0C0&
            BorderStyle     =   0  'None
            Height          =   285
            Left            =   1545
            TabIndex        =   8
            ToolTipText     =   "Ingrese el RUT sin puntos ni guion."
            Top             =   2115
            Width           =   2175
         End
         Begin VB.TextBox txtComuna 
            Appearance      =   0  'Flat
            BackColor       =   &H00C0C0C0&
            BorderStyle     =   0  'None
            Height          =   285
            Left            =   1560
            TabIndex        =   7
            ToolTipText     =   "Ingrese el RUT sin puntos ni guion."
            Top             =   1785
            Width           =   2160
         End
         Begin VB.TextBox TxtDireccion 
            Appearance      =   0  'Flat
            BackColor       =   &H00C0C0C0&
            BorderStyle     =   0  'None
            Height          =   285
            Left            =   1560
            TabIndex        =   6
            ToolTipText     =   "Ingrese el RUT sin puntos ni guion."
            Top             =   1455
            Width           =   5670
         End
         Begin VB.TextBox TxtGiro 
            Appearance      =   0  'Flat
            BackColor       =   &H00C0C0C0&
            BorderStyle     =   0  'None
            Height          =   285
            Left            =   1560
            TabIndex        =   5
            ToolTipText     =   "Ingrese el RUT sin puntos ni guion."
            Top             =   1125
            Width           =   5715
         End
         Begin VB.TextBox TxtRut 
            Appearance      =   0  'Flat
            BackColor       =   &H00C0C0C0&
            BorderStyle     =   0  'None
            Height          =   285
            Left            =   1560
            TabIndex        =   4
            ToolTipText     =   "Ingrese el RUT sin puntos ni guion."
            Top             =   465
            Width           =   1395
         End
         Begin VB.CommandButton CmdBuscaCliente 
            Caption         =   "F1 - Buscar"
            Height          =   255
            Left            =   2985
            TabIndex        =   3
            Top             =   465
            Width           =   1395
         End
         Begin VB.TextBox TxtRazonSocial 
            Appearance      =   0  'Flat
            BackColor       =   &H00C0C0C0&
            BorderStyle     =   0  'None
            Height          =   285
            Left            =   1560
            TabIndex        =   2
            ToolTipText     =   "Ingrese el RUT sin puntos ni guion."
            Top             =   795
            Width           =   5730
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkCupoCredito 
            Height          =   270
            Left            =   4890
            OleObjectBlob   =   "venPosModificaNV.frx":00FA
            TabIndex        =   12
            Top             =   1800
            Width           =   1020
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
            Height          =   240
            Index           =   0
            Left            =   1005
            OleObjectBlob   =   "venPosModificaNV.frx":015A
            TabIndex        =   13
            Top             =   495
            Width           =   495
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
            Height          =   210
            Index           =   2
            Left            =   840
            OleObjectBlob   =   "venPosModificaNV.frx":01C4
            TabIndex        =   14
            Top             =   840
            Width           =   660
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
            Height          =   255
            Index           =   1
            Left            =   435
            OleObjectBlob   =   "venPosModificaNV.frx":022E
            TabIndex        =   15
            Top             =   1470
            Width           =   1065
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel19 
            Height          =   255
            Index           =   0
            Left            =   645
            OleObjectBlob   =   "venPosModificaNV.frx":029E
            TabIndex        =   16
            Top             =   1815
            Width           =   855
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel18 
            Height          =   255
            Left            =   1080
            OleObjectBlob   =   "venPosModificaNV.frx":0308
            TabIndex        =   17
            Top             =   1155
            Width           =   420
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel19 
            Height          =   255
            Index           =   1
            Left            =   615
            OleObjectBlob   =   "venPosModificaNV.frx":036E
            TabIndex        =   18
            Top             =   2145
            Width           =   855
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel19 
            Height          =   255
            Index           =   2
            Left            =   195
            OleObjectBlob   =   "venPosModificaNV.frx":03D4
            TabIndex        =   19
            Top             =   2445
            Width           =   1320
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel19 
            Height          =   255
            Index           =   3
            Left            =   3810
            OleObjectBlob   =   "venPosModificaNV.frx":0456
            TabIndex        =   20
            Top             =   1800
            Width           =   1035
         End
      End
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   -15
      OleObjectBlob   =   "venPosModificaNV.frx":04CE
      Top             =   3795
   End
   Begin VB.Timer Timer1 
      Interval        =   30
      Left            =   0
      Top             =   0
   End
End
Attribute VB_Name = "venPosModificaNV"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub CmdAceptar_Click()
    If CboDocInidicio.ListIndex = -1 Then Exit Sub
    If Len(TxtRut) > 0 Then
            'modifica NV
          '  cn.Execute "UPDATE ven_doc_venta SET doc_id_indicio=" & Me.CboDocInidicio.ItemData(Me.CboDocInidicio.ListIndex) & ", " & _
                                                "rut_cliente='" & TxtRut & "',nombre_cliente='" & Me.TxtRazonSocial & "' " & _
                            "WHERE id=" & Me.FrmNV.Tag
                            
            cn.Execute "UPDATE ven_nota_venta SET doc_id=" & Me.CboDocInidicio.ItemData(Me.CboDocInidicio.ListIndex) & ",rut_cliente='" & TxtRut & "' " & _
                        "WHERE nve_id=" & Me.FrmNV.Tag
                        
            cn.Execute "UPDATE maestro_clientes SET nombre_rsocial='" & TxtRazonSocial & "',giro='" & TxtGiro & "',direccion='" & TxtDireccion & "',comuna='" & txtComuna & "' " & _
                            "WHERE rut_cliente='" & TxtRut & "'"
                        
                        
            Unload Me
    Else
        MsgBox "Seleccione Cliente...", vbInformation
    End If
End Sub

Private Sub CmdBuscaCliente_Click()
    LlamaClienteDe = "VD"
    
    ClienteEncontrado = False
    BuscaCliente.Show 1
    TxtRut = SG_codigo
    TxtRut.SetFocus
End Sub

Private Sub cmdSalir_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    Aplicar_skin Me
    Centrar Me, False
    LLenarCombo CboDocInidicio, "doc_nombre", "doc_id", "sis_documentos", "doc_documento='VENTA' AND doc_nota_de_venta='NO'  AND doc_utiliza_en_pos='SI'", "doc_orden"
End Sub

Private Sub Timer1_Timer()
    On Error Resume Next
    CboDocInidicio.SetFocus
    Timer1.Enabled = False
End Sub



Private Sub txtComuna_KeyPress(KeyAscii As Integer)
KeyAscii = Asc(UCase(Chr(KeyAscii)))
If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtDireccion_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
    If KeyAscii = 39 Then KeyAscii = 0
End Sub





Private Sub TxtGiro_KeyPress(KeyAscii As Integer)
KeyAscii = Asc(UCase(Chr(KeyAscii)))
If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtRazonSocial_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
    If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtRut_GotFocus()
    En_Foco TxtRut
End Sub

Private Sub TxtRut_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
    If KeyAscii = 13 Then SendKeys ("{TAB}")
    If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtRut_KeyUp(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF1 Then CmdBuscaCliente_Click
End Sub

Private Sub TxtRut_LostFocus()
    If Len(TxtRut.Text) = 0 Then
        Me.TxtRazonSocial.Text = ""
        Exit Sub
    End If
    
    
    If ClienteEncontrado Then
     
      
        
    Else
       ' TxtRut.SetFocus
    End If
End Sub

Private Sub TxtRut_Validate(Cancel As Boolean)
    TxtRazonSocial.Tag = ""
    If Len(TxtRut.Text) = 0 Then Exit Sub

    TxtRut.Text = Replace(TxtRut.Text, ".", "")
    TxtRut.Text = Replace(TxtRut.Text, "-", "")
    Respuesta = VerificaRut(TxtRut.Text, NuevoRut)
   
    If Respuesta = True Then
        Me.TxtRut.Text = NuevoRut
        'Buscar el cliente
        Sql = "SELECT rut_cliente,nombre_rsocial,giro,direccion,comuna,fono,email, " & _
               "cli_monto_credito " & _
              "FROM maestro_clientes m " & _
              "INNER JOIN par_asociacion_ruts  a ON m.rut_cliente=a.rut_cli " & _
              "LEFT JOIN par_lista_precios l ON m.lst_id=l.lst_id " & _
              "WHERE habilitado='SI' AND a.rut_emp='" & SP_Rut_Activo & "' AND rut_cliente = '" & Me.TxtRut.Text & "'"
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
                'Cliente encontrado
            With RsTmp
                TxtMontoCredito = !cli_monto_credito
         
                TxtRut.Text = !rut_cliente
                TxtGiro = !giro
                TxtDireccion = !direccion
                txtComuna = !comuna
                txtFono = !fono
                TxtEmail = !Email
                TxtRazonSocial.Text = !nombre_rsocial
               
                ClienteEncontrado = True
              
                
     
                
                If Val(TxtMontoCredito) > 0 Then
                    'Debemos consultar el saldo de credito disponible del cliente.
                    TxtCupoUtilizado = ConsultaSaldoCliente(TxtRut)
                    SkCupoCredito = NumFormat(Val(TxtMontoCredito) - Val(TxtCupoUtilizado))
                End If
                
                
                
                
            End With
                
            If bm_SoloVistaDoc Then Exit Sub
                                
        
        Else
                'Cliente no existe aun �crearlo??
                Respuesta = MsgBox("Cliente no encontrado..." & Chr(13) & _
                "     �Desea Crear?    ", vbYesNo + vbQuestion)
                If Respuesta = 6 Then
                    'crear
                    SG_codigo = ""
                    AccionCliente = 4
                    AgregoCliente.TxtRut.Text = Me.TxtRut.Text
                    
                    AgregoCliente.TxtRut.Locked = True
                    ClienteEncontrado = False
                    AgregoCliente.Timer1.Enabled = True
            
                    AgregoCliente.Show 1
                    If ClienteEncontrado = True Then
                        TxtRut_Validate False
                       
                        TxtCodigo.SetFocus
                    Else
                        TxtRut_Validate True
                    End If
                Else
                    'volver al rut
                    ClienteEncontrado = False
                    Me.TxtRut.Text = ""
                    On Error Resume Next
                    SendKeys "+{TAB}" ' Shift TAB retrocede al control anterior segun el orden del tabindex
                End If
        
          
        End If
       
    Else
        Me.TxtRut.Text = ""
        On Error Resume Next
        SendKeys "+{TAB}" ' Shift TAB retrocede al control anterior segun el orden del tabindex
    End If
    Exit Sub
CancelaImpesionFacturacion:
    'No imprime
    Unload Me
End Sub


