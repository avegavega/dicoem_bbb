VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form Mantenciones 
   Caption         =   "Consulta de Mantenciones"
   ClientHeight    =   8280
   ClientLeft      =   3285
   ClientTop       =   795
   ClientWidth     =   10845
   LinkTopic       =   "Form1"
   ScaleHeight     =   8280
   ScaleWidth      =   10845
   Begin VB.CommandButton Command3 
      Caption         =   "Nueva Mantencion"
      Height          =   405
      Left            =   8565
      TabIndex        =   29
      Top             =   480
      Width           =   1845
   End
   Begin VB.TextBox Text9 
      Height          =   300
      Left            =   1740
      TabIndex        =   26
      Top             =   4230
      Width           =   3690
   End
   Begin VB.TextBox Text8 
      Height          =   300
      Left            =   1740
      TabIndex        =   25
      Top             =   3885
      Width           =   3690
   End
   Begin VB.TextBox Text7 
      Height          =   300
      Left            =   1740
      TabIndex        =   24
      Top             =   3525
      Width           =   3690
   End
   Begin VB.Frame Frame1 
      Caption         =   "Consulta de Mantenciones"
      Height          =   7830
      Left            =   75
      TabIndex        =   3
      Top             =   105
      Width           =   10545
      Begin VB.TextBox Text1 
         Height          =   330
         Left            =   1155
         TabIndex        =   22
         Top             =   435
         Width           =   1905
      End
      Begin VB.CommandButton Command1 
         Caption         =   "Buscar"
         Height          =   480
         Left            =   3300
         TabIndex        =   21
         Top             =   375
         Width           =   1290
      End
      Begin VB.Frame FramF 
         Caption         =   "Rango de Fechas"
         Height          =   600
         Left            =   5295
         TabIndex        =   17
         ToolTipText     =   "Esta opcion ignora la fecha de emision"
         Top             =   225
         Width           =   2985
         Begin VB.CheckBox Chk 
            Height          =   195
            Left            =   105
            TabIndex        =   18
            Top             =   285
            Visible         =   0   'False
            Width           =   240
         End
         Begin MSComCtl2.DTPicker DtInicioC 
            CausesValidation=   0   'False
            Height          =   285
            Left            =   405
            TabIndex        =   19
            Top             =   240
            Width           =   1200
            _ExtentX        =   2117
            _ExtentY        =   503
            _Version        =   393216
            Format          =   94765057
            CurrentDate     =   42768
         End
         Begin MSComCtl2.DTPicker DtFinC 
            Height          =   285
            Left            =   1620
            TabIndex        =   20
            Top             =   240
            Width           =   1200
            _ExtentX        =   2117
            _ExtentY        =   503
            _Version        =   393216
            Format          =   94765057
            CurrentDate     =   42768
         End
      End
      Begin VB.Frame Frame2 
         Caption         =   "Listado de Mantenciones"
         Height          =   3555
         Left            =   135
         TabIndex        =   9
         Top             =   945
         Width           =   10200
         Begin VB.ListBox List1 
            Height          =   255
            ItemData        =   "Mantenciones.frx":0000
            Left            =   6840
            List            =   "Mantenciones.frx":000D
            TabIndex        =   28
            Top             =   2115
            Width           =   3135
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel7 
            Height          =   210
            Left            =   5430
            OleObjectBlob   =   "Mantenciones.frx":0032
            TabIndex        =   27
            Top             =   2160
            Width           =   615
         End
         Begin VB.TextBox Text2 
            Height          =   300
            Left            =   1530
            TabIndex        =   11
            Top             =   2085
            Width           =   3690
         End
         Begin VB.CommandButton Command2 
            Caption         =   "Cambiar Estado"
            Height          =   360
            Left            =   8280
            TabIndex        =   10
            Top             =   2520
            Width           =   1695
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel5 
            Height          =   240
            Left            =   810
            OleObjectBlob   =   "Mantenciones.frx":009C
            TabIndex        =   12
            Top             =   3240
            Width           =   645
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
            Height          =   225
            Left            =   915
            OleObjectBlob   =   "Mantenciones.frx":0108
            TabIndex        =   13
            Top             =   2910
            Width           =   525
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
            Height          =   225
            Left            =   600
            OleObjectBlob   =   "Mantenciones.frx":0170
            TabIndex        =   14
            Top             =   2535
            Width           =   855
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
            Height          =   300
            Left            =   45
            OleObjectBlob   =   "Mantenciones.frx":01E2
            TabIndex        =   15
            Top             =   2145
            Width           =   1935
         End
         Begin MSComctlLib.ListView LvMantenciones 
            Height          =   1725
            Left            =   165
            TabIndex        =   16
            Top             =   210
            Width           =   9810
            _ExtentX        =   17304
            _ExtentY        =   3043
            View            =   3
            LabelWrap       =   0   'False
            HideSelection   =   0   'False
            FullRowSelect   =   -1  'True
            GridLines       =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   6
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Object.Tag             =   "F1200"
               Text            =   "Numero Mantencion"
               Object.Width           =   2646
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Object.Tag             =   "T1100"
               Text            =   "Nombre Tecnico"
               Object.Width           =   5292
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   2
               Object.Tag             =   "T100"
               Text            =   "Fecha Inicio"
               Object.Width           =   2117
            EndProperty
            BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   3
               Object.Tag             =   "T100"
               Text            =   "Tiempo Estimado"
               Object.Width           =   2293
            EndProperty
            BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   4
               Text            =   "Estado"
               Object.Width           =   2117
            EndProperty
            BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   5
               Text            =   "Comentario"
               Object.Width           =   2540
            EndProperty
         End
      End
      Begin VB.Frame Frame3 
         Caption         =   "Listado de Insumos"
         Height          =   3180
         Left            =   150
         TabIndex        =   4
         Top             =   4575
         Width           =   10185
         Begin VB.TextBox Text6 
            Height          =   300
            Left            =   7800
            TabIndex        =   6
            Text            =   "$ 0"
            Top             =   2325
            Width           =   2115
         End
         Begin VB.CommandButton cmdSalir 
            Cancel          =   -1  'True
            Caption         =   "&Retornar"
            Height          =   405
            Left            =   8580
            TabIndex        =   5
            Top             =   2715
            Width           =   1350
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel6 
            Height          =   240
            Left            =   5940
            OleObjectBlob   =   "Mantenciones.frx":0262
            TabIndex        =   7
            Top             =   2400
            Width           =   1740
         End
         Begin MSComctlLib.ListView LvInsumos 
            Height          =   1890
            Left            =   105
            TabIndex        =   8
            Top             =   360
            Width           =   9810
            _ExtentX        =   17304
            _ExtentY        =   3334
            View            =   3
            LabelWrap       =   0   'False
            HideSelection   =   0   'False
            FullRowSelect   =   -1  'True
            GridLines       =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   4
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Object.Tag             =   "F1200"
               Text            =   "Codigo"
               Object.Width           =   2117
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Object.Tag             =   "T1100"
               Text            =   "Nombre Producto"
               Object.Width           =   7056
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   2
               Object.Tag             =   "T100"
               Text            =   "Estado"
               Object.Width           =   5292
            EndProperty
            BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   3
               Object.Tag             =   "T100"
               Text            =   "Monto"
               Object.Width           =   2646
            EndProperty
         End
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   285
         Left            =   300
         OleObjectBlob   =   "Mantenciones.frx":02EC
         TabIndex        =   23
         Top             =   525
         Width           =   765
      End
   End
   Begin VB.TextBox Text3 
      Height          =   300
      Left            =   1755
      TabIndex        =   2
      Top             =   3480
      Width           =   3690
   End
   Begin VB.TextBox Text4 
      Height          =   300
      Left            =   1755
      TabIndex        =   1
      Top             =   3840
      Width           =   3690
   End
   Begin VB.TextBox Text5 
      Height          =   300
      Left            =   1800
      TabIndex        =   0
      Top             =   4185
      Width           =   3690
   End
End
Attribute VB_Name = "Mantenciones"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub CmdSalir_Click()
    Unload Me
End Sub

Private Sub Command3_Click()
Ingreso_Mantenciones.Show 1
End Sub

Private Sub Form_Load()
    Centrar Me
    Aplicar_skin Me
End Sub

