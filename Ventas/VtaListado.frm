VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{28D47522-CF84-11D1-834C-00A0249F0C28}#1.0#0"; "gif89.dll"
Object = "{DA729E34-689F-49EA-A856-B57046630B73}#1.0#0"; "progressbar-xp.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{DE4917CD-D9B7-45E2-B0C7-DA1DB0A76109}#1.0#0"; "jhTextBoxM.ocx"
Begin VB.Form VtaListado 
   Caption         =   "Listado Ventas"
   ClientHeight    =   10140
   ClientLeft      =   5340
   ClientTop       =   345
   ClientWidth     =   15030
   LinkTopic       =   "Form1"
   ScaleHeight     =   10140
   ScaleWidth      =   15030
   Visible         =   0   'False
   Begin VB.CommandButton CmdXmL 
      Caption         =   "Obtener XML"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   630
      Left            =   10830
      TabIndex        =   73
      Top             =   8670
      Width           =   1935
   End
   Begin VB.Frame FrmLoad 
      Height          =   1680
      Left            =   5940
      TabIndex        =   56
      Top             =   5610
      Visible         =   0   'False
      Width           =   2805
      Begin GIF89LibCtl.Gif89a Gif89a1 
         Height          =   1275
         Left            =   210
         OleObjectBlob   =   "VtaListado.frx":0000
         TabIndex        =   57
         Top             =   285
         Width           =   2445
      End
   End
   Begin jhTextBoxM.TextBoxM TextBoxM1 
      Height          =   300
      Left            =   9270
      TabIndex        =   72
      Top             =   8655
      Visible         =   0   'False
      Width           =   525
      _ExtentX        =   926
      _ExtentY        =   529
      Text            =   ""
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   7.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Frame Frame8 
      Caption         =   "Caja"
      Height          =   675
      Left            =   11115
      TabIndex        =   70
      Top             =   1125
      Width           =   1035
      Begin VB.TextBox TxtCaja 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   75
         TabIndex        =   71
         Text            =   "0"
         Top             =   300
         Width           =   780
      End
   End
   Begin VB.CommandButton Command3 
      Caption         =   "Resumenes"
      Height          =   450
      Left            =   345
      TabIndex        =   69
      Top             =   7980
      Width           =   1035
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Visualizar PDF"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   630
      Left            =   8865
      TabIndex        =   59
      Top             =   8670
      Width           =   1935
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Consultar Acuse de Clientes"
      Height          =   285
      Left            =   5850
      TabIndex        =   58
      Top             =   9015
      Width           =   3000
   End
   Begin VB.CommandButton CmdCuotas 
      Caption         =   "Ct"
      Height          =   315
      Left            =   14745
      TabIndex        =   55
      Top             =   5340
      Visible         =   0   'False
      Width           =   330
   End
   Begin VB.CommandButton CmdCambiaSuc 
      Caption         =   "CS"
      Height          =   300
      Left            =   14715
      TabIndex        =   51
      Top             =   4290
      Width           =   345
   End
   Begin VB.Frame FrmCambiaSuc 
      Caption         =   "Cambia a Sucursal"
      Height          =   1035
      Left            =   11940
      TabIndex        =   50
      Top             =   3990
      Visible         =   0   'False
      Width           =   2790
      Begin VB.CommandButton CmdOkCambiaSuc 
         Caption         =   "Ok"
         Height          =   285
         Left            =   2205
         TabIndex        =   54
         Top             =   495
         Width           =   375
      End
      Begin VB.ComboBox CboCambiaSuc 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   75
         Style           =   2  'Dropdown List
         TabIndex        =   53
         Top             =   495
         Width           =   2130
      End
      Begin VB.CommandButton CmdCierrarSuc 
         Caption         =   "Command1"
         Height          =   195
         Left            =   2520
         TabIndex        =   52
         Top             =   120
         Width           =   150
      End
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
      Height          =   195
      Left            =   405
      OleObjectBlob   =   "VtaListado.frx":0086
      TabIndex        =   49
      Top             =   105
      Width           =   1440
   End
   Begin VB.ComboBox CboSucEmpresa 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   1890
      Style           =   2  'Dropdown List
      TabIndex        =   48
      Top             =   75
      Width           =   2130
   End
   Begin VB.CommandButton CmdCambiaNroDoc 
      Caption         =   "Cambiar Nro Documento"
      Height          =   315
      Left            =   5850
      TabIndex        =   46
      Top             =   8670
      Visible         =   0   'False
      Width           =   3000
   End
   Begin VB.TextBox Text1 
      Height          =   285
      Left            =   10380
      TabIndex        =   45
      Text            =   "Text1"
      Top             =   8685
      Visible         =   0   'False
      Width           =   3645
   End
   Begin VB.Frame Frame7 
      Caption         =   "Nro Doc"
      Height          =   675
      Left            =   9870
      TabIndex        =   43
      Top             =   1125
      Width           =   1320
      Begin VB.TextBox TxtNroDoc 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   75
         TabIndex        =   44
         Text            =   "0"
         Top             =   300
         Width           =   1080
      End
   End
   Begin VB.CommandButton CmdCompruebaDTE 
      Caption         =   "Dte"
      Height          =   195
      Left            =   300
      TabIndex        =   42
      Top             =   9945
      Width           =   1365
   End
   Begin VB.CommandButton CmdCambiaFPago 
      Caption         =   "Mod. F. Pago"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   12825
      TabIndex        =   41
      ToolTipText     =   "Permite Cambiar la Forma de Pago"
      Top             =   8670
      Width           =   1785
   End
   Begin VB.Frame Frame6 
      Caption         =   "Documento"
      Height          =   750
      Left            =   12180
      TabIndex        =   39
      Top             =   390
      Width           =   2355
      Begin VB.ComboBox CboDocVenta 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   105
         Style           =   2  'Dropdown List
         TabIndex        =   40
         Top             =   315
         Width           =   2130
      End
   End
   Begin VB.CommandButton CmdTodos 
      Caption         =   "Todos"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   13590
      TabIndex        =   38
      ToolTipText     =   "Muestra todas las compras"
      Top             =   1485
      Width           =   1095
   End
   Begin VB.CommandButton CmdBusca 
      Caption         =   "Buscar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   12495
      TabIndex        =   37
      ToolTipText     =   "Busca texto ingresado"
      Top             =   1485
      Width           =   1095
   End
   Begin VB.Timer Timer1 
      Interval        =   50
      Left            =   450
      Top             =   105
   End
   Begin VB.CommandButton CmdVtaConBoletas 
      Caption         =   "F4 - Ingreso de Boletas"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   2220
      TabIndex        =   36
      ToolTipText     =   "Ingreso de Boletas"
      Top             =   9450
      Width           =   2595
   End
   Begin VB.CheckBox ChkCotizacion 
      Caption         =   "Mostrar cotizaciones"
      Height          =   195
      Left            =   12315
      TabIndex        =   35
      Top             =   1200
      Width           =   1995
   End
   Begin VB.Frame FraContable 
      Caption         =   "Exclusivo para OF. Contable"
      Height          =   1380
      Left            =   1800
      TabIndex        =   33
      Top             =   8655
      Visible         =   0   'False
      Width           =   3450
      Begin VB.CommandButton cmdVentaSimple 
         Caption         =   "Ingreso de Facturas"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   525
         Left            =   420
         TabIndex        =   34
         Top             =   255
         Width           =   2595
      End
   End
   Begin VB.Frame Frame5 
      Caption         =   "Forma de Pago"
      Height          =   750
      Left            =   9855
      TabIndex        =   29
      Top             =   390
      Width           =   2355
      Begin VB.ComboBox CboFpago 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         ItemData        =   "VtaListado.frx":0104
         Left            =   105
         List            =   "VtaListado.frx":0106
         Style           =   2  'Dropdown List
         TabIndex        =   30
         Top             =   315
         Width           =   2130
      End
   End
   Begin VB.CommandButton cmdEntrega 
      Caption         =   "Modulo Entrega"
      Height          =   510
      Left            =   210
      TabIndex        =   8
      ToolTipText     =   "M�dulo entrega"
      Top             =   9360
      Width           =   1515
   End
   Begin VB.CommandButton cmdCambiaMercaderia 
      Caption         =   "Cambio Mercad."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   180
      TabIndex        =   27
      ToolTipText     =   "Cambio de Mercader�as"
      Top             =   8790
      Width           =   1530
   End
   Begin VB.Frame FraProgreso 
      Caption         =   "Progreso exportaci�n"
      Height          =   795
      Left            =   1830
      TabIndex        =   15
      Top             =   4275
      Visible         =   0   'False
      Width           =   11430
      Begin Proyecto2.XP_ProgressBar BarraProgreso 
         Height          =   330
         Left            =   150
         TabIndex        =   16
         Top             =   315
         Width           =   11130
         _ExtentX        =   19632
         _ExtentY        =   582
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BrushStyle      =   0
         Color           =   16777088
         Scrolling       =   1
         ShowText        =   -1  'True
      End
   End
   Begin VB.Frame frmOts 
      Caption         =   "Listado historico"
      Height          =   6735
      Left            =   135
      TabIndex        =   7
      Top             =   1845
      Width           =   14610
      Begin VB.Frame FrmResumenes 
         Caption         =   "Resumenes"
         Height          =   675
         Left            =   45
         TabIndex        =   60
         Top             =   6000
         Visible         =   0   'False
         Width           =   14475
         Begin VB.TextBox TxtTotalFacturas 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0FFC0&
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   2100
            Locked          =   -1  'True
            TabIndex        =   64
            Text            =   "0"
            Top             =   270
            Width           =   1215
         End
         Begin VB.TextBox txtBoletas 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0FFC0&
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   5925
            Locked          =   -1  'True
            TabIndex        =   63
            Text            =   "0"
            Top             =   255
            Width           =   1260
         End
         Begin VB.TextBox TxtGuias 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0FFC0&
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   9345
            Locked          =   -1  'True
            TabIndex        =   62
            Text            =   "0"
            Top             =   270
            Width           =   1335
         End
         Begin VB.TextBox TxtNc 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0FFC0&
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   12975
            Locked          =   -1  'True
            TabIndex        =   61
            Text            =   "0"
            Top             =   240
            Width           =   1335
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkFacturas 
            Height          =   255
            Left            =   60
            OleObjectBlob   =   "VtaListado.frx":0108
            TabIndex        =   65
            Top             =   330
            Width           =   1920
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkBoletas 
            Height          =   255
            Left            =   3870
            OleObjectBlob   =   "VtaListado.frx":0189
            TabIndex        =   66
            Top             =   315
            Width           =   1920
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkGuias 
            Height          =   255
            Left            =   7290
            OleObjectBlob   =   "VtaListado.frx":01EE
            TabIndex        =   67
            Top             =   330
            Width           =   1920
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkNc 
            Height          =   255
            Left            =   10980
            OleObjectBlob   =   "VtaListado.frx":024F
            TabIndex        =   68
            Top             =   315
            Width           =   1920
         End
      End
      Begin VB.CommandButton CmdXcien 
         Caption         =   "%"
         Height          =   390
         Left            =   12990
         TabIndex        =   47
         Top             =   5640
         Visible         =   0   'False
         Width           =   255
      End
      Begin VB.TextBox TxtXcienUtilidad 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   12015
         Locked          =   -1  'True
         TabIndex        =   32
         TabStop         =   0   'False
         Text            =   "0"
         ToolTipText     =   "% Promedio"
         Top             =   5655
         Visible         =   0   'False
         Width           =   960
      End
      Begin VB.TextBox TxtTotal 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFC0&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   9690
         Locked          =   -1  'True
         TabIndex        =   26
         Text            =   "0"
         Top             =   5655
         Width           =   1560
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkTotal 
         Height          =   255
         Left            =   8340
         OleObjectBlob   =   "VtaListado.frx":02AA
         TabIndex        =   25
         Top             =   5760
         Width           =   1185
      End
      Begin MSComctlLib.ListView LvDetalle 
         Height          =   5355
         Left            =   225
         TabIndex        =   28
         Top             =   255
         Width           =   14265
         _ExtentX        =   25162
         _ExtentY        =   9446
         View            =   3
         LabelWrap       =   0   'False
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   18
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "N109"
            Text            =   "Id Unico"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "F1000"
            Text            =   "Fecha"
            Object.Width           =   1852
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "T1000"
            Text            =   "Nombre Cliente"
            Object.Width           =   5821
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Object.Tag             =   "T2500"
            Text            =   "Tipo Documento"
            Object.Width           =   5292
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   4
            Object.Tag             =   "N109"
            Text            =   "Nro Doc"
            Object.Width           =   1940
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Object.Tag             =   "T1500"
            Text            =   "Movimiento"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   6
            Key             =   "total"
            Object.Tag             =   "N100"
            Text            =   "Total"
            Object.Width           =   2646
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   7
            Object.Tag             =   "T2500"
            Text            =   "Vendedor"
            Object.Width           =   4410
         EndProperty
         BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   8
            Object.Tag             =   "N100"
            Text            =   "Abonos"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   9
            Object.Tag             =   "N109"
            Text            =   "Boleta Final"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   10
            Object.Tag             =   "T100"
            Text            =   "Signo"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(12) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   11
            Object.Tag             =   "T1000"
            Text            =   "Forma de Pago"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(13) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   12
            Object.Tag             =   "N102"
            Text            =   "% Utilidad"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(14) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   13
            Object.Tag             =   "T500"
            Text            =   "Venta Simple"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(15) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   14
            Object.Tag             =   "T1000"
            Text            =   "OficinaContable"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(16) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   15
            Object.Tag             =   "T1000"
            Text            =   "SII"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(17) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   16
            Text            =   "Acuse Cliente"
            Object.Width           =   6174
         EndProperty
         BeginProperty ColumnHeader(18) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   17
            Object.Tag             =   "T1000"
            Text            =   "Hra Transaccion"
            Object.Width           =   2540
         EndProperty
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel6 
         Height          =   255
         Left            =   195
         OleObjectBlob   =   "VtaListado.frx":030B
         TabIndex        =   31
         Top             =   5640
         Width           =   8055
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Estado documento"
      Height          =   750
      Left            =   7095
      TabIndex        =   21
      Top             =   390
      Width           =   2745
      Begin VB.ComboBox CboPago 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   225
         Style           =   2  'Dropdown List
         TabIndex        =   24
         Top             =   315
         Width           =   2385
      End
   End
   Begin VB.Frame Frame3 
      Caption         =   "Vendedor"
      Height          =   660
      Left            =   7110
      TabIndex        =   22
      Top             =   1125
      Width           =   2730
      Begin VB.ComboBox CboVendedores 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   225
         Style           =   2  'Dropdown List
         TabIndex        =   23
         Top             =   240
         Width           =   2400
      End
   End
   Begin VB.CommandButton cmdExportar 
      Caption         =   "F3- Exportar a Excel"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   8865
      TabIndex        =   17
      ToolTipText     =   "Exportar"
      Top             =   9360
      Width           =   1950
   End
   Begin VB.Frame Frame4 
      Caption         =   "Mes y A�o"
      Height          =   1395
      Left            =   4200
      TabIndex        =   9
      Top             =   390
      Width           =   2970
      Begin VB.CheckBox ChkFecha 
         Height          =   225
         Left            =   210
         TabIndex        =   20
         Top             =   1035
         Width           =   225
      End
      Begin MSComCtl2.DTPicker DtDesde 
         CausesValidation=   0   'False
         Height          =   285
         Left            =   450
         TabIndex        =   18
         Top             =   1020
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   503
         _Version        =   393216
         Enabled         =   0   'False
         CalendarBackColor=   16777215
         CalendarForeColor=   -2147483640
         CalendarTitleBackColor=   8388608
         CalendarTitleForeColor=   16777088
         CalendarTrailingForeColor=   255
         Format          =   95485953
         CurrentDate     =   41001
      End
      Begin VB.ComboBox ComAno 
         Height          =   315
         Left            =   1710
         Style           =   2  'Dropdown List
         TabIndex        =   11
         Top             =   435
         Width           =   1215
      End
      Begin VB.ComboBox comMes 
         Height          =   315
         ItemData        =   "VtaListado.frx":03F5
         Left            =   135
         List            =   "VtaListado.frx":03F7
         Style           =   2  'Dropdown List
         TabIndex        =   10
         Top             =   450
         Width           =   1575
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
         Height          =   255
         Left            =   120
         OleObjectBlob   =   "VtaListado.frx":03F9
         TabIndex        =   12
         Top             =   255
         Width           =   375
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel5 
         Height          =   255
         Left            =   1725
         OleObjectBlob   =   "VtaListado.frx":045D
         TabIndex        =   13
         Top             =   255
         Width           =   375
      End
      Begin MSComCtl2.DTPicker DtHasta 
         Height          =   285
         Left            =   1680
         TabIndex        =   19
         Top             =   1020
         Width           =   1200
         _ExtentX        =   2117
         _ExtentY        =   503
         _Version        =   393216
         Enabled         =   0   'False
         Format          =   95485953
         CurrentDate     =   41001
      End
   End
   Begin VB.CommandButton CmdSalir 
      Cancel          =   -1  'True
      Caption         =   "Esc  -Retornar"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   12840
      TabIndex        =   6
      ToolTipText     =   "Salir"
      Top             =   9360
      Width           =   1770
   End
   Begin VB.CommandButton CmdSeleccionar 
      Caption         =   "F2 - Ver"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   7365
      TabIndex        =   5
      ToolTipText     =   "Visualizar Venta"
      Top             =   9360
      Width           =   1470
   End
   Begin VB.Frame Frame1 
      Caption         =   "Filtro"
      Height          =   1395
      Left            =   135
      TabIndex        =   2
      Top             =   390
      Width           =   4050
      Begin VB.TextBox TxtBusqueda 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   150
         TabIndex        =   3
         Top             =   525
         Width           =   3750
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   255
         Left            =   165
         OleObjectBlob   =   "VtaListado.frx":04C1
         TabIndex        =   4
         Top             =   285
         Width           =   2295
      End
   End
   Begin VB.CommandButton CmdNueva 
      Caption         =   "F1 - Nueva"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   5880
      TabIndex        =   1
      ToolTipText     =   "Nueva Venta"
      Top             =   9360
      Width           =   1500
   End
   Begin VB.CommandButton CmdEliminaDocumento 
      Caption         =   "Eliminar Documento "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   10830
      TabIndex        =   0
      ToolTipText     =   "Elimina Documento"
      Top             =   9360
      Width           =   1995
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   0
      OleObjectBlob   =   "VtaListado.frx":0526
      Top             =   8775
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkEmpresaActiva 
      Height          =   255
      Left            =   7440
      OleObjectBlob   =   "VtaListado.frx":075A
      TabIndex        =   14
      Top             =   30
      Width           =   6780
   End
End
Attribute VB_Name = "VtaListado"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim Sp_Condicion As String
Dim Sm_FiltroSucursal As String
Dim Sm_FiltroFpago As String
Dim Sm_CondicionHaving As String
Dim Sm_SoloNotasDeVentas As String
Dim Sm_FiltroCaja As String

Private Sub cmdMesAno_Click()
    CargaDatos
End Sub

Private Sub CboFpago_Click()
    CboFpago.Tag = ""
    If CboFpago.Text = "CREDITO" Then
        CboFpago.Tag = ">1"
    ElseIf CboFpago.Text = "TODOS" Then
        CboFpago.Tag = ""
    Else
        Sql = "SELECT pla_dias " & _
                "FROM par_medios_de_pago " & _
                "WHERE mpa_id=" & CboFpago.ItemData(CboFpago.ListIndex)
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
            CboFpago.Tag = "=" & RsTmp!pla_dias
        End If
    End If
End Sub

Private Sub ChkFecha_Click()
    On Error Resume Next
    If ChkFecha.Value = 1 Then
        comMes.Enabled = False
        ComAno.Enabled = False
        DtDesde.Enabled = True
        DtHasta.Enabled = True
        DtDesde.SetFocus
    Else
        comMes.SetFocus
        DtDesde.Enabled = False
        DtHasta.Enabled = False
        comMes.Enabled = True
        ComAno.Enabled = True
    End If
End Sub

Private Sub CmdBusca_Click()
    Dim Ip_IdPago As Integer, Ip_PlazoDias As Integer
    Dim Sp_SucEmp As String
    Sp_SucEmp = ""
    Sp_Condicion = ""
    If Len(TxtBusqueda) = 0 Then
        Sp_Condicion = " AND 1=1 "
    Else
        Sp_Condicion = " AND nombre_cliente LIKE '%" & TxtBusqueda & "%' "
    End If
    
    If Val(TxtNroDoc) > 0 Then
        '18 - Marzo _
        Se agrega busqueda por numero tambien.
        Sp_Condicion = Sp_Condicion & " AND v.no_documento=" & TxtNroDoc & " "
    End If
        
    
    If ChkFecha.Value = 1 Then
        Sp_Condicion = Sp_Condicion & " AND v.fecha BETWEEN '" & Fql(DtDesde.Value) & "' AND '" & Fql(DtHasta.Value) & "' "
    Else
        Sp_Condicion = Sp_Condicion & " AND MONTH(v.fecha)=" & comMes.ItemData(comMes.ListIndex) & " AND YEAR(v.fecha)=" & IIf(ComAno.ListCount = 0, Year(Date), ComAno.Text)
    End If
    Sm_FiltroCaja = ""
    If Val(TxtCaja) > 0 Then
        Sm_FiltroCaja = " AND caj_id=" & TxtCaja & " "
    End If
    
    FrmLoad.Visible = True
    DoEvents
    If CboFpago.ListCount > 0 Then
        If CboFpago.ItemData(CboFpago.ListIndex) = 0 Then
            Sm_FiltroFpago = ""
        Else
            'Crear filtro de forma de pago
            
            '01-05-2015
            'Se cambia este filtro, buscando en la tabla ven_doc_venta el campo ven_plazo, que son los dias
            'de plazo. 0=contado o transbank, 1=cheque, >1 = credito
            If CboFpago.Tag = ">1" Then
                Sm_FiltroFpago = " AND ven_plazo>1 "
            Else
                Sm_FiltroFpago = " AND ven_plazo" & CboFpago.Tag & " AND (SELECT p.mpa_id " & _
                                "FROM abo_tipos_de_pagos p " & _
                                "JOIN cta_abono_documentos l ON p.abo_id=l.abo_id " & _
                                "JOIN cta_abonos a ON l.abo_id=a.abo_id " & _
                                "WHERE a.abo_cli_pro='CLI' AND a.rut_emp='" & SP_Rut_Activo & "' AND l.id=v.id " & _
                                "LIMIT 1)=" & CboFpago.ItemData(CboFpago.ListIndex)
            End If
        End If
    Else
        Sm_FiltroFpago = ""
    End If
    
    
    
     If CboVendedores.ListIndex > -1 Then
        If CboVendedores.Text = "TODOS" Then
            Sp_Condicion = Sp_Condicion
        Else
            Sp_Condicion = Sp_Condicion & " AND ven_id=" & CboVendedores.ItemData(CboVendedores.ListIndex)
        End If
    End If
    
    'nuevo filtro por sucursales, ignorar otro
    If CboSucEmpresa.ListCount > 2 Then
        If CboSucEmpresa.ItemData(CboSucEmpresa.ListIndex) = 0 Then
            Sp_SucEmp = ""
        Else
            Sp_Condicion = Sp_Condicion & " AND sue_id=" & CboSucEmpresa.ItemData(CboSucEmpresa.ListIndex) & " "
        End If
        
        
    
    Else
    
        Sp_SucEmp = ""
    End If
    
    If CboPago.ListCount > 0 Then
        Ip_IdPago = CboPago.ItemData(CboPago.ListIndex)
        Sm_CondicionHaving = ""
        Select Case Ip_IdPago
            
            Case Is = 1
                'Sp_Condicion = Sp_Condicion
            Case Is = 2
                Sm_CondicionHaving = " HAVING abonos  < bruto"
            Case Is = 3
                Sm_CondicionHaving = " HAVING abonos = bruto "
        End Select
    End If
   
    CargaDatos
    FrmLoad.Visible = False
End Sub

Private Sub CmdCambiaFPago_Click()
                If LVDetalle.SelectedItem Is Nothing Then Exit Sub
                
                Sql = "SELECT rut_cliente " & _
                        "FROM ven_doc_venta " & _
                        "WHERE rut_emp='" & SP_Rut_Activo & "' AND id=" & LVDetalle.SelectedItem
                Consulta RsTmp, Sql
                If RsTmp.RecordCount = 0 Then Exit Sub

                Sql = " SELECT  v.id,   no_documento,doc_nombre,    v.rut_cliente,  IFNULL(cli_nombre_fantasia,nombre_rsocial)cliente, " & _
                        "IF(v.suc_id = 0,'CM',CONCAT(suc_ciudad,' - ',suc_direccion))sucursal,fecha, v.ven_fecha_vencimiento, " & _
                        "bruto,0 abonos, bruto saldo, bruto nuevo, v.suc_id, " & _
                        "(SELECT  a.abo_id " & _
                            "FROM            cta_abono_documentos t,             cta_abonos a " & _
                            "Where   a.rut_emp='" & SP_Rut_Activo & "'        AND t.abo_id = a.abo_id         AND a.abo_cli_pro = 'CLI'       AND t.id = v.id " & _
                        "limit 1) idabono " & _
                        "FROM ven_doc_venta v,maestro_clientes c,sis_documentos d,par_sucursales s " & _
                        "WHERE v.rut_emp = '" & SP_Rut_Activo & "' AND v.suc_id = s.suc_id AND v.rut_cliente = c.rut_cliente " & _
                        "AND v.doc_id = d.doc_id AND v.doc_id <> 11 AND v.rut_cliente = '" & RsTmp!rut_cliente & "' AND v.id IN(" & LVDetalle.SelectedItem & ") " & _
                        "ORDER BY id "
                
                 ctacte_GestionDePagos.Im_Fpago_Defecto = 1
          '      If Mid(CboPlazos.Text, 1, 7) = "CONTADO" Then ctacte_GestionDePagos.Im_Fpago_Defecto = 1
          '      If Mid(CboPlazos.Text, 1, 6) = "CHEQUE" Then ctacte_GestionDePagos.Im_Fpago_Defecto = 2
          '      If Mid(CboPlazos.Text, 1, 14) = "DEBITO/CREDITO" Then ctacte_GestionDePagos.Im_Fpago_Defecto = 8
                
         
                ctacte_GestionDePagos.Sm_Cli_Pro = "CLI"
                ctacte_GestionDePagos.Sm_NombreAbono = Me.LVDetalle.SelectedItem.SubItems(2) ' txtCliente
                ctacte_GestionDePagos.Sm_RutAbono = RsTmp!rut_cliente
                ctacte_GestionDePagos.Sm_RelacionMP = 0  'Right(CboPlazos.Text, 3)
                ctacte_GestionDePagos.CmdSalir.Visible = True
                ctacte_GestionDePagos.SkORIGEN = "CAMBIOFP"
                ctacte_GestionDePagos.txtSuperMensaje.Top = 6870
                ctacte_GestionDePagos.Show 1
End Sub

Private Sub cmdCambiaMercaderia_Click()
  '  VenlistadoCambiosMercaderias.Show 1
   ' Exit Sub
    ven_cambiomercaderia_listado.Show 1
End Sub

Private Sub CmdCambiaNroDoc_Click()
    Dim Vp_Nro As Variant
    Dim Lp_NroActual As Long
    Dim Ip_DocId As Integer
    If LVDetalle.SelectedItem Is Nothing Then Exit Sub
        
    If LVDetalle.SelectedItem.SubItems(15) = "SI" Then
        MsgBox "Esta opcion no aplica para documentos electronicos...", vbInformation
        Exit Sub
    End If
    
    Sql = "SELECT doc_id,no_documento " & _
            "FROM ven_doc_venta " & _
            "WHERE rut_emp='" & SP_Rut_Activo & "' AND id=" & LVDetalle.SelectedItem
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        Lp_NroActual = RsTmp!no_documento
        Ip_DocId = RsTmp!doc_id
        Vp_Nro = InputBox("Ingrese el Nro nuevo..", "Cambio de Nro a Doc.", RsTmp!no_documento)
        If Val(Vp_Nro) = Val(RsTmp!no_documento) Then
            MsgBox "El numero ingresado es el mismo que tenia..", vbInformation
            Exit Sub
        End If
        If Val(Vp_Nro) = 0 Then
            MsgBox "Nro no valido...", vbExclamation
            Exit Sub
        End If
        
        If Val(Vp_Nro) > 0 Then
            'Debemos comprobar si el nro nuevo ya existe
            Sql = "SELECT doc_id,no_documento " & _
                    "FROM ven_doc_venta " & _
                    "WHERE rut_emp='" & SP_Rut_Activo & "' AND no_documento=" & Val(Vp_Nro) & " AND doc_id=" & RsTmp!doc_id
            Consulta RsTmp, Sql
            If RsTmp.RecordCount > 0 Then
                MsgBox "El nro ingresado ya esta en uso...", vbExclamation
                Exit Sub
            
            Else
                'proceder a cambiar el nro.
                cn.Execute "UPDATE ven_doc_venta SET no_documento=" & Val(Vp_Nro) & " " & _
                "WHERE rut_emp='" & SP_Rut_Activo & "' AND no_documento=" & Lp_NroActual & " AND doc_id=" & Ip_DocId
            'proceder a cambiar el nro.
                cn.Execute "UPDATE ven_detalle SET no_documento=" & Val(Vp_Nro) & " " & _
                "WHERE rut_emp='" & SP_Rut_Activo & "' AND no_documento=" & Lp_NroActual & " AND doc_id=" & Ip_DocId
            
                CmdBusca_Click
            End If
            
        End If
        
       
    
    End If
    
    
    
End Sub

Private Sub CmdCambiaSuc_Click()
    FrmCambiaSuc.Visible = True
End Sub

Private Sub CmdCierrarSuc_Click()
    Me.FrmCambiaSuc.Visible = False
End Sub

Private Sub CmdCompruebaDTE_Click()
    For i = 1 To LVDetalle.ListItems.Count
        Sql = "SELECT doc_cod_sii,no_documento " & _
                "FROM ven_doc_venta " & _
                "JOIN sis_documentos USING(doc_id) " & _
                "WHERE id=" & LVDetalle.ListItems(i)
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
            If ConsultarDTESII(RsTmp!doc_cod_sii, RsTmp!no_documento) Then
                LVDetalle.ListItems(i).SubItems(15) = "OK"
            Else
                LVDetalle.ListItems(i).SubItems(15) = "NO"
            End If
            
        End If
    
    
    Next
End Sub

Private Sub CmdEliminaDocumento_Click()
    Dim s_Inventario As String, L_Unico As Long, s_mov As String
    Dim rs_Detalle As Recordset, Sp_Llave As String, Sp_Bodega As Integer, Lp_IdAbono As Long
    Dim s_FacturaGuias As String * 2, Sp_NCutilizada As String * 2
    Dim Ip_Doc_Doc_Sii As Integer, Lm_Nro_Documento_sii As Long
  '  If Principal.CmdMenu(8).Visible = True Then
  '      If ConsultaCentralizado("2,10", IG_Mes_Contable, IG_Ano_Contable) Then
  '          MsgBox "periodo contable ya ha sido centralizado, " & vbNewLine & "No puede modificar"
  '          Exit Sub
  '      End If
  '  End If
    Sp_NCutilizada = "NO"
    Dim Sp_Codigos As String
    If LVDetalle.SelectedItem Is Nothing Then Exit Sub
    L_Unico = LVDetalle.SelectedItem.Text
    SG_codigo2 = Empty
    
    If LVDetalle.SelectedItem.SubItems(3) = "BOLETA FISCAL " Then
        If SP_Rut_Activo = "12.072.366-9" Then
            FrmLoad.Visible = False
            MsgBox "Solo se puede anular esta boleta con Nota de Credito...", vbInformation
            Exit Sub
        End If
    End If
    
    Sql = "SELECT nro_factura,doc_id_factura " & _
            "FROM ven_doc_venta " & _
            "WHERE id=" & L_Unico & " AND nro_factura>0 "
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        FrmLoad.Visible = False
        MsgBox "Es una guia facturada..." & vbNewLine & " Primero elimine la factura...", vbInformation
        Exit Sub
    End If
    
    
    
    '
    Sql = "SELECT no_documento,doc_dte,doc_cod_sii,no_documento,v.doc_id,doc_nombre,fecha " & _
            "FROM ven_doc_venta v " & _
            "join sis_documentos d using(doc_id) " & _
            "WHERE id=" & L_Unico
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        If RsTmp!doc_dte = "SI" Then
        
            If SG_Oficina_Contable = "SI" Then
                If MsgBox("El documento es electronico..., si es emitido en este sistema no debiera eliminarse..." & vbNewLine & "Desea continuar con la eliminaci�n...", vbOKCancel + vbQuestion) = vbCancel Then Exit Sub
            Else
                Ip_Doc_Doc_Sii = RsTmp!doc_cod_sii
                Lm_Nro_Documento_sii = RsTmp!no_documento
                
                
                If RsTmp!doc_nombre = "BOLETA ELECTRONICA" Then
                    If RsTmp!Fecha <> Date Then
                        MsgBox "No se puede anular boleta de dias anterios, debe emitir NC...", vbExclamation
                        Exit Sub
                    End If
                    
                Else
                    
                    
                    If ConsultarDTESII(RsTmp!doc_cod_sii, RsTmp!no_documento) Then
                        FrmLoad.Visible = False
                        
                        MsgBox "Documento electronico no puede ser eliminado, emitir NCo ND segun corresponda            ..." & vbNewLine & "No es posible eliminar...", vbInformation
                        
                        
                        If MsgBox("Puede que el documento no se encuentre en el SII, aun desea eliminar reste registro del sistema?", vbQuestion + vbOKCancel) = vbCancel Then Exit Sub
                        FrmLoad.Visible = True
    
                      '  Exit Sub
                    Else
                        'Permite eliminar por que no encontr� en el ssii
                    
                    End If
                End If
                
            End If
        End If
    End If
    
    
    
    '
    Sql = "SELECT id_ref " & _
            "FROM ven_doc_venta " & _
            "WHERE id_ref=" & L_Unico
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        FrmLoad.Visible = False
        MsgBox "Documento tiene N.Credito asociada..." & vbNewLine & "No es posible eliminar...", vbInformation
        Exit Sub
    End If
    
    
   ' Sql = "SELECT id_ref " & _
            "FROM ven_doc_venta " & _
  '          "WHERE id_ref=" & L_Unico
'    Consulta RsTmp, Sql
'    If RsTmp.RecordCount > 0 Then
'        MsgBox "Documento tiene N.Credito asociada..." & vbNewLine & "No es posible eliminar...", vbInformation
'        Exit Sub
'    End If
    FrmLoad.Visible = False
    sis_InputBox.FramBox = "Ingrese llave para anulacion"
    sis_InputBox.Show 1
    Sp_Llave = UCase(SG_codigo2)
    If Len(Sp_Llave) = 0 Then Exit Sub
    FrmLoad.Visible = True
    
    
    
    
    Sql = "SELECT usu_id,usu_nombre,usu_login " & _
          "FROM sis_usuarios " & _
          "WHERE usu_pwd = MD5('" & Sp_Llave & "')"
    Consulta RsTmp3, Sql
    
    
    '1ro Extraeremos el abo_id en el caso que exista 21-01-2012
    Sql = "SELECT abo_id " & _
          "FROM cta_abono_documentos d " & _
          "INNER JOIN cta_abonos a USING(abo_id) " & _
          "WHERE abo_cli_pro='CLI' AND a.rut_emp ='" & SP_Rut_Activo & "' AND d.rut_emp ='" & SP_Rut_Activo & "' AND id=" & L_Unico
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
                '2DO SABREMOS QUE TIPO DE ABONO FUE,
            Lp_IdAbono = RsTmp!abo_id
            Sql = "SELECT abo_id,abo_fecha,mpa_nombre,usu_nombre,a.mpa_id,abo_fecha_pago " & _
                  "FROM cta_abonos a " & _
                  "INNER JOIN par_medios_de_pago m USING(mpa_id) " & _
                  "INNER JOIN cta_abono_documentos d USING(abo_id) " & _
                  "WHERE  abo_cli_pro='CLI' AND a.rut_emp='" & SP_Rut_Activo & "' AND d.rut_emp='" & SP_Rut_Activo & "' AND a.abo_id=" & Lp_IdAbono
            Consulta RsTmp, Sql
            If RsTmp.RecordCount > 0 Then
                If RsTmp.RecordCount > 1 Then
                    'Ojo que el abono de este documento fue junto con varios documento
                    'por lo tanto no debemos permitir eliminar este documento
                    MsgBox "No es posible eliminar este documento..." & vbNewLine & "Es parte de un abono de multiples documentos " & _
                    vbNewLine & "Cant. Documentos " & RsTmp.RecordCount & " Fecha Abono " & Format(RsTmp!abo_fecha_pago, "DD-MM-YYYY") & " Usuario:" & RsTmp!usu_nombre & " Medio de pago:" & RsTmp!mpa_nombre, vbExclamation
                    Exit Sub
                End If
                If RsTmp.RecordCount = 1 Then
                    If RsTmp!mpa_id = 5 Then
                        '19 Agosto de 2013
                        'aqui debieremos pasar no mas por q el abono es de una guia
                    ElseIf RsTmp!mpa_id <> 1 Then
                        MsgBox "No es posible eliminar este documento..." & vbNewLine & "Ya se realiz� abono ... " & _
                        vbNewLine & " Fecha Abono " & Format(RsTmp!abo_fecha_pago, "DD-MM-YYYY") & " Usuario:" & RsTmp!usu_nombre & " Medio de pago:" & RsTmp!mpa_nombre, vbExclamation
                        Exit Sub
                    Else
                        'El abono es solo a un documento y en contado(efectivo) por lo que no es problema eliminarlo
                    End If
                End If
            
            
            End If
            
            If MsgBox("El documento tiene un abono registrado ..." & vbNewLine & "Nro Comprobante XX " & vbNewLine & "�Continuar?", vbQuestion + vbYesNo) = vbNo Then Exit Sub
            
    Else
        'El documento es al credito pero no tiene abonos, es posible continuar, no afectara nada
    
    End If
    
    
    
    If RsTmp3.RecordCount > 0 Then
        X = InputBox("Usuario:" & RsTmp3!usu_nombre & vbNewLine & "Ingrese motivo", "Motivo de eliminaci�n")
        If Len(X) = 0 Then
            MsgBox "No ingreso motivo, no fue eliminado el documento"
            Exit Sub
        End If
        
        With LVDetalle.SelectedItem
             Sql = "INSERT INTO sis_eliminacion_documentos (id,eli_fecha,eli_motivo,usu_login,eli_ven_com) VALUES (" & _
                   LVDetalle.SelectedItem & ",'" & Fql(Date) & "','" & UCase(X) & "','" & RsTmp3!usu_login & "','VEN')"
            
            'Sql = "INSERT INTO com_historial_eliminaciones (doc_id,no_documento,rut_proveedor,eli_fecha,eli_motivo,usu_id) VALUES (" & _
                        .SubItems(7) & "," & .SubItems(4) & ",'" & .SubItems(8) & "','" & Format(Now, "YYYY-DD-MM HH:MM:SS") & "','" & X & "'," & RsTmp3!usu_id & ")"
            
        End With
            
    Else
        MsgBox "Contrase�a ingresada no fue encontrada...", vbInformation
        Exit Sub
    End If
    FrmLoad.Visible = True
    DoEvents
    
    On Error GoTo ErrorElimina
    cn.BeginTrans 'COMENZAMOS LA TRANSACCION
    cn.Execute Sql
    
    Sql = "DELETE FROM cta_abonos " & _
          "WHERE abo_cli_pro='CLI' AND rut_emp='" & SP_Rut_Activo & "' AND abo_id=" & Lp_IdAbono
    cn.Execute Sql
    Sql = "DELETE FROM cta_abono_documentos " & _
          "WHERE rut_emp='" & SP_Rut_Activo & "' AND abo_id=" & Lp_IdAbono
    cn.Execute Sql
    
    'Tambien eliminaremos los chueque que este en cartera
    '29-11-2014
    cn.Execute "DELETE FROM abo_cheques " & _
                "WHERE rut_emp='" & SP_Rut_Activo & "' AND abo_id=" & Lp_IdAbono
    
    
    Sql = "SELECT rut_cliente rut,nombre_cliente nombre_proveedor,doc_movimiento inventario," & _
                 "no_documento,c.doc_id,doc_nombre,bod_id,doc_factura_guias,ven_nc_utilizada,id," & _
                 "ven_boleta_hasta,rut_emp,sue_id " & _
          "FROM ven_doc_venta c,sis_documentos d " & _
          "WHERE d.doc_id=c.doc_id AND id=" & L_Unico
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        Sp_Bodega = RsTmp!bod_id
        s_FacturaGuias = RsTmp!doc_factura_guias
        If RsTmp!Inventario = "ENTRADA" Then s_mov = "SALIDA"
        If RsTmp!Inventario = "SALIDA" Then s_mov = "ENTRADA"
        
        If s_mov = "SALIDA" Or s_mov = "ENTRADA" Then
            'Debe ser entrada o Salida
            'si es NN no pasa por aqui
            Sql = "SELECT d.codigo pro_codigo,unidades cantidad," & _
                  "IFNULL((SELECT pro_precio_neto*d.unidades " & _
                   "FROM inv_kardex k " & _
                   "WHERE k.rut_emp='" & SP_Rut_Activo & "' AND k.pro_codigo=d.codigo ORDER BY kar_id DESC LIMIT 1),0) total_linea " & _
                  "FROM ven_detalle d " & _
                  "INNER JOIN maestro_productos m ON d.codigo=m.codigo " & _
                  "WHERE m.rut_emp='" & SP_Rut_Activo & "' AND  pro_inventariable='SI' AND d.rut_emp='" & SP_Rut_Activo & "' AND no_documento=" & RsTmp!no_documento & " AND doc_id=" & RsTmp!doc_id
            Consulta rs_Detalle, Sql
            If rs_Detalle.RecordCount > 0 Then
                rs_Detalle.MoveFirst
                Do While Not rs_Detalle.EOF
                    Kardex Format(Date, "YYYY-MM-DD"), s_mov, RsTmp!doc_id, RsTmp!no_documento, Sp_Bodega, _
                        rs_Detalle!pro_codigo, rs_Detalle!cantidad, "ELIMINA " & RsTmp!doc_nombre & " Nro:" & RsTmp!no_documento, Round(rs_Detalle!total_linea / rs_Detalle!cantidad), _
                        rs_Detalle!total_linea, RsTmp!Rut, RsTmp!nombre_proveedor, "NO", Round(rs_Detalle!total_linea / rs_Detalle!cantidad), , , , , 0
                        
                    rs_Detalle.MoveNext
                Loop
            End If
        End If
        Sp_NCutilizada = RsTmp!ven_nc_utilizada
        
        'DEBEMOS CONSULTAR SI EXISTEN DOCUMENTOS QUE HACEN REFERENCIA AL QUE SE ESTA INGRESANDO
        Sql = "UPDATE ven_doc_venta " & _
                     "SET id_ref=0,doc_id_factura=0,nro_factura=0 " & _
              "WHERE id_ref=" & L_Unico
        cn.Execute Sql
        
                
        'DEBEMOS CONSULTAR SI EXISTEN DOCUMENTOS FACTURADOS CON ESTE DOCUMENTO PARA LIBERARLOS Abril 2012
        If s_FacturaGuias = "SI" Then
            Sql = "UPDATE ven_doc_venta " & _
                     "SET id_ref=0,doc_id_factura=0,nro_factura=0 " & _
                  "WHERE nro_factura=" & RsTmp!no_documento & " AND doc_id_factura=" & RsTmp!doc_id & " " & _
                       "AND rut_emp='" & SP_Rut_Activo & "'"
            cn.Execute Sql
        End If
                
                
                
                
        If RsTmp!Inventario = "NN" Then
            '28 Abril 2011
            'No modifica inventario
            'por lo tanto podriamos eliminar el documeneto sin tocar el
            'kardex ni el stock
        End If
        'multidelete
        
        If SP_Control_Inventario = "SI" Then
            'Antes de eliminar los registros
            'Debemos saber si hay algun activo inmovilizado
            Sql = "SELECT aim_id " & _
                    "FROM ven_detalle d " & _
                    "JOIN ven_doc_venta v ON (v.no_documento=d.no_documento AND v.doc_id=d.doc_id) " & _
                    "WHERE aim_id>0 AND v.rut_emp='" & SP_Rut_Activo & "' AND  v.id=" & L_Unico
            Consulta RsTmp, Sql
            If RsTmp.RecordCount > 0 Then
                RsTmp.MoveFirst
                Do While Not RsTmp.EOF
                    cn.Execute "UPDATE con_activo_inmovilizado " & _
                                    "SET aim_fecha_egreso=Null,aim_habilitado='SI' " & _
                                    "WHERE aim_id=" & RsTmp!aim_id
                    RsTmp.MoveNext
                Loop
            End If
        
        
            Sql = "DELETE FROM " & _
                  "ven_doc_venta, " & _
                  "ven_detalle " & _
                  "USING ven_doc_venta " & _
                  "LEFT JOIN ven_detalle ON (ven_doc_venta.no_documento=ven_detalle.no_documento AND ven_doc_venta.doc_id=ven_detalle.doc_id) " & _
                  "WHERE ven_doc_venta.rut_emp='" & SP_Rut_Activo & "' AND  ven_doc_venta.id=" & L_Unico
            'Consulta RsTmp, Sql
        Else
            'Antes de eliminar los registros
            'Debemos saber si hay algun activo inmovilizado
            Sql = "SELECT aim_id " & _
                    "FROM ven_sin_detalle " & _
                    "WHERE aim_id>0 AND id=" & L_Unico
            Consulta RsTmp, Sql
            If RsTmp.RecordCount > 0 Then
                RsTmp.MoveFirst
                Do While Not RsTmp.EOF
                    cn.Execute "UPDATE con_activo_inmovilizado " & _
                                    "SET aim_fecha_egreso=Null,aim_habilitado='SI' " & _
                                    "WHERE aim_id=" & RsTmp!aim_id
                    RsTmp.MoveNext
                Loop
            End If
            
        
        
            Sql = "DELETE FROM " & _
                  "ven_doc_venta, " & _
                  "ven_sin_detalle " & _
                  "USING ven_doc_venta " & _
                  "LEFT JOIN ven_sin_detalle USING(id) " & _
                  "WHERE ven_doc_venta.id=" & L_Unico
            'Consulta RsTmp, Sql
        End If
        cn.Execute Sql
    End If
    If Sp_NCutilizada = "SI" Then
        EliminaAbonosDeNC "CLI", LVDetalle.SelectedItem '16
    End If
    
    If Val(Ip_Doc_Doc_Sii) > 0 Then
        cn.Execute "UPDATE dte_folios SET dte_disponible='SI' " & _
                "WHERE doc_id=" & Ip_Doc_Doc_Sii & " AND rut_emp='" & SP_Rut_Activo & "' AND dte_folio=" & Lm_Nro_Documento_sii
        If Val(Ip_Doc_Doc_Sii) = 39 Then
            cn.Execute Sql
        End If
       ' cn.Execute Sql
    End If
    
    
     '28 Sep 2013
        'DEBEMOS DETECTAR SI LA NOTA DE CREDITO SE UTILIZO PARA ABONAR A SU DOCUMENTO ORIGINAL
   ' If Sp_NCutilizada = "SI" Then
   '     'con esto sabemos q se trata de una nota de credito, y podemos referenciar a la tabla de abonos
   '     Sql = "DELETE FROM cta_abonos " & _
   '             "WHERE abo_cli_pro='CLI' AND rut_emp='" & SP_Rut_Activo & "' AND id_ref=" & RsTmp!ID
   '     cn.Execute Sql
   ' End If
    
    cn.CommitTrans
    CargaDatos
    FrmLoad.Visible = False
    Exit Sub
ErrorElimina:
    FrmLoad.Visible = False
                

    MsgBox "Ocurrio un error al eliminar documento", vbExclamation
    cn.RollbackTrans
End Sub

Private Sub cmdEntrega_Click()
    ven_modulo_entrega.Show 1
End Sub

Private Sub CmdExportar_Click()
    Dim tit(2) As String
    If LVDetalle.ListItems.Count = 0 Then Exit Sub
    FraProgreso.Visible = True
    tit(0) = Me.Caption & " " & DtFecha & " - " & Time
    tit(1) = ""
    tit(2) = ""
    ExportarNuevo LVDetalle, tit, Me, BarraProgreso
    FraProgreso.Visible = False
End Sub

Private Sub CmdNueva_Click()
    On Error GoTo fina
    DG_ID_Unico = 0
    
   ' If Principal.CmdMenu(8).Visible = True Then
   '     If ConsultaCentralizado("2,10", IG_Mes_Contable, IG_Ano_Contable) Then
   '         MsgBox "periodo contable ya ha sido centralizado, " & vbNewLine & "No puede modificar"
   '         Exit Sub
   '     End If
   ' End If
    
    'Consultar el estado de la caja
    Sql = "SELECT caj_estado " & _
            "FROM ven_caja " & _
            "WHERE caj_id=" & LG_id_Caja
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        If RsTmp!caj_estado = "CERRADA" Then
            MsgBox "La caja ya esta cerrada..." & _
                    "El programa finalizar�...", vbInformation
            End
        End If
    End If
            
 '   If SP_Rut_Activo = "7.242.181-7" Or SP_Rut_Activo = "76.610.750-8" Or SP_Rut_Activo = "77.416.270-4" Then
 '       VtaDirectaEspumas.Tag = "NUEVA"
 '       VtaDirectaEspumas.Show 1
 '       CargaDatos
 '       Exit Sub
 '   Else
        
        VtaDirecta.Tag = "NUEVA"
        VtaDirecta.Show 1
        CargaDatos
   '     Exit Sub
  '  End If
fina:
End Sub

Private Sub CmdOkCambiaSuc_Click()
    If LVDetalle.SelectedItem Is Nothing Then Exit Sub
    If MsgBox("Cambiar " & LVDetalle.SelectedItem.SubItems(3) & " " & LVDetalle.SelectedItem.SubItems(4) & vbNewLine & " a SUC:" & Me.CboCambiaSuc.Text & " ...?", vbQuestion + vbOKCancel) = vbNo Then Exit Sub
    
    Sql = "UPDATE ven_doc_venta SET sue_id=" & Me.CboCambiaSuc.ItemData(Me.CboCambiaSuc.ListIndex) & " " & _
    "WHERE id=" & LVDetalle.SelectedItem
    cn.Execute Sql
    
    Me.FrmCambiaSuc.Visible = False
    CmdBusca_Click
End Sub

Private Sub CmdSAlir_Click()
    On Error Resume Next
    Unload Me
End Sub

Private Sub CmdSeleccionar_Click()
    If LVDetalle.SelectedItem Is Nothing Then Exit Sub
    If LVDetalle.SelectedItem.SubItems(5) = "NULO" Then
        MsgBox "Documento NULO, no se puede editar...", vbInformation
        Exit Sub
    End If
    DG_ID_Unico = LVDetalle.SelectedItem.Text
    
    If LVDetalle.SelectedItem.SubItems(13) = "SI" And LVDetalle.SelectedItem.SubItems(14) = "NO" Then
       'Se trata de ingreso de boletas
        VtaBoletas.Show 1
        
        
    Else
    
        If LVDetalle.SelectedItem.SubItems(13) = "SI" And LVDetalle.SelectedItem.SubItems(14) = "SI" Then
            vta_OficinaContable.Show 1
        Else
           ' If SP_Rut_Activo = "7.242.181-7" Or SP_Rut_Activo = "76.610.750-8" Or SP_Rut_Activo = "77.416.270-4" Then
           '     VtaDirectaEspumas.Tag = "VIEJA"
           '     VtaDirectaEspumas.Show 1
           ' Else
            
                VtaDirecta.Tag = "VIEJA"
                VtaDirecta.Show 1
           ' End If
        End If
    End If
    CargaDatos
End Sub

Private Sub CmdTodos_Click()
    Sp_Condicion = ""
    CargaDatos
End Sub

Private Sub cmdVentaSimple_Click()
    vta_OficinaContable.Show 1
    CargaDatos
End Sub

Private Sub CmdVtaConBoletas_Click()


  '  If Principal.CmdMenu(8).Visible = True Then
  '      If ConsultaCentralizado("2,10", IG_Mes_Contable, IG_Ano_Contable) Then
  '          MsgBox "periodo contable ya ha sido centralizado, " & vbNewLine & "No puede modificar"
  '          Exit Sub
  '      End If
  '  End If
    
    
    DG_ID_Unico = 0
    VtaBoletas.Show 1
    CargaDatos
End Sub

Private Sub CmdXcien_Click()
    XcienTotal
End Sub




Private Sub CmdXmL_Click()
FrmLoad.Visible = True
    DoEvents
    Sql = "SELECT doc_cod_sii,no_documento " & _
                       "FROM ven_doc_venta " & _
                       "JOIN sis_documentos USING(doc_id) " & _
                       "WHERE id=" & LVDetalle.SelectedItem
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
         If RsTmp!doc_cod_sii = 33 Or RsTmp!doc_cod_sii = 34 Or RsTmp!doc_cod_sii = 52 Or RsTmp!doc_cod_sii = 61 Then
     '   Sp_cod_SII = RsTmp!doc_cod_sii
     '   Sp_Folio = RsTmp!no_documento
         Else
            FrmLoad.Visible = False
             Exit Sub
         End If
    End If
    If RsTmp.RecordCount > 0 Then
        Dim VerCedible As Boolean
        VerCedible = False
       ' Archivo = "C:\FACTURAELECTRONICA\PDF\" & Replace(SP_Rut_Activo, ".", "") & "\T" & Trim(Str(RsTmp!doc_cod_sii)) & Trim("F" & RsTmp!no_documento) & ".PDF"
        dte_MostrarPDF.XML Str(RsTmp!doc_cod_sii), RsTmp!no_documento
    End If
    FrmLoad.Visible = False     '   Sp_Folio = RsTmp!no_documento

End Sub

Private Sub Command1_Click()
        Dim Sp_Archivo2 As String
        Dim Sp_cod_SII As String
        Dim Sp_Folio As String
        Dim Sp_Resultado As String
        FrmLoad.Visible = True
        Dim resp As Boolean
        DoEvents
        'Consulta electronica
        Dim obj As DTECloud.Integracion
        Set obj = New DTECloud.Integracion
        Dim X As Integer
        '
        On Error GoTo ErrorConexion
          obj.UrlServicioFacturacion = SG_Url_Factura_Electronica ' "http://wsdte.dyndns.biz/WSDTE/Service.asmx?WSDL"
               'obj.HabilitarDescarga = True
        For i = 1 To LVDetalle.ListItems.Count
               FrmLoad.Visible = True
               DoEvents
               Sp_Resultado = ""
               Sql = "SELECT doc_cod_sii,no_documento " & _
                       "FROM ven_doc_venta " & _
                       "JOIN sis_documentos USING(doc_id) " & _
                       "WHERE id=" & LVDetalle.ListItems(i)
               Consulta RsTmp, Sql
               If RsTmp.RecordCount > 0 Then
                   Sp_cod_SII = RsTmp!doc_cod_sii
                   Sp_Folio = RsTmp!no_documento
               End If
               
             
            '   obj.CarpetaDestino = "C:\FACTURAELECTRONICA\PDF\" & Replace(SP_Rut_Activo, ".", "")
               
               obj.Password = "1234"
               resp = obj.ConsultaEstadoDTE(Replace(SP_Rut_Activo, ".", ""), Replace(SP_Rut_Activo, ".", ""), Sp_cod_SII, Sp_Folio, "E", True)
               
               'resp = obj.DescargarPDF2(Replace(SP_Rut_Activo, ".", ""), Replace(LvDetalle.SelectedItem.SubItems(6), ".", ""), LvDetalle.SelectedItem.SubItems(8), LvDetalle.SelectedItem.SubItems(5), "R", "1", True)
               Select Case obj.CodigoAcuseComercial
                   Case 0
                       Sp_Resultado = "0=Aceptaro "
                       LVDetalle.ListItems(i).ListSubItems(14).Bold = True
                   Case 1
                       Sp_Resultado = "1=Aceptaro con Rep. "
                   Case 2
                       Sp_Resultado = "2=Rechazado "
                       LVDetalle.ListItems(i).ListSubItems(14).ForeColor = vbRed
                   Case 99
                       Sp_Resultado = ""
               End Select
               Sp_Resultado = Sp_Resultado & obj.GlosaAcuseComercial & " " & obj.FechaHoraAcuseComercial
               
               LVDetalle.ListItems(i).SubItems(16) = Sp_Resultado
               'Sp_Archivo2 = obj.FechaHoraAcuseComercial
              ' sp_archiv2 = obj.CodigoAcuseComercial
                
            '   Sp_Archivo2 = obj.ConsultaRecepcionadosF1(Replace(SP_Rut_Activo, ".", ""), Fql(DtDesde), Fql(DtHasta), True)
               Me.FrmLoad.Visible = False
            'End If
        Next
        Exit Sub
ErrorConexion:
Me.FrmLoad.Visible = False
    MsgBox "Algo fallo :(  ..." & vbNewLine & vbNewLine & Err.Description, vbExclamation
End Sub

Private Sub Command2_Click()
    FrmLoad.Visible = True
    DoEvents
    Sql = "SELECT doc_cod_sii,no_documento " & _
                       "FROM ven_doc_venta " & _
                       "JOIN sis_documentos USING(doc_id) " & _
                       "WHERE id=" & LVDetalle.SelectedItem
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
         If RsTmp!doc_cod_sii = 33 Or RsTmp!doc_cod_sii = 34 Or RsTmp!doc_cod_sii = 52 Or RsTmp!doc_cod_sii = 61 Then
     '   Sp_cod_SII = RsTmp!doc_cod_sii
     '   Sp_Folio = RsTmp!no_documento
         Else
            FrmLoad.Visible = False
             Exit Sub
         End If
     
    End If
    
    If RsTmp.RecordCount > 0 Then
        SG_LlevaIVAAnticipado = "NO"
       ' If Val(Me.TxtIvaAnticipado) > 0 Then
       '     SG_LlevaIVAAnticipado = "SI"
       ' End If
        Dim VerCedible As Boolean
        VerCedible = False
       ' If ChkCedible.Value = 1 Then VerCedible = True
        Archivo = "C:\FACTURAELECTRONICA\PDF\" & Replace(SP_Rut_Activo, ".", "") & "\T" & Trim(Str(RsTmp!doc_cod_sii)) & Trim("F" & RsTmp!no_documento) & ".PDF"
        dte_MostrarPDF.Dte Str(RsTmp!doc_cod_sii), RsTmp!no_documento, VerCedible
      ' MostrarPDF RsTmp!doc_cod_sii
    End If
    FrmLoad.Visible = False
End Sub

Private Sub Command3_Click()
    Me.FrmResumenes.Visible = True
    Command3.Visible = False
    ResumenesDoc
End Sub

Private Sub Form_KeyUp(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF1 Then CmdNueva_Click
    If KeyCode = vbKeyF2 Then CmdSeleccionar_Click
    If KeyCode = vbKeyF3 Then CmdExportar_Click
    If KeyCode = vbKeyF4 Then CmdVtaConBoletas_Click
End Sub

Private Sub Form_Load()
    Centrar Me
    Skin2 Me, , 5
    'FILTRO ESTADO DE PAGOS DE DOCUMENTOS
    Me.Height = 10605
    Me.Width = 14985
End Sub
Private Sub CargaForm()
  If SG_Oficina_Contable = "SI" Then
        FraContable.Visible = True
    
    End If
    Sql = "SELECT men_infoextra " & _
          "FROM sis_menu " & _
          "WHERE men_id=" & IP_IDMenu
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        Sp_extra = RsTmp!men_infoextra
    End If
    
    '7 marzo 2019 kyr
    If SP_Rut_Activo = "76.553.302-3" Then
        If LogPerfil <> 1 Then
            SkTotal.Visible = False
            txtTotal.Visible = False
        End If
    End If
    
    Sm_SoloNotasDeVentas = Sp_extra
    
    CboPago.AddItem "TODOS"
    CboPago.ItemData(CboPago.ListCount - 1) = 1
    CboPago.AddItem "CON DEUDA"
    CboPago.ItemData(CboPago.ListCount - 1) = 2
    CboPago.AddItem "SIN DEUDA"
    CboPago.ItemData(CboPago.ListCount - 1) = 3
    CboPago.ListIndex = 0
        
    LLenarCombo CboFpago, "mpa_nombre", "mpa_id", "par_medios_de_pago", "mpa_activo='SI'"
    CboFpago.AddItem "CREDITO"
    CboFpago.ItemData(CboFpago.ListCount - 1) = 100
    CboFpago.AddItem "TODOS"
    CboFpago.ItemData(CboFpago.ListCount - 1) = 0
    CboFpago.ListIndex = CboFpago.ListCount - 1
    'If SP_Control_Inventario = "SI" Then
    '    CmdVtaConBoletas.Visible = False
    'End If
        
    '9 Noviembre 2013
    'Verificamos si la empresa permite cambio mercaderia
    cmdCambiaMercaderia.Visible = False 'Por defcto
    Sql = "SELECT emp_permite_cambio_mercaderia cambio,emp_modulo_entrega entrega " & _
            "FROM sis_empresas " & _
                "WHERE emp_id=" & IG_id_Empresa
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        If RsTmp!cambio = "SI" Then cmdCambiaMercaderia.Visible = True
        If RsTmp!entrega = "SI" Then Me.cmdCambiaMercaderia.Visible = True
    End If
        
    'Comenzamos a separar las sucurales en los informes
    'si corresponde
    Sql = "SELECT IFNULL(COUNT(sue_id),0) cant " & _
            "FROM sis_empresas_sucursales " & _
            "WHERE emp_id=" & IG_id_Empresa
    Consulta RsTmp, Sql
    Sm_FiltroSucursal = ""
    If RsTmp.RecordCount > 0 Then
        If RsTmp!cant > 0 Then
            'Aplicar filtro de sucursal
            
            'Sm_FiltroSucursal = " AND sue_id=" & IG_id_Sucursal_Empresa
        End If
    End If
        
        
    DtDesde.Value = Date
    DtHasta.Value = Date
    Centrar Me
    SkEmpresaActiva = SP_Empresa_Activa
    Sp_Condicion = ""
    For i = 1 To 12
        comMes.AddItem UCase(MonthName(i, False))
        comMes.ItemData(comMes.ListCount - 1) = i
    Next
    Busca_Id_Combo comMes, Val(IG_Mes_Contable)
    LLenaYears ComAno, 2010
    'For i = Year(Date) To Year(Date) - 1 Step -1
    '    ComAno.AddItem i
    '    ComAno.ItemData(ComAno.ListCount - 1) = i
    'Next
    'Busca_Id_Combo ComAno, Val(IG_Ano_Contable)
    
    'VENDEDORES PARA FILTRO
    LLenarCombo CboVendedores, "ven_nombre", "ven_id", "par_vendedores", "ven_activo='SI' AND rut_emp='" & SP_Rut_Activo & "'", "ven_nombre"
    CboVendedores.AddItem "TODOS"
    CboVendedores.ListIndex = CboVendedores.ListCount - 1
    
    Sql = "SELECT bod_id " & _
            "FROM par_bodegas " & _
            "WHERE rut_emp='" & SP_Rut_Activo & "' AND sue_id=" & IG_id_Sucursal_Empresa
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        IG_id_Bodega_Ventas = RsTmp!bod_id
    End If
    LLenarCombo CboDocVenta, "doc_nombre", "doc_id", "sis_documentos", "doc_activo='SI' AND doc_documento='VENTA' AND doc_utiliza_en_caja='" & SG_Funcion_Equipo & "'", "doc_id"
    If SP_Rut_Activo = "76.169.962-8" Or SP_Rut_Activo = "76.881.179-2" Or SP_Rut_Activo = "76.063.757-2" Then
        CboDocVenta.AddItem "VOUCHER TRANSBANK"
        CboDocVenta.ItemData(CboDocVenta.ListCount - 1) = 40
        CmdCambiaNroDoc.Visible = True
    End If
   ' If SP_Rut_Activo = "76.005.337-6" Or SP_Rut_Activo = "76.553.302-3" Or SP_Rut_Activo = "77.774.580-8" Or SP_Rut_Activo = "12.072.366-9" Or SP_Rut_Activo = "76.354.346-3" Or SP_Rut_Activo = "11.907.734-6" Or SP_Rut_Activo = "76.362.703-9" Or SP_Rut_Activo = "76.740.738-6" Then
        CmdCambiaNroDoc.Visible = True
  '  End If
    
    
    'Filtrar sucursales
    LLenarCombo CboSucEmpresa, "CONCAT(sue_direccion,' -',sue_ciudad)", "sue_id", "sis_empresas_sucursales", "sue_activa='SI'"
    CboSucEmpresa.AddItem "CASA MATRIZ"
    CboSucEmpresa.ItemData(CboSucEmpresa.ListCount - 1) = "1"
    CboSucEmpresa.AddItem "TODAS"
    CboSucEmpresa.ItemData(CboSucEmpresa.ListCount - 1) = "0"
    
    CboSucEmpresa.ListIndex = CboSucEmpresa.ListCount - 1
    
    LLenarCombo Me.CboCambiaSuc, "CONCAT(sue_direccion,' -',sue_ciudad)", "sue_id", "sis_empresas_sucursales", "sue_activa='SI'"
    CboCambiaSuc.AddItem "CASA MATRIZ"
    CboCambiaSuc.ItemData(CboCambiaSuc.ListCount - 1) = "1"
    CboCambiaSuc.ListIndex = CboCambiaSuc.ListCount - 1
    
    CmdBusca_Click
End Sub


Private Sub CargaDatos()
    Dim Sp_FiltroNV As String
    Dim Sp_FiltroDocId As String
  TxtTotalFacturas = 0
'IF( doc_factura_guias='SI','{ Por Guia(s) }',''),IF(nro_factura>0,' {FACTURADA} ',''))
   ' Sql = "SELECT id,fecha,nombre_cliente,CONCAT(tipo_doc,' ',IF(doc_factura_guias='SI','POR GUIA(S)','')  ," & _
                "IF(nro_factura>0,' {FACTURADA} ','')) tipo_d    ,no_documento," & _
                " IF(ven_boleta_hasta>0,CONCAT(' AL ',CAST(ven_boleta_hasta AS CHAR)), tipo_movimiento)," & _
                "bruto * IF(doc_signo_libro='-',-1,1) bruto  ,ven_nombre,IFNULL((SELECT SUM(d.ctd_monto) " & _
                                            "FROM cta_abono_documentos d " & _
                                            "INNER JOIN cta_abonos a USING(abo_id) " & _
                                            "WHERE a.abo_cli_pro='CLI' AND d.id=ven_doc_venta.id " & _
                                            "GROUP BY d.id),0) abonos, " & _
                                                "ven_boleta_hasta " & _
          "FROM ven_doc_venta " & _
          "JOIN sis_documentos d USING(doc_id) " & _
          "WHERE ven_estado_nota_venta='PROCESO' AND rut_emp='" & SP_Rut_Activo & "' " & sp_Condicion
          
          
        If CboDocVenta.ListIndex > -1 Then
            Sp_FiltroDocId = " AND doc_id=" & CboDocVenta.ItemData(CboDocVenta.ListIndex) & " "
        Else
            Sp_FiltroDocId = " "
        End If
        Sp_FiltroNV = ""
        If Me.ChkCotizacion.Value = 0 Then
                Sp_FiltroNV = " AND doc_nota_de_venta='NO' "
        End If
        If Sm_SoloNotasDeVentas = "SOLONV" Then
            'solo y solo notas de venta
                Sp_FiltroNV = " AND doc_nota_de_venta='SI' "
               Sql = "SELECT id,fecha,nombre_cliente,tipo_d,no_documento," & _
                        " movimiento,bruto,ven_nombre,abonos,ven_boleta_hasta,doc_signo_libro,'', " & _
                        " 0,ven_simple,ven_oficina_contable " & _
                  "FROM vi_venta_listado v " & _
                  "WHERE rut_emp='" & SP_Rut_Activo & "' " & Sm_FiltroSucursal & " " & Sp_FiltroNV & Sp_FiltroDocId
        
        Else
            'normal
                tumaguia = ""
                If SP_Rut_Activo = "76.978.874-3" Then
                    Sp_FiltroDocId = Sp_FiltroDocId & " AND doc_id<>76 "
                End If
        
                Sql = "SELECT id,fecha,nombre_cliente,tipo_d,no_documento," & _
                        " movimiento,bruto,ven_nombre,abonos,ven_boleta_hasta,doc_signo_libro," & _
                            "IFNULL((SELECT m.mpa_nombre " & _
                             "FROM abo_tipos_de_pagos p " & _
                            "JOIN cta_abono_documentos l ON p.abo_id = l.abo_id " & _
                            "JOIN cta_abonos a ON l.abo_id = a.abo_id " & _
                            "JOIN par_medios_de_pago m ON p.mpa_id=m.mpa_id " & _
                            "WHERE a.abo_cli_pro = 'CLI' AND a.rut_emp = '" & SP_Rut_Activo & "' " & _
                            "AND l.id = v.id " & _
                            "LIMIT 1),'CREDITO')," & _
                        "   (SELECT (SUM(ved_precio_venta_bruto)  - (v.ven_descuento_valor +v.ven_ajuste_descuento-ven_ajuste_recargo)) / SUM(precio_costo) " & _
                            "FROM ven_detalle g " & _
                            "WHERE  g.doc_id=v.doc_id AND g.no_documento=v.no_documento AND g.rut_emp='" & SP_Rut_Activo & "'),ven_simple,ven_oficina_contable,'','', hora " & _
                  "FROM vi_venta_listado v " & _
                  "WHERE  rut_emp='" & SP_Rut_Activo & "' " & Sm_FiltroSucursal & Sp_Condicion & " " & Sm_FiltroFpago & " " & Sp_FiltroNV & Sp_FiltroDocId & Sm_FiltroCaja & Sm_CondicionHaving
                  
    
                  
        End If
    ' DoEvents
     'Cargar_Recordset Sql
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, Me.LVDetalle, False, True, True, False
    '6 y 9
    For i = 1 To LVDetalle.ListItems.Count
        If CDbl(LVDetalle.ListItems(i).SubItems(6)) = CDbl(LVDetalle.ListItems(i).SubItems(8)) Then
            For X = 1 To LVDetalle.ColumnHeaders.Count - 3
                LVDetalle.ListItems(i).ListSubItems(X).ForeColor = vbBlue
            Next
        Else
            For X = 1 To LVDetalle.ColumnHeaders.Count - 3
                LVDetalle.ListItems(i).ListSubItems(X).ForeColor = vbRed
            Next
        End If
    Next
    txtTotal = 0
    Dim Sp_Ids As String
    Sp_Ids = ""
    TxtXcienUtilidad = 0
    CONTADOR = 0
    For i = 1 To LVDetalle.ListItems.Count
        Sp_Ids = Sp_Ids & LVDetalle.ListItems(i) & ","
        If LVDetalle.ListItems(i).SubItems(10) = "+" And LVDetalle.ListItems(i).SubItems(5) = "VD" Then
            CONTADOR = CONTADOR + 1
            txtTotal = NumFormat(CDbl(txtTotal) + CDbl(LVDetalle.ListItems(i).SubItems(6)))
   '         TxtXcienUtilidad = TxtXcienUtilidad + Val(CxP(LvDetalle.ListItems(i).SubItems(12)))
        ElseIf LVDetalle.ListItems(i).SubItems(10) = "-" And LVDetalle.ListItems(i).SubItems(5) = "VD" Then
            CONTADOR = CONTADOR + 1
            txtTotal = NumFormat(CDbl(txtTotal) + CDbl(LVDetalle.ListItems(i).SubItems(6)))
  '          TxtXcienUtilidad = TxtXcienUtilidad - Val(CxP(LvDetalle.ListItems(i).SubItems(12)))
        End If
    Next
 '   If contador > 0 Then
 '       TxtXcienUtilidad = Format(Val(CxP(TxtXcienUtilidad)) / contador, "#0.0#")
 '   End If
 
 '   txtTotal = NumFormat(TotalizaColumna(LvDetalle, "total"))
    
   'buscar la forma de pago
   '2 agosto 2014
  ' If Len(Sp_Ids) > 0 Then
  '      Sql = "SELECT id,mpa_nombre " & _
  '           "FROM abo_tipos_de_pagos p " & _
  '           "JOIN par_medios_de_pago m ON p.mpa_id=m.mpa_id " & _
  '           "JOIN cta_abonos a ON p.abo_id=a.abo_id " & _
  '           "JOIN cta_abono_documentos d ON d.abo_id=p.abo_id " & _
  '           "WHERE abo_cli_pro='CLI' " & _
  '           "AND d.id IN(" & Mid(Sp_Ids, 1, Len(Sp_Ids) - 1) & ")"
  '      Consulta RsTmp, Sql
  '      If RsTmp.RecordCount > 0 Then
  '          For i = 1 To LvDetalle.ListItems.Count
  '              RsTmp.Find "id=" & LvDetalle.ListItems(i), , adSearchForward, 1
  '              If Not RsTmp.EOF Then
  '                  LvDetalle.ListItems(i).SubItems (11)=lvdetalle.ListItems(i).SubItems(11) &
  '              End If
  '
  '          Next
  '      End If
  '
  ' End If
    'Costo
    
    '11 Nov 2014
    'Solo para ALCALDE y KYR

    If SP_Rut_Activo = "76.169.962-8" Or SP_Rut_Activo = "76.553.302-3" Then ' RUT ALCALDE REPUESTOS
        CmdXcien.Visible = True
    End If
    
    
    
End Sub
Private Sub ResumenesDoc()
    
    FrmLoad.Visible = True
    DoEvents
       ' If 1 = 2 Then
                    'facturas
                     sql2 = "SELECT SUM(bruto) facturas,COUNT(id) cant " & _
                           "FROM vi_venta_listado v " & _
                           "WHERE doc_id=29 AND rut_emp='" & SP_Rut_Activo & "' " & Sm_FiltroSucursal & Sp_Condicion & " " & Sm_FiltroFpago & " " & Sp_FiltroNV & Sp_FiltroDocId & Sm_CondicionHaving
                     Consulta RsTmp2, sql2
                     If RsTmp2.RecordCount > 0 Then
                         Me.TxtTotalFacturas = NumFormat(RsTmp2!facturas)
                         Me.SkFacturas = RsTmp2!cant & " FAC. ELECTRONICAS"
                     End If
                     
                     'boletas
                     sql2 = "SELECT SUM(bruto) boletas,COUNT(id) cant " & _
                           "FROM vi_venta_listado v " & _
                           "WHERE doc_id=2 AND rut_emp='" & SP_Rut_Activo & "' " & Sm_FiltroSucursal & Sp_Condicion & " " & Sm_FiltroFpago & " " & Sp_FiltroNV & Sp_FiltroDocId & Sm_CondicionHaving
                     Consulta RsTmp2, sql2
                     If RsTmp2.RecordCount > 0 Then
                         Me.TxtBoletas = NumFormat(RsTmp2!boletas)
                         Me.SkBoletas = RsTmp2!cant & " BOLETAS"
                     End If
                     
                     'guias
                     sql2 = "SELECT SUM(bruto) guias,COUNT(id) cant " & _
                           "FROM vi_venta_listado v " & _
                           "WHERE doc_id IN(3,39) AND rut_emp='" & SP_Rut_Activo & "' " & Sm_FiltroSucursal & Sp_Condicion & " " & Sm_FiltroFpago & " " & Sp_FiltroNV & Sp_FiltroDocId & Sm_CondicionHaving
                     Consulta RsTmp2, sql2
                     If RsTmp2.RecordCount > 0 Then
                         Me.TxtGuias = NumFormat(RsTmp2!guias)
                         Me.SkGuias = RsTmp2!cant & " GUIAS"
                     End If
                     
                        'nc
                     sql2 = "SELECT SUM(bruto) ncs,COUNT(id) cant " & _
                           "FROM vi_venta_listado v " & _
                           "WHERE doc_id IN(11,34) AND rut_emp='" & SP_Rut_Activo & "' " & Sm_FiltroSucursal & Sp_Condicion & " " & Sm_FiltroFpago & " " & Sp_FiltroNV & Sp_FiltroDocId & Sm_CondicionHaving
                     Consulta RsTmp2, sql2
                     If RsTmp2.RecordCount > 0 Then
                         Me.TxtNc = NumFormat(RsTmp2!ncs)
                         Me.SkNc = RsTmp2!cant & " NC's"
                     End If
         '   End If
        FrmLoad.Visible = False
End Sub
Private Sub XcienTotal()
    Dim Dp_Venta As Double
    Dim Dp_Costo As Double
    Dim Dp_Dsctos As Double
    If SP_Rut_Activo = "76.169.962-8" Or SP_Rut_Activo = "76.553.302-3" Then ' RUT ALCALDE REPUESTOS
            TxtXcienUtilidad.Visible = True
         '   SkinLabel2.Visible = True
            'Sql = "SELECT SUM(ved_precio_venta_bruto)/SUM( IF(doc_signo_libro='-',precio_costo*-1,precio_costo)) costo " & _
                    "FROM ven_detalle d " & _
                    "JOIN ven_doc_venta v ON d.doc_id=v.doc_id AND d.no_documento=v.no_documento " & _
                    "JOIN sis_documentos s ON d.doc_id=s.doc_id " & _
                    " WHERE s.doc_contable='SI' AND  d.rut_emp='" & SP_Rut_Activo & "' AND  v.rut_emp = '" & SP_Rut_Activo & "' " & Sm_FiltroSucursal & Sp_Condicion & " " & Sm_FiltroFpago
                    
                    
            
        Sql = "SELECT  SUM(ved_precio_venta_bruto) venta, SUM(precio_costo) costo " & _
                    "FROM ven_detalle d " & _
                    "JOIN ven_doc_venta v ON d.doc_id=v.doc_id AND d.no_documento=v.no_documento " & _
                    "JOIN sis_documentos s ON d.doc_id=s.doc_id " & _
                    " WHERE nombre_cliente<>'ANULADO' AND doc_nota_de_credito='NO' AND s.doc_contable='SI' AND  d.rut_emp='" & SP_Rut_Activo & "' AND  v.rut_emp = '" & SP_Rut_Activo & "' " & Sm_FiltroSucursal & Sp_Condicion & " " & Sm_FiltroFpago
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
            Dp_Venta = RsTmp!venta
            Dp_Costo = RsTmp!costo
        End If
            
            
        Sql = "SELECT  SUM(v.ven_descuento_valor) + SUM(v.ven_ajuste_descuento) +SUM(ven_ajuste_recargo) dsctos " & _
                    "FROM ven_doc_venta v  " & _
                    "/* JOIN ven_doc_venta v ON d.doc_id=v.doc_id AND d.no_documento=v.no_documento  */ " & _
                    "JOIN sis_documentos s ON v.doc_id=s.doc_id " & _
                    " WHERE nombre_cliente<>'ANULADO' AND doc_nota_de_credito='NO' AND s.doc_contable='SI' AND  v.rut_emp='" & SP_Rut_Activo & "' AND  v.rut_emp = '" & SP_Rut_Activo & "' " & Sm_FiltroSucursal & Sp_Condicion & " " & Sm_FiltroFpago
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
            Dp_Dsctos = RsTmp!dsctos
        End If
        
        
        
        If Dp_Venta > 0 Then
            TxtXcienUtilidad = Format("" & (Dp_Venta - Dp_Dsctos) / Dp_Costo, "#0.#0")
            Exit Sub
            
        End If
        
            'Sql = "SELECT (SUM(ved_precio_venta_bruto) - (ven_descuento_valor +ven_ajuste_descuento+ven_ajuste_recargo))/SUM( IF(doc_signo_libro='-',precio_costo*-1,precio_costo)) costo " & _
                    "FROM ven_detalle d " & _
                    "JOIN ven_doc_venta v ON d.doc_id=v.doc_id AND d.no_documento=v.no_documento " & _
                    "JOIN sis_documentos s ON d.doc_id=s.doc_id " & _
                    " WHERE doc_nota_de_credito='NO' AND s.doc_contable='SI' AND  d.rut_emp='" & SP_Rut_Activo & "' AND  v.rut_emp = '" & SP_Rut_Activo & "' " & Sm_FiltroSucursal & Sp_Condicion & " " & Sm_FiltroFpago
            
            Consulta RsTmp, Sql
            If RsTmp.RecordCount > 0 Then
                
            End If
    End If
End Sub

Private Sub LvDetalle_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    ordListView ColumnHeader, Me, LVDetalle
End Sub

Private Sub LvDetalle_DblClick()
    CmdSeleccionar_Click
End Sub

Private Sub Timer1_Timer()
    FrmLoad.Visible = True
    Timer1.Enabled = False
    DoEvents
    CargaForm
    FrmLoad.Visible = False
End Sub

Private Sub TxtBusqueda_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
    If KeyAscii = 39 Then KeyAscii = 0
End Sub



Private Sub TxtNroDoc_GotFocus()
    En_Foco TxtNroDoc
End Sub

Private Sub TxtNroDoc_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
End Sub

Private Sub TxtNroDoc_Validate(Cancel As Boolean)
    If Val(TxtNroDoc) = 0 Then TxtNroDoc = "0"
End Sub
