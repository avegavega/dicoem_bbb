VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form ven_cambiomercaderia_listado 
   Caption         =   "Cambios de Mercaderia"
   ClientHeight    =   10065
   ClientLeft      =   4275
   ClientTop       =   1155
   ClientWidth     =   13335
   LinkTopic       =   "Form1"
   ScaleHeight     =   10065
   ScaleWidth      =   13335
   Begin VB.CommandButton cmdSalir 
      Cancel          =   -1  'True
      Caption         =   "Retornar"
      Height          =   390
      Left            =   11520
      TabIndex        =   17
      Top             =   9600
      Width           =   1440
   End
   Begin VB.Frame Frame4 
      Caption         =   "Mes y A�o"
      Height          =   1170
      Left            =   645
      TabIndex        =   4
      Top             =   150
      Width           =   12315
      Begin VB.CommandButton CmdBuscar 
         Caption         =   "Buscar"
         Height          =   330
         Left            =   6195
         TabIndex        =   16
         Top             =   405
         Width           =   2310
      End
      Begin VB.ComboBox comMes 
         Height          =   315
         ItemData        =   "ven_cambiomercaderia_listado.frx":0000
         Left            =   135
         List            =   "ven_cambiomercaderia_listado.frx":0002
         Style           =   2  'Dropdown List
         TabIndex        =   8
         Top             =   450
         Width           =   1575
      End
      Begin VB.ComboBox ComAno 
         Height          =   315
         Left            =   1710
         Style           =   2  'Dropdown List
         TabIndex        =   7
         Top             =   435
         Width           =   1215
      End
      Begin VB.CheckBox ChkFecha 
         Height          =   225
         Left            =   3210
         TabIndex        =   5
         Top             =   480
         Width           =   225
      End
      Begin MSComCtl2.DTPicker DtDesde 
         CausesValidation=   0   'False
         Height          =   285
         Left            =   3480
         TabIndex        =   6
         Top             =   450
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   503
         _Version        =   393216
         Enabled         =   0   'False
         Format          =   273416193
         CurrentDate     =   41001
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
         Height          =   255
         Left            =   120
         OleObjectBlob   =   "ven_cambiomercaderia_listado.frx":0004
         TabIndex        =   9
         Top             =   255
         Width           =   375
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel5 
         Height          =   255
         Left            =   1725
         OleObjectBlob   =   "ven_cambiomercaderia_listado.frx":0068
         TabIndex        =   10
         Top             =   255
         Width           =   375
      End
      Begin MSComCtl2.DTPicker DtHasta 
         Height          =   285
         Left            =   4680
         TabIndex        =   11
         Top             =   450
         Width           =   1200
         _ExtentX        =   2117
         _ExtentY        =   503
         _Version        =   393216
         Enabled         =   0   'False
         Format          =   273416193
         CurrentDate     =   41001
      End
   End
   Begin VB.Frame frmOts 
      Caption         =   "Listado historico"
      Height          =   8040
      Left            =   675
      TabIndex        =   0
      Top             =   1500
      Width           =   12315
      Begin VB.CommandButton CmdAnulaCambio 
         Caption         =   "Anular Cambio"
         Height          =   465
         Left            =   9720
         TabIndex        =   19
         Top             =   4200
         Width           =   2325
      End
      Begin VB.CommandButton CmdNuevo 
         Caption         =   "Realizar cambio"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   465
         Left            =   210
         TabIndex        =   18
         Top             =   4230
         Width           =   2325
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   315
         Left            =   165
         OleObjectBlob   =   "ven_cambiomercaderia_listado.frx":00CC
         TabIndex        =   14
         Top             =   4815
         Width           =   2535
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   270
         Left            =   13020
         OleObjectBlob   =   "ven_cambiomercaderia_listado.frx":0156
         TabIndex        =   1
         Top             =   5730
         Visible         =   0   'False
         Width           =   420
      End
      Begin MSComctlLib.ListView LvDetalle 
         Height          =   3945
         Left            =   195
         TabIndex        =   2
         Top             =   255
         Width           =   11790
         _ExtentX        =   20796
         _ExtentY        =   6959
         View            =   3
         LabelWrap       =   0   'False
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   9
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "N109"
            Text            =   "Id Unico"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "N109"
            Text            =   "Nro"
            Object.Width           =   1587
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "F1000"
            Text            =   "Fecha"
            Object.Width           =   1852
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Object.Tag             =   "T1000"
            Text            =   "Usuario"
            Object.Width           =   5821
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Object.Tag             =   "T2500"
            Text            =   "Tipo Documento"
            Object.Width           =   5292
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   5
            Object.Tag             =   "N109"
            Text            =   "Nro Doc"
            Object.Width           =   1940
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   6
            Object.Tag             =   "T1500"
            Text            =   "Hora"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   7
            Object.Tag             =   "T1000"
            Text            =   "Estado"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   8
            Object.Tag             =   "N109"
            Text            =   "Id"
            Object.Width           =   2540
         EndProperty
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel6 
         Height          =   255
         Left            =   4350
         OleObjectBlob   =   "ven_cambiomercaderia_listado.frx":01B6
         TabIndex        =   3
         Top             =   4290
         Width           =   8055
      End
      Begin MSComctlLib.ListView LvProductosDocumento 
         Height          =   2655
         Left            =   180
         TabIndex        =   12
         ToolTipText     =   "Marque los productos que cambiar�"
         Top             =   5145
         Width           =   5865
         _ExtentX        =   10345
         _ExtentY        =   4683
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   3
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "N109"
            Text            =   "Codigo"
            Object.Width           =   1499
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "T4500"
            Text            =   "Descripcion"
            Object.Width           =   6174
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   2
            Object.Tag             =   "N109"
            Text            =   "Cant."
            Object.Width           =   2117
         EndProperty
      End
      Begin MSComctlLib.ListView LvProductosSalida 
         Height          =   2700
         Left            =   6285
         TabIndex        =   13
         ToolTipText     =   "Marque los productos que cambiar�"
         Top             =   5130
         Width           =   5700
         _ExtentX        =   10054
         _ExtentY        =   4763
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   3
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "N109"
            Text            =   "Codigo"
            Object.Width           =   1499
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "T4500"
            Text            =   "Descripcion"
            Object.Width           =   6174
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   2
            Object.Tag             =   "N109"
            Text            =   "Cant."
            Object.Width           =   2117
         EndProperty
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   315
         Left            =   6330
         OleObjectBlob   =   "ven_cambiomercaderia_listado.frx":026E
         TabIndex        =   15
         Top             =   4800
         Width           =   2535
      End
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   0
      OleObjectBlob   =   "ven_cambiomercaderia_listado.frx":02F8
      Top             =   0
   End
End
Attribute VB_Name = "ven_cambiomercaderia_listado"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim Sp_Condicion As String
Private Sub ChkFecha_Click()
    On Error Resume Next
    If ChkFecha.Value = 1 Then
        comMes.Enabled = False
        ComAno.Enabled = False
        DtDesde.Enabled = True
        DtHasta.Enabled = True
        DtDesde.SetFocus
    Else
        comMes.SetFocus
        DtDesde.Enabled = False
        DtHasta.Enabled = False
        comMes.Enabled = True
        ComAno.Enabled = True
    End If
End Sub

Private Sub CmdAnulaCambio_Click()
    
    Dim Lp_IdCambio As Long, Lp_Id_inv As Long
'    If Len(TxtCodigo) = 0 Then
'        MsgBox "Debe seleccionar producto que va a cambiar...", vbInformation
'        TxtCodigo.SetFocus
'        Exit Sub
'    End If
'    If Val(txtCantACambiar) = 0 Then
'        MsgBox "No ha seleccionado productos para cambiar...", vbInformation
'        LvProductosDocumento.SetFocus
'        Exit Sub
'    End If
'
'
'    If LvProductos.ListItems.Count = 0 Then
'        MsgBox "Debe seleccionar productos para el cambio...", vbInformation
'        txtCodigoC.SetFocus
'        Exit Sub
'    End If
'    If Len(txtDocumento) = 0 Then
'        MsgBox "No ha seleccionado documento de referencia...", vbInformation
'        LvDetalle.SetFocus
'        Exit Sub
'    End If
'    If Val(txtDevuelve) = 0 Then
'        MsgBox "Debe ingresar cantidad a devolver...", vbInformation
'        txtDevuelve.SetFocus
'        Exit Sub
'    End If
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    
    
    If MsgBox("Est� seguro de Anular el cambio seleccionado? ...", vbOKCancel + vbQuestion) = vbCancel Then Exit Sub
    
    
    
    On Error GoTo ErrorCambio
    cn.BeginTrans
        Lp_IdCambio = UltimoNro("ven_cambio_mercaderia", "cam_id")
     '   Sql = "INSERT INTO ven_cambio_mercaderia (cam_id,cam_fecha,id,pro_codigo,cam_usuario,cam_cantidad,cam) " & _
                "VALUES(" & Lp_IdCambio & ",'" & Fql(Date) & " " & Time & "'," & CboDocVenta.Tag & "," & 0 & ",'" & LogUsuario & "'," & Val(Me.txtCantACambiar) & ")"
                
           Sql = "INSERT INTO ven_cambio_mercaderia (cam_id,cam_fecha,id,pro_codigo,cam_usuario,cam_cantidad,cam_anulado) " & _
                "SELECT " & Lp_IdCambio & ",'" & Fql(Date) & " " & Time & "',id,pro_codigo,'" & LogUsuario & "',cam_cantidad,'ANUL. CAMB' " & _
                "FROM ven_cambio_mercaderia " & _
                "WHERE cam_id=" & LvDetalle.SelectedItem
        cn.Execute Sql
        'For i = 1 To LvProductos.ListItems.Count
            'Productos que salen / Ahora vuelven a entrar
            'cn.Execute "INSERT INTO ven_cambio_mercaderia_detalle (cam_id,pro_codigo,cbd_cantidad,cbd_precio_venta,cbd_tipo) VALUES(" & _
                Lp_IdCambio & "," & LvProductos.ListItems(i) & "," & LvProductos.ListItems(i).SubItems(3) & "," & CDbl(LvProductos.ListItems(i).SubItems(4)) & ",'SALIDA')"
                
                
            cn.Execute "INSERT INTO ven_cambio_mercaderia_detalle (cam_id,pro_codigo,cbd_cantidad,cbd_precio_venta,cbd_tipo) SELECT " & _
                Lp_IdCambio & ",pro_codigo,cbd_cantidad,cbd_precio_venta,'ENTRADA' " & _
                    "FROM ven_cambio_mercaderia_detalle " & _
                    "WHERE cam_id=" & LvDetalle.SelectedItem & " AND cbd_tipo='SALIDA'"
                    
        'Next
        'For i = 1 To LvProductosDocumento.ListItems.Count
            'Productos que entran  / AHORA SALDRAN 9-10-15
         '   If LvProductosDocumento.ListItems(i).Checked And Val(LvProductosDocumento.ListItems(i).SubItems(6)) > 0 Then
                 '   cn.Execute "INSERT INTO ven_cambio_mercaderia_detalle (cam_id,pro_codigo,cbd_cantidad,cbd_precio_venta,cbd_tipo) VALUES(" & _
                        Lp_IdCambio & "," & LvProductosDocumento.ListItems(i).SubItems(1) & "," & LvProductosDocumento.ListItems(i).SubItems(6) & "," & CDbl(LvProductosDocumento.ListItems(i).SubItems(4)) & ",'ENTRADA')"
                        
                     cn.Execute "INSERT INTO ven_cambio_mercaderia_detalle (cam_id,pro_codigo,cbd_cantidad,cbd_precio_venta,cbd_tipo)  SELECT " & _
                Lp_IdCambio & ",pro_codigo,cbd_cantidad,cbd_precio_venta,'SALIDA' " & _
                    "FROM ven_cambio_mercaderia_detalle " & _
                    "WHERE cam_id=" & LvDetalle.SelectedItem & " AND cbd_tipo='ENTRADA'"
         '   End If
        'Next
        
        cn.Execute "UPDATE ven_cambio_mercaderia SET cam_anulado='ANULADO' " & _
                    "WHERE cam_id=" & LvDetalle.SelectedItem
        
       

                
        'For i = 1 To LvProductos.ListItems.Count
            'Lp_Id_inv = UltimoNro("inv_kardex", "kar_id")
            'ctd = Val(txtDevuelve) 'LvProductos.ListItems(i).SubItems(3)
            
           ' Sql = " INSERT INTO inv_kardex SELECT " & Lp_Id_inv & ",CURRENT_DATE(),'ENTRADA',kar_documento,kar_numero,bod_id,pro_codigo,/* saldo anterior*/ kar_nuevo_saldo SALDO_ANTERIOR," & _
                    ctd & ", kar_nuevo_saldo + " & Val(ctd) & " NUEVOSALDO,'" & LogUsuario & "'," & _
                    "pro_precio_neto,0,CONCAT('CAMBIO MERCADERIA ',kar_descripcion),rut,rsocial,/*anterior */kar_nuevo_saldo_valor SALDOANTERIOR,pro_precio_neto* " & ctd & ", " & _
                    "kar_nuevo_saldo_valor+(pro_precio_neto*" & ctd & ") NUEVOSALDOVALOR,CURRENT_TIMESTAMP(),kar_mostrar,rut_emp,doc_id,0 " & _
                     "FROM inv_kardex WHERE pro_codigo=" & txtCodigo & " AND id=" & txtDocumento.Tag
            'cn.Execute Sql
       ' Next
        'Ahora viene los movimientos del KARD
                    Dim Dp_SAnterior As Double
                    Dim Dp_SAnteriorvalor As Double
                    Dim Dp_Diferencia As Double
                    Dim Dp_PrecioNeto As Double
                    Dim Ip_IdBodega As Integer
                    Dim Lp_Numero As Long
                
                'Prudcto que vuelve a entrar
                
                
                
                For i = 1 To LvProductosDocumento.ListItems.Count
                        '7 Octubre,2013
                        'Rehacer kardex para ajuste de inventario
                   
                            Sql = "SELECT kar_documento,kar_numero,bod_id " & _
                                     "FROM inv_kardex WHERE pro_codigo='" & LvProductosDocumento.ListItems(i) & "' AND id=" & LvDetalle.SelectedItem.SubItems(8)
                            Consulta RsTmp, Sql
                            If RsTmp.RecordCount > 0 Then
                                Ip_IdBodega = RsTmp!bod_id
                                Lp_Numero = RsTmp!kar_numero
                            End If
                    
                            Sql = "SELECT kar_nuevo_saldo,pro_precio_neto,kar_nuevo_saldo_valor " & _
                                            "FROM inv_kardex " & _
                                            "WHERE pro_codigo='" & LvProductosDocumento.ListItems(i) & "' AND rut_emp='" & SP_Rut_Activo & "' " & _
                                            "ORDER BY kar_id DESC " & _
                                            "LIMIT 1"
                                    Consulta RsTmp, Sql
                                    If RsTmp.RecordCount > 0 Then
                                        Dp_SAnterior = RsTmp!kar_nuevo_saldo
                                        Dp_SAnteriorvalor = RsTmp!kar_nuevo_saldo_valor
                                        Dp_Diferencia = Val(LvProductosDocumento.ListItems(i).SubItems(2))
                                        Dp_PrecioNeto = RsTmp!pro_precio_neto
                                    End If
                                'Hacer el insert directo sin la funcion Kardex
                             Sql = "INSERT INTO inv_kardex (kar_fecha,kar_movimiento,kar_numero,bod_id,pro_codigo," & _
                                    "kar_saldo_anterior,kar_cantidad,kar_nuevo_saldo,kar_usuario,pro_precio_neto,kar_descripcion," & _
                                    "kar_saldo_anterior_valor,kar_cantidad_valor,kar_nuevo_saldo_valor,rut_emp) VALUES("
                            cn.Execute Sql & "'" & Fql(Date) & "','SALIDA'," & Lp_Numero & "," & Ip_IdBodega & ",'" & LvProductosDocumento.ListItems(i) & "'," & _
                                    Dp_SAnterior & "," & Abs(Dp_Diferencia) & "," & Dp_SAnterior - Val(LvProductosDocumento.ListItems(i).SubItems(2)) & ",'" & LogUsuario & "'," & CxP(Dp_PrecioNeto) & ",'" & _
                                    "ANULA CAMBIO MERCADERIA" & "'," & CxP(Dp_PrecioNeto * Dp_SAnterior) & "," & _
                                    CxP(Abs(Dp_Diferencia) * Dp_PrecioNeto) & "," & Dp_SAnteriorvalor - (CxP(Dp_PrecioNeto) * Val(Val(LvProductosDocumento.ListItems(i).SubItems(2)))) & ",'" & SP_Rut_Activo & "')"
                                
                            cn.Execute "UPDATE pro_stock SET sto_stock=" & Dp_SAnterior - Val(LvProductosDocumento.ListItems(i).SubItems(2)) & " " & _
                                                "WHERE bod_id=" & Ip_IdBodega & " AND rut_emp='" & SP_Rut_Activo & "' AND pro_codigo='" & LvProductosDocumento.ListItems(i) & "'"
                
                            'Hasta aqui el kardex q retira productos.
                              'Prudcto que vuelve a entrar
                   
                Next
        '    GoTo fin
          
          
          'Ahora productos que va a SALIR POR EL CAMBIO
          For i = 1 To LvProductosSalida.ListItems.Count
                'Sql = "SELECT kar_documento,kar_numero,bod_id " & _
                '     "FROM inv_kardex WHERE pro_codigo=" & LvProductos.ListItems(i) & " AND id=" & txtDocumento.Tag
                'Consulta RsTmp, Sql
                'If RsTmp.RecordCount > 0 Then
                '    Ip_IdBodega = RsTmp!bod_id
                '    Lp_Numero = RsTmp!kar_numero
                'End If
                '7 Octubre,2013
                'Rehacer kardex para ajuste de inventario
                    Sql = "SELECT kar_nuevo_saldo,pro_precio_neto,kar_nuevo_saldo_valor " & _
                            "FROM inv_kardex " & _
                            "WHERE pro_codigo='" & LvProductosSalida.ListItems(i) & "' AND rut_emp='" & SP_Rut_Activo & "' " & _
                            "ORDER BY kar_id DESC " & _
                            "LIMIT 1"
                    Consulta RsTmp, Sql
                    If RsTmp.RecordCount > 0 Then
                        Dp_SAnterior = RsTmp!kar_nuevo_saldo
                        Dp_SAnteriorvalor = RsTmp!kar_nuevo_saldo_valor
                        Dp_Diferencia = LvProductosSalida.ListItems(i).SubItems(2)
                        Dp_PrecioNeto = RsTmp!pro_precio_neto
                    End If
                'Hacer el insert directo sin la funcion Kardex
             Sql = "INSERT INTO inv_kardex (kar_fecha,kar_movimiento,kar_numero,bod_id,pro_codigo," & _
                    "kar_saldo_anterior,kar_cantidad,kar_nuevo_saldo,kar_usuario,pro_precio_neto,kar_descripcion," & _
                    "kar_saldo_anterior_valor,kar_cantidad_valor,kar_nuevo_saldo_valor,rut_emp) VALUES("
            cn.Execute Sql & "'" & Fql(Date) & "','ENTRADA'," & Lp_Numero & "," & Ip_IdBodega & ",'" & LvProductosSalida.ListItems(i) & "'," & _
                    Dp_SAnterior & "," & Abs(Dp_Diferencia) & "," & Dp_SAnterior + Val(LvProductosSalida.ListItems(i).SubItems(2)) & ",'" & LogUsuario & "'," & CxP(Dp_PrecioNeto) & ",'" & _
                    "ANULA CAMBIO MERCADERIA" & "'," & CxP(Dp_PrecioNeto * Dp_SAnterior) & "," & _
                    CxP(Abs(Dp_Diferencia) * Dp_PrecioNeto) & "," & Dp_SAnteriorvalor + (CxP(Dp_PrecioNeto) * Val(LvProductosSalida.ListItems(i).SubItems(2))) & ",'" & SP_Rut_Activo & "')"
                
            cn.Execute "UPDATE pro_stock SET sto_stock=" & Dp_SAnterior + Val(LvProductosSalida.ListItems(i).SubItems(2)) & " " & _
                                "WHERE bod_id=" & Ip_IdBodega & " AND rut_emp='" & SP_Rut_Activo & "' AND pro_codigo='" & LvProductosSalida.ListItems(i) & "'"
        
        
        Next
fin:
    cn.CommitTrans
    CmdBuscar_Click
    MsgBox "Anulado", vbInformation
   
    Exit Sub
ErrorCambio:
    
    MsgBox "Error en el proceso..." & vbNewLine & Err.Description, vbInformation
    cn.RollbackTrans
    

End Sub

Private Sub CmdBuscar_Click()
    Me.LvProductosDocumento.ListItems.Clear
    Me.LvProductosSalida.ListItems.Clear
     If ChkFecha.Value = 1 Then
        Sp_Condicion = " AND c.cam_fecha BETWEEN '" & Fql(DtDesde.Value) & "'  AND '" & Fql(DtHasta.Value) & " 23:59' "
    Else
        Sp_Condicion = " AND MONTH(c.cam_fecha)=" & comMes.ItemData(comMes.ListIndex) & " AND YEAR(c.cam_fecha)=" & IIf(ComAno.ListCount = 0, Year(Date), ComAno.Text) & " "
    End If
    CargaCambios
End Sub

Private Sub CmdNuevo_Click()
    ven_cambiomercaderia.Show 1
End Sub

Private Sub cmdSalir_Click()
    Unload Me
End Sub

Private Sub Command1_Click()

End Sub

Private Sub Form_Load()
    Centrar Me
    Aplicar_skin Me
    For i = 1 To 12
        comMes.AddItem UCase(MonthName(i, False))
        comMes.ItemData(comMes.ListCount - 1) = i
    Next
    Busca_Id_Combo comMes, Val(IG_Mes_Contable)
    LLenaYears ComAno, 2010
    DtDesde = Date
    DtHasta = Date
    CmdBuscar_Click
End Sub
Private Sub CargaCambios()
    Sql = "SELECT cam_id, @rownum:=@rownum+1 AS rownum,cam_fecha,cam_usuario,doc_nombre,v.no_documento,DATE_FORMAT(cam_fecha,'%h:%i %p') hora,cam_anulado,c.id " & _
                "FROM (SELECT @rownum:=0) r,ven_cambio_mercaderia c " & _
                "JOIN ven_doc_venta v ON c.id=v.id " & _
                "JOIN sis_documentos d ON v.doc_id=d.doc_id " & _
                "WHERE v.rut_emp='" & SP_Rut_Activo & "' " & Sp_Condicion & _
                "ORDER BY @rownum :=@rownum + 1"
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvDetalle, False, True, True, False

End Sub

Private Sub LvDetalle_Click()
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    Sql = "SELECT pro_codigo,descripcion,cbd_cantidad " & _
            "FROM ven_cambio_mercaderia_detalle d " & _
            "JOIN maestro_productos p ON d.pro_codigo=p.codigo " & _
            "WHERE cam_id=" & LvDetalle.SelectedItem & " AND d.cbd_tipo='ENTRADA'"
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, Me.LvProductosDocumento, False, True, True, False
    
    Sql = "SELECT pro_codigo,descripcion,cbd_cantidad " & _
            "FROM ven_cambio_mercaderia_detalle d " & _
            "JOIN maestro_productos p ON d.pro_codigo=p.codigo " & _
            "WHERE cam_id=" & LvDetalle.SelectedItem & " AND d.cbd_tipo='SALIDA'"
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, Me.LvProductosSalida, False, True, True, False
End Sub
