VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form vta_Facturacion_Guias 
   Caption         =   "Facturacion Guias de Ventas"
   ClientHeight    =   8250
   ClientLeft      =   4050
   ClientTop       =   1545
   ClientWidth     =   14940
   LinkTopic       =   "Form1"
   ScaleHeight     =   8250
   ScaleWidth      =   14940
   Begin VB.Frame FraGiros 
      Caption         =   "Giro de la factura"
      Height          =   765
      Left            =   6660
      TabIndex        =   18
      Top             =   210
      Visible         =   0   'False
      Width           =   4065
      Begin VB.ComboBox CboGiros 
         Height          =   315
         Left            =   75
         Style           =   2  'Dropdown List
         TabIndex        =   19
         Top             =   360
         Width           =   3915
      End
   End
   Begin VB.Frame FrmRs 
      Caption         =   "Facturar como"
      Height          =   765
      Left            =   105
      TabIndex        =   16
      Top             =   210
      Visible         =   0   'False
      Width           =   3420
      Begin VB.ComboBox CboRs 
         Height          =   315
         Left            =   75
         Style           =   2  'Dropdown List
         TabIndex        =   17
         Top             =   360
         Visible         =   0   'False
         Width           =   3300
      End
   End
   Begin VB.TextBox Text2 
      Height          =   285
      Left            =   9870
      TabIndex        =   15
      Text            =   "Text2"
      Top             =   7905
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.TextBox Text1 
      Height          =   285
      Left            =   8055
      TabIndex        =   14
      Text            =   "Text1"
      Top             =   7920
      Visible         =   0   'False
      Width           =   1725
   End
   Begin VB.Frame Frame3 
      Caption         =   "Sucursal"
      Height          =   1200
      Left            =   3510
      TabIndex        =   10
      Top             =   210
      Width           =   7215
      Begin VB.TextBox txtComuna 
         BackColor       =   &H8000000F&
         Height          =   285
         Left            =   5505
         Locked          =   -1  'True
         TabIndex        =   23
         TabStop         =   0   'False
         Top             =   825
         Width           =   1635
      End
      Begin VB.TextBox txtDireccion 
         BackColor       =   &H8000000F&
         Height          =   285
         Left            =   750
         Locked          =   -1  'True
         TabIndex        =   21
         TabStop         =   0   'False
         Top             =   840
         Width           =   4080
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   240
         Left            =   30
         OleObjectBlob   =   "vta_Facturacion_Guias.frx":0000
         TabIndex        =   20
         Top             =   870
         Width           =   795
      End
      Begin VB.ComboBox CboSucursal 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   90
         Style           =   2  'Dropdown List
         TabIndex        =   11
         Top             =   375
         Width           =   2775
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   240
         Left            =   4695
         OleObjectBlob   =   "vta_Facturacion_Guias.frx":0072
         TabIndex        =   22
         Top             =   855
         Width           =   795
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Presentacion de Factura"
      Height          =   765
      Left            =   10695
      TabIndex        =   9
      Top             =   210
      Width           =   4035
      Begin VB.OptionButton Option2 
         Caption         =   "Productos Vendidos"
         Height          =   330
         Left            =   2130
         TabIndex        =   13
         ToolTipText     =   "Resumen de los productos vendidos"
         Top             =   255
         Width           =   1740
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Segun guias 1,2,3...."
         Height          =   330
         Left            =   210
         TabIndex        =   12
         ToolTipText     =   "En la factura aparecera Segun guias Nro 1,2,4,5,6 ..."
         Top             =   240
         Value           =   -1  'True
         Width           =   1815
      End
   End
   Begin VB.Timer Timer1 
      Interval        =   20
      Left            =   0
      Top             =   5325
   End
   Begin VB.Frame Frame2 
      Caption         =   "Guias pendientes de facturacion"
      Height          =   5970
      Left            =   225
      TabIndex        =   1
      Top             =   1530
      Width           =   14580
      Begin VB.TextBox TxtComentario 
         Height          =   285
         Left            =   1725
         TabIndex        =   25
         Tag             =   "T"
         Top             =   4650
         Width           =   12045
      End
      Begin VB.CommandButton cmdFacturar 
         Caption         =   "&Facturar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   510
         Left            =   105
         TabIndex        =   6
         Top             =   5415
         Width           =   1845
      End
      Begin VB.TextBox TxtTotal 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         Height          =   315
         Left            =   10440
         Locked          =   -1  'True
         TabIndex        =   5
         TabStop         =   0   'False
         Tag             =   "N"
         Text            =   "0"
         Top             =   5190
         Width           =   1185
      End
      Begin VB.TextBox TxtIVA 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         Height          =   315
         Left            =   9255
         Locked          =   -1  'True
         TabIndex        =   4
         TabStop         =   0   'False
         Tag             =   "N"
         Text            =   "0"
         Top             =   5190
         Width           =   1185
      End
      Begin VB.TextBox txtTotalNeto 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         Height          =   315
         Left            =   8070
         Locked          =   -1  'True
         TabIndex        =   3
         TabStop         =   0   'False
         Tag             =   "N"
         Text            =   "0"
         Top             =   5190
         Width           =   1185
      End
      Begin VB.CheckBox Check 
         Caption         =   "Check1"
         Height          =   195
         Left            =   315
         TabIndex        =   2
         Top             =   525
         Width           =   165
      End
      Begin MSComctlLib.ListView LvDetalle 
         Height          =   4080
         Left            =   270
         TabIndex        =   7
         Top             =   495
         Width           =   14145
         _ExtentX        =   24950
         _ExtentY        =   7197
         View            =   3
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   10
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "T1000"
            Object.Width           =   529
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "T1300"
            Text            =   "Fecha"
            Object.Width           =   2293
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "N109"
            Text            =   "Nro Guia"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Object.Tag             =   "T800"
            Text            =   "Rut"
            Object.Width           =   2469
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Object.Tag             =   "T3000"
            Text            =   "Cliente"
            Object.Width           =   6703
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   5
            Object.Tag             =   "N100"
            Text            =   "Neto"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   6
            Object.Tag             =   "N100"
            Text            =   "IVA"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   7
            Object.Tag             =   "N100"
            Text            =   "Total"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   8
            Object.Tag             =   "T2000"
            Text            =   "Sucursal"
            Object.Width           =   4410
         EndProperty
         BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   9
            Object.Tag             =   "N109"
            Text            =   "Suc_id"
            Object.Width           =   2540
         EndProperty
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel14 
         Height          =   180
         Index           =   4
         Left            =   6105
         OleObjectBlob   =   "vta_Facturacion_Guias.frx":00DE
         TabIndex        =   8
         Top             =   5220
         Width           =   1860
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel14 
         Height          =   180
         Index           =   0
         Left            =   570
         OleObjectBlob   =   "vta_Facturacion_Guias.frx":015C
         TabIndex        =   24
         Top             =   4665
         Width           =   990
      End
   End
   Begin VB.CommandButton CmdSalir 
      Cancel          =   -1  'True
      Caption         =   "&Retornar"
      Height          =   375
      Left            =   13560
      TabIndex        =   0
      Top             =   7770
      Width           =   1095
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   -30
      OleObjectBlob   =   "vta_Facturacion_Guias.frx":01CE
      Top             =   7365
   End
End
Attribute VB_Name = "vta_Facturacion_Guias"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public D_Fecha As Date
Public L_Plazo As Long
Public I_Doc_Id As Integer
Public L_NDoc As Long
Public S_rut As String
Public S_Nombre As String
Public I_Id_Vendedor As Integer
Public S_documento As String
Public S_NombreVendedor As String
Public I_ID_Rs As Integer

Private Sub CboSucursal_Click()
    If CboSucursal.ListIndex = -1 Then
        LvDetalle.ListItems.Clear
        Exit Sub
    End If
    
    If CboSucursal.Text = "TODAS" Then
        Consulta RsTmp2, SG_codigo2 & ""
    Else
        Consulta RsTmp2, SG_codigo2 & " AND v.suc_id=" & CboSucursal.ItemData(CboSucursal.ListIndex)
    End If
    LLenar_Grilla RsTmp2, Me, LvDetalle, True, True, True, False


    Dim Ip_IdSuc As Integer
    If CboSucursal.ListIndex = -1 Then Exit Sub
    
    Ip_IdSuc = CboSucursal.ItemData(CboSucursal.ListIndex)
    If Ip_IdSuc = 0 Then
        Busca_Id_Combo CboGiros, 0
        Sql = "SELECT direccion,comuna,ciudad " & _
                "FROM maestro_clientes " & _
                "WHERE rut_cliente='" & VtaDirecta.TxtRut & "'"
        Consulta RsTmp2, Sql
        If RsTmp2.RecordCount > 0 Then
            txtDireccion = RsTmp2!direccion
            TxtCiudad = RsTmp2!ciudad
            txtComuna = RsTmp2!comuna
        End If
        Exit Sub
    End If
    Sql = "SELECT suc_direccion,suc_ciudad,suc_contacto suc_comuna,gir_id " & _
          "FROM par_sucursales " & _
          "WHERE suc_id=" & Ip_IdSuc
    Consulta RsTmp2, Sql
    If RsTmp2.RecordCount > 0 Then
        txtDireccion = RsTmp2!suc_direccion
        TxtCiudad = RsTmp2!suc_ciudad
        txtComuna = RsTmp2!suc_comuna
        Busca_Id_Combo CboGiros, Val("" & RsTmp2!gir_id)
    End If

End Sub

Private Sub Check_Click()
    For i = 1 To LvDetalle.ListItems.Count
        If LvDetalle.ListItems(i).Checked Then LvDetalle.ListItems(i).Checked = False Else LvDetalle.ListItems(i).Checked = True
    Next
    SumaGuias
End Sub
Private Sub CmdFacturar_Click()
    Dim L_Abo_id As Long
    Dim L_NVenta As Long
    Dim sp_Guias As String
    Dim Lp_Comision As Long
    Dim Ip_IdGiro As Integer
    If Val(TxtTotal) = 0 Then
        MsgBox "Debe seleccionar al menos una guia...", vbOKOnly + vbInformation
        Exit Sub
    End If
    Ip_IdGiro = 0
    I_ID_Rs = 0
    If CboRs.Visible = True Then
        If CboRs.ListIndex > -1 Then
            I_ID_Rs = CboRs.ItemData(CboRs.ListIndex)
            S_Nombre = CboRs.Text
        End If
    End If
    If FraGiros.Visible Then
        If CboGiros.ListIndex > -1 Then
            Ip_IdGiro = CboGiros.ItemData(CboGiros.ListIndex)
        End If
    End If
    
    If MsgBox("Seguro de emitir Factura por las guias seleccionadas?", vbYesNo + vbQuestion) = vbNo Then Exit Sub
    On Error GoTo ErrorFacturar
    cn.BeginTrans
    
    
        If Me.Option1 Then
            SG_Presentacion_Factura = "SEGUN"
        Else
            SG_Presentacion_Factura = "DETALLE"
        End If
        
        
        
        sp_Guias = "(0"
        L_NVenta = UltimoNro("ven_doc_venta", "id")
        
        
        AutoIncremento "GUARDA", I_Doc_Id, L_NDoc, IG_id_Sucursal_Empresa
        
       '   Sql = "INSERT INTO ven_doc_venta " & _
              "(id,no_documento,fecha,rut_cliente,nombre_cliente,tipo_movimiento,neto,bruto,iva,comentario,CondicionPago," & _
              "ven_id,ven_nombre,ven_comision,forma_pago,tipo_doc,doc_id,suc_id,ven_fecha_vencimiento," & _
              "usu_nombre,id_ref,rut_emp,pla_id,are_id,cen_id,exento,ven_iva_retenido,bod_id,ven_plazo,ven_comentario," & _
              "ven_ordendecompra,ven_tipo_calculo,ven_venta_arriendo,ven_nc_utilizada,caj_id,sue_id,ven_plazo_id," & _
              "ven_entrega_inmediata,gir_id,rso_id,tnc_id,ven_boleta_hasta) " & _
              "VALUES(" & Lp_Id_Nueva_Venta & "," & _
              CDbl(TxtNroDocumento) & ",'" & Format(DtFecha, "YYYY-MM-DD") & "','" & TxtRut & "','" & TxtRazonSocial & "','VD'," & _
              CDbl(SkTotalMateriales) & "," & CDbl(SkBrutoMateriales) & "," & CDbl(SkIvaMateriales) & "," & _
              "'" & TxtMotivoRef & "','" & Sp_Fpago & "'," & _
              CboVendedores.ItemData(CboVendedores.ListIndex) & ",'" & _
              CboVendedores.Text & "'," & _
              TxtComisionVendedor & ",'" & _
              FormaDelPago & "','" & _
              CboDocVenta.Text & "'," & _
              CboDocVenta.ItemData(CboDocVenta.ListIndex) & "," & _
              ip_Sucursal_id & ",'" & Format(DtFecha.Value + CboPlazos.ItemData(CboPlazos.ListIndex), "YYYY-MM-DD") & "','" & _
              LogUsuario & "'," & TxtNroReferencia.Tag & ",'" & SP_Rut_Activo & "'," & Ip_PlaId & "," & Ip_AreId & "," & _
              Ip_CenId & "," & CDbl(TxtExentos) & "," & CDbl(TxtIVARetenido) & "," & IG_id_Bodega_Ventas & _
              "," & CboPlazos.ItemData(CboPlazos.ListIndex) & ",'" & TxtComentario & "','" & Me.TxtOrdenesCompra & _
              "'," & Im_Tcal & ",'" & Sp_Modalidad & "','" & Sp_NC_Utilizada & "'," & LG_id_Caja & "," & IG_id_Sucursal_Empresa & "," & elplazo & ",'" & Me.CboEntregaInmediata.Text & "'," & CboGiros.ItemData(CboGiros.ListIndex) & "," & iP_RS & "," & Ip_TipoNotaCredito & "," & TxtNroDocumento & ")"
            
       elplazo = Right(VtaDirecta.CboPlazos.Text, 5)
        
        Sql = "INSERT INTO ven_doc_venta (id,rut_cliente,nombre_cliente,tipo_doc,no_documento,condicionpago,neto,iva,bruto," & _
                                          "fecha,doc_id,tipo_movimiento,doc_factura_guias,ven_fecha_vencimiento," & _
                                          "ven_id,forma_pago,ven_nombre,usu_nombre,rut_emp,ven_tipo_calculo,sue_id,caj_id,bod_id,rso_id,gir_id,ven_plazo_id,ven_comentario) " & _
              "VALUES (" & L_NVenta & ",'" & S_rut & "','" & S_Nombre & "','" & S_documento & "'," & L_NDoc & ",'CREDITO'," & _
                      CDbl(txtTotalNeto) & "," & CDbl(TxtIVA) & "," & CDbl(TxtTotal) & ",'" & Format(D_Fecha, "YYYY-MM-DD") & "'," & _
                      I_Doc_Id & ",'VD','SI','" & Format(D_Fecha + L_Plazo, "YYYY-MM-DD") & "'," & I_Id_Vendedor & "," & _
                      "'PENDIENTE','" & S_NombreVendedor & "','" & LogUsuario & "','" & SP_Rut_Activo & "'," & _
                      VtaDirecta.Im_Tcal & "," & IG_id_Sucursal_Empresa & "," & LG_id_Caja & "," & IG_id_Bodega_Ventas & "," & I_ID_Rs & "," & Ip_IdGiro & "," & elplazo & ",'" & TxtComentario & "')"
                      
        cn.Execute Sql
        For i = 1 To LvDetalle.ListItems.Count
            If LvDetalle.ListItems(i).Checked Then sp_Guias = sp_Guias & "," & LvDetalle.ListItems(i)
        Next
        sp_Guias = sp_Guias & ")"
        
        Sql = "SELECT SUM(ven_comision) comision " & _
              "FROM ven_doc_venta " & _
              "WHERE  rut_emp='" & SP_Rut_Activo & "' AND  id IN " & sp_Guias
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
            Lp_Comision = RsTmp!comision
        Else
            Lp_Comision = 0
        End If
        
        Sql = "UPDATE ven_doc_venta " & _
              "SET doc_id_factura=" & I_Doc_Id & "," & _
              "nro_factura=" & L_NDoc & ", " & _
              "ven_comision=0 " & _
              "WHERE  rut_emp='" & SP_Rut_Activo & "' AND  id IN " & sp_Guias
        cn.Execute Sql
        
        cn.Execute "UPDATE ven_doc_venta SET ven_comision=" & Lp_Comision & " " & _
                   "WHERE id=" & L_NVenta
        
        
        If CboRs.Visible Then
            Busca_Id_Combo VtaDirecta.CboRs, Val(CboRs.ItemData(CboRs.ListIndex))
            VtaDirecta.TxtRazonSocial = CboRs.Text
            'VtaDirecta.CboRs.ItemData = CboRs.ItemData(CboRs.ListIndex)
        End If
        
        VtaDirecta.S_Facturado = "SI"
        Busca_Id_Combo VtaDirecta.CboGiros, Val(Ip_IdGiro)
        VtaDirecta.txtDireccion = txtDireccion
        VtaDirecta.txtComuna = txtComuna
        For a = 1 To LvDetalle.ListItems.Count
            If LvDetalle.ListItems(a).Checked Then SP_GUIAS2 = SP_GUIAS2 & LvDetalle.ListItems(a).SubItems(2) & " - "
        Next
        VtaDirecta.TxtComentario = "FACTURA POR GUIAS:" & SP_GUIAS2 & " " & TxtComentario
        SP_GUIAS2 = ""
        'Cta Cte 30 de Junio de 2011
        'Debemos consultar los abonos que tengan estas guias y abonarlas a la factura que se esta creando
        'Solo tomar el valor
        
        Sql = "SELECT sum(abo_monto) abonos " & _
              "FROM cta_abonos a,cta_abono_documentos d " & _
              "WHERE a.abo_id=d.abo_id AND abo_cli_pro='CLI' AND abo_rut='" & S_rut & "' AND d.id IN " & sp_Guias & "  " & _
              "GROUP BY abo_rut "
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
            L_Abo_id = UltimoNro("cta_abonos", "abo_id")
        
            Sql = "INSERT INTO cta_abonos (abo_id,abo_cli_pro,abo_rut,abo_fecha,abo_monto,mpa_id,abo_observacion,usu_nombre,rut_emp,caj_id,abo_origen) " & _
                  "VALUES(" & L_Abo_id & ",'CLI','" & S_rut & "','" & Format(D_Fecha, "YYYY-MM-DD") & "'," & RsTmp!abonos & ",5,'Abonos en guias','" & LogUsuario & "','" & SP_Rut_Activo & "'," & LG_id_Caja & ",'FCT')"
            cn.Execute Sql
            Sql = "INSERT INTO cta_abono_documentos (abo_id,id,ctd_monto,rut_emp) " & _
                  "VALUES(" & L_Abo_id & "," & L_NVenta & "," & RsTmp!abonos & ",'" & SP_Rut_Activo & "')"
            cn.Execute Sql
            '19 Agosto 2013
            Sql = "INSERT INTO abo_tipos_de_pagos  (abo_id,mpa_id,pad_valor) " & _
                    "VALUES(" & L_Abo_id & ",5," & RsTmp!abonos & ")"
            cn.Execute Sql
            
        End If
    
    cn.CommitTrans
    Unload Me
    Exit Sub
ErrorFacturar:
    MsgBox "Ocurrio un error al intentar Facturar" & vbNewLine & "Err: " & Err.Number & vbNewLine & Err.Description
    
    
End Sub

Private Sub CmdSalir_Click()
    Unload Me
End Sub



Private Sub Form_Load()
    Aplicar_skin Me
    Centrar Me
    
    LLenarCombo CboGiros, "gir_nombre", "gir_id", "par_clientes_giros", "cli_rut='" & VtaDirecta.TxtRut & "'"
    If CboGiros.ListCount > 1 Then
        CboGiros.AddItem VtaDirecta.TxtGiro, 0
        Busca_Id_Combo CboGiros, 0
        FraGiros.Visible = True
    End If
    LLenarCombo Me.CboRs, "rso_nombre", "rso_id", "par_razon_social_clientes", "rut_cliente='" & VtaDirecta.TxtRut & "'"
    If CboRs.ListCount > 0 Then
        Me.FrmRs.Visible = True
        CboRs.Visible = True
        CboRs.ListIndex = 0
    End If
End Sub


Private Sub LvDetalle_ItemCheck(ByVal Item As MSComctlLib.ListItem)
  '  ordListView ColumnHeader, Me, LvDetalle
    SumaGuias
End Sub
Private Sub SumaGuias()
    If LvDetalle.ListItems.Count = 0 Then Exit Sub
    txtTotalNeto = 0
    TxtIVA = 0
    TxtTotal = 0
    For i = 1 To LvDetalle.ListItems.Count
        If LvDetalle.ListItems(i).Checked Then
            txtTotalNeto = CDbl(txtTotalNeto) + CDbl(LvDetalle.ListItems(i).SubItems(5))
            TxtIVA = CDbl(TxtIVA) + CDbl(LvDetalle.ListItems(i).SubItems(6))
            TxtTotal = CDbl(TxtTotal) + CDbl(LvDetalle.ListItems(i).SubItems(7))
        End If
    Next
    txtTotalNeto = Format(txtTotalNeto, "#,##0")
    TxtIVA = Format(TxtIVA, "#,#")
    TxtTotal = Format(TxtTotal, "#,#")
    TxtTotalFacturaGuias = TxtTotal
    If Val(TxtTotal) = 0 Then Exit Sub
    Text1 = NumFormat(CDbl(TxtTotal) / Val("1." & DG_IVA)) ' neto
    Text2 = NumFormat(CDbl(TxtTotal) - CDbl(Text1)) ' iva
    txtTotalNeto = NumFormat(Text1)
    TxtIVA = NumFormat(Text2)
        
    

End Sub
'ordena columnas
Private Sub LvDetalle_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    ordListView ColumnHeader, Me, LvDetalle
End Sub


Private Sub Timer1_Timer()
    On Error Resume Next
    LvDetalle.SetFocus
    Timer1.Enabled = False
End Sub
