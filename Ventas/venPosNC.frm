VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{28D47522-CF84-11D1-834C-00A0249F0C28}#1.0#0"; "gif89.dll"
Object = "{DA729E34-689F-49EA-A856-B57046630B73}#1.0#0"; "progressbar-xp.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form venPosNC 
   Caption         =   "Emision Nota de Credito Electronica"
   ClientHeight    =   10440
   ClientLeft      =   3885
   ClientTop       =   1635
   ClientWidth     =   17940
   LinkTopic       =   "Form1"
   ScaleHeight     =   10440
   ScaleWidth      =   17940
   Begin VB.TextBox TxtEmpDireccion 
      Height          =   510
      Left            =   18060
      TabIndex        =   89
      Text            =   "Text1"
      Top             =   3510
      Visible         =   0   'False
      Width           =   2640
   End
   Begin VB.TextBox TxtCiudadEmp 
      Height          =   480
      Left            =   17940
      TabIndex        =   88
      Text            =   "Text1"
      Top             =   3510
      Visible         =   0   'False
      Width           =   2430
   End
   Begin VB.TextBox TxtOficinaSII 
      Height          =   420
      Left            =   17940
      TabIndex        =   87
      Text            =   "Text1"
      Top             =   3510
      Visible         =   0   'False
      Width           =   2445
   End
   Begin VB.Frame FrmLoad 
      Height          =   1635
      Left            =   8145
      TabIndex        =   85
      Top             =   4515
      Visible         =   0   'False
      Width           =   2955
      Begin GIF89LibCtl.Gif89a Gif89a1 
         Height          =   1275
         Left            =   165
         OleObjectBlob   =   "venPosNC.frx":0000
         TabIndex        =   86
         Top             =   210
         Width           =   2655
      End
   End
   Begin VB.TextBox TxtVendedorID 
      Height          =   300
      Left            =   17850
      TabIndex        =   84
      Text            =   "Text1"
      Top             =   3510
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.Frame FraProcesandoDTE 
      Height          =   1485
      Left            =   2355
      TabIndex        =   68
      Top             =   6315
      Visible         =   0   'False
      Width           =   13020
      Begin VB.Timer Timer2 
         Enabled         =   0   'False
         Interval        =   20
         Left            =   2940
         Top             =   930
      End
      Begin Proyecto2.XP_ProgressBar XP_ProgressBar1 
         Height          =   255
         Left            =   4050
         TabIndex        =   70
         Top             =   975
         Width           =   4335
         _ExtentX        =   7646
         _ExtentY        =   450
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BrushStyle      =   0
         Color           =   16750899
         Scrolling       =   2
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel13 
         Height          =   510
         Left            =   1350
         OleObjectBlob   =   "venPosNC.frx":0086
         TabIndex        =   69
         Top             =   420
         Width           =   10275
      End
   End
   Begin VB.CommandButton CmdAceptar 
      Caption         =   "&Generar Nota de Credito"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   705
      Left            =   5940
      TabIndex        =   41
      Top             =   9360
      Width           =   4620
   End
   Begin VB.TextBox TxtNroManual 
      Height          =   285
      Left            =   3660
      TabIndex        =   40
      Top             =   9810
      Width           =   2175
   End
   Begin VB.CheckBox Check1 
      Caption         =   "Electronica"
      Height          =   255
      Left            =   2325
      TabIndex        =   39
      Top             =   9900
      Value           =   1  'Checked
      Width           =   1155
   End
   Begin VB.CommandButton CmdSalir 
      Caption         =   "&Retornar"
      Height          =   405
      Left            =   495
      TabIndex        =   38
      Top             =   9765
      Width           =   1680
   End
   Begin VB.Frame FrmNV 
      Caption         =   "Datos para nota de credito"
      Height          =   10335
      Left            =   240
      TabIndex        =   7
      Top             =   0
      Width           =   17475
      Begin VB.TextBox TxtComentario 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   750
         TabIndex        =   75
         Text            =   "Escriba aqui comentario para la NC"
         Top             =   8925
         Width           =   9645
      End
      Begin VB.Frame FrameSelecciona 
         Caption         =   "Caja/Abono CtaCte"
         Height          =   660
         Left            =   13080
         TabIndex        =   66
         Top             =   4065
         Width           =   4080
         Begin VB.ComboBox CboCajaAbono 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            ItemData        =   "venPosNC.frx":014E
            Left            =   255
            List            =   "venPosNC.frx":0155
            Style           =   2  'Dropdown List
            TabIndex        =   67
            ToolTipText     =   "Seleccione Documento de  venta. Ventas con Retenci�n, cuando el contribuyente recibe factura de terceros "
            Top             =   255
            Width           =   3585
         End
      End
      Begin VB.Frame FraProductos 
         Caption         =   "Productos"
         Enabled         =   0   'False
         Height          =   4050
         Left            =   645
         TabIndex        =   46
         Top             =   4710
         Width           =   15585
         Begin VB.CheckBox ChkProductos 
            Caption         =   "Check2"
            Height          =   285
            Left            =   420
            TabIndex        =   60
            Top             =   1260
            Width           =   270
         End
         Begin VB.TextBox TxtDescripcion 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H00E0E0E0&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   15.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   465
            Left            =   2790
            MultiLine       =   -1  'True
            TabIndex        =   53
            TabStop         =   0   'False
            Top             =   720
            Width           =   6855
         End
         Begin VB.TextBox TxtCantidad 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   18
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   465
            Left            =   9690
            TabIndex        =   52
            Text            =   "1"
            Top             =   720
            Width           =   1380
         End
         Begin VB.TextBox TxtPrecio 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   18
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   465
            Left            =   11085
            TabIndex        =   51
            Text            =   "0"
            Top             =   720
            Width           =   1815
         End
         Begin VB.TextBox TxtTotalLinea 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00E0E0E0&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   18
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   465
            Left            =   12930
            Locked          =   -1  'True
            TabIndex        =   50
            TabStop         =   0   'False
            Text            =   "0"
            Top             =   720
            Width           =   1650
         End
         Begin VB.CommandButton CmdAceptaLinea 
            Appearance      =   0  'Flat
            BackColor       =   &H8000000A&
            Caption         =   "Ok"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   465
            Left            =   14610
            TabIndex        =   49
            Top             =   720
            Width           =   720
         End
         Begin VB.TextBox TxtCodigo 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   18
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   465
            Left            =   390
            TabIndex        =   48
            Top             =   720
            Width           =   2370
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkUbicacion 
            Height          =   300
            Left            =   3855
            OleObjectBlob   =   "venPosNC.frx":0166
            TabIndex        =   47
            Top             =   585
            Width           =   5760
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
            Height          =   240
            Index           =   8
            Left            =   390
            OleObjectBlob   =   "venPosNC.frx":01C3
            TabIndex        =   54
            Top             =   435
            Width           =   660
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
            Height          =   225
            Index           =   9
            Left            =   2805
            OleObjectBlob   =   "venPosNC.frx":0225
            TabIndex        =   55
            Top             =   495
            Width           =   1530
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
            Height          =   240
            Index           =   3
            Left            =   9660
            OleObjectBlob   =   "venPosNC.frx":0291
            TabIndex        =   56
            Top             =   480
            Width           =   1395
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
            Height          =   240
            Index           =   10
            Left            =   11220
            OleObjectBlob   =   "venPosNC.frx":02F7
            TabIndex        =   57
            Top             =   480
            Width           =   1635
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
            Height          =   240
            Index           =   12
            Left            =   13155
            OleObjectBlob   =   "venPosNC.frx":0359
            TabIndex        =   58
            Top             =   480
            Width           =   1440
         End
         Begin MSComctlLib.ListView LvVenta 
            Height          =   2430
            Left            =   375
            TabIndex        =   59
            Top             =   1260
            Width           =   14955
            _ExtentX        =   26379
            _ExtentY        =   4286
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            FullRowSelect   =   -1  'True
            GridLines       =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483646
            BackColor       =   -2147483643
            Appearance      =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   14.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            NumItems        =   9
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Object.Tag             =   "N109"
               Object.Width           =   529
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Object.Tag             =   "T1000"
               Text            =   "Codigo"
               Object.Width           =   3660
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   2
               Object.Tag             =   "T3000"
               Text            =   "Descripci�n"
               Object.Width           =   12153
            EndProperty
            BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   3
               Object.Tag             =   "N102"
               Text            =   "Cantidad"
               Object.Width           =   2443
            EndProperty
            BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   4
               Object.Tag             =   "N102"
               Text            =   "Precio"
               Object.Width           =   3201
            EndProperty
            BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   5
               Key             =   "total"
               Object.Tag             =   "N100"
               Text            =   "Total"
               Object.Width           =   3201
            EndProperty
            BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   6
               Object.Tag             =   "N109"
               Text            =   "Stock"
               Object.Width           =   0
            EndProperty
            BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   7
               Object.Tag             =   "T1000"
               Text            =   "Inventario"
               Object.Width           =   0
            EndProperty
            BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   8
               Object.Tag             =   "109"
               Text            =   "Precio Costo"
               Object.Width           =   0
            EndProperty
         End
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   255
         Index           =   1
         Left            =   7440
         OleObjectBlob   =   "venPosNC.frx":03B9
         TabIndex        =   45
         Top             =   4380
         Width           =   1905
      End
      Begin VB.ComboBox CboModalidadNC 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         ItemData        =   "venPosNC.frx":0449
         Left            =   9420
         List            =   "venPosNC.frx":0456
         Style           =   2  'Dropdown List
         TabIndex        =   44
         ToolTipText     =   "Seleccione Documento de  venta. Ventas con Retenci�n, cuando el contribuyente recibe factura de terceros "
         Top             =   4305
         Width           =   2865
      End
      Begin VB.Frame Frame2 
         Caption         =   "Total Nota de Credito"
         Height          =   1425
         Left            =   10515
         TabIndex        =   42
         Top             =   8805
         Width           =   6480
         Begin VB.CommandButton CmdSignoMas 
            Caption         =   "ajustar"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Left            =   4335
            TabIndex        =   83
            Top             =   1095
            Width           =   1950
         End
         Begin VB.TextBox TxtOtrosImpuestos 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00C0E0FF&
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Left            =   1560
            TabIndex        =   71
            Text            =   "0"
            ToolTipText     =   "Total NC"
            Top             =   735
            Width           =   1470
         End
         Begin VB.TextBox TxtSubTotal 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00C0E0FF&
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   4260
            TabIndex        =   65
            Text            =   "0"
            ToolTipText     =   "Total NC"
            Top             =   195
            Width           =   2085
         End
         Begin VB.TextBox TxtDescuentoNC 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00C0E0FF&
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   4260
            TabIndex        =   63
            Text            =   "0"
            ToolTipText     =   "Total NC"
            Top             =   510
            Width           =   2085
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel5 
            Height          =   195
            Left            =   2535
            OleObjectBlob   =   "venPosNC.frx":0494
            TabIndex        =   61
            Top             =   450
            Width           =   1530
         End
         Begin VB.TextBox TxtTotalNC 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H0080FF80&
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   13.5
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   4245
            TabIndex        =   43
            Text            =   "0"
            ToolTipText     =   "Total NC"
            Top             =   810
            Width           =   2085
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel6 
            Height          =   195
            Left            =   2565
            OleObjectBlob   =   "venPosNC.frx":0506
            TabIndex        =   62
            Top             =   780
            Width           =   1500
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel7 
            Height          =   195
            Left            =   2535
            OleObjectBlob   =   "venPosNC.frx":056E
            TabIndex        =   64
            Top             =   150
            Width           =   1530
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel8 
            Height          =   195
            Left            =   180
            OleObjectBlob   =   "venPosNC.frx":05DC
            TabIndex        =   72
            Top             =   825
            Width           =   1290
         End
      End
      Begin VB.Frame Frame6 
         Caption         =   "Documento Referencia"
         Height          =   3600
         Left            =   7380
         TabIndex        =   32
         Top             =   375
         Width           =   9885
         Begin VB.Frame Frame3 
            Caption         =   "Datos de pago del documento seleccionado"
            Height          =   1230
            Left            =   120
            TabIndex        =   76
            Top             =   2280
            Width           =   9480
            Begin VB.TextBox TxtFpagoDetectada 
               Appearance      =   0  'Flat
               BackColor       =   &H00C0C0C0&
               BorderStyle     =   0  'None
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   195
               Locked          =   -1  'True
               TabIndex        =   77
               ToolTipText     =   "Ingrese el RUT sin puntos ni guion."
               Top             =   600
               Width           =   3390
            End
            Begin ACTIVESKINLibCtl.SkinLabel SkinLabel19 
               Height          =   255
               Index           =   4
               Left            =   180
               OleObjectBlob   =   "venPosNC.frx":0658
               TabIndex        =   78
               Top             =   405
               Width           =   2730
            End
            Begin MSComctlLib.ListView LvCheques 
               Height          =   990
               Left            =   3885
               TabIndex        =   79
               Top             =   180
               Visible         =   0   'False
               Width           =   5400
               _ExtentX        =   9525
               _ExtentY        =   1746
               View            =   3
               LabelEdit       =   1
               LabelWrap       =   0   'False
               HideSelection   =   0   'False
               FullRowSelect   =   -1  'True
               GridLines       =   -1  'True
               _Version        =   393217
               ForeColor       =   -2147483640
               BackColor       =   -2147483643
               BorderStyle     =   1
               Appearance      =   1
               NumItems        =   4
               BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                  Object.Tag             =   "N109"
                  Text            =   "che_id"
                  Object.Width           =   0
               EndProperty
               BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                  SubItemIndex    =   1
                  Object.Tag             =   "T1000"
                  Text            =   "Banco"
                  Object.Width           =   3175
               EndProperty
               BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                  SubItemIndex    =   2
                  Object.Tag             =   "N109"
                  Text            =   "Nro Cheque"
                  Object.Width           =   1940
               EndProperty
               BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                  Alignment       =   1
                  SubItemIndex    =   3
                  Key             =   "total"
                  Object.Tag             =   "N100"
                  Text            =   "Total"
                  Object.Width           =   2293
               EndProperty
            End
         End
         Begin VB.CommandButton CmdModificaValor 
            Caption         =   "Modificar Valor"
            Height          =   285
            Left            =   8040
            TabIndex        =   73
            Top             =   1890
            Visible         =   0   'False
            Width           =   1410
         End
         Begin MSComCtl2.DTPicker DtFechaBoleta 
            Height          =   315
            Left            =   5985
            TabIndex        =   4
            Top             =   480
            Width           =   1200
            _ExtentX        =   2117
            _ExtentY        =   556
            _Version        =   393216
            Format          =   96468993
            CurrentDate     =   42279
         End
         Begin VB.ComboBox CboDocVenta 
            Height          =   315
            ItemData        =   "venPosNC.frx":06E4
            Left            =   165
            List            =   "venPosNC.frx":06E6
            Style           =   2  'Dropdown List
            TabIndex        =   2
            Top             =   495
            Width           =   4170
         End
         Begin VB.TextBox txtNroDocumentoBuscar 
            BackColor       =   &H8000000E&
            Height          =   315
            Left            =   4335
            TabIndex        =   3
            Top             =   495
            Width           =   1650
         End
         Begin VB.TextBox txtTotalDocBuscado 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   7170
            TabIndex        =   5
            TabStop         =   0   'False
            Top             =   495
            Width           =   1305
         End
         Begin VB.CommandButton cmdBuscar 
            Caption         =   "Buscar"
            Height          =   315
            Left            =   8475
            TabIndex        =   6
            Top             =   480
            Width           =   1125
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
            Height          =   210
            Index           =   9
            Left            =   180
            OleObjectBlob   =   "venPosNC.frx":06E8
            TabIndex        =   33
            Top             =   315
            Width           =   1980
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
            Height          =   210
            Index           =   10
            Left            =   4365
            OleObjectBlob   =   "venPosNC.frx":0758
            TabIndex        =   34
            Top             =   300
            Width           =   1020
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
            Height          =   195
            Index           =   11
            Left            =   6090
            OleObjectBlob   =   "venPosNC.frx":07BC
            TabIndex        =   35
            Top             =   285
            Width           =   870
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
            Height          =   210
            Index           =   12
            Left            =   7215
            OleObjectBlob   =   "venPosNC.frx":0824
            TabIndex        =   36
            Top             =   315
            Width           =   1185
         End
         Begin MSComctlLib.ListView LvDetalle 
            Height          =   1410
            Left            =   150
            TabIndex        =   37
            Top             =   855
            Width           =   9480
            _ExtentX        =   16722
            _ExtentY        =   2487
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   0   'False
            HideSelection   =   0   'False
            FullRowSelect   =   -1  'True
            GridLines       =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   9
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Object.Tag             =   "N109"
               Text            =   "Id Unico"
               Object.Width           =   0
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Object.Tag             =   "T1000"
               Text            =   "Documento"
               Object.Width           =   7355
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   2
               Object.Tag             =   "N109"
               Text            =   "Nro"
               Object.Width           =   2910
            EndProperty
            BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   3
               Object.Tag             =   "F1000"
               Text            =   "Fecha"
               Object.Width           =   2117
            EndProperty
            BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   4
               Key             =   "total"
               Object.Tag             =   "N100"
               Text            =   "Total"
               Object.Width           =   2293
            EndProperty
            BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   5
               Object.Tag             =   "N109"
               Text            =   "doc_id"
               Object.Width           =   0
            EndProperty
            BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   6
               Object.Tag             =   "N109"
               Text            =   "doc_sii"
               Object.Width           =   0
            EndProperty
            BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   7
               Text            =   "Factura Por Guias"
               Object.Width           =   0
            EndProperty
            BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   8
               Text            =   "Neto-Bruto"
               Object.Width           =   0
            EndProperty
         End
      End
      Begin VB.Frame Frame7 
         Caption         =   "Cliente"
         Height          =   2940
         Left            =   405
         TabIndex        =   9
         Top             =   1680
         Width           =   6840
         Begin VB.CommandButton CmdBuscarBuscar 
            Caption         =   "Buscar"
            Height          =   315
            Left            =   5535
            TabIndex        =   82
            Top             =   465
            Width           =   1155
         End
         Begin VB.TextBox CboNroBuscar 
            BackColor       =   &H8000000E&
            Height          =   315
            Left            =   3855
            TabIndex        =   81
            Top             =   480
            Width           =   1650
         End
         Begin VB.ComboBox CboDocBuscar 
            Height          =   315
            ItemData        =   "venPosNC.frx":0896
            Left            =   3855
            List            =   "venPosNC.frx":0898
            Style           =   2  'Dropdown List
            TabIndex        =   80
            Top             =   150
            Width           =   2865
         End
         Begin VB.TextBox TxtRazonSocial 
            Appearance      =   0  'Flat
            BackColor       =   &H00C0C0C0&
            BorderStyle     =   0  'None
            Height          =   285
            Left            =   900
            TabIndex        =   19
            ToolTipText     =   "Ingrese el RUT sin puntos ni guion."
            Top             =   795
            Width           =   5730
         End
         Begin VB.CommandButton CmdBuscaCliente 
            Caption         =   "F1 - Buscar"
            Height          =   255
            Left            =   900
            TabIndex        =   18
            Top             =   195
            Width           =   945
         End
         Begin VB.TextBox TxtRut 
            Appearance      =   0  'Flat
            BackColor       =   &H00C0C0C0&
            BorderStyle     =   0  'None
            Height          =   285
            Left            =   900
            TabIndex        =   17
            ToolTipText     =   "Ingrese el RUT sin puntos ni guion."
            Top             =   465
            Width           =   1395
         End
         Begin VB.TextBox TxtGiro 
            Appearance      =   0  'Flat
            BackColor       =   &H00C0C0C0&
            BorderStyle     =   0  'None
            Height          =   285
            Left            =   900
            TabIndex        =   16
            ToolTipText     =   "Ingrese el RUT sin puntos ni guion."
            Top             =   1125
            Width           =   5715
         End
         Begin VB.TextBox TxtDireccion 
            Appearance      =   0  'Flat
            BackColor       =   &H00C0C0C0&
            BorderStyle     =   0  'None
            Height          =   285
            Left            =   900
            TabIndex        =   15
            ToolTipText     =   "Ingrese el RUT sin puntos ni guion."
            Top             =   1455
            Width           =   5670
         End
         Begin VB.TextBox txtComuna 
            Appearance      =   0  'Flat
            BackColor       =   &H00C0C0C0&
            BorderStyle     =   0  'None
            Height          =   285
            Left            =   900
            TabIndex        =   14
            ToolTipText     =   "Ingrese el RUT sin puntos ni guion."
            Top             =   1785
            Width           =   2160
         End
         Begin VB.TextBox txtFono 
            Appearance      =   0  'Flat
            BackColor       =   &H00C0C0C0&
            BorderStyle     =   0  'None
            Height          =   285
            Left            =   885
            TabIndex        =   13
            ToolTipText     =   "Ingrese el RUT sin puntos ni guion."
            Top             =   2115
            Width           =   2175
         End
         Begin VB.TextBox TxtEmail 
            Appearance      =   0  'Flat
            BackColor       =   &H00C0C0C0&
            BorderStyle     =   0  'None
            Height          =   285
            Left            =   885
            TabIndex        =   12
            ToolTipText     =   "Ingrese el RUT sin puntos ni guion."
            Top             =   2445
            Width           =   3855
         End
         Begin VB.TextBox TxtMontoCredito 
            Height          =   255
            Left            =   6420
            TabIndex        =   11
            Text            =   "Text1"
            Top             =   2040
            Visible         =   0   'False
            Width           =   1995
         End
         Begin VB.TextBox TxtCupoUtilizado 
            Height          =   285
            Left            =   6405
            TabIndex        =   10
            Text            =   "Text1"
            Top             =   2370
            Visible         =   0   'False
            Width           =   2010
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkCupoCredito 
            Height          =   270
            Left            =   4890
            OleObjectBlob   =   "venPosNC.frx":089A
            TabIndex        =   20
            Top             =   1800
            Width           =   1020
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
            Height          =   240
            Index           =   0
            Left            =   345
            OleObjectBlob   =   "venPosNC.frx":08FA
            TabIndex        =   21
            Top             =   495
            Width           =   495
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
            Height          =   210
            Index           =   2
            Left            =   180
            OleObjectBlob   =   "venPosNC.frx":0964
            TabIndex        =   22
            Top             =   840
            Width           =   660
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
            Height          =   255
            Index           =   1
            Left            =   -225
            OleObjectBlob   =   "venPosNC.frx":09CE
            TabIndex        =   23
            Top             =   1470
            Width           =   1065
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel19 
            Height          =   255
            Index           =   0
            Left            =   -15
            OleObjectBlob   =   "venPosNC.frx":0A3E
            TabIndex        =   24
            Top             =   1815
            Width           =   855
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel18 
            Height          =   255
            Left            =   420
            OleObjectBlob   =   "venPosNC.frx":0AA8
            TabIndex        =   25
            Top             =   1155
            Width           =   420
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel19 
            Height          =   255
            Index           =   1
            Left            =   -45
            OleObjectBlob   =   "venPosNC.frx":0B0E
            TabIndex        =   26
            Top             =   2145
            Width           =   855
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel19 
            Height          =   255
            Index           =   2
            Left            =   -120
            OleObjectBlob   =   "venPosNC.frx":0B74
            TabIndex        =   27
            Top             =   2445
            Width           =   930
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel19 
            Height          =   255
            Index           =   3
            Left            =   3810
            OleObjectBlob   =   "venPosNC.frx":0BDC
            TabIndex        =   28
            Top             =   1800
            Width           =   1035
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "Documento Actual"
         Height          =   1290
         Left            =   420
         TabIndex        =   8
         Top             =   375
         Width           =   6750
         Begin MSComCtl2.DTPicker DtFecha 
            Height          =   285
            Left            =   5310
            TabIndex        =   1
            Top             =   900
            Width           =   1395
            _ExtentX        =   2461
            _ExtentY        =   503
            _Version        =   393216
            Format          =   96468993
            CurrentDate     =   42269
         End
         Begin VB.TextBox TxtDocActual 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H00C0C0C0&
            BorderStyle     =   0  'None
            Height          =   285
            Left            =   2520
            TabIndex        =   29
            Text            =   "(auto)"
            ToolTipText     =   "Nro NC"
            Top             =   450
            Width           =   3225
         End
         Begin VB.ComboBox CboNC 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            ItemData        =   "venPosNC.frx":0C54
            Left            =   225
            List            =   "venPosNC.frx":0C56
            Style           =   2  'Dropdown List
            TabIndex        =   0
            ToolTipText     =   "Seleccione Documento de  venta. Ventas con Retenci�n, cuando el contribuyente recibe factura de terceros "
            Top             =   840
            Width           =   4155
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
            Height          =   300
            Index           =   2
            Left            =   180
            OleObjectBlob   =   "venPosNC.frx":0C58
            TabIndex        =   30
            Top             =   465
            Width           =   2250
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
            Height          =   300
            Index           =   3
            Left            =   4275
            OleObjectBlob   =   "venPosNC.frx":0CC8
            TabIndex        =   31
            Top             =   900
            Width           =   960
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkFoliosDisponibles 
            Height          =   195
            Left            =   2445
            OleObjectBlob   =   "venPosNC.frx":0D28
            TabIndex        =   74
            Top             =   180
            Width           =   3660
         End
      End
   End
   Begin VB.Timer Timer1 
      Interval        =   50
      Left            =   0
      Top             =   0
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   60
      OleObjectBlob   =   "venPosNC.frx":0D8A
      Top             =   5220
   End
End
Attribute VB_Name = "venPosNC"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public lP_Documento_Referenciado As Long
Public iP_Tipo_NC As Integer
Dim Sm_Nc_Boleta As String * 2
Dim Lp_Id_Venta As Long
Dim Bm_AfectarCaja As Boolean
Dim Sm_Sql_Productos_NC As String
Dim Lp_Folio As Long
Dim Im_TipoNC As Integer
Dim Im_id_Doc_SII As Integer
Dim Sp_BoletaSinDetalle As Boolean
Dim Sm_BuscaPorNumero As String * 2
Dim Sp_FpagoTermica As String
Private Declare Function ShellExecute Lib "shell32.dll" Alias "ShellExecuteA" (ByVal hWnd As Long, ByVal lpOperation As String, ByVal lpFile As String, ByVal lpParameters As String, ByVal lpDirectory As String, ByVal nShowCmd As Long) As Long




Private Sub CboModalidadNC_Click()
    Im_TipoNC = 1
    TxtSubTotal = "0"
    TxtDescuentoNC = "0"
    If LvDetalle.ListItems.Count = 0 Then Exit Sub
    If CboModalidadNC.ListIndex = 0 Then
        FraProductos.Enabled = False
        LvDetalle_Click
        SumaSeleccion
        BuscaDescuentoNC
        
    ElseIf CboModalidadNC.ListIndex = 1 Then
    
        
        Im_TipoNC = 3
        FraProductos.Enabled = True
        
        
        SumaSeleccion
    
    ElseIf CboModalidadNC.ListIndex = 2 Then
        '
        Im_TipoNC = 2
        TxtSubTotal = 0
        TxtDescuentoNC = 0
        TxtTotalNC = 0
        TxtOtrosImpuestos = 0
    Else
        'Ingreso manual
        LvVenta.ListItems.Clear
        FraProductos.Enabled = True
        TxtSubTotal = 0
        TxtDescuentoNC = 0
        TxtTotalNC = 0
    End If
End Sub
Private Sub BuscaDescuentoNC()
        Dim Dp_xCien As Double

        'busca descuentos del documento que no estaba considerando
        '19 Marzo,
        Sql = "SELECT ven_descuento_valor+ven_ajuste_recargo+ven_ajuste_descuento descuento,bruto " & _
                "FROM ven_doc_venta " & _
                "WHERE no_documento =" & LvDetalle.ListItems(1).SubItems(2) & " AND doc_id = " & LvDetalle.ListItems(1).SubItems(5) & " AND rut_emp='" & SP_Rut_Activo & "'"
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
                TxtSubTotal = NumFormat(RsTmp!bruto + RsTmp!Descuento)
                TxtDescuentoNC = NumFormat(RsTmp!Descuento)
                TxtTotalNC = NumFormat(CDbl(TxtSubTotal) - RsTmp!Descuento)
                
        
        End If
        
        If CDbl(TxtDescuentoNC) > 0 Then
                Dp_xCien = CDbl(TxtDescuentoNC) / CDbl(TxtSubTotal)
                For i = 1 To LvVenta.ListItems.Count
                    LvVenta.ListItems(i).SubItems(4) = NumFormat(Round(LvVenta.ListItems(i).SubItems(4) - (CDbl(LvVenta.ListItems(i).SubItems(4)) * Dp_xCien)))
                    LvVenta.ListItems(i).SubItems(5) = NumFormat(CDbl(LvVenta.ListItems(i).SubItems(4)) * CDbl(LvVenta.ListItems(i).SubItems(3)))
                
                Next
        
        
        End If
End Sub




Private Sub CboNC_Click()
    Dim Rp_DocVenta As Recordset
    If CboNC.ListIndex = -1 Then Exit Sub
    
    Sql = "SELECT doc_nc_por_boleta NC " & _
            "FROM sis_documentos " & _
            "WHERE doc_id=" & CboNC.ItemData(CboNC.ListIndex)
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        If RsTmp!NC = "SI" Then
            LLenarCombo CboDocVenta, "doc_nombre", "doc_id", "sis_documentos", "doc_activo='SI' AND doc_boleta_venta='SI' AND doc_dte='SI'"
             Sm_Nc_Boleta = "SI"
            
        Else
            LLenarCombo CboDocVenta, "doc_nombre", "doc_id", "sis_documentos", "doc_dte='SI' AND doc_activo='SI' AND doc_documento='VENTA' AND doc_nota_de_credito='NO' AND doc_contable='SI' AND doc_nota_de_venta='NO'"
            Sm_Nc_Boleta = "NO"
        End If
    End If
    
    If SP_Rut_Activo = "78.967.170-2" Then
           LLenarCombo CboDocVenta, "doc_nombre", "doc_id", "sis_documentos", "doc_activo='SI' AND doc_documento='VENTA'  AND doc_nota_de_credito='NO' AND doc_contable='SI' "
    End If

    If CboNC.ListIndex = -1 Then Exit Sub
        Ip_DocIdDocumento = CboNC.ItemData(CboNC.ListIndex)
  
        Sql = "SELECT doc_dte,doc_cod_sii " & _
                "FROM sis_documentos " & _
                "WHERE doc_id=" & Ip_DocIdDocumento
        Consulta Rp_DocVenta, Sql
        If Rp_DocVenta.RecordCount > 0 Then
            If Rp_DocVenta!doc_dte = "SI" Then
                Sql = "SELECT dte_id,dte_folio numero " & _
                    "FROM dte_folios " & _
                    "WHERE rut_emp='" & SP_Rut_Activo & "' AND doc_id=" & Rp_DocVenta!doc_cod_sii & " AND dte_venta_compra='VENTA' AND dte_disponible='SI' " & _
                    "ORDER BY dte_folio " & _
                    "LIMIT 1"
                Consulta Rp_DocVenta, Sql
                TxtNroDocumento = 0
                If Rp_DocVenta.RecordCount > 0 Then
                    TxtNroDocumento = Rp_DocVenta!Numero
                Else
                    Me.SkFoliosDisponibles = "No quedan folios disponibles.."
                End If
            End If
        End If


End Sub

Private Sub CboNroBuscar_GotFocus()
    En_Foco CboNroBuscar
End Sub

Private Sub CboNroBuscar_KeyPress(KeyAscii As Integer)
    KeyAscii = AceptaSoloNumeros(KeyAscii)
    If KeyAscii = 13 Then
        CmdBuscarBuscar_Click
        
    End If
End Sub

Private Sub ChkProductos_Click()
    If ChkProductos.Value = 1 Then
        For i = 1 To LvVenta.ListItems.Count
            LvVenta.ListItems(i).Checked = True
        Next
    Else
    
        For i = 1 To LvVenta.ListItems.Count
            LvVenta.ListItems(i).Checked = False
        Next
    
    End If
    SumaSeleccion
End Sub

Private Sub SumaSeleccion()
    TxtTotalNC = 0
    For i = 1 To LvVenta.ListItems.Count
        
        If LvVenta.ListItems(i).Checked Then TxtTotalNC = NumFormat(CDbl(TxtTotalNC) + CDbl(LvVenta.ListItems(i).SubItems(5)))
    Next

    If LvDetalle.ListItems(1).SubItems(8) = "NETO" Then
    
        TxtTotalNC = NumFormat(CDbl(TxtTotalNC) * Val("1." & DG_IVA))
    End If
End Sub

Private Sub CmdAceptaLinea_Click()
    Dim p As Integer
    If Len(TxtCodigo) = 0 Then
        MsgBox "Ingrese c�digo...", vbInformation
        On Error Resume Next
        TxtCodigo.SetFocus
        Exit Sub
    End If
    
    If Len(TxtDescripcion) = 0 Then
        MsgBox "Faltan datos para agregar linea...", vbInformation
        TxtCodigo.SetFocus
        Exit Sub
    End If
    If Val(CxP(TxtCantidad)) = 0 Then
        MsgBox "Falta cantidad...", vbInformation
        TxtCantidad.SetFocus
        Exit Sub
    End If
    If Val(TxtPrecio) = 0 Then
        MsgBox "Falta precio...", vbInformation
        TxtPrecio.SetFocus
        Exit Sub
    End If
    
    LvVenta.ListItems.Add , , TxtCodigo.Tag
    p = LvVenta.ListItems.Count
    LvVenta.ListItems(p).SubItems(1) = TxtCodigo
    LvVenta.ListItems(p).SubItems(2) = TxtDescripcion
    LvVenta.ListItems(p).SubItems(3) = TxtCantidad
    LvVenta.ListItems(p).SubItems(4) = TxtPrecio
    
    If Sm_VentaRapida = "SI" Then
        LvVenta.ListItems(p).SubItems(5) = Round(CDbl(TxtPrecio) * Val(TxtCantidad), 0)
    Else
        LvVenta.ListItems(p).SubItems(5) = TxtTotalLinea
    End If
    LvVenta.ListItems(p).SubItems(6) = TxtStockLinea
    LvVenta.ListItems(p).SubItems(7) = SkInventariable
  '  LvVenta.ListItems(p).SubItems(8) = Val(TxtStockLinea.Tag)
    'LimpiaTxt
    'CalculaGrilla
    
    LvVenta.ListItems(LvVenta.ListItems.Count).Selected = True
    LvVenta.ListItems(LvVenta.ListItems.Count).EnsureVisible
    TxtTotalNC = NumFormat(TotalizaColumna(LvVenta, "total"))
    TxtCodigo = ""
    TxtDescripcion = ""
    TxtCantidad = ""
    TxtPrecio = ""
    SumaSeleccion
    
    TxtCodigo.SetFocus
    
End Sub

Private Sub CmdAceptar_Click()
    Dim Lp_SaldoDoc As Double
    Dim Sp_MontoACtaCte As String
    If LvVenta.ListItems.Count = 0 And Sm_Nc_Boleta = "NO" Then Exit Sub
    If Me.TxtRut = "11.111.111-1" Then
        MsgBox "Rut no v�lido para emitir Nota de Credito!!", vbExclamation
        CboNC.SetFocus
        Exit Sub
    End If
    If CboNC.ListIndex = -1 Then
        MsgBox "Seleccione NC...", vbInformation
        CboNC.SetFocus
        Exit Sub
    End If
    If LvDetalle.ListItems.Count = 0 And Me.CboModalidadNC.ListIndex <> 2 Then
        MsgBox "No hay documento referenciado...", vbInformation
        CboDocVenta.SetFocus
        Exit Sub
    End If
    If Len(TxtRut) = 0 Then
        MsgBox "Debe seleccionar/ingresar cliente...", vbInformation
        TxtRut.SetFocus
        Exit Sub
    End If
    If CDbl(TxtTotalNC) = 0 And CboModalidadNC.ListIndex <> 2 Then
        MsgBox "Haga clic en el documento de la grilla..."
        LvDetalle.SetFocus
        Exit Sub
    End If
    If CboCajaAbono.ListIndex = -1 And CboModalidadNC.ListIndex <> 2 Then
        MsgBox "Seleccione tipo devolucion $", vbInformation
        CboCajaAbono.SetFocus
        Exit Sub
    End If
    Sp_MontoACtaCte = "NO"
  '  If CboCajaAbono.ListIndex = 0 Then
  '      MsgBox "Seleccione Accion de NC en caja...", vbInformation
  '      CboCajaAbono.SetFocus
  '      Exit Sub
  '  Else
    If CboModalidadNC.ListIndex <> 2 Then
        Lp_SaldoDoc = 0
        If Sm_Nc_Boleta = "NO" Then Lp_SaldoDoc = SaldoDocumento(LvDetalle.ListItems(1), "CLI")
        If Lp_SaldoDoc > 0 Then
            MsgBox "Se abonar� el monto de la NC al documento..." & vbNewLine & "    (documento Ref fue al credito)   ", vbInformation
        Else
            If CboCajaAbono.ItemData(CboCajaAbono.ListIndex) = 2 Then
                Sp_MontoACtaCte = "SI"
            End If
        End If
    End If
    DoEvents
    Me.Timer2.Enabled = True
    FraProcesandoDTE.Visible = True
    Dim Dp_Total As Double
    Dim Dp_Neto As Double
    Dim Dp_Iva As Double
    Dim Rp_DocVenta As Recordset
    Dim Sp_Documento_Referencia As String
    CmdAceptar.Enabled = False
    If TxtComentario = "Escriba aqui comentario para la NC" Then TxtComentario = ""
   If Len(TxtComentario) = 0 And CboModalidadNC.ListIndex = 2 Then
        MsgBox "Debe especificar un comentario para este tipo de NC..", vbInformation
        TxtComentario.SetFocus
        Exit Sub
   End If
    If Me.Check1 = 1 Then
                Sql = "call sp_sel_DteDisponible('" & SP_Rut_Activo & "',61);"
                'Sql = "call sp_sel_DteDisponible('" & SP_Rut_Activo & "',61);"
                Consulta Rp_DocVenta, Sql
                If Rp_DocVenta.RecordCount > 0 Then
                    TxtDocActual = Rp_DocVenta!dte_folio
                    TxtDocActual.Tag = Rp_DocVenta!dte_id
                Else
                    FraProcesandoDTE.Visible = False
                    TxtDocActual = 0
                    MsgBox "No cuenta con folios disponibles para continuar..."
                    Exit Sub
                End If
            dte_id = Rp_DocVenta!dte_id
            Lp_Folio = Rp_DocVenta!dte_folio
    Else
            Lp_Folio = Me.TxtNroManual
    End If
    Lp_Id_Venta = UltimoNro("ven_doc_venta", "id")
    'Se debe generar la NC
    'con los mismos datos de la venta
     Sp_FpagoTermica = CboCajaAbono.Text
    'TAMBIEN HAY QUE VERIFICAR COMO SE PAGA LA NC
    If Sm_Nc_Boleta = "NO" Then
        'Nc por factura
        Bm_AfectarCaja = True
        'verificamos si el doc es a credito,
        AbonaNCCredito LvDetalle.ListItems(1)
        lacajita = LG_id_Caja
        If Not Bm_AfectarCaja Then lacajita = 0
        If Sp_MontoACtaCte = "SI" Then
            lacajita = 0
                           'Guardaremos estos valores en la tabla cta_pozo
            Sql = "INSERT INTO cta_pozo (abo_id,pzo_fecha,rut_emp,pzo_cli_pro,pzo_rut,pzo_abono) " & _
                    "VALUES(" & 0 & ",'" & Fql(DtFecha) & "','" & SP_Rut_Activo & "','CLI','" & TxtRut & "'," & CDbl(TxtTotalNC) & ")"
            cn.Execute Sql
        End If
        If Im_TipoNC = 1 Then
                    'Nota de Credito por anulacion de documento *****************************
                    Sql = "INSERT INTO ven_doc_venta (" & _
                                "id,no_documento,fecha,rut_cliente,nombre_cliente,tipo_movimiento,neto,bruto,iva,comentario,CondicionPago," & _
                                "ven_id,ven_nombre,ven_comision,forma_pago,tipo_doc,doc_id,suc_id,ven_fecha_vencimiento,usu_nombre," & _
                                "id_ref,rut_emp,pla_id,are_id,cen_id,exento,ven_iva_retenido,bod_id,ven_plazo,ven_comentario,ven_ordendecompra," & _
                                "ven_tipo_calculo,ven_venta_arriendo,ven_nc_utilizada,caj_id,sue_id,ven_plazo_id,ven_entrega_inmediata," & _
                                "gir_id,rso_id,tnc_id,ven_boleta_hasta,doc_time,doc_genero,ven_solicitante) " & _
                            "SELECT " & Lp_Id_Venta & "," & Lp_Folio & ",'" & Fql(DtFecha) & "',rut_cliente,nombre_cliente,'VD',neto,bruto,iva," & _
                                    "'ANULA DOCUMENTO','CONTADO',ven_id,ven_nombre,0,'PENDIENTE','NOTA CREDITO'," & _
                                    CboNC.ItemData(CboNC.ListIndex) & ",suc_id,'" & Fql(DtFecha) & "','" & LogUsuario & "'," & LvDetalle.ListItems(1) & "," & _
                                    "rut_emp,pla_id,are_id,cen_id,exento,ven_iva_retenido,bod_id,ven_plazo," & _
                                    "ven_comentario,ven_ordendecompra,ven_tipo_calculo,ven_venta_arriendo," & _
                                    "'SI'," & lacajita & ",sue_id,ven_plazo_id,'SI',gir_id,rso_id,1,1,curtime(),doc_genero,'" & CboModalidadNC.Text & " - " & CboCajaAbono.Text & "' " & _
                            "FROM ven_doc_venta " & _
                            "WHERE id=" & LvDetalle.ListItems(1) & " AND rut_emp='" & SP_Rut_Activo & "'"
                    cn.Execute Sql
                        
                   
                            If LvDetalle.SelectedItem.SubItems(7) = "NO" Then
                            
                                    Sql = "INSERT INTO ven_detalle (" & _
                                                "codigo,marca,descripcion,precio_real,descuento,unidades,precio_final," & _
                                                "subtotal,precio_costo,btn_especial,comision,fecha,no_documento,doc_id," & _
                                                "rut_emp,pla_id,are_id,cen_id,aim_id,ved_precio_venta_bruto,ved_precio_venta_neto) " & _
                                            "SELECT codigo,marca,descripcion,precio_real,descuento,unidades,precio_final,subtotal," & _
                                                "precio_costo,btn_especial,comision,'" & Fql(DtFecha) & "'," & Val(Lp_Folio) & "," & Val(CboNC.ItemData(CboNC.ListIndex)) & ",rut_emp,pla_id,are_id,cen_id," & _
                                                "aim_id , ved_precio_venta_bruto, ved_precio_venta_neto " & _
                                            "FROM ven_detalle " & _
                                            "WHERE rut_emp='" & SP_Rut_Activo & "' AND no_documento=" & Val(LvDetalle.ListItems(1).SubItems(2)) & "  AND doc_id=" & Val(LvDetalle.ListItems(1).SubItems(5))
                            Else
                                'nota de credito por factura de guias _
                                13 Mayo 2016
                                    Sql = "INSERT INTO ven_detalle (" & _
                                                "codigo,marca,descripcion,precio_real,descuento,unidades,precio_final," & _
                                                "subtotal,precio_costo,btn_especial,comision,fecha,no_documento,doc_id," & _
                                                "rut_emp,pla_id,are_id,cen_id,aim_id,ved_precio_venta_bruto,ved_precio_venta_neto) " & _
                                            "SELECT codigo,marca,descripcion,precio_real,ROUND(AVG(descuento),0),SUM(unidades),ROUND(AVG(precio_final),0),ROUND(SUM(subtotal),0)," & _
                                                "SUM(precio_costo),btn_especial,comision,'" & Fql(DtFecha) & "'," & Lp_Folio & "," & CboNC.ItemData(CboNC.ListIndex) & ",d.rut_emp,d.pla_id,d.are_id,d.cen_id," & _
                                                "aim_id , SUM(ved_precio_venta_bruto), ROUND(SUM(ved_precio_venta_neto),0) " & _
                                            "FROM ven_detalle d " & _
                                            "JOIN ven_doc_venta v ON d.no_documento=v.no_documento AND d.doc_id=v.doc_id " & _
                                            "WHERE v.rut_emp='" & SP_Rut_Activo & "' AND d.rut_emp='" & SP_Rut_Activo & "' AND nro_factura=" & LvDetalle.ListItems(1).SubItems(2) & "  AND doc_id_factura=" & LvDetalle.ListItems(1).SubItems(5) & " " & _
                                            "GROUP BY codigo"
                            
                            End If
                   
        
                    cn.Execute Sql
                    
                    
                    
                    Sm_Sql_Productos_NC = "SELECT codigo,marca,descripcion,precio_real,descuento,unidades,precio_final,subtotal," & _
                                "precio_costo,btn_especial,comision,'" & Fql(DtFecha) & "'," & Lp_Folio & "," & CboNC.ItemData(CboNC.ListIndex) & ",rut_emp,pla_id,are_id,cen_id," & _
                                "aim_id , ved_precio_venta_bruto, ved_precio_venta_neto " & _
                            "FROM ven_detalle " & _
                            "WHERE rut_emp='" & SP_Rut_Activo & "' AND no_documento=" & LvDetalle.ListItems(1).SubItems(2) & "  AND doc_id=" & LvDetalle.ListItems(1).SubItems(5)
    
        ElseIf Im_TipoNC = 2 Then
            'solo corrige montos
            '19-11-2018
                                'Nota de Credito por correccion de texto *****************************
                    Sql = "INSERT INTO ven_doc_venta (" & _
                                "id,no_documento,fecha,rut_cliente,nombre_cliente,tipo_movimiento,neto,bruto,iva,comentario,CondicionPago," & _
                                "ven_id,ven_nombre,ven_comision,forma_pago,tipo_doc,doc_id,suc_id,ven_fecha_vencimiento,usu_nombre," & _
                                "id_ref,rut_emp,pla_id,are_id,cen_id,exento,ven_iva_retenido,bod_id,ven_plazo,ven_comentario,ven_ordendecompra," & _
                                "ven_tipo_calculo,ven_venta_arriendo,ven_nc_utilizada,caj_id,sue_id,ven_plazo_id,ven_entrega_inmediata," & _
                                "gir_id,rso_id,tnc_id,ven_boleta_hasta,doc_time,doc_genero,ven_solicitante) " & _
                            "SELECT " & Lp_Id_Venta & "," & Lp_Folio & ",'" & Fql(DtFecha) & "',rut_cliente,nombre_cliente,'VD',0,0,0," & _
                                    "'CORRIGE TEXTO','CONTADO',ven_id,ven_nombre,0,'PENDIENTE','NOTA CREDITO'," & _
                                    CboNC.ItemData(CboNC.ListIndex) & ",suc_id,'" & Fql(DtFecha) & "','" & LogUsuario & "'," & LvDetalle.ListItems(1) & "," & _
                                    "rut_emp,pla_id,are_id,cen_id,0,0,bod_id,ven_plazo," & _
                                    "'" & TxtComentario & "',ven_ordendecompra,ven_tipo_calculo,ven_venta_arriendo," & _
                                    "'SI'," & lacajita & ",sue_id,ven_plazo_id,'SI',gir_id,rso_id,1,1,curtime(),doc_genero,'" & CboModalidadNC.Text & " - " & CboCajaAbono.Text & "' " & _
                            "FROM ven_doc_venta " & _
                            "WHERE id=" & LvDetalle.ListItems(1) & " AND rut_emp='" & SP_Rut_Activo & "'"
                    cn.Execute Sql
            
            
                    Sql = "INSERT INTO ven_detalle (" & _
                                "codigo,marca,descripcion,precio_real,descuento,unidades,precio_final," & _
                                "subtotal,precio_costo,btn_especial,comision,fecha,no_documento,doc_id," & _
                                "rut_emp,pla_id,are_id,cen_id,aim_id,ved_precio_venta_bruto,ved_precio_venta_neto) " & _
                            "VALUES('1','1','CORRIGE TEXTO',0,0,1,0,0," & _
                                "0,'no',0,'" & Fql(DtFecha) & "'," & Val(Lp_Folio) & "," & Val(CboNC.ItemData(CboNC.ListIndex)) & ",'" & SP_Rut_Activo & "',pla_id,are_id,cen_id," & _
                                "0 , 0, 0 )"
                    cn.Execute Sql
                     
                     Sm_Sql_Productos_NC = "SELECT codigo,marca,descripcion,precio_real,descuento,unidades,precio_final,subtotal," & _
                                "precio_costo,btn_especial,comision,'" & Fql(DtFecha) & "'," & Lp_Folio & "," & CboNC.ItemData(CboNC.ListIndex) & ",rut_emp,pla_id,are_id,cen_id," & _
                                "aim_id , ved_precio_venta_bruto, ved_precio_venta_neto " & _
                            "FROM ven_detalle " & _
                            "WHERE rut_emp='" & SP_Rut_Activo & "' AND no_documento=" & LvDetalle.ListItems(1).SubItems(2) & "  AND doc_id=" & LvDetalle.ListItems(1).SubItems(5)
        
        Else
            ' Nota de Credito Parcial, solo los productos de la grilla
                 'Nota de Credito por anulacion de documento *****************************
                                 'Totales
                    If LvDetalle.ListItems(1).SubItems(8) = "NETO" Then
                        Dp_Total = CDbl(TxtTotalNC)
                        Dp_Neto = Round(Dp_Total / Val("1." & DG_IVA), 0)
                        Dp_Iva = Dp_Total - Dp_Neto
                    
                    Else
                                     
                        Dp_Total = CDbl(TxtTotalNC)
                        Dp_Neto = Round(Dp_Total / Val("1." & DG_IVA), 0)
                        Dp_Iva = Dp_Total - Dp_Neto
                    End If
                    
                 
                    Sql = "INSERT INTO ven_doc_venta (" & _
                                "id,no_documento,fecha,rut_cliente,nombre_cliente,tipo_movimiento,neto,bruto,iva,comentario,CondicionPago," & _
                                "ven_id,ven_nombre,ven_comision,forma_pago,tipo_doc,doc_id,suc_id,ven_fecha_vencimiento,usu_nombre," & _
                                "id_ref,rut_emp,pla_id,are_id,cen_id,exento,ven_iva_retenido,bod_id,ven_plazo,ven_comentario,ven_ordendecompra," & _
                                "ven_tipo_calculo,ven_venta_arriendo,ven_nc_utilizada,caj_id,sue_id,ven_plazo_id,ven_entrega_inmediata," & _
                                "gir_id,rso_id,tnc_id,ven_boleta_hasta,doc_time,doc_genero,ven_solicitante) " & _
                            "SELECT " & Lp_Id_Venta & "," & Lp_Folio & ",'" & Fql(DtFecha) & "',rut_cliente,nombre_cliente,'VD'," & Dp_Neto & "," & Dp_Total & "," & Dp_Iva & "," & _
                                    "'DEVOLUCION DE PRODUCTOS','CONTADO',ven_id,ven_nombre,0,'PENDIENTE','NOTA CREDITO'," & _
                                    CboNC.ItemData(CboNC.ListIndex) & ",suc_id,'" & Fql(DtFecha) & "','" & LogUsuario & "'," & LvDetalle.ListItems(1) & "," & _
                                    "rut_emp,pla_id,are_id,cen_id,exento,ven_iva_retenido,bod_id,ven_plazo," & _
                                    "ven_comentario,ven_ordendecompra,ven_tipo_calculo,ven_venta_arriendo," & _
                                    "'SI'," & lacajita & ",sue_id,ven_plazo_id,'SI',gir_id,rso_id,1,1,curtime(),doc_genero ,'" & CboModalidadNC.Text & " - " & CboCajaAbono.Text & "' " & _
                            "FROM ven_doc_venta " & _
                            "WHERE id=" & LvDetalle.ListItems(1) & " AND rut_emp='" & SP_Rut_Activo & "'"
                    cn.Execute Sql
                    
                    '***** Detalle va solo con los productos de la grilla
                    
                    
                    
                    
                    Sql = "INSERT INTO ven_detalle (" & _
                                "codigo,marca,descripcion,precio_real,descuento,unidades,precio_final," & _
                                "subtotal,precio_costo,btn_especial,comision,fecha,no_documento,doc_id," & _
                                "rut_emp,pla_id,are_id,cen_id,aim_id,ved_precio_venta_bruto,ved_precio_venta_neto) VALUES "
                    CONTADOR = 0
                    For i = 1 To LvVenta.ListItems.Count
                        If LvVenta.ListItems(i).Checked And Val(LvVenta.ListItems(i)) > 0 Then
                             CONTADOR = CONTADOR + 1
                             If LvDetalle.ListItems(1).SubItems(6) = "NETO" Then
                             
                                 Dp_Neto = CDbl(LvVenta.ListItems(i).SubItems(5))
                                 Dp_Total = Round(Dp_Total * Val("1." & DG_IVA), 0)
                             Else
                                
                                 Dp_Total = CDbl(LvVenta.ListItems(i).SubItems(5))
                                 Dp_Neto = Round(Dp_Total / Val("1." & DG_IVA), 0)
                             End If
                             sql2 = "SELECT precio_costo,pla_id,are_id,cen_id,aim_id " & _
                                    "FROM ven_detalle " & _
                                    "WHERE id=" & LvVenta.ListItems(i)
                            Consulta RsTmp, sql2
                            If RsTmp.RecordCount > 0 Then
                                Sql = Sql & "(" & LvVenta.ListItems(i).SubItems(1) & ",'','" & LvVenta.ListItems(i).SubItems(2) & "'," & CDbl(LvVenta.ListItems(i).SubItems(4)) & ",0," & CxP(CDbl(LvVenta.ListItems(i).SubItems(3))) & "," & CDbl(LvVenta.ListItems(i).SubItems(5)) & "," & CDbl(LvVenta.ListItems(i).SubItems(5)) & "," & _
                                CxP(RsTmp!precio_costo) & ",'NO',0,'" & Fql(DtFecha) & "'," & Lp_Folio & "," & CboNC.ItemData(CboNC.ListIndex) & ",'" & SP_Rut_Activo & "'," & RsTmp!pla_id & "," & _
                                RsTmp!are_id & "," & RsTmp!cen_id & "," & _
                                RsTmp!aim_id & "," & Dp_Total & "," & Dp_Neto & "),"
                            
                            End If
                            
                        End If
                    Next
                    If CONTADOR = 0 Then
                        ' ESTO ES CODIGO 0 PARA DESCUENTO ' 28 ENERO 2019
                        For i = 1 To LvVenta.ListItems.Count
                             If LvVenta.ListItems(i).Checked And Val(LvVenta.ListItems(i)) = 0 Then
                                Sql = Sql & "(" & LvVenta.ListItems(i).SubItems(1) & ",'','" & LvVenta.ListItems(i).SubItems(2) & "'," & CDbl(LvVenta.ListItems(i).SubItems(4)) & ",0," & CxP(CDbl(LvVenta.ListItems(i).SubItems(3))) & "," & CDbl(LvVenta.ListItems(i).SubItems(5)) & "," & CDbl(LvVenta.ListItems(i).SubItems(5)) & "," & _
                                    0 & ",'NO',0,'" & Fql(DtFecha) & "'," & Lp_Folio & "," & CboNC.ItemData(CboNC.ListIndex) & ",'" & SP_Rut_Activo & "'," & 0 & "," & _
                                    0 & "," & 0 & "," & _
                                    0 & "," & Dp_Total & "," & Dp_Neto & "),"
                             End If
                        Next
                    End If
                    Sql = Mid(Sql, 1, Len(Sql) - 1)
                    cn.Execute Sql
                    
                    
                    
                       Sm_Sql_Productos_NC = "SELECT codigo,marca,descripcion,precio_real,descuento,unidades,precio_final,subtotal," & _
                                "precio_costo,btn_especial,comision,'" & Fql(DtFecha) & "'," & Lp_Folio & "," & CboNC.ItemData(CboNC.ListIndex) & ",rut_emp,pla_id,are_id,cen_id," & _
                                "aim_id , ved_precio_venta_bruto, ved_precio_venta_neto " & _
                            "FROM ven_detalle " & _
                            "WHERE rut_emp='" & SP_Rut_Activo & "' AND no_documento=" & Lp_Folio & "  AND doc_id=" & CboNC.ItemData(CboNC.ListIndex)
                    
        
        
        
        End If
            
        NotaCreditoElectronica 61, Lp_Folio, CboNC.ItemData(CboNC.ListIndex), False
               
        'Folio ya no esta disponible
        cn.Execute "UPDATE dte_folios SET dte_disponible='NO' " & _
                            "WHERE dte_id=" & Val(dte_id)
                            
                        
                
        ActualizaNC
        
      '  MsgBox "Se genero NC" & vbNewLine & "NRO:" & Lp_Folio, vbInformation
    Else
        'POR BOLETA
        If Im_TipoNC = 1 Then
                    Dp_Total = CDbl(Me.TxtTotalNC)
                    Dp_Neto = Round(Dp_Total / Val("1." & DG_IVA), 0)
                    Dp_Iva = Dp_Total - Dp_Neto
                    If Val(TxtVendedorID.Tag) = 0 Then TxtVendedorID.Tag = 1
              '      If Val(LvDetalle.ListItems(1)) = 0 Then LvDetalle.ListItems(1) = 1
                    Sql = "INSERT INTO ven_doc_venta (" & _
                                "id,no_documento,fecha,rut_cliente,nombre_cliente,tipo_movimiento,neto,bruto,iva,comentario,CondicionPago," & _
                                "ven_id,ven_nombre,ven_comision,forma_pago,tipo_doc,doc_id,suc_id,ven_fecha_vencimiento,usu_nombre," & _
                                "id_ref,rut_emp,pla_id,are_id,cen_id,exento,ven_iva_retenido,bod_id,ven_plazo,ven_comentario,ven_ordendecompra," & _
                                "ven_tipo_calculo,ven_venta_arriendo,ven_nc_utilizada,caj_id,sue_id,ven_plazo_id,ven_entrega_inmediata," & _
                                "gir_id,rso_id,tnc_id,ven_boleta_hasta,doc_time,doc_genero,ven_solicitante) " & _
                            "SELECT " & Lp_Id_Venta & "," & Lp_Folio & ",'" & Fql(DtFecha) & "','" & TxtRut & "','" & TxtRazonSocial & "','VD'," & Dp_Neto & "," & Dp_Total & "," & Dp_Iva & "," & _
                                    "'ANULA DOCUMENTO','CONTADO'," & TxtVendedorID.Tag & ",'EMPRESA',0,'PENDIENTE','NOTA CREDITO'," & _
                                    CboNC.ItemData(CboNC.ListIndex) & ",0,'" & Fql(DtFecha) & "','" & LogUsuario & "',0" & LvDetalle.ListItems(1) & ",'" & _
                                    SP_Rut_Activo & "',99999,99999,99999,0,0,1,1," & _
                                    "'NC',0,1,'VTA'," & _
                                    "'SI'," & LG_id_Caja & "," & IG_id_Sucursal_Empresa & ",1,'SI',0,0,1," & Lp_Folio & ",curtime(),'F','" & CboModalidadNC.Text & " - " & CboCajaAbono.Text & "' "
            
                    cn.Execute Sql
                        
                    
                    
                    
                   ' Dim Sp_Detalle_Boletas
                    
                    Sm_Sql_Productos_NC = "SELECT codigo,marca,descripcion,precio_real,descuento,unidades,precio_final,subtotal," & _
                                            "precio_costo,btn_especial,comision,'" & Fql(DtFecha) & "'," & Lp_Folio & "," & CboNC.ItemData(CboNC.ListIndex) & ",rut_emp,pla_id,are_id,cen_id," & _
                                            "aim_id , ved_precio_venta_bruto, ved_precio_venta_neto " & _
                                        "FROM ven_detalle " & _
                                        "WHERE rut_emp='" & SP_Rut_Activo & "' AND no_documento=" & LvDetalle.ListItems(1).SubItems(2) & "  AND doc_id=" & LvDetalle.ListItems(1).SubItems(5)
                    If LvDetalle.ListItems.Count > 1 Then
                        For i = 2 To LvDetalle.ListItems.Count
                        
                            Sm_Sql_Productos_NC = Sm_Sql_Productos_NC & " UNION SELECT codigo,marca,descripcion,precio_real,descuento,unidades,precio_final,subtotal," & _
                                                "precio_costo,btn_especial,comision,'" & Fql(DtFecha) & "'," & Lp_Folio & "," & CboNC.ItemData(CboNC.ListIndex) & ",rut_emp,pla_id,are_id,cen_id," & _
                                                "aim_id , ved_precio_venta_bruto, ved_precio_venta_neto " & _
                                            "FROM ven_detalle " & _
                                            "WHERE rut_emp='" & SP_Rut_Activo & "' AND no_documento=" & LvDetalle.ListItems(i).SubItems(2) & "  AND doc_id=" & LvDetalle.ListItems(i).SubItems(5)
                        Next
                    
                    End If
                    
                    
                    
                    Sql = "INSERT INTO ven_detalle (" & _
                                "codigo,marca,descripcion,precio_real,descuento,unidades,precio_final," & _
                                "subtotal,precio_costo,btn_especial,comision,fecha,no_documento,doc_id," & _
                                "rut_emp,pla_id,are_id,cen_id,aim_id,ved_precio_venta_bruto,ved_precio_venta_neto) VALUES "
                    sql2 = ""
                    
                    
                    If LvVenta.ListItems.Count > 0 Then
                    
                        For i = 1 To LvVenta.ListItems.Count
                            If LvVenta.ListItems(i).Checked Then
                                 Dp_Total = CDbl(LvVenta.ListItems(i).SubItems(5))
                                 Dp_Neto = Round(Dp_Total / Val("1." & DG_IVA), 0)
                                
                                sql2 = "SELECT precio_costo,pla_id,are_id,cen_id,aim_id " & _
                                        "FROM ven_detalle " & _
                                        "WHERE id=" & LvVenta.ListItems(i)
                                Consulta RsTmp, sql2
                                If RsTmp.RecordCount > 0 Then
                                    Sql = Sql & "(" & LvVenta.ListItems(i).SubItems(1) & ",'','" & LvVenta.ListItems(i).SubItems(2) & "'," & CDbl(LvVenta.ListItems(i).SubItems(4)) & ",0," & CxP(CDbl(LvVenta.ListItems(i).SubItems(3))) & "," & CDbl(LvVenta.ListItems(i).SubItems(5)) & "," & CDbl(LvVenta.ListItems(i).SubItems(5)) & "," & _
                                    CxP(RsTmp!precio_costo) & ",'NO',0,'" & Fql(DtFecha) & "'," & Lp_Folio & "," & CboNC.ItemData(CboNC.ListIndex) & ",'" & SP_Rut_Activo & "'," & RsTmp!pla_id & "," & _
                                    RsTmp!are_id & "," & RsTmp!cen_id & "," & _
                                    RsTmp!aim_id & "," & Dp_Total & "," & Dp_Neto & "),"
                                End If
                                
                            End If
                        Next
                        
                        cn.Execute Mid(Sql, 1, Len(Sql) - 1)
                    Else
                        
                        For i = 1 To LvDetalle.ListItems.Count
                                
                            dp_netito = Round(CDbl(LvDetalle.ListItems(i).SubItems(4)) / 1.19, 0)
                        
                            sql2 = sql2 & "(0,'','" & LvDetalle.ListItems(i).SubItems(1) & " " & LvDetalle.ListItems(i).SubItems(2) & "'," & CDbl(LvDetalle.ListItems(i).SubItems(4)) & ",0,1," & CDbl(LvDetalle.ListItems(i).SubItems(4)) & "," & CDbl(LvDetalle.ListItems(i).SubItems(4)) & "," & _
                                                "0,'NO',0,'" & Fql(DtFecha) & "'," & Lp_Folio & "," & CboNC.ItemData(CboNC.ListIndex) & ",'" & SP_Rut_Activo & "',99999,99999,99999," & _
                                    "0 ," & LvDetalle.ListItems(i).SubItems(4) & "," & dp_netito & "),"
                
                        Next
                        cn.Execute Sql & Mid(sql2, 1, Len(sql2) - 1)
                    End If
                    
                
        Else
            ' NOTA DE CREDITO PARCIAL POR BOLETA
                    Dp_Total = CDbl(Me.TxtTotalNC)
                    Dp_Neto = Round(Dp_Total / Val("1." & DG_IVA), 0)
                    Dp_Iva = Dp_Total - Dp_Neto
                    Sql = "INSERT INTO ven_doc_venta (" & _
                                "id,no_documento,fecha,rut_cliente,nombre_cliente,tipo_movimiento,neto,bruto,iva,comentario,CondicionPago," & _
                                "ven_id,ven_nombre,ven_comision,forma_pago,tipo_doc,doc_id,suc_id,ven_fecha_vencimiento,usu_nombre," & _
                                "id_ref,rut_emp,pla_id,are_id,cen_id,exento,ven_iva_retenido,bod_id,ven_plazo,ven_comentario,ven_ordendecompra," & _
                                "ven_tipo_calculo,ven_venta_arriendo,ven_nc_utilizada,caj_id,sue_id,ven_plazo_id,ven_entrega_inmediata," & _
                                "gir_id,rso_id,tnc_id,ven_boleta_hasta,doc_time,doc_genero,ven_solicitante) " & _
                            "SELECT " & Lp_Id_Venta & "," & Lp_Folio & ",'" & Fql(DtFecha) & "','" & TxtRut & "','" & TxtRazonSocial & "','VD'," & Dp_Neto & "," & Dp_Total & "," & Dp_Iva & "," & _
                                    "'DEVOLUCION DE PRODUCTOS','CONTADO'," & TxtVendedorID.Tag & ",'EMPRESA',0,'PENDIENTE','NOTA CREDITO'," & _
                                    CboNC.ItemData(CboNC.ListIndex) & ",0,'" & Fql(DtFecha) & "','" & LogUsuario & "',0" & LvDetalle.ListItems(1) & ",'" & _
                                    SP_Rut_Activo & "',99999,99999,99999,0,0,1,1," & _
                                    "'NC',0,1,'VTA'," & _
                                    "'SI'," & LG_id_Caja & "," & IG_id_Sucursal_Empresa & ",1,'SI',0,0,1," & Lp_Folio & ",curtime(),'F','" & CboModalidadNC.Text & " - " & CboCajaAbono.Text & "' "
            
                    cn.Execute Sql
                                         
                    
                    
                    Sql = "INSERT INTO ven_detalle (" & _
                                "codigo,marca,descripcion,precio_real,descuento,unidades,precio_final," & _
                                "subtotal,precio_costo,btn_especial,comision,fecha,no_documento,doc_id," & _
                                "rut_emp,pla_id,are_id,cen_id,aim_id,ved_precio_venta_bruto,ved_precio_venta_neto) VALUES "
                    If Sp_BoletaSinDetalle Then
                    
                                Sql = Sql & "(0,'','" & Me.TxtDescripcion & "'," & CDbl(TxtPrecio) & ",0," & CDbl(TxtCantidad) & "," & CDbl(TxtPrecio) & "," & CDbl(TxtPrecio) & ",1,'NO','0','" & Fql(DtFecha) & "'," & Lp_Folio & "," & CboNC.ItemData(CboNC.ListIndex) & ",'" & SP_Rut_Activo & "'," & 133 & "," & _
                                        99999 & "," & 99999 & "," & _
                                        0 & "," & Dp_Total & "," & Dp_Neto & "),"
                    Else
                            For i = 1 To LvVenta.ListItems.Count
                                If LvVenta.ListItems(i).Checked Then
                                     Dp_Total = CDbl(LvVenta.ListItems(i).SubItems(5))
                                     Dp_Neto = Round(Dp_Total / Val("1." & DG_IVA), 0)
                                    
                                    sql2 = "SELECT precio_costo,pla_id,are_id,cen_id,aim_id " & _
                                            "FROM ven_detalle " & _
                                            "WHERE id=" & LvVenta.ListItems(i)
                                    Consulta RsTmp, sql2
                                    If RsTmp.RecordCount > 0 Then
                                        Sql = Sql & "(" & LvVenta.ListItems(i).SubItems(1) & ",'','" & LvVenta.ListItems(i).SubItems(2) & "'," & CDbl(LvVenta.ListItems(i).SubItems(4)) & ",0," & CxP(CDbl(LvVenta.ListItems(i).SubItems(3))) & "," & CDbl(LvVenta.ListItems(i).SubItems(5)) & "," & CDbl(LvVenta.ListItems(i).SubItems(5)) & "," & _
                                        CxP(RsTmp!precio_costo) & ",'NO',0,'" & Fql(DtFecha) & "'," & Lp_Folio & "," & CboNC.ItemData(CboNC.ListIndex) & ",'" & SP_Rut_Activo & "'," & RsTmp!pla_id & "," & _
                                        RsTmp!are_id & "," & RsTmp!cen_id & "," & _
                                        RsTmp!aim_id & "," & Dp_Total & "," & Dp_Neto & "),"
                                    End If
                                    
                                End If
                            Next
                    End If
                    Sql = Mid(Sql, 1, Len(Sql) - 1)
                    cn.Execute Sql
                    
                    
                    
                       Sm_Sql_Productos_NC = "SELECT codigo,marca,descripcion,precio_real,descuento,unidades,precio_final,subtotal," & _
                                "precio_costo,btn_especial,comision,'" & Fql(DtFecha) & "'," & Lp_Folio & "," & CboNC.ItemData(CboNC.ListIndex) & ",rut_emp,pla_id,are_id,cen_id," & _
                                "aim_id , ved_precio_venta_bruto, ved_precio_venta_neto " & _
                            "FROM ven_detalle " & _
                            "WHERE rut_emp='" & SP_Rut_Activo & "' AND no_documento=" & Lp_Folio & "  AND doc_id=" & CboNC.ItemData(CboNC.ListIndex)
                    
                    
               
        
        
        End If
        
        
                
        If Me.Check1.Value = 1 Then
                NotaCreditoElectronica 61, Lp_Folio, CboNC.ItemData(CboNC.ListIndex), True
                'Folio ya no esta disponible
                cn.Execute "UPDATE dte_folios SET dte_disponible='NO' " & _
                                    "WHERE dte_id=" & dte_id
        End If
        ActualizaNC
        
        
        
        'MsgBox "Se genero NC" & vbNewLine & "NRO:" & Lp_Folio, vbInformation
        
       ' TxtTotalNC = 0
    
    End If
    
    If Me.CboModalidadNC.ListIndex <> 2 Then
        If CboCajaAbono.ItemData(CboCajaAbono.ListIndex) = 3 Or CboCajaAbono.ItemData(CboCajaAbono.ListIndex) = 4 Then
            '17-febrero 2018
            'Anulacion de pago con transbank
             'Se eliminar� el pago previo
             CmdEliminar_Click
            
             AbonaNCCredito LvDetalle.ListItems(1)
        End If
    End If
    SkFoliosDisponibles = ""

    Sql = "SELECT count(dte_id) dte " & _
            "FROM dte_folios " & _
            "WHERE rut_emp='" & SP_Rut_Activo & "' AND doc_id=" & 61 & " AND dte_disponible='SI' " & _
            "GROUP BY doc_id"
    Consulta RsTmp3, Sql
    If RsTmp3.RecordCount > 0 Then
        SkFoliosDisponibles = "Folios disponibles:" & RsTmp3!Dte
    End If
                    
             
    FraProcesandoDTE.Visible = False
    FrmLoad.Visible = False
    MsgBox "Documento emitido... " & vbNewLine & "Nro : " & Lp_Folio & vbNewLine & SkFoliosDisponibles, vbInformation
    Sql = ""
    LvDetalle.ListItems.Clear
    TxtTotalNC = 0
    TxtRut = ""
    TxtDireccion = ""
    TxtGiro = ""
    txtFono = ""
    txtComuna = ""
    TxtCiudad = ""
    Me.TxtRazonSocial = ""
    Me.txtNroDocumentoBuscar = ""
    Me.txtTotalDocBuscado = ""
    LvVenta.ListItems.Clear
    LvVenta.Enabled = False
    Me.CboModalidadNC.ListIndex = 0
    TxtSubTotal = 0
    Me.TxtDescuentoNC = 0
    CmdAceptar.Enabled = True
    
    Me.Timer2.Enabled = False
    
    FraProcesandoDTE.Visible = False
    
End Sub
Private Sub CmdEliminar_Click()
    Dim Lp_Nro As Long, Lp_Abo As Long, Sp_Wh As String, Sp_Eli As String, Lp_MovId As Long
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    If Val(LvDetalle.SelectedItem) = 0 Then Exit Sub
    
  
    
    
    
    
    Sp_Eli = "ING"
    
    Lp_Nro = 0 ' LvDetalle.SelectedItem.SubItems(9)
    Lp_Abo = TxtFpagoDetectada.Tag
  '  If MsgBox("Esta a punto de eliminar el pago anterior, reemplazado por una NC " & vbNewLine & "�Continuar...?", vbYesNo + vbQuestion) = vbNo Then Exit Sub
    SG_codigo2 = Empty
   ' sis_InputBox.Caption = "Se requiere autenticaci�n"
   ' sis_InputBox.FramBox.Caption = "Ingrese clave"
   ' sis_InputBox.Show 1
   ' If SG_codigo2 <> Empty Then
  '      Sql = "SELECT usu_nombre,usu_login " & _
              "FROM sis_usuarios " & _
              "WHERE usu_pwd=MD5('" & UCase(SG_codigo2) & "')"
   '     Consulta RsTmp, Sql
    '    If RsTmp.RecordCount > 0 Then
            On Error GoTo errorEliminando
            cn.BeginTrans
            
          
            
            
            cn.Execute "UPDATE ven_doc_venta " & _
                            "SET caj_id=0 " & _
                            "WHERE id=" & Lp_Id_Venta
            
            
            Sp_Wh = " WHERE abo_id=" & Lp_Abo
            cn.Execute "DELETE FROM cta_abonos " & Sp_Wh
            cn.Execute "DELETE FROM abo_transferencia " & Sp_Wh
            cn.Execute "DELETE FROM cta_abono_documentos " & Sp_Wh
            
            cn.Execute "DELETE FROM abo_tipos_de_pagos " & Sp_Wh
            
            
            '5 5 2016 _
            Eliminar tambien del pozo los abonos/cargos del abono
            cn.Execute "DELETE FROM cta_pozo WHERE abo_id=" & Lp_Abo
            
            
            
            If Sm_SistemaBanco = "SI" Then
                'Ubicar cheques relacionados, si correponde
                
                
                'O tambien buscar depositos de cheques en cartera 19 Octubre 2013
                
                Sql = "SELECT mov_id " & _
                      "FROM ban_movimientos " & Sp_Wh
                Consulta RsTmp3, Sql
                If RsTmp3.RecordCount > 0 Then
                
                    RsTmp3.MoveFirst
                    Do While Not RsTmp3.EOF
                        Sql = "DELETE FROM com_con_multicuenta " & _
                                "WHERE dcu_tipo='CHEQUE' AND id_unico IN(" & _
                                        "SELECT che_id " & _
                                        "FROM ban_cheques " & _
                                        "WHERE mov_id=" & RsTmp3!mov_id & ")"
                        cn.Execute Sql
                            
                        
                    
                        cn.Execute "UPDATE ban_cheques SET che_estado='SIN EMITIR',che_concepto='',che_destinatario=''," & _
                                          "che_valor=0,pla_id=0," & _
                                          "mes_contable=0,ano_contable=0,mov_id=0 " & _
                                   "WHERE mov_id=" & RsTmp3!mov_id
                    
                        RsTmp3.MoveNext
                    Loop
                End If
                cn.Execute "DELETE FROM ban_movimientos " & Sp_Wh
                
                If Sp_extra = "CLIENTES" Then
                    For i = 1 To LvCheques.ListItems.Count
                        cn.Execute "DELETE FROM ban_movimientos WHERE mov_id=" & LvCheques.ListItems(i).SubItems(6)
                    Next
                End If
                
            End If
            cn.Execute "DELETE FROM abo_cheques " & Sp_Wh
            
            
            Sql = "INSERT INTO sis_eliminacion_documentos (id,eli_fecha,eli_motivo,usu_login,eli_ven_com)" & _
                  "VALUES(" & Lp_Abo & ",'" & Fql(Date) & "','" & "ELIMINACION COMPROBANTE " & Sp_extra & "','" & LogUsuario & "','" & Sp_Eli & "')"
            cn.Execute Sql
            
            cn.CommitTrans
            MsgBox LogUsuario & " ha eliminado el comprobante Nro " & Lp_Nro
          '  Me.LvDocumentos.ListItems.Clear
          '  Me.LvCheques.ListItems.Clear
          '  Me.LvMediosDePago.ListItems.Clear
          '  CmdConsultar_Click
     '   Else
       '     MsgBox "Usuario no encontrado...", vbInformation
      '
        'End If
    'End If
    Exit Sub
        
errorEliminando:
    cn.RollbackTrans
    MsgBox "Error Eliminando abono..." & vbNewLine & Err.Number & vbNewLine & Err.Description, vbInformation
End Sub

Private Sub CmdBuscaCliente_Click()
    LlamaClienteDe = "VD"
    
    ClienteEncontrado = False
    BuscaCliente.Show 1
    TxtRut = SG_codigo
    TxtRut.SetFocus
End Sub

Private Sub CmdBuscar_Click()
    'GoTo pasaraqui
    For i = 1 To LvDetalle.ListItems.Count
        If LvDetalle.ListItems(i).SubItems(6) = 33 Then
            MsgBox "Es posible hacer notas de credito por una sola Factura Electronica...", vbInformation
            Exit Sub
        End If
    Next
    
    If CboDocVenta.ListIndex = -1 Then
        MsgBox "Seleccione documento...", vbInformation
        CboDocVenta.SetFocus
        Exit Sub
    End If
    
    If Val(txtNroDocumentoBuscar) = 0 Then
        MsgBox "Debe ingresar Nro para de " & CboDocVenta.Text & " ...", vbInformation
        txtNroDocumentoBuscar.SetFocus
        Exit Sub
    End If
    
    If Val(txtTotalDocBuscado) = 0 Then
        MsgBox "Ingrese valor del documento...", vbInformation
        txtTotalDocBuscado.SetFocus
        Exit Sub
    End If
    Sp_BoletaSinDetalle = False
    Filtro = ""
    If Sm_Nc_Boleta = "NO" Then
        Filtro = "rut_cliente='" & TxtRut & "' AND "
    
    
    End If
    
    Sql = "SELECT id,doc_nombre,no_documento,fecha,bruto,doc_cod_sii,v.doc_id " & _
                                "FROM ven_doc_venta v " & _
                                "JOIN sis_documentos d ON v.doc_id=d.doc_id " & _
                                "WHERE doc_nota_de_credito='NO' AND " & Filtro & "  v.doc_id=" & CboDocVenta.ItemData(CboDocVenta.ListIndex) & " AND no_documento=" & txtNroDocumentoBuscar
 '   GoTo sigue
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        LvDetalle.ListItems.Add , , RsTmp!ID
        p = LvDetalle.ListItems.Count
        LvDetalle.ListItems(p).SubItems(1) = RsTmp!doc_nombre
        LvDetalle.ListItems(p).SubItems(2) = RsTmp!no_documento
        LvDetalle.ListItems(p).SubItems(3) = RsTmp!Fecha
        LvDetalle.ListItems(p).SubItems(4) = NumFormat(RsTmp!bruto)
        LvDetalle.ListItems(p).SubItems(5) = RsTmp!doc_id
        LvDetalle.ListItems(p).SubItems(6) = RsTmp!doc_cod_sii
        
    Else
sigue:
        If Sm_Nc_Boleta = "NO" Then
            MsgBox "Documento no encontrado para este cliente...", vbInformation
            CboDocVenta.SetFocus
            Exit Sub
        End If
    
        
        If MsgBox("Documento no se encontr� en los registros del sistema..." & vbNewLine & " �Agregar de todas formas...?", vbQuestion + vbOKCancel) = vbCancel Then Exit Sub
        Sql = "SELECT doc_cod_sii " & _
                "FROM sis_documentos " & _
                "WHERE doc_id=" & CboDocVenta.ItemData(CboDocVenta.ListIndex)
        Consulta RsTmp, Sql
pasaraqui:
        LvDetalle.ListItems.Add , , ""
        p = LvDetalle.ListItems.Count
        LvDetalle.ListItems(p).SubItems(1) = CboDocVenta.Text
        LvDetalle.ListItems(p).SubItems(2) = txtNroDocumentoBuscar
        LvDetalle.ListItems(p).SubItems(3) = DtFechaBoleta
        LvDetalle.ListItems(p).SubItems(4) = NumFormat(txtTotalDocBuscado)
        LvDetalle.ListItems(p).SubItems(5) = CboDocVenta.ItemData(CboDocVenta.ListIndex)
        LvDetalle.ListItems(p).SubItems(6) = 39 'RsTmp!doc_cod_sii
        Sp_BoletaSinDetalle = True
        
    End If
    SumaGrilla
    BuscaDescuentoNC
    txtTotalDocBuscado = ""
    txtFechaDocBuscado = ""
    txtTotalDocBuscado = ""
    CboDocVenta.SetFocus
    
End Sub

Private Sub CmdBuscarBuscar_Click()
    Sm_BuscaPorNumero = "NO"
    If Val(CboNroBuscar) = 0 Then
        MsgBox "Falta Nro documento...", vbInformation
        CboNroBuscar.SetFocus
        Exit Sub
    End If
    FrmLoad.Visible = True
    DoEvents
    Sql = "SELECT rut_cliente,no_documento,id " & _
            "FROM ven_doc_venta " & _
            "WHERE rut_emp='" & SP_Rut_Activo & "' AND no_documento=" & CboNroBuscar & " AND doc_id=" & CboDocBuscar.ItemData(CboDocBuscar.ListIndex)
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        TxtRut = RsTmp!rut_cliente
        Sm_BuscaPorNumero = "SI"
        CboNroBuscar.Tag = RsTmp!ID
        txtRut_Validate True
        LvDetalle_Click
    End If
    FrmLoad.Visible = False
End Sub

Private Sub CmdModificaValor_Click()
    Dim Lp_NuevoValor As Variant
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
        Lp_NuevoValor = InputBox("Ingrese Valor", "Cambiar monto", LvDetalle.SelectedItem.SubItems(4))
        If Val(Lp_NuevoValor) > 0 Then
            LvDetalle.SelectedItem.SubItems(4) = NumFormat(Val(Lp_NuevoValor))
        End If
    
        
        
End Sub

Private Sub CmdSalir_Click()
    Unload Me
End Sub



Private Sub CmdSignoMas_Click()
    Dim Lp_Ul As Integer
    Lp_Ul = Right(Me.TxtTotalNC, 1)
    If Lp_Ul < 5 Then
        TxtTotalNC = NumFormat(CDbl(TxtTotalNC) - Lp_Ul)
    Else
        TxtTotalNC = NumFormat(CDbl(TxtTotalNC) + (10 - Lp_Ul))
    
    End If
    
End Sub

Private Sub Form_Load()
    Skin2 Me, , 5
    Centrar Me
    
    Sql = "SELECT ciudad,emp_oficina_sii,direccion " & _
            "FROM sis_empresas " & _
            "WHERE rut='" & SP_Rut_Activo & "'"
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        Me.TxtOficinaSII = RsTmp!emp_oficina_sii
        Me.TxtCiudadEmp = RsTmp!ciudad
        TxtEmpDireccion = RsTmp!direccion
    End If
    CboCajaAbono.ListIndex = 0
     Sm_BuscaPorNumero = "NO"
     
     
    Im_id_Doc_SII = 33
    Sql = "SELECT bod_id " & _
            "FROM par_bodegas " & _
            "WHERE rut_emp='" & SP_Rut_Activo & "' AND sue_id=" & IG_id_Sucursal_Empresa
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        IG_id_Bodega_Ventas = RsTmp!bod_id
    End If
  '  Me.SkNCManual.Visible = False
    LLenarCombo CboDocBuscar, "doc_nombre", "doc_id", "sis_documentos", "doc_activo='SI' AND  doc_dte='SI' AND doc_nota_de_credito='NO' AND doc_documento='VENTA'"
    CboDocBuscar.ListIndex = 0
    Me.TxtNroManual.Visible = False
    If SP_Rut_Activo = "77.774.580-8" Or SP_Rut_Activo = "76.039.757-1" Or SP_Rut_Activo = "77.150.088-9" Then
        LLenarCombo CboNC, "doc_nombre", "doc_id", "sis_documentos", "doc_documento='VENTA' AND doc_nota_de_credito='SI' AND doc_dte='SI'", "doc_orden"
       ' Me.SkNCManual.Visible = True
        Me.TxtNroManual.Visible = True
        
    Else
        LLenarCombo CboNC, "doc_nombre", "doc_id", "sis_documentos", "doc_documento='VENTA' AND doc_nota_de_credito='SI' AND doc_dte='SI'", "doc_orden"
    End If
    CboNC.ListIndex = 0
    LLenarCombo CboDocVenta, "doc_nombre", "doc_id", "sis_documentos", "doc_documento='VENTA' AND doc_nota_de_credito='NO'", "doc_orden"
  '  CboDocVenta.ListIndex = 0
    DtFecha = Date
    DtFechaBoleta = Date
    CboModalidadNC.ListIndex = 0
    
    
    
    If SP_Rut_Activo = "78.967.170-2" Then
        CmdModificaValor.Visible = True
    End If
   '      Me.CboModalidadNC.AddItem "PERSONALIZADA"
   Dim Rp_DocVenta As Recordset
    Sql = "call sp_sel_DteDisponible('" & SP_Rut_Activo & "',61);"
    'Sql = "call sp_sel_DteDisponible('" & SP_Rut_Activo & "',61);"
    Consulta Rp_DocVenta, Sql
    If Rp_DocVenta.RecordCount > 0 Then
        TxtDocActual = Rp_DocVenta!dte_folio
        TxtDocActual.Tag = Rp_DocVenta!dte_id
    Else
        TxtDocActual = 0
        MsgBox "No cuenta con folios disponibles para continuar..."
        Exit Sub
    End If
    Me.Height = 10950
End Sub






Private Sub LvDetalle_Click()
  '  2 nro 5 id
    Dim Sp_FiltroDPNC As String
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    LvCheques.ListItems.Clear
    LvCheques.Visible = False
    TxtFpagoDetectada = "CREDITO"
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    Im_id_Doc_SII = LvDetalle.SelectedItem.SubItems(6)
    Sp_FiltroDPNC = "dnc_id IN(2)"
    LLenarCombo Me.CboCajaAbono, "dnc_nombre", "dnc_id", "abo_tipo_devolucion_con_nc", Sp_FiltroDPNC, "dnc_id"
    CboCajaAbono.ListIndex = 0
    If Sm_Nc_Boleta = "NO" Then
        If LvDetalle.SelectedItem.SubItems(7) = "NO" Then
            Sql = "SELECT d.id,d.codigo,d.descripcion,unidades,precio_final,subtotal,(SELECT pro_inventariable FROM maestro_productos p WHERE p.codigo=d.codigo AND p.rut_emp='" & SP_Rut_Activo & "') pro_inventariable " & _
                    "FROM ven_detalle d " & _
                    "JOIN ven_doc_venta v ON d.no_documento =v.no_documento AND v.doc_id=d.doc_id " & _
                    "WHERE d.no_documento=" & LvDetalle.SelectedItem.SubItems(2) & " AND d.doc_id=" & LvDetalle.SelectedItem.SubItems(5) & " AND " & _
                    "d.rut_emp='" & SP_Rut_Activo & "' AND v.rut_emp='" & SP_Rut_Activo & "'"
        Else
            Sql = "SELECT d.id,d.codigo,d.descripcion,SUM(unidades),ROUND(AVG(precio_final),0),SUM(subtotal) " & _
                    "FROM ven_detalle d " & _
                    "JOIN ven_doc_venta v ON d.no_documento =v.no_documento AND v.doc_id=d.doc_id " & _
                    "JOIN maestro_productos m ON d.codigo=m.codigo " & _
                    "WHERE v.nro_factura=" & LvDetalle.SelectedItem.SubItems(2) & " AND v.doc_id_factura=" & LvDetalle.SelectedItem.SubItems(5) & " AND " & _
                    "d.rut_emp='" & SP_Rut_Activo & "' AND v.rut_emp='" & SP_Rut_Activo & "' AND m.rut_emp='" & SP_Rut_Activo & "' " & _
                    "GROUP BY codigo"
        End If
        Consulta RsTmp, Sql
        LLenar_Grilla RsTmp, Me, LvVenta, True, True, True, False
        '30-01-2017
        'Buscar sucursal de la factura
        Sql = "SELECT suc_id,suc_ciudad,suc_direccion " & _
                "FROM par_sucursales " & _
                "WHERE suc_id=(SELECT suc_id " & _
                                    "FROM ven_doc_venta " & _
                                    "WHERE rut_emp='" & SP_Rut_Activo & "' AND no_documento=" & LvDetalle.SelectedItem.SubItems(2) & " AND doc_id=" & LvDetalle.SelectedItem.SubItems(5) & ")"
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
            If RsTmp!suc_id <> 0 Then
                Me.TxtDireccion = RsTmp!suc_direccion
                Me.txtComuna = RsTmp!suc_ciudad
                MsgBox "Modifica direccion a direccion de sucursal que fue emitida la factura...", vbInformation
            End If
        End If
        Me.ChkProductos.Value = 1
        For i = 1 To LvVenta.ListItems.Count
            LvVenta.ListItems(i).Checked = True
        Next
        BuscaDescuentoNC
    Else
        'Por boleta
        TxtVendedorID = 1
        TxtVendedorID.Tag = 1
        Sql = "SELECT ven_id,ven_nombre " & _
                "FROM ven_doc_venta " & _
                "WHERE  no_documento=" & LvDetalle.SelectedItem.SubItems(2) & " AND doc_id=" & LvDetalle.SelectedItem.SubItems(5) & " AND rut_emp='" & SP_Rut_Activo & "'"
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
             TxtVendedorID = RsTmp!ven_nombre
             TxtVendedorID.Tag = RsTmp!ven_id
        
        End If
        Sql = "SELECT d.id,d.codigo,d.descripcion,unidades,precio_final,subtotal,pro_inventariable,ven_id " & _
                    "FROM ven_detalle d " & _
                    "JOIN ven_doc_venta v ON d.no_documento =v.no_documento AND v.doc_id=d.doc_id " & _
                    "JOIN maestro_productos m ON d.codigo=m.codigo " & _
                    "WHERE d.no_documento=" & LvDetalle.SelectedItem.SubItems(2) & " AND d.doc_id=" & LvDetalle.SelectedItem.SubItems(5) & " AND " & _
                    "d.rut_emp='" & SP_Rut_Activo & "' AND v.rut_emp='" & SP_Rut_Activo & "' AND m.rut_emp='" & SP_Rut_Activo & "'"
        Consulta RsTmp, Sql
        LLenar_Grilla RsTmp, Me, LvVenta, True, True, True, False
        Me.ChkProductos.Value = 1
        For i = 1 To LvVenta.ListItems.Count
            LvVenta.ListItems(i).Checked = True
        Next
        BuscaDescuentoNC
    End If
   TxtTotalNC = LvDetalle.ListItems(1).SubItems(4)
   '04-02-2017
   'Verificaremos que pagos tiene el documento para mostrarlos.
   'MsgBox "buscar fpago."
   Sp_FiltroDPNC = "dnc_id IN(2)"
   If Sp_BoletaSinDetalle Then
   
   Else
        Sql = " SELECT mpa_nombre,mpa_id,abo_id " & _
                "FROM vi_tipos_de_pago_documento " & _
                    "WHERE id =" & LvDetalle.SelectedItem & " AND rut_emp='" & SP_Rut_Activo & "' AND abo_cli_pro='CLI' AND rut_empd='" & SP_Rut_Activo & "'"
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
            TxtFpagoDetectada = RsTmp!mpa_nombre
            TxtFpagoDetectada.Tag = RsTmp!abo_id
            ' si la forma de pago fue chque(s) mostrar los cheques.
            Sp_FiltroDPNC = "dnc_id IN(1,2)"
            Select Case RsTmp!mpa_id
                
                Case 1
                    Sp_FiltroDPNC = "dnc_id IN(1,2)"
                Case 2
                    Sp_FiltroDPNC = "dnc_id IN(1,2,4)"
                Case 3
                    Sp_FiltroDPNC = "dnc_id IN(1,2)"
                Case 4
                    Sp_FiltroDPNC = "dnc_id IN(1,2)"
                Case 5
                    Sp_FiltroDPNC = "dnc_id IN(1,2)"
                Case 6
                    Sp_FiltroDPNC = "dnc_id IN(1,2)"
                Case 7
                    Sp_FiltroDPNC = "dnc_id IN(1,2)"
                Case 8
                    Sp_FiltroDPNC = "dnc_id IN(1,2,3)"
                Case 9
                    Sp_FiltroDPNC = "dnc_id IN(1,2,3)"
                Case 8888
                    Sp_FiltroDPNC = "dnc_id IN(1,2,3)"
            End Select
                    
            If RsTmp!mpa_id = 2 Then 'cheques mostrarlos
                LvCheques.Visible = True
                Sql = "SELECT che_id,ban_nombre,che_numero,che_monto " & _
                    "FROM abo_cheques c " & _
                    " JOIN par_bancos b ON c.ban_id=b.ban_id " & _
                    "WHERE  c.abo_id =" & RsTmp!abo_id
                Consulta RsTmp, Sql
                LLenar_Grilla RsTmp, Me, LvCheques, False, True, True, False
            End If
        End If
    End If
   LLenarCombo Me.CboCajaAbono, "dnc_nombre", "dnc_id", "abo_tipo_devolucion_con_nc", Sp_FiltroDPNC, "dnc_id"
    
End Sub

Private Sub LvDetalle_DblClick()
    Exit Sub
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    
    Busca_Id_Combo CboDocVenta, Val(LvDetalle.SelectedItem.SubItems(5))
    txtNroDocumentoBuscar = LvDetalle.SelectedItem.SubItems(2)
    txtFechaDocBuscado = LvDetalle.SelectedItem.SubItems(3)
    txtTotalDocBuscado = LvDetalle.SelectedItem.SubItems(4)
    
    LvDetalle.ListItems.Remove LvDetalle.SelectedItem.Index
    
    CboDocVenta.SetFocus
End Sub





Private Sub LvVenta_DblClick()
        If LvVenta.SelectedItem Is Nothing Then Exit Sub
        With LvVenta
            TxtCodigo.Tag = LvVenta.SelectedItem
            TxtCodigo = .SelectedItem.SubItems(1)
            Me.TxtDescripcion = .SelectedItem.SubItems(2)
            TxtCantidad = .SelectedItem.SubItems(3)
            TxtPrecio = .SelectedItem.SubItems(4)
            TxtTotalLinea = .SelectedItem.SubItems(5)
            TxtStock = .SelectedItem.SubItems(6)
            TxtCodigo.SetFocus
            LvVenta.ListItems.Remove .SelectedItem.Index
        End With
End Sub

Private Sub LvVenta_ItemCheck(ByVal Item As MSComctlLib.ListItem)
    SumaSeleccion
End Sub



Private Sub Timer2_Timer()
    Me.XP_ProgressBar1.Value = Me.XP_ProgressBar1.Value + 10
    If XP_ProgressBar1.Value = 100 Then XP_ProgressBar1.Value = 1
End Sub

Private Sub TxtCantidad_GotFocus()
    En_Foco TxtCantidad
End Sub

Private Sub TxtCantidad_KeyPress(KeyAscii As Integer)
    KeyAscii = AceptaSoloNumeros(KeyAscii)
    If KeyAscii = 46 Then KeyAscii = 44
    If KeyAscii = 39 Then KeyAscii = 0
End Sub
Private Sub TxtCantidad_Validate(Cancel As Boolean)
 '   Dim Dp_Cantidad As decim
    If Val(CxP(TxtCantidad)) > 0 Then
        CalculaCantPrecio
    Else
       TxtCantidad = "0"
    End If
End Sub
Private Sub CalculaCantPrecio()
    If Val(TxtPrecio) = 0 Then TxtPrecio = 0
    If Val(CxP(TxtCantidad)) > 0 Then
        TxtCantidad = Format(TxtCantidad, "#0")
        'Dp_Cantidad = TxtCantidad
        TxtTotalLinea = NumFormat(TxtCantidad * CDbl(TxtPrecio))
    End If
End Sub

Private Sub TxtCodigo_Validate(Cancel As Boolean)

    If TxtCodigo = "DESCNC" Then
        TxtDescripcion = "DESCUENTO CLIENTE"
        Exit Sub
    End If


    If Me.CboModalidadNC.ListIndex = -1 Then Exit Sub
    
    If Me.CboModalidadNC.ListIndex < 2 Then
        Sql = "SELECT d.id " & _
                "FROM ven_detalle d " & _
                "WHERE d.no_documento=" & LvDetalle.SelectedItem.SubItems(2) & " AND d.doc_id=" & LvDetalle.SelectedItem.SubItems(5) & " AND " & _
                "d.rut_emp='" & SP_Rut_Activo & "' AND codigo='" & TxtCodigo & "'"
                
                
        Consulta RsTmp, Sql
        If RsTmp.RecordCount = 0 Then
            
        
        
            'MsgBox "Producto no encontrado en su Nota de Credito...", vbInformation
            'TxtCodigo = ""
        End If
    Else
        '
    
    
    End If
End Sub



Private Sub TxtComentario_GotFocus()
    If TxtComentario = "Escriba aqui comentario para la NC" Then
        TxtComentario = ""
    End If
    En_Foco TxtComentario
End Sub

Private Sub txtNroDocumentoBuscar_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
    If KeyAscii = 39 Then KeyAscii = 0
End Sub
Private Sub txtComuna_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
    If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtDireccion_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
    If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtGiro_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
    If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtPrecio_GotFocus()
    En_Foco TxtPrecio
End Sub


Private Sub TxtPrecio_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
    If KeyAscii = 39 Then KeyAscii = 0
End Sub


Private Sub TxtPrecio_Validate(Cancel As Boolean)
    If Val(TxtPrecio) = 0 Then
        TxtPrecio = 0
    Else
    
        TxtPrecio = NumFormat(TxtPrecio)
        CalculaCantPrecio
    End If
End Sub

Private Sub TxtRazonSocial_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
    If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtRUT_GotFocus()
    En_Foco TxtRut
End Sub

Private Sub TxtRut_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
    On Error Resume Next
    If KeyAscii = 13 Then SendKeys ("{TAB}")
    If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtRut_KeyUp(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF1 Then CmdBuscaCliente_Click
End Sub

Private Sub TxtRut_LostFocus()
    If Len(TxtRut.Text) = 0 Then
        Me.TxtRazonSocial.Text = ""
        Exit Sub
    End If
    
    
    If ClienteEncontrado Then
     
      
        
    Else
       ' TxtRut.SetFocus
    End If
End Sub

Private Sub txtRut_Validate(Cancel As Boolean)
    TxtRazonSocial.Tag = ""
    If Len(TxtRut.Text) = 0 Then Exit Sub
    
    If CboNC.ListIndex = -1 Then
        MsgBox "Selecione Nota de Credito...", vbOKCancel
        CboNC.SetFocus
        Exit Sub
    End If
    
    LvDetalle.ListItems.Clear
    
    TxtRut.Text = Replace(TxtRut.Text, ".", "")
    TxtRut.Text = Replace(TxtRut.Text, "-", "")
    Respuesta = VerificaRut(TxtRut.Text, NuevoRut)
   
    If Respuesta = True Then
        Me.TxtRut.Text = NuevoRut
        'Buscar el cliente
        Sql = "SELECT rut_cliente,nombre_rsocial,giro,direccion,comuna,fono,email, " & _
               "cli_monto_credito " & _
              "FROM maestro_clientes m " & _
              "INNER JOIN par_asociacion_ruts  a ON m.rut_cliente=a.rut_cli " & _
              "LEFT JOIN par_lista_precios l ON m.lst_id=l.lst_id " & _
              "WHERE habilitado='SI' AND a.rut_emp='" & SP_Rut_Activo & "' AND rut_cliente = '" & Me.TxtRut.Text & "'"
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
                'Cliente encontrado
            With RsTmp
                TxtMontoCredito = !cli_monto_credito
         
                TxtRut.Text = !rut_cliente
                TxtGiro = !giro
                TxtDireccion = !direccion
                txtComuna = !comuna
                txtFono = !fono
                TxtEmail = !Email
                TxtRazonSocial.Text = !nombre_rsocial
               
                ClienteEncontrado = True
              
                
     
                
                If Val(TxtMontoCredito) > 0 Then
                    'Debemos consultar el saldo de credito disponible del cliente.
                    TxtCupoUtilizado = ConsultaSaldoCliente(TxtRut)
                    SkCupoCredito = NumFormat(Val(TxtMontoCredito) - Val(TxtCupoUtilizado))
                End If
                
                If Sm_BuscaPorNumero = "SI" Then
                    LLena_Referenciado CboNroBuscar.Tag
                    Exit Sub
                End If
                
                If Sm_Nc_Boleta = "SI" Then Exit Sub ' si es nc por boleta, no debmos mostrar las nc de este rut
                 'buscaremos documentos emitidos a este cliente
                '23-9-2015
                Filtro = "doc_nc_por_boleta='" & Sm_Nc_Boleta & "' AND "
                Sql = "SELECT v.id,fecha,CONCAT(tipo_doc,' ',IF(doc_factura_guias='SI','POR GUIA(S)','')) tipo_d ,no_documento,v.rut_cliente,IFNULL(cli_nombre_fantasia,nombre_rsocial) nombre,neto,iva,bruto " & _
                      "FROM ven_doc_venta v,sis_documentos d,maestro_clientes c " & _
                      "WHERE doc_nombre NOT LIKE 'GUIA%' AND doc_nota_de_credito='NO' AND " & Filtro & "v.rut_emp='" & SP_Rut_Activo & "' AND c.rut_cliente=v.rut_cliente AND v.doc_id=d.doc_id AND doc_referenciable='SI' AND v.rut_cliente='" & TxtRut & "' " & _
                      "ORDER BY id DESC"
                Consulta RsTmp, Sql
                If RsTmp.RecordCount > 0 Then
                    iP_Tipo_NC = 0
                    SG_codigo2 = CboNC.ItemData(CboNC.ListIndex)
                    vta_BuscaDocumentos.SM_SoloSelecciona = "SI"
                 '   vta_BuscaDocumentos.CmdContinuar.Visible = False
                    vta_BuscaDocumentos.CmdNC.Visible = True
                    vta_BuscaDocumentos.B_Seleccion = True
                    'ver documentos del cliente
                    
                    vta_BuscaDocumentos.Show 1
                    If venPosNC.iP_Tipo_NC > 0 Then
                        LLena_Referenciado venPosNC.lP_Documento_Referenciado
                         
                    End If
                    
                    'if DG_ID_Unico = Me.LP_Documento_Referenciado
                Else
                    If Sm_Nc_Boleta = "NO" Then
                        MsgBox "El cliente no tiene facturas registradas en el sistema...", vbInformation
                        TxtRut.SetFocus
                        Exit Sub
                    End If
                    
                End If
                
                
                
                
            End With
                
            If bm_SoloVistaDoc Then Exit Sub
                                
        
        Else
                'Cliente no existe aun �crearlo??
                Respuesta = MsgBox("Cliente no encontrado..." & Chr(13) & _
                "     �Desea Crear?    ", vbYesNo + vbQuestion)
                If Respuesta = 6 Then
                    'crear
                    SG_codigo = ""
                    AccionCliente = 4
                    AgregoCliente.TxtRut.Text = Me.TxtRut.Text
                    
                    AgregoCliente.TxtRut.Locked = True
                    ClienteEncontrado = False
                    AgregoCliente.Timer1.Enabled = True
            
                    AgregoCliente.Show 1
                    If ClienteEncontrado = True Then
                        txtRut_Validate False
                       
                        TxtCodigo.SetFocus
                    Else
                        txtRut_Validate True
                    End If
                Else
                    'volver al rut
                    ClienteEncontrado = False
                    Me.TxtRut.Text = ""
                    On Error Resume Next
                    SendKeys "+{TAB}" ' Shift TAB retrocede al control anterior segun el orden del tabindex
                End If
        
          
        End If
       
    Else
        Me.TxtRut.Text = ""
        On Error Resume Next
        SendKeys "+{TAB}" ' Shift TAB retrocede al control anterior segun el orden del tabindex
    End If
    Exit Sub
CancelaImpesionFacturacion:
    'No imprime
    Unload Me
End Sub

Private Sub txtTotalDocBuscado_GotFocus()
    En_Foco txtTotalDocBuscado
End Sub

Private Sub txtTotalDocBuscado_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
    If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub txtTotalDocBuscado_Validate(Cancel As Boolean)
    If Val(txtTotalDocBuscado) = 0 Then txtTotalDocBuscado = "0"
End Sub
Private Sub SumaGrilla()
    TxtTotalNC = NumFormat(TotalizaColumna(LvDetalle, "total"))
End Sub
Private Sub NotaCreditoElectronica(Tipo As String, NroDocumento As Long, Docid As Integer, PorBoletas As Boolean)
    Dim Sp_Xml As String, Sp_XmlAdicional As String
    Dim obj As DTECloud.Integracion
    Dim Lp_Sangria As Long
    Dim Sp_Sql As String
    Dim Bp_Adicional As Boolean
    Dim Dp_Neto As Long
    Dim Dp_Iva As Long
    Dim Dp_Total As Long
    '3 Octubre 2015 Pos
    'Modulo genera NC electronica
    'Autor: alvamar
    Bp_Adicional = False
    
    '1ro Crear xml
    Lp_Sangria = 4
    X = FreeFile
    
   ' Sp_Xml = App.Path & "\dte\" & CboDocVenta.ItemData(CboDocVenta.ListIndex) & "_" & TxtNroDocumento & ".xml"
    Sp_Xml = "C:\FACTURAELECTRONICA\nueva.xml"
      'Open Sp_Archivo For Output As X
    Open Sp_Xml For Output As X
        '<?xml version="1.0" encoding="ISO-8859-1"?>
        Print #X, "<DTE version=" & Chr(34) & "1.0" & Chr(34) & ">"
        Print #X, "" & Space(Lp_Sangria) & "<Documento ID=" & Chr(34) & "ID" & Lp_Id_Venta & Chr(34) & ">"
        'Encabezado
        Print #X, "" & Space(Lp_Sangria) & "<Encabezado>"
            'id documento
            Print #X, "" & Space(Lp_Sangria * 2) & "<IdDoc>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<TipoDTE>" & Tipo & "</TipoDTE>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<Folio>" & NroDocumento & "</Folio>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<FchEmis>" & Fql(DtFecha) & "</FchEmis>"
           '     Print #X, "" & Space(Lp_Sangria * 3) & "<FchVenc>" & Format(DtFecha.Value + CboPlazos.ItemData(CboPlazos.ListIndex), "; YYYY - MM - DD; ") & "</FchVenc>"
                If Sm_Con_Precios_Brutos = "SI" Then
                    '30 Mayo 2015, cuando las lineas de detalle son precios brutos se debe identificar aqui.
                 '   Print #X, "" & Space(Lp_Sangria * 2) & "<MntBruto>1</MntBruto>"
                End If
            Print #X, "" & Space(Lp_Sangria * 2) & "</IdDoc>"
            'Emisor
            'Select empresa para obtener los datos del emisor
            Sp_Sql = "SELECT nombre_empresa,direccion,comuna,ciudad,giro,emp_codigo_actividad_economica actividad " & _
                    "FROM sis_empresas " & _
                    "WHERE rut='" & SP_Rut_Activo & "'"
            Consulta RsTmp, Sp_Sql
            
            Print #X, "" & Space(Lp_Sangria * 2) & " <Emisor>"
                'Print #X, "" & Space(Lp_Sangria * 3) & "<RUTEmisor>" & Replace(SP_Rut_Activo, ".", "") & "</RUTEmisor>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<RUTEmisor>" & Replace(SP_Rut_Activo, ".", "") & "</RUTEmisor>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<RznSoc>" & Replace(RsTmp!nombre_empresa, "�", "N") & "</RznSoc>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<GiroEmis>" & Replace(RsTmp!giro, "�", "N") & "</GiroEmis>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<Acteco>" & Replace(RsTmp!actividad, "�", "N") & "</Acteco>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<DirOrigen>" & Replace(RsTmp!direccion, "�", "N") & "</DirOrigen>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<CmnaOrigen>" & Replace(RsTmp!comuna, "�", "N") & "</CmnaOrigen>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<CiudadOrigen>" & Replace(RsTmp!ciudad, "�", "N") & "</CiudadOrigen>"
            Print #X, "" & Space(Lp_Sangria * 2) & "</Emisor>"
            
            'Receptor
            
            'Select datos cliente
            Sp_Sql = "SELECT nombre_rsocial,giro,direccion,comuna,ciudad " & _
                        "FROM maestro_clientes " & _
                        "WHERE rut_cliente='" & TxtRut & "'"
            Consulta RsTmp, Sp_Sql
            
            Print #X, "" & Space(Lp_Sangria * 2) & " <Receptor>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<RUTRecep>" & Replace(TxtRut, ".", "") & "</RUTRecep>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<RznSocRecep>" & QuitarAcentos(Mid(Replace(RsTmp!nombre_rsocial, "�", "N"), 1, 100)) & "</RznSocRecep>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<GiroRecep>" & QuitarAcentos(Mid(Replace(RsTmp!giro, "�", "N"), 1, 40)) & "</GiroRecep>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<Contacto></Contacto>"
                'Print #X, "" & Space(Lp_Sangria * 3) & "<DirRecep>" & QuitarAcentos(Mid(Replace(RsTmp!direccion, "�", "N"), 1, 60)) & "</DirRecep>"
                    Print #X, "" & Space(Lp_Sangria * 3) & "<DirRecep>" & QuitarAcentos(Mid(Replace(TxtDireccion, "�", "N"), 1, 60)) & "</DirRecep>"
                'Print #X, "" & Space(Lp_Sangria * 3) & "<CmnaRecep>" & QuitarAcentos(Mid(Replace(RsTmp!comuna, "�", "N"), 1, 20)) & "</CmnaRecep>"
                
                Print #X, "" & Space(Lp_Sangria * 3) & "<CmnaRecep>" & QuitarAcentos(Mid(Replace(txtComuna, "�", "N"), 1, 20)) & "</CmnaRecep>"
                
                
                Print #X, "" & Space(Lp_Sangria * 3) & "<CiudadRecep>" & QuitarAcentos(Mid(Replace(RsTmp!ciudad, "�", "N"), 1, 20)) & "</CiudadRecep>"
            Print #X, "" & Space(Lp_Sangria * 2) & "</Receptor>"
            
             'Totales
            Dp_Total = CDbl(TxtTotalNC)
            Dp_Neto = Round(Dp_Total / Val("1." & DG_IVA), 0)
            Dp_Iva = Dp_Total - Dp_Neto
             
            Print #X, "" & Space(Lp_Sangria * 2) & " <Totales>"
                If LvDetalle.ListItems(1).SubItems(8) = "EXENT" Then
                        Print #X, "" & Space(Lp_Sangria * 3) & "<MntNeto>" & 0 & "</MntNeto>"
                        Print #X, "" & Space(Lp_Sangria * 3) & "<MntExe>" & Dp_Total & "</MntExe>"
                     '   Print #X, "" & Space(Lp_Sangria * 3) & " <TasaIVA>" & 0 & "</TasaIVA>"
                        Print #X, "" & Space(Lp_Sangria * 3) & " <IVA>" & 0 & "</IVA>"
                
                
                
                
                Else
                        
                        Print #X, "" & Space(Lp_Sangria * 3) & "<MntNeto>" & Dp_Neto & "</MntNeto>"
                        Print #X, "" & Space(Lp_Sangria * 3) & "<MntExe>" & 0 & "</MntExe>"
                        Print #X, "" & Space(Lp_Sangria * 3) & " <TasaIVA>" & DG_IVA & "</TasaIVA>"
                        Print #X, "" & Space(Lp_Sangria * 3) & " <IVA>" & Dp_Iva & "</IVA>"
                End If
                    'Antes del monto total debemos verificar si la factura incluye impuestos retenidos
    '                                         <ImptoReten>
    '                                            <TipoImp>18</TipoImp>
    '                                            <TasaImp>5</TasaImp>
    '                                            <MontoImp>8887</MontoImp>
    '                                        </ImptoReten>
                Print #X, "" & Space(Lp_Sangria * 3) & "<MntTotal>" & Dp_Total & "</MntTotal>"
            Print #X, "" & Space(Lp_Sangria * 2) & "</Totales>"
                
            'Cerramos encabezado
            Print #X, "" & Space(Lp_Sangria) & "</Encabezado>"
                
                
           'Comenzamos el detalle
           
           If SP_Control_Inventario = "SI" Then
           'Recorremos la grilla con productos
           
                If Im_TipoNC = 1 Then
                        'SI ES NC POR ANULACION,
                        'LEEMOS LOS MISMO DATOS DE LA VENTA
                    '    If LvDetalle.SelectedItem.SubItems(7) = "NO" Then
                            Sql = "SELECT codigo,unidades,descripcion,precio_final,ved_codigo_interno,ved_precio_venta_neto,ROUND(ved_precio_venta_neto/unidades,2) unitario  " & _
                                    "FROM ven_detalle " & _
                                    "WHERE doc_id=" & Docid & " AND no_documento=" & NroDocumento & " AND rut_emp='" & SP_Rut_Activo & "'"
                    '    Else
                    '        Sql = "SELECT unidades,descripcion,precio_final,ved_codigo_interno,ved_precio_venta_neto,ROUND(ved_precio_venta_neto/unidades,2) unitario  " & _
                    '                "FROM ven_detalle d " & _
                                    "JOIN  ven_doc_venta v ON d.doc_id=v.doc_id AND d.no_documento=v.no_documento " & _
                                    "WHERE doc_id_factura=" & Docid & " AND nro_factura=" & NroDocumento & " d.AND rut_emp='" & SP_Rut_Activo & "' AND v.rut_activo='" & SP_Rut_Activo & "'"
                        
                    '    End If
                        Consulta RsTmp, Sql
                        i = 1
                        RsTmp.MoveFirst
                        Do While Not RsTmp.EOF
                            'For i = 1 To LvMateriales.ListItems.Count
                              Print #X, "" & Space(Lp_Sangria) & "<Detalle>"
                              Print #X, "" & Space(Lp_Sangria * 2) & "<NroLinDet>" & i & "</NroLinDet>"
                              Print #X, "" & Space(Lp_Sangria * 2) & " <CdgItem>"
                              Print #X, "" & Space(Lp_Sangria * 3) & "<TpoCodigo>PLU</TpoCodigo>"
                              Print #X, "" & Space(Lp_Sangria * 3) & "<VlrCodigo>" & "" & RsTmp!Codigo & "</VlrCodigo>"
                              Print #X, "" & Space(Lp_Sangria * 2) & "</CdgItem>"
                              
                              nombre = RsTmp!Descripcion
                              If Len(nombre) > 80 Then
                                    nombre = Mid(QuitarAcentos(LvMateriales.ListItems(i).SubItems(3)), 1, InStr(1, QuitarAcentos(RsTmp!Descripcion), " ", vbTextCompare))
                                    Print #X, "" & Space(Lp_Sangria * 2) & "<NmbItem>" & nombre & "</NmbItem>"
                                
                                    nombre = Mid(QuitarAcentos(LvMateriales.ListItems(i).SubItems(3)), InStr(1, QuitarAcentos(RsTmp!Descripcion), " ", vbTextCompare))
                                    Print #X, "" & Space(Lp_Sangria * 2) & "<DscItem>" & nombre & "</DscItem>"
                              Else
                              
                                    Print #X, "" & Space(Lp_Sangria * 2) & "<NmbItem>" & QuitarAcentos(Replace(Mid(RsTmp!Descripcion, 1, 80), "�", "N")) & "</NmbItem>"
                              End If
                              Print #X, "" & Space(Lp_Sangria * 2) & "<QtyItem>" & Replace(RsTmp!Unidades, ",", ".") & "</QtyItem>"
                       '       Print #X, "" & Space(Lp_Sangria * 2) & "<PrcItem>" & Replace(RsTmp!unitario, ",", ".") & "</PrcItem>"
                              'Print #X, "" & Space(Lp_Sangria * 2) & "<PrcItem>2065699</PrcItem>"
                              Print #X, "" & Space(Lp_Sangria * 2) & "<PrcItem>" & Replace(Round(RsTmp!unitario, 2), ",", ".") & "</PrcItem>"
              '                    <DescuentoPct>15</DescuentoPct>
              '                    <DescuentoMonto>4212</DescuentoMonto>
              '                             <!- Solo se indica el c�digo del impuesto adicional '
              '                    <CodImpAdic>17</CodImpAdic>
                              Print #X, "" & Space(Lp_Sangria * 2) & "<MontoItem>" & Replace(Round(RsTmp!ved_precio_venta_neto, 0), ",", ".") & "</MontoItem>"
                      
                            Print #X, "" & Space(Lp_Sangria) & "</Detalle>"
                             'Next
                            RsTmp.MoveNext
                            i = i + 1
                        Loop
                        
                         dsctoGeneral = CDbl(TxtDescuentoNC)
                        If dsctoGeneral > 0 Then
                            dsctoGeneral = Round(dsctoGeneral / (1.19), 0)
                            Print #X, "" & Space(Lp_Sangria) & "<DscRcgGlobal>"
                                   Print #X, "" & Space(Lp_Sangria * 2) & "<NroLinDR>1</NroLinDR>"
                                   Print #X, "" & Space(Lp_Sangria * 2) & "<TpoMov>D</TpoMov>"
                                   Print #X, "" & Space(Lp_Sangria * 2) & "<GlosaDR>Dscto. Cliente</GlosaDR>"
                                    Print #X, "" & Space(Lp_Sangria * 2) & "<TpoValor>$</TpoValor>"
                                   Print #X, "" & Space(Lp_Sangria * 2) & "<ValorDR>" & dsctoGeneral & "</ValorDR>"
                            Print #X, "" & Space(Lp_Sangria) & "</DscRcgGlobal>"
                        
                                '<DscRcgGlobal>
                                '    <NroLinDR>1</NroLinDR>
                                '    <TpoMov>D</TpoMov>
                                '    <GlosaDR>Descuento global al documento</GlosaDR>
                                ''    <TpoValor>%</TpoValor>
                                 '   <ValorDR>12</ValorDR>
                                '</DscRcgGlobal>
                        End If
                        
                        
                        
                        
                        
                        
                        
                Else
                    'CUANDO ES PARCIAL, SOLO SE HARA POR LOS PRODUCTOS QUE ESTA SACANDO
                    'LOS QUE ESTAN EN LA GRILLA MARCADOS
                    n = 1
                    For i = 1 To LvVenta.ListItems.Count
                        If LvVenta.ListItems(i).Checked Then
                            Print #X, "" & Space(Lp_Sangria) & "<Detalle>"
                              Print #X, "" & Space(Lp_Sangria * 2) & "<NroLinDet>" & n & "</NroLinDet>"
                              Print #X, "" & Space(Lp_Sangria * 2) & " <CdgItem>"
                              Print #X, "" & Space(Lp_Sangria * 3) & "<TpoCodigo>PLU</TpoCodigo>"
                              Print #X, "" & Space(Lp_Sangria * 3) & "<VlrCodigo>" & LvVenta.ListItems(i).SubItems(1) & "</VlrCodigo>"
                              Print #X, "" & Space(Lp_Sangria * 2) & "</CdgItem>"
                              nombre = LvVenta.ListItems(i).SubItems(2)
                            If Len(nombre) > 80 Then
                                    nombre = Mid(QuitarAcentos(LvVenta.ListItems(i).SubItems(2)), 1, InStr(1, QuitarAcentos(LvVenta.ListItems(i).SubItems(2)), " ", vbTextCompare))
                                    Print #X, "" & Space(Lp_Sangria * 2) & "<NmbItem>" & nombre & "</NmbItem>"
                                
                                    nombre = Mid(QuitarAcentos(LvVenta.ListItems(i).SubItems(2)), InStr(1, QuitarAcentos(LvVenta.ListItems(i).SubItems(2)), " ", vbTextCompare))
                                    Print #X, "" & Space(Lp_Sangria * 2) & "<DscItem>" & nombre & "</DscItem>"
                              Else
                              
                                    Print #X, "" & Space(Lp_Sangria * 2) & "<NmbItem>" & QuitarAcentos(LvVenta.ListItems(i).SubItems(2)) & "</NmbItem>"
                              End If
                             ' Print #X, "" & Space(Lp_Sangria * 2) & "<NmbItem>" & QuitarAcentos(Mid(LvVenta.ListItems(i).SubItems(2), 1, 80)) & "</NmbItem>"
                              Print #X, "" & Space(Lp_Sangria * 2) & "<QtyItem>" & CxP(LvVenta.ListItems(i).SubItems(3)) & "</QtyItem>"
                              If LvDetalle.ListItems(1).SubItems(8) = "BRUTO" Then
                                
                                    Print #X, "" & Space(Lp_Sangria * 2) & "<PrcItem>" & Round(CDbl(LvVenta.ListItems(i).SubItems(4)) / (1.19), 0) & "</PrcItem>"
                                    Print #X, "" & Space(Lp_Sangria * 2) & "<MontoItem>" & Round(CDbl(LvVenta.ListItems(i).SubItems(5)) / (1.19), 0) & "</MontoItem>"
                              Else
                                  
                                    Print #X, "" & Space(Lp_Sangria * 2) & "<PrcItem>" & CDbl(LvVenta.ListItems(i).SubItems(4)) & "</PrcItem>"
                                    Print #X, "" & Space(Lp_Sangria * 2) & "<MontoItem>" & CDbl(LvVenta.ListItems(i).SubItems(5)) & "</MontoItem>"
                              End If
                              
                            Print #X, "" & Space(Lp_Sangria) & "</Detalle>"
                            n = n + 1
                        
                        End If
                        
                    Next
                
                
                
                
                End If
            Else
                'CUANDO ES SIN INVENTARIO
                
            
            
            
            End If
            'Cerramos detalle
            
                
'        If FrmReferencia.Visible Then
'                'CREAR REFERENCIA
                Print #X, "" & Space(Lp_Sangria) & "<Referencia>"
                    If Not PorBoletas Then
                        'NC por factura
                        
                     
                            Print #X, "" & Space(Lp_Sangria * 2) & "<NroLinRef>1</NroLinRef>"
                            Print #X, "" & Space(Lp_Sangria * 2) & "<TpoDocRef>" & Im_id_Doc_SII & "</TpoDocRef>"
                            Print #X, "" & Space(Lp_Sangria * 2) & "<FolioRef>" & LvDetalle.ListItems(1).SubItems(2) & "</FolioRef>"
        
                            Print #X, "" & Space(Lp_Sangria * 2) & "<FchRef>" & Fql(LvDetalle.ListItems(1).SubItems(3)) & "</FchRef>"
                            Print #X, "" & Space(Lp_Sangria * 2) & "<CodRef>" & Im_TipoNC & "</CodRef>"
                            If Im_TipoNC = 1 Then
                                Print #X, "" & Space(Lp_Sangria * 2) & "<RazonRef>ANULA DOCUMENTO</RazonRef>"
                            ElseIf Im_TipoNC = 2 Then
                                Print #X, "" & Space(Lp_Sangria * 2) & "<RazonRef>" & TxtComentario & "</RazonRef>"
                            Else
                                If TxtComentario = "Escriba aqui comentario para la NC" Then
                                
                                    Print #X, "" & Space(Lp_Sangria * 2) & "<RazonRef>DEVOLUCION DE MERCADERIAS</RazonRef>"
                                Else
                                    Print #X, "" & Space(Lp_Sangria * 2) & "<RazonRef>" & TxtComentario & "</RazonRef>"
                                End If
                            End If
                    Else
                        
                            Print #X, "" & Space(Lp_Sangria * 2) & "<NroLinRef>1</NroLinRef>"
                            Print #X, "" & Space(Lp_Sangria * 2) & "<TpoDocRef>35</TpoDocRef>"
                            Print #X, "" & Space(Lp_Sangria * 2) & "<FolioRef>" & LvDetalle.ListItems(1).SubItems(2) & "</FolioRef>"
        
                            Print #X, "" & Space(Lp_Sangria * 2) & "<FchRef>" & Fql(LvDetalle.ListItems(1).SubItems(3)) & "</FchRef>"
                            Print #X, "" & Space(Lp_Sangria * 2) & "<CodRef>" & Im_TipoNC & "</CodRef>"
                            If Im_TipoNC = 1 Then
                                Print #X, "" & Space(Lp_Sangria * 2) & "<RazonRef>ANULA DOCUMENTO</RazonRef>"
                            
                            Else
                                If TxtComentario = "Escriba aqui comentario para la NC" Then
                                
                                    Print #X, "" & Space(Lp_Sangria * 2) & "<RazonRef>DEVOLUCION DE MERCADERIAS</RazonRef>"
                                Else
                                    Print #X, "" & Space(Lp_Sangria * 2) & "<RazonRef>" & TxtComentario & "</RazonRef>"
                                End If
                            End If
                        'Nc por boletas
                        
                        
                    End If
                    
                 Print #X, "" & Space(Lp_Sangria) & "</Referencia>"
                    


        Print #X, "" & Space(Lp_Sangria) & "</Documento>"
        Print #X, "" & Space(Lp_Sangria) & "</DTE>"
    Close #X
    Bp_Adicional = False
    If Len(TxtComentario) > 0 Then
        Bp_Adicional = True
        
        X = FreeFile
        Sp_XmlAdicional = "C:\nueva_adicional.xml"
        Open Sp_XmlAdicional For Output As X
            Print #X, "<Adicional>"
                Print #X, "" & Space(Lp_Sangria) & "<Uno>" & TxtComentario & "</Uno>"
            Print #X, "</Adicional>"
        Close #X
    End If
'        End
'    If Len(TxtComentario) > 0 Or Len(TxtOrdenesCompra) > 0 Then
'        Bp_Adicional = True

'            Print #X, "<Adicional>"
'            If Len(TxtComentario) > 0 Then
'                Print #X, "" & Space(Lp_Sangria) & "<Uno>" & TxtComentario & "</Uno>"
'                If Len(TxtOrdenesCompra) > 0 Then
'                    Print #X, "" & Space(Lp_Sangria) & "<Dos>" & TxtOrdenesCompra & "</Dos>"
'                End If
'            Else
'                If Len(TxtOrdenesCompra) > 0 Then
'                    Print #X, "" & Space(Lp_Sangria) & "<Uno>" & TxtOrdenesCompra & "</Uno>"
'                End If
'            End If
'            Print #X, "" & Space(Lp_Sangria) & "<Tres></Tres>"
'            Print #X, "" & Space(Lp_Sangria) & "<Cuatro></Cuatro>"
'            Print #X, "" & Space(Lp_Sangria) & "<Cinco></Cinco>"
'            Print #X, "" & Space(Lp_Sangria) & "<Seis></Seis>"
'            Print #X, "" & Space(Lp_Sangria) & "<Siete></Siete>"
'            Print #X, "" & Space(Lp_Sangria) & "<Ocho></Ocho>"
'            Print #X, "" & Space(Lp_Sangria) & "<Nueve></Nueve>"
'            Print #X, "" & Space(Lp_Sangria) & "<Diez></Diez>"
'            Print #X, "</Adicional>"
'        Close #X
'
'
'    End If
        
        Set obj = New DTECloud.Integracion
   
        obj.UrlServicioFacturacion = SG_Url_Factura_Electronica '"http://wsdtedesa.dyndns.biz/WSDTE/Service.asmx"
        obj.HabilitarDescarga = True
        obj.FormatoNombrePDF = "0"
        obj.CarpetaDestino = "C:\FACTURAELECTRONICA\PDF"
        obj.Password = "1234"
        
        obj.AsignarFolio = False
        obj.IncluirCopiaCesion = True
       ' obj.FolioAsignado
        
        
        'MSXML.DOMDocument
        Dim Documento As MSXML2.DOMDocument30
        Set Documento = New DOMDocument30
        
        Documento.preserveWhiteSpace = True
        Documento.Load (Sp_Xml)
        '  Dim oXMLAdicional As New xml.XMLDocument
        Dim documento2 As MSXML2.DOMDocument30
        Set documento2 = New DOMDocument30
        
        documento2.preserveWhiteSpace = True
        If Bp_Adicional Then
            documento2.Load (Sp_XmlAdicional)
        Else
            documento2.Load ("")
        End If
        
        '******************************* modo productivo
        
        obj.Productivo = BG_ModoProductivoDTE
        
        '***********************************
        If obj.ProcesarXMLBasico(Documento.XML, documento2.XML) Then
             'MsgBox obj.URLPDF
             Archivo = "C:\FACTURAELECTRONICA\PDF\T" & Tipo & "F" & NroDocumento & ".PDF"
            ' dte_MostrarPDF.Dte "33", Str(NroDocumento)
         '   If SP_Rut_Activo = "76.178.895-7" Then 'Solo total gomas
         '       DescargaForzadaDTE Trim(Str(NroDocumento))
            
         '   Else
                '16-09-2015
                'Se supone que esto es lo normal, sin embargo, totalgomas no funcionan
               ShellExecute Me.hWnd, "open", Archivo, "", "", 4
          '  End If
            
            
            ' MsgBox obj.URLPDF
        Else
            MsgBox (CStr(obj.IDResultado) + " " + obj.DescripcionResultado)
        End If
    
        

        
End Sub


Private Sub AbonaNCCredito(IdDocumento As String)
    Dim Lp_SaldoRef As Long
    Dim Lp_Id As Long
    Dim Lp_IdAbo As Long
    Dim Lp_Nro_Comprobante As Long
    
  'Buscar el documento referenciado    /// 20-10-2012
            'y ver si su saldo.
            'si el saldo es mayor o igual a la NC abonar el monto de la nota de credito
            
            Lp_SaldoRef = SaldoDocumento(IdDocumento, "CLI")
            
            Dim Lp_Abono_a_Realizar As Long
            Dim Lp_Abono_a_Pozo As Long
            Lp_Id = Val(IdDocumento)
            'If Lp_SaldoRef >= CDbl(TxtTotal) Then
            If Lp_SaldoRef > 0 Then
                    Bm_AfectarCaja = False
                    'hacer abono
                    lp_abono_a_plazo = 0
                    
                    If Lp_SaldoRef >= CDbl(Me.TxtTotalNC) Then
                        Lp_Abono_a_Realizar = CDbl(TxtTotalNC)
                    Else
                        Lp_Abono_a_Realizar = Lp_SaldoRef
                        Lp_Abono_a_Pozo = CDbl(TxtTotalNC) - Lp_SaldoRef
                    End If

                    
                    
                 '   MsgBox "El monto " & NumFormat(Lp_Abono_a_Realizar) & " de esta NC se abono al documento original...", vbInformation
                    Lp_Nro_Comprobante = AutoIncremento("lee", 200, , IG_id_Sucursal_Empresa)
                
                    'Dim lp_IDAbo As Long
                    Bp_Comprobante_Pago = True
                    Lp_IdAbo = UltimoNro("cta_abonos", "abo_id")
                    PagoDocumento Lp_IdAbo, "CLI", TxtRut, DtFecha, CDbl(Lp_Abono_a_Realizar), 8888, "NC", "ABONO CON NC", LogUsuario, Lp_Nro_Comprobante, Lp_Id, "NCVENTA"
                    Sql = "INSERT INTO cta_abono_documentos (abo_id,id,ctd_monto,rut_emp) VALUES "
                    Sql = Sql & "(" & Lp_IdAbo & "," & IdDocumento & "," & CDbl(Lp_Abono_a_Realizar) & ",'" & SP_Rut_Activo & "')"
                    cn.Execute Sql
                    
                    AutoIncremento "GUARDA", 200, Lp_Nro_Comprobante, IG_id_Sucursal_Empresa
                    
                    Sp_NC_Utilizada = "SI"
                    If Lp_Abono_a_Pozo > 0 Then
                        
                        Sql = "INSERT INTO cta_pozo (id_ref,pzo_fecha,rut_emp,pzo_cli_pro,pzo_rut,pzo_abono) " & _
                            "VALUES(" & Lp_Id & ",'" & Fql(DtFecha) & "','" & SP_Rut_Activo & "','CLI','" & Me.TxtRut & "'," & Lp_Abono_a_Pozo & ")"
                        cn.Execute Sql
                    
                    End If
                    
                    
            Else
                    'PagoDocumento Lp_Id, "PRO", Me.TxtRutProveedor, Me.DtFecha, Lp_SaldoRef, 8888, "DESCUENTA NC", "", LogUsuario,
                    Sql = "INSERT INTO cta_pozo (id_ref,pzo_fecha,rut_emp,pzo_cli_pro,pzo_rut,pzo_abono) " & _
                        "VALUES(" & Lp_Id & ",'" & Fql(DtFecha) & "','" & SP_Rut_Activo & "','CLI','" & TxtRut & "'," & Lp_Abono_a_Pozo & ")"
                    cn.Execute Sql
                    Sp_NC_Utilizada = "SI"
            End If
End Sub

Private Sub EstableImpresora(NombreImpresora As String)
   'Seteamos al impresora.
    Dim impresora As Printer
    ' Establece la impresora que se utilizar� para imprimir
    'Recorremos el objeto Printers
    For Each impresora In Printers
        'Si es igual al contenido se�alado en el combobox
        If UCase(impresora.DeviceName) = UCase(NombreImpresora) Then
            'Lo seteamos as�.
            Set Printer = impresora
            Exit For
        End If
    Next

End Sub

Public Sub ActualizaNC()
    Dim lp_Promedio As Double
    Dim Sp_Producto As String
    Dim Rp_Producto As Recordset
    
    Consulta RsTmp, Sm_Sql_Productos_NC
    If RsTmp.RecordCount > 0 Then
        RsTmp.MoveFirst
        Do While Not RsTmp.EOF
                Sp_Producto = "SELECT pro_inventariable " & _
                                "FROM maestro_productos " & _
                                "WHERE codigo='" & RsTmp!Codigo & "' AND rut_emp='" & SP_Rut_Activo & "'"
                Consulta Rp_Producto, Sp_Producto
                If Rp_Producto.RecordCount > 0 Then
                    If Rp_Producto!pro_inventariable = "SI" Then
                    
                        Sql = "UPDATE maestro_productos " & _
                          "SET stock_Actual = stock_actual -" & CxP(RsTmp!Unidades) & _
                          " WHERE  rut_emp='" & SP_Rut_Activo & "' AND  codigo='" & RsTmp!Codigo & "'"
                        cn.Execute Sql
                        
                       '  = 0
                        lp_Promedio = CostoAVG(RsTmp!Codigo, IG_id_Bodega_Ventas)
                        
                        
                      ' MsgBox Val(CxP(Val(.ListItems(i).SubItems(6))))
                        Kardex Fql(Date), "ENTRADA", Me.CboNC.ItemData(CboNC.ListIndex), _
                                        Val(Lp_Folio), IG_id_Bodega_Ventas, RsTmp!Codigo, Val(CxP(RsTmp!Unidades)), _
                                      "ANULA DOCUMENTO " & LvDetalle.ListItems(1).SubItems(1) & " " & LvDetalle.ListItems(1).SubItems(2), lp_Promedio, lp_Promedio * Val(CxP(RsTmp!Unidades)), Me.TxtRut.Text, Me.TxtRazonSocial, "NO", 0, CboNC.ItemData(CboNC.ListIndex), , , , 0
                    Else
                        'Al no ser inventariable, no pasa nada
                    End If
                End If
                RsTmp.MoveNext
        Loop
    End If
            '
End Sub
Private Sub LLena_Referenciado(Referenciado As Long)
                    Sql = "SELECT id,doc_nombre,no_documento,fecha,bruto,v.doc_id,doc_cod_sii,doc_factura_guias,doc_bruto_neto_en_venta netobruto " & _
                                "FROM ven_doc_venta v " & _
                                "JOIN sis_documentos d ON v.doc_id=d.doc_id " & _
                                "WHERE id=" & Referenciado
                         Consulta RsTmp, Sql
                         If RsTmp.RecordCount > 0 Then
                            TxtTotalNC = NumFormat(RsTmp!bruto)
                            LLenar_Grilla RsTmp, Me, LvDetalle, False, True, True, False
                            SumaGrilla
                         
                         End If
End Sub
