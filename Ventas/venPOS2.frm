VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{28D47522-CF84-11D1-834C-00A0249F0C28}#1.0#0"; "gif89.dll"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form venPOS 
   Caption         =   "Generar Contrato Arriendo"
   ClientHeight    =   8220
   ClientLeft      =   6780
   ClientTop       =   630
   ClientWidth     =   14490
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   8220
   ScaleWidth      =   14490
   Begin VB.Frame FrmLoad 
      BorderStyle     =   0  'None
      Height          =   1950
      Left            =   5250
      TabIndex        =   124
      Top             =   2880
      Visible         =   0   'False
      Width           =   2805
      Begin ACTIVESKINLibCtl.SkinLabel SkLoad 
         Height          =   270
         Left            =   210
         OleObjectBlob   =   "venPOS2.frx":0000
         TabIndex        =   125
         Top             =   120
         Width           =   2415
      End
      Begin GIF89LibCtl.Gif89a Gif89a1 
         Height          =   1320
         Left            =   180
         OleObjectBlob   =   "venPOS2.frx":0088
         TabIndex        =   126
         Top             =   420
         Width           =   2445
      End
   End
   Begin VB.Timer Timer1 
      Interval        =   500
      Left            =   60
      Top             =   60
   End
   Begin VB.Frame Frame1 
      Caption         =   "Maquinarias-DICOEM"
      Height          =   3930
      Left            =   30
      TabIndex        =   27
      Top             =   4245
      Width           =   14280
      Begin ACTIVESKINLibCtl.SkinLabel SkNnumcontrato 
         Height          =   390
         Left            =   2730
         OleObjectBlob   =   "venPOS2.frx":010E
         TabIndex        =   123
         Top             =   210
         Width           =   1920
      End
      Begin ACTIVESKINLibCtl.SkinLabel Sknnum 
         Height          =   300
         Left            =   420
         OleObjectBlob   =   "venPOS2.frx":016C
         TabIndex        =   122
         Top             =   330
         Width           =   2610
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkUbicacion 
         Height          =   300
         Left            =   3360
         OleObjectBlob   =   "venPOS2.frx":01F2
         TabIndex        =   101
         Top             =   195
         Width           =   5760
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkUltimoInventario 
         Height          =   255
         Left            =   13695
         OleObjectBlob   =   "venPOS2.frx":024F
         TabIndex        =   106
         ToolTipText     =   "Ultimo ajuste de inventario"
         Top             =   270
         Visible         =   0   'False
         Width           =   3720
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkUm 
         Height          =   240
         Left            =   9825
         OleObjectBlob   =   "venPOS2.frx":02C5
         TabIndex        =   105
         Top             =   375
         Visible         =   0   'False
         Width           =   345
      End
      Begin VB.CommandButton CmdVisorCotizaciones 
         Caption         =   "V"
         Height          =   495
         Left            =   12150
         TabIndex        =   103
         Top             =   3315
         Visible         =   0   'False
         Width           =   225
      End
      Begin VB.CommandButton CmdCargarCotizacion 
         Caption         =   "Cotizacion"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   510
         Left            =   10380
         TabIndex        =   102
         Top             =   3300
         Visible         =   0   'False
         Width           =   1740
      End
      Begin VB.CommandButton CmdF8 
         Caption         =   "F8 - Ficha Producto"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   510
         Left            =   7680
         TabIndex        =   100
         Top             =   3300
         Width           =   2670
      End
      Begin VB.Frame FrmPago 
         Caption         =   "Ultima Venta"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   1080
         Left            =   13155
         TabIndex        =   89
         Top             =   5415
         Visible         =   0   'False
         Width           =   5670
         Begin VB.TextBox TxtSumaPagos 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFF80&
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   14.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   405
            Left            =   2265
            Locked          =   -1  'True
            TabIndex        =   93
            TabStop         =   0   'False
            Text            =   "0"
            Top             =   540
            Width           =   1605
         End
         Begin VB.TextBox TxtAPagar 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFF80&
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   14.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   405
            Left            =   645
            Locked          =   -1  'True
            TabIndex        =   91
            TabStop         =   0   'False
            Text            =   "0"
            Top             =   540
            Width           =   1605
         End
         Begin VB.TextBox TxtSaldoPago 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFF80&
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   14.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   405
            Left            =   3885
            Locked          =   -1  'True
            TabIndex        =   90
            TabStop         =   0   'False
            Text            =   "0"
            Top             =   540
            Width           =   1605
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
            Height          =   375
            Index           =   0
            Left            =   2235
            OleObjectBlob   =   "venPOS2.frx":0327
            TabIndex        =   92
            Top             =   285
            Width           =   1605
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
            Height          =   375
            Index           =   1
            Left            =   360
            OleObjectBlob   =   "venPOS2.frx":0395
            TabIndex        =   94
            Top             =   300
            Width           =   1845
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
            Height          =   375
            Index           =   2
            Left            =   3645
            OleObjectBlob   =   "venPOS2.frx":03F9
            TabIndex        =   95
            Top             =   285
            Width           =   1830
         End
      End
      Begin VB.CommandButton CmdF5 
         Caption         =   "F5 - Emitir"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   14.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   510
         Left            =   375
         TabIndex        =   77
         Top             =   3300
         Width           =   2235
      End
      Begin VB.CommandButton CmdF7 
         Caption         =   "F7 - Anular Marcado"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   510
         Left            =   2670
         TabIndex        =   76
         Top             =   3300
         Width           =   2670
      End
      Begin VB.CommandButton CmdF9 
         Caption         =   "F9 - Anular Todo"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   510
         Left            =   5400
         TabIndex        =   75
         Top             =   3300
         Width           =   2235
      End
      Begin VB.TextBox TxtCodigo 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   18
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   465
         Left            =   405
         TabIndex        =   24
         Top             =   975
         Width           =   2910
      End
      Begin VB.CommandButton CmdAceptaLinea 
         Appearance      =   0  'Flat
         BackColor       =   &H8000000A&
         Caption         =   "Ok"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   450
         Left            =   12150
         TabIndex        =   26
         Top             =   990
         Width           =   1245
      End
      Begin VB.TextBox TxtStockLinea 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   18
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   465
         Left            =   15300
         Locked          =   -1  'True
         TabIndex        =   68
         TabStop         =   0   'False
         Text            =   "0"
         Top             =   990
         Visible         =   0   'False
         Width           =   765
      End
      Begin VB.TextBox TxtTotalLinea 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   18
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   465
         Left            =   14430
         Locked          =   -1  'True
         TabIndex        =   67
         TabStop         =   0   'False
         Text            =   "0"
         Top             =   990
         Visible         =   0   'False
         Width           =   855
      End
      Begin VB.TextBox TxtPrecio 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   18
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   465
         Left            =   10155
         TabIndex        =   25
         Text            =   "0"
         Top             =   975
         Width           =   1935
      End
      Begin VB.TextBox TxtCantidad 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   18
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   465
         Left            =   10545
         TabIndex        =   23
         Text            =   "1"
         Top             =   210
         Visible         =   0   'False
         Width           =   1380
      End
      Begin VB.TextBox TxtDescripcion 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   15.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   465
         Left            =   3300
         MultiLine       =   -1  'True
         TabIndex        =   66
         TabStop         =   0   'False
         Top             =   975
         Width           =   6855
      End
      Begin VB.TextBox TxtTemp 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00000000&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   18
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H0000FF00&
         Height          =   465
         Left            =   7800
         TabIndex        =   64
         Text            =   "0"
         Top             =   510
         Visible         =   0   'False
         Width           =   1545
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   240
         Index           =   8
         Left            =   390
         OleObjectBlob   =   "venPOS2.frx":045B
         TabIndex        =   65
         Top             =   675
         Width           =   2880
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   225
         Index           =   9
         Left            =   3300
         OleObjectBlob   =   "venPOS2.frx":04DF
         TabIndex        =   69
         Top             =   750
         Width           =   1530
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   240
         Index           =   3
         Left            =   12195
         OleObjectBlob   =   "venPOS2.frx":054B
         TabIndex        =   70
         Top             =   150
         Visible         =   0   'False
         Width           =   1395
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   240
         Index           =   10
         Left            =   10110
         OleObjectBlob   =   "venPOS2.frx":05B1
         TabIndex        =   71
         Top             =   735
         Width           =   615
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   240
         Index           =   12
         Left            =   13440
         OleObjectBlob   =   "venPOS2.frx":0613
         TabIndex        =   72
         Top             =   630
         Visible         =   0   'False
         Width           =   510
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   240
         Index           =   11
         Left            =   14640
         OleObjectBlob   =   "venPOS2.frx":0673
         TabIndex        =   73
         Top             =   750
         Visible         =   0   'False
         Width           =   1440
      End
      Begin MSComctlLib.ListView LvVenta 
         Height          =   1800
         Left            =   375
         TabIndex        =   74
         Top             =   1455
         Width           =   13020
         _ExtentX        =   22966
         _ExtentY        =   3175
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483646
         BackColor       =   -2147483643
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   11
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "N109"
            Text            =   "Id Interno"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "T1000"
            Text            =   "Codigo"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "T3000"
            Text            =   "Descripci�n"
            Object.Width           =   5292
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   3
            Object.Tag             =   "N102"
            Text            =   "Cantidad"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   4
            Object.Tag             =   "N100"
            Text            =   "Precio"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   5
            Text            =   "Total"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   6
            Text            =   "Stock"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   7
            Object.Tag             =   "T1000"
            Text            =   "Inventario"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   8
            Text            =   "Precio Costo"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   9
            Text            =   "Dscto Volumen"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   10
            Object.Tag             =   "T1000"
            Text            =   "UM"
            Object.Width           =   0
         EndProperty
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkInventariable 
         Height          =   240
         Left            =   16050
         OleObjectBlob   =   "venPOS2.frx":06D3
         TabIndex        =   78
         Top             =   600
         Visible         =   0   'False
         Width           =   1440
      End
   End
   Begin VB.Frame FrmAccesorios 
      Caption         =   "Accesorios"
      Height          =   2265
      Left            =   45
      TabIndex        =   114
      Top             =   8190
      Visible         =   0   'False
      Width           =   14235
      Begin VB.CommandButton CmdAccesorios 
         Caption         =   "Agregar"
         Height          =   345
         Left            =   12390
         TabIndex        =   116
         Top             =   210
         Width           =   840
      End
      Begin VB.TextBox TxtAccesorio 
         Height          =   330
         Left            =   1365
         TabIndex        =   115
         Top             =   210
         Width           =   10980
      End
      Begin MSComctlLib.ListView LvAccesorios 
         Height          =   1620
         Left            =   1365
         TabIndex        =   117
         Top             =   570
         Width           =   11850
         _ExtentX        =   20902
         _ExtentY        =   2858
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483646
         BackColor       =   -2147483643
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   2
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Descripcion Accesorios"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Descripcion Accesorios"
            Object.Width           =   19403
         EndProperty
      End
   End
   Begin VB.TextBox TxtRutFirma 
      Height          =   315
      Left            =   5610
      TabIndex        =   10
      Top             =   3015
      Width           =   1710
   End
   Begin VB.TextBox TxtNombreFirma 
      Height          =   315
      Left            =   1590
      TabIndex        =   9
      Top             =   3030
      Width           =   2595
   End
   Begin VB.Frame Frame5 
      Caption         =   "Detalle de Arriendo"
      Height          =   4155
      Left            =   7485
      TabIndex        =   40
      Top             =   90
      Width           =   6780
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel17 
         Height          =   225
         Left            =   2370
         OleObjectBlob   =   "venPOS2.frx":073D
         TabIndex        =   121
         Top             =   3735
         Width           =   1170
      End
      Begin VB.ComboBox CmbListaPrecios 
         Height          =   315
         Left            =   3540
         Style           =   2  'Dropdown List
         TabIndex        =   22
         Top             =   3675
         Width           =   3105
      End
      Begin VB.CheckBox ChkAnexo 
         Caption         =   "Anexo Accesorios"
         Height          =   300
         Left            =   165
         TabIndex        =   21
         Tag             =   "Al Seleccionar, se creara anexo de accesorios."
         Top             =   3705
         Width           =   1695
      End
      Begin VB.TextBox TxtGarantia 
         Height          =   315
         Left            =   2145
         TabIndex        =   17
         Top             =   1635
         Width           =   3330
      End
      Begin VB.Frame FramF 
         Caption         =   "Fecha y Hora Inicio de Arriendo"
         Height          =   840
         Left            =   90
         TabIndex        =   107
         ToolTipText     =   "Esta opcion ignora la fecha de emision"
         Top             =   2655
         Width           =   6570
         Begin VB.CheckBox Chk 
            Height          =   195
            Left            =   150
            TabIndex        =   18
            Top             =   435
            Visible         =   0   'False
            Width           =   240
         End
         Begin MSComCtl2.DTPicker DtInicioC 
            CausesValidation=   0   'False
            Height          =   285
            Left            =   555
            TabIndex        =   19
            Top             =   360
            Width           =   3015
            _ExtentX        =   5318
            _ExtentY        =   503
            _Version        =   393216
            Format          =   95289344
            CurrentDate     =   42768
         End
         Begin MSComCtl2.DTPicker Hora_Inicio 
            CausesValidation=   0   'False
            Height          =   285
            Left            =   3690
            TabIndex        =   20
            Top             =   345
            Width           =   1080
            _ExtentX        =   1905
            _ExtentY        =   503
            _Version        =   393216
            Format          =   95289346
            CurrentDate     =   42768
         End
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkPlazoEntrega 
         Height          =   225
         Left            =   3735
         OleObjectBlob   =   "venPOS2.frx":07BB
         TabIndex        =   104
         Top             =   1275
         Visible         =   0   'False
         Width           =   1290
      End
      Begin VB.TextBox TxtPlazoEntrega 
         Height          =   315
         Left            =   5040
         TabIndex        =   16
         Text            =   "Plazo entrega: 10 dias, Pago Contado"
         Top             =   1230
         Visible         =   0   'False
         Width           =   915
      End
      Begin VB.TextBox TxtOC 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0C0C0&
         BorderStyle     =   0  'None
         Height          =   285
         Left            =   2145
         MaxLength       =   18
         TabIndex        =   15
         ToolTipText     =   "Ingrese el RUT sin puntos ni guion."
         Top             =   1245
         Width           =   1395
      End
      Begin VB.Frame Frame3 
         Height          =   735
         Left            =   165
         TabIndex        =   61
         Top             =   1920
         Width           =   5430
         Begin VB.TextBox txtTotal 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00C0FFC0&
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   24
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Left            =   1905
            Locked          =   -1  'True
            TabIndex        =   63
            TabStop         =   0   'False
            Text            =   "0"
            Top             =   135
            Width           =   3465
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel5 
            Height          =   480
            Left            =   60
            OleObjectBlob   =   "venPOS2.frx":0839
            TabIndex        =   62
            Top             =   165
            Width           =   2055
         End
      End
      Begin VB.TextBox TxtNeto 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFC0&
         BeginProperty Font 
            Name            =   "Arial Narrow"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00808080&
         Height          =   285
         Left            =   8790
         Locked          =   -1  'True
         TabIndex        =   59
         TabStop         =   0   'False
         Text            =   "0"
         Top             =   1110
         Visible         =   0   'False
         Width           =   1200
      End
      Begin VB.TextBox TxtIva 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFC0&
         BeginProperty Font 
            Name            =   "Arial Narrow"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00808080&
         Height          =   285
         Left            =   8805
         Locked          =   -1  'True
         TabIndex        =   57
         TabStop         =   0   'False
         Text            =   "0"
         Top             =   1485
         Visible         =   0   'False
         Width           =   1200
      End
      Begin VB.TextBox Text11 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFC0&
         BeginProperty Font 
            Name            =   "Arial Narrow"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   7035
         TabIndex        =   55
         ToolTipText     =   "Ingrese el RUT sin puntos ni guion."
         Top             =   2460
         Visible         =   0   'False
         Width           =   1200
      End
      Begin VB.TextBox TxtRecargoAjuste 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFC0&
         BorderStyle     =   0  'None
         Height          =   285
         Left            =   6855
         TabIndex        =   44
         Text            =   "0"
         Top             =   1485
         Visible         =   0   'False
         Width           =   615
      End
      Begin VB.TextBox TxtDsctoAjuste 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFC0&
         BorderStyle     =   0  'None
         Height          =   285
         Left            =   6855
         TabIndex        =   43
         Text            =   "0"
         Top             =   1065
         Visible         =   0   'False
         Width           =   615
      End
      Begin VB.ComboBox CboVendedor 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         ItemData        =   "venPOS2.frx":0899
         Left            =   2145
         List            =   "venPOS2.frx":089B
         Style           =   2  'Dropdown List
         TabIndex        =   14
         ToolTipText     =   "Seleccione Documento de  venta. Ventas con Retenci�n, cuando el contribuyente recibe factura de terceros "
         Top             =   825
         Width           =   3300
      End
      Begin VB.ComboBox CboDocInidicio 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         ItemData        =   "venPOS2.frx":089D
         Left            =   2130
         List            =   "venPOS2.frx":089F
         Style           =   2  'Dropdown List
         TabIndex        =   13
         ToolTipText     =   "Seleccione Documento de  venta. Ventas con Retenci�n, cuando el contribuyente recibe factura de terceros "
         Top             =   405
         Width           =   3285
      End
      Begin VB.ComboBox CboDocVenta 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         ItemData        =   "venPOS2.frx":08A1
         Left            =   3825
         List            =   "venPOS2.frx":08A3
         Style           =   2  'Dropdown List
         TabIndex        =   47
         ToolTipText     =   "Seleccione Documento de  venta. Ventas con Retenci�n, cuando el contribuyente recibe factura de terceros "
         Top             =   150
         Visible         =   0   'False
         Width           =   2370
      End
      Begin VB.TextBox TxtListaPrecio 
         BackColor       =   &H00E0E0E0&
         Height          =   285
         Left            =   8670
         Locked          =   -1  'True
         TabIndex        =   46
         TabStop         =   0   'False
         Top             =   90
         Visible         =   0   'False
         Width           =   2775
      End
      Begin VB.TextBox TxtSubTotal 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00C0C0C0&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   270
         Left            =   7770
         Locked          =   -1  'True
         TabIndex        =   45
         TabStop         =   0   'False
         Text            =   "0"
         Top             =   210
         Visible         =   0   'False
         Width           =   1530
      End
      Begin VB.TextBox TxtDescuentoX100 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFC0&
         BorderStyle     =   0  'None
         Height          =   285
         Left            =   6825
         TabIndex        =   42
         Text            =   "0"
         Top             =   585
         Visible         =   0   'False
         Width           =   615
      End
      Begin VB.TextBox TxtValorDescuento 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFC0&
         BorderStyle     =   0  'None
         Height          =   285
         Left            =   7770
         Locked          =   -1  'True
         TabIndex        =   41
         TabStop         =   0   'False
         Text            =   "0"
         Top             =   600
         Visible         =   0   'False
         Width           =   1530
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   300
         Index           =   0
         Left            =   45
         OleObjectBlob   =   "venPOS2.frx":08A5
         TabIndex        =   48
         Top             =   495
         Width           =   1650
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
         Height          =   330
         Left            =   75
         OleObjectBlob   =   "venPOS2.frx":091B
         TabIndex        =   49
         Top             =   885
         Width           =   1305
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel7 
         Height          =   330
         Left            =   45
         OleObjectBlob   =   "venPOS2.frx":0981
         TabIndex        =   50
         Top             =   1230
         Width           =   1785
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel8 
         Height          =   330
         Left            =   5985
         OleObjectBlob   =   "venPOS2.frx":09F5
         TabIndex        =   51
         Top             =   210
         Visible         =   0   'False
         Width           =   1710
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel9 
         Height          =   330
         Index           =   0
         Left            =   5895
         OleObjectBlob   =   "venPOS2.frx":0A5B
         TabIndex        =   52
         Top             =   615
         Visible         =   0   'False
         Width           =   915
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel9 
         Height          =   330
         Index           =   1
         Left            =   5805
         OleObjectBlob   =   "venPOS2.frx":0ABF
         TabIndex        =   53
         Top             =   1125
         Visible         =   0   'False
         Width           =   915
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel9 
         Height          =   330
         Index           =   2
         Left            =   5805
         OleObjectBlob   =   "venPOS2.frx":0B2D
         TabIndex        =   54
         Top             =   1545
         Visible         =   0   'False
         Width           =   915
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel9 
         Height          =   330
         Index           =   3
         Left            =   5910
         OleObjectBlob   =   "venPOS2.frx":0B91
         TabIndex        =   56
         Top             =   2460
         Visible         =   0   'False
         Width           =   915
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel9 
         Height          =   330
         Index           =   4
         Left            =   7665
         OleObjectBlob   =   "venPOS2.frx":0BF8
         TabIndex        =   58
         Top             =   1485
         Visible         =   0   'False
         Width           =   915
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel9 
         Height          =   330
         Index           =   5
         Left            =   7650
         OleObjectBlob   =   "venPOS2.frx":0C5A
         TabIndex        =   60
         Top             =   1110
         Visible         =   0   'False
         Width           =   915
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel11 
         Height          =   330
         Left            =   45
         OleObjectBlob   =   "venPOS2.frx":0CB8
         TabIndex        =   113
         Top             =   1650
         Width           =   2055
      End
   End
   Begin VB.Frame Frame7 
      Caption         =   "Cliente"
      Height          =   4140
      Left            =   60
      TabIndex        =   30
      Top             =   90
      Width           =   7425
      Begin VB.Frame FrmQuienEntrega 
         Caption         =   "Personal Dicoem - Quien Digita"
         Height          =   705
         Left            =   45
         TabIndex        =   118
         Top             =   3315
         Width           =   7275
         Begin VB.TextBox TxtRutEntrega 
            Height          =   300
            Left            =   5505
            TabIndex        =   12
            Top             =   255
            Width           =   1710
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel16 
            Height          =   195
            Left            =   4995
            OleObjectBlob   =   "venPOS2.frx":0D34
            TabIndex        =   120
            Top             =   315
            Width           =   360
         End
         Begin VB.TextBox TxtNombreEntrega 
            Height          =   300
            Left            =   1515
            TabIndex        =   11
            Top             =   285
            Width           =   2610
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel15 
            Height          =   255
            Left            =   150
            OleObjectBlob   =   "venPOS2.frx":0D98
            TabIndex        =   119
            Top             =   315
            Width           =   675
         End
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel14 
         Height          =   165
         Left            =   4200
         OleObjectBlob   =   "venPOS2.frx":0E02
         TabIndex        =   112
         Top             =   3000
         Width           =   1200
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel13 
         Height          =   225
         Left            =   45
         OleObjectBlob   =   "venPOS2.frx":0E80
         TabIndex        =   111
         Top             =   2970
         Width           =   1530
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel12 
         Height          =   255
         Left            =   135
         OleObjectBlob   =   "venPOS2.frx":0F04
         TabIndex        =   110
         Top             =   2610
         Width           =   945
      End
      Begin VB.TextBox TxtObservacion 
         Height          =   300
         Left            =   1530
         TabIndex        =   8
         Top             =   2565
         Width           =   5760
      End
      Begin VB.TextBox TxtObra 
         Height          =   315
         Left            =   1530
         TabIndex        =   7
         Top             =   2205
         Width           =   5760
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel10 
         Height          =   240
         Left            =   135
         OleObjectBlob   =   "venPOS2.frx":0F78
         TabIndex        =   108
         Top             =   2280
         Width           =   1080
      End
      Begin VB.ComboBox CboSucursal 
         Height          =   315
         Left            =   1545
         Style           =   2  'Dropdown List
         TabIndex        =   3
         Top             =   1170
         Width           =   5760
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkCupoCredito 
         Height          =   270
         Left            =   4890
         OleObjectBlob   =   "venPOS2.frx":0FF2
         TabIndex        =   84
         Top             =   1530
         Width           =   1020
      End
      Begin VB.TextBox TxtCupoUtilizado 
         Height          =   285
         Left            =   7365
         TabIndex        =   82
         Text            =   "Text1"
         Top             =   2370
         Visible         =   0   'False
         Width           =   2010
      End
      Begin VB.TextBox TxtMontoCredito 
         Height          =   255
         Left            =   7320
         TabIndex        =   81
         Text            =   "Text1"
         Top             =   2025
         Visible         =   0   'False
         Width           =   1995
      End
      Begin VB.TextBox TxtEmail 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0C0C0&
         BorderStyle     =   0  'None
         Height          =   285
         Left            =   5175
         TabIndex        =   6
         ToolTipText     =   "Ingrese el RUT sin puntos ni guion."
         Top             =   1815
         Width           =   2100
      End
      Begin VB.TextBox txtFono 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0C0C0&
         BorderStyle     =   0  'None
         Height          =   285
         Left            =   1545
         TabIndex        =   5
         ToolTipText     =   "Ingrese el RUT sin puntos ni guion."
         Top             =   1845
         Width           =   2175
      End
      Begin VB.TextBox txtComuna 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0C0C0&
         BorderStyle     =   0  'None
         Height          =   285
         Left            =   1560
         TabIndex        =   4
         ToolTipText     =   "Ingrese el RUT sin puntos ni guion."
         Top             =   1515
         Width           =   2160
      End
      Begin VB.TextBox TxtDireccion 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0C0C0&
         BorderStyle     =   0  'None
         Height          =   285
         Left            =   7785
         TabIndex        =   37
         ToolTipText     =   "Ingrese el RUT sin puntos ni guion."
         Top             =   1425
         Width           =   5670
      End
      Begin VB.TextBox TxtGiro 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0C0C0&
         BorderStyle     =   0  'None
         Height          =   285
         Left            =   1560
         TabIndex        =   2
         ToolTipText     =   "Ingrese el RUT sin puntos ni guion."
         Top             =   855
         Width           =   5715
      End
      Begin VB.TextBox TxtRut 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0C0C0&
         BorderStyle     =   0  'None
         Height          =   285
         Left            =   1560
         TabIndex        =   0
         ToolTipText     =   "Ingrese el RUT sin puntos ni guion."
         Top             =   195
         Width           =   1395
      End
      Begin VB.CommandButton CmdBuscaCliente 
         Caption         =   "F1 - Buscar"
         Height          =   255
         Left            =   2985
         TabIndex        =   31
         Top             =   195
         Width           =   1395
      End
      Begin VB.TextBox TxtRazonSocial 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0C0C0&
         BorderStyle     =   0  'None
         Height          =   285
         Left            =   1560
         TabIndex        =   1
         ToolTipText     =   "Ingrese el RUT sin puntos ni guion."
         Top             =   525
         Width           =   5730
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   240
         Index           =   0
         Left            =   60
         OleObjectBlob   =   "venPOS2.frx":1052
         TabIndex        =   32
         Top             =   225
         Width           =   645
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   210
         Index           =   2
         Left            =   45
         OleObjectBlob   =   "venPOS2.frx":10BC
         TabIndex        =   33
         Top             =   570
         Width           =   585
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   255
         Index           =   1
         Left            =   45
         OleObjectBlob   =   "venPOS2.frx":1126
         TabIndex        =   34
         Top             =   1200
         Width           =   735
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel19 
         Height          =   255
         Index           =   0
         Left            =   45
         OleObjectBlob   =   "venPOS2.frx":1196
         TabIndex        =   35
         Top             =   1545
         Width           =   645
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel18 
         Height          =   255
         Left            =   75
         OleObjectBlob   =   "venPOS2.frx":1200
         TabIndex        =   36
         Top             =   885
         Width           =   300
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel19 
         Height          =   255
         Index           =   1
         Left            =   60
         OleObjectBlob   =   "venPOS2.frx":1266
         TabIndex        =   38
         Top             =   1875
         Width           =   420
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel19 
         Height          =   255
         Index           =   2
         Left            =   3795
         OleObjectBlob   =   "venPOS2.frx":12CC
         TabIndex        =   39
         Top             =   1890
         Width           =   1320
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel19 
         Height          =   255
         Index           =   3
         Left            =   3810
         OleObjectBlob   =   "venPOS2.frx":134E
         TabIndex        =   83
         Top             =   1530
         Width           =   1035
      End
   End
   Begin MSComDlg.CommonDialog Dialogo 
      Left            =   17865
      Top             =   7080
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.TextBox TxtEmpMail 
      Height          =   405
      Left            =   18375
      TabIndex        =   99
      Text            =   "Text3"
      Top             =   8115
      Width           =   3300
   End
   Begin VB.TextBox TxtEmpFono 
      Height          =   285
      Left            =   18270
      TabIndex        =   98
      Text            =   "Text2"
      Top             =   7470
      Width           =   3255
   End
   Begin VB.TextBox TxtEmpDireccion 
      Height          =   315
      Left            =   18330
      TabIndex        =   97
      Text            =   "Text1"
      Top             =   6885
      Width           =   3510
   End
   Begin VB.TextBox TxtBarCode 
      Height          =   285
      Left            =   9345
      TabIndex        =   96
      Text            =   "Text1"
      Top             =   10200
      Visible         =   0   'False
      Width           =   1080
   End
   Begin VB.TextBox TxtUbicacion 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C0C0C0&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   18
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   450
      Left            =   6330
      TabIndex        =   86
      Top             =   10575
      Width           =   2805
   End
   Begin VB.TextBox TxtStock 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C0C0C0&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   18
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   450
      Left            =   2520
      TabIndex        =   85
      Top             =   4515
      Visible         =   0   'False
      Width           =   2760
   End
   Begin VB.TextBox TxtPOS 
      BackColor       =   &H8000000F&
      Height          =   285
      Left            =   18240
      Locked          =   -1  'True
      TabIndex        =   29
      TabStop         =   0   'False
      Text            =   "POS"
      Top             =   -30
      Width           =   1275
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
      Height          =   285
      Index           =   1
      Left            =   18315
      OleObjectBlob   =   "venPOS2.frx":13C6
      TabIndex        =   28
      Top             =   165
      Width           =   1650
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   75
      OleObjectBlob   =   "venPOS2.frx":142A
      Top             =   735
   End
   Begin MSComctlLib.ListView LVFpago 
      Height          =   3450
      Left            =   18750
      TabIndex        =   79
      Top             =   465
      Width           =   2940
      _ExtentX        =   5186
      _ExtentY        =   6085
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483646
      BackColor       =   -2147483643
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   3
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Object.Tag             =   "N109"
         Text            =   "Id Interno"
         Object.Width           =   1764
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Object.Tag             =   "N109"
         Text            =   "valor"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Object.Tag             =   "N109"
         Text            =   "id IF"
         Object.Width           =   2540
      EndProperty
   End
   Begin MSComctlLib.ListView LvCheques 
      Height          =   1260
      Left            =   18225
      TabIndex        =   80
      Top             =   5025
      Width           =   8205
      _ExtentX        =   14473
      _ExtentY        =   2223
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   12648384
      BorderStyle     =   1
      Appearance      =   0
      NumItems        =   9
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Object.Tag             =   "N100"
         Text            =   "id doc"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Object.Tag             =   "T1000"
         Text            =   "RUT"
         Object.Width           =   2117
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Object.Tag             =   "N109"
         Text            =   "Numero"
         Object.Width           =   1764
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Object.Tag             =   "T1500"
         Text            =   "Banco"
         Object.Width           =   2646
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Object.Tag             =   "T1000"
         Text            =   "Plaza"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   5
         Object.Tag             =   "F1000"
         Text            =   "Fecha"
         Object.Width           =   2196
      EndProperty
      BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   6
         Object.Tag             =   "N100"
         Text            =   "Monto"
         Object.Width           =   2117
      EndProperty
      BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   7
         Object.Tag             =   "T1000"
         Text            =   "Observacion"
         Object.Width           =   3528
      EndProperty
      BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   8
         Object.Tag             =   "N109"
         Text            =   "banco_id"
         Object.Width           =   2540
      EndProperty
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
      Height          =   225
      Index           =   5
      Left            =   1875
      OleObjectBlob   =   "venPOS2.frx":165E
      TabIndex        =   87
      Top             =   4740
      Width           =   660
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
      Height          =   225
      Index           =   6
      Left            =   5865
      OleObjectBlob   =   "venPOS2.frx":16C6
      TabIndex        =   88
      Top             =   4605
      Width           =   1035
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel6 
      Height          =   330
      Left            =   7830
      OleObjectBlob   =   "venPOS2.frx":1736
      TabIndex        =   109
      Top             =   1710
      Width           =   1710
   End
   Begin VB.Menu mn_edicion 
      Caption         =   "Edicion"
      Begin VB.Menu mn_editacontrato 
         Caption         =   "Editar contrato"
      End
   End
End
Attribute VB_Name = "venPOS"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim Sm_TipoPos As String
Dim Sm_PermiteDscto As String
Dim Sm_PrecioVtaModificable As String
Dim Sm_VentaRapida As String
Dim Dp_Descuento As Double
Dim Im_Redondear As Integer
Dim Lm_CuentaVentas As Long
Dim Sm_UtilizaCodigoInterno As String * 2
Dim Im_Descuento_Maximo As Double
Dim Im_Recargo_Maximo As Double
Dim Sm_Codigo_Barra_Pesable As String * 2
Dim Lp_Id_Unico_Venta As Long 'id del documento
Dim Lp_Id_Nueva_Venta As Long 'Nro documento
Dim Bm_BoletaEnProceso As Boolean
Dim Sm_ImprimeTicketNv As String * 2
Dim Sp_EmpresaRepuestos As String * 2
Dim Sp_MatrizDescuentos As String * 2
Dim sue_id As Integer

Private Function Redondear(Valor As Double, Redo As Integer) As Long
    If Redo = 100 Then 'Redondeamos a 100
    
        If Val(Right(Str(Valor), 2)) > 0 Then
            If Val(Right(Str(Valor), 2)) >= 50 Then
                Valor = Valor + (100 - Val(Right(Str(Valor), 2)))
            Else
                Valor = Valor - Val(Right(Str(Valor), 2))
            End If
        End If
        
    End If
    Redondear = Valor

End Function
Private Sub CboDocInidicio_Click()
    Sql = "SELECT doc_requiere_rut,doc_cantidad_lineas " & _
            "FROM sis_documentos " & _
            "WHERE doc_id=" & Me.CboDocInidicio.ItemData(Me.CboDocInidicio.ListIndex)
    Consulta RsTmp2, Sql
    If RsTmp2.RecordCount > 0 Then
        'SkLimiteLineas.Tag = RsTmp2!doc_cantidad_lineas
        CboDocInidicio.Tag = RsTmp2!doc_requiere_rut
    End If
    If CboDocInidicio.Text = "COTIZACION" Then
        If SP_Rut_Activo = "78.967.170-2" Then
            Me.SkPlazoEntrega.Visible = True
            Me.TxtPlazoEntrega.Visible = True
        End If
    Else
            Me.SkPlazoEntrega.Visible = False
            Me.TxtPlazoEntrega.Visible = False
    End If
End Sub

Private Sub CboDocInidicio_KeyPress(KeyAscii As Integer)
    On Error Resume Next
    If KeyAscii = 13 Then SendKeys ("{TAB}")
    If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub CboSucursal_Click()
    Dim Ip_IdSuc As Integer
    If CboSucursal.ListIndex = -1 Then Exit Sub
    
    Ip_IdSuc = CboSucursal.ItemData(CboSucursal.ListIndex)
    If Ip_IdSuc = 0 Then
        'Busca_Id_Combo CboGiros, 0
        Exit Sub
    End If
    Sql = "SELECT suc_direccion,suc_ciudad,suc_contacto suc_comuna,gir_id " & _
          "FROM par_sucursales " & _
          "WHERE suc_id=" & Ip_IdSuc
    Consulta RsTmp2, Sql
    If RsTmp2.RecordCount > 0 Then
        TxtDireccion = RsTmp2!suc_direccion
        TxtCiudad = RsTmp2!suc_ciudad
        txtComuna = RsTmp2!suc_comuna
    End If
End Sub

Private Sub CboSucursal_KeyPress(KeyAscii As Integer)
    On Error Resume Next
    If KeyAscii = 13 Then SendKeys ("{TAB}")
    If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub CboVendedor_KeyPress(KeyAscii As Integer)
    On Error Resume Next
    If KeyAscii = 13 Then SendKeys ("{TAB}")
    If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub ChkAnexo_Click()
If ChkAnexo = False Then
FrmAccesorios.Visible = False
Else
FrmAccesorios.Visible = True
    
End If

End Sub

Private Sub ChkAnexo_KeyPress(KeyAscii As Integer)
    On Error Resume Next
    If KeyAscii = 13 Then SendKeys ("{TAB}")
    If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub CmbListaPrecios_Change()
Dim pppp As Integer
pppp = CmbListaPrecios.ListIndex

End Sub
Private Sub CmbListaPrecios_Click()
ModificaLista = CmbListaPrecios.ListIndex
End Sub
Private Sub CmbListaPrecios_KeyPress(KeyAscii As Integer)
    On Error Resume Next
    If KeyAscii = 13 Then SendKeys ("{TAB}")
    If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub CmbListaPrecios_Validate(Cancel As Boolean)
Dim pppp As Integer
pppp = CmbListaPrecios.ListIndex
End Sub
Private Sub CmdAccesorios_Click()
    If Len(TxtAccesorio) = 0 Then
        TxtAccesorio.SetFocus
        Exit Sub
    End If
    
    LvAccesorios.ListItems.Add , , TxtAccesorio.Tag
    p = LvAccesorios.ListItems.Count
    LvAccesorios.ListItems(p).SubItems(1) = TxtAccesorio
    TxtAccesorio = ""
    TxtAccesorio.SetFocus
End Sub
Private Sub CmdAceptaLinea_Click()
    Dim p As Integer
    If Len(TxtCodigo) = 0 Then
        MsgBox "Ingrese c�digo...", vbInformation
        On Error Resume Next
        TxtCodigo.SetFocus
        Exit Sub
    End If
    If Len(TxtDescripcion) = 0 Then
        MsgBox "Faltan datos para agregar linea...", vbInformation
        TxtCodigo.SetFocus
        Exit Sub
    End If
    TxtCantidad = 1
    
    
    '04-10-2016' verificar que la empresa venda con o sin stock
    If SG_VenderSinStock = "NO" Then
    
        If Val(TxtStockLinea) < 1 Then
            MsgBox "Producto sin stock...", vbCritical
            Exit Sub
        End If
    
    End If
    
    ''Valida que el producto no este arrendado
     Sql = "SELECT pro_estado  " & _
                "FROM maestro_productos " & _
                "WHERE pro_codigo_interno='" & Me.TxtCodigo & "'"
    Consulta RsTmp2, Sql
    If RsTmp2.RecordCount > 0 Then
        If RsTmp2!pro_estado <> "DISPONIBLE" Then
              MsgBox "Producto No Disponible Para ser Arrendado... Se encuentra en el siguiente estado : " & RsTmp2!pro_estado & "", vbInformation
              'TxtCantidad.SetFocus
              Exit Sub
        End If
    End If
    LvVenta.ListItems.Add , , TxtCodigo.Tag
    p = LvVenta.ListItems.Count
    LvVenta.ListItems(p).SubItems(1) = TxtCodigo
    LvVenta.ListItems(p).SubItems(2) = TxtDescripcion
    LvVenta.ListItems(p).SubItems(3) = TxtCantidad
    LvVenta.ListItems(p).SubItems(4) = CDbl(TxtPrecio)
    LvVenta.ListItems(p).SubItems(5) = Round(CDbl(TxtPrecio) * Val(CxP(TxtCantidad)), 0)
    LvVenta.ListItems(p).SubItems(6) = TxtStockLinea
    LvVenta.ListItems(p).SubItems(7) = SkInventariable
    LvVenta.ListItems(p).SubItems(8) = Val(TxtStockLinea.Tag)
    
    LvVenta.ColumnHeaders(4).Width = 0
    LvVenta.ColumnHeaders(6).Width = 0
    LvVenta.ColumnHeaders(7).Width = 0
    
    
    If Sp_MatrizDescuentos = "SI" Then
        
        LvVenta.ListItems(p).SubItems(9) = TxtCantidad.Tag
    
    End If
    
    
    If SP_Rut_Activo = "78.967.170-2" Then
        LvVenta.ListItems(p).SubItems(10) = "UN"
        Sql = "SELECT ume_nombre " & _
                "FROM sis_unidad_medida " & _
                "WHERE ume_id=" & Val(SkUm)
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
            LvVenta.ListItems(p).SubItems(10) = RsTmp!ume_nombre
        End If
    
        
        
    End If
    
    LimpiaTxt
    CalculaGrilla
    
    LvVenta.ListItems(LvVenta.ListItems.Count).Selected = True
    LvVenta.ListItems(LvVenta.ListItems.Count).EnsureVisible
    
    TxtCodigo.SetFocus
    
End Sub

Private Sub CalculaGrilla()
    Dim Dp_DctoX100 As Double
    Dim Lp_DsctoAjuste As Long
    Dim Lp_RecargoAjuste As Long
    If LvVenta.ListItems.Count > 0 Then
     '   If Val(TxtDescuentoX100) > 0 Then
            Dp_DctoX100 = TxtDescuentoX100
             Lp_DsctoAjuste = TxtDsctoAjuste
            Lp_RecargoAjuste = TxtRecargoAjuste
            
            TotalesEn0 'en 0 todos los totales
            TxtSubTotal = 0
            
            For i = 1 To LvVenta.ListItems.Count
                TxtSubTotal = Val(TxtSubTotal) + CDbl(LvVenta.ListItems(i).SubItems(5))
            Next
            
            TxtSubTotal = Replace(NumFormat(TxtSubTotal), ",", ".")
            txtTotal = TxtSubTotal
                     
            If Dp_DctoX100 > 0 Then
                TxtDescuentoX100 = Dp_DctoX100
                
                If Sp_MatrizDescuentos = "SI" Then
                    CalculaDescuentox100Matriz
                Else
                    CalculaDescuentox100
                End If
            End If

            TxtDsctoAjuste = Lp_DsctoAjuste
            TxtRecargoAjuste = Lp_RecargoAjuste
            If Val(Me.TxtDsctoAjuste) > 0 Then 'Ajuste de descuento
                txtTotal = NumFormat(CDbl(txtTotal) - CDbl(TxtDsctoAjuste))
            
            End If
            If Val(Me.TxtRecargoAjuste) > 0 Then
                txtTotal = NumFormat(CDbl(txtTotal) + (TxtRecargoAjuste))
            End If
   
    Else
        TotalesEn0
    End If
    txtTotal = Replace(txtTotal, ",", ".")
    
    
    'Ver codigo repetidos y cambiarles color
    NormalizaColores
End Sub
Private Sub NormalizaColores()
    Exit Sub
    LvVenta.Refresh
    For X = 1 To LvVenta.ListItems.Count
        'Normalizar antes
        For Z = 1 To LvVenta.ColumnHeaders.Count - 4
            LvVenta.ListItems(X).ListSubItems(Z).ForeColor = vbBlack
            LvVenta.ListItems(X).ListSubItems(Z).Bold = False
        Next
    Next
    
    LvVenta.Refresh
        
    For i = 1 To LvVenta.ListItems.Count
        
        For X = 1 To LvVenta.ListItems.Count
            If X <> i Then
                If LvVenta.ListItems(X).SubItems(1) = LvVenta.ListItems(i).SubItems(1) Then
                    'codigo igual
                    'pintar
                    For Z = 1 To LvVenta.ColumnHeaders.Count - 5
                        LvVenta.ListItems(X).ListSubItems(Z).ForeColor = vbRed
                        LvVenta.ListItems(X).ListSubItems(Z).Bold = True
                    Next
                End If
            End If
        Next
    
    Next
    LvVenta.Refresh
End Sub
Private Sub CalculaDescuentox100()
    If CDbl(TxtSubTotal) > 0 Then
        TxtValorDescuento = NumFormat(CDbl(TxtSubTotal) / 100 * Val(TxtDescuentoX100))
        txtTotal = NumFormat(CDbl(TxtSubTotal) - CDbl(TxtValorDescuento))
    End If
End Sub
Private Sub CalculaDescuentox100Matriz()
    Dim Lp_SubTotalVolumen As Long
    Lp_SubTotalVolumen = 0
    For i = 1 To LvVenta.ListItems.Count
        If LvVenta.ListItems(i).SubItems(9) = "NO" Then
                Lp_SubTotalVolumen = Val(Lp_SubTotalVolumen) + CDbl(LvVenta.ListItems(i).SubItems(5))
        End If
    Next

    If CDbl(Lp_SubTotalVolumen) > 0 Then
        TxtValorDescuento = NumFormat(CDbl(Lp_SubTotalVolumen) / 100 * Val(TxtDescuentoX100))
        txtTotal = NumFormat(CDbl(TxtSubTotal) - CDbl(TxtValorDescuento))
    End If
    
End Sub
Private Sub TotalesEn0()
    TxtSubTotal = 0
    Me.TxtDescuentoX100 = 0
    Me.TxtDsctoAjuste = 0
    Me.TxtRecargoAjuste = 0
    TxtNeto = 0
    txtIVA = 0
    txtTotal = 0
End Sub
Private Sub LimpiaTxt()
    TxtCodigo.Tag = ""
    TxtCodigo = ""
    TxtDescripcion = ""
    TxtCantidad = "1"
    TxtPrecio = "0"
    SkInventariable = "SI"
    TxtStockLinea = "0"
    TxtTotalLinea = "0"
End Sub

Private Sub CmdBuscaCliente_Click()
    LlamaClienteDe = "VD"
    ClienteEncontrado = False
    BuscaCliente.Show 1
    TxtRut = SG_codigo
    ''TxtRut.Text = Replace(TxtRut.Text, ".", "")
    ''TxtRut.Text = Replace(TxtRut.Text, "-", "")
    TxtRut.SetFocus
End Sub
Private Sub CmdCargarCotizacion_Click()
    sis_InputBox.Sm_TipoDato = "N"
    sis_InputBox.Caption = "CARGAR COTIZACION"
    sis_InputBox.texto.PasswordChar = ""
    sis_InputBox.texto.Tag = "N"
    sis_InputBox.FramBox = "Nro de Cotizacion"
    sis_InputBox.Show 1
    Sp_Llave = UCase(SG_codigo2)
    If Len(Sp_Llave) = 0 Then Exit Sub
    
   CargaCotizacion Val(Sp_Llave)
    
End Sub
Private Sub CargaCotizacion(Nro As Long)
 
    Sql = "SELECT * " & _
            "FROM ven_nota_venta " & _
            "WHERE nve_cotizacion='SI' AND nve_terminada='NO' AND  nve_id=" & Val(Nro)
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        
        Busca_Id_Combo CboDocInidicio, Val(RsTmp!doc_id)
        Busca_Id_Combo CboVendedor, Val(RsTmp!ven_id)
        TxtSubTotal = NumFormat(RsTmp!nve_total + RsTmp!nve_valor_descuento + RsTmp!nve_valor_descuento_adicional - RsTmp!nve_valor_recargo)
        txtTotal = NumFormat(RsTmp!nve_total)
        Me.TxtDescuentoX100 = RsTmp!nve_descuentoxcien
        TxtValorDescuento = RsTmp!nve_valor_descuento
        TxtDsctoAjuste = RsTmp!nve_valor_descuento_adicional
        TxtRecargoAjuste = RsTmp!nve_valor_recargo
        TxtNeto = RsTmp!nve_neto
        txtIVA = RsTmp!nve_iva
        TxtRut = RsTmp!rut_cliente
        Sql = "SELECT codigo cod1,pro_codigo_interno cod2,nvd_descripcion,nvd_cantidad,nvd_precio_unitario,nvd_precio_total,0,nvd_inventariable,round(nvd_precio_costo/nvd_Cantidad,0) " & _
                "FROM ven_nota_venta_detalle " & _
                "WHERE nve_id=" & Val(Nro)
        Consulta RsTmp, Sql
        LLenar_Grilla RsTmp, Me, LvVenta, False, True, True, False
        
        
        txtRut_Validate True
    
    Else
        MsgBox "Cotizacion no encontrada...", vbExclamation
        
    
    End If
    
End Sub
Private Sub CmdF5_Click()
    Dim Sp_ElRut As String
    Dim Ip_Id_Doc As Integer
    Dim Sp_Nombre_doc As String
    Dim Lp_IdAbo As Long
    Dim Lp_Nro_Comprobante As Long
    Dim Ip_Id_Suc As Integer
    Dim Sp_ProductosSS As String
    If Val(Me.Tag) > 0 Then
        ''Modificar
        If MsgBox("Seguro de modificar contrato:" & vbNewLine & Me.SkNnumcontrato, vbQuestion + vbOKCancel, "Confirmar") = vbCancel Then Exit Sub
    
    End If
    Ip_Id_Suc = 0
    TxtNroDocumento = Lp_Id_Unico_Venta
    If Me.LvVenta.ListItems.Count >= 5 Then
                MsgBox "Maximo 4 Maquinas por Contrato...", vbExclamation
              Exit Sub
    End If
    If Me.TxtObra = "" Then
                MsgBox "Debe Ingresar Direccion de la Obra...", vbExclamation
                TxtObra.SetFocus
              Exit Sub
    End If
    If Me.TxtObservacion = "" Then
                MsgBox "Debe Ingresar Observacion...", vbExclamation
                TxtObservacion.SetFocus
              Exit Sub
    End If
    If Me.TxtNombreFirma = "" Then
                MsgBox "Debe Ingresar Nombre Firmante...", vbExclamation
                TxtNombreFirma.SetFocus
              Exit Sub
    End If
      If Me.TxtRutFirma = "" Then
                MsgBox "Debe Ingresar Rut Firmante...", vbExclamation
                TxtRutFirma.SetFocus
              Exit Sub
    End If
    If Me.TxtGarantia = "" Then
                MsgBox "Debe Ingresar Comentario Garantia...", vbExclamation
                TxtGarantia.SetFocus
              Exit Sub
    End If
  
    If Me.TxtNombreEntrega = "" Then
              MsgBox "Debe Ingresar Nombre Quien Entrega...", vbExclamation
              TxtNombreEntrega.SetFocus
            Exit Sub
  End If
      If Me.TxtRutEntrega = "" Then
              MsgBox "Debe Ingresar Rut Quien Entrega...", vbExclamation
              TxtRutEntrega.SetFocus
            Exit Sub
  End If
    
    CmdF5.Enabled = False
    
    If LvVenta.ListItems.Count = 0 Then
        CmdF5.Enabled = True
        Exit Sub
    End If
    
    
    
    If Val(SkNnumcontrato) = 0 Then
    
        MsgBox "No hay folios para contrato, debe solicitar...", vbInformation
        Exit Sub
    End If
    FrmLoad.Visible = True
    DoEvents
    'Sknnumcontrato 'verificar el nro de contrato
    If Val(Me.Tag) = 0 Then
             Sql = "SELECT arr_id " & _
                     "FROM ven_arriendo " & _
                     "WHERE sue_id=" & LogSueID & " AND arr_id=" & SkNnumcontrato
             Consulta RsTmp, Sql
             If RsTmp.RecordCount > 0 Then
             
                             Sql = "SELECT contrato_id,contrato_numero numero " & _
                                 "FROM dte_contrato " & _
                                 "WHERE contrato_sucursal='" & LogSueID & "' AND contrato_disponible='SI' " & _
                                 "ORDER BY contrato_id " & _
                                 "LIMIT 1"
                             Consulta RsTmp, Sql
                             TxtNroDocumento = 0
                             If RsTmp.RecordCount > 0 Then
                                 FrmLoad.Visible = False
                                 MsgBox "El sistema detect� que el Nro de Contrato: " & SkNnumcontrato & " ya ha sido utilizado.." & vbNewLine & _
                                 "Se asignar� el siguiente Nro:" & RsTmp!Numero, vbInformation + vbOKOnly
                             
                                 TxtNroDocumento = RsTmp!Numero
                                 Me.SkNnumcontrato = RsTmp!Numero
                                 Lp_Id_Unico_Venta = RsTmp!Numero
                             Else
                                FrmLoad.Visible = False
                              ''   Me.SkFoliosDisponibles = "No quedan folios disponibles.."
                                 MsgBox "No hay Nros para contrato disponibles..."
                                 
                                 Exit Sub
                             
                             End If
            ' Else
             '    MsgBox "No quedan folios de contrato asigandos... Solicitar con administracion...", vbInformation
              '   Exit Sub
             End If
    End If
    
    
                        
                   
    If Val(TxtDescuentoX100) = 0 Then
        TxtDescuentoX100 = "0"
        TxtValorDescuento = "0"
    End If
    If Val(TxtDsctoAjuste) = 0 Then TxtDsctoAjuste = "0"
    CalculaGrilla
                        
    

            'POS SOLO EMITE NOTA DE VENTA
    If Me.CboDocInidicio.ListIndex = -1 Then
         FrmLoad.Visible = False
        MsgBox "Falta documento final...", vbOKOnly + vbInformation
         CmdF5.Enabled = True
        Exit Sub
    End If
    

    
    
    If CboDocInidicio.Tag = "SI" Then
        If Len(TxtRut) = 0 Then
            FrmLoad.Visible = False
            MsgBox "Requiere ingresar cliente...", vbInformation
            TxtRut.SetFocus
            CmdF5.Enabled = True
            Exit Sub
        End If
    End If
    
    'verificar descuentos/recargos maximos.
    If Not PermiteAjuste Then
         FrmLoad.Visible = False
        MsgBox "Ajustes fuera de rango permitido...", vbInformation
        On Error Resume Next
        Me.TxtDescuentoX100.SetFocus
        CmdF5.Enabled = True
        Exit Sub
    End If
    
    If Me.CboSucursal.ListIndex > -1 Then
        Ip_Id_Suc = CboSucursal.ItemData(CboSucursal.ListIndex)
    End If
    
    
    
    
    
    'Hasta aqui se podria grabar la nota de venta.
    '22 Agosto


    'Lp_Id_Nueva_Venta = UltimoNro("ven_doc_venta", "no_documento")
    'Lp_Id_Nueva_Venta = AutoIncremento("LEE", , 100, 1)
    
    
    If Len(TxtRut) = 0 Then Sp_ElRut = "11.111.111-1" Else Sp_ElRut = TxtRut
    
    
    elbruto = CDbl(txtTotal)
    elneto = Round(CDbl(txtTotal) / Val("1." & DG_IVA), 0)
    

    Ip_Id_Doc = CboDocVenta.ItemData(CboDocVenta.ListIndex)
    Sp_Nombre_doc = CboDocVenta.Text

    
    

        
        'Solo nota de venta
        '2-9 2015, utilizaremos tablas para notas de ventas.
        'ven_nota_venta
        'ven_nota_venta_detalle
        If Len(TxtRut) = 0 Then TxtRut = "11.111.111-1"
        Lp_Id_Unico_Venta = TxtNroDocumento

              

         
    sql2 = ""
        On Error GoTo GrabandoContrato
        
        cn.BeginTrans
        
        If Val(Me.Tag) > 0 Then
            '28 Enero 2021
            
            cn.Execute "DELETE FROM ven_arriendo WHERE arr_id=" & TxtNroDocumento
            cn.Execute "DELETE FROM ven_arr_detalle WHERE arr_id=" & TxtNroDocumento
            cn.Execute "DELETE FROM ven_arriendo_anexos WHERE arr_id=" & TxtNroDocumento
            
            
            
            
        End If
    
    ''Graba el Contrato
    
    Sql = "INSERT INTO ven_arriendo (arr_id,arr_rut_cliente,arr_nombre_cliente,arr_direccion_cliente,arr_comuna_cliente,arr_giro_cliente,arr_fono_cliente,arr_direccion_obra," & _
                                   "arr_garantia,arr_observacion,arr_valor,arr_fecha_contrato,arr_fecha_inicio,arr_fecha_termino,arr_hora_inicio,arr_estado," & _
                                   "arr_oc,arr_nombre_retira,arr_rut_retira,arr_nombre_digita,arr_rut_digita,sue_id) " & _
       "VALUES(" & Lp_Id_Unico_Venta & ",'" & TxtRut & "','" & TxtRazonSocial & "','" & TxtDireccion & "','" & txtComuna & "','" & TxtGiro & "','" & txtFono & "','" & TxtObra & "','" & _
         TxtGarantia & "','" & TxtObservacion & "'," & Replace(Me.txtTotal, ".", "") & ",'" & Fql(Date) & "','" & Fql(Me.DtInicioC) & "','" & Fql(Date) & "','" & Me.Hora_Inicio & "','En Arriendo','" & _
         Me.TxtOC & "','" & Me.TxtNombreFirma & "','" & Me.TxtRutFirma & "','" & Me.TxtNombreEntrega & "','" & Me.TxtRutEntrega & "','" & LogSueID & "')"
    Dim xxx As String
    xxx = Me.Hora_Inicio
    Dim xxxx As String
    xxxx = Fql(Me.DtInicioC)
 
    cn.Execute Sql
    
    With venPOS.LvVenta
    Dim Idx As String
    Idx = Lp_Id_Unico_Venta
        
    For i = 1 To LvVenta.ListItems.Count
            Sql = "INSERT INTO ven_arr_detalle (arr_id,det_arr_codigo,det_arr_codigo_interno,det_arr_descripcion,det_arr_total,sue_id) " & _
            "VALUES(" & Lp_Id_Unico_Venta & ",'" & Me.LvVenta.ListItems(i) & "','" & Me.LvVenta.ListItems(i).SubItems(1) & "','" & Me.LvVenta.ListItems(i).SubItems(2) & "','" & CDbl(LvVenta.ListItems(i).SubItems(5)) & "','" & LogSueID & "')"
            cn.Execute Sql
            Idx = Idx + 1
            
                ''Actualiza el estado del producto en el maestro del producto
        
            Sql = "UPDATE maestro_productos " & _
              "SET pro_estado = 'En Arriendo' " & _
              " WHERE  rut_emp='" & SP_Rut_Activo & "' AND  codigo='" & Me.LvVenta.ListItems(i) & "'"
        
           cn.Execute Sql
    
        ''Actualiza el estado del numero de contrato ..lo deja como no disponible
           Dim suc1 As String
           Sql = ""
            Sql = "SELECT sue_id FROM sis_usuarios WHERE usu_login='" & LogUsuario & "'"
            Consulta RsTmp, Sql
            If RsTmp.RecordCount > 0 Then
                 suc1 = RsTmp!sue_id
            End If
            Sql = ""
            Sql = "UPDATE dte_contrato " & _
              "SET contrato_disponible = 'NO' " & _
              " WHERE  contrato_sucursal='" & suc1 & "' AND  contrato_numero='" & Lp_Id_Unico_Venta & "'"
        
            cn.Execute Sql
        
    Next
        
    End With
    
    '13 Noviembre 2018
    'Accesorios del conrato, guardar
    If LvAccesorios.ListItems.Count > 0 Then
        Sql = "INSERT INTO ven_arriendo_anexos (arr_id,anx_accesorio) VALUES"
        For i = 1 To LvAccesorios.ListItems.Count
            Sql = Sql & "(" & Lp_Id_Unico_Venta & ",'" & LvAccesorios.ListItems(i) & "'),"
        Next
        Sql = Mid(Sql, 1, Len(Sql) - 1)
        cn.Execute Sql
    End If
    
    cn.CommitTrans
    
    
    MsgBox "Contrato Creado Nro:" & vbNewLine & Lp_Id_Unico_Venta, vbInformation + vbOKOnly
    GoTo CrearWord
GrabandoContrato:
    FrmLoad.Visible = False
    cn.RollbackTrans
    MsgBox "Ocurrio un error al intentar grabar contrato..." & vbNewLine & Err.Description
    CmdF5.Enabled = True
    Exit Sub
CrearWord:
    ''Fin Graba Contrato
    If Me.ChkAnexo = 0 Then
        contrato
    Else
        contratoAnexo
    End If
    
    
final:

    
CancelaImpesionNV:
    CmdF5.Enabled = True
    LimpiaTodo
    Me.Tag = ""
End Sub
Private Sub EmiteBoletaEpson()
        Bm_BoletaEnProceso = True
        CmdF5.Enabled = False
        Frame1.Enabled = False
        Me.Enabled = False
        With vtaBoletaFiscalEpson.LvDetalle
            .ListItems.Clear
            For d = 1 To Me.LvVenta.ListItems.Count
                .ListItems.Add , , LvVenta.ListItems(d).SubItems(3)
                .ListItems(d).SubItems(1) = LvVenta.ListItems(d).SubItems(2)
                .ListItems(d).SubItems(2) = CDbl(LvVenta.ListItems(d).SubItems(4))
                .ListItems(d).SubItems(3) = CDbl(LvVenta.ListItems(d).SubItems(1))


            Next
            vtaBoletaFiscalEpson.LvPagos.ListItems.Clear
            For i = 1 To venPOS.LVFpago.ListItems.Count
                 vtaBoletaFiscalEpson.LvPagos.ListItems.Add , , venPOS.LVFpago.ListItems(i).SubItems(2)
                 vtaBoletaFiscalEpson.LvPagos.ListItems(vtaBoletaFiscalEpson.LvPagos.ListItems.Count).SubItems(2) = CDbl(venPOS.LVFpago.ListItems(i).SubItems(1))
            Next

            vtaBoletaFiscalEpson.txtTotal = txtTotal
            vtaBoletaFiscalEpson.TxtEfectivo = CDbl(venPOS.TxtSumaPagos)
            vtaBoletaFiscalEpson.EmiteBoleta


        End With

        TxtMontoPago = CDbl(venPOS.TxtSumaPagos)

'        With Epson.LvDetalle
'                .ListItems.Clear
'                For d = 1 To Me.LvVenta.ListItems.Count
'                    .ListItems.Add , , LvVenta.ListItems(d).SubItems(3)
'                    .ListItems(d).SubItems(1) = LvVenta.ListItems(d).SubItems(2)
'                    .ListItems(d).SubItems(2) = CDbl(LvVenta.ListItems(d).SubItems(4))
'                    .ListItems(d).SubItems(3) = CDbl(LvVenta.ListItems(d).SubItems(1))
'
'
'                Next
'
'        End With
'        Epson.HaceteLaBoleta
    


        Bm_BoletaEnProceso = False
        CmdF5.Enabled = True
        Frame1.Enabled = True
        Me.Enabled = True
        
    
End Sub
Private Sub EmiteBoletaSamsung()
            With vtaBoletaFiscalSamsumg
                .LvDetalle.ListItems.Clear
                For d = 1 To Me.LvVenta.ListItems.Count
                    .LvDetalle.ListItems.Add , , Me.LvVenta.ListItems(d).SubItems(3)
                    .LvDetalle.ListItems(.LvDetalle.ListItems.Count).SubItems(1) = "COD:" & Me.LvVenta.ListItems(d).SubItems(1) & " - " & Me.LvVenta.ListItems(d).SubItems(2) & " - " & Me.LvVenta.ListItems(d).SubItems(10)
                    .LvDetalle.ListItems(.LvDetalle.ListItems.Count).SubItems(2) = Me.LvVenta.ListItems(d).SubItems(4)
                Next
                
                .LvDescuentos.ListItems.Clear
                If Val(Me.TxtValorDescuento) > 0 Then
                    .LvDescuentos.ListItems.Add , , ""
                    .LvDescuentos.ListItems(.LvDescuentos.ListItems.Count).SubItems(1) = TxtDescuentoX100 & " % DESCUENTO"
                    .LvDescuentos.ListItems(.LvDescuentos.ListItems.Count).SubItems(2) = CDbl(TxtValorDescuento)
                End If
                If Val(Me.TxtDsctoAjuste) > 0 Then
                    .LvDescuentos.ListItems.Add , , ""
                    .LvDescuentos.ListItems(.LvDescuentos.ListItems.Count).SubItems(1) = "DESC. ADIC."
                    .LvDescuentos.ListItems(.LvDescuentos.ListItems.Count).SubItems(2) = CDbl(Me.TxtDsctoAjuste)
                End If
                If Val(Me.TxtRecargoAjuste) > 0 Then
                    .LvDescuentos.ListItems.Add , , ""
                    .LvDescuentos.ListItems(.LvDescuentos.ListItems.Count).SubItems(1) = "AJUSTE SIMPLE"
                    .LvDescuentos.ListItems(.LvDescuentos.ListItems.Count).SubItems(2) = CDbl(Me.TxtRecargoAjuste)
                End If
                
                 .LvPagos.ListItems.Clear
                For i = 1 To venPOS.LVFpago.ListItems.Count
                     .LvPagos.ListItems.Add , , venPOS.LVFpago.ListItems(i).SubItems(2)
                     .LvPagos.ListItems(.LvPagos.ListItems.Count).SubItems(2) = CDbl(venPOS.LVFpago.ListItems(i).SubItems(1))
                Next
                
                .txtTotal = LG_Monto_Pagado_BF
                .EmiteBoleta
            End With
                        
End Sub
Private Sub LimpiaTodo()
    CboSucursal.Clear
    TxtRut = ""
    Me.TxtRazonSocial = ""
    TxtGiro = ""
    TxtDireccion = ""
    txtComuna = ""
    TxtCiudad = ""
    txtFono = ""
    TxtEmail = ""
    TxtValorDescuento = 0
    LvVenta.ListItems.Clear
    CalculaGrilla
    TxtSumaPagos = venPOS.TxtSumaPagos
    TxtSaldoPago = venPOS.TxtSaldoPago
    TxtOC = ""
    Me.TxtObra = ""
    Me.TxtObservacion = ""
    Me.TxtRutFirma = ""
    Me.TxtNombreFirma = ""
    Me.TxtGarantia = ""
    Me.ChkAnexo = 0
    Me.TxtNombreEntrega = ""
    Me.TxtRutEntrega = ""
    Me.Sknnum = ""
    SkNnumcontrato = ""
    Unload Me
    'Me.TxtAccesorios = ""
   '' Unload Me
    If SP_Rut_Activo = "76.169.962-8" Then
        'Solo para alcalde, seleccionaremos vendedor
    
        CboVendedor.ListIndex = CboVendedor.ListCount - 1
    End If
    
    
    On Error Resume Next
    TxtCodigo.SetFocus
End Sub



Private Function PermiteAjuste() As Boolean
    PermiteAjuste = True
    If CDbl(TxtSubTotal) > CDbl(txtTotal) Then
        'Valor con descue
            If (1 - (CDbl(txtTotal) / CDbl(TxtSubTotal))) * 100 > Im_Descuento_Maximo Then
                
                PermiteAjuste = False
            End If
        
        
    
    ElseIf CDbl(TxtSubTotal) < CDbl(txtTotal) Then
            'Con recargo
             If (CDbl(TxtSubTotal) / CDbl(txtTotal)) > Im_Recargo_Maximo Then
                   
                    PermiteAjuste = False
            End If
    End If
End Function
Private Sub CmdF7_Click()
    If LvVenta.SelectedItem Is Nothing Then Exit Sub
    
    LvVenta.ListItems.Remove LvVenta.SelectedItem.Index
    
    CalculaGrilla
    TxtCodigo.SetFocus
End Sub

Private Sub CmdF8_Click()
            If Val(TxtCodigo.Tag) = 0 Then
                    SG_codigo2 = ""
                 
                    If SG_Es_la_Flor = "SI" Then
                        AgregarProductoFlor.Bm_Nuevo = True
                        AgregarProductoFlor.TxtCodigoInterno = TxtCodigo
                        AgregarProductoFlor.Show 1
                    Else
                        AgregarProducto.Bm_Nuevo = True
                        AgregarProducto.Show 1
                    End If
                    If Len(SG_codigo2) > 0 Then
                        TxtCodigo = SG_codigo2
                    End If
            Else
                    SG_codigo2 = ""
                    SG_codigo = TxtCodigo.Tag
                    AgregarProducto.Bm_Nuevo = False
                    If SG_Es_la_Flor = "SI" Then
                        AgregarProductoFlor.Show 1
                    Else
                        AgregarProducto.Show 1
                    End If
                    If Len(SG_codigo2) > 0 Then
                        TxtCodigo = SG_codigo2
                    End If
            End If
            TxtCodigo.SetFocus
End Sub

Private Sub CmdF9_Click()
    If MsgBox("Quitar todos los productos...", vbQuestion + vbOKCancel) = vbOK Then
        LvVenta.ListItems.Clear
        TxtValorDescuento = 0
        CalculaGrilla
        TxtCodigo.SetFocus
    End If
    
End Sub

'Private Sub CmdOk_Click()
'    If Len(TxtPLU) = 0 Then Exit Sub
'    If CDbl(TxtCant) = 0 Then Exit Sub
'
'    Dim Dp_Total As Double
'    Dim Ip_C As Integer
'
'    Dim Dp_Unitario As Double
'    Sql = ""
'    Filtro = "codigo = '" & TxtPLU & "' "
'    If Sm_UtilizaCodigoInterno = "SI" Then
'                Sp_FiltroCI = "pro_codigo_interno='" & TxtPLU & "' "
'                Sql = "SELECT id,pro_inventariable,descripcion,marca,ubicacion_bodega, " & _
'                        "IF((SELECT lst_id FROM maestro_clientes WHERE rut_cliente='" & TxtRut & "')=0 " & _
'                         "OR ISNULL((SELECT lst_id FROM maestro_clientes WHERE rut_cliente='" & TxtRut & "')) ,precio_venta," & _
'                        "IFNULL((SELECT lsd_precio FROM par_lista_precios_detalle d WHERE lst_id=" & Val(TxtListaPrecio.Tag) & " AND m.id=d.id),precio_venta)) precio_venta," & _
'                        "IFNULL((SELECT AVG(pro_ultimo_precio_compra) FROM pro_stock s WHERE s.rut_emp='" & SP_Rut_Activo & "' AND s.pro_codigo=m.codigo),0) precio_compra, " & _
'                        "(SELECT sto_stock FROM pro_stock WHERE rut_emp='" & SP_Rut_Activo & "' AND pro_codigo=m.codigo) stock,ubicacion_bodega " & _
'                    "FROM maestro_productos m " & _
'                    "WHERE pro_activo='SI' AND m.rut_emp='" & SP_Rut_Activo & "' AND " & Sp_FiltroCI & " UNION "
'    End If
'    Sql = Sql & "SELECT id, pro_inventariable,descripcion,marca,ubicacion_bodega, " & _
'            "IF((SELECT lst_id FROM maestro_clientes WHERE rut_cliente='" & TxtRut & "')=0 " & _
'             "OR ISNULL((SELECT lst_id FROM maestro_clientes WHERE rut_cliente='" & TxtRut & "')) ,precio_venta," & _
'            "IFNULL((SELECT lsd_precio FROM par_lista_precios_detalle d WHERE lst_id=" & Val(TxtListaPrecio.Tag) & " AND m.id=d.id),precio_venta)) precio_venta," & _
'            "IFNULL((SELECT AVG(pro_ultimo_precio_compra) FROM pro_stock s WHERE s.rut_emp='" & SP_Rut_Activo & "' AND s.pro_codigo=m.codigo),0) precio_compra, " & _
'            "(SELECT sto_stock FROM pro_stock WHERE rut_emp='" & SP_Rut_Activo & "' AND pro_codigo=m.codigo) stock,ubicacion_bodega " & _
'          "FROM maestro_productos m " & _
'          "WHERE pro_activo='SI' AND  m.rut_emp='" & SP_Rut_Activo & "' AND " & Filtro & " LIMIT 1"
'    Consulta RsTmp, Sql
'    If RsTmp.RecordCount Then
'
'        'If Sm_PermiteDscto = "NO" And Sm_VentaRapida = "SI" Then
'        'probar
'        If Sm_VentaRapida = "SI" Then
'
'                Dp_Descuento = 0
'                Dp_Unitario = RsTmp!precio_venta - Dp_Descuento
'
'                Dp_Total = Dp_Unitario * Val(CxP(TxtCant))
'                LvDetalle.ListItems.Add , , LvDetalle.ListItems.Count + 1
'                Ip_C = LvDetalle.ListItems.Count
'                LvDetalle.ListItems(Ip_C).SubItems(1) = RsTmp!Id
'                LvDetalle.ListItems(Ip_C).SubItems(2) = TxtCant
'                LvDetalle.ListItems(Ip_C).SubItems(3) = RsTmp!Descripcion
'                LvDetalle.ListItems(Ip_C).SubItems(4) = NumFormat(RsTmp!precio_venta) 'unitario
'                LvDetalle.ListItems(Ip_C).SubItems(5) = NumFormat(Dp_Total) ' cant x unitario
'                LvDetalle.ListItems(Ip_C).SubItems(6) = RsTmp!pro_inventariable
'                LvDetalle.ListItems(Ip_C).SubItems(7) = RsTmp!precio_compra
'                LvDetalle.ListItems(Ip_C).SubItems(8) = RsTmp!precio_venta
'                LvDetalle.ListItems(Ip_C).SubItems(9) = Dp_Descuento
'
'                TxtStock = Val("" & RsTmp!stock)
'                TxtUbicacion = "" & RsTmp!ubicacion_bodega
'                TxtDescripcion = RsTmp!Descripcion
'                TxtCant = "1,000"
'                CboDescuento.ListIndex = 0
'                TxtPLU = ""
'
'                If LvDetalle.ListItems.Count > 0 Then
'                    LvDetalle.ListItems(LvDetalle.ListItems.Count).Selected = True
'                End If
'
'
'
'                ElTotal
'                TxtPLU.SetFocus
'        Else
'
'                '
'                TxtDescripcion = RsTmp!Descripcion
'                TxtPrecioOriginal = RsTmp!precio_venta
'                TxtStock = Val("0" & RsTmp!stock)
'                TxtUbicacion = RsTmp!ubicacion_bodega
'                On Error Resume Next
'                CboDescuento.SetFocus
'
'
'        End If
'
'    End If
'
'
'
'End Sub
Private Sub ElTotal()
    'txtTotal = NumFormat(TotalizaColumna(LvDetalle, "total"))
End Sub


Private Sub CmdVisorCotizaciones_Click()
    SG_codigo2 = ""
    ven_visor_cotizaciones.Show 1
    If Val(SG_codigo) > 0 Then
        CargaCotizacion Val(SG_codigo)
        If Val(SG_codigo2) > 0 Then
            'Imprimir cotizzcion
           ' MsgBox "Imprimir cotizacion Nro " & SG_codigo2
            CargaCotizacion Val(SG_codigo)
            PrevisualizaCotizacion
            On Error GoTo CancelaImpesionNV
            If SP_Rut_Activo = "76.553.302-3" Or SP_Rut_Activo = "76.169.962-8" Then
            'kyr y alcalde
            
                    If SG_codigo = "print" Then
                            
                            Dialogo.CancelError = True
                            Dialogo.ShowPrinter
                            La_Establecer_Impresora Printer.DeviceName
                            
                            If SP_Rut_Activo = "" Then
                                ImprimeCOTIZACION_Alcalde
                            Else
                          '  EstableImpresora Printer.DeviceName
                                ImprimeCOTIZACION
                            End If
                    End If
            ElseIf SP_Rut_Activo = "78.967.170-2" Then
                'Plasticos aldunate
                If SG_codigo = "print" Then
                    Dialogo.CancelError = True
                    Dialogo.ShowPrinter
                    
                    If Printer.DeviceName = "TERMICA" Then
                        ProcCOTIZACION
                    Else
                        La_Establecer_Impresora Printer.DeviceName
                        ImprimeCOTIZACION
                    End If
                End If
            Else
            
                'Termica
                If SG_codigo = "print" Then ProcCOTIZACION
            End If
            
            GoTo CancelaImpesionNV
            
        End If
    End If
    Exit Sub
    
CancelaImpesionNV:
    CmdF5.Enabled = True
    LimpiaTodo
End Sub

Private Sub contrato()
    Dim Sp_Plantilla As String
    Dim Sp_PlantillaCpraVta As String
    Dim Sp_Contrato As String
    Dim Sp_ContratoCpraVta As String
    Dim Sp_TxtHolding(2) As String
    Dim Sp_Variedad As String
    Dim Plantilla As New Word.Application
    Dim PlantillaCpraVta As New Word.Application
    Dim contrato As New Word.Document
    Dim ContratoCpraVta As New Word.Document
    Sp_TxtHolding(0) = "A efectos de calcular las cantidades normalizadas totales respecto a acceder a la bonificaci�n por Volumen, se consideraran, en conjunto con el PRODUCTO que sea recepcionado por MALTEXCO asociado al presente contrato, aquella cebada normalizada resultante de los contratos que se identifican a continuaci�n, en la medida que estos hayan sido firmados por ambas partes y se cumplan copulativamente la totalidad de las cl�usulas establecidas en aquellos contratos:"
    Sp_TxtHolding(1) = "Asimismo, en caso de acceder a esta modalidad denominada �HOLDING�, a efectos de la determinaci�n de la bonificaci�n por antig�edad, se considerar� a partir del contrato m�s antiguo. Cabe destacar que las entregas de m�s de un AGRICULTOR en un mismo per�odo de cosecha no se suman para estos efectos."
    
    
    If SP_Rut_Activo = "76.115.474-5" Then
        Sp_Plantilla = App.Path & "\wordr\" & "ContratoArriendoDICOEM.rtf"
        Dialogo.FileName = App.Path & "\wordr\" & Lp_Id_Unico_Venta
    Else
        Sp_Plantilla = App.Path & "\word\" & "ContratoArriendoDICOEM.rtf"
        Dialogo.FileName = App.Path & "\word\" & Lp_Id_Unico_Venta
    End If

    Sp_Contrato = Dialogo.FileName & ".doc"
    'Sp_Contrato = App.Path & "\word\" & TxtRut & ".doc"
    
    FileCopy Sp_Plantilla, Sp_Contrato

    Set contrato = Plantilla.Documents.Open(Sp_Contrato)
        
        Sp_ContratoCpraVta = Dialogo.FileName & "_cpravta.doc"
        FileCopy Sp_Plantilla, Sp_ContratoCpraVta
        Set ContratoCpraVta = PlantillaCpraVta.Documents.Open(Sp_ContratoCpraVta)

         With ContratoCpraVta.Bookmarks
            .Item("NUMERO_CONTRATO").Range.Text = Lp_Id_Unico_Venta
            .Item("FECHA").Range.Text = UCase(FormatDateTime(Date, vbLongDate))
            .Item("NOMBRE_CLIENTE").Range.Text = TxtRazonSocial
            .Item("RUT_CLIENTE").Range.Text = Me.TxtRut
            .Item("DIRECCION_CLIENTE").Range.Text = Me.TxtDireccion
            .Item("GIRO_CLIENTE").Range.Text = Me.TxtGiro
            .Item("COMUNA_CLIENTE").Range.Text = Me.txtComuna
            
            .Item("CODIGO_PRODUCTO1").Range.Text = Me.LvVenta.ListItems(1).SubItems(1)
            .Item("NOMBRE_PRODUCTO1").Range.Text = Me.LvVenta.ListItems(1).SubItems(2)
            .Item("PRECIO_PRODUCTO1").Range.Text = Replace(Me.LvVenta.ListItems(1).SubItems(4), ",", ".")
            
            If Me.LvVenta.ListItems.Count = 2 Then
                .Item("CODIGO_PRODUCTO2").Range.Text = Me.LvVenta.ListItems(2).SubItems(1)
                .Item("NOMBRE_PRODUCTO2").Range.Text = Me.LvVenta.ListItems(2).SubItems(2)
                .Item("PRECIO_PRODUCTO2").Range.Text = Replace(Me.LvVenta.ListItems(2).SubItems(4), ",", ".")
            End If
            If Me.LvVenta.ListItems.Count = 3 Then
                .Item("CODIGO_PRODUCTO2").Range.Text = Me.LvVenta.ListItems(2).SubItems(1)
                .Item("NOMBRE_PRODUCTO2").Range.Text = Me.LvVenta.ListItems(2).SubItems(2)
                .Item("PRECIO_PRODUCTO2").Range.Text = Replace(Me.LvVenta.ListItems(2).SubItems(4), ",", ".")
                .Item("CODIGO_PRODUCTO3").Range.Text = Me.LvVenta.ListItems(3).SubItems(1)
                .Item("NOMBRE_PRODUCTO3").Range.Text = Me.LvVenta.ListItems(3).SubItems(2)
                .Item("PRECIO_PRODUCTO3").Range.Text = Replace(Me.LvVenta.ListItems(3).SubItems(4), ",", ".")
            End If
            If Me.LvVenta.ListItems.Count = 4 Then
                .Item("CODIGO_PRODUCTO2").Range.Text = Me.LvVenta.ListItems(2).SubItems(1)
                .Item("NOMBRE_PRODUCTO2").Range.Text = Me.LvVenta.ListItems(2).SubItems(2)
                .Item("PRECIO_PRODUCTO2").Range.Text = Replace(Me.LvVenta.ListItems(2).SubItems(4), ",", ".")
                .Item("CODIGO_PRODUCTO3").Range.Text = Me.LvVenta.ListItems(3).SubItems(1)
                .Item("NOMBRE_PRODUCTO3").Range.Text = Me.LvVenta.ListItems(3).SubItems(2)
                .Item("PRECIO_PRODUCTO3").Range.Text = Replace(Me.LvVenta.ListItems(3).SubItems(4), ",", ".")
                .Item("CODIGO_PRODUCTO4").Range.Text = Me.LvVenta.ListItems(4).SubItems(1)
                .Item("NOMBRE_PRODUCTO4").Range.Text = Me.LvVenta.ListItems(4).SubItems(2)
                .Item("PRECIO_PRODUCTO4").Range.Text = Replace(Me.LvVenta.ListItems(4).SubItems(4), ",", ".")
            End If
            
            .Item("OBSERVACION").Range.Text = Me.TxtObservacion
            .Item("OBRA").Range.Text = Me.TxtObra
            .Item("TELEFONO").Range.Text = Me.txtFono
            .Item("NOMBRE_FIRMA").Range.Text = Me.TxtNombreFirma
            .Item("RUT_FIRMA").Range.Text = Me.TxtRutFirma
            '.Item("HORA").Range.Text = Time
            .Item("GARANTIA").Range.Text = Me.TxtGarantia
            .Item("FECHA_INICIO_CONTRATO").Range.Text = FormatDateTime(DtInicioC, vbLongDate)
            .Item("HORA_INICIO_CONTRATO").Range.Text = Me.Hora_Inicio
            .Item("ORDEN_DE_COMPRA").Range.Text = Me.TxtOC
            .Item("NOMBRE_QUIEN_ENTREGA").Range.Text = Me.TxtNombreEntrega
            .Item("RUT_QUIEN_ENTREGA").Range.Text = Me.TxtRutEntrega
''Control Interno
            .Item("NUMERO_CONTRATOI").Range.Text = Lp_Id_Unico_Venta
            .Item("FECHAI").Range.Text = FormatDateTime(Date, vbLongDate)
            .Item("NOMBRE_CLIENTEI").Range.Text = TxtRazonSocial
            .Item("RUT_CLIENTEI").Range.Text = Me.TxtRut
            .Item("DIRECCION_CLIENTEI").Range.Text = Me.TxtDireccion
            .Item("GIRO_CLIENTEI").Range.Text = Me.TxtGiro
            .Item("COMUNA_CLIENTEI").Range.Text = Me.txtComuna
            
            .Item("CODIGO_PRODUCTOI1").Range.Text = Me.LvVenta.ListItems(1).SubItems(1)
            .Item("NOMBRE_PRODUCTOI1").Range.Text = Me.LvVenta.ListItems(1).SubItems(2)
            .Item("PRECIO_PRODUCTOI1").Range.Text = Replace(Me.LvVenta.ListItems(1).SubItems(4), ",", ".")
            
            If Me.LvVenta.ListItems.Count = 2 Then
                .Item("CODIGO_PRODUCTOI2").Range.Text = Me.LvVenta.ListItems(2).SubItems(1)
                .Item("NOMBRE_PRODUCTOI2").Range.Text = Me.LvVenta.ListItems(2).SubItems(2)
                .Item("PRECIO_PRODUCTOI2").Range.Text = Replace(Me.LvVenta.ListItems(2).SubItems(4), ",", ".")
            End If
            If Me.LvVenta.ListItems.Count = 3 Then
                .Item("CODIGO_PRODUCTOI2").Range.Text = Me.LvVenta.ListItems(2).SubItems(1)
                .Item("NOMBRE_PRODUCTOI2").Range.Text = Me.LvVenta.ListItems(2).SubItems(2)
                .Item("PRECIO_PRODUCTOI2").Range.Text = Replace(Me.LvVenta.ListItems(2).SubItems(4), ",", ".")
                .Item("CODIGO_PRODUCTOI3").Range.Text = Me.LvVenta.ListItems(3).SubItems(1)
                .Item("NOMBRE_PRODUCTOI3").Range.Text = Me.LvVenta.ListItems(3).SubItems(2)
                .Item("PRECIO_PRODUCTOI3").Range.Text = Replace(Me.LvVenta.ListItems(3).SubItems(4), ",", ".")
            End If
            If Me.LvVenta.ListItems.Count = 4 Then
                .Item("CODIGO_PRODUCTOI2").Range.Text = Me.LvVenta.ListItems(2).SubItems(1)
                .Item("NOMBRE_PRODUCTOI2").Range.Text = Me.LvVenta.ListItems(2).SubItems(2)
                .Item("PRECIO_PRODUCTOI2").Range.Text = Replace(Me.LvVenta.ListItems(2).SubItems(4), ",", ".")
                .Item("CODIGO_PRODUCTOI3").Range.Text = Me.LvVenta.ListItems(3).SubItems(1)
                .Item("NOMBRE_PRODUCTOI3").Range.Text = Me.LvVenta.ListItems(3).SubItems(2)
                .Item("PRECIO_PRODUCTOI3").Range.Text = Replace(Me.LvVenta.ListItems(3).SubItems(4), ",", ".")
                .Item("CODIGO_PRODUCTOI4").Range.Text = Me.LvVenta.ListItems(4).SubItems(1)
                .Item("NOMBRE_PRODUCTOI4").Range.Text = Me.LvVenta.ListItems(4).SubItems(2)
                .Item("PRECIO_PRODUCTOI4").Range.Text = Replace(Me.LvVenta.ListItems(4).SubItems(4), ",", ".")
            End If
            
            .Item("OBSERVACIONI").Range.Text = Me.TxtObservacion
            .Item("OBRAI").Range.Text = Me.TxtObra
            .Item("TELEFONOI").Range.Text = Me.txtFono
            .Item("NOMBRE_FIRMAI").Range.Text = Me.TxtNombreFirma
            .Item("RUT_FIRMAI").Range.Text = Me.TxtRutFirma
            '.Item("HORA").Range.Text = Time
            .Item("GARANTIAI").Range.Text = Me.TxtGarantia
            .Item("FECHA_INICIO_CONTRATOI").Range.Text = FormatDateTime(DtInicioC, vbLongDate)
            .Item("HORA_INICIO_CONTRATOI").Range.Text = Me.Hora_Inicio
            .Item("ORDEN_DE_COMPRAI").Range.Text = Me.TxtOC
            .Item("NOMBRE_QUIEN_ENTREGAI").Range.Text = Me.TxtNombreEntrega
            .Item("RUT_QUIEN_ENTREGAI").Range.Text = Me.TxtRutEntrega
''Cliente
            .Item("NUMERO_CONTRATOC").Range.Text = Lp_Id_Unico_Venta
            .Item("FECHAC").Range.Text = FormatDateTime(Date, vbLongDate)
            .Item("NOMBRE_CLIENTEC").Range.Text = TxtRazonSocial
            .Item("RUT_CLIENTEC").Range.Text = Me.TxtRut
            .Item("DIRECCION_CLIENTEC").Range.Text = Me.TxtDireccion
            .Item("GIRO_CLIENTEC").Range.Text = Me.TxtGiro
            .Item("COMUNA_CLIENTEC").Range.Text = Me.txtComuna
            
            .Item("CODIGO_PRODUCTOC1").Range.Text = Me.LvVenta.ListItems(1).SubItems(1)
            .Item("NOMBRE_PRODUCTOC1").Range.Text = Me.LvVenta.ListItems(1).SubItems(2)
            .Item("PRECIO_PRODUCTOC1").Range.Text = Replace(Me.LvVenta.ListItems(1).SubItems(4), ",", ".")
            
            If Me.LvVenta.ListItems.Count = 2 Then
                .Item("CODIGO_PRODUCTOC2").Range.Text = Me.LvVenta.ListItems(2).SubItems(1)
                .Item("NOMBRE_PRODUCTOC2").Range.Text = Me.LvVenta.ListItems(2).SubItems(2)
                .Item("PRECIO_PRODUCTOC2").Range.Text = Replace(Me.LvVenta.ListItems(2).SubItems(4), ",", ".")
            End If
            If Me.LvVenta.ListItems.Count = 3 Then
                .Item("CODIGO_PRODUCTOC2").Range.Text = Me.LvVenta.ListItems(2).SubItems(1)
                .Item("NOMBRE_PRODUCTOC2").Range.Text = Me.LvVenta.ListItems(2).SubItems(2)
                .Item("PRECIO_PRODUCTOC2").Range.Text = Replace(Me.LvVenta.ListItems(2).SubItems(4), ",", ".")
                .Item("CODIGO_PRODUCTOC3").Range.Text = Me.LvVenta.ListItems(3).SubItems(1)
                .Item("NOMBRE_PRODUCTOC3").Range.Text = Me.LvVenta.ListItems(3).SubItems(2)
                .Item("PRECIO_PRODUCTOC3").Range.Text = Replace(Me.LvVenta.ListItems(3).SubItems(4), ",", ".")
            End If
            If Me.LvVenta.ListItems.Count = 4 Then
                .Item("CODIGO_PRODUCTOC2").Range.Text = Me.LvVenta.ListItems(2).SubItems(1)
                .Item("NOMBRE_PRODUCTOC2").Range.Text = Me.LvVenta.ListItems(2).SubItems(2)
                .Item("PRECIO_PRODUCTOC2").Range.Text = Replace(Me.LvVenta.ListItems(2).SubItems(4), ",", ".")
                .Item("CODIGO_PRODUCTOC3").Range.Text = Me.LvVenta.ListItems(3).SubItems(1)
                .Item("NOMBRE_PRODUCTOC3").Range.Text = Me.LvVenta.ListItems(3).SubItems(2)
                .Item("PRECIO_PRODUCTOC3").Range.Text = Replace(Me.LvVenta.ListItems(3).SubItems(4), ",", ".")
                .Item("CODIGO_PRODUCTOC4").Range.Text = Me.LvVenta.ListItems(4).SubItems(1)
                .Item("NOMBRE_PRODUCTOC4").Range.Text = Me.LvVenta.ListItems(4).SubItems(2)
                .Item("PRECIO_PRODUCTOC4").Range.Text = Replace(Me.LvVenta.ListItems(4).SubItems(4), ",", ".")
            End If
            
            .Item("OBSERVACIONC").Range.Text = Me.TxtObservacion
            .Item("OBRAC").Range.Text = Me.TxtObra
            .Item("TELEFONOC").Range.Text = Me.txtFono
            .Item("NOMBRE_FIRMAC").Range.Text = Me.TxtNombreFirma
            .Item("RUT_FIRMAC").Range.Text = Me.TxtRutFirma
            '.Item("HORA").Range.Text = Time
            .Item("GARANTIAC").Range.Text = Me.TxtGarantia
            .Item("FECHA_INICIO_CONTRATOC").Range.Text = FormatDateTime(DtInicioC, vbLongDate)
            .Item("HORA_INICIO_CONTRATOC").Range.Text = Me.Hora_Inicio
            .Item("ORDEN_DE_COMPRAC").Range.Text = Me.TxtOC
            .Item("NOMBRE_QUIEN_ENTREGAC").Range.Text = Me.TxtNombreEntrega
            .Item("RUT_QUIEN_ENTREGAC").Range.Text = Me.TxtRutEntrega
        End With
        PlantillaCpraVta.Visible = True

End Sub
Private Sub contratoAnulado()
    Dim Sp_Plantilla As String
    Dim Sp_PlantillaCpraVta As String
    Dim Sp_Contrato As String
    Dim Sp_ContratoCpraVta As String
    Dim Sp_TxtHolding(2) As String
    Dim Sp_Variedad As String
    Dim Plantilla As New Word.Application
    Dim PlantillaCpraVta As New Word.Application
    Dim contrato As New Word.Document
    Dim ContratoCpraVta As New Word.Document
    Sp_TxtHolding(0) = "A efectos de calcular las cantidades normalizadas totales respecto a acceder a la bonificaci�n por Volumen, se consideraran, en conjunto con el PRODUCTO que sea recepcionado por MALTEXCO asociado al presente contrato, aquella cebada normalizada resultante de los contratos que se identifican a continuaci�n, en la medida que estos hayan sido firmados por ambas partes y se cumplan copulativamente la totalidad de las cl�usulas establecidas en aquellos contratos:"
    Sp_TxtHolding(1) = "Asimismo, en caso de acceder a esta modalidad denominada �HOLDING�, a efectos de la determinaci�n de la bonificaci�n por antig�edad, se considerar� a partir del contrato m�s antiguo. Cabe destacar que las entregas de m�s de un AGRICULTOR en un mismo per�odo de cosecha no se suman para estos efectos."
    
    Sp_Plantilla = App.Path & "\word\" & "ContratoArriendoDICOEMAnulado.rtf"

    Dialogo.FileName = App.Path & "\word\" & Lp_Id_Unico_Venta
    'Dialogo.FileName = "C:" & "\word\" & Lp_Id_Unico_Venta
    '  Dialogo.ShowSave
    '  On Error GoTo problemaGenerar
    Sp_Contrato = Dialogo.FileName & ".doc"
    'Sp_Contrato = App.Path & "\word\" & TxtRut & ".doc"
    
    FileCopy Sp_Plantilla, Sp_Contrato

    Set contrato = Plantilla.Documents.Open(Sp_Contrato)
        
        Sp_ContratoCpraVta = Dialogo.FileName & "_cpravta.doc"
        FileCopy Sp_Plantilla, Sp_ContratoCpraVta
        Set ContratoCpraVta = PlantillaCpraVta.Documents.Open(Sp_ContratoCpraVta)

         With ContratoCpraVta.Bookmarks
            .Item("NUMERO_CONTRATO").Range.Text = Lp_Id_Unico_Venta
            .Item("FECHA").Range.Text = FormatDateTime(Date, vbLongDate)
            .Item("NOMBRE_CLIENTE").Range.Text = TxtRazonSocial
            .Item("RUT_CLIENTE").Range.Text = Me.TxtRut
            .Item("DIRECCION_CLIENTE").Range.Text = Me.TxtDireccion
            .Item("GIRO_CLIENTE").Range.Text = Me.TxtGiro
            .Item("COMUNA_CLIENTE").Range.Text = Me.txtComuna
            
            .Item("CODIGO_PRODUCTO1").Range.Text = Me.LvVenta.ListItems(1).SubItems(1)
            .Item("NOMBRE_PRODUCTO1").Range.Text = Me.LvVenta.ListItems(1).SubItems(2)
            .Item("PRECIO_PRODUCTO1").Range.Text = Me.LvVenta.ListItems(1).SubItems(4)
            
            If Me.LvVenta.ListItems.Count = 2 Then
            .Item("CODIGO_PRODUCTO2").Range.Text = Me.LvVenta.ListItems(2).SubItems(1)
            .Item("NOMBRE_PRODUCTO2").Range.Text = Me.LvVenta.ListItems(2).SubItems(2)
            .Item("PRECIO_PRODUCTO2").Range.Text = Me.LvVenta.ListItems(2).SubItems(4)
            Else

            End If
            If Me.LvVenta.ListItems.Count = 3 Then
            .Item("CODIGO_PRODUCTO2").Range.Text = Me.LvVenta.ListItems(2).SubItems(1)
            .Item("NOMBRE_PRODUCTO2").Range.Text = Me.LvVenta.ListItems(2).SubItems(2)
            .Item("PRECIO_PRODUCTO2").Range.Text = Me.LvVenta.ListItems(2).SubItems(4)
            .Item("CODIGO_PRODUCTO3").Range.Text = Me.LvVenta.ListItems(3).SubItems(1)
            .Item("NOMBRE_PRODUCTO3").Range.Text = Me.LvVenta.ListItems(3).SubItems(2)
            .Item("PRECIO_PRODUCTO3").Range.Text = Me.LvVenta.ListItems(3).SubItems(4)
            Else

            End If
            If Me.LvVenta.ListItems.Count = 4 Then
            .Item("CODIGO_PRODUCTO2").Range.Text = Me.LvVenta.ListItems(2).SubItems(1)
            .Item("NOMBRE_PRODUCTO2").Range.Text = Me.LvVenta.ListItems(2).SubItems(2)
            .Item("PRECIO_PRODUCTO2").Range.Text = Me.LvVenta.ListItems(2).SubItems(4)
            .Item("CODIGO_PRODUCTO3").Range.Text = Me.LvVenta.ListItems(3).SubItems(1)
            .Item("NOMBRE_PRODUCTO3").Range.Text = Me.LvVenta.ListItems(3).SubItems(2)
            .Item("PRECIO_PRODUCTO3").Range.Text = Me.LvVenta.ListItems(3).SubItems(4)
            .Item("CODIGO_PRODUCTO4").Range.Text = Me.LvVenta.ListItems(4).SubItems(1)
            .Item("NOMBRE_PRODUCTO4").Range.Text = Me.LvVenta.ListItems(4).SubItems(2)
            .Item("PRECIO_PRODUCTO4").Range.Text = Me.LvVenta.ListItems(4).SubItems(4)
            Else

            End If
            
            .Item("OBSERVACION").Range.Text = Me.TxtObservacion
            .Item("OBRA").Range.Text = Me.TxtObra
            .Item("TELEFONO").Range.Text = Me.txtFono
            .Item("NOMBRE_FIRMA").Range.Text = Me.TxtNombreFirma
            .Item("RUT_FIRMA").Range.Text = Me.TxtRutFirma
            '.Item("HORA").Range.Text = Time
            .Item("GARANTIA").Range.Text = Me.TxtGarantia
            .Item("FECHA_INICIO_CONTRATO").Range.Text = FormatDateTime(DtInicioC, vbLongDate)
            .Item("HORA_INICIO_CONTRATO").Range.Text = Me.Hora_Inicio
            .Item("ORDEN_DE_COMPRA").Range.Text = Me.TxtOC
            .Item("NOMBRE_QUIEN_ENTREGA").Range.Text = Me.TxtNombreEntrega
            .Item("RUT_QUIEN_ENTREGA").Range.Text = Me.TxtRutEntrega
''Control Interno
            .Item("NUMERO_CONTRATOI").Range.Text = Lp_Id_Unico_Venta
            .Item("FECHAI").Range.Text = FormatDateTime(Date, vbLongDate)
            .Item("NOMBRE_CLIENTEI").Range.Text = TxtRazonSocial
            .Item("RUT_CLIENTEI").Range.Text = Me.TxtRut
            .Item("DIRECCION_CLIENTEI").Range.Text = Me.TxtDireccion
            .Item("GIRO_CLIENTEI").Range.Text = Me.TxtGiro
            .Item("COMUNA_CLIENTEI").Range.Text = Me.txtComuna
            
            .Item("CODIGO_PRODUCTOI1").Range.Text = Me.LvVenta.ListItems(1).SubItems(1)
            .Item("NOMBRE_PRODUCTOI1").Range.Text = Me.LvVenta.ListItems(1).SubItems(2)
            .Item("PRECIO_PRODUCTOI1").Range.Text = Me.LvVenta.ListItems(1).SubItems(4)
            
            If Me.LvVenta.ListItems.Count = 2 Then
            .Item("CODIGO_PRODUCTOI2").Range.Text = Me.LvVenta.ListItems(2).SubItems(1)
            .Item("NOMBRE_PRODUCTOI2").Range.Text = Me.LvVenta.ListItems(2).SubItems(2)
            .Item("PRECIO_PRODUCTOI2").Range.Text = Me.LvVenta.ListItems(2).SubItems(4)
            Else

            End If
            If Me.LvVenta.ListItems.Count = 3 Then
            .Item("CODIGO_PRODUCTOI2").Range.Text = Me.LvVenta.ListItems(2).SubItems(1)
            .Item("NOMBRE_PRODUCTOI2").Range.Text = Me.LvVenta.ListItems(2).SubItems(2)
            .Item("PRECIO_PRODUCTOI2").Range.Text = Me.LvVenta.ListItems(2).SubItems(4)
            .Item("CODIGO_PRODUCTOI3").Range.Text = Me.LvVenta.ListItems(3).SubItems(1)
            .Item("NOMBRE_PRODUCTOI3").Range.Text = Me.LvVenta.ListItems(3).SubItems(2)
            .Item("PRECIO_PRODUCTOI3").Range.Text = Me.LvVenta.ListItems(3).SubItems(4)
            Else

            End If
            If Me.LvVenta.ListItems.Count = 4 Then
            .Item("CODIGO_PRODUCTOI2").Range.Text = Me.LvVenta.ListItems(2).SubItems(1)
            .Item("NOMBRE_PRODUCTOI2").Range.Text = Me.LvVenta.ListItems(2).SubItems(2)
            .Item("PRECIO_PRODUCTOI2").Range.Text = Me.LvVenta.ListItems(2).SubItems(4)
            .Item("CODIGO_PRODUCTOI3").Range.Text = Me.LvVenta.ListItems(3).SubItems(1)
            .Item("NOMBRE_PRODUCTOI3").Range.Text = Me.LvVenta.ListItems(3).SubItems(2)
            .Item("PRECIO_PRODUCTOI3").Range.Text = Me.LvVenta.ListItems(3).SubItems(4)
            .Item("CODIGO_PRODUCTOI4").Range.Text = Me.LvVenta.ListItems(4).SubItems(1)
            .Item("NOMBRE_PRODUCTOI4").Range.Text = Me.LvVenta.ListItems(4).SubItems(2)
            .Item("PRECIO_PRODUCTOI4").Range.Text = Me.LvVenta.ListItems(4).SubItems(4)
            Else

            End If
            
            .Item("OBSERVACIONI").Range.Text = Me.TxtObservacion
            .Item("OBRAI").Range.Text = Me.TxtObra
            .Item("TELEFONOI").Range.Text = Me.txtFono
            .Item("NOMBRE_FIRMAI").Range.Text = Me.TxtNombreFirma
            .Item("RUT_FIRMAI").Range.Text = Me.TxtRutFirma
            '.Item("HORA").Range.Text = Time
            .Item("GARANTIAI").Range.Text = Me.TxtGarantia
            .Item("FECHA_INICIO_CONTRATOI").Range.Text = FormatDateTime(DtInicioC, vbLongDate)
            .Item("HORA_INICIO_CONTRATOI").Range.Text = Me.Hora_Inicio
            .Item("ORDEN_DE_COMPRAI").Range.Text = Me.TxtOC
            .Item("NOMBRE_QUIEN_ENTREGAI").Range.Text = Me.TxtNombreEntrega
            .Item("RUT_QUIEN_ENTREGAI").Range.Text = Me.TxtRutEntrega
''Cliente
            .Item("NUMERO_CONTRATOC").Range.Text = Lp_Id_Unico_Venta
            .Item("FECHAC").Range.Text = FormatDateTime(Date, vbLongDate)
            .Item("NOMBRE_CLIENTEC").Range.Text = TxtRazonSocial
            .Item("RUT_CLIENTEC").Range.Text = Me.TxtRut
            .Item("DIRECCION_CLIENTEC").Range.Text = Me.TxtDireccion
            .Item("GIRO_CLIENTEC").Range.Text = Me.TxtGiro
            .Item("COMUNA_CLIENTEC").Range.Text = Me.txtComuna
            
            .Item("CODIGO_PRODUCTOC1").Range.Text = Me.LvVenta.ListItems(1).SubItems(1)
            .Item("NOMBRE_PRODUCTOC1").Range.Text = Me.LvVenta.ListItems(1).SubItems(2)
            .Item("PRECIO_PRODUCTOC1").Range.Text = Me.LvVenta.ListItems(1).SubItems(4)
            
            If Me.LvVenta.ListItems.Count = 2 Then
            .Item("CODIGO_PRODUCTOC2").Range.Text = Me.LvVenta.ListItems(2).SubItems(1)
            .Item("NOMBRE_PRODUCTOC2").Range.Text = Me.LvVenta.ListItems(2).SubItems(2)
            .Item("PRECIO_PRODUCTOC2").Range.Text = Me.LvVenta.ListItems(2).SubItems(4)
            Else

            End If
            If Me.LvVenta.ListItems.Count = 3 Then
            .Item("CODIGO_PRODUCTOC2").Range.Text = Me.LvVenta.ListItems(2).SubItems(1)
            .Item("NOMBRE_PRODUCTOC2").Range.Text = Me.LvVenta.ListItems(2).SubItems(2)
            .Item("PRECIO_PRODUCTOC2").Range.Text = Me.LvVenta.ListItems(2).SubItems(4)
            .Item("CODIGO_PRODUCTOC3").Range.Text = Me.LvVenta.ListItems(3).SubItems(1)
            .Item("NOMBRE_PRODUCTOC3").Range.Text = Me.LvVenta.ListItems(3).SubItems(2)
            .Item("PRECIO_PRODUCTOC3").Range.Text = Me.LvVenta.ListItems(3).SubItems(4)
            Else

            End If
            If Me.LvVenta.ListItems.Count = 4 Then
            .Item("CODIGO_PRODUCTOC2").Range.Text = Me.LvVenta.ListItems(2).SubItems(1)
            .Item("NOMBRE_PRODUCTOC2").Range.Text = Me.LvVenta.ListItems(2).SubItems(2)
            .Item("PRECIO_PRODUCTOC2").Range.Text = Me.LvVenta.ListItems(2).SubItems(4)
            .Item("CODIGO_PRODUCTOC3").Range.Text = Me.LvVenta.ListItems(3).SubItems(1)
            .Item("NOMBRE_PRODUCTOC3").Range.Text = Me.LvVenta.ListItems(3).SubItems(2)
            .Item("PRECIO_PRODUCTOC3").Range.Text = Me.LvVenta.ListItems(3).SubItems(4)
            .Item("CODIGO_PRODUCTOC4").Range.Text = Me.LvVenta.ListItems(4).SubItems(1)
            .Item("NOMBRE_PRODUCTOC4").Range.Text = Me.LvVenta.ListItems(4).SubItems(2)
            .Item("PRECIO_PRODUCTOC4").Range.Text = Me.LvVenta.ListItems(4).SubItems(4)
            Else

            End If
            
            .Item("OBSERVACIONC").Range.Text = Me.TxtObservacion
            .Item("OBRAC").Range.Text = Me.TxtObra
            .Item("TELEFONOC").Range.Text = Me.txtFono
            .Item("NOMBRE_FIRMAC").Range.Text = Me.TxtNombreFirma
            .Item("RUT_FIRMAC").Range.Text = Me.TxtRutFirma
            '.Item("HORA").Range.Text = Time
            .Item("GARANTIAC").Range.Text = Me.TxtGarantia
            .Item("FECHA_INICIO_CONTRATOC").Range.Text = FormatDateTime(DtInicioC, vbLongDate)
            .Item("HORA_INICIO_CONTRATOC").Range.Text = Me.Hora_Inicio
            .Item("ORDEN_DE_COMPRAC").Range.Text = Me.TxtOC
            .Item("NOMBRE_QUIEN_ENTREGAC").Range.Text = Me.TxtNombreEntrega
            .Item("RUT_QUIEN_ENTREGAC").Range.Text = Me.TxtRutEntrega
        End With
        PlantillaCpraVta.Visible = True

End Sub

Private Sub contratoAnexo()
    Dim Sp_Plantilla As String
    Dim Sp_PlantillaCpraVta As String
    Dim Sp_Contrato As String
    Dim Sp_ContratoCpraVta As String
    Dim Sp_TxtHolding(2) As String
    
'    VerificaNroContrato
'    VerificaDatos
 '   If Not Bm_Grabas Then Exit Sub
    
    Dim Sp_Variedad As String
    Dim Plantilla As New Word.Application
    Dim PlantillaCpraVta As New Word.Application
    Dim contrato As New Word.Document
    Dim ContratoCpraVta As New Word.Document
    Sp_TxtHolding(0) = "A efectos de calcular las cantidades normalizadas totales respecto a acceder a la bonificaci�n por Volumen, se consideraran, en conjunto con el PRODUCTO que sea recepcionado por MALTEXCO asociado al presente contrato, aquella cebada normalizada resultante de los contratos que se identifican a continuaci�n, en la medida que estos hayan sido firmados por ambas partes y se cumplan copulativamente la totalidad de las cl�usulas establecidas en aquellos contratos:"
    Sp_TxtHolding(1) = "Asimismo, en caso de acceder a esta modalidad denominada �HOLDING�, a efectos de la determinaci�n de la bonificaci�n por antig�edad, se considerar� a partir del contrato m�s antiguo. Cabe destacar que las entregas de m�s de un AGRICULTOR en un mismo per�odo de cosecha no se suman para estos efectos."
    
  
            Sp_Plantilla = App.Path & "\word\" & "ContratoArriendoDICOEMAnexo.rtf"

    '
    'Dialogo.FileName = TxtRut '& "_" & CboOAUso.Text & "_" & TxtNroContrato
  '  On Error GoTo CancelaCrea
  Dialogo.FileName = App.Path & "\word\" & TxtRut
  '  Dialogo.ShowSave
  '  On Error GoTo problemaGenerar
    Sp_Contrato = Dialogo.FileName & ".doc"
    'Sp_Contrato = App.Path & "\word\" & TxtRut & ".doc"
    
    FileCopy Sp_Plantilla, Sp_Contrato

    Set contrato = Plantilla.Documents.Open(Sp_Contrato)
    '
 '   If LG_Periodo = 2016 Then
        'Agregar ctrato compra venta
        
        Sp_ContratoCpraVta = Dialogo.FileName & "_cpravta.doc"
        FileCopy Sp_Plantilla, Sp_ContratoCpraVta
        Set ContratoCpraVta = PlantillaCpraVta.Documents.Open(Sp_ContratoCpraVta)

         With ContratoCpraVta.Bookmarks
            .Item("NUMERO_CONTRATO").Range.Text = Lp_Id_Unico_Venta
            .Item("FECHA").Range.Text = FormatDateTime(Date, vbLongDate)
            .Item("NOMBRE_CLIENTE").Range.Text = TxtRazonSocial
            .Item("RUT_CLIENTE").Range.Text = Me.TxtRut
            .Item("DIRECCION_CLIENTE").Range.Text = Me.TxtDireccion
            .Item("GIRO_CLIENTE").Range.Text = Me.TxtGiro
            .Item("COMUNA_CLIENTE").Range.Text = Me.txtComuna
            
            .Item("CODIGO_PRODUCTO1").Range.Text = Me.LvVenta.ListItems(1).SubItems(1)
            .Item("NOMBRE_PRODUCTO1").Range.Text = Me.LvVenta.ListItems(1).SubItems(2)
            .Item("PRECIO_PRODUCTO1").Range.Text = Me.LvVenta.ListItems(1).SubItems(4)
            
            If Me.LvVenta.ListItems.Count = 2 Then
                .Item("CODIGO_PRODUCTO2").Range.Text = Me.LvVenta.ListItems(2).SubItems(1)
                .Item("NOMBRE_PRODUCTO2").Range.Text = Me.LvVenta.ListItems(2).SubItems(2)
                .Item("PRECIO_PRODUCTO2").Range.Text = Me.LvVenta.ListItems(2).SubItems(4)
            End If
            If Me.LvVenta.ListItems.Count = 3 Then
                .Item("CODIGO_PRODUCTO2").Range.Text = Me.LvVenta.ListItems(2).SubItems(1)
                .Item("NOMBRE_PRODUCTO2").Range.Text = Me.LvVenta.ListItems(2).SubItems(2)
                .Item("PRECIO_PRODUCTO2").Range.Text = Me.LvVenta.ListItems(2).SubItems(4)
                .Item("CODIGO_PRODUCTO3").Range.Text = Me.LvVenta.ListItems(3).SubItems(1)
                .Item("NOMBRE_PRODUCTO3").Range.Text = Me.LvVenta.ListItems(3).SubItems(2)
                .Item("PRECIO_PRODUCTO3").Range.Text = Me.LvVenta.ListItems(3).SubItems(4)
            End If
            If Me.LvVenta.ListItems.Count = 4 Then
                .Item("CODIGO_PRODUCTO2").Range.Text = Me.LvVenta.ListItems(2).SubItems(1)
                .Item("NOMBRE_PRODUCTO2").Range.Text = Me.LvVenta.ListItems(2).SubItems(2)
                .Item("PRECIO_PRODUCTO2").Range.Text = Me.LvVenta.ListItems(2).SubItems(4)
                .Item("CODIGO_PRODUCTO3").Range.Text = Me.LvVenta.ListItems(3).SubItems(1)
                .Item("NOMBRE_PRODUCTO3").Range.Text = Me.LvVenta.ListItems(3).SubItems(2)
                .Item("PRECIO_PRODUCTO3").Range.Text = Me.LvVenta.ListItems(3).SubItems(4)
                .Item("CODIGO_PRODUCTO4").Range.Text = Me.LvVenta.ListItems(4).SubItems(1)
                .Item("NOMBRE_PRODUCTO4").Range.Text = Me.LvVenta.ListItems(4).SubItems(2)
                .Item("PRECIO_PRODUCTO4").Range.Text = Me.LvVenta.ListItems(4).SubItems(4)
            End If
            
            '.Item("MONTO").Range.Text = Me.txtTotal
            .Item("OBSERVACION").Range.Text = Me.TxtObservacion
            .Item("OBRA").Range.Text = Me.TxtObra
            .Item("TELEFONO").Range.Text = Me.txtFono
            .Item("NOMBRE_FIRMA").Range.Text = Me.TxtNombreFirma
            .Item("RUT_FIRMA").Range.Text = Me.TxtRutFirma
            '.Item("HORA").Range.Text = Time
            .Item("GARANTIA").Range.Text = Me.TxtGarantia
            .Item("FECHA_INICIO_CONTRATO").Range.Text = FormatDateTime(DtInicioC, vbLongDate)
            .Item("HORA_INICIO_CONTRATO").Range.Text = Me.Hora_Inicio
            .Item("ORDEN_DE_COMPRA").Range.Text = Me.TxtOC
            '.Item("ACCESORIOS").Range.Text = Me.TxtAccesorios
            .Item("NOMBRE_FIRMA_ANEXO").Range.Text = Me.TxtNombreFirma
            .Item("RUT_FIRMA_ANEXO").Range.Text = Me.TxtRutFirma
            .Item("NOMBRE_QUIEN_ENTREGA").Range.Text = Me.TxtNombreEntrega
            .Item("RUT_QUIEN_ENTREGA").Range.Text = Me.TxtRutEntrega
            .Item("NOMBRE_QUIEN_ENTREGA2").Range.Text = Me.TxtNombreEntrega
            .Item("RUT_QUIEN_ENTREGA2").Range.Text = Me.TxtRutEntrega
            
            
            If LvAccesorios.ListItems.Count > 0 Then .Item("ACC1").Range.Text = LvAccesorios.ListItems(1).SubItems(1)
            If LvAccesorios.ListItems.Count > 1 Then .Item("ACC2").Range.Text = LvAccesorios.ListItems(2).SubItems(1)
            If LvAccesorios.ListItems.Count > 2 Then .Item("ACC3").Range.Text = LvAccesorios.ListItems(3).SubItems(1)
            If LvAccesorios.ListItems.Count > 3 Then .Item("ACC4").Range.Text = LvAccesorios.ListItems(4).SubItems(1)
            If LvAccesorios.ListItems.Count > 4 Then .Item("ACC5").Range.Text = LvAccesorios.ListItems(5).SubItems(1)
            If LvAccesorios.ListItems.Count > 5 Then .Item("ACC6").Range.Text = LvAccesorios.ListItems(6).SubItems(1)
            If LvAccesorios.ListItems.Count > 6 Then .Item("ACC7").Range.Text = LvAccesorios.ListItems(7).SubItems(1)
            If LvAccesorios.ListItems.Count > 7 Then .Item("ACC8").Range.Text = LvAccesorios.ListItems(8).SubItems(1)
            If LvAccesorios.ListItems.Count > 8 Then .Item("ACC9").Range.Text = LvAccesorios.ListItems(9).SubItems(1)
            If LvAccesorios.ListItems.Count > 9 Then .Item("ACC10").Range.Text = LvAccesorios.ListItems(10).SubItems(1)
            If LvAccesorios.ListItems.Count > 10 Then .Item("ACC11").Range.Text = LvAccesorios.ListItems(11).SubItems(1)
            If LvAccesorios.ListItems.Count > 11 Then .Item("ACC12").Range.Text = LvAccesorios.ListItems(12).SubItems(1)
            If LvAccesorios.ListItems.Count > 12 Then .Item("ACC13").Range.Text = LvAccesorios.ListItems(13).SubItems(1)
            If LvAccesorios.ListItems.Count > 13 Then .Item("ACC14").Range.Text = LvAccesorios.ListItems(14).SubItems(1)
            If LvAccesorios.ListItems.Count > 14 Then .Item("ACC15").Range.Text = LvAccesorios.ListItems(15).SubItems(1)
            If LvAccesorios.ListItems.Count > 15 Then .Item("ACC16").Range.Text = LvAccesorios.ListItems(16).SubItems(1)
            
            .Item("NUMERO_CONTRATO_ANEXO").Range.Text = Lp_Id_Unico_Venta
            
  
        End With
        PlantillaCpraVta.Visible = True

End Sub

Private Sub DtInicioC_KeyPress(KeyAscii As Integer)
    On Error Resume Next
    If KeyAscii = 13 Then SendKeys ("{TAB}")
    If KeyAscii = 39 Then KeyAscii = 0
End Sub
Private Sub Form_KeyUp(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF5 Then
        If CmdF5.Enabled Then CmdF5_Click
    End If
    
    If KeyCode = vbKeyF8 Then
        CmdF8_Click
    End If
    
    If KeyCode = vbKeyF7 Then
        CmdF7_Click
    End If
    If KeyCode = vbKeyF9 Then
        CmdF9_Click
    End If
End Sub
Private Sub Form_Load()
    Aplicar_skin Me
    Centrar Me, False
    Dim ModificaLista As Integer
    ModificaLista = 0
    DtInicioC = Date
    Me.Hora_Inicio = Time
    If LogUsuario <> "KAROL" Then mn_edicion.Enabled = False
    
    LvVenta.ColumnHeaders(4).Width = 0
    LvVenta.ColumnHeaders(5).Width = 0
        LvVenta.ColumnHeaders(6).Width = 0
    LvVenta.ColumnHeaders(7).Width = 0
    'CboDocInidicio.ListIndex = 2
    sue_id = LogSueID
    
    Dim lista1 As Integer
        Sql = ""
    Sql = "SELECT lst_id, sue_id FROM sis_usuarios WHERE usu_login='" & LogUsuario & "'"
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
     lista1 = RsTmp!lst_id
     sue_id = RsTmp!sue_id
   End If
   
   ''Sacar Numero de Contrato
       Dim numcon As String
     numcon = IG_id_Sucursal_Empresa

    
  '' Me.Sknnumcontrato = numcon
   
   
   ''MASG 10-07-2017 NUEVA FORMA DE OBTENER NUMERO DE CONTRATO POR SUCURSAL TIPO DTE FOLIO
   Dim suc As String
           Sql = ""
    Sql = "SELECT sue_id FROM sis_usuarios WHERE usu_login='" & LogUsuario & "'"
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
     suc = RsTmp!sue_id
   End If
   
                       Sql = "SELECT contrato_id,contrato_numero numero " & _
                        "FROM dte_contrato " & _
                        "WHERE contrato_sucursal='" & suc & "' AND contrato_disponible='SI' " & _
                        "ORDER BY contrato_id " & _
                        "LIMIT 1"
                    Consulta RsTmp, Sql
                    TxtNroDocumento = 0
                    If RsTmp.RecordCount > 0 Then
                        TxtNroDocumento = RsTmp!Numero
                        Me.SkNnumcontrato = RsTmp!Numero
                        Lp_Id_Unico_Venta = RsTmp!Numero
                    Else
                     ' Me.SkFoliosDisponibles = "No quedan folios disponibles.."
                        MsgBox "Debe solicitar folios para contratos..."
                    End If
    ''FIN MASG 10-07-2017 NUEVA FORMA DE OBTENER NUMERO DE CONTRATO POR SUCURSAL TIPO DTE FOLIO
    
    LLenarCombo Me.CmbListaPrecios, "lst_nombre", "lst_id", "par_lista_precios", "sue_id=" & LogSueID
            If CmbListaPrecios.ListCount = 0 Then
          '  CmbListaPrecios.AddItem "X"
            CmbListaPrecios.ListIndex = lista1
        End If
    CmbListaPrecios.ListIndex = lista1 - 1
    
    If SG_Equipo_Solo_Nota_de_Venta = "SI" Then
        TxtPOS = "POS"
        LLenarCombo CboDocVenta, "doc_nombre", "doc_id", "sis_documentos", "doc_nota_de_venta='SI'"
        If CboDocVenta.ListCount = 0 Then
            CboDocVenta.AddItem "X"
            CboDocVenta.ListIndex = 0
        End If
        
        CboDocVenta.ListIndex = 0
        LLenarCombo CboDocInidicio, "doc_nombre", "doc_id", "sis_documentos", "doc_documento='VENTA'   AND doc_utiliza_en_pos='SI'", "doc_orden"

        
        CboDocInidicio.ListIndex = 1
    Else
        TxtPOS = "CAJA"
        LLenarCombo CboDocInidicio, "doc_nombre", "doc_id", "sis_documentos", "doc_documento='VENTA' AND doc_utiliza_en_pos='SI'", "doc_orden"
        CboDocInidicio.ListIndex = 0
    End If
    
    LLenarCombo CboVendedor, "ven_nombre", "ven_id", "par_vendedores", "ven_activo='SI' AND rut_emp='" & SP_Rut_Activo & "'"
    CboVendedor.ListIndex = 0
    

    
    Sql = "SELECT pde_permite,pde_descuento_maximo,pde_recargo_maximo " & _
                "FROM par_perfiles_atributo_descuento " & _
                "WHERE per_id=" & LogPerfil & " AND rut_emp='" & SP_Rut_Activo & "'"
    Consulta RsTmp, Sql
    
    Sm_PermiteDscto = "NO"
    If RsTmp.RecordCount > 0 Then
        If RsTmp!pde_permite = "SI" Then
                Im_Descuento_Maximo = RsTmp!pde_descuento_maximo
                Im_Recargo_Maximo = RsTmp!pde_recargo_maximo
        
        
              '  For i = 0 To RsTmp!pde_descuento_maximo
              '      CboDescuento.AddItem i
              '      CboDsctoTotal.AddItem i
              '  Next
                Sm_PermiteDscto = "SI"
        Else
            CboDescuento.Enabled = False
            CboDescuento.AddItem "0"
        End If
    Else
'        CboDescuento.Enabled = False
'        CboDescuento.AddItem "0"
    End If
    If Sm_PermiteDscto = "NO" Then
        Me.TxtDescuentoX100.Locked = True
        Me.TxtDsctoAjuste.Locked = True
        'Me.TxtDsctoTotal.Locked = True
        Me.TxtRecargoAjuste.Locked = True
    End If
    
   ' CboDescuento.ListIndex = 0
    Sm_PrecioVtaModificable = "NO"
    Sql = "SELECT  direccion,fono,email,   emp_precio_venta_modificable pv,emp_redondear_descuento,emp_cuenta_ventas,emp_utiliza_codigos_internos_productos,emp_de_repuestos " & _
            "FROM sis_empresas " & _
            "WHERE rut='" & SP_Rut_Activo & "'"
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        Sm_PrecioVtaModificable = RsTmp!PV
        Im_Redondear = RsTmp!emp_redondear_descuento
        Lm_CuentaVentas = RsTmp!emp_cuenta_ventas
        Sm_UtilizaCodigoInterno = RsTmp!emp_utiliza_codigos_internos_productos
        TxtEmpDireccion = RsTmp!direccion
        TxtEmpFono = RsTmp!fono
        TxtEmpMail = RsTmp!Email
        Sp_EmpresaRepuestos = RsTmp!emp_de_repuestos
        
    End If
    
'    If Sm_PrecioVtaModificable = "NO" Then
'        TxtPrecioFinal.BackColor = ClrDesha
'        TxtPrecioFinal.Locked = True
'    End If
    
    Sm_VentaRapida = "NO"
    Sm_Codigo_Barra_Pesable = "NO"
    Sql = "SELECT ema_venta_rapida,ema_codigos_barra_pesables,ema_imprime_ticket_nota_venta imp_ticket " & _
          "FROM sis_empresa_activa " & _
          "WHERE UPPER(ema_nombre_equipo)='" & UCase(SP_Nombre_Equipo) & "'"
    Consulta RsTmp2, Sql

    If RsTmp2.RecordCount > 0 Then
        Sm_VentaRapida = RsTmp2!ema_venta_rapida
        Sm_Codigo_Barra_Pesable = RsTmp2!ema_codigos_barra_pesables
        Sm_ImprimeTicketNv = RsTmp2!imp_ticket
    End If

    
    LvVenta.ColumnHeaders(2).Width = TxtCodigo.Width + 10
    LvVenta.ColumnHeaders(3).Width = TxtDescripcion.Width + 20
  '  LvVenta.ColumnHeaders(4).Width = TxtCantidad.Width + 10
    LvVenta.ColumnHeaders(5).Width = TxtPrecio.Width + 20
   ' LvVenta.ColumnHeaders(6).Width = TxtTotalLinea.Width + 10
   ' LvVenta.ColumnHeaders(7).Width = TxtStockLinea.Width + 10
    
    


    
    TxtNombreEntrega = Principal.TxtNombre
    
End Sub

Private Sub Hora_Inicio_KeyPress(KeyAscii As Integer)
    On Error Resume Next
    If KeyAscii = 13 Then SendKeys ("{TAB}")
    If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub LvAccesorios_DblClick()
    If LvAccesorios.SelectedItem Is Nothing Then Exit Sub
    LvAccesorios.ListItems.Remove LvAccesorios.SelectedItem.Index
End Sub


Private Sub LvVenta_DblClick()
    If LvVenta.SelectedItem Is Nothing Then Exit Sub
    With LvVenta
        TxtCodigo.Tag = LvVenta.SelectedItem
        TxtCodigo = .SelectedItem.SubItems(1)
        Me.TxtDescripcion = .SelectedItem.SubItems(2)
        TxtCantidad = .SelectedItem.SubItems(3)
        TxtPrecio = .SelectedItem.SubItems(4)
        TxtTotalLinea = .SelectedItem.SubItems(5)
        TxtStock = .SelectedItem.SubItems(6)
        TxtCodigo.SetFocus
        LvVenta.ListItems.Remove .SelectedItem.Index
    End With
    CalculaGrilla
    NormalizaColores
    TxtCodigo.SetFocus
     
End Sub

Private Sub mn_editacontrato_Click()
    sis_InputBox.Caption = "Editar contrato"
    sis_InputBox.FramBox = "Numero de contrato"
    sis_InputBox.texto.PasswordChar = ""
    sis_InputBox.Sm_TipoDato = "N"
    sis_InputBox.Show 1
    If Val(SG_codigo2) = 0 Then Exit Sub
    
    Sql = "SELECT * " & _
            "FROM ven_arriendo " & _
            "WHERE arr_id=" & SG_codigo2
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        Me.Tag = SG_codigo2
         TxtNroDocumento = SG_codigo2
        Me.SkNnumcontrato = SG_codigo2
        Lp_Id_Unico_Venta = SG_codigo2
        TxtRut = RsTmp!arr_rut_cliente
        TxtRazonSocial = "" & RsTmp!arr_nombre_cliente
        TxtGiro = "" & RsTmp!arr_giro_cliente
        txtComuna = "" & RsTmp!arr_comuna_cliente
        txtFono = "" & RsTmp!arr_fono_cliente
        TxtObra = "" & RsTmp!arr_direccion_obra
        TxtObservacion = "" & RsTmp!arr_observacion
        TxtNombreFirma = "" & RsTmp!arr_nombre_retira
        TxtRutFirma = "" & RsTmp!arr_rut_retira
        TxtNombreEntrega = "" & RsTmp!arr_nombre_digita
        TxtRutEntrega = "" & RsTmp!arr_rut_digita
        DtInicioC = "" & RsTmp!arr_fecha_inicio
        Hora_Inicio = "" & RsTmp!arr_hora_inicio
        TxtGarantia = "" & RsTmp!arr_garantia
        TxtOC = "" & RsTmp!arr_oc
        txtTotal = "" & RsTmp!arr_valor
     '   TxtPlazoEntrega = "" & RsTmp!arr_plazo_entrega
        
        If CboSucursal.ListCount > 0 Then CboSucursal.ListIndex = 0
    
    
    
        Sql = "SELECT det_arr_codigo,det_arr_codigo_interno,det_arr_descripcion,0 cant,det_arr_total tot2,det_arr_total tot1 " & _
                "FROM ven_arr_detalle " & _
                "WHERE arr_id =" & SG_codigo2
        Consulta RsTmp, Sql
        
        LLenar_Grilla RsTmp, Me, LvVenta, False, True, True, False
    End If
    
    
End Sub

Private Sub Timer1_Timer()
    On Error Resume Next
    TxtRut.SetFocus
    Timer1.Enabled = False
End Sub


Private Sub TxtAccesorio_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
    If KeyAscii = 13 Then CmdAccesorios_Click
End Sub


Private Sub TxtCantidad_GotFocus()
    En_Foco TxtCantidad
End Sub

Private Sub TxtCantidad_KeyPress(KeyAscii As Integer)
    KeyAscii = AceptaSoloNumeros(KeyAscii)
    If KeyAscii = 46 Then KeyAscii = 44
    If KeyAscii = 39 Then KeyAscii = 0
End Sub
Private Sub TxtCantidad_Validate(Cancel As Boolean)
 '   Dim Dp_Cantidad As decim
    SkUm.Tag = ""
    If Val(CxP(TxtCantidad)) > 0 And Val(TxtCodigo) > 0 Then
        If Sp_MatrizDescuentos = "SI" Then
            'solo plastcos aldunate
            
            Sql = "SELECT   mpr_precio_unitario " & _
                    "FROM  par_productos_matriz_descuento " & _
                    "WHERE pro_codigo=" & Val(TxtCodigo.Tag) & " AND " & CxP(TxtCantidad) & " BETWEEN mpr_desde AND mpr_hasta"
            Consulta RsTmp, Sql
            If RsTmp.RecordCount > 0 Then
                TxtCantidad.Tag = "SI"
                TxtPrecio.Tag = RsTmp!mpr_precio_unitario
                TxtPrecio = RsTmp!mpr_precio_unitario
            Else
                'No lleva descuento de la matriz.
                TxtCantidad.Tag = "NO"
                If Val(TxtDescripcion.Tag) > 0 Then
                    TxtPrecio.Tag = Val(TxtDescripcion.Tag)
                    TxtPrecio = Val(TxtDescripcion.Tag)
                End If
                
                'Verificaremos si lleva descuento por tipo
                
                Sql = "SELECT  dti_descuento " & _
                        "FROM par_descuento_por_tipo d " & _
                        "WHERE tip_id=(SELECT tip_id " & _
                                        "FROM maestro_productos " & _
                                        "WHERE codigo=" & Val(TxtCodigo.Tag) & " " & _
                                        "LIMIT 1) AND " & CxP(TxtCantidad) & ">=dti_desde"
                
                Consulta RsTmp, Sql
                If RsTmp.RecordCount > 0 Then
                    'Aplicar descuento por tipo
                    TxtPrecio = Val(CxP(CDbl(TxtPrecio.Tag))) - Round((CDbl(TxtPrecio.Tag) / 100 * RsTmp!dti_descuento), 0)
                    SkUm.Tag = TxtPrecio
                
                
                End If
                
                
                
            End If
            
        End If
    
    
        CalculaCantPrecio
    Else
      ''Dicoem TxtCantidad = "0"
    End If
End Sub
Private Sub CalculaCantPrecio()
    If Val(CxP(TxtCantidad)) > 0 Then
        TxtCantidad = Format(TxtCantidad, "#0.00")
        'Dp_Cantidad = TxtCantidad
        If SP_Rut_Activo = "76.337.408-4" Then
            TxtTotalLinea = Format(TxtCantidad * Val(TxtPrecio), "#.#0")
        Else
        
            TxtTotalLinea = NumFormat(TxtCantidad * CDbl(TxtPrecio))
        End If
       ' If SP_Rut_Activo = "78.967.170-2" Then
       '     TxtTotalLinea = NumFormat(TxtCantidad * Val(CxP(TxtPrecio)))
       ' End If
    End If
End Sub

Private Sub TxtCodigo_GotFocus()
    En_Foco TxtCodigo
End Sub


Private Sub TxtCodigo_KeyPress(KeyAscii As Integer)
   ' If SG_Codigos_Alfanumericos = "SI" Then
        KeyAscii = Asc(UCase(Chr(KeyAscii)))
    'Else
     '   KeyAscii = SoloNumeros(KeyAscii)
    'End If
    'I'f KeyAscii = 39 Then KeyAscii = 0
'
            On Error Resume Next
    If KeyAscii = 13 Then SendKeys ("{TAB}")
    If KeyAscii = 39 Then KeyAscii = 0
End Sub


Private Sub TxtCodigo_KeyUp(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF1 Then
                If Sp_EmpresaRepuestos = "SI" Then
                    SG_codigo = ""
                    SG_codigo2 = ""
                    Busca_Producto_Repuestos.Show 1
                    If Sm_UtilizaCodigoInterno = "SI" Then
                        TxtCodigo = SG_codigo2
                    Else
                        TxtCodigo = SG_codigo
                    End If
                    TxtCodigo_Validate True
                    TxtCantidad.SetFocus
                Else
                    BuscaProducto.Show 1
                    TxtCodigo = SG_codigo
                    TxtCodigo_Validate True
                End If
               
    Else
        If KeyCode = 107 Then
            TxtCantidad = Val(TxtCantidad) + 1
            
        ElseIf KeyCode = 109 Then
            TxtCantidad = Val(TxtCantidad) - 1
            If Val(TxtCantidad) = 0 Then TxtCantidad = 1
        End If
    End If
End Sub


Private Sub TxtCodigo_Validate(Cancel As Boolean)
    Dim Dp_Cantidad_Pesable As Double
    Dim Sp_PVenta As String
    
    If Len(TxtCodigo) = 0 Then Exit Sub
   
   
    If TxtCodigo = "OTRO" Then
        TxtDescripcion.Locked = False
        TxtDescripcion.BackColor = vbWhite
        TxtDescripcion.SetFocus
        Exit Sub
    Else
        TxtDescripcion.BackColor = &HE0E0E0
        TxtDescripcion.Locked = True
    End If
    Dim Dp_Total As Double
    Dim Ip_C As Integer
   
    Dim Dp_Unitario As Double
    Dim lista As String
    ModificaLista = CmbListaPrecios.ListIndex
    If ModificaLista = "" Then
            Sql = ""
            Sql = "SELECT lst_id FROM sis_usuarios WHERE usu_nombre='" & LogUsuario & "'"
            Consulta RsTmp, Sql
            If RsTmp.RecordCount > 0 Then
                lista = RsTmp!lst_id
            End If
    Else
            lista = ModificaLista + 1
    End If
    
    Sql = ""
    Filtro = "codigo = '" & TxtCodigo & "' "
    Dim co As String
    Sql = ""
    co = "0"
    Sql = "SELECT id codigo FROM maestro_productos WHERE pro_codigo_interno='" & TxtCodigo & "'"
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        co = RsTmp!Codigo
    End If
    
    
    Sp_PVenta = "IFNULL((SELECT lsd_precio FROM par_lista_precios_detalle  WHERE lst_id= " & lista & " and id = '" & co & "'  ),0) precio_venta,"
                     '"IFNULL((SELECT lst_id FROM sis_usuarios WHERE usu_nombre='" & LogUsuario & "'),0) precio_venta,"
        ' "IF((SELECT lst_id FROM sis_usuarios WHERE usu_nombre='" & LogUsuario & "')=0 " & _
        '                 "OR ISNULL((SELECT lst_id FROM sis_usuarios WHERE usu_nombre='" & LogUsuario & "')) ,precio_venta," & _
        '                "IFNULL((SELECT lsd_precio FROM par_lista_precios_detalle d WHERE lst_id=" & Val(Principal.SkSucursal.Tag) & " AND m.id=d.id),precio_venta)) precio_venta,"
    
'     Sp_PVenta = "IF((SELECT lst_id FROM maestro_clientes WHERE rut_cliente='" & TxtRut & "')=0 " & _
'                         "OR ISNULL((SELECT lst_id FROM maestro_clientes WHERE rut_cliente='" & TxtRut & "')) ,precio_venta," & _
'                        "IFNULL((SELECT lsd_precio FROM par_lista_precios_detalle d WHERE lst_id=" & Val(Principal.SkSucursal.Tag) & " AND m.id=d.id),precio_venta)) precio_venta,"

    
    If Sm_UtilizaCodigoInterno = "SI" Then
                Sp_FiltroCI = "pro_codigo_interno='" & TxtCodigo & "' "
                
'                If Sm_Codigo_Barra_Pesable = "SI" Then
'                    If Mid(TxtCodigo, 1, 2) = 26 Then 'UNITARIOS
'                        Sp_FiltroCI = "codigo=" & Val(Mid(TxtCodigo, 3, 5)) & " "
'                    ElseIf Mid(TxtCodigo, 1, 2) = 25 Then 'PESABLES
'                        Sp_FiltroCI = "codigo=" & Val(Mid(TxtCodigo, 3, 5)) & " "
'                        'Dp_Cantidad_Pesable = Mid(TxtCodigo, 8, 2) & "." & Mid(TxtCodigo, 10, 3)'
'                         TxtCantidad = Mid(TxtCodigo, 8, 2) & "." & Mid(TxtCodigo, 10, 3)
'                    End If
'                End If
                
              
'                If SP_Rut_Activo = "76.370.578-1" Then
'                    'Este codigo es especial para talentos aeropuerto, para tomar otra lista de precios
'                    If Val(Principal.SkSucursal.Tag) > 0 Then
'                        Sp_PVenta = "IFNULL((SELECT lsd_precio FROM par_lista_precios_detalle d WHERE m.id=d.id AND lst_id=" & Val(Principal.SkSucursal.Tag) & "),precio_venta) precio_venta,"
'                    End If
'                End If
                sql2 = "SELECT ume_id,id,pro_inventariable,descripcion,marca,ubicacion_bodega, " & _
                            Sp_PVenta & _
                        " IFNULL((SELECT AVG(pro_ultimo_precio_compra) FROM pro_stock s WHERE s.rut_emp='" & SP_Rut_Activo & "' AND s.pro_codigo=m.codigo),0) precio_compra, " & _
                        "IFNULL((SELECT SUM(sto_stock) FROM pro_stock WHERE rut_emp='" & SP_Rut_Activo & "' AND pro_codigo=m.codigo),0) stock,ubicacion_bodega,codigo,pro_estado,pro_dado_de_baja_motivo " & _
                         "FROM maestro_productos m " & _
                        "WHERE pro_activo='SI' AND m.rut_emp='" & SP_Rut_Activo & "' AND " & Sp_FiltroCI
    
                
    End If
   ' sql2 = ""
    'sql2 = Sql & "UNION SELECT ume_id,id, pro_inventariable,descripcion,marca,ubicacion_bodega, " & _
            Sp_PVenta & _
            "IFNULL((SELECT AVG(pro_ultimo_precio_compra) FROM pro_stock s WHERE s.rut_emp='" & SP_Rut_Activo & "' AND s.pro_codigo=m.codigo),0) precio_compra, " & _
            "IFNULL((SELECT SUM(sto_stock) FROM pro_stock WHERE rut_emp='" & SP_Rut_Activo & "' AND pro_codigo=m.codigo),0) stock,ubicacion_bodega,codigo,pro_estado,pro_dado_de_baja_motivo " & _
          "FROM maestro_productos m " & _
          "WHERE pro_activo='SI' AND  m.rut_emp='" & SP_Rut_Activo & "' AND  " & Filtro & " LIMIT 1"
    Consulta RsTmp, sql2
    If RsTmp.RecordCount > 0 Then
                
                If RsTmp!pro_estado = "DADO DE BAJA" Then
                    MsgBox "ARTICULO DADO DE BAJA" & vbNewLine & " Motivo:" & vbNewLine & RsTmp!pro_dado_de_baja_motivo
                    GoTo nopasanada
                End If
                
                
                Me.FrmPago.Visible = False
        'If Sm_PermiteDscto = "NO" And Sm_VentaRapida = "SI" Then
        'probar
 
                
                '
                Me.SkUm = "" & RsTmp!ume_id
                TxtCodigo.Tag = RsTmp!Codigo
                TxtDescripcion = RsTmp!Descripcion
                'If SP_Rut_Activo = "76.337.408-4" Then
                        
                 '       TxtPrecio.Tag = RsTmp!precio_venta
                 '       TxtPrecio = CxP(RsTmp!precio_venta)
                 '       TxtDescripcion.Tag = CxP(TxtPrecio.Tag)
                
                                
                'Else
               '
                        
                        TxtPrecio.Tag = RsTmp!precio_venta
                        TxtPrecio = RsTmp!precio_venta
                        TxtDescripcion.Tag = TxtPrecio.Tag
                'End If
                
               ' If SP_Rut_Activo = "76.337.408-4" Then
               '     TxtTotalLinea = Format(RsTmp!precio_venta, "#,#0")
               ' Else
               '     TxtTotalLinea = NumFormat(RsTmp!precio_venta)
               ' End If
                TxtStockLinea = CxP(RsTmp!stock)
                TxtStockLinea.Tag = CxP(RsTmp!precio_compra)
                
                If CDbl(TxtStockLinea) < 1 Then
                    TxtStockLinea.BackColor = vbRed
                    TxtStockLinea.ForeColor = vbGreen
                End If
                
                TxtUbicacion = RsTmp!ubicacion_bodega
                If Len(TxtUbicacion) > 0 Then SkUbicacion = "UBICACION: " & TxtUbicacion Else SkUbicacion = ""
                
                SkInventariable = RsTmp!pro_inventariable
                If Sm_VentaRapida = "SI" Then
                    CmdAceptaLinea_Click
                    Cancel = True
                End If
                
                
                '03/10/2016
                'Buscar ultimo ajuste de inventario, y mostrarlo en Label SkUltimoInventario.
                'SkUltimoInventario = ""
                'Sql = "SELECT tmi_fecha,tmi_cantidad " & _
                '        "FROM inv_toma_inventario_detalle d " & _
                '        "JOIN inv_toma_inventario i ON d.tmi_id=i.tmi_id " & _
                '        "WHERE rut_emp='" & SP_Rut_Activo & "' AND pro_codigo='" & TxtCodigo.Tag & "' AND tdi_estado='OK' " & _
                '        "ORDER BY tmi_fecha DESC " & _
                        "LIMIT 1"
               '' Consulta RsTmp, Sql
                'If RsTmp.RecordCount > 0 Then
                '    Me.SkUltimoInventario = RsTmp!tmi_fecha & " - Cant.:" & RsTmp!tmi_cantidad
                '
                'End If
                'TxtPrecio = Replace(TxtPrecio, ",", ".")
                
        
    Else
nopasanada:
        TxtCodigo.Tag = ""
        TxtDescripcion = ""
        TxtPrecio.Tag = ""
        TxtPrecio = ""
        TxtTotalLinea = ""
        TxtStockLinea = ""
        TxtStockLinea.Tag = ""
        TxtUbicacion = ""
        SkInventariable = ""
        Cancel = True
        
    End If
End Sub



Private Sub txtComuna_KeyPress(KeyAscii As Integer)
    On Error Resume Next
    If KeyAscii = 13 Then SendKeys ("{TAB}")
    If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtDescripcion_GotFocus()
    En_Foco TxtDescripcion
End Sub


Private Sub txtDescripcion_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
    If KeyAscii = 39 Then KeyAscii = 0
End Sub


Private Sub TxtDescuentoX100_GotFocus()
    En_Foco TxtDescuentoX100
End Sub

Private Sub TxtDescuentoX100_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
    If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtDescuentoX100_Validate(Cancel As Boolean)
    If Val(TxtDescuentoX100) = 0 Then
        TxtDescuentoX100 = "0"
        TxtValorDescuento = "0"
    End If
    If Val(TxtDescuentoX100) > Im_Descuento_Maximo Then
        MsgBox "Descuento sobrepasa el permitido..", vbInformation
        Cancel = True
        Exit Sub
    End If
        
    
    CalculaGrilla
End Sub



Private Sub TxtDsctoAjuste_GotFocus()
    En_Foco TxtDsctoAjuste
End Sub

Private Sub TxtDsctoAjuste_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
    If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtDsctoAjuste_Validate(Cancel As Boolean)
    If Val(TxtDsctoAjuste) = 0 Then TxtDsctoAjuste = "0"
    
    
    If SP_Rut_Activo = "76.169.962-8" Then
        If Val(TxtDsctoAjuste) > 100 Then
            MsgBox "Limite para este campo $100..", vbExclamation
            Cancel = True
            Exit Sub
        End If
    
    End If
    
    CalculaGrilla
End Sub

Private Sub TxtEmail_KeyPress(KeyAscii As Integer)
    On Error Resume Next
    If KeyAscii = 13 Then SendKeys ("{TAB}")
    If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtFono_KeyPress(KeyAscii As Integer)
    On Error Resume Next
    If KeyAscii = 13 Then SendKeys ("{TAB}")
    If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtGarantia_KeyPress(KeyAscii As Integer)
    On Error Resume Next
    If KeyAscii = 13 Then SendKeys ("{TAB}")
    If KeyAscii = 39 Then KeyAscii = 0
End Sub
Private Sub TxtGiro_KeyPress(KeyAscii As Integer)
    On Error Resume Next
    If KeyAscii = 13 Then SendKeys ("{TAB}")
    If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtNombreEntrega_KeyPress(KeyAscii As Integer)
    On Error Resume Next
    If KeyAscii = 13 Then SendKeys ("{TAB}")
    If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtNombreFirma_KeyPress(KeyAscii As Integer)
    On Error Resume Next
    If KeyAscii = 13 Then SendKeys ("{TAB}")
    If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtObra_KeyPress(KeyAscii As Integer)
    On Error Resume Next
    If KeyAscii = 13 Then SendKeys ("{TAB}")
    If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtObservacion_KeyPress(KeyAscii As Integer)
    On Error Resume Next
    If KeyAscii = 13 Then SendKeys ("{TAB}")
    If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtOC_KeyPress(KeyAscii As Integer)
    On Error Resume Next
    If KeyAscii = 13 Then SendKeys ("{TAB}")
    If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtPlazoEntrega_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub
Private Sub TxtPrecio_GotFocus()
    En_Foco TxtPrecio
End Sub
Private Sub TxtPrecio_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
    If KeyAscii = 39 Then KeyAscii = 0
        On Error Resume Next
    If KeyAscii = 13 Then SendKeys ("{TAB}")
    If KeyAscii = 39 Then KeyAscii = 0
End Sub
Private Sub TxtPrecio_Validate(Cancel As Boolean)
 '' TxtPrecio.Text = Replace(TxtPrecio.Text, ",", ".")
    If Val(TxtPrecio) = 0 Then
        TxtPrecio = 0
    Else
        If LogUsuario <> "LUIS" Then
        Else
          MsgBox "Usuario No Autorizado Para Realizar Descuentos", vbInformation
          Exit Sub
        End If
        If Val(SkUm.Tag) = 0 Then
      '      If CDbl(TxtPrecio) < (TxtPrecio.Tag - (CDbl(TxtPrecio.Tag) * 30 / 100)) Then
      '          MsgBox "Precio no valido... Descuento no puede ser mayor a un 30%", vbInformation
      '          TxtPrecio = NumFormat(TxtPrecio.Tag)
       '         Cancel = True
       '     End If
        Else
            'esto es si hay descuento por tipo de producto, plasticos aldunate
        '    If CDbl(TxtPrecio) < CDbl(SkUm.Tag) Then
        '        MsgBox "Precio no valido..", vbInformation
        '        TxtPrecio = NumFormat(TxtPrecio.Tag)
        '        Cancel = True
         '   End If
        
        End If
        CalculaCantPrecio
    End If
End Sub



Private Sub TxtRazonSocial_KeyPress(KeyAscii As Integer)
    On Error Resume Next
    If KeyAscii = 13 Then SendKeys ("{TAB}")
    If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtRecargoAjuste_GotFocus()
        En_Foco TxtRecargoAjuste
End Sub

Private Sub TxtRecargoAjuste_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
    If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtRecargoAjuste_Validate(Cancel As Boolean)
    If Val(TxtRecargoAjuste) = 0 Then TxtRecargoAjuste = "0"
    CalculaGrilla
End Sub

Private Sub TxtRUT_GotFocus()
    En_Foco TxtRut
End Sub

Private Sub TxtRut_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
    On Error Resume Next
    If KeyAscii = 13 Then SendKeys ("{TAB}")
    If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtRut_KeyUp(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF1 Then CmdBuscaCliente_Click
End Sub

Private Sub TxtRut_LostFocus()
    If Len(TxtRut.Text) = 0 Then
        Me.TxtRazonSocial.Text = ""
        Exit Sub
    End If
    If ClienteEncontrado Then
    Else       ' TxtRut.SetFocus
    End If
End Sub

Private Sub txtRut_Validate(Cancel As Boolean)
    TxtListaPrecio = "NORMAL"
    TxtListaPrecio.Tag = 0
    TxtRazonSocial.Tag = ""
    If Len(TxtRut.Text) = 0 Then Exit Sub
    If CboDocVenta.ListIndex = -1 Then
        MsgBox "Seleccione documento...", vbInformation
        On Error Resume Next
        CboDocVenta.SetFocus
        Exit Sub
    End If
    TxtRut.Text = Replace(TxtRut.Text, ".", "")
    TxtRut.Text = Replace(TxtRut.Text, "-", "")
    Respuesta = VerificaRut(TxtRut.Text, NuevoRut)
    
    ''TxtRut.Lo
  ''  TxtRut.Text = Replace(TxtRut.Text, ".", "")
  ''  TxtRut.Text = Replace(TxtRut.Text, "-", "")
  ''  Respuesta = VerificaRut_V2(TxtRut.Text, NuevoRut)
  ''  TxtRut = NuevoRut
   
    If Respuesta = True Then
        Me.TxtRut.Text = NuevoRut
            TxtRut.Text = Replace(TxtRut.Text, ",", ".")
   '' TxtRut.Text = Replace(TxtRut.Text, "-", "")
        'Buscar el cliente
        Sql = "SELECT rut_cliente,nombre_rsocial,giro,direccion,comuna,fono,email, " & _
                "IFNULL(lst_nombre,'LISTA PRECIO GENERAL') listaprecios,m.lst_id,ven_id,cli_monto_credito,cli_bloqueado_dicoem,cli_motivo_bloqueo " & _
              "FROM maestro_clientes m " & _
              "INNER JOIN par_asociacion_ruts  a ON m.rut_cliente=a.rut_cli " & _
              "LEFT JOIN par_lista_precios l USING(lst_id) " & _
              "WHERE habilitado='SI' AND a.rut_emp='" & SP_Rut_Activo & "' AND rut_cliente = '" & Me.TxtRut.Text & "'"
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
                'Cliente encontrado
            With RsTmp
                If !cli_bloqueado_dicoem = "SI" Then
                    MsgBox "EL CLIENTE " & vbNewLine & vbNewLine & !nombre_rsocial & vbNewLine & " est� bloqueado por el siguiente motivo:" & vbNewLine & vbNewLine & !cli_motivo_bloqueo
                    TxtRut = ""
                    Exit Sub
                End If
            
                TxtMontoCredito = !cli_monto_credito
                TxtListaPrecio = !ListaPrecios
                TxtListaPrecio.Tag = !lst_id
                TxtRut.Text = !rut_cliente
                TxtGiro = !giro
                TxtDireccion = !direccion
                txtComuna = !comuna
                txtFono = !fono
                TxtEmail = !Email
                TxtRazonSocial.Text = !nombre_rsocial
               
                ClienteEncontrado = True
              
                
                Sql = "SELECT a.lst_id,lst_nombre " & _
                      "FROM par_asociacion_lista_precios  a " & _
                      "INNER JOIN par_lista_precios l USING(lst_id) " & _
                      "WHERE l.rut_emp='" & SP_Rut_Activo & "' AND cli_rut='" & TxtRut & "'"
                Consulta RsTmp2, Sql
                TxtListaPrecio.Tag = !lst_id
                TxtListaPrecio = "LISTA DE PRECIOS PRINCIPAL"
                If RsTmp2.RecordCount > 0 Then
                    TxtListaPrecio = RsTmp2!lst_nombre
                    TxtListaPrecio.Tag = RsTmp2!lst_id
                End If
                
                If Val(TxtMontoCredito) > 0 Then
                    'Debemos consultar el saldo de credito disponible del cliente.
                    TxtCupoUtilizado = ConsultaSaldoCliente(TxtRut)
                    SkCupoCredito = NumFormat(Val(TxtMontoCredito) - Val(TxtCupoUtilizado))
                End If
                CboSucursal.Clear
                LLenarCombo CboSucursal, "CONCAT(suc_ciudad,' ',suc_direccion,' ',suc_contacto)", "suc_id", "par_sucursales", "suc_activo='SI' AND rut_cliente='" & TxtRut & "'", "suc_id"
                CboSucursal.AddItem "CASA MATRIZ"
                CboSucursal.ItemData(CboSucursal.ListCount - 1) = 0
                CboSucursal.ListIndex = CboSucursal.ListCount - 1
                
                
            End With
                
            If bm_SoloVistaDoc Then Exit Sub
                                
        
        Else
                'Cliente no existe aun �crearlo??
                Respuesta = MsgBox("Cliente no encontrado..." & Chr(13) & _
                "     �Desea Crear?    ", vbYesNo + vbQuestion)
                If Respuesta = 6 Then
                    'crear
                    SG_codigo = ""
                    AccionCliente = 4
                    AgregoCliente.TxtRut.Text = Me.TxtRut.Text
                    
                    AgregoCliente.TxtRut.Locked = True
                    ClienteEncontrado = False
                    AgregoCliente.Timer1.Enabled = True
            
                    AgregoCliente.Show 1
                    If ClienteEncontrado = True Then
                        txtRut_Validate False
                       
                        TxtCodigo.SetFocus
                    Else
                        txtRut_Validate True
                    End If
                Else
                    'volver al rut
                    ClienteEncontrado = False
                    Me.TxtRut.Text = ""
                    On Error Resume Next
                    SendKeys "+{TAB}" ' Shift TAB retrocede al control anterior segun el orden del tabindex
                End If
        
          
        End If
       
    Else
        Me.TxtRut.Text = ""
        On Error Resume Next
        SendKeys "+{TAB}" ' Shift TAB retrocede al control anterior segun el orden del tabindex
    End If
    Exit Sub
CancelaImpesionFacturacion:
    'No imprime
    Unload Me
End Sub



Private Sub TxtRutEntrega_KeyPress(KeyAscii As Integer)
''    KeyAscii = SoloNumeros(KeyAscii)
''    If KeyAscii = 39 Then KeyAscii = 0
''        On Error Resume Next
    If KeyAscii = 13 Then SendKeys ("{TAB}")
    If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtRutEntrega_Validate(Cancel As Boolean)
TxtRutEntrega.Text = Replace(TxtRutEntrega.Text, ".", "")
    TxtRutEntrega.Text = Replace(TxtRutEntrega.Text, "-", "")
    Respuesta = VerificaRut_V2(TxtRutEntrega.Text, NuevoRut)
    TxtRutEntrega = NuevoRut
    TxtRutEntrega.Text = Replace(TxtRutEntrega.Text, ",", ".")
End Sub

Private Sub TxtRutFirma_KeyPress(KeyAscii As Integer)
''    KeyAscii = SoloNumeros(KeyAscii)
''    If KeyAscii = 39 Then KeyAscii = 0
''        On Error Resume Next


    If KeyAscii = 13 Then SendKeys ("{TAB}")
    If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtRutFirma_Validate(Cancel As Boolean)
    TxtRutFirma.Text = Replace(TxtRutFirma.Text, ".", "")
    TxtRutFirma.Text = Replace(TxtRutFirma.Text, "-", "")
    Respuesta = VerificaRut_V2(TxtRutFirma.Text, NuevoRut)
    TxtRutFirma = NuevoRut
    TxtRutFirma.Text = Replace(TxtRutFirma.Text, ",", ".")
End Sub

Private Sub TxtTemp_GotFocus()
    En_Foco TxtTemp
End Sub

Private Sub TxtTemp_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
    If KeyAscii = 13 Then
        If Val(TxtTemp) <= CDbl(LvDetalle.ListItems(Val(TxtTemp.Tag)).SubItems(8)) Then
            MsgBox "No puede bajar el valor...", vbInformation
            TxtTemp = LvDetalle.ListItems(Val(TxtTemp.Tag)).SubItems(4)
        Else
            LvDetalle.ListItems(Val(TxtTemp.Tag)).SubItems(4) = NumFormat(TxtTemp)
            LvDetalle.ListItems(Val(TxtTemp.Tag)).SubItems(5) = CDbl(LvDetalle.ListItems(Val(TxtTemp.Tag)).SubItems(4)) * Val(LvDetalle.ListItems(Val(TxtTemp.Tag)).SubItems(2))
            LvDetalle.ListItems(Val(TxtTemp.Tag)).SubItems(12) = LvDetalle.ListItems(Val(TxtTemp.Tag)).SubItems(4)
            ElTotal
        End If
        TxtTemp.Visible = False
        
    End If
    If KeyAscii = 27 Then
        TxtTemp.Visible = False
    End If
    
End Sub

Private Sub TxtTemp_LostFocus()
    TxtTemp.Visible = False
End Sub

Public Sub ActualizaStock()
        Dim Dp_Promedio As Double
        With Me.LvVenta
            If .ListItems.Count > 0 Then
                For i = 1 To .ListItems.Count
                   
                    
                        If .ListItems(i).SubItems(7) = "SI" Then 'inventariable
                          
                              Sql = "UPDATE maestro_productos " & _
                                    "SET stock_Actual = stock_actual -" & .ListItems(i).SubItems(6) & _
                                    " WHERE  rut_emp='" & SP_Rut_Activo & "' AND  codigo='" & .ListItems(i) & "'"
                              cn.Execute Sql
                              
                         
                              Dp_Promedio = 0
                             
                              'Dp_Promedio = CostoAVG(.ListItems(i).SubItems(1), IG_id_Bodega_Ventas)
                              
                               Sql = "SELECT pro_precio_neto promedio " & _
                                        "FROM inv_kardex k " & _
                                        "WHERE pro_codigo='" & .ListItems(i) & "' AND rut_emp='" & SP_Rut_Activo & "' AND bod_id=" & IG_id_Bodega_Ventas & " " & _
                                        "ORDER BY kar_id DESC " & _
                                         "LIMIT 1 "
                              Consulta RsTmp, Sql
                              If RsTmp.RecordCount > 0 Then Dp_Promedio = RsTmp!promedio
                              
                              
                              
                           
                              KardexVenta Format(Date, "YYYY-MM-DD"), "SALIDA", _
                               CboDocInidicio.ItemData(CboDocInidicio.ListIndex), Val(Lp_Id_Nueva_Venta), IG_id_Bodega_Ventas, .ListItems(i), _
                               Val(.ListItems(i).SubItems(3)), "VENTA " & CboDocVenta.Text & " " & Lp_Id_Nueva_Venta, _
                              Dp_Promedio, Dp_Promedio * Val(.ListItems(i).SubItems(3)), Me.TxtRut.Text, Me.TxtRazonSocial, , , CboDocInidicio.ItemData(CboDocInidicio.ListIndex), , , , Lp_Id_Unico_Venta
                              '    Kardex Format(.ListItems(i).SubItems(14), "YYYY-MM-DD"), "SALIDA", _
                              CboDocVenta.ItemData(CboDocVenta.ListIndex), TxtNroDocumento, CboBodega.ItemData(CboBodega.ListIndex), .ListItems(i).SubItems(1), _
                              Val(.ListItems(i).SubItems(6)), "VENTA " & CboDocVenta.Text & " " & TxtNroDocumento, _
                              0, 0, Me.TxtRut.Text, Me.TxtRazonSocial, , , CboDocVenta.ItemData(CboDocVenta.ListIndex)
                        End If
                    
                Next
            End If
        End With
        
End Sub
Private Sub ProcImprimeTicket()
    Dim Cx As Double, Cy As Double, Sp_Fecha As String, Ip_Mes As Integer, Dp_S As Double, Sp_Letras As String
    Dim Sp_Neto As String * 12, Sp_IVA As String * 12, Sp_Total As String * 12
    Dim Sp_GuiasFacturadas As String
    
    Dim p_Codigo As String * 6
    Dim p_Cantidad As String * 7
    Dim p_UM As String * 4
    Dim p_Detalle As String * 40
    Dim p_Unitario As String * 9
    Dim p_Total As String * 9
    Dim p_Mes As String * 10
    Dim p_CiudadF As String * 20
    
    
    If Printer.DeviceName <> "TERMICA" Then
            '28 agosto 2015 _
        se imprime ticket para ser leido en caja
        For Each pr In Printers
            If pr.DeviceName = "TERMICA" Then
                Establecer_Impresora pr.DeviceName
                Set Printer = pr 'Cambiamos la impresora por defecto
                Exit For        ' a la tengamos configurada en los parametros
            End If               'para las FACTURAs
        Next
    End If
    On Error GoTo ProblemaImpresora
    Printer.FontName = "Arial"
    Printer.FontSize = 12
    Printer.FontBold = True
    Printer.FontItalic = False
    Printer.ScaleMode = 7
        
        Cx = 0.1 'horizontal
        Cy = 0.1 'vertical
    
    
    Dp_S = 0.1
   
    

    Printer.CurrentX = Cx
    
    pos = Printer.CurrentY
    Printer.Print "NOTA DE VENTA " & Lp_Id_Unico_Venta
    'Printer.CurrentY = POS + 0.3
    'Printer.CurrentX = Cx
    'POS = Printer.CurrentY
    'Printer.Print "NRO " & Lp_Id_Nueva_Venta
    '
    Printer.CurrentY = pos + 0.3
    Printer.CurrentX = Cx
    pos = Printer.CurrentY
    Printer.Print "-------------------------------------------"
    Printer.FontName = "Courier New"
    Printer.FontSize = 10
    Printer.FontBold = False
    Printer.ScaleMode = 7
    
    Printer.FontBold = True
    
    Printer.CurrentY = pos + 0.5
    Printer.CurrentX = Cx
    pos = Printer.CurrentY
    Printer.Print "DOC: " & Me.CboDocInidicio.Text
   
    Printer.CurrentY = pos + 0.3
    Printer.CurrentX = Cx
    pos = Printer.CurrentY
    Printer.Print "VEN:" & CboVendedor.Text
    
    Printer.CurrentY = pos + 0.3
    Printer.CurrentX = Cx
    pos = Printer.CurrentY
    Printer.Print "FECHA " & Date & " HORA:" & Time
    
    Printer.CurrentY = pos + 0.3
    Printer.CurrentX = Cx
    pos = Printer.CurrentY
    Printer.Print "-----[ DATOS CLIENTE ]-----"
    
    
     Printer.CurrentY = pos + 0.5
    Printer.CurrentX = Cx
    pos = Printer.CurrentY
    Printer.Print "CLIENTE: " & TxtRazonSocial
   
    Printer.CurrentY = pos + 0.3
    Printer.CurrentX = Cx
    pos = Printer.CurrentY
    Printer.Print "RUT    : " & TxtRut
    
    Printer.CurrentY = pos + 0.3
    Printer.CurrentX = Cx
    pos = Printer.CurrentY
    Printer.Print "---------------------------------"
    
   ' GoTo fin_impresion
   Printer.FontName = "Courier New"
    Printer.CurrentY = pos + 0.5
    pos = Printer.CurrentY
    For i = 1 To Me.LvVenta.ListItems.Count
        Printer.CurrentY = pos + 0.1
        p_Codigo = Me.LvVenta.ListItems(i).SubItems(1)
        LSet p_Cantidad = Me.LvVenta.ListItems(i).SubItems(3)

        p_Detalle = Me.LvVenta.ListItems(i).SubItems(2)
        LSet p_Unitario = Me.LvVenta.ListItems(i).SubItems(4)
        LSet p_Total = Me.LvVenta.ListItems(i).SubItems(5)
        Printer.CurrentX = Cx
        Printer.Print "COD.:" & p_Codigo & "    CANT.:" & p_Cantidad
        Printer.CurrentX = Cx
        Printer.Print "P.U.:" & p_Unitario & " TOTAL:" & p_Total
        Printer.CurrentX = Cx
        Printer.Print p_Detalle
        pos = Printer.CurrentY
    Next
    Printer.CurrentY = pos + 0.3
    Printer.CurrentX = Cx
    pos = Printer.CurrentY
    Printer.Print "------------------------------------"
    
    Printer.FontBold = True
    Printer.FontSize = 14
    Printer.CurrentY = pos + 0.3
    Printer.CurrentX = Cx
    pos = Printer.CurrentY
    Printer.Print "TOTAL:" & txtTotal
    
    Printer.FontBold = False
    Printer.FontSize = 10
    
    Printer.CurrentY = pos + 0.3
    Printer.CurrentX = Cx
    pos = Printer.CurrentY
    Printer.Print "-----------------------"
    
    
    Printer.CurrentY = pos + 0.3
    Printer.CurrentX = Cx
    Printer.FontName = "C39HrP48DhTt" ' codigo de barras
    Printer.FontSize = 40
    SP_Ean = Right("0000000000000" & Lp_Id_Unico_Venta, 15)
    TxtBarCode = Lp_Id_Unico_Venta ' SP_Ean
    Printer.Print "    "; "*" & TxtBarCode & "*"
    

     
fin_impresion:
    
    Printer.NewPage
    Printer.EndDoc
    Exit Sub
ProblemaImpresora:
    MsgBox "Ocurrio un error al intentar imprimir..." & vbNewLine & Err.Description & vbNewLine & Err.Number

End Sub



Private Sub ProcCOTIZACION()
    Dim Cx As Double, Cy As Double, Sp_Fecha As String, Ip_Mes As Integer, Dp_S As Double, Sp_Letras As String
    Dim Sp_Neto As String * 12, Sp_IVA As String * 12, Sp_Total As String * 12
    Dim Sp_GuiasFacturadas As String
    
    Dim p_Codigo As String * 6
    Dim p_Cantidad As String * 7
    Dim p_UM As String * 4
    Dim p_Detalle As String * 40
    Dim p_Unitario As String * 9
    Dim p_Total As String * 9
    Dim p_Mes As String * 10
    Dim p_CiudadF As String * 20
    
        '28 agosto 2015 _
    se imprime ticket para ser leido en caja
    
    
    If Printer.DeviceName <> "TERMICA" Then
        For Each pr In Printers
            If pr.DeviceName = "TERMICA" Then
                Establecer_Impresora pr.DeviceName
                Set Printer = pr 'Cambiamos la impresora por defecto
                Exit For        ' a la tengamos configurada en los parametros
            End If               'para las FACTURAs
        Next
    End If
    On Error GoTo ProblemaImpresora
        Cx = 0.1 'horizontal
        Cy = 0.1 'vertical
    
    Printer.FontName = "Arial"
    Printer.FontSize = 12
    Printer.FontBold = True
    Printer.FontItalic = False
    Printer.ScaleMode = 7
    
    
    Printer.CurrentX = Cx
    Printer.CurrentY = Cy
    
    Printer.PaintPicture LoadPicture(App.Path & "\REDMAROK.jpg"), Cx + 0.62, 1
    
    pos = Printer.CurrentY + 2.5
        
    Dp_S = 0.1
   
    

    Printer.CurrentX = Cx
    
    
    
    If SP_Rut_Activo = "76.005.337-6" Then
        
            'gempp
            
            Printer.CurrentY = pos + 0.1
            pos = Printer.CurrentY
            Printer.Print "       RUT: 76.005.337-6"
            pos = Printer.CurrentY
            Printer.CurrentY = pos + 0.5
            
            
    End If
    
    
    
       ' Printer.CurrentY = POS + 0.3
    Printer.CurrentY = pos
    Printer.Print "     " & TxtEmpDireccion
    pos = Printer.CurrentY
    Printer.CurrentY = pos + 0.3
    
     Printer.CurrentY = pos + 0.1
    pos = Printer.CurrentY
    Printer.Print "          FONO:" & TxtEmpFono
    pos = Printer.CurrentY
    Printer.CurrentY = pos + 0.3

    


    Printer.CurrentY = pos + 0.3
    pos = Printer.CurrentY
    Printer.Print "email:" & TxtEmpMail
    'Printer.CurrentY = pos + 0.5

    Printer.CurrentY = pos + 1
        pos = Printer.CurrentY
    Printer.Print "           COTIZACION NRO " & Lp_Id_Unico_Venta
    Printer.CurrentY = pos + 0.3
    'Printer.CurrentX = Cx
    'POS = Printer.CurrentY
    'Printer.Print "NRO " & Lp_Id_Nueva_Venta
    '
    Printer.CurrentY = pos + 0.3
    Printer.CurrentX = Cx
    pos = Printer.CurrentY
    Printer.Print "----------------------------------------------------"
    Printer.FontName = "Courier New"
    Printer.FontSize = 10
    Printer.FontBold = False
    Printer.ScaleMode = 7
    
    
'    Printer.CurrentY = pos + 0.5
'    Printer.CurrentX = Cx
'    pos = Printer.CurrentY
'    Printer.Print "DOC: BOLETA FISCAL"
   
    Printer.CurrentY = pos + 0.3
    Printer.CurrentX = Cx
    pos = Printer.CurrentY
    Printer.Print "VEN:" & Me.CboVendedor.Text
    
    Printer.CurrentY = pos + 0.3
    Printer.CurrentX = Cx
    pos = Printer.CurrentY
    Printer.Print "FECHA " & Date & " HORA:" & Time
    
    Printer.CurrentY = pos + 0.5
    Printer.CurrentX = Cx
    pos = Printer.CurrentY
    Printer.Print "----[ DATOS CLIENTE ]-----"
    
    Printer.CurrentY = pos + 0.3
    Printer.CurrentX = Cx
    pos = Printer.CurrentY
    Printer.Print "CLIENTE:" & Me.TxtRazonSocial
    
    Printer.CurrentY = pos + 0.3
    Printer.CurrentX = Cx
    pos = Printer.CurrentY
    Printer.Print "RUT    : " & Me.TxtRut
    
    Printer.CurrentY = pos + 0.3
    Printer.CurrentX = Cx
    pos = Printer.CurrentY
    Printer.Print "DIRECC.:" & Me.TxtDireccion
    
    Printer.CurrentY = pos + 0.3
    Printer.CurrentX = Cx
    pos = Printer.CurrentY
    Printer.Print "GIRO   : " & Me.TxtGiro
    
    
    Printer.CurrentY = pos + 0.5
    Printer.CurrentX = Cx
    
    
    
    
    Printer.Print "----------------------------------------------------"
   ' GoTo fin_impresion
   Printer.FontName = "Courier New"
    Printer.CurrentY = pos + 0.6
    pos = Printer.CurrentY
    For i = 1 To Me.LvVenta.ListItems.Count
        Printer.CurrentY = pos + 0.3
        p_Codigo = Me.LvVenta.ListItems(i).SubItems(1)
        LSet p_Cantidad = Me.LvVenta.ListItems(i).SubItems(3)

        p_Detalle = Me.LvVenta.ListItems(i).SubItems(2)
        LSet p_Unitario = Me.LvVenta.ListItems(i).SubItems(4)
        LSet p_Total = Me.LvVenta.ListItems(i).SubItems(5)
        Printer.CurrentX = Cx
        Printer.Print "COD.:" & p_Codigo & "    CANT.:" & p_Cantidad
        Printer.CurrentX = Cx
        Printer.Print "P.U.:" & p_Unitario & " TOTAL:" & p_Total
        Printer.CurrentX = Cx
        Printer.Print p_Detalle
        pos = Printer.CurrentY
    Next
    Printer.CurrentY = pos + 0.5
    Printer.CurrentX = Cx
    pos = Printer.CurrentY
    Printer.Print "------------------------------------"
    
    Printer.FontBold = True
    Printer.FontSize = 14
    Printer.CurrentY = pos + 0.3
    Printer.CurrentX = Cx
    pos = Printer.CurrentY
    Printer.Print "        TOTAL:" & txtTotal
    
    Printer.FontBold = False
    Printer.FontSize = 10
    
    Printer.CurrentY = pos + 0.3
    Printer.CurrentX = Cx
    pos = Printer.CurrentY
    Printer.Print "---------------------------------------------"
    
    Printer.CurrentY = pos + 0.3
    Printer.CurrentX = Cx
    pos = Printer.CurrentY
    Printer.Print "Validez: 3 dias."
'    Printer.CurrentY = pos + 0.3
'    Printer.CurrentX = Cx
'    Printer.FontName = "C39HrP48DhTt" ' codigo de barras
'    Printer.FontSize = 40
'    SP_Ean = Right("0000000000000" & Lp_Id_Unico_Venta, 15)
'    TxtBarCode = Lp_Id_Unico_Venta ' SP_Ean
'    Printer.Print "    "; "*" & TxtBarCode & "*"
    

     
fin_impresion:
    
    Printer.NewPage
    Printer.EndDoc
    Exit Sub
ProblemaImpresora:
    MsgBox "Ocurrio un error al intentar imprimir..." & vbNewLine & Err.Description & vbNewLine & Err.Number

End Sub


Private Sub ImprimeCOTIZACION()
    Dim Cx As Double, Cy As Double, Sp_Fecha As String, Ip_Mes As Integer, Dp_S As Double, Sp_Letras As String
    Dim Sp_Neto As String * 12, Sp_IVA As String * 12
    
    Dim Sp_Nro_Comprobante As String
    
    Dim Sp_Empresa As String
    Dim Sp_Giro As String
    Dim Sp_Direccion As String
    Dim Sp_Fono As String * 12
    Dim Sp_Mail As String
    Dim Sp_Ciudad As String * 17
    Dim Sp_Comuna As String * 17
    
    
    Dim PSp_Empresa As String
    Dim PSp_Giro As String
    Dim PSp_Direccion As String
    Dim PSp_Fono As String
    Dim PSp_Mail As String
    Dim PSp_Ciudad As String
    Dim PSp_Comuna As String
    Dim PSp_Sucursal As String
    
    
    'Variables para detalle de articulos
    Dim Sp_CodigoInt As String * 13
    Dim Sp_Descripcin As String * 42
    Dim Sp_Cod_Acalde As String * 6
    Dim Sp_PU As String * 11
    Dim Sp_Cant As String * 7
    Dim Sp_UM As String * 3
    Dim Sp_TotalL As String * 12
    
    Dim sp_AExento As String * 15
    Dim Sp_ANeto As String * 15
    Dim Sp_AIva As String * 15
    Dim Sp_ATotal As String * 15
    
    
    
    
    
    Dim Sp_Nombre As String * 45
    Dim Sp_Rut As String * 15
    Dim Sp_Concepto As String
    
    Dim Sp_Nro_Doc As String * 12
    Dim Sp_Documento As String * 35
    Dim Sp_Valor As String * 15
    Dim Sp_Saldo As String * 15
    Dim Sp_Fpago As String * 24
    Dim Sp_Total As String * 12
    
    Dim Sp_Banco As String * 20
    Dim Sp_NroCheque As String * 10
    Dim Sp_FechaCheque As String * 10
    Dim Sp_ValorCheque As String * 15
    Dim Sp_SumaCheque As String * 15
    
    
    
    
    
    
    Dim Sp_Observacion As String * 80
    
    On Error GoTo ERRORIMPRESION
    Sql = "SELECT giro,direccion,ciudad,fono,email,comuna " & _
         "FROM sis_empresas " & _
         "WHERE rut='" & SP_Rut_Activo & "'"
    Consulta RsTmp2, Sql
    If RsTmp2.RecordCount > 0 Then
        Sp_Giro = RsTmp2!giro
        Sp_Direccion = RsTmp2!direccion
        Sp_Fono = RsTmp2!fono
        Sp_Mail = RsTmp2!Email
        Sp_Ciudad = RsTmp2!ciudad
        Sp_Comuna = RsTmp2!comuna
    End If
            
    Sql = "SELECT direccion,ciudad,fono,email,comuna,ciudad " & _
         "FROM maestro_clientes " & _
         "WHERE rut_cliente='" & TxtRut & "'"
    Consulta RsTmp2, Sql
    If RsTmp2.RecordCount > 0 Then
        PSp_Direccion = Me.TxtDireccion
        PSp_Fono = Mid(RsTmp2!fono, 1, 10)
        PSp_Mail = Trim(RsTmp2!Email)
        PSp_Ciudad = Mid(RsTmp2!ciudad, 1, 20)
        PSp_Comuna = Mid(RsTmp2!comuna, 1, 20)
    End If
 '   PSp_Sucursal = CboSucursal.Text
            
            
    Printer.FontName = "Courier New"
    Printer.FontSize = 10
    Printer.ScaleMode = 7
    Cx = 2
    Cy = 3
    Dp_S = 0.1
    
    
    Printer.CurrentX = Cx
    Printer.CurrentY = Cy
    Printer.PaintPicture LoadPicture(App.Path & "\REDMAROK.jpg"), Cx + 0.62, 1
    
    
    Printer.CurrentY = Printer.CurrentY + Dp_S
    
    Printer.FontSize = 18
    Printer.FontBold = True
    
    Printer.Print "           COTIZACION  NRO " & Lp_Id_Unico_Venta
    Printer.CurrentY = Printer.CurrentY + Dp_S + Dp_S
            
    Printer.FontSize = 12
    Printer.CurrentX = Cx
    Printer.Print SP_Empresa_Activa
    
    
    Printer.CurrentX = Cx
    Printer.CurrentY = Printer.CurrentY + Dp_S
  
    Printer.FontSize = 12
  
    pos = Printer.CurrentY
    Printer.Print "R.U.T.   :" & SP_Rut_Activo
    Printer.CurrentY = pos
    Printer.CurrentX = Cx + 10
    Printer.Print "FECHA:" & Date
    
    Printer.CurrentY = Printer.CurrentY + Dp_S
    
    Printer.FontBold = False
    Printer.FontSize = 12
    Printer.CurrentX = Cx
    Printer.Print "GIRO     :" & Sp_Giro
    Printer.CurrentY = Printer.CurrentY + Dp_S
    
    Printer.FontSize = 12
    Printer.CurrentX = Cx
    Printer.Print "DIRECCION:" & Sp_Direccion
    Printer.CurrentY = Printer.CurrentY + Dp_S
    
    'email
    Printer.FontSize = 12
    Printer.CurrentX = Cx
    Printer.Print "EMAIL    :" & Sp_Mail
    Printer.CurrentY = Printer.CurrentY + Dp_S
    
    
    Printer.FontSize = 12
    Printer.CurrentX = Cx
    Printer.Print "CIUDAD   :" & Sp_Ciudad & " COMUNA:" & Sp_Comuna & " FONO:" & Sp_Fono
    Printer.CurrentY = Printer.CurrentY + Dp_S + (Dp_S * 3)
    
    'FIN SECCION EMPRESA
    
    
    
    'AHORA SECCION CLIENTE
    'Printer.FontSize = 10
    pos = Printer.CurrentY
    Printer.CurrentX = Cx
    Printer.Print "SE�ORES  :" & TxtRazonSocial
    Printer.CurrentY = Printer.CurrentY + Dp_S
    
    
    
    Printer.CurrentX = Cx
    pos = Printer.CurrentY
    Printer.Print "RUT      :" & TxtRut
    
    Printer.CurrentY = pos
    Printer.CurrentX = Cx + 8
    Printer.Print "CONDICION DE PAGO:" ' & Me.CboFpago.Text & " " & Mid(CboPlazos.Text, 1, 25)
    
    Printer.CurrentY = Printer.CurrentY + Dp_S
    
    
    
    
    Printer.CurrentX = Cx
    Printer.Print "DIRECCION:" & PSp_Direccion
    Printer.CurrentY = Printer.CurrentY + Dp_S
    
    Printer.CurrentX = Cx
    Printer.Print "EMAIL    :" & PSp_Mail
    Printer.CurrentY = Printer.CurrentY + Dp_S
        
    Printer.CurrentX = Cx
    Printer.Print "CIUDAD   :" & PSp_Ciudad & "    COMUNA:" & PSp_Comuna & "    FONO:" & PSp_Fono
    Printer.CurrentY = Printer.CurrentY + Dp_S + Dp_S
    
    Printer.CurrentX = Cx
    Printer.Print "SUCURSAL   :" & PSp_Sucursal
    Printer.CurrentY = Printer.CurrentY + Dp_S + (Dp_S * 3)
    
    
    'Fin seccion CLIENTE
    
    
    
    'Inicio SECCION DETALLE DE ARTICULOS
            With LvVenta
                If .ListItems.Count > 0 Then
                    Printer.CurrentX = Cx
                    Printer.Print "Detalle de Art�culos"
                    Printer.CurrentY = Printer.CurrentY + Dp_S
                    Sp_CodigoInt = "Cod.Int."
                    Sp_Descripcin = "Descripcion"
                    RSet Sp_PU = "Pre. Unitario"
                    RSet Sp_Cant = "Cant."
                    RSet Sp_UM = "      "
                    RSet Sp_TotalL = "Total"
                    
                    Printer.FontSize = 10
                    Printer.FontBold = False
                    
                    Printer.CurrentX = Cx
                    If SP_Rut_Activo = "78.967.170-2" Then
                        Printer.Print Mid(Sp_CodigoInt, 1, 6) & "  " & Sp_Cant & " " & Sp_Descripcin & "  " & Sp_PU & " " & Sp_TotalL
                    Else
                    
                        Printer.Print Mid(Sp_CodigoInt, 1, 6) & " " & Sp_Descripcin & " " & Sp_PU & " " & Sp_Cant & " " & Sp_TotalL
                    End If
                    Printer.CurrentY = Printer.CurrentY + Dp_S
                    
                    For i = 1 To .ListItems.Count
                        Sp_CodigoInt = .ListItems(i).SubItems(1)
                        If SP_Rut_Activo = "76.169.962-8" Or SP_Rut_Activo = "76.553.302-3" Or SP_Rut_Activo = "78.967.170-2" Then
                            Sp_Cod_Acalde = .ListItems(i).SubItems(1)
                        End If
                        Sp_Descripcin = .ListItems(i).SubItems(2)
                        RSet Sp_PU = .ListItems(i).SubItems(4)
                        If SP_Rut_Activo = "76.553.302-3" Then
                             RSet Sp_Cant = Round(CDbl(.ListItems(i).SubItems(3)), 0)
                        Else
                            RSet Sp_Cant = .ListItems(i).SubItems(3)
                        End If
                        If SP_Rut_Activo = "78.967.170-2" Then
                        
                            RSet Sp_UM = .ListItems(i).SubItems(10) 'DSCTO
                        End If
                        
                      '  if
                        RSet Sp_TotalL = .ListItems(i).SubItems(5)
                        Printer.CurrentX = Cx
                        
                        If SP_Rut_Activo = "76.169.962-8" Or SP_Rut_Activo = "76.553.302-3" Then
                            Printer.Print Sp_Cod_Acalde & " " & Sp_Descripcin & " " & Sp_PU & " " & Sp_Cant & " " & Sp_TotalL
                        ElseIf SP_Rut_Activo = "78.967.170-2" Then
                            'Plasticos alduante, lleva Unidad de medida
                            Printer.Print Sp_Cod_Acalde & "- " & Sp_Cant & " " & Sp_UM & " " & Mid(Sp_Descripcin, 1, 39) & " " & Sp_PU & " " & Sp_TotalL
                        ElseIf SP_Rut_Activo = "76.239.518-5" Then
                            Printer.Print Mid(Sp_CodigoInt, 1, 7) & " " & Sp_Descripcin & " " & Sp_PU & " " & Sp_Cant & " " & Sp_TotalL
                            
                        Else
                            Printer.Print Sp_CodigoInt & " " & Sp_Descripcin & " " & Sp_PU & " " & Sp_Cant & " " & Sp_TotalL
                        End If
                        
                        Printer.CurrentY = Printer.CurrentY + Dp_S - 0.2
                    Next
                    Printer.CurrentY = Printer.CurrentY + Dp_S
                    posdetalle = Printer.CurrentY
                End If
            End With
    'FIN SECCION DETALLE DE ARTICULOS
    Printer.CurrentX = Cx
    Printer.FontBold = True
    
    
                 'Totales
    Dp_Total = CDbl(txtTotal)
    Dp_Neto = Round(Dp_Total / Val("1." & DG_IVA), 0)
    Dp_Iva = Dp_Total - Dp_Neto
    
    
    
    'Dim Sp_ANeto As String * 15
    'Dim Sp_AIva As String * 15
    'Dim Sp_ATotal As String * 15
  '  RSet sp_AExento = TxtExentos
    RSet Sp_ANeto = NumFormat(Dp_Neto)
    RSet Sp_AIva = NumFormat(Dp_Iva)
    RSet Sp_ATotal = txtTotal
    
   ' RSet sp_AExento = TxtExentos
    'RSet Sp_ANeto = TxtNeto
    'RSet Sp_AIva = TxtIva
   ' RSet Sp_ATotal = txtTotal
    
  '  Printer.CurrentX = Cx + 12
    If SP_Rut_Activo = "78.967.170-2" Then
            tac = Printer.FontSize
            Printer.FontBold = False
            Printer.FontSize = 8
            posx = Printer.CurrentX
            Printer.Print "Validez: 7 dias, Valores con IVA incluido, "
            Printer.CurrentX = posx
            Printer.Print TxtPlazoEntrega
            Printer.FontSize = tac
  
    End If
    Printer.CurrentY = Printer.CurrentY + Dp_S + Dp_S + Dp_S
  '  pos = Printer.CurrentY
  '  Printer.Print "E X E N T O:"
  '  Printer.CurrentY = pos
  '  Printer.CurrentX = Cx + 14.5
  '  Printer.Print sp_AExento
    If CDbl(TxtValorDescuento) + CDbl(TxtDsctoAjuste) - CDbl(TxtRecargoAjuste) > 0 Then
    
        Printer.CurrentX = Cx + 12
       Printer.CurrentY = Printer.CurrentY + Dp_S + Dp_S + Dp_S
        pos = Printer.CurrentY
        
        Printer.FontBold = True
       Printer.Print "D E S C U E N T O:"
        Printer.CurrentY = pos
        Printer.CurrentX = Cx + 14.5
        RSet sp_AExento = NumFormat(CDbl(TxtValorDescuento) + CDbl(TxtDsctoAjuste) - CDbl(TxtRecargoAjuste))
        Printer.Print sp_AExento
        
    End If
  
  
  
  
    
    Printer.CurrentX = Cx + 12
    Printer.CurrentY = Printer.CurrentY + Dp_S - 0.2
    pos = Printer.CurrentY
    Printer.Print "N E T O    :"
    Printer.CurrentY = pos
    Printer.CurrentX = Cx + 14.5
    Printer.Print Sp_ANeto
    
    Printer.CurrentX = Cx + 12
    Printer.CurrentY = Printer.CurrentY + Dp_S - 0.2
    pos = Printer.CurrentY
    Printer.Print "I.V.A.     :"
    Printer.CurrentY = pos
    Printer.CurrentX = Cx + 14.5
    Printer.Print Sp_AIva
    
   
    Printer.CurrentX = Cx + 12
    Printer.CurrentY = Printer.CurrentY + Dp_S - 0.2
    pos = Printer.CurrentY
    Printer.Print "T O T A L  :"
    Printer.CurrentY = pos
    Printer.CurrentX = Cx + 14.5
    Printer.Print Sp_ATotal
    
    Printer.FontBold = False
        
   ' Printer.DrawMode = 1
    Printer.DrawWidth = 3
    
    Printer.Line (1.2, 4)-(19, 1), , B  'Rectangulo Encabezado y N� Nota de Venta
    Printer.Line (1.2, 4)-(19, 7.7), , B  'Datos de la Empresa
    Printer.Line (1.2, 4)-(19, 11.2), , B 'Datos del Proveedor
    Printer.Line (1.2, 4)-(19, posdetalle), , B 'Detalle de Nota de Vena
    Printer.Line (1.2, posdetalle)-(19, posdetalle + 2.3), , B 'Detalle del Exento,Neto,Iva,Total
       
    Printer.NewPage
    Printer.EndDoc
    Exit Sub
ERRORIMPRESION:
    MsgBox Err.Number & vbNewLine & Err.Description
End Sub

Private Sub PrevisualizaCotizacion()
 Dim Cx As Double, Cy As Double, Sp_Fecha As String, Ip_Mes As Integer, Dp_S As Double, Sp_Letras As String
    Dim Sp_Neto As String * 12, Sp_IVA As String * 12
    
    Dim Sp_Nro_Comprobante As String
    
    Dim Sp_Empresa As String
    Dim Sp_Giro As String
    Dim Sp_Direccion As String
    Dim Sp_Fono As String * 12
    Dim Sp_Mail As String
    Dim Sp_Ciudad As String * 17
    Dim Sp_Comuna As String * 17
    
    
    Dim PSp_Empresa As String
    Dim PSp_Giro As String
    Dim PSp_Direccion As String
    Dim PSp_Fono As String
    Dim PSp_Mail As String
    Dim PSp_Ciudad As String
    Dim PSp_Comuna As String
    Dim PSp_Sucursal As String
    
    
    'Variables para detalle de articulos
    Dim Sp_CodigoInt As String * 13
    Dim Sp_Descripcin As String * 42
    Dim Sp_Cod_Acalde As String * 6
    Dim Sp_PU As String * 11
    Dim Sp_Cant As String * 7
    Dim Sp_UM As String * 8
    Dim Sp_TotalL As String * 12
    
    Dim sp_AExento As String * 15
    Dim Sp_ANeto As String * 15
    Dim Sp_AIva As String * 15
    Dim Sp_ATotal As String * 15
    
    
    
    
    
    Dim Sp_Nombre As String * 45
    Dim Sp_Rut As String * 15
    Dim Sp_Concepto As String
    
    Dim Sp_Nro_Doc As String * 12
    Dim Sp_Documento As String * 35
    Dim Sp_Valor As String * 15
    Dim Sp_Saldo As String * 15
    Dim Sp_Fpago As String * 24
    Dim Sp_Total As String * 12
    
    Dim Sp_Banco As String * 20
    Dim Sp_NroCheque As String * 10
    Dim Sp_FechaCheque As String * 10
    Dim Sp_ValorCheque As String * 15
    Dim Sp_SumaCheque As String * 15
    
    
    
    
    
    
    Dim Sp_Observacion As String * 80
    
    On Error GoTo ERRORIMPRESION
    Sql = "SELECT giro,direccion,ciudad,fono,email,comuna " & _
         "FROM sis_empresas " & _
         "WHERE rut='" & SP_Rut_Activo & "'"
    Consulta RsTmp2, Sql
    If RsTmp2.RecordCount > 0 Then
        Sp_Giro = RsTmp2!giro
        Sp_Direccion = RsTmp2!direccion
        Sp_Fono = RsTmp2!fono
        Sp_Mail = RsTmp2!Email
        Sp_Ciudad = RsTmp2!ciudad
        Sp_Comuna = RsTmp2!comuna
    End If
            
    Sql = "SELECT direccion,ciudad,fono,email,comuna,ciudad " & _
         "FROM maestro_clientes " & _
         "WHERE rut_cliente='" & TxtRut & "'"
    Consulta RsTmp2, Sql
    If RsTmp2.RecordCount > 0 Then
        PSp_Direccion = Me.TxtDireccion
        PSp_Fono = Mid(RsTmp2!fono, 1, 10)
        PSp_Mail = Trim(RsTmp2!Email)
        PSp_Ciudad = Mid(RsTmp2!ciudad, 1, 20)
        PSp_Comuna = Mid(RsTmp2!comuna, 1, 20)
    End If
 '   PSp_Sucursal = CboSucursal.Text
            
       Sis_Previsualizar.Pic.ScaleMode = vbCentimeters
    Sis_Previsualizar.Pic.BackColor = vbWhite
    Sis_Previsualizar.Pic.AutoRedraw = True
    Sis_Previsualizar.Pic.DrawWidth = 1
    Sis_Previsualizar.Pic.DrawMode = 1
    
    Sis_Previsualizar.Pic.FontName = "Courier New"
    Sis_Previsualizar.Pic.FontSize = 10
   
    Cx = 2
    Cy = 3
    Dp_S = 0.1
    
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.CurrentY = Cy
    Sis_Previsualizar.Pic.FontSize = 16  'tama�o de letra
    Sis_Previsualizar.Pic.FontBold = True
    
    
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.CurrentY = Cy
    Sis_Previsualizar.Pic.PaintPicture LoadPicture(App.Path & "\REDMAROK.jpg"), Cx + 0.62, 1
    
    
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
    
    Sis_Previsualizar.Pic.FontSize = 18
    Sis_Previsualizar.Pic.FontBold = True
    
    Sis_Previsualizar.Pic.Print "           COTIZACION NRO " & Lp_Id_Unico_Venta
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S + Dp_S
            
    Sis_Previsualizar.Pic.FontSize = 12
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.Print SP_Empresa_Activa
    
    
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
  
    Sis_Previsualizar.Pic.FontSize = 12
  
    pos = Sis_Previsualizar.Pic.CurrentY
    Sis_Previsualizar.Pic.Print "R.U.T.   :" & SP_Rut_Activo
    Sis_Previsualizar.Pic.CurrentY = pos
    Sis_Previsualizar.Pic.CurrentX = Cx + 10
    Sis_Previsualizar.Pic.Print "FECHA:" & Date
    
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
    
    Sis_Previsualizar.Pic.FontBold = False
    Sis_Previsualizar.Pic.FontSize = 12
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.Print "GIRO     :" & Sp_Giro
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
    
    Sis_Previsualizar.Pic.FontSize = 12
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.Print "DIRECCION:" & Sp_Direccion
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
    
    'email
    Sis_Previsualizar.Pic.FontSize = 12
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.Print "EMAIL    :" & Sp_Mail
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
    
    
    Sis_Previsualizar.Pic.FontSize = 12
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.Print "CIUDAD   :" & Sp_Ciudad & "   COMUNA:" & Sp_Comuna & "   FONO:" & Sp_Fono
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S + (Dp_S * 3)
    
    'FIN SECCION EMPRESA
    
    
    
    'AHORA SECCION CLIENTE
    'sis_previsualizar.pic.FontSize = 10
    pos = Sis_Previsualizar.Pic.CurrentY
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.Print "SE�ORES  :" & TxtRazonSocial
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
    
    
    
    Sis_Previsualizar.Pic.CurrentX = Cx
    pos = Sis_Previsualizar.Pic.CurrentY
    Sis_Previsualizar.Pic.Print "RUT      :" & TxtRut
    
    Sis_Previsualizar.Pic.CurrentY = pos
    Sis_Previsualizar.Pic.CurrentX = Cx + 8
    Sis_Previsualizar.Pic.Print "CONDICION DE PAGO:" ' & Me.CboFpago.Text & " " & Mid(CboPlazos.Text, 1, 25)
    
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
    
    
    
    
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.Print "DIRECCION:" & PSp_Direccion
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
    
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.Print "EMAIL    :" & PSp_Mail
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
        
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.Print "CIUDAD   :" & PSp_Ciudad & "    COMUNA:" & PSp_Comuna & "    FONO:" & PSp_Fono
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S + Dp_S
    
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.Print "SUCURSAL   :" & PSp_Sucursal
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S + (Dp_S * 3)
    
    
    'Fin seccion CLIENTE
    
    
    
    'Inicio SECCION DETALLE DE ARTICULOS
            With LvVenta
                If .ListItems.Count > 0 Then
                    Sis_Previsualizar.Pic.CurrentX = Cx
                    Sis_Previsualizar.Pic.Print "Detalle de Art�culos"
                    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
                    Sp_CodigoInt = "Cod.Int."
                    Sp_Descripcin = "Descripcion"
                    RSet Sp_PU = "Pre. Unitario"
                    RSet Sp_Cant = "Cant."
                    RSet Sp_UM = "      "
                    RSet Sp_TotalL = "Total"
                    
                    Sis_Previsualizar.Pic.FontSize = 10
                    Sis_Previsualizar.Pic.FontBold = False
                    
                    Sis_Previsualizar.Pic.CurrentX = Cx
                    
                    Sis_Previsualizar.Pic.Print Mid(Sp_CodigoInt, 1, 6) & " " & Sp_Descripcin & " " & Sp_PU & " " & Sp_Cant & " " & Sp_TotalL
                    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
                    
                    For i = 1 To .ListItems.Count
                        Sp_CodigoInt = .ListItems(i).SubItems(1)
                        If SP_Rut_Activo = "76.169.962-8" Or SP_Rut_Activo = "76.553.302-3" Or SP_Rut_Activo = "78.967.170-2" Then
                            Sp_Cod_Acalde = .ListItems(i).SubItems(1)
                        End If
                        Sp_Descripcin = .ListItems(i).SubItems(2)
                        RSet Sp_PU = .ListItems(i).SubItems(4)
                        If SP_Rut_Activo = "76.553.302-3" Then
                             RSet Sp_Cant = Round(CDbl(.ListItems(i).SubItems(3)), 0)
                        Else
                            RSet Sp_Cant = .ListItems(i).SubItems(3)
                        End If
                        'RSet Sp_UM = .ListItems(i).SubItems(5) 'DSCTO
                      '  if
                        RSet Sp_TotalL = .ListItems(i).SubItems(5)
                        Sis_Previsualizar.Pic.CurrentX = Cx
                        
                        If SP_Rut_Activo = "76.169.962-8" Or SP_Rut_Activo = "76.553.302-3" Or SP_Rut_Activo = "78.967.170-2" Then
                            Sis_Previsualizar.Pic.Print Sp_Cod_Acalde & " " & Sp_Descripcin & " " & Sp_PU & " " & Sp_Cant & " " & Sp_TotalL
                        Else
                            Sis_Previsualizar.Pic.Print Sp_CodigoInt & " " & Sp_Descripcin & " " & Sp_PU & " " & Sp_Cant & " " & Sp_TotalL
                        End If
                        
                        Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S - 0.2
                    Next
                    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
                    posdetalle = Sis_Previsualizar.Pic.CurrentY
                End If
            End With
    'FIN SECCION DETALLE DE ARTICULOS
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.FontBold = True
    
    
                 'Totales
    Dp_Total = CDbl(txtTotal)
    Dp_Neto = Round(Dp_Total / Val("1." & DG_IVA), 0)
    Dp_Iva = Dp_Total - Dp_Neto
    
    
    
    'Dim Sp_ANeto As String * 15
    'Dim Sp_AIva As String * 15
    'Dim Sp_ATotal As String * 15
  '  RSet sp_AExento = TxtExentos
    RSet Sp_ANeto = NumFormat(Dp_Neto)
    RSet Sp_AIva = NumFormat(Dp_Iva)
    RSet Sp_ATotal = txtTotal
    
   ' RSet sp_AExento = TxtExentos
    'RSet Sp_ANeto = TxtNeto
    'RSet Sp_AIva = TxtIva
   ' RSet Sp_ATotal = txtTotal
    
  
      
    If CDbl(TxtValorDescuento) + CDbl(TxtDsctoAjuste) - CDbl(TxtRecargoAjuste) > 0 Then
    
        Sis_Previsualizar.Pic.CurrentX = Cx + 12
        Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S + Dp_S + Dp_S
        pos = Sis_Previsualizar.Pic.CurrentY
        tl = Sis_Previsualizar.Pic.FontSize
        Sis_Previsualizar.Pic.FontBold = True
        Sis_Previsualizar.Pic.Print "D E S C U E N T O:"
        Sis_Previsualizar.Pic.CurrentY = pos
        Sis_Previsualizar.Pic.CurrentX = Cx + 14.5
        RSet sp_AExento = NumFormat(CDbl(TxtValorDescuento) + CDbl(TxtDsctoAjuste) - CDbl(TxtRecargoAjuste))
        Sis_Previsualizar.Pic.Print sp_AExento
        Sis_Previsualizar.Pic.FontSize = tl
    End If
    Sis_Previsualizar.Pic.CurrentX = Cx + 12
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S - 0.2
    pos = Sis_Previsualizar.Pic.CurrentY
    Sis_Previsualizar.Pic.Print "N E T O    :"
    Sis_Previsualizar.Pic.CurrentY = pos
    Sis_Previsualizar.Pic.CurrentX = Cx + 14.5
    Sis_Previsualizar.Pic.Print Sp_ANeto
    
    Sis_Previsualizar.Pic.CurrentX = Cx + 12
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S - 0.2
    pos = Sis_Previsualizar.Pic.CurrentY
    Sis_Previsualizar.Pic.Print "I.V.A.     :"
    Sis_Previsualizar.Pic.CurrentY = pos
    Sis_Previsualizar.Pic.CurrentX = Cx + 14.5
    Sis_Previsualizar.Pic.Print Sp_AIva
    
    Sis_Previsualizar.Pic.CurrentX = Cx + 12
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S - 0.2
    pos = Sis_Previsualizar.Pic.CurrentY
    Sis_Previsualizar.Pic.Print "T O T A L  :"
    Sis_Previsualizar.Pic.CurrentY = pos
    Sis_Previsualizar.Pic.CurrentX = Cx + 14.5
    Sis_Previsualizar.Pic.Print Sp_ATotal
    
    Sis_Previsualizar.Pic.FontBold = False
        
   ' sis_previsualizar.pic.DrawMode = 1
    Sis_Previsualizar.Pic.DrawWidth = 3
    
    Sis_Previsualizar.Pic.Line (1.2, 4)-(21, 1), , B  'Rectangulo Encabezado y N� Nota de Venta
    Sis_Previsualizar.Pic.Line (1.2, 4)-(21, 7.7), , B  'Datos de la Empresa
    Sis_Previsualizar.Pic.Line (1.2, 4)-(21, 11.2), , B 'Datos del Proveedor
    Sis_Previsualizar.Pic.Line (1.2, 4)-(21, posdetalle), , B 'Detalle de Nota de Vena
    Sis_Previsualizar.Pic.Line (1.2, posdetalle)-(21, posdetalle + 2.3), , B 'Detalle del Exento,Neto,Iva,Total
    Sis_Previsualizar.Caption = "COTIZACION"
    Sis_Previsualizar.Show 1
    Exit Sub
ERRORIMPRESION:
    MsgBox Err.Number & vbNewLine & Err.Description
End Sub


Private Sub EstableImpresora(NombreImpresora As String)
   'Seteamos al impresora.
    Dim impresora As Printer
    ' Establece la impresora que se utilizar� para imprimir
    'Recorremos el objeto Printers
    For Each impresora In Printers
        'Si es igual al contenido se�alado en el combobox
        If UCase(impresora.DeviceName) = UCase(NombreImpresora) Then
            'Lo seteamos as�.
            Set Printer = impresora
            Exit For
        End If
    Next

End Sub
Private Function La_Establecer_Impresora(ByVal NamePrinter As String) As Boolean
  
On Error GoTo errSub
  
     
  
    'Variable de referencia
  
    Dim obj_Impresora As Object
  
     
  
    'Creamos la referencia
  
    Set obj_Impresora = CreateObject("WScript.Network")
  
        obj_Impresora.setdefaultprinter NamePrinter
  
     
  
    Set obj_Impresora = Nothing
  
         
  
        'La funci�n devuelve true y se cambi� con �xito
  
       La_Establecer_Impresora = True
  
  '      MsgBox "La impresora se cambi� correctamente", vbInformation
  
    Exit Function
  
     
  
     
  
'Error al cambiar la impresora
  
errSub:
  
If Err.Number = 0 Then Exit Function
  
   La_Establecer_Impresora = False
  
   MsgBox "error: " & Err.Number & Chr(13) & "Description: " & Err.Description
  
   On Error GoTo 0
  
End Function



Private Sub ImprimeCOTIZACION_Alcalde()
    Dim Cx As Double, Cy As Double, Sp_Fecha As String, Ip_Mes As Integer, Dp_S As Double, Sp_Letras As String
    Dim Sp_Neto As String * 12, Sp_IVA As String * 12
    
    Dim Sp_Nro_Comprobante As String
    
    Dim Sp_Empresa As String
    Dim Sp_Giro As String
    Dim Sp_Direccion As String
    Dim Sp_Fono As String * 12
    Dim Sp_Mail As String
    Dim Sp_Ciudad As String * 17
    Dim Sp_Comuna As String * 17
    
    
    Dim PSp_Empresa As String
    Dim PSp_Giro As String
    Dim PSp_Direccion As String
    Dim PSp_Fono As String
    Dim PSp_Mail As String
    Dim PSp_Ciudad As String
    Dim PSp_Comuna As String
    Dim PSp_Sucursal As String
    
    
    'Variables para detalle de articulos
    Dim Sp_CodigoInt As String * 13
    Dim Sp_Descripcin As String * 42
    Dim Sp_Cod_Acalde As String * 6
    Dim Sp_PU As String * 11
    Dim Sp_Cant As String * 7
    Dim Sp_UM As String * 3
    Dim Sp_TotalL As String * 12
    
    Dim sp_AExento As String * 15
    Dim Sp_ANeto As String * 15
    Dim Sp_AIva As String * 15
    Dim Sp_ATotal As String * 15
    '
    Dim Sp_XSubTotal As String * 15
    
    
    Dim Sp_Nombre As String * 45
    Dim Sp_Rut As String * 15
    Dim Sp_Concepto As String
    
    Dim Sp_Nro_Doc As String * 12
    Dim Sp_Documento As String * 35
    Dim Sp_Valor As String * 15
    Dim Sp_Saldo As String * 15
    Dim Sp_Fpago As String * 24
    Dim Sp_Total As String * 12
    
    Dim Sp_Banco As String * 20
    Dim Sp_NroCheque As String * 10
    Dim Sp_FechaCheque As String * 10
    Dim Sp_ValorCheque As String * 15
    Dim Sp_SumaCheque As String * 15
    
    Dim Sp_Observacion As String * 80
    
    On Error GoTo ERRORIMPRESION
    Sql = "SELECT giro,direccion,ciudad,fono,email,comuna " & _
         "FROM sis_empresas " & _
         "WHERE rut='" & SP_Rut_Activo & "'"
    Consulta RsTmp2, Sql
    If RsTmp2.RecordCount > 0 Then
        Sp_Giro = RsTmp2!giro
        Sp_Direccion = RsTmp2!direccion
        Sp_Fono = RsTmp2!fono
        Sp_Mail = RsTmp2!Email
        Sp_Ciudad = RsTmp2!ciudad
        Sp_Comuna = RsTmp2!comuna
    End If
            
    Sql = "SELECT direccion,ciudad,fono,email,comuna,ciudad " & _
         "FROM maestro_clientes " & _
         "WHERE rut_cliente='" & TxtRut & "'"
    Consulta RsTmp2, Sql
    If RsTmp2.RecordCount > 0 Then
        PSp_Direccion = Me.TxtDireccion
        PSp_Fono = Mid(RsTmp2!fono, 1, 10)
        PSp_Mail = Trim(RsTmp2!Email)
        PSp_Ciudad = Mid(RsTmp2!ciudad, 1, 20)
        PSp_Comuna = Mid(RsTmp2!comuna, 1, 20)
    End If
 '   PSp_Sucursal = CboSucursal.Text
            
            
    Printer.FontName = "Courier New"
    Printer.FontSize = 10
    Printer.ScaleMode = 7
    Cx = 1.55
    Cy = 3
    Dp_S = 0.1
    
    
    Printer.CurrentX = Cx
    Printer.CurrentY = Cy
    Printer.PaintPicture LoadPicture(App.Path & "\REDMAROK.jpg"), Cx + 0.62, 1
    
    
    Printer.CurrentY = Printer.CurrentY + Dp_S
    
    Printer.FontSize = 18
    Printer.FontBold = True
    
    Printer.Print "           COTIZACION  NRO " & Lp_Id_Unico_Venta
    Printer.CurrentY = Printer.CurrentY + Dp_S + Dp_S
            
    Printer.FontSize = 12
    Printer.CurrentX = Cx
    Printer.Print SP_Empresa_Activa
    
    
    Printer.CurrentX = Cx
    Printer.CurrentY = Printer.CurrentY + Dp_S
  
    Printer.FontSize = 12
  
    pos = Printer.CurrentY
    Printer.Print "R.U.T.   :" & SP_Rut_Activo
    Printer.CurrentY = pos
    Printer.CurrentX = Cx + 10
    Printer.Print "FECHA:" & Date
    
    Printer.CurrentY = Printer.CurrentY + Dp_S
    
    Printer.FontBold = False
    Printer.FontSize = 12
    Printer.CurrentX = Cx
    Printer.Print "GIRO     :" & Sp_Giro
    Printer.CurrentY = Printer.CurrentY + Dp_S
    
    Printer.FontSize = 12
    Printer.CurrentX = Cx
    Printer.Print "DIRECCION:" & Sp_Direccion
    Printer.CurrentY = Printer.CurrentY + Dp_S
    
    'email
    Printer.FontSize = 12
    Printer.CurrentX = Cx
    Printer.Print "EMAIL    :" & Sp_Mail
    Printer.CurrentY = Printer.CurrentY + Dp_S
    
    
    Printer.FontSize = 12
    Printer.CurrentX = Cx
    Printer.Print "CIUDAD   :" & Sp_Ciudad & " COMUNA:" & Sp_Comuna & " FONO:" & Sp_Fono
    Printer.CurrentY = Printer.CurrentY + Dp_S + (Dp_S * 3)
    
    'FIN SECCION EMPRESA
    
    'AHORA SECCION CLIENTE
    'Printer.FontSize = 10
    pos = Printer.CurrentY
    Printer.CurrentX = Cx
    Printer.Print "SE�ORES  :" & TxtRazonSocial
    Printer.CurrentY = Printer.CurrentY + Dp_S
    
    Printer.CurrentX = Cx
    pos = Printer.CurrentY
    Printer.Print "RUT      :" & TxtRut
    
    Printer.CurrentY = pos
    Printer.CurrentX = Cx + 8
    Printer.Print "CONDICION DE PAGO:" ' & Me.CboFpago.Text & " " & Mid(CboPlazos.Text, 1, 25)
    
    Printer.CurrentY = Printer.CurrentY + Dp_S
    
    Printer.CurrentX = Cx
    Printer.Print "DIRECCION:" & PSp_Direccion
    Printer.CurrentY = Printer.CurrentY + Dp_S
    
    Printer.CurrentX = Cx
    Printer.Print "EMAIL    :" & PSp_Mail
    Printer.CurrentY = Printer.CurrentY + Dp_S
        
    Printer.CurrentX = Cx
    Printer.Print "CIUDAD   :" & PSp_Ciudad & "    COMUNA:" & PSp_Comuna & "    FONO:" & PSp_Fono
    Printer.CurrentY = Printer.CurrentY + Dp_S + Dp_S
    
    Printer.CurrentX = Cx
    Printer.Print "SUCURSAL   :" & PSp_Sucursal
    Printer.CurrentY = Printer.CurrentY + Dp_S + (Dp_S * 3)
    
    
    'Fin seccion CLIENTE
    'Inicio SECCION DETALLE DE ARTICULOS
            With LvVenta
                If .ListItems.Count > 0 Then
                    Printer.CurrentX = Cx
                    Printer.Print "Detalle de Art�culos"
                    Printer.CurrentY = Printer.CurrentY + Dp_S
                    Sp_CodigoInt = "Cod.Int."
                    Sp_Descripcin = "Descripcion"
                    RSet Sp_PU = "Pre. Unitario"
                    RSet Sp_Cant = "Cant."
                    RSet Sp_UM = "      "
                    RSet Sp_TotalL = "Total"
                    
                    Printer.FontSize = 10
                    Printer.FontBold = False
                    
                    Printer.CurrentX = Cx
                    If SP_Rut_Activo = "78.967.170-2" Then
                        Printer.Print Mid(Sp_CodigoInt, 1, 6) & "  " & Sp_Cant & " " & Sp_Descripcin & "  " & Sp_PU & " " & Sp_TotalL
                    Else
                    
                        Printer.Print Mid(Sp_CodigoInt, 1, 6) & " " & Sp_Descripcin & " " & Sp_PU & " " & Sp_Cant & " " & Sp_TotalL
                    End If
                    Printer.CurrentY = Printer.CurrentY + Dp_S
                    
                    For i = 1 To .ListItems.Count
                        Sp_CodigoInt = .ListItems(i).SubItems(1)
                        If SP_Rut_Activo = "76.169.962-8" Or SP_Rut_Activo = "76.553.302-3" Or SP_Rut_Activo = "78.967.170-2" Then
                            Sp_Cod_Acalde = .ListItems(i).SubItems(1)
                        End If
                        Sp_Descripcin = .ListItems(i).SubItems(2)
                        RSet Sp_PU = .ListItems(i).SubItems(4)
                        If SP_Rut_Activo = "76.553.302-3" Then
                             RSet Sp_Cant = Round(CDbl(.ListItems(i).SubItems(3)), 0)
                        Else
                            RSet Sp_Cant = .ListItems(i).SubItems(3)
                        End If
                        If SP_Rut_Activo = "78.967.170-2" Then
                        
                            RSet Sp_UM = .ListItems(i).SubItems(10) 'DSCTO
                        End If
                        
                      '  if
                        RSet Sp_TotalL = .ListItems(i).SubItems(5)
                        Printer.CurrentX = Cx
                        
                        If SP_Rut_Activo = "76.169.962-8" Or SP_Rut_Activo = "76.553.302-3" Then
                            Printer.Print Sp_Cod_Acalde & " " & Sp_Descripcin & " " & Sp_PU & " " & Sp_Cant & " " & Sp_TotalL
                        ElseIf SP_Rut_Activo = "78.967.170-2" Then
                            'Plasticos alduante, lleva Unidad de medida
                            Printer.Print Sp_Cod_Acalde & "- " & Sp_Cant & " " & Sp_UM & " " & Mid(Sp_Descripcin, 1, 39) & " " & Sp_PU & " " & Sp_TotalL
                        ElseIf SP_Rut_Activo = "76.239.518-5" Then
                            Printer.Print Mid(Sp_CodigoInt, 1, 7) & " " & Sp_Descripcin & " " & Sp_PU & " " & Sp_Cant & " " & Sp_TotalL
                            
                        Else
                            Printer.Print Sp_CodigoInt & " " & Sp_Descripcin & " " & Sp_PU & " " & Sp_Cant & " " & Sp_TotalL
                        End If
                        
                        Printer.CurrentY = Printer.CurrentY + Dp_S - 0.2
                    Next
                    Printer.CurrentY = Printer.CurrentY + Dp_S
                    posdetalle = Printer.CurrentY
                End If
            End With
    'FIN SECCION DETALLE DE ARTICULOS
    Printer.CurrentX = Cx
    Printer.FontBold = True
    
    
                 'Totales
    Dp_Total = CDbl(txtTotal)
    Dp_Neto = Round(Dp_Total / Val("1." & DG_IVA), 0)
    Dp_Iva = Dp_Total - Dp_Neto
    
    
    
    'Dim Sp_ANeto As String * 15
    'Dim Sp_AIva As String * 15
    'Dim Sp_ATotal As String * 15
  '  RSet sp_AExento = TxtExentos
    RSet Sp_ANeto = NumFormat(Dp_Neto)
    RSet Sp_AIva = NumFormat(Dp_Iva)
    RSet Sp_ATotal = txtTotal
    
   ' RSet sp_AExento = TxtExentos
    'RSet Sp_ANeto = TxtNeto
    'RSet Sp_AIva = TxtIva
   ' RSet Sp_ATotal = txtTotal
    
  '  Printer.CurrentX = Cx + 12
    If SP_Rut_Activo = "78.967.170-2" Then
            tac = Printer.FontSize
            Printer.FontBold = False
            Printer.FontSize = 8
            posx = Printer.CurrentX
            Printer.Print "Validez: 7 dias, Valores con IVA incluido, "
            Printer.CurrentX = posx
            Printer.Print TxtPlazoEntrega
            Printer.FontSize = tac
  
    End If
    Printer.CurrentY = Printer.CurrentY + Dp_S + Dp_S + Dp_S
  '  pos = Printer.CurrentY
  '  Printer.Print "E X E N T O:"
  '  Printer.CurrentY = pos
  '  Printer.CurrentX = Cx + 14.5
  '  Printer.Print sp_AExento

  
  
  
  
    
'    Printer.CurrentX = Cx + 12
'    Printer.CurrentY = Printer.CurrentY + Dp_S - 0.2
'    pos = Printer.CurrentY
'    Printer.Print "N E T O    :"
'    Printer.CurrentY = pos
'    Printer.CurrentX = Cx + 14.5
'    Printer.Print Sp_ANeto
'
'    Printer.CurrentX = Cx + 12
'    Printer.CurrentY = Printer.CurrentY + Dp_S - 0.2
'    pos = Printer.CurrentY
'    Printer.Print "I.V.A.     :"
'    Printer.CurrentY = pos
'    Printer.CurrentX = Cx + 14.5
'    Printer.Print Sp_AIva
    
   
    Printer.CurrentX = Cx + 12
    Printer.CurrentY = Printer.CurrentY + Dp_S - 0.2
    pos = Printer.CurrentY
    Printer.Print "T O T A L  :"
    Printer.CurrentY = pos
    Printer.CurrentX = Cx + 12.5
    RSet Sp_XSubTotal = TxtSubTotal
    Printer.Print Sp_XSubTotal
    
    
    If CDbl(TxtValorDescuento) + CDbl(TxtDsctoAjuste) - CDbl(TxtRecargoAjuste) > 0 Then
    
        Printer.CurrentX = Cx + 10
       Printer.CurrentY = Printer.CurrentY + Dp_S + Dp_S + Dp_S
        pos = Printer.CurrentY
        
        Printer.FontBold = True
        Printer.Print "D E S C U E N T O:"
        Printer.CurrentY = pos
        Printer.CurrentX = Cx + 12.5
        RSet sp_AExento = NumFormat(CDbl(TxtValorDescuento) + CDbl(TxtDsctoAjuste) - CDbl(TxtRecargoAjuste))
        Printer.Print sp_AExento
                
        Printer.CurrentX = Cx + 12
        Printer.CurrentY = Printer.CurrentY + Dp_S - 0.2
        pos = Printer.CurrentY
        Printer.Print "T O T A L  :"
        Printer.CurrentY = pos
        Printer.CurrentX = Cx + 12.5
        Printer.Print Sp_ATotal
              
    End If

    Printer.FontBold = False
        
   ' Printer.DrawMode = 1
    Printer.DrawWidth = 3
    
    Printer.Line (1.2, 4)-(19, 1), , B  'Rectangulo Encabezado y N� Nota de Venta
    Printer.Line (1.2, 4)-(19, 7.7), , B  'Datos de la Empresa
    Printer.Line (1.2, 4)-(19, 11.2), , B 'Datos del Proveedor
    Printer.Line (1.2, 4)-(19, posdetalle), , B 'Detalle de Nota de Vena
    Printer.Line (1.2, posdetalle)-(19, posdetalle + 2.3), , B 'Detalle del Exento,Neto,Iva,Total
       
    Printer.NewPage
    Printer.EndDoc
    Exit Sub
ERRORIMPRESION:
    MsgBox Err.Number & vbNewLine & Err.Description
End Sub


