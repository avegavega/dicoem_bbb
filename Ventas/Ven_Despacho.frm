VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{DA729E34-689F-49EA-A856-B57046630B73}#1.0#0"; "progressbar-xp.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form Ven_Despacho 
   Caption         =   "Visualizador de Despachos"
   ClientHeight    =   6855
   ClientLeft      =   1965
   ClientTop       =   2730
   ClientWidth     =   15195
   LinkTopic       =   "Form1"
   ScaleHeight     =   6855
   ScaleWidth      =   15195
   Begin VB.Timer Timer1 
      Interval        =   10
      Left            =   75
      Top             =   7155
   End
   Begin VB.CommandButton cmdSalir 
      Cancel          =   -1  'True
      Caption         =   "&Retornar"
      Height          =   405
      Left            =   13350
      TabIndex        =   4
      Top             =   6195
      Width           =   1350
   End
   Begin VB.Frame Frame5 
      Caption         =   "Detalle de Productos"
      Height          =   2985
      Left            =   510
      TabIndex        =   0
      Top             =   3840
      Width           =   14505
      Begin Proyecto2.XP_ProgressBar BarraProgreso 
         Height          =   495
         Left            =   345
         TabIndex        =   15
         Top             =   1125
         Visible         =   0   'False
         Width           =   8460
         _ExtentX        =   14923
         _ExtentY        =   873
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BrushStyle      =   0
         Color           =   16777088
         Scrolling       =   1
         ShowText        =   -1  'True
      End
      Begin MSComctlLib.ListView LvDetalle 
         Height          =   2550
         Left            =   135
         TabIndex        =   1
         Top             =   270
         Width           =   8925
         _ExtentX        =   15743
         _ExtentY        =   4498
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   0   'False
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   4
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "T1000"
            Text            =   "Codigo"
            Object.Width           =   2028
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "T1500"
            Text            =   "Cantidad"
            Object.Width           =   2205
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "T3000"
            Text            =   "Precio Total"
            Object.Width           =   4674
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   3
            Object.Tag             =   "N102"
            Text            =   "Descripcion"
            Object.Width           =   4410
         EndProperty
      End
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   630
      OleObjectBlob   =   "Ven_Despacho.frx":0000
      Top             =   7110
   End
   Begin VB.Frame Frame1 
      Caption         =   "Filtro"
      Height          =   3090
      Left            =   9735
      TabIndex        =   5
      Top             =   465
      Width           =   5235
      Begin VB.CommandButton CmdExportar 
         Caption         =   "Exportar a Excel"
         Height          =   465
         Left            =   3420
         TabIndex        =   14
         Top             =   1620
         Width           =   1395
      End
      Begin VB.CommandButton cmdConsultar 
         Caption         =   "&Consultar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   420
         Left            =   3480
         TabIndex        =   6
         Top             =   2280
         Width           =   1365
      End
      Begin VB.Frame Frame4 
         Caption         =   "Informacion"
         Height          =   1875
         Left            =   360
         TabIndex        =   7
         Top             =   765
         Width           =   2790
         Begin VB.OptionButton Option1 
            Caption         =   "Todas las fechas"
            Height          =   240
            Left            =   495
            TabIndex        =   9
            Top             =   345
            Value           =   -1  'True
            Width           =   1740
         End
         Begin VB.OptionButton Option2 
            Caption         =   "Seleccionar fechas"
            Height          =   240
            Left            =   465
            TabIndex        =   8
            Top             =   720
            Width           =   1725
         End
         Begin ACTIVESKINLibCtl.SkinLabel skIni 
            Height          =   240
            Left            =   150
            OleObjectBlob   =   "Ven_Despacho.frx":0234
            TabIndex        =   10
            Top             =   1125
            Width           =   795
         End
         Begin MSComCtl2.DTPicker DTInicio 
            Height          =   300
            Left            =   1095
            TabIndex        =   11
            Top             =   1110
            Width           =   1305
            _ExtentX        =   2302
            _ExtentY        =   529
            _Version        =   393216
            Enabled         =   0   'False
            Format          =   88932353
            CurrentDate     =   40628
         End
         Begin MSComCtl2.DTPicker DtHasta 
            Height          =   285
            Left            =   1095
            TabIndex        =   12
            Top             =   1410
            Width           =   1305
            _ExtentX        =   2302
            _ExtentY        =   503
            _Version        =   393216
            Enabled         =   0   'False
            Format          =   88932353
            CurrentDate     =   40628
         End
         Begin ACTIVESKINLibCtl.SkinLabel skFin 
            Height          =   240
            Left            =   255
            OleObjectBlob   =   "Ven_Despacho.frx":029C
            TabIndex        =   13
            Top             =   1395
            Width           =   795
         End
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Despachos"
      Height          =   3315
      Left            =   495
      TabIndex        =   2
      Top             =   345
      Width           =   14520
      Begin MSComctlLib.ListView LvDocumentos 
         Height          =   2535
         Left            =   195
         TabIndex        =   3
         Top             =   360
         Width           =   8940
         _ExtentX        =   15769
         _ExtentY        =   4471
         View            =   3
         LabelWrap       =   0   'False
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   5
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "N100"
            Text            =   "Id Unico"
            Object.Width           =   353
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "F1100"
            Text            =   "Rut Cliente"
            Object.Width           =   2116
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "T1000"
            Text            =   "Fecha"
            Object.Width           =   2028
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Object.Tag             =   "T2000"
            Text            =   "Direccion"
            Object.Width           =   8819
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Text            =   "nve_id"
            Object.Width           =   1764
         EndProperty
      End
   End
End
Attribute VB_Name = "Ven_Despacho"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim sm_FiltroFecha As String
Dim Sm_FiltroSucursal As String
Dim Sm_FiltroCliente As String
Private Sub cmdBtnRut_Click()
    ClienteEncontrado = False
    SG_codigo = Empty
    
    If Sp_extra = "CLIENTES" Then
        BuscaCliente.Show 1
    Else
        BuscaProveedor.Show 1
    End If
    
    TxtRut = SG_codigo
    If SG_codigo <> Empty Then
       '' TxtRut_Validate (True)
    End If
End Sub

Private Sub CmdCierraDetalleCheques_Click()
    FrmDetalles.Visible = False
End Sub

Private Sub cmdConsultar_Click()
      
    LvDocumentos.ListItems.Clear
    LvDetalle.ListItems.Clear
    
    If Option1.Value Then
      Sql = ""
                Sql = "SELECT a.despacho_id,a.rut_cliente,DATE_FORMAT(despacho_fecha,'%d-%m-%Y')despacho_fecha,a.despacho_direccion,a.nve_id " & _
                "FROM ven_despacho a " & _
                "inner join ven_nota_venta b " & _
                " WHERE a.nve_id = (select b.nve_id from ven_nota_venta inner join ven_despacho a where b.id > 0 and b.nve_id = a.nve_id limit 1);"
                
                
    Else
    Sql = ""
'                Sql = "SELECT despacho_id,rut_cliente,DATE_FORMAT(despacho_fecha,'%d-%m-%Y')despacho_fecha,despacho_direccion,nve_id " & _
'                "FROM ven_despacho " & _
'                "where despacho_fecha BETWEEN '" & Format(DTInicio.Value, "YYYY-MM-DD") & "' AND '" & Format(DtHasta.Value, "YYYY-MM-DD") & "'"

                Sql = "SELECT a.despacho_id,a.rut_cliente,DATE_FORMAT(despacho_fecha,'%d-%m-%Y')despacho_fecha,a.despacho_direccion,a.nve_id " & _
                "FROM ven_despacho a " & _
                "inner join ven_nota_venta b " & _
                " WHERE a.nve_id = (select b.nve_id from ven_nota_venta inner join ven_despacho a where b.id > 0 and b.nve_id = a.nve_id limit 1)" & _
                " and despacho_fecha BETWEEN '" & Format(DTInicio.Value, "YYYY-MM-DD") & "' AND '" & Format(DtHasta.Value, "YYYY-MM-DD") & "'"


    End If

    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvDocumentos, False, True, True, False
        
End Sub
Private Sub CmdExportar_Click()
 Dim tit(2) As String
    If LvDocumentos.ListItems.Count = 0 Then Exit Sub
    BarraProgreso.Visible = True
    tit(0) = Me.Caption & " " & Date & " - " & Time
    tit(1) = ""
    tit(2) = ""
    ExportarNuevo LvDocumentos, tit, Me, BarraProgreso
    BarraProgreso.Visible = False
End Sub
Private Sub cmdSalir_Click()
    Unload Me
End Sub
''
''Private Sub cmdSinRut_Click()
''    TxtRut = Empty
''    Me.txtCliente = Empty
''    CboSucursal.ListIndex = -1
''    txtFono = Empty
''    TxtMail = Empty
''End Sub

Private Sub Form_Load()
    Centrar Me
    Aplicar_skin Me
'    Filtro = Empty
'    DTInicio.Value = Date - 30
'    DtHasta.Value = Date
'
'

'    sm_FiltroFecha = Empty
'
    
    
    
    
End Sub


Private Sub LVCheques_LostFocus()
    Me.FrmDetalles.Visible = False
End Sub

Private Sub LvDetalle_DblClick()
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    TxtRut = LvDetalle.SelectedItem.Text
    TxtRut_Validate (True)
    cmdConsultar_Click
End Sub



Private Sub LvDocumentos_Click()
    
    If LvDocumentos.SelectedItem Is Nothing Then Exit Sub
    
    'DETALLE DEL DOCUMENTO
    DG_ID_Unico = LvDocumentos.SelectedItem.Text

'            Sql = "SELECT codigo,nvd_cantidad,nvd_precio_total,nvd_descripcion " & _
'                  "FROM ven_nota_venta_detalle " & _
'                  "WHERE rut_cliente='" & LvDocumentos.SelectedItem.SubItems(1) & "' AND  nve_id=" & LvDocumentos.SelectedItem & " "
           Sql = "SELECT codigo,nvd_cantidad,nvd_precio_total,nvd_descripcion " & _
                  "FROM ven_nota_venta_detalle " & _
                  "WHERE nve_id=" & LvDocumentos.SelectedItem.SubItems(4) & " "

                  
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvDetalle, False, True, True, False
    
 
    
End Sub



Private Sub LvDocumentos_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    ordListView ColumnHeader, Me, LvDocumentos
End Sub

Private Sub LvDetalle_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    ordListView ColumnHeader, Me, LvDetalle
End Sub

Private Sub LvPagos_Click()
    If LvPagos.SelectedItem Is Nothing Then Exit Sub
    DG_ID_Unico = LvPagos.SelectedItem.Text
    LvCheques.ListItems.Clear
    Sql = "SELECT ban_nombre,che_plaza,che_numero,che_fecha,che_monto,che_estado " & _
          "FROM abo_cheques c INNER JOIN par_bancos b USING(ban_id) " & _
          "WHERE abo_id=" & DG_ID_Unico
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        FrmDetalles.Visible = True
        LLenar_Grilla RsTmp, Me, LvCheques, False, True, True, False
        LvCheques.SetFocus
    End If
End Sub

Private Sub Option1_Click()
    DTInicio.Enabled = False
    DtHasta.Enabled = False
End Sub

Private Sub Option2_Click()
    DTInicio.Enabled = True
    DtHasta.Enabled = True
   
End Sub

Private Sub Timer1_Timer()
'    LvDetalle.SetFocus
'    Timer1.Enabled = False
End Sub




