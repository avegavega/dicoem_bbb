VERSION 5.00
Object = "{531C1877-C9DA-4778-97E9-91EA75A2F898}#1.0#0"; "ocxsam350.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form vtaBoletaFiscalSamsumg 
   Caption         =   "Emitiendo Boleta Fiscal"
   ClientHeight    =   10530
   ClientLeft      =   2040
   ClientTop       =   735
   ClientWidth     =   17160
   LinkTopic       =   "Form1"
   ScaleHeight     =   10530
   ScaleWidth      =   17160
   Begin VB.TextBox TxtDireccion 
      Height          =   315
      Left            =   2970
      TabIndex        =   16
      Text            =   "Direccion"
      Top             =   9000
      Width           =   3330
   End
   Begin VB.TextBox TxtRutCliente 
      Height          =   300
      Left            =   2985
      TabIndex        =   15
      Text            =   "RutCliente"
      Top             =   8595
      Width           =   3615
   End
   Begin VB.TextBox TxtNombreCliente 
      Height          =   285
      Left            =   2985
      TabIndex        =   14
      Text            =   "NombreCliente"
      Top             =   8205
      Width           =   3135
   End
   Begin VB.TextBox TxtVendedor 
      Height          =   585
      Left            =   13875
      TabIndex        =   13
      Text            =   "vendedor"
      Top             =   240
      Width           =   4125
   End
   Begin MSComCtl2.DTPicker DtHoraIF 
      Height          =   270
      Left            =   10800
      TabIndex        =   11
      Top             =   3585
      Width           =   1140
      _ExtentX        =   2011
      _ExtentY        =   476
      _Version        =   393216
      Format          =   41091074
      CurrentDate     =   42483
   End
   Begin VB.CommandButton CmdFechaHora 
      Caption         =   "Ajustar"
      Height          =   300
      Left            =   12000
      TabIndex        =   10
      Top             =   3585
      Width           =   1470
   End
   Begin MSComCtl2.DTPicker DtFechaIF 
      Height          =   285
      Left            =   9645
      TabIndex        =   9
      Top             =   3585
      Width           =   1170
      _ExtentX        =   2064
      _ExtentY        =   503
      _Version        =   393216
      Format          =   41091073
      CurrentDate     =   42483
   End
   Begin VB.CommandButton Command3 
      Caption         =   "Command3"
      Height          =   255
      Left            =   2385
      TabIndex        =   7
      Top             =   2250
      Width           =   420
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Command2"
      Height          =   525
      Left            =   4185
      TabIndex        =   5
      Top             =   6120
      Width           =   1575
   End
   Begin VB.TextBox TxtNroBoletaFiscal 
      Height          =   585
      Left            =   7500
      TabIndex        =   4
      Text            =   "Text1"
      Top             =   5865
      Width           =   3945
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Command1"
      Height          =   555
      Left            =   8400
      TabIndex        =   3
      Top             =   4545
      Width           =   1395
   End
   Begin OCXSAM350Lib.Ocxsam350 OcxFiscal 
      Height          =   615
      Left            =   1935
      TabIndex        =   2
      Top             =   3705
      Width           =   495
      _Version        =   65536
      _ExtentX        =   873
      _ExtentY        =   1085
      _StockProps     =   0
   End
   Begin VB.TextBox TxtTotal 
      Height          =   360
      Left            =   4695
      TabIndex        =   0
      Top             =   5100
      Width           =   2220
   End
   Begin MSComctlLib.ListView LvDetalle 
      Height          =   2730
      Left            =   4605
      TabIndex        =   1
      Top             =   420
      Width           =   6660
      _ExtentX        =   11748
      _ExtentY        =   4815
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   0
      NumItems        =   3
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Id"
         Object.Width           =   1058
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Nombre"
         Object.Width           =   8043
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "Habilitado"
         Object.Width           =   1614
      EndProperty
   End
   Begin MSComctlLib.ListView LvDescuentos 
      Height          =   2730
      Left            =   135
      TabIndex        =   6
      Top             =   4635
      Width           =   4140
      _ExtentX        =   7303
      _ExtentY        =   4815
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   0
      NumItems        =   3
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Id"
         Object.Width           =   1058
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Detalle"
         Object.Width           =   8043
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "Valor"
         Object.Width           =   1614
      EndProperty
   End
   Begin MSComctlLib.ListView LvPagos 
      Height          =   2115
      Left            =   8130
      TabIndex        =   8
      Top             =   7800
      Width           =   4980
      _ExtentX        =   8784
      _ExtentY        =   3731
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   0
      NumItems        =   3
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Id"
         Object.Width           =   1058
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Detalle"
         Object.Width           =   8043
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "Monto"
         Object.Width           =   1614
      EndProperty
   End
   Begin MSComctlLib.ListView LvCuotas 
      Height          =   3330
      Left            =   12795
      TabIndex        =   12
      Top             =   3930
      Visible         =   0   'False
      Width           =   6675
      _ExtentX        =   11774
      _ExtentY        =   5874
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   3
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Numero"
         Object.Width           =   3528
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Fecha"
         Object.Width           =   3528
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "Monto"
         Object.Width           =   3528
      EndProperty
   End
End
Attribute VB_Name = "vtaBoletaFiscalSamsumg"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim Vr As Variant

Public Sub EmiteBoleta()
    Dim Lp_CuantoPaga As Long, m As Integer
  'Emitimos boleta
  Dim dato As String, dato2 As String * 3
  Dim Bp_NoFiscal As Boolean
  Bp_NoFiscal = False
                   ' MsgBox "inciado EmiteBoleta"
                    Lp_CuantoPaga = 0
                    'MsgBox "Puerto imp fiscal " & IG_Puerto_Impresora_Fiscal
                    'Vr = OcxFiscal.Init(IG_Puerto_Impresora_Fiscal)
                    Vr = OcxFiscal.Init(1)
                    'MsgBox "iniciada la conexion con if " & Vr
                    If Vr = 0 Then
                        'cn.RollbackTrans
                        MsgBox "No se pudo completar la boleta..." & vbNewLine & Err.Number & " " & Err.Description, vbInformation
                        Exit Sub
                    End If
                  
                    If SP_Rut_Activo = "76.354.346-3" Then
                            'solo para mafil
                            
                          If Me.LvCuotas.ListItems.Count > 0 Then
                                Sp_Nomb = TxtNombreCliente
                                Sp_Rut = TxtRutCliente
                     '           MsgBox "comenzar con encabezado"
                                Vr = OcxFiscal.conflineaencabezado(1, "==========================")
                      '          MsgBox "comenzar con encabezado L1"
                                Vr = OcxFiscal.conflineaencabezado(2, "SE�OR(ES)")
                                Vr = OcxFiscal.conflineaencabezado(3, "RUT   :" & Sp_Rut)
                                Vr = OcxFiscal.conflineaencabezado(4, "NOMBRE:" & Sp_Nomb)
                       '         MsgBox "comenzar con encabezado L5"
                                Vr = OcxFiscal.conflineaencabezado(5, "==========================")
                                'MsgBox "XXX Cargando encabezdso"
                         Else
                                
                         End If
                    End If
                  
                    Vr = OcxFiscal.conflineacola(1, "==UD FUE ATENDIDO POR ===")
                    Vr = OcxFiscal.conflineacola(2, "===" & TxtVendedor & " ===")
                    If SP_Rut_Activo = "76.354.346-3" Then
                        'Mafil
                        If Me.LvCuotas.ListItems.Count > 0 Then
                        '    MsgBox "XXX Cargando Cola"
                            Vr = OcxFiscal.conflineacola(3, "=======CUOTAS========")
                            m = 3
                            For G = 1 To Me.LvCuotas.ListItems.Count
                               m = m + 1
                               Vr = OcxFiscal.conflineacola(m, Me.LvCuotas.ListItems(G) & " " & Me.LvCuotas.ListItems(G).SubItems(1) & " $" & Me.LvCuotas.ListItems(G).SubItems(2))
                               'Vr = OcxFiscal.conflineacola(M, "   " & Me.LvCuotas.ListItems(G).SubItems(1))
                               'Vr = OcxFiscal.conflineacola(M, " $ " & Me.LvCuotas.ListItems(G).SubItems(2))
                            Next
                            m = m + 1
                            Vr = OcxFiscal.conflineacola(m, "======= FIN CUOTAS ========")
                            m = m + 1
                            Vr = OcxFiscal.conflineacola(m, "")
                            m = m + 1
                            Vr = OcxFiscal.conflineacola(m, "")
                            m = m + 1
                            Vr = OcxFiscal.conflineacola(m, "")
                            m = m + 1
                            Vr = OcxFiscal.conflineacola(m, "")
                            Bp_NoFiscal = True
                            If 1 = 2 Then

                                        m = m + 1
                                        Vr = OcxFiscal.conflineacola(m, " - - - - - - - - - - - - - - - - - ")
                                        
                                        
                                      'Aqui se deben confiugrar
                                        m = m + 1
                                        Vr = OcxFiscal.conflineacola(m, "=======[ COPIA INTERNA ]==========")
                         '               MsgBox "Copia cola"
                                        m = m + 1
                                        Vr = OcxFiscal.conflineacola(m, "==  CLIENTE  ======")
                                        m = m + 1
                                        Vr = OcxFiscal.conflineacola(m, " ")
                                        m = m + 1
                                        Vr = OcxFiscal.conflineacola(m, "SE�OR(ES)")
                                        m = m + 1
                                        Vr = OcxFiscal.conflineacola(m, "RUT       :" & Sp_Rut)
                                        m = m + 1
                                        Vr = OcxFiscal.conflineacola(m, "NOMBRE    :" & Sp_Nomb)
                                        m = m + 1
                                        Vr = OcxFiscal.conflineacola(m, "NRO BOLETA:" & Lp_Nro_Boleta_Obtenida_IF)
                                        m = m + 1
                                        Vr = OcxFiscal.conflineacola(m, "FECHA     :" & Date)
                                        m = m + 1
                                        Vr = OcxFiscal.conflineacola(m, "==========================")
                                        m = m + 1
                                        Vr = OcxFiscal.conflineacola(m, "=====  C U O T A S =======")
                                        m = m + 1
            
                                        For G = 1 To Me.LvCuotas.ListItems.Count
                                            
                                            Vr = OcxFiscal.conflineacola(m, Me.LvCuotas.ListItems(G) & " " & Me.LvCuotas.ListItems(G).SubItems(1) & " $" & Me.LvCuotas.ListItems(G).SubItems(2))
                                            m = m + 1
                                        Next
            
                                        Vr = OcxFiscal.conflineacola(m, "==========================")
                                        m = m + 1
                                        Vr = OcxFiscal.conflineacola(m, "Firma ")
                                        m = m + 1
                                        Vr = OcxFiscal.conflineacola(m, "__________________________ ")
                                        m = m + 1
                                        Vr = OcxFiscal.conflineacola(m, "RUT ")
                                        m = m + 1
                                        Vr = OcxFiscal.conflineacola(m, "__________________________ ")
                                        m = m + 1
                                        Vr = OcxFiscal.conflineacola(m, "==========================")
                                        
                                    
                            End If
                            
                            
                            
                        Else
                        
                                Vr = OcxFiscal.conflineacola(3, "====================")
                                Vr = OcxFiscal.conflineacola(4, "   GRACIAS POR ")
                                Vr = OcxFiscal.conflineacola(5, "       --- ")
                                Vr = OcxFiscal.conflineacola(6, "   PREFERIRNOS :) ")
                                Vr = OcxFiscal.conflineacola(7, "====================")
                        End If
                        
                        
                    End If
                  
                    'MsgBox "abriendo boleta"
                    If Me.LvCuotas.ListItems.Count > 0 Then
                        Vr = OcxFiscal.abrirboleta(1, 1)
                    Else
                        Vr = OcxFiscal.abrirboleta(1, 0)
                    End If
                    
                    'Vr = OcxFiscal.abrirboleta(0, 1)
                    Vr = OcxFiscal.obtenernumboleta()
                    Lp_Nro_Boleta_Obtenida_IF = OcxFiscal.boleta
                    
                    'Aqui iria el detalle de la boleta
                    
                    For i = 1 To LvDetalle.ListItems.Count
                        dato = LvDetalle.ListItems(i)
                    '    dato = InputBox("Cantidad", "datos buenos", dato)
                    
                      '  Vr = OcxFiscal.agregaitem(Mid(LvDetalle.ListItems(i).SubItems(1), 1, 40), dato, CDbl(LvDetalle.ListItems(i).SubItems(2)))
                     '   Vr = OcxFiscal.agregaitemdec(Mid(LvDetalle.ListItems(i).SubItems(1), 1, 40), LvDetalle.ListItems(i), 0, CDbl(LvDetalle.ListItems(i).SubItems(2)))
                         Lp_CuantoPaga = Lp_CuantoPaga + (Val(LvDetalle.ListItems(i)) * CDbl(LvDetalle.ListItems(i).SubItems(2)))
                        dato2 = "000"
                        If InStr(1, dato, ".", vbTextCompare) > 0 Then
                            dato2 = Mid(dato, InStr(1, dato, ".", vbTextCompare) + 1) & "000"
                        End If
                         
                        Vr = OcxFiscal.agregaitemdec(Mid(LvDetalle.ListItems(i).SubItems(1), 1, 40), Int(Replace(dato, ".", ",")), dato2, CDbl(LvDetalle.ListItems(i).SubItems(2)))
                    Next
                   ' MsgBox Lp_CuantoPaga
                    If LvDescuentos.ListItems.Count > 0 Then
                        For i = 1 To LvDescuentos.ListItems.Count
                            If LvDescuentos.ListItems(i).SubItems(1) <> "AJUSTE SIMPLE" Then
                                Vr = OcxFiscal.agregadescuento(LvDescuentos.ListItems(i).SubItems(1), Val(LvDescuentos.ListItems(i).SubItems(2)))
                                Lp_CuantoPaga = Lp_CuantoPaga - Val(LvDescuentos.ListItems(i).SubItems(2))
                            Else
                                Vr = OcxFiscal.agregarecargo(LvDescuentos.ListItems(i).SubItems(1), Val(LvDescuentos.ListItems(i).SubItems(2)))
                                Lp_CuantoPaga = Lp_CuantoPaga + Val(LvDescuentos.ListItems(i).SubItems(2))
                            End If
                        Next
                    End If
                        
                        
                        
                    
                    
                    'Vr = OcxFiscal.agregapago(0, CDbl(Me.TxtTotal))
                    
                    cuantopaso = 0
                    
                    For i = 1 To Me.LvPagos.ListItems.Count
                        cuantopaso = cuantopaso + CDbl(Me.LvPagos.ListItems(i).SubItems(2))
                        Vr = OcxFiscal.agregapago(Me.LvPagos.ListItems(i), CDbl(Me.LvPagos.ListItems(i).SubItems(2)))
                        
                    Next
                    If CDbl(Me.TxtTotal) > cuantopaso Then
                        Vr = OcxFiscal.agregapago(0, (CDbl(Me.TxtTotal) - cuantopaso) + 100)
                    End If
                   
                    
                   
                    
                    If SP_Rut_Activo = "76.354.346-3" Then
                        If Me.LvCuotas.ListItems.Count > 0 Then
                            Vr = OcxFiscal.cierraboleta(1)
                        Else
                            Vr = OcxFiscal.cierraboleta(0)
                        End If
                    Else
                        Vr = OcxFiscal.cierraboleta(1)
                    End If
                    
                    If Bp_NoFiscal = True Then
                        'vamos a emitir una copia de la boelta en modo no fiscal
                            Vr = OcxFiscal.abrirnofiscal(1, 0)
                        
                            If Me.LvCuotas.ListItems.Count > 0 Then

                                'Aqui se deben confiugrar
                                Vr = OcxFiscal.lineanofiscal("=====  CLIENTE  ======")
                                Vr = OcxFiscal.lineanofiscal(" ")
                                Vr = OcxFiscal.lineanofiscal("SE�OR(ES)")
                                Vr = OcxFiscal.lineanofiscal("RUT       :" & Sp_Rut)
                                Vr = OcxFiscal.lineanofiscal("NOMBRE    :" & Sp_Nomb)
                                Vr = OcxFiscal.lineanofiscal("NRO BOLETA:" & Lp_Nro_Boleta_Obtenida_IF)
                                Vr = OcxFiscal.lineanofiscal("FECHA     :" & Date)
                                Vr = OcxFiscal.lineanofiscal("==========================")
                                
                                
                                'Productos
                                Vr = OcxFiscal.lineanofiscal("=====  DETALLE =======")
                                
                                For i = 1 To LvDetalle.ListItems.Count
                                    
                                  Vr = OcxFiscal.lineanofiscal(Mid(LvDetalle.ListItems(i).SubItems(1), 1, 40) & " " & dato & " " & CDbl(LvDetalle.ListItems(i).SubItems(2)))
                                
                                Next
                                
                                'Cuotas
                                Vr = OcxFiscal.lineanofiscal("=====  C U O T A S =======")
                                Vr = OcxFiscal.lineanofiscal("Nro  Fecha      Valor")
                                Vr = OcxFiscal.lineanofiscal("--------------------------")
                                For G = 1 To Me.LvCuotas.ListItems.Count
                                
                                    Vr = OcxFiscal.lineanofiscal(Me.LvCuotas.ListItems(G) & "   " & Me.LvCuotas.ListItems(G).SubItems(1) & " $" & Me.LvCuotas.ListItems(G).SubItems(2))
                               
                                Next

                                Vr = OcxFiscal.lineanofiscal("==========================")
                                Vr = OcxFiscal.lineanofiscal("Firma ")
                                Vr = OcxFiscal.lineanofiscal("__________________________ ")
                                Vr = OcxFiscal.lineanofiscal("RUT ")
                                Vr = OcxFiscal.lineanofiscal("__________________________ ")
                                
                                Vr = OcxFiscal.lineanofiscal("==========================")
                            End If
                            Vr = OcxFiscal.cierranofiscal(0)
                    
                    
                    End If
                    
                    
                    
                    
                    
                    ' If SP_Rut_Activo = "76.370.578-1" Then
                    '    Vr = OcxFiscal.cierraboleta(0)    'talentos
                    'Else
                    '    Vr = OcxFiscal.cierraboleta(1)
                    'End If
                  
                    Vr = OcxFiscal.fini()
                    Bp_NoFiscal = False
                    Me.LvCuotas.ListItems.Clear
                    Me.TxtNombreCliente = ""
                    Me.TxtRutCliente = ""
        Unload Me
End Sub
Public Sub EmitaInformeZ()

                With OcxFiscal
                    Vr = .Init(IG_Puerto_Impresora_Fiscal)
                    If Vr = 0 Then
                        
                        MsgBox "No se pudo completar la boleta...", vbInformation
                        Exit Sub
                    End If
                   
                    
                    Vr = .cierrejornada()
                    Vr = .fini()
                End With
End Sub




Private Sub Command1_Click()
  
     
    Vr = OcxFiscal.Init(1)
    Vr = OcxFiscal.obtenernumboleta()
   
    Me.TxtNroBoletaFiscal = OcxFiscal.boleta
    Vr = OcxFiscal.fini()
End Sub

Private Sub Command2_Click()
    Vr = OcxFiscal.Init(1)
    Vr = OcxFiscal.obtenerestado()
    
    Me.TxtNroBoletaFiscal = OcxFiscal.Estado
    Vr = OcxFiscal.fini()
End Sub
Public Function EstadoSamgumn() As Integer
    Vr = OcxFiscal.Init(1)
    Vr = OcxFiscal.obtenerestado()
    TxtNroBoletaFiscal = OcxFiscal.Estado
    EstadoSamgumn = OcxFiscal.Estado
    Vr = OcxFiscal.fini()
End Function


