VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{28D47522-CF84-11D1-834C-00A0249F0C28}#1.0#0"; "gif89.dll"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{DE4917CD-D9B7-45E2-B0C7-DA1DB0A76109}#1.0#0"; "jhTextBoxM.ocx"
Begin VB.Form VtaDirecta 
   Caption         =   "VENTA DIRECTA"
   ClientHeight    =   10530
   ClientLeft      =   2955
   ClientTop       =   435
   ClientWidth     =   13230
   LinkMode        =   1  'Source
   LinkTopic       =   "Form1"
   ScaleHeight     =   10530
   ScaleWidth      =   13230
   Begin VB.CheckBox ChkActivoFijo 
      Caption         =   "Activo Fijo"
      Height          =   345
      Left            =   9975
      TabIndex        =   215
      Top             =   9450
      Width           =   2670
   End
   Begin VB.TextBox TxtBrutoNetoTYT 
      Height          =   330
      Left            =   13440
      TabIndex        =   214
      Text            =   "BRUTO"
      Top             =   2460
      Visible         =   0   'False
      Width           =   1755
   End
   Begin VB.Frame Frame7 
      Height          =   4200
      Left            =   13680
      TabIndex        =   199
      Top             =   4875
      Visible         =   0   'False
      Width           =   6840
      Begin MSComCtl2.DTPicker DtFechaDespacho 
         Height          =   375
         Left            =   3675
         TabIndex        =   200
         Top             =   1545
         Width           =   1440
         _ExtentX        =   2540
         _ExtentY        =   661
         _Version        =   393216
         Format          =   95420417
         CurrentDate     =   43935
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel23 
         Height          =   255
         Left            =   1410
         OleObjectBlob   =   "VtaDirecta6.frx":0000
         TabIndex        =   201
         Top             =   375
         Width           =   1965
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   255
         Index           =   2
         Left            =   1470
         OleObjectBlob   =   "VtaDirecta6.frx":007E
         TabIndex        =   202
         Top             =   795
         Width           =   1905
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   255
         Index           =   6
         Left            =   1410
         OleObjectBlob   =   "VtaDirecta6.frx":00F6
         TabIndex        =   203
         Top             =   1230
         Width           =   1965
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   255
         Index           =   1
         Left            =   1470
         OleObjectBlob   =   "VtaDirecta6.frx":0162
         TabIndex        =   204
         Top             =   1620
         Width           =   1905
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   255
         Index           =   3
         Left            =   1470
         OleObjectBlob   =   "VtaDirecta6.frx":01DC
         TabIndex        =   205
         Top             =   1995
         Width           =   1905
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel24 
         Height          =   255
         Left            =   1410
         OleObjectBlob   =   "VtaDirecta6.frx":025A
         TabIndex        =   206
         Top             =   2355
         Width           =   1965
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   255
         Index           =   4
         Left            =   105
         OleObjectBlob   =   "VtaDirecta6.frx":02D2
         TabIndex        =   207
         Top             =   2805
         Width           =   3270
      End
      Begin jhTextBoxM.TextBoxM TxtNombre 
         Height          =   375
         Left            =   3675
         TabIndex        =   208
         Top             =   375
         Width           =   2295
         _ExtentX        =   4048
         _ExtentY        =   661
         TipoDeDato      =   7
         Text            =   ""
         Estilo          =   4
         BackColorDisabled=   12632256
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   7.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin jhTextBoxM.TextBoxM TxtRutConductor 
         Height          =   375
         Left            =   3675
         TabIndex        =   209
         Top             =   765
         Width           =   2295
         _ExtentX        =   4048
         _ExtentY        =   661
         TipoDeDato      =   7
         Text            =   ""
         Estilo          =   4
         BackColorDisabled=   12632256
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   7.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin jhTextBoxM.TextBoxM TxtPatente 
         Height          =   375
         Left            =   3675
         TabIndex        =   210
         Top             =   1155
         Width           =   2295
         _ExtentX        =   4048
         _ExtentY        =   661
         TipoDeDato      =   7
         Text            =   ""
         Estilo          =   4
         BackColorDisabled=   12632256
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   7.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin jhTextBoxM.TextBoxM TxtDestinoDespacho 
         Height          =   375
         Left            =   3675
         TabIndex        =   211
         Top             =   1950
         Width           =   2295
         _ExtentX        =   4048
         _ExtentY        =   661
         TipoDeDato      =   7
         Text            =   ""
         Estilo          =   4
         BackColorDisabled=   12632256
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   7.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin jhTextBoxM.TextBoxM TxtRangoHorario 
         Height          =   375
         Left            =   3675
         TabIndex        =   212
         Top             =   2340
         Width           =   2295
         _ExtentX        =   4048
         _ExtentY        =   661
         TipoDeDato      =   7
         Text            =   ""
         Estilo          =   4
         BackColorDisabled=   12632256
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   7.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin jhTextBoxM.TextBoxM TxtProductoObjeto 
         Height          =   375
         Left            =   3675
         TabIndex        =   213
         Top             =   2730
         Width           =   2295
         _ExtentX        =   4048
         _ExtentY        =   661
         TipoDeDato      =   7
         Text            =   ""
         Estilo          =   4
         BackColorDisabled=   12632256
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   7.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.CommandButton CmdInfoTransporte 
      Caption         =   "Info Transporte"
      Height          =   390
      Left            =   60
      TabIndex        =   198
      Top             =   9375
      Width           =   2220
   End
   Begin VB.Frame FrmLoad 
      Height          =   1680
      Left            =   4395
      TabIndex        =   196
      Top             =   4950
      Visible         =   0   'False
      Width           =   2805
      Begin GIF89LibCtl.Gif89a Gif89a1 
         Height          =   1275
         Left            =   210
         OleObjectBlob   =   "VtaDirecta6.frx":0368
         TabIndex        =   197
         Top             =   285
         Width           =   2445
      End
   End
   Begin VB.Frame FrmAdicional 
      Caption         =   "Adicionales"
      Height          =   735
      Left            =   2550
      TabIndex        =   188
      Top             =   7020
      Visible         =   0   'False
      Width           =   4845
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel22 
         Height          =   180
         Index           =   0
         Left            =   450
         OleObjectBlob   =   "VtaDirecta6.frx":03EE
         TabIndex        =   191
         Top             =   210
         Width           =   1590
      End
      Begin VB.TextBox TxtSolicitante 
         Height          =   285
         Left            =   2310
         TabIndex        =   190
         Top             =   405
         Width           =   2400
      End
      Begin VB.TextBox TxtCentroCosto 
         Height          =   285
         Left            =   135
         TabIndex        =   189
         Top             =   405
         Width           =   2145
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel22 
         Height          =   180
         Index           =   1
         Left            =   2955
         OleObjectBlob   =   "VtaDirecta6.frx":0464
         TabIndex        =   192
         Top             =   210
         Width           =   975
      End
   End
   Begin VB.TextBox TxteMail 
      Height          =   360
      Left            =   14430
      Locked          =   -1  'True
      TabIndex        =   187
      Text            =   "Text2"
      Top             =   2775
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.ComboBox CboSucEmpresa 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      ItemData        =   "VtaDirecta6.frx":04D8
      Left            =   10920
      List            =   "VtaDirecta6.frx":04DA
      Style           =   2  'Dropdown List
      TabIndex        =   186
      Top             =   720
      Width           =   1920
   End
   Begin VB.Frame Frame6 
      Caption         =   "Referenciar guias (Factura Electronica)"
      Height          =   1335
      Left            =   2355
      TabIndex        =   177
      Top             =   9075
      Width           =   7230
      Begin VB.CommandButton CmdAgregaRefGuia 
         Caption         =   "Agregar"
         Height          =   300
         Left            =   6105
         TabIndex        =   182
         Top             =   195
         Width           =   1005
      End
      Begin VB.TextBox TxtNroGuiaRef 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   3405
         TabIndex        =   180
         ToolTipText     =   "Nro de Documento"
         Top             =   195
         Width           =   1320
      End
      Begin VB.ComboBox CboGuiasRef 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         ItemData        =   "VtaDirecta6.frx":04DC
         Left            =   450
         List            =   "VtaDirecta6.frx":04DE
         Style           =   2  'Dropdown List
         TabIndex        =   178
         Top             =   195
         Width           =   2970
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel15 
         Height          =   225
         Left            =   -900
         OleObjectBlob   =   "VtaDirecta6.frx":04E0
         TabIndex        =   179
         Top             =   255
         Width           =   1260
      End
      Begin MSComCtl2.DTPicker DtGuiaRef 
         Height          =   315
         Left            =   4740
         TabIndex        =   181
         Top             =   195
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   556
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   95420417
         CurrentDate     =   39857
      End
      Begin MSComctlLib.ListView LvGuiasReferencia 
         Height          =   780
         Left            =   450
         TabIndex        =   183
         Top             =   510
         Width           =   6675
         _ExtentX        =   11774
         _ExtentY        =   1376
         View            =   3
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         HideColumnHeaders=   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   6
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "T1000"
            Text            =   "id venta"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "T1300"
            Text            =   "tipo"
            Object.Width           =   5248
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "N109"
            Text            =   "nro"
            Object.Width           =   2337
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Object.Tag             =   "T800"
            Text            =   "fecha"
            Object.Width           =   2364
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Object.Tag             =   "N109"
            Text            =   "cod SII"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Object.Tag             =   "N109"
            Text            =   "doc_siiiiii"
            Object.Width           =   0
         EndProperty
      End
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Command1"
      Height          =   225
      Left            =   14355
      TabIndex        =   176
      Top             =   7440
      Width           =   675
   End
   Begin VB.TextBox TxtCodigoSIIILA 
      Height          =   285
      Left            =   13605
      TabIndex        =   175
      Text            =   "Text2"
      Top             =   3105
      Visible         =   0   'False
      Width           =   1005
   End
   Begin VB.CommandButton CmdRefectura 
      Caption         =   "Re-procesar"
      Height          =   285
      Left            =   1095
      TabIndex        =   161
      Top             =   9015
      Width           =   1245
   End
   Begin VB.TextBox TxtProIvaAnticipado 
      Height          =   285
      Left            =   16680
      TabIndex        =   160
      Text            =   "Text2"
      Top             =   2310
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.ComboBox CboGenero 
      Height          =   315
      ItemData        =   "VtaDirecta6.frx":0546
      Left            =   90
      List            =   "VtaDirecta6.frx":0548
      Style           =   2  'Dropdown List
      TabIndex        =   157
      Top             =   9000
      Width           =   1005
   End
   Begin VB.Frame FrmDescripcion 
      Caption         =   "Descripcion Larga"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   825
      Left            =   2295
      TabIndex        =   141
      Top             =   2715
      Visible         =   0   'False
      Width           =   9870
      Begin VB.Frame Frame5 
         Caption         =   "Ubicaci�n del art�culo"
         Height          =   780
         Left            =   6750
         TabIndex        =   155
         Top             =   45
         Width           =   3225
         Begin ACTIVESKINLibCtl.SkinLabel SkUbicacion 
            Height          =   315
            Left            =   135
            OleObjectBlob   =   "VtaDirecta6.frx":054A
            TabIndex        =   156
            Top             =   420
            Width           =   2955
         End
      End
      Begin ACTIVESKINLibCtl.SkinLabel skDescripcionLarga 
         Height          =   480
         Left            =   120
         OleObjectBlob   =   "VtaDirecta6.frx":05C2
         TabIndex        =   142
         Top             =   360
         Width           =   7500
      End
   End
   Begin VB.Frame FraProcesandoDTE 
      Height          =   1080
      Left            =   60
      TabIndex        =   151
      Top             =   4605
      Visible         =   0   'False
      Width           =   13020
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel13 
         Height          =   510
         Left            =   1800
         OleObjectBlob   =   "VtaDirecta6.frx":0644
         TabIndex        =   152
         Top             =   360
         Width           =   10275
      End
   End
   Begin MSComctlLib.ListView LvMateriales 
      Height          =   2310
      Left            =   300
      TabIndex        =   146
      Top             =   4035
      Width           =   12525
      _ExtentX        =   22093
      _ExtentY        =   4075
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   0   'False
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   0
      NumItems        =   27
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Nro Orden"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Codigo"
         Object.Width           =   2452
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "Marca"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Text            =   "Descripcion"
         Object.Width           =   6174
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   4
         Object.Tag             =   "N100"
         Text            =   "Precio"
         Object.Width           =   1676
      EndProperty
      BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   5
         Text            =   "Dscto."
         Object.Width           =   1676
      EndProperty
      BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   6
         Text            =   "Un."
         Object.Width           =   1235
      EndProperty
      BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   7
         Object.Tag             =   "N100"
         Text            =   "Precio U."
         Object.Width           =   1676
      EndProperty
      BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   8
         Object.Tag             =   "N100"
         Text            =   "Total"
         Object.Width           =   1940
      EndProperty
      BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   9
         Text            =   "Id Familia"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   10
         Text            =   "Costo"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(12) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   11
         Text            =   "Nro de Ot"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(13) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   12
         Text            =   "especial"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(14) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   13
         Text            =   "Comision"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(15) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   14
         Text            =   "Fec. Salida"
         Object.Width           =   2187
      EndProperty
      BeginProperty ColumnHeader(16) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   15
         Text            =   "Utilidad"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(17) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   16
         Object.Tag             =   "T100"
         Text            =   "Inventariable"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(18) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   17
         Object.Tag             =   "N109"
         Text            =   "Cuenta"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(19) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   18
         Object.Tag             =   "N109"
         Text            =   "ARea"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(20) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   19
         Object.Tag             =   "N109"
         Text            =   "CentroCosto"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(21) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   20
         Object.Tag             =   "N109"
         Text            =   "Codigo Activo Inmovilizado"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(22) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   21
         Object.Tag             =   "T1000"
         Text            =   "Unidad Medida"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(23) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   22
         Object.Tag             =   "N109"
         Text            =   "Monto Iva anticipado,"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(24) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   23
         Object.Tag             =   "N109"
         Text            =   " calculo sobre el neto Codigo Iva Anticipado"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(25) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   24
         Object.Tag             =   "N102"
         Text            =   "ILA %"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(26) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   25
         Object.Tag             =   "N109"
         Text            =   "codigo SII ila"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(27) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   26
         Object.Tag             =   "N109"
         Text            =   "Codigo Sistema ILA"
         Object.Width           =   2540
      EndProperty
   End
   Begin VB.TextBox TxtNuevoTotal 
      Alignment       =   1  'Right Justify
      Height          =   240
      Left            =   8490
      TabIndex        =   145
      Text            =   "0"
      Top             =   8745
      Visible         =   0   'False
      Width           =   1440
   End
   Begin VB.Frame FramGuias 
      Caption         =   "Guias pendientes de facturacion"
      Height          =   3345
      Left            =   13185
      TabIndex        =   69
      Top             =   3465
      Visible         =   0   'False
      Width           =   13035
      Begin VB.Frame Frame4 
         Caption         =   "Presentacion de Factura"
         Height          =   570
         Left            =   7275
         TabIndex        =   122
         Top             =   2295
         Width           =   5595
         Begin VB.OptionButton Option1 
            Caption         =   "Segun guias 1,2,3...."
            Height          =   330
            Left            =   240
            TabIndex        =   124
            ToolTipText     =   "En la factura aparecera Segun guias Nro 1,2,4,5,6 ..."
            Top             =   180
            Value           =   -1  'True
            Width           =   2055
         End
         Begin VB.OptionButton Option2 
            Caption         =   "Productos Vendidos"
            Height          =   330
            Left            =   2805
            TabIndex        =   123
            ToolTipText     =   "Resumen de los productos vendidos"
            Top             =   195
            Width           =   2055
         End
      End
      Begin MSComctlLib.ListView LvDetalle 
         Height          =   1950
         Left            =   240
         TabIndex        =   144
         Top             =   255
         Width           =   12540
         _ExtentX        =   22119
         _ExtentY        =   3440
         View            =   3
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   8
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "T1000"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "T1300"
            Text            =   "Fecha"
            Object.Width           =   2381
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "N109"
            Text            =   "Nro Guia"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Object.Tag             =   "T800"
            Text            =   "Rut"
            Object.Width           =   2469
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Object.Tag             =   "T3000"
            Text            =   "Nombre Proveedor"
            Object.Width           =   6703
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   5
            Object.Tag             =   "N100"
            Text            =   "Neto"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   6
            Object.Tag             =   "N100"
            Text            =   "IVA"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   7
            Object.Tag             =   "N100"
            Text            =   "Total"
            Object.Width           =   2117
         EndProperty
      End
   End
   Begin VB.Frame FrameContable 
      Caption         =   "Datos Contables"
      Height          =   885
      Left            =   135
      TabIndex        =   71
      Top             =   2490
      Visible         =   0   'False
      Width           =   12765
      Begin VB.CommandButton CmdBuscaActivo 
         Caption         =   "A"
         Height          =   315
         Left            =   6360
         TabIndex        =   154
         ToolTipText     =   "Buscar Activo Inmovilizado"
         Top             =   570
         Visible         =   0   'False
         Width           =   195
      End
      Begin VB.TextBox TxtNombreActivo 
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   7140
         Locked          =   -1  'True
         TabIndex        =   129
         TabStop         =   0   'False
         Tag             =   "N2"
         ToolTipText     =   "Ingrese el codigo de la cuenta"
         Top             =   555
         Width           =   2040
      End
      Begin VB.TextBox TxtCodigoActivo 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   6555
         Locked          =   -1  'True
         TabIndex        =   128
         TabStop         =   0   'False
         Tag             =   "N2"
         ToolTipText     =   "Codigo del activo inmovilizado"
         Top             =   555
         Width           =   615
      End
      Begin VB.TextBox TxtNroCuenta 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   270
         Left            =   1155
         TabIndex        =   127
         Tag             =   "N2"
         ToolTipText     =   "Ingrese el codigo de la cuenta"
         Top             =   285
         Width           =   930
      End
      Begin VB.CommandButton CmdModifica 
         Caption         =   "Modificar"
         Height          =   210
         Left            =   11550
         TabIndex        =   117
         ToolTipText     =   "Modifica datos contables de la linea"
         Top             =   135
         Width           =   1170
      End
      Begin VB.CommandButton CmdOkSd 
         Caption         =   "Ok"
         Height          =   300
         Left            =   12300
         TabIndex        =   96
         Top             =   555
         Visible         =   0   'False
         Width           =   405
      End
      Begin VB.TextBox TxtSdExento 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   9165
         TabIndex        =   93
         Tag             =   "N"
         Text            =   "0"
         Top             =   555
         Visible         =   0   'False
         Width           =   1050
      End
      Begin VB.TextBox TxtSDNeto 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   10200
         TabIndex        =   94
         Tag             =   "N"
         Text            =   "0"
         Top             =   555
         Visible         =   0   'False
         Width           =   1050
      End
      Begin VB.TextBox TxtSdIva 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   11235
         TabIndex        =   95
         Tag             =   "N"
         Text            =   "0"
         Top             =   555
         Visible         =   0   'False
         Width           =   1050
      End
      Begin VB.CommandButton cmdCuenta 
         Caption         =   "Cuenta"
         Height          =   240
         Left            =   165
         TabIndex        =   100
         Top             =   285
         Width           =   960
      End
      Begin VB.ComboBox CboArea 
         Height          =   315
         Left            =   2835
         Style           =   2  'Dropdown List
         TabIndex        =   91
         Top             =   555
         Width           =   1755
      End
      Begin VB.ComboBox CboCuenta 
         Height          =   315
         Left            =   60
         Style           =   2  'Dropdown List
         TabIndex        =   90
         ToolTipText     =   "Para cambiar CUENTA doble click en linea de detalle"
         Top             =   555
         Width           =   2805
      End
      Begin VB.ComboBox CboCentroCosto 
         Height          =   315
         Left            =   4575
         Style           =   2  'Dropdown List
         TabIndex        =   92
         Top             =   555
         Width           =   1815
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel14 
         Height          =   180
         Index           =   5
         Left            =   4560
         OleObjectBlob   =   "VtaDirecta6.frx":070C
         TabIndex        =   101
         Top             =   360
         Width           =   2055
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel14 
         Height          =   180
         Index           =   2
         Left            =   2820
         OleObjectBlob   =   "VtaDirecta6.frx":0788
         TabIndex        =   102
         Top             =   360
         Width           =   2055
      End
      Begin ACTIVESKINLibCtl.SkinLabel SksdExento 
         Height          =   180
         Left            =   9195
         OleObjectBlob   =   "VtaDirecta6.frx":07EE
         TabIndex        =   105
         Top             =   360
         Visible         =   0   'False
         Width           =   990
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkSdneto 
         Height          =   180
         Left            =   10245
         OleObjectBlob   =   "VtaDirecta6.frx":0858
         TabIndex        =   106
         Top             =   360
         Visible         =   0   'False
         Width           =   990
      End
      Begin ACTIVESKINLibCtl.SkinLabel SksdIva 
         Height          =   180
         Left            =   11790
         OleObjectBlob   =   "VtaDirecta6.frx":08BE
         TabIndex        =   107
         Top             =   360
         Visible         =   0   'False
         Width           =   480
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkAi 
         Height          =   180
         Index           =   1
         Left            =   6615
         OleObjectBlob   =   "VtaDirecta6.frx":0922
         TabIndex        =   130
         Top             =   375
         Width           =   540
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkAi 
         Height          =   180
         Index           =   2
         Left            =   7140
         OleObjectBlob   =   "VtaDirecta6.frx":0986
         TabIndex        =   131
         Top             =   375
         Width           =   1980
      End
      Begin MSComctlLib.ListView LvSinDetalle 
         Height          =   3015
         Left            =   60
         TabIndex        =   139
         Top             =   870
         Width           =   12630
         _ExtentX        =   22278
         _ExtentY        =   5318
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   0   'False
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   12
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "N100"
            Text            =   "id"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "T1000"
            Text            =   "Cuenta"
            Object.Width           =   3704
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "T1500"
            Text            =   "Area"
            Object.Width           =   3704
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Object.Tag             =   "T3000"
            Text            =   "Centro de Costo"
            Object.Width           =   3704
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Object.Tag             =   "T1000"
            Text            =   "Cod"
            Object.Width           =   1438
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Object.Tag             =   "T1000"
            Text            =   "Nombre Activo"
            Object.Width           =   3598
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   6
            Key             =   "otros"
            Object.Tag             =   "N100"
            Text            =   "Exento"
            Object.Width           =   1852
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   7
            Key             =   "neto"
            Object.Tag             =   "N100"
            Text            =   "Neto"
            Object.Width           =   1852
         EndProperty
         BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   8
            Key             =   "iva"
            Object.Tag             =   "N100"
            Text            =   "IVA"
            Object.Width           =   1852
         EndProperty
         BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   9
            Object.Tag             =   "N109"
            Text            =   "PLA_ID"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   10
            Object.Tag             =   "N109"
            Text            =   "ARE_ID"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(12) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   11
            Object.Tag             =   "N109"
            Text            =   "CEN_ID"
            Object.Width           =   0
         EndProperty
      End
   End
   Begin VB.Frame FraModalidad 
      Caption         =   "Modalidad"
      Height          =   1110
      Left            =   10860
      TabIndex        =   134
      Top             =   180
      Visible         =   0   'False
      Width           =   2070
      Begin VB.ComboBox CboModalidad 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   420
         ItemData        =   "VtaDirecta6.frx":0A18
         Left            =   150
         List            =   "VtaDirecta6.frx":0A22
         Style           =   2  'Dropdown List
         TabIndex        =   135
         Top             =   225
         Width           =   1785
      End
   End
   Begin VB.TextBox TxtComentario 
      Height          =   285
      Left            =   2265
      TabIndex        =   120
      Tag             =   "T"
      Top             =   6450
      Width           =   6840
   End
   Begin VB.CommandButton CmdBuscaProducto 
      Caption         =   "B"
      Height          =   255
      Left            =   1395
      TabIndex        =   132
      ToolTipText     =   "Busqueda de productos"
      Top             =   3765
      Width           =   225
   End
   Begin VB.Frame frmEspecial 
      Caption         =   "Detalle"
      Height          =   3705
      Left            =   13605
      TabIndex        =   56
      Top             =   8970
      Visible         =   0   'False
      Width           =   12800
      Begin VB.TextBox txtNeto 
         Alignment       =   1  'Right Justify
         Height          =   405
         Left            =   5280
         Locked          =   -1  'True
         TabIndex        =   37
         Top             =   3120
         Width           =   1935
      End
      Begin VB.TextBox txtespecial 
         Height          =   280
         Index           =   5
         Left            =   1920
         Locked          =   -1  'True
         MaxLength       =   45
         MultiLine       =   -1  'True
         TabIndex        =   32
         Top             =   1560
         Width           =   5295
      End
      Begin VB.TextBox txtespecial 
         Height          =   280
         Index           =   9
         Left            =   1920
         Locked          =   -1  'True
         MaxLength       =   45
         TabIndex        =   36
         Top             =   2520
         Width           =   5295
      End
      Begin VB.TextBox txtespecial 
         Height          =   280
         Index           =   8
         Left            =   1920
         Locked          =   -1  'True
         MaxLength       =   45
         TabIndex        =   35
         Top             =   2280
         Width           =   5295
      End
      Begin VB.TextBox txtespecial 
         Height          =   280
         Index           =   7
         Left            =   1920
         Locked          =   -1  'True
         MaxLength       =   45
         TabIndex        =   34
         Top             =   2040
         Width           =   5295
      End
      Begin VB.TextBox txtespecial 
         Height          =   280
         Index           =   6
         Left            =   1920
         Locked          =   -1  'True
         MaxLength       =   45
         TabIndex        =   33
         Top             =   1800
         Width           =   5295
      End
      Begin VB.TextBox txtespecial 
         Height          =   280
         Index           =   1
         Left            =   1920
         LinkTimeout     =   45
         Locked          =   -1  'True
         MaxLength       =   45
         TabIndex        =   28
         Top             =   600
         Width           =   5295
      End
      Begin VB.TextBox txtespecial 
         Height          =   280
         Index           =   2
         Left            =   1920
         Locked          =   -1  'True
         MaxLength       =   45
         TabIndex        =   29
         Top             =   810
         Width           =   5295
      End
      Begin VB.TextBox txtespecial 
         Height          =   280
         Index           =   3
         Left            =   1920
         Locked          =   -1  'True
         MaxLength       =   45
         TabIndex        =   30
         Top             =   1080
         Width           =   5295
      End
      Begin VB.TextBox txtespecial 
         Height          =   280
         Index           =   4
         Left            =   1920
         Locked          =   -1  'True
         MaxLength       =   45
         TabIndex        =   31
         Top             =   1320
         Width           =   5295
      End
      Begin VB.CommandButton cmdCancel 
         Caption         =   "Descartar Especial"
         Height          =   375
         Left            =   9360
         TabIndex        =   38
         Top             =   3240
         Width           =   1695
      End
      Begin VB.TextBox txtespecial 
         Height          =   280
         Index           =   0
         Left            =   1920
         Locked          =   -1  'True
         MaxLength       =   45
         MultiLine       =   -1  'True
         TabIndex        =   27
         Top             =   360
         Width           =   5295
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   255
         Index           =   1
         Left            =   5880
         OleObjectBlob   =   "VtaDirecta6.frx":0A37
         TabIndex        =   57
         Top             =   2880
         Width           =   1335
      End
   End
   Begin MSComDlg.CommonDialog Dialogo 
      Left            =   45
      Top             =   9405
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.TextBox TxtComisionVendedor 
      Alignment       =   1  'Right Justify
      Height          =   285
      Left            =   210
      Locked          =   -1  'True
      TabIndex        =   116
      Top             =   9840
      Visible         =   0   'False
      Width           =   1635
   End
   Begin VB.CommandButton CmdAnularDocumento 
      Caption         =   "Anula Documento"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   210
      Left            =   90
      TabIndex        =   103
      ToolTipText     =   "Dejar documento como nulo"
      Top             =   10170
      Width           =   1830
   End
   Begin VB.Frame FrameDoc 
      Caption         =   "Documento"
      Height          =   2175
      Left            =   60
      TabIndex        =   74
      Top             =   6825
      Width           =   13005
      Begin VB.TextBox TxtNotasPedido 
         Height          =   285
         Left            =   6240
         MaxLength       =   18
         TabIndex        =   194
         Top             =   1140
         Width           =   1230
      End
      Begin MSComCtl2.UpDown UpDown2 
         Height          =   270
         Left            =   10095
         TabIndex        =   171
         Top             =   525
         Visible         =   0   'False
         Width           =   255
         _ExtentX        =   423
         _ExtentY        =   476
         _Version        =   393216
         Enabled         =   -1  'True
      End
      Begin MSComCtl2.UpDown UpDown1 
         Height          =   270
         Left            =   10095
         TabIndex        =   170
         Top             =   210
         Visible         =   0   'False
         Width           =   255
         _ExtentX        =   423
         _ExtentY        =   476
         _Version        =   393216
         Enabled         =   -1  'True
      End
      Begin MSComCtl2.DTPicker DtFechaOC 
         Height          =   285
         Left            =   4380
         TabIndex        =   169
         Top             =   1155
         Width           =   1245
         _ExtentX        =   2196
         _ExtentY        =   503
         _Version        =   393216
         Format          =   95420417
         CurrentDate     =   42572
      End
      Begin VB.TextBox TxtCreditoEmpresaConstructora 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   285
         Left            =   8940
         TabIndex        =   168
         Text            =   "0"
         Top             =   1125
         Visible         =   0   'False
         Width           =   795
      End
      Begin VB.CommandButton CmdIvaConstructoras 
         Caption         =   "Iva Constructora"
         CausesValidation=   0   'False
         Height          =   240
         Left            =   8880
         TabIndex        =   167
         Top             =   870
         Visible         =   0   'False
         Width           =   975
      End
      Begin VB.ComboBox CboTipoTraslado 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   4680
         Style           =   2  'Dropdown List
         TabIndex        =   164
         Top             =   1785
         Visible         =   0   'False
         Width           =   5280
      End
      Begin VB.ComboBox CboTipoDespacho 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   4680
         Style           =   2  'Dropdown List
         TabIndex        =   163
         Top             =   1470
         Visible         =   0   'False
         Width           =   5280
      End
      Begin VB.CheckBox ChkCedible 
         Caption         =   "Cedible"
         Height          =   195
         Left            =   2490
         TabIndex        =   162
         Top             =   1575
         Visible         =   0   'False
         Width           =   825
      End
      Begin VB.TextBox TxtIvaAnticipado 
         Alignment       =   1  'Right Justify
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   11265
         TabIndex        =   158
         TabStop         =   0   'False
         Tag             =   "N"
         Text            =   "0"
         ToolTipText     =   "Valor IVA retenido"
         Top             =   1350
         Width           =   1440
      End
      Begin VB.CommandButton CmdXML 
         Caption         =   "Generar XML"
         Height          =   135
         Left            =   75
         TabIndex        =   150
         Top             =   1860
         Visible         =   0   'False
         Width           =   2385
      End
      Begin VB.TextBox txtNuevoDescuento 
         Height          =   390
         Left            =   10155
         TabIndex        =   70
         Text            =   "Text2"
         Top             =   2040
         Visible         =   0   'False
         Width           =   885
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel11 
         Height          =   195
         Left            =   6780
         OleObjectBlob   =   "VtaDirecta6.frx":0AA9
         TabIndex        =   104
         Top             =   705
         Visible         =   0   'False
         Width           =   1890
      End
      Begin VB.TextBox txtOriginal 
         Height          =   390
         Left            =   8265
         TabIndex        =   119
         Text            =   "Text2"
         Top             =   2085
         Visible         =   0   'False
         Width           =   2160
      End
      Begin VB.ComboBox CboEntregaInmediata 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         ItemData        =   "VtaDirecta6.frx":0B25
         Left            =   1530
         List            =   "VtaDirecta6.frx":0B2F
         Style           =   2  'Dropdown List
         TabIndex        =   136
         Top             =   270
         Width           =   870
      End
      Begin VB.TextBox TxtOrdenesCompra 
         Height          =   285
         Left            =   3150
         MaxLength       =   18
         TabIndex        =   125
         Top             =   1155
         Width           =   1230
      End
      Begin VB.TextBox TxtIVARetenido 
         Alignment       =   1  'Right Justify
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   11265
         TabIndex        =   112
         TabStop         =   0   'False
         Tag             =   "N"
         Text            =   "0"
         ToolTipText     =   "Valor IVA retenido"
         Top             =   1020
         Width           =   1440
      End
      Begin VB.ComboBox CboFpago 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         ItemData        =   "VtaDirecta6.frx":0B3B
         Left            =   75
         List            =   "VtaDirecta6.frx":0B45
         Style           =   2  'Dropdown List
         TabIndex        =   110
         Top             =   810
         Width           =   2355
      End
      Begin VB.TextBox TxtExentos 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   11265
         Locked          =   -1  'True
         TabIndex        =   108
         Text            =   "0"
         ToolTipText     =   "Nro de Documento"
         Top             =   180
         Width           =   1440
      End
      Begin VB.TextBox SkBrutoMateriales 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   11265
         Locked          =   -1  'True
         TabIndex        =   99
         Text            =   "0"
         ToolTipText     =   "Nro de Documento"
         Top             =   1665
         Width           =   1440
      End
      Begin VB.TextBox SkIvaMateriales 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   11265
         Locked          =   -1  'True
         TabIndex        =   98
         TabStop         =   0   'False
         Text            =   "0"
         ToolTipText     =   "Nro de Documento"
         Top             =   705
         Width           =   1440
      End
      Begin VB.TextBox SkTotalMateriales 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   11265
         Locked          =   -1  'True
         TabIndex        =   97
         Text            =   "0"
         ToolTipText     =   "Nro de Documento"
         Top             =   450
         Width           =   1440
      End
      Begin VB.CommandButton CmdEmiteDocumento 
         Caption         =   "Emitir Documento - F5"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   660
         Left            =   75
         TabIndex        =   89
         ToolTipText     =   "Si lleva control de INVENTARIO, no puede modifcar este documento. S�... puede eliminar con N.C. u otro documento"
         Top             =   1095
         Width           =   2385
      End
      Begin VB.TextBox TxtSubTotalMateriales 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   9045
         Locked          =   -1  'True
         TabIndex        =   83
         Text            =   "0"
         Top             =   210
         Width           =   1035
      End
      Begin VB.TextBox TxtTotalDescuento 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   9045
         Locked          =   -1  'True
         TabIndex        =   82
         Text            =   "0"
         Top             =   510
         Width           =   1035
      End
      Begin VB.Frame FrmReferencia 
         Caption         =   "Referencia"
         Height          =   900
         Left            =   2460
         TabIndex        =   75
         Top             =   240
         Visible         =   0   'False
         Width           =   5175
         Begin ACTIVESKINLibCtl.SkinLabel SkDocIdReferencia 
            Height          =   285
            Left            =   2400
            OleObjectBlob   =   "VtaDirecta6.frx":0B5B
            TabIndex        =   149
            Top             =   1485
            Width           =   1485
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkFechaReferencia 
            Height          =   315
            Left            =   210
            OleObjectBlob   =   "VtaDirecta6.frx":0BC7
            TabIndex        =   148
            Top             =   1425
            Width           =   1500
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkCodigoReferencia 
            Height          =   270
            Left            =   225
            OleObjectBlob   =   "VtaDirecta6.frx":0C37
            TabIndex        =   147
            Top             =   1020
            Width           =   1800
         End
         Begin VB.TextBox TxtMotivoRef 
            BackColor       =   &H00E0E0E0&
            Height          =   285
            Left            =   1110
            Locked          =   -1  'True
            TabIndex        =   79
            Text            =   "-"
            Top             =   180
            Width           =   4005
         End
         Begin VB.TextBox TxtNroReferencia 
            BackColor       =   &H00E0E0E0&
            Height          =   285
            Left            =   4305
            Locked          =   -1  'True
            TabIndex        =   77
            Tag             =   "0"
            Top             =   510
            Width           =   780
         End
         Begin VB.TextBox TxtDocReferencia 
            BackColor       =   &H00E0E0E0&
            Height          =   285
            Left            =   1095
            Locked          =   -1  'True
            TabIndex        =   76
            Top             =   510
            Width           =   2775
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel6 
            Height          =   225
            Left            =   75
            OleObjectBlob   =   "VtaDirecta6.frx":0CC3
            TabIndex        =   78
            Top             =   255
            Width           =   930
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel7 
            Height          =   225
            Left            =   3900
            OleObjectBlob   =   "VtaDirecta6.frx":0D2D
            TabIndex        =   80
            Top             =   540
            Width           =   345
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel8 
            Height          =   225
            Left            =   90
            OleObjectBlob   =   "VtaDirecta6.frx":0D91
            TabIndex        =   81
            Top             =   525
            Width           =   960
         End
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel25 
         Height          =   255
         Left            =   7740
         OleObjectBlob   =   "VtaDirecta6.frx":0E01
         TabIndex        =   84
         Top             =   510
         Width           =   1335
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel17 
         Height          =   255
         Index           =   0
         Left            =   8955
         OleObjectBlob   =   "VtaDirecta6.frx":0E7F
         TabIndex        =   85
         Top             =   525
         Width           =   2295
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel26 
         Height          =   255
         Left            =   8355
         OleObjectBlob   =   "VtaDirecta6.frx":0EF1
         TabIndex        =   86
         Top             =   210
         Width           =   735
      End
      Begin ACTIVESKINLibCtl.SkinLabel skLiva 
         Height          =   255
         Left            =   8955
         OleObjectBlob   =   "VtaDirecta6.frx":0F5F
         TabIndex        =   87
         Top             =   765
         Width           =   2295
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel17 
         Height          =   225
         Index           =   2
         Left            =   8955
         OleObjectBlob   =   "VtaDirecta6.frx":0FC7
         TabIndex        =   88
         Top             =   1725
         Width           =   2295
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel17 
         Height          =   255
         Index           =   1
         Left            =   8955
         OleObjectBlob   =   "VtaDirecta6.frx":102F
         TabIndex        =   109
         Top             =   225
         Width           =   2295
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   225
         Index           =   2
         Left            =   90
         OleObjectBlob   =   "VtaDirecta6.frx":1099
         TabIndex        =   111
         Top             =   615
         Width           =   1575
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel10 
         Height          =   255
         Left            =   8955
         OleObjectBlob   =   "VtaDirecta6.frx":110A
         TabIndex        =   113
         Top             =   1095
         Width           =   2295
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   225
         Index           =   3
         Left            =   2565
         OleObjectBlob   =   "VtaDirecta6.frx":1180
         TabIndex        =   126
         Top             =   1185
         Width           =   555
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   225
         Index           =   4
         Left            =   120
         OleObjectBlob   =   "VtaDirecta6.frx":11E3
         TabIndex        =   137
         Top             =   285
         Width           =   1395
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkIvaAnticipado 
         Height          =   255
         Left            =   8970
         OleObjectBlob   =   "VtaDirecta6.frx":125C
         TabIndex        =   159
         Top             =   1425
         Width           =   2295
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkTdespacho 
         Height          =   225
         Left            =   3315
         OleObjectBlob   =   "VtaDirecta6.frx":12D6
         TabIndex        =   165
         Top             =   1515
         Visible         =   0   'False
         Width           =   1260
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkTTraslado 
         Height          =   225
         Left            =   3330
         OleObjectBlob   =   "VtaDirecta6.frx":134E
         TabIndex        =   166
         Top             =   1845
         Visible         =   0   'False
         Width           =   1260
      End
      Begin MSComCtl2.UpDown UpDown3 
         Height          =   270
         Left            =   12705
         TabIndex        =   172
         Top             =   450
         Visible         =   0   'False
         Width           =   255
         _ExtentX        =   423
         _ExtentY        =   476
         _Version        =   393216
         Enabled         =   -1  'True
      End
      Begin MSComCtl2.UpDown UpDown4 
         Height          =   270
         Left            =   12705
         TabIndex        =   173
         Top             =   750
         Visible         =   0   'False
         Width           =   255
         _ExtentX        =   423
         _ExtentY        =   476
         _Version        =   393216
         Enabled         =   -1  'True
      End
      Begin MSComCtl2.UpDown UpDown5 
         Height          =   270
         Left            =   12675
         TabIndex        =   174
         Top             =   1695
         Visible         =   0   'False
         Width           =   255
         _ExtentX        =   423
         _ExtentY        =   476
         _Version        =   393216
         Enabled         =   -1  'True
      End
      Begin MSComCtl2.DTPicker DtFechaNP 
         Height          =   285
         Left            =   7470
         TabIndex        =   193
         Top             =   1140
         Width           =   1245
         _ExtentX        =   2196
         _ExtentY        =   503
         _Version        =   393216
         Format          =   95420417
         CurrentDate     =   42572
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   225
         Index           =   5
         Left            =   5655
         OleObjectBlob   =   "VtaDirecta6.frx":13C6
         TabIndex        =   195
         Top             =   1170
         Width           =   555
      End
   End
   Begin VB.Timer Timer2 
      Interval        =   10
      Left            =   12930
      Top             =   8970
   End
   Begin VB.Frame Frame2 
      Caption         =   "Datos Venta"
      Height          =   1110
      Left            =   90
      TabIndex        =   63
      Top             =   180
      Width           =   12825
      Begin VB.Frame Frame1 
         Caption         =   "Documento de Venta"
         Height          =   1110
         Left            =   6315
         TabIndex        =   67
         Top             =   0
         Width           =   4440
         Begin ACTIVESKINLibCtl.SkinLabel SkFoliosDisponibles 
            Height          =   195
            Left            =   195
            OleObjectBlob   =   "VtaDirecta6.frx":1429
            TabIndex        =   153
            Top             =   855
            Width           =   3660
         End
         Begin VB.TextBox TxtNroDocumento 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   3255
            TabIndex        =   4
            ToolTipText     =   "Nro de Documento"
            Top             =   510
            Width           =   1140
         End
         Begin VB.ComboBox CboDocVenta 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            ItemData        =   "VtaDirecta6.frx":148B
            Left            =   210
            List            =   "VtaDirecta6.frx":148D
            Style           =   2  'Dropdown List
            TabIndex        =   3
            ToolTipText     =   "Seleccione Documento de  venta. Ventas con Retenci�n, cuando el contribuyente recibe factura de terceros "
            Top             =   510
            Width           =   3060
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
            Height          =   210
            Left            =   3105
            OleObjectBlob   =   "VtaDirecta6.frx":148F
            TabIndex        =   68
            Top             =   285
            Width           =   1260
         End
      End
      Begin VB.ComboBox CboPlazos 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         ItemData        =   "VtaDirecta6.frx":1509
         Left            =   1575
         List            =   "VtaDirecta6.frx":150B
         Style           =   2  'Dropdown List
         TabIndex        =   1
         Top             =   615
         Width           =   1860
      End
      Begin VB.Frame Frame3 
         Caption         =   "Bodega Entrega"
         Height          =   1110
         Left            =   12600
         TabIndex        =   114
         Top             =   0
         Visible         =   0   'False
         Width           =   2490
         Begin VB.ComboBox CboBodega 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            ItemData        =   "VtaDirecta6.frx":150D
            Left            =   90
            List            =   "VtaDirecta6.frx":151A
            Style           =   2  'Dropdown List
            TabIndex        =   115
            ToolTipText     =   "Seleccione Bodega"
            Top             =   600
            Width           =   2355
         End
      End
      Begin VB.ComboBox CboVendedores 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   3450
         Style           =   2  'Dropdown List
         TabIndex        =   2
         Top             =   615
         Width           =   2880
      End
      Begin ACTIVESKINLibCtl.SkinLabel Skvendedor 
         Height          =   180
         Left            =   3435
         OleObjectBlob   =   "VtaDirecta6.frx":153F
         TabIndex        =   64
         Top             =   450
         Width           =   810
      End
      Begin MSComCtl2.DTPicker DtFecha 
         Height          =   315
         Left            =   225
         TabIndex        =   0
         Top             =   615
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   556
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   95420417
         CurrentDate     =   39857
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   255
         Index           =   0
         Left            =   255
         OleObjectBlob   =   "VtaDirecta6.frx":15A6
         TabIndex        =   65
         Top             =   420
         Width           =   1035
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel9 
         Height          =   285
         Left            =   1605
         OleObjectBlob   =   "VtaDirecta6.frx":1613
         TabIndex        =   66
         Top             =   420
         Width           =   420
      End
   End
   Begin VB.CommandButton CmdSalir 
      Cancel          =   -1  'True
      Caption         =   "Retornar"
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   11955
      TabIndex        =   24
      Top             =   9975
      Width           =   1140
   End
   Begin VB.Frame FrameMA 
      Caption         =   "Articulos"
      Height          =   3315
      Left            =   165
      TabIndex        =   59
      Top             =   3495
      Width           =   12885
      Begin VB.TextBox TxtNetoSG 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   8955
         TabIndex        =   185
         ToolTipText     =   "Nro de Documento"
         Top             =   2955
         Width           =   1320
      End
      Begin VB.CommandButton CmdSg 
         Caption         =   "Sg"
         Height          =   270
         Left            =   10305
         TabIndex        =   184
         Top             =   2970
         Width           =   345
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkCantLineas 
         Height          =   270
         Left            =   11115
         OleObjectBlob   =   "VtaDirecta6.frx":1674
         TabIndex        =   143
         Tag             =   "0"
         Top             =   2970
         Width           =   1470
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel14 
         Height          =   180
         Index           =   0
         Left            =   1125
         OleObjectBlob   =   "VtaDirecta6.frx":16D4
         TabIndex        =   121
         Top             =   3000
         Width           =   990
      End
      Begin VB.TextBox txtInfoPrecio 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   5025
         Locked          =   -1  'True
         TabIndex        =   73
         ToolTipText     =   "Stock Actual"
         Top             =   30
         Visible         =   0   'False
         Width           =   3000
      End
      Begin VB.TextBox TxtCodigo 
         Height          =   285
         Left            =   120
         MaxLength       =   13
         TabIndex        =   7
         Top             =   255
         Width           =   1110
      End
      Begin VB.CommandButton CmdAgregarProducto 
         Caption         =   "Ok"
         Height          =   255
         Left            =   12285
         TabIndex        =   11
         Top             =   270
         Width           =   330
      End
      Begin VB.TextBox TxtMarca 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1485
         Locked          =   -1  'True
         TabIndex        =   19
         Top             =   270
         Width           =   1455
      End
      Begin VB.TextBox TxtDescrp 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2925
         Locked          =   -1  'True
         TabIndex        =   20
         Top             =   270
         Width           =   3495
      End
      Begin VB.TextBox TxtPrecio 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   6420
         TabIndex        =   8
         Top             =   270
         Width           =   960
      End
      Begin VB.TextBox TxtStock 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   7620
         Locked          =   -1  'True
         TabIndex        =   18
         ToolTipText     =   "Stock Actual"
         Top             =   45
         Visible         =   0   'False
         Width           =   2205
      End
      Begin VB.TextBox TxtCantidad 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   8310
         TabIndex        =   9
         Text            =   "0"
         Top             =   270
         Width           =   690
      End
      Begin VB.TextBox TxtSubTotal 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   9975
         Locked          =   -1  'True
         TabIndex        =   21
         Top             =   270
         Width           =   1095
      End
      Begin VB.CommandButton CMDeliminaMaterial 
         Caption         =   "X Elimina"
         Height          =   240
         Left            =   120
         TabIndex        =   23
         ToolTipText     =   "Eliminar Linea seleccionada"
         Top             =   3015
         Width           =   855
      End
      Begin VB.CommandButton CmdEspecial 
         Caption         =   "Especial"
         Height          =   225
         Left            =   -840
         TabIndex        =   22
         ToolTipText     =   "Venta especial"
         Top             =   3750
         Visible         =   0   'False
         Width           =   855
      End
      Begin VB.TextBox Text1 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         Height          =   285
         Left            =   7365
         Locked          =   -1  'True
         TabIndex        =   61
         Text            =   "0"
         Top             =   270
         Width           =   945
      End
      Begin VB.TextBox txtPUni 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         Height          =   285
         Left            =   9015
         Locked          =   -1  'True
         TabIndex        =   60
         Text            =   "0"
         Top             =   270
         Width           =   945
      End
      Begin MSComCtl2.DTPicker DtSalidaMaterial 
         Height          =   285
         Left            =   11040
         TabIndex        =   10
         Top             =   270
         Width           =   1260
         _ExtentX        =   2223
         _ExtentY        =   503
         _Version        =   393216
         Format          =   95420417
         CurrentDate     =   40543
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel12 
         Height          =   255
         Left            =   120
         OleObjectBlob   =   "VtaDirecta6.frx":1746
         TabIndex        =   62
         Top             =   960
         Visible         =   0   'False
         Width           =   10995
      End
   End
   Begin VB.TextBox txtCostoRepuestos 
      Height          =   285
      Left            =   7740
      TabIndex        =   58
      Text            =   "0"
      Top             =   9765
      Width           =   1680
   End
   Begin VB.TextBox TxtPcosto 
      Height          =   375
      Left            =   4710
      TabIndex        =   55
      Text            =   "Text1"
      Top             =   9870
      Width           =   2535
   End
   Begin VB.Frame FrameCliente 
      Caption         =   "Datos del cliente"
      Height          =   1230
      Left            =   120
      TabIndex        =   39
      Top             =   1290
      Width           =   12810
      Begin VB.ComboBox CboRs 
         Height          =   315
         Left            =   3690
         Style           =   2  'Dropdown List
         TabIndex        =   140
         Top             =   195
         Visible         =   0   'False
         Width           =   3060
      End
      Begin VB.ComboBox CboGiros 
         Height          =   315
         Left            =   3690
         Style           =   2  'Dropdown List
         TabIndex        =   133
         Top             =   810
         Width           =   9060
      End
      Begin VB.TextBox TxtListaPrecio 
         BackColor       =   &H00E0E0E0&
         Height          =   285
         Left            =   135
         Locked          =   -1  'True
         TabIndex        =   118
         TabStop         =   0   'False
         Top             =   795
         Width           =   2775
      End
      Begin VB.ComboBox CboSucursal 
         Height          =   315
         Left            =   7680
         Style           =   2  'Dropdown List
         TabIndex        =   6
         Top             =   210
         Width           =   5070
      End
      Begin VB.TextBox TxtRut 
         BackColor       =   &H0080FF80&
         Height          =   285
         Left            =   675
         TabIndex        =   5
         ToolTipText     =   "Ingrese el RUT sin puntos ni guion."
         Top             =   240
         Width           =   1575
      End
      Begin VB.TextBox TxtRazonSocial 
         BackColor       =   &H00E0E0E0&
         Height          =   285
         Left            =   3720
         Locked          =   -1  'True
         TabIndex        =   41
         TabStop         =   0   'False
         Top             =   210
         Width           =   3015
      End
      Begin VB.TextBox TxtDireccion 
         Height          =   285
         Left            =   3690
         Locked          =   -1  'True
         TabIndex        =   13
         TabStop         =   0   'False
         Top             =   510
         Width           =   3030
      End
      Begin VB.TextBox TxtComuna 
         Height          =   285
         Left            =   7680
         Locked          =   -1  'True
         TabIndex        =   15
         TabStop         =   0   'False
         Text            =   " "
         Top             =   540
         Width           =   1425
      End
      Begin VB.TextBox TxtCiudad 
         Height          =   285
         Left            =   9795
         Locked          =   -1  'True
         TabIndex        =   16
         TabStop         =   0   'False
         Text            =   " "
         Top             =   540
         Width           =   1335
      End
      Begin VB.CommandButton CmdBuscaCliente 
         Caption         =   "F1 - Buscar"
         Height          =   255
         Left            =   675
         TabIndex        =   12
         Top             =   510
         Width           =   1575
      End
      Begin VB.TextBox TxtGiro 
         Height          =   285
         Left            =   12870
         Locked          =   -1  'True
         TabIndex        =   14
         TabStop         =   0   'False
         Top             =   195
         Width           =   1815
      End
      Begin VB.TextBox TxtDscto 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   11910
         MaxLength       =   6
         TabIndex        =   17
         Top             =   525
         Width           =   615
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   255
         Left            =   2025
         OleObjectBlob   =   "VtaDirecta6.frx":1924
         TabIndex        =   40
         Top             =   540
         Width           =   1605
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   255
         Index           =   0
         Left            =   150
         OleObjectBlob   =   "VtaDirecta6.frx":1994
         TabIndex        =   42
         Top             =   240
         Width           =   495
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel16 
         Height          =   255
         Left            =   1935
         OleObjectBlob   =   "VtaDirecta6.frx":19F8
         TabIndex        =   43
         Top             =   240
         Width           =   1695
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel18 
         Height          =   255
         Left            =   3210
         OleObjectBlob   =   "VtaDirecta6.frx":1A62
         TabIndex        =   44
         Top             =   840
         Width           =   420
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel19 
         Height          =   255
         Left            =   6765
         OleObjectBlob   =   "VtaDirecta6.frx":1AC8
         TabIndex        =   45
         Top             =   555
         Width           =   855
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel20 
         Height          =   255
         Left            =   9135
         OleObjectBlob   =   "VtaDirecta6.frx":1B32
         TabIndex        =   46
         Top             =   570
         Width           =   735
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel21 
         Height          =   255
         Left            =   11310
         OleObjectBlob   =   "VtaDirecta6.frx":1B9C
         TabIndex        =   47
         Top             =   540
         Width           =   1395
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel5 
         Height          =   255
         Left            =   6750
         OleObjectBlob   =   "VtaDirecta6.frx":1C26
         TabIndex        =   72
         Top             =   210
         Width           =   900
      End
   End
   Begin VB.Timer Timer1 
      Interval        =   1
      Left            =   4215
      Top             =   8715
   End
   Begin VB.TextBox TxtIDFamilia 
      Height          =   285
      Left            =   18510
      TabIndex        =   26
      Text            =   "Text1"
      Top             =   5640
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.TextBox TxtNombreFamilia 
      Height          =   285
      Left            =   18510
      TabIndex        =   25
      Text            =   "Text2"
      Top             =   6120
      Visible         =   0   'False
      Width           =   1695
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   660
      OleObjectBlob   =   "VtaDirecta6.frx":1C94
      Top             =   5235
   End
   Begin MSComctlLib.ListView LvMaterialesCopia 
      Height          =   2040
      Left            =   13320
      TabIndex        =   138
      Top             =   -15
      Visible         =   0   'False
      Width           =   12525
      _ExtentX        =   22093
      _ExtentY        =   3598
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   0   'False
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   0
      NumItems        =   24
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Nro Orden"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Codigo"
         Object.Width           =   2399
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "Marca"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Text            =   "Descripcion"
         Object.Width           =   6174
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   4
         Text            =   "Precio"
         Object.Width           =   1676
      EndProperty
      BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   5
         Text            =   "Dscto."
         Object.Width           =   1676
      EndProperty
      BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   6
         Text            =   "Un."
         Object.Width           =   1235
      EndProperty
      BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   7
         Text            =   "Precio U."
         Object.Width           =   1676
      EndProperty
      BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   8
         Text            =   "Total"
         Object.Width           =   1940
      EndProperty
      BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   9
         Text            =   "Id Familia"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   10
         Text            =   "Costo"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(12) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   11
         Text            =   "Nro de Ot"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(13) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   12
         Text            =   "especial"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(14) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   13
         Text            =   "Comision"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(15) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   14
         Text            =   "Fec. Salida"
         Object.Width           =   2187
      EndProperty
      BeginProperty ColumnHeader(16) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   15
         Text            =   "Utilidad"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(17) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   16
         Object.Tag             =   "T100"
         Text            =   "Inventariable"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(18) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   17
         Object.Tag             =   "N109"
         Text            =   "Cuenta"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(19) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   18
         Object.Tag             =   "N109"
         Text            =   "ARea"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(20) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   19
         Object.Tag             =   "N109"
         Text            =   "CentroCosto"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(21) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   20
         Object.Tag             =   "N109"
         Text            =   "Codigo Activo Inmovilizado"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(22) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   21
         Object.Tag             =   "T1000"
         Text            =   "Unidad mediad"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(23) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   22
         Object.Tag             =   "N109"
         Text            =   "codigo iva anticipado"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(24) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   23
         Object.Tag             =   "N102"
         Text            =   "monto iva anticipado"
         Object.Width           =   2540
      EndProperty
   End
   Begin VB.Label Label4 
      DataField       =   "NOMBRE_MECANICO"
      DataSource      =   "AdoMecanicos"
      Height          =   255
      Left            =   4920
      TabIndex        =   54
      Top             =   8880
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.Label Label5 
      Caption         =   "Label5"
      DataField       =   "NOMBRE_ADMINISTRATIVO"
      DataSource      =   "AdoAdministrativos"
      Height          =   255
      Left            =   6960
      TabIndex        =   53
      Top             =   8880
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.Label Label6 
      Caption         =   "Label6"
      DataField       =   "RUT_EMPRESA"
      DataSource      =   "AdoOT"
      Height          =   255
      Left            =   0
      TabIndex        =   52
      Top             =   8640
      Width           =   2175
   End
   Begin VB.Label Label7 
      Caption         =   "Label7"
      DataField       =   "OrdenTrabajo"
      DataSource      =   "AdoOTMO"
      Height          =   255
      Left            =   795
      TabIndex        =   51
      Top             =   9675
      Width           =   1215
   End
   Begin VB.Label Label8 
      Caption         =   "Label8"
      DataField       =   "OrdenTrabajo"
      DataSource      =   "AdoOTMateriales"
      Height          =   255
      Left            =   3495
      TabIndex        =   50
      Top             =   9315
      Width           =   1215
   End
   Begin VB.Label LbTelefono 
      Caption         =   "Label11"
      Height          =   255
      Left            =   13635
      TabIndex        =   49
      Top             =   2055
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.Label Label12 
      Caption         =   "Label12"
      DataField       =   "Codigo"
      DataSource      =   "AdoVDTemporal"
      Height          =   255
      Left            =   1320
      TabIndex        =   48
      Top             =   10920
      Width           =   1215
   End
End
Attribute VB_Name = "VtaDirecta"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public S_Facturado As String
Dim Lm_Ids As String
Public iP_Tipo_NC As Integer
Public lP_Documento_Referenciado As Long
Public Im_Tcal As Integer
Dim Bm_Modifica_Inventario As Boolean
Dim Bm_Cambia_Precios As Boolean
Dim Sm_operador As String
Dim CodigoEncontrado As Boolean
Dim Fococodigo As Boolean
Dim Fococantidad As Boolean
Dim AdoTempVd As ADODB.Recordset
Dim RsVdirecta As Recordset
Dim Codigo As String
Dim MARCA As String
Dim Descripcion As String
Dim Descuento As String
Dim Unidades As String
Dim Precio As String
Dim PrecioRef As String
Dim SubTotal As String
Dim DescuentoTemp As String
Dim IdFamiliaR As String
Dim NomFamiliaR As String
Dim PCosto As String
Dim Bm_Requiere_Referencia As Boolean
Dim Bm_ReIntegraStock As Boolean
Dim bm_SoloVistaDoc As Boolean
Dim Bm_EditandoPorGuias As Boolean
Dim Lp_IdAbo As Long
Dim Sm_GuiasEditando As String
Dim Bm_Nueva_Ventas As Boolean
Dim Bm_Factura_por_Guias As Boolean
Dim Bm_Es_Nota_de_Credito As Boolean
Dim bm_Documento_Permite_Pago As Boolean
Dim Sm_SoloExento As String * 2
Dim Sm_RetieneIva As String * 2
Dim Bm_Cuenta As Boolean
Dim Sm_NotaDeVenta As String * 2
Dim Sm_Bitacora_Productos As String * 2
Dim Lp_Nro_Comprobante As Long
Dim Sm_Factor_IVA As Integer
Dim Sm_Con_Precios_Brutos As String * 2
Dim Sm_Avisa_Sin_Stock As String * 2
Dim Sm_PermiteFechaFueraPeriodo As String * 2
Dim Sm_SoloVendedorAsignado As String * 2
Dim Sm_PermiteCrearProductos As String * 2
Dim Lp_MontoCreditoAprobado As Long
Dim Sp_EmpresaRepuestos As String * 2

Dim IM_TCALORIGINAL As Integer
Dim Sm_Actualiza_PrecioVenta As String * 2
Dim Lp_Id_Nueva_Venta As Long
Dim Sp_FacturaElectronica As String
Dim Sm_UtilizaCodigoInterno As String * 2

'23-05-2015 Esto para regularizar el descuento, que no pase del max  segun la atribucion del perfil
Dim Sm_Precio_Original As Long
Dim Ip_DocIndicio As Integer 'documento seleccionao en el POS

'17-7-2015
Dim Lp_IdCuentaVentasPorDefecto As Long

Dim Bm_BuscarReferencia As Boolean
'7-11-2015 saber si la empresa vendera con iva anticipado, _
leer de los productos la informacion del codigo del impuesto _
y el % que se debe aplicar, sobre el neto del mismo
Dim Sm_Empresa_IVA_Anticipado As String * 2

Dim proivA As Variant
Dim proivacodigO As Variant

'25082016
'Conexin a acces para pachipap
Dim cnAccess As ADODB.Connection

Private Declare Function ShellExecute Lib "shell32.dll" Alias "ShellExecuteA" (ByVal hWnd As Long, ByVal lpOperation As String, ByVal lpFile As String, ByVal lpParameters As String, ByVal lpDirectory As String, ByVal nShowCmd As Long) As Long


Private Sub CalculaTotal()
    SkTotalMateriales = 0
    SkIvaMateriales = 0
    TxtExentos = 0
    SkBrutoMateriales = 0
        
        SkTotalMateriales = NumFormat(TotalizaColumna(LvSinDetalle, "neto"))
        SkIvaMateriales = NumFormat(TotalizaColumna(LvSinDetalle, "iva"))
        TxtExentos = NumFormat(TotalizaColumna(LvSinDetalle, "otros"))
        SkBrutoMateriales = NumFormat(CDbl(SkTotalMateriales) + CDbl(SkIvaMateriales) + CDbl(TxtExentos) - CDbl(TxtIVARetenido) + CDbl(Me.TxtIvaAnticipado))
    
            


End Sub

Private Sub limpieza()
            TxtNroDocumento = Empty
            TxtRut = Empty
            TxtRazonSocial = Empty
            TxtGiro = Empty
            TxtCiudad = Empty
            TxtDireccion = Empty
            TxtComuna = Empty
            CboSucursal.ListIndex = -1
            Me.LvSinDetalle.ListItems.Clear
            TxtExentos = 0
            SkTotalMateriales = 0
            SkIvaMateriales = 0
            SkBrutoMateriales = 0
            TxtMotivoRef = "-"
            TxtDocReferencia = Empty
            TxtNroReferencia = Empty
            DG_ID_Unico = 0
            bm_SoloVistaDoc = False
            CboDocVenta.ListIndex = -1
            TxtIVARetenido = 0
            DtFecha.SetFocus
End Sub

Private Sub TotalBruto()
     
     SkBrutoMateriales = NumFormat(CDbl(SkTotalMateriales) + CDbl(SkIvaMateriales) + CDbl(TxtExentos) - CDbl(TxtIVARetenido) + CDbl(TxtIvaAnticipado))
End Sub

Private Sub CboCentroCosto_LostFocus()
    On Error Resume Next
    If SP_Control_Inventario = "SI" Then If TxtCodigo.Enabled Then TxtCodigo.SetFocus Else TxtSdExento.SetFocus
    


    CmdBuscaActivo.Visible = False
    If CboCuenta.Visible And CboCuenta.ListIndex > -1 Then
        
        Sql = "SELECT pla_id " & _
                    "FROM con_plan_de_cuentas " & _
                    "WHERE tpo_id=1 AND det_id=4 AND pla_analisis='SI' AND pla_id=" & CboCuenta.ItemData(CboCuenta.ListIndex)
            Consulta RsTmp, Sql
            If RsTmp.RecordCount > 0 Then
                Me.CmdBuscaActivo.Visible = True
            End If
    
    End If
End Sub
Private Sub CboCuenta_Click()
        Dim Rp_Cuenta As Recordset
        CmdBuscaActivo.Visible = False
        If CboCuenta.Visible And CboCuenta.ListIndex > -1 Then
            TxtNroCuenta = CboCuenta.ItemData(CboCuenta.ListIndex)
            Sql = "SELECT pla_id " & _
                    "FROM con_plan_de_cuentas " & _
                    "WHERE tpo_id=1 AND det_id=4 AND pla_analisis='SI' AND pla_id=" & CboCuenta.ItemData(CboCuenta.ListIndex)
            Consulta Rp_Cuenta, Sql
            If Rp_Cuenta.RecordCount > 0 Then
                Me.TxtNroCuenta = ""
                CboCuenta.ListIndex = -1
                MsgBox "Venta de activs en modulo de depreciaciones", vbInformation
             '   Me.CmdBuscaActivo.Visible = True
             '   TxtCodigo.Visible = False
            Else
                TxtCodigoActivo = ""
                TxtNombreActivo = ""
                TxtCodigo.Visible = True
            End If
    
        End If
End Sub



Private Sub CboDocVenta_Click()
    Dim Ip_DocIdDocumento As Integer
    Dim Rp_DocVenta As Recordset
    'If SG_Equipo_Solo_Nota_de_Venta = "SI" Then Exit Sub
    FrmReferencia.Visible = False
    Bm_Requiere_Referencia = False
    Bm_ReIntegraStock = False
    TxtCodigo.Enabled = True
    CmdBuscaProducto.Enabled = True
    Bm_Modifica_Inventario = True
    
    'parametros para guia
    Me.SkTdespacho.Visible = False
    Me.SkTTraslado.Visible = False
    Me.CboTipoDespacho.Visible = False
    Me.CboTipoTraslado.Visible = False
    
    If CboDocVenta.ListIndex = -1 Then Exit Sub
    
    
    Ip_DocIdDocumento = CboDocVenta.ItemData(CboDocVenta.ListIndex)
    
    If DG_ID_Unico > 0 Then GoTo aLareferencia
  '  If SG_Equipo_Solo_Nota_de_Venta = "SI" Then
        ' NADA Exit Sub
  '  Else
       ' If Ip_DocIndicio = 0 Then
            'On Error Resume Next
            Sql = "SELECT doc_dte,doc_cod_sii " & _
                    "FROM sis_documentos " & _
                    "WHERE doc_id=" & Ip_DocIdDocumento
            Consulta Rp_DocVenta, Sql
            If Rp_DocVenta.RecordCount > 0 Then
                If Rp_DocVenta!doc_dte = "SI" Then
                        
                
                        'Obtener folio disponible
                    ' Sql = "call sp_sel_DteDisponible('" & SP_Rut_Activo & "'," & Rp_DocVenta!doc_cod_sii & "," & IG_Nro_Caja & ");"
                    Sql = "call sp_sel_DteDisponible('" & SP_Rut_Activo & "'," & Rp_DocVenta!doc_cod_sii & ");"
                    Consulta Rp_DocVenta, Sql
                    If Rp_DocVenta.RecordCount > 0 Then
                        TxtNroDocumento = Rp_DocVenta!dte_folio
                        TxtNroDocumento.Tag = Rp_DocVenta!dte_id
                    Else
                        TxtDocActual = 0
                        MsgBox "No cuenta con folios disponibles para continuar..."
                        Exit Sub
                    End If
                
                
                   If CboDocVenta.Text = "GUIA DESPACHO ELECTRONICA" Then
                        Me.SkTdespacho.Visible = True
                        Me.SkTTraslado.Visible = True
                        Me.CboTipoDespacho.Visible = True
                        Me.CboTipoTraslado.Visible = True
                        
                        If DG_ID_Unico = 0 Then
                             LLenarCombo CboTipoDespacho, "gel_detalle", "gel_codigo", "par_parametros_guias_electronicas", "gel_tipo=1 AND gel_activo='SI'"
                            CboTipoDespacho.ListIndex = 0
                            LLenarCombo Me.CboTipoTraslado, "gel_detalle", "gel_codigo", "par_parametros_guias_electronicas", "gel_tipo=2 AND gel_activo='SI'"
                            CboTipoTraslado.ListIndex = 1
                        End If
                    End If
                    
                    
                Else
                    TxtNroDocumento = AutoIncremento("LEE", Ip_DocIdDocumento, , IG_id_Sucursal_Empresa)
                End If
            Else
            
                TxtNroDocumento = AutoIncremento("LEE", Ip_DocIdDocumento, , IG_id_Sucursal_Empresa)
            End If
       ' End If
   ' End If
       
    If SP_Rut_Activo = "12.307.912-4" Then
        If CboDocVenta.Text = "COTIZACION" Then
            Me.FrmAdicional.Visible = True
        Else
            Me.FrmAdicional.Visible = False
        End If
    End If
    'If Ip_DocIndicio = 0 Then
   ' On Error Resume Next
   
   
aLareferencia:
    BuscaRefencia
    
    
    
    
End Sub
Private Sub CuentaLineas()
    Me.SkCantLineas = "Linea " & LvMateriales.ListItems.Count & " / " & SkCantLineas.Tag
End Sub
Private Sub BuscaRefencia()
    If Not Bm_BuscarReferencia Then Exit Sub
    Dim sp_Motivo As String
    Dim Sp_DocRefExento As String * 2
    Dim SkInfo As String
    Dim LaFecha As Date
    Dim i As Integer
    Sp_DocRefExento = "NO"
    Bm_EditandoPorGuias = False
    CboFpago.Locked = False
    SkInfo = ""
    
            '4-7-15
    'si viene de la Pantalla POS
    'veremos cual es documento de INDICIO
'    If Sm_NotaDeVenta = "SI" Then
'        If Ip_DocIndicio > 0 Then
'            Busca_Id_Combo CboDocVenta, Val(Ip_DocIndicio)
'            If CboSucursal.ListCount > 0 Then
'                CboSucursal.ListIndex = 0
'            End If
'            If CboDocVenta.ListIndex = -1 Then CboDocVenta.ListIndex = 0
'
'        End If
'    End If
    
    
    
    If bm_SoloVistaDoc Then Exit Sub
    If CboDocVenta.ListIndex = -1 Then Exit Sub
    Sql = "SELECT doc_requiere_referencia refe,doc_nota_de_credito,doc_permite_pago," & _
            "doc_solo_exento,doc_retiene_iva,doc_nota_de_venta,doc_factor_iva," & _
            "doc_cod_sii,doc_cantidad_lineas,doc_dte,doc_bruto_neto_en_venta precios_brutos,doc_nc_por_boleta,doc_nota_de_debito " & _
          "FROM sis_documentos " & _
          "WHERE doc_id=" & CboDocVenta.ItemData(CboDocVenta.ListIndex)
    Consulta RsTmp2, Sql
    If RsTmp2.RecordCount > 0 Then
        
  
        '24 Abril 2015
        'Leeremos desde el documento si es con precio brutos o netos, no desde la empresa (keramik)
        Im_Tcal = 1
        If RsTmp2!precios_brutos = "NETO" Then
            Sm_Con_Precios_Brutos = "NO"
            Im_Tcal = 2
        ElseIf RsTmp2!precios_brutos = "BRUTO" Then
            Sm_Con_Precios_Brutos = "SI"
            Im_Tcal = 1
        ElseIf RsTmp2!precios_brutos = "EXENT" Then
            Im_Tcal = 3
        End If
        IM_TCALORIGINAL = Im_Tcal
        '------------------------------------------------------------------------------------------
        
        SkFoliosDisponibles = ""
        If RsTmp2!doc_dte = "SI" Then
            
            If Sp_FacturaElectronica = "SI" Then
                Sql = "SELECT count(dte_id) dte " & _
                        "FROM dte_folios " & _
                        "WHERE rut_emp='" & SP_Rut_Activo & "' AND doc_id=" & RsTmp2!doc_cod_sii & " AND dte_disponible='SI' " & _
                        "GROUP BY doc_id"
                Consulta RsTmp3, Sql
                If RsTmp3.RecordCount > 0 Then
                    SkFoliosDisponibles = "Folios disponibles:" & RsTmp3!Dte
                Else
                    SkFoliosDisponibles = "No quedan Folios disponibles..."
                End If
                
                        
            End If
        End If
        Sm_Factor_IVA = RsTmp2!doc_factor_iva
        If Sm_Factor_IVA = 0 Then
            'SkTotalMateriales = NUMFORMAT(CDbl(SkBrutoMateriales) - CDbl(SkIvaMateriales)
            SkIvaMateriales = 0
            SkBrutoMateriales = SkTotalMateriales
        Else
            If CDbl(SkTotalMateriales) > 0 Then
                SkBrutoMateriales = AgregarIVA(CDbl(SkTotalMateriales), Val(DG_IVA))
                SkIvaMateriales = NumFormat(CDbl(SkBrutoMateriales) - CDbl(SkTotalMateriales))
            End If
        End If
        SkCantLineas.Tag = RsTmp2!doc_cantidad_lineas
        CuentaLineas
        Bm_Es_Nota_de_Credito = IIf(RsTmp2!doc_nota_de_credito = "SI", True, False)
        bm_Documento_Permite_Pago = IIf(RsTmp2!doc_permite_pago = "SI", True, False)
        Sm_SoloExento = RsTmp2!doc_solo_exento
        Im_Tcal = IM_TCALORIGINAL
        
    
        
        If Sm_SoloExento = "SI" Then Im_Tcal = 3  '22 Nov 2012 si es solo exento la suma
        Me.SumaListaMaterialesYMobra
        'de los productos sera exento
        Sm_NotaDeVenta = RsTmp2!doc_nota_de_venta
        Sm_RetieneIva = RsTmp2!doc_retiene_iva
        If Sm_SoloExento = "SI" Then
            TxtSDNeto.Enabled = False
            TxtSdIva.Enabled = False
        Else
            TxtSDNeto.Enabled = True
            TxtSdIva.Enabled = True
        End If
        
        If Sm_RetieneIva = "SI" Then
            Me.TxtIVARetenido.Enabled = True
            TxtIVARetenido.Locked = False
        Else
            TotalBruto
            TxtIVARetenido = 0
            TxtIVARetenido.Enabled = False
        End If
            
        
        If RsTmp2!refe = "SI" Then
            Bm_Requiere_Referencia = True
            If CboDocVenta.ListIndex > -1 And Len(TxtRut) > 0 Then
                Sql = "SELECT v.id,fecha,CONCAT(tipo_doc,' ',IF(doc_factura_guias='SI','POR GUIA(S)','')) tipo_d ,no_documento,v.rut_cliente,IFNULL(cli_nombre_fantasia,nombre_rsocial) nombre,neto,iva,bruto " & _
                      "FROM ven_doc_venta v,sis_documentos d,maestro_clientes c " & _
                      "WHERE v.rut_emp='" & SP_Rut_Activo & "' AND c.rut_cliente=v.rut_cliente AND v.doc_id=d.doc_id AND doc_referenciable='SI' AND v.rut_cliente='" & TxtRut & "' " & _
                      "ORDER BY id DESC"
                Consulta RsTmp, Sql
                iP_Tipo_NC = 0
                If RsTmp.RecordCount > 0 Then
                    SG_codigo2 = CboDocVenta.ItemData(CboDocVenta.ListIndex)
                    vta_BuscaDocumentos.B_Seleccion = True
                    If CboDocVenta.ItemData(CboDocVenta.ListIndex) = IG_id_Nota_Credito_Clientes Then
                        vta_BuscaDocumentos.B_Seleccion = False
                    End If
                    vta_BuscaDocumentos.Show 1
                    
                    
                End If
                If iP_Tipo_NC > 0 Then
                    
                    Sql = "SELECT tnc_nombre,tnc_modifica_inventario,tnc_anula_documento " & _
                          "FROM ntc_tipos " & _
                          "WHERE tnc_id=" & iP_Tipo_NC
                    Consulta RsTmp3, Sql
                    If RsTmp3.RecordCount > 0 Then
                        sp_Motivo = RsTmp3!tnc_nombre
                        TxtNroReferencia.Tag = lP_Documento_Referenciado
                        DG_ID_Unico = Me.lP_Documento_Referenciado
                        
                        'Ya tenemos identificado el numero unico del documento de venta
                        'que se esta referenciando-  Buscaremos el documento con el cual
                        'Se emitio y sabremos sus atributos.
                        
                        Sql = "SELECT doc_solo_exento,fecha,v.doc_id,doc_cod_sii " & _
                                "FROM ven_doc_venta v " & _
                                "INNER JOIN sis_documentos d  ON v.doc_id=d.doc_id " & _
                                "WHERE id=" & DG_ID_Unico
                        Consulta RsTmp, Sql
                        If RsTmp.RecordCount > 0 Then
                            Sp_DocRefExento = RsTmp!doc_solo_exento
                            SkCodigoReferencia = iP_Tipo_NC
                            If SkCodigoReferencia = 4 Then SkCodigoReferencia = 3
                            SkFechaReferencia = Format(RsTmp!Fecha, "YYYY-MM-DD")
                            SkDocIdReferencia = RsTmp!doc_cod_sii
                        End If
                        
                        
                        LaFecha = DtFecha.Value
                        'SkInfo = "POR ANULACION DE DOCUMENTO "
                        If RsTmp3!tnc_anula_documento = "SI" Then
                            'Se anulara el documento
                            'Devolver articulos al stock
                            'Abonar el total del documento a la NC
                            Bm_ReIntegraStock = True
                            bm_SoloVistaDoc = False
                            
                            CargaDocumento Sp_DocRefExento
                            Me.CmdAgregarProducto.Enabled = False
                            Me.CMDeliminaMaterial.Enabled = False
                            Me.CmdEmiteDocumento.SetFocus
                        End If
                        If RsTmp3!tnc_anula_documento = "NO" And RsTmp3!tnc_modifica_inventario = "NO" Then
                            'ESTO SOLO CORRIGE PRECIOS NO CANTIDADES
                            Bm_Modifica_Inventario = False
                            Bm_Cambia_Precios = True
                            CargaDocumento Sp_DocRefExento
                            'Inhabilitamos entrada de nuevo codigo
                            TxtCodigo.Locked = True
                            CmdBuscaProducto.Enabled = False
                            TxtCantidad.Locked = True
                          '  If CboDocVenta.ItemData(CboDocVenta.ListIndex) = IG_id_Nota_Credito_Clientes Then
                          '      Sm_operador = "<"
                          '  End If
                          '  If CboDocVenta.ItemData(CboDocVenta.ListIndex) = IG_id_Nota_Debito_Clientes Then
                          '      Sm_operador = ">"
                          '  End If
                        End If
                        If RsTmp3!tnc_anula_documento = "NO" And RsTmp3!tnc_modifica_inventario = "SI" Then
                            'No anula pero modifica cantidades
                            Bm_Modifica_Inventario = True
                            Bm_Cambia_Precios = True
                            Bm_ReIntegraStock = True
                            CargaDocumento Sp_DocRefExento
                            TxtCodigo.Locked = True
                            CmdBuscaProducto.Enabled = False
                            TxtCantidad.Locked = False
                        End If
                        DtFecha = LaFecha
                    End If
                    Sql = "SELECT id, v.doc_id,no_documento,doc_nombre,doc_factura_guias,condicionpago,ven_iva_retenido,ven_plazo,ven_plazo_id " & _
                          "FROM ven_doc_venta v,sis_documentos d " & _
                          "WHERE  v.rut_emp='" & SP_Rut_Activo & "' AND v.doc_id=d.doc_id AND v.id=" & lP_Documento_Referenciado
                    Consulta RsTmp, Sql
                    If RsTmp.RecordCount > 0 Then
                        FrmReferencia.Visible = True
                        FrmReferencia.Tag = RsTmp!doc_id
                        TxtMotivoRef = sp_Motivo
                        TxtNroReferencia = RsTmp!no_documento
                        TxtDocReferencia = RsTmp!doc_nombre
                        TxtIVARetenido = RsTmp!ven_iva_retenido
                        CboFpago.Clear
                    '    Sql = "SELECT pla_id " & _
                    '            "FROM par_plazos_vencimiento " & _
                    '            "WHERE pla_dias=" & RsTmp!ven_plazo
                    '    Consulta RsTmp2, Sql
                        
                        
                        
                        'Busca_Id_Combo CboPlazos, Val(RsTmp!ven_plazo)
                        'If CboPlazos.ListIndex = -1 Then
                            For i = 0 To CboPlazos.ListCount - 1
                                If Val(RsTmp!ven_plazo_id) = Val(Right(CboPlazos.List(i), 5)) Then
                                    CboPlazos.ListIndex = i
                                    Exit For
                                End If
                            Next
                        'End If
                        If RsTmp!condicionpago = "CONTADO" Then CboFpago.AddItem "CONTADO"
                        If RsTmp!condicionpago = "CREDITO" Then CboFpago.AddItem "CREDITO"
                        CboPlazos.Enabled = False
                        CboFpago.Locked = True
                        'Aqui debemos consultar si el documento de referencia corresponde a guias en el caso que
                        'estemos haciendo una nota de credito por una factura que contiene guias.
                        If RsTmp!doc_factura_guias = "SI" Then
                            Bm_EditandoPorGuias = True
                            Sql = "SELECT CONCAT(CAST(doc_id AS CHAR),'-',CAST(no_documento AS CHAR)) guia   " & _
                                  "FROM ven_doc_venta " & _
                                  "WHERE  rut_emp='" & SP_Rut_Activo & "' AND doc_id_factura=" & RsTmp!doc_id & " AND nro_factura=" & RsTmp!no_documento
                            Consulta RsTmp2, Sql
                            If RsTmp2.RecordCount > 0 Then
                                With RsTmp2
                                    .MoveFirst
                                    Sm_GuiasEditando = "'0'"
                                    Do While Not .EOF
                                        Sm_GuiasEditando = Sm_GuiasEditando & ",'" & !guia & "'"
                                        .MoveNext
                                    Loop
                                End With
                                EditarVD RsTmp!ID
                            End If
                        End If
                        
                        
                    End If
                Else
                    CboDocVenta.SetFocus
                End If
                
                
            End If
        End If
    End If
    

    
End Sub


Private Sub CboDocVenta_KeyPress(KeyAscii As Integer)
     If KeyAscii = 13 Then SendKeys ("{TAB}")
End Sub

Private Sub CboFpago_GotFocus()
    On Error Resume Next
    If Me.CboPlazos.ItemData(CboPlazos.ListIndex) = 0 Then
        CboFpago.Clear
        CboFpago.AddItem "CONTADO"
    Else
        CboFpago.Clear
        CboFpago.AddItem "CREDITO"
    End If
    CboFpago.ListIndex = 0
End Sub






Private Sub CboPlazos_Click()
    Dim Rp_DocVenta As Recordset
    On Error Resume Next
    
    If Me.CboPlazos.ItemData(CboPlazos.ListIndex) = 0 Then
        CboFpago.Clear
        CboFpago.AddItem "CONTADO"
    Else
        CboFpago.Clear
        CboFpago.AddItem "CREDITO"
    End If
    CboFpago.ListIndex = 0
    
    
    If DG_ID_Unico = 0 Then
        If CboPlazos.ListIndex > -1 Then
            Sql = "SELECT doc_id " & _
                    "FROM par_plazos_vencimiento " & _
                    "WHERE pla_id=" & Val(Right(CboPlazos.Text, 9))
            Consulta Rp_DocVenta, Sql
            If Rp_DocVenta.RecordCount > 0 Then
                If Rp_DocVenta!doc_id > 0 Then
                    'Combo solo con debito
                    LLenaDocumentosDeVenta " AND doc_id = " & Rp_DocVenta!doc_id
                   ' CboDocVenta.ListIndex = 0
                Else
                    LLenaDocumentosDeVenta ""
                   ' If CboDocVenta.ListCount > 0 Then CboDocVenta.ListIndex = 0
                End If
            Else
                    'If CboDocVenta.ListCount > 0 Then CboDocVenta.ListIndex = 0
            End If
        End If
    Else
        Sql = "SELECT doc_id " & _
                "FROM par_plazos_vencimiento " & _
                "WHERE pla_id=" & Val(Right(CboPlazos.Text, 9))
        Consulta Rp_DocVenta, Sql
        If Rp_DocVenta.RecordCount > 0 Then
            If Rp_DocVenta!doc_id > 0 Then
                'Combo solo con debito
                
                If SP_Rut_Activo <> "96.803.210-0" Then
                
                    LLenaDocumentosDeVenta " AND doc_id = " & Rp_DocVenta!doc_id
                    
                End If
               ' CboDocVenta.ListIndex = 0
            Else
                If SP_Rut_Activo <> "96.803.210-0" Then
                    LLenaDocumentosDeVenta ""
                End If
               ' If CboDocVenta.ListCount > 0 Then CboDocVenta.ListIndex = 0
            End If
        End If
    
    
    
       
    End If
    
    
    
End Sub

Private Sub CboPlazos_KeyPress(KeyAscii As Integer)
     If KeyAscii = 13 Then SendKeys ("{TAB}")
End Sub


Private Sub CboPlazos_LostFocus()
    On Error Resume Next
    If Me.CboPlazos.ItemData(CboPlazos.ListIndex) = 0 Then
        CboFpago.Clear
        CboFpago.AddItem "CONTADO"
    Else
        CboFpago.Clear
        CboFpago.AddItem "CREDITO"
    End If
    CboFpago.ListIndex = 0
End Sub


Private Sub CboPlazos_Validate(Cancel As Boolean)
    On Error Resume Next
    If Me.CboPlazos.ItemData(CboPlazos.ListIndex) = 0 Then
        CboFpago.Clear
        CboFpago.AddItem "CONTADO"
    Else
        CboFpago.Clear
        CboFpago.AddItem "CREDITO"
    End If
    CboFpago.ListIndex = 0
End Sub


Private Sub CboRs_Click()
    If CboRs.ListIndex = -1 Then Exit Sub
    Me.TxtRazonSocial = CboRs.Text
    
End Sub

Private Sub CboSucursal_Click()
    Dim Ip_IdSuc As Integer
    If CboSucursal.ListIndex = -1 Then Exit Sub
    
    Ip_IdSuc = CboSucursal.ItemData(CboSucursal.ListIndex)
    If Ip_IdSuc = 0 Then
        Busca_Id_Combo CboGiros, 0
        TxtDireccion = TxtDireccion.Tag
        TxtCiudad = TxtCiudad.Tag
        TxtComuna = TxtComuna.Tag
        Exit Sub
    End If
    Sql = "SELECT suc_direccion,suc_ciudad,suc_contacto suc_comuna,gir_id " & _
          "FROM par_sucursales " & _
          "WHERE suc_id=" & Ip_IdSuc
    Consulta RsTmp2, Sql
    If RsTmp2.RecordCount > 0 Then
        TxtDireccion = RsTmp2!suc_direccion
        
       ' If SP_Rut_Activo = "11.500.319-4" Then
            'No cambia , especial para Panaderia perla del sur
       ' Else
            'En cualquier otro caso si cambia la ciudad.
            TxtCiudad = RsTmp2!suc_ciudad
       ' End If
        TxtComuna = RsTmp2!suc_comuna
         Busca_Id_Combo CboGiros, Val("" & RsTmp2!gir_id)
    End If
End Sub


Private Sub CboSucursal_GotFocus()
'    If Len(TxtRut) = 0 Then TxtRut.SetFocus
End Sub

Private Sub CboSucursal_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then TxtCodigo.SetFocus
End Sub

Private Sub CboVendedores_Click()
    Me.SumaListaMaterialesYMobra
End Sub

Private Sub CboVendedores_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then SendKeys ("{TAB}")
End Sub


Private Sub CboVendedores_Validate(Cancel As Boolean)
    Me.SumaListaMaterialesYMobra
End Sub

Private Sub CmdAgregaRefGuia_Click()
    If Me.CboGuiasRef.ListIndex = -1 Then Exit Sub
    If Len(Me.TxtNroGuiaRef) = 0 Then Exit Sub
    
    LvGuiasReferencia.ListItems.Add , , ""
    LvGuiasReferencia.ListItems(LvGuiasReferencia.ListItems.Count).SubItems(1) = CboGuiasRef.Text
    LvGuiasReferencia.ListItems(LvGuiasReferencia.ListItems.Count).SubItems(2) = TxtNroGuiaRef
    LvGuiasReferencia.ListItems(LvGuiasReferencia.ListItems.Count).SubItems(3) = Me.DtGuiaRef
    LvGuiasReferencia.ListItems(LvGuiasReferencia.ListItems.Count).SubItems(4) = Val(Right(CboGuiasRef.Text, 5))
    LvGuiasReferencia.ListItems(LvGuiasReferencia.ListItems.Count).SubItems(5) = CboGuiasRef.ItemData(CboGuiasRef.ListIndex)
    
    TxtNroGuiaRef = ""
    
End Sub

Private Sub CmdAgregarProducto_Click()
    Dim Reemplazar As Boolean, Sp_CodigoAI As String
    Dim ElemntoAreemplazar As Integer
    
    
    '10 Mayo 2014
    'se reemplazo por cantidad de lineas por documento.
    'se lee del documento seleccionado la cantidad de lineas
    'If LvMateriales.ListItems.Count > Ip_CantLineas_Factura Then
    '    MsgBox "La factura solo acepta hasta " & Ip_CantLineas_Factura & " lineas "
    '    Exit Sub
    'End If
    If Val(TxtPrecio) = 0 Then
        MsgBox "Falta Precio ...", vbInformation
        TxtPrecio.SetFocus
        Exit Sub
    End If
        
    
    If Len(TxtCodigo.Text) = 0 Then
        MsgBox "Debe ingresar un codigo", vbOKOnly + vbInformation
        TxtCodigo.SetFocus
        Exit Sub
    End If
    If Val(TxtCantidad.Text) = 0 Then
        MsgBox "Debe ingresar Unidades", vbOKOnly + vbInformation
        TxtCantidad.SetFocus
        Exit Sub
    End If
    If CboDocVenta.ListIndex = -1 Then
        MsgBox "Primero seleccione documento de venta...", vbInformation
        CboDocVenta.SetFocus
        Exit Sub
    End If
    ' en tabla de parametros se agrega opcion,(copiar del db_redmar_backup)
    If CboCuenta.ListIndex < 0 Or CboArea.ListIndex < 0 Or CboCentroCosto.ListIndex < 0 Then
        MsgBox "Faltan datos contables..." & vbNewLine & "Cuenta - Area - Centro Costo", vbInformation
'        CboCuenta.SetFocus
        Exit Sub
    End If
    
    'Contar la cantidad de lineas
    If LvMateriales.ListItems.Count = Val(SkCantLineas.Tag) Then
        MsgBox "Ya lleg� al l�mite de lineas para este documento...", vbInformation
        Exit Sub
    End If
    
    If LvMateriales.ListItems.Count > Val(SkCantLineas.Tag) Then
        MsgBox "Ya sobrepas� la cantidad de lineas para este documento...", vbInformation
        Exit Sub
    End If
    
    
    
    
    Reemplazar = False
    Codigo = Trim(Me.TxtCodigo.Text)
    
    If CmdBuscaActivo.Visible Then
        Sp_CodigoAI = Trim(Me.TxtCodigo.Text)
    Else
        Sp_CodigoAI = "0"
    End If
    'CONSULTAR PRECIO COSTO NO SEAN SUPERIORES AL PRECIO VENTA
    ' Y VICEVERSA CON RESPECTA AL MARGEN DE UTILIDAD
    
    

    
    If CboDocVenta.ItemData(CboDocVenta.ListIndex) <> IG_id_Nota_Debito_Clientes And CboDocVenta.ItemData(CboDocVenta.ListIndex) <> IG_id_Nota_Credito_Clientes Then
            'Ignoramos esta consulta si es nota de credito o debito
            Sql = "SELECT " & TxtPrecio & "-" & Val(TxtPcosto) & " AS diferencia,precio_compra,precio_venta,margen " & _
                  "FROM maestro_productos " & _
                  "WHERE  rut_emp='" & SP_Rut_Activo & "' AND codigo='" & Trim(Codigo) & "'"
            Call Consulta(RsTmp, Sql)
            If RsTmp.RecordCount > 0 Then
                If RsTmp!diferencia < 0 Then
                    If MsgBox("EL PRECIO DE COSTO ES MAYOR AL PRECIO DE VENTA" & vbNewLine & "PRECIO COSTO: " & _
                            NumFormat(TxtPcosto) & vbNewLine & _
                            "PRECIO VENTA : " & NumFormat(TxtPrecio) & vbNewLine & _
                            "" & vbNewLine & vbNewLine & " � CONTINUAR ? ... ", vbYesNo + vbInformation) = vbNo Then
                            
                            LimpiaMaterial
                            Exit Sub
                    Else
                        'Continuando...
                    End If
                End If
                If RsTmp!diferencia > RsTmp!Margen Then
                  '  MsgBox "PRECIO DE VENTA ES MAYOR AL ESTABLECIDO EN EL MAESTRO DE PRODUCTOS PARA ESTE CODIGO (MARGEN SUPERIOR)..." & vbNewLine & _
                           "PRECIO VENTA MAESTRO  : " & NumFormat(RsTmp!precio_venta) & vbNewLine & _
                           "PRECIO VENTA INGRESADO: " & NumFormat(TxtPRecio), vbInformation + vbOKOnly, "INFORMACION"
                End If
            End If
            ' BUSCAR SI EL REPUESTO YA ESTA INGRESADO
            For i = 1 To LvMateriales.ListItems.Count
                If Codigo = LvMateriales.ListItems(i).SubItems(1) And Sp_CodigoAI = LvMateriales.ListItems(i).SubItems(20) Then
                
                    'Diferenciar si alguno corresponde a Activo Inmo y al MaestroProductos
                    
                
                    
                    Respuesta = MsgBox("Este producto ya esta en la lista de materiales" & Chr(13) & _
                           "�Desea reemplazar el existente?", vbQuestion + vbYesNo)
                    If Respuesta = 6 Then
                        Reemplazar = True
                        ElemntoAreemplazar = i
                        'reemplazar
                    Else
                        LimpiaMaterial
                        Exit Sub
                    End If
                End If
            Next
    Else
        'En caso que sea nota de credito o debito
        
        'Vamos a limpiar la grilla copia y llenarla de nuevo-
        'Con los datos que tenga la grilla original
        LvMaterialesCopia.ListItems.Clear
        For i = 1 To LvMateriales.ListItems.Count
            LvMaterialesCopia.ListItems.Add , , LvMateriales.ListItems(i)
            For X = 1 To LvMateriales.ColumnHeaders.Count - 1
                LvMaterialesCopia.ListItems(i).SubItems(X) = LvMateriales.ListItems(i).SubItems(X)
            
            Next
        Next
        
        
        
        For i = 1 To LvMaterialesCopia.ListItems.Count
                If Codigo = LvMaterialesCopia.ListItems(i).SubItems(1) Then
                    If CboDocVenta.ItemData(CboDocVenta.ListIndex) = IG_id_Nota_Credito_Clientes Then
                        If CDbl(TxtPrecio) > CDbl(LvMaterialesCopia.ListItems(i).SubItems(4)) Then
                            MsgBox "No puede ser mayor al precio original en una Nota de Credito que corrige montos..." & vbNewLine & _
                            "Precio Original $" & LvMaterialesCopia.ListItems(i).SubItems(4), vbInformation
                            TxtPrecio.SetFocus
                            Exit Sub
                        End If
                        If TxtCantidad.Enabled = True Then
                            If CDbl(TxtCantidad) > CDbl(LvMaterialesCopia.ListItems(i).SubItems(6)) Then
                                MsgBox "La cantidad no puede ser superior a la original..." & vbNewLine & _
                                       "Cantidad original " & CDbl(LvMaterialesCopia.ListItems(i).SubItems(6)), vbInformation
                                TxtCantidad.SetFocus
                                Exit Sub
                            End If
                        End If
                    End If
                    ElemntoAreemplazar = i
                    Reemplazar = True
                    Exit For
                End If
        Next
    End If
    MARCA = Me.TxtMarca.Text
    
    DescuentoTemp = "0"
    'Si es nota de debito no consideramos el dscto de cliente 17-ENE-2012
    If CboDocVenta.ItemData(CboDocVenta.ListIndex) <> IG_id_Nota_Debito_Clientes Then
            DescuentoTemp = IIf(Val(Me.TxtDscto.Text) > 0, Me.TxtDscto.Text, "0")
    End If
        
    Descripcion = Me.TxtDescrp.Text
  '  DescuentoTemp = IIf(Val(Me.TxtDscto.Text) > 0, Me.TxtDscto.Text, "0")
    Unidades = Me.TxtCantidad.Text
    If SG_Decimales = "SI" Or SP_Rut_Activo = "76.337.408-4" Or SP_Rut_Activo = "10.722.977-9" Or SP_Rut_Activo = "12.318.896-9" Or SP_Rut_Activo = "15.565.714-6" Then
        'Suyai Traipi
        PrecioRef = TxtPrecio
        TxtCantidad = CxP(TxtCantidad)
    Else
        PrecioRef = Format(Me.TxtPrecio.Text, "###,#0")
    End If
    Precio = Me.TxtPrecio.Text 'y formato con separacion de miles
    SubTotal = Me.TxtSubTotal.Text
    Descuento = Val(Precio) - (Val(Precio) - (Val(Precio) / 100 * Val(DescuentoTemp)))
    Descuento = Format(Descuento, "###,#0")
    Precio = Val(Precio) - (Val(Precio) / 100 * Val(DescuentoTemp))
    '30-7-2009 REEMPLAZO LINEA PARA DSCTO
     If SG_Decimales = "SI" Or SP_Rut_Activo = "76.337.408-4" Or SP_Rut_Activo = "10.722.977-9" Or SP_Rut_Activo = "12.318.896-9" Or SP_Rut_Activo = "15.565.714-6" Then
        'suyai jtraipi
        SubTotal = Round(Val(CxP(Me.TxtCantidad.Text)) * Val(CxP(Precio)))
        Precio = CxP(Precio)
    Else
    
        SubTotal = NumFormat(Val(Me.TxtCantidad.Text) * Precio)
        Precio = Format(Precio, "###,##")
    End If
    
    IdFamiliaR = Me.TxtIDFamilia.Text
    NomFamiliaR = Me.TxtNombreFamilia.Text
    PCosto = Val(Me.TxtPcosto) * Val(Unidades)
    
    With LvMateriales
        If Reemplazar Then
            posicion = ElemntoAreemplazar
        Else
            .ListItems.Add , , txtNoOrden
            posicion = .ListItems.Count
        End If
        If CboDocVenta.ItemData(CboDocVenta.ListIndex) <> IG_id_Nota_Debito_Clientes And CboDocVenta.ItemData(CboDocVenta.ListIndex) <> IG_id_Nota_Credito_Clientes Then
            .ListItems(posicion).SubItems(1) = Codigo
            .ListItems(posicion).SubItems(2) = MARCA
            .ListItems(posicion).SubItems(3) = Descripcion
            .ListItems(posicion).SubItems(4) = PrecioRef
            .ListItems(posicion).SubItems(5) = Descuento
            .ListItems(posicion).SubItems(6) = Unidades
            If SG_Decimales = "SI" Or SP_Rut_Activo = "76.337.408-4" Or SP_Rut_Activo = "10.722.977-9" Or SP_Rut_Activo = "12.318.896-9" Or SP_Rut_Activo = "15.565.714-6" Then
                'suyai traipi
                .ListItems(posicion).SubItems(7) = CxP(Precio)
            Else
            
                .ListItems(posicion).SubItems(7) = Precio
            End If
            .ListItems(posicion).SubItems(8) = SubTotal
            .ListItems(posicion).SubItems(9) = IdFamiliaR
            .ListItems(posicion).SubItems(10) = PCosto
            .ListItems(posicion).SubItems(11) = txtNoOrden
            .ListItems(posicion).SubItems(12) = "NO"
            .ListItems(posicion).SubItems(13) = "SI"
            .ListItems(posicion).SubItems(14) = DtSalidaMaterial.Value
            .ListItems(posicion).SubItems(15) = CDbl(.ListItems(posicion).SubItems(8)) - CDbl(.ListItems(posicion).SubItems(10))
            .ListItems(posicion).SubItems(16) = TxtCodigo.Tag
            .ListItems(posicion).SubItems(17) = CboCuenta.ItemData(CboCuenta.ListIndex)
            .ListItems(posicion).SubItems(18) = CboArea.ItemData(CboArea.ListIndex)
            .ListItems(posicion).SubItems(19) = CboCentroCosto.ItemData(CboCentroCosto.ListIndex)
            If CmdBuscaActivo.Visible Then
                .ListItems(posicion).SubItems(20) = Codigo
                
            Else
                .ListItems(posicion).SubItems(20) = 0
            End If
            .ListItems(posicion).SubItems(21) = TxtMarca.Tag
            .ListItems(posicion).SubItems(22) = Me.TxtProIvaAnticipado
            .ListItems(posicion).SubItems(25) = TxtCodigoSIIILA
            
        Else
            .ListItems(posicion).SubItems(5) = Descuento
            .ListItems(posicion).SubItems(6) = Unidades
            .ListItems(posicion).SubItems(4) = PrecioRef
            .ListItems(posicion).SubItems(7) = Precio
            .ListItems(posicion).SubItems(8) = SubTotal
        End If
    End With
    Me.SkTotalMateriales = 0
    Me.SumaListaMaterialesYMobra
    LimpiaMaterial
    
    
    

    
End Sub

Private Sub CmdAnularDocumento_Click()
    Dim Sp_Llave As String
    Dim Lp_IdMantemer As Long
    If TxtRut = "NULO" Then
            MsgBox "Documento esta NULO...", vbOKOnly + vbInformation
            Me.CmdSalir.SetFocus
            Exit Sub
    End If
    If CboDocVenta.ListIndex > -1 And Val(TxtNroDocumento) > 0 Then
        
        If CboDocVenta.Text = "BOLETA FISCAL" Then
            MsgBox "Este documento solo se puede anular con Nota de Credito...", vbInformation
            Exit Sub
        End If
        
         'En el caso que sea DTE informar que no se puede anular,
         'se requiere emitir NC o ND segun corresponda
            If CmdEmiteDocumento.Caption = "Ver PDF" Then
                MsgBox "Los documentos electronicos no se pueden anular," & vbNewLine & "se debe emitir Nota de Credito o Nota de Debito...", vbInformation + vbOKOnly
                Exit Sub
            End If
        
        
        
        
        Sql = "SELECT tipo_movimiento " & _
                "FROM ven_doc_venta " & _
                "WHERE no_documento=" & TxtNroDocumento & " AND doc_id=" & CboDocVenta.ItemData(CboDocVenta.ListIndex) & " AND rut_emp='" & SP_Rut_Activo & "'"
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
            If RsTmp!tipo_movimiento = "NULO" Then
                MsgBox "Documento " & CboDocVenta.Text & " Nro " & TxtNroDocumento & " ya ha sido anulado...", vbInformation
                Exit Sub
            Else
               ' MsgBox "El documento existe..., buscar para anular...", vbInformation
               ' Exit Sub
            End If
        End If
        If DG_ID_Unico = 0 Then
        
                sis_InputBox.FramBox = "Ingrese llave para anulacion"
                sis_InputBox.Show 1
                Sp_Llave = UCase(SG_codigo2)
                If Len(Sp_Llave) = 0 Then Exit Sub
                
              '  If SP_Rut_Activo = "76.369.600-6" Then
              '      Sql = "SELECT usu_id,usu_nombre,usu_login " & _
                          "FROM sis_usuarios " & _
                          "WHERE usu_pwd = MD5('" & Sp_Llave & "') AND per_id=1 "
             '
             '   Else
                    Sql = "SELECT usu_id,usu_nombre,usu_login " & _
                      "FROM sis_usuarios " & _
                      "WHERE usu_pwd = MD5('" & Sp_Llave & "')"
             '   End If
                      
                Consulta RsTmp3, Sql
                If RsTmp3.RecordCount = 0 Then
                    MsgBox "Contrase�a incorrecta o su perfil no permite Anular documentos...", vbInformation
                    Exit Sub
                End If
                     
                     
              Sql = "INSERT INTO ven_doc_venta (id,fecha,doc_id,no_documento,rut_emp,rut_cliente,tipo_doc,nombre_cliente," & _
                                          "tipo_movimiento,usu_nombre,sue_id,caj_id) VALUES(" & _
                        Lp_IdMantemer & "," & _
                         "'" & Format(DtFecha.Value, "YYYY-MM-DD") & _
                         "'," & CboDocVenta.ItemData(CboDocVenta.ListIndex) & _
                         "," & TxtNroDocumento & _
                         ",'" & SP_Rut_Activo & _
                         "','NULO' " & _
                         ",'" & CboDocVenta.Text & _
                         "','ANULADO','NULO','" & Sp_Llave & "'," & IG_id_Sucursal_Empresa & "," & LG_id_Caja & ")"
            cn.Execute Sql
            AutoIncremento "GUARDA", CboDocVenta.ItemData(CboDocVenta.ListIndex), TxtNroDocumento, IG_id_Sucursal_Empresa
            Unload Me
            Exit Sub
            
        
        Else
        
    
            AnulacionDeDocumento
        
        End If
        
        Exit Sub
        
        
        'CODIGO OBSOLETO
    
        Sql = "SELECT v.id " & _
              "FROM ven_doc_venta v INNER JOIN ven_doc_venta d ON v.id=d.id_ref " & _
              "WHERE v.rut_emp='" & SP_Rut_Activo & "' AND " & _
                        "v.doc_id=" & CboDocVenta.ItemData(CboDocVenta.ListIndex) & " AND " & _
                        "v.no_documento=" & TxtNroDocumento & " AND v.rut_cliente='" & TxtRut & "'"
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
                MsgBox "Documento tiene Nota(s) de Credito o Nota(s) de debito..." & vbNewLine & "No es posible anular...", vbExclamation
                Exit Sub
        End If
        If MsgBox("Confirmar Anulacion de documento" & vbNewLine & CboDocVenta.Text & vbNewLine & "Nro. " & Me.TxtNroDocumento, vbYesNo + vbQuestion) = vbNo Then Exit Sub
        
            'Anulando documento
            '27 Agosto 2011
                SG_codigo2 = Empty
                sis_InputBox.texto.PasswordChar = "*"
                sis_InputBox.FramBox = "Ingrese clave de usuario para anulacion"
                sis_InputBox.Show 1
                Sp_Llave = UCase(SG_codigo2)
                If Len(Sp_Llave) > 0 Then
                    Sql = "SELECT usu_nombre " & _
                         "FROM sis_usuarios " & _
                         "WHERE usu_pwd=MD5('" & Sp_Llave & "')"
                    Consulta RsTmp, Sql
                    If RsTmp.RecordCount > 0 Then
                        Sp_Llave = RsTmp!usu_nombre
                    Else
                        MsgBox "Llave no encontrada...", vbExclamation
                        Exit Sub
                    End If
                Else
                    Exit Sub
                End If
        'Regenerar Kardex
                AnulacionDeDocumento
                
                Exit Sub
        
        
        
        
        
         On Error GoTo Anulando
         cn.BeginTrans
                Sql = "SELECT c.id,  c.fecha,c.doc_id,doc_nombre,c.no_documento,d.codigo pro_codigo,m.descripcion,d.unidades cantidad,precio_costo/unidades unitario,doc_movimiento " & _
                      "FROM ven_detalle d " & _
                      "INNER JOIN ven_doc_venta c ON (d.doc_id=c.doc_id AND d.no_documento=c.no_documento) " & _
                      "INNER JOIN maestro_productos m ON m.codigo=d.codigo " & _
                      "INNER JOIN sis_documentos s ON c.doc_id=s.doc_id " & _
                      "WHERE c.rut_emp='" & SP_Rut_Activo & "'  AND c.no_documento=" & TxtNroDocumento & "  AND m.rut_emp='" & SP_Rut_Activo & "' AND d.rut_emp='" & SP_Rut_Activo & "' " & _
                          " AND c.doc_id=" & CboDocVenta.ItemData(CboDocVenta.ListIndex)
                Consulta RsTmp, Sql
                If RsTmp.RecordCount > 0 Then
                    Lp_IdMantemer = RsTmp!ID
                    RsTmp.MoveFirst
                    Do While Not RsTmp.EOF
                         Kardex Format(Date, "YYYY-MM-DD"), IIf(RsTmp!doc_movimiento = "ENTRADA", "SALIDA", "ENTRADA"), RsTmp!doc_id, RsTmp!no_documento, IG_id_Bodega_Ventas, RsTmp!pro_codigo, _
                          RsTmp!cantidad, "ANULA " & RsTmp!doc_nombre & " Nro:" & RsTmp!no_documento, RsTmp!unitario, RsTmp!unitario * RsTmp!cantidad, , , , , CboDocVenta.ItemData(CboDocVenta.ListIndex), , , , 0
                        RsTmp.MoveNext
                    Loop
                End If
            
            
                'CONSULTAMOS SALDO ACTUAL DE LA CTA CTE
                If DG_ID_Unico > 0 Then ctacte TxtRut, "CLI", CboDocVenta.ItemData(CboDocVenta.ListIndex), Format(DtFecha.Value, "YYYY-MM-DD"), "ANULACION " & CboDocVenta.Text & " " & TxtNroDocumento, TxtNroDocumento, CDbl(SkBrutoMateriales), True
       
            'Fin cta corriente
            
         'DEBEMOS CONSULTAR SI EXISTEN DOCUMENTOS QUE HACEN REFERENCIA AL QUE SE ESTA INGRESANDO
        Sql = "UPDATE ven_doc_venta c " & _
                     "SET id_ref=0,doc_id_factura=0,nro_factura=0 " & _
              "WHERE c.rut_emp='" & SP_Rut_Activo & "'  AND c.nro_factura=" & TxtNroDocumento & _
                          " AND c.doc_id_factura=" & CboDocVenta.ItemData(CboDocVenta.ListIndex)
        cn.Execute Sql
            
        Sql = "DELETE FROM ven_doc_venta,ven_detalle USING ven_doc_venta " & _
              "LEFT JOIN ven_detalle " & _
              "ON( ven_doc_venta.doc_id= ven_detalle.doc_id AND ven_doc_venta.no_documento= ven_detalle.no_documento) " & _
              "WHERE ven_doc_venta.rut_emp='" & SP_Rut_Activo & "' AND " & _
              "ven_doc_venta.doc_id=" & CboDocVenta.ItemData(CboDocVenta.ListIndex) & " AND ven_doc_venta.no_documento=" & TxtNroDocumento
              
         cn.Execute Sql
         
         Sql = "INSERT INTO ven_doc_venta (id,fecha,doc_id,no_documento,rut_emp,rut_cliente,tipo_doc,nombre_cliente," & _
                                          "tipo_movimiento,usu_nombre) VALUES(" & _
                        Lp_IdMantemer & "," & _
                         "'" & Format(DtFecha.Value, "YYYY-MM-DD") & _
                         "'," & CboDocVenta.ItemData(CboDocVenta.ListIndex) & _
                         "," & TxtNroDocumento & _
                         ",'" & SP_Rut_Activo & _
                         "','NULO' " & _
                         ",'" & CboDocVenta.Text & _
                         "','ANULADO','NULO','" & Sp_Llave & "')"
         cn.Execute Sql
         AutoIncremento "GUARDA", CboDocVenta.ItemData(CboDocVenta.ListIndex), TxtNroDocumento, IG_id_Sucursal_Empresa
         
        cn.CommitTrans
         MsgBox "Documento anulado " & vbNewLine & "AUTORIZADO POR:" & Sp_Llave
         Unload Me
         Exit Sub
    End If
    Exit Sub
Anulando:
    cn.RollbackTrans
    MsgBox "Hubo un error al intentar anular el documento..." & vbNewLine & Err.Number & vbNewLine & Err.Description, vbInformation
End Sub



Private Sub CmdBuscaActivo_Click()
    SG_codigo = Empty
    SG_codigo2 = CboCuenta.ItemData(CboCuenta.ListIndex)
    Con_ActivoInmovilizado.CboFiltroActivo.Enabled = False
    Con_ActivoInmovilizado.Sm_FiltroCta = " AND a.pla_id=" & SG_codigo2
    Con_ActivoInmovilizado.CmdSelecciona.Visible = True
    Con_ActivoInmovilizado.Show 1
    If Val(SG_codigo) > 0 Then
        Sql = "SELECT aim_nombre,aim_valor " & _
                "FROM con_activo_inmovilizado " & _
                "WHERE aim_id=" & Val(SG_codigo)
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
            If SP_Control_Inventario = "NO" Then
                TxtCodigoActivo = SG_codigo
                TxtNombreActivo = RsTmp!aim_nombre
                
                If TxtSDNeto.Enabled Then
                    TxtSDNeto = RsTmp!aim_valor
                    TxtSDNeto.SetFocus
                Else
                    TxtSdExento = RsTmp!aim_valor
                    TxtSdExento.SetFocus
                End If
            
            Else
                TxtCodigo = SG_codigo
                TxtDescrp = RsTmp!aim_nombre
                TxtCantidad = 1
                TxtPrecio = RsTmp!aim_valor
                txtPUni = RsTmp!aim_valor
                TxtSubTotal = RsTmp!aim_valor
            
            End If
        End If
    End If
    SG_codigo = Empty
    SG_codigo2 = Empty
End Sub

Private Sub CmdBuscaCliente_Click()
    LlamaClienteDe = "VD"
    'BuscaCliente.AdoCliente.Recordset.Filter = 0
    'Set BuscaCliente.MshClientes.DataSource = BuscaCliente.AdoCliente.Recordset
    ClienteEncontrado = False
    BuscaCliente.Show 1
    TxtRut = SG_codigo
  '  TxtDscto.SetFocus
   ' SendKeys ("{TAB}")
    Me.SumaListaMaterialesYMobra
    CompruebaCambioCliente
    'Call SaldoCliente(TxtRut, TxtRazonSocial)
    TxtRut.SetFocus
End Sub

Private Sub CmdBuscaProducto_Click()
  
    If Sp_EmpresaRepuestos = "SI" Then
        Busca_Producto_Repuestos.Show 1
    Else
        BuscaProducto.Show 1
    End If
    TxtCodigo = SG_codigo
    If RegSeleccionado Then
        Me.TxtCantidad.SetFocus
    Else
        On Error Resume Next
        Me.TxtCodigo.SetFocus
    End If
End Sub


Private Sub cmdCancel_Click()
    frmEspecial.Visible = False
    VentaEspecial = False
    LvMateriales.ListItems.Clear
     frmEspecial.Visible = False
    CMDeliminaMaterial.Enabled = True
    CmdEspecial.Enabled = True
    SumaListaMaterialesYMobra
End Sub

Private Sub cmdCuenta_Click()
    With BuscarSimple
        SG_codigo = Empty
        .Sm_Consulta = "SELECT pla_id, pla_nombre,tpo_nombre,det_nombre " & _
                       "FROM    con_plan_de_cuentas p,  con_tipo_de_cuenta t,   con_detalle_tipo_cuenta d " & _
                       "WHERE   p.tpo_id = t.tpo_id AND p.det_id = d.det_id "
        .Sm_CampoLike = "pla_nombre"
        .Im_Columnas = 2
        .Show 1
        If SG_codigo = Empty Then Exit Sub
        Busca_Id_Combo CboCuenta, Val(SG_codigo)
    End With
End Sub

Private Sub CMDeliminaMaterial_Click()
    If LvMateriales.SelectedItem Is Nothing Then Exit Sub
    LvMateriales.ListItems.Remove LvMateriales.SelectedItem.Index
    SumaListaMaterialesYMobra
End Sub




Private Sub CmdEmiteDocumento_Click()
    Dim ip_Sucursal_id As Integer, Ip_PlaId As Long, Ip_AreId As Long, Ip_CenId As Long, Dp_FactorComision As Double
    Dim Ip_Giro_Id As Integer, iP_RS As Integer
    Dim Sp_Modalidad As String, Ip_TipoNotaCredito As Integer
    Dim Sp_NC_Utilizada As String
    Dim Lp_SaldoRef As Long
    Dim Bp_Comprobante_Pago As Boolean
    Dim FormaDelPago As String
    Dim elplazo As String
    Dim i As Integer, d As Integer
    Dim sql_values As String
    Dim elCodigo As String
    Dim elbruto As Variant
    Dim elneto As Variant
    Dim Lp_NuevoNumero As Long
    Ip_TipoNotaCredito = 0
    Sp_Modalidad = IIf(CboModalidad.ItemData(CboModalidad.ListIndex) = 1, "VTA", "ARR")
    If CboFpago.ListIndex < 0 Then
        MsgBox "SELECCIONE FORMA DE PAGO...", vbOKOnly + vbInformation
        CboFpago.SetFocus
        Exit Sub
    End If
    iP_RS = 0
    CuentaLineas
    'validamos cantidad de lineas por documento
    If Mid(CmdEmiteDocumento.Caption, 1, 4) = "Emit" Then
        If LvMateriales.ListItems.Count > Val(SkCantLineas.Tag) Then
            MsgBox "Ya sobrepas� la cantidad de lineas para este documento...", vbInformation
            Exit Sub
        End If
    End If
    If CboRs.Visible Then iP_RS = CboRs.ItemData(CboRs.ListIndex)
    If CmdEmiteDocumento.Caption = "Ver PDF" Then
        FrmLoad.Visible = True
        DoEvents
        Sql = "SELECT doc_cod_sii " & _
                "FROM sis_documentos " & _
                "WHERE doc_id=" & CboDocVenta.ItemData(CboDocVenta.ListIndex)
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
            SG_LlevaIVAAnticipado = "NO"
            If Val(Me.TxtIvaAnticipado) > 0 Then
                SG_LlevaIVAAnticipado = "SI"
            End If
            Dim VerCedible As Boolean
            VerCedible = False
            If ChkCedible.Value = 1 Then VerCedible = True
            Archivo = "C:\FACTURAELECTRONICA\PDF\" & Replace(SP_Rut_Activo, ".", "") & "\T" & Trim(Str(RsTmp!doc_cod_sii)) & "F" & Me.TxtNroDocumento & ".PDF"
            dte_MostrarPDF.Dte Str(RsTmp!doc_cod_sii), TxtNroDocumento, VerCedible
          ' MostrarPDF RsTmp!doc_cod_sii
        End If
        FrmLoad.Visible = False
        Exit Sub
    
    End If
    If UCase(Mid(CmdEmiteDocumento.Caption, 1, 2)) = "RE" Then
        '21 Enero 2015
        'Sabes que viene de reimpresion
        'Entonces detectar si es nota de venta, y si esta cambiando el documento, en ese caso, se emite el documento
        'y la NV desaparece.
        If CboSucursal.ListIndex = -1 Then CboSucursal.ListIndex = 0
        If Val(Frame1.Tag) > 0 Then
            Sql = "SELECT doc_nota_de_venta " & _
                    "FROM sis_documentos " & _
                    "WHERE doc_id=" & CboDocVenta.ItemData(CboDocVenta.ListIndex)
            Consulta RsTmp, Sql
            If RsTmp.RecordCount > 0 Then
                If RsTmp!doc_nota_de_venta = "NO" Then
                    'Aca ya sabemos que se trata de otro documento que sera cambiado por la nota de venta
                    CmdEmiteDocumento.Caption = "EMITIENDO DOCUMENTO...-"
                End If
            End If
        End If
    End If
    
    If UCase(Mid(CmdEmiteDocumento.Caption, 1, 2)) <> "RE" Then
        FrmLoad.Visible = True
        DoEvents
        If SP_Rut_Activo = "11.907.734-6" Then
            If Len(TxtOrdenesCompra) = 0 Then
                FrmLoad.Visible = False
                If MsgBox("No ha ingresado Orden de Compra..!!!" & vbNewLine & " �Continuar?...", vbQuestion + vbYesNo) = vbNo Then
                    TxtOrdenesCompra.SetFocus
                    Exit Sub
                End If
                FrmLoad.Visible = True
                DoEvents
            End If
        End If
        
        'POR AHORA SOLO TRANSPORTES ESPUMAS GUIDO NU�OZ
         '14 ABRIL 2020 - CONFINAMIENTO COVID
         If SP_Rut_Activo = "77.416.270-4" Then
            If Len(TxtNombre) = 0 Then
                MsgBox "Debe ingresar informacion adicional ...", vbInformation
                FrmLoad.Visible = False
                Exit Sub
            End If
         End If
         If Sm_PermiteFechaFueraPeriodo = "NO" Then
            If Month(DtFecha.Value) <> IG_Mes_Contable Or Year(DtFecha.Value) <> IG_Ano_Contable Then
                 MsgBox "Fecha de venta no corresponde al periodo contable..."
                 DtFecha.SetFocus
                 FrmLoad.Visible = False
                 Exit Sub
            End If
        End If
    
        sql2 = "SELECT pla_requiere_validacion valida " & _
            "FROM par_plazos_vencimiento " & _
            "WHERE pla_id=" & Val(Right(CboPlazos.Text, 10))
        Consulta RsTmp2, sql2
        If RsTmp2!valida = "SI" Then
            If CDbl(Me.SkTotalMateriales) > Lp_MontoCreditoAprobado Then
                MsgBox "El cliente seleccionado no esta habilitado para ventas al credito..."
                TxtRut = ""
                FrmLoad.Visible = False
                Exit Sub
            End If
        End If
   
    
        On Error GoTo errorGrabar
        cn.BeginTrans
        If CboVendedores.ListIndex = -1 Then
            MsgBox "No ha seleccionado Vendedor..."
            CboVendedores.SetFocus
            cn.RollbackTrans
            Exit Sub
        End If
        
        If LvMateriales.ListItems.Count = 0 And SP_Control_Inventario = "SI" Then
            FrmLoad.Visible = False
            MsgBox "Debe agregar alg�n producto... ", vbOKOnly + vbInformation
            TxtCodigo.SetFocus
            cn.RollbackTrans
            
            Exit Sub
        ElseIf CboDocVenta.ListIndex = -1 Then
            FrmLoad.Visible = False
            MsgBox "Seleccione alg�n documento de venta...", vbOKOnly + vbInformation
            CboDocVenta.SetFocus
            cn.RollbackTrans
            Exit Sub
        ElseIf Val(TxtNroDocumento) = 0 Then
        
            If SG_Equipo_Solo_Nota_de_Venta = "SI" Then
                'AQUI SE GENERA EL NUMERO DE NOTA DE VENTA
                TxtNroDocumento = AutoIncremento("LEE", CboDocVenta.ItemData(CboDocVenta.ListIndex), , IG_id_Sucursal_Empresa)
            Else
                'PROCESO NORMAL , ES NECESARIO EL NRO DE DOCUMETNO
                FrmLoad.Visible = False
                MsgBox "No ha ingresado Nro de Documento...", vbOKOnly + vbInformation
                TxtNroDocumento.SetFocus
                cn.RollbackTrans
                Exit Sub
                'NADA
            End If
        
        End If
    
        If SP_Control_Inventario = "NO" Then
                If CboCuenta.ListIndex < 0 Or CboArea.ListIndex < 0 Or CboCentroCosto.ListIndex < 0 Then
                    FrmLoad.Visible = False
                    MsgBox "Faltan datos contables..." & vbNewLine & "Cuenta - Area - Centro Costo", vbInformation
                    CboCuenta.SetFocus
                    cn.RollbackTrans
                    Exit Sub
                End If
        End If
        If CDbl(SkBrutoMateriales) < 1 Then
            MsgBox "Total de documento no es v�lido...", vbInformation
            FrmLoad.Visible = False
            If SP_Control_Inventario = "NO" Then SkTotalMateriales.SetFocus Else TxtCodigo.SetFocus
            cn.RollbackTrans
            Exit Sub
        End If
        
        'Calcular comision vendedor
        Dp_FactorComision = 0
        Sql = "SELECT ven_comision " & _
                "FROM par_vendedores " & _
                "WHERE ven_id=" & CboVendedores.ItemData(CboVendedores.ListIndex)
        Consulta RsTmp, Sql 'Extraemos factor
        If RsTmp.RecordCount > 0 Then Dp_FactorComision = RsTmp!ven_comision
        'Ahora viene el calculo
        If Dp_FactorComision > 0 Then
            TxtComisionVendedor = Int(SkTotalMateriales / 100 * Dp_FactorComision)
            Sql = "SELECT doc_signo_libro signo " & _
                  "FROM sis_documentos " & _
                  "WHERE doc_id=" & CboDocVenta.ItemData(CboDocVenta.ListIndex)
            Consulta RsTmp, Sql 'Si el documento es nota de credito debe dejar comision negativa
            If RsTmp.RecordCount > 0 Then If RsTmp!Signo = "-" Then TxtComisionVendedor = TxtComisionVendedor * -1
        Else
            TxtComisionVendedor = "0"
        End If
        
        
        Sql = "SELECT doc_requiere_rut,doc_mueve_inventario " & _
              "FROM sis_documentos " & _
              "WHERE doc_id=" & CboDocVenta.ItemData(CboDocVenta.ListIndex)
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
            Bm_Modifica_Inventario = IIf(RsTmp!doc_mueve_inventario = "SI", True, False)
            If RsTmp!doc_requiere_rut = "SI" Then
                If Len(TxtRut) = 0 Then
                    FrmLoad.Visible = False
                    MsgBox "Necesita ingresar Cliente para este documento...", vbOKOnly + vbInformation
                    TxtRut.SetFocus
                    cn.RollbackTrans
                    Exit Sub
                End If
            End If
        End If
    
        If Sm_RetieneIva = "SI" Then
            If Val(TxtIVARetenido) = 0 Then
                FrmLoad.Visible = False
                MsgBox "Falta Valor IVA retenido...", vbExclamation + vbOKOnly
                TxtIVARetenido.SetFocus
                cn.RollbackTrans
                Exit Sub
            End If
        End If
        '24 Jun 15
        'Validaciones listas
        DoEvents
    
         If CboSucursal.ListCount > 0 Then
             ip_Sucursal_id = CboSucursal.ItemData(CboSucursal.ListIndex)
         Else
             ip_Sucursal_id = 0
         End If
        
        If CboGiros.ListCount = 0 Then
            CboGiros.AddItem "PRINCIPAL"
            CboGiros.ListIndex = 0
        ElseIf CboGiros.ListIndex = -1 Then
            CboGiros.ListIndex = 0
        End If
         
         Dim Sp_Fpago As String
         Sp_Fpago = Me.CboFpago.Text
         
         
         If SP_Control_Inventario = "NO" Then
             'sabemos que estamos editando un documento
             '25 Agosto 2011
             Ip_PlaId = CboCuenta.ItemData(CboCuenta.ListIndex)
             Ip_AreId = CboArea.ItemData(CboArea.ListIndex)
             Ip_CenId = CboCentroCosto.ItemData(CboCentroCosto.ListIndex)
             If Me.Tag = "VIEJA" Then
                Sql = "SELECT condicionpago " & _
                        "FROM ven_doc_venta " & _
                        "WHERE id=" & DG_ID_Unico & _
                        " AND rut_emp='" & SP_Rut_Activo & "'"
                Consulta RsTmp, Sql
                If RsTmp.RecordCount > 0 Then
                    If RsTmp!condicionpago = "CONTADO" Then
                        'SIGUEL
                        Sql = "SELECT a.abo_id " & _
                             "FROM cta_abonos a " & _
                             "INNER JOIN cta_abono_documentos d ON a.abo_id=d.abo_id " & _
                             "WHERE d.rut_emp='" & SP_Rut_Activo & "' AND a.rut_emp='" & SP_Rut_Activo & "' AND abo_cli_pro='CLI' AND d.id=" & DG_ID_Unico
                        Consulta RsTmp, Sql
                        If RsTmp.RecordCount > 0 Then
                            'Eliminamos abnos asociados al documento cuando
                            'fue cancelado al contado
                            cn.Execute "DELETE FROM cta_abonos WHERE abo_id=" & RsTmp!abo_id
                            cn.Execute "DELETE FROM cta_abono_documentos WHERE abo_id=" & RsTmp!abo_id
                            cn.Execute "DELETE FROM abo_tipos_de_pagos WHERE abo_id=" & RsTmp!abo_id
                        End If
                    Else
                        If Mid(CboPlazos.Text, 1, 25) <> "CONTADO" Then
                            'SIGUE
                            
                        Else
                            cn.RollbackTrans
                            FrmLoad.Visible = False
                            MsgBox "Este documento fue ingresado al CREDITO..." & vbNewLine & "No es posible cambiar su forma de pago, debe eliminarlo e ingresarlo nuevamente...", vbInformation + vbOKOnly
                            Exit Sub
                        End If
                    End If
                
                End If
             
             
                 Sql = "DELETE FROM ven_doc_venta " & _
                       "WHERE id=" & DG_ID_Unico
                       
                 cn.Execute Sql
  
             End If
         
         Else
             
             Ip_PlaId = 0
             Ip_AreId = 0
             Ip_CenId = 0
             
         End If
    
    
        Sql = "SELECT no_documento " & _
              "FROM ven_doc_venta " & _
              "WHERE rut_emp='" & SP_Rut_Activo & "' AND no_documento = " & TxtNroDocumento & " AND doc_id=" & CboDocVenta.ItemData(CboDocVenta.ListIndex)
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
            FrmLoad.Visible = False
            MsgBox "Este Nro de Documento ya ha sido utilizado ...", vbOKOnly + vbInformation
            TxtNroDocumento.SetFocus
            cn.RollbackTrans
            Exit Sub
        End If
        Timer1.Enabled = False
        bm_NoTimer = False
        
        Lp_Id_Nueva_Venta = UltimoNro("ven_doc_venta", "id")
        
        
                'POR AHORA SOLO TRANSPORTES ESPUMAS GUIDO NU�OZ
         '14 ABRIL 2020 - CONFINAMIENTO COVID
         If SP_Rut_Activo = "77.416.270-4" Then
                Sql = "INSERT INTO ven_add_transportes (tra_nombre_conductor,tra_rut_conductor,tra_patente,tra_fecha_despacho,tra_destino,tra_rango,tra_productos_objeto,id_venta) " & _
                "VALUES('" & TxtNombre & "','" & TxtRutConductor & "','" & TxtPatente & "','" & Fql(Me.DtFechaDespacho) & "','" & Me.TxtDestinoDespacho & "','" & Me.TxtRangoHorario & "','" & Me.TxtProductoObjeto & "'," & Lp_Id_Nueva_Venta & ")"
                cn.Execute Sql
         End If
        
        
        Sp_NC_Utilizada = "NO"
        If Bm_Es_Nota_de_Credito Then
            'Buscar el documento referenciado    /// 20-10-2013
            'y ver si su saldo.
            'si el saldo es mayor o igual a la NC abonar el monto de la nota de credito
            Lp_SaldoRef = SaldoDocumento(Val(lP_Documento_Referenciado), "CLI")
            
            '10-10-2013
            'si el saldo del documento es mayor a 0, abonar la nc parte de ella para completar el pago, si la
            'nc es mayor al saldo, el resto enviarlo al pozo, referenciando la nc, para poder eliminar ese
            'abono en caso que se elimine la nc
            
            '13'10'2014
            'definir el tipo de nota de credito (se utilizar en DTE)
            Ip_TipoNotaCredito = Val(SkCodigoReferencia)
            
            
            
            Dim Lp_Abono_a_Realizar As Long
            Dim Lp_Abono_a_Pozo As Long
        '    If Lp_SaldoRef >= CDbl(SkBrutoMateriales) Then
            If Lp_SaldoRef > 0 Then
                    Lp_Abono_a_Pozo = 0
                    If Lp_SaldoRef >= CDbl(SkBrutoMateriales) Then
                        Lp_Abono_a_Realizar = CDbl(SkBrutoMateriales)
                    Else
                        Lp_Abono_a_Realizar = Lp_SaldoRef
                        Lp_Abono_a_Pozo = CDbl(SkBrutoMateriales) - Lp_SaldoRef
                    End If
                    
                    'hacer abono
                    MsgBox "El monto " & NumFormat(Lp_Abono_a_Realizar) & " de esta NC se abon� al documento original...", vbInformation
                    Lp_Nro_Comprobante = AutoIncremento("lee", 200, , IG_id_Sucursal_Empresa)
                
                   
                    Bp_Comprobante_Pago = True
                    Lp_IdAbo = UltimoNro("cta_abonos", "abo_id")
                    PagoDocumento Lp_IdAbo, "CLI", TxtRut, DtFecha, CDbl(Lp_Abono_a_Realizar), 8888, "NC", "ABONO CON NC", LogUsuario, Lp_Nro_Comprobante, Lp_Id_Nueva_Venta, "VENTA"
                    Sql = "INSERT INTO cta_abono_documentos (abo_id,id,ctd_monto,rut_emp) VALUES "
                    Sql = Sql & "(" & Lp_IdAbo & "," & lP_Documento_Referenciado & "," & CDbl(Lp_Abono_a_Realizar) & ",'" & SP_Rut_Activo & "')"
                    cn.Execute Sql
                    
                    AutoIncremento "GUARDA", 200, Lp_Nro_Comprobante, IG_id_Sucursal_Empresa
                    
                    Sp_NC_Utilizada = "SI"
                    
                    
            
            Else
                    'Aqui detectamos que el el documento ya fue pagado
                    'por lo tanto el valor de esta factura pasa a favor del cliente
                    Lp_Abono_a_Pozo = CDbl(SkBrutoMateriales)
            End If
            'Aqui agragaremos al pozo el saldo de la nc
            Sql = "INSERT INTO cta_pozo (id_ref,pzo_fecha,rut_emp,pzo_cli_pro,pzo_rut,pzo_abono) " & _
                        "VALUES(" & Lp_Id_Nueva_Venta & ",'" & Fql(DtFecha) & "','" & SP_Rut_Activo & "','CLI','" & TxtRut & "'," & Lp_Abono_a_Pozo & ")"
            cn.Execute Sql
             Sp_NC_Utilizada = "SI"
                    'PagoDocumento Lp_Id, "PRO", Me.TxtRutProveedor, Me.DtFecha, Lp_SaldoRef, 8888, "DESCUENTA NC", "", LogUsuario,
        End If
    
            'Miercoles 2 de Febrero 2011
            'ahora procedemos a grabar el documento de venta
             
             ''aqui pregunto fomra de pago
        
        TipoDocumentoVenta = CboDocVenta.Text

        FormaDelPago = "PENDIENTE"
        '2 de febrero 2011
        If Val(Frame1.Tag) > 0 Then 'Si cargamos una Nota de Venta, la dejamos como terminada 5-1-2012
            cn.Execute "UPDATE ven_doc_venta SET ven_estado_nota_venta='TERMINADA' WHERE id=" & Frame1.Tag
            Frame1.Tag = 0
        End If
            
        AutoIncremento "GUARDA", CboDocVenta.ItemData(CboDocVenta.ListIndex), TxtNroDocumento, IG_id_Sucursal_Empresa
        If Len(TxtRut) = 0 Then TxtRut = "11.111.111-1"
        'aqui grabamos el documento de venta
        
        '14 Dic 2013
        'Id del plazo, identificar el id del plazo, ya que el id del combo plazo son los dias
            elplazo = Right(CboPlazos.Text, 5)
        tipodespacho = 0
        tipotraslado = 0
            
        '*********************************************
        If Me.CboTipoDespacho.ListCount > 0 Then
            If CboTipoDespacho.ListIndex > -1 Then tipodespacho = CboTipoDespacho.ItemData(CboTipoDespacho.ListIndex)
        End If
        
        If Me.CboTipoTraslado.ListCount > 0 Then
            If CboTipoTraslado.ListIndex > -1 Then tipotraslado = CboTipoTraslado.ItemData(CboTipoTraslado.ListIndex)
        End If
        
        If Val(TxtCreditoEmpresaConstructora) > 0 Then
            
        
        End If
        
        'Aqui detectamos si es espumas
        If CboSucEmpresa.Visible Then
            lasucursalempresa = CboSucEmpresa.ItemData(CboSucEmpresa.ListIndex)
        Else
            lasucursalempresa = IG_id_Sucursal_Empresa
        End If
        
        Sql = "INSERT INTO ven_doc_venta " & _
              "(id,no_documento,fecha,rut_cliente,nombre_cliente,tipo_movimiento,neto,bruto,iva,comentario,CondicionPago," & _
              "ven_id,ven_nombre,ven_comision,forma_pago,tipo_doc,doc_id,suc_id,ven_fecha_vencimiento," & _
              "usu_nombre,id_ref,rut_emp,pla_id,are_id,cen_id,exento,ven_iva_retenido,bod_id,ven_plazo,ven_comentario," & _
              "ven_ordendecompra,ven_tipo_calculo,ven_venta_arriendo,ven_nc_utilizada,caj_id,sue_id,ven_plazo_id," & _
              "ven_entrega_inmediata,gir_id,rso_id,tnc_id,ven_boleta_hasta,doc_time,doc_genero,ven_guia_tipo_despacho,ven_guia_tipo_traslado,patente,doc_afecto_comision,ven_centro_costo,ven_solicitante) " & _
              "VALUES(" & Lp_Id_Nueva_Venta & "," & _
              CDbl(TxtNroDocumento) & ",'" & Format(DtFecha, "YYYY-MM-DD") & "','" & TxtRut & "','" & TxtRazonSocial & "','VD'," & _
              CDbl(SkTotalMateriales) & "," & CDbl(SkBrutoMateriales) & "," & CDbl(SkIvaMateriales) & "," & _
              "'" & TxtMotivoRef & "','" & Sp_Fpago & "'," & _
              CboVendedores.ItemData(CboVendedores.ListIndex) & ",'" & _
              CboVendedores.Text & "'," & _
              TxtComisionVendedor & ",'" & _
              FormaDelPago & "','" & _
              CboDocVenta.Text & "'," & _
              CboDocVenta.ItemData(CboDocVenta.ListIndex) & "," & _
              ip_Sucursal_id & ",'" & Format(DtFecha.Value + CboPlazos.ItemData(CboPlazos.ListIndex), "YYYY-MM-DD") & "','" & _
              LogUsuario & "'," & TxtNroReferencia.Tag & ",'" & SP_Rut_Activo & "'," & Ip_PlaId & "," & Ip_AreId & "," & _
              Ip_CenId & "," & CDbl(TxtExentos) & "," & CDbl(TxtIVARetenido) & "," & IG_id_Bodega_Ventas & _
              "," & CboPlazos.ItemData(CboPlazos.ListIndex) & ",'" & TxtComentario & "','" & Me.TxtOrdenesCompra & _
              "'," & Im_Tcal & ",'" & Sp_Modalidad & "','" & Sp_NC_Utilizada & "'," & LG_id_Caja & "," & lasucursalempresa & "," & _
              elplazo & ",'" & Me.CboEntregaInmediata.Text & "'," & CboGiros.ItemData(CboGiros.ListIndex) & "," & iP_RS & "," & _
              Ip_TipoNotaCredito & "," & TxtNroDocumento & ",curtime(),'" & CboGenero.Text & "'," & tipodespacho & "," & tipotraslado & ",'" & TxtCreditoEmpresaConstructora & "','" & Fql(DtFechaOC) & "','" & Me.TxtCentroCosto & "','" & Me.TxtSolicitante & "')"
            
              cn.Execute Sql
              
        'Cta Cte
        'OBSOLETA no se utiliza en ningun lugar
        ' 10 Agosto 2013
        'CtaCte TxtRut, "CLI", CboDocVenta.ItemData(CboDocVenta.ListIndex), Format(DtFecha.Value, "YYYY-MM-DD"), "VENTA CON " & CboDocVenta.Text & " " & TxtNroDocumento, TxtNroDocumento, CDbl(SkBrutoMateriales), False
              
        Me.Tag = "NUEVA"
        If SP_Control_Inventario = "NO" Then
            'Si no lleva control de inventario limpiamos los
            'controles para ingrear un nuevo documento
            'Else ' '  ACA GRABAMOS EL DETALLE SIN DETALLE 6 SEPTIEMBRE 2011
            If Not Bm_Nueva_Ventas Then
                Sql = "DELETE FROM ven_sin_detalle WHERE id=" & Lp_Id_Nueva_Venta
                cn.Execute Sql
            End If
        
            Sql = "INSERT INTO ven_sin_detalle(id,vsd_exento,vsd_neto,vsd_iva,pla_id,are_id,cen_id,aim_id) VALUES "
            With LvSinDetalle
            
                For i = 1 To .ListItems.Count
                    sql_values = sql_values & "(" & Lp_Id_Nueva_Venta & "," & CDbl(.ListItems(i).SubItems(6)) & "," & _
                    CDbl(.ListItems(i).SubItems(7)) & "," & _
                    CDbl(.ListItems(i).SubItems(8)) & "," & _
                    CDbl(.ListItems(i).SubItems(9)) & "," & _
                    CDbl(.ListItems(i).SubItems(10)) & "," & _
                    CDbl(.ListItems(i).SubItems(11)) & "," & _
                    Val(.ListItems(i).SubItems(4)) & "),"
                    
                    'Tambien debemos darle la salida a los activos
                    'inmovilizados, fecha de salida y estado a inactivo.
                    
                    If Val(.ListItems(i).SubItems(4)) > 0 Then
                            cn.Execute "UPDATE con_activo_inmovilizado " & _
                                        "SET aim_fecha_egreso='" & Fql(DtFecha) & "',aim_habilitado='NO' " & _
                                        "WHERE aim_id=" & Val(.ListItems(i).SubItems(4))
                    End If
                    
                Next
            End With
            
            sql_values = Mid(sql_values, 1, Len(sql_values) - 1)
            cn.Execute Sql & sql_values
            
            
            'Exit Sub
        End If
        
        'Miercoles 12 Feb 2020
        'Grabar la lista de los referenciados.
        'Guias y notas de pedido  ''   Por ahora para pendola
        
        If Me.LvGuiasReferencia.ListItems.Count > 0 Then
            cn.Execute "DELETE FROM ven_doc_venta_guias_ref " & _
                        "WHERE id =" & Lp_Id_Nueva_Venta
            For i = 1 To LvGuiasReferencia.ListItems.Count
                Sql = "INSERT INTO ven_doc_venta_guias_ref (id,gur_fecha,gur_nro,doc_id) " & _
                        "VALUES(" & Lp_Id_Nueva_Venta & ",'" & Fql(LvGuiasReferencia.ListItems(i).SubItems(2)) & "'," & LvGuiasReferencia.ListItems(i).SubItems(3) & "," & LvGuiasReferencia.ListItems(i).SubItems(5) & ")"
                cn.Execute Sql
            Next
        
        End If
'
        'Aqui se Graban los materiales
        'If SP_Control_Inventario = "SI" Then
            With LvMateriales
                If .ListItems.Count > 0 Then
                
                
                    '25 de Enero de 2014
                    'Cambiaremos por un Insert
                
                    'Sql = "SELECT * " & _
                          "FROM ven_detalle " & _
                          "WHERE 'x'='r'"
                    'Consulta RsTmp, Sql
                    Sql = "INSERT INTO ven_detalle (codigo,marca,descripcion,precio_real," & _
                        "descuento,unidades,precio_final,subtotal,precio_costo,btn_especial," & _
                        "comision,fecha,no_documento,doc_id,rut_emp,pla_id,are_id,cen_id,aim_id," & _
                        "ved_precio_venta_bruto,ved_precio_venta_neto) VALUES"
                    sql2 = ""
                    For i = 1 To .ListItems.Count
                     '   RsTmp.AddNew
                     
                        'Aqui comprobamos si corresponde a Activo Inmovilizado
                        If Val((.ListItems(i).SubItems(20))) = 0 Then
                            elCodigo = .ListItems(i).SubItems(1)
                        Else
                            elCodigo = 0
                        End If
                        
                        If Im_Tcal = 3 Then
                            'Si los valoles son exentos iguala el campo neto y bruto
                            elbruto = CDbl(.ListItems(i).SubItems(8))
                            elneto = CDbl(.ListItems(i).SubItems(8)) ' / Val("1." & DG_IVA)
                        
                        Else
                            
                            If Sm_Con_Precios_Brutos = "SI" Then
                                elbruto = CDbl(.ListItems(i).SubItems(8))
                                elneto = CxP(CDbl(.ListItems(i).SubItems(8)) / Val("1." & DG_IVA))
                            Else
                                elneto = CDbl(.ListItems(i).SubItems(8))
                                elbruto = CxP(CDbl(.ListItems(i).SubItems(8)) + (CDbl(.ListItems(i).SubItems(8)) * DG_IVA / 100))
                            End If
                        End If
                        
                        If SP_Rut_Activo = "76.337.408-4" Or SP_Rut_Activo = "10.722.977-9" Or SP_Rut_Activo = "12.318.896-9" Then
                        
                            sql2 = sql2 & "('" & _
                               elCodigo & "','" & .ListItems(i).SubItems(2) & "','" & .ListItems(i).SubItems(3) & "'," & _
                               .ListItems(i).SubItems(4) & "," & CDbl(.ListItems(i).SubItems(5)) & "," & _
                               CxP(.ListItems(i).SubItems(6)) & "," & .ListItems(i).SubItems(7) & "," & _
                               CDbl(.ListItems(i).SubItems(8)) & "," & CxP(CDbl(.ListItems(i).SubItems(10))) & ",'" & _
                               .ListItems(i).SubItems(12) & "','" & .ListItems(i).SubItems(13) & "','" & Fql(.ListItems(i).SubItems(14)) & "'," & _
                               TxtNroDocumento & "," & CboDocVenta.ItemData(CboDocVenta.ListIndex) & ",'" & SP_Rut_Activo & "'," & _
                              .ListItems(i).SubItems(17) & "," & .ListItems(i).SubItems(18) & "," & .ListItems(i).SubItems(19) & "," & _
                              Val(.ListItems(i).SubItems(20)) & "," & elbruto & "," & elneto & "),"
                        Else
                               
                               sql2 = sql2 & "('" & _
                               elCodigo & "','" & .ListItems(i).SubItems(2) & "','" & .ListItems(i).SubItems(3) & "'," & _
                               CDbl(.ListItems(i).SubItems(4)) & "," & CDbl(.ListItems(i).SubItems(5)) & "," & _
                               CxP(.ListItems(i).SubItems(6)) & "," & CDbl(.ListItems(i).SubItems(7)) & "," & _
                               CDbl(.ListItems(i).SubItems(8)) & "," & CxP(CDbl(.ListItems(i).SubItems(10))) & ",'" & _
                               .ListItems(i).SubItems(12) & "','" & .ListItems(i).SubItems(13) & "','" & Fql(.ListItems(i).SubItems(14)) & "'," & _
                               TxtNroDocumento & "," & CboDocVenta.ItemData(CboDocVenta.ListIndex) & ",'" & SP_Rut_Activo & "'," & _
                              .ListItems(i).SubItems(17) & "," & .ListItems(i).SubItems(18) & "," & .ListItems(i).SubItems(19) & "," & _
                              Val(.ListItems(i).SubItems(20)) & "," & elbruto & "," & elneto & "),"
                        End If
                        
                              'Tambien debemos darle la salida a los activos
                        'inmovilizados, fecha de salida y estado a inactivo.
                        If Val(.ListItems(i).SubItems(20)) > 0 Then
                                cn.Execute "UPDATE con_activo_inmovilizado " & _
                                            "SET aim_fecha_egreso='" & Fql(DtFecha) & "',aim_habilitado='NO' " & _
                                            "WHERE aim_id=" & Val(.ListItems(i).SubItems(20))
                        End If
                        If Sm_Actualiza_PrecioVenta = "SI" Then
                            'Actualizamos precio de venta en maestro o lista de precio
                            '16 Febrero 2013
                            If Val(TxtListaPrecio.Tag) = 0 Then
                                cn.Execute "UPDATE maestro_productos " & _
                                            "SET precio_venta=" & CDbl(.ListItems(i).SubItems(7)) & " " & _
                                            "WHERE codigo='" & .ListItems(i).SubItems(1) & "'"
                            Else
                            
                                cn.Execute "UPDATE par_lista_precios_detalle " & _
                                            "SET lsd_precio=" & CDbl(.ListItems(i).SubItems(7)) & " " & _
                                            "WHERE lst_id=" & TxtListaPrecio.Tag & " AND  id=" & "(SELECT id FROM maestro_productos WHERE rut_emp='" & SP_Rut_Activo & "' AND codigo=" & .ListItems(i).SubItems(1) & " LIMIT 1)"
                                'Aqui actualizaremos cuando tenga lista de precios
                                '17-7-2016
                                
                            End If
                        End If
                        
                    Next
                    If Len(sql2) > 0 Then
                        sql2 = Mid(sql2, 1, Len(sql2) - 1)
                        cn.Execute Sql & sql2
                    End If
                End If
                
                If CboModalidad.Text = "ARRIENDO" Then
                   
                    '20 JULIO 2013
                    'Agregamos a la tabla inv_productos_arrendados
                    Sql = ""
                    For i = 1 To .ListItems.Count
                        Sql = Sql & "(" & IG_id_Empresa & "," & Lp_Id_Nueva_Venta & "," & .ListItems(i).SubItems(1) & "," & CDbl(Replace(.ListItems(i).SubItems(6), ".", ",")) & "),"
                        
                    Next
                    cn.Execute "INSERT INTO inv_productos_arrendados (emp_id,ven_id,pro_codigo,arr_cantidad) " & _
                                "VALUES " & Mid(Sql, 1, Len(Sql) - 1)
                End If
                
            End With
        
        

     
        
        
        
        
        
    ''Rutina que actualiza stock con la venta (salida de productos)
        If CboModalidad.Text = "VENTA" Then
            If Bm_Modifica_Inventario Then
                If Bm_ReIntegraStock And Bm_Requiere_Referencia Then
                    ActualizaNC
                Else
                            
                    Me.ActualizaStock
                End If
            End If
         End If
         If CboFpago.Text = "CONTADO" Then
            
            'Aqui realizar abono a la cta cte
            '8 Octubre 2011
            'bar si este documento ya tiene un pago
            'Sql = "DELETE FROM cta_abonos WHERE "
            If LG_id_Caja = 0 Then
                
                Lp_Nro_Comprobante = AutoIncremento("LEE", 100, , IG_id_Sucursal_Empresa)
                Lp_IdAbo = UltimoNro("cta_abonos", "abo_id")
                PagoDocumento Lp_IdAbo, "CLI", Me.TxtRut, DtFecha, CDbl(Me.SkBrutoMateriales), 1, "EFECTIVO", "PAGO CONTADO", LogUsuario, Lp_Nro_Comprobante, 0, "VENTA"
                Sql = "INSERT INTO cta_abono_documentos (abo_id,id,ctd_monto,rut_emp) VALUES "
                Sql = Sql & "(" & Lp_IdAbo & "," & Lp_Id_Nueva_Venta & "," & CDbl(CDbl(Me.SkBrutoMateriales)) & ",'" & SP_Rut_Activo & "')"
                cn.Execute Sql
                AutoIncremento "GUARDAR", CboDocVenta.ItemData(CboDocVenta.ListIndex), Lp_Nro_Comprobante, IG_id_Sucursal_Empresa
            End If
        End If
        cn.CommitTrans
        
        
        If LG_id_Caja > 0 And bm_Documento_Permite_Pago Then
            'Empresa lleva modulo de caja 31 Octubre 2013
            'SI ES CREDITO EL PROCESO TERMINA AQUI, ENTONCES IDENTIFICAMOS EL PLAZO DE PAGO
            If CboPlazos.ItemData(CboPlazos.ListIndex) < 2 Then
                'Solo si se requiere detalle del pago
                'ejemplo CONTADO = EFECTIVO, DEBITO, T.CREDITO,
                '        CHEQUE  = DETALLE DE LOS CHEQUES.1
                Sql = " SELECT  v.id,   no_documento,doc_nombre,    v.rut_cliente,  IFNULL(cli_nombre_fantasia,nombre_rsocial)cliente, " & _
                        "IF(v.suc_id = 0,'CM',CONCAT(suc_ciudad,' - ',suc_direccion))sucursal,fecha, v.ven_fecha_vencimiento, " & _
                        "bruto,0 abonos, bruto saldo, bruto nuevo, v.suc_id " & _
                        "FROM ven_doc_venta v,maestro_clientes c,sis_documentos d,par_sucursales s " & _
                        "WHERE v.rut_emp = '" & SP_Rut_Activo & "' AND v.suc_id = s.suc_id AND v.rut_cliente = c.rut_cliente " & _
                        "AND v.doc_id = d.doc_id AND  d.doc_nota_de_credito='NO' AND v.rut_cliente = '" & TxtRut & "' AND v.id IN(" & Lp_Id_Nueva_Venta & ") " & _
                        "ORDER BY id "
                
                Consulta RsTmp, Sql
                If RsTmp.RecordCount = 0 Then GoTo ContinuandO
                
                If Mid(CboPlazos.Text, 1, 7) = "CONTADO" Then ctacte_GestionDePagos.Im_Fpago_Defecto = 1
                If Mid(CboPlazos.Text, 1, 6) = "CHEQUE" Then ctacte_GestionDePagos.Im_Fpago_Defecto = 2
                If Mid(CboPlazos.Text, 1, 14) = "DEBITO/CREDITO" Then ctacte_GestionDePagos.Im_Fpago_Defecto = 8
                
         
                ctacte_GestionDePagos.Sm_Cli_Pro = "CLI"
                ctacte_GestionDePagos.Sm_NombreAbono = Me.TxtRazonSocial ' txtCliente
                ctacte_GestionDePagos.Sm_RutAbono = TxtRut
                ctacte_GestionDePagos.Sm_RelacionMP = Right(CboPlazos.Text, 3)
                ctacte_GestionDePagos.CmdSalir.Visible = False
                ctacte_GestionDePagos.SkORIGEN = "VENTA"
                ctacte_GestionDePagos.txtSuperMensaje.Top = 6870
                ctacte_GestionDePagos.Show 1
                
                If CboDocVenta.Text = "BOLETA FISCAL" Then
                    If SG_ImpresoraFiscalBixolon = "SI" Then
                        With vtaBoletaFiscalSamsumg
                            For d = 1 To LvMateriales.ListItems.Count
                                .LVDetalle.ListItems.Add , , LvMateriales.ListItems(d).SubItems(6)
                                .LVDetalle.ListItems(.LVDetalle.ListItems.Count).SubItems(1) = LvMateriales.ListItems(d).SubItems(3)
                                .LVDetalle.ListItems(.LVDetalle.ListItems.Count).SubItems(2) = CDbl(LvMateriales.ListItems(d).SubItems(7))
                            
                            Next
                            .txtTotal = Me.SkBrutoMateriales
                            .EmiteBoleta
                        End With
                    End If
                End If
            End If
         End If
    Else
        'ESTA PARTE CORRESPONDE A LA REIMPRESION DEL DOCUMENTO DE VENTA
    
        If TxtRut = "NULO" Then
            MsgBox "Documento esta NULO...", vbOKOnly + vbInformation
            Me.CmdSalir.SetFocus
            Exit Sub
        End If
        Lp_NuevoNumero = Val(InputBox("Nro Documento", CboDocVenta.Text, TxtNroDocumento))
        If Lp_NuevoNumero = 0 Then
            MsgBox "Numero de " & CboDocVenta.Text & " No v�lido..."
            Exit Sub
        End If
        
        
        If Me.Option1 Then SG_Presentacion_Factura = "SEGUN" Else SG_Presentacion_Factura = "DETALLE"

        If Lp_NuevoNumero = TxtNroDocumento Then
            'No cambia nada
        Else
            'Cambia Nro de documento
            'Primero consultamos si el numero ya esta ocupado
            '
            Sql = "SELECT no_documento " & _
                  "FROM ven_doc_venta " & _
                  "WHERE rut_emp='" & SP_Rut_Activo & "' AND no_documento=" & Lp_NuevoNumero & " AND doc_id=" & CboDocVenta.ItemData(CboDocVenta.ListIndex)
            Consulta RsTmp, Sql
            If RsTmp.RecordCount > 0 Then
                MsgBox "El Nro ingresado ya esta en uso...", vbInformation
                Exit Sub
            End If
            
            'Actualizar documento de venta,detalle,pagos, cheques, etc
            Sql = "UPDATE ven_doc_venta " & _
                  "SET no_documento=" & Lp_NuevoNumero & _
                " WHERE  rut_emp='" & SP_Rut_Activo & "' AND  doc_id=" & CboDocVenta.ItemData(CboDocVenta.ListIndex) & " AND no_documento=" & TxtNroDocumento
            cn.Execute Sql
            Sql = "UPDATE ven_detalle " & _
                  "SET no_documento=" & Lp_NuevoNumero & _
                 " WHERE  rut_emp='" & SP_Rut_Activo & "' AND doc_id=" & CboDocVenta.ItemData(CboDocVenta.ListIndex) & " AND no_documento=" & TxtNroDocumento
            cn.Execute Sql
            Sql = "UPDATE inv_kardex " & _
                  "SET kar_numero=" & Lp_NuevoNumero & ",kar_descripcion=REPLACE(kar_descripcion,'" & TxtNroDocumento & "','" & Lp_NuevoNumero & "') " & _
                  "WHERE  rut_emp='" & SP_Rut_Activo & "' AND  doc_id=" & CboDocVenta.ItemData(CboDocVenta.ListIndex) & " AND kar_numero=" & TxtNroDocumento
            cn.Execute Sql
            TxtNroDocumento = Lp_NuevoNumero
        
        End If
    
    
    End If 'hasta aqui si solo es reimpresion
    
    FrmLoad.Visible = False
    'Aun no hay impresion de documento de venta sin inventario 22 Octubre 2011
    If SP_Control_Inventario = "NO" Then
        limpieza
        Exit Sub
    End If
    
    
ContinuandO:
    If Mid(CboPlazos.Text, 1, 14) = "DEBITO/CREDITO" Then
        '2 DE FEBRERO DE 2014
        ' SEGUN RESOLUCION DEL SII
        ' NO IMPRIME NADA.
        'aplicado para alcalde especificamente
        GoTo fin
    End If
    
    If CboDocVenta.Text = "BOLETA" Then
        Sql = "SELECT codigo,descripcion,unidades,(precio_final*1." & DG_IVA & ") AS precio_finalx,(subtotal*1." & DG_IVA & ") as subtotalx " & _
              "FROM ven_detalle " & _
              "WHERE rut_emp='" & SP_Rut_Activo & "' AND  doc_id=" & CboDocVenta.ItemData(CboDocVenta.ListIndex) & " AND no_documento=" & TxtNroDocumento
    Else

        Sql = "SELECT codigo,descripcion,unidades,precio_final,subtotal " & _
                  "FROM ven_detalle " & _
                  "WHERE  rut_emp='" & SP_Rut_Activo & "' AND   doc_id=" & CboDocVenta.ItemData(CboDocVenta.ListIndex) & " AND no_documento=" & TxtNroDocumento
      
    End If
    Consulta RsTmp, Sql
    
    If CboDocVenta.Text = "BOLETA" Then
      
        'If Principal.TxtImpresionDirecta = "SI" Then
            'If Principal.LvImpresorasDefecto.ListItems.Count = 0 Then
            '    MsgBox "Agregar en la BD este equipo con sus impresoras instaladas..."
            'Else
            'EstableImpresora Principal.LvImpresorasDefecto.ListItems(1).SubItems(3)
           '         If UCase(Printer.DeviceName) = UCase(Principal.LvImpresorasDefecto.ListItems(1).SubItems(3)) Then
                        'SIGUIE CON LA MISMA
           '         Else
           '             La_Establecer_Impresora Principal.LvImpresorasDefecto.ListItems(1).SubItems(3)
            '        End If
           ' End If
        'Else
       '     Dialogo.CancelError = True
       '     On Error GoTo CancelaImpesionNV
      '      Dialogo.ShowPrinter
        
       ' End If
        If SP_Rut_Activo = "76.369.600-6" Then
                        'fertiquimica
            ImprimeBoletaFertiquimica
        ElseIf SP_Rut_Activo = "76.169.962-8" Then
            'alcalde
            ImprimeBoletaAlcalde
        ElseIf SP_Rut_Activo = "11.500.319-4" Then
            ImprimeBoletaAlcalde
        ElseIf SP_Rut_Activo = "76.178.895-7" Then
            ImprimeBoletaTotalGomas
        End If
        
    Else
    
            If Sm_NotaDeVenta = "NO" Then
                'If MsgBox("Desea imprimir " & CboDocVenta.Text & " " & TxtNroDocumento, vbYesNo + vbQuestion) = vbYes Then
                
                    'MsgBox "Encienda su impresora para emitir documento", vbOKOnly + vbInformation
                    '.Show
                    'Bm_Factura_por_Guias = False
                    
                    'consultar en la bd si es facturador electronico
                    
                  '  FacturaElectronica
                  '  Exit Sub
                    sql2 = "SELECT   doc_dte,doc_nota_de_credito,doc_cod_sii,(SELECT emp_factura_electronica " & _
                                                                                    "FROM sis_empresas " & _
                                                                                    "WHERE rut='" & SP_Rut_Activo & "') dte " & _
                                "FROM sis_documentos " & _
                                "WHERE doc_id=" & CboDocVenta.ItemData(CboDocVenta.ListIndex)
                    Consulta RsTmp2, sql2
                    If RsTmp2.RecordCount > 0 Then
                        If RsTmp2!Dte = "SI" And RsTmp2!doc_dte = "SI" Then
                      '      FacturaElectronica RsTmp2!doc_cod_sii
                           ' If MsgBox("Se emitir� un DTE y enviar� al SII !" & vbNewLine & "�Continuar?..", vbQuestion + vbOKCancel) = vbCancel Then GoTo Fin
                            
                            
                        Else
                        
                        
                            If Principal.TxtImpresionDirecta = "NO" Then
                        
                                Dialogo.CancelError = True
                                On Error GoTo CancelaImpesionNV
                                
                                Dialogo.ShowPrinter
                            Else
                                'Impresion directa sin seleccionar la impresora
                          '      La_Establecer_Impresora Principal.LvImpresorasDefecto.ListItems(1).SubItems(2)
                                
                            End If
                        End If
                    End If
                    
                    Dim Bp_Empresa_Fac_Electronica As Boolean
                    Bp_Empresa_Fac_Electronica = False
                 '   For s = 1 To Principal.LvFacturaElectronica.ListItems.Count
                 '       If SP_Rut_Activo = Principal.LvFacturaElectronica.ListItems(s) Then
                 '           If Principal.LvFacturaElectronica.ListItems(s).SubItems(1) = "SI" Then
                                Bp_Empresa_Fac_Electronica = True
                 '               Exit For
                  '          End If
                  '      End If
                  '  Next
                    If Bp_Empresa_Fac_Electronica Then
                    'If SP_Rut_Activo = "76.417.727-4" Or SP_Rut_Activo = "96.803.210-0" Or SP_Rut_Activo = "76.361.539-1" Or SP_Rut_Activo = "11.500.319-4" Or SP_Rut_Activo = "12.307.912-4" Then
                        'ALVAMAR
                        'consultamos si factura electronicamente
                        sql2 = "SELECT   doc_dte,doc_nota_de_credito,doc_cod_sii,(SELECT emp_factura_electronica " & _
                                                                                    "FROM sis_empresas " & _
                                                                                    "WHERE rut='" & SP_Rut_Activo & "') dte " & _
                                "FROM sis_documentos " & _
                                "WHERE doc_id=" & CboDocVenta.ItemData(CboDocVenta.ListIndex)
                        Consulta RsTmp2, sql2
                        If RsTmp2.RecordCount > 0 Then
                            If RsTmp2!Dte = "SI" And RsTmp2!doc_dte = "SI" Then
                                FraProcesandoDTE.Visible = True
                                DoEvents
                                FacturaElectronica RsTmp2!doc_cod_sii
                                
                                'cn.Execute "UPDATE dte_folios SET dte_disponible='NO' " & _
                                            "WHERE rut_emp='" & SP_Rut_Activo & "' AND doc_id=" & RsTmp2!doc_cod_sii & " AND dte_folio=" & TxtNroDocumento
                            Else
                                If SP_Rut_Activo = "76.417.727-4" Or SP_Rut_Activo = "12.307.912-4" Then
                                    'alvamar factura normal
                                    ImprimeFactura
                                ElseIf SP_Rut_Activo = "96.803.210-0" Then
                                    'vegamodelo factura normal preimpresa manual
                                     ImprimeFacturaVegaModelo
                                ElseIf SP_Rut_Activo = "xxx" Then
                                    'keramica factura normal
                                ElseIf SP_Rut_Activo = "anccay" Then
                                    'factura nomral kerami
                                ElseIf SP_Rut_Activo = "11.500.319-4" Then
                                    ImprimeFactura
                                ElseIf SP_Rut_Activo = "76.178.895-7" Then
                                
                                    'totalgomas
                                    ImprimeFacturaTotalGomas
                                ElseIf SP_Rut_Activo = "77.680.920-9" Then
                                    'TyT
                                    ImprimeFacturaTyT
                                
                                ElseIf SP_Rut_Activo = "11.907.734-6" Then
                                    'Sandra Ivonne Ramirez
                                    ImprimeFacturaSandraRamirez
                                ElseIf SP_Rut_Activo = "77.349.180-1" Then
                                    ImprimeFacturaProbotem
                                    
                                
                                End If
                                
                            End If
                        End If
                    
                    ElseIf SP_Rut_Activo = "76.095.156-0" Then
                        ImprimeFactura76095156
                    ElseIf SP_Rut_Activo = "76.155.097-7" Or SP_Rut_Activo = "76.532.135-2" Then
                        ImprimeFacturaAnncay
                    ElseIf SP_Rut_Activo = "11.500.319-4" Then
                    'Perla del sur
                        ImprimeFacturaPerlaDelSur
                    ElseIf SP_Rut_Activo = "76.244.196-9" Then
                        'MAQMIN
                        ImprimeFacturaMaqmin
                    ElseIf SP_Rut_Activo = "76.369.600-6" Then
                        'fertiquimica
                        If CboDocVenta.Text = "FACTURA MANUAL" Then
                            ImprimeFacturaFertiquimica
                        ElseIf CboDocVenta.Text = "GUIA" Then
                            ImprimeGuiaFertiquimica
                        End If
                    ElseIf SP_Rut_Activo = "76.169.962-8" Then
                        'alcalde
                        If CboDocVenta.Text = "FACTURA MANUAL" Then
                            ImprimeFacturaAlcalde
                        End If
                                
                    ElseIf SP_Rut_Activo = "7.539.285-0" Then
                        'Alcalde
                        ImprimeFacturaJoseMoreno
                    ElseIf SP_Rut_Activo = "96.803.210-0" Then
                        'Vega modelo temuco
                        'ImprimeFacturaVegaModelo
                    ElseIf SP_Rut_Activo = "11.500.319-4" Then
                        ImprimeFactura
                    
                    ElseIf SP_Rut_Activo = "76.178.895-7" Then
                        'totalgomas
                        ImprimeFacturaTotalGomas
                    ElseIf SP_Rut_Activo = "77.680.920-9" Then
                        'TyT
                        ImprimeFacturaTyT
                    ElseIf SP_Rut_Activo = "11.907.734-6" Then
                        'Sandra Ivonne Ramirez
                        ImprimeFacturaSandraRamirez
                        
                    ElseIf SP_Rut_Activo = "77.349.180-1" Then
                        ImprimeFacturaProbotem
                    Else
                        ImprimeFactura
                    End If

                   '.PrintReport
                'End If
            Else 'Es nota de VENTA
                
                Dialogo.CancelError = True
                On Error GoTo CancelaImpesionNV
                Dialogo.ShowPrinter
                
               If SP_Rut_Activo = "76.169.962-8" Then
                    'Imprime cotizacion Alcalde
                    ImprimeCOTIZACION
                Else
                    ImprimeNV
                End If
            
            End If
            '27 Marzo 2017
            'Para TDCondor, imprimir nota venta
            If SP_Rut_Activo = "76.039.757-1" And CboDocVenta.Text = "nota venta" Then
                ImprimeNV
            End If
    End If
fin:
    
    Im_Tcal = IM_TCALORIGINAL
    FrmLoad.Visible = False
    Unload Me
    Exit Sub
CancelaImpesionNV:
    'no imprime nota de venta
    FrmLoad.Visible = False
    Unload Me
    Exit Sub
errorGrabar:
    FrmLoad.Visible = False
    MsgBox "Ocurrio un error al grabar Doc. de Venta..." & vbNewLine & Err.Number & vbNewLine & Err.Description, vbInformation
    MsgBox Err.Source
    cn.RollbackTrans
End Sub
Private Sub ImprimeFacturaProbotem()
    Dim Cx As Double, Cy As Double, Sp_Fecha As String, Ip_Mes As Integer, Dp_S As Double, Sp_Letras As String
    Dim Sp_Neto As String * 12, Sp_IVA As String * 12, Sp_Total As String * 12
    Dim Sp_GuiasFacturadas As String
   
    Printer.FontName = "Courier New"
    Printer.FontSize = 10
    Printer.ScaleMode = 7
    Cx = 2
    Cy = 5.9
    Dp_S = 0.33
    Printer.CurrentX = Cx
    Printer.CurrentY = Cy
    Sp_Fecha = DtFecha.Value
    Ip_Mes = Val(Mid(Sp_Fecha, 4, 2))
    Printer.Print Mid(Sp_Fecha, 1, 2) & Space(10) & UCase(MonthName(Ip_Mes)) & Space(15) & Mid(Sp_Fecha, 9, 2)
    Printer.CurrentX = Cx
    Printer.CurrentY = Printer.CurrentY + Dp_S + 0.2
    
     pos = Printer.CurrentY
    Printer.Print TxtRazonSocial
    
    Printer.CurrentY = pos
    Printer.CurrentX = Printer.CurrentX + 15
    Printer.Print Me.TxtRut
    
    Printer.CurrentX = Cx
    Printer.CurrentY = Printer.CurrentY + Dp_S
    pos = Printer.CurrentY
    Printer.Print TxtDireccion
    
    Printer.CurrentY = pos
    Printer.CurrentX = Printer.CurrentX + 15
    Printer.Print TxtCiudad
    
    Printer.CurrentX = Cx
    Printer.CurrentY = Printer.CurrentY + Dp_S
    pos = Printer.CurrentY
    Printer.Print TxtGiro
    
    
'TELEFONO
If CboDocVenta.Text = "FACTURA" Then
    Printer.CurrentY = pos
    Printer.CurrentX = Printer.CurrentX + 15
    Printer.Print Me.LbTelefono
Else
    'GUIA
    Printer.CurrentY = pos
    Printer.CurrentX = Cx + 13.5
    Printer.Print TxtComuna
End If

'COMUNA
If CboDocVenta.Text = "FACTURA" Then
    Printer.CurrentY = Printer.CurrentY + Dp_S
    pos = Printer.CurrentY
    Printer.CurrentX = Cx + 8.5
    Printer.Print TxtComuna
Else
    Printer.CurrentY = Printer.CurrentY + Dp_S
    pos = Printer.CurrentY
    Printer.CurrentX = Cx + 13.5
    Printer.Print LbTelefono
End If

'PLAZOS
If CboDocVenta.Text = "FACTURA" Then
    Printer.CurrentY = pos
    Printer.CurrentX = Cx + 15
    Printer.Print Me.CboPlazos.Text
    'pos = Printer.CurrentY
End If
    'Acaba el detalle de los productos
    
    Printer.CurrentY = Printer.CurrentY + 2
    
    
    
    If Bm_Factura_por_Guias Then
        '**************************************************
        'Factura por guias 6 Octubre 2011
        '**************************************************
        'LvMateriales.ListItems.Add , , ""
        'LvMateriales.ListItems(LvMateriales.ListItems.Count).SubItems(3) = "SEGUN GUIAS NRO"
        Sql = "SELECT id, no_documento " & _
              "FROM ven_doc_venta " & _
              "WHERE doc_id_factura=" & CboDocVenta.ItemData(CboDocVenta.ListIndex) & " AND nro_factura=" & Me.TxtNroDocumento
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
            Sp_GuiasFacturadas = Empty
            RsTmp.MoveFirst
            cont = 1
            Do While Not RsTmp.EOF
                Sp_GuiasFacturadas = Sp_GuiasFacturadas & RsTmp!no_documento & ","
                Lm_Ids = Lm_Ids & RsTmp!ID & ","
               ' cont = cont + 1
                RsTmp.MoveNext
                'If cont = 3 Then
                 '   LvMateriales.ListItems.Add , , ""
                  '  LvMateriales.ListItems(LvMateriales.ListItems.Count).SubItems(3) = Mid(Sp_GuiasFacturadas, 1, Len(Sp_GuiasFacturadas) - 1)
                  '  Sp_GuiasFacturadas = Empty
            '    End If
            Loop
            Sp_GuiasFacturadas = Mid(Sp_GuiasFacturadas, 1, Len(Sp_GuiasFacturadas) - 1)
            Printer.CurrentY = pos
            Printer.CurrentX = Cx + 4
            Printer.Print Sp_GuiasFacturadas
            pos = Printer.CurrentY
            'If Sp_GuiasFacturadas <> Empty Then
            '        LvMateriales.ListItems.Add , , ""
            '        LvMateriales.ListItems(LvMateriales.ListItems.Count).SubItems(3) = Mid(Sp_GuiasFacturadas, 1, Len(Sp_GuiasFacturadas) - 1)
            'End If
            Sql = "SELECT neto,iva,bruto " & _
                  "FROM ven_doc_venta " & _
                  "WHERE doc_id=" & CboDocVenta.ItemData(CboDocVenta.ListIndex) & " AND no_documento=" & TxtNroDocumento
            Consulta RsTmp, Sql
            If RsTmp.RecordCount > 0 Then
                Me.SkBrutoMateriales = NumFormat(RsTmp!bruto)
                SkTotalMateriales = NumFormat(RsTmp!Neto)
                SkIvaMateriales = NumFormat(RsTmp!Iva)
            End If
        End If
        ' Fin
    End If
    'Aqui cargaremos los productos
    If Bm_Factura_por_Guias Then DetalleParaFacturaGuias Lm_Ids
    
    For i = 1 To LvMateriales.ListItems.Count
        '6 - 3 - 7 - 8
        Printer.CurrentX = Printer.CurrentX + 0.3
        Printer.CurrentX = Cx - 2
        Printer.Print Right(Space(8) & LvMateriales.ListItems(i).SubItems(6), 8) & Space(5) & _
                Left(LvMateriales.ListItems(i).SubItems(3) & Space(46), 46) & Space(4) & _
                Right(Space(11) & LvMateriales.ListItems(i).SubItems(7), 11) & Space(3) & _
                Right(Space(11) & LvMateriales.ListItems(i).SubItems(8), 11)
    Next
    
    
        
    'If Bm_Factura_por_Guias Then LvMateriales.ListItems.Clear
        
    Printer.CurrentX = Cx + 1
    Printer.CurrentY = Cy + 13
    pos = Printer.CurrentY
    If Len(TxtInformacion) > 0 Then
        Printer.Print TxtInformacion
    End If
    
    
    
    Printer.CurrentX = Cx - 1
    Printer.CurrentY = Cy + 16
    pos = Printer.CurrentY
    
    Sp_Letras = UCase(BrutoAletras(CDbl(SkBrutoMateriales), True))
    
    mitad = Len(Sp_Letras) \ 2
    buscaespacio = InStr(mitad, Sp_Letras, " ")
    If CboDocVenta.Text = "FACTURA" Then
        If buscaespacio > 0 Then
                Printer.Print Mid(Sp_Letras, 1, buscaespacio)
                Printer.CurrentX = Cx - 1
                Printer.CurrentY = Cy + 16.6
                Printer.Print Mid(Sp_Letras, buscaespacio)
        Else
                  Printer.Print Sp_Letras
        End If
    End If
    
    
    
    RSet Sp_Neto = SkTotalMateriales
    RSet Sp_IVA = SkIvaMateriales
    RSet Sp_Total = SkBrutoMateriales
    pos = pos + 0.3
    If CboDocVenta.Text = "FACTURA" Then
        Printer.CurrentY = pos
        Printer.CurrentX = Cx + 14
        Printer.Print Sp_Neto
    Else
        Printer.CurrentY = pos - 1
        Printer.CurrentX = Cx + 14
        Printer.Print Sp_Neto
    
    End If
    If CboDocVenta.Text = "FACTURA" Then
        Printer.CurrentX = Cx + 14
        Printer.CurrentY = pos + 1
        Printer.Print Sp_IVA
        
        Printer.CurrentX = Cx + 14
        Printer.CurrentY = pos + 2
        Printer.Print Sp_Total
    End If
    Printer.NewPage
    Printer.EndDoc

End Sub



Private Sub EstableImpresora(NombreImpresora As String)
   'Seteamos al impresora.
    Dim impresora As Printer
    ' Establece la impresora que se utilizar� para imprimir
    'Recorremos el objeto Printers
    For Each impresora In Printers
        'Si es igual al contenido se�alado en el combobox
        If UCase(impresora.DeviceName) = UCase(NombreImpresora) Then
            'Lo seteamos as�.
            Set Printer = impresora
            Exit For
        End If
    Next

End Sub

Private Sub ImprimeFacturaMaqmin()
    Dim Cx As Double, Cy As Double, Sp_Fecha As String, Ip_Mes As Integer, Dp_S As Double, Sp_Letras As String
    Dim Sp_Neto As String * 12, Sp_IVA As String * 12, Sp_Total As String * 12
    Dim Sp_GuiasFacturadas As String
    On Error GoTo ProblemaImpresora
    Printer.FontName = "Courier New"
    Printer.FontSize = 10
    Printer.ScaleMode = 7
    
    'Cx = 2 'horizontal
    'Cy = 5.9 'vertical
    'Dp_S = 0.35
    
    Cx = 2 'horizontal
    Cy = 6 'vertical
    Dp_S = 0.2
    
    Printer.CurrentX = Cx
    Printer.CurrentY = Cy
    Sp_Fecha = DtFecha.Value
    Ip_Mes = Val(Mid(Sp_Fecha, 4, 2))
    Printer.Print Space(43) & Mid(Sp_Fecha, 1, 2) & Space(7) & UCase(MonthName(Ip_Mes)) & Space(21) & Mid(Sp_Fecha, 9, 2)
    
    Printer.CurrentX = Cx
    'Printer.CurrentY = Printer.CurrentY + Dp_S + 0.2
    Printer.CurrentY = Printer.CurrentY + Dp_S + 0.1
     pos = Printer.CurrentY
    Printer.Print TxtRazonSocial
    
    Printer.CurrentY = pos
    Printer.CurrentX = Printer.CurrentX + 15
    Printer.Print Me.TxtRut
    
    Printer.CurrentX = Cx
    Printer.CurrentY = Printer.CurrentY + Dp_S
    pos = Printer.CurrentY
    Printer.Print TxtDireccion
    
    Printer.CurrentY = pos
    Printer.CurrentX = Printer.CurrentX + 15
   ' Printer.Print TxtCiudad
    Printer.Print TxtComuna
    
    Printer.CurrentX = Cx
    Printer.CurrentY = Printer.CurrentY + Dp_S
    pos = Printer.CurrentY
    Printer.Print CboGiros.Text ' MARIO ****   TxtGiro
    'Printer.Print TxtGiro
    
    Printer.CurrentY = pos
    Printer.CurrentX = Printer.CurrentX + 15
    Printer.Print Me.LbTelefono
    
    
    Printer.CurrentY = Printer.CurrentY + Dp_S
    pos = Printer.CurrentY
    
    'Printer.CurrentX = Cx + 8
    Printer.CurrentX = Cx + 8.5
    'Printer.Print TxtComuna
    
    'Orden de compra
    Printer.CurrentY = pos
    Printer.CurrentX = Cx + 1.5
    Printer.Print TxtOrdenesCompra.Text
    'pos = Printer.CurrentY
    
    Printer.CurrentY = Printer.CurrentY + 0.2
    
    'Printer.CurrentY = pos
    Printer.CurrentX = Cx + 6.5
    Printer.Print Mid(CboPlazos.Text, 1, 25)
    pos = Printer.CurrentY
    
    'Acaba el detalle de los productos
    
    'Printer.CurrentY = Printer.CurrentY + 2
    Printer.CurrentY = Printer.CurrentY + 1
    
    
    
    If Bm_Factura_por_Guias Then
        '**************************************************
        'Factura por guias 6 Octubre 2011
        '**************************************************-
        Sql = "SELECT no_documento,id " & _
              "FROM ven_doc_venta " & _
              "WHERE  rut_emp='" & SP_Rut_Activo & "' AND doc_id_factura=" & CboDocVenta.ItemData(CboDocVenta.ListIndex) & " AND nro_factura=" & Me.TxtNroDocumento
        Consulta RsTmp, Sql
        
        If SG_Presentacion_Factura = "SEGUN" Then
                LvMateriales.ListItems.Add , , ""
                LvMateriales.ListItems(LvMateriales.ListItems.Count).SubItems(3) = "SEGUN GUIAS NRO"

                If RsTmp.RecordCount > 0 Then
                    Sp_GuiasFacturadas = Empty
                    RsTmp.MoveFirst
                    cont = 1
                    Do While Not RsTmp.EOF
                        Sp_GuiasFacturadas = Sp_GuiasFacturadas & RsTmp!no_documento & ","
                        cont = cont + 1
                        RsTmp.MoveNext
                        If cont = 3 Then
                            LvMateriales.ListItems.Add , , ""
                            LvMateriales.ListItems(LvMateriales.ListItems.Count).SubItems(3) = Mid(Sp_GuiasFacturadas, 1, Len(Sp_GuiasFacturadas) - 1)
                            Sp_GuiasFacturadas = Empty
                        End If
                    Loop
                    If Sp_GuiasFacturadas <> Empty Then
                            LvMateriales.ListItems.Add , , ""
                            LvMateriales.ListItems(LvMateriales.ListItems.Count).SubItems(3) = Mid(Sp_GuiasFacturadas, 1, Len(Sp_GuiasFacturadas) - 1)
                    End If
        
        
        
                    Sql = "SELECT neto,iva,bruto " & _
                          "FROM ven_doc_venta " & _
                          "WHERE  rut_emp='" & SP_Rut_Activo & "' AND doc_id=" & CboDocVenta.ItemData(CboDocVenta.ListIndex) & " AND no_documento=" & TxtNroDocumento
                    Consulta RsTmp, Sql
                    If RsTmp.RecordCount > 0 Then
                        Me.SkBrutoMateriales = NumFormat(RsTmp!bruto)
                        SkTotalMateriales = NumFormat(RsTmp!Neto)
                        SkIvaMateriales = NumFormat(RsTmp!Iva)
                    End If
                End If
        Else
            'Factura con detalle de productos
                
                If RsTmp.RecordCount > 0 Then
                    RsTmp.MoveFirst
                    Lm_Ids = ""
                    Do While Not RsTmp.EOF
                        Lm_Ids = Lm_Ids & RsTmp!ID & ","
                        RsTmp.MoveNext
                    Loop
                End If
                Lm_Ids = Mid(Lm_Ids, 1, Len(Lm_Ids) - 1)
                DetalleParaFacturaGuias Lm_Ids
        End If
        ' Fin
    End If
    For i = 1 To LvMateriales.ListItems.Count
        '6 - 3 - 7 - 8
        Printer.CurrentX = Printer.CurrentX + 0.2 'INTERLINIA ARTICULOS
        Printer.CurrentX = Cx - 2
        Printer.Print Right(Space(8) & LvMateriales.ListItems(i).SubItems(6), 8) & Space(5) & _
                "COD(" & LvMateriales.ListItems(i).SubItems(1) & ") " & Left(LvMateriales.ListItems(i).SubItems(3) & Space(40), 40) & Space(4) & _
                Right(Space(11) & LvMateriales.ListItems(i).SubItems(7), 11) & Space(3) & _
                Right(Space(11) & LvMateriales.ListItems(i).SubItems(8), 11)
    Next
    
    
        
    If Bm_Factura_por_Guias Then LvMateriales.ListItems.Clear
        
    
    
    
    Printer.CurrentX = Cx - 1
    Printer.CurrentY = Cy + 16
    pos = Printer.CurrentY
        
    Printer.CurrentX = Cx + 1
    Printer.CurrentY = pos - 2.2
    Printer.Print TxtComentario
    
  '  pos = Printer.CurrentY
  
    Printer.CurrentX = Cx + 1
        
    'Printer.CurrentY = pos
    Printer.CurrentY = pos - 1.4
    
    Sp_Letras = UCase(BrutoAletras(CDbl(SkBrutoMateriales), True))
    
    mitad = Len(Sp_Letras) \ 2
    buscaespacio = InStr(mitad, Sp_Letras, " ")
    If buscaespacio > 0 Then
            Printer.Print Mid(Sp_Letras, 1, buscaespacio)
            Printer.CurrentX = Cx - 1
            'Printer.CurrentY = Cy + 16.6
            Printer.CurrentY = Cy + 15.3
            Printer.Print Mid(Sp_Letras, buscaespacio)
    Else
              Printer.Print Sp_Letras
    End If
    
    
    
    
    RSet Sp_Neto = SkTotalMateriales
    RSet Sp_IVA = SkIvaMateriales
    RSet Sp_Total = SkBrutoMateriales
    'pos = pos + 0.3
    pos = pos - 1.4
    Printer.CurrentY = pos
    Printer.CurrentX = Cx + 14.5
    Printer.Print Sp_Neto
    
    Printer.CurrentX = Cx + 14.5
    Printer.CurrentY = pos + 0.8
    Printer.Print Sp_IVA
    
    Printer.CurrentX = Cx + 14.5
    Printer.CurrentY = pos + 1.7
    Printer.Print Sp_Total
    
    Printer.NewPage
    Printer.EndDoc
    Exit Sub
ProblemaImpresora:
    MsgBox "Ocurrio un error al intentar imprimir..." & vbNewLine & Err.Description & vbNewLine & Err.Number

End Sub

Private Sub ImprimeFacturaFertiquimica()
    Dim Cx As Double, Cy As Double, Sp_Fecha As String, Ip_Mes As Integer, Dp_S As Double, Sp_Letras As String
    Dim Sp_Neto As String * 12, Sp_IVA As String * 12, Sp_Total As String * 12
    Dim Sp_GuiasFacturadas As String
    
    Dim p_Codigo As String * 5
    Dim p_Cantidad As String * 7
    Dim p_UM As String * 4
    Dim p_Detalle As String * 40
    Dim p_Unitario As String * 10
    Dim p_Total As String * 11
    
    
    On Error GoTo ProblemaImpresora
    Printer.FontName = "Courier New"
    Printer.FontSize = 10
    Printer.ScaleMode = 7
        
    'Cx = 2 'horizontal
    'Cy = 5.9 'vertical
    'Dp_S = 0.35
    
    Sql = "SELECT sue_cx,sue_cy " & _
            "FROM sis_empresas_sucursales " & _
            "WHERE sue_id=" & IG_id_Sucursal_Empresa
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        Cx = Val(RsTmp!sue_cx)
        Cy = Val(RsTmp!sue_cy) 'vertical)
    Else
        Cx = 1.1 'horizontal
        Cy = 3 'vertical
    
    End If
    
    
    Dp_S = 0.2
    
    Printer.CurrentX = Cx
    Printer.CurrentY = Cy
    Sp_Fecha = DtFecha.Value
    Ip_Mes = Val(Mid(Sp_Fecha, 4, 2))
    
    Printer.CurrentY = Printer.CurrentY + Dp_S + 1.1
    Printer.CurrentX = Cx + 3.4
    Printer.Print Mid(Sp_Fecha, 1, 2) & Space(4) & Mid(Sp_Fecha, 4, 2) & Space(2) & Mid(Sp_Fecha, 7, 4)

    Printer.CurrentX = Cx
    Printer.CurrentY = Printer.CurrentY + Dp_S + 0.4
    pos = Printer.CurrentY
    Printer.Print TxtRazonSocial
   
    Printer.CurrentX = Cx
    Printer.CurrentY = Printer.CurrentY + Dp_S + 0.2
    pos = Printer.CurrentY
    posd = Printer.CurrentY
    Printer.Print TxtDireccion
    
    Printer.CurrentY = pos
    Printer.CurrentX = Printer.CurrentX + 13.5
    Printer.Print Mid(TxtCiudad & " / " & TxtComuna, 1, 35)
    
    Printer.CurrentX = Cx - 0.2
    Printer.CurrentY = Printer.CurrentY + Dp_S + 0.2
    pos = Printer.CurrentY
    Printer.Print Me.TxtRut
    
    Printer.CurrentY = pos
    Printer.CurrentX = Printer.CurrentX + 6
    Printer.Print CboGiros.Text ' MARIO ****   TxtGiro
   
    Printer.CurrentY = pos
    Printer.CurrentX = Printer.CurrentX + 13
    Printer.Print Me.LbTelefono
    
    '***
    Printer.CurrentX = Cx
    Printer.CurrentY = Printer.CurrentY + Dp_S - 0.2
    Printer.CurrentX = Printer.CurrentX + 10.9
    Printer.Print CboVendedores
    
    
    Printer.CurrentY = Printer.CurrentY + Dp_S - 0.3
    '***
    'Printer.CurrentY = Printer.CurrentY + Dp_S
    pos = Printer.CurrentY
    
    Printer.CurrentX = Cx + 8.5
    Printer.CurrentY = Printer.CurrentY + 0.2
       
    Printer.CurrentY = Printer.CurrentY + 1
    
    If Bm_Factura_por_Guias Then
        '**************************************************
        'Factura por guias Diciembre del 2013
        '**************************************************-
        Sql = "SELECT no_documento,id " & _
              "FROM ven_doc_venta " & _
              "WHERE  rut_emp='" & SP_Rut_Activo & "' AND doc_id_factura=" & CboDocVenta.ItemData(CboDocVenta.ListIndex) & " AND nro_factura=" & Me.TxtNroDocumento
        Consulta RsTmp, Sql
        
        If SG_Presentacion_Factura = "SEGUN" Then
                LvMateriales.ListItems.Add , , ""
                LvMateriales.ListItems(LvMateriales.ListItems.Count).SubItems(3) = "SEGUN GUIAS NRO"

                If RsTmp.RecordCount > 0 Then
                    Sp_GuiasFacturadas = Empty
                    RsTmp.MoveFirst
                    cont = 1
                    Do While Not RsTmp.EOF
                        Sp_GuiasFacturadas = Sp_GuiasFacturadas & RsTmp!no_documento & ","
                        cont = cont + 1
                        RsTmp.MoveNext
                        If cont = 3 Then
                            LvMateriales.ListItems.Add , , ""
                            LvMateriales.ListItems(LvMateriales.ListItems.Count).SubItems(3) = Mid(Sp_GuiasFacturadas, 1, Len(Sp_GuiasFacturadas) - 1)
                            Sp_GuiasFacturadas = Empty
                        End If
                    Loop
                    If Sp_GuiasFacturadas <> Empty Then
                            LvMateriales.ListItems.Add , , ""
                            LvMateriales.ListItems(LvMateriales.ListItems.Count).SubItems(3) = Mid(Sp_GuiasFacturadas, 1, Len(Sp_GuiasFacturadas) - 1)
                    End If
        
        
        
                    Sql = "SELECT neto,iva,bruto " & _
                          "FROM ven_doc_venta " & _
                          "WHERE  rut_emp='" & SP_Rut_Activo & "' AND doc_id=" & CboDocVenta.ItemData(CboDocVenta.ListIndex) & " AND no_documento=" & TxtNroDocumento
                    Consulta RsTmp, Sql
                    If RsTmp.RecordCount > 0 Then
                        Me.SkBrutoMateriales = NumFormat(RsTmp!bruto)
                        SkTotalMateriales = NumFormat(RsTmp!Neto)
                        SkIvaMateriales = NumFormat(RsTmp!Iva)
                    End If
                End If
        Else
            'Factura con detalle de productos
                
                If RsTmp.RecordCount > 0 Then
                    RsTmp.MoveFirst
                    Lm_Ids = ""
                    Do While Not RsTmp.EOF
                        Lm_Ids = Lm_Ids & RsTmp!ID & ","
                        RsTmp.MoveNext
                    Loop
                End If
                Lm_Ids = Mid(Lm_Ids, 1, Len(Lm_Ids) - 1)
                DetalleParaFacturaGuias Lm_Ids
        End If
        ' Fin
    End If
    Printer.CurrentY = Printer.CurrentY + 0.4
    
    For i = 1 To LvMateriales.ListItems.Count
        '6 - 3 - 7 - 8
        Printer.CurrentX = Printer.CurrentX + 0.2 'INTERLINIA ARTICULOS
       ' Printer.CurrentX = Cx - 2
        Printer.CurrentX = Cx - 1
        
        p_Codigo = LvMateriales.ListItems(i).SubItems(1)
        RSet p_Cantidad = LvMateriales.ListItems(i).SubItems(6)
        p_UM = LvMateriales.ListItems(i).SubItems(21)
        p_Detalle = LvMateriales.ListItems(i).SubItems(3)
        RSet p_Unitario = LvMateriales.ListItems(i).SubItems(7)
        RSet p_Total = LvMateriales.ListItems(i).SubItems(8)
        
        Printer.Print p_Codigo & " " & p_Cantidad & "    " & p_UM & " " & p_Detalle & " " & p_Unitario & " " & p_Total
       
    Next
        
    If Bm_Factura_por_Guias Then LvMateriales.ListItems.Clear
    
    Printer.CurrentX = Cx - 1
    Printer.CurrentY = Cy + 14.2
    pos = Printer.CurrentY
        
    Printer.CurrentX = Cx + 1
    Printer.CurrentY = pos - 2.2
    Printer.Print TxtComentario
    
  '  pos = Printer.CurrentY
  
    Printer.CurrentX = Cx + 1
        
    'Printer.CurrentY = pos
    Printer.CurrentY = pos - 1.4
    
   ' Sp_Letras = UCase(BrutoAletras(CDbl(SkBrutoMateriales), True))
    
   ' mitad = Len(Sp_Letras) \ 2
   ' buscaespacio = InStr(mitad, Sp_Letras, " ")
   ' If buscaespacio > 0 Then
   '         Printer.Print Mid(Sp_Letras, 1, buscaespacio)
   '         Printer.CurrentX = Cx - 1
   '         Printer.CurrentY = Cy + 15.3
   '         Printer.Print Mid(Sp_Letras, buscaespacio)
   ' Else
   '           Printer.Print Sp_Letras
   ' End If
    
    
    
    
    RSet Sp_Neto = SkTotalMateriales
    RSet Sp_IVA = SkIvaMateriales
    RSet Sp_Total = SkBrutoMateriales
    pos = pos - 1.4
    Printer.CurrentY = pos
    Printer.CurrentX = Cx + 14.5
    Printer.Print Sp_Neto
    
    Printer.CurrentX = Cx + 14.5
    Printer.CurrentY = pos + 0.9
    Printer.Print Sp_IVA
    
    Printer.CurrentX = Cx + 14.5
    Printer.CurrentY = pos + 1.8
    Printer.Print Sp_Total
    
    Printer.NewPage
    Printer.EndDoc
    Exit Sub
ProblemaImpresora:
    MsgBox "Ocurrio un error al intentar imprimir..." & vbNewLine & Err.Description & vbNewLine & Err.Number

End Sub
Private Sub ImprimeFacturaAlcalde()
    Dim Cx As Double, Cy As Double, Sp_Fecha As String, Ip_Mes As Integer, Dp_S As Double, Sp_Letras As String
    Dim Sp_Neto As String * 12, Sp_IVA As String * 12, Sp_Total As String * 12
    Dim Sp_GuiasFacturadas As String
    
    Dim p_Codigo As String * 6
    Dim p_Cantidad As String * 7
    Dim p_UM As String * 4
    Dim p_Detalle As String * 40
    Dim p_Unitario As String * 10
    Dim p_Total As String * 11
    
    
    On Error GoTo ProblemaImpresora
    Printer.FontName = "Courier New"
    Printer.FontSize = 10
    Printer.ScaleMode = 7
        
    'Cx = 2 'horizontal
    'Cy = 5.9 'vertical
    'Dp_S = 0.35
    
    Sql = "SELECT sue_cx,sue_cy " & _
            "FROM sis_empresas_sucursales " & _
            "WHERE sue_id=" & IG_id_Sucursal_Empresa
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        Cx = Val(RsTmp!sue_cx)
        Cy = Val(RsTmp!sue_cy) 'vertical)
    Else
        Cx = 1.1 'horizontal
        Cy = 2.9 'vertical
    
    End If
    
    
    Dp_S = 0.2
    
    Printer.CurrentX = Cx
    Printer.CurrentY = Cy
    Sp_Fecha = DtFecha.Value
    Ip_Mes = Val(Mid(Sp_Fecha, 4, 2))
    
    
    'FECHA
    Printer.CurrentY = Cy + 2.5
    Printer.CurrentX = Cx + 1.2
    pos = Printer.CurrentY
    Printer.Print "       " & Mid(Sp_Fecha, 1, 2) & Space(4) & UCase(MonthName(Val(Mid(Sp_Fecha, 4, 2))))
    Printer.CurrentY = pos
    Printer.CurrentX = Cx + 9
    Printer.Print Mid(Sp_Fecha, 7, 4)

    
    'RUT
    Printer.CurrentX = Cx + 16
    Printer.CurrentY = pos
    pos = Printer.CurrentY
    Printer.Print Me.TxtRut


    'RAZON SOCIAL
    Printer.CurrentX = Cx + 2
    Printer.CurrentY = Cy + 3
    pos = Printer.CurrentY
    Printer.Print TxtRazonSocial
    
    'CIUDAD
    Printer.CurrentY = pos
    Printer.CurrentX = Cx + 16
    Printer.Print TxtCiudad
     
   
    'DIRECCION
    Printer.CurrentX = Cx + 2
    Printer.CurrentY = Cy + 3.4
    pos = Printer.CurrentY
    posd = Printer.CurrentY
    Printer.Print TxtDireccion
    
    'TELEFONO
    Printer.CurrentY = pos
    Printer.CurrentX = Cx + 16
    Printer.Print Me.LbTelefono
    
    'GIRO
    Printer.CurrentY = Cy + 3.8
    Printer.CurrentX = Cx + 2
    pos = Printer.CurrentY
    Printer.Print CboGiros.Text ' MARIO ****   TxtGiro
   
    'COOMUNA
    Printer.CurrentY = pos
    Printer.CurrentX = Cx + 11
    Printer.Print TxtComuna
    
    'CONDICION DE VENTA
    Printer.CurrentY = pos
    Printer.CurrentX = Cx + 16.6
    Printer.Print Mid(CboPlazos.Text, 1, 20)
    
    
    '***
   ' Printer.CurrentX = Cx
   ' Printer.CurrentY = Printer.CurrentY + Dp_S - 0.2
  '  Printer.CurrentX = Printer.CurrentX + 10.9
   ' Printer.Print CboVendedores
    
    
    Printer.CurrentY = Printer.CurrentY + Dp_S - 0.3
    '***
    'Printer.CurrentY = Printer.CurrentY + Dp_S
    pos = Printer.CurrentY
    
    Printer.CurrentX = Cx + 8.5
    Printer.CurrentY = Printer.CurrentY + 1
       
    
    
    If Bm_Factura_por_Guias Then
        '**************************************************
        'Factura por guias Diciembre del 2013
        '**************************************************-
        Sql = "SELECT no_documento,id " & _
              "FROM ven_doc_venta " & _
              "WHERE  rut_emp='" & SP_Rut_Activo & "' AND doc_id_factura=" & CboDocVenta.ItemData(CboDocVenta.ListIndex) & " AND nro_factura=" & Me.TxtNroDocumento
        Consulta RsTmp, Sql
        
        If SG_Presentacion_Factura = "SEGUN" Then
                LvMateriales.ListItems.Add , , ""
                LvMateriales.ListItems(LvMateriales.ListItems.Count).SubItems(3) = "SEGUN GUIAS NRO"

                If RsTmp.RecordCount > 0 Then
                    Sp_GuiasFacturadas = Empty
                    RsTmp.MoveFirst
                    cont = 1
                    Do While Not RsTmp.EOF
                        Sp_GuiasFacturadas = Sp_GuiasFacturadas & RsTmp!no_documento & ","
                        cont = cont + 1
                        RsTmp.MoveNext
                        If cont = 3 Then
                            LvMateriales.ListItems.Add , , ""
                            LvMateriales.ListItems(LvMateriales.ListItems.Count).SubItems(3) = Mid(Sp_GuiasFacturadas, 1, Len(Sp_GuiasFacturadas) - 1)
                            Sp_GuiasFacturadas = Empty
                        End If
                    Loop
                    If Sp_GuiasFacturadas <> Empty Then
                            LvMateriales.ListItems.Add , , ""
                            LvMateriales.ListItems(LvMateriales.ListItems.Count).SubItems(3) = Mid(Sp_GuiasFacturadas, 1, Len(Sp_GuiasFacturadas) - 1)
                    End If
        
        
        
                    Sql = "SELECT neto,iva,bruto " & _
                          "FROM ven_doc_venta " & _
                          "WHERE  rut_emp='" & SP_Rut_Activo & "' AND doc_id=" & CboDocVenta.ItemData(CboDocVenta.ListIndex) & " AND no_documento=" & TxtNroDocumento
                    Consulta RsTmp, Sql
                    If RsTmp.RecordCount > 0 Then
                        Me.SkBrutoMateriales = NumFormat(RsTmp!bruto)
                        SkTotalMateriales = NumFormat(RsTmp!Neto)
                        SkIvaMateriales = NumFormat(RsTmp!Iva)
                    End If
                End If
        Else
            'Factura con detalle de productos
                
                If RsTmp.RecordCount > 0 Then
                    RsTmp.MoveFirst
                    Lm_Ids = ""
                    Do While Not RsTmp.EOF
                        Lm_Ids = Lm_Ids & RsTmp!ID & ","
                        RsTmp.MoveNext
                    Loop
                End If
                Lm_Ids = Mid(Lm_Ids, 1, Len(Lm_Ids) - 1)
                DetalleParaFacturaGuias Lm_Ids
        End If
        ' Fin
    End If
    Printer.CurrentY = Printer.CurrentY + 0.4
    
    For i = 1 To LvMateriales.ListItems.Count
        '6 - 3 - 7 - 8
       ' Printer.CurrentX = Printer.CurrentX + 0.07 'INTERLINIA ARTICULOS
       'esperar el interlinedo automtico
       ' Printer.CurrentX = Cx - 2
        Printer.CurrentX = Cx - 0.5
        
        p_Codigo = LvMateriales.ListItems(i).SubItems(1)
        RSet p_Cantidad = LvMateriales.ListItems(i).SubItems(6)
        p_UM = LvMateriales.ListItems(i).SubItems(21)
        p_Detalle = LvMateriales.ListItems(i).SubItems(3)
        RSet p_Unitario = LvMateriales.ListItems(i).SubItems(7)
        RSet p_Total = LvMateriales.ListItems(i).SubItems(8)
        
        Printer.Print p_Cantidad & "    " & p_Codigo & "      " & p_Detalle & "       " & p_Unitario & " " & p_Total
       
    Next
        
    If Bm_Factura_por_Guias Then LvMateriales.ListItems.Clear
    
    Printer.CurrentX = Cx - 1
    Printer.CurrentY = Cy + 14.2
    pos = Printer.CurrentY
        
    Printer.CurrentX = Cx + 1
    Printer.CurrentY = pos - 2.2
    Printer.Print TxtComentario
    
  '  pos = Printer.CurrentY
  
    Printer.CurrentX = Cx + 1
        
    'Printer.CurrentY = pos
    Printer.CurrentY = pos - 1.4
    
   
    
   ' mitad = Len(Sp_Letras) \ 2
   ' buscaespacio = InStr(mitad, Sp_Letras, " ")
   ' If buscaespacio > 0 Then
   '         Printer.Print Mid(Sp_Letras, 1, buscaespacio)
   '         Printer.CurrentX = Cx - 1
   '         Printer.CurrentY = Cy + 15.3
   '         Printer.Print Mid(Sp_Letras, buscaespacio)
   ' Else
   '           Printer.Print Sp_Letras
   ' End If
    
    
    
    
    RSet Sp_Neto = SkTotalMateriales
    RSet Sp_IVA = SkIvaMateriales
    RSet Sp_Total = SkBrutoMateriales
    pos = pos - 1.4
    
    
    Printer.CurrentX = Cx + 9.5
    Printer.CurrentY = Cy + 10.8
    Printer.Print Sp_Neto
    
    Printer.CurrentX = Cx + 13
    Printer.CurrentY = Cy + 10.8
    Printer.Print Sp_IVA
    
    Printer.CurrentX = Cx + 16
    Printer.CurrentY = Cy + 10.8
    Printer.Print Sp_Total
    
    Printer.CurrentX = Cx + 1
    Printer.CurrentY = Cy + 11.4
    Sp_Letras = UCase(BrutoAletras(CDbl(SkBrutoMateriales), True))
    Printer.Print Sp_Letras
    Printer.NewPage
    Printer.EndDoc
    Exit Sub
ProblemaImpresora:
    MsgBox "Ocurrio un error al intentar imprimir..." & vbNewLine & Err.Description & vbNewLine & Err.Number

End Sub
Private Sub ImprimeFacturaJoseMoreno()
    Dim Cx As Double, Cy As Double, Sp_Fecha As String, Ip_Mes As Integer, Dp_S As Double, Sp_Letras As String
    Dim Sp_Neto As String * 12, Sp_IVA As String * 12, Sp_Total As String * 12
    Dim Sp_GuiasFacturadas As String
    
    Dim p_Codigo As String * 5
    Dim p_Cantidad As String * 7
    Dim p_UM As String * 4
    Dim p_Detalle As String * 40
    Dim p_Unitario As String * 10
    Dim p_Total As String * 11
    
    
    On Error GoTo ProblemaImpresora
    Printer.FontName = "Courier New"
    Printer.FontSize = 10
    Printer.ScaleMode = 7
        
    'Cx = 2 'horizontal
    'Cy = 5.9 'vertical
    'Dp_S = 0.35
    
    Sql = "SELECT sue_cx,sue_cy " & _
            "FROM sis_empresas_sucursales " & _
            "WHERE sue_id=" & IG_id_Sucursal_Empresa
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        Cx = Val(RsTmp!sue_cx)
        Cy = Val(RsTmp!sue_cy) 'vertical)
    Else
        Cx = 1 'horizontal
        Cy = 0.6
    
    End If
    
    
    Dp_S = 0.2
    
    Printer.CurrentX = Cx
    Printer.CurrentY = Cy
    Sp_Fecha = DtFecha.Value
    Ip_Mes = Val(Mid(Sp_Fecha, 4, 2))
    
    
    'FECHA
    Printer.CurrentY = Cy + 2.5
    Printer.CurrentX = Cx + 2
    pos = Printer.CurrentY
    Printer.Print Mid(Sp_Fecha, 1, 2); " "; UCase(MonthName(Val(Mid(Sp_Fecha, 4, 2)))); " "; Mid(Sp_Fecha, 7, 4)

   
    'CIUDAD
    Printer.CurrentY = pos
    Printer.CurrentX = Cx + 15
    Printer.Print TxtCiudad


    'RAZON SOCIAL
    Printer.CurrentX = Cx + 2
    Printer.CurrentY = Cy + 2.9
    pos = Printer.CurrentY
    Printer.Print TxtRazonSocial

    
    'RUT
    Printer.CurrentX = Cx + 15
    Printer.CurrentY = pos
    pos = Printer.CurrentY
    Printer.Print Me.TxtRut


        
   
    'DIRECCION
    Printer.CurrentX = Cx + 2
    Printer.CurrentY = Cy + 3.3
    pos = Printer.CurrentY
    posd = Printer.CurrentY
    Printer.Print TxtDireccion
    
    'TELEFONO
    Printer.CurrentY = pos
    Printer.CurrentX = Cx + 15
    Printer.Print Me.LbTelefono
    
    'GIRO
    Printer.CurrentY = Cy + 3.7
    Printer.CurrentX = Cx + 2
    pos = Printer.CurrentY
    Printer.Print CboGiros.Text ' MARIO ****   TxtGiro
   
    'COND. VENTA
    Printer.CurrentY = pos
    Printer.CurrentX = Cx + 15
    Printer.Print Mid(CboPlazos, 1, 20)
    
    
    '***
   ' Printer.CurrentX = Cx
   ' Printer.CurrentY = Printer.CurrentY + Dp_S - 0.2
  '  Printer.CurrentX = Printer.CurrentX + 10.9
   ' Printer.Print CboVendedores
    
    
    Printer.CurrentY = Printer.CurrentY + Dp_S - 0.3
    '***
    'Printer.CurrentY = Printer.CurrentY + Dp_S
    pos = Printer.CurrentY
    
    Printer.CurrentX = Cx + 8.5
    Printer.CurrentY = Printer.CurrentY + 1
       
    
    
    If Bm_Factura_por_Guias Then
        '**************************************************
        'Factura por guias Diciembre del 2013
        '**************************************************-
        Sql = "SELECT no_documento,id " & _
              "FROM ven_doc_venta " & _
              "WHERE  rut_emp='" & SP_Rut_Activo & "' AND doc_id_factura=" & CboDocVenta.ItemData(CboDocVenta.ListIndex) & " AND nro_factura=" & Me.TxtNroDocumento
        Consulta RsTmp, Sql
        
        If SG_Presentacion_Factura = "SEGUN" Then
                LvMateriales.ListItems.Add , , ""
                LvMateriales.ListItems(LvMateriales.ListItems.Count).SubItems(3) = "SEGUN GUIAS NRO"

                If RsTmp.RecordCount > 0 Then
                    Sp_GuiasFacturadas = Empty
                    RsTmp.MoveFirst
                    cont = 1
                    Do While Not RsTmp.EOF
                        Sp_GuiasFacturadas = Sp_GuiasFacturadas & RsTmp!no_documento & ","
                        cont = cont + 1
                        RsTmp.MoveNext
                        If cont = 3 Then
                            LvMateriales.ListItems.Add , , ""
                            LvMateriales.ListItems(LvMateriales.ListItems.Count).SubItems(3) = Mid(Sp_GuiasFacturadas, 1, Len(Sp_GuiasFacturadas) - 1)
                            Sp_GuiasFacturadas = Empty
                        End If
                    Loop
                    If Sp_GuiasFacturadas <> Empty Then
                            LvMateriales.ListItems.Add , , ""
                            LvMateriales.ListItems(LvMateriales.ListItems.Count).SubItems(3) = Mid(Sp_GuiasFacturadas, 1, Len(Sp_GuiasFacturadas) - 1)
                    End If
        
        
        
                    Sql = "SELECT neto,iva,bruto " & _
                          "FROM ven_doc_venta " & _
                          "WHERE  rut_emp='" & SP_Rut_Activo & "' AND doc_id=" & CboDocVenta.ItemData(CboDocVenta.ListIndex) & " AND no_documento=" & TxtNroDocumento
                    Consulta RsTmp, Sql
                    If RsTmp.RecordCount > 0 Then
                        Me.SkBrutoMateriales = NumFormat(RsTmp!bruto)
                        SkTotalMateriales = NumFormat(RsTmp!Neto)
                        SkIvaMateriales = NumFormat(RsTmp!Iva)
                    End If
                End If
        Else
            'Factura con detalle de productos
                
                If RsTmp.RecordCount > 0 Then
                    RsTmp.MoveFirst
                    Lm_Ids = ""
                    Do While Not RsTmp.EOF
                        Lm_Ids = Lm_Ids & RsTmp!ID & ","
                        RsTmp.MoveNext
                    Loop
                End If
                Lm_Ids = Mid(Lm_Ids, 1, Len(Lm_Ids) - 1)
                DetalleParaFacturaGuias Lm_Ids
                
                 Sql = "SELECT neto,iva,bruto " & _
                          "FROM ven_doc_venta " & _
                          "WHERE  rut_emp='" & SP_Rut_Activo & "' AND doc_id=" & CboDocVenta.ItemData(CboDocVenta.ListIndex) & " AND no_documento=" & TxtNroDocumento
                Consulta RsTmp, Sql
                If RsTmp.RecordCount > 0 Then
                    Me.SkBrutoMateriales = NumFormat(RsTmp!bruto)
                    SkTotalMateriales = NumFormat(RsTmp!Neto)
                    SkIvaMateriales = NumFormat(RsTmp!Iva)
                End If
            
                
                
        End If
        ' Fin
    End If
    Printer.CurrentY = Printer.CurrentY + 0.4
    
    For i = 1 To LvMateriales.ListItems.Count
        '6 - 3 - 7 - 8
       ' Printer.CurrentX = Printer.CurrentX + 0.07 'INTERLINIA ARTICULOS
       'esperar el interlinedo automtico
       ' Printer.CurrentX = Cx - 2
        Printer.CurrentX = Cx - 0.5
        
        p_Codigo = LvMateriales.ListItems(i).SubItems(1)
        RSet p_Cantidad = LvMateriales.ListItems(i).SubItems(6)
        p_UM = LvMateriales.ListItems(i).SubItems(21)
        p_Detalle = LvMateriales.ListItems(i).SubItems(3)
        RSet p_Unitario = LvMateriales.ListItems(i).SubItems(7)
        RSet p_Total = LvMateriales.ListItems(i).SubItems(8)
        
        Printer.Print p_Codigo & "    " & p_Cantidad & "      " & p_Detalle & "    " & p_Unitario & "    " & p_Total
       
    Next
        
    If Bm_Factura_por_Guias Then LvMateriales.ListItems.Clear
    
    Printer.CurrentX = Cx - 1
    Printer.CurrentY = Cy + 14.2
    pos = Printer.CurrentY
        
    Printer.CurrentX = Cx + 1
    Printer.CurrentY = pos - 2.2
    Printer.Print TxtComentario
    
  '  pos = Printer.CurrentY
  
    Printer.CurrentX = Cx + 1
        
    'Printer.CurrentY = pos
    Printer.CurrentY = pos - 1.4
    
   
    
   ' mitad = Len(Sp_Letras) \ 2
   ' buscaespacio = InStr(mitad, Sp_Letras, " ")
   ' If buscaespacio > 0 Then
   '         Printer.Print Mid(Sp_Letras, 1, buscaespacio)
   '         Printer.CurrentX = Cx - 1
   '         Printer.CurrentY = Cy + 15.3
   '         Printer.Print Mid(Sp_Letras, buscaespacio)
   ' Else
   '           Printer.Print Sp_Letras
   ' End If
    
    
    
    
    RSet Sp_Neto = SkTotalMateriales
    RSet Sp_IVA = SkIvaMateriales
    RSet Sp_Total = SkBrutoMateriales
    pos = pos - 1.4
    
    
    Printer.CurrentX = Cx + 16.1
    Printer.CurrentY = Cy + 10.3
    Printer.Print Sp_Neto
    
    Printer.CurrentX = Cx + 16.1
    Printer.CurrentY = Cy + 10.8
    Printer.Print Sp_IVA
    
    Printer.CurrentX = Cx + 16.1
    Printer.CurrentY = Cy + 11.3
    Printer.Print Sp_Total
    
    Printer.CurrentX = Cx + 1
    Printer.CurrentY = Cy + 11.1
    Sp_Letras = UCase(BrutoAletras(CDbl(SkBrutoMateriales), True))
    Printer.Print Sp_Letras
    Printer.NewPage
    Printer.EndDoc
    Exit Sub
ProblemaImpresora:
    MsgBox "Ocurrio un error al intentar imprimir..." & vbNewLine & Err.Description & vbNewLine & Err.Number

End Sub

Private Sub ImprimeGuiaFertiquimica()
    Dim Cx As Double, Cy As Double, Sp_Fecha As String, Ip_Mes As Integer, Dp_S As Double, Sp_Letras As String
    Dim Sp_Neto As String * 12, Sp_IVA As String * 12, Sp_Total As String * 12
    Dim Sp_GuiasFacturadas As String
    
    Dim p_Codigo As String * 5
    Dim p_Cantidad As String * 7
    Dim p_UM As String * 4
    Dim p_Detalle As String * 40
    Dim p_Unitario As String * 10
    Dim p_Total As String * 11
    Dim p_Mes As String * 10
    Dim p_CiudadF As String * 20
    
    On Error GoTo ProblemaImpresora
    Printer.FontName = "Courier New"
    Printer.FontSize = 10
    Printer.ScaleMode = 7
        
    Sql = "SELECT sue_cx,sue_cy " & _
            "FROM sis_empresas_sucursales " & _
            "WHERE sue_id=" & IG_id_Sucursal_Empresa
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        Cx = Val(RsTmp!sue_cx) + 0.2
        Cy = Val(RsTmp!sue_cy) + 0.2 'vertical)
    Else
        Cx = 1.1 'horizontal
        Cy = 3 'vertical
    
    End If
    
    Printer.CurrentX = Cx
    Printer.CurrentY = Cy
    Sp_Fecha = DtFecha.Value
    Ip_Mes = Val(Mid(Sp_Fecha, 4, 2))
    
    Printer.CurrentY = Printer.CurrentY + 1.9
    Printer.CurrentX = Cx + 5.2
    p_Mes = UCase(MonthName(Ip_Mes))
    RSet p_CiudadF = TxtCiudad
    Printer.Print p_CiudadF & "  " & Mid(Sp_Fecha, 1, 2) & Space(6) & p_Mes & Space(10) & Mid(Sp_Fecha, 7, 4)

    Printer.CurrentX = Cx - 0.4
  '  Printer.CurrentY = Printer.CurrentY + Dp_S
    pos = Printer.CurrentY
    Printer.Print TxtRazonSocial
    
    Printer.CurrentX = Cx + 12
    Printer.CurrentY = pos
    pos = Printer.CurrentY
    Printer.Print Me.TxtRut
    
   
    Printer.CurrentX = Cx - 0.4
   ' Printer.CurrentY = Printer.CurrentY + Dp_S
    pos = Printer.CurrentY
    posd = Printer.CurrentY
    Printer.Print TxtDireccion
    
    Printer.CurrentY = pos
    Printer.CurrentX = Printer.CurrentX + 10
    Printer.Print Me.LbTelefono
    
    

    Printer.CurrentY = pos
    Printer.CurrentX = Cx + 12
    Printer.Print TxtComuna
    
    
    
    pos = Printer.CurrentY
    Printer.CurrentY = pos
    Printer.CurrentX = Cx - 0.6
    Printer.Print TxtGiro
   
    
    Printer.CurrentY = Printer.CurrentY + Dp_S
    pos = Printer.CurrentY
    
    Printer.CurrentX = Cx + 8.5
    Printer.CurrentY = Printer.CurrentY + 0.2
       
    Printer.CurrentY = Printer.CurrentY + 1
    
    If Bm_Factura_por_Guias Then
        '**************************************************
        'Factura por guias Diciembre del 2013
        '**************************************************-
        Sql = "SELECT no_documento,id " & _
              "FROM ven_doc_venta " & _
              "WHERE  rut_emp='" & SP_Rut_Activo & "' AND doc_id_factura=" & CboDocVenta.ItemData(CboDocVenta.ListIndex) & " AND nro_factura=" & Me.TxtNroDocumento
        Consulta RsTmp, Sql
        
        If SG_Presentacion_Factura = "SEGUN" Then
                LvMateriales.ListItems.Add , , ""
                LvMateriales.ListItems(LvMateriales.ListItems.Count).SubItems(3) = "SEGUN GUIAS NRO"

                If RsTmp.RecordCount > 0 Then
                    Sp_GuiasFacturadas = Empty
                    RsTmp.MoveFirst
                    cont = 1
                    Do While Not RsTmp.EOF
                        Sp_GuiasFacturadas = Sp_GuiasFacturadas & RsTmp!no_documento & ","
                        cont = cont + 1
                        RsTmp.MoveNext
                        If cont = 3 Then
                            LvMateriales.ListItems.Add , , ""
                            LvMateriales.ListItems(LvMateriales.ListItems.Count).SubItems(3) = Mid(Sp_GuiasFacturadas, 1, Len(Sp_GuiasFacturadas) - 1)
                            Sp_GuiasFacturadas = Empty
                        End If
                    Loop
                    If Sp_GuiasFacturadas <> Empty Then
                            LvMateriales.ListItems.Add , , ""
                            LvMateriales.ListItems(LvMateriales.ListItems.Count).SubItems(3) = Mid(Sp_GuiasFacturadas, 1, Len(Sp_GuiasFacturadas) - 1)
                    End If
        
                    Sql = "SELECT neto,iva,bruto " & _
                          "FROM ven_doc_venta " & _
                          "WHERE  rut_emp='" & SP_Rut_Activo & "' AND doc_id=" & CboDocVenta.ItemData(CboDocVenta.ListIndex) & " AND no_documento=" & TxtNroDocumento
                    Consulta RsTmp, Sql
                    If RsTmp.RecordCount > 0 Then
                        Me.SkBrutoMateriales = NumFormat(RsTmp!bruto)
                        SkTotalMateriales = NumFormat(RsTmp!Neto)
                        SkIvaMateriales = NumFormat(RsTmp!Iva)
                    End If
                End If
        Else
            'Factura con detalle de productos
                
                If RsTmp.RecordCount > 0 Then
                    RsTmp.MoveFirst
                    Lm_Ids = ""
                    Do While Not RsTmp.EOF
                        Lm_Ids = Lm_Ids & RsTmp!ID & ","
                        RsTmp.MoveNext
                    Loop
                End If
                Lm_Ids = Mid(Lm_Ids, 1, Len(Lm_Ids) - 1)
                DetalleParaFacturaGuias Lm_Ids
        End If
        ' Fin
    End If
    Printer.CurrentY = Printer.CurrentY + 1
    
    For i = 1 To LvMateriales.ListItems.Count
        '6 - 3 - 7 - 8
        Printer.CurrentX = Printer.CurrentX + 0.2 'INTERLINIA ARTICULOS
       ' Printer.CurrentX = Cx - 2
        Printer.CurrentX = Cx - 1
        
        p_Codigo = LvMateriales.ListItems(i).SubItems(1)
        RSet p_Cantidad = LvMateriales.ListItems(i).SubItems(6)
       p_UM = " " 'LvMateriales.ListItems(i).SubItems(21)
        p_Detalle = LvMateriales.ListItems(i).SubItems(3)
        RSet p_Unitario = NumFormat(Round(CDbl(LvMateriales.ListItems(i).SubItems(7)) / 1.19, 0))
        RSet p_Total = NumFormat(Round(CDbl(LvMateriales.ListItems(i).SubItems(8)) / 1.19, 0))
        
        Printer.Print p_Codigo & " " & p_Cantidad & " " & p_UM & " " & p_Detalle & "    " & p_Unitario & " " & p_Total
    Next
    
    
        
    If Bm_Factura_por_Guias Then LvMateriales.ListItems.Clear
        
    
    
    
    Printer.CurrentX = Cx - 1
    Printer.CurrentY = Cy + 14.5
    pos = Printer.CurrentY
        
    Printer.CurrentX = Cx + 1
    Printer.CurrentY = pos - 2.2
   ' Printer.Print TxtComentario
    
  '  pos = Printer.CurrentY
  
    Printer.CurrentX = Cx + 1
        
    'Printer.CurrentY = pos
    Printer.CurrentY = pos - 1.4
    
   ' Sp_Letras = UCase(BrutoAletras(CDbl(SkBrutoMateriales), True))
    
   ' mitad = Len(Sp_Letras) \ 2
   ' buscaespacio = InStr(mitad, Sp_Letras, " ")
   ' If buscaespacio > 0 Then
   '         Printer.Print Mid(Sp_Letras, 1, buscaespacio)
   '         Printer.CurrentX = Cx - 1
   '         Printer.CurrentY = Cy + 15.3
   '         Printer.Print Mid(Sp_Letras, buscaespacio)
   ' Else
   '           Printer.Print Sp_Letras
   ' End If
    
    
    
    
    RSet Sp_Neto = SkTotalMateriales
    RSet Sp_IVA = SkIvaMateriales
    RSet Sp_Total = SkBrutoMateriales
    pos = pos - 1.6
    'pos = pos - 1.4
    Printer.CurrentY = pos - 0.5
    Printer.CurrentX = Cx + 11.9
    Printer.Print "Total Neto $" & Sp_Neto
    
    Printer.CurrentY = Printer.CurrentY + 1
    
    Printer.CurrentX = Cx + 0.5
    Printer.Print TxtComentario
     
    'Printer.CurrentX = Cx + 14.5
    'Printer.CurrentY = pos + 0.9
    'Printer.Print Sp_IVA
    
    'Printer.CurrentX = Cx + 14.5
    'Printer.CurrentY = pos + 1.8
    'Printer.Print Sp_Total
    
    Printer.NewPage
    Printer.EndDoc
    Exit Sub
ProblemaImpresora:
    MsgBox "Ocurrio un error al intentar imprimir..." & vbNewLine & Err.Description & vbNewLine & Err.Number

End Sub

Private Sub ImprimeBoletaFertiquimica()
    Dim Cx As Double, Cy As Double, Sp_Fecha As String, Ip_Mes As Integer, Dp_S As Double, Sp_Letras As String
    Dim Sp_Neto As String * 12, Sp_IVA As String * 12, Sp_Total As String * 12
    Dim Sp_GuiasFacturadas As String
    
    Dim p_Codigo As String * 5
    Dim p_Cantidad As String * 7
    Dim p_UM As String * 4
    Dim p_Detalle As String * 40
    Dim p_Unitario As String * 10
    Dim p_Total As String * 11
    Dim p_Mes As String * 10
    Dim p_CiudadF As String * 20
    
    On Error GoTo ProblemaImpresora
    Printer.FontName = "Courier New"
    Printer.FontSize = 8
    Printer.ScaleMode = 7
        
  'Aqui leemos el X y el Y iniciales de la tabla de sucursales
    Sql = "SELECT sue_cx,sue_cy " & _
            "FROM sis_empresas_sucursales " & _
            "WHERE sue_id=" & IG_id_Sucursal_Empresa
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        Cx = Val(RsTmp!sue_cx) - 0.4
        Cy = Val(RsTmp!sue_cy) - 2 'vertical)
    Else
        Cx = 1.1 'horizontal
        Cy = 3 'vertical
    
    End If
    Dp_S = 0.1
    
    Printer.CurrentX = Cx
    Printer.CurrentY = Cy
    Sp_Fecha = DtFecha.Value
    Ip_Mes = Val(Mid(Sp_Fecha, 4, 2))
    
    Printer.CurrentY = Printer.CurrentY + 1.3
    Printer.CurrentX = Cx + 10.3
    p_Mes = UCase(MonthName(Ip_Mes))
    Printer.Print Mid(Sp_Fecha, 1, 2) & Space(2) & Ip_Mes & Space(2) & Mid(Sp_Fecha, 7, 4)

    Printer.CurrentX = Cx
    Printer.CurrentY = Printer.CurrentY - 0.2
    pos = Printer.CurrentY
    Printer.Print TxtRazonSocial
    
    Printer.CurrentY = Printer.CurrentY + 0.1
    
    
   
    Printer.CurrentX = Cx
   ' Printer.CurrentY = Printer.CurrentY + Dp_S
    'pos = Printer.CurrentY
    'posd = Printer.CurrentY
    Printer.Print TxtDireccion
    
    Printer.CurrentY = Printer.CurrentY + 0.1
    
    Printer.CurrentX = Cx
    'Printer.CurrentY = pos
    'pos = Printer.CurrentY
    Printer.Print Me.TxtRut
    
    
    
       
    'Printer.CurrentY = Printer.CurrentY + 1
    
    If Bm_Factura_por_Guias Then
        '**************************************************
        'Factura por guias Diciembre del 2013
        '**************************************************-
        Sql = "SELECT no_documento,id " & _
              "FROM ven_doc_venta " & _
              "WHERE  rut_emp='" & SP_Rut_Activo & "' AND doc_id_factura=" & CboDocVenta.ItemData(CboDocVenta.ListIndex) & " AND nro_factura=" & Me.TxtNroDocumento
        Consulta RsTmp, Sql
        
        If SG_Presentacion_Factura = "SEGUN" Then
                LvMateriales.ListItems.Add , , ""
                LvMateriales.ListItems(LvMateriales.ListItems.Count).SubItems(3) = "SEGUN GUIAS NRO"

                If RsTmp.RecordCount > 0 Then
                    Sp_GuiasFacturadas = Empty
                    RsTmp.MoveFirst
                    cont = 1
                    Do While Not RsTmp.EOF
                        Sp_GuiasFacturadas = Sp_GuiasFacturadas & RsTmp!no_documento & ","
                        cont = cont + 1
                        RsTmp.MoveNext
                        If cont = 3 Then
                            LvMateriales.ListItems.Add , , ""
                            LvMateriales.ListItems(LvMateriales.ListItems.Count).SubItems(3) = Mid(Sp_GuiasFacturadas, 1, Len(Sp_GuiasFacturadas) - 1)
                            Sp_GuiasFacturadas = Empty
                        End If
                    Loop
                    If Sp_GuiasFacturadas <> Empty Then
                            LvMateriales.ListItems.Add , , ""
                            LvMateriales.ListItems(LvMateriales.ListItems.Count).SubItems(3) = Mid(Sp_GuiasFacturadas, 1, Len(Sp_GuiasFacturadas) - 1)
                    End If
        
                    Sql = "SELECT neto,iva,bruto " & _
                          "FROM ven_doc_venta " & _
                          "WHERE  rut_emp='" & SP_Rut_Activo & "' AND doc_id=" & CboDocVenta.ItemData(CboDocVenta.ListIndex) & " AND no_documento=" & TxtNroDocumento
                    Consulta RsTmp, Sql
                    If RsTmp.RecordCount > 0 Then
                        Me.SkBrutoMateriales = NumFormat(RsTmp!bruto)
                        SkTotalMateriales = NumFormat(RsTmp!Neto)
                        SkIvaMateriales = NumFormat(RsTmp!Iva)
                    End If
                End If
        Else
            'Factura con detalle de productos
                
                If RsTmp.RecordCount > 0 Then
                    RsTmp.MoveFirst
                    Lm_Ids = ""
                    Do While Not RsTmp.EOF
                        Lm_Ids = Lm_Ids & RsTmp!ID & ","
                        RsTmp.MoveNext
                    Loop
                End If
                Lm_Ids = Mid(Lm_Ids, 1, Len(Lm_Ids) - 1)
                DetalleParaFacturaGuias Lm_Ids
        End If
        ' Fin
    End If
    Printer.CurrentY = Printer.CurrentY + 0.8
    
    For i = 1 To LvMateriales.ListItems.Count
        '6 - 3 - 7 - 8
        Printer.CurrentX = Printer.CurrentX + 0.2 'INTERLINIA ARTICULOS
       ' Printer.CurrentX = Cx - 2
        Printer.CurrentX = Cx - 1
        
        p_Codigo = LvMateriales.ListItems(i).SubItems(1)
        RSet p_Cantidad = LvMateriales.ListItems(i).SubItems(6)
       p_UM = " " 'LvMateriales.ListItems(i).SubItems(21)
        p_Detalle = LvMateriales.ListItems(i).SubItems(3)
        RSet p_Unitario = LvMateriales.ListItems(i).SubItems(7)
        RSet p_Total = LvMateriales.ListItems(i).SubItems(8)
        
        Printer.Print p_Codigo & " " & p_Cantidad & " " & p_Detalle & " " & p_Unitario & " " & p_Total
    Next
        
    If Bm_Factura_por_Guias Then LvMateriales.ListItems.Clear
       
    Printer.CurrentX = Cx - 1
    Printer.CurrentY = Cy + 10.2
    pos = Printer.CurrentY
        
    Printer.CurrentX = Cx + 1
    Printer.CurrentY = pos - 2.2
   ' Printer.Print TxtComentario
    
  '  pos = Printer.CurrentY
  
    Printer.CurrentX = Cx + 1
        
    'Printer.CurrentY = pos
    'Printer.CurrentY = pos - 1.4
    
    RSet Sp_Neto = SkTotalMateriales
    RSet Sp_IVA = SkIvaMateriales
    RSet Sp_Total = SkBrutoMateriales
    pos = pos - 1.6
    'pos = pos - 1.4
    Printer.CurrentY = pos - 0.5
    Printer.CurrentX = Cx + 10
    Printer.Print Sp_Total
    
    Printer.CurrentY = Printer.CurrentY + 1
    
    'Printer.CurrentX = Cx + 0.5
    'Printer.Print TxtComentario
     
  
    
    Printer.NewPage
    Printer.EndDoc
    Exit Sub
ProblemaImpresora:
    MsgBox "Ocurrio un error al intentar imprimir..." & vbNewLine & Err.Description & vbNewLine & Err.Number

End Sub

Private Sub ImprimeBoletaAlcalde()
    Dim Cx As Double, Cy As Double, Sp_Fecha As String, Ip_Mes As Integer, Dp_S As Double, Sp_Letras As String
    Dim Sp_Neto As String * 12, Sp_IVA As String * 12, Sp_Total As String * 12
    Dim Sp_GuiasFacturadas As String
    
    Dim p_Codigo As String * 6
    Dim p_Cantidad As String * 7
    Dim p_UM As String * 4
    Dim p_Detalle As String * 40
    Dim p_Unitario As String * 10
    Dim p_Total As String * 11
    Dim p_Mes As String * 10
    Dim p_CiudadF As String * 20
    
    On Error GoTo ProblemaImpresora
    Printer.FontName = "Courier New"
    Printer.FontSize = 10
    Printer.ScaleMode = 7
        
  'Aqui leemos el X y el Y iniciales de la tabla de sucursales
    Sql = "SELECT sue_cx,sue_cy " & _
            "FROM sis_empresas_sucursales " & _
            "WHERE sue_id=" & IG_id_Sucursal_Empresa
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        Cx = Val(RsTmp!sue_cx) - 0.4
        Cy = Val(RsTmp!sue_cy) - 2 'vertical)
    Else
        Cx = 1.1 'horizontal
        Cy = 0.1 'vertical
    
    End If
    Dp_S = 0.1
    
    Printer.CurrentX = Cx
    Printer.CurrentY = Cy
    Sp_Fecha = DtFecha.Value
    Ip_Mes = Val(Mid(Sp_Fecha, 4, 2))
    
    'Printer.CurrentY = Printer.CurrentY + 1.3
    Printer.CurrentX = Cx + 13.5
    p_Mes = UCase(MonthName(Ip_Mes))
    pos = Printer.CurrentY
    Printer.Print Mid(Sp_Fecha, 1, 2) & Space(18) & Mid(Sp_Fecha, 7, 4)
    Printer.CurrentY = pos
    Printer.CurrentX = Cx + 15
    Printer.Print MonthName(Ip_Mes)
    

  '  Printer.CurrentX = Cx
  '  Printer.CurrentY = Printer.CurrentY - 0.2
  '  pos = Printer.CurrentY
  '  Printer.Print TxtRazonSocial
    
  '  Printer.CurrentY = Printer.CurrentY + 0.1
    
    
   
'    Printer.CurrentX = Cx
   ' Printer.CurrentY = Printer.CurrentY + Dp_S
    'pos = Printer.CurrentY
    'posd = Printer.CurrentY
    'Printer.Print txtDireccion
    
  '  Printer.CurrentY = Printer.CurrentY + 0.1
    
  '  Printer.CurrentX = Cx
    'Printer.CurrentY = pos
    'pos = Printer.CurrentY
'    Printer.Print Me.TxtRut
    
    
    
       
    'Printer.CurrentY = Printer.CurrentY + 1
    
    If Bm_Factura_por_Guias Then
        '**************************************************
        'Factura por guias Diciembre del 2013
        '**************************************************-
        Sql = "SELECT no_documento,id " & _
              "FROM ven_doc_venta " & _
              "WHERE  rut_emp='" & SP_Rut_Activo & "' AND doc_id_factura=" & CboDocVenta.ItemData(CboDocVenta.ListIndex) & " AND nro_factura=" & Me.TxtNroDocumento
        Consulta RsTmp, Sql
        
        If SG_Presentacion_Factura = "SEGUN" Then
                LvMateriales.ListItems.Add , , ""
                LvMateriales.ListItems(LvMateriales.ListItems.Count).SubItems(3) = "SEGUN GUIAS NRO"

                If RsTmp.RecordCount > 0 Then
                    Sp_GuiasFacturadas = Empty
                    RsTmp.MoveFirst
                    cont = 1
                    Do While Not RsTmp.EOF
                        Sp_GuiasFacturadas = Sp_GuiasFacturadas & RsTmp!no_documento & ","
                        cont = cont + 1
                        RsTmp.MoveNext
                        If cont = 3 Then
                            LvMateriales.ListItems.Add , , ""
                            LvMateriales.ListItems(LvMateriales.ListItems.Count).SubItems(3) = Mid(Sp_GuiasFacturadas, 1, Len(Sp_GuiasFacturadas) - 1)
                            Sp_GuiasFacturadas = Empty
                        End If
                    Loop
                    If Sp_GuiasFacturadas <> Empty Then
                            LvMateriales.ListItems.Add , , ""
                            LvMateriales.ListItems(LvMateriales.ListItems.Count).SubItems(3) = Mid(Sp_GuiasFacturadas, 1, Len(Sp_GuiasFacturadas) - 1)
                    End If
        
                    Sql = "SELECT neto,iva,bruto " & _
                          "FROM ven_doc_venta " & _
                          "WHERE  rut_emp='" & SP_Rut_Activo & "' AND doc_id=" & CboDocVenta.ItemData(CboDocVenta.ListIndex) & " AND no_documento=" & TxtNroDocumento
                    Consulta RsTmp, Sql
                    If RsTmp.RecordCount > 0 Then
                        Me.SkBrutoMateriales = NumFormat(RsTmp!bruto)
                        SkTotalMateriales = NumFormat(RsTmp!Neto)
                        SkIvaMateriales = NumFormat(RsTmp!Iva)
                    End If
                End If
        Else
            'Factura con detalle de productos
                
                If RsTmp.RecordCount > 0 Then
                    RsTmp.MoveFirst
                    Lm_Ids = ""
                    Do While Not RsTmp.EOF
                        Lm_Ids = Lm_Ids & RsTmp!ID & ","
                        RsTmp.MoveNext
                    Loop
                End If
                Lm_Ids = Mid(Lm_Ids, 1, Len(Lm_Ids) - 1)
                DetalleParaFacturaGuias Lm_Ids
        End If
        ' Fin
    End If
    Printer.CurrentY = Printer.CurrentY + 0.8 + 1
    
    For i = 1 To LvMateriales.ListItems.Count
        '6 - 3 - 7 - 8
        Printer.CurrentX = Printer.CurrentX + 0.2 'INTERLINIA ARTICULOS
       ' Printer.CurrentX = Cx - 2
        Printer.CurrentX = Cx - 2
        
        p_Codigo = LvMateriales.ListItems(i).SubItems(1)
        RSet p_Cantidad = LvMateriales.ListItems(i).SubItems(6)
       p_UM = " " 'LvMateriales.ListItems(i).SubItems(21)
        p_Detalle = LvMateriales.ListItems(i).SubItems(3)
        RSet p_Unitario = LvMateriales.ListItems(i).SubItems(7)
        RSet p_Total = LvMateriales.ListItems(i).SubItems(8)
        
        Printer.Print p_Cantidad & "     " & p_Codigo & "      " & p_Detalle & " " & p_Unitario & " " & p_Total
    Next
        
    If Bm_Factura_por_Guias Then LvMateriales.ListItems.Clear
       
    Printer.CurrentX = Cx - 1
    Printer.CurrentY = Cy + 10.2
    pos = Printer.CurrentY
        
    Printer.CurrentX = Cx + 1
    Printer.CurrentY = pos - 2.2
   ' Printer.Print TxtComentario
    
  '  pos = Printer.CurrentY
  
    Printer.CurrentX = Cx + 1
        
    'Printer.CurrentY = pos
    'Printer.CurrentY = pos - 1.4
    
    RSet Sp_Neto = SkTotalMateriales
    RSet Sp_IVA = SkIvaMateriales
    RSet Sp_Total = SkBrutoMateriales
    pos = pos - 1.6
    'pos = pos - 1.4
    Printer.CurrentY = pos - 0.5
    Printer.CurrentY = 10.4
    Printer.CurrentX = Cx + 13.9
    Printer.FontBold = True
    Printer.Print Sp_Total
    
    Printer.CurrentY = Printer.CurrentY + 1
    
    'Printer.CurrentX = Cx + 0.5
    'Printer.Print TxtComentario
     
  
    
    Printer.NewPage
    Printer.EndDoc
    Exit Sub
ProblemaImpresora:
    MsgBox "Ocurrio un error al intentar imprimir..." & vbNewLine & Err.Description & vbNewLine & Err.Number

End Sub


Private Sub ImprimeBoletaTotalGomas()
    Dim Cx As Double, Cy As Double, Sp_Fecha As String, Ip_Mes As Integer, Dp_S As Double, Sp_Letras As String
    Dim Sp_Neto As String * 12, Sp_IVA As String * 12, Sp_Total As String * 12
    Dim Sp_GuiasFacturadas As String
    
    Dim p_Codigo As String * 6
    Dim p_Cantidad As String * 7
    Dim p_UM As String * 4
    Dim p_Detalle As String * 40
    Dim p_Unitario As String * 10
    Dim p_Total As String * 11
    Dim p_Mes As String * 10
    Dim p_CiudadF As String * 20
    
    On Error GoTo ProblemaImpresora
    Printer.FontName = "Courier New"
    Printer.FontSize = 10
    Printer.ScaleMode = 7
        
  'Aqui leemos el X y el Y iniciales de la tabla de sucursales
    Sql = "SELECT sue_cx,sue_cy " & _
            "FROM sis_empresas_sucursales " & _
            "WHERE sue_id=" & IG_id_Sucursal_Empresa
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        Cx = Val(RsTmp!sue_cx) - 0.4
        Cy = Val(RsTmp!sue_cy) - 2 'vertical)
    Else
        Cx = 1.3 'horizontal
        Cy = 3.2 'vertical
    
    End If
    Dp_S = 0.1
    
    Printer.CurrentX = Cx
    Printer.CurrentY = Cy
    Sp_Fecha = DtFecha.Value
    Ip_Mes = Val(Mid(Sp_Fecha, 4, 2))
    
    'Printer.CurrentY = Printer.CurrentY + 1.3
    Printer.CurrentX = Cx + 12.2
    p_Mes = UCase(MonthName(Ip_Mes))
    pos = Printer.CurrentY
    Printer.Print Mid(Sp_Fecha, 1, 2) & Space(16) & Mid(Sp_Fecha, 7, 4)
    Printer.CurrentY = pos
    Printer.CurrentX = Cx + 14
    Printer.Print Mid(MonthName(Ip_Mes), 1, 7)
    

  '  Printer.CurrentX = Cx
  '  Printer.CurrentY = Printer.CurrentY - 0.2
  '  pos = Printer.CurrentY
  '  Printer.Print TxtRazonSocial
    
  '  Printer.CurrentY = Printer.CurrentY + 0.1
    
    
   
'    Printer.CurrentX = Cx
   ' Printer.CurrentY = Printer.CurrentY + Dp_S
    'pos = Printer.CurrentY
    'posd = Printer.CurrentY
    'Printer.Print txtDireccion
    
  '  Printer.CurrentY = Printer.CurrentY + 0.1
    
  '  Printer.CurrentX = Cx
    'Printer.CurrentY = pos
    'pos = Printer.CurrentY
'    Printer.Print Me.TxtRut
    
    
    
       
    'Printer.CurrentY = Printer.CurrentY + 1
    
    If Bm_Factura_por_Guias Then
        '**************************************************
        'Factura por guias Diciembre del 2013
        '**************************************************-
        Sql = "SELECT no_documento,id " & _
              "FROM ven_doc_venta " & _
              "WHERE  rut_emp='" & SP_Rut_Activo & "' AND doc_id_factura=" & CboDocVenta.ItemData(CboDocVenta.ListIndex) & " AND nro_factura=" & Me.TxtNroDocumento
        Consulta RsTmp, Sql
        
        If SG_Presentacion_Factura = "SEGUN" Then
                LvMateriales.ListItems.Add , , ""
                LvMateriales.ListItems(LvMateriales.ListItems.Count).SubItems(3) = "SEGUN GUIAS NRO"

                If RsTmp.RecordCount > 0 Then
                    Sp_GuiasFacturadas = Empty
                    RsTmp.MoveFirst
                    cont = 1
                    Do While Not RsTmp.EOF
                        Sp_GuiasFacturadas = Sp_GuiasFacturadas & RsTmp!no_documento & ","
                        cont = cont + 1
                        RsTmp.MoveNext
                        If cont = 3 Then
                            LvMateriales.ListItems.Add , , ""
                            LvMateriales.ListItems(LvMateriales.ListItems.Count).SubItems(3) = Mid(Sp_GuiasFacturadas, 1, Len(Sp_GuiasFacturadas) - 1)
                            Sp_GuiasFacturadas = Empty
                        End If
                    Loop
                    If Sp_GuiasFacturadas <> Empty Then
                            LvMateriales.ListItems.Add , , ""
                            LvMateriales.ListItems(LvMateriales.ListItems.Count).SubItems(3) = Mid(Sp_GuiasFacturadas, 1, Len(Sp_GuiasFacturadas) - 1)
                    End If
        
                    Sql = "SELECT neto,iva,bruto " & _
                          "FROM ven_doc_venta " & _
                          "WHERE  rut_emp='" & SP_Rut_Activo & "' AND doc_id=" & CboDocVenta.ItemData(CboDocVenta.ListIndex) & " AND no_documento=" & TxtNroDocumento
                    Consulta RsTmp, Sql
                    If RsTmp.RecordCount > 0 Then
                        Me.SkBrutoMateriales = NumFormat(RsTmp!bruto)
                        SkTotalMateriales = NumFormat(RsTmp!Neto)
                        SkIvaMateriales = NumFormat(RsTmp!Iva)
                    End If
                End If
        Else
            'Factura con detalle de productos
                
                If RsTmp.RecordCount > 0 Then
                    RsTmp.MoveFirst
                    Lm_Ids = ""
                    Do While Not RsTmp.EOF
                        Lm_Ids = Lm_Ids & RsTmp!ID & ","
                        RsTmp.MoveNext
                    Loop
                End If
                Lm_Ids = Mid(Lm_Ids, 1, Len(Lm_Ids) - 1)
                DetalleParaFacturaGuias Lm_Ids
        End If
        ' Fin
    End If
    Printer.CurrentY = Printer.CurrentY + 0.8 + 0.5
    
    For i = 1 To LvMateriales.ListItems.Count
        '6 - 3 - 7 - 8
        Printer.CurrentX = Printer.CurrentX + 0.2 'INTERLINIA ARTICULOS
       ' Printer.CurrentX = Cx - 2
        Printer.CurrentX = Cx - 1.9
        
        p_Codigo = LvMateriales.ListItems(i).SubItems(1)
        RSet p_Cantidad = LvMateriales.ListItems(i).SubItems(6)
       p_UM = " " 'LvMateriales.ListItems(i).SubItems(21)
        p_Detalle = LvMateriales.ListItems(i).SubItems(3)
        RSet p_Unitario = LvMateriales.ListItems(i).SubItems(7)
        RSet p_Total = LvMateriales.ListItems(i).SubItems(8)
        
        Printer.Print p_Cantidad & "     " & p_Codigo & "      " & p_Detalle & " " & p_Unitario & " " & p_Total
    Next
        
    If Bm_Factura_por_Guias Then LvMateriales.ListItems.Clear
       
    Printer.CurrentX = Cx - 1
    Printer.CurrentY = Cy + 10.2
    pos = Printer.CurrentY
        
    Printer.CurrentX = Cx + 1
    Printer.CurrentY = pos - 2.2
   ' Printer.Print TxtComentario
    
  '  pos = Printer.CurrentY
  
    Printer.CurrentX = Cx + 1
        
    'Printer.CurrentY = pos
    'Printer.CurrentY = pos - 1.4
    
    RSet Sp_Neto = SkTotalMateriales
    RSet Sp_IVA = SkIvaMateriales
    RSet Sp_Total = SkBrutoMateriales
    pos = pos - 1.6
    'pos = pos - 1.4
    Printer.CurrentY = pos - 0.5
    Printer.CurrentY = 12
    Printer.CurrentX = Cx + 14.1
    Printer.FontBold = True
    Printer.Print Sp_Total
    
    Printer.CurrentY = Printer.CurrentY + 1
    
    'Printer.CurrentX = Cx + 0.5
    'Printer.Print TxtComentario
     
  
    
    Printer.NewPage
    Printer.EndDoc
    Exit Sub
ProblemaImpresora:
    MsgBox "Ocurrio un error al intentar imprimir..." & vbNewLine & Err.Description & vbNewLine & Err.Number

End Sub



Private Sub ImprimeFacturaPerlaDelSur()
    Dim Cx As Double, Cy As Double, Sp_Fecha As String, Ip_Mes As Integer, Dp_S As Double, Sp_Letras As String
    Dim Sp_Neto As String * 12, Sp_IVA As String * 12, Sp_Total As String * 12
    Dim Sp_GuiasFacturadas As String
    On Error GoTo ProblemaImpresora
    Printer.FontName = "Courier New"
    Printer.FontBold = False
    Printer.FontSize = 10
    Printer.ScaleMode = 7
    
    Cx = 3 'MARGEN HORIZONTAL INICIAL
    Cy = 3.75 'MARGEN VERTICAL INICIAL
    If CboDocVenta.Text = "GUIA" Then
        Dp_S = 0.5
    Else
        Dp_S = 0.35
    End If
    Printer.CurrentY = Cy - 1.6
    Printer.CurrentX = Cx + 13
    Printer.Print Me.TxtNroDocumento
    
    
    Printer.CurrentX = Cx
    Printer.CurrentY = Cy
    Sp_Fecha = DtFecha.Value
    Ip_Mes = Val(Mid(Sp_Fecha, 4, 2))
    If CboDocVenta.Text = "GUIA" Then
        Printer.Print Mid(Sp_Fecha, 1, 2) & Space(10) & UCase(MonthName(Ip_Mes)) & Space(17) & Mid(Sp_Fecha, 9, 2)
    Else
        Printer.Print Mid(Sp_Fecha, 1, 2) & Space(10) & UCase(MonthName(Ip_Mes)) & Space(24) & Mid(Sp_Fecha, 9, 2)
    End If
    Printer.CurrentX = Cx
    Printer.CurrentY = Printer.CurrentY + Dp_S - 0.1
    If CboDocVenta.Text = "GUIA" Then
        Printer.CurrentY = Printer.CurrentY + 0.3
    Else
        'POS = Printer.CurrentY
    End If
    pos = Printer.CurrentY
    Printer.Print "  " & TxtRazonSocial
    
    Printer.CurrentY = pos
    Printer.CurrentX = Printer.CurrentX + 15
    Printer.Print "  " & TxtRut
    
    Printer.CurrentX = Cx
    Printer.CurrentY = Printer.CurrentY + Dp_S
    pos = Printer.CurrentY
    Printer.Print "  " & TxtDireccion
    If CboSucursal.Text <> "CASA MATRIZ" Then
        Printer.CurrentX = Cx
        Printer.Print "  " & Me.TxtCiudad
    End If
    
    Printer.CurrentY = pos
    Printer.CurrentX = Printer.CurrentX + 15
    If SP_Rut_Activo = "11.500.319-4" Then 'Especial perla del sur
        Printer.Print "   " & TxtCiudad.Tag
    Else
        Printer.Print "   " & TxtCiudad
    End If
    
    
    Printer.CurrentX = Cx
    Printer.CurrentY = Printer.CurrentY + Dp_S
    pos = Printer.CurrentY
    Printer.Print CboGiros.Text ' MARIO ****   TxtGiro
    'Printer.Print "  " & TxtGiro
    
    Printer.CurrentY = pos
    Printer.CurrentX = Printer.CurrentX + 15
    Printer.Print "   " & TxtComuna
    
    If CboDocVenta.Text = "GUIA" Then
        Printer.CurrentY = Printer.CurrentY + Dp_S
        pos = Printer.CurrentY
        Printer.CurrentX = Cx + 13
        Printer.Print "  " & LbTelefono
    End If
  '  Printer.CurrentY = pos
  '  Printer.CurrentX = Cx + 7
  '  Printer.Print Me.CboPlazos.Text
  '  pos = Printer.CurrentY
    
    'Acaba el detalle de los productos
    
    Printer.CurrentY = Printer.CurrentY + 3
    
    
    
    If Bm_Factura_por_Guias Then
        '**************************************************
        'Factura por guias 6 Octubre 2011
        '**************************************************
        If SG_Presentacion_Factura = "SEGUN" Then
            Sql = "SELECT no_documento,id " & _
                    "FROM ven_doc_venta " & _
                    "WHERE  rut_emp='" & SP_Rut_Activo & "' AND doc_id_factura=" & CboDocVenta.ItemData(CboDocVenta.ListIndex) & " AND nro_factura=" & Me.TxtNroDocumento
            Consulta RsTmp, Sql
            If RsTmp.RecordCount > 0 Then
                Do While Not RsTmp.EOF
                    Lm_Ids = Lm_Ids & RsTmp!ID & ","
                    RsTmp.MoveNext
                Loop
                Lm_Ids = Mid(Lm_Ids, 1, Len(Lm_Ids) - 1)
                DetalleParaFacturaGuias Lm_Ids
            End If
        End If
        Lm_Ids = Empty
        
        LvMateriales.ListItems.Add , , ""
        LvMateriales.ListItems(LvMateriales.ListItems.Count).SubItems(3) = "SEGUN GUIAS NRO"
        Sql = "SELECT no_documento,id " & _
              "FROM ven_doc_venta " & _
              "WHERE  rut_emp='" & SP_Rut_Activo & "' AND doc_id_factura=" & CboDocVenta.ItemData(CboDocVenta.ListIndex) & " AND nro_factura=" & Me.TxtNroDocumento
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
            Sp_GuiasFacturadas = Empty
            RsTmp.MoveFirst
            cont = 1
            
                
            Do While Not RsTmp.EOF
                Lm_Ids = Lm_Ids & RsTmp!ID & ","
                Sp_GuiasFacturadas = Sp_GuiasFacturadas & RsTmp!no_documento & ","
                cont = cont + 1
                RsTmp.MoveNext
                If cont = 9 Then
                    LvMateriales.ListItems.Add , , ""
                    LvMateriales.ListItems(LvMateriales.ListItems.Count).SubItems(3) = Mid(Sp_GuiasFacturadas, 1, Len(Sp_GuiasFacturadas) - 1)
                    Sp_GuiasFacturadas = Empty
                    cont = 1
                End If
                
            Loop
            If Sp_GuiasFacturadas <> Empty Then
                    LvMateriales.ListItems.Add , , ""
                    LvMateriales.ListItems(LvMateriales.ListItems.Count).SubItems(3) = Mid(Sp_GuiasFacturadas, 1, Len(Sp_GuiasFacturadas) - 1)
            End If
            Sql = "SELECT neto,iva,bruto " & _
                  "FROM ven_doc_venta " & _
                  "WHERE  rut_emp='" & SP_Rut_Activo & "' AND doc_id=" & CboDocVenta.ItemData(CboDocVenta.ListIndex) & " AND no_documento=" & TxtNroDocumento
            Consulta RsTmp, Sql
            If RsTmp.RecordCount > 0 Then
                Me.SkBrutoMateriales = NumFormat(RsTmp!bruto)
                SkTotalMateriales = NumFormat(RsTmp!Neto)
                SkIvaMateriales = NumFormat(RsTmp!Iva)
            End If
            
            If SG_Presentacion_Factura = "DETALLE" Then
                Lm_Ids = Mid(Lm_Ids, 1, Len(Lm_Ids) - 1)
                DetalleParaFacturaGuias Lm_Ids
            End If
        End If
    End If
        ' Fin
    
    If CboDocVenta.Text = "GUIA" Then
        Printer.CurrentY = 10
    Else
        Printer.CurrentY = 8.5
    End If
    
    For i = 1 To LvMateriales.ListItems.Count
        '6 - 3 - 7 - 8
        Printer.CurrentX = Printer.CurrentX + 0.3
        Printer.CurrentX = Cx - 2
        If Val(LvMateriales.ListItems(i).SubItems(6)) > 0 Then
            Printer.Print Right(Space(8) & LvMateriales.ListItems(i).SubItems(6), 8) & " Kl " & Space(1) & _
                    Left(LvMateriales.ListItems(i).SubItems(3) & Space(49), 49) & Space(1) & _
                    Right(Space(11) & NumFormat(LvMateriales.ListItems(i).SubItems(7)), 11) & Space(3) & _
                    Right(Space(11) & NumFormat(LvMateriales.ListItems(i).SubItems(8)), 11)
        Else
        
            Printer.Print Right(Space(8) & LvMateriales.ListItems(i).SubItems(6), 8) & Space(5) & _
                    Left(LvMateriales.ListItems(i).SubItems(3) & Space(49), 49) & Space(1) & _
                    Right(Space(11) & LvMateriales.ListItems(i).SubItems(7), 11) & Space(3) & _
                    Right(Space(11) & LvMateriales.ListItems(i).SubItems(8), 11)
        End If
    Next
    
    If CboDocVenta.Text = "GUIA" Then
        Printer.CurrentX = Cx + 14
        'Printer.CurrentY = Printer.CurrentY + 1
        Printer.Print "      ----------"
        Printer.CurrentX = Cx + 14
     '   Printer.CurrentY = Printer.CurrentY + 2�
        RSet Sp_Total = SkBrutoMateriales
        Printer.Print Sp_Total
        Printer.CurrentX = Cx + 14
    
     '   Printer.CurrentY = Printer.CurrentY + 1
        Printer.Print "      ----------"
    End If
    
    Printer.CurrentY = Cy + 9
    Printer.CurrentX = Cx + 3
    Printer.Print TxtComentario
        
    'End If
    
        
    If Bm_Factura_por_Guias Then LvMateriales.ListItems.Clear
        
    
    
    Printer.CurrentX = Cx
    Printer.CurrentY = Cy + 12
    pos = Printer.CurrentY
    
    If Mid(CboDocVenta.Text, 1, 7) = "FACTURA" Then
        Sp_Letras = UCase(BrutoAletras(CDbl(SkBrutoMateriales), True))
        
      '� mitad = Len(Sp_Letras) \ 2
      '  buscaespacio = InStr(mitad, Sp_Letras, " ")
      '  If buscaespacio > 0 Then
      '          Printer.Print Mid(Sp_Letras, 1, buscaespacio)
      '          Printer.CurrentX = Cx - 1
      '          Printer.CurrentY = Cy + 13.6
      '          Printer.Print Mid(Sp_Letras, buscaespacio)
      '  Else
                  Printer.Print Sp_Letras
       ' End If
    End If
    
        
    If Mid(CboDocVenta.Text, 1, 7) = "FACTURA" Then
        Printer.CurrentY = Cy + 11.7
        RSet Sp_Neto = SkTotalMateriales
        RSet Sp_IVA = SkIvaMateriales
        RSet Sp_Total = SkBrutoMateriales
        pos = Printer.CurrentY
        Printer.CurrentY = pos
        Printer.CurrentX = Cx + 14
        Printer.Print Sp_Neto
        
        Printer.CurrentX = Cx + 14
        Printer.CurrentY = pos + 1
        Printer.Print Sp_IVA
        
        Printer.CurrentX = Cx + 14
        Printer.CurrentY = pos + 2
        Printer.Print Sp_Total
    End If
    Printer.NewPage
    Printer.EndDoc
    Exit Sub
ProblemaImpresora:
    MsgBox "Ocurrio un error al intentar imprimir..." & vbNewLine & Err.Description & vbNewLine & Err.Number

End Sub



Private Sub CmdEspecial_Click()
    If Me.LvMateriales.ListItems.Count > 0 Then
        MsgBox "Para usar esta opcion, no puede haber productos en la lista", vbOKOnly + vbExclamation
        Exit Sub
    End If
    VentaEspecial = False
    frmEspecial.Visible = True
    VtaEspecial.Show 1
  
   txtespecial(0).SetFocus
   If Not VentaEspecial Then Exit Sub
    frmEspecial.Visible = True
    frmEspecial.Left = 100
    CMDeliminaMaterial.Enabled = False
    CmdEspecial.Enabled = False
   ' FrameMA.Visible = False
    Dim EsDescp As String
    Dim EsPrecio As String
    Dim EsCodigo As String
    'MsgBox "Opcion especial para emitir boleta o factura sin afectar inventario", vbInformation
    
    
    EsDescp = txtespecial(0) 'InputBox("Ingrese descripcion")
    'EsPrecio = InputBox("Ingrese Valor de Venta Neto")
    EsPrecio = SkTotalMateriales
    If Len(EsDescp) > 0 And IsNumeric(EsPrecio) = True Then
          Codigo = "ZZZZZZZ1"
          MARCA = "-" & Space(10)
          Descripcion = txtespecial(0)     '"VENTA QUE NO AFECTA INVENTARIO
          RSet PrecioRef = EsPrecio
          Descuento = ""
          RSet Unidades = "1"
          RSet SubTotal = NumFormat(EsPrecio)
          RSet Precio = NumFormat(EsPrecio)
          RSet Descuento = Descuento
          
          'ListaMateriales.AddItem Codigo & " " & MARCA & " " & Descripcion & " " & PrecioRef & " " & Descuento & " " & Unidades & " " & Precio & " " & SubTotal
          With LvMateriales
                If Reemplazar Then
                    posicion = ElemntoAreemplazar
                Else
                    .ListItems.Add , , txtNoOrden
                    posicion = .ListItems.Count
                End If
                .ListItems(posicion).SubItems(1) = Codigo
                .ListItems(posicion).SubItems(2) = MARCA
                .ListItems(posicion).SubItems(3) = Descripcion
                .ListItems(posicion).SubItems(4) = PrecioRef
                .ListItems(posicion).SubItems(5) = Descuento
                .ListItems(posicion).SubItems(6) = Unidades
                .ListItems(posicion).SubItems(7) = Precio
                .ListItems(posicion).SubItems(8) = SubTotal
                .ListItems(posicion).SubItems(9) = 0
                .ListItems(posicion).SubItems(10) = 0
                .ListItems(posicion).SubItems(11) = 0
                .ListItems(posicion).SubItems(12) = "SI"
                .ListItems(posicion).SubItems(13) = IIf(DM_Comision_Boton_Especial, "SI", "NO")
                .ListItems(posicion).SubItems(14) = DtSalidaMaterial.Value
                .ListItems(posicion).SubItems(15) = CDbl(.ListItems(posicion).SubItems(8)) - CDbl(.ListItems(posicion).SubItems(10))
            
            End With
          
          
          
          Me.SumaListaMaterialesYMobra
    End If
    If DM_Comision_Boton_Especial = False Then TxtComisionAdmin = 0
End Sub

Private Sub CmdInfoTransporte_Click()
ven_add_transporte.Show 1
End Sub

Private Sub CmdIvaConstructoras_Click()
    
    Dim Ip_Xcien As Variant
    Dim Lp_ValorDescuento As Long
    If Val(SkIvaMateriales) > 0 Then
        TxtCreditoEmpresaConstructora.Visible = True
        '8 Julio 2016 Calcular iva contstructora
        Ip_Xcien = InputBox("Ingrese %", "Credito Constructoras", "65")
        If Val(Ip_Xcien) > 0 Then
            Lp_ValorDescuento = CDbl(SkIvaMateriales) / 100 * Ip_Xcien
            Me.TxtCreditoEmpresaConstructora = NumFormat(Lp_ValorDescuento)
            SkBrutoMateriales = NumFormat(CDbl(SkBrutoMateriales) - Lp_ValorDescuento)
            
        End If
        
    Else
        MsgBox "Debe ingresar valores antes de ralizar este calculo...", vbInformation
        TxtCodigo.SetFocus
        Exit Sub
    End If
    
End Sub

Private Sub CmdModifica_Click()
   
    If LvMateriales.SelectedItem Is Nothing Or LvMateriales.ListItems.Count = 0 Then Exit Sub
    If Val(LvMateriales.SelectedItem) = 0 Then Exit Sub
    LvMateriales.SelectedItem.SubItems(17) = CboCuenta.ItemData(CboCuenta.ListIndex)
    LvMateriales.SelectedItem.SubItems(18) = CboArea.ItemData(CboArea.ListIndex)
    LvMateriales.SelectedItem.SubItems(19) = CboCentroCosto.ItemData(CboCentroCosto.ListIndex)
    
    
    If SP_Control_Inventario = "SI" Then
        
        Sql = "UPDATE ven_detalle " & _
              "SET pla_id=" & CboCuenta.ItemData(CboCuenta.ListIndex) & "," & _
                  "are_id=" & CboArea.ItemData(CboArea.ListIndex) & "," & _
                  "cen_id=" & CboCentroCosto.ItemData(CboCentroCosto.ListIndex) & " " & _
              "WHERE id=" & LvMateriales.SelectedItem
   Else
        Sql = "UPDATE ven_sin_detalle " & _
              "SET pla_id=" & CboCuenta.ItemData(CboCuenta.ListIndex) & "," & _
                  "are_id=" & CboArea.ItemData(CboArea.ListIndex) & "," & _
                  "cen_id=" & CboCentroCosto.ItemData(CboCentroCosto.ListIndex) & " " & _
              "WHERE vsd_id=" & LvMateriales.SelectedItem
   End If
        
    
    cn.Execute Sql
    MsgBox "Datos contables modificados...", vbInformation
    
End Sub

Private Sub CmdOkSd_Click()
    If CboCuenta.ListIndex = -1 Or CboArea.ListIndex = -1 Or CboCentroCosto.ListIndex = -1 Then
        MsgBox "Faltan datos contables...", vbInformation
        CboCuenta.SetFocus
        Exit Sub
    End If
    If CDbl(TxtSdExento) + CDbl(TxtSDNeto) + CDbl(TxtSdIvaRetenido) = 0 Then
        MsgBox "Faltan valores...", vbInformation
        TxtSDNeto.SetFocus
        Exit Sub
    End If
    
    If Me.CmdBuscaActivo.Visible And Val(TxtCodigoActivo) = 0 Then
        MsgBox "Debe seleccionar un activo inmovilizado...", vbInformation
        CmdBuscaActivo.SetFocus
        Exit Sub
    End If
    
    LvSinDetalle.ListItems.Add
    LvSinDetalle.ListItems(LvSinDetalle.ListItems.Count).SubItems(1) = CboCuenta.Text
    LvSinDetalle.ListItems(LvSinDetalle.ListItems.Count).SubItems(2) = CboArea.Text
    LvSinDetalle.ListItems(LvSinDetalle.ListItems.Count).SubItems(3) = CboCentroCosto.Text
    
    LvSinDetalle.ListItems(LvSinDetalle.ListItems.Count).SubItems(4) = TxtCodigoActivo
    LvSinDetalle.ListItems(LvSinDetalle.ListItems.Count).SubItems(5) = TxtNombreActivo
    
    LvSinDetalle.ListItems(LvSinDetalle.ListItems.Count).SubItems(6) = TxtSdExento
    LvSinDetalle.ListItems(LvSinDetalle.ListItems.Count).SubItems(7) = TxtSDNeto
    LvSinDetalle.ListItems(LvSinDetalle.ListItems.Count).SubItems(8) = TxtSdIva
    
    LvSinDetalle.ListItems(LvSinDetalle.ListItems.Count).SubItems(9) = CboCuenta.ItemData(CboCuenta.ListIndex)
    LvSinDetalle.ListItems(LvSinDetalle.ListItems.Count).SubItems(10) = CboArea.ItemData(CboArea.ListIndex)
    LvSinDetalle.ListItems(LvSinDetalle.ListItems.Count).SubItems(11) = CboCentroCosto.ItemData(CboCentroCosto.ListIndex)
    
    
    TxtSdExento = 0
    TxtSDNeto = 0
    
    TxtSdIva = 0
    TxtCodigoActivo = ""
    TxtNombreActivo = ""
    'Me.CmdBuscaActivo.Visible = False
    
    CboCuenta.SetFocus
    
    SkTotalMateriales = NumFormat(TotalizaColumna(LvSinDetalle, "neto"))
    SkIvaMateriales = NumFormat(TotalizaColumna(LvSinDetalle, "iva"))
    TxtExentos = NumFormat(TotalizaColumna(LvSinDetalle, "otros"))
    SkBrutoMateriales = NumFormat(CDbl(SkTotalMateriales) + CDbl(SkIvaMateriales) + CDbl(TxtExentos) + CDbl(TxtIVARetenido))
    
    
'    calculaTotal
End Sub

Private Sub CmdRefectura_Click()
        '18-12-2015
        FrmLoad.Visible = True
        DoEvents
        
        Sql = "SELECT doc_cod_sii,no_documento,id " & _
                "FROM ven_doc_venta v " & _
                "JOIN sis_documentos d ON v.doc_id=d.doc_id " & _
                "WHERE no_documento=" & TxtNroDocumento & " AND v.doc_id=" & CboDocVenta.ItemData(CboDocVenta.ListIndex) & " AND rut_emp='" & SP_Rut_Activo & "'"
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
        GoTo directoaqui
            
        
            If Not ConsultarDTESII(RsTmp!doc_cod_sii, RsTmp!no_documento) Then
                'Rehacer el DTE
                Lp_Id_Nueva_Venta = RsTmp!ID
                
directoaqui:
                FacturaElectronica Str(RsTmp!doc_cod_sii)
                FrmLoad.Visible = False
                MsgBox "Listo...", vbQuestion + vbOKOnly
                Unload Me
                Exit Sub
            Else
                If MsgBox("Documento ya procesado... Ahora bien, si el documento no puede ser visualizado, podemos volver a emitirlo con el mismo numero..." & vbNewLine & "� Volver procesar.. ?", vbQuestion + vbOKCancel) = vbOK Then
                    
                    NoDocumento = RsTmp!no_documento
                    DocID = RsTmp!doc_cod_sii
                    
                    
                    Lp_Id_Nueva_Venta = RsTmp!ID
                    FacturaElectronica Str(RsTmp!doc_cod_sii)
                    
                    cn.Execute "UPDATE dte_folios SET dte_disponible='NO' " & _
                                    "WHERE rut_emp='" & SP_Rut_Activo & "' AND doc_id=" & DocID & " AND dte_folio=" & NoDocumento
                    MsgBox "Listo... Puede volver a visualizar...", vbQuestion + vbOKOnly
                End If
                
                
            End If
        End If
        FrmLoad.Visible = False
End Sub

Private Sub CmdSAlir_Click()
    
    If MsgBox("�Esta seguro de salir de Venta?..." & vbNewLine & "Se perderan los movimientos...", vbQuestion + vbYesNo) = vbYes Then
        Unload Me
    End If
End Sub

Private Sub CmdSg_Click()
    LvMateriales.ListItems.Clear
    LvMateriales.ListItems.Add , , ""
    LvMateriales.ListItems(LvMateriales.ListItems.Count).SubItems(1) = 111 ' CODIGO
    LvMateriales.ListItems(LvMateriales.ListItems.Count).SubItems(3) = TxtComentario
    LvMateriales.ListItems(LvMateriales.ListItems.Count).SubItems(4) = TxtNetoSG 'neto'
    LvMateriales.ListItems(LvMateriales.ListItems.Count).SubItems(6) = 1 'unidades
    LvMateriales.ListItems(LvMateriales.ListItems.Count).SubItems(8) = TxtNetoSG 'total
    LvMateriales.ListItems(LvMateriales.ListItems.Count).SubItems(14) = Date
    LvMateriales.ListItems(LvMateriales.ListItems.Count).SubItems(17) = 0
    LvMateriales.ListItems(LvMateriales.ListItems.Count).SubItems(18) = 0
    LvMateriales.ListItems(LvMateriales.ListItems.Count).SubItems(19) = 0
    SumaListaMaterialesYMobra
End Sub

Private Sub CmdXmL_Click()
    Dim Sp_Xml As String
    Dim obj As DTECloud.Integracion
    Dim Lp_Sangria As Long
    Dim Sp_Sql As String
    '13-9-2014
    'Modulo genera factura electronica
    'Autor: alvamar
    
    Dim S_tipo As String
    's_tipo =
    Sql = "SELECT id,(select doc_cod_sii from sis_documentos WHERE doc_id=" & CboDocVenta.ItemData(CboDocVenta.ListIndex) & ") sii " & _
            "FROM ven_doc_venta " & _
            "WHERE no_documento=" & TxtNroDocumento & " AND doc_id=" & CboDocVenta.ItemData(CboDocVenta.ListIndex)
            
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
    
    End If
    '1ro Crear xml
    Lp_Sangria = 4
    X = FreeFile
    
    Sp_Xml = App.Path & "\dte\" & CboDocVenta.ItemData(CboDocVenta.ListIndex) & "_" & TxtNroDocumento & ".xml"
    Sp_Xml = "C:\nueva.xml"
    Dialogo.ShowSave
    Sp_Xml = Dialogo.FileName  '  Mid(CboDocVenta.Text, 1, 2) & "_" & TxtNroDocumento & ".XML" '   Dialogo.FileName
      'Open Sp_Archivo For Output As X
    Open Sp_Xml For Output As X
        '<?xml version="1.0" encoding="ISO-8859-1"?>
        Print #X, "<DTE version=" & Chr(34) & "1.0" & Chr(34) & ">"
        Print #X, "" & Space(Lp_Sangria) & "<Documento ID=" & Chr(34) & "F" & TxtNroDocumento & "T" & RsTmp!sii & Chr(34) & ">"
        'Encabezado
        Print #X, "" & Space(Lp_Sangria) & "<Encabezado>"
            'id documento
            Print #X, "" & Space(Lp_Sangria * 2) & "<IdDoc>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<TipoDTE>" & RsTmp!sii & "</TipoDTE>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<Folio>" & TxtNroDocumento & "</Folio>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<FchEmis>" & Fql(DtFecha) & "</FchEmis>"
           '     Print #X, "" & Space(Lp_Sangria * 3) & "<FchVenc>" & Format(DtFecha.Value + CboPlazos.ItemData(CboPlazos.ListIndex), "; YYYY - MM - DD; ") & "</FchVenc>"
            Print #X, "" & Space(Lp_Sangria * 2) & "</IdDoc>"
            'Emisor
            'Select empresa para obtener los datos del emisor
            Sp_Sql = "SELECT nombre_empresa,direccion,comuna,ciudad,giro,emp_codigo_actividad_economica actividad " & _
                    "FROM sis_empresas " & _
                    "WHERE rut='" & SP_Rut_Activo & "'"
            Consulta RsTmp, Sp_Sql
            
            Print #X, "" & Space(Lp_Sangria * 2) & " <Emisor>"
                'Print #X, "" & Space(Lp_Sangria * 3) & "<RUTEmisor>" & Replace(SP_Rut_Activo, ".", "") & "</RUTEmisor>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<RUTEmisor>" & Replace(SP_Rut_Activo, ".", "") & "</RUTEmisor>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<RznSoc>" & RsTmp!nombre_empresa & "</RznSoc>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<GiroEmis>" & RsTmp!giro & "</GiroEmis>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<Acteco>" & RsTmp!actividad & "</Acteco>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<DirOrigen>" & RsTmp!direccion & "</DirOrigen>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<CmnaOrigen>" & RsTmp!comuna & "</CmnaOrigen>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<CiudadOrigen>" & RsTmp!ciudad & "</CiudadOrigen>"
            Print #X, "" & Space(Lp_Sangria * 2) & "</Emisor>"
            
            'Receptor
            Print #X, "" & Space(Lp_Sangria * 2) & " <Receptor>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<RUTRecep>" & Replace(TxtRut, ".", "") & "</RUTRecep>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<RznSocRecep>" & Me.TxtRazonSocial & "</RznSocRecep>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<GiroRecep>" & TxtGiro & "</GiroRecep>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<Contacto></Contacto>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<DirRecep>" & TxtDireccion & "</DirRecep>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<CmnaRecep>" & TxtComuna & "</CmnaRecep>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<CiudadRecep>" & TxtCiudad & "</CiudadRecep>"
            Print #X, "" & Space(Lp_Sangria * 2) & "</Receptor>"
            
             'Totales
            Print #X, "" & Space(Lp_Sangria * 2) & " <Totales>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<MntNeto>" & CDbl(SkTotalMateriales) & "</MntNeto>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<MntExe>" & CDbl(TxtExentos) & "</MntExe>"
                Print #X, "" & Space(Lp_Sangria * 3) & " <TasaIVA>" & DG_IVA & "</TasaIVA>"
                Print #X, "" & Space(Lp_Sangria * 3) & " <IVA>" & CDbl(SkIvaMateriales) & "</IVA>"
                'Antes del monto total debemos verificar si la factura incluye impuestos retenidos
'                                         <ImptoReten>
'                                            <TipoImp>18</TipoImp>
'                                            <TasaImp>5</TasaImp>
'                                            <MontoImp>8887</MontoImp>
'                                        </ImptoReten>
                Print #X, "" & Space(Lp_Sangria * 3) & "<MntTotal>" & CDbl(SkBrutoMateriales) & "</MntTotal>"
            Print #X, "" & Space(Lp_Sangria * 2) & "</Totales>"
                
            'Cerramos encabezado
            Print #X, "" & Space(Lp_Sangria) & "</Encabezado>"
                
                
           'Comenzamos el detalle
           
           If SP_Control_Inventario = "SI" Then
           'Recorremos la grilla con productos
                      For i = 1 To LvMateriales.ListItems.Count
                          Print #X, "" & Space(Lp_Sangria) & "<Detalle>"
                              Print #X, "" & Space(Lp_Sangria * 2) & "<NroLinDet>" & i & "</NroLinDet>"
                              Print #X, "" & Space(Lp_Sangria * 2) & " <CdgItem>"
                              Print #X, "" & Space(Lp_Sangria * 3) & "<TpoCodigo>PLU</TpoCodigo>"
                              Print #X, "" & Space(Lp_Sangria * 3) & "<VlrCodigo>" & LvMateriales.ListItems(i).SubItems(1) & "</VlrCodigo>"
                              Print #X, "" & Space(Lp_Sangria * 2) & "</CdgItem>"
                              Print #X, "" & Space(Lp_Sangria * 2) & "<NmbItem>" & LvMateriales.ListItems(i).SubItems(3) & "</NmbItem>"
                              Print #X, "" & Space(Lp_Sangria * 2) & "<QtyItem>" & LvMateriales.ListItems(i).SubItems(6) & "</QtyItem>"
                              Print #X, "" & Space(Lp_Sangria * 2) & "<PrcItem>" & CDbl(LvMateriales.ListItems(i).SubItems(7)) & "</PrcItem>"
              '                    <DescuentoPct>15</DescuentoPct>
              '                    <DescuentoMonto>4212</DescuentoMonto>
              '                             <!- Solo se indica el c�digo del impuesto adicional '
              '                    <CodImpAdic>17</CodImpAdic>
                              Print #X, "" & Space(Lp_Sangria * 2) & "<MontoItem>" & CDbl(LvMateriales.ListItems(i).SubItems(8)) & "</MontoItem>"
                      
                          Print #X, "" & Space(Lp_Sangria) & "</Detalle>"
                      Next
            Else
                'CUANDO ES SIN INVENTARIO
                
            
            
            
            End If
            'Cerramos detalle
            
                
        If FrmReferencia.Visible Then
                'CREAR REFERENCIA
                    Sql = "SELECT doc_cod_sii " & _
                            "FROM sis_documentos d " & _
                            "join ven_doc_venta v ON d.doc_id=v.doc_id " & _
                            "WHERE  id_ref =(SELECT id FROM ven_doc_venta WHERE rut_emp='" & SP_Rut_Activo & "' and no_documento=" & TxtNroDocumento & " AND doc_id=" & CboDocVenta.ItemData(CboDocVenta.ListIndex) & ")"
                
                    Consulta RsTmp, Sql
                    If RsTmp.RecordCount > 0 Then
                        SkDocIdReferencia = rsmtp!doc_cod_sii
                    End If
                    
                    If SkCodigoReferencia = 4 Then SkCodigoReferencia = 3
                    SkFechaReferencia = Format(Me.SkFechaReferencia, "YYYY-MM-DD")
                          '1  SkDocIdReferencia = 33
                
                    Print #X, "" & Space(Lp_Sangria) & "<Referencia>"
                    Print #X, "" & Space(Lp_Sangria * 2) & "<NroLinRef>1</NroLinRef>"
                    Print #X, "" & Space(Lp_Sangria * 2) & "<TpoDocRef>" & SkDocIdReferencia & "</TpoDocRef>"
                    Print #X, "" & Space(Lp_Sangria * 2) & "<FolioRef>" & TxtNroReferencia & "</FolioRef>"
                        
                    Print #X, "" & Space(Lp_Sangria * 2) & "<FchRef>" & SkFechaReferencia & "</FchRef>"
                    Print #X, "" & Space(Lp_Sangria * 2) & "<CodRef>" & 1 & "</CodRef>"
                    Print #X, "" & Space(Lp_Sangria * 2) & "<RazonRef>" & "ANULA DOCUMENTO" & "</RazonRef>"
                       
                Print #X, "" & Space(Lp_Sangria) & "</Referencia>"

            
            'CERRAR REFERNCIA
        End If

        Print #X, "" & Space(Lp_Sangria) & "</Documento>"
        Print #X, "" & "</DTE>"
    Close #X
End Sub

Private Sub Command1_Click()
    ConexionAcces
End Sub



Private Sub DtFecha_GotFocus()
    Me.DtFecha.CalendarBackColor = ClrCfoco
End Sub

Private Sub DtFecha_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then SendKeys ("{TAB}")
End Sub
Private Sub DtFecha_KeyUp(KeyCode As Integer, Shift As Integer)
    On Error Resume Next
    If KeyCode = 13 Then SendKeys ("{TAB}")
End Sub

Private Sub DtFecha_LostFocus()
    Me.DtFecha.CalendarBackColor = ClrSfoco
    DtSalidaMaterial = DtFecha
End Sub

Private Sub DtFecha_Validate(Cancel As Boolean)
    Exit Sub
    'no corre
    Sql = "SELECT fecha " & _
            "FROM ven_doc_venta " & _
            "WHERE rut_emp='" & SP_Rut_Activo & "' " & _
            "ORDER BY id DESC " & _
            "LIMIT 1"
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        If DtFecha.Value <> RsTmp!Fecha Then
            MsgBox "Fecha es distinta a la ultima venta ingresada.." & vbNewLine & vbNewLine & " ... (solo a modo de informacion)..", vbInformation
        End If
        
        
    End If
            
    DtFechaOC = DtFecha
End Sub

Private Sub Form_KeyUp(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF5 Then CmdEmiteDocumento_Click
    If KeyCode = vbKeyF7 Then
        'DESCUENTO  Y RECARGO ESPECIAL PARA ALCALDE
        '17 MAY 2014
        If TxtNuevoTotal.Visible Then
            TxtDscto = "0"
            TxtDscto_Validate (True)
            TxtNuevoTotal.Visible = False
            
        Else
            TxtNuevoTotal.Visible = True
            TxtNuevoTotal.SetFocus
        End If
    End If
        
End Sub

Private Sub Form_Load()
    DtGuiaRef = Date
    DtFechaOC = Date
    Sm_SoloExento = "NO"
    Bm_BuscarReferencia = False
    Sp_FacturaElectronica = ""
    CboModalidad.ListIndex = 0
    Centrar Me
    Bm_Cuenta = True
    CboEntregaInmediata.ListIndex = 0
    Bm_EditandoPorGuias = False
    FramGuias.Top = Me.FrameMA.Top
    FramGuias.Left = 140
    
    Me.Height = 10845
     'Filtrar sucursales
     '5'10'2016 ' Para espumas se agrega opcion de seleccionar sucursal en la venta
     CboSucEmpresa.Visible = False
     If SP_Rut_Activo = "7.242.181-7" Or SP_Rut_Activo = "76.610.750-8" Or SP_Rut_Activo = "77.416.270-4" Then
         CboSucEmpresa.Visible = True
         LLenarCombo CboSucEmpresa, "CONCAT(sue_direccion,' -',sue_ciudad)", "sue_id", "sis_empresas_sucursales", "sue_activa='SI'"
    End If
    CboSucEmpresa.AddItem "CASA MATRIZ"
    CboSucEmpresa.ItemData(CboSucEmpresa.ListCount - 1) = "1"
    ' CboSucEmpresa.AddItem "TODAS"
    ' CboSucEmpresa.ItemData(CboSucEmpresa.ListCount - 1) = "0"
     
    CboSucEmpresa.ListIndex = CboSucEmpresa.ListCount - 1
    
    
    If DG_ID_Unico > 0 Then
        'Editando
        bm_SoloVistaDoc = True
        Bm_Nueva_Ventas = False
    Else
        'nUEVO
        Bm_Nueva_Ventas = True
        bm_SoloVistaDoc = False
    End If
    CboGenero.AddItem "F"
    CboGenero.AddItem "M"
    CboGenero.ListIndex = 0
    If SP_Control_Inventario = "NO" Then Me.CmdModifica.Visible = False
    Bm_Factura_por_Guias = False
    Sm_operador = Empty
    S_Facturado = "NO"
    DM_Comision_Boton_Especial = False
    VentaEspecial = False
    Fococantidad = False
    Fococodigo = False
   ' QueActividad = 5 ' Informamos este valor a la forma de pago
                    ' para que sepa que se trata de una venta
    Skin2 Me, , 5
    bm_NoTimer = True
    Me.DtFecha.Value = Date
    'LaVentaEspecial = False
    
    If SP_Rut_Activo = "96.803.210-0" Then
        'para vega modelo solo cargaremos 30 dias
        LLenarCombo CboPlazos, "CONCAT(pla_nombre,'                                    ',CAST(pla_id AS CHAR))", "pla_dias", "par_plazos_vencimiento", "pla_activo='SI' AND pla_id=40", "pla_orden"
    Else
    
    
        LLenarCombo CboPlazos, "CONCAT(pla_nombre,'                                    ',CAST(pla_id AS CHAR))", "pla_dias", "par_plazos_vencimiento", "pla_activo='SI'", "pla_orden"
        
        
    End If
    
    CboPlazos.ListIndex = 0
    DtSalidaMaterial.Value = Date
    LLenarCombo CboCuenta, "CONCAT(pla_nombre,'-',MID(tpo_nombre,1,2))", "pla_id", "con_plan_de_cuentas join con_tipo_de_cuenta USING(tpo_id) ", "pla_activo='SI'"
    LLenarCombo CboCentroCosto, "cen_nombre", "cen_id", "par_centros_de_costo", "cen_activo='SI' AND rut_emp='" & SP_Rut_Activo & "'"
    LLenarCombo CboArea, "are_nombre", "are_id", "par_areas", "are_activo='SI' AND rut_emp='" & SP_Rut_Activo & "'"
    LLenarCombo CboBodega, "bod_nombre", "bod_id", "par_bodegas", "bod_activo='SI' AND rut_emp='" & SP_Rut_Activo & "'", "bod_id"
    LLenarCombo CboVendedores, "ven_nombre", "ven_id", "par_vendedores", "ven_activo='SI' AND rut_emp='" & SP_Rut_Activo & "'", "ven_nombre"
    If CboVendedores.ListCount > 0 Then CboVendedores.ListIndex = 0
    CboBodega.ListIndex = 0
    If CboVendedores.ListCount = 0 Then
        MsgBox "No hay vendedores creados, es necesario para realizar ventas...", vbInformation
        CmdSAlir_Click
        Exit Sub
    End If
    
    
    CboVendedores.ListIndex = 0
    'buscamos vendedor asociado a la sucursal
    Sql = "SELECT ven_id " & _
            "FROM sis_empresas_sucursales " & _
            "WHERE sue_id=" & IG_id_Sucursal_Empresa
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then Busca_Id_Combo CboVendedores, Val(RsTmp!ven_id)
    
 
    If SP_Control_Inventario = "SI" Then
        
        'Consultamos si daremos acceso a a modificar combos.
        Sql = "SELECT are_id,emp_seleciona_datos_contables_venta datos,emp_bitacora_productos bitacora," & _
                     "emp_venta_precios_brutos,emp_avisa_sin_stock,emp_permite_venta_fuera_periodo, " & _
                     "emp_solo_vendedor_asignado venasignado,emp_precio_venta_modificable modificapv, " & _
                     "emp_permite_crear_producto_form_venta crea_productos,emp_actualiza_precio_venta," & _
                     "emp_arrienda_productos arrienda,emp_permite_ajustar_descuento ajustadscto," & _
                     "emp_factura_electronica dte,emp_utiliza_codigos_internos_productos cod_interno,  " & _
                     "emp_de_repuestos,emp_iva_anticipado_ventas " & _
              "FROM sis_empresas " & _
              "WHERE rut='" & SP_Rut_Activo & "'"
        Consulta RsTmp, Sql
        CboCentroCosto.ListIndex = 0
        Sm_PermiteCrearProductos = "NO"
        If RsTmp.RecordCount > 0 Then
            Sp_EmpresaRepuestos = RsTmp!emp_de_repuestos
            TxtPrecio.Locked = False
            If RsTmp!ajustadscto = "SI" Then
                TxtTotalDescuento.Locked = False
                TxtTotalDescuento.BackColor = vbWhite
            
            End If
            
            '22 5 2015
            'Alvaro
            'Se cambia este atributo, y se le otorga al perfil, el que define ademas un max de dscto o un max de recargo
            If RsTmp!modificapv = "NO" Then
                TxtPrecio.Locked = True
                TxtPrecio.TabStop = False
            End If
            Dim Rp_AtributoPerfil As Recordset
            Dim Sp_Dscto_Max As String * 2
            Dim Sp_Recargo_Max As String * 2
            Sql = "SELECT pde_permite,pde_descuento_maximo,pde_recargo_maximo " & _
                    "FROM par_perfiles_atributo_descuento " & _
                    "WHERE rut_emp='" & SP_Rut_Activo & "' AND per_id=" & LogPerfil
            
            Consulta Rp_AtributoPerfil, Sql
            If Rp_AtributoPerfil.RecordCount > 0 Then
                If Rp_AtributoPerfil!pde_permite = "SI" Then
                    Sp_Dscto_Max = Rp_AtributoPerfil!pde_descuento_maximo
                    Sp_Recargo_Max = Rp_AtributoPerfil!pde_recargo_maximo
                    TxtPrecio.Tag = "SI-" & Sp_Dscto_Max & "-" & Sp_Recargo_Max
                    
                Else
                   
                    TxtPrecio.Locked = True
                    TxtPrecio.TabStop = False
                End If
            End If
                    
            
            '___________________________________________/
            
            Sm_UtilizaCodigoInterno = RsTmp!cod_interno
            
            Sm_Empresa_IVA_Anticipado = RsTmp!emp_iva_anticipado_ventas
            
            If RsTmp!arrienda = "SI" Then FraModalidad.Visible = True
            If RsTmp!crea_productos = "SI" Then Sm_PermiteCrearProductos = "SI"
            Sm_Actualiza_PrecioVenta = RsTmp!emp_actualiza_precio_venta
                
            Sm_Con_Precios_Brutos = "NO"

            
            
            
            Sm_Avisa_Sin_Stock = "SI"
            Sm_SoloVendedorAsignado = "NO"
            Sm_SoloVendedorAsignado = RsTmp!venasignado
            If Sm_SoloVendedorAsignado = "SI" Then CboVendedores.Locked = True
            Sm_PermiteFechaFueraPeriodo = RsTmp!emp_permite_venta_fuera_periodo
            If RsTmp!emp_venta_precios_brutos = "BRUTO" Then Sm_Con_Precios_Brutos = "SI"
            
            If Sm_Con_Precios_Brutos = "NO" Then
                Im_Tcal = 2
            Else
                Im_Tcal = 1
            End If
            IM_TCALORIGINAL = Im_Tcal
            If RsTmp!emp_avisa_sin_stock = "NO" Then Sm_Avisa_Sin_Stock = "NO"
            Sm_Bitacora_Productos = RsTmp!bitacora
            Busca_Id_Combo CboArea, RsTmp!are_id
            

            If RsTmp!datos = "SI" Then
                CboCuenta.Locked = False
                CboArea.Locked = False
                Me.CboCentroCosto.Locked = False
            Else
                'Me.FrameContable.Visible = False
                CboCuenta.Locked = True
                CboArea.Locked = True
                Me.CboCentroCosto.Locked = True
            End If
            
            If RsTmp!Dte = "SI" Then
                'Consultar estado libro del periodo.
                Sp_FacturaElectronica = "SI"
            End If
            
        Else
            CboArea.ListIndex = 0
        End If
        SkAi(1).Visible = False
        SkAi(2).Visible = False
        TxtCodigoActivo.Visible = False
        TxtNombreActivo.Visible = False
        CmdBuscaActivo.Visible = False
        
        CmdBuscaActivo.Top = CmdBuscaProducto.Top
        CmdBuscaActivo.Left = CmdBuscaProducto.Left
        
        'Sm_UtilizaCodigoInterno
        
    Else   'Sin inventarios
            Me.CmdBuscaProducto.Visible = False
    
            Sql = "SELECT are_id,emp_seleciona_datos_contables_venta datos,emp_bitacora_productos bitacora," & _
                     "emp_venta_precios_brutos,emp_avisa_sin_stock,emp_permite_venta_fuera_periodo, " & _
                     "emp_solo_vendedor_asignado venasignado,emp_precio_venta_modificable modificapv, " & _
                     "emp_permite_crear_producto_form_venta crea_productos " & _
              "FROM sis_empresas " & _
              "WHERE rut='" & SP_Rut_Activo & "'"
            Consulta RsTmp, Sql
            If RsTmp.RecordCount > 0 Then
                Sm_PermiteFechaFueraPeriodo = RsTmp!emp_permite_venta_fuera_periodo
            
        
            End If
            

            
            
    End If
    Me.Caption = "FORMULARIO DE VENTAS"
        
        
    
    ClienteEncontrado = False

    LLenaDocumentosDeVenta ""
    
    CargaDocumento    'Carga el documento
    
    
    If SP_Control_Inventario = "NO" Then
        'FrameMA.Visible = False
        'FrameDoc.Top = FrameMA.Top
        'CmdSalir.Top = FrameMA.Top + FrameDoc.Height + 200
        Me.SksdExento.Visible = True
        Me.SksdIva.Visible = True
        SkSdneto.Visible = True
        TxtSdExento.Visible = True
        TxtSDNeto.Visible = True
        TxtSdIva.Visible = True
        Me.CmdOkSd.Visible = True
        
        FrameContable.Height = 4050
       ' SkTotalMateriales.Locked = False
       ' SkBrutoMateriales.Locked = False
        Me.CmdEmiteDocumento.Caption = "Guardar Documento"
        'Me.Height = Me.CmdSalir.Top + Me.CmdSalir.Height * 2.3
        CmdAnularDocumento.Top = CmdSalir.Top
        Centrar Me
    
    End If
    
    
    'CONSULTAMOS SI LA EMPRESA ACTIVA
    'LLEVA CENTRO DE COSTO, AREAS
    '8 OCTUBRE 2011
    Sql = "SELECT emp_centro_de_costos,emp_areas,emp_cuenta,emp_seleciona_datos_contables_venta datoscontables," & _
                  "emp_avisa_sin_stock,emp_fecha_salida_en_venta salida,emp_columna_marca_en_venta marca,emp_cuenta_ventas " & _
          "FROM sis_empresas " & _
          "WHERE rut='" & SP_Rut_Activo & "'"
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        Lp_IdCuentaVentasPorDefecto = RsTmp!emp_cuenta_ventas
        If RsTmp!salida = "NO" Then
           'columna salida a 0
            TxtSubTotal.Width = TxtSubTotal.Width + Me.DtSalidaMaterial.Width
            Me.DtSalidaMaterial.Visible = False
            LvMateriales.ColumnHeaders(9).Width = LvMateriales.ColumnHeaders(9).Width + LvMateriales.ColumnHeaders(15).Width
            LvMateriales.ColumnHeaders(15).Width = 0
           
        End If
        If RsTmp!MARCA = "NO" Then
            'Columna marca a 0
            TxtMarca.Visible = False
            TxtDescrp.Width = TxtDescrp.Width + TxtMarca.Width
            TxtDescrp.Left = TxtMarca.Left
            LvMateriales.ColumnHeaders(4).Width = LvMateriales.ColumnHeaders(4).Width + LvMateriales.ColumnHeaders(3).Width
            LvMateriales.ColumnHeaders(3).Width = 0
            
        End If
        
        If RsTmp!emp_centro_de_costos = "NO" Then
            LLenarCombo CboCentroCosto, "cen_nombre", "cen_id", "par_centros_de_costo", "cen_id=99999"
            CboCentroCosto.ListIndex = 0
            LLenarCombo CboCentroCosto, "cen_nombre", "cen_id", "par_centros_de_costo", "cen_id=99999"
            CboCentroCosto.ListIndex = 0
            CboCentroCosto.Visible = False
            SkinLabel14(5).Visible = False
        Else
            FrameContable.Visible = True
        End If
        
        If RsTmp!emp_areas = "NO" Then
            LLenarCombo CboArea, "are_nombre", "are_id", "par_areas", "are_id=99999"
            CboArea.ListIndex = 0
            SkinLabel14(2).Visible = False
            
            CboArea.Visible = False
        Else
            FrameContable.Visible = True
        End If
        
        If RsTmp!emp_cuenta = "NO" Then
            LLenarCombo CboCuenta, "pla_nombre", "pla_id", "con_plan_de_cuentas", "pla_id=99999"
            If RsTmp!emp_cuenta_ventas > 0 Then
                LLenarCombo CboCuenta, "pla_nombre", "pla_id", "con_plan_de_cuentas", "pla_id=" & RsTmp!emp_cuenta_ventas
            End If
            If CboCuenta.ListCount = 0 Then
                CboCuenta.AddItem 0
            End If
            CboCuenta.ListIndex = 0
            Bm_Cuenta = False
            CboCuenta.Visible = False
            cmdCuenta.Visible = False
            TxtNroCuenta.Visible = False
            
            
            
        Else
            FrameContable.Visible = True
            If RsTmp!emp_cuenta_ventas > 0 Then
                LLenarCombo CboCuenta, "pla_nombre", "pla_id", "con_plan_de_cuentas", "pla_id=" & RsTmp!emp_cuenta_ventas
            End If
            
            Busca_Id_Combo CboCuenta, Val(RsTmp!emp_cuenta_ventas)
                
            If CboCuenta.ListCount > 0 Then CboCuenta.ListIndex = 0
          If SP_Rut_Activo = "76.369.600-6" Or SP_Rut_Activo = "11.500.319-4" Then FrameContable.Visible = True
            
        End If
      '  If RsTmp!emp_cuenta = "NO" Then
      '      cmdCuenta.Visible = False
      '      LLenarCombo CboCuenta, "pla_nombre", "pla_id", "con_plan_de_cuentas", "pla_id=99999"
      '      CboCuenta.ListIndex = 0
      '      CboCuenta.Visible = False
      '  End If
    End If
    
    If RsTmp!datoscontables = "NO" Then
        FrameContable.Visible = False
    Else
        FrameContable.Visible = True
    End If
   
   
    '2 Abril 2012 Seleccionamos documento de venta por defecto
    If DG_ID_Unico = 0 Then
    'If Val(TxtNroDocumento) = 0 Then
    
        Sql = "SELECT par_valor " & _
              "FROM tabla_parametros " & _
              "WHERE par_id=200"
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
            If CboDocVenta.ListCount > 0 Then Busca_Id_Combo CboDocVenta, Val(RsTmp!par_valor)
        End If
    End If
    '---------------------------------------------------------/
    Me.CboGuiasRef.AddItem "GUIA ELECTRONICA"
    Me.CboGuiasRef.ItemData(CboGuiasRef.ListCount - 1) = 52
    Me.CboGuiasRef.AddItem "GUIA MANUAL"
    Me.CboGuiasRef.ItemData(CboGuiasRef.ListCount - 1) = 50
    Me.CboGuiasRef.AddItem "ORDEN DE COMPRA"
    Me.CboGuiasRef.ItemData(CboGuiasRef.ListCount - 1) = 801
    Me.CboGuiasRef.AddItem "NOTA DE PEDIDO"
    Me.CboGuiasRef.ItemData(CboGuiasRef.ListCount - 1) = 802
    Me.CboGuiasRef.AddItem "LIQUIDACION"
    Me.CboGuiasRef.ItemData(CboGuiasRef.ListCount - 1) = 103
    
    Bm_BuscarReferencia = True
    BuscaRefencia
    
    
    
    
    If SP_Rut_Activo = "76.230.764-2" Then
        Me.CmdIvaConstructoras.Visible = True
    End If
    
    
    '19 Abril 2016 _
    Solo para t y t
    If SP_Rut_Activo = "11.907.734-6" Then
        Me.UpDown1.Visible = True
        Me.UpDown2.Visible = True
        Me.UpDown3.Visible = True
        Me.UpDown4.Visible = True
        Me.UpDown5.Visible = True
    End If
    
    
End Sub
Private Sub CargaDocumento(Optional ByVal Solo_Exento As String)
    If DG_ID_Unico > 0 Then
        EditarVD DG_ID_Unico, Solo_Exento
       ', VDdoc 'Extraemos los datos de la FACTURA o BOLETA
  '  ElseIf EstadoOT = "VentaDirecta" Then
     '   paso = UltimaFACTURAOBOLETA("FACTURA")
     '   paso = UltimaFACTURAOBOLETA("BOLETA")
    End If

End Sub
Private Sub LLenaDocumentosDeVenta(Sm_FiltroDocVentas As String)

'    If SG_Equipo_Solo_Nota_de_Venta = "SI" Then
'
'        TxtNroDocumento.Locked = True
'        TxtNroDocumento = "(Automatico)"
'        'El equipo esta configurado solo para emitir notas de venta
'        LLenarCombo CboDocVenta, "doc_nombre", "doc_id", "sis_documentos", "doc_activo='SI' AND doc_documento='VENTA' AND doc_utiliza_en_caja='" & SG_Funcion_Equipo & "' AND doc_nota_de_venta='SI'", "doc_id"
'
'  '      CboDocVenta.ListIndex = 0
'    Else
        If Len(Sm_FiltroDocVentas) = 0 Then
                
                If SP_Control_Inventario = "SI" Then
                    If DG_ID_Unico > 0 Then
                        LLenarCombo CboDocVenta, "doc_nombre", "doc_id", "sis_documentos", "doc_activo='SI' AND doc_documento='VENTA' AND  doc_utiliza_en_caja='" & SG_Funcion_Equipo & "' " & Sm_FiltroDocVentas, "doc_id"
                    Else
                    
                        LLenarCombo CboDocVenta, "doc_nombre", "doc_id", "sis_documentos", "doc_activo='SI' AND doc_documento='VENTA' AND doc_nota_de_credito='NO' AND  doc_utiliza_en_caja='" & SG_Funcion_Equipo & "' " & Sm_FiltroDocVentas, "doc_id"
                  '      LLenarCombo CboDocVenta, "doc_nombre", "doc_id", "sis_documentos", "doc_activo='SI' AND doc_documento='VENTA' AND  doc_utiliza_en_caja='" & SG_Funcion_Equipo & "' " & Sm_FiltroDocVentas, "doc_id"
                    End If
                Else
                    LLenarCombo CboDocVenta, "doc_nombre", "doc_id", "sis_documentos", "doc_boleta_venta='NO' AND doc_activo='SI' AND doc_documento='VENTA' AND doc_contable='SI' AND doc_nota_de_credito='NO' AND  doc_utiliza_en_caja='" & SG_Funcion_Equipo & "' " & Sm_FiltroDocVentas, "doc_id"
                    LvMateriales.Visible = False
                End If
        Else
                LLenarCombo CboDocVenta, "doc_nombre", "doc_id", "sis_documentos", "doc_activo='NO' AND doc_documento='VENTA' AND doc_nota_de_credito='NO' AND doc_utiliza_en_caja='" & SG_Funcion_Equipo & "' " & Sm_FiltroDocVentas, "doc_id"
        
        End If
   ' End If
    If CboDocVenta.ListCount > 0 Then CboDocVenta.ListIndex = 0
End Sub

Private Sub Form_Unload(Cancel As Integer)
    SG_codigo = ""
    SG_codigo2 = ""
End Sub

Private Sub LvGuiasReferencia_DblClick()
    If LvGuiasReferencia.SelectedItem Is Nothing Then Exit Sub
    Busca_Id_Combo Me.CboGuiasRef, Val(Me.LvGuiasReferencia.SelectedItem.SubItems(5))
    Me.TxtNroGuiaRef = Me.LvGuiasReferencia.SelectedItem.SubItems(2)
    Me.DtGuiaRef = Me.LvGuiasReferencia.SelectedItem.SubItems(3)
    Me.LvGuiasReferencia.ListItems.Remove Me.LvGuiasReferencia.SelectedItem.Index
End Sub

Private Sub LvMateriales_Click()
    On Error Resume Next
    If LvMateriales.SelectedItem Is Nothing Then Exit Sub
    With LvMateriales
        Busca_Id_Combo CboCuenta, .SelectedItem.SubItems(17)
        Busca_Id_Combo CboArea, .SelectedItem.SubItems(18)
        Busca_Id_Combo CboCentroCosto, .SelectedItem.SubItems(19)
    End With
End Sub

Private Sub LvMateriales_DblClick()
 '   ordListView ColumnHeader, Me, LvMateriales
    If LvMateriales.SelectedItem Is Nothing Then Exit Sub
   ' If Not FrameMA.Enabled Then Exit Sub
    With LvMateriales
        TxtCodigo = .SelectedItem.SubItems(1)
        TxtMarca = .SelectedItem.SubItems(2)
        Me.FrmDescripcion.Visible = True
        Me.skDescripcionLarga = .SelectedItem.SubItems(3)
        TxtDescrp = .SelectedItem.SubItems(3)
        TxtPrecio = CDbl(.SelectedItem.SubItems(4))
        Me.Text1 = 0 '.SelectedItem.SubItems(5)
        Me.Text1 = .SelectedItem.SubItems(5)
        Me.TxtCantidad = .SelectedItem.SubItems(6)
        txtPUni = .SelectedItem.SubItems(7)
        Me.TxtSubTotal = .SelectedItem.SubItems(8)
     '   Me.DtSalidaMaterial.Value = .SelectedItem.SubItems(14)
        Busca_Id_Combo CboCuenta, .SelectedItem.SubItems(17)
        Busca_Id_Combo CboArea, .SelectedItem.SubItems(18)
        Busca_Id_Combo CboCentroCosto, .SelectedItem.SubItems(19)
        If TxtPrecio.Enabled Then TxtPrecio.SetFocus
        
    End With
    On Error Resume Next
    TxtCodigo.SetFocus
    
End Sub









Private Sub LvSinDetalle_DblClick()
    If LvSinDetalle.SelectedItem Is Nothing Then Exit Sub
    With LvSinDetalle.SelectedItem
        Busca_Id_Combo CboCuenta, .SubItems(9)
        Busca_Id_Combo CboArea, .SubItems(10)
        Busca_Id_Combo CboCentroCosto, .SubItems(11)
        TxtSdExento = .SubItems(6)
        TxtSDNeto = .SubItems(7)
        TxtSdIva = .SubItems(8)
        LvSinDetalle.ListItems.Remove .Index
        
        If CboArea.ListIndex = -1 Then CboArea.ListIndex = 0
        If CboCentroCosto.ListIndex = -1 Then CboCentroCosto.ListIndex = 0
    End With
    CalculaTotal
End Sub



Private Sub SkBrutoMateriales_GotFocus()
    En_Foco SkBrutoMateriales
End Sub

Private Sub SkBrutoMateriales_KeyPress(KeyAscii As Integer)
        KeyAscii = AceptaSoloNumeros(KeyAscii)
End Sub


Private Sub SkBrutoMateriales_Validate(Cancel As Boolean)
    If Val(SkBrutoMateriales) = 0 Then
        SkBrutoMateriales = 0
    Else
        SkTotalMateriales = Round(CDbl(SkBrutoMateriales) / Val("1." & DG_IVA))
        SkIvaMateriales = CDbl(SkBrutoMateriales) - CDbl(SkTotalMateriales)
        SkBrutoMateriales = NumFormat(SkBrutoMateriales)
        SkTotalMateriales = NumFormat(SkTotalMateriales)
        SkIvaMateriales = NumFormat(SkIvaMateriales)
        Me.CmdEmiteDocumento.SetFocus
    End If
End Sub

Private Sub SkIdReferencia_DragDrop(Source As Control, X As Single, Y As Single)

End Sub

Private Sub SkIvaMateriales_KeyPress(KeyAscii As Integer)
        KeyAscii = AceptaSoloNumeros(KeyAscii)
End Sub


Private Sub SkTotalMateriales_GotFocus()
    En_Foco SkTotalMateriales
End Sub

Private Sub SkTotalMateriales_KeyPress(KeyAscii As Integer)
    KeyAscii = AceptaSoloNumeros(KeyAscii)
End Sub


Private Sub SkTotalMateriales_Validate(Cancel As Boolean)
    If Val(SkTotalMateriales) = 0 Then
        SkTotalMateriales = 0
    Else
        SkBrutoMateriales = Round(CDbl(SkTotalMateriales) * Val("1." & DG_IVA))
        SkIvaMateriales = CDbl(SkBrutoMateriales) - CDbl(SkTotalMateriales)
        SkBrutoMateriales = NumFormat(SkBrutoMateriales)
        SkTotalMateriales = NumFormat(SkTotalMateriales)
        SkIvaMateriales = NumFormat(SkIvaMateriales)
    End If
End Sub


Private Sub Timer1_Timer()
    On Error GoTo ErrorTimer
    Timer1.Enabled = False
    If Not bm_NoTimer Then Exit Sub
    If Fococantidad = True Then
        Me.TxtCantidad.SetFocus
        Fococantidad = False
    End If
    If Fococodigo = True Then
        Me.TxtCodigo.SetFocus
        Fococodigo = False
    End If
    
    For Each CTEXTOMO In Controls
        If (TypeOf CTEXTOMO Is TextBox) Then
            If Me.ActiveControl.Name = CTEXTOMO.Name Then 'Foco activo
                CTEXTOMO.BackColor = IIf(CTEXTOMO.Locked, ClrDesha, ClrCfoco)
            Else
                CTEXTOMO.BackColor = IIf(CTEXTOMO.Locked, ClrDesha, ClrSfoco)
            End If
        End If
    Next
    
    'Ell timer controla la habilitacion de del boton guardar siempre y
    'cuando los campos tengan algo
    If EstadoOT = "VentaDirecta" Then
        If Me.CboVendedores.ListIndex = -1 Or _
           Me.LvMateriales.ListItems.Count = 0 Then
            
        Else
            
        End If
        
    
    Else 'Aqui estamos trabanjando con Ordenes de trabajo
    End If
    Exit Sub
ErrorTimer:
    'no se que pasa pero da un error
        
End Sub

Private Sub Timer2_Timer()
    On Error Resume Next
    If DG_ID_Unico > 0 Then
        CmdSalir.SetFocus
    Else
        If Sp_EmpresaRepuestos = "SI" Then
            TxtCodigo.SetFocus
        Else
            DtFecha.SetFocus
        End If
    End If
    Timer2.Enabled = False
End Sub


Private Sub TxtCantidad_Change()
    'Me.TxtSubTotal.Text = NumFormat(Round(Val(Me.TxtCantidad.Text) * Val(TxtPrecio.Text), 0))
    TxtSubTotal.Text = NumFormat(Val(Me.TxtCantidad.Text) * Val(CxP(TxtPrecio)))
End Sub

Private Sub txtCantidad_GotFocus()
    En_Foco TxtCantidad
    If CboDocVenta.ListIndex = -1 Then Exit Sub
    If CboDocVenta.ItemData(CboDocVenta.ListIndex) <> IG_id_Nota_Debito_Clientes And _
     CboDocVenta.ItemData(CboDocVenta.ListIndex) <> IG_id_Nota_Debito_Clientes Then
    
        TxtStock.Visible = True
    End If
End Sub

Private Sub txtCantidad_KeyPress(KeyAscii As Integer)
    'KeyAscii = AceptaSoloNumeros(KeyAscii)
    
    If IsNumeric(Chr(KeyAscii)) Or KeyAscii = 8 Or KeyAscii = 44 Or KeyAscii = 46 Then  'pas
    Else
        If KeyAscii = 13 Then
            On Error Resume Next
          '  CmdAgregarProducto.SetFocus
            SendKeys "{TAB}" 'envia un tab
        ElseIf KeyAscii = 46 Then 'pase
        Else
            KeyAscii = 0 'No acepta el caracter ingresado
        End If
    End If
    If KeyAscii = 39 Then KeyAscii = 0
    
End Sub



Private Sub TxtCantidad_LostFocus()
    TxtStock.Visible = False
End Sub

'Private Sub TxtCantidad_LostFocus()
'    TxtStock.Visible = False
'End Sub

Private Sub TxtCantidad_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    TxtCantidad.ToolTipText = "Stock Actual = " & TxtStock
End Sub



Private Sub TxtCodigo_DblClick()
    If Sm_Bitacora_Productos = "SI" Then
        'Cargaremos bitacora de productos
        Sql = "SELECT  c.codigo,p.descripcion " & _
                "FROM    ven_detalle c " & _
                "INNER JOIN maestro_productos p ON c.codigo = p.codigo " & _
                "WHERE MONTH(fecha)=" & IG_Mes_Contable & " AND YEAR(fecha)=" & IG_Ano_Contable & " AND pro_activo='SI' AND " & _
                "p.rut_emp = '" & SP_Rut_Activo & "' AND c.rut_emp='" & SP_Rut_Activo & "' " & _
                "GROUP BY    c.codigo " & _
                "ORDER BY SUM(unidades) DESC " & _
                "LIMIT 5"
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
            'Mostraremos Bitocara
            Inv_BitacoraProductos.Show 1
            If Len(SG_codigo) > 0 Then
                TxtCodigo = SG_codigo
            End If
        End If
    End If
End Sub

Private Sub TxtCodigo_GotFocus()
    'on error
    En_Foco TxtCodigo
    
End Sub



Private Sub TxtCodigo_KeyPress(KeyAscii As Integer)
    On Error Resume Next
    If KeyAscii = 13 Then SendKeys ("{TAB}")
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
    If KeyAscii = 39 Then KeyAscii = 0
End Sub



Private Sub TxtCodigo_KeyUp(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF1 Then CmdBuscaProducto_Click
End Sub

Private Sub TxtCodigo_LostFocus()
    On Error Resume Next
    If Len(TxtCodigo.Text) > 0 Then
        If CodigoEncontrado Then Me.TxtPrecio.SetFocus
    End If
End Sub

Private Sub TxtCodigo_Validate(Cancel As Boolean)
    Dim Sp_FiltroCI As String
    If Len(TxtCodigo.Text) = 0 Then Exit Sub
    CodigoEncontrado = False
     Me.FrmDescripcion.Visible = False
    Filtro = "codigo = '" & Me.TxtCodigo.Text & "' "
    Sql = ""
    
    '26 agosto 2016 02.04am
    'En el caso que sea pachipap, saltarse _
    a la etiqueta pachipap
    
    If SP_Rut_Activo = "76.263.763-4" Then GoTo Pachipap
    
    
    If Bm_Cuenta Then
        
        If Sm_UtilizaCodigoInterno = "SI" Then
                Sp_FiltroCI = "pro_codigo_interno='" & TxtCodigo & "' "
                Sql = "SELECT codigo, pro_inventariable,descripcion,marca,ubicacion_bodega, " & _
                        "IF((SELECT lst_id FROM maestro_clientes WHERE rut_cliente='" & TxtRut & "')=0 " & _
                         "OR ISNULL((SELECT lst_id FROM maestro_clientes WHERE rut_cliente='" & TxtRut & "')) ,precio_venta," & _
                        "IFNULL((SELECT lsd_precio FROM par_lista_precios_detalle d WHERE lst_id=" & Val(TxtListaPrecio.Tag) & " AND m.id=d.id),precio_venta)) precio_venta," & _
                        "0,'',p.pla_id, " & _
                        "IFNULL((SELECT AVG(pro_ultimo_precio_compra) FROM pro_stock s WHERE s.rut_emp='" & SP_Rut_Activo & "' AND s.pro_codigo=m.codigo),0) precio_compra,ume_nombre,pro_iva_anticipado ,pro_iva_anticipado_codigo " & _
                    "FROM maestro_productos m " & _
                    "INNER JOIN par_tipos_productos t USING(tip_id) " & _
                    "INNER JOIN con_plan_de_cuentas p USING(pla_id) " & _
                    "INNER JOIN sis_unidad_medida d ON m.ume_id=d.ume_id " & _
                    "WHERE pro_activo='SI' AND t.rut_emp='" & SP_Rut_Activo & "' AND  m.rut_emp='" & SP_Rut_Activo & "' AND " & Sp_FiltroCI & " UNION "
         
                    
            
        End If
        
        Sql = Sql & "SELECT codigo, pro_inventariable,descripcion,marca,ubicacion_bodega, " & _
                "IF((SELECT lst_id FROM maestro_clientes WHERE rut_cliente='" & TxtRut & "')=0 " & _
                 "OR ISNULL((SELECT lst_id FROM maestro_clientes WHERE rut_cliente='" & TxtRut & "')) ,precio_venta," & _
                "IFNULL((SELECT lsd_precio FROM par_lista_precios_detalle d WHERE lst_id=" & Val(TxtListaPrecio.Tag) & " AND m.id=d.id),precio_venta)) precio_venta," & _
                "0,'',p.pla_id, " & _
                "IFNULL((SELECT AVG(pro_ultimo_precio_compra) FROM pro_stock s WHERE s.rut_emp='" & SP_Rut_Activo & "' AND s.pro_codigo=m.codigo),0) precio_compra,ume_nombre,pro_iva_anticipado,pro_iva_anticipado_codigo  " & _
              "FROM maestro_productos m " & _
              "INNER JOIN par_tipos_productos t ON m.tip_id=t.tip_id " & _
              "INNER JOIN con_plan_de_cuentas p USING(pla_id) " & _
              "INNER JOIN sis_unidad_medida d ON m.ume_id=d.ume_id " & _
              "WHERE pro_activo='SI' AND t.rut_emp='" & SP_Rut_Activo & "' AND  m.rut_emp='" & SP_Rut_Activo & "' AND " & Filtro & " LIMIT 1"
    Else
    
        If Sm_UtilizaCodigoInterno = "SI" Then
            Sp_FiltroCI = "pro_codigo_interno='" & TxtCodigo & "' "
            Sql = "SELECT codigo,pro_inventariable,descripcion,marca,ubicacion_bodega," & _
                    "IFNULL((SELECT lst_id FROM par_asociacion_lista_precios WHERE rut_emp='" & SP_Rut_Activo & "' AND cli_rut='" & TxtRut & "'),0)=0,precio_venta," & _
                    "IFNULL((SELECT lsd_precio FROM par_lista_precios_detalle d WHERE lst_id=" & Val(TxtListaPrecio.Tag) & " AND m.id=d.id),precio_venta) precio_venta," & _
                    "0,'',99999 pla_id," & _
                    "IFNULL((SELECT AVG(pro_ultimo_precio_compra) FROM pro_stock s WHERE s.rut_emp='" & SP_Rut_Activo & "' AND s.pro_codigo=m.codigo),0) precio_compra," & _
                    "pro_iva_anticipado_codigo,ume_nombre,pro_iva_anticipado  " & _
              "FROM maestro_productos m " & _
              "INNER JOIN par_tipos_productos t ON m.tip_id=t.tip_id " & _
              "INNER JOIN sis_unidad_medida d ON m.ume_id=d.ume_id " & _
              "WHERE pro_activo='SI' AND m.rut_emp='" & SP_Rut_Activo & "' AND " & Sp_FiltroCI & " UNION "
        End If
            
        Sql = Sql & "SELECT codigo,pro_inventariable,descripcion,marca,ubicacion_bodega," & _
                "IFNULL((SELECT lst_id FROM par_asociacion_lista_precios WHERE rut_emp='" & SP_Rut_Activo & "' AND cli_rut='" & TxtRut & "'),0)=0,precio_venta," & _
                "IFNULL((SELECT lsd_precio FROM par_lista_precios_detalle d WHERE lst_id=" & Val(TxtListaPrecio.Tag) & " AND m.id=d.id),precio_venta) precio_venta," & _
                "0,'',99999 pla_id," & _
                "IFNULL((SELECT AVG(pro_ultimo_precio_compra) FROM pro_stock s WHERE s.rut_emp='" & SP_Rut_Activo & "' AND s.pro_codigo=m.codigo),0) precio_compra," & _
                "pro_iva_anticipado_codigo,ume_nombre,pro_iva_anticipado  " & _
          "FROM maestro_productos m " & _
          "INNER JOIN par_tipos_productos t USING(tip_id) " & _
          "INNER JOIN sis_unidad_medida d ON m.ume_id=d.ume_id " & _
          "WHERE pro_activo='SI' AND m.rut_emp='" & SP_Rut_Activo & "' AND " & Filtro & " LIMIT 1"

    End If
    
    
    Consulta RsTmp, Sql
    
    
Pachipap:
    proivA = 0
    proivacodigO = 0
    If SP_Rut_Activo = "76.263.763-4" Then
          '  Sql = "SELECT codigo,Nombre AS descripcion,Precio1 AS precio_venta,impto,' ' AS ubicacion_bodega,' ' AS marca," & _
                    "'NO' AS pro_inventariable,' ' AS ume_nombre,0 AS precio_compra,0 AS pro_iva_anticipado, 0 AS pro_iva_anticipado_codigo  " & _
                    "FROM maeprod " & _
                    "WHERE Codigo='00000" & TxtCodigo & "'"
             
                    If Mid(TxtCodigo, 1, 2) = 26 Then 'UNITARIOS
                        Sp_FiltroCI = "codigo=" & Val(Mid(TxtCodigo, 3, 5)) & " "
                    ElseIf Mid(TxtCodigo, 1, 2) = 25 And Len(TxtCodigo) > 2 Then 'PESABLES
                        
                        'Dp_Cantidad_Pesable = Mid(TxtCodigo, 8, 2) & "." & Mid(TxtCodigo, 10, 3)'
                         TxtCantidad = Mid(TxtCodigo, 8, 2) & "." & Mid(TxtCodigo, 10, 3)
                         TxtCodigo = Val(Mid(TxtCodigo, 3, 5))
                    End If
             
                    
            
             Sql = "SELECT codigo,Nombre AS descripcion,Precio1 AS precio_venta,impto,'NO' as pro_inventariable,'' as ubicacion_bodega," & _
                            "'' as marca,'' as ume_nombre,1 as precio_compra, 0 as pro_iva_anticipado,0 as pro_iva_anticipado_codigo " & _
                    "FROM maeprod " & _
                    "WHERE Codigo LIKE '%" & TxtCodigo & "'"
    
            ConsultaAccess RsTmp, Sql
            
            If RsTmp.RecordCount > 0 Then
                impto = RsTmp!impto
                If Trim(Mid(impto, 5)) = "ICA" Then
                    proivA = 5
                    proivacodigO = 18
                    
                    'Imp, Carne
                ElseIf Trim(Mid(impto, 5)) = "IHA" Then
                    'HARINA
                    
                    proivA = 12
                    proivacodigO = 19
                    
                
                ElseIf Trim(Mid(impto, 5)) = "ILA" Then
                    'BEBIDA
                    proivA = 10
                    proivacodigO = 17
                
                
                ElseIf Trim(Mid(impto, 5)) = "ILA1" Then
                    'CERVEZ VINO
                     proivA = 20.5
                    proivacodigO = 25
                
                ElseIf Trim(Mid(impto, 5)) = "ILA2" Then
                    'LICORES
                    proivA = 31.5
                    proivacodigO = 24
                End If
            End If
    
            
    End If
    
    
    
    
    
    With RsTmp
        DtSalidaMaterial.Value = Date
        If .RecordCount = 0 Then
            If Sm_PermiteCrearProductos = "SI" Then
                    Respuesta = MsgBox("C�digo no encontrado..." & Chr(13) & "�Crear nuevo producto?", vbYesNo + vbQuestion)
                    If Respuesta = 6 Then                'Crear nuevo
                        Fococantidad = False
                        Fococodigo = False
                        AccionProducto = 8
                        AgregarProducto.Bm_Nuevo = True
                        SG_codigo = Empty
                        AgregarProducto.Caption = "Nuevo Producto"
                        AgregarProducto.TxtCodigo.Text = Me.TxtCodigo.Text
                        
                        
                        If SG_Es_la_Flor = "SI" Then
                            AgregarProductoFlor.Show 1
                        Else
                             AgregarProducto.Show 1
                        End If
                         If Len(Me.TxtMarca) = 0 Then
                            Fococodigo = True
                            Fococantidad = False
                        Else
                            Fococodigo = False
                            Fococantidad = True
                        End If
                    Else
                        TxtCodigo.Text = "": TxtDescrp.Text = "": Me.TxtMarca.Text = "": TxtPrecio.Text = ""
                        Me.TxtCantidad.Text = "": TxtIDFamilia.Text = "": TxtNombreFamilia.Text = ""
                        Me.TxtCodigo.SetFocus
                        Fococodigo = True
                        SendKeys "+{TAB}" ' Shift TAB retrocede al control anterior segun el orden del tabindex
        
                    End If
            Else
                    MsgBox "Codigo No encontrado..." & vbNewLine & "Presione F1 para buscar ayuda...", vbInformation + vbOKOnly
                    TxtCodigo.Text = "": TxtDescrp.Text = "": Me.TxtMarca.Text = "": TxtPrecio.Text = ""
                    Me.TxtCantidad.Text = "": TxtIDFamilia.Text = "": TxtNombreFamilia.Text = ""
                    Me.TxtCodigo.SetFocus
                    Fococodigo = True
                    
                    SendKeys "+{TAB}" ' Shift TAB retrocede al control anterior segun el orden del tabindex
            End If
        Else
            'Codigo ingresado  encontrado
            If SP_Rut_Activo = "76.263.763-4" Then
                'pachiapap
            Else
            
                TxtCodigo = !Codigo
                proivA = Val("" & !pro_iva_anticipado)
                proivacodigO = Val("" & !pro_iva_anticipado_codigo)
            End If
            If CboCuenta.Locked = True Then Busca_Id_Combo CboCuenta, IIf(Bm_Cuenta, Lp_IdCuentaVentasPorDefecto, 99999)
            TxtCodigo.Tag = !pro_inventariable
            Me.FrmDescripcion.Visible = True
            If SG_Usuario_Ve_Pcosto = "SI" Then
                
                Me.skDescripcionLarga = !Descripcion & " / ($" & NumFormat(!precio_compra) & ".-)"
            Else
                Me.skDescripcionLarga = !Descripcion & " .-)"
            End If
            SkUbicacion = "" & !ubicacion_bodega
            Me.TxtDescrp.Text = QuitarAcentos(!Descripcion)
            Me.TxtMarca.Text = "" & !MARCA
            TxtMarca.Tag = !ume_nombre
            '''Comrobar centro costo para definir
            '''si es precio venta o precio compra
            'Verificamos el combro Centro de cstos
            '0 y 2 son precio venta -  1 precio compra
          '  TxtStock = "Stock Actual  : " & .Fields(8)
          
            If SP_Rut_Activo = "76.337.408-4" Or SP_Rut_Activo = "10.722.977-9" Or SP_Rut_Activo = "12.318.896-9" Then
                TxtPrecio = CxP("" & !precio_venta)
            Else
            
                TxtPrecio = "" & !precio_venta
            
            End If
            Sm_Precio_Original = "" & !precio_venta
            'TxtCantidad.ToolTipText = .Fields(8)
            'Me.TxtIDFamilia.Text = "" & !familia
            'Me.TxtNombreFamilia = "" & !nombre_familia
            Me.TxtPcosto = !precio_compra
             
                     
            TxtProIvaAnticipado = proivA
            TxtCodigoSIIILA = proivacodigO
            
            Fococantidad = True
           CodigoEncontrado = True
           TxtStock = 0
            
            Sql = "SELECT sto_stock stock " & _
                  "FROM  pro_stock " & _
                  "WHERE rut_emp='" & SP_Rut_Activo & "' AND pro_codigo='" & TxtCodigo & "' AND bod_id=" & IG_id_Bodega_Ventas & " " & _
                  "GROUP BY pro_codigo"
            Consulta RsTmp2, Sql
            If RsTmp2.RecordCount > 0 Then
                TxtStock = IIf(IsNull(RsTmp2!stock), 0, RsTmp2!stock)
                If Sm_Avisa_Sin_Stock = "SI" And RsTmp!pro_inventariable = "SI" Then
                    If Val(TxtStock) < 1 Then
                        If MsgBox("Sin stock (" & TxtStock & ")" & vbNewLine & " �Continuar?...", vbQuestion + vbYesNo) = vbNo Then
                            TxtCodigo = Empty
                            TxtCodigo.Tag = Empty
                            TxtDescrp = Empty
                            TxtMarca = Empty
                            TxtPrecio = 0
                            TxtPcosto = 0
                            TxtIDFamilia = Empty
                            TxtNombreFamilia = Empty
                            Exit Sub
                        End If
                    End If
                End If
            End If
            
          '  Sql = "SELECT pro_ultimo_precio_compra costo " & _
          '        "FROM  pro_stock " & _
          '        "WHERE rut_emp='" & SP_Rut_Activo & "' AND  pro_codigo='" & TxtCodigo & "' " & _
          '        "LIMIT 1"
          ''  Consulta RsTmp, Sql
           ' If RsTmp.RecordCount > 0 Then TxtPcosto = IIf(IsNull(RsTmp!costo), 0, RsTmp!costo)
            
            
        End If
    
    End With
    
    
End Sub



Private Sub TxtComentario_GotFocus()
    En_Foco TxtComentario
End Sub

Private Sub TxtComentario_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub











Private Sub TxtCreditoEmpresaConstructora_GotFocus()
    En_Foco TxtCreditoEmpresaConstructora
End Sub

Private Sub TxtCreditoEmpresaConstructora_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
End Sub

Private Sub TxtDscto_GotFocus()
    En_Foco TxtDscto
End Sub

Private Sub TxtDscto_KeyPress(KeyAscii As Integer)
    KeyAscii = AceptaSoloNumeros(KeyAscii)
    If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtDscto_Validate(Cancel As Boolean)
    TxtNuevoTotal = 0
    TxtTotalDescuento = 0
    Me.SkTotalMateriales = 0
    Me.CompruebaCambioCliente
    Me.SumaListaMaterialesYMobra
End Sub


Private Sub txtespecial_KeyPress(Index As Integer, KeyAscii As Integer)
    If Len(txtespecial(Index)) >= 45 Then SendKeys ("{TAB}")
End Sub




Private Sub txtIvaRetenido_GotFocus()
    En_Foco TxtIVARetenido
End Sub


Private Sub TxtIVARetenido_KeyPress(KeyAscii As Integer)
    KeyAscii = AceptaSoloNumeros(KeyAscii)
End Sub


Private Sub txtIvaRetenido_Validate(Cancel As Boolean)
    If Val(TxtIVARetenido) = 0 Then
        TxtIVARetenido = 0
    Else
        TxtIVARetenido = NumFormat(TxtIVARetenido)
    End If
    TotalBruto
    

End Sub








Private Sub TxtNeto_Change()
    SkTotalMateriales = TxtNeto
End Sub

Private Sub TxtNeto_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
    If KeyAscii = 39 Then KeyAscii = 0
End Sub


Private Sub TxtNetoSG_KeyPress(KeyAscii As Integer)
    KeyAscii = AceptaSoloNumeros(KeyAscii)
End Sub

Private Sub TxtNroCuenta_GotFocus()
    En_Foco TxtNroCuenta
End Sub
Private Sub TxtNroCuenta_KeyPress(KeyAscii As Integer)
    KeyAscii = AceptaSoloNumeros(KeyAscii)
End Sub
Private Sub TxtNroCuenta_Validate(Cancel As Boolean)
    If Val(TxtNroCuenta) = 0 Then Exit Sub
    Busca_Id_Combo CboCuenta, Val(TxtNroCuenta)
    If CboCuenta.ListIndex = -1 Then
        MsgBox "Cuenta no existe..."
    End If
End Sub
Private Sub TxtNroDocumento_GotFocus()
    En_Foco TxtNroDocumento
End Sub

Private Sub TxtNroDocumento_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
'    If KeyAscii = 13 Then SendKeys ("{TAB}")
    If KeyAscii = 39 Then KeyAscii = 0
End Sub





Private Sub TxtNroDocumento_Validate(Cancel As Boolean)
    CompruebaFolioRango
End Sub
Private Sub CompruebaFolioRango()
    '17 12 2016 Comprobar si el rango de la fac electronica esta dentro del rango
    If CboDocVenta.ListIndex > -1 And Val(TxtNroDocumento) > 0 Then
        Sql = "SELECT doc_cod_sii,doc_dte " & _
                "FROM sis_documentos " & _
                "WHERE doc_id=" & CboDocVenta.ItemData(CboDocVenta.ListIndex)
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
            If RsTmp!doc_dte = "SI" Then
                Sql = "SELECT dte_disponible " & _
                        "FROM dte_folios " & _
                        "WHERE rut_emp='" & SP_Rut_Activo & "' AND doc_id=" & RsTmp!doc_cod_sii & " AND dte_folio=" & TxtNroDocumento
                Consulta RsTmp, Sql
                If RsTmp.RecordCount > 0 Then
                    If RsTmp!dte_disponible = "NO" Then
                        MsgBox "Folio ingresado esta fuera del rango autorizado ...", vbExclamation
                        TxtNroDocumento = 0
                    End If
                Else
                    MsgBox "Folio ingresado esta fuera del rango autorizado ...", vbExclamation
                    TxtNroDocumento = 0
                End If
            End If
        End If
    
    End If
End Sub

Private Sub TxtNuevoTotal_GotFocus()
    En_Foco TxtNuevoTotal
End Sub

Private Sub TxtNuevoTotal_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
    If KeyAscii = 39 Then KeyAscii = 0
End Sub


Private Sub TxtNuevoTotal_Validate(Cancel As Boolean)
    Dim Lp_Diferencias As Long
    Dim Dp_Descuento As Double
    If Val(TxtNuevoTotal) = 0 Then Exit Sub
    Lp_Diferencias = CDbl(SkBrutoMateriales) - CDbl(TxtNuevoTotal)
    If Lp_Diferencias > 0 Then
        'descuento hacia abajo
            Dp_Descuento = Lp_Diferencias / CDbl(SkBrutoMateriales) * 100
            Me.TxtDscto = Dp_Descuento
            TxtDscto_Validate (True)
        
    ElseIf Lp_Diferencias < 0 Then
            Dp_Descuento = Lp_Diferencias / CDbl(SkBrutoMateriales) * 100
            Me.TxtDscto = Dp_Descuento
            TxtDscto_Validate (True)
    
    
    End If
    

End Sub


Private Sub TxtOrdenesCompra_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub


Private Sub TxtPrecio_Change()
     Me.TxtSubTotal.Text = Val(Me.TxtCantidad.Text) * Val(CxP(TxtPrecio.Text))
End Sub

Private Sub TxtPrecio_GotFocus()
    En_Foco TxtPrecio
    If CboDocVenta.ListIndex = -1 Then Exit Sub
    If CboDocVenta.ItemData(CboDocVenta.ListIndex) = IG_id_Nota_Debito_Clientes Or _
      CboDocVenta.ItemData(CboDocVenta.ListIndex) = IG_id_Nota_Credito_Clientes Then
        txtInfoPrecio.Visible = True
        txtInfoPrecio = "Ingrese la diferencia del precio"
    End If
    If TxtPrecio.Locked Then
        TxtCantidad.SetFocus
    End If
End Sub


Private Sub TxtPrecio_KeyPress(KeyAscii As Integer)
    KeyAscii = AceptaSoloNumeros(KeyAscii)
    If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtPrecio_LostFocus()
    txtInfoPrecio.Visible = False
End Sub


Private Sub TxtPrecio_Validate(Cancel As Boolean)

    If Val(TxtPrecio) = 0 Then TxtPrecio = 0
    
    
     If SP_Rut_Activo = "76.337.408-4" Or SP_Rut_Activo = "10.722.977-9" Or SP_Rut_Activo = "12.318.896-9" Or SP_Rut_Activo = "15.565.714-6" Or SG_Decimales = "SI" Then
        'Suyai y JTraipi
        TxtPrecio = CxP(TxtPrecio)
     Else
     
         TxtPrecio = Format(TxtPrecio, "#,0")
       
        TxtPrecio = Format(Abs(CDbl(TxtPrecio)))
    End If
    GoTo finsub
    If SP_Rut_Activo <> "78.967.170-2" Then
            'Verificar con atributos de perfiles
            If Mid(TxtPrecio.Tag, 1, 2) = "SI" Then
                If CDbl(TxtPrecio) < Sm_Precio_Original Then
                    'Descuento
                    precio_minimo = Sm_Precio_Original - ((Sm_Precio_Original / 100) * Val(Mid(TxtPrecio.Tag, 4, 2)))
                    If CDbl(TxtPrecio) < precio_minimo Then
                        MsgBox "Precio excede m�nimo...", vbExclamation
                        TxtPrecio = NumFormat(Sm_Precio_Original)
                    End If
                Else
                    'Recargo
                     precio_max = Sm_Precio_Original + (Sm_Precio_Original / 100 * Val(Mid(TxtPrecio.Tag, 4, 2)))
                    If CDbl(TxtPrecio) > precio_max Then
                        MsgBox "Precio excede m�ximo...", vbExclamation
                        TxtPrecio = NumFormat(Sm_Precio_Original)
                    End If
                
               End If
            End If
    End If
   
finsub:
    
End Sub



Private Sub TxtRut_GotFocus()
    En_Foco TxtRut
End Sub

Private Sub TxtRut_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
    On Error Resume Next
    If KeyAscii = 13 Then SendKeys ("{TAB}")
    If KeyAscii = 39 Then KeyAscii = 0
    
End Sub

Private Sub TxtRut_KeyUp(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF1 Then CmdBuscaCliente_Click
End Sub

Private Sub TxtRut_LostFocus()
    If Len(TxtRut.Text) = 0 Then
        Me.TxtRazonSocial.Text = ""
        Me.TxtDireccion.Text = ""
        Me.TxtCiudad.Text = ""
        Me.TxtGiro.Text = ""
        Me.TxtComuna.Text = ""
        Me.TxtDscto.Text = ""
        Exit Sub
    End If
    If ClienteEncontrado Then
        BuscaRefencia
    Else
       ' TxtRut.SetFocus
    End If
    If SP_Control_Inventario = "SI" Then Me.SumaListaMaterialesYMobra Else CalculaTotal
End Sub

Private Sub TxtRut_Validate(Cancel As Boolean)
    Dim iP_RS As Integer
    TxtListaPrecio = "NORMAL"
    TxtListaPrecio.Tag = 0
    TxtRazonSocial.Tag = ""
    If Len(TxtRut.Text) = 0 Then Exit Sub
    If CboDocVenta.ListIndex = -1 Then
        MsgBox "Seleccione documento...", vbInformation
        On Error Resume Next
        CboDocVenta.SetFocus
        Exit Sub
    End If
    TxtRut.Text = Replace(TxtRut.Text, ".", "")
    TxtRut.Text = Replace(TxtRut.Text, "-", "")
    Respuesta = VerificaRut(TxtRut.Text, NuevoRut)
    CboSucursal.Clear
    If Respuesta = True Then
        Me.TxtRut.Text = NuevoRut
        'Buscar el cliente
        Sql = "SELECT rut_cliente,nombre_rsocial,direccion direccion_,ciudad,comuna,giro,descuento descuento_,fono,email,anticipo," & _
                "IFNULL(lst_nombre,'LISTA PRECIO GENERAL') listaprecios,m.lst_id,ven_id,cli_monto_credito,cli_bloqueado,cli_motivo_bloqueo,cli_contacto3 " & _
              "FROM maestro_clientes m " & _
              "INNER JOIN par_asociacion_ruts  a ON m.rut_cliente=a.rut_cli " & _
              "LEFT JOIN par_lista_precios l USING(lst_id) " & _
              "WHERE habilitado='SI' AND a.rut_emp='" & SP_Rut_Activo & "' AND rut_cliente = '" & Me.TxtRut.Text & "'"
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
                'Cliente encontrado
            With RsTmp
                Lp_MontoCreditoAprobado = !cli_monto_credito
                'solo para tt
                If SP_Rut_Activo = "77.680.920-9" Then
                    Busca_Id_Combo CboPlazos, Val("" & !anticipo)
                End If
                sql2 = "SELECT pla_requiere_validacion valida " & _
                        "FROM par_plazos_vencimiento " & _
                        "WHERE pla_id=" & Val(Right(CboPlazos.Text, 10))
                Consulta RsTmp2, sql2
                If RsTmp2!valida = "SI" Then
                    If !cli_monto_credito = 0 Then
                        If MsgBox("El cliente seleccionado no esta habilitado para ventas al credito..." & vbNewLine & _
                            "�Ver ficha ahora...?", vbQuestion + vbYesNo) = vbNo Then
                            TxtRut = ""
                            Exit Sub
                        Else
                            SG_codigo = TxtRut
                            AgregoCliente.Show 1
                            Sql = "SELECT cli_monto_credito " & _
                                    "FROM maestro_clientes " & _
                                    "WHERE rut_cliente='" & TxtRut & "'"
                            Consulta RsTmp3, Sql
                            Lp_MontoCreditoAprobado = RsTmp3!cli_monto_credito
                        End If
                    End If
                    If !cli_bloqueado = "SI" Then
                       MsgBox "El cliente seleccionado esta bloqueado por:" & vbNewLine & vbnewlien & !cli_motivo_bloqueo & "..." & vbNewLine & _
                            ".....", vbInformation
                            TxtRut = ""
                            Exit Sub
                    End If
                End If
                TxteMail = "" & !Email
                TxtListaPrecio = !ListaPrecios
                TxtListaPrecio.Tag = !lst_id
                TxtRut.Text = !rut_cliente
                TxtBrutoNetoTYT = "" & !cli_contacto3
                TxtRazonSocial.Text = !nombre_rsocial
                TxtRazonSocial.Tag = "" & !Email
                TxtDireccion.Text = "" & !direccion_
                TxtDireccion.Tag = TxtDireccion
                TxtCiudad.Text = "" & !ciudad
                TxtCiudad.Tag = TxtCiudad
                
                TxtCiudad.Tag = "" & !ciudad
                TxtComuna.Text = "" & !comuna
                TxtComuna.Tag = TxtComuna
                TxtGiro.Text = "" & !giro
              '  TxtDscto.Text = "" & !Descuento_
                LbTelefono.Caption = "" & !fono
                ClienteEncontrado = True
                If ClienteEncontrado = True Then
                    CompruebaCambioCliente
                    'Call SaldoCliente(TxtRut, TxtRazonSocial)
                End If
                Sql = "SELECT a.lst_id,lst_nombre " & _
                      "FROM par_asociacion_lista_precios  a " & _
                      "INNER JOIN par_lista_precios l USING(lst_id) " & _
                      "WHERE l.rut_emp='" & SP_Rut_Activo & "' AND cli_rut='" & TxtRut & "'"
                Consulta RsTmp2, Sql
                TxtListaPrecio.Tag = !lst_id
                TxtListaPrecio = "LISTA DE PRECIOS PRINCIPAL"
                If RsTmp2.RecordCount > 0 Then
                    TxtListaPrecio = RsTmp2!lst_nombre
                    TxtListaPrecio.Tag = RsTmp2!lst_id
                End If
                If Sm_SoloVendedorAsignado = "SI" Then
                    Busca_Id_Combo CboVendedores, Val(!ven_id)
                End If
                LLenarCombo CboSucursal, "CONCAT(suc_ciudad,' ',suc_direccion,' ',suc_contacto)", "suc_id", "par_sucursales", "suc_activo='SI' AND rut_cliente='" & TxtRut & "'", "suc_id"
                CboSucursal.AddItem "CASA MATRIZ"
                CboSucursal.ItemData(CboSucursal.ListCount - 1) = 0
                CboSucursal.ListIndex = CboSucursal.ListCount - 1
                CboGiros.Clear
                LLenarCombo CboGiros, "gir_nombre", "gir_id", "par_clientes_giros", "cli_rut='" & TxtRut & "'"
                CboGiros.AddItem !giro, 0
                Busca_Id_Combo CboGiros, 0
                CboRs.Clear
                LLenarCombo CboRs, "rso_nombre", "rso_id", "par_razon_social_clientes", "rut_cliente='" & Me.TxtRut & "'"
                If CboRs.ListCount > 0 Then
                   CboRs.Visible = True
                   CboRs.ListIndex = 0
                Else
                    CboRs.Visible = False
                End If
                If bm_SoloVistaDoc Then Exit Sub
                Sql = "SELECT doc_id " & _
                      "FROM sis_documentos " & _
                      "WHERE doc_id=" & CboDocVenta.ItemData(CboDocVenta.ListIndex) & " AND doc_id IN(" & SP_Facturas_De_Ventas & ")"
                Consulta RsTmp, Sql
                If RsTmp.RecordCount > 0 Then
                    'Consultamos las guias por facturar
                    Sql = "SELECT id " & _
                             "FROM ven_doc_venta " & _
                             "WHERE rut_emp='" & SP_Rut_Activo & "' AND doc_id=" & CboDocVenta.ItemData(CboDocVenta.ListIndex) & " AND no_documento=" & TxtNroDocumento
                    Consulta RsTmp, Sql
                    If RsTmp.RecordCount > 0 Then
                        MsgBox "Nro de documento ya fue utilizado...", vbInformation
                        Exit Sub
                    End If
                    SG_codigo2 = "SELECT id,fecha,no_documento,v.rut_cliente,nombre_cliente,neto,iva,bruto," & _
                            "IF(v.suc_id=0,'CM',CONCAT(suc_ciudad,' - ',suc_direccion)) sucursal, " & _
                            "v.suc_id,doc_nombre " & _
                          "FROM ven_doc_venta v " & _
                          "INNER JOIN par_sucursales s USING(suc_id) " & _
                          "JOIN sis_documentos d ON v.doc_id=d.doc_id " & _
                          "WHERE tipo_movimiento<>'NULA' AND rut_emp='" & SP_Rut_Activo & "' AND nro_factura=0 AND v.rut_cliente='" & TxtRut & "' AND v.doc_id IN(" & IG_id_GuiaClientes & ")"
                    Consulta RsTmp, SG_codigo2
                    If RsTmp.RecordCount > 0 Then
                        If MsgBox("Cliente tiene guias por facturar..." & vbNewLine & "Desea facturarlas?", vbOKCancel + vbQuestion) = vbCancel Then Exit Sub
                        With vta_Facturacion_Guias
                            iP_RS = 0
                            If CboRs.Visible Then
                                If CboRs.ListIndex > -1 Then
                                    .I_ID_Rs = CboRs.ItemData(CboRs.ListIndex)
                                End If
                            End If
                            SG_Presentacion_Factura = Empty
                            CboSucursal.Clear
                            LLenarCombo .CboSucursal, "CONCAT(suc_ciudad,' ',suc_direccion,' ',suc_contacto)", "suc_id", "par_sucursales", "suc_activo='SI' AND rut_cliente='" & TxtRut & "'", "suc_id"
                            .CboSucursal.AddItem "CASA MATRIZ"
                            .CboSucursal.ItemData(.CboSucursal.ListCount - 1) = 0
                            .CboSucursal.AddItem "TODAS"
                            .CboSucursal.ListIndex = .CboSucursal.ListCount - 1
                            
                            .S_NombreVendedor = CboVendedores.Text
                            .D_Fecha = DtFecha.Value
                            .L_Plazo = CboPlazos.ItemData(CboPlazos.ListIndex)
                            .I_Doc_Id = CboDocVenta.ItemData(CboDocVenta.ListIndex)
                            .L_NDoc = TxtNroDocumento
                            .S_rut = TxtRut
                            .S_Nombre = TxtRazonSocial
                            .I_Id_Vendedor = CboVendedores.ItemData(CboVendedores.ListIndex)
                            .S_documento = CboDocVenta.Text
                            vta_Facturacion_Guias.Show 1
                            If S_Facturado = "SI" Then 'FACUTRACION DE GUIAS
                                 Dim Bp_Empresa_Fac_Electronica As Boolean
                                 Bp_Empresa_Fac_Electronica = False
                               ' For s = 1 To Principal.LvFacturaElectronica.ListItems.Count
                               '     If SP_Rut_Activo = Principal.LvFacturaElectronica.ListItems(s) Then
                               '         If Principal.LvFacturaElectronica.ListItems(s).SubItems(1) = "SI" Then
                                            Bp_Empresa_Fac_Electronica = True
                              '              Exit For
                              '              End If
                              '      End If
                              '  Next
                                If MsgBox(CboDocVenta.Text & " Nro " & TxtNroDocumento & " Guardado correctamente" & vbNewLine & "(Los Doc. Electronicos se generan en PDF)" & vbNewLine & "�Imprimir? ", vbYesNo + vbQuestion) = vbYes Then
                                    Bm_Factura_por_Guias = True
                                    If Bp_Empresa_Fac_Electronica Then
                                        'ALVAMAR
                                        'consultamos si factura electronicamente
                                        sql2 = "SELECT   doc_dte,doc_nota_de_credito,doc_cod_sii,(SELECT emp_factura_electronica " & _
                                                                                                    "FROM sis_empresas " & _
                                                                                                    "WHERE rut='" & SP_Rut_Activo & "') dte " & _
                                                "FROM sis_documentos " & _
                                                "WHERE doc_id=" & CboDocVenta.ItemData(CboDocVenta.ListIndex)
                                        Consulta RsTmp2, sql2
                                        If RsTmp2.RecordCount > 0 Then
                                            If RsTmp2!Dte = "SI" And RsTmp2!doc_dte = "SI" Then
                                                FraProcesandoDTE.Visible = True
                                                DoEvents
                                                sql2 = "SELECT id,'',no_documento,fecha,v.doc_id,doc_cod_sii " & _
                                                            "FROM ven_doc_venta v " & _
                                                            "JOIN sis_documentos d ON v.doc_id=d.doc_id " & _
                                                            "WHERE nro_factura=" & TxtNroDocumento & " AND doc_id_factura=" & Me.CboDocVenta.ItemData(CboDocVenta.ListIndex) & " AND rut_emp='" & SP_Rut_Activo & "'"
                                                Consulta RsTmp3, sql2
                                                If RsTmp2.RecordCount > 0 Then
                                                    LLenar_Grilla RsTmp3, Me, Me.LvGuiasReferencia, False, False, True, False
                                                End If
                                                FacturaElectronica RsTmp2!doc_cod_sii
                                                FraProcesandoDTE.Visible = False
                                            Else
                                                If SP_Rut_Activo = "76.417.727-4" Then
                                                    'alvamar factura normal
                                                    ImprimeFactura
                                                ElseIf SP_Rut_Activo = "96.803.210-0" Then
                                                    'vegamodelo factura normal preimpresa manual
                                                     ImprimeFacturaVegaModelo
                                                ElseIf SP_Rut_Activo = "xxx" Then
                                                   ' Me.impi
                                                 ElseIf SP_Rut_Activo = "76.155.097-7" Or SP_Rut_Activo = "76.532.135-2" Then ' anccay
                                                    ImprimeFacturaAnncay
                                                ElseIf SP_Rut_Activo = "11.500.319-4" Then
                                                    ImprimeFacturaPerlaDelSur
                                                Else
                                                    ImprimeFactura
                                                End If
                                                
                                            End If
                                        Else
                                            Dialogo.CancelError = True
                                            On Error GoTo CancelaImpesionFacturacion
                                            
                                            Dialogo.ShowPrinter
                                            'AQUI INCLUIR FACTURACION ELECTRONICA
                                            'KERAMIK
                                            
                                            If SP_Rut_Activo = "76.095.156-0" Then
                                                ImprimeFactura76095156 'Cimexpor
                                            ElseIf SP_Rut_Activo = "76.155.097-7" Or SP_Rut_Activo = "76.532.135-2" Then
                                                ImprimeFacturaAnncay 'Anncay
                             
                                            ElseIf SP_Rut_Activo = "76.244.196-9" Then
                                                ImprimeFacturaMaqmin 'Maqmin
                           
                                            ElseIf SP_Rut_Activo = "11.500.319-4" Then
                                                'Perla del sur
                                                ImprimeFacturaPerlaDelSur
                                            ElseIf SP_Rut_Activo = "76.169.962-8" Then
                                                'Alcalde
                                                ImprimeFacturaAlcalde
                                            
                                            ElseIf SP_Rut_Activo = "7.539.285-0" Then
                                                'Alcalde
                                                ImprimeFacturaJoseMoreno
                                            
        
                                            Else
                                                ImprimeFactura
                                            End If
                                        End If
                                    End If
                                End If
                            End If
                            SG_codigo2 = Empty
                        End With
                    End If
                Else
                    
                End If
            End With
        Else
                'Cliente no existe aun �crearlo??
                Respuesta = MsgBox("Cliente no encontrado..." & Chr(13) & _
                "     �Desea Crear?    ", vbYesNo + vbQuestion)
                If Respuesta = 6 Then
                    'crear
                    SG_codigo = ""
                    AccionCliente = 4
                    AgregoCliente.TxtRut.Text = Me.TxtRut.Text
                    
                    AgregoCliente.TxtRut.Locked = True
                    ClienteEncontrado = False
                    AgregoCliente.Timer1.Enabled = True
            
                    AgregoCliente.Show 1
                    If ClienteEncontrado = True Then
                        TxtRut_Validate False
                        CompruebaCambioCliente
                        TxtCodigo.SetFocus
                    Else
                        TxtRut_Validate True
                    End If
                Else
                    'volver al rut
                    ClienteEncontrado = False
                    Me.TxtRut.Text = ""
                    On Error Resume Next
                    SendKeys "+{TAB}" ' Shift TAB retrocede al control anterior segun el orden del tabindex
                End If
        
            End If
       
    Else
        Me.TxtRut.Text = ""
        SendKeys "+{TAB}" ' Shift TAB retrocede al control anterior segun el orden del tabindex
    End If
    Exit Sub
CancelaImpesionFacturacion:
    'No imprime
    Unload Me
End Sub

Public Sub LimpiaMaterial()
    CuentaLineas
    txtPUni = Empty
    Me.TxtCodigo.Text = ""
    Me.TxtMarca.Text = ""
    Me.TxtCantidad.Text = ""
    Me.TxtDescrp.Text = ""
    Me.TxtStock.Text = ""
    Me.TxtPrecio.Text = ""
    TxtMarca.Tag = ""
    Me.FrmDescripcion.Visible = False
    If TxtCodigo.Visible Then TxtCodigo.SetFocus
End Sub

Public Sub CompruebaCambioCliente()
       '  Me.TxtPatente.SetFocus
       
      '  RecalculaGrillaMateriales
        SumaListaMaterialesYMobra
        
End Sub
Private Sub RecalculaGrillaMateriales()
            Dim Codigo As String * 8
            Dim MARCA As String * 11
            Dim Descripcion As String * 35
            Dim Descuento As String * 8
            Dim Unidades As String * 4
            Dim Precio As String * 9
            Dim PrecioRef As String * 9
            Dim SubTotal As String * 10
            Dim NuevoDscto As String
            NuevoDscto = Me.TxtDscto.Text
            
            With LvMateriales
                If .ListItems.Count > 0 Then
                    For i = 1 To .ListItems.Count
                        Codigo = .ListItems(i).SubItems(1)
                        PrecioRef = CDbl(.ListItems(i).SubItems(4))
                        Unidades = .ListItems(i).SubItems(6)
                        Sql = "SELECT * " & _
                              "FROM maestro_productos " & _
                              "WHERE  rut_emp='" & SP_Rut_Activo & "' AND codigo='" & Codigo & "'"
                        Call Consulta(RsTmp, Sql)
                        If RsTmp.RecordCount > 0 Then
                            
                            PrecioRef = RsTmp!precio_venta
                            
                        End If
                        
                                RSet Descuento = "0"
                        
                                RSet Descuento = Val(PrecioRef) - (Val(PrecioRef) - (Val(PrecioRef) / 100 * Val(NuevoDscto)))
                        
                        Precio = Val(PrecioRef) - Val(Descuento)
                        RSet SubTotal = Format(Val(Precio) * Val(Unidades), "#,##0")
                        
                        .ListItems(i).SubItems(4) = Format(PrecioRef, "#,##0")
                        .ListItems(i).SubItems(5) = Format(Descuento, "#,##0")
                        .ListItems(i).SubItems(7) = Format(Precio, "#,##0")
                        .ListItems(i).SubItems(8) = SubTotal
                        .ListItems(i).SubItems(15) = SubTotal - CDbl(.ListItems(i).SubItems(10))
                    Next
                End If
            End With
End Sub




Public Sub SumaListaMaterialesYMobra(Optional ByVal B_RefExento As String)
    Dim Lp_Neto_tmp As Double
    Dim Sp_RefSoloExento As String * 2
    Dim d_TotalCosto_Materiales As Double
'Right(ListaMateriales.List(ListaMateriales.ListIndex), 10)


    '19-11-2012
    'Para el calculo de los valores totales, debemos tener claro
    'Si los precios seran brutos (iva incluido), netos (mas iva)
    'o simplemente exentos, donde solo habra valor exento y bruto.
    
    'Tipo calculo
    '------------
    '1.Bruto
    '2.Neto
    '3.Exento


    If SP_Control_Inventario = "NO" Then
        CalculaTotal
        Exit Sub
    End If

    Dim Sumando As String
    Dim Sumando2 As Double
    Dim SCosto As String
    Dim CRepuestos As Double
    Dim DescuentosAcum As String
    Dim PorcentajeMO As Double
    Dim PorcentajeMA As Double
    Dim CostoComision As Double
    Dim NetoComision As Double
    Dim i As Integer
    If SP_Control_Inventario = "NO" Then Exit Sub
    
      Sp_RefSoloExento = "NO"
    If B_RefExento <> Empty Then
        Sp_RefSoloExento = B_RefExento
    End If
    
    
    Sumando = 0
    Me.SkTotalMateriales = 0
    Me.TxtTotalDescuento.Text = 0
 '   Me.TxtExentos = 0
    
    'Calculo de articulos
    With LvMateriales
        If .ListItems.Count > 0 Then
            DescuentosAcum = 0
            CRepuestos = 0
            
            If Val(CxP(TxtDscto)) > 0 Then
                'Cuando el % desuento del cliente es > 0 realizar descuento a todas las lineas
                '7 Nov 2013
                txtOriginal = 0
                For i = 1 To .ListItems.Count
                        Precio = CDbl(LvMateriales.ListItems(i).SubItems(4))
                        txtOriginal = Val(txtOriginal) + (Precio * Val(LvMateriales.ListItems(i).SubItems(6)))
                        DescuentoTemp = CxP(TxtDscto)
                        Descuento = Precio - (Val(Precio) - (Val(Precio) / 100 * Val(DescuentoTemp)))
                        Descuento = Format(Descuento, "###,#0")
                        Precio = Val(Precio) - (Val(Precio) / 100 * Val(DescuentoTemp))
                        '30-7-2009 REEMPLAZO LINEA PARA DSCTO
                        SubTotal = NumFormat(Val(.ListItems(i).SubItems(6)) * Precio)
                        
                        
                        .ListItems(i).SubItems(7) = NumFormat(Precio)
                        .ListItems(i).SubItems(5) = Descuento
                        .ListItems(i).SubItems(8) = SubTotal
                        
                        
                Next
                
                
         '       If SP_Rut_Activo = "11.907.734-6" Then
                    'Sandra ramirez, descuento al sub total
                 '   TxtSubTotalMateriales = NumFormat(Val(txtOriginal) + (Precio * Val(LvMateriales.ListItems(i).SubItems(6))))
          '          TxtTotalDescuento = NumFormat(TxtSubTotalMateriales - (CDbl(TxtSubTotalMateriales) - (CDbl(TxtSubTotalMateriales) / 100 * Val(DescuentoTemp))))
          '      End If
                
                
            ElseIf Val(CxP(TxtDscto)) < 0 Then
                'Recargo
                'Cuando el % desuento del cliente es < 0 realizar descuento a todas las lineas
                '17 Mayo 2014
                txtOriginal = 0
                For i = 1 To .ListItems.Count
                        Precio = CDbl(LvMateriales.ListItems(i).SubItems(4))
                        txtOriginal = Val(txtOriginal) + (Precio * Val(LvMateriales.ListItems(i).SubItems(6)))
                        DescuentoTemp = CxP(TxtDscto)
                        Descuento = Precio - (Val(Precio) - (Val(Precio) / 100 * Abs(Val(DescuentoTemp))))
                        Descuento = Format(Descuento, "###,#0")
                        Precio = Val(Precio) + (Val(Precio) / 100 * Abs(Val(DescuentoTemp)))
                        '30-7-2009 REEMPLAZO LINEA PARA DSCTO
                        SubTotal = NumFormat(Val(.ListItems(i).SubItems(6)) * Precio)
                        
                        
                        .ListItems(i).SubItems(7) = NumFormat(Precio)
                        .ListItems(i).SubItems(5) = Descuento
                        .ListItems(i).SubItems(8) = SubTotal
                Next
                
            ElseIf Val(CxP(TxtDscto)) = 0 Then
                'Recargo
                'Cuando el % desuento del cliente es < 0 realizar descuento a todas las lineas
                '17 Mayo 2014
                txtOriginal = 0
                For i = 1 To .ListItems.Count
                        If LvMateriales.ListItems(i).SubItems(4) = "" Then LvMateriales.ListItems(i).SubItems(4) = 0
                        If SP_Rut_Activo = "76.337.408-4" Or SP_Rut_Activo = "10.722.977-9" Or SP_Rut_Activo = "12.318.896-9" Or SG_Decimales = "SI" Then
                            Precio = LvMateriales.ListItems(i).SubItems(4)
                            txtOriginal = NumFormat(Val(txtOriginal) + (Val(Precio) * Val(LvMateriales.ListItems(i).SubItems(6))))
                        Else
                        
                            Precio = CDbl(LvMateriales.ListItems(i).SubItems(4))
                            txtOriginal = NumFormat(Val(txtOriginal) + (Precio * Val(LvMateriales.ListItems(i).SubItems(6))))
                        End If
                   
                        DescuentoTemp = 0
                        If Val(LvMateriales.ListItems(i).SubItems(5)) = 0 Then
                            Descuento = 0
                        Else
                            Descuento = LvMateriales.ListItems(i).SubItems(5)
                        End If
                        'Descuento = 0
                        Precio = Val(Precio) - Descuento
                        '30-7-2009 REEMPLAZO LINEA PARA DSCTO
                        SubTotal = NumFormat(Val(.ListItems(i).SubItems(6)) * Precio)
                        
                        
                        If SP_Rut_Activo = "76.337.408-4" Or SP_Rut_Activo = "10.722.977-9" Or SP_Rut_Activo = "12.318.896-9" Or SG_Decimales = "SI" Then
                            .ListItems(i).SubItems(7) = CxP(Precio)
                        Else
                            .ListItems(i).SubItems(7) = NumFormat(Precio)
                        End If
                        .ListItems(i).SubItems(5) = Descuento
                        .ListItems(i).SubItems(8) = SubTotal
                Next
                
                

            
            End If
            
            For i = 1 To .ListItems.Count
                Sumando = CDbl(.ListItems(i).SubItems(8))
                If Val(.ListItems(i).SubItems(10)) = 0 Then .ListItems(i).SubItems(10) = 0
                
                SCosto = CDbl(.ListItems(i).SubItems(10))
                If Val(.ListItems(i).SubItems(5)) = 0 Then .ListItems(i).SubItems(5) = "0"
                DescuentosAcum = DescuentosAcum + (CDbl(.ListItems(i).SubItems(5)) * Val(.ListItems(i).SubItems(6)))
                Me.SkTotalMateriales = Val(Me.SkTotalMateriales) + Val(Sumando)
                CRepuestos = CRepuestos + Val(SCosto)
        
            Next
            Me.TxtTotalDescuento.Text = NumFormat(DescuentosAcum)
            Me.TxtTotalDescuento.Text = Format(DescuentosAcum, "###,#0")
            
            

            
            
            If Im_Tcal = 0 Then Im_Tcal = 1
            
            Select Case Im_Tcal
                Case 1
                    If Me.LvMateriales.ListItems.Count = 1 Then
                      '  MsgBox "queda uno"
                    End If
                    'Brutos -++++++++++++++++++++++++++++++++++
                    SkBrutoMateriales = SkTotalMateriales
                    SkTotalMateriales = Round((CDbl(SkBrutoMateriales) - CDbl(TxtExentos)) / Val("1." & DG_IVA), 0)
                    Lp_Neto_tmp = (CDbl(SkBrutoMateriales) - CDbl(TxtExentos)) / Val("1." & DG_IVA)
                    Me.SkIvaMateriales = Round((Lp_Neto_tmp / 100 * DG_IVA), 3)
                    SkBrutoMateriales = NumFormat(SkBrutoMateriales)
                    SkTotalMateriales = NumFormat(SkTotalMateriales)
                    SkIvaMateriales = NumFormat(SkIvaMateriales)
                    'Brutos *_____________________________________
                Case 2
                    'Netos
                    Me.SkIvaMateriales = Round(Val(SkTotalMateriales) / 100 * DG_IVA, 0)
                    Me.SkBrutoMateriales = Format(Str(Val(Me.SkTotalMateriales) + Val(Me.SkIvaMateriales)), "###,##")
                    Me.SkTotalMateriales = Format(SkTotalMateriales, "###,#0")
                    Me.SkIvaMateriales = Format(Str(Val(Me.SkIvaMateriales)), "###,#0")
                Case 3
                    'Exentos
                    Me.TxtSubTotalMateriales.Text = Val(Replace(Me.SkTotalMateriales, ".", "")) + Val(Replace(Me.TxtTotalDescuento.Text, ".", ""))
                    Me.TxtSubTotalMateriales.Text = Format(Me.TxtSubTotalMateriales.Text, "###,##")
                    TxtExentos = NumFormat(SkTotalMateriales)
                    SkTotalMateriales = 0
                    SkIvaMateriales = 0
                    SkBrutoMateriales = NumFormat(TxtExentos)
            End Select
            
            'If Sm_Con_Precios_Brutos = "SI" And Sm_SoloExento = "NO" And Sp_RefSoloExento = "NO" Then
            '    SkBrutoMateriales = SkTotalMateriales
            '    SkTotalMateriales = Round((CDbl(SkBrutoMateriales) - CDbl(TxtExentos)) / Val("1." & DG_IVA), 0)
            ''    Lp_Neto_tmp = (CDbl(SkBrutoMateriales) - CDbl(TxtExentos)) / Val("1." & DG_IVA)
             '   Me.SkIvaMateriales = Round((Lp_Neto_tmp / 100 * DG_IVA), 3)
             '   SkBrutoMateriales = NumFormat(SkBrutoMateriales)
             '   SkTotalMateriales = NumFormat(SkTotalMateriales)
             '   SkIvaMateriales = NumFormat(SkIvaMateriales)
             '
           ' E 'lse
           '     Me.SkIvaMateriales = Round(Val(SkTotalMateriales) / 100 * DG_IVA, 0)
           '     Me.SkBrutoMateriales = Format(Str(Val(Me.SkTotalMateriales) + Val(Me.SkIvaMateriales)), "###,##")
           ''     Me.SkTotalMateriales = Format(SkTotalMateriales, "###,#0")
            '    Me.SkIvaMateriales = Format(Str(Val(Me.SkIvaMateriales)), "###,#0")
            '
            'End If
           ' 'Me.txtCostoRepuestos = CRepuestos
            
           ' If Sm_SoloExento = "SI" Or Sp_RefSoloExento = "SI" Then
          '      Me.TxtSubTotalMateriales.Text = Val(Replace(Me.SkTotalMateriales, ".", "")) + Val(Replace(Me.TxtTotalDescuento.Text, ".", ""))
          '      Me.TxtSubTotalMateriales.Text = Format(Me.TxtSubTotalMateriales.Text, "###,##")
          '      TxtExentos = Me.SkTotalMateriales
          '      SkTotalMateriales = 0
          '      SkIvaMateriales = 0
          '      SkBrutoMateriales = TxtExentos
          '  ElseIf Sm_Factor_IVA = 0 Then
          '      SkTotalMateriales = NumFormat(CDbl(SkBrutoMateriales) - CDbl(SkIvaMateriales))
          '      SkIvaMateriales = 0
          ''      SkBrutoMateriales = SkTotalMateriales
           ' End If
        End If
        
        
        '7-11-15 _
        Calcularemos los montos de los IVA anticipados
        TxtIvaAnticipado = 0
        If Sm_Empresa_IVA_Anticipado = "SI" Then
            
            If SP_Rut_Activo = "77.328.790-2" Or SP_Rut_Activo = "76.263.763-4" Then
                '10Agosto2016
                'Santa Baraba, lleva ILAS pero vende con precios brutos, por lo que hay que calcular
                SkIvaAnticipado = "OTROS IMP."
            
                    For i = 1 To .ListItems.Count
                    
                        If SP_Rut_Activo = "76.263.763-4" Then
                            'pachipap
                            If DG_ID_Unico = 0 Then
                                Sql = "SELECT '" & proivA & "' AS pro_iva_anticipado," & proivacodigO & " AS pro_iva_anticipado_codigo "
                            Else
                                    Sql = "SELECT '" & 0 & "' AS pro_iva_anticipado," & 0 & " AS pro_iva_anticipado_codigo "
                            End If
                            
                        Else
                            Sql = "SELECT pro_iva_anticipado,pro_iva_anticipado_codigo " & _
                                    "FROM maestro_productos m " & _
                                    "WHERE rut_emp='" & SP_Rut_Activo & "' AND codigo='" & .ListItems(i).SubItems(1) & "'"
                        End If
                        Consulta RsTmp, Sql
                        If RsTmp.RecordCount > 0 Then
                            If RsTmp!pro_iva_anticipado > 0 Then
                                .ListItems(i).SubItems(23) = RsTmp!pro_iva_anticipado_codigo
                                'Calcular monto del IVA ANTICIPADO
                                .ListItems(i).SubItems(22) = Round(CDbl(.ListItems(i).SubItems(8)) / Val("1." & Replace((DG_IVA + RsTmp!pro_iva_anticipado), ",", "")), 0)
                                .ListItems(i).SubItems(24) = .ListItems(i).SubItems(22)
                                .ListItems(i).SubItems(22) = Round(CDbl(.ListItems(i).SubItems(22)) / 100 * RsTmp!pro_iva_anticipado, 0)
                                .ListItems(i).SubItems(26) = Round(CDbl(.ListItems(i).SubItems(8)) - CDbl(.ListItems(i).SubItems(22)) - CDbl(.ListItems(i).SubItems(24)))
                            Else
                                .ListItems(i).SubItems(23) = 0
                                .ListItems(i).SubItems(22) = 0
                            End If
                            
                        End If
                        TxtIvaAnticipado = CDbl(TxtIvaAnticipado) + CDbl(.ListItems(i).SubItems(22))
                        
                    Next
                    
                    'Recalcular Neto
                    'IVA
                    'ILAS
                    'Bruto
                    SkBrutoMateriales = 0
                    SkTotalMateriales = 0 'NETO
                    SkIvaMateriales = 0
                    For i = 1 To .ListItems.Count
                    '    if val(.li
                        SkBrutoMateriales = CDbl(SkBrutoMateriales) + CDbl(.ListItems(i).SubItems(8))
                        If Val(.ListItems(i).SubItems(22)) = 0 Then
                            SkTotalMateriales = CDbl(SkTotalMateriales) + (CDbl(.ListItems(i).SubItems(8)) / Val("1." & DG_IVA))
                            SkIvaMateriales = CDbl(SkIvaMateriales) + (CDbl(.ListItems(i).SubItems(8)) - (CDbl(.ListItems(i).SubItems(8)) / Val("1." & DG_IVA)))
                        Else
                            SkTotalMateriales = CDbl(SkTotalMateriales) + CDbl(.ListItems(i).SubItems(24))
                            SkIvaMateriales = CDbl(SkIvaMateriales) + CDbl(.ListItems(i).SubItems(26))
                        End If
                        
            
                    Next
                    SkIvaMateriales = NumFormat(SkIvaMateriales)
                    SkBrutoMateriales = NumFormat(SkBrutoMateriales)
                    SkTotalMateriales = NumFormat(SkTotalMateriales)
            
            Else
                'Molineras
        
        
        
                    For i = 1 To .ListItems.Count
                        Sql = "SELECT pro_iva_anticipado,pro_iva_anticipado_codigo " & _
                                "FROM maestro_productos m " & _
                                "WHERE rut_emp='" & SP_Rut_Activo & "' AND codigo='" & .ListItems(i).SubItems(1) & "'"
                        Consulta RsTmp, Sql
                        If RsTmp.RecordCount > 0 Then
                            If RsTmp!pro_iva_anticipado > 0 Then
                                .ListItems(i).SubItems(23) = RsTmp!pro_iva_anticipado_codigo
                                'Calcular monto del IVA ANTICIPADO
                                .ListItems(i).SubItems(22) = Round(CDbl(.ListItems(i).SubItems(8)) / 100 * RsTmp!pro_iva_anticipado, 0)
                                
                            Else
                                .ListItems(i).SubItems(23) = 0
                                .ListItems(i).SubItems(22) = 0
                            End If
                            
                        End If
                        TxtIvaAnticipado = CDbl(TxtIvaAnticipado) + CDbl(.ListItems(i).SubItems(22))
                        
                        
                    Next
                    SkBrutoMateriales = NumFormat(CDbl(SkBrutoMateriales) + CDbl(TxtIvaAnticipado))
            End If
            
        End If
       
        
        
        
    End With
    
    
   
   ' If Sm_SoloExento = "SI" Then
   '
'
'    Else
'
        Me.TxtSubTotalMateriales.Text = Val(Replace(Me.SkTotalMateriales, ".", "")) + Val(Replace(Me.TxtTotalDescuento.Text, ".", ""))
       Me.TxtSubTotalMateriales.Text = Format(Me.TxtSubTotalMateriales.Text, "###,##")
  '  End If
     
'        If SP_Rut_Activo = "11.907.734-6" Then
                'Sandra ramirez, descuento al sub total
             '   TxtSubTotalMateriales = NumFormat(Val(txtOriginal) + (Precio * Val(LvMateriales.ListItems(i).SubItems(6))))
                
              '  If SP_Rut_Activo = "11.907.734-6" Then
                    'Sandra ramirez, descuento al sub total
                 '   TxtSubTotalMateriales = NumFormat(Val(txtOriginal) + (Precio * Val(LvMateriales.ListItems(i).SubItems(6))))
   
             '   End If
                
             
 '               TxtSubTotalMateriales = 0
 '               For i = 1 To LvMateriales.ListItems.Count
 '                   TxtSubTotalMateriales = TxtSubTotalMateriales + (CDbl(LvMateriales.ListItems(i).SubItems(4)) * Trim(Replace(LvMateriales.ListItems(i).SubItems(6), ",", ".")))
 '
 '               Next
 '               TxtSubTotalMateriales = NumFormat(TxtSubTotalMateriales)
                
                
 '               If Val(DescuentoTemp) > 0 Then
 '                   TxtTotalDescuento = NumFormat(CDbl(TxtSubTotalMateriales) - (CDbl(TxtSubTotalMateriales) - (CDbl(TxtSubTotalMateriales) / 100 * Val(DescuentoTemp))))
 '               End If
                
                
   '             SkTotalMateriales = NumFormat(CDbl(TxtSubTotalMateriales) - CDbl(TxtTotalDescuento))
   '             SkIvaMateriales = NumFormat(CDbl(SkTotalMateriales) / 100 * 19)
   '             SkBrutoMateriales = NumFormat(CDbl(SkTotalMateriales) + CDbl(SkIvaMateriales))
   '
  '      End If
    
    
    
        'If Mid(CboPlazos.Text, 1, 7) <> "CONTADO" Then
        '    If CDbl(Me.SkTotalMateriales) > Lp_MontoCreditoAprobado Then MsgBox "El monto autorizado para credito es " & NumFormat(Lp_MontoCreditoAprobado) & vbNewLine & "Contacte con administracion"
        'End If
        

End Sub




Public Sub ActualizaStock()
        Dim Dp_Promedio As Double
        With LvMateriales
            If .ListItems.Count > 0 Then
                For i = 1 To .ListItems.Count
                    If .ListItems(i).SubItems(1) <> "ZZZZZZZ1" Then
                    
                        If .ListItems(i).SubItems(16) = "SI" Then
                          
                              Sql = "UPDATE maestro_productos " & _
                                    "SET stock_Actual = stock_actual -" & CxP(.ListItems(i).SubItems(6)) & _
                                    " WHERE  rut_emp='" & SP_Rut_Activo & "' AND  codigo='" & .ListItems(i).SubItems(1) & "'"
                              cn.Execute Sql
                              
                         
                              Dp_Promedio = 0
                             
                              'Dp_Promedio = CostoAVG(.ListItems(i).SubItems(1), IG_id_Bodega_Ventas)
                              
                               Sql = "SELECT pro_precio_neto promedio " & _
                                        "FROM inv_kardex k " & _
                                        "WHERE pro_codigo='" & .ListItems(i).SubItems(1) & "' AND rut_emp='" & SP_Rut_Activo & "' AND bod_id=" & IG_id_Bodega_Ventas & " " & _
                                        "ORDER BY kar_id DESC " & _
                                         "LIMIT 1 "
                              Consulta RsTmp, Sql
                              If RsTmp.RecordCount > 0 Then Dp_Promedio = RsTmp!promedio
                              
                              
                              
                           
                              KardexVenta Format(Date, "YYYY-MM-DD"), "SALIDA", _
                              CboDocVenta.ItemData(CboDocVenta.ListIndex), TxtNroDocumento, IG_id_Bodega_Ventas, .ListItems(i).SubItems(1), _
                              Val(.ListItems(i).SubItems(6)), "VENTA " & CboDocVenta.Text & " " & TxtNroDocumento, _
                              Dp_Promedio, Dp_Promedio * Val(.ListItems(i).SubItems(6)), Me.TxtRut.Text, Me.TxtRazonSocial, , , CboDocVenta.ItemData(CboDocVenta.ListIndex), , , , Lp_Id_Nueva_Venta
                              '    Kardex Format(.ListItems(i).SubItems(14), "YYYY-MM-DD"), "SALIDA", _
                              CboDocVenta.ItemData(CboDocVenta.ListIndex), TxtNroDocumento, CboBodega.ItemData(CboBodega.ListIndex), .ListItems(i).SubItems(1), _
                              Val(.ListItems(i).SubItems(6)), "VENTA " & CboDocVenta.Text & " " & TxtNroDocumento, _
                              0, 0, Me.TxtRut.Text, Me.TxtRazonSocial, , , CboDocVenta.ItemData(CboDocVenta.ListIndex)
                        End If
                    End If
                Next
            End If
        End With
        
End Sub
Public Sub ActualizaNC()
    Dim lp_Promedio As Double
        With LvMateriales
            If .ListItems.Count > 0 Then
                For i = 1 To .ListItems.Count
                    If .ListItems(i).SubItems(1) <> "ZZZZZZZ1" Then
                        Sql = "UPDATE maestro_productos " & _
                              "SET stock_Actual = stock_actual -" & .ListItems(i).SubItems(6) & _
                              " WHERE  rut_emp='" & SP_Rut_Activo & "' AND  codigo='" & .ListItems(i).SubItems(1) & "'"
                        cn.Execute Sql
                        
                       '  = 0
                        lp_Promedio = CostoAVG(.ListItems(i).SubItems(1), IG_id_Bodega_Ventas)
                        
                        
                      ' MsgBox Val(CxP(Val(.ListItems(i).SubItems(6))))
                        Kardex Format(.ListItems(i).SubItems(14), "YYYY-MM-DD"), "ENTRADA", CboDocVenta.ItemData(CboDocVenta.ListIndex), _
                                       TxtNroDocumento, IG_id_Bodega_Ventas, .ListItems(i).SubItems(1), Val(CxP(Val(.ListItems(i).SubItems(6)))), _
                                       TxtMotivoRef & TxtDocReferencia & " " & TxtNroReferencia, lp_Promedio, lp_Promedio * CxP(Val(.ListItems(i).SubItems(6))), Me.TxtRut.Text, Me.TxtRazonSocial, "NO", 0, CboDocVenta.ItemData(CboDocVenta.ListIndex), , , , 0
                    End If
                Next
            End If
        End With
        '
End Sub

Function EditarVD(L_ID As Double, Optional ByVal B_SoloExento As String)
            Dim L_No_Doc As Long
            Dim Cod_Admin As String * 3
            Dim s_FacturaGuias As String * 2
            Dim Lp_Referencia As Long
            Dim Sp_Comentario As String
            Dim Ip_Suc_id As Integer
            Dim Sp_DRSoloExento As String * 2 'DR documento referenciado
            Dim Ip_GiroId As Integer, Ip_Rso As Integer
            Dim i As Integer
            
            Sp_DRSoloExento = "NO"
            
            If B_SoloExento <> Empty Then
                Sp_DRSoloExento = B_SoloExento
            End If
          
            
            
            If Not Bm_EditandoPorGuias Then
                Sql = "SELECT * " & _
                      "FROM ven_doc_venta " & _
                      "INNER JOIN sis_documentos d USING(doc_id) " & _
                      "WHERE id=" & L_ID
                      
            Else
                Sql = "SELECT * " & _
                      "FROM ven_doc_venta " & _
                      "INNER JOIN sis_documentos d USING(doc_id) " & _
                      "WHERE  rut_emp='" & SP_Rut_Activo & "' AND  doc_id=" & FrmReferencia.Tag & " AND no_documento=" & TxtNroReferencia
            End If
            Consulta RsTmp, Sql
            If RsTmp.RecordCount = 0 Then
                MsgBox "No se encontraron datos..."
                Exit Function
            Else
                CboRs.Visible = False
                Ip_Rso = RsTmp!rso_id
  
               ' Me.SkCantLineas.Tag = RsTmp!doc_cantidad_lineas
                If RsTmp!ven_venta_arriendo = "ARR" Then Me.CboModalidad.ListIndex = 1
                TxtCreditoEmpresaConstructora = "" & RsTmp!patente
                If Val(TxtCreditoEmpresaConstructora) > 0 Then
                    TxtCreditoEmpresaConstructora.Visible = True
                Else
                    TxtCreditoEmpresaConstructora.Visible = False
                End If
                DtFechaOC = "" & IIf(Len("" & RsTmp!doc_afecto_comision) = 0, DtFecha, RsTmp!doc_afecto_comision)
                Im_Tcal = RsTmp!ven_tipo_calculo
                Busca_Id_Combo CboVendedores, Val(RsTmp!ven_id)
                TxtComentario = RsTmp!ven_comentario
                Sm_Factor_IVA = RsTmp!doc_factor_iva
                TxtExentos = RsTmp!Exento
                Sm_NotaDeVenta = RsTmp!doc_nota_de_venta
                If Sm_NotaDeVenta = "SI" Then Frame1.Tag = RsTmp!ID
                Ip_Suc_id = RsTmp!suc_id
                Ip_GiroId = Val("" & RsTmp!gir_id)
                L_No_Doc = RsTmp!no_documento
                IP_Doc_ID = RsTmp!doc_id
                Ip_DocIndicio = RsTmp!doc_id_indicio
                Me.TxtOrdenesCompra = RsTmp!ven_ordendecompra
                If RsTmp!tnc_id > 0 Then
                    SkCodigoReferencia = RsTmp!tnc_id
                End If
             '   Me.TxtNroDocumento = L_No_Doc
             '   Busca_Id_Combo CboDocVenta, Val(IP_Doc_ID)
                
                s_FacturaGuias = RsTmp!doc_factura_guias
                If s_FacturaGuias = "SI" Then Bm_Factura_por_Guias = True Else Bm_Factura_por_Guias = False
                SkTotalMateriales.Tag = NumFormat(RsTmp!Neto)
                SkIvaMateriales = NumFormat(RsTmp!Iva)
                SkBrutoMateriales = NumFormat(RsTmp!bruto)
                TxtIVARetenido = NumFormat(RsTmp!ven_iva_retenido)
                Lp_Referencia = RsTmp!id_ref
                Sp_Comentario = "" & RsTmp!comentario
                
                
                
                If Val("" & RsTmp!ven_guia_tipo_despacho) > 0 Then
                    LLenarCombo CboTipoDespacho, "gel_detalle", "gel_codigo", "par_parametros_guias_electronicas", "gel_tipo=1 AND gel_activo='SI'"
                    CboTipoDespacho.ListIndex = 0
                    'Tipo de guia
                    Busca_Id_Combo CboTipoDespacho, Val(RsTmp!ven_guia_tipo_despacho)
                End If
                If Val("" & RsTmp!ven_guia_tipo_traslado) > 0 Then
                    'Guia traslasdo
                    LLenarCombo Me.CboTipoTraslado, "gel_detalle", "gel_codigo", "par_parametros_guias_electronicas", "gel_tipo=2 AND gel_activo='SI'"
                    CboTipoTraslado.ListIndex = 1
                    Busca_Id_Combo Me.CboTipoTraslado, Val(RsTmp!ven_guia_tipo_traslado)
                End If
                
                
                'Descuento desde el POS _
                16 Enero 2016
                Me.TxtTotalDescuento = Val("" & RsTmp!ven_descuento_valor) - Val("" & RsTmp!ven_ajuste_recargo) + Val("" & RsTmp!ven_ajuste_descuento)
                If Val(TxtTotalDescuento) > 0 Then
                    Me.TxtDscto = Round((Val(TxtTotalDescuento) / (CDbl(SkBrutoMateriales) + Val(TxtTotalDescuento))), 6) * 100
                    
                End If
                
                
             '   Busca_Id_Combo CboPlazos, RsTmp!ven_plazo_id
                
              '  If CboPlazos.ListIndex = -1 Then
                    '8 Ene 2014
                    'Ajuste para buscar plazo de pago, despues de algunos cambios con el id y la cantidad de dias
                    For i = 0 To CboPlazos.ListCount - 1
                        If Val(Right(CboPlazos.List(i), 6)) = RsTmp!ven_plazo_id Then
                            CboPlazos.ListIndex = i
                            Exit For
                        End If
                    Next
               ' End If
                If SP_Control_Inventario = "NO" Then
                    Busca_Id_Combo CboCuenta, RsTmp!pla_id
                    Busca_Id_Combo CboArea, RsTmp!are_id
                    Busca_Id_Combo CboCentroCosto, RsTmp!cen_id
                End If
                
            End If
            DtFecha.Value = RsTmp!Fecha
            Me.TxtRut.Text = RsTmp!rut_cliente
            
            Me.TxtRazonSocial.Text = "" & RsTmp!nombre_cliente
            'para completar los datos del cliente validar el RUT
            'y mostrara los datos del cliente
            
            If RsTmp!condicionpago = "CONTADO" Then
                CboFpago.ListIndex = 0
            Else
                CboFpago.Clear
                CboFpago.AddItem "CREDITO"
                CboFpago.ListIndex = 0
            End If
            
            
            Busca_Id_Combo CboVendedores, RsTmp!ven_id


            
            
            If IP_Doc_ID = 0 Then
            '    TxtNroDocumento.Tag = dpago
                Sql = "SELECT * FROM ven_detalle " & _
                      "WHERE  rut_emp='" & SP_Rut_Activo & "' AND  no_documento = " & L_No_Doc & " and doc_id =" & IP_Doc_ID
            Else
                If bm_SoloVistaDoc Then
                    Busca_Id_Combo CboDocVenta, Val(IP_Doc_ID)
                    TxtNroDocumento = L_No_Doc
                    TxtRut_Validate (True)
                    CboSucursal.Clear
                    LLenarCombo CboSucursal, "CONCAT(suc_ciudad,' ',suc_direccion,' ',suc_contacto)", "suc_id", "par_sucursales", "suc_activo='SI' AND rut_cliente='" & TxtRut & "'", "suc_id"
                    CboSucursal.AddItem "CASA MATRIZ"
                    CboSucursal.ItemData(CboSucursal.ListCount - 1) = 0
                    Busca_Id_Combo CboSucursal, Val(Ip_Suc_id)
                    Busca_Id_Combo CboGiros, Val(Ip_GiroId)
                    
                    If Ip_Suc_id > 0 Then
                        '12-11-2015
                        'Si la sucursal es >0 buscamos, de lo contrario queda lo principal
                            Sql = "SELECT suc_direccion,suc_ciudad,suc_contacto suc_comuna,gir_id " & _
                                "FROM par_sucursales " & _
                                "WHERE suc_id=" & Ip_Suc_id
                                
                            Consulta RsTmp2, Sql
                            If RsTmp2.RecordCount > 0 Then
                                    Busca_Id_Combo CboGiros, Val(0 & RsTmp2!gir_id)
                                    TxtDireccion = RsTmp2!suc_direccion
                                   
                                    TxtCiudad = RsTmp2!suc_ciudad
                            End If
                    End If
                    
                End If
                
                If Not Bm_EditandoPorGuias Then
                    Sql = "SELECT *,(SELECT ume_nombre " & _
                                        "FROM sis_unidad_medida s " & _
                                        "JOIN maestro_productos m ON m.ume_id=s.ume_id " & _
                                        "WHERE m.codigo=d.codigo AND m.rut_emp='" & SP_Rut_Activo & "' AND s.rut_emp='" & SP_Rut_Activo & "' ) ume_nombre, " & _
                                         "(SELECT pro_inventariable FROM maestro_productos x WHERE d.codigo=x.codigo AND x.rut_emp='" & SP_Rut_Activo & "') pro_inventariable  " & _
                            "FROM ven_detalle d " & _
                          "WHERE  rut_emp='" & SP_Rut_Activo & "' AND  no_documento = " & L_No_Doc & " and doc_id =" & IP_Doc_ID
                Else
                   
                    Sql = "SELECT *,(SELECT ume_nombre " & _
                                    "FROM sis_unidad_medida s " & _
                                    "JOIN maestro_productos m ON m.ume_id=s.ume_id " & _
                                    "WHERE m.codigo=d.codigo AND m.rut_emp='" & SP_Rut_Activo & "' AND s.rut_emp='" & SP_Rut_Activo & "' ) ume_nombre, " & _
                                    "(SELECT pro_inventariable FROM maestro_productos x WHERE d.codigo=x.codigo AND x.rut_emp='" & SP_Rut_Activo & "') pro_inventariable  " & _
                            "FROM ven_detalle d " & _
                          "WHERE  rut_emp='" & SP_Rut_Activo & "' AND CONCAT(CAST(doc_id AS CHAR),'-',CAST(no_documento AS CHAR)) IN (" & Sm_GuiasEditando & ")"
                    Consulta RsTmp, Sql
                    If RsTmp.RecordCount > 40 Then
                        Sql = "SELECT id,marca,codigo,descripcion,SUM(unidades) unidades,AVG(descuento) descuento,AVG(precio_real) precio_real," & _
                                "AVG(precio_final) precio_final,AVG(precio_costo) precio_costo,SUM(subtotal) subtotal,btn_especial," & _
                                "fecha,pla_id,cen_id,are_id,0 comision,(SELECT ume_nombre " & _
                                    "FROM sis_unidad_medida s " & _
                                    "JOIN maestro_productos m ON m.ume_id=s.ume_id " & _
                                    "WHERE m.codigo=d.codigo AND m.rut_emp='" & SP_Rut_Activo & "' AND s.rut_emp='" & SP_Rut_Activo & "' ) ume_nombre, " & _
                                    "(SELECT pro_inventariable FROM maestro_productos x WHERE d.codigo=x.codigo AND x.rut_emp='" & SP_Rut_Activo & "') pro_inventariable  " & _
                            "FROM ven_detalle d " & _
                          "WHERE  rut_emp='" & SP_Rut_Activo & "' AND CONCAT(CAST(doc_id AS CHAR),'-',CAST(no_documento AS CHAR)) IN (" & Sm_GuiasEditando & ") " & _
                          "GROUP BY codigo"
                          
                    End If
                End If
            End If
            
            Consulta RsTmp, Sql
            
            
            
            If RsTmp.RecordCount > 0 Then
               ' Busca_Id_Combo CboDocVenta, Val(IP_Doc_ID)
                LvMateriales.ListItems.Clear
                With RsTmp
                    .MoveFirst
                    For i = 1 To .RecordCount
                        Codigo = "" & !Codigo
                        MARCA = "" & !MARCA
                        Descripcion = "" & !Descripcion
                        If SP_Rut_Activo = "76.337.408-4" Or SP_Rut_Activo = "10.722.977-9" Or SP_Rut_Activo = "12.318.896-9" Then
                                PrecioRef = CxP("" & !precio_real)
                                Descuento = Format(Str(0 & !Descuento), "###,##")
                                Precio = CxP("" & !precio_final)
                        
                        Else
                        
                        
                                
                                PrecioRef = Format(Str(0 & !precio_real), "###,##")
                                Descuento = Format(Str(0 & !Descuento), "###,##")
                                Precio = Format(Str(0 & !precio_final), "###,##")
                        End If
                        
                        If .Fields(6) < 1 Then
                            Unidades = "0" & Trim(Str(!Unidades))
                        Else
                            Unidades = Str(0 & !Unidades)
                        End If
                        
                        'IdFamiliaR = Str(!familia)
                        'NomFamiliaR = "" & !nombre_familia
                        PCosto = 0 & !precio_costo
                                            
                     
                        SubTotal = Format(Str(0 & !SubTotal), "#,0")
                        
                        'ListaMateriales.AddItem Codigo & " " & MARCA & " " & Descripcion & " " & PrecioRef & " " & Descuento & " " & Unidades & " " & Precio & " " & SubTotal & " " & IdFamiliaR & " " & NomFamiliaR & " " & PCosto
                        With LvMateriales
                            .ListItems.Add , , RsTmp!ID
                            .ListItems(.ListItems.Count).SubItems(1) = Codigo
                            .ListItems(.ListItems.Count).SubItems(2) = MARCA
                            .ListItems(.ListItems.Count).SubItems(3) = Descripcion
                            .ListItems(.ListItems.Count).SubItems(4) = PrecioRef
                            .ListItems(.ListItems.Count).SubItems(5) = Descuento
                            .ListItems(.ListItems.Count).SubItems(6) = Unidades
                            .ListItems(.ListItems.Count).SubItems(7) = Precio
                            .ListItems(.ListItems.Count).SubItems(8) = SubTotal
                            .ListItems(.ListItems.Count).SubItems(9) = IdFamiliaR
                            .ListItems(.ListItems.Count).SubItems(10) = PCosto
                            .ListItems(.ListItems.Count).SubItems(11) = TxtNroDocumento
                            .ListItems(.ListItems.Count).SubItems(12) = "" & RsTmp!btn_especial
                            .ListItems(.ListItems.Count).SubItems(13) = "" & RsTmp!comision
                            .ListItems(.ListItems.Count).SubItems(14) = "" & RsTmp!Fecha
                            .ListItems(.ListItems.Count).SubItems(15) = SubTotal - PCosto
                            .ListItems(.ListItems.Count).SubItems(16) = "" & RsTmp!pro_inventariable
                            .ListItems(.ListItems.Count).SubItems(17) = RsTmp!pla_id
                            .ListItems(.ListItems.Count).SubItems(18) = RsTmp!are_id
                            .ListItems(.ListItems.Count).SubItems(19) = RsTmp!cen_id
                            .ListItems(.ListItems.Count).SubItems(20) = 0
                            .ListItems(.ListItems.Count).SubItems(21) = "" & RsTmp!ume_nombre
                        End With
                        With LvMaterialesCopia
                            .ListItems.Add , , RsTmp!ID
                            .ListItems(.ListItems.Count).SubItems(1) = Codigo
                            .ListItems(.ListItems.Count).SubItems(2) = MARCA
                            .ListItems(.ListItems.Count).SubItems(3) = Descripcion
                            .ListItems(.ListItems.Count).SubItems(4) = PrecioRef
                            .ListItems(.ListItems.Count).SubItems(5) = Descuento
                            .ListItems(.ListItems.Count).SubItems(6) = Unidades
                            .ListItems(.ListItems.Count).SubItems(7) = Precio
                            .ListItems(.ListItems.Count).SubItems(8) = SubTotal
                            .ListItems(.ListItems.Count).SubItems(9) = IdFamiliaR
                            .ListItems(.ListItems.Count).SubItems(10) = PCosto
                            .ListItems(.ListItems.Count).SubItems(11) = TxtNroDocumento
                            .ListItems(.ListItems.Count).SubItems(12) = "" & RsTmp!btn_especial
                            .ListItems(.ListItems.Count).SubItems(13) = "" & RsTmp!comision
                            .ListItems(.ListItems.Count).SubItems(14) = "" & RsTmp!Fecha
                            .ListItems(.ListItems.Count).SubItems(15) = SubTotal - PCosto
                            .ListItems(.ListItems.Count).SubItems(16) = "" & RsTmp!pro_inventariable
                            .ListItems(.ListItems.Count).SubItems(17) = RsTmp!pla_id
                            .ListItems(.ListItems.Count).SubItems(18) = RsTmp!are_id
                            .ListItems(.ListItems.Count).SubItems(19) = RsTmp!cen_id
                            .ListItems(.ListItems.Count).SubItems(20) = 0
                            .ListItems(.ListItems.Count).SubItems(21) = "" & RsTmp!ume_nombre
                            
                        End With
                        .MoveNext
                    Next
                    LvMateriales_Click
                End With
            End If
            
'             esto era de citroen
'            If LvMateriales.ListItems.Count > 0 Then
'                If LvMateriales.ListItems(1).SubItems(12) = "SI" Then
'                    Sql = "SELECT detalle " & _
'                          "FROM detalle_zzz " & _
'                          "WHERE tipo_documento='" & dpago & "' AND no_documento =" & nnum
'
'                    Call Consulta(RsTmp, Sql)
'                    If RsTmp.RecordCount > 0 Then
'                        RsTmp.MoveFirst
'                        i = 0
'                        Do While Not RsTmp.EOF
'                           txtespecial(i) = RsTmp!Detalle
'                           i = i + 1
'                           RsTmp.MoveNext
'                        Loop
'                        frmEspecial.Visible = True
'                        frmEspecial.Left = FrameMA.Left
'                        frmEspecial.Top = FrameMA.Top - 50
'                        CmdEspecial.Enabled = False
'                        CMDeliminaMaterial.Enabled = False
'                        cmdCancel.Enabled = False
'                    End If
'                    txtespecial(0) = Descripcion
'                End If
'            End If
            

                        
            If Len(Me.TxtRut) > 0 Then
                'Datos del cliente
                Filtro = "Rut_Cliente = '" & Me.TxtRut.Text & "'"
                Sql = "SELECT rut_cliente,nombre_rsocial,direccion direccion_,ciudad,comuna,giro,descuento descuento_,fono " & _
                "FROM maestro_clientes m INNER JOIN par_asociacion_ruts a ON m.rut_cliente=a.rut_cli " & _
                "WHERE a.rut_emp='" & SP_Rut_Activo & "' AND rut_cliente = '" & Me.TxtRut.Text & "'"
                Consulta RsTmp, Sql
                If RsTmp.RecordCount > 0 Then
                        'Cliente encontrado
                    With RsTmp
                        TxtRut.Text = !rut_cliente
                        TxtRazonSocial.Text = !nombre_rsocial
                       ' TxtDireccion.Text = !direccion_
                       ' TxtCiudad.Text = !ciudad
                        TxtComuna.Text = !comuna
                        TxtGiro.Text = !giro
                        
                       ' TxtDscto.Text = "" & !Descuento_
                        Me.LbTelefono.Caption = "" & !fono
                        ClienteEncontrado = True
                    End With
                End If
            End If
            
                          'Distinta razon social
                '3 mayo 2014
            LLenarCombo CboRs, "rso_nombre", "rso_id", "par_razon_social_clientes", "rut_cliente='" & TxtRut & "'"
            If CboRs.ListCount > 0 Then
                CboRs.Visible = True
                CboRs.ListIndex = 0
                Busca_Id_Combo CboRs, Val(Ip_Rso)
            End If
            
            
            If s_FacturaGuias = "SI" Then
                 LvMateriales.Visible = False
                 If LvMateriales.ListItems.Count > 0 Then LvMateriales.Visible = True
                 
                 Sql = "SELECT id,fecha,no_documento,rut_cliente,nombre_cliente,neto,iva,bruto " & _
                        "FROM ven_doc_venta " & _
                        "WHERE  rut_emp='" & SP_Rut_Activo & "' AND nro_factura='" & L_No_Doc & "' AND doc_id_factura=" & IP_Doc_ID
                Consulta RsTmp, Sql
                Me.FramGuias.Visible = True
                Me.CmdBuscaProducto.Visible = False
                LLenar_Grilla RsTmp, Me, LVDetalle, False, True, True, False
            Else
                If SP_Control_Inventario = "NO" Then
                        Sql = "SELECT  d.vsd_id,pla_nombre, are_nombre, cen_nombre,d.aim_id," & _
                                    "IF(d.aim_id=0,'',(SELECT aim_nombre FROM con_activo_inmovilizado i WHERE d.aim_id=i.aim_id)) nombre_activo," & _
                                    "d.vsd_exento,d.vsd_neto,d.vsd_iva,d.pla_id,d.are_id,d.cen_id " & _
                                "FROM    ven_sin_detalle d " & _
                                "left JOIN con_plan_de_cuentas c USING(pla_id) " & _
                                "left JOIN par_areas a USING(are_id) " & _
                                "left JOIN par_centros_de_costo o USING(cen_id) " & _
                                "WHERE   d.id =" & DG_ID_Unico
                            Consulta RsTmp, Sql
                            LLenar_Grilla RsTmp, Me, LvSinDetalle, False, True, True, False
                            CalculaTotal
                            Bm_Nueva_Ventas = False
                Else
                    'Aqui realiza suma de materiales
                    'Pero debemos identificar si esta carga corresponde
                    'a un documento referenciado, ejmplo Nota de credito
                    'asi q el valor podria ser distinto a  esta suma.
                    'Nov 10 2012
                   ' if dg_id_unico
                 '   Me.SumaListaMaterialesYMobra Sp_DRSoloExento
                End If
            End If
            
            
            If bm_SoloVistaDoc Then
                If SP_Control_Inventario = "SI" Then
                    Me.CMDeliminaMaterial.Visible = False
                    CboDocVenta.Locked = True
                    TxtNroDocumento.Locked = True
                    Me.DtFecha.Enabled = False
                    Me.FrameMA.Enabled = False
                    'Me.FrameContable.Enabled = False
                   
                   ' If Sm_NotaDeVenta = "NO" Then
                        CmdEmiteDocumento.Caption = "Re-Emite Documento"
                   ' End If
                    TxtNroDocumento = L_No_Doc
                    Busca_Id_Combo CboDocVenta, Val(IP_Doc_ID)
                    'CmdEmiteDocumento.Enabled = False
                    
                    '11 de Julio 2015 _
                    Cuando el documento emitido es un DTE _
                    debemos cambiar el boton de emitir _
                    por uno que diga: Ver PDF
                    ChkCedible.Visible = False
                    Sql = "SELECT  doc_dte " & _
                            "FROM sis_documentos " & _
                            "WHERE doc_id=" & IP_Doc_ID
                    Consulta RsTmp2, Sql
                    If RsTmp2.RecordCount > 0 Then
                        If RsTmp2!doc_dte = "SI" Then
                            'Aqui cambiaremos el boton a ver DTE
                            Me.CmdEmiteDocumento.Caption = "Ver PDF"
                            ChkCedible.Visible = True
                        End If
                    End If
                    
                    
                    
                    
                    
                    '____________fin ver PDF_________________________/
                End If
                TxtNroDocumento = L_No_Doc
                Busca_Id_Combo CboDocVenta, Val(IP_Doc_ID)
                If Lp_Referencia > 0 Then
                    Sql = "SELECT no_documento,doc_nombre,comentario,doc_cod_sii,fecha " & _
                          "FROM ven_doc_venta v,sis_documentos d " & _
                          "WHERE  v.rut_emp='" & SP_Rut_Activo & "' AND v.doc_id=d.doc_id AND id=" & Lp_Referencia
                    Consulta RsTmp2, Sql
                    If RsTmp2.RecordCount > 0 Then
                        Me.FrmReferencia.Visible = True
                        TxtMotivoRef = Sp_Comentario
                        TxtNroReferencia = RsTmp2!no_documento
                        TxtDocReferencia = RsTmp2!doc_nombre
                        Me.SkDocIdReferencia = RsTmp2!doc_cod_sii
                        Me.SkFechaReferencia = Format(RsTmp2!Fecha, "YYYY-MM-DD")
                        'SkCodigoReferencia = RsTmp2!tnc_id
                    End If
                End If
                
                
                
                
            End If
            If Bm_EditandoPorGuias Then FramGuias.Visible = False
            TxtNeto = SkTotalMateriales
            If SP_Control_Inventario = "SI" Then
                If Sm_NotaDeVenta = "SI" Then
                    Me.CMDeliminaMaterial.Visible = True
                    CboDocVenta.Locked = False
                    TxtNroDocumento.Locked = False
                    Me.DtFecha.Enabled = True
                    Me.FrameMA.Enabled = True
                    Me.FrameContable.Enabled = True
                    CboDocVenta.Locked = False
                    FrameCliente.Enabled = True
                    bm_SoloVistaDoc = False
                    Frame1.Caption = "Doc. Venta desde Nota de Venta " & TxtNroDocumento
             '       Frame1.Tag = "SI"
                    
                Else
                    Me.FrameCliente.Enabled = False
                    Me.CboVendedores.Locked = True
                End If
            End If
            SkTotalMateriales = SkTotalMateriales.Tag
            
            
            If Val(TxtCreditoEmpresaConstructora) > 0 Then
                SkIvaMateriales = NumFormat(CDbl(SkIvaMateriales) - CDbl(TxtCreditoEmpresaConstructora))
                SkBrutoMateriales = NumFormat(CDbl(SkBrutoMateriales) - CDbl(TxtCreditoEmpresaConstructora))
            
            End If
End Function
Function GrabaDocVd(Tabla As String, Campo As String, AnteriorNo As Double, NuevoNo As Double)
       
        Sql = "SELECT * FROM " & Tabla & " WHERE " & Campo & " = " & AnteriorNo
       paso = Consulta(AdoTempVd, Sql)
       
        Sql = "UPDATE " & Tabla & " Set " & Campo & " = " & NuevoNo & " WHERE " & Campo & " = " & AnteriorNo
        cn.Execute Sql
End Function








Private Sub TxtSdExento_GotFocus()
    En_Foco TxtSdExento
End Sub


Private Sub TxtSdExento_KeyPress(KeyAscii As Integer)
    KeyAscii = AceptaSoloNumeros(KeyAscii)
End Sub


Private Sub TxtSdExento_Validate(Cancel As Boolean)

    If SinDesborde(Me.ActiveControl) Then
        Cancel = False
    Else
        Cancel = True
        TxtSdExento = 0 'ESTE SE REEMPLAZA POR EL CONTROL ACTIVO
    End If

    If Val(TxtSdExento) = 0 Then TxtSdExento = "0" Else TxtSdExento = NumFormat(TxtSdExento)
End Sub

Private Sub TxtSdIva_GotFocus()
    En_Foco TxtSdIva
End Sub


Private Sub TxtSdIva_KeyPress(KeyAscii As Integer)
    KeyAscii = AceptaSoloNumeros(KeyAscii)
End Sub


Private Sub TxtSdIva_Validate(Cancel As Boolean)
    
    'ESTO ES PARA EVITAR ERROR POR DESBORDAMIENTO
    'EVITANDO LA CAIDA DEL SISTEMA
    If SinDesborde(Me.ActiveControl) Then
        Cancel = False
    Else
        Cancel = True
        TxtSdIva = 0 'ESTE SE REEMPLAZA POR EL CONTROL ACTIVO
    End If
    'REEMPLAZAR EL NOMBRE DEL CONTROL EJ: TXTSDIVA  , TXTNETO,
    '----------------------------------------
    
    
    If Val(TxtSdIva) = 0 Then TxtSdIva = NumFormat(TxtSdIva)
End Sub
Private Sub TxtSDNeto_GotFocus()
    En_Foco TxtSDNeto
End Sub
Private Sub TxtSDNeto_KeyPress(KeyAscii As Integer)
           KeyAscii = AceptaSoloNumeros(KeyAscii)
End Sub
Private Sub TxtSDNeto_Validate(Cancel As Boolean)
    If SinDesborde(Me.ActiveControl) Then
        Cancel = False
    Else
        Cancel = True
        TxtSDNeto = 0
    End If

    If Val(TxtSDNeto) = 0 Then
        TxtSDNeto = "0"
    Else
        TxtSDNeto = NumFormat(TxtSDNeto)
        TxtSdIva = NumFormat(CDbl(TxtSDNeto) * Val(("1." & DG_IVA)) - CDbl(TxtSDNeto))
    End If
End Sub
Private Sub ImprimeNV()
    Dim Cx As Double, Cy As Double, Sp_Fecha As String, Ip_Mes As Integer, Dp_S As Double, Sp_Letras As String
    Dim Sp_Neto As String * 12, Sp_IVA As String * 12
    
    Dim Sp_Nro_Comprobante As String
    
    Dim Sp_Empresa As String
    Dim Sp_Giro As String
    Dim Sp_Direccion As String
    Dim Sp_Fono As String * 12
    Dim Sp_Mail As String
    Dim Sp_Ciudad As String * 17
    Dim Sp_Comuna As String * 17
    
    
    Dim PSp_Empresa As String
    Dim PSp_Giro As String
    Dim PSp_Direccion As String
    Dim PSp_Fono As String
    Dim PSp_Mail As String
    Dim PSp_Ciudad As String
    Dim PSp_Comuna As String
    Dim PSp_Sucursal As String
    
    
    'Variables para detalle de articulos
    Dim Sp_CodigoInt As String * 13
    Dim Sp_Descripcin As String * 28
    Dim Sp_PU As String * 11
    Dim Sp_Cant As String * 7
    Dim Sp_UM As String * 8
    Dim Sp_TotalL As String * 12
    
    Dim sp_AExento As String * 15
    Dim Sp_ANeto As String * 15
    Dim Sp_AIva As String * 15
    Dim Sp_ATotal As String * 15
    
    
    
    
    
    Dim Sp_Nombre As String * 45
    Dim Sp_Rut As String * 15
    Dim Sp_Concepto As String
    
    Dim Sp_Nro_Doc As String * 12
    Dim Sp_Documento As String * 35
    Dim Sp_Valor As String * 15
    Dim Sp_Saldo As String * 15
    Dim Sp_Fpago As String * 24
    Dim Sp_Total As String * 12
    
    Dim Sp_Banco As String * 20
    Dim Sp_NroCheque As String * 10
    Dim Sp_FechaCheque As String * 10
    Dim Sp_ValorCheque As String * 15
    Dim Sp_SumaCheque As String * 15
    
    
    
    
    
    
    Dim Sp_Observacion As String * 80
    
    On Error GoTo ERRORIMPRESION
    Sql = "SELECT giro,direccion,ciudad,fono,email,comuna " & _
         "FROM sis_empresas " & _
         "WHERE rut='" & SP_Rut_Activo & "'"
    Consulta RsTmp2, Sql
    If RsTmp2.RecordCount > 0 Then
        Sp_Giro = RsTmp2!giro
        Sp_Direccion = RsTmp2!direccion
        Sp_Fono = RsTmp2!fono
        Sp_Mail = RsTmp2!Email
        Sp_Ciudad = RsTmp2!ciudad
        Sp_Comuna = RsTmp2!comuna
    End If
            
    Sql = "SELECT direccion,ciudad,fono,email,comuna " & _
         "FROM maestro_clientes " & _
         "WHERE rut_cliente='" & TxtRut & "'"
    Consulta RsTmp2, Sql
    If RsTmp2.RecordCount > 0 Then
        PSp_Direccion = Me.TxtDireccion
        PSp_Fono = Mid(RsTmp2!fono, 1, 10)
        PSp_Mail = Trim(RsTmp2!Email)
        PSp_Ciudad = Mid(TxtCiudad, 1, 20)
        PSp_Comuna = Mid(TxtComuna, 1, 20)
    End If
    PSp_Sucursal = CboSucursal.Text
            
            
    Printer.FontName = "Courier New"
    Printer.FontSize = 10
    Printer.ScaleMode = 7
    Cx = 2
    Cy = 3
    Dp_S = 0.1
    
    
    Printer.CurrentX = Cx
    Printer.CurrentY = Cy
    Printer.PaintPicture LoadPicture(App.Path & "\REDMAROK.jpg"), Cx + 0.62, 1
    
    
    Printer.CurrentY = Printer.CurrentY + Dp_S
    
    Printer.FontSize = 18
    Printer.FontBold = True
    
    Printer.Print "           " & CboDocVenta.Text & "  " & TxtNroDocumento
    Printer.CurrentY = Printer.CurrentY + Dp_S + Dp_S
            
    Printer.FontSize = 12
    Printer.CurrentX = Cx
    Printer.Print SP_Empresa_Activa
    
    
    Printer.CurrentX = Cx
    Printer.CurrentY = Printer.CurrentY + Dp_S
  
    Printer.FontSize = 12
  
    pos = Printer.CurrentY
    Printer.Print "R.U.T.   :" & SP_Rut_Activo
    Printer.CurrentY = pos
    Printer.CurrentX = Cx + 10
    Printer.Print "FECHA:" & DtFecha.Value
    
    Printer.CurrentY = Printer.CurrentY + Dp_S
    
    Printer.FontBold = False
    Printer.FontSize = 12
    Printer.CurrentX = Cx
    Printer.Print "GIRO     :" & Sp_Giro
    Printer.CurrentY = Printer.CurrentY + Dp_S
    
    Printer.FontSize = 12
    Printer.CurrentX = Cx
    Printer.Print "DIRECCION:" & Sp_Direccion
    Printer.CurrentY = Printer.CurrentY + Dp_S
    
    'email
    Printer.FontSize = 12
    Printer.CurrentX = Cx
    Printer.Print "EMAIL    :" & Sp_Mail
    Printer.CurrentY = Printer.CurrentY + Dp_S
    
    
    Printer.FontSize = 12
    Printer.CurrentX = Cx
    Printer.Print "CIUDAD   :" & Sp_Ciudad & "   COMUNA:" & Sp_Comuna & "   FONO:" & Sp_Fono
    Printer.CurrentY = Printer.CurrentY + Dp_S + (Dp_S * 3)
    
    'FIN SECCION EMPRESA
    
    
    
    'AHORA SECCION CLIENTE
    'Printer.FontSize = 10
    pos = Printer.CurrentY
    Printer.CurrentX = Cx
    Printer.Print "SE�ORES  :" & TxtRazonSocial
    Printer.CurrentY = Printer.CurrentY + Dp_S
    
    
    
    Printer.CurrentX = Cx
    pos = Printer.CurrentY
    Printer.Print "RUT      :" & TxtRut
    
    Printer.CurrentY = pos
    Printer.CurrentX = Cx + 8
    Printer.Print "CONDICION DE PAGO:" & Me.CboFpago.Text & " " & Mid(CboPlazos.Text, 1, 25)
    
    Printer.CurrentY = Printer.CurrentY + Dp_S
    
    
    
    
    Printer.CurrentX = Cx
    Printer.Print "DIRECCION:" & PSp_Direccion
    Printer.CurrentY = Printer.CurrentY + Dp_S
    
    Printer.CurrentX = Cx
    Printer.Print "EMAIL    :" & PSp_Mail
    Printer.CurrentY = Printer.CurrentY + Dp_S
        
    Printer.CurrentX = Cx
    Printer.Print "CIUDAD   :" & PSp_Ciudad & "    COMUNA:" & PSp_Comuna & "    FONO:" & PSp_Fono
    Printer.CurrentY = Printer.CurrentY + Dp_S + Dp_S
    
    Printer.CurrentX = Cx
    Printer.Print "SUCURSAL   :" & PSp_Sucursal
    Printer.CurrentY = Printer.CurrentY + Dp_S + (Dp_S * 3)
    
    ''MASG
    Dim PSp_comentario As String
    Dim PSp_ven_centro_costo As String
    Dim PSp_ven_solicitante As String
    
    Sql = ""
         Sql = "SELECT comentario,ven_centro_costo,ven_solicitante " & _
         "FROM ven_doc_venta " & _
         "WHERE tipo_doc ='COTIZACION' and no_documento='" & TxtNroDocumento & "'"
    Consulta RsTmp2, Sql
    If RsTmp2.RecordCount > 0 Then
        PSp_comentario = RsTmp2!comentario
        PSp_ven_centro_costo = RsTmp2!ven_centro_costo
        PSp_ven_solicitante = RsTmp2!ven_solicitante
    End If
    
        Printer.CurrentX = Cx
    Printer.Print "COMENTARIO   :" & PSp_comentario
    Printer.CurrentY = Printer.CurrentY + Dp_S + (Dp_S * 3)
        Printer.CurrentX = Cx
    Printer.Print "CENTRO COSTO   :" & PSp_ven_centro_costo
    Printer.CurrentY = Printer.CurrentY + Dp_S + (Dp_S * 3)
        Printer.CurrentX = Cx
    Printer.Print "SOLICITANTE   :" & PSp_ven_solicitante
    Printer.CurrentY = Printer.CurrentY + Dp_S + (Dp_S * 3)
    ''MASG
    'Fin seccion CLIENTE
    
    'Inicio SECCION DETALLE DE ARTICULOS
            With LvMateriales
                If .ListItems.Count > 0 Then
                    Printer.CurrentX = Cx
                    Printer.Print "Detalle de Art�culos"
                    Printer.CurrentY = Printer.CurrentY + Dp_S
                    Sp_CodigoInt = "Cod. Interno"
                    Sp_Descripcin = "Descripcion"
                    RSet Sp_PU = "Pre. Unitario"
                    RSet Sp_Cant = "Cant."
                    RSet Sp_UM = "Dscto"
                    RSet Sp_TotalL = "Total"
                    
                    Printer.FontSize = 10
                    Printer.FontBold = False
                    
                    Printer.CurrentX = Cx
                    
                    Printer.Print Sp_CodigoInt & " " & Sp_Descripcin & " " & Sp_PU & " " & Sp_Cant & " " & Sp_UM & " " & Sp_TotalL
                    Printer.CurrentY = Printer.CurrentY + Dp_S
                    
                    For i = 1 To .ListItems.Count
                        Sp_CodigoInt = .ListItems(i).SubItems(1)
                        Sp_Descripcin = .ListItems(i).SubItems(3)
                        RSet Sp_PU = .ListItems(i).SubItems(4)
                        RSet Sp_Cant = .ListItems(i).SubItems(6)
                        RSet Sp_UM = .ListItems(i).SubItems(5) 'DSCTO
                      '  if
                        RSet Sp_TotalL = .ListItems(i).SubItems(8)
                        Printer.CurrentX = Cx
                        Printer.Print Sp_CodigoInt & " " & Sp_Descripcin & " " & Sp_PU & " " & Sp_Cant & " " & Sp_UM & " " & Sp_TotalL
                        Printer.CurrentY = Printer.CurrentY + Dp_S - 0.2
                    Next
                    Printer.CurrentY = Printer.CurrentY + Dp_S
                    posdetalle = Printer.CurrentY
                End If
            End With
    'FIN SECCION DETALLE DE ARTICULOS
    Printer.CurrentX = Cx
    Printer.FontBold = True
    
    'Dim Sp_ANeto As String * 15
    'Dim Sp_AIva As String * 15
    'Dim Sp_ATotal As String * 15
    RSet sp_AExento = TxtExentos
    RSet Sp_ANeto = TxtNeto
    RSet Sp_AIva = TxtIva
    RSet Sp_ATotal = txtTotal
    
    RSet sp_AExento = TxtExentos
    RSet Sp_ANeto = Me.SkTotalMateriales
    RSet Sp_AIva = Me.SkIvaMateriales
    RSet Sp_ATotal = Me.SkBrutoMateriales
    
    Printer.CurrentX = Cx + 12
    Printer.CurrentY = Printer.CurrentY + Dp_S + Dp_S + Dp_S
    pos = Printer.CurrentY
    Printer.Print "E X E N T O:"
    Printer.CurrentY = pos
    Printer.CurrentX = Cx + 14.5
    Printer.Print sp_AExento
    
    Printer.CurrentX = Cx + 12
    Printer.CurrentY = Printer.CurrentY + Dp_S - 0.2
    pos = Printer.CurrentY
    Printer.Print "N E T O    :"
    Printer.CurrentY = pos
    Printer.CurrentX = Cx + 14.5
    Printer.Print Sp_ANeto
    
    Printer.CurrentX = Cx + 12
    Printer.CurrentY = Printer.CurrentY + Dp_S - 0.2
    pos = Printer.CurrentY
    Printer.Print "I.V.A.     :"
    Printer.CurrentY = pos
    Printer.CurrentX = Cx + 14.5
    Printer.Print Sp_AIva
    
    Printer.CurrentX = Cx + 12
    Printer.CurrentY = Printer.CurrentY + Dp_S - 0.2
    pos = Printer.CurrentY
    Printer.Print "T O T A L  :"
    Printer.CurrentY = pos
    Printer.CurrentX = Cx + 14.5
    Printer.Print Sp_ATotal
    
    Printer.FontBold = False
        
   ' Printer.DrawMode = 1
    Printer.DrawWidth = 3
    
    Printer.Line (1.2, 4)-(20, 1), , B  'Rectangulo Encabezado y N� Nota de Venta
    Printer.Line (1.2, 4)-(20, 7.7), , B  'Datos de la Empresa
    Printer.Line (1.2, 4)-(20, 11.2), , B 'Datos del Proveedor
    Printer.Line (1.2, 4)-(20, posdetalle), , B 'Detalle de Nota de Vena
    Printer.Line (1.2, posdetalle)-(20, posdetalle + 2.3), , B 'Detalle del Exento,Neto,Iva,Total
       
    Printer.NewPage
    Printer.EndDoc
    Exit Sub
ERRORIMPRESION:
    MsgBox Err.Number & vbNewLine & Err.Description
End Sub
Private Sub ImprimeCOTIZACION()
    Dim Cx As Double, Cy As Double, Sp_Fecha As String, Ip_Mes As Integer, Dp_S As Double, Sp_Letras As String
    Dim Sp_Neto As String * 12, Sp_IVA As String * 12
    
    Dim Sp_Nro_Comprobante As String
    
    Dim Sp_Empresa As String
    Dim Sp_Giro As String
    Dim Sp_Direccion As String
    Dim Sp_Fono As String * 12
    Dim Sp_Mail As String
    Dim Sp_Ciudad As String * 17
    Dim Sp_Comuna As String * 17
    
    
    Dim PSp_Empresa As String
    Dim PSp_Giro As String
    Dim PSp_Direccion As String
    Dim PSp_Fono As String
    Dim PSp_Mail As String
    Dim PSp_Ciudad As String
    Dim PSp_Comuna As String
    Dim PSp_Sucursal As String
    
    
    'Variables para detalle de articulos
    Dim Sp_CodigoInt As String * 13
    Dim Sp_Descripcin As String * 28
    Dim Sp_PU As String * 11
    Dim Sp_Cant As String * 7
    Dim Sp_UM As String * 8
    Dim Sp_TotalL As String * 12
    
    Dim sp_AExento As String * 15
    Dim Sp_ANeto As String * 15
    Dim Sp_AIva As String * 15
    Dim Sp_ATotal As String * 15
    
    
    
    
    
    Dim Sp_Nombre As String * 45
    Dim Sp_Rut As String * 15
    Dim Sp_Concepto As String
    
    Dim Sp_Nro_Doc As String * 12
    Dim Sp_Documento As String * 35
    Dim Sp_Valor As String * 15
    Dim Sp_Saldo As String * 15
    Dim Sp_Fpago As String * 24
    Dim Sp_Total As String * 12
    
    Dim Sp_Banco As String * 20
    Dim Sp_NroCheque As String * 10
    Dim Sp_FechaCheque As String * 10
    Dim Sp_ValorCheque As String * 15
    Dim Sp_SumaCheque As String * 15
    
    
    
    
    
    
    Dim Sp_Observacion As String * 80
    
    On Error GoTo ERRORIMPRESION
    Sql = "SELECT giro,direccion,ciudad,fono,email,comuna " & _
         "FROM sis_empresas " & _
         "WHERE rut='" & SP_Rut_Activo & "'"
    Consulta RsTmp2, Sql
    If RsTmp2.RecordCount > 0 Then
        Sp_Giro = RsTmp2!giro
        Sp_Direccion = RsTmp2!direccion
        Sp_Fono = RsTmp2!fono
        Sp_Mail = RsTmp2!Email
        Sp_Ciudad = RsTmp2!ciudad
        Sp_Comuna = RsTmp2!comuna
    End If
            
    Sql = "SELECT direccion,ciudad,fono,email,comuna " & _
         "FROM maestro_clientes " & _
         "WHERE rut_cliente='" & TxtRut & "'"
    Consulta RsTmp2, Sql
    If RsTmp2.RecordCount > 0 Then
        PSp_Direccion = Me.TxtDireccion
        PSp_Fono = Mid(RsTmp2!fono, 1, 10)
        PSp_Mail = Trim(RsTmp2!Email)
        PSp_Ciudad = Mid(TxtCiudad, 1, 20)
        PSp_Comuna = Mid(TxtComuna, 1, 20)
    End If
    PSp_Sucursal = CboSucursal.Text
            
            
    Printer.FontName = "Courier New"
    Printer.FontSize = 10
    Printer.ScaleMode = 7
    Cx = 2
    Cy = 3
    Dp_S = 0.1
    
    
    Printer.CurrentX = Cx
    Printer.CurrentY = Cy
    Printer.PaintPicture LoadPicture(App.Path & "\REDMAROK.jpg"), Cx + 0.62, 1
    
    
    Printer.CurrentY = Printer.CurrentY + Dp_S
    
    Printer.FontSize = 18
    Printer.FontBold = True
    
    Printer.Print "           COTIZACION NRO " & TxtNroDocumento
    Printer.CurrentY = Printer.CurrentY + Dp_S + Dp_S
            
    Printer.FontSize = 12
    Printer.CurrentX = Cx
    Printer.Print SP_Empresa_Activa
    
    
    Printer.CurrentX = Cx
    Printer.CurrentY = Printer.CurrentY + Dp_S
  
    Printer.FontSize = 12
  
    pos = Printer.CurrentY
    Printer.Print "R.U.T.   :" & SP_Rut_Activo
    Printer.CurrentY = pos
    Printer.CurrentX = Cx + 10
    Printer.Print "FECHA:" & DtFecha.Value
    
    Printer.CurrentY = Printer.CurrentY + Dp_S
    
    Printer.FontBold = False
    Printer.FontSize = 12
    Printer.CurrentX = Cx
    Printer.Print "GIRO     :" & Sp_Giro
    Printer.CurrentY = Printer.CurrentY + Dp_S
    
    Printer.FontSize = 12
    Printer.CurrentX = Cx
    Printer.Print "DIRECCION:" & Sp_Direccion
    Printer.CurrentY = Printer.CurrentY + Dp_S
    
    'email
    Printer.FontSize = 12
    Printer.CurrentX = Cx
    Printer.Print "EMAIL    :" & Sp_Mail
    Printer.CurrentY = Printer.CurrentY + Dp_S
    
    
    Printer.FontSize = 12
    Printer.CurrentX = Cx
    Printer.Print "CIUDAD   :" & Sp_Ciudad & "   COMUNA:" & Sp_Comuna & "   FONO:" & Sp_Fono
    Printer.CurrentY = Printer.CurrentY + Dp_S + (Dp_S * 3)
    
    'FIN SECCION EMPRESA
    
    
    
    'AHORA SECCION CLIENTE
    'Printer.FontSize = 10
    pos = Printer.CurrentY
    Printer.CurrentX = Cx
    Printer.Print "SE�ORES  :" & TxtRazonSocial
    Printer.CurrentY = Printer.CurrentY + Dp_S
    
    
    
    Printer.CurrentX = Cx
    pos = Printer.CurrentY
    Printer.Print "RUT      :" & TxtRut
    
    Printer.CurrentY = pos
    Printer.CurrentX = Cx + 8
    Printer.Print "CONDICION DE PAGO:" & Me.CboFpago.Text & " " & Mid(CboPlazos.Text, 1, 25)
    
    Printer.CurrentY = Printer.CurrentY + Dp_S
    
    
    
    
    Printer.CurrentX = Cx
    Printer.Print "DIRECCION:" & PSp_Direccion
    Printer.CurrentY = Printer.CurrentY + Dp_S
    
    Printer.CurrentX = Cx
    Printer.Print "EMAIL    :" & PSp_Mail
    Printer.CurrentY = Printer.CurrentY + Dp_S
        
    Printer.CurrentX = Cx
    Printer.Print "CIUDAD   :" & PSp_Ciudad & "    COMUNA:" & PSp_Comuna & "    FONO:" & PSp_Fono
    Printer.CurrentY = Printer.CurrentY + Dp_S + Dp_S
    
    Printer.CurrentX = Cx
    Printer.Print "SUCURSAL   :" & PSp_Sucursal
    Printer.CurrentY = Printer.CurrentY + Dp_S + (Dp_S * 3)
    
    
    'Fin seccion CLIENTE
    
    
    
    'Inicio SECCION DETALLE DE ARTICULOS
            With LvMateriales
                If .ListItems.Count > 0 Then
                    Printer.CurrentX = Cx
                    Printer.Print "Detalle de Art�culos"
                    Printer.CurrentY = Printer.CurrentY + Dp_S
                    Sp_CodigoInt = "Cod. Interno"
                    Sp_Descripcin = "Descripcion"
                    RSet Sp_PU = "Pre. Unitario"
                    RSet Sp_Cant = "Cant."
                    RSet Sp_UM = "Dscto"
                    RSet Sp_TotalL = "Total"
                    
                    Printer.FontSize = 10
                    Printer.FontBold = False
                    
                    Printer.CurrentX = Cx
                    
                    Printer.Print Sp_CodigoInt & " " & Sp_Descripcin & " " & Sp_PU & " " & Sp_Cant & " " & Sp_UM & " " & Sp_TotalL
                    Printer.CurrentY = Printer.CurrentY + Dp_S
                    
                    For i = 1 To .ListItems.Count
                        Sp_CodigoInt = .ListItems(i).SubItems(1)
                        Sp_Descripcin = .ListItems(i).SubItems(3)
                        RSet Sp_PU = .ListItems(i).SubItems(4)
                        RSet Sp_Cant = .ListItems(i).SubItems(6)
                        RSet Sp_UM = .ListItems(i).SubItems(5) 'DSCTO
                      '  if
                        RSet Sp_TotalL = .ListItems(i).SubItems(8)
                        Printer.CurrentX = Cx
                        Printer.Print Sp_CodigoInt & " " & Sp_Descripcin & " " & Sp_PU & " " & Sp_Cant & " " & Sp_UM & " " & Sp_TotalL
                        Printer.CurrentY = Printer.CurrentY + Dp_S - 0.2
                    Next
                    Printer.CurrentY = Printer.CurrentY + Dp_S
                    posdetalle = Printer.CurrentY
                End If
            End With
    'FIN SECCION DETALLE DE ARTICULOS
    Printer.CurrentX = Cx
    Printer.FontBold = True
    
    'Dim Sp_ANeto As String * 15
    'Dim Sp_AIva As String * 15
    'Dim Sp_ATotal As String * 15
    RSet sp_AExento = TxtExentos
    RSet Sp_ANeto = TxtNeto
    RSet Sp_AIva = TxtIva
    RSet Sp_ATotal = txtTotal
    
    RSet sp_AExento = TxtExentos
    RSet Sp_ANeto = Me.SkTotalMateriales
    RSet Sp_AIva = Me.SkIvaMateriales
    RSet Sp_ATotal = Me.SkBrutoMateriales
    
    Printer.CurrentX = Cx + 12
    Printer.CurrentY = Printer.CurrentY + Dp_S + Dp_S + Dp_S
    pos = Printer.CurrentY
    Printer.Print "" '"E X E N T O:"
    Printer.CurrentY = pos
    Printer.CurrentX = Cx + 14.5
    Printer.Print " " 'sp_AExento
    
    Printer.CurrentX = Cx + 12
    Printer.CurrentY = Printer.CurrentY + Dp_S - 0.2
    pos = Printer.CurrentY
    Printer.Print "N E T O    :"
    Printer.CurrentY = pos
    Printer.CurrentX = Cx + 14.5
    Printer.Print Sp_ANeto
    
    Printer.CurrentX = Cx + 12
    Printer.CurrentY = Printer.CurrentY + Dp_S - 0.2
    pos = Printer.CurrentY
    Printer.Print "I.V.A.     :"
    Printer.CurrentY = pos
    Printer.CurrentX = Cx + 14.5
    Printer.Print Sp_AIva
    
    Printer.CurrentX = Cx + 12
    Printer.CurrentY = Printer.CurrentY + Dp_S - 0.2
    pos = Printer.CurrentY
    Printer.Print "T O T A L  :"
    Printer.CurrentY = pos
    Printer.CurrentX = Cx + 14.5
    Printer.Print Sp_ATotal
    
    Printer.FontBold = False
        
   ' Printer.DrawMode = 1
    Printer.DrawWidth = 3
    
    Printer.Line (1.2, 4)-(20, 1), , B  'Rectangulo Encabezado y N� Nota de Venta
    Printer.Line (1.2, 4)-(20, 7.7), , B  'Datos de la Empresa
    Printer.Line (1.2, 4)-(20, 11.2), , B 'Datos del Proveedor
    Printer.Line (1.2, 4)-(20, posdetalle), , B 'Detalle de Nota de Vena
    Printer.Line (1.2, posdetalle)-(20, posdetalle + 2.3), , B 'Detalle del Exento,Neto,Iva,Total
       
    Printer.NewPage
    Printer.EndDoc
    Exit Sub
ERRORIMPRESION:
    MsgBox Err.Number & vbNewLine & Err.Description
End Sub


Private Sub ImprimeFactura76095156()
    Dim Cx As Double, Cy As Double, Sp_Fecha As String, Ip_Mes As Integer, Dp_S As Double, Sp_Letras As String
    Dim Sp_Neto As String * 12, Sp_IVA As String * 12, Sp_Total As String * 12
    Dim Sp_GuiasFacturadas As String
    Printer.FontName = "Courier New"
    Printer.FontBold = False
    Printer.ScaleMode = vbCentimeters
    Printer.FontSize = 10
    Printer.ScaleMode = 7
    Printer.FontBold = False
    Cx = 1 'Comienzo horizontal
    Cy = 4.75    'Comienzo vertical
    Dp_S = 0.19 'interlinea
    Printer.CurrentX = Cx
    Printer.CurrentY = Cy
    Sp_Fecha = DtFecha.Value
    Ip_Mes = Val(Mid(Sp_Fecha, 4, 2))
    Printer.Print Mid(Sp_Fecha, 1, 2) & Space(10) & UCase(MonthName(Ip_Mes)) & Space(13) & Mid(Sp_Fecha, 9, 2)
    Printer.CurrentX = Cx + 0.6
    Printer.CurrentY = Printer.CurrentY + Dp_S
    pos = Printer.CurrentY
    Printer.Print TxtRazonSocial '' RAZON SOCIAL
    pos = pos + 0.7
    Coloca Cx + 13.5, pos, TxtRut, False, 10 ' R U T
    pos = pos + 0.7
    Coloca Cx, pos, TxtDireccion, False, 10 'DIRECCION
    Coloca Cx + 13.5, pos, TxtCiudad, False, 10 'CIUDAD
    pos = Printer.CurrentY + Dp_S
    Coloca Cx, pos, TxtGiro, False, 10 'GIRO
    Coloca Cx + 8.3, pos, LbTelefono, False, 10 'TELEFONO
    Coloca Cx + 13.5, pos, TxtComuna, False, 10 ' COMUNA
    pos = pos + 0.7
    Coloca Cx + 13.7, pos, Mid(CboPlazos.Text, 1, 25), False, 10 'PLAZOS
    pos = Printer.CurrentY
    'Acaba el detalle de los productos
    Printer.CurrentY = Printer.CurrentY + 1.2
    If Bm_Factura_por_Guias = True Then
        '**************************************************
        'Factura por guias 6 Octubre 2011
        '**************************************************
        LvMateriales.ListItems.Add , , ""
        LvMateriales.ListItems(LvMateriales.ListItems.Count).SubItems(3) = "SEGUN GUIAS NRO"
        Sql = "SELECT no_documento " & _
              "FROM ven_doc_venta " & _
              "WHERE  rut_emp='" & SP_Rut_Activo & "' AND doc_id_factura=" & CboDocVenta.ItemData(CboDocVenta.ListIndex) & " AND nro_factura=" & Me.TxtNroDocumento
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
            Sp_GuiasFacturadas = Empty
            RsTmp.MoveFirst
            cont = 1
            Do While Not RsTmp.EOF
                Sp_GuiasFacturadas = Sp_GuiasFacturadas & RsTmp!no_documento & ","
                cont = cont + 1
                RsTmp.MoveNext
                If cont = 3 Then
                    LvMateriales.ListItems.Add , , ""
                    LvMateriales.ListItems(LvMateriales.ListItems.Count).SubItems(3) = Mid(Sp_GuiasFacturadas, 1, Len(Sp_GuiasFacturadas) - 1)
                    Sp_GuiasFacturadas = Empty
                    cont = 1
                End If
            Loop
            If Sp_GuiasFacturadas <> Empty Then
                    LvMateriales.ListItems.Add , , ""
                    LvMateriales.ListItems(LvMateriales.ListItems.Count).SubItems(3) = Mid(Sp_GuiasFacturadas, 1, Len(Sp_GuiasFacturadas) - 1)
            End If
            Sql = "SELECT neto,iva,bruto " & _
                  "FROM ven_doc_venta " & _
                  "WHERE  rut_emp='" & SP_Rut_Activo & "' AND doc_id=" & CboDocVenta.ItemData(CboDocVenta.ListIndex) & " AND no_documento=" & TxtNroDocumento
            Consulta RsTmp, Sql
            If RsTmp.RecordCount > 0 Then
                Me.SkBrutoMateriales = NumFormat(RsTmp!bruto)
                SkTotalMateriales = NumFormat(RsTmp!Neto)
                SkIvaMateriales = NumFormat(RsTmp!Iva)
            End If
            
            'Printer.CurrentY = Printer.CurrentY  1.3
            For i = 1 To LvMateriales.ListItems.Count
                Printer.CurrentX = Cx + 2
                Printer.Print LvMateriales.ListItems(i).SubItems(3) '& " " & LvMateriales.ListItems(2).SubItems(3)
                Printer.CurrentY = Printer.CurrentY + Dp_S
            Next
        End If
        ' Fin
    End If
    If Bm_Factura_por_Guias Then DetalleParaFacturaGuias "xx" ' LvMateriales.ListItems.Clear
    
    For i = 1 To LvMateriales.ListItems.Count
        '6 - 3 - 7 - 8
        Printer.CurrentY = Printer.CurrentY + Dp_S
        Printer.CurrentX = Cx - 2
        Printer.Print Right(Space(8) & LvMateriales.ListItems(i).SubItems(6), 8) & Space(7) & _
                Left(LvMateriales.ListItems(i).SubItems(3) & Space(44), 44) & Space(5) & _
                Right(Space(11) & LvMateriales.ListItems(i).SubItems(7), 11) & Space(2) & _
                Right(Space(11) & LvMateriales.ListItems(i).SubItems(8), 11)
    Next
   
    
    
    Printer.CurrentX = Cx + 1.2
    Printer.CurrentY = Cy + 13.9
    Printer.Print Mid(TxtComentario, 1, 80)
    
    
    Printer.CurrentX = Cx
    Printer.CurrentY = Cy + 17.8
    If Mid(CboDocVenta.Text, 1, 4) <> "GUIA" Then 'SI ES GUIA NO IMPRIME VALOR EN LETRAS
        pos = Printer.CurrentY
        Sp_Letras = UCase(BrutoAletras(CDbl(SkBrutoMateriales), True))
        mitad = Len(Sp_Letras) \ 2
        buscaespacio = InStr(mitad, Sp_Letras, " ")
        If buscaespacio > 0 Then
                Printer.Print Mid(Sp_Letras, 1, buscaespacio)
                Printer.CurrentX = Cx
                Printer.CurrentY = Cy + 18.4
                Printer.Print Mid(Sp_Letras, buscaespacio)
        Else
                  Printer.Print Sp_Letras
        End If
    End If
    RSet Sp_Neto = SkTotalMateriales
    RSet Sp_IVA = SkIvaMateriales
    RSet Sp_Total = SkBrutoMateriales
    Printer.CurrentY = Cy + 15.15
    Printer.CurrentX = Cx + 14
    If Mid(CboDocVenta.Text, 1, 4) <> "GUIA" Then
        Printer.Print Sp_Neto 'SI ES GUIA NO IMPRIME NETO IVA TOTAL, SOLO NETO
        Printer.CurrentX = Cx + 14
        Printer.CurrentY = Cy + 15.95
        Printer.Print Sp_IVA
    Else
        
        Printer.CurrentY = Printer.CurrentY + Dp_S + 0.4
    End If
    Printer.CurrentX = Cx + 14
    Printer.CurrentY = Cy + 16.75
    If Mid(CboDocVenta.Text, 1, 4) = "GUIA" Then
        Printer.Print Sp_Neto
    Else
        Printer.Print Sp_Total
    End If
    Printer.NewPage
    Printer.EndDoc
End Sub

Private Sub DetalleParaFacturaGuias(sp_Guias As String)
    Sql = "SELECT v.id, v.codigo,'' marca,v.descripcion,v.precio_real,v.descuento,SUM(v.unidades),v.precio_final," & _
                             "SUM(v.subtotal),v.familia " & _
                      "FROM ven_detalle AS v " & _
                      "INNER JOIN maestro_productos AS p ON v.codigo = p.codigo " & _
                      "INNER JOIN ven_doc_venta d ON v.no_documento=d.no_documento AND v.doc_id=d.doc_id " & _
                      "WHERE v.rut_emp='" & SP_Rut_Activo & "' AND  d.rut_emp='" & SP_Rut_Activo & "' AND  p.rut_emp='" & SP_Rut_Activo & "' AND d.nro_factura=" & Me.TxtNroDocumento & " AND doc_id_factura=" & CboDocVenta.ItemData(CboDocVenta.ListIndex) & " " & _
                      "GROUP BY v.codigo,v.precio_final"
                          
    
     
     If Bm_Es_Nota_de_Credito Then
        
         Sql = "SELECT v.id, v.codigo,v.marca,v.descripcion,v.precio_real,v.descuento,SUM(v.unidades),v.precio_final," & _
                             "SUM(v.subtotal),v.familia " & _
                      "FROM ven_detalle AS v " & _
                      "INNER JOIN maestro_productos AS p ON v.codigo = p.codigo " & _
                      "WHERE v.rut_emp='" & SP_Rut_Activo & "' AND p.rut_emp='" & SP_Rut_Activo & "' AND no_documento=" & Me.TxtNroDocumento & " AND doc_id=" & CboDocVenta.ItemData(CboDocVenta.ListIndex) & " " & _
                      "GROUP BY v.codigo,v.precio_final"
     
     End If
    Consulta RsTmp3, Sql
     
     LLenar_Grilla RsTmp3, Me, LvMateriales, False, True, True, False

End Sub
Private Sub ImprimeFacturaAnncay()
    Dim Cx As Double, Cy As Double, Sp_Fecha As String, Ip_Mes As Integer, Dp_S As Double, Sp_Letras As String
    Dim Sp_Neto As String * 12, Sp_IVA As String * 12, Sp_Total As String * 12
    Dim Sp_GuiasFacturadas As String
    Dim Sp_Segun_Guias As String
    Printer.FontName = "Courier New"
    Printer.FontBold = False
    Printer.ScaleMode = vbCentimeters
    Printer.FontSize = 10
    Printer.ScaleMode = 7
    Printer.FontBold = False
    Cx = 3.2
    Cy = 3.9
    Dp_S = 0.17 'interlinea
    Printer.CurrentX = Cx
    Printer.CurrentY = Cy
    pos = Printer.CurrentY
    Sp_Fecha = DtFecha.Value
    Ip_Mes = Val(Mid(Sp_Fecha, 4, 2))
    Printer.Print Mid(Sp_Fecha, 1, 2) & Space(10) & UCase(MonthName(Ip_Mes))
    
   ' Printer.Print Mid(Sp_Fecha, 1, 2) & Space(10) & UCase(MonthName(Ip_Mes)) & Space(16) & Mid(Sp_Fecha, 9, 2)
    
    Printer.CurrentX = Cx + 6.7
    Printer.CurrentY = pos
    Printer.Print Mid(Sp_Fecha, 9, 2)
    Printer.CurrentX = Cx + 0.6
    Printer.CurrentY = Printer.CurrentY + Dp_S
    pos = Printer.CurrentY
    Printer.Print TxtRazonSocial
    Printer.CurrentY = Printer.CurrentY + Dp_S - 0.1
    pos = Printer.CurrentY
    Printer.CurrentX = Cx
    Printer.Print Me.TxtRut
    'Printer.CurrentY = Printer.CurrentY + Dp_S
    Printer.CurrentY = pos
    Printer.CurrentX = Cx + 5.7
    Printer.Print TxtComuna
    Printer.CurrentY = pos
    Printer.CurrentX = Cx + 10.5
    Printer.Print TxtCiudad
    Printer.CurrentY = pos
    Printer.CurrentX = Cx + 15.2
    Printer.Print Me.LbTelefono
    Printer.CurrentX = Cx + 1
    Printer.CurrentY = Printer.CurrentY + Dp_S - 0.1
    pos = Printer.CurrentY
    Printer.Print TxtDireccion
    Printer.CurrentX = Cx
    Printer.CurrentY = Printer.CurrentY + Dp_S - 0.1
    pos = Printer.CurrentY
    Printer.Print CboGiros.Text ' MARIO ****   TxtGiro
    'Printer.Print TxtGiro
    Printer.CurrentY = pos
    Printer.CurrentX = Cx + 14.7
    Printer.Print Mid(CboPlazos.Text, 1, 25)
    
    
    'Solo para sacar nro de orden de compra
    Sql = "SELECT no_documento,ven_ordendecompra " & _
              "FROM ven_doc_venta " & _
              "WHERE  rut_emp='" & SP_Rut_Activo & "' AND doc_id_factura=" & CboDocVenta.ItemData(CboDocVenta.ListIndex) & " AND nro_factura=" & Me.TxtNroDocumento
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
            TxtOrdenesCompra = RsTmp!ven_ordendecompra
    End If
    
    
    
    Printer.CurrentX = Cx + 7.6
    Printer.CurrentY = Printer.CurrentY + Dp_S - 0.1
    pos = Printer.CurrentY
    Printer.Print TxtOrdenesCompra
    
          
    pos = Printer.CurrentY
    'Acaba el detalle de los productos
    Printer.CurrentY = Printer.CurrentY + 1.5
    If Bm_Factura_por_Guias = True Then
        '**************************************************
        'Factura por guias 6 Octubre 2011
        '**************************************************
        LvMateriales.ListItems.Add , , ""
        LvMateriales.ListItems(LvMateriales.ListItems.Count).SubItems(3) = "SEGUN GUIAS NRO"
        Sql = "SELECT no_documento,ven_ordendecompra " & _
              "FROM ven_doc_venta " & _
              "WHERE  rut_emp='" & SP_Rut_Activo & "' AND doc_id_factura=" & CboDocVenta.ItemData(CboDocVenta.ListIndex) & " AND nro_factura=" & Me.TxtNroDocumento
        Consulta RsTmp, Sql
        Sp_Segun_Guias = Empty
        If RsTmp.RecordCount > 0 Then
        
            Sp_GuiasFacturadas = Empty
            RsTmp.MoveFirst
            cont = 1
            Sp_Segun_Guias = "SEGUN GUIAS "
            Do While Not RsTmp.EOF
                'Sp_GuiasFacturadas = Sp_GuiasFacturadas & RsTmp!no_documento & ","
                'cont = cont + 1
                'RsTmp.MoveNext
                'If cont = 3 Then
                '    LvMateriales.ListItems.Add , , ""
                '    LvMateriales.ListItems(LvMateriales.ListItems.Count).SubItems(3) = Mid(Sp_GuiasFacturadas, 1, Len(Sp_GuiasFacturadas) - 1)
                '    Sp_GuiasFacturadas = Empty
                'End If
                
                Sp_Segun_Guias = Sp_Segun_Guias & RsTmp!no_documento & ","
                RsTmp.MoveNext
            Loop
            Sp_Segun_Guias = Mid(Sp_Segun_Guias, 1, Len(Sp_Segun_Guias) - 1)
            If Sp_GuiasFacturadas <> Empty Then
                    LvMateriales.ListItems.Add , , ""
                    LvMateriales.ListItems(LvMateriales.ListItems.Count).SubItems(3) = Mid(Sp_GuiasFacturadas, 1, Len(Sp_GuiasFacturadas) - 1)
            End If
            Sql = "SELECT neto,iva,bruto " & _
                  "FROM ven_doc_venta " & _
                  "WHERE  rut_emp='" & SP_Rut_Activo & "' AND doc_id=" & CboDocVenta.ItemData(CboDocVenta.ListIndex) & " AND no_documento=" & TxtNroDocumento
            Consulta RsTmp, Sql
            If RsTmp.RecordCount > 0 Then
                Me.SkBrutoMateriales = NumFormat(RsTmp!bruto)
                SkTotalMateriales = NumFormat(RsTmp!Neto)
                SkIvaMateriales = NumFormat(RsTmp!Iva)
            End If
            'Printer.CurrentX = Cx + 2
            'Printer.CurrentY = Printer.CurrentY - 1.3
            'Printer.Print LvMateriales.ListItems(1).SubItems(3) & " " & LvMateriales.ListItems(2).SubItems(3)
            'Printer.CurrentY = Printer.CurrentY + 1.7
        End If
        ' Fin
    End If
    If Bm_Factura_por_Guias Then DetalleParaFacturaGuias "xx" ' LvMateriales.ListItems.Clear
    Printer.CurrentY = Cy + 4
    For i = 1 To LvMateriales.ListItems.Count
        '6 - 3 - 7 - 8
        Printer.CurrentY = Printer.CurrentY + 0.11
        Printer.CurrentX = Cx - 1.5
        Printer.Print Right(Space(8) & LvMateriales.ListItems(i).SubItems(6), 8) & Space(7) & _
                Left(LvMateriales.ListItems(i).SubItems(3) & Space(44), 44) & Space(3) & _
                Right(Space(11) & NumFormat(LvMateriales.ListItems(i).SubItems(7)), 11) & Space(1) & _
                Right(Space(11) & NumFormat(LvMateriales.ListItems(i).SubItems(8)), 11)
    Next
   
    
    Printer.CurrentX = Cx + 1.2
    Printer.CurrentY = Cy + 13.9
    Printer.Print Mid(Sp_Segun_Guias, 1, 80)
    
    
    
    Printer.CurrentX = Cx + 1.2
    Printer.CurrentY = Cy + 14.6
    Printer.Print Mid(TxtComentario, 1, 80)
    
    
    Printer.CurrentX = Cx
    Printer.CurrentY = Cy + 15.2
    If Mid(CboDocVenta.Text, 1, 4) <> "GUIA" Then 'SI ES GUIA NO IMPRIME VALOR EN LETRAS
        pos = Printer.CurrentY
        Sp_Letras = UCase(BrutoAletras(CDbl(SkBrutoMateriales), True))
        mitad = Len(Sp_Letras) \ 2
        buscaespacio = InStr(mitad, Sp_Letras, " ")
        If buscaespacio > 0 Then
                Printer.Print Mid(Sp_Letras, 1, buscaespacio)
                Printer.CurrentX = Cx
                Printer.CurrentY = Cy + 15.8
                Printer.Print Mid(Sp_Letras, buscaespacio)
        Else
                Printer.Print Sp_Letras
        End If
    End If
    RSet Sp_Neto = SkTotalMateriales
    RSet Sp_IVA = SkIvaMateriales
    RSet Sp_Total = SkBrutoMateriales
    Printer.CurrentY = Cy + 15
    Printer.CurrentX = Cx + 13.8
   ' Printer.CurrentX = Cx + 14.2
    If Mid(CboDocVenta.Text, 1, 4) <> "GUIA" Then
        Printer.Print Sp_Neto 'SI ES GUIA NO IMPRIME NETO IVA TOTAL, SOLO NETO
        Printer.CurrentX = Cx + 13.8
        Printer.CurrentY = Printer.CurrentY + Dp_S + 0.1
        Printer.Print Sp_IVA
    Else
        
        Printer.CurrentY = Printer.CurrentY + Dp_S + 0.4
    End If
    Printer.CurrentX = Cx + 13.8
    Printer.CurrentY = Printer.CurrentY + Dp_S + 0.1
    If Mid(CboDocVenta.Text, 1, 4) = "GUIA" Then
        Printer.CurrentY = Printer.CurrentY + Dp_S + 0.3
        Printer.Print Sp_Neto
    Else
        Printer.Print Sp_Total
    End If
    Printer.NewPage
    Printer.EndDoc
End Sub

Private Sub ImprimeFactura()
    Dim Cx As Double, Cy As Double, Sp_Fecha As String, Ip_Mes As Integer, Dp_S As Double, Sp_Letras As String
    Dim Sp_Neto As String * 12, Sp_IVA As String * 12, Sp_Total As String * 12
    Dim Sp_Cantidad As String * 7
    Dim Sp_Detalle As String * 45
    Dim Sp_Codigo As String * 10
    Dim Sp_Unitario As String * 11
    Dim Sp_Total_linea As String * 11
    Dim Sp_GuiasFacturadas As String
    Dim Sp_Segun_Guias As String
    Printer.FontName = "Courier New"
    Printer.FontBold = False
    Printer.ScaleMode = vbCentimeters
    Printer.FontSize = 10
    Printer.ScaleMode = 7
    Printer.FontBold = False
    Cx = 2.1
    'Cy = 5.9
    Cy = 5.8
    Dp_S = 0.17 'interlinea
    Printer.CurrentX = Cx
    Printer.CurrentY = Cy
    pos = Printer.CurrentY
    Sp_Fecha = DtFecha.Value
    Ip_Mes = Val(Mid(Sp_Fecha, 4, 2))
    Printer.Print Space(43) & Mid(Sp_Fecha, 1, 2) & Space(7) & UCase(MonthName(Ip_Mes))
    Printer.CurrentX = Cx + 6.7
    Printer.CurrentY = pos
    Printer.Print Space(49) & Mid(Sp_Fecha, 9, 2)  'A�O DE LA FECHA
    Printer.CurrentX = Cx + 0.6
    'Printer.CurrentY = Printer.CurrentY + Dp_S
    Printer.CurrentY = Printer.CurrentY + Dp_S + 0.1
    
    pos = Printer.CurrentY
    
    Printer.Print TxtRazonSocial
    
    Printer.CurrentX = Cx + 13
    Printer.CurrentY = pos
    Printer.Print TxtRut
    
    'Printer.CurrentY = Printer.CurrentY + Dp_S - 0.1
    Printer.CurrentY = Printer.CurrentY + Dp_S
    
    pos = Printer.CurrentY
    Printer.CurrentX = Cx
    Printer.CurrentY = pos
    
    'Printer.CurrentX = Cx + 5.7
    
    Printer.CurrentX = Cx + 0.6
    Printer.CurrentY = Printer.CurrentY + Dp_S
    pos = Printer.CurrentY
    
    Printer.Print TxtDireccion
    
    Printer.CurrentX = Cx + 13.2
    Printer.CurrentY = pos
    Printer.Print TxtComuna

    
    Printer.CurrentX = Cx + 0.6
    Printer.CurrentY = Printer.CurrentY + Dp_S
    pos = Printer.CurrentY
    
    Printer.Print TxtGiro 'GIRO
    
    Printer.CurrentX = Cx + 13
    Printer.CurrentY = pos
    Printer.Print LbTelefono 'TELEFONO
    
    

    
    
    'Solo para sacar nro de orden de compra
    Sql = "SELECT no_documento,ven_ordendecompra " & _
              "FROM ven_doc_venta " & _
              "WHERE  rut_emp='" & SP_Rut_Activo & "' AND doc_id_factura=" & CboDocVenta.ItemData(CboDocVenta.ListIndex) & " AND nro_factura=" & Me.TxtNroDocumento
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
            TxtOrdenesCompra = RsTmp!ven_ordendecompra
    End If
    
    
    
   
    
    Printer.CurrentX = Cx + 0.6
    Printer.CurrentY = Printer.CurrentY + Dp_S
    pos = Printer.CurrentY
    
    Printer.Print Space(5) & TxtOrdenesCompra
    Printer.CurrentY = Printer.CurrentY + 0.2
    If Mid(CboPlazos.Text, 1, 7) = "CONTADO" Then
        Printer.CurrentX = Cx + 3.6
        
    Else
        Printer.CurrentX = Cx + 5.7
    End If
    Printer.Print "X"
    pos = Printer.CurrentY
    'Acaba el detalle de los productos
    Printer.CurrentY = Printer.CurrentY + 1.5
    If Bm_Factura_por_Guias = True Then
        '**************************************************
        'Factura por guias 6 Octubre 2011
        '**************************************************
        LvMateriales.ListItems.Add , , ""
        LvMateriales.ListItems(LvMateriales.ListItems.Count).SubItems(3) = "SEGUN GUIAS NRO"
        Sql = "SELECT no_documento,ven_ordendecompra " & _
              "FROM ven_doc_venta " & _
              "WHERE  rut_emp='" & SP_Rut_Activo & "' AND doc_id_factura=" & CboDocVenta.ItemData(CboDocVenta.ListIndex) & " AND nro_factura=" & Me.TxtNroDocumento
        Consulta RsTmp, Sql
        Sp_Segun_Guias = Empty
        If RsTmp.RecordCount > 0 Then
        
            Sp_GuiasFacturadas = Empty
            RsTmp.MoveFirst
            cont = 1
            Sp_Segun_Guias = "SEGUN GUIAS "
            Do While Not RsTmp.EOF
                'Sp_GuiasFacturadas = Sp_GuiasFacturadas & RsTmp!no_documento & ","
                'cont = cont + 1
                'RsTmp.MoveNext
                'If cont = 3 Then
                '    LvMateriales.ListItems.Add , , ""
                '    LvMateriales.ListItems(LvMateriales.ListItems.Count).SubItems(3) = Mid(Sp_GuiasFacturadas, 1, Len(Sp_GuiasFacturadas) - 1)
                '    Sp_GuiasFacturadas = Empty
                'End If
                
                Sp_Segun_Guias = Sp_Segun_Guias & RsTmp!no_documento & ","
                RsTmp.MoveNext
            Loop
            Sp_Segun_Guias = Mid(Sp_Segun_Guias, 1, Len(Sp_Segun_Guias) - 1)
            If Sp_GuiasFacturadas <> Empty Then
                    LvMateriales.ListItems.Add , , ""
                    LvMateriales.ListItems(LvMateriales.ListItems.Count).SubItems(3) = Mid(Sp_GuiasFacturadas, 1, Len(Sp_GuiasFacturadas) - 1)
            End If
            Sql = "SELECT neto,iva,bruto " & _
                  "FROM ven_doc_venta " & _
                  "WHERE  rut_emp='" & SP_Rut_Activo & "' AND doc_id=" & CboDocVenta.ItemData(CboDocVenta.ListIndex) & " AND no_documento=" & TxtNroDocumento
            Consulta RsTmp, Sql
            If RsTmp.RecordCount > 0 Then
                Me.SkBrutoMateriales = NumFormat(RsTmp!bruto)
                SkTotalMateriales = NumFormat(RsTmp!Neto)
                SkIvaMateriales = NumFormat(RsTmp!Iva)
            End If
            'Printer.CurrentX = Cx + 2
            'Printer.CurrentY = Printer.CurrentY - 1.3
            'Printer.Print LvMateriales.ListItems(1).SubItems(3) & " " & LvMateriales.ListItems(2).SubItems(3)
            'Printer.CurrentY = Printer.CurrentY + 1.7
        End If
        ' Fin
    End If
    If Bm_Factura_por_Guias Then DetalleParaFacturaGuias "xx" ' LvMateriales.ListItems.Clear
    'Printer.CurrentY = Cy + 4
    
    Printer.CurrentY = Cy + 4.5
    Printer.FontSize = 9
    
    For i = 1 To LvMateriales.ListItems.Count
        '6 - 3 - 7 - 8
        Printer.CurrentY = Printer.CurrentY + 0.11
        'Printer.CurrentX = Cx - 1.5
        
        Printer.CurrentX = Cx - 1
        
        
        'Printer.Print Right(Space(8) & LvMateriales.ListItems(i).SubItems(6), 8) & Space(7) & _
                Left(LvMateriales.ListItems(i).SubItems(3) & Space(44), 44) & Space(3) & _
                Right(Space(11) & NumFormat(LvMateriales.ListItems(i).SubItems(7)), 11) & Space(2) & _
                Right(Space(11) & NumFormat(LvMateriales.ListItems(i).SubItems(8)), 11)
        RSet Sp_Cantidad = LvMateriales.ListItems(i).SubItems(6)
        LSet Sp_Detalle = LvMateriales.ListItems(i).SubItems(3)
        LSet Sp_Codigo = LvMateriales.ListItems(i).SubItems(1)
        RSet Sp_Unitario = LvMateriales.ListItems(i).SubItems(7)
        RSet Sp_Total_linea = LvMateriales.ListItems(i).SubItems(8)
        Printer.Print Sp_Cantidad & Space(5) & Sp_Detalle & Space(3) & Sp_Codigo & Space(2) & Sp_Unitario & Space(4) & Sp_Total_linea
         
                
    Next
   
     Printer.FontSize = 10
    
    Printer.CurrentX = Cx + 1.2
    Printer.CurrentY = Cy + 13
    
    Printer.Print Mid(Sp_Segun_Guias, 1, 80)
    
    
    
    Printer.CurrentX = Cx + 1.2
    Printer.CurrentY = Cy + 13.8
    Printer.Print Mid(TxtComentario, 1, 80)
    
    
    Printer.CurrentX = Cx
    Printer.CurrentY = Cy + 15.2
    If Mid(CboDocVenta.Text, 1, 4) <> "GUIA" Then 'SI ES GUIA NO IMPRIME VALOR EN LETRAS
        pos = Printer.CurrentY
        Sp_Letras = UCase(BrutoAletras(CDbl(SkBrutoMateriales), True))
        mitad = Len(Sp_Letras) \ 2
        buscaespacio = InStr(mitad, Sp_Letras, " ")
        If buscaespacio > 0 Then
                Printer.Print Mid(Sp_Letras, 1, buscaespacio)
                Printer.CurrentX = Cx
                Printer.CurrentY = Cy + 15.8
                Printer.Print Mid(Sp_Letras, buscaespacio)
        Else
                  Printer.Print Sp_Letras
        End If
    End If
    RSet Sp_Neto = SkTotalMateriales
    RSet Sp_IVA = SkIvaMateriales
    RSet Sp_Total = SkBrutoMateriales
    'Printer.CurrentY = Cy + 15
    'Printer.CurrentX = Cx + 14.2
    
    Printer.CurrentY = Cy + 14.6
    Printer.CurrentX = Cx + 15
    
    'If Mid(CboDocVenta.Text, 1, 4) <> "GUIA" Then
        Printer.Print Sp_Neto 'SI ES GUIA NO IMPRIME NETO IVA TOTAL, SOLO NETO
        'Printer.CurrentX = Cx + 14.2
        
        Printer.CurrentX = Cx + 15
        
        Printer.CurrentY = Printer.CurrentY + Dp_S + 0.3
        Printer.Print Sp_IVA
    'Else
        
    '    Printer.CurrentY = Printer.CurrentY + Dp_S + 0.4
    
    '    Printer.CurrentY = Printer.CurrentY + Dp_S + 0.1
        
    'End If
    'Printer.CurrentX = Cx + 14.2
    
    Printer.CurrentX = Cx + 15
    
    Printer.CurrentY = Printer.CurrentY + Dp_S + 0.3
    'If Mid(CboDocVenta.Text, 1, 4) = "GUIA" Then
    '    Printer.CurrentY = Printer.CurrentY + Dp_S + 0.3
    '    Printer.Print Sp_Neto
    'Else
        Printer.Print Sp_Total
    'End If
    Printer.NewPage
    Printer.EndDoc
End Sub
Private Sub AnulacionDeDocumento()
    Dim s_Inventario As String, L_Unico As Long, s_mov As String
    Dim rs_Detalle As Recordset, Sp_Llave As String, Sp_Bodega As Integer, Lp_IdAbono As Long
    Dim s_FacturaGuias As String * 2
    'If Principal.CmdMenu(8).Visible = True Then
    '    If ConsultaCentralizado("2,10", IG_Mes_Contable, IG_Ano_Contable) Then
    '        MsgBox "periodo contable ya ha sido centralizado, " & vbNewLine & "No puede modificar"
    ''        Exit Sub
     '   End If
    'End If
    Dim Sp_Codigos As String
    
    L_Unico = DG_ID_Unico
    SG_codigo2 = Empty
    
    Sql = "SELECT nro_factura,doc_id_factura " & _
            "FROM ven_doc_venta " & _
            "WHERE id=" & L_Unico & " AND nro_factura>0 "
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        MsgBox "Es una guia facturada..." & vbNewLine & " Primero elimine la factura...", vbInformation
        Exit Sub
    End If
    
    Sql = "SELECT id_ref " & _
            "FROM ven_doc_venta " & _
            "WHERE id_ref=" & L_Unico
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        MsgBox "Documento tiene N.Credito asociada..." & vbNewLine & "No es posible eliminar...", vbInformation
        Exit Sub
    End If
    
    sis_InputBox.FramBox = "Ingrese llave para anulacion"
    sis_InputBox.Show 1
    Sp_Llave = UCase(SG_codigo2)
    If Len(Sp_Llave) = 0 Then Exit Sub
    
    
    
    
    
    Sql = "SELECT usu_id,usu_nombre,usu_login " & _
          "FROM sis_usuarios " & _
          "WHERE usu_pwd = MD5('" & Sp_Llave & "')"
    Consulta RsTmp3, Sql
    
    
    '1ro Extraeremos el abo_id en el caso que exista 21-01-2012
    Sql = "SELECT abo_id " & _
          "FROM cta_abono_documentos d " & _
          "INNER JOIN cta_abonos a USING(abo_id) " & _
          "WHERE abo_cli_pro='CLI' AND a.rut_emp ='" & SP_Rut_Activo & "' AND d.rut_emp ='" & SP_Rut_Activo & "' AND id=" & L_Unico
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
                '2DO SABREMOS QUE TIPO DE ABONO FUE,
            Lp_IdAbono = RsTmp!abo_id
            Sql = "SELECT abo_id,abo_fecha,mpa_nombre,usu_nombre,a.mpa_id,abo_fecha_pago " & _
                  "FROM cta_abonos a " & _
                  "INNER JOIN par_medios_de_pago m USING(mpa_id) " & _
                  "INNER JOIN cta_abono_documentos d USING(abo_id) " & _
                  "WHERE  abo_cli_pro='CLI' AND a.rut_emp='" & SP_Rut_Activo & "' AND d.rut_emp='" & SP_Rut_Activo & "' AND a.abo_id=" & Lp_IdAbono
            Consulta RsTmp, Sql
            If RsTmp.RecordCount > 0 Then
                If RsTmp.RecordCount > 1 Then
                    'Ojo que el abono de este documento fue junto con varios documento
                    'por lo tanto no debemos permitir eliminar este documento
                    MsgBox "No es posible eliminar este documento..." & vbNewLine & "Es parte de un abono de multiples documentos " & _
                    vbNewLine & "Cant. Documentos " & RsTmp.RecordCount & " Fecha Abono " & Format(RsTmp!abo_fecha_pago, "DD-MM-YYYY") & " Usuario:" & RsTmp!usu_nombre & " Medio de pago:" & RsTmp!mpa_nombre, vbExclamation
                    Exit Sub
                End If
                If RsTmp.RecordCount = 1 Then
                    If RsTmp!mpa_id = 5 Then
                        '19 Agosto de 2013
                        'aqui debieremos pasar no mas por q el abono es de una guia
                    ElseIf RsTmp!mpa_id <> 1 Then
                        MsgBox "No es posible eliminar este documento..." & vbNewLine & "Ya se realiz� abono ... " & _
                        vbNewLine & " Fecha Abono " & Format(RsTmp!abo_fecha_pago, "DD-MM-YYYY") & " Usuario:" & RsTmp!usu_nombre & " Medio de pago:" & RsTmp!mpa_nombre, vbExclamation
                        Exit Sub
                    Else
                        'El abono es solo a un documento y en contado(efectivo) por lo que no es problema eliminarlo
                    End If
                End If
            
            
            End If
            
            If MsgBox("El documento tiene un abono registrado ..." & vbNewLine & "Nro Comprobante XX " & vbNewLine & "�Continuar?", vbQuestion + vbYesNo) = vbNo Then Exit Sub
            
    Else
        'El documento es al credito pero no tiene abonos, es posible continuar, no afectara nada
    
    End If
    
    
    
    If RsTmp3.RecordCount > 0 Then
        X = InputBox("Usuario:" & RsTmp3!usu_nombre & vbNewLine & "Ingrese motivo", "Motivo de eliminaci�n")
        If Len(X) = 0 Then
            MsgBox "No ingreso motivo, no fue eliminado el documento"
            Exit Sub
        End If
        
      '  With LvDetalle.SelectedItem
             Sql = "INSERT INTO sis_eliminacion_documentos (id,eli_fecha,eli_motivo,usu_login,eli_ven_com) VALUES (" & _
                    DG_ID_Unico & ",'" & Fql(Date) & "','" & UCase(X) & "','" & RsTmp3!usu_login & "','VEN')"
            
            'Sql = "INSERT INTO com_historial_eliminaciones (doc_id,no_documento,rut_proveedor,eli_fecha,eli_motivo,usu_id) VALUES (" & _
                        .SubItems(7) & "," & .SubItems(4) & ",'" & .SubItems(8) & "','" & Format(Now, "YYYY-DD-MM HH:MM:SS") & "','" & X & "'," & RsTmp3!usu_id & ")"
            
       ' End With
            
    Else
        MsgBox "Contrase�a ingresada no fue encontrada...", vbInformation
        Exit Sub
    End If
    On Error GoTo ErrorElimina
    cn.BeginTrans 'COMENZAMOS LA TRANSACCION
    cn.Execute Sql
    
    Sql = "DELETE FROM cta_abonos " & _
          "WHERE abo_cli_pro='CLI' AND rut_emp='" & SP_Rut_Activo & "' AND abo_id=" & Lp_IdAbono
    cn.Execute Sql
    Sql = "DELETE FROM cta_abono_documentos " & _
          "WHERE rut_emp='" & SP_Rut_Activo & "' AND abo_id=" & Lp_IdAbono
    cn.Execute Sql
    
     'Tambien eliminaremos los chueque que este en cartera
    '29-11-2014
    cn.Execute "DELETE FROM abo_cheques " & _
                "WHERE rut_emp='" & SP_Rut_Activo & "' AND abo_id=" & Lp_IdAbono
    
    
    Sql = "SELECT rut_cliente rut,nombre_cliente nombre_proveedor,doc_movimiento inventario," & _
                 "no_documento,c.doc_id,doc_nombre,bod_id,doc_factura_guias,ven_nc_utilizada,id " & _
          "FROM ven_doc_venta c,sis_documentos d " & _
          "WHERE d.doc_id=c.doc_id AND id=" & L_Unico
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        Sp_Bodega = RsTmp!bod_id
        s_FacturaGuias = RsTmp!doc_factura_guias
        If RsTmp!Inventario = "ENTRADA" Then s_mov = "SALIDA"
        If RsTmp!Inventario = "SALIDA" Then s_mov = "ENTRADA"
        
        Sql = "SELECT d.codigo pro_codigo,unidades cantidad," & _
              "IFNULL((SELECT pro_precio_neto*d.unidades " & _
               "FROM inv_kardex k " & _
               "WHERE k.rut_emp='" & SP_Rut_Activo & "' AND k.pro_codigo=d.codigo ORDER BY kar_id DESC LIMIT 1),0) total_linea " & _
              "FROM ven_detalle d " & _
              "INNER JOIN maestro_productos m ON d.codigo=m.codigo " & _
              "WHERE m.rut_emp='" & SP_Rut_Activo & "' AND  pro_inventariable='SI' AND d.rut_emp='" & SP_Rut_Activo & "' AND no_documento=" & RsTmp!no_documento & " AND doc_id=" & RsTmp!doc_id
        Consulta rs_Detalle, Sql
        If rs_Detalle.RecordCount > 0 Then
            rs_Detalle.MoveFirst
            Do While Not rs_Detalle.EOF
                Kardex Format(Date, "YYYY-MM-DD"), s_mov, RsTmp!doc_id, RsTmp!no_documento, Sp_Bodega, _
                    rs_Detalle!pro_codigo, rs_Detalle!cantidad, "ANULA " & RsTmp!doc_nombre & " Nro:" & RsTmp!no_documento, Round(rs_Detalle!total_linea / rs_Detalle!cantidad), _
                    rs_Detalle!total_linea, RsTmp!Rut, RsTmp!nombre_proveedor, "NO", Round(rs_Detalle!total_linea / rs_Detalle!cantidad), , , , , 0
                    
                rs_Detalle.MoveNext
            Loop
        End If
        
        '28 Sep 2013
        'DEBEMOS DETECTAR SI LA NOTA DE CREDITO SE UTILIZO PARA ABONAR A SU DOCUMENTO ORIGINAL
        If RsTmp!ven_nc_utilizada = "SI" Then
            'con esto sabemos q se trata de una nota de credito, y podemos referenciar a la tabla de abonos
            Sql = "DELETE FROM cta_abonos " & _
                    "WHERE abo_cli_pro='CLI' AND rut_emp='" & SP_Rut_Activo & "' AND id_ref=" & RsTmp!ID
            cn.Execute Sql
        End If
        
        'DEBEMOS CONSULTAR SI EXISTEN DOCUMENTOS QUE HACEN REFERENCIA AL QUE SE ESTA INGRESANDO
        Sql = "UPDATE ven_doc_venta " & _
                     "SET id_ref=0,doc_id_factura=0,nro_factura=0 " & _
              "WHERE id_ref=" & L_Unico
        cn.Execute Sql
        
                
        'DEBEMOS CONSULTAR SI EXISTEN DOCUMENTOS FACTURADOS CON ESTE DOCUMENTO PARA LIBERARLOS Abril 2012
        If s_FacturaGuias = "SI" Then
            Sql = "UPDATE ven_doc_venta " & _
                     "SET id_ref=0,doc_id_factura=0,nro_factura=0 " & _
                  "WHERE nro_factura=" & RsTmp!no_documento & " AND doc_id_factura=" & RsTmp!doc_id & " " & _
                       "AND rut_emp='" & SP_Rut_Activo & "'"
            cn.Execute Sql
        End If
                
                
                
                
        If RsTmp!Inventario = "NN" Then
            '28 Abril 2011
            'No modifica inventario
            'por lo tanto podriamos eliminar el documeneto sin tocar el
            'kardex ni el stock
        End If
        'multidelete
        
        If SP_Control_Inventario = "SI" Then
            'Antes de eliminar los registros
            'Debemos saber si hay algun activo inmovilizado
            Sql = "SELECT aim_id " & _
                    "FROM ven_detalle d " & _
                    "JOIN ven_doc_venta v ON (v.no_documento=d.no_documento AND v.doc_id=d.doc_id) " & _
                    "WHERE aim_id>0 AND v.rut_emp='" & SP_Rut_Activo & "' AND  v.id=" & L_Unico
            Consulta RsTmp, Sql
            If RsTmp.RecordCount > 0 Then
                RsTmp.MoveFirst
                Do While Not RsTmp.EOF
                    cn.Execute "UPDATE con_activo_inmovilizado " & _
                                    "SET aim_fecha_egreso=Null,aim_habilitado='SI' " & _
                                    "WHERE aim_id=" & RsTmp!aim_id
                    RsTmp.MoveNext
                Loop
            End If
        
        
            Sql = "DELETE FROM " & _
                  "ven_doc_venta, " & _
                  "ven_detalle " & _
                  "USING ven_doc_venta " & _
                  "LEFT JOIN ven_detalle ON (ven_doc_venta.no_documento=ven_detalle.no_documento AND ven_doc_venta.doc_id=ven_detalle.doc_id) " & _
                  "WHERE ven_doc_venta.rut_emp='" & SP_Rut_Activo & "' AND  ven_doc_venta.id=" & L_Unico
            'Consulta RsTmp, Sql
        Else
            'Antes de eliminar los registros
            'Debemos saber si hay algun activo inmovilizado
            Sql = "SELECT aim_id " & _
                    "FROM ven_sin_detalle " & _
                    "WHERE aim_id>0 AND id=" & L_Unico
            Consulta RsTmp, Sql
            If RsTmp.RecordCount > 0 Then
                RsTmp.MoveFirst
                Do While Not RsTmp.EOF
                    cn.Execute "UPDATE con_activo_inmovilizado " & _
                                    "SET aim_fecha_egreso=Null,aim_habilitado='SI' " & _
                                    "WHERE aim_id=" & RsTmp!aim_id
                    RsTmp.MoveNext
                Loop
            End If
            
        
        
            Sql = "DELETE FROM " & _
                  "ven_doc_venta, " & _
                  "ven_sin_detalle " & _
                  "USING ven_doc_venta " & _
                  "LEFT JOIN ven_sin_detalle USING(id) " & _
                  "WHERE ven_doc_venta.id=" & L_Unico
            'Consulta RsTmp, Sql
        End If
        cn.Execute Sql
    End If
    
    EliminaAbonosDeNC "CLI", Val(DG_ID_Unico)
    
    
    
                 Sql = "INSERT INTO ven_doc_venta (id,fecha,doc_id,no_documento,rut_emp,rut_cliente,tipo_doc,nombre_cliente," & _
                                          "tipo_movimiento,usu_nombre,sue_id,caj_id) VALUES(" & _
                        DG_ID_Unico & "," & _
                         "'" & Format(DtFecha.Value, "YYYY-MM-DD") & _
                         "'," & CboDocVenta.ItemData(CboDocVenta.ListIndex) & _
                         "," & TxtNroDocumento & _
                         ",'" & SP_Rut_Activo & _
                         "','NULO' " & _
                         ",'" & CboDocVenta.Text & _
                         "','ANULADO','NULO','" & Sp_Llave & "'," & IG_id_Sucursal_Empresa & "," & LG_id_Caja & ")"
             cn.Execute Sql
             AutoIncremento "GUARDA", CboDocVenta.ItemData(CboDocVenta.ListIndex), TxtNroDocumento, IG_id_Sucursal_Empresa
    
    
    cn.CommitTrans
    Unload Me
    Exit Sub
ErrorElimina:
    MsgBox "Ocurrio un error al eliminar documento"
    cn.RollbackTrans
End Sub





'Private Sub TxtSubTotal_Change()

'End Sub

'Private Sub TxtSubTotal_Change()


Private Sub TxtTotalDescuento_Validate(Cancel As Boolean)
    SkBrutoMateriales = txtOriginal - CDbl(Me.TxtTotalDescuento)
    
   txtNuevoDescuento = (Val(txtOriginal) - CDbl(SkBrutoMateriales)) / Val(txtOriginal) * 100
End Sub


Private Sub FacturaElectronica(Tipo As String)
    Dim Sp_BrutoNeto As String
    Dim Sp_Xml As String, Sp_XmlAdicional As String
    Dim obj As DTECloud.Integracion
    Dim Lp_Sangria As Long
    Dim Sp_Sql As String
    Dim Bp_Adicional As Boolean
    '13-9-2014
    'Modulo genera factura electronica
    'Autor: alvamar
    Bp_Adicional = False
    If Len(Sm_SoloExento) = 0 Then Sm_SoloExento = "NO"
    '11 Abril 2015
    'La factuar es por guias.
    If Bm_Factura_por_Guias = True Then
        '**************************************************
        'Factura por guias 6 Octubre 2011
        '**************************************************
        LvMateriales.ListItems.Add , , ""
        LvMateriales.ListItems(LvMateriales.ListItems.Count).SubItems(3) = "SEGUN GUIAS NRO"
        Sql = "SELECT no_documento,ven_ordendecompra " & _
              "FROM ven_doc_venta " & _
              "WHERE  rut_emp='" & SP_Rut_Activo & "' AND doc_id_factura=" & CboDocVenta.ItemData(CboDocVenta.ListIndex) & " AND nro_factura=" & Me.TxtNroDocumento
        Consulta RsTmp, Sql
        Sp_Segun_Guias = Empty
        If RsTmp.RecordCount > 0 Then
        
            Sp_GuiasFacturadas = Empty
            RsTmp.MoveFirst
            cont = 1
            Sp_Segun_Guias = "SEGUN GUIAS "
            Do While Not RsTmp.EOF
                                
                Sp_Segun_Guias = Sp_Segun_Guias & RsTmp!no_documento & ","
                RsTmp.MoveNext
            Loop
            Sp_Segun_Guias = Mid(Sp_Segun_Guias, 1, Len(Sp_Segun_Guias) - 1)
            If Sp_GuiasFacturadas <> Empty Then
                    LvMateriales.ListItems.Add , , ""
                    LvMateriales.ListItems(LvMateriales.ListItems.Count).SubItems(3) = Mid(Sp_GuiasFacturadas, 1, Len(Sp_GuiasFacturadas) - 1)
            End If
                    
            
            Sql = "SELECT neto,iva,bruto,id " & _
                  "FROM ven_doc_venta " & _
                  "WHERE  rut_emp='" & SP_Rut_Activo & "' AND doc_id=" & CboDocVenta.ItemData(CboDocVenta.ListIndex) & " AND no_documento=" & TxtNroDocumento
            Consulta RsTmp, Sql
            If RsTmp.RecordCount > 0 Then
                Me.SkBrutoMateriales = NumFormat(RsTmp!bruto)
                SkTotalMateriales = NumFormat(RsTmp!Neto)
                SkIvaMateriales = NumFormat(RsTmp!Iva)
                Lp_Id_Nueva_Venta = RsTmp!ID
            End If
            
            If Len(Sp_Segun_Guias) > 0 Then
            '    If SP_Rut_Activo = "11.907.734-6" Then
                    LvMateriales.ListItems.Clear
                    LvMateriales.ListItems.Add , , ""
                    LvMateriales.ListItems(LvMateriales.ListItems.Count).SubItems(3) = Sp_Segun_Guias
                    LvMateriales.ListItems(LvMateriales.ListItems.Count).SubItems(6) = 1
                    LvMateriales.ListItems(LvMateriales.ListItems.Count).SubItems(4) = CDbl(Me.SkTotalMateriales)
                     LvMateriales.ListItems(LvMateriales.ListItems.Count).SubItems(8) = CDbl(Me.SkTotalMateriales)
                
                    TxtComentario = ""
            '    End If
            End If
           
        End If
        ' Fin
    End If
    If Bm_Factura_por_Guias Then
        'If SP_Rut_Activo = "11.907.734-6" Then
            'no traiga detalle esta listo
        'Else
        If SG_Presentacion_Factura = "SEGUN" Then
        'VIENE YA LA INFO DE LAS GUIAS
        Else
                DetalleParaFacturaGuias "xx" ' LvMateriales.ListItems.Clear
        End If
    End If
    '1ro Crear xml
    Lp_Sangria = 4
    X = FreeFile
    
    Sp_Xml = App.Path & "\dte\" & CboDocVenta.ItemData(CboDocVenta.ListIndex) & "_" & TxtNroDocumento & ".xml"
    Sp_Xml = "C:\FACTURAELECTRONICA\nueva.xml"
      'Open Sp_Archivo For Output As X
    Open Sp_Xml For Output As X
        '<?xml version="1.0" encoding="ISO-8859-1"?>
        Print #X, "<DTE version=" & Chr(34) & "1.0" & Chr(34) & ">"
        Print #X, "" & Space(Lp_Sangria) & "<Documento ID=" & Chr(34) & "ID" & Lp_Id_Nueva_Venta & Chr(34) & ">"
        'Encabezado
        Print #X, "" & Space(Lp_Sangria) & "<Encabezado>"
            'id documento
            Print #X, "" & Space(Lp_Sangria * 2) & "<IdDoc>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<TipoDTE>" & Trim(Tipo) & "</TipoDTE>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<Folio>" & Trim(TxtNroDocumento) & "</Folio>"
                
                
                Print #X, "" & Space(Lp_Sangria * 3) & "<FchEmis>" & Fql(DtFecha) & "</FchEmis>"
                
                If Me.CboTipoDespacho.Visible = True Then
                    Print #X, "" & Space(Lp_Sangria * 3) & "<TipoDespacho>" & Me.CboTipoDespacho.ItemData(CboTipoDespacho.ListIndex) & "</TipoDespacho>"
                    Print #X, "" & Space(Lp_Sangria * 3) & "<IndTraslado>" & Me.CboTipoTraslado.ItemData(CboTipoTraslado.ListIndex) & "</IndTraslado>"
                End If
                fmapago = 2
                If Mid(CboPlazos.Text, 1, 7) = "CONTADO" Then
                    fmapago = 1
                End If
                ' Print #X, "" & Space(Lp_Sangria * 3) & "<FmaPago>" & fmapago & "</FmaPago>"
           '     Print #X, "" & Space(Lp_Sangria * 3) & "<FchVenc>" & Format(DtFecha.Value + CboPlazos.ItemData(CboPlazos.ListIndex), "; YYYY - MM - DD; ") & "</FchVenc>"
                If SP_Rut_Activo = "6.030.088-7" Then
                    Sm_Con_Precios_Brutos = "SI"
                End If
                If Sm_Con_Precios_Brutos = "SI" Then
                    '30 Mayo 2015, cuando las lineas de detalle son precios brutos se debe identificar aqui.
                    Print #X, "" & Space(Lp_Sangria * 2) & "<MntBruto>1</MntBruto>"
                End If
                If Me.ChkActivoFijo.Value = 1 Then
                    Print #X, "" & Space(Lp_Sangria * 3) & "<TpoTranVenta>2</TpoTranVenta>"
                End If
            Print #X, "" & Space(Lp_Sangria * 2) & "</IdDoc>"
            'Emisor
            'Select empresa para obtener los datos del emisor
            Sp_Sql = "SELECT nombre_empresa,direccion,comuna,ciudad,giro,emp_codigo_actividad_economica actividad " & _
                    "FROM sis_empresas " & _
                    "WHERE rut='" & SP_Rut_Activo & "'"
            Consulta RsTmp, Sp_Sql
            
            Print #X, "" & Space(Lp_Sangria * 2) & " <Emisor>"
                'Print #X, "" & Space(Lp_Sangria * 3) & "<RUTEmisor>" & Replace(SP_Rut_Activo, ".", "") & "</RUTEmisor>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<RUTEmisor>" & Replace(SP_Rut_Activo, ".", "") & "</RUTEmisor>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<RznSoc>" & QuitarAcentos(RsTmp!nombre_empresa) & "</RznSoc>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<GiroEmis>" & QuitarAcentos(RsTmp!giro) & "</GiroEmis>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<Acteco>" & Replace(RsTmp!actividad, "�", "N") & "</Acteco>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<DirOrigen>" & QuitarAcentos(RsTmp!direccion) & "</DirOrigen>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<CmnaOrigen>" & QuitarAcentos(RsTmp!comuna) & "</CmnaOrigen>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<CiudadOrigen>" & QuitarAcentos(RsTmp!ciudad) & "</CiudadOrigen>"
            Print #X, "" & Space(Lp_Sangria * 2) & "</Emisor>"
            
            'Receptor
            Print #X, "" & Space(Lp_Sangria * 2) & " <Receptor>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<RUTRecep>" & Replace(TxtRut, ".", "") & "</RUTRecep>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<RznSocRecep>" & Mid(QuitarAcentos(Me.TxtRazonSocial), 1, 100) & "</RznSocRecep>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<GiroRecep>" & Mid(QuitarAcentos(TxtGiro), 1, 40) & "</GiroRecep>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<Contacto>SM</Contacto>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<DirRecep>" & Mid(QuitarAcentos(TxtDireccion), 1, 60) & "</DirRecep>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<CmnaRecep>" & Mid(QuitarAcentos(TxtComuna), 1, 20) & "</CmnaRecep>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<CiudadRecep>" & Mid(QuitarAcentos(TxtCiudad), 1, 20) & "</CiudadRecep>"
            Print #X, "" & Space(Lp_Sangria * 2) & "</Receptor>"
            
             'Totales
                Print #X, "" & Space(Lp_Sangria * 2) & " <Totales>"
                
                    Sql = "SELECT doc_solo_exento " & _
                            "FROM sis_documentos " & _
                            "WHERE doc_id=" & CboDocVenta.ItemData(CboDocVenta.ListIndex)
                    Consulta RsTmp, Sql
                    If RsTmp.RecordCount > 0 Then
                        Sm_SoloExento = RsTmp!doc_solo_exento
                    End If
                    
                    
                    If Sm_SoloExento = "NO" Then
                        'MsgBox Sm_SoloExento
                    
                    'If SP_Rut_Activo <> "96.803.210-0" And SP_Rut_Activo <> "76.229.935-6" Then
                        'para cualquier empresa que no sea vega modelo, incluir NETO
                        '19 Mayo 2016
                        Print #X, "" & Space(Lp_Sangria * 3) & "<MntNeto>" & CDbl(SkTotalMateriales) & "</MntNeto>"
                    Else ' vega modelo, soc inmobiliaria e inversiones hettich
                        Print #X, "" & Space(Lp_Sangria * 3) & "<MntExe>" & CDbl(TxtExentos) & "</MntExe>"
                    End If
                    If Sm_SoloExento = "NO" Then
                    'If SP_Rut_Activo <> "96.803.210-0" Then
                        Print #X, "" & Space(Lp_Sangria * 3) & " <TasaIVA>" & DG_IVA & "</TasaIVA>"
                        Print #X, "" & Space(Lp_Sangria * 3) & " <IVA>" & CDbl(SkIvaMateriales) & "</IVA>"
                    Else
                        'Print #X, "" & Space(Lp_Sangria * 3) & " <TasaIVA>0</TasaIVA>"
                    End If
                    If Val(TxtIvaAnticipado) > 0 Then
                        'Antes del monto total debemos verificar si la factura incluye impuestos retenidos
        '                                         <ImptoReten>
        '                                            <TipoImp>18</TipoImp>
        '                                            <TasaImp>5</TasaImp>
        '                                            <MontoImp>8887</MontoImp>
        '                                        </ImptoReten>
                         Print #X, "" & Space(Lp_Sangria * 3) & " <ImptoReten>"
                         Print #X, "" & Space(Lp_Sangria * 6) & "<TipoImp>19</TipoImp>"
                         Print #X, "" & Space(Lp_Sangria * 6) & "<TasaImp>12</TasaImp>"
                        Print #X, "" & Space(Lp_Sangria * 6) & "<MontoImp>" & CDbl(TxtIvaAnticipado) & "</MontoImp>"
                               
                        Print #X, "" & Space(Lp_Sangria * 3) & " </ImptoReten>"
                    End If
                    
                '    If Me.ChkActivoFijo.Value = 1 Then
                        '20 Octubre 2020 Venta de activo fijo
                  '      Print #X, "" & Space(Lp_Sangria * 3) & " <MntActivoFijo>" & CDbl(SkTotalMateriales) & "</MntActivoFijo>"
                   '     Print #X, "" & Space(Lp_Sangria * 3) & " <MntIVAActivoFijo>" & CDbl(SkIvaMateriales) & "</MntIVAActivoFijo>"
                    '
'                    End If


                    Print #X, "" & Space(Lp_Sangria * 3) & "<MntTotal>" & CDbl(SkBrutoMateriales) & "</MntTotal>"
                Print #X, "" & Space(Lp_Sangria * 2) & "</Totales>"
                
            'Cerramos encabezado
            Print #X, "" & Space(Lp_Sangria) & "</Encabezado>"
           'Comenzamos el detalle
           If SP_Control_Inventario = "SI" Then
           'Recorremos la grilla con productos
                      For i = 1 To LvMateriales.ListItems.Count
                          Print #X, "" & Space(Lp_Sangria) & "<Detalle>"
                              Print #X, "" & Space(Lp_Sangria * 2) & "<NroLinDet>" & i & "</NroLinDet>"
                              Print #X, "" & Space(Lp_Sangria * 2) & " <CdgItem>"
                              Print #X, "" & Space(Lp_Sangria * 3) & "<TpoCodigo>PLU</TpoCodigo>"
                              
                              If SP_Rut_Activo = "76.115.474-5" Then
                                sql2 = "SELECT pro_codigo_interno " & _
                                        "FROM maestro_productos " & _
                                        "WHERE codigo=" & LvMateriales.ListItems(i).SubItems(1)
                                Consulta RsTmp2, sql2
                                If RsTmp2.RecordCount > 0 Then
                                    Print #X, "" & Space(Lp_Sangria * 3) & "<VlrCodigo>" & RsTmp2!pro_codigo_interno & "</VlrCodigo>"
                                End If
                                
                              Else
                                Print #X, "" & Space(Lp_Sangria * 3) & "<VlrCodigo>" & LvMateriales.ListItems(i).SubItems(1) & "</VlrCodigo>"
                              End If
                              
                              Print #X, "" & Space(Lp_Sangria * 2) & "</CdgItem>"
                              'If SP_Rut_Activo = "76.230.764-2" Or SP_Rut_Activo = "76.417.727-4" Or SP_Rut_Activo = "76.025.292-1" Then
                              If Len(LvMateriales.ListItems(i).SubItems(3)) > 80 Then
                              
                                     nombre = Mid(QuitarAcentos(LvMateriales.ListItems(i).SubItems(3)), 1, InStr(1, QuitarAcentos(LvMateriales.ListItems(i).SubItems(3)), " ", vbTextCompare))
                                    Print #X, "" & Space(Lp_Sangria * 2) & "<NmbItem>" & nombre & "</NmbItem>"
                                
                                    nombre = Mid(QuitarAcentos(LvMateriales.ListItems(i).SubItems(3)), InStr(1, QuitarAcentos(LvMateriales.ListItems(i).SubItems(3)), " ", vbTextCompare))
                                    Print #X, "" & Space(Lp_Sangria * 2) & "<DscItem>" & nombre & "</DscItem>"
                                
                              Else
                              
                                    Print #X, "" & Space(Lp_Sangria * 2) & "<NmbItem>" & Mid(QuitarAcentos(LvMateriales.ListItems(i).SubItems(3)), 1, 80) & "</NmbItem>"
                              End If
                              Print #X, "" & Space(Lp_Sangria * 2) & "<QtyItem>" & Trim(Replace(LvMateriales.ListItems(i).SubItems(6), ",", ".")) & "</QtyItem>"
                              
                              If SP_Rut_Activo = "11.907.734-6" Then
                                    'sandra ramirez
                                    Print #X, "" & Space(Lp_Sangria * 2) & "<PrcItem>" & CDbl(LvMateriales.ListItems(i).SubItems(4)) & "</PrcItem>"
                              ElseIf SP_Rut_Activo = "76.337.408-4" Or SP_Rut_Activo = "10.722.977-9" Or SP_Rut_Activo = "12.318.896-9" Or SG_Decimales = "SI" Then
                                      Print #X, "" & Space(Lp_Sangria * 2) & "<PrcItem>" & LvMateriales.ListItems(i).SubItems(4) & "</PrcItem>"
                              
                              Else
                                    'precio con dscto
                                    '4 mayo 2020  para  tyt  todos los item  deben ir con  precios netos
                                    If SP_Rut_Activo = "77.680.920-9" Then
                                         Print #X, "" & Space(Lp_Sangria * 2) & "<PrcItem>" & CxP(Round(CDbl(LvMateriales.ListItems(i).SubItems(7)) / Val("1." & DG_IVA), 2)) & "</PrcItem>"
                                    Else
                                    
                                        Print #X, "" & Space(Lp_Sangria * 2) & "<PrcItem>" & CDbl(LvMateriales.ListItems(i).SubItems(7)) & "</PrcItem>"
                                    End If
                              End If
              '                    <DescuentoPct>15</DescuentoPct>
              '                    <DescuentoMonto>4212</DescuentoMonto>
              '                             <!- Solo se indica el c�digo del impuesto adicional '
                              If Val(LvMateriales.ListItems(i).SubItems(23)) > 0 Then
                                    Print #X, "" & Space(Lp_Sangria * 2) & "<CodImpAdic>" & LvMateriales.ListItems(i).SubItems(23) & "</CodImpAdic>"
              '
                              End If
                              
                              If SP_Rut_Activo = "11.907.734-6" Then
                                    'precio sin descuento * cantidad
                                    'sandra ramirez
                                    If Bm_Factura_por_Guias Then
                                        Print #X, "" & Space(Lp_Sangria * 2) & "<MontoItem>" & CxP(CDbl(LvMateriales.ListItems(i).SubItems(8))) & "</MontoItem>"
                                    Else
                                        Print #X, "" & Space(Lp_Sangria * 2) & "<MontoItem>" & Int(Val(CxP(CDbl(LvMateriales.ListItems(i).SubItems(4)))) * Val(CxP(Trim(Replace(LvMateriales.ListItems(i).SubItems(6), ",", "."))))) & "</MontoItem>"
                                    End If
                              Else
                                                                  '4 mayo 2020  para  tyt  todos los item  deben ir con  precios netos
                                    If SP_Rut_Activo = "77.680.920-9" Then
                                        'actualiacion, solo aglunos con neto, otros con bruto, compo cli_contacto3 'bruto-neto'
                                        If TxtBrutoNetoTYT = "NETO" Then
                                            Print #X, "" & Space(Lp_Sangria * 2) & "<MontoItem>" & Round(CDbl(LvMateriales.ListItems(i).SubItems(8)) / Val("1." & DG_IVA), 0) & "</MontoItem>"
                                        Else
                                            Print #X, "" & Space(Lp_Sangria * 2) & "<MontoItem>" & CDbl(LvMateriales.ListItems(i).SubItems(8)) & "</MontoItem>"
                                        End If
                                    Else
                                        Print #X, "" & Space(Lp_Sangria * 2) & "<MontoItem>" & CDbl(LvMateriales.ListItems(i).SubItems(8)) & "</MontoItem>"
                                    End If
                              End If
                      
                          Print #X, "" & Space(Lp_Sangria) & "</Detalle>"
                      Next
            Else
                'CUANDO ES SIN INVENTARIO
            End If
            'Cerramos detalle
            'Si referencia a OC incluir en el modulo _
            21 -07 - 2016
            If CDbl(TxtTotalDescuento) > 0 Then
                Print #X, "" & Space(Lp_Sangria) & "<DscRcgGlobal>"
                Print #X, "" & Space(Lp_Sangria * 2) & "<NroLinDR>" & IIf(Len(TxtOrdenesCompra) > 0, 2, 1) & "</NroLinDR>"
                Print #X, "" & Space(Lp_Sangria * 2) & "<TpoMov>D</TpoMov>"
                Print #X, "" & Space(Lp_Sangria * 2) & "<GlosaDR>Dscto. Cliente</GlosaDR>"
                Print #X, "" & Space(Lp_Sangria * 2) & "<TpoValor>$</TpoValor>"
                Print #X, "" & Space(Lp_Sangria * 2) & "<ValorDR>" & CDbl(TxtTotalDescuento) & "</ValorDR>"
                Print #X, "" & Space(Lp_Sangria) & "</DscRcgGlobal>"
            End If
            
            If Len(TxtOrdenesCompra) > 0 Or Me.LvGuiasReferencia.ListItems.Count > 0 Or Len(TxtNotasPedido) > 0 Then
                    
                        nrefe = 1
                        If Len(TxtOrdenesCompra) > 0 Then
                            Print #X, "" & Space(Lp_Sangria) & "<Referencia>"
                                 Print #X, "" & Space(Lp_Sangria * 2) & "<NroLinRef>" & nrefe & " </NroLinRef>"
                                 Print #X, "" & Space(Lp_Sangria * 2) & "<TpoDocRef>801</TpoDocRef>"
                                 Print #X, "" & Space(Lp_Sangria * 2) & "<FolioRef>" & TxtOrdenesCompra & "</FolioRef>"
                                 Print #X, "" & Space(Lp_Sangria * 2) & "<FchRef>" & Fql(DtFechaOC) & "</FchRef>"
                                nrefe = 2
                                
                           Print #X, "" & Space(Lp_Sangria) & "</Referencia>"
                        End If
                        
                         If Len(TxtNotasPedido) > 0 Then
                            Print #X, "" & Space(Lp_Sangria) & "<Referencia>"
                                 Print #X, "" & Space(Lp_Sangria * 2) & "<NroLinRef>" & nrefe & " </NroLinRef>"
                                 Print #X, "" & Space(Lp_Sangria * 2) & "<TpoDocRef>802</TpoDocRef>"
                                 Print #X, "" & Space(Lp_Sangria * 2) & "<FolioRef>" & TxtNotasPedido & "</FolioRef>"
                                 Print #X, "" & Space(Lp_Sangria * 2) & "<FchRef>" & Fql(DtFechaNP) & "</FchRef>"
                                nrefe = 3
                                
                           Print #X, "" & Space(Lp_Sangria) & "</Referencia>"
                        End If
                        If Me.LvGuiasReferencia.ListItems.Count > 0 Then
                                For i = 1 To LvGuiasReferencia.ListItems.Count
                                  Print #X, "" & Space(Lp_Sangria) & "<Referencia>"
                            
                                     Print #X, "" & Space(Lp_Sangria * 2) & "<NroLinRef>" & nrefe & "</NroLinRef>"
                                     Print #X, "" & Space(Lp_Sangria * 2) & "<TpoDocRef>" & LvGuiasReferencia.ListItems(i).SubItems(5) & "</TpoDocRef>"
                                     Print #X, "" & Space(Lp_Sangria * 2) & "<FolioRef>" & LvGuiasReferencia.ListItems(i).SubItems(2) & "</FolioRef>"
                                     Print #X, "" & Space(Lp_Sangria * 2) & "<FchRef>" & Fql(LvGuiasReferencia.ListItems(i).SubItems(3)) & "</FchRef>"
                                     nrefe = nrefe + 1
                                     Print #X, "" & Space(Lp_Sangria) & "</Referencia>"
                                Next
                        End If
            End If
                
        If FrmReferencia.Visible Then
                'CREAR REFERENCIA
                    If Val(SkCodigoReferencia) = 4 Then SkCodigoReferencia = 3
                    SkFechaReferencia = Format(Me.SkFechaReferencia, "YYYY-MM-DD")
                          '1  SkDocIdReferencia = 33
                
                    Print #X, "" & Space(Lp_Sangria) & "<Referencia>"
                    Print #X, "" & Space(Lp_Sangria * 2) & "<NroLinRef>1</NroLinRef>"
                    Print #X, "" & Space(Lp_Sangria * 2) & "<TpoDocRef>" & SkDocIdReferencia & "</TpoDocRef>"
                    Print #X, "" & Space(Lp_Sangria * 2) & "<FolioRef>" & TxtNroReferencia & "</FolioRef>"
                        
                    Print #X, "" & Space(Lp_Sangria * 2) & "<FchRef>" & SkFechaReferencia & "</FchRef>"
                    Print #X, "" & Space(Lp_Sangria * 2) & "<CodRef>" & SkCodigoReferencia & "</CodRef>"
                    Print #X, "" & Space(Lp_Sangria * 2) & "<RazonRef>" & TxtMotivoRef & "</RazonRef>"
                       
                Print #X, "" & Space(Lp_Sangria) & "</Referencia>"
            'CERRAR REFERNCIA
        End If

        Print #X, "" & Space(Lp_Sangria) & "</Documento>"
        Print #X, "" & Space(Lp_Sangria) & "</DTE>"
    Close #X
        
        
  '  If Len(TxtComentario) > 0 Or Len(TxtOrdenesCompra) > 0 Then
        Bp_Adicional = True
        X = FreeFile
        Sp_XmlAdicional = "C:\FACTURAELECTRONICA\nueva_adicional.xml"
        Open Sp_XmlAdicional For Output As X
            Print #X, "<Adicional>"
                   If Len(TxtComentario) > 0 Then
                       Print #X, "" & Space(Lp_Sangria) & "<Uno>" & QuitarAcentos(TxtComentario) & "</Uno>"
                '       If Len(TxtOrdenesCompra) > 0 Then
                '           Print #X, "" & Space(Lp_Sangria) & "<Dos>" & TxtOrdenesCompra & "</Dos>"
                   End If
                '   Else
                   If Len(TxtOrdenesCompra) > 0 Then
                           Print #X, "" & Space(Lp_Sangria) & "<Dos>" & TxtOrdenesCompra & "</Dos>"
                           'Print #X, "" & Space(Lp_Sangria) & "<Uno>" & TxtOrdenesCompra & "</Uno>"
                    '   End If
                   End If
                   Print #X, "" & Space(Lp_Sangria) & "<Tres>" & Mid(CboPlazos.Text, 1, Len(CboPlazos.Text) - 10) & "</Tres>"
                   Print #X, "" & Space(Lp_Sangria) & "<Cuatro>" & "</Cuatro>"
                   Print #X, "" & Space(Lp_Sangria) & "<Cinco>" & TxtSubTotalMateriales & "</Cinco>"
                   Print #X, "" & Space(Lp_Sangria) & "<Seis>" & "</Seis>"
                    If SP_Rut_Activo = "76.230.764-2" Then
                        'PUMALAL
                        'IVA CONSTRUCTORA
                        If Val(TxtCreditoEmpresaConstructora) > 0 Then
                             Print #X, "" & Space(Lp_Sangria * 3) & " <Siete>" & NumFormat(CDbl(TxtCreditoEmpresaConstructora)) & "</Siete>"
                        End If
                        
                    End If
                   
                   Print #X, "" & Space(Lp_Sangria) & "<Ocho>" & "</Ocho>"
                   Print #X, "" & Space(Lp_Sangria) & "<Nueve>" & "</Nueve>"
                   Print #X, "" & Space(Lp_Sangria) & "<Diez>" & "</Diez>"
                   
                   
'******************************************************************************************************************************************
                   If SP_Rut_Activo = "77.328.790-2" Then
                    'sta barbara
                        If CDbl(TxtIvaAnticipado) > 0 Then
                            elcod40 = 0 'whisky
                            For i = 1 To LvMateriales.ListItems.Count
                                If LvMateriales.ListItems(i).SubItems(23) = 40 Then
                                    elcod40 = elcod40 + CDbl(LvMateriales.ListItems(i).SubItems(22))
                                    
                                End If
                            Next
                            If elcod40 > 0 Then Print #X, "" & Space(Lp_Sangria) & "<Diecisiete>" & NumFormat(elcod40) & "</Diecisiete>"
                            
                            elcod24 = 0 'licores
                            For i = 1 To LvMateriales.ListItems.Count
                                If LvMateriales.ListItems(i).SubItems(23) = 24 Then
                                    elcod24 = elcod24 + CDbl(LvMateriales.ListItems(i).SubItems(22))
                                    
                                End If
                            Next
                            If elcod24 > 0 Then Print #X, "" & Space(Lp_Sangria) & "<Veintitres>" & NumFormat(elcod24) & "</Veintitres>"
                            
                            
                            elcod25 = 0 'vinos
                            For i = 1 To LvMateriales.ListItems.Count
                                If LvMateriales.ListItems(i).SubItems(23) = 25 Then
                                    elcod25 = elcod25 + CDbl(LvMateriales.ListItems(i).SubItems(22))
                                    
                                End If
                            Next
                            If elcod25 > 0 Then Print #X, "" & Space(Lp_Sangria) & "<Once>" & NumFormat(elcod25) & "</Once>"
                            
                            
                            elcod26 = 0 'Cervezas
                            For i = 1 To LvMateriales.ListItems.Count
                                If LvMateriales.ListItems(i).SubItems(23) = 26 Then
                                    elcod26 = elcod26 + CDbl(LvMateriales.ListItems(i).SubItems(22))
                                    
                                End If
                            Next
                            If elcod26 > 0 Then Print #X, "" & Space(Lp_Sangria) & "<Veintiuno>" & NumFormat(elcod26) & "</Veintiuno>"
                            
                            elcod27 = 0 'Bebidas
                            For i = 1 To LvMateriales.ListItems.Count
                                If LvMateriales.ListItems(i).SubItems(23) = 27 Then
                                    elcod27 = elcod27 + CDbl(LvMateriales.ListItems(i).SubItems(22))
                                    
                                End If
                            Next
                            If elcod27 > 0 Then Print #X, "" & Space(Lp_Sangria) & "<Trece>" & NumFormat(elcod27) & "</Trece>"
                            
                            
                            elcod35 = 0 'Jugos
                            For i = 1 To LvMateriales.ListItems.Count
                                If LvMateriales.ListItems(i).SubItems(23) = 35 Then
                                    elcod35 = elcod35 + CDbl(LvMateriales.ListItems(i).SubItems(22))
                                    
                                End If
                            Next
                            If elcod35 > 0 Then Print #X, "" & Space(Lp_Sangria) & "<Diecinueve>" & NumFormat(elcod35) & "</Diecinueve>"
                            
                             elcod60 = 0 'beb analcoholicas
                            For i = 1 To LvMateriales.ListItems.Count
                                If LvMateriales.ListItems(i).SubItems(23) = 60 Then
                                    elcod60 = elcod60 + CDbl(LvMateriales.ListItems(i).SubItems(22))
                                    
                                End If
                            Next
                            If elcod60 > 0 Then Print #X, "" & Space(Lp_Sangria) & "<Veinticinco>" & NumFormat(elcod60) & "</Veinticinco>"
                            
                            
                             elcod50 = 0 'Pisco
                            For i = 1 To LvMateriales.ListItems.Count
                                If LvMateriales.ListItems(i).SubItems(23) = 50 Then
                                    elcod50 = elcod50 + CDbl(LvMateriales.ListItems(i).SubItems(22))
                                    
                                End If
                            Next
                            If elcod50 > 0 Then Print #X, "" & Space(Lp_Sangria) & "<Quince>" & NumFormat(elcod50) & "</Quince>"
                            
                            
                        End If
                        'ilas
                       ' Print #X, "" & Space(Lp_Sangria) & "<Once>1.200</Once>"
                      '  Print #X, "" & Space(Lp_Sangria) & "<Doce>BEBIDAS 10%</Doce>"
                        
                       '  Print #X, "" & Space(Lp_Sangria) & "<Trece>350</Trece>"
                      '  Print #X, "" & Space(Lp_Sangria) & "<Catorce>WHISHKI 30.5%</Catorce>"
                        
                        
                   End If
'******************************************************************************************************************************************
'******************************************************************************************************************************************
                   If SP_Rut_Activo = "76.263.763-4" Then
                    'pachipap
                    
                        If CDbl(TxtIvaAnticipado) > 0 Then
                            elcod5 = 0 'carne
                            For i = 1 To LvMateriales.ListItems.Count
                                If LvMateriales.ListItems(i).SubItems(23) = 18 Then
                                    elcod5 = elcod5 + CDbl(LvMateriales.ListItems(i).SubItems(22))
                                    
                                End If
                            Next
                            If elcod5 > 0 Then Print #X, "" & Space(Lp_Sangria) & "<Trece>" & NumFormat(elcod5) & "</Trece>"
                            
                            elcod12 = 0 'Harina
                            For i = 1 To LvMateriales.ListItems.Count
                                If LvMateriales.ListItems(i).SubItems(23) = 12 Then
                                    elcod12 = elcod12 + CDbl(LvMateriales.ListItems(i).SubItems(22))
                                    
                                End If
                            Next
                            If elcod12 > 0 Then Print #X, "" & Space(Lp_Sangria) & "<Quince>" & NumFormat(elcod12) & "</Quince>"
                            
                            
                            elcod25 = 0 'vinos y cer ezas
                            For i = 1 To LvMateriales.ListItems.Count
                                If LvMateriales.ListItems(i).SubItems(23) = 25 Then
                                    elcod25 = elcod25 + CDbl(LvMateriales.ListItems(i).SubItems(22))
                                    
                                End If
                            Next
                            If elcod25 > 0 Then Print #X, "" & Space(Lp_Sangria) & "<Once>" & NumFormat(elcod25) & "</Once>"
                            
                            
                                                    
                            elcod27 = 0 'Bebidas
                            For i = 1 To LvMateriales.ListItems.Count
                                If LvMateriales.ListItems(i).SubItems(23) = 17 Then
                                    elcod27 = elcod27 + CDbl(LvMateriales.ListItems(i).SubItems(22))
                                    
                                End If
                            Next
                            If elcod27 > 0 Then Print #X, "" & Space(Lp_Sangria) & "<Veintiuno>" & NumFormat(elcod27) & "</Veintiuno>"
                            
                            
                            
                            elcod35 = 0 'licroes
                            For i = 1 To LvMateriales.ListItems.Count
                                If LvMateriales.ListItems(i).SubItems(23) = 24 Then
                                    elcod35 = elcod35 + CDbl(LvMateriales.ListItems(i).SubItems(22))
                                    
                                End If
                            Next
                            If elcod35 > 0 Then Print #X, "" & Space(Lp_Sangria) & "<Diecisiete>" & NumFormat(elcod35) & "</Diecisiete>"
                            
                            
                            
                            
                        End If
                        'ilas
                       ' Print #X, "" & Space(Lp_Sangria) & "<Once>1.200</Once>"
                      '  Print #X, "" & Space(Lp_Sangria) & "<Doce>BEBIDAS 10%</Doce>"
                        
                       '  Print #X, "" & Space(Lp_Sangria) & "<Trece>350</Trece>"
                      '  Print #X, "" & Space(Lp_Sangria) & "<Catorce>WHISHKI 30.5%</Catorce>"
                        
                        
                   End If
    '*****************************************************************
                    'Espumas, incluye fono y mail en las facturas.
                   If SP_Rut_Activo = "7.242.181-7" Or SP_Rut_Activo = "76.610.750-8" Or SP_Rut_Activo = "77.416.270-4" Then
                      Print #X, "" & Space(Lp_Sangria) & "<Once>" & Me.LbTelefono & "</Once>"
                      Print #X, "" & Space(Lp_Sangria) & "<Doce>" & TxteMail & "</Doce>"
                   
                   End If
                       '*****************************************************************
                       '14-04-2020
                    'Espumas, En transportes, incluye datos de info adicional covid 19.
                   If SP_Rut_Activo = "77.416.270-4" Then
                        Print #X, "" & Space(Lp_Sangria) & "<Veinte>" & Me.TxtNombre & "</Veinte>"
                        Print #X, "" & Space(Lp_Sangria) & "<Veintiuno>" & Me.TxtRutConductor & "</Veintiuno>"
                        Print #X, "" & Space(Lp_Sangria) & "<Veintidos>" & Me.TxtPatente & "</Veintidos>"
                        Print #X, "" & Space(Lp_Sangria) & "<Veintitres>" & Me.DtFechaDespacho & "</Veintitres>"
                        Print #X, "" & Space(Lp_Sangria) & "<Veinticuatro>" & Me.TxtDestinoDespacho & "</Veinticuatro>"
                        Print #X, "" & Space(Lp_Sangria) & "<Veinticinco>" & Me.TxtRangoHorario & "</Veinticinco>"
                        Print #X, "" & Space(Lp_Sangria) & "<Veintiseis>" & Me.TxtProductoObjeto & "</Veintiseis>"
                   End If
                     
                     
            Print #X, "</Adicional>"
        Close #X
    
    
  '  End If
        
        Set obj = New DTECloud.Integracion
   
        obj.UrlServicioFacturacion = SG_Url_Factura_Electronica '"http://wsdtedesa.dyndns.biz/WSDTE/Service.asmx"
        obj.HabilitarDescarga = True
        obj.FormatoNombrePDF = "0"
        
        If Dir("C:\FACTURAELECTRONICA\PDF\" & Replace(SP_Rut_Activo, ".", ""), vbDirectory) = "" Then
            MsgBox "La carpeta no existe, se creara automaticamente"
            MkDir ("C:\FACTURAELECTRONICA\PDF\" & Replace(SP_Rut_Activo, ".", ""))
            
        End If
        
        obj.CarpetaDestino = "C:\FACTURAELECTRONICA\PDF\" & Replace(SP_Rut_Activo, ".", "")
        obj.Password = "1234"
        If Sm_Empresa_IVA_Anticipado = "SI" And Val(TxtIvaAnticipado) > 0 Then
            obj.TipoImpresionRDL = "6-7"
        End If
        
        If SP_Rut_Activo = "11.500.319-4" Or SP_Rut_Activo = "76.361.539-1" Or SP_Rut_Activo = "76.370.578-1" Then
            '16 Eneror 2016 _
            'Obtener 3 copias
            obj.TipoImpresionRDL = "1-2-1"
        
        End If
        
       If SP_Rut_Activo = "76.230.764-2" Then
                        'PUMALAL
                        'IVA CONSTRUCTORA
            If Val(TxtCreditoEmpresaConstructora) > 0 Then
                obj.TipoImpresionRDL = "7-8"
            End If
        End If
        
        'DTECLOUD obj.TipoImpresionRDL="6-7";
        obj.AsignarFolio = False
        obj.IncluirCopiaCesion = True
        
        'MSXML.DOMDocument
        Dim Documento As MSXML2.DOMDocument30
        Set Documento = New DOMDocument30
        
        Documento.preserveWhiteSpace = True
        Documento.Load (Sp_Xml)
        '  Dim oXMLAdicional As New xml.XMLDocument
        Dim documento2 As MSXML2.DOMDocument30
        Set documento2 = New DOMDocument30
        
        documento2.preserveWhiteSpace = True
        If Bp_Adicional Then
            documento2.Load (Sp_XmlAdicional)
        Else
            documento2.Load ("")
        End If
        obj.Productivo = BG_ModoProductivoDTE
        
        cn.Execute "UPDATE dte_folios SET dte_disponible='NO' " & _
                    "WHERE rut_emp='" & SP_Rut_Activo & "' AND doc_id=" & Trim(Tipo) & " AND dte_folio=" & TxtNroDocumento
        If obj.ProcesarXMLBasico(Documento.XML, documento2.XML) Then
             'MsgBox obj.URLPDF
           
             Archivo = "C:\FACTURAELECTRONICA\PDF\" & Replace(SP_Rut_Activo, ".", "") & "\T" & Trim(Tipo) & "F" & TxtNroDocumento & ".PDF"
             If Dir$(Archivo) = "" Then
             
                     dte_MostrarPDF.Dte Tipo, TxtNroDocumento, False
                    ' dte_MostrarPDF.Dte Tipo, TxtNroDocumento, True
             Else
                ShellExecute Me.hWnd, "open", Archivo, "", "", 4
            End If
             
             
            ' MsgBox obj.URLPDF
        Else
            MsgBox (CStr(obj.IDResultado) + " " + obj.DescripcionResultado)
        End If
        
        
        

        
End Sub


Private Function La_Establecer_Impresora(ByVal NamePrinter As String) As Boolean
  
On Error GoTo errSub
  
     
  
    'Variable de referencia
  
    Dim obj_Impresora As Object
  
     
  
    'Creamos la referencia
  
    Set obj_Impresora = CreateObject("WScript.Network")
  
        obj_Impresora.setdefaultprinter NamePrinter
  
     
  
    Set obj_Impresora = Nothing
  
         
  
        'La funci�n devuelve true y se cambi� con �xito
  
       La_Establecer_Impresora = True
  
  '      MsgBox "La impresora se cambi� correctamente", vbInformation
  
    Exit Function
  
     
  
     
  
'Error al cambiar la impresora
  
errSub:
  
If Err.Number = 0 Then Exit Function
  
   La_Establecer_Impresora = False
  
   MsgBox "error: " & Err.Number & Chr(13) & "Description: " & Err.Description
  
   On Error GoTo 0
  
End Function


Private Sub ImprimeFacturaVegaModelo()
    Dim Cx As Double, Cy As Double, Sp_Fecha As String, Ip_Mes As Integer, Dp_S As Double, Sp_Letras As String
    Dim Sp_Neto As String * 12, Sp_IVA As String * 12, Sp_Total As String * 12
    Dim Sp_GuiasFacturadas As String
    
    Dim p_Codigo As String * 5
    Dim p_Cantidad As String * 7
    Dim p_UM As String * 4
    Dim p_Detalle As String * 40
    Dim p_Unitario As String * 10
    Dim p_Total As String * 11
    
    
    On Error GoTo ProblemaImpresora
  '  Printer.ScaleMode = vbcentimter
    Printer.FontName = "Courier New"
    Printer.FontSize = 10
    Printer.ScaleMode = 7
        
    'Cx = 2 'horizontal
    'Cy = 5.9 'vertical
    'Dp_S = 0.35
    
    Sql = "SELECT sue_cx,sue_cy " & _
            "FROM sis_empresas_sucursales " & _
            "WHERE sue_id=" & IG_id_Sucursal_Empresa
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        Cx = Val(RsTmp!sue_cx)
        Cy = Val(RsTmp!sue_cy) 'vertical)
    Else
        Cx = 1 'horizontal
        Cy = 0.6
    
    End If
    
    
    Dp_S = 0.2
    
    Printer.CurrentX = Cx
    Printer.CurrentY = Cy
    Sp_Fecha = DtFecha.Value
    Ip_Mes = Val(Mid(Sp_Fecha, 4, 2))
    
    
    'FECHA
    Printer.CurrentY = Cy + 1.5
    Printer.CurrentX = Cx + 2
    
    
    pos = Printer.CurrentY
    Printer.Print Mid(Sp_Fecha, 1, 2) & "   " & UCase(MonthName(Val(Mid(Sp_Fecha, 4, 2)))) & "   " & Mid(Sp_Fecha, 7, 4)
    pos = pos + 2
   
    'CIUDAD
    Printer.CurrentY = pos
    Printer.CurrentX = Cx + 15
    Printer.Print "Ciudad: " & TxtCiudad


     'comuna
    Printer.CurrentY = pos
    Printer.CurrentX = Cx + 2
    Printer.Print "Comuna   : " & TxtComuna

    'RAZON SOCIAL
    Printer.CurrentX = Cx + 2
    Printer.CurrentY = Cy + 2.9
    pos = Printer.CurrentY
    Printer.Print "Cliente  : " & TxtRazonSocial

    
    'RUT
    Printer.CurrentX = Cx + 15
    Printer.CurrentY = pos
    pos = Printer.CurrentY
    Printer.Print "R.U.T.: " & TxtRut


        
   
    'DIRECCION
    Printer.CurrentX = Cx + 2
    Printer.CurrentY = Cy + 4
    pos = Printer.CurrentY
    posd = Printer.CurrentY
    Printer.Print "Direcci�n: " & TxtDireccion
    
    'TELEFONO
    Printer.CurrentY = pos
    Printer.CurrentX = Cx + 15
    Printer.Print "Fono  : " & Me.LbTelefono
    
    'GIRO
    Printer.CurrentY = Cy + 4.5
    Printer.CurrentX = Cx + 2
    pos = Printer.CurrentY
    Printer.Print "Giro     : " & CboGiros.Text ' MARIO ****   TxtGiro
   
    'COND. VENTA
    Printer.CurrentY = pos
    Printer.CurrentX = Cx + 15
    Printer.Print "Plazo : " & Mid(CboPlazos, 1, 20)
    
    
    '***
   ' Printer.CurrentX = Cx
   ' Printer.CurrentY = Printer.CurrentY + Dp_S - 0.2
  '  Printer.CurrentX = Printer.CurrentX + 10.9
   ' Printer.Print CboVendedores
    
    
    Printer.CurrentY = Printer.CurrentY + Dp_S - 0.3
    '***
    'Printer.CurrentY = Printer.CurrentY + Dp_S
    pos = Printer.CurrentY
    
    Printer.CurrentX = Cx + 8.5
    Printer.CurrentY = Printer.CurrentY + 1
       
    
    
    If Bm_Factura_por_Guias Then
        '**************************************************
        'Factura por guias Diciembre del 2013
        '**************************************************-
        Sql = "SELECT no_documento,id " & _
              "FROM ven_doc_venta " & _
              "WHERE  rut_emp='" & SP_Rut_Activo & "' AND doc_id_factura=" & CboDocVenta.ItemData(CboDocVenta.ListIndex) & " AND nro_factura=" & Me.TxtNroDocumento
        Consulta RsTmp, Sql
        
        If SG_Presentacion_Factura = "SEGUN" Then
                LvMateriales.ListItems.Add , , ""
                LvMateriales.ListItems(LvMateriales.ListItems.Count).SubItems(3) = "SEGUN GUIAS NRO"

                If RsTmp.RecordCount > 0 Then
                    Sp_GuiasFacturadas = Empty
                    RsTmp.MoveFirst
                    cont = 1
                    Do While Not RsTmp.EOF
                        Sp_GuiasFacturadas = Sp_GuiasFacturadas & RsTmp!no_documento & ","
                        cont = cont + 1
                        RsTmp.MoveNext
                        If cont = 3 Then
                            LvMateriales.ListItems.Add , , ""
                            LvMateriales.ListItems(LvMateriales.ListItems.Count).SubItems(3) = Mid(Sp_GuiasFacturadas, 1, Len(Sp_GuiasFacturadas) - 1)
                            Sp_GuiasFacturadas = Empty
                        End If
                    Loop
                    If Sp_GuiasFacturadas <> Empty Then
                            LvMateriales.ListItems.Add , , ""
                            LvMateriales.ListItems(LvMateriales.ListItems.Count).SubItems(3) = Mid(Sp_GuiasFacturadas, 1, Len(Sp_GuiasFacturadas) - 1)
                    End If
        
        
        
                    Sql = "SELECT neto,iva,bruto " & _
                          "FROM ven_doc_venta " & _
                          "WHERE  rut_emp='" & SP_Rut_Activo & "' AND doc_id=" & CboDocVenta.ItemData(CboDocVenta.ListIndex) & " AND no_documento=" & TxtNroDocumento
                    Consulta RsTmp, Sql
                    If RsTmp.RecordCount > 0 Then
                        Me.SkBrutoMateriales = NumFormat(RsTmp!bruto)
                        SkTotalMateriales = NumFormat(RsTmp!Neto)
                        SkIvaMateriales = NumFormat(RsTmp!Iva)
                    End If
                End If
        Else
            'Factura con detalle de productos
                
                If RsTmp.RecordCount > 0 Then
                    RsTmp.MoveFirst
                    Lm_Ids = ""
                    Do While Not RsTmp.EOF
                        Lm_Ids = Lm_Ids & RsTmp!ID & ","
                        RsTmp.MoveNext
                    Loop
                End If
                Lm_Ids = Mid(Lm_Ids, 1, Len(Lm_Ids) - 1)
                DetalleParaFacturaGuias Lm_Ids
                
                 Sql = "SELECT neto,iva,bruto " & _
                          "FROM ven_doc_venta " & _
                          "WHERE  rut_emp='" & SP_Rut_Activo & "' AND doc_id=" & CboDocVenta.ItemData(CboDocVenta.ListIndex) & " AND no_documento=" & TxtNroDocumento
                Consulta RsTmp, Sql
                If RsTmp.RecordCount > 0 Then
                    Me.SkBrutoMateriales = NumFormat(RsTmp!bruto)
                    SkTotalMateriales = NumFormat(RsTmp!Neto)
                    SkIvaMateriales = NumFormat(RsTmp!Iva)
                End If
            
                
                
        End If
        ' Fin
    End If
    Printer.CurrentY = Printer.CurrentY + 2.8
    
    For i = 1 To LvMateriales.ListItems.Count
        '6 - 3 - 7 - 8
       ' Printer.CurrentX = Printer.CurrentX + 0.07 'INTERLINIA ARTICULOS
       'esperar el interlinedo automtico
       ' Printer.CurrentX = Cx - 2
        Printer.CurrentX = Cx - 0.5
        
        p_Codigo = LvMateriales.ListItems(i).SubItems(1)
        RSet p_Cantidad = LvMateriales.ListItems(i).SubItems(6)
        p_UM = LvMateriales.ListItems(i).SubItems(21)
        p_Detalle = LvMateriales.ListItems(i).SubItems(3)
        RSet p_Unitario = LvMateriales.ListItems(i).SubItems(7)
        RSet p_Total = LvMateriales.ListItems(i).SubItems(8)
        
        Printer.Print "     " & "    " & p_Cantidad & "      " & p_Detalle & "    " & p_Unitario & "    " & p_Total
        'Printer.Print p_Codigo & "    " & p_Cantidad & "      " & p_Detalle & "    " & p_Unitario & "    " & p_Total
    Next
        
    If Bm_Factura_por_Guias Then LvMateriales.ListItems.Clear
    
    Printer.CurrentX = Cx - 1
    Printer.CurrentY = Cy + 14.2
    pos = Printer.CurrentY
        
    Printer.CurrentX = Cx + 1
    Printer.CurrentY = pos - 2.2
    Printer.Print TxtComentario
    
  '  pos = Printer.CurrentY
  
    Printer.CurrentX = Cx + 1
        
    'Printer.CurrentY = pos
    Printer.CurrentY = pos - 1.4
    
   
    
   ' mitad = Len(Sp_Letras) \ 2
   ' buscaespacio = InStr(mitad, Sp_Letras, " ")
   ' If buscaespacio > 0 Then
   '         Printer.Print Mid(Sp_Letras, 1, buscaespacio)
   '         Printer.CurrentX = Cx - 1
   '         Printer.CurrentY = Cy + 15.3
   '         Printer.Print Mid(Sp_Letras, buscaespacio)
   ' Else
   '           Printer.Print Sp_Letras
   ' End If
    
    
    Cy = 13
    
    RSet Sp_Neto = IIf(CDbl(SkTotalMateriales) = 0, "", SkTotalMateriales)
    RSet Sp_IVA = IIf(CDbl(SkIvaMateriales) = 0, "", SkIvaMateriales)
    RSet Sp_Total = SkBrutoMateriales
    pos = pos - 1.4
    
    
    Printer.CurrentX = Cx + 16.1
    Printer.CurrentY = Cy + 9.3
    Printer.Print Sp_Neto
    
    Printer.CurrentX = Cx + 16.1
    Printer.CurrentY = Cy + 9.8
    Printer.Print Sp_IVA
    
    Printer.CurrentX = Cx + 16.1
    Printer.CurrentY = Cy + 9.3
    Printer.Print Sp_Total
    
    Printer.CurrentX = Cx + 2
    Printer.CurrentY = Cy + 10.1
    Sp_Letras = UCase(BrutoAletras(CDbl(SkBrutoMateriales), True))
    Printer.Print Sp_Letras
    Printer.NewPage
    Printer.EndDoc
    Exit Sub
ProblemaImpresora:
    MsgBox "Ocurrio un error al intentar imprimir..." & vbNewLine & Err.Description & vbNewLine & Err.Number

End Sub

Private Sub ImprimeFacturaTotalGomas()
    Dim Cx As Double, Cy As Double, Sp_Fecha As String, Ip_Mes As Integer, Dp_S As Double, Sp_Letras As String
    Dim Sp_Neto As String * 12, Sp_IVA As String * 12, Sp_Total As String * 12
    Dim Sp_GuiasFacturadas As String
    
    Dim p_Codigo As String * 5
    Dim p_Cantidad As String * 7
    Dim p_UM As String * 4
    Dim p_Detalle As String * 40
    Dim p_Unitario As String * 10
    Dim p_Total As String * 11
    
    
    On Error GoTo ProblemaImpresora
  '  Printer.ScaleMode = vbcentimter
    Printer.FontName = "Courier New"
    Printer.FontSize = 10
    Printer.ScaleMode = 7
        
    'Cx = 2 'horizontal
    'Cy = 5.9 'vertical
    'Dp_S = 0.35
    
    Sql = "SELECT sue_cx,sue_cy " & _
            "FROM sis_empresas_sucursales " & _
            "WHERE sue_id=" & IG_id_Sucursal_Empresa
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        Cx = Val(RsTmp!sue_cx)
        Cy = Val(RsTmp!sue_cy) 'vertical)
    Else
        Cx = 1 'horizontal
        Cy = 2.4
    
    End If
    
    
    Dp_S = 0.23
    
    Printer.CurrentX = Cx
    Printer.CurrentY = Cy
    Sp_Fecha = DtFecha.Value
    Ip_Mes = Val(Mid(Sp_Fecha, 4, 2))
    
    
    'FECHA
    Printer.CurrentY = 4.7
    Printer.CurrentX = 2.4
    
    
    pos = Printer.CurrentY
    Printer.Print Mid(Sp_Fecha, 1, 2) & "   " & UCase(MonthName(Val(Mid(Sp_Fecha, 4, 2)))) & "                  " & Mid(Sp_Fecha, 7, 4)
    pos = pos + Dp_S
   
    'RAZON SOCIAL
    Printer.CurrentX = Cx + 1
    Printer.CurrentY = Cy + 2.7
    pos = Printer.CurrentY
    posn = pos
    Printer.Print TxtRazonSocial
 
    
   
    'DIRECCION
    Printer.CurrentX = Cx + 1
    Printer.CurrentY = pos + Dp_S + Dp_S
    pos = Printer.CurrentY
    posd = Printer.CurrentY
    Printer.Print TxtDireccion
    
    
    
    'GIRO
    Printer.CurrentX = Cy + 1
    Printer.CurrentY = pos + Dp_S + Dp_S
    pos = Printer.CurrentY
    Printer.Print CboGiros.Text  ' MARIO ****   TxtGiro
    
    
   
    
    'RUT
    Printer.CurrentX = Cx + 14.4
    Printer.CurrentY = posn
    pos = Printer.CurrentY
    Printer.Print TxtRut

 
    
    
    'CIUDAD
    'Printer.CurrentY = pos
    'Printer.CurrentX = Cx + 15
    'Printer.Print TxtCiudad


     'comuna
    Printer.CurrentY = pos + Dp_S + Dp_S
    Printer.CurrentX = Cx + 14.4
    pos = Printer.CurrentY
    Printer.Print TxtComuna

   
        
    'TELEFONO
    Printer.CurrentY = pos + Dp_S + Dp_S
    pos = Printer.CurrentY
    Printer.CurrentX = Cx + 14.4
    Printer.Print Me.LbTelefono
    
   
    'COND. VENTA
    Printer.CurrentY = pos + Dp_S + Dp_S
    Printer.CurrentX = Cx + 14.4
    Printer.Print Mid(CboPlazos, 1, 20)
    
    
    '***
   ' Printer.CurrentX = Cx
   ' Printer.CurrentY = Printer.CurrentY + Dp_S - 0.2
  '  Printer.CurrentX = Printer.CurrentX + 10.9
   ' Printer.Print CboVendedores
    
    
    Printer.CurrentY = Printer.CurrentY + Dp_S - 0.3
    '***
    'Printer.CurrentY = Printer.CurrentY + Dp_S
    pos = Printer.CurrentY
    
    Printer.CurrentX = Cx + 8.5
    Printer.CurrentY = Printer.CurrentY + 0.5
       
    
    
    If Bm_Factura_por_Guias Then
        '**************************************************
        'Factura por guias Diciembre del 2013
        '**************************************************-
        Sql = "SELECT no_documento,id " & _
              "FROM ven_doc_venta " & _
              "WHERE  rut_emp='" & SP_Rut_Activo & "' AND doc_id_factura=" & CboDocVenta.ItemData(CboDocVenta.ListIndex) & " AND nro_factura=" & Me.TxtNroDocumento
        Consulta RsTmp, Sql
        
        If SG_Presentacion_Factura = "SEGUN" Then
                LvMateriales.ListItems.Add , , ""
                LvMateriales.ListItems(LvMateriales.ListItems.Count).SubItems(3) = "SEGUN GUIAS NRO"

                If RsTmp.RecordCount > 0 Then
                    Sp_GuiasFacturadas = Empty
                    RsTmp.MoveFirst
                    cont = 1
                    Do While Not RsTmp.EOF
                        Sp_GuiasFacturadas = Sp_GuiasFacturadas & RsTmp!no_documento & ","
                        cont = cont + 1
                        RsTmp.MoveNext
                        If cont = 3 Then
                            LvMateriales.ListItems.Add , , ""
                            LvMateriales.ListItems(LvMateriales.ListItems.Count).SubItems(3) = Mid(Sp_GuiasFacturadas, 1, Len(Sp_GuiasFacturadas) - 1)
                            Sp_GuiasFacturadas = Empty
                        End If
                    Loop
                    If Sp_GuiasFacturadas <> Empty Then
                            LvMateriales.ListItems.Add , , ""
                            LvMateriales.ListItems(LvMateriales.ListItems.Count).SubItems(3) = Mid(Sp_GuiasFacturadas, 1, Len(Sp_GuiasFacturadas) - 1)
                    End If
        
        
        
                    Sql = "SELECT neto,iva,bruto " & _
                          "FROM ven_doc_venta " & _
                          "WHERE  rut_emp='" & SP_Rut_Activo & "' AND doc_id=" & CboDocVenta.ItemData(CboDocVenta.ListIndex) & " AND no_documento=" & TxtNroDocumento
                    Consulta RsTmp, Sql
                    If RsTmp.RecordCount > 0 Then
                        Me.SkBrutoMateriales = NumFormat(RsTmp!bruto)
                        SkTotalMateriales = NumFormat(RsTmp!Neto)
                        SkIvaMateriales = NumFormat(RsTmp!Iva)
                    End If
                End If
        Else
            'Factura con detalle de productos
                
                If RsTmp.RecordCount > 0 Then
                    RsTmp.MoveFirst
                    Lm_Ids = ""
                    Do While Not RsTmp.EOF
                        Lm_Ids = Lm_Ids & RsTmp!ID & ","
                        RsTmp.MoveNext
                    Loop
                End If
                Lm_Ids = Mid(Lm_Ids, 1, Len(Lm_Ids) - 1)
                DetalleParaFacturaGuias Lm_Ids
                
                 Sql = "SELECT neto,iva,bruto " & _
                          "FROM ven_doc_venta " & _
                          "WHERE  rut_emp='" & SP_Rut_Activo & "' AND doc_id=" & CboDocVenta.ItemData(CboDocVenta.ListIndex) & " AND no_documento=" & TxtNroDocumento
                Consulta RsTmp, Sql
                If RsTmp.RecordCount > 0 Then
                    Me.SkBrutoMateriales = NumFormat(RsTmp!bruto)
                    SkTotalMateriales = NumFormat(RsTmp!Neto)
                    SkIvaMateriales = NumFormat(RsTmp!Iva)
                End If
            
                
                
        End If
        ' Fin
    End If
    Printer.CurrentY = Printer.CurrentY + 1
    
    For i = 1 To LvMateriales.ListItems.Count
        '6 - 3 - 7 - 8
       ' Printer.CurrentX = Printer.CurrentX + 0.07 'INTERLINIA ARTICULOS
       'esperar el interlinedo automtico
       ' Printer.CurrentX = Cx - 2
        Printer.CurrentX = Cx - 0.5
        
        p_Codigo = LvMateriales.ListItems(i).SubItems(1)
        RSet p_Cantidad = LvMateriales.ListItems(i).SubItems(6)
        p_UM = LvMateriales.ListItems(i).SubItems(21)
        p_Detalle = LvMateriales.ListItems(i).SubItems(3)
        RSet p_Unitario = LvMateriales.ListItems(i).SubItems(7)
        RSet p_Total = LvMateriales.ListItems(i).SubItems(8)
        
        Printer.Print "     " & "    " & p_Cantidad & "   " & p_Codigo & "     " & p_Detalle & " " & p_Unitario & "  " & p_Total
        'Printer.Print p_Codigo & "    " & p_Cantidad & "      " & p_Detalle & "    " & p_Unitario & "    " & p_Total
    Next
        
    If Bm_Factura_por_Guias Then LvMateriales.ListItems.Clear
    
    Printer.CurrentX = Cx - 1
    Printer.CurrentY = Cy + 14.2
    pos = Printer.CurrentY
        
    Printer.CurrentX = Cx + 1
    Printer.CurrentY = pos - 2.2
    Printer.Print TxtComentario
    
  '  pos = Printer.CurrentY
  
    Printer.CurrentX = Cx + 1
        
    'Printer.CurrentY = pos
    Printer.CurrentY = pos - 1.4
    
   
    
   ' mitad = Len(Sp_Letras) \ 2
   ' buscaespacio = InStr(mitad, Sp_Letras, " ")
   ' If buscaespacio > 0 Then
   '         Printer.Print Mid(Sp_Letras, 1, buscaespacio)
   '         Printer.CurrentX = Cx - 1
   '         Printer.CurrentY = Cy + 15.3
   '         Printer.Print Mid(Sp_Letras, buscaespacio)
   ' Else
   '           Printer.Print Sp_Letras
   ' End If
    
    
    Cy = 13
    
    RSet Sp_Neto = IIf(CDbl(SkTotalMateriales) = 0, "", SkTotalMateriales)
    RSet Sp_IVA = IIf(CDbl(SkIvaMateriales) = 0, "", SkIvaMateriales)
    RSet Sp_Total = SkBrutoMateriales
    pos = pos - 1.4
    
    
    Printer.CurrentX = 12.1
    Printer.CurrentY = 16.5
    Printer.Print Sp_Neto
    
    Printer.CurrentX = 14.7
    Printer.CurrentY = 16.5
    Printer.Print Sp_IVA
    
    Printer.CurrentX = 17.5
    Printer.CurrentY = 16.5
    Printer.Print Sp_Total
    
    Printer.CurrentX = Cx + 2
    Printer.CurrentY = 17.5
    Sp_Letras = UCase(BrutoAletras(CDbl(SkBrutoMateriales), True))
    Printer.Print Sp_Letras
    Printer.NewPage
    Printer.EndDoc
    Exit Sub
ProblemaImpresora:
    MsgBox "Ocurrio un error al intentar imprimir..." & vbNewLine & Err.Description & vbNewLine & Err.Number

End Sub


Private Sub ImprimeFacturaTyT()
    Dim Cx As Double, Cy As Double, Sp_Fecha As String, Ip_Mes As Integer, Dp_S As Double, Sp_Letras As String
    Dim Sp_Neto As String * 12, Sp_IVA As String * 12, Sp_Total As String * 12
    Dim Sp_GuiasFacturadas As String
    
    Dim p_Codigo As String * 5
    Dim p_Cantidad As String * 7
    Dim p_UM As String * 4
    Dim p_Detalle As String * 40
    Dim p_Unitario As String * 10
    Dim p_Total As String * 11
    
    
    On Error GoTo ProblemaImpresora
  '  Printer.ScaleMode = vbcentimter
    Printer.FontName = "Courier New"
    Printer.FontSize = 10
    Printer.ScaleMode = 7
        
    'Cx = 2 'horizontal
    'Cy = 5.9 'vertical
    'Dp_S = 0.35
    
    Sql = "SELECT sue_cx,sue_cy " & _
            "FROM sis_empresas_sucursales " & _
            "WHERE sue_id=" & IG_id_Sucursal_Empresa
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        Cx = Val(RsTmp!sue_cx)
        Cy = Val(RsTmp!sue_cy) 'vertical)
    Else
        Cx = 1 'horizontal
        Cy = 1.2
    
    End If
    
    
    Dp_S = 0.23
    
    Printer.CurrentX = Cx
    Printer.CurrentY = Cy
    Sp_Fecha = DtFecha.Value
    Ip_Mes = Val(Mid(Sp_Fecha, 4, 2))
    
    
    'FECHA
    Printer.CurrentY = 3.8
    Printer.CurrentX = 3.3
    
    
    pos = Printer.CurrentY
    Printer.Print Mid(Sp_Fecha, 1, 2) & "         " & UCase(MonthName(Val(Mid(Sp_Fecha, 4, 2))))
    Printer.CurrentY = pos
    Printer.CurrentX = 9.7
    Printer.Print Mid(Sp_Fecha, 9, 2)
    
    
    pos = pos + Dp_S
    
    
   
    'RAZON SOCIAL
    Printer.CurrentX = Cx + 2.5
    Printer.CurrentY = Cy + 3.1
    
    
    pos = Printer.CurrentY
    posn = pos
    Printer.Print TxtRazonSocial
 
    
   
    'DIRECCION
    Printer.CurrentX = Cx + 2.7
    Printer.CurrentY = Cy + 3.8
    pos = Printer.CurrentY
    
    posd = Printer.CurrentY
    Printer.Print TxtDireccion
    
    
    
    'GIRO
    Printer.CurrentX = Cx + 2.1
    Printer.CurrentY = Cy + 4.5
    pos = Printer.CurrentY
    posg = pos
    Printer.Print CboGiros.Text  ' MARIO ****   TxtGiro
    
    
   
    
    'RUT
    Printer.CurrentX = Cx + 15
    Printer.CurrentY = posn + 0.1
    pos = Printer.CurrentY
    Printer.Print TxtRut

 
    
    
    'CIUDAD
    Printer.CurrentY = posd
    Printer.CurrentX = Cx + 15.8
    Printer.Print TxtCiudad


     'comuna
    Printer.CurrentY = posg
    Printer.CurrentX = Cx + 15.8
    pos = Printer.CurrentY
    Printer.Print TxtComuna

   
        
    'TELEFONO
    Printer.CurrentY = posd
    pos = Printer.CurrentY
    Printer.CurrentX = Cx + 11.7
    Printer.Print Me.LbTelefono
    
   
    'COND. VENTA
    Printer.CurrentY = Cy + 5
    Printer.CurrentX = Cx + 17.3
    Printer.Print Mid(CboPlazos, 1, 20)
    
    
    '***
   ' Printer.CurrentX = Cx
   ' Printer.CurrentY = Printer.CurrentY + Dp_S - 0.2
  '  Printer.CurrentX = Printer.CurrentX + 10.9
   ' Printer.Print CboVendedores
    
    
    Printer.CurrentY = Printer.CurrentY + Dp_S - 0.3
    '***
    'Printer.CurrentY = Printer.CurrentY + Dp_S
    pos = Printer.CurrentY
    
    Printer.CurrentX = Cx + 8.5
    Printer.CurrentY = Printer.CurrentY + 0.5
       
    
    
    If Bm_Factura_por_Guias Then
        '**************************************************
        'Factura por guias Diciembre del 2013
        '**************************************************-
        Sql = "SELECT no_documento,id " & _
              "FROM ven_doc_venta " & _
              "WHERE  rut_emp='" & SP_Rut_Activo & "' AND doc_id_factura=" & CboDocVenta.ItemData(CboDocVenta.ListIndex) & " AND nro_factura=" & Me.TxtNroDocumento
        Consulta RsTmp, Sql
        
        If SG_Presentacion_Factura = "SEGUN" Then
                LvMateriales.ListItems.Add , , ""
                LvMateriales.ListItems(LvMateriales.ListItems.Count).SubItems(3) = "SEGUN GUIAS NRO"

                If RsTmp.RecordCount > 0 Then
                    Sp_GuiasFacturadas = Empty
                    RsTmp.MoveFirst
                    cont = 1
                    Do While Not RsTmp.EOF
                        Sp_GuiasFacturadas = Sp_GuiasFacturadas & RsTmp!no_documento & ","
                        cont = cont + 1
                        RsTmp.MoveNext
                        If cont = 3 Then
                            LvMateriales.ListItems.Add , , ""
                            LvMateriales.ListItems(LvMateriales.ListItems.Count).SubItems(3) = Mid(Sp_GuiasFacturadas, 1, Len(Sp_GuiasFacturadas) - 1)
                            Sp_GuiasFacturadas = Empty
                        End If
                    Loop
                    If Sp_GuiasFacturadas <> Empty Then
                            LvMateriales.ListItems.Add , , ""
                            LvMateriales.ListItems(LvMateriales.ListItems.Count).SubItems(3) = Mid(Sp_GuiasFacturadas, 1, Len(Sp_GuiasFacturadas) - 1)
                    End If
        
        
        
                    Sql = "SELECT neto,iva,bruto " & _
                          "FROM ven_doc_venta " & _
                          "WHERE  rut_emp='" & SP_Rut_Activo & "' AND doc_id=" & CboDocVenta.ItemData(CboDocVenta.ListIndex) & " AND no_documento=" & TxtNroDocumento
                    Consulta RsTmp, Sql
                    If RsTmp.RecordCount > 0 Then
                        Me.SkBrutoMateriales = NumFormat(RsTmp!bruto)
                        SkTotalMateriales = NumFormat(RsTmp!Neto)
                        SkIvaMateriales = NumFormat(RsTmp!Iva)
                    End If
                End If
        Else
            'Factura con detalle de productos
                
                If RsTmp.RecordCount > 0 Then
                    RsTmp.MoveFirst
                    Lm_Ids = ""
                    Do While Not RsTmp.EOF
                        Lm_Ids = Lm_Ids & RsTmp!ID & ","
                        RsTmp.MoveNext
                    Loop
                End If
                Lm_Ids = Mid(Lm_Ids, 1, Len(Lm_Ids) - 1)
                DetalleParaFacturaGuias Lm_Ids
                
                 Sql = "SELECT neto,iva,bruto " & _
                          "FROM ven_doc_venta " & _
                          "WHERE  rut_emp='" & SP_Rut_Activo & "' AND doc_id=" & CboDocVenta.ItemData(CboDocVenta.ListIndex) & " AND no_documento=" & TxtNroDocumento
                Consulta RsTmp, Sql
                If RsTmp.RecordCount > 0 Then
                    Me.SkBrutoMateriales = NumFormat(RsTmp!bruto)
                    SkTotalMateriales = NumFormat(RsTmp!Neto)
                    SkIvaMateriales = NumFormat(RsTmp!Iva)
                End If
            
                
                
        End If
        ' Fin
    End If
    Printer.CurrentY = 8.6
    
    For i = 1 To LvMateriales.ListItems.Count
        '6 - 3 - 7 - 8
       ' Printer.CurrentX = Printer.CurrentX + 0.07 'INTERLINIA ARTICULOS
       'esperar el interlinedo automtico
       ' Printer.CurrentX = Cx - 2
        Printer.CurrentX = Cx - 0.5
        
        p_Codigo = "" 'LvMateriales.ListItems(i).SubItems(1)
        RSet p_Cantidad = LvMateriales.ListItems(i).SubItems(6)
        p_UM = LvMateriales.ListItems(i).SubItems(21)
        p_Detalle = LvMateriales.ListItems(i).SubItems(3)
        RSet p_Unitario = LvMateriales.ListItems(i).SubItems(7)
        RSet p_Total = LvMateriales.ListItems(i).SubItems(8)
        
        Printer.Print "    " & p_Cantidad & "   " & p_Codigo & "  " & p_Detalle & "       " & p_Unitario & "  " & p_Total
        'Printer.Print p_Codigo & "    " & p_Cantidad & "      " & p_Detalle & "    " & p_Unitario & "    " & p_Total
    Next
        
    If Bm_Factura_por_Guias Then LvMateriales.ListItems.Clear
    
    Printer.CurrentX = Cx - 1
    Printer.CurrentY = Cy + 14.2
    pos = Printer.CurrentY
        
    Printer.CurrentX = Cx + 1
    Printer.CurrentY = pos - 2.2
    Printer.Print TxtComentario
    
  '  pos = Printer.CurrentY
  
    Printer.CurrentX = Cx + 1
        
    'Printer.CurrentY = pos
    Printer.CurrentY = pos - 1.4
    
   
    
   ' mitad = Len(Sp_Letras) \ 2
   ' buscaespacio = InStr(mitad, Sp_Letras, " ")
   ' If buscaespacio > 0 Then
   '         Printer.Print Mid(Sp_Letras, 1, buscaespacio)
   '         Printer.CurrentX = Cx - 1
   '         Printer.CurrentY = Cy + 15.3
   '         Printer.Print Mid(Sp_Letras, buscaespacio)
   ' Else
   '           Printer.Print Sp_Letras
   ' End If
    
    
    Cy = 13
    
    RSet Sp_Neto = IIf(CDbl(SkTotalMateriales) = 0, "", SkTotalMateriales)
    RSet Sp_IVA = IIf(CDbl(SkIvaMateriales) = 0, "", SkIvaMateriales)
    RSet Sp_Total = SkBrutoMateriales
    pos = pos - 1.4
    
    
    Printer.CurrentX = 17.5
    Printer.CurrentY = 18.5
    Printer.Print Sp_Neto
    
    Printer.CurrentX = 17.5
    Printer.CurrentY = 19.1
    Printer.Print Sp_IVA
    
    Printer.CurrentX = 17.5
    Printer.CurrentY = 19.7
    Printer.Print Sp_Total
    
    Printer.CurrentX = Cx + 2
    Printer.CurrentY = 18.6
    Sp_Letras = UCase(BrutoAletras(CDbl(SkBrutoMateriales), True))
    Printer.Print Sp_Letras
    Printer.NewPage
    Printer.EndDoc
    Exit Sub
ProblemaImpresora:
    MsgBox "Ocurrio un error al intentar imprimir..." & vbNewLine & Err.Description & vbNewLine & Err.Number

End Sub

Private Sub ImprimeFacturaTyTv2()
    Dim Cx As Double, Cy As Double, Sp_Fecha As String, Ip_Mes As Integer, Dp_S As Double, Sp_Letras As String
    Dim Sp_Neto As String * 12, Sp_IVA As String * 12, Sp_Total As String * 12
    Dim Sp_GuiasFacturadas As String
    
    Dim p_Codigo As String * 5
    Dim p_Cantidad As String * 7
    Dim p_UM As String * 4
    Dim p_Detalle As String * 40
    Dim p_Unitario As String * 10
    Dim p_Total As String * 11
    
    
    On Error GoTo ProblemaImpresora
  '  Printer.ScaleMode = vbcentimter
    Printer.FontName = "Courier New"
    Printer.FontSize = 10
    Printer.ScaleMode = 7
        
    'Cx = 2 'horizontal
    'Cy = 5.9 'vertical
    'Dp_S = 0.35
    
    Sql = "SELECT sue_cx,sue_cy " & _
            "FROM sis_empresas_sucursales " & _
            "WHERE sue_id=" & IG_id_Sucursal_Empresa
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        Cx = Val(RsTmp!sue_cx)
        Cy = Val(RsTmp!sue_cy) 'vertical)
    Else
        Cx = 1 'horizontal
        Cy = 1.2
    
    End If
    
    
    Dp_S = 0.23
    
    Printer.CurrentX = Cx
    Printer.CurrentY = Cy
    Sp_Fecha = DtFecha.Value
    Ip_Mes = Val(Mid(Sp_Fecha, 4, 2))
    
    
    'FECHA
    Printer.CurrentY = 3.8
    Printer.CurrentX = 3.3
    
    
    pos = Printer.CurrentY
    Printer.Print Mid(Sp_Fecha, 1, 2) & "         " & UCase(MonthName(Val(Mid(Sp_Fecha, 4, 2))))
    Printer.CurrentY = pos
    Printer.CurrentX = 9.7
    Printer.Print Mid(Sp_Fecha, 9, 2)
    
    
    pos = pos + Dp_S
    
    
   
    'RAZON SOCIAL
    Printer.CurrentX = Cx + 2.5
    Printer.CurrentY = Cy + 3.1
    
    
    pos = Printer.CurrentY
    posn = pos
    Printer.Print TxtRazonSocial
 
    
   
    'DIRECCION
    Printer.CurrentX = Cx + 2.7
    Printer.CurrentY = Cy + 3.8
    pos = Printer.CurrentY
    
    posd = Printer.CurrentY
    Printer.Print TxtDireccion
    
    
    
    'GIRO
    Printer.CurrentX = Cx + 2.1
    Printer.CurrentY = Cy + 4.5
    pos = Printer.CurrentY
    posg = pos
    Printer.Print CboGiros.Text  ' MARIO ****   TxtGiro
    
    
   
    
    'RUT
    Printer.CurrentX = Cx + 15
    Printer.CurrentY = posn + 0.1
    pos = Printer.CurrentY
    Printer.Print TxtRut

 
    
    
    'CIUDAD
    Printer.CurrentY = posd
    Printer.CurrentX = Cx + 15.8
    Printer.Print TxtCiudad


     'comuna
    Printer.CurrentY = posg
    Printer.CurrentX = Cx + 15.8
    pos = Printer.CurrentY
    Printer.Print TxtComuna

   
        
    'TELEFONO
    Printer.CurrentY = posd
    pos = Printer.CurrentY
    Printer.CurrentX = Cx + 11.7
    Printer.Print Me.LbTelefono
    
   
    'COND. VENTA
    Printer.CurrentY = Cy + 5
    Printer.CurrentX = Cx + 17.3
    Printer.Print Mid(CboPlazos, 1, 20)
    
    
    '***
   ' Printer.CurrentX = Cx
   ' Printer.CurrentY = Printer.CurrentY + Dp_S - 0.2
  '  Printer.CurrentX = Printer.CurrentX + 10.9
   ' Printer.Print CboVendedores
    
    
    Printer.CurrentY = Printer.CurrentY + Dp_S - 0.3
    '***
    'Printer.CurrentY = Printer.CurrentY + Dp_S
    pos = Printer.CurrentY
    
    Printer.CurrentX = Cx + 8.5
    Printer.CurrentY = Printer.CurrentY + 0.5
       
    
    
    If Bm_Factura_por_Guias Then
        '**************************************************
        'Factura por guias Diciembre del 2013
        '**************************************************-
        Sql = "SELECT no_documento,id " & _
              "FROM ven_doc_venta " & _
              "WHERE  rut_emp='" & SP_Rut_Activo & "' AND doc_id_factura=" & CboDocVenta.ItemData(CboDocVenta.ListIndex) & " AND nro_factura=" & Me.TxtNroDocumento
        Consulta RsTmp, Sql
        
        If SG_Presentacion_Factura = "SEGUN" Then
                LvMateriales.ListItems.Add , , ""
                LvMateriales.ListItems(LvMateriales.ListItems.Count).SubItems(3) = "SEGUN GUIAS NRO"

                If RsTmp.RecordCount > 0 Then
                    Sp_GuiasFacturadas = Empty
                    RsTmp.MoveFirst
                    cont = 1
                    Do While Not RsTmp.EOF
                        Sp_GuiasFacturadas = Sp_GuiasFacturadas & RsTmp!no_documento & ","
                        cont = cont + 1
                        RsTmp.MoveNext
                        If cont = 3 Then
                            LvMateriales.ListItems.Add , , ""
                            LvMateriales.ListItems(LvMateriales.ListItems.Count).SubItems(3) = Mid(Sp_GuiasFacturadas, 1, Len(Sp_GuiasFacturadas) - 1)
                            Sp_GuiasFacturadas = Empty
                        End If
                    Loop
                    If Sp_GuiasFacturadas <> Empty Then
                            LvMateriales.ListItems.Add , , ""
                            LvMateriales.ListItems(LvMateriales.ListItems.Count).SubItems(3) = Mid(Sp_GuiasFacturadas, 1, Len(Sp_GuiasFacturadas) - 1)
                    End If
        
        
        
                    Sql = "SELECT neto,iva,bruto " & _
                          "FROM ven_doc_venta " & _
                          "WHERE  rut_emp='" & SP_Rut_Activo & "' AND doc_id=" & CboDocVenta.ItemData(CboDocVenta.ListIndex) & " AND no_documento=" & TxtNroDocumento
                    Consulta RsTmp, Sql
                    If RsTmp.RecordCount > 0 Then
                        Me.SkBrutoMateriales = NumFormat(RsTmp!bruto)
                        SkTotalMateriales = NumFormat(RsTmp!Neto)
                        SkIvaMateriales = NumFormat(RsTmp!Iva)
                    End If
                End If
        Else
            'Factura con detalle de productos
                
                If RsTmp.RecordCount > 0 Then
                    RsTmp.MoveFirst
                    Lm_Ids = ""
                    Do While Not RsTmp.EOF
                        Lm_Ids = Lm_Ids & RsTmp!ID & ","
                        RsTmp.MoveNext
                    Loop
                End If
                Lm_Ids = Mid(Lm_Ids, 1, Len(Lm_Ids) - 1)
                DetalleParaFacturaGuias Lm_Ids
                
                 Sql = "SELECT neto,iva,bruto " & _
                          "FROM ven_doc_venta " & _
                          "WHERE  rut_emp='" & SP_Rut_Activo & "' AND doc_id=" & CboDocVenta.ItemData(CboDocVenta.ListIndex) & " AND no_documento=" & TxtNroDocumento
                Consulta RsTmp, Sql
                If RsTmp.RecordCount > 0 Then
                    Me.SkBrutoMateriales = NumFormat(RsTmp!bruto)
                    SkTotalMateriales = NumFormat(RsTmp!Neto)
                    SkIvaMateriales = NumFormat(RsTmp!Iva)
                End If
            
                
                
        End If
        ' Fin
    End If
    Printer.CurrentY = 8.6
    
    For i = 1 To LvMateriales.ListItems.Count
        '6 - 3 - 7 - 8
       ' Printer.CurrentX = Printer.CurrentX + 0.07 'INTERLINIA ARTICULOS
       'esperar el interlinedo automtico
       ' Printer.CurrentX = Cx - 2
        Printer.CurrentX = Cx - 0.5
        
        p_Codigo = "" 'LvMateriales.ListItems(i).SubItems(1)
        RSet p_Cantidad = LvMateriales.ListItems(i).SubItems(6)
        p_UM = LvMateriales.ListItems(i).SubItems(21)
        p_Detalle = LvMateriales.ListItems(i).SubItems(3)
        RSet p_Unitario = LvMateriales.ListItems(i).SubItems(7)
        RSet p_Total = LvMateriales.ListItems(i).SubItems(8)
        
        Printer.Print "    " & p_Cantidad & "   " & p_Codigo & "  " & p_Detalle & "       " & p_Unitario & "  " & p_Total
        'Printer.Print p_Codigo & "    " & p_Cantidad & "      " & p_Detalle & "    " & p_Unitario & "    " & p_Total
    Next
        
    If Bm_Factura_por_Guias Then LvMateriales.ListItems.Clear
    
    Printer.CurrentX = Cx - 1
    Printer.CurrentY = Cy + 14.2
    pos = Printer.CurrentY
        
    Printer.CurrentX = Cx + 1
    Printer.CurrentY = pos - 2.2
    Printer.Print TxtComentario
    
  '  pos = Printer.CurrentY
  
    Printer.CurrentX = Cx + 1
        
    'Printer.CurrentY = pos
    Printer.CurrentY = pos - 1.4
    
   
    
   ' mitad = Len(Sp_Letras) \ 2
   ' buscaespacio = InStr(mitad, Sp_Letras, " ")
   ' If buscaespacio > 0 Then
   '         Printer.Print Mid(Sp_Letras, 1, buscaespacio)
   '         Printer.CurrentX = Cx - 1
   '         Printer.CurrentY = Cy + 15.3
   '         Printer.Print Mid(Sp_Letras, buscaespacio)
   ' Else
   '           Printer.Print Sp_Letras
   ' End If
    
    
    Cy = 13
    
    RSet Sp_Neto = IIf(CDbl(SkTotalMateriales) = 0, "", SkTotalMateriales)
    RSet Sp_IVA = IIf(CDbl(SkIvaMateriales) = 0, "", SkIvaMateriales)
    RSet Sp_Total = SkBrutoMateriales
    pos = pos - 1.4
    
    
    Printer.CurrentX = 17.5
    Printer.CurrentY = 18.5
    Printer.Print Sp_Neto
    
    Printer.CurrentX = 17.5
    Printer.CurrentY = 19.1
    Printer.Print Sp_IVA
    
    Printer.CurrentX = 17.5
    Printer.CurrentY = 19.7
    Printer.Print Sp_Total
    
    Printer.CurrentX = Cx + 2
    Printer.CurrentY = 18.6
    Sp_Letras = UCase(BrutoAletras(CDbl(SkBrutoMateriales), True))
    Printer.Print Sp_Letras
    Printer.NewPage
    Printer.EndDoc
    Exit Sub
ProblemaImpresora:
    MsgBox "Ocurrio un error al intentar imprimir..." & vbNewLine & Err.Description & vbNewLine & Err.Number

End Sub
Private Sub ImprimeFacturaSandraRamirez()
    Dim Cx As Double, Cy As Double, Sp_Fecha As String, Ip_Mes As Integer, Dp_S As Double, Sp_Letras As String
    Dim Sp_Neto As String * 12, Sp_IVA As String * 12, Sp_Total As String * 12
    Dim Sp_GuiasFacturadas As String
    
    Dim p_Codigo As String * 1
    Dim p_Cantidad As String * 7
    Dim p_UM As String * 4
    Dim p_Detalle As String * 35
    Dim p_Unitario As String * 10
    Dim p_Total As String * 11
    
    Dim Sp_Hospital  As String
    
    On Error GoTo ProblemaImpresora
  '  Printer.ScaleMode = vbcentimter
    Printer.FontName = "Courier New"
    Printer.FontSize = 10
    Printer.ScaleMode = 7
        
    'Cx = 2 'horizontal
    'Cy = 5.9 'vertical
    'Dp_S = 0.35
    
    Sql = "SELECT sue_cx,sue_cy " & _
            "FROM sis_empresas_sucursales " & _
            "WHERE sue_id=" & IG_id_Sucursal_Empresa
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        Cx = Val(RsTmp!sue_cx)
        Cy = Val(RsTmp!sue_cy) 'vertical)
    Else
        Cx = 2.2 'horizontal
        Cy = 3.3
    
    End If
    
    
    Dp_S = 0.23
    
    Printer.CurrentX = Cx
    Printer.CurrentY = Cy
    Sp_Fecha = DtFecha.Value
    Ip_Mes = Val(Mid(Sp_Fecha, 4, 2))
    
    
    'FECHA
    Printer.CurrentY = 5.8
    Printer.CurrentX = 4.3
    
    
    pos = Printer.CurrentY
    Printer.Print "                                    "; Mid(Sp_Fecha, 1, 2) & "                " & UCase(MonthName(Val(Mid(Sp_Fecha, 4, 2))))
    Printer.CurrentY = pos
    Printer.CurrentX = 19.5
    Printer.Print Mid(Sp_Fecha, 7, 4)
    
    
    pos = pos + Dp_S
    
    
   
    'RAZON SOCIAL
    Printer.CurrentX = Cx + 1.5
    Printer.CurrentY = Cy + 3.1
    
    
    pos = Printer.CurrentY
    posn = pos
    Printer.Print TxtRazonSocial
 
    
   
    'DIRECCION
    Printer.CurrentX = Cx + 1.7
    Printer.CurrentY = Cy + 3.8
    pos = Printer.CurrentY
    
    posd = Printer.CurrentY
    Printer.Print TxtDireccion
    
    
    
    'GIRO/ciudad
    Printer.CurrentX = Cx + 14
    Printer.CurrentY = Cy + 4.5
    pos = Printer.CurrentY
    posg = pos
    Printer.Print TxtCiudad ' CboGiros.Text  ' MARIO ****   TxtGiro
    
    
    
    'GIRO
    Printer.CurrentX = Cx + 1
    Printer.CurrentY = Cy + 4.5
    pos = Printer.CurrentY
    posg = pos
    Printer.Print CboGiros.Text  ' MARIO ****   TxtGiro
    
   
    
    'RUT
    Printer.CurrentX = Cx + 14
    Printer.CurrentY = posn + 0.1
    pos = Printer.CurrentY
    Printer.Print TxtRut

 
    
    
    'fono  CIUDAD
   ' Printer.CurrentY = posd
   ' Printer.CurrentX = Cx + 15.8
   ' Printer.Print Me.LbTelefono


     'comuna
    Printer.CurrentY = posd
    Printer.CurrentX = Cx + 14
    pos = Printer.CurrentY
    Printer.Print TxtComuna

   
        
'    TELEFONO
    Printer.CurrentY = posg
    pos = Printer.CurrentY
    Printer.CurrentX = Cx + 15
    Printer.Print Me.LbTelefono
    
   
    'COND. VENTA
    Printer.CurrentY = Cy + 5.1
    Printer.CurrentX = Cx + 4
    Printer.Print Mid(CboPlazos, 1, 20)
    
    
    '***
   ' Printer.CurrentX = Cx
   ' Printer.CurrentY = Printer.CurrentY + Dp_S - 0.2
  '  Printer.CurrentX = Printer.CurrentX + 10.9
   ' Printer.Print CboVendedores
    
    
    Printer.CurrentY = Printer.CurrentY + Dp_S - 0.3
    '***
    'Printer.CurrentY = Printer.CurrentY + Dp_S
    pos = Printer.CurrentY
    
    Printer.CurrentX = Cx + 8.5
    Printer.CurrentY = Printer.CurrentY + 0.5
       
    
    
    If Bm_Factura_por_Guias Then
        '**************************************************
        'Factura por guias Diciembre del 2013
        '**************************************************-
        Sql = "SELECT no_documento,id " & _
              "FROM ven_doc_venta " & _
              "WHERE  rut_emp='" & SP_Rut_Activo & "' AND doc_id_factura=" & CboDocVenta.ItemData(CboDocVenta.ListIndex) & " AND nro_factura=" & Me.TxtNroDocumento
        Consulta RsTmp, Sql
        
        If SG_Presentacion_Factura = "SEGUN" Then
                LvMateriales.ListItems.Add , , ""
                LvMateriales.ListItems(LvMateriales.ListItems.Count).SubItems(3) = "SEGUN GUIAS NRO"

                If RsTmp.RecordCount > 0 Then
                    Sp_GuiasFacturadas = Empty
                    RsTmp.MoveFirst
                    cont = 1
                    Do While Not RsTmp.EOF
                        Sp_GuiasFacturadas = Sp_GuiasFacturadas & RsTmp!no_documento & ","
                        cont = cont + 1
                        RsTmp.MoveNext
                        If cont = 3 Then
                            LvMateriales.ListItems.Add , , ""
                            LvMateriales.ListItems(LvMateriales.ListItems.Count).SubItems(3) = Mid(Sp_GuiasFacturadas, 1, Len(Sp_GuiasFacturadas) - 1)
                            Sp_GuiasFacturadas = Empty
                        End If
                    Loop
                    If Sp_GuiasFacturadas <> Empty Then
                            LvMateriales.ListItems.Add , , ""
                            LvMateriales.ListItems(LvMateriales.ListItems.Count).SubItems(3) = Mid(Sp_GuiasFacturadas, 1, Len(Sp_GuiasFacturadas) - 1)
                    End If
        
        
        
                    Sql = "SELECT neto,iva,bruto " & _
                          "FROM ven_doc_venta " & _
                          "WHERE  rut_emp='" & SP_Rut_Activo & "' AND doc_id=" & CboDocVenta.ItemData(CboDocVenta.ListIndex) & " AND no_documento=" & TxtNroDocumento
                    Consulta RsTmp, Sql
                    If RsTmp.RecordCount > 0 Then
                        Me.SkBrutoMateriales = NumFormat(RsTmp!bruto)
                        SkTotalMateriales = NumFormat(RsTmp!Neto)
                        SkIvaMateriales = NumFormat(RsTmp!Iva)
                    End If
                End If
        Else
            'Factura con detalle de productos
                
                If RsTmp.RecordCount > 0 Then
                    RsTmp.MoveFirst
                    Lm_Ids = ""
                    Do While Not RsTmp.EOF
                        Lm_Ids = Lm_Ids & RsTmp!ID & ","
                        RsTmp.MoveNext
                    Loop
                End If
                Lm_Ids = Mid(Lm_Ids, 1, Len(Lm_Ids) - 1)
                DetalleParaFacturaGuias Lm_Ids
                
                 Sql = "SELECT neto,iva,bruto " & _
                          "FROM ven_doc_venta " & _
                          "WHERE  rut_emp='" & SP_Rut_Activo & "' AND doc_id=" & CboDocVenta.ItemData(CboDocVenta.ListIndex) & " AND no_documento=" & TxtNroDocumento
                Consulta RsTmp, Sql
                If RsTmp.RecordCount > 0 Then
                    Me.SkBrutoMateriales = NumFormat(RsTmp!bruto)
                    SkTotalMateriales = NumFormat(RsTmp!Neto)
                    SkIvaMateriales = NumFormat(RsTmp!Iva)
                End If
            
                
                
        End If
        ' Fin
    End If
    Printer.CurrentY = 10.5
    Printer.FontSize = Printer.FontSize + 1
    For i = 1 To LvMateriales.ListItems.Count
        '6 - 3 - 7 - 8
       ' Printer.CurrentX = Printer.CurrentX + 0.07 'INTERLINIA ARTICULOS
       'esperar el interlinedo automtico
       ' Printer.CurrentX = Cx - 2
       
        Printer.CurrentX = Cx - 0.5
        
        p_Codigo = "" 'LvMateriales.ListItems(i).SubItems(1)
        RSet p_Cantidad = LvMateriales.ListItems(i).SubItems(6)
        p_UM = LvMateriales.ListItems(i).SubItems(21)
        p_Detalle = LvMateriales.ListItems(i).SubItems(3)
        RSet p_Unitario = LvMateriales.ListItems(i).SubItems(7)
        RSet p_Total = LvMateriales.ListItems(i).SubItems(8)
        
        Printer.Print "  " & p_Cantidad & " " & p_UM & p_Codigo & "   " & p_Detalle & "   " & p_Unitario & "  " & p_Total
        'Printer.Print p_Codigo & "    " & p_Cantidad & "      " & p_Detalle & "    " & p_Unitario & "    " & p_Total
    Next
    posparatotaol = Printer.CurrentY + 0.5
    If Bm_Factura_por_Guias Then LvMateriales.ListItems.Clear
    
    Printer.CurrentX = Cx - 1
    Printer.CurrentY = Cy + 14.2
    pos = Printer.CurrentY
        
    Printer.CurrentX = Cx + 1
    Printer.CurrentY = pos - 2.2
    Printer.Print TxtComentario
    
  '  pos = Printer.CurrentY
  
    Printer.CurrentX = Cx + 1
        
    'Printer.CurrentY = pos
    Printer.CurrentY = pos - 1.4
    
   
    
   ' mitad = Len(Sp_Letras) \ 2
   ' buscaespacio = InStr(mitad, Sp_Letras, " ")
   ' If buscaespacio > 0 Then
   '         Printer.Print Mid(Sp_Letras, 1, buscaespacio)
   '         Printer.CurrentX = Cx - 1
   '         Printer.CurrentY = Cy + 15.3
   '         Printer.Print Mid(Sp_Letras, buscaespacio)
   ' Else
   '           Printer.Print Sp_Letras
   ' End If
    
    
    Cy = 13
    
    RSet Sp_Neto = IIf(CDbl(SkTotalMateriales) = 0, "", SkTotalMateriales)
    RSet Sp_IVA = IIf(CDbl(SkIvaMateriales) = 0, "", SkIvaMateriales)
    RSet Sp_Total = SkBrutoMateriales
    pos = pos - 1.4
    
    
    
    Printer.CurrentX = 15
    Printer.CurrentY = posparatotaol
    Printer.Print "NETO ===>"
    
    Printer.CurrentX = 17
    Printer.CurrentY = posparatotaol
    Printer.Print Sp_Neto
    Sp_Hospital = "SI"
   ' sql2 = "SELECT cli_mail1 " & _
            "FROM maestro_clientes " & _
            "WHERE rut_cliente='" & TxtRUT & "'"
   'Consulta RsTmp, sql2
    'If RsTmp.RecordCount > 0 Then
     '   If RsTmp!cli_mail1 = "SI" Then Sp_Hospital = "SI"
    'End If
    
    If Sp_Hospital = "SI" Then
        Printer.CurrentX = 15
        Printer.CurrentY = posparatotaol + 0.5
        Printer.Print "IVA ===>"
        
        Printer.CurrentX = 17
        Printer.CurrentY = posparatotaol + 0.5
        Printer.Print Sp_IVA
        
        Printer.FontBold = True
        Printer.CurrentX = 15
        Printer.CurrentY = posparatotaol + 1
        Printer.Print "TOTAL ===>"
        Printer.CurrentX = 17
        Printer.CurrentY = posparatotaol + 1
        Printer.Print Sp_Total
    End If
    
 '   Printer.CurrentX = Cx + 2
  '  Printer.CurrentY = 18.6
  '  Sp_Letras = UCase(BrutoAletras(CDbl(SkBrutoMateriales), True))
   ' Printer.Print Sp_Letras
    Printer.NewPage
    Printer.EndDoc
    Exit Sub
ProblemaImpresora:
    MsgBox "Ocurrio un error al intentar imprimir..." & vbNewLine & Err.Description & vbNewLine & Err.Number

End Sub

Private Sub UpDown1_DownClick()
    TxtSubTotalMateriales = NumFormat(CDbl(TxtSubTotalMateriales) - 1)
End Sub

Private Sub UpDown1_UpClick()
    TxtSubTotalMateriales = NumFormat(CDbl(TxtSubTotalMateriales) + 1)
End Sub

Private Sub UpDown2_DownClick()
    TxtTotalDescuento = CDbl(TxtTotalDescuento) - 1
End Sub

Private Sub UpDown2_UpClick()
    TxtTotalDescuento = CDbl(TxtTotalDescuento) + 1
End Sub

Private Sub UpDown3_DownClick()
     SkTotalMateriales = NumFormat(CDbl(SkTotalMateriales) - 1)
End Sub

Private Sub UpDown3_UpClick()
    SkTotalMateriales = NumFormat(CDbl(SkTotalMateriales) + 1)
End Sub

Private Sub UpDown4_DownClick()
SkIvaMateriales = NumFormat(CDbl(SkIvaMateriales) - 1)
End Sub

Private Sub UpDown4_UpClick()
    SkIvaMateriales = NumFormat(CDbl(SkIvaMateriales) + 1)
End Sub

Private Sub UpDown5_DownClick()
    SkBrutoMateriales = NumFormat(CDbl(SkBrutoMateriales) - 1)
    
End Sub

Private Sub UpDown5_UpClick()
     SkBrutoMateriales = NumFormat(CDbl(SkBrutoMateriales) + 1)
End Sub

Function ConexionAcces()
            On Error GoTo FalloConexion
                Connection_String = "PROVIDER=MSDASQL;dsn=db_pachipap;uid=;pwd=;" '   ' le agrega el path de la base de datos al Connectionstring
                Set cnAccess = New ADODB.Connection '  ' Nuevo objeto Connection
                cnAccess.CursorLocation = adUseClient
                cnAccess.ConnectionTimeout = 30
                cnAccess.Open Connection_String '  'Abre la conexi�n
            Exit Function
FalloConexion:
    MsgBox "No fue posible establecer la conecci�n con el Servidor..." & vbNewLine & "Verifique que la Ip ingresada sea la correcta" & vbNewLine & Err.Description, vbInformation
    Exit Function
End Function
Function ConsultaAccess(RecSet As ADODB.Recordset, CSQL As String)
InicioConsulta:
            IS_IntentosConexion = IS_IntentosConexion + 1
             Set RecSet = New ADODB.Recordset '  'Nuevo recordset
            'Debug.Print CSQL
            On Error GoTo FalloConsulta
            
            RecSet.Open CSQL, cnAccess, adOpenStatic, adLockOptimistic, adAsyncFetch  '  ' abre
 IS_IntentosConexion = 0
            Exit Function
FalloConsulta:
    If IS_IntentosConexion < 2 Then
        ConexionAcces
        GoTo InicioConsulta
    End If
        
    If EjecucionIDE Then
        MsgBox "Fallo la consulta a la base de datos " & vbNewLine & CSQL & vbNewLine & Err.Number & " - " & Err.Description
    Else
        MsgBox "Fallo la consulta a la base de datos " & vbNewLine & vbNewLine & Err.Number & " - " & Err.Description
    End If
    If IS_IntentosConexion = 2 Then End
End Function
