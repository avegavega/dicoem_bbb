VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form ven_modulo_entrega 
   Caption         =   "Mod�lo de entrega de productos"
   ClientHeight    =   7605
   ClientLeft      =   1410
   ClientTop       =   2415
   ClientWidth     =   15420
   LinkTopic       =   "Form1"
   ScaleHeight     =   7605
   ScaleWidth      =   15420
   Begin MSComDlg.CommonDialog Dialogo 
      Left            =   300
      Top             =   7170
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.Frame FrameCliente 
      Caption         =   "Datos del cliente"
      Height          =   1140
      Left            =   810
      TabIndex        =   31
      Top             =   8295
      Width           =   12810
      Begin VB.TextBox TxtRut 
         BackColor       =   &H0080FF80&
         Height          =   285
         Left            =   630
         TabIndex        =   37
         ToolTipText     =   "Ingrese el RUT sin puntos ni guion."
         Top             =   225
         Width           =   1575
      End
      Begin VB.TextBox txtSucursalCliente 
         Height          =   285
         Left            =   3960
         Locked          =   -1  'True
         TabIndex        =   36
         TabStop         =   0   'False
         Top             =   480
         Width           =   3285
      End
      Begin VB.TextBox TxtComuna 
         Height          =   285
         Left            =   8040
         Locked          =   -1  'True
         TabIndex        =   35
         TabStop         =   0   'False
         Text            =   " "
         Top             =   210
         Width           =   1650
      End
      Begin VB.TextBox TxtCiudad 
         Height          =   285
         Left            =   10440
         Locked          =   -1  'True
         TabIndex        =   34
         TabStop         =   0   'False
         Text            =   " "
         Top             =   210
         Width           =   2205
      End
      Begin VB.TextBox TxtGiro 
         Height          =   285
         Left            =   8055
         Locked          =   -1  'True
         TabIndex        =   33
         TabStop         =   0   'False
         Top             =   510
         Width           =   4590
      End
      Begin VB.TextBox txtDireccion 
         Height          =   285
         Left            =   3960
         Locked          =   -1  'True
         TabIndex        =   32
         TabStop         =   0   'False
         Top             =   795
         Width           =   8670
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   255
         Left            =   2880
         OleObjectBlob   =   "ven_modulo_entrega-.frx":0000
         TabIndex        =   38
         Top             =   810
         Width           =   1035
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   255
         Index           =   0
         Left            =   120
         OleObjectBlob   =   "ven_modulo_entrega-.frx":0072
         TabIndex        =   39
         Top             =   240
         Width           =   495
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel18 
         Height          =   255
         Left            =   7155
         OleObjectBlob   =   "ven_modulo_entrega-.frx":00D6
         TabIndex        =   40
         Top             =   495
         Width           =   855
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel19 
         Height          =   255
         Left            =   7140
         OleObjectBlob   =   "ven_modulo_entrega-.frx":013C
         TabIndex        =   41
         Top             =   240
         Width           =   855
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel20 
         Height          =   255
         Left            =   9810
         OleObjectBlob   =   "ven_modulo_entrega-.frx":01A6
         TabIndex        =   42
         Top             =   240
         Width           =   735
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel5 
         Height          =   255
         Index           =   0
         Left            =   2280
         OleObjectBlob   =   "ven_modulo_entrega-.frx":0210
         TabIndex        =   43
         Top             =   510
         Width           =   1605
      End
   End
   Begin VB.CommandButton CmdRetornar 
      Caption         =   "Retornar"
      Height          =   420
      Left            =   13050
      TabIndex        =   30
      Top             =   6990
      Width           =   1995
   End
   Begin VB.Frame FraArticulos 
      Caption         =   "Articulos"
      Height          =   6585
      Left            =   210
      TabIndex        =   3
      Top             =   270
      Width           =   14850
      Begin VB.Frame Frame3 
         Caption         =   "Guias emitidas"
         Height          =   2220
         Left            =   75
         TabIndex        =   26
         Top             =   4305
         Width           =   7470
         Begin VB.CommandButton cmdVerGuia 
            Caption         =   "Ver Productos retirados"
            Height          =   315
            Left            =   4440
            TabIndex        =   29
            Top             =   255
            Width           =   2595
         End
         Begin VB.ComboBox CboEmitidas 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            ItemData        =   "ven_modulo_entrega-.frx":027E
            Left            =   120
            List            =   "ven_modulo_entrega-.frx":0280
            Style           =   2  'Dropdown List
            TabIndex        =   27
            ToolTipText     =   "Seleccione Documento de  venta. Ventas con Retenci�n, cuando el contribuyente recibe factura de terceros "
            Top             =   255
            Width           =   4335
         End
         Begin MSComctlLib.ListView LvEmitidos 
            Height          =   1530
            Left            =   105
            TabIndex        =   28
            Top             =   570
            Width           =   6975
            _ExtentX        =   12303
            _ExtentY        =   2699
            View            =   3
            LabelWrap       =   0   'False
            HideSelection   =   0   'False
            FullRowSelect   =   -1  'True
            GridLines       =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   3
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Object.Tag             =   "T1200"
               Text            =   "Codigo"
               Object.Width           =   1587
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Object.Tag             =   "T1100"
               Text            =   "Nombre Articulo"
               Object.Width           =   7056
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   2
               Object.Tag             =   "N109"
               Text            =   "Retirado"
               Object.Width           =   1940
            EndProperty
         End
      End
      Begin VB.Frame Frame2 
         Caption         =   "Guia de entrega"
         Height          =   4470
         Left            =   7620
         TabIndex        =   17
         Top             =   1995
         Width           =   6945
         Begin VB.TextBox txtTotalGuia 
            Alignment       =   1  'Right Justify
            BackColor       =   &H8000000F&
            Height          =   285
            Left            =   5220
            Locked          =   -1  'True
            TabIndex        =   44
            TabStop         =   0   'False
            Top             =   3300
            Width           =   1380
         End
         Begin VB.CommandButton Cmd 
            Caption         =   "Emitir Guia"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   435
            Left            =   5280
            TabIndex        =   25
            Top             =   3870
            Width           =   1425
         End
         Begin MSComCtl2.DTPicker DtFechaGuia 
            Height          =   315
            Left            =   4005
            TabIndex        =   23
            Top             =   3975
            Width           =   1260
            _ExtentX        =   2223
            _ExtentY        =   556
            _Version        =   393216
            Format          =   273416193
            CurrentDate     =   41614
         End
         Begin VB.TextBox TxtNroGuia 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   2685
            TabIndex        =   20
            ToolTipText     =   "Nro de Documento"
            Top             =   3975
            Width           =   1320
         End
         Begin VB.ComboBox CboGuia 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            ItemData        =   "ven_modulo_entrega-.frx":0282
            Left            =   135
            List            =   "ven_modulo_entrega-.frx":0284
            Style           =   2  'Dropdown List
            TabIndex        =   19
            ToolTipText     =   "Seleccione Documento de  venta. Ventas con Retenci�n, cuando el contribuyente recibe factura de terceros "
            Top             =   3975
            Width           =   2565
         End
         Begin MSComctlLib.ListView LvDetalle 
            Height          =   2925
            Left            =   135
            TabIndex        =   18
            Top             =   345
            Width           =   6630
            _ExtentX        =   11695
            _ExtentY        =   5159
            View            =   3
            LabelWrap       =   0   'False
            HideSelection   =   0   'False
            FullRowSelect   =   -1  'True
            GridLines       =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   5
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Object.Tag             =   "T1200"
               Text            =   "Codigo"
               Object.Width           =   1587
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Object.Tag             =   "T1100"
               Text            =   "Nombre Articulo"
               Object.Width           =   5292
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   2
               Object.Tag             =   "N109"
               Text            =   "Retirando"
               Object.Width           =   1587
            EndProperty
            BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   3
               Object.Tag             =   "N100"
               Text            =   "Neto Unidad"
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   4
               Key             =   "total"
               Object.Tag             =   "N100"
               Text            =   "Neto Total"
               Object.Width           =   2540
            EndProperty
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
            Height          =   210
            Index           =   2
            Left            =   3225
            OleObjectBlob   =   "ven_modulo_entrega-.frx":0286
            TabIndex        =   21
            Top             =   3780
            Width           =   735
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
            Height          =   210
            Index           =   3
            Left            =   135
            OleObjectBlob   =   "ven_modulo_entrega-.frx":02F0
            TabIndex        =   22
            Top             =   3780
            Width           =   990
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
            Height          =   210
            Index           =   4
            Left            =   4095
            OleObjectBlob   =   "ven_modulo_entrega-.frx":0360
            TabIndex        =   24
            Top             =   3765
            Width           =   735
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
            Height          =   210
            Index           =   5
            Left            =   3480
            OleObjectBlob   =   "ven_modulo_entrega-.frx":03C8
            TabIndex        =   45
            Top             =   3330
            Width           =   1650
         End
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   255
         Left            =   135
         OleObjectBlob   =   "ven_modulo_entrega-.frx":0444
         TabIndex        =   16
         Top             =   1905
         Width           =   3480
      End
      Begin VB.Frame Frame1 
         Caption         =   "Documento de Venta"
         Height          =   1395
         Index           =   1
         Left            =   105
         TabIndex        =   4
         Top             =   390
         Width           =   14430
         Begin VB.TextBox TxtRazonSocial 
            BackColor       =   &H00E0E0E0&
            Height          =   285
            Left            =   9480
            Locked          =   -1  'True
            TabIndex        =   13
            TabStop         =   0   'False
            Top             =   135
            Width           =   3225
         End
         Begin VB.CommandButton cmdBusca 
            Caption         =   "Buscar Documento"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   630
            Left            =   4590
            TabIndex        =   2
            Top             =   495
            Width           =   2655
         End
         Begin VB.TextBox txtTotalDocumento 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   9480
            Locked          =   -1  'True
            TabIndex        =   11
            TabStop         =   0   'False
            Top             =   990
            Width           =   1380
         End
         Begin VB.TextBox txtFecha 
            Height          =   285
            Left            =   9480
            Locked          =   -1  'True
            TabIndex        =   7
            TabStop         =   0   'False
            Top             =   705
            Width           =   3225
         End
         Begin VB.TextBox txtSucEmpresa 
            BackColor       =   &H00E0E0E0&
            Height          =   285
            Left            =   9480
            Locked          =   -1  'True
            TabIndex        =   6
            TabStop         =   0   'False
            Top             =   420
            Width           =   3225
         End
         Begin VB.ComboBox CboDocVenta 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            ItemData        =   "ven_modulo_entrega-.frx":04CC
            Left            =   1485
            List            =   "ven_modulo_entrega-.frx":04CE
            Style           =   2  'Dropdown List
            TabIndex        =   0
            ToolTipText     =   "Seleccione Documento de  venta. Ventas con Retenci�n, cuando el contribuyente recibe factura de terceros "
            Top             =   525
            Width           =   2565
         End
         Begin VB.TextBox TxtNroDocumento 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   1485
            TabIndex        =   1
            ToolTipText     =   "Nro de Documento"
            Top             =   855
            Width           =   1320
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
            Height          =   210
            Index           =   0
            Left            =   135
            OleObjectBlob   =   "ven_modulo_entrega-.frx":04D0
            TabIndex        =   5
            Top             =   855
            Width           =   1260
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel6 
            Height          =   255
            Left            =   7515
            OleObjectBlob   =   "ven_modulo_entrega-.frx":053A
            TabIndex        =   8
            Top             =   1020
            Width           =   1845
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel16 
            Height          =   255
            Index           =   1
            Left            =   7680
            OleObjectBlob   =   "ven_modulo_entrega-.frx":05B6
            TabIndex        =   9
            Top             =   450
            Width           =   1695
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel5 
            Height          =   255
            Index           =   1
            Left            =   7725
            OleObjectBlob   =   "ven_modulo_entrega-.frx":0624
            TabIndex        =   10
            Top             =   720
            Width           =   1605
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
            Height          =   210
            Index           =   1
            Left            =   150
            OleObjectBlob   =   "ven_modulo_entrega-.frx":0698
            TabIndex        =   12
            Top             =   570
            Width           =   1260
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel16 
            Height          =   255
            Index           =   0
            Left            =   7680
            OleObjectBlob   =   "ven_modulo_entrega-.frx":0708
            TabIndex        =   14
            Top             =   165
            Width           =   1695
         End
      End
      Begin MSComctlLib.ListView LvDetalleDoc 
         Height          =   2025
         Left            =   60
         TabIndex        =   15
         Top             =   2130
         Width           =   7485
         _ExtentX        =   13203
         _ExtentY        =   3572
         View            =   3
         LabelWrap       =   0   'False
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   7
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "T1200"
            Text            =   "Codigo"
            Object.Width           =   1587
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "T1100"
            Text            =   "Nombre Articulo"
            Object.Width           =   5292
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   2
            Object.Tag             =   "N109"
            Text            =   "Vendido"
            Object.Width           =   1587
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   3
            Object.Tag             =   "N109"
            Text            =   "Retirado"
            Object.Width           =   1587
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   4
            Object.Tag             =   "N109"
            Text            =   "Pendiente"
            Object.Width           =   1587
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Object.Tag             =   "N109"
            Text            =   "Valor Unitario"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   6
            Object.Tag             =   "N109"
            Text            =   "Valor Total"
            Object.Width           =   2540
         EndProperty
      End
   End
   Begin VB.Timer Timer1 
      Interval        =   20
      Left            =   840
      Top             =   765
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   0
      OleObjectBlob   =   "ven_modulo_entrega-.frx":0788
      Top             =   0
   End
End
Attribute VB_Name = "ven_modulo_entrega"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False


Private Sub CboGuia_Click()
    If CboGuia.ListIndex = -1 Then Exit Sub
    TxtNroGuia = AutoIncremento("LEE", CboGuia.ItemData(CboGuia.ListIndex), , IG_id_Sucursal_Empresa)
End Sub

Private Sub Cmd_Click()
    Dim Lp_IdDoc As Long
    If LvDetalle.ListItems.Count = 0 Then
        MsgBox "No hay productos seleccionados para entregar...", vbInformation
        Exit Sub
    End If
    
    If CboGuia.ListIndex = -1 Then
        MsgBox "Seleccione guia...", vbInformation
        CboGuia.SetFocus
        Exit Sub
    End If
    If Val(TxtNroGuia) = 0 Then
        MsgBox "Falta nro de guia"
        TxtNroGuia.SetFocus
        Exit Sub
    End If
    
    'Tambien debemos revisar que el correlativo
    'de la guia no este ocupado
    Sql = "SELECT id " & _
            "FROM ven_doc_venta " & _
            "WHERE no_documento=" & TxtNroGuia & " " & _
            "AND doc_id=" & CboGuia.ItemData(CboGuia.ListIndex)
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        MsgBox "Este documento ya ha sido utilizado...", vbInformation
        TxtNroGuia.SetFocus
        Exit Sub
    End If
    
     AutoIncremento "GUARDA", CboGuia.ItemData(CboGuia.ListIndex), TxtNroGuia, IG_id_Sucursal_Empresa
    'Ahora si estamos en condiciones de grabar la guia
    On Error GoTo errorGraba
    cn.BeginTrans
        Sql = "INSERT INTO ven_doc_venta (doc_id,no_documento,fecha,ven_entrega,ven_entrega_ref,rut_emp,rut_cliente,usu_nombre) " & _
                "VALUES(" & CboGuia.ItemData(CboGuia.ListIndex) & "," & Val(TxtNroGuia) & ",'" & Fql(DtFechaGuia) & "','SI'," & TxtRut.Tag & ",'" & SP_Rut_Activo & "','" & TxtRut & "','" & LogUsuario & "')"
        cn.Execute Sql
        Sql = "INSERT INTO ven_detalle (doc_id,no_documento,codigo,unidades,rut_emp,ven_entrega,ven_entrega_ref) VALUES"
        sql2 = ""
        For i = 1 To LvDetalle.ListItems.Count
            sql2 = sql2 & "(" & CboGuia.ItemData(CboGuia.ListIndex) & "," & TxtNroGuia & "," & LvDetalle.ListItems(i) & "," & CDbl(LvDetalle.ListItems(i).SubItems(2)) & ",'" & SP_Rut_Activo & "','SI'," & TxtRut.Tag & "),"
        
        Next
        sql2 = Mid(sql2, 1, Len(sql2) - 1)
        cn.Execute Sql & sql2
    cn.CommitTrans
    
    'AHORA IMPRIMIMOS LA GUIA
    
    
    Dialogo.CancelError = True
    On Error GoTo CancelaImpesionNV
    Dialogo.ShowPrinter
    ImprimeGuiaFertiquimica
    
    TxtNroGuia = ""
    CboGuia.ListIndex = -1
    CmdBusca_Click
    Exit Sub
errorGraba:
    cn.RollbackTrans
    MsgBox Err.Description & vbNewLine & Err.Number
    Exit Sub
CancelaImpesionNV:
 'no imprime guia
End Sub

Private Sub CmdBusca_Click()
    Dim Sp_Ndoc As String * 10
    Dim Sp_Did As String * 3
    If CboDocVenta.ListIndex = -1 Then
        MsgBox "Seleccione Documento...", vbInformation
        CboDocVenta.SetFocus
        Exit Sub
    End If
    If Val(TxtNroDocumento) = 0 Then
        MsgBox "Ingrese Nro de documento...", vbInformation
        TxtNroDocumento.SetFocus
        Exit Sub
    End If
    '5 Diciembre 2013
    'Ahora buscamos el documento y el cliente
    LvDetalle.ListItems.Clear
    
    Sql = "SELECT v.rut_cliente,v.id,nombre_cliente,giro,ciudad,direccion,comuna," & _
            "IF(v.suc_id=0,'CASA MATRIZ',(SELECT CONCAT(s.suc_direccion,' - ',suc_ciudad) FROM par_sucursales s WHERE s.suc_id=v.suc_id)) sucursal_cliente, " & _
            "bod_nombre suc_empresa, Fecha, bruto " & _
        "FROM ven_doc_venta v " & _
        "JOIN maestro_clientes m ON v.rut_cliente=m.rut_cliente " & _
        "JOIN par_bodegas b ON v.sue_id=b.sue_id " & _
        "WHERE v.rut_emp='" & SP_Rut_Activo & "' AND b.rut_emp='" & SP_Rut_Activo & "' " & _
        "AND doc_id=" & CboDocVenta.ItemData(CboDocVenta.ListIndex) & " AND no_documento=" & TxtNroDocumento & " AND ven_entrega_inmediata='NO'"
        
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        With RsTmp
            TxtRut = !rut_cliente
            TxtRut.Tag = !Id
            Me.TxtRazonSocial = !nombre_cliente
            TxtGiro = !giro
            TxtDireccion = !direccion
            txtSucursalCliente = !sucursal_cliente
            txtComuna = !comuna
            TxtCiudad = !ciudad
            TxtFecha = !Fecha
            txtTotalDocumento = NumFormat(!bruto)
            Me.txtSucEmpresa = !suc_empresa
            'Combo con las guias emitidas
            Sql = "SELECT fecha,doc_nombre,no_documento,v.doc_id " & _
                    "FROM ven_doc_venta v " & _
                    "JOIN sis_documentos USING(doc_id) " & _
                    "WHERE ven_entrega='SI' AND ven_entrega_ref=" & TxtRut.Tag
            Consulta RsTmp, Sql
            CboEmitidas.Clear
            If RsTmp.RecordCount > 0 Then
                 RsTmp.MoveFirst
                 Do While Not RsTmp.EOF
                    Sp_Ndoc = RsTmp!no_documento
                    Sp_Did = RsTmp!doc_id
                    CboEmitidas.AddItem RsTmp!Fecha & "    -    " & RsTmp!doc_nombre & "       -       Nro " & Sp_Ndoc & Space(30) & Sp_Did
                    
                    'CboEmitidas.ItemData(CboEmitidas.ListCount - 1) = 111  'Sp_Ndoc & "    -    " & Sp_Did
                    RsTmp.MoveNext
                 Loop
            End If
            'Buscar los productos
            Sql = "SELECT  d.codigo,   m.descripcion,unidades venta," & _
                              "IFNULL((SELECT  SUM(unidades) " & _
                                    "FROM ven_detalle x " & _
                                    "WHERE   ven_entrega = 'SI' AND ven_entrega_ref =" & TxtRut.Tag & " AND x.codigo = d.codigo " & _
                                    "GROUP BY x.codigo),0)retirado," & _
                                    "unidades - IFNULL((SELECT   SUM(unidades) " & _
                                                        "FROM ven_detalle x " & _
                                                        "WHERE   ven_entrega = 'SI'AND ven_entrega_ref = " & TxtRut.Tag & " AND x.codigo = d.codigo " & _
                                                        "GROUP BY x.codigo),0)pendiente,ved_precio_venta_neto/unidades,ved_precio_venta_neto " & _
                        "FROM ven_detalle d " & _
                    "JOIN maestro_productos m ON d.codigo = m.codigo " & _
                    "WHERE d.rut_emp='" & SP_Rut_Activo & "' AND m.rut_emp='" & SP_Rut_Activo & "' " & _
                    "AND d.doc_id=" & CboDocVenta.ItemData(CboDocVenta.ListIndex) & " AND d.no_documento=" & TxtNroDocumento
            
           

            Consulta RsTmp, Sql
            LLenar_Grilla RsTmp, Me, LvDetalleDoc, False, True, True, False
        End With
    Else
        MsgBox "Documento no encontrado..." & vbNewLine & " O el documento no corresponde a pendiente de entrega", vbInformation
        TxtNroDocumento.SetFocus
    End If
    
    
End Sub

Private Sub CmdRetornar_Click()
    Unload Me
End Sub

Private Sub cmdVerGuia_Click()
    If CboEmitidas.ListIndex = -1 Then Exit Sub
    'Mostrar productos de la guia seleccinaaa
    Sql = "SELECT m.codigo,m.descripcion,unidades " & _
            "FROM ven_detalle d " & _
            "JOIN maestro_productos m ON d.codigo=m.codigo " & _
            "WHERE doc_id=" & Right(CboEmitidas.Text, 3) & " AND no_documento = " & Val(Mid(Right(CboEmitidas.Text, 43), 1, 10))
    Consulta RsTmp, Sql
    LvEmitidos.ListItems.Clear
    LLenar_Grilla RsTmp, Me, LvEmitidos, False, True, True, False
End Sub

Private Sub Form_Load()
    Aplicar_skin Me
    Centrar Me
    LLenarCombo Me.CboDocVenta, "doc_nombre", "doc_id", "sis_documentos", "doc_activo='SI' AND doc_documento='VENTA' AND doc_utiliza_en_caja='" & SG_Funcion_Equipo & "' AND doc_nombre LIKE '%FACTURA%'", "doc_id"
    LLenarCombo CboGuia, "doc_nombre", "doc_id", "sis_documentos", "doc_activo='SI' AND doc_documento='VENTA' AND doc_utiliza_en_caja='" & SG_Funcion_Equipo & "' AND doc_nombre LIKE '%GUIA%'", "doc_id"
    DtFechaGuia = Date
    
End Sub
Private Sub LvDetalle_DblClick()
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    
    LvDetalle.ListItems.Remove LvDetalle.SelectedItem.Index
End Sub

Private Sub LvDetalleDoc_DblClick()
    Dim Sp_Cantidad As String
    Dim Sp_Codigo As String
    Dim Bp_Agregar As Boolean
    'cantidad a entregar
    
    If LvDetalleDoc.SelectedItem Is Nothing Then Exit Sub
    
    Sp_Cantidad = InputBox("Ingrese cantidad a entregar de " & vbNewLine & LvDetalleDoc.SelectedItem.SubItems(1), "Entrega", LvDetalleDoc.SelectedItem.SubItems(4))
    If Val(Sp_Cantidad) = 0 Then Exit Sub
    If Val(Sp_Cantidad) > LvDetalleDoc.SelectedItem.SubItems(4) Then
        MsgBox "No puede superar lo pendiente...", vbInformation
        Exit Sub
    End If
    Sp_Codigo = LvDetalleDoc.SelectedItem
    
    'Ahora traspasar el producto a la guia con la cantidad ingresada
    '1ro revisar si el codigo ya existe en la guia
    Bp_Agregar = True
    For i = 1 To LvDetalle.ListItems.Count
        If Sp_Codigo = LvDetalle.ListItems(i) Then
            Bp_Agregar = False
            LvDetalle.ListItems(i).SubItems(1) = LvDetalleDoc.SelectedItem.SubItems(1)
            LvDetalle.ListItems(i).SubItems(2) = Sp_Cantidad
            LvDetalle.ListItems(i).SubItems(3) = LvDetalleDoc.SelectedItem.SubItems(5)
            LvDetalle.ListItems(i).SubItems(4) = CDbl(LvDetalleDoc.SelectedItem.SubItems(5)) * Val(Sp_Cantidad)
            Exit For
        End If
    Next
    If Bp_Agregar Then
        LvDetalle.ListItems.Add , , Sp_Codigo
        LvDetalle.ListItems(LvDetalle.ListItems.Count).SubItems(1) = LvDetalleDoc.SelectedItem.SubItems(1)
        LvDetalle.ListItems(LvDetalle.ListItems.Count).SubItems(2) = Sp_Cantidad
        LvDetalle.ListItems(LvDetalle.ListItems.Count).SubItems(3) = LvDetalleDoc.SelectedItem.SubItems(5)
        LvDetalle.ListItems(LvDetalle.ListItems.Count).SubItems(4) = CDbl(LvDetalleDoc.SelectedItem.SubItems(5)) * Val(Sp_Cantidad)
    End If
        
    txtTotalGuia = NumFormat(TotalizaColumna(LvDetalle, "total"))
        
End Sub



Private Sub Timer1_Timer()
    On Error Resume Next
    CboDocVenta.SetFocus
    Timer1.Enabled = False
End Sub



Private Sub TxtNroDocumento_GotFocus()
    En_Foco TxtNroDocumento
End Sub

Private Sub TxtNroDocumento_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
    If KeyAscii = 39 Then KeyAscii = 0
End Sub
Private Sub ImprimeGuiaFertiquimica()
    Dim Cx As Double, Cy As Double, Sp_Fecha As String, Ip_Mes As Integer, Dp_S As Double, Sp_Letras As String
    Dim Sp_Neto As String * 12, Sp_IVA As String * 12, Sp_Total As String * 12
    Dim Sp_GuiasFacturadas As String
    
    Dim p_Codigo As String * 5
    Dim p_Cantidad As String * 7
    Dim p_UM As String * 4
    Dim p_Detalle As String * 40
    Dim p_Unitario As String * 10
    Dim p_Total As String * 11
    Dim p_Mes As String * 10
    Dim p_CiudadF As String * 20
    
    On Error GoTo ProblemaImpresora
    Printer.FontName = "Courier New"
    Printer.FontSize = 10
    Printer.ScaleMode = 7
        
    'Aqui leemos el X y el Y iniciales de la tabla de sucursales
    Sql = "SELECT sue_cx,sue_cy " & _
            "FROM sis_empresas_sucursales " & _
            "WHERE sue_id=" & IG_id_Sucursal_Empresa
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        Cx = Val(RsTmp!sue_cx) + 0.2
        Cy = Val(RsTmp!sue_cy) + 0.2 'vertical)
    Else
        Cx = 1.1 'horizontal
        Cy = 3 'vertical
    
    End If
    Dp_S = 0.1
    
    Printer.CurrentX = Cx
    Printer.CurrentY = Cy
    Sp_Fecha = DtFechaGuia
    Ip_Mes = Val(Mid(Sp_Fecha, 4, 2))
    
    Printer.CurrentY = Printer.CurrentY + 1.9
    Printer.CurrentX = Cx + 5.2
    p_Mes = UCase(MonthName(Ip_Mes))
    RSet p_CiudadF = TxtCiudad
    Printer.Print p_CiudadF & "  " & Mid(Sp_Fecha, 1, 2) & Space(6) & p_Mes & Space(10) & Mid(Sp_Fecha, 7, 4)

    Printer.CurrentX = Cx - 0.4
  '  Printer.CurrentY = Printer.CurrentY + Dp_S
    pos = Printer.CurrentY
    Printer.Print TxtRazonSocial
    
    Printer.CurrentX = Cx + 12
    Printer.CurrentY = pos
    pos = Printer.CurrentY
    Printer.Print Me.TxtRut
    
   
    Printer.CurrentX = Cx - 0.4
   ' Printer.CurrentY = Printer.CurrentY + Dp_S
    pos = Printer.CurrentY
    posd = Printer.CurrentY
    Printer.Print TxtDireccion
    
    Printer.CurrentY = pos
    Printer.CurrentX = Printer.CurrentX + 10
    'Printer.Print Me.LbTelefono
    
    

    Printer.CurrentY = pos
    Printer.CurrentX = Cx + 12
    Printer.Print txtComuna
    
    
    
    pos = Printer.CurrentY
    Printer.CurrentY = pos
    Printer.CurrentX = Cx - 0.6
    Printer.Print TxtGiro
    
    Printer.CurrentY = pos
    Printer.CurrentX = Cx + 12
    Printer.Print TxtNroDocumento
    
    Printer.CurrentY = Printer.CurrentY + Dp_S
    pos = Printer.CurrentY
    
    Printer.CurrentX = Cx + 8.5
    Printer.CurrentY = Printer.CurrentY + 0.2
       
    Printer.CurrentY = Printer.CurrentY + 1
    
    
    Printer.CurrentY = Printer.CurrentY + 1
    
    For i = 1 To LvDetalle.ListItems.Count
        '6 - 3 - 7 - 8
        Printer.CurrentX = Printer.CurrentX + 0.2 'INTERLINIA ARTICULOS
       ' Printer.CurrentX = Cx - 2
        Printer.CurrentX = Cx - 1
        
        p_Codigo = LvDetalle.ListItems(i)
        RSet p_Cantidad = LvDetalle.ListItems(i).SubItems(2)
       p_UM = " " 'LvMateriales.ListItems(i).SubItems(21)
        p_Detalle = LvDetalle.ListItems(i).SubItems(1)
        RSet p_Unitario = NumFormat(Round(CDbl(LvDetalle.ListItems(i).SubItems(3))))
        RSet p_Total = NumFormat(Round(CDbl(LvDetalle.ListItems(i).SubItems(4))))
        
        Printer.Print p_Codigo & " " & p_Cantidad & " " & p_UM & " " & p_Detalle & "    " & p_Unitario & " " & p_Total
    Next
    
           
    
    
    
    Printer.CurrentX = Cx - 1
    Printer.CurrentY = Cy + 14.5
    pos = Printer.CurrentY
        
    Printer.CurrentX = Cx + 1
    Printer.CurrentY = pos - 2.2
   ' Printer.Print TxtComentario
    
  '  pos = Printer.CurrentY
  
    Printer.CurrentX = Cx + 1
        
    'Printer.CurrentY = pos
    Printer.CurrentY = pos - 1.4
    
   ' Sp_Letras = UCase(BrutoAletras(CDbl(SkBrutoMateriales), True))
    

    
    
    
    RSet Sp_Neto = txtTotalGuia
    RSet Sp_IVA = SkIvaMateriales
    RSet Sp_Total = SkBrutoMateriales
    pos = pos - 1.6
    'pos = pos - 1.4
    Printer.CurrentY = pos - 0.5
    Printer.CurrentX = Cx + 11.9
    Printer.Print "Total Neto $" & Sp_Neto
    
    Printer.CurrentY = Printer.CurrentY + 1
    
    Printer.CurrentX = Cx + 0.5
    Printer.Print TxtComentario
     
  
    
    Printer.NewPage
    Printer.EndDoc
    Exit Sub
ProblemaImpresora:
    MsgBox "Ocurrio un error al intentar imprimir..." & vbNewLine & Err.Description & vbNewLine & Err.Number

End Sub




