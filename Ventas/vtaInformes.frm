VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form vtaInformes 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Informes de Ventas"
   ClientHeight    =   7680
   ClientLeft      =   2175
   ClientTop       =   1515
   ClientWidth     =   12645
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7680
   ScaleWidth      =   12645
   ShowInTaskbar   =   0   'False
   Begin ACTIVESKINLibCtl.SkinLabel SkEmpresaActiva 
      Height          =   495
      Left            =   7320
      OleObjectBlob   =   "vtaInformes.frx":0000
      TabIndex        =   25
      Top             =   120
      Width           =   5055
   End
   Begin VB.Timer Timer1 
      Interval        =   10
      Left            =   765
      Top             =   7380
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   30
      OleObjectBlob   =   "vtaInformes.frx":007A
      Top             =   7365
   End
   Begin VB.Frame Frame4 
      Caption         =   "Informacion"
      Height          =   1230
      Left            =   285
      TabIndex        =   3
      Top             =   555
      Width           =   12060
      Begin VB.CommandButton CmdInformes 
         Caption         =   "Generar"
         Height          =   435
         Left            =   10380
         TabIndex        =   11
         Top             =   510
         Width           =   1455
      End
      Begin VB.Frame Frame3 
         Caption         =   "Intervalo"
         Height          =   960
         Left            =   2460
         TabIndex        =   6
         Top             =   195
         Width           =   2490
         Begin ACTIVESKINLibCtl.SkinLabel skIni 
            Height          =   240
            Left            =   165
            OleObjectBlob   =   "vtaInformes.frx":02AE
            TabIndex        =   7
            Top             =   315
            Width           =   705
         End
         Begin MSComCtl2.DTPicker DTInicio 
            Height          =   300
            Left            =   1005
            TabIndex        =   8
            Top             =   285
            Width           =   1215
            _ExtentX        =   2143
            _ExtentY        =   529
            _Version        =   393216
            Enabled         =   0   'False
            Format          =   91815937
            CurrentDate     =   40628
         End
         Begin MSComCtl2.DTPicker DtHasta 
            Height          =   285
            Left            =   1005
            TabIndex        =   9
            Top             =   600
            Width           =   1215
            _ExtentX        =   2143
            _ExtentY        =   503
            _Version        =   393216
            Enabled         =   0   'False
            Format          =   91815937
            CurrentDate     =   40628
         End
         Begin ACTIVESKINLibCtl.SkinLabel skFin 
            Height          =   240
            Left            =   165
            OleObjectBlob   =   "vtaInformes.frx":0316
            TabIndex        =   10
            Top             =   585
            Width           =   705
         End
      End
      Begin VB.OptionButton Option2 
         Caption         =   "Seleccionar fechas"
         Height          =   240
         Left            =   420
         TabIndex        =   5
         Top             =   765
         Width           =   1725
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Todas las fechas"
         Height          =   240
         Left            =   420
         TabIndex        =   4
         Top             =   450
         Value           =   -1  'True
         Width           =   1740
      End
   End
   Begin VB.CommandButton cmdSalir 
      Cancel          =   -1  'True
      Caption         =   "&Retornar"
      Height          =   405
      Left            =   10995
      TabIndex        =   1
      Top             =   7245
      Width           =   1350
   End
   Begin VB.Frame Frame1 
      Caption         =   "Filtros"
      Height          =   5475
      Left            =   270
      TabIndex        =   0
      Top             =   1860
      Width           =   12075
      Begin TabDlg.SSTab SSTab1 
         Height          =   4935
         Left            =   300
         TabIndex        =   2
         Top             =   375
         Width           =   11610
         _ExtentX        =   20479
         _ExtentY        =   8705
         _Version        =   393216
         Tab             =   1
         TabHeight       =   520
         TabCaption(0)   =   "Resumen"
         TabPicture(0)   =   "vtaInformes.frx":037E
         Tab(0).ControlEnabled=   0   'False
         Tab(0).Control(0)=   "Frame2"
         Tab(0).Control(1)=   "Frame5"
         Tab(0).ControlCount=   2
         TabCaption(1)   =   "Ventas Por Cliente"
         TabPicture(1)   =   "vtaInformes.frx":039A
         Tab(1).ControlEnabled=   -1  'True
         Tab(1).Control(0)=   "Frame6"
         Tab(1).Control(0).Enabled=   0   'False
         Tab(1).ControlCount=   1
         TabCaption(2)   =   "Venta por Productos"
         TabPicture(2)   =   "vtaInformes.frx":03B6
         Tab(2).ControlEnabled=   0   'False
         Tab(2).Control(0)=   "Frame7"
         Tab(2).ControlCount=   1
         Begin VB.Frame Frame7 
            Caption         =   "Resumen de ventas por productos"
            Height          =   4290
            Left            =   -74730
            TabIndex        =   23
            Top             =   540
            Width           =   11100
            Begin MSComctlLib.ListView LvVtaProductos 
               Height          =   3870
               Left            =   180
               TabIndex        =   24
               Top             =   285
               Width           =   10755
               _ExtentX        =   18971
               _ExtentY        =   6826
               View            =   3
               LabelEdit       =   1
               LabelWrap       =   -1  'True
               HideSelection   =   0   'False
               FullRowSelect   =   -1  'True
               GridLines       =   -1  'True
               _Version        =   393217
               ForeColor       =   -2147483640
               BackColor       =   -2147483643
               BorderStyle     =   1
               Appearance      =   0
               NumItems        =   6
               BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                  Object.Tag             =   "T3000"
                  Text            =   "Codigo"
                  Object.Width           =   2293
               EndProperty
               BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                  SubItemIndex    =   1
                  Object.Tag             =   "T2000"
                  Text            =   "Marca"
                  Object.Width           =   3528
               EndProperty
               BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                  SubItemIndex    =   2
                  Object.Tag             =   "T2500"
                  Text            =   "Descripcion"
                  Object.Width           =   4410
               EndProperty
               BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                  Alignment       =   1
                  SubItemIndex    =   3
                  Object.Tag             =   "N100"
                  Text            =   "Cant."
                  Object.Width           =   2540
               EndProperty
               BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                  Alignment       =   1
                  SubItemIndex    =   4
                  Object.Tag             =   "N100"
                  Text            =   "Precio Promedio"
                  Object.Width           =   2646
               EndProperty
               BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                  Alignment       =   1
                  SubItemIndex    =   5
                  Key             =   "total"
                  Object.Tag             =   "N100"
                  Text            =   "Total"
                  Object.Width           =   2646
               EndProperty
            End
         End
         Begin VB.Frame Frame6 
            Caption         =   "Resumen de ventas por cliente"
            Height          =   4290
            Left            =   225
            TabIndex        =   18
            Top             =   480
            Width           =   11100
            Begin VB.TextBox txtIVA 
               Alignment       =   1  'Right Justify
               BackColor       =   &H8000000F&
               Height          =   315
               Left            =   7695
               Locked          =   -1  'True
               TabIndex        =   22
               TabStop         =   0   'False
               Tag             =   "T"
               Top             =   3795
               Width           =   1400
            End
            Begin VB.TextBox txtNeto 
               Alignment       =   1  'Right Justify
               BackColor       =   &H8000000F&
               Height          =   315
               Left            =   6180
               Locked          =   -1  'True
               TabIndex        =   21
               TabStop         =   0   'False
               Tag             =   "T"
               Top             =   3795
               Width           =   1500
            End
            Begin VB.TextBox TxtBruto 
               Alignment       =   1  'Right Justify
               BackColor       =   &H8000000F&
               Height          =   315
               Left            =   9105
               Locked          =   -1  'True
               TabIndex        =   19
               TabStop         =   0   'False
               Tag             =   "T"
               Top             =   3795
               Width           =   1500
            End
            Begin MSComctlLib.ListView LVVtasPorClientes 
               Height          =   3480
               Left            =   180
               TabIndex        =   20
               Top             =   285
               Width           =   10755
               _ExtentX        =   18971
               _ExtentY        =   6138
               View            =   3
               LabelEdit       =   1
               LabelWrap       =   -1  'True
               HideSelection   =   0   'False
               FullRowSelect   =   -1  'True
               GridLines       =   -1  'True
               _Version        =   393217
               ForeColor       =   -2147483640
               BackColor       =   -2147483643
               BorderStyle     =   1
               Appearance      =   0
               NumItems        =   4
               BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                  Object.Tag             =   "T3000"
                  Text            =   "Cliente"
                  Object.Width           =   10583
               EndProperty
               BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                  Alignment       =   1
                  SubItemIndex    =   1
                  Key             =   "neto"
                  Object.Tag             =   "N100"
                  Text            =   "Neto"
                  Object.Width           =   2646
               EndProperty
               BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                  Alignment       =   1
                  SubItemIndex    =   2
                  Key             =   "iva"
                  Object.Tag             =   "N100"
                  Text            =   "IVA"
                  Object.Width           =   2469
               EndProperty
               BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                  Alignment       =   1
                  SubItemIndex    =   3
                  Key             =   "total"
                  Object.Tag             =   "N100"
                  Text            =   "Total"
                  Object.Width           =   2646
               EndProperty
            End
         End
         Begin VB.Frame Frame5 
            Caption         =   "Formas de pago"
            Height          =   3915
            Left            =   -68880
            TabIndex        =   13
            Top             =   630
            Width           =   5250
            Begin VB.TextBox TxtFpagos 
               Alignment       =   1  'Right Justify
               BackColor       =   &H8000000F&
               Height          =   300
               Left            =   3660
               Locked          =   -1  'True
               TabIndex        =   17
               TabStop         =   0   'False
               Tag             =   "T"
               Top             =   3465
               Width           =   1125
            End
            Begin MSComctlLib.ListView LvFpagos 
               Height          =   3090
               Left            =   180
               TabIndex        =   16
               Top             =   285
               Width           =   4905
               _ExtentX        =   8652
               _ExtentY        =   5450
               View            =   3
               LabelEdit       =   1
               LabelWrap       =   -1  'True
               HideSelection   =   0   'False
               FullRowSelect   =   -1  'True
               GridLines       =   -1  'True
               _Version        =   393217
               ForeColor       =   -2147483640
               BackColor       =   -2147483643
               BorderStyle     =   1
               Appearance      =   0
               NumItems        =   2
               BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                  Object.Tag             =   "T3000"
                  Text            =   "Forma de Pago"
                  Object.Width           =   5292
               EndProperty
               BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                  Alignment       =   1
                  SubItemIndex    =   1
                  Key             =   "total"
                  Object.Tag             =   "N100"
                  Text            =   "Monto"
                  Object.Width           =   2646
               EndProperty
            End
         End
         Begin VB.Frame Frame2 
            Caption         =   "Ventas por documentos"
            Height          =   3915
            Left            =   -74835
            TabIndex        =   12
            Top             =   645
            Width           =   5805
            Begin VB.TextBox TxtTotalVtaDoc 
               Alignment       =   1  'Right Justify
               BackColor       =   &H8000000F&
               Height          =   315
               Left            =   4290
               Locked          =   -1  'True
               TabIndex        =   15
               TabStop         =   0   'False
               Tag             =   "T"
               Top             =   3435
               Width           =   1125
            End
            Begin MSComctlLib.ListView LvDocumentos 
               Height          =   3090
               Left            =   195
               TabIndex        =   14
               Top             =   270
               Width           =   5505
               _ExtentX        =   9710
               _ExtentY        =   5450
               View            =   3
               LabelEdit       =   1
               LabelWrap       =   -1  'True
               HideSelection   =   0   'False
               FullRowSelect   =   -1  'True
               GridLines       =   -1  'True
               _Version        =   393217
               ForeColor       =   -2147483640
               BackColor       =   -2147483643
               BorderStyle     =   1
               Appearance      =   0
               NumItems        =   3
               BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                  Object.Tag             =   "T3000"
                  Text            =   "Doc. Venta"
                  Object.Width           =   4939
               EndProperty
               BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                  SubItemIndex    =   1
                  Object.Tag             =   "N109"
                  Text            =   "Cant. Docs"
                  Object.Width           =   2117
               EndProperty
               BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                  Alignment       =   1
                  SubItemIndex    =   2
                  Key             =   "total"
                  Object.Tag             =   "N100"
                  Text            =   "Monto"
                  Object.Width           =   2117
               EndProperty
            End
         End
      End
   End
End
Attribute VB_Name = "vtaInformes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim sm_FiltroFecha As String

Private Sub CmdInformes_Click()
    If Option2.Value Then
        sm_FiltroFecha = " v.fecha BETWEEN '" & Format(DTInicio.Value, "YYYY-MM-DD") & "' AND '" & Format(DtHasta.Value, "YYYY-MM-DD") & "' "
    Else
        sm_FiltroFecha = "     1=1"
    End If
    'Por documentos de venta
    Sql = "SELECT doc_nombre,COUNT(*), SUM(bruto) " & _
          "FROM ven_doc_venta v " & _
          "INNER JOIN sis_documentos d USING(doc_id) " & _
          "WHERE v.rut_emp='" & SP_Rut_Activo & "' AND  " & Mid(sm_FiltroFecha, 4) & " AND doc_signo_libro='+' " & _
          "GROUP BY v.doc_id"
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvDocumentos, False, True, True, False
    
    'Notas de credito
    Sql = "SELECT doc_nombre,COUNT(*) cant, SUM(bruto) total " & _
          "FROM ven_doc_venta v " & _
          "INNER JOIN sis_documentos d USING(doc_id) " & _
          "WHERE v.rut_emp='" & SP_Rut_Activo & "' AND  " & Mid(sm_FiltroFecha, 4) & " AND doc_signo_libro='-' " & _
          "GROUP BY v.doc_id"
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        LvDocumentos.ListItems.Add , , " "
        LvDocumentos.ListItems.Add , , "Notas de Credito"
        LvDocumentos.ListItems.Add , , RsTmp!doc_nombre
        LvDocumentos.ListItems(LvDocumentos.ListItems.Count).SubItems(1) = RsTmp!cant
        LvDocumentos.ListItems(LvDocumentos.ListItems.Count).SubItems(2) = RsTmp!Total
    End If
    
    'Guias sin facturar
    Sql = "SELECT doc_nombre,COUNT(*) cant, SUM(bruto) total " & _
          "FROM ven_doc_venta v " & _
          "INNER JOIN sis_documentos d USING(doc_id) " & _
          "WHERE  v.rut_emp='" & SP_Rut_Activo & "' AND  " & Mid(sm_FiltroFecha, 4) & " AND doc_signo_libro='' AND v.nro_factura=0 " & _
          "GROUP BY v.doc_id"
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        LvDocumentos.ListItems.Add , , " "
        LvDocumentos.ListItems.Add , , "Documentos sin Facturar"
        LvDocumentos.ListItems.Add , , RsTmp!doc_nombre
        LvDocumentos.ListItems(LvDocumentos.ListItems.Count).SubItems(1) = RsTmp!cant
        LvDocumentos.ListItems(LvDocumentos.ListItems.Count).SubItems(2) = RsTmp!Total
    End If
    
    
    TxtTotalVtaDoc = NumFormat(TotalizaColumna(LvDocumentos, "total"))
    '********************************************************************
    'Ingresos por tipo
    Sql = "SELECT p.mpa_nombre,SUM(d.ctd_monto) " & _
          "FROM ven_doc_venta v,cta_abono_documentos d,cta_abonos b,par_medios_de_pago p " & _
          "WHERE  v.rut_emp='" & SP_Rut_Activo & "' AND  abo_cli_pro='CLI' AND v.id=d.id AND d.abo_id=b.abo_id AND b.mpa_id=p.mpa_id AND " & sm_FiltroFecha & " " & _
          "GROUP BY b.mpa_id "
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvFpagos, False, True, True, False
    TxtFpagos = NumFormat(TotalizaColumna(LvFpagos, "total"))
    
    
    '********************************************************************
    'VENTAS POR CLIENTES
    
    Sql = "SELECT v.nombre_cliente,SUM(neto),SUM(iva),SUM(bruto) " & _
          "FROM ven_doc_venta v INNER JOIN maestro_clientes c USING(rut_cliente) " & _
          "WHERE  v.rut_emp='" & SP_Rut_Activo & "' AND  doc_id <> 3 AND " & sm_FiltroFecha & " " & _
          "GROUP BY v.rut_cliente"
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LVVtasPorClientes, False, True, True, False
    TxtNeto = NumFormat(TotalizaColumna(LVVtasPorClientes, "neto"))
    TxtIva = NumFormat(TotalizaColumna(LVVtasPorClientes, "iva"))
    TxtBruto = NumFormat(TotalizaColumna(LVVtasPorClientes, "total"))
    
    ''********************************************************************
    'VENTAS POR PRODUCTOS
    Sql = "SELECT d.codigo,marca,d.descripcion,SUM(d.unidades), AVG(d.precio_final),SUM(subtotal) " & _
          "FROM ven_doc_venta v INNER JOIN ven_detalle d USING(id) " & _
          "WHERE  v.rut_emp='" & SP_Rut_Activo & "' AND d.rut_emp='" & SP_Rut_Activo & "' AND  v.doc_id <> 3 AND " & sm_FiltroFecha & " " & _
          "GROUP BY d.codigo"
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvVtaProductos, False, True, True, False
    
End Sub

Private Sub cmdSalir_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    Centrar Me
    Aplicar_skin Me
    sm_FiltroFecha = Empty
    DtHasta.Value = Date
    DTInicio.Value = Date
End Sub


'Private Sub LvVtaProductos_BeforeLabelEdit(Cancel As Integer)
 '   ordListView ColumnHeader, Me, LvVtaProductos
'End Sub

Private Sub Option1_Click()
    If Option1.Value Then
        DTInicio.Enabled = False
        DtHasta.Enabled = False
    Else
        DTInicio.Enabled = True
        DtHasta.Enabled = True
    End If
    
End Sub

Private Sub Option2_Click()
    If Option2.Value Then
        DTInicio.Enabled = True
        DtHasta.Enabled = True
    Else
        DTInicio.Enabled = False
        DtHasta.Enabled = False
    End If
End Sub
Private Sub Timer1_Timer()
    Option1.SetFocus
    Timer1.Enabled = False
End Sub

