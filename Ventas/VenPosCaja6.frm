VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{28D47522-CF84-11D1-834C-00A0249F0C28}#1.0#0"; "gif89.dll"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form VenPosCaja 
   Caption         =   "Caja"
   ClientHeight    =   8550
   ClientLeft      =   2295
   ClientTop       =   2895
   ClientWidth     =   16995
   LinkTopic       =   "Form1"
   ScaleHeight     =   570
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   1133
   Begin VB.Frame FrmLoad 
      Height          =   1905
      Left            =   4665
      TabIndex        =   77
      Top             =   6720
      Visible         =   0   'False
      Width           =   2805
      Begin GIF89LibCtl.Gif89a Gif89a1 
         Height          =   1725
         Left            =   210
         OleObjectBlob   =   "VenPosCaja6.frx":0000
         TabIndex        =   78
         Top             =   285
         Width           =   2445
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Productos"
      Height          =   3165
      Left            =   12375
      TabIndex        =   65
      Top             =   30
      Width           =   4620
      Begin MSComctlLib.ListView LvProductos 
         Height          =   2730
         Left            =   105
         TabIndex        =   66
         Top             =   330
         Width           =   4335
         _ExtentX        =   7646
         _ExtentY        =   4815
         View            =   3
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   16777152
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   6
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "T1000"
            Text            =   "id"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "N109"
            Text            =   "Codigo"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "T2000"
            Text            =   "Producto"
            Object.Width           =   5292
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Object.Tag             =   "N102"
            Text            =   "Cantidad"
            Object.Width           =   1587
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   4
            Object.Tag             =   "N102"
            Text            =   "Precio"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Object.Tag             =   "T1000"
            Text            =   "UM"
            Object.Width           =   2540
         EndProperty
      End
   End
   Begin VB.Frame FrmDte 
      Height          =   885
      Left            =   240
      TabIndex        =   57
      Top             =   1575
      Visible         =   0   'False
      Width           =   11985
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel8 
         Height          =   570
         Left            =   645
         OleObjectBlob   =   "VenPosCaja6.frx":0086
         TabIndex        =   58
         Top             =   240
         Width           =   10410
      End
   End
   Begin VB.CommandButton Cmd10p 
      BackColor       =   &H00FFFFFF&
      Height          =   1065
      Left            =   4125
      Picture         =   "VenPosCaja6.frx":016A
      Style           =   1  'Graphical
      TabIndex        =   44
      Tag             =   "10"
      Top             =   7095
      Width           =   1140
   End
   Begin VB.CommandButton Cmd100p 
      BackColor       =   &H00FFFFFF&
      Height          =   1080
      Left            =   4140
      Picture         =   "VenPosCaja6.frx":0FAF
      Style           =   1  'Graphical
      TabIndex        =   43
      Tag             =   "100"
      Top             =   4785
      Width           =   1095
   End
   Begin VB.CommandButton Cmd50p 
      BackColor       =   &H00FFFFFF&
      Height          =   1035
      Left            =   4140
      Picture         =   "VenPosCaja6.frx":1D8B
      Style           =   1  'Graphical
      TabIndex        =   37
      Tag             =   "50"
      Top             =   5955
      Width           =   1065
   End
   Begin VB.CommandButton Cmd500p 
      Height          =   1170
      Left            =   4110
      Picture         =   "VenPosCaja6.frx":5226
      Style           =   1  'Graphical
      TabIndex        =   36
      Tag             =   "500"
      Top             =   3570
      Width           =   1125
   End
   Begin VB.Frame FraFPago 
      Caption         =   "F2 - Forma de pago"
      ForeColor       =   &H00000000&
      Height          =   8085
      Left            =   255
      TabIndex        =   6
      Top             =   3285
      Width           =   16650
      Begin ACTIVESKINLibCtl.SkinLabel SkImpresoraEnUso 
         Height          =   210
         Left            =   7530
         OleObjectBlob   =   "VenPosCaja6.frx":630E
         TabIndex        =   76
         Top             =   4470
         Width           =   1560
      End
      Begin VB.TextBox TxtEfectivoReal 
         Height          =   240
         Left            =   2205
         Locked          =   -1  'True
         TabIndex        =   74
         Text            =   "0"
         Top             =   105
         Width           =   435
      End
      Begin MSComCtl2.DTPicker DtFechaFac 
         Height          =   270
         Left            =   11985
         TabIndex        =   73
         Top             =   510
         Visible         =   0   'False
         Width           =   1245
         _ExtentX        =   2196
         _ExtentY        =   476
         _Version        =   393216
         Format          =   91684865
         CurrentDate     =   42502
      End
      Begin VB.TextBox TxtNextVoucher 
         Height          =   285
         Left            =   15000
         TabIndex        =   71
         Text            =   "0"
         Top             =   345
         Visible         =   0   'False
         Width           =   1260
      End
      Begin VB.TextBox TxtNextBoleta 
         Height          =   285
         Left            =   8730
         TabIndex        =   70
         Text            =   "0"
         Top             =   345
         Visible         =   0   'False
         Width           =   1335
      End
      Begin VB.TextBox TxtNextFactura 
         Height          =   285
         Left            =   11970
         TabIndex        =   69
         Text            =   "0"
         Top             =   210
         Visible         =   0   'False
         Width           =   1245
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel10 
         Height          =   315
         Index           =   0
         Left            =   7320
         OleObjectBlob   =   "VenPosCaja6.frx":636E
         TabIndex        =   67
         Top             =   345
         Visible         =   0   'False
         Width           =   1425
      End
      Begin VB.PictureBox Picture1 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFFF&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   4755
         Left            =   315
         ScaleHeight     =   4755
         ScaleWidth      =   3345
         TabIndex        =   38
         Top             =   285
         Width           =   3345
         Begin VB.ComboBox CboPlazos 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            ItemData        =   "VenPosCaja6.frx":63F0
            Left            =   690
            List            =   "VenPosCaja6.frx":63F2
            Style           =   2  'Dropdown List
            TabIndex        =   62
            Top             =   4440
            Width           =   2550
         End
         Begin VB.TextBox TxtDeposito 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFF80&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   15.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   480
            Left            =   1575
            TabIndex        =   60
            Tag             =   "4"
            Text            =   "0"
            ToolTipText     =   "Deposito"
            Top             =   1845
            Width           =   1665
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel6 
            Height          =   240
            Left            =   45
            OleObjectBlob   =   "VenPosCaja6.frx":63F4
            TabIndex        =   53
            Top             =   3750
            Width           =   630
         End
         Begin VB.TextBox TxtNombreCliente 
            Appearance      =   0  'Flat
            BackColor       =   &H0080FF80&
            BeginProperty Font 
               Name            =   "Arial Narrow"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   465
            Left            =   690
            Locked          =   -1  'True
            MultiLine       =   -1  'True
            TabIndex        =   52
            TabStop         =   0   'False
            Top             =   4005
            Width           =   2595
         End
         Begin VB.TextBox TxtRutCliente 
            Appearance      =   0  'Flat
            BackColor       =   &H0080FF80&
            BeginProperty Font 
               Name            =   "Arial Narrow"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   690
            Locked          =   -1  'True
            TabIndex        =   51
            TabStop         =   0   'False
            Top             =   3585
            Width           =   2595
         End
         Begin VB.TextBox TxtCredito 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFF80&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   15.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   480
            Left            =   1560
            TabIndex        =   50
            Tag             =   "5"
            Text            =   "0"
            ToolTipText     =   "Credito"
            Top             =   3030
            Width           =   1665
         End
         Begin VB.TextBox TxtEfectivo 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFF80&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   15.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   480
            Left            =   1560
            TabIndex        =   42
            Tag             =   "1"
            Text            =   "0"
            Top             =   60
            Width           =   1650
         End
         Begin VB.TextBox TxtTC 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFF80&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   15.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   480
            Left            =   1560
            TabIndex        =   41
            Tag             =   "9"
            Text            =   "0"
            Top             =   660
            Width           =   1665
         End
         Begin VB.TextBox TxtTD 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFF80&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   15.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   480
            Left            =   1560
            TabIndex        =   40
            Tag             =   "8"
            Text            =   "0"
            Top             =   1230
            Width           =   1665
         End
         Begin VB.TextBox TxtCh 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFF80&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   15.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   480
            Left            =   1560
            TabIndex        =   39
            Tag             =   "2"
            Text            =   "0"
            Top             =   2430
            Width           =   1665
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel7 
            Height          =   240
            Left            =   30
            OleObjectBlob   =   "VenPosCaja6.frx":6458
            TabIndex        =   54
            Top             =   4005
            Width           =   630
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel9 
            Height          =   285
            Left            =   165
            OleObjectBlob   =   "VenPosCaja6.frx":64C2
            TabIndex        =   63
            Top             =   4455
            Width           =   420
         End
         Begin VB.Image ImaDeposito 
            Height          =   510
            Left            =   135
            Picture         =   "VenPosCaja6.frx":6523
            Stretch         =   -1  'True
            ToolTipText     =   "Deposito"
            Top             =   1815
            Width           =   1305
         End
         Begin VB.Image ImaCredito 
            Height          =   615
            Left            =   120
            Picture         =   "VenPosCaja6.frx":6F97
            ToolTipText     =   "Credito"
            Top             =   2970
            Width           =   1335
         End
         Begin VB.Image ImaEfectivo 
            Height          =   570
            Left            =   165
            Picture         =   "VenPosCaja6.frx":7AC3
            Top             =   45
            Width           =   1245
         End
         Begin VB.Image ImaTC 
            Height          =   540
            Left            =   165
            Picture         =   "VenPosCaja6.frx":8454
            Top             =   645
            Width           =   1350
         End
         Begin VB.Image ImaTD 
            Height          =   600
            Left            =   165
            Picture         =   "VenPosCaja6.frx":90C4
            Top             =   1215
            Width           =   1260
         End
         Begin VB.Image ImaCh 
            Height          =   585
            Left            =   105
            Picture         =   "VenPosCaja6.frx":9AF8
            Top             =   2385
            Width           =   1380
         End
      End
      Begin VB.Frame Frame2 
         Height          =   4965
         Left            =   3765
         TabIndex        =   35
         Top             =   105
         Width           =   3450
         Begin VB.CommandButton CmdBillete 
            Height          =   900
            Index           =   0
            Left            =   1440
            Picture         =   "VenPosCaja6.frx":A6A3
            Style           =   1  'Graphical
            TabIndex        =   49
            Tag             =   "1000"
            Top             =   165
            Width           =   1845
         End
         Begin VB.CommandButton CmdBillete 
            Height          =   900
            Index           =   1
            Left            =   1440
            Picture         =   "VenPosCaja6.frx":BABA
            Style           =   1  'Graphical
            TabIndex        =   48
            Tag             =   "2000"
            Top             =   1125
            Width           =   1845
         End
         Begin VB.CommandButton CmdBillete 
            Height          =   930
            Index           =   4
            Left            =   1455
            Picture         =   "VenPosCaja6.frx":CE72
            Style           =   1  'Graphical
            TabIndex        =   47
            Tag             =   "20000"
            Top             =   3945
            Width           =   1845
         End
         Begin VB.CommandButton CmdBillete 
            Height          =   900
            Index           =   2
            Left            =   1440
            Picture         =   "VenPosCaja6.frx":E621
            Style           =   1  'Graphical
            TabIndex        =   46
            Tag             =   "5000"
            Top             =   2055
            Width           =   1845
         End
         Begin VB.CommandButton CmdBillete 
            Height          =   900
            Index           =   3
            Left            =   1455
            Picture         =   "VenPosCaja6.frx":FAAF
            Style           =   1  'Graphical
            TabIndex        =   45
            Tag             =   "10000"
            Top             =   3000
            Width           =   1845
         End
      End
      Begin VB.CommandButton CmdSalir 
         BackColor       =   &H00FFFFFF&
         Caption         =   "&Atras"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   12150
         MaskColor       =   &H00FF0000&
         TabIndex        =   18
         Top             =   4320
         Width           =   3975
      End
      Begin VB.CommandButton CmdF5 
         BackColor       =   &H00FFFFFF&
         Caption         =   "&F5 - Emitir"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   14.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   930
         Left            =   7530
         MaskColor       =   &H00FFFFFF&
         TabIndex        =   17
         Top             =   3150
         Width           =   8715
      End
      Begin VB.Frame FraCheques 
         Caption         =   "Cheques"
         ForeColor       =   &H00000000&
         Height          =   2565
         Left            =   285
         TabIndex        =   16
         Top             =   5265
         Width           =   8730
         Begin VB.TextBox TxtTotalCheques 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00E0E0E0&
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Left            =   6675
            Locked          =   -1  'True
            TabIndex        =   33
            TabStop         =   0   'False
            Text            =   "0"
            Top             =   2145
            Width           =   1200
         End
         Begin VB.CommandButton CmdOkCheque 
            Caption         =   "Ok"
            Height          =   285
            Left            =   7845
            TabIndex        =   30
            Top             =   480
            Width           =   600
         End
         Begin VB.TextBox TxtMontoCheque 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFF80&
            Height          =   300
            Left            =   6630
            TabIndex        =   29
            Top             =   495
            Width           =   1200
         End
         Begin VB.TextBox TxtNroCheque 
            BackColor       =   &H00FFFF80&
            Height          =   300
            Left            =   1455
            TabIndex        =   22
            Top             =   495
            Width           =   1000
         End
         Begin VB.TextBox TxtRutCheque 
            BackColor       =   &H00FFFF80&
            Height          =   300
            Left            =   255
            TabIndex        =   21
            Top             =   495
            Width           =   1200
         End
         Begin VB.TextBox TxtPlaza 
            BackColor       =   &H00FFFF80&
            Height          =   300
            Left            =   3960
            TabIndex        =   25
            Top             =   495
            Width           =   1440
         End
         Begin VB.ComboBox CboBanco 
            BackColor       =   &H00FFFF80&
            Height          =   315
            Left            =   2475
            Style           =   2  'Dropdown List
            TabIndex        =   24
            Top             =   495
            Width           =   1500
         End
         Begin MSComctlLib.ListView LvCheques 
            Height          =   1260
            Left            =   270
            TabIndex        =   19
            Top             =   855
            Width           =   8205
            _ExtentX        =   14473
            _ExtentY        =   2223
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            FullRowSelect   =   -1  'True
            GridLines       =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   12648384
            BorderStyle     =   1
            Appearance      =   0
            NumItems        =   9
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Object.Tag             =   "N100"
               Text            =   "id doc"
               Object.Width           =   0
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Object.Tag             =   "T1000"
               Text            =   "RUT"
               Object.Width           =   2117
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   2
               Object.Tag             =   "N109"
               Text            =   "Numero"
               Object.Width           =   1764
            EndProperty
            BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   3
               Object.Tag             =   "T1500"
               Text            =   "Banco"
               Object.Width           =   2646
            EndProperty
            BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   4
               Object.Tag             =   "T1000"
               Text            =   "Plaza"
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   5
               Object.Tag             =   "F1000"
               Text            =   "Fecha"
               Object.Width           =   2196
            EndProperty
            BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   6
               Key             =   "total"
               Object.Tag             =   "N100"
               Text            =   "Monto"
               Object.Width           =   2117
            EndProperty
            BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   7
               Object.Tag             =   "T1000"
               Text            =   "Observacion"
               Object.Width           =   3528
            EndProperty
            BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   8
               Object.Tag             =   "N109"
               Text            =   "banco_id"
               Object.Width           =   2540
            EndProperty
         End
         Begin MSComCtl2.DTPicker DtFecha 
            Height          =   315
            Left            =   5400
            TabIndex        =   27
            Top             =   495
            Width           =   1245
            _ExtentX        =   2196
            _ExtentY        =   556
            _Version        =   393216
            CalendarBackColor=   16777152
            CalendarTitleBackColor=   16777088
            Format          =   91684865
            CurrentDate     =   40731
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
            Height          =   165
            Index           =   0
            Left            =   3975
            OleObjectBlob   =   "VenPosCaja6.frx":10F8A
            TabIndex        =   20
            Top             =   300
            Width           =   885
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel5 
            Height          =   165
            Left            =   270
            OleObjectBlob   =   "VenPosCaja6.frx":10FF2
            TabIndex        =   23
            Top             =   300
            Width           =   885
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
            Height          =   165
            Index           =   1
            Left            =   1470
            OleObjectBlob   =   "VenPosCaja6.frx":11056
            TabIndex        =   26
            Top             =   300
            Width           =   885
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
            Height          =   165
            Index           =   2
            Left            =   2475
            OleObjectBlob   =   "VenPosCaja6.frx":110C0
            TabIndex        =   28
            Top             =   300
            Width           =   885
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
            Height          =   165
            Index           =   3
            Left            =   5400
            OleObjectBlob   =   "VenPosCaja6.frx":11128
            TabIndex        =   31
            Top             =   300
            Width           =   885
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
            Height          =   165
            Index           =   4
            Left            =   6645
            OleObjectBlob   =   "VenPosCaja6.frx":11190
            TabIndex        =   32
            Top             =   300
            Width           =   1125
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
            Height          =   240
            Index           =   5
            Left            =   4410
            OleObjectBlob   =   "VenPosCaja6.frx":111F8
            TabIndex        =   34
            Top             =   2160
            Width           =   2205
         End
      End
      Begin VB.Frame FrmPago 
         Caption         =   "Pagando NV"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   2295
         Left            =   7545
         TabIndex        =   8
         Top             =   765
         Width           =   8730
         Begin VB.TextBox TxtSaldoPago 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFF80&
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   14.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   540
            Left            =   2220
            Locked          =   -1  'True
            TabIndex        =   14
            TabStop         =   0   'False
            Text            =   "0"
            Top             =   1575
            Width           =   3825
         End
         Begin VB.TextBox TxtAPagar 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFF80&
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   14.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   540
            Left            =   2220
            Locked          =   -1  'True
            TabIndex        =   13
            TabStop         =   0   'False
            Text            =   "0"
            Top             =   480
            Width           =   3825
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
            Height          =   390
            Index           =   0
            Left            =   120
            OleObjectBlob   =   "VenPosCaja6.frx":11276
            TabIndex        =   10
            Top             =   1155
            Width           =   1890
         End
         Begin VB.TextBox TxtSumaPagos 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFF80&
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   14.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   525
            Left            =   2220
            Locked          =   -1  'True
            TabIndex        =   9
            TabStop         =   0   'False
            Text            =   "0"
            Top             =   1035
            Width           =   3810
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
            Height          =   375
            Index           =   1
            Left            =   120
            OleObjectBlob   =   "VenPosCaja6.frx":112E4
            TabIndex        =   11
            Top             =   600
            Width           =   1845
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
            Height          =   330
            Index           =   2
            Left            =   120
            OleObjectBlob   =   "VenPosCaja6.frx":11348
            TabIndex        =   12
            Top             =   1665
            Width           =   1830
         End
      End
      Begin VB.TextBox TxtTemp 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFF80&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   -1140
         TabIndex        =   7
         Tag             =   "N"
         Text            =   "0"
         Top             =   360
         Visible         =   0   'False
         Width           =   1470
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel10 
         Height          =   315
         Index           =   1
         Left            =   10470
         OleObjectBlob   =   "VenPosCaja6.frx":113AA
         TabIndex        =   68
         Top             =   375
         Visible         =   0   'False
         Width           =   1455
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel10 
         Height          =   315
         Index           =   2
         Left            =   13380
         OleObjectBlob   =   "VenPosCaja6.frx":1142E
         TabIndex        =   72
         Top             =   375
         Visible         =   0   'False
         Width           =   1515
      End
      Begin MSComctlLib.ListView LvCuotas 
         Height          =   3330
         Left            =   9690
         TabIndex        =   75
         Top             =   4800
         Visible         =   0   'False
         Width           =   6675
         _ExtentX        =   11774
         _ExtentY        =   5874
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   3
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Numero"
            Object.Width           =   3528
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Fecha"
            Object.Width           =   3528
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Monto"
            Object.Width           =   3528
         EndProperty
      End
   End
   Begin VB.Frame FraNVs 
      Caption         =   "Notas de Venta"
      ForeColor       =   &H00000000&
      Height          =   3165
      Left            =   225
      TabIndex        =   0
      Top             =   60
      Width           =   12030
      Begin VB.CommandButton CmdAnular 
         Caption         =   "&Eliminar"
         Height          =   375
         Left            =   240
         TabIndex        =   59
         Top             =   2700
         Width           =   1665
      End
      Begin VB.CommandButton CmdModifica 
         Caption         =   "&Modificar"
         Height          =   375
         Left            =   7515
         TabIndex        =   55
         Top             =   2700
         Width           =   1665
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   255
         Left            =   2700
         OleObjectBlob   =   "VenPosCaja6.frx":114B2
         TabIndex        =   15
         Top             =   2715
         Width           =   3255
      End
      Begin VB.CommandButton CmdSeleccion 
         Caption         =   "&Seleccionar"
         Height          =   375
         Left            =   9315
         TabIndex        =   5
         Top             =   2700
         Width           =   2175
      End
      Begin VB.TextBox TxtScanner 
         Alignment       =   2  'Center
         BackColor       =   &H00000000&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H0000FFFF&
         Height          =   405
         Left            =   3435
         TabIndex        =   4
         Top             =   240
         Width           =   2535
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   255
         Left            =   1755
         OleObjectBlob   =   "VenPosCaja6.frx":11564
         TabIndex        =   3
         Top             =   360
         Width           =   1575
      End
      Begin VB.CommandButton CmdBuscar 
         Caption         =   "Actualizar"
         Height          =   435
         Left            =   10305
         TabIndex        =   1
         Top             =   240
         Width           =   1575
      End
      Begin MSComctlLib.ListView LvDetalle 
         Height          =   1935
         Left            =   240
         TabIndex        =   2
         Top             =   705
         Width           =   11640
         _ExtentX        =   20532
         _ExtentY        =   3413
         View            =   3
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   16777152
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   17
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "T1000"
            Text            =   "id"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "N109"
            Text            =   "Nro Doc."
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "T1500"
            Text            =   "Documento"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Object.Tag             =   "T1300"
            Text            =   "Rut"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Object.Tag             =   "T2000"
            Text            =   "Cliente"
            Object.Width           =   6526
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Object.Tag             =   "T1000"
            Text            =   "Emision"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   6
            Key             =   "total"
            Object.Tag             =   "N100"
            Text            =   "Total"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   7
            Object.Tag             =   "T1000"
            Text            =   "Doc. Final"
            Object.Width           =   3175
         EndProperty
         BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   8
            Object.Tag             =   "N109"
            Text            =   "id doc final"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   9
            Object.Tag             =   "T1000"
            Text            =   "Vendedor"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   10
            Text            =   "DOC_ID_NOTA_vETNA"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(12) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   11
            Object.Tag             =   "N109"
            Text            =   "Dscto 1"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(13) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   12
            Object.Tag             =   "N109"
            Text            =   "Dscto 2 (ajuste)"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(14) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   13
            Object.Tag             =   "N109"
            Text            =   "Recargo"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(15) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   14
            Object.Tag             =   "N109"
            Text            =   "CODIGO SII"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(16) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   15
            Object.Tag             =   "T1000"
            Text            =   "O.COMPRA"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(17) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   16
            Object.Tag             =   "N109"
            Text            =   "Suc_ID"
            Object.Width           =   2540
         EndProperty
      End
   End
   Begin VB.Timer Timer1 
      Interval        =   20
      Left            =   90
      Top             =   6945
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   120
      OleObjectBlob   =   "VenPosCaja6.frx":115EA
      Top             =   6255
   End
   Begin MSComctlLib.ListView LVFpago 
      Height          =   3450
      Left            =   16650
      TabIndex        =   56
      Top             =   465
      Visible         =   0   'False
      Width           =   5325
      _ExtentX        =   9393
      _ExtentY        =   6085
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483646
      BackColor       =   -2147483643
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   3
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Object.Tag             =   "N109"
         Text            =   "Id Interno"
         Object.Width           =   2646
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Object.Tag             =   "N109"
         Text            =   "valor"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Object.Tag             =   "N109"
         Text            =   "Nfpago IF"
         Object.Width           =   13229
      EndProperty
   End
   Begin MSComctlLib.ListView LvPagos 
      Height          =   2970
      Left            =   16725
      TabIndex        =   61
      Top             =   4455
      Visible         =   0   'False
      Width           =   7455
      _ExtentX        =   13150
      _ExtentY        =   5239
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   16777152
      BorderStyle     =   1
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   7
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Object.Tag             =   "N109"
         Text            =   "Id"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Object.Tag             =   "T3000"
         Text            =   "Forma de pago"
         Object.Width           =   4410
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   2
         Key             =   "pago"
         Object.Tag             =   "N109"
         Text            =   "Id Nro Fiscal"
         Object.Width           =   1940
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Object.Tag             =   "T1000"
         Text            =   "4"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Text            =   "5"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   5
         Text            =   "6"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   6
         Object.Tag             =   "N"
         Text            =   "ID Nro Fiscal"
         Object.Width           =   38100
      EndProperty
   End
   Begin MSComctlLib.ListView LvVentas 
      Height          =   1260
      Left            =   12000
      TabIndex        =   64
      Top             =   8550
      Width           =   8205
      _ExtentX        =   14473
      _ExtentY        =   2223
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   12648384
      BorderStyle     =   1
      Appearance      =   0
      NumItems        =   9
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Object.Tag             =   "N100"
         Text            =   "id doc"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Object.Tag             =   "T1000"
         Text            =   "RUT"
         Object.Width           =   2117
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Object.Tag             =   "N109"
         Text            =   "Numero"
         Object.Width           =   1764
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Object.Tag             =   "T1500"
         Text            =   "Banco"
         Object.Width           =   2646
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Object.Tag             =   "T1000"
         Text            =   "Plaza"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   5
         Object.Tag             =   "F1000"
         Text            =   "Fecha"
         Object.Width           =   2196
      EndProperty
      BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   6
         Key             =   "total"
         Object.Tag             =   "N100"
         Text            =   "Monto"
         Object.Width           =   2117
      EndProperty
      BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   7
         Object.Tag             =   "T1000"
         Text            =   "Observacion"
         Object.Width           =   3528
      EndProperty
      BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   8
         Object.Tag             =   "N109"
         Text            =   "banco_id"
         Object.Width           =   2540
      EndProperty
   End
End
Attribute VB_Name = "VenPosCaja"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim Lp_Nro_Documento As Long
Dim Lm_Alto As Long
Dim Lp_Id_Nueva_Venta As Long
Dim Lp_Id_Unico_Venta As Long
Dim Sm_Fpago As String
Dim Id_Ventas_Unico As Long
Dim Im_Dias As Integer 'dias de plazo, contado td tc = 0, cheq = 1, credito,=30
Dim Sql_Caja As String
Dim Sm_Flag As String
'Private Declare Function ShellExecute Lib "shell32.dll" Alias "ShellExecuteA" (ByVal hWnd As Long, ByVal lpOperation As String, ByVal lpFile As String, ByVal lpParameters As String, ByVal lpDirectory As String, ByVal nShowCmd As Long) As Long
Private Declare Function ShellExecute Lib "shell32.dll" Alias "ShellExecuteA" (ByVal hWnd As Long, ByVal lpOperation As String, ByVal lpFile As String, ByVal lpParameters As String, ByVal lpDirectory As String, ByVal nShowCmd As Long) As Long
 


Private Sub LimpiaTodito()
            LvCuotas.ListItems.Clear
            LvDetalle.SelectedItem = Nothing
            FrmPago.Caption = "PAGANDO NV [ ] "
            TxtAPagar = 0
            TxtSaldoPago = 0
            TxtSumaPagos = 0
            FrmPago.Tag = 0
            TxtEfectivo = 0
            TxtDeposito = 0
            TxtTC = 0
            TxtTD = 0
            TxtCh = 0
            TxtCredito = 0
            TxtRutCliente = ""
            TxtNombreCliente = ""
            LvProductos.ListItems.Clear
            CmdBuscar_Click
            TxtScanner = ""
            TxtScanner.SetFocus
            LimpiaCheques
            Me.Height = 8865
            TxtEfectivoReal = 0
End Sub

Private Sub Cmd100p_Click()
    TxtEfectivo = CDbl(TxtEfectivo) + Val(Cmd100p.Tag)
    SumaPagos
End Sub

Private Sub Cmd10p_Click()
    TxtEfectivo = CDbl(TxtEfectivo) + Val(Cmd10p.Tag)
    SumaPagos
End Sub

Private Sub Cmd500p_Click()
    TxtEfectivo = CDbl(TxtEfectivo) + Val(Cmd500p.Tag)
    SumaPagos
End Sub

Private Sub Cmd50p_Click()
    TxtEfectivo = CDbl(TxtEfectivo) + Val(Cmd50p.Tag)
    SumaPagos
End Sub

Private Sub CmdAnular_Click()
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    
    sis_InputBox.FramBox = "Ingrese llave para anulacion"
    sis_InputBox.Show 1
    Sp_Llave = UCase(SG_codigo2)
    If Len(Sp_Llave) = 0 Then Exit Sub
    Sql = "SELECT usu_id,usu_nombre,usu_login " & _
          "FROM sis_usuarios " & _
          "WHERE usu_pwd = MD5('" & Sp_Llave & "')"
    Consulta RsTmp3, Sql
    If RsTmp3.RecordCount > 0 Then
        X = InputBox("Usuario:" & RsTmp3!usu_nombre & vbNewLine & "Ingrese motivo" & vbNewLine & "Nota Venta NRo:" & LvDetalle.SelectedItem, "Motivo de eliminaci�n")
        If Len(X) = 0 Then
            MsgBox "No ingreso motivo, no fue eliminado el documento"
            Exit Sub
        End If
        
      '  With LvDetalle.SelectedItem
            
            Sql = "UPDATE ven_nota_venta SET nve_anulada='SI', nve_terminada='SI', nve_anulada_usuario='" & RsTmp3!usu_nombre & "' " & _
                    "WHERE nve_id=" & LvDetalle.SelectedItem
        
            cn.Execute Sql
             'Sql = "INSERT INTO sis_eliminacion_documentos (id,eli_fecha,eli_motivo,usu_login,eli_ven_com) VALUES (" & _
                   LvDetalle.SelectedItem & ",'" & Fql(Date) & "','" & UCase(X) & "','" & RsTmp3!usu_login & "','VEN')"
            
            'Sql = "INSERT INTO com_historial_eliminaciones (doc_id,no_documento,rut_proveedor,eli_fecha,eli_motivo,usu_id) VALUES (" & _
                        .SubItems(7) & "," & .SubItems(4) & ",'" & .SubItems(8) & "','" & Format(Now, "YYYY-DD-MM HH:MM:SS") & "','" & X & "'," & RsTmp3!usu_id & ")"
            
      '  End With
         CargaNotas
    Else
        MsgBox "Contrase�a ingresada no fue encontrada...", vbInformation
        Exit Sub
    End If
    
    
    
    
    
End Sub

Private Sub CmdBillete_Click(Index As Integer)
    TxtEfectivo = CDbl(TxtEfectivo) + Val(CmdBillete(Index).Tag)
    SumaPagos
End Sub



Private Sub CmdBuscar_Click()
    FrmLoad.Visible = True
    DoEvents
    LvDetalle.SelectedItem = Nothing
    FrmPago.Caption = "PAGANDO NV [ ] "
    TxtAPagar = 0
    TxtSaldoPago = 0
    TxtSumaPagos = 0
    FrmPago.Tag = 0
    TxtEfectivo = 0
    TxtDeposito = 0
    TxtTC = 0
    TxtTD = 0
    TxtCh = 0
    TxtCredito = 0
    TxtRutCliente = ""
    TxtNombreCliente = ""
    LvProductos.ListItems.Clear
    
    TxtScanner = ""
    TxtScanner.SetFocus
    LimpiaCheques
    Me.Height = 8865
    
    CargaNotas
    FrmLoad.Visible = False
End Sub

Private Sub CmdF5_Click()
    Dim Lp_Folio As Long
    Dim Fp_FechaVencimiento As Date
    Dim Sp_FormaPago As String
    Dim Sp_Condicion As String
    Dim Ip_PlazoID As Integer

    Dim Id_Documento As Long
    Dim Sp_NombreDocumento As String
    
    Ip_PlazoID = 0
    Sp_Condicion = "CONTADO"
    Sp_FormaPago = "EFECTIVO"
    If Not CmdF5.Enabled Then Exit Sub
    TxtEfectivoReal = 0
    If Val(FrmPago.Tag) = 0 And Frame1.Visible Then Exit Sub
    If CDbl(TxtSumaPagos) < CDbl(TxtAPagar) Then
        MsgBox "Suma de pagos no alcanza...", vbInformation
        TxtEfectivo.SetFocus
        Exit Sub
    End If
    
    If Val(TxtCh) > 0 Then
        If CDbl(TxtTotalCheques) <> CDbl(TxtCh) Then
            MsgBox "Suma de cheques no cuadra...", vbInformation
            LvCheques.SetFocus
            Exit Sub
        End If
    End If
    
    If CDbl(TxtCredito) > 0 Then
    '   VAlidamos que el cupo disponible alcance
        ClienteTieneCredito
        If CDbl(TxtCredito) > CDbl(TxtRutCliente.Tag) Then
            MsgBox "Cupo credito no es suficiente...", vbInformation
            TxtCredito.SetFocus
            Exit Sub
        End If
        
        If SP_Rut_Activo = "76.354.346-3" Then
            VenPosCaja.LvCuotas.ListItems.Clear
            VenPosCajaCuotas.LvDetalle.ListItems.Clear
            VenPosCajaCuotas.TxtNombreCliente = TxtNombreCliente
            VenPosCajaCuotas.TxtRutCliente = TxtRutCliente
            VenPosCajaCuotas.TxtMonto = TxtCredito
            VenPosCajaCuotas.TxtDocumento = LvDetalle.SelectedItem.SubItems(2)
            VenPosCajaCuotas.Show 1
            
            If VenPosCaja.LvCuotas.ListItems.Count = 0 Then Exit Sub
        End If
        
        
    End If
    
    If Im_Nv > 0 Then
        'Cuando se trabaja con venta rapida, quiere decir que del pos se efectura el pago inmediatamente _
        'desde caja se envian los datos al pos para finalizar la venta
        
        'V E N T A       R A P I D A
        CmdF5.Enabled = False
        EnviarDatosDePagoAlPos
    Else
        CmdF5.Enabled = False
        'Trabajamos con POS, separado de CAJA, aqui pagamos las NV emitidas por los POS
        'Pagando nota de venta
        
        If Val(FrmPago.Tag) = 0 Then Exit Sub
        Me.LVFpago.ListItems.Clear
        
'        If LvDetalle.SelectedItem.SubItems(7) = "BOLETA FISCAL" Then
'            If SG_ImpresoraFiscalBixolon = "SI" Then
'                If SG_Marca_Impresora_Fiscal = "SAMSUNG" Then
'                        If vtaBoletaFiscalSamsumg.EstadoSamgumn() = 2 Then
'                            MsgBox "Debe hacer cierre de caja ..." & vbNewLine & "( Emitir Z ) ", vbInformation
'                            Exit Sub
'                        End If
'                End If
'            End If
'        End If
'
        
        If Val(TxtEfectivo) > 0 Then
            Sm_Fpago = "EFECTIVO"
            Im_Dias = 0
            TxtEfectivoReal = CDbl(TxtEfectivo)
            TxtEfectivo = CDbl(TxtAPagar) - CDbl(TxtTC) - CDbl(TxtTD) - CDbl(TxtCh) - CDbl(TxtCredito)
        
            Me.LVFpago.ListItems.Add , , TxtEfectivo.Tag
            Me.LVFpago.ListItems(Me.LVFpago.ListItems.Count).SubItems(1) = Val(Me.TxtEfectivo)
            For i = 1 To LvPagos.ListItems.Count
                If LvPagos.ListItems(i) = TxtEfectivo.Tag Then
                    Me.LVFpago.ListItems(Me.LVFpago.ListItems.Count).SubItems(2) = LvPagos.ListItems(i).SubItems(2)
                     Exit For
                End If
            Next
            
            
            
        End If
    
        If Val(TxtTC) > 0 Then
            Im_Dias = 0
            Sm_Fpago = "TARJETA CREDITO"
            Me.LVFpago.ListItems.Add , , TxtTC.Tag
            Me.LVFpago.ListItems(Me.LVFpago.ListItems.Count).SubItems(1) = CDbl(TxtTC)
            For i = 1 To LvPagos.ListItems.Count
                If LvPagos.ListItems(i) = TxtTC.Tag Then
                    Me.LVFpago.ListItems(Me.LVFpago.ListItems.Count).SubItems(2) = LvPagos.ListItems(i).SubItems(2)
                     Exit For
                End If
            Next
        End If
        
         If Val(TxtTD) > 0 Then
            Im_Dias = 0
            Sm_Fpago = "TARJETA DEBITO"
            Me.LVFpago.ListItems.Add , , TxtTD.Tag
            Me.LVFpago.ListItems(Me.LVFpago.ListItems.Count).SubItems(1) = CDbl(TxtTD)
            For i = 1 To LvPagos.ListItems.Count
                If LvPagos.ListItems(i) = TxtTD.Tag Then
                    Me.LVFpago.ListItems(Me.LVFpago.ListItems.Count).SubItems(2) = LvPagos.ListItems(i).SubItems(2)
                     Exit For
                End If
            Next
        End If
    
        If Val(TxtCh) > 0 Then
            Im_Dias = 1
            
            
            Ip_PlazoID = 1
            
            Sm_Fpago = "CHEQUE"
            Me.LVFpago.ListItems.Add , , TxtCh.Tag
            Me.LVFpago.ListItems(Me.LVFpago.ListItems.Count).SubItems(1) = CDbl(TxtCh)
            For i = 1 To LvPagos.ListItems.Count
                If LvPagos.ListItems(i) = TxtCh.Tag Then
                    Me.LVFpago.ListItems(Me.LVFpago.ListItems.Count).SubItems(2) = LvPagos.ListItems(i).SubItems(2)
                     Exit For
                End If
            Next
        End If
        
        If Val(TxtDeposito) > 0 Then
            Im_Dias = 0
            Sm_Fpago = "DEPOSITO"
            Me.LVFpago.ListItems.Add , , TxtDeposito.Tag
            Me.LVFpago.ListItems(Me.LVFpago.ListItems.Count).SubItems(1) = CDbl(TxtDeposito)
            For i = 1 To LvPagos.ListItems.Count
                If LvPagos.ListItems(i) = TxtDeposito.Tag Then
                    Me.LVFpago.ListItems(Me.LVFpago.ListItems.Count).SubItems(2) = LvPagos.ListItems(i).SubItems(2)
                     Exit For
                End If
            Next
        End If
        
        Fp_FechaVencimiento = Date
        
        If Val(TxtCredito) > 0 Then
            Ip_PlazoID = Right(CboPlazos.Text, 6)
            Im_Dias = CboPlazos.ItemData(CboPlazos.ListIndex)
            Sm_Fpago = Mid(CboPlazos.Text, 1, 30) '"PENDIENTE"
            Sp_Condicion = "CREDITO"
            
            If SP_Rut_Activo <> "76.354.346-3" Then
                Me.LVFpago.ListItems.Add , , 0
            Else
                Me.LVFpago.ListItems.Add , , TxtCredito.Tag
            End If
            Me.LVFpago.ListItems(Me.LVFpago.ListItems.Count).SubItems(1) = CDbl(TxtCredito)
            Fp_FechaVencimiento = Date + CboPlazos.ItemData(CboPlazos.ListIndex)
            If SP_Rut_Activo = "76.354.346-3" And LvDetalle.SelectedItem.SubItems(7) = "BOLETA FISCAL" Then
                    
                    For i = 1 To LvPagos.ListItems.Count
                        If LvPagos.ListItems(i) = TxtCredito.Tag Then
                            Me.LVFpago.ListItems(Me.LVFpago.ListItems.Count).SubItems(2) = LvPagos.ListItems(i).SubItems(2)
                             Exit For
                        End If
                    Next
            End If
            
            
        End If
        
        FrmLoad.Visible = True
        DoEvents
    
        Id_Ventas_Unico = UltimoNro("ven_doc_venta", "id")
    
        Id_Documento = LvDetalle.SelectedItem.SubItems(8) 'Documento ID de venta orginal, esto cambia cuando _
        la empresa usa boletas manuales, y queda en id 40
        Sp_NombreDocumento = LvDetalle.SelectedItem.SubItems(7) 'idem
    
        'Coprobamos si la venta es completamente a credito.
        'Si es asi, nos saltamos el abono del documento.
        For i = 1 To Me.LVFpago.ListItems.Count
                If Val(Me.LVFpago.ListItems(i)) = 0 And CDbl(Me.LVFpago.ListItems(i).SubItems(1)) = CDbl(TxtAPagar) Then GoTo final
        Next
        '_____/ 10-10-15 /
        
        Dim Lp_IdAbo As Long
        Dim Lp_Nro_Comprobante As Long
       
      '  Lp_Id_Nueva_Venta
        TxtTotal = CDbl(TxtAPagar)
        TxtRut = LvDetalle.SelectedItem.SubItems(3)
        Lp_Id_Unico_Venta = LvDetalle.SelectedItem
        Lp_IdAbo = UltimoNro("cta_abonos", "abo_id")
        Lp_Nro_Comprobante = AutoIncremento("lee", 100, , IG_id_Sucursal_Empresa)
        If Len(TxtRut) = 0 Then TxtRut = "11.111.111-1"
         
        
         
         'Aqui registra el pago con su formas
        Sql = "INSERT INTO cta_abonos (abo_id,abo_cli_pro,abo_rut,abo_fecha,abo_fecha_pago,abo_monto,abo_observacion,usu_nombre,suc_id,abo_obs_extra,rut_emp,abo_nro_comprobante,caj_id,abo_origen) " & _
               "VALUES(" & Lp_IdAbo & ",'CLI','" & TxtRut & "','" & _
               Format(Date, "YYYY-MM-DD") & "','" & Format(Date, "YYYY-MM-DD") & "'," & CDbl(TxtTotal) & _
               ",'PAGO POS CAJA','" & LogUsuario & "'," & 0 & ",'VENTA RAPIDA','" & SP_Rut_Activo & "'," & Lp_Nro_Comprobante & "," & LG_id_Caja & ",'VENTA')"
               
        cn.Execute Sql
             
        Sql = "INSERT INTO cta_abono_documentos (abo_id,id,ctd_monto,rut_emp) VALUES "
        ' For i = 1 To LVDetalle.ListItems.Count
             Sql = Sql & "(" & Lp_IdAbo & "," & Id_Ventas_Unico & "," & CDbl(TxtTotal) & ",'" & SP_Rut_Activo & "')"
        ' Next
         
         cn.Execute Sql
         
         AutoIncremento "GUARDA", 100, Lp_Nro_Comprobante, IG_id_Sucursal_Empresa
                     
                 
         If Me.LvCheques.ListItems.Count > 0 Then
             'CHEQUE, AQUI GRABAMOS EL DETALLE DE LOS CHEQUES
             
                 Sql = "INSERT INTO abo_cheques (abo_id,ban_id,che_plaza,che_numero,che_monto,che_fecha,che_estado,che_autorizacion,rut_emp,che_ban_nombre) " & _
                          "VALUES "
                 With Me.LvCheques
                     For i = 1 To .ListItems.Count
                         Sql = Sql & "(" & Lp_IdAbo & "," & .ListItems(i).SubItems(8) & ",'" & .ListItems(i).SubItems(4) & _
                         "'," & .ListItems(i).SubItems(2) & "," & CDbl(.ListItems(i).SubItems(6)) & ",'" & _
                         Format(.ListItems(i).SubItems(5), "YYYY-MM-DD") & "','CARTERA','','" & SP_Rut_Activo & "','" & .ListItems(i).SubItems(3) & "'),"
                     Next
                 End With
                 Sql = Mid(Sql, 1, Len(Sql) - 1)
                 cn.Execute Sql
            
             
            
         End If
         'Grabaremos la(s) formas de pago en que se pago o pagaron los documentos
         '26 Marzo 2012
         Sql = "INSERT INTO abo_tipos_de_pagos (abo_id,mpa_id,pad_valor,caj_id) VALUES"
         
         For i = 1 To Me.LVFpago.ListItems.Count
             If CDbl(Me.LVFpago.ListItems(i).SubItems(1)) > 0 Then
                 Sql = Sql & "(" & Lp_IdAbo & "," & Me.LVFpago.ListItems(i) & "," & CDbl(Me.LVFpago.ListItems(i).SubItems(1)) & "," & LG_id_Caja & "),"
             End If
         Next
         Sql = Mid(Sql, 1, Len(Sql) - 1)
         cn.Execute Sql
         
         
         'Cambiar NV a terminada, y convertirla al documento final
        ' cn.Execute "UPDATE ven_doc_venta SET  tipo_doc='" & LvDetalle.SelectedItem.SubItems(7) & "', doc_id=" & LvDetalle.SelectedItem.SubItems(8) & ",ven_estado_nota_venta='PROCESO' " & _
                    "WHERE no_documento=" & LvDetalle.SelectedItem.SubItems(1) & " AND doc_id=" & LvDetalle.SelectedItem.SubItems(10) & " AND rut_emp='" & SP_Rut_Activo & "'"
                    
        ' cn.Execute "UPDATE ven_detalle SET doc_id=" & LvDetalle.SelectedItem.SubItems(8) & " " & _
                    "WHERE no_documento=" & LvDetalle.SelectedItem.SubItems(1) & " AND doc_id=" & LvDetalle.SelectedItem.SubItems(10) & " AND rut_emp='" & SP_Rut_Activo & "'"
                    
                    
                    
                    
final:
                                '22 Agosto 2015
          
        
        
            If LvDetalle.SelectedItem.SubItems(7) = "BOLETA FISCAL" Then
                '
                ' BOLETA FISCAL
                ' EPSON O SAMGUNG
                
                If SG_ImpresoraFiscalBixolon = "SI" Then
                    If SG_Marca_Impresora_Fiscal = "EPSON" Then
                        'Emite Boleta EPSON
                        
                        EmiteBoletaEpson
                        'CODIGO NO TERMINADO, NO TENEMOS EMPRESAS CON IF EPSON CON POS Y CAJA SEPARADA
                        '_____________________________________________________________________________/ 10-10-2015 /
                    
                    ElseIf SG_Marca_Impresora_Fiscal = "SAMSUNG" Then
                        '29 Agosto o2015
                        'Impresion de boleta en bixolon o smagum
                        EmiteBoletaSamsung
                        Lp_Nro_Documento = Lp_Nro_Boleta_Obtenida_IF
                        If ConvertirADocVenta(Lp_Nro_Boleta_Obtenida_IF, Fp_FechaVencimiento, Im_Dias, Sm_Fpago, Sp_Condicion, Trim(Str(Ip_PlazoID))) Then
                                'Aqui colocamos el numero de boleta obtenido desde la impresora fiscal
                                
                                cn.Execute "UPDATE ven_nota_venta SET nve_terminada='SI',id=" & Id_Ventas_Unico & " " & _
                                            "WHERE nve_id=" & LvDetalle.SelectedItem
                        End If
                        CmdF5.Enabled = True
                        FrmLoad.Visible = False
                        MsgBox "Documento Emitido..." & vbNewLine & "Nro:" & Lp_Nro_Documento, vbInformation
                        
                    
                    End If
                End If
                
            ElseIf LvDetalle.SelectedItem.SubItems(7) = "FACTURA ELECTRONICA" Or LvDetalle.SelectedItem.SubItems(7) = "GUIA DESPACHO ELECTRONICA" Then
                'F A C T U R A   O    G U I A      E L E C T R O N I C A
                
                FrmDte.Visible = True
            '    Sql = "SELECT IFNULL(MAX(no_documento),0) +1 numero " & _
                        "FROM " & _
                        "WHERE doc_id=" & LvDetalle.SelectedItem.SubItems(8) & " AND rut_emp='" & SP_Rut_Activo & "'"
                DoEvents
                Sql = "SELECT dte_id,dte_folio numero " & _
                        "FROM dte_folios " & _
                        "WHERE doc_id=" & LvDetalle.SelectedItem.SubItems(14) & " AND dte_venta_compra='VENTA' AND dte_disponible='SI' " & _
                        "ORDER BY dte_folio " & _
                        "LIMIT 1"
                        
                Consulta RsTmp, Sql
                
                If RsTmp.RecordCount = 0 Then
                    MsgBox "No quedan folios autorizados"
                    LimpiaTodito
                    Exit Sub
                End If
                
                
                dte_id = RsTmp!dte_id
                Lp_Folio = RsTmp!Numero
                
             '   Lp_Folio = InputBox("Nro Fac", "Digite nro fac", Lp_Folio)
             '   LG_id_Caja = InputBox("Caja nro:", "Caja:", LG_id_Caja)
                
                Lp_Nro_Documento = Lp_Folio
                'Convierte NV Documento de venta
                If ConvertirADocVenta(Lp_Folio, Fp_FechaVencimiento, Im_Dias, Sm_Fpago, Sp_Condicion, Trim(Str(Ip_PlazoID))) Then
                       
                       
                       'Totales
                       Dp_Total = CDbl(TxtAPagar)
                       Dp_Neto = Round(Dp_Total / Val("1." & DG_IVA), 0)
                       Dp_Iva = Dp_Total - Dp_Neto
                       
                       
                       
                       
                    '   CrearDTE LvDetalle.SelectedItem.SubItems(14), Lp_Folio, LvDetalle.SelectedItem.SubItems(3), Date, Val(Dp_Neto), 0, Val(Dp_Iva), Val(Dp_Total), _
                       Round((Val(LvDetalle.SelectedItem.SubItems(11) + LvDetalle.SelectedItem.SubItems(12)) / Val("1." & DG_IVA)), 0), LvDetalle.SelectedItem.SubItems(13), Id_Ventas_Unico, 0, 0, "", 0
                       
                       FacturaElectronicaPOS LvDetalle.SelectedItem.SubItems(14), Lp_Folio, LvDetalle.SelectedItem.SubItems(8), Sm_Fpago
                
                        
                        cn.Execute "UPDATE dte_folios SET dte_disponible='NO' " & _
                                    "WHERE dte_id=" & dte_id
                                    
                                
                        cn.Execute "UPDATE ven_nota_venta SET nve_terminada='SI',id=" & Id_Ventas_Unico & " " & _
                                    "WHERE nve_id=" & LvDetalle.SelectedItem
                                    
                                    
                        If Sm_Fpago = "TARJETA CREDITO" Or Sm_Fpago = "TARJETA DEBITO" Then
                            LP_FOLIOX = AutoIncremento("LEE", 40, , IG_id_Sucursal_Empresa)
                            AutoIncremento "GUARDA", 40, LP_FOLIOX, IG_id_Sucursal_Empresa
                        End If

            
                End If
                FrmDte.Visible = False
                'con acuse de recibo
                
                If SG_UtilizaImpresoraTermicaDTE = "SI" Then
                        'Solo si la empresa utiliza impresora termica para la emision de facturas electronicas / 10-10-2015
                        If Principal.LvImpresorasDefecto.ListItems.Count > 0 Then
                            If UCase(Printer.DeviceName) = UCase(Principal.LvImpresorasDefecto.ListItems(1).SubItems(2)) Then
                                    'SIGUIE CON LA MISMA
                            Else
                                    EstableImpresora Principal.LvImpresorasDefecto.ListItems(1).SubItems(2)
                            End If
                        Else
                            MsgBox "No se establecio impresora, imprimira en la por defecto..."
                        End If
                        'Copia empresa
                        ImprimeFacturaTermica Str(Lp_Folio), LvDetalle.SelectedItem.SubItems(8), False, True, LvDetalle.SelectedItem.SubItems(7)
                        If Val(TxtCredito) = 0 Then
                            'cedible si no corresponde a credito
                            ImprimeFacturaTermica Str(Lp_Folio), LvDetalle.SelectedItem.SubItems(8), True, False, LvDetalle.SelectedItem.SubItems(7)
                        End If
                        'copia cliente
                        ImprimeFacturaTermica Str(Lp_Folio), LvDetalle.SelectedItem.SubItems(8), False, False, LvDetalle.SelectedItem.SubItems(7)
                Else
                    If SP_Rut_Activo = "76.553.302-3" Then
                        'Imprimir directo el pdf KYR
                      '  If UCase(Printer.DeviceName) = UCase(Principal.LvImpresorasDefecto.ListItems(1).SubItems(2)) Then
                                'SIGUIE CON LA MISMA
                      '  Else
                      '          EstableImpresora Principal.LvImpresorasDefecto.ListItems(1).SubItems(2)
                      '  End If
                      '  DoEvents
                      ' ShellExecute hWnd, "print", SG_NombrePDF, vbNullString, vbNullString, 1
                        
                    End If
                
                    
                End If
                
                
                
                SkFoliosDisponibles = ""
        
                Sql = "SELECT count(dte_id) dte " & _
                        "FROM dte_folios " & _
                        "WHERE rut_emp='" & SP_Rut_Activo & "' AND doc_id=" & LvDetalle.SelectedItem.SubItems(14) & " AND dte_disponible='SI' " & _
                        "GROUP BY doc_id"
                Consulta RsTmp3, Sql
                If RsTmp3.RecordCount > 0 Then
                    SkFoliosDisponibles = "Folios disponibles:" & RsTmp3!Dte
                End If
                                
                         
                FrmLoad.Visible = False
                MsgBox "Documento emitido..." & vbNewLine & SkFoliosDisponibles, vbInformation
                
            ElseIf LvDetalle.SelectedItem.SubItems(7) = "FACTURA MANUAL" Then
                If Principal.LvImpresorasDefecto.ListItems.Count = 0 Then
                    MsgBox "No ha configurado impresoras para este equipo...", vbExclamation
                Else
                    'Fuerza a dejar impresora predeterminada
                    La_Establecer_Impresora Principal.LvImpresorasDefecto.ListItems(1).SubItems(2)
                End If
            
                'F A C T U R A       M A N U A L
                EmitirFacturaManual
                'Revisar codigo, NO HAY EMPRESAS UTILIZANDO ESTA MODALIDAD
                '__________________________________________________________/ 10-10-15 /
                ObtenerProxNumeros
                
            ElseIf LvDetalle.SelectedItem.SubItems(7) = "BOLETA" Then
                'B O L E T A     M A N U A L
                       
                        Id_Documento = LvDetalle.SelectedItem.SubItems(8)
                
                        
                        DoEvents
                        'Para KYR Y ALCALDE debemos detectar si paga con tarjeta , _
                        Para que no se emita la boleta
                        If SP_Rut_Activo = "76.169.962-8" Or SP_Rut_Activo = "76.553.302-3" Then
                            If Sm_Fpago <> "TARJETA CREDITO" And Sm_Fpago <> "TARJETA DEBITO" Then
                            
                            Else
                                Sp_NombreDocumento = "VOUCHER TRANSBANK"
                                Id_Documento = 40
                            End If
                            Lp_Folio = AutoIncremento("LEE", Val(Id_Documento), , IG_id_Sucursal_Empresa)
                            Lp_Nro_Documento = Lp_Folio
                    
                    
                            'Consultar si el folio ya ha sido utilizado,
                            '19 2 2016
                            'Esto para solucionar problema de boletas repetidas ya que el autoincremento no lo hace correctamente
                            Sql = "SELECT id " & _
                                    "FROM ven_doc_venta " & _
                                    "WHERE no_documento=" & Lp_Nro_Documento & " AND doc_id=" & Id_Documento & " AND rut_emp='" & SP_Rut_Activo & "'"
                            Consulta RsTmp, Sql
                            If RsTmp.RecordCount > 0 Then
                                'El Numero esta ocupado por lo tanto se debe asiganar el ultimo +1
                                
                                
                                        Sql = "SELECT IFNULL(MAX(no_documento),0) +1 numero " & _
                                                    "FROM ven_doc_venta " & _
                                                    "WHERE doc_id=" & Id_Documento & " AND rut_emp='" & SP_Rut_Activo & "'"
                                        Consulta RsTmp, Sql
                                        If RsTmp.RecordCount > 0 Then
                                            Lp_Folio = RsTmp!Numero
                                            Lp_Nro_Documento = Lp_Folio
                                        
                                        Else
                                            Lp_Folio = 1
                                            Lp_Nro_Documento = 1

                                        End If
                                        

                                        
                                        
                                        
                            End If
                        End If
                        
                        If SP_Rut_Activo = "76.169.962-8" Or SP_Rut_Activo = "76.553.302-3" Or SP_Rut_Activo = "76.354.346-3" Or SP_Rut_Activo = "76.239.518-5" Then
                            If Sm_Fpago <> "TARJETA CREDITO" And Sm_Fpago <> "TARJETA DEBITO" Then
                                 Lp_Folio = TxtNextBoleta
                                 Lp_Nro_Documento = TxtNextBoleta
                            End If
                            If Sm_Fpago = "TARJETA CREDITO" Or Sm_Fpago = "TARJETA DEBITO" Then
                                Lp_Folio = TxtNextVoucher
                                Lp_Nro_Documento = TxtNextVoucher
                            
                            End If
                        
                        End If
        
                        
                        If ConvertirADocVenta(Lp_Folio, Fp_FechaVencimiento, Im_Dias, Sm_Fpago, Sp_Condicion, Trim(Str(Ip_PlazoID))) Then
                            'Aqui colocamos el numero de boleta obtenido desde la impresora fiscal
                            AutoIncremento "GUARDA", Val(Id_Documento), Lp_Folio, IG_id_Sucursal_Empresa
                            
                            cn.Execute "UPDATE ven_nota_venta SET nve_terminada='SI',id=" & Id_Ventas_Unico & " " & _
                                        "WHERE nve_id=" & LvDetalle.SelectedItem
                        
                        End If
                        
             
                
                
                
                        
                
                
                
                        
                        If Principal.LvImpresorasDefecto.ListItems.Count = 0 Then
                            MsgBox "No hay impresoras configuradas..."
                        Else
                            
                            EstableImpresora Principal.LvImpresorasDefecto.ListItems(1).SubItems(3)
                            If UCase(Printer.DeviceName) = UCase(Principal.LvImpresorasDefecto.ListItems(1).SubItems(3)) Then
                                La_Establecer_Impresora Principal.LvImpresorasDefecto.ListItems(1).SubItems(3)
                                
                            'SIGUIE CON LA MISMA
                            Else
                                La_Establecer_Impresora Principal.LvImpresorasDefecto.ListItems(1).SubItems(3)
                                
                                'Forzaremos a volver a establer la impresora que corresonde, en caso que en el primer intento no lo haga. 1 vez
                                If UCase(Printer.DeviceName) <> UCase(Principal.LvImpresorasDefecto.ListItems(1).SubItems(3)) Then
                                    La_Establecer_Impresora Principal.LvImpresorasDefecto.ListItems(1).SubItems(3)
                                End If
                                'Forzaremos a volver a establer la impresora que corresonde, en caso que en el primer intento no lo haga. 2 vez
                                If UCase(Printer.DeviceName) <> UCase(Principal.LvImpresorasDefecto.ListItems(1).SubItems(3)) Then
                                    La_Establecer_Impresora Principal.LvImpresorasDefecto.ListItems(1).SubItems(3)
                                End If
                                
                                
                                
                                
                            End If
                        End If
                        If Sm_Fpago <> "TARJETA CREDITO" And Sm_Fpago <> "TARJETA DEBITO" Then
                        Select Case SP_Rut_Activo
                            Case "76.169.962-8"
                                ImprimeBoletaKyR
                            Case "76.553.302-3"
                                ImprimeBoletaKyR
                        End Select
                        End If
                        
                
                      '  ImprimeBoletaKyR
                       
                       CmdF5.Enabled = True
                       FrmLoad.Visible = False
                       MsgBox "Documento Emitido...", vbInformation
                        ObtenerProxNumeros
                
                
                
                
                
                'Revisar codigo, SOLO KYR ESTARIA USANDO ESTA MODALIDAD
                '_________________________________________________________
                
                
            End If
                    
                    
              'inventarios _
            21/12/2015
            Me.ActualizaStock Lp_Nro_Documento, Val(Id_Documento), Sp_NombreDocumento
                    
                    
            'Finaliza la venta
            LimpiaTodito
    
    End If
    CmdF5.Enabled = True
    FrmLoad.Visible = False
    
End Sub
Private Sub CmdModifica_Click()
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    venPosModificaNV.TxtDocActual = LvDetalle.SelectedItem.SubItems(7)
    venPosModificaNV.FrmNV.Caption = "NOTA VENTA NRO:" & LvDetalle.SelectedItem.SubItems(1) & " "
    venPosModificaNV.FrmNV.Tag = Me.LvDetalle.SelectedItem
    venPosModificaNV.Show 1
    LimpiaTodito
    CargaNotas
End Sub

Private Sub CmdOkCheque_Click()
    Dim u As Integer
    If Len(TxtRutCheque) = 0 Then
        MsgBox "Falta RUT...", vbInformation
        TxtRutCheque.SetFocus
        Exit Sub
    End If
    
    If Val(TxtNroCheque) = 0 Then
        MsgBox "Falta nro de cheque...", vbInformation
        TxtNroCheque.SetFocus
        Exit Sub
    End If

    If CboBanco.ListIndex = -1 Then
        MsgBox "Seleccione banco...", vbInformation
        CboBanco.SetFocus
        Exit Sub
    End If
    
    If Len(TxtPlaza) = 0 Then
        MsgBox "Seleccione plaza...", vbInformation
        TxtPlaza.SetFocus
        Exit Sub
    End If
    If Val(TxtMontoCheque) = 0 Then
        MsgBox "Falta monto de cheque...", vbInformation
        TxtMontoCheque.SetFocus
        Exit Sub
    End If
    
    LvCheques.ListItems.Add , , ""
    u = LvCheques.ListItems.Count
    LvCheques.ListItems(u).SubItems(1) = TxtRutCheque
    LvCheques.ListItems(u).SubItems(2) = TxtNroCheque
    LvCheques.ListItems(u).SubItems(3) = CboBanco.Text
    LvCheques.ListItems(u).SubItems(4) = TxtPlaza
    LvCheques.ListItems(u).SubItems(5) = DtFecha
    LvCheques.ListItems(u).SubItems(6) = TxtMontoCheque
    LvCheques.ListItems(u).SubItems(8) = CboBanco.ItemData(CboBanco.ListIndex)
    
    
    TxtNroCheque = Val(TxtNroCheque) + 1
    DtFecha = DtFecha + 30
    TxtMonto = "0"
    SumaCheques
    
    
    
End Sub
Private Sub SumaCheques()
'    TxtTotalCheques = 0
'    For i = 1 To LvCheques.ListItems.Count
'        TxtTotalCheques = CDbl(TxtTotalCheques) + CDbl(TxtMontoCheque)
'    Next
 '   TxtTotalCheques = NumFormat(TxtTotalCheques)
 TxtTotalCheques = NumFormat(TotalizaColumna(LvCheques, "total"))
End Sub


Private Sub CmdSalir_Click()
    SG_Pago_Correcto = "NO"
    
    Unload Me
    'Me.Hide
End Sub

Private Sub CmdSeleccion_Click()
    TxtRutCliente = ""
    Me.TxtNombreCliente = ""
    TxtRutCliente.Tag = 0
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    LvDetalle.SelectedItem.EnsureVisible
    FrmPago.Caption = "PAGANDO NV [" & LvDetalle.SelectedItem.SubItems(1) & " ] "
    TxtAPagar = LvDetalle.SelectedItem.SubItems(6)
    TxtSaldoPago = TxtAPagar
    FrmPago.Tag = LvDetalle.SelectedItem
    TxtRutCliente.Tag = 0
    ClienteTieneCredito
    
    'Ver Productos
    Sql = "SELECT nvd_id,d.codigo,descripcion,nvd_cantidad,nvd_precio_unitario " & _
            "FROM ven_nota_venta_detalle d " & _
            "JOIN maestro_productos p ON d.codigo=p.codigo " & _
            "WHERE p.rut_emp='" & SP_Rut_Activo & "' AND nve_id=" & LvDetalle.SelectedItem
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvProductos, False, True, True, False
    TxtEfectivo.SetFocus
    
    
    
End Sub
Private Sub ClienteTieneCredito()
    If LvDetalle.ListItems.Count = 0 Then
        'Detectamos que es venta rapida
        MsgBox "Forma de pago no autorizada...", vbInformation
        Exit Sub
    End If
 'consultar si el "cliente" tiene credito
    If Len(LvDetalle.SelectedItem.SubItems(3)) > 0 Then
        If LvDetalle.SelectedItem.SubItems(3) <> "11.111.111-1" Then
                    TxtRutCliente = LvDetalle.SelectedItem.SubItems(3)
                    TxtNombreCliente = LvDetalle.SelectedItem.SubItems(4)
                    Sql = "SELECT cli_monto_credito " & _
                            "FROM maestro_clientes " & _
                            "WHERE rut_cliente='" & LvDetalle.SelectedItem.SubItems(3) & "'"
                    Consulta RsTmp, Sql
                    If RsTmp.RecordCount > 0 Then
                    'Debemos consultar el saldo de credito disponible del cliente.
                        TxtCupoUtilizado = ConsultaSaldoCliente(TxtRutCliente)
                        SkCupoCredito = NumFormat(Val(RsTmp!cli_monto_credito) - Val(TxtCupoUtilizado))
                        TxtRutCliente.Tag = SkCupoCredito
                    End If
        End If
    End If
End Sub



Private Sub Form_KeyUp(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF4 Then
        On Error Resume Next
        Me.TxtScanner.SetFocus
    End If
    If KeyCode = vbKeyF2 Then
        TxtEfectivo.SetFocus
    End If
    
    If KeyCode = vbKeyF5 Then
        If CmdF5.Enabled Then CmdF5_Click
    End If
    
   
    If KeyCode = 107 Then
        TxtEfectivo = TxtAPagar
        SumaPagos
        CmdF5.SetFocus
    End If
    
End Sub

Public Sub ActualizaStock(NroDoc As Long, IdDoc As Integer, NombreDoc As String)
        Dim Dp_Promedio As Double
        Dim Rp_Stock As Recordset
        Sql = "SELECT d.codigo,unidades,pro_inventariable,d.doc_id,v.id " & _
                "FROM ven_detalle d " & _
                "JOIN ven_doc_venta v ON d.doc_id=v.doc_id AND d.no_documento=v.no_documento " & _
                "JOIN maestro_productos p ON p.codigo=d.codigo " & _
                "WHERE d.rut_emp='" & SP_Rut_Activo & "' AND d.no_documento=" & NroDoc & " AND d.doc_id=" & IdDoc
        Consulta Rp_Stock, Sql
       If Rp_Stock.RecordCount > 0 Then
            Rp_Stock.MoveFirst
            Do While Not Rp_Stock.EOF
                If Rp_Stock!pro_inventariable = "SI" Then 'inventariable
                              Dp_Promedio = 0 '
                              Dp_Promedio = CostoAVG(Str(Rp_Stock!Codigo), 1) '
                               Sql = "SELECT pro_precio_neto promedio " & _
                                        "FROM inv_kardex k " & _
                                        "WHERE pro_codigo='" & Rp_Stock!Codigo & "' AND rut_emp='" & SP_Rut_Activo & "' AND bod_id=" & IG_id_Bodega_Ventas & " " & _
                                        "ORDER BY kar_id DESC " & _
                                         "LIMIT 1 "
                              Consulta RsTmp, Sql
                              If RsTmp.RecordCount > 0 Then Dp_Promedio = RsTmp!promedio '
                             
                              Kardex Fql(Date), "SALIDA", _
                                Rp_Stock!doc_id, Val(NroDoc), IG_id_Bodega_Ventas, Rp_Stock!Codigo, _
                                Rp_Stock!Unidades, "VENTA " & NombreDoc & " " & NroDoc, _
                                0, 0, LvDetalle.SelectedItem.SubItems(3), LvDetalle.SelectedItem.SubItems(4), , , Rp_Stock!doc_id, , , , Rp_Stock!Id
                End If
                ActulizaStockPackVirtual Rp_Stock!Codigo, NroDoc, NombreDoc, Rp_Stock!doc_id, Rp_Stock!Id
                Rp_Stock.MoveNext
          Loop
       End If
End Sub
Private Sub ActulizaStockPackVirtual(CodigoPV As Long, NroDoc As Long, NombreDoc As String, Docid As Integer, Id As Long)
        Dim Dp_Promedio2 As Double
        Dim Rp_Stock2 As Recordset
       Sql = "SELECT d.pro_codigo codigo,descripcion,'SI' pro_inventariable,red_cantidad unidades " & _
                    "FROM inv_receta_detalle d " & _
                    "JOIN inv_receta r ON d.rec_id=r.rec_id " & _
                    "JOIN maestro_productos m ON d.pro_codigo=m.codigo " & _
                    "WHERE r.pro_codigo=" & CodigoPV
       Consulta Rp_Stock2, Sql
        
       If Rp_Stock2.RecordCount > 0 Then
            Rp_Stock2.MoveFirst
            Do While Not Rp_Stock2.EOF
                If Rp_Stock2!pro_inventariable = "SI" Then 'inventariable
                              Dp_Promedio2 = 0 '
                              Dp_Promedio2 = CostoAVG(Str(Rp_Stock2!Codigo), 1) '
                               Sql = "SELECT pro_precio_neto promedio " & _
                                        "FROM inv_kardex k " & _
                                        "WHERE pro_codigo='" & Rp_Stock2!Codigo & "' AND rut_emp='" & SP_Rut_Activo & "' AND bod_id=" & IG_id_Bodega_Ventas & " " & _
                                        "ORDER BY kar_id DESC " & _
                                         "LIMIT 1 "
                              Consulta RsTmp, Sql
                              If RsTmp.RecordCount > 0 Then Dp_Promedio = RsTmp!promedio '
                             
                              Kardex Fql(Date), "SALIDA", _
                                Docid, Val(NroDoc), IG_id_Bodega_Ventas, Rp_Stock2!Codigo, _
                                Rp_Stock2!Unidades, "VENTA " & NombreDoc & " " & NroDoc, _
                                0, 0, LvDetalle.SelectedItem.SubItems(3), LvDetalle.SelectedItem.SubItems(4), , , Docid, , , , Id
                End If
                Rp_Stock2.MoveNext
          Loop
       End If

End Sub
Private Sub Form_Load()
   'Aplicar_skin Me
   Sm_Flag = "CARGADO"
    Skin2 Me, , 6
    CargaFormasDePago
    DtFechaFac = Date
    Centrar Me
    LLenarCombo CboBanco, "ban_nombre", "ban_id", "par_bancos", "ban_activo='SI'"
    DtFecha = Date
    
    If Im_Nv > 0 Then
        Lm_Alto = 6000
       ' CargaNotas
        Frame1.Visible = False
    
        FraFPago.Top = 20
        Me.Height = FraFPago.Height + 6000
        
        
        Cmd500p.Top = FraFPago.Top + 20
        
        Cmd100p.Top = Cmd500p.Top + Cmd500p.Height + 2
        
        Cmd50p.Top = Cmd100p.Top + Cmd100p.Height + 2
        Cmd10p.Top = Cmd50p.Top + Cmd50p.Height + 2
        
       ' Me.TxtScanner = Im_Nv
        
        
        
        
        
       ' TxtScanner_Validate True
        'CmdSeleccion_Click
        FraNVs.Visible = False
    Else
        CargaNotas
        Me.Height = 8865
        Lm_Alto = 9000
    End If
    RedondearControl Cmd10p
    RedondearControl Cmd500p
    RedondearControl Cmd100p
    RedondearControl Cmd50p
    LLenarCombo CboPlazos, "CONCAT(pla_nombre,'                                    ',CAST(pla_id AS CHAR))", "pla_dias", "par_plazos_vencimiento", "pla_activo='SI' AND pla_dias>1", "pla_orden"
    CboPlazos.ListIndex = 0
    
    If SP_Rut_Activo = "76.553.302-3" Or SP_Rut_Activo = "76.169.962-8" Or SP_Rut_Activo = "78.967.170-2" Or SP_Rut_Activo = "76.354.346-3" Or SP_Rut_Activo = "76.239.518-5" Then
        Me.SkinLabel10(0).Visible = True
        TxtNextBoleta.Visible = True
        If SP_Rut_Activo = "76.169.962-8" Or SP_Rut_Activo = "78.967.170-2" Or SP_Rut_Activo = "76.239.518-5" Then
            Me.SkinLabel10(1).Visible = True
            TxtNextFactura.Visible = True
            Me.SkinLabel10(2).Visible = True
            TxtNextVoucher.Visible = True
        End If
        If SP_Rut_Activo = "78.967.170-2" Or SP_Rut_Activo = "76.354.346-3" Then
            Me.DtFechaFac.Visible = True
        End If
        ObtenerProxNumeros
    End If
End Sub
Private Sub Form_Unload(Cancel As Integer)
    Im_Nv = 0
End Sub





Private Sub ImaCh_Click()
    If Val(TxtCh) > 0 Then
        TxtCh = 0
        LimpiaCheques
    Else
       Me.TxtCh = CDbl(TxtAPagar) ' - CDbl(TxtSumaPagos)
    End If
    CompruebaCheque
    SumaPagos
End Sub

Private Sub ImaCredito_Click()
    If Val(TxtCredito) > 0 Then
        TxtCredito = 0
    Else
        TxtCredito = CDbl(TxtAPagar) '- CDbl(TxtSumaPagos)
    End If
   SumaPagos
End Sub

Private Sub ImaDeposito_Click()
    If Val(TxtDeposito) > 0 Then
        TxtDeposito = 0
    Else
        Me.TxtDeposito = CDbl(TxtAPagar) '- CDbl(TxtSumaPagos)
    End If
    SumaPagos
End Sub

Private Sub ImaEfectivo_Click()
    If Val(TxtEfectivo) > 0 Then
        TxtEfectivo = 0
    Else
        Me.TxtEfectivo = CDbl(TxtAPagar) ' - CDbl(TxtSumaPagos)
    End If
    SumaPagos
End Sub

Private Sub ImaTC_Click()
    If Val(TxtTC) > 0 Then
        TxtTC = 0
    Else
        Me.TxtTC = CDbl(TxtAPagar) '- CDbl(TxtSumaPagos)
    End If
    SumaPagos
End Sub

Private Sub ImaTD_Click()
    If Val(TxtTD) > 0 Then
        TxtTD = 0
    Else
        Me.TxtTD = CDbl(TxtAPagar) '- CDbl(TxtSumaPagos)
    End If
    SumaPagos
End Sub

Private Sub LvCheques_DblClick()
    If LvCheques.SelectedItem Is Nothing Then Exit Sub
    
    TxtRutCheque = LvCheques.SelectedItem.SubItems(1)
    TxtNroCheque = LvCheques.SelectedItem.SubItems(2)
    Busca_Id_Combo CboBanco, Val(LvCheques.SelectedItem.SubItems(8))
    TxtPlaza = LvCheques.SelectedItem.SubItems(4)
    DtFecha = LvCheques.SelectedItem.SubItems(5)
    TxtMontoCheque = LvCheques.SelectedItem.SubItems(6)
    
    LvCheques.ListItems.Remove LvCheques.SelectedItem.Index
    SumaCheques
    
End Sub

Private Sub LvDetalle_Click()
    CmdSeleccion_Click
End Sub

Private Sub LvDetalle_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
ordListView ColumnHeader, Me, LvDetalle
End Sub

Private Sub LvDetalle_DblClick()
    CmdSeleccion_Click
End Sub



Private Sub SumaPagos()
    TxtSumaPagos = 0

    TxtSumaPagos = CDbl(TxtSumaPagos) + CDbl(TxtEfectivo)
    TxtSumaPagos = CDbl(TxtSumaPagos) + CDbl(TxtTC)
    TxtSumaPagos = CDbl(TxtSumaPagos) + CDbl(TxtTD)
    TxtSumaPagos = CDbl(TxtSumaPagos) + CDbl(TxtCh)
    TxtSumaPagos = CDbl(TxtSumaPagos) + CDbl(TxtCredito)
    TxtSumaPagos = CDbl(TxtSumaPagos) + CDbl(TxtDeposito)

    TxtSumaPagos = NumFormat(TxtSumaPagos)
    If CDbl(TxtAPagar) > CDbl(TxtSumaPagos) Then
        TxtSaldoPago = 0
    Else
        TxtSaldoPago = NumFormat(CDbl(TxtSumaPagos) - CDbl(TxtAPagar))
    End If
End Sub







Private Sub PicBilllete_Click()
    TxtEfectivo = TxtAPagar
End Sub






Private Sub LvProductos_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    ordListView ColumnHeader, Me, Me.LvProductos
End Sub

Private Sub Timer1_Timer()
    On Error Resume Next
    TxtEfectivo.SetFocus
    Timer1.Enabled = False
End Sub
Private Sub CargaNotas()

'    Sql = "SELECT id, no_documento,doc_nombre,v.rut_cliente,nombre_cliente,fecha,bruto, " & _
            "(SELECT doc_nombre FROM sis_documentos x WHERE x.doc_id=v.doc_id_indicio) docfinal,v.doc_id_indicio,r.ven_nombre,v.doc_id, " & _
            "ven_descuento_valor,ven_ajuste_descuento " & _
            "FROM ven_doc_venta v " & _
            "JOIN sis_documentos d ON v.doc_id=d.doc_id " & _
            "JOIN maestro_clientes c ON v.rut_cliente=c.rut_cliente " & _
            "JOIN par_vendedores r ON v.ven_id=r.ven_id " & _
            "WHERE doc_nota_de_venta='SI' AND ven_estado_nota_venta='PROCESO'"
            
    Sql = "SELECT nve_id, nve_id no_documento,'NOTA DE VENTA',IFNULL(v.rut_cliente,''),IFNULL(nombre_rsocial,'') ,nve_fecha,nve_total, " & _
            "(SELECT doc_nombre FROM sis_documentos x WHERE x.doc_id=v.doc_id) docfinal,v.doc_id,r.ven_nombre,v.doc_id, " & _
            "nve_valor_descuento,nve_valor_descuento_adicional,nve_valor_recargo,doc_cod_sii,nve_oc,suc_id " & _
            "FROM ven_nota_venta v " & _
            "JOIN sis_documentos d ON v.doc_id=d.doc_id " & _
            "LEFT JOIN maestro_clientes c ON v.rut_cliente=c.rut_cliente " & _
            "JOIN par_vendedores r ON v.ven_id=r.ven_id " & _
            "WHERE nve_cotizacion='NO' AND nve_terminada='NO' AND sue_id=" & IG_id_Sucursal_Empresa
    
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvDetalle, False, True, True, False
    LvProductos.ListItems.Clear
   
   
End Sub
Private Sub CargaFormasDePago()

    Sql = "SELECT mpa_id,mpa_nombre,mpa_fpago_if " & _
            "FROM par_medios_de_pago " & _
            "WHERE /*mpa_activo='SI' AND */ mpa_utiliza_pos='SI' " & _
            "ORDER BY mpa_orden"
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvPagos, False, True, True, False

End Sub




Private Sub TxtCh_GotFocus()
    En_Foco TxtCh
End Sub

Private Sub TxtCh_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
    If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtCh_Validate(Cancel As Boolean)
    If Val(TxtCh) = 0 Then TxtCh = "0"
    SumaPagos
    CompruebaCheque
End Sub
Private Sub CompruebaCheque()
    If Val(TxtCh) = 0 Then
        Me.FraFPago.Height = 350
        
        Me.Height = Lm_Alto
    Else
         Me.FraFPago.Height = 530 + 273
         
         Me.Height = Lm_Alto + 2730
         
    End If
    Centrar Me
    
End Sub


Private Sub TxtCredito_GotFocus()
    En_Foco TxtCredito
End Sub

Private Sub TxtCredito_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
    If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtCredito_Validate(Cancel As Boolean)
    If Val(TxtCredito) = 0 Then TxtCredito = "0"
    SumaPagos
End Sub



Private Sub TxtEfectivo_GotFocus()
    En_Foco TxtEfectivo
End Sub

Private Sub TxtEfectivo_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
    If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtEfectivo_Validate(Cancel As Boolean)
    If Val(TxtEfectivo) = 0 Then TxtEfectivo = "0"
    SumaPagos
End Sub

Private Sub TxtMontoCheque_GotFocus()
    En_Foco TxtMontoCheque
End Sub

Private Sub TxtMontoCheque_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
End Sub

Private Sub TxtMontoCheque_Validate(Cancel As Boolean)
    If Val(TxtMontoCheque) = 0 Then TxtMontoCheque = "0"
End Sub



Private Sub TxtNextBoleta_GotFocus()
    En_Foco TxtNextBoleta
End Sub

Private Sub TxtNextBoleta_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
End Sub

Private Sub TxtNextBoleta_Validate(Cancel As Boolean)
    If Val(TxtNextBoleta) = 0 Then
        ObtenerProxNumeros
        Exit Sub
    End If
    If Val(TxtNextBoleta) > 0 Then
        Sql = "SELECT id " & _
                "FROM ven_doc_venta " & _
                "WHERE rut_emp='" & SP_Rut_Activo & "' AND doc_id=2 AND no_documento=" & Val(TxtNextBoleta)
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
            MsgBox "Nro Utilizado ya ha sido utilizado...", vbInformation
            ObtenerProxNumeros
        
        End If
    
    
    End If
End Sub

Private Sub TxtNextFactura_GotFocus()
    En_Foco TxtNextFactura
End Sub

Private Sub TxtNextFactura_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
End Sub



Private Sub TxtNextFactura_Validate(Cancel As Boolean)
     If Val(TxtNextFactura) = 0 Then
        ObtenerProxNumeros
        Exit Sub
    End If
    If Val(TxtNextFactura) > 0 Then
        Sql = "SELECT id " & _
                "FROM ven_doc_venta " & _
                "WHERE rut_emp='" & SP_Rut_Activo & "' AND doc_id=1 AND no_documento=" & Val(TxtNextFactura)
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
            MsgBox "Nro Utilizado ya ha sido utilizado...", vbInformation
            ObtenerProxNumeros
        
        End If
    End If
End Sub

Private Sub TxtNextVoucher_GotFocus()
    En_Foco TxtNextVoucher
End Sub

Private Sub TxtNextVoucher_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
End Sub

Private Sub TxtNextVoucher_Validate(Cancel As Boolean)
    If Val(TxtNextVoucher) = 0 Then
        ObtenerProxNumeros
        Exit Sub
    End If
    If Val(TxtNextBoleta) > 0 Then
        Sql = "SELECT id " & _
                "FROM ven_doc_venta " & _
                "WHERE rut_emp='" & SP_Rut_Activo & "' AND doc_id=40 AND no_documento=" & Val(TxtNextVoucher)
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
            MsgBox "Nro Utilizado ya ha sido utilizado...", vbInformation
            ObtenerProxNumeros
        
        End If
    End If
End Sub

Private Sub TxtNroCheque_GotFocus()
    En_Foco TxtNroCheque
End Sub

Private Sub TxtNroCheque_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
    If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtPlaza_GotFocus()
    En_Foco TxtPlaza
End Sub

Private Sub TxtPlaza_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub TxtRutCheque_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub TxtRutCheque_Validate(Cancel As Boolean)
    Dim Sp_RutRevisado As String
    If Not VerificaRut(TxtRutCheque, Sp_RutRevisado) Then
        TxtRutCheque = ""
        Exit Sub
    End If
    TxtRutCheque = Sp_RutRevisado
End Sub





Private Sub TxtScanner_GotFocus()
    En_Foco TxtScanner
End Sub

Private Sub TxtScanner_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
    If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtScanner_Validate(Cancel As Boolean)
    If Val(TxtScanner) = 0 Then Exit Sub
    For i = 1 To LvDetalle.ListItems.Count
        If Val(LvDetalle.ListItems(i).SubItems(1)) = Val(TxtScanner) Then
            LvDetalle.ListItems(i).Selected = True
            LvDetalle.ListItems(i).EnsureVisible
            CmdSeleccion_Click
            TxtEfectivo.SetFocus
            Exit For
        End If
    Next
End Sub




Private Sub TxtTC_GotFocus()
    En_Foco TxtTC
End Sub

Private Sub TxtTC_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
    If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtTC_Validate(Cancel As Boolean)
    If Val(TxtTC) = 0 Then TxtTC = 0
    SumaPagos
End Sub

Private Sub TxtTD_GotFocus()
    En_Foco TxtTD
End Sub

Private Sub TxtTD_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
    If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtTD_Validate(Cancel As Boolean)
    If Val(TxtTD) = 0 Then TxtTD = "0"
    SumaPagos
End Sub

Private Sub TxtTemp_GotFocus()
    En_Foco TxtTemp
End Sub



Private Sub FacturaElectronicaPOS(Tipo As String, NroDocumento As Long, Docid As Integer, FormaPago As String)
    Dim Sp_Xml As String, Sp_XmlAdicional As String
    Dim obj As DTECloud.Integracion
    Dim Lp_Sangria As Long
    Dim Sp_Sql As String
    Dim Bp_Adicional As Boolean
    Dim Dp_Neto As Long
    Dim Dp_Iva As Long
    Dim Dp_Total As Long
    Dim dsctoGeneral As Long
    '31 Agosto 2015 Pos
    'Modulo genera factura electronica
    'Autor: alvamar
    Bp_Adicional = False
    
    '1ro Crear xml
    Lp_Sangria = 4
    X = FreeFile
    dsctoGeneral = 0
   ' Sp_Xml = App.Path & "\dte\" & CboDocVenta.ItemData(CboDocVenta.ListIndex) & "_" & TxtNroDocumento & ".xml"
    Sp_Xml = "C:\FACTURAELECTRONICA\nueva.xml"
      'Open Sp_Archivo For Output As X
      Sm_Con_Precios_Brutos = "SI"
    Open Sp_Xml For Output As X
        '<?xml version="1.0" encoding="ISO-8859-1"?>
        Print #X, "<DTE version=" & Chr(34) & "1.0" & Chr(34) & ">"
        Print #X, "" & Space(Lp_Sangria) & "<Documento ID=" & Chr(34) & "ID" & Id_Ventas_Unico & Chr(34) & ">"
        'Encabezado
        Print #X, "" & Space(Lp_Sangria) & "<Encabezado>"
            'id documento
            Print #X, "" & Space(Lp_Sangria * 2) & "<IdDoc>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<TipoDTE>" & Tipo & "</TipoDTE>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<Folio>" & NroDocumento & "</Folio>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<FchEmis>" & Fql(Date) & "</FchEmis>"
           '     Print #X, "" & Space(Lp_Sangria * 3) & "<FchVenc>" & Format(DtFecha.Value + CboPlazos.ItemData(CboPlazos.ListIndex), "; YYYY - MM - DD; ") & "</FchVenc>"
            '    If Sm_Con_Precios_Brutos = "SI" Then
            '        '30 Mayo 2015, cuando las lineas de detalle son precios brutos se debe identificar aqui.
            '        Print #X, "" & Space(Lp_Sangria * 2) & "<MntBruto>1</MntBruto>"
            '    End If
            Print #X, "" & Space(Lp_Sangria * 2) & "</IdDoc>"
            'Emisor
            'Select empresa para obtener los datos del emisor
            Sp_Sql = "SELECT nombre_empresa,direccion,comuna,ciudad,giro,emp_codigo_actividad_economica actividad " & _
                    "FROM sis_empresas " & _
                    "WHERE rut='" & SP_Rut_Activo & "'"
            Consulta RsTmp, Sp_Sql
            
            Print #X, "" & Space(Lp_Sangria * 2) & " <Emisor>"
                'Print #X, "" & Space(Lp_Sangria * 3) & "<RUTEmisor>" & Replace(SP_Rut_Activo, ".", "") & "</RUTEmisor>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<RUTEmisor>" & Replace(SP_Rut_Activo, ".", "") & "</RUTEmisor>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<RznSoc>" & Replace(RsTmp!nombre_empresa, "�", "N") & "</RznSoc>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<GiroEmis>" & Replace(RsTmp!giro, "�", "N") & "</GiroEmis>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<Acteco>" & Replace(RsTmp!actividad, "�", "N") & "</Acteco>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<DirOrigen>" & Replace(RsTmp!direccion, "�", "N") & "</DirOrigen>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<CmnaOrigen>" & Replace(RsTmp!comuna, "�", "N") & "</CmnaOrigen>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<CiudadOrigen>" & Replace(RsTmp!ciudad, "�", "N") & "</CiudadOrigen>"
            Print #X, "" & Space(Lp_Sangria * 2) & "</Emisor>"
            
            'Receptor
            
            'Select datos cliente
            Sp_Sql = "SELECT nombre_rsocial,giro,direccion,comuna,ciudad " & _
                        "FROM maestro_clientes " & _
                        "WHERE rut_cliente='" & LvDetalle.SelectedItem.SubItems(3) & "'"
            Consulta RsTmp, Sp_Sql
            
            
            If RsTmp.RecordCount > 0 Then
                If Val(LvDetalle.SelectedItem.SubItems(16)) > 0 Then
                
                    Sp_Sql = "SELECT '" & RsTmp!nombre_rsocial & "' AS nombre_rsocial,'" & RsTmp!giro & "' AS giro,suc_direccion direccion,'" & RsTmp!comuna & "' AS comuna,suc_ciudad ciudad " & _
                                "FROM par_sucursales " & _
                                "WHERE suc_id=" & Val(LvDetalle.SelectedItem.SubItems(16))
                    Consulta RsTmp, Sp_Sql
                End If
            End If
            
            
            Print #X, "" & Space(Lp_Sangria * 2) & " <Receptor>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<RUTRecep>" & Replace(LvDetalle.SelectedItem.SubItems(3), ".", "") & "</RUTRecep>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<RznSocRecep>" & Mid(QuitarAcentos(RsTmp!nombre_rsocial), 1, 100) & "</RznSocRecep>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<GiroRecep>" & Mid(QuitarAcentos(RsTmp!giro), 1, 40) & "</GiroRecep>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<Contacto></Contacto>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<DirRecep>" & Mid(QuitarAcentos(RsTmp!direccion), 1, 60) & "</DirRecep>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<CmnaRecep>" & Mid(QuitarAcentos(RsTmp!comuna), 1, 20) & "</CmnaRecep>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<CiudadRecep>" & Mid(QuitarAcentos(RsTmp!ciudad), 1, 20) & "</CiudadRecep>"
            Print #X, "" & Space(Lp_Sangria * 2) & "</Receptor>"
            
             'Totales
            Dp_Total = CDbl(TxtAPagar)
            Dp_Neto = Round(Dp_Total / Val("1." & DG_IVA), 0)
            Dp_Iva = Dp_Total - Dp_Neto
             
            Print #X, "" & Space(Lp_Sangria * 2) & " <Totales>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<MntNeto>" & Dp_Neto & "</MntNeto>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<MntExe>" & 0 & "</MntExe>"
                Print #X, "" & Space(Lp_Sangria * 3) & " <TasaIVA>" & DG_IVA & "</TasaIVA>"
                Print #X, "" & Space(Lp_Sangria * 3) & " <IVA>" & Dp_Iva & "</IVA>"
                'Antes del monto total debemos verificar si la factura incluye impuestos retenidos
'                                         <ImptoReten>
'                                            <TipoImp>18</TipoImp>
'                                            <TasaImp>5</TasaImp>
'                                            <MontoImp>8887</MontoImp>
'                                        </ImptoReten>
            Print #X, "" & Space(Lp_Sangria * 3) & "<MntTotal>" & Dp_Total & "</MntTotal>"
            Print #X, "" & Space(Lp_Sangria * 2) & "</Totales>"
                
            'Cerramos encabezado
            Print #X, "" & Space(Lp_Sangria) & "</Encabezado>"
                
                
           'Comenzamos el detalle
           
           If SP_Control_Inventario = "SI" Then
           'Recorremos la grilla con productos
                        Sql = "SELECT unidades,descripcion,precio_final,ved_codigo_interno,ved_precio_venta_neto,ROUND(ved_precio_venta_neto/unidades,2) unitario " & _
                                "FROM ven_detalle " & _
                                "WHERE doc_id=" & Docid & " AND no_documento=" & NroDocumento & " AND rut_emp='" & SP_Rut_Activo & "'"
                        Consulta RsTmp, Sql
                        i = 1
                        RsTmp.MoveFirst
                        Do While Not RsTmp.EOF
                            'For i = 1 To LvMateriales.ListItems.Count
                            Print #X, "" & Space(Lp_Sangria) & "<Detalle>"
                              Print #X, "" & Space(Lp_Sangria * 2) & "<NroLinDet>" & i & "</NroLinDet>"
                              Print #X, "" & Space(Lp_Sangria * 2) & " <CdgItem>"
                              Print #X, "" & Space(Lp_Sangria * 3) & "<TpoCodigo>PLU</TpoCodigo>"
                              Print #X, "" & Space(Lp_Sangria * 3) & "<VlrCodigo>" & RsTmp!ved_codigo_interno & "</VlrCodigo>"
                              Print #X, "" & Space(Lp_Sangria * 2) & "</CdgItem>"
                              Print #X, "" & Space(Lp_Sangria * 2) & "<NmbItem>" & Mid(QuitarAcentos(RsTmp!Descripcion), 1, 80) & "</NmbItem>"
                              Print #X, "" & Space(Lp_Sangria * 2) & "<QtyItem>" & Replace(RsTmp!Unidades, ",", ".") & "</QtyItem>"
                              Print #X, "" & Space(Lp_Sangria * 2) & "<PrcItem>" & CxP(RsTmp!unitario) & "</PrcItem>"
              '                    <DescuentoPct>15</DescuentoPct>
              '                    <DescuentoMonto>4212</DescuentoMonto>
              '                             <!- Solo se indica el c�digo del impuesto adicional '
              '                    <CodImpAdic>17</CodImpAdic>
                              Print #X, "" & Space(Lp_Sangria * 2) & "<MontoItem>" & RsTmp!ved_precio_venta_neto & "</MontoItem>"
                      
                            Print #X, "" & Space(Lp_Sangria) & "</Detalle>"
                             'Next
                            RsTmp.MoveNext
                            i = i + 1
                        Loop
                        dsctoGeneral = Val(LvDetalle.SelectedItem.SubItems(11)) + Val(LvDetalle.SelectedItem.SubItems(12)) - Val(LvDetalle.SelectedItem.SubItems(13))
                        If dsctoGeneral > 0 Then
                            dsctoGeneral = Round(dsctoGeneral / (1.19), 0)
                            Print #X, "" & Space(Lp_Sangria) & "<DscRcgGlobal>"
                                   Print #X, "" & Space(Lp_Sangria * 2) & "<NroLinDR>1</NroLinDR>"
                                   Print #X, "" & Space(Lp_Sangria * 2) & "<TpoMov>D</TpoMov>"
                                   Print #X, "" & Space(Lp_Sangria * 2) & "<GlosaDR>Dscto. Cliente</GlosaDR>"
                                    Print #X, "" & Space(Lp_Sangria * 2) & "<TpoValor>$</TpoValor>"
                                   Print #X, "" & Space(Lp_Sangria * 2) & "<ValorDR>" & dsctoGeneral & "</ValorDR>"
                            Print #X, "" & Space(Lp_Sangria) & "</DscRcgGlobal>"
                        
                                '<DscRcgGlobal>
                                '    <NroLinDR>1</NroLinDR>
                                '    <TpoMov>D</TpoMov>
                                '    <GlosaDR>Descuento global al documento</GlosaDR>
                                ''    <TpoValor>%</TpoValor>
                                 '   <ValorDR>12</ValorDR>
                                '</DscRcgGlobal>
                        End If
            Else
                'CUANDO ES SIN INVENTARIO
                
            
            
            
            End If
            'Cerramos detalle
                        'Si referencia a OC incluir en el modulo _
            21 -07 - 2016
            If Len(LvDetalle.SelectedItem.SubItems(15)) > 0 Then
                Print #X, "" & Space(Lp_Sangria) & "<Referencia>"
                     Print #X, "" & Space(Lp_Sangria * 2) & "<NroLinRef>1</NroLinRef>"
                     Print #X, "" & Space(Lp_Sangria * 2) & "<TpoDocRef>801</TpoDocRef>"
                     Print #X, "" & Space(Lp_Sangria * 2) & "<FolioRef>" & LvDetalle.SelectedItem.SubItems(15) & "</FolioRef>"
                     Print #X, "" & Space(Lp_Sangria * 2) & "<FchRef>" & Fql(DtFecha) & "</FchRef>"
                Print #X, "" & Space(Lp_Sangria) & "</Referencia>"
            
            
            
            End If
                
'        If FrmReferencia.Visible Then
'                'CREAR REFERENCIA
'
'                    If SkCodigoReferencia = 4 Then SkCodigoReferencia = 3
'                    SkFechaReferencia = Format(Me.SkFechaReferencia, "YYYY-MM-DD")
'                          '1  SkDocIdReferencia = 33
'
'                    Print #X, "" & Space(Lp_Sangria) & "<Referencia>"
'                    Print #X, "" & Space(Lp_Sangria * 2) & "<NroLinRef>1</NroLinRef>"
'                    Print #X, "" & Space(Lp_Sangria * 2) & "<TpoDocRef>" & SkDocIdReferencia & "</TpoDocRef>"
'                    Print #X, "" & Space(Lp_Sangria * 2) & "<FolioRef>" & TxtNroReferencia & "</FolioRef>"
'
'                    Print #X, "" & Space(Lp_Sangria * 2) & "<FchRef>" & SkFechaReferencia & "</FchRef>"
'                    Print #X, "" & Space(Lp_Sangria * 2) & "<CodRef>" & SkCodigoReferencia & "</CodRef>"
'                    Print #X, "" & Space(Lp_Sangria * 2) & "<RazonRef>" & TxtMotivoRef & "</RazonRef>"
'
'                Print #X, "" & Space(Lp_Sangria) & "</Referencia>"
'
'
'            'CERRAR REFERNCIA
'        End If

        Print #X, "" & Space(Lp_Sangria) & "</Documento>"
        Print #X, "" & Space(Lp_Sangria) & "</DTE>"
    Close #X
        
        
        
        
        
    Bp_Adicional = True
    X = FreeFile
    Sp_XmlAdicional = "C:\FACTURAELECTRONICA\nueva_adicional.xml"
    Open Sp_XmlAdicional For Output As X
        Print #X, "<Adicional>"
        
        Print #X, "" & Space(Lp_Sangria) & "<Uno></Uno>"
        Print #X, "" & Space(Lp_Sangria) & "<Tres>" & FormaPago & "</Tres>"
        Print #X, "" & Space(Lp_Sangria) & "<Cuatro></Cuatro>"
        Print #X, "" & Space(Lp_Sangria) & "<Cinco>" & NumFormat(Dp_Neto + dsctoGeneral) & "</Cinco>"
        Print #X, "" & Space(Lp_Sangria) & "<Seis></Seis>"
        Print #X, "" & Space(Lp_Sangria) & "<Siete></Siete>"
        Print #X, "" & Space(Lp_Sangria) & "<Ocho></Ocho>"
        Print #X, "" & Space(Lp_Sangria) & "<Nueve></Nueve>"
        Print #X, "" & Space(Lp_Sangria) & "<Diez></Diez>"
        Print #X, "</Adicional>"
    Close #X
        
'    If Len(TxtComentario) > 0 Or Len(TxtOrdenesCompra) > 0 Then
'        Bp_Adicional = True
'        X = FreeFile
'        Sp_XmlAdicional = "C:\nueva_adicional.xml"
'        Open Sp_XmlAdicional For Output As X
'            Print #X, "<Adicional>"
'            If Len(TxtComentario) > 0 Then
'                Print #X, "" & Space(Lp_Sangria) & "<Uno>" & TxtComentario & "</Uno>"
'                If Len(TxtOrdenesCompra) > 0 Then
'                    Print #X, "" & Space(Lp_Sangria) & "<Dos>" & TxtOrdenesCompra & "</Dos>"
'                End If
'            Else
'                If Len(TxtOrdenesCompra) > 0 Then
'                    Print #X, "" & Space(Lp_Sangria) & "<Uno>" & TxtOrdenesCompra & "</Uno>"
'                End If
'            End If
'            Print #X, "" & Space(Lp_Sangria) & "<Tres></Tres>"
'            Print #X, "" & Space(Lp_Sangria) & "<Cuatro></Cuatro>"
'            Print #X, "" & Space(Lp_Sangria) & "<Cinco></Cinco>"
'            Print #X, "" & Space(Lp_Sangria) & "<Seis></Seis>"
'            Print #X, "" & Space(Lp_Sangria) & "<Siete></Siete>"
'            Print #X, "" & Space(Lp_Sangria) & "<Ocho></Ocho>"
'            Print #X, "" & Space(Lp_Sangria) & "<Nueve></Nueve>"
'            Print #X, "" & Space(Lp_Sangria) & "<Diez></Diez>"
'            Print #X, "</Adicional>"
'        Close #X
'
'
'    End If
        
        Set obj = New DTECloud.Integracion
   
        obj.UrlServicioFacturacion = SG_Url_Factura_Electronica '"http://wsdtedesa.dyndns.biz/WSDTE/Service.asmx"
        obj.HabilitarDescarga = True
        obj.FormatoNombrePDF = "0"
        obj.CarpetaDestino = "C:\FACTURAELECTRONICA\PDF"
        obj.Password = "1234"
        
        obj.AsignarFolio = False
        obj.IncluirCopiaCesion = True
       ' obj.FolioAsignado
        
        
        'MSXML.DOMDocument
        Dim Documento As MSXML2.DOMDocument30
        Set Documento = New DOMDocument30
        
        Documento.preserveWhiteSpace = True
        Documento.Load (Sp_Xml)
        '  Dim oXMLAdicional As New xml.XMLDocument
        Dim documento2 As MSXML2.DOMDocument30
        Set documento2 = New DOMDocument30
        
        documento2.preserveWhiteSpace = True
        If Bp_Adicional Then
            documento2.Load (Sp_XmlAdicional)
        Else
            documento2.Load ("")
        End If
        '******************************* modo productivo
        
        obj.Productivo = BG_ModoProductivoDTE
        
        '***********************************
        If obj.ProcesarXMLBasico(Documento.XML, documento2.XML) Then
             'MsgBox obj.URLPDF
             Archivo = "C:\FACTURAELECTRONICA\PDF\T" & Tipo & "F" & NroDocumento & ".PDF"
             SG_NombrePDF = Archivo
             
             If Trim(Len(SG_NombrePDF)) = 0 Then
                        If obj.ProcesarXMLBasico(Documento.XML, documento2.XML) Then
                            'MsgBox obj.URLPDF
                            Archivo = "C:\FACTURAELECTRONICA\PDF\T" & Tipo & "F" & NroDocumento & ".PDF"
                            SG_NombrePDF = Archivo
                            ShellExecute Me.hWnd, "open", Archivo, "", "", 4
                        Else
                             
                             MsgBox (CStr(obj.IDResultado) + " " + obj.DescripcionResultado)
                        End If
            Else
             
                 ShellExecute Me.hWnd, "open", Archivo, "", "", 4
            End If
          
        Else
            MsgBox (CStr(obj.IDResultado) + " " + obj.DescripcionResultado)
        End If
End Sub

Private Function ConvertirADocVenta(NroDoc As Long, Vencimiento As Date, Dias As Integer, FormaS As String, CondicionP As String, PlazoID As String) As Boolean
    Dim Id_Documento As Long
    Dim Tipo_Documento As String
    Dim LaFecha As Date
    Dim Ip_Suc_id As Integer
    On Error GoTo ProblemaConvertir
    Ip_Suc_id = LvDetalle.SelectedItem.SubItems(16)
    Id_Documento = LvDetalle.SelectedItem.SubItems(10)
    Tipo_Documento = LvDetalle.SelectedItem.SubItems(7)
    
    If (SP_Rut_Activo = "76.169.962-8" Or SP_Rut_Activo = "76.553.302-3") And LvDetalle.SelectedItem.SubItems(10) = 2 Then
        If FormaS = "TARJETA CREDITO" Or FormaS = "TARJETA DEBITO" Then
        
            Id_Documento = 40
            Tipo_Documento = "VOUCHER TRANSBANK"
            PlazoID = 5
        End If
    End If
    
    
    'Consultar si el doc existe
    Sql = "SELECT id " & _
            "FROM ven_doc_venta " & _
            "WHERE rut_emp='" & SP_Rut_Activo & "' AND doc_id=" & Id_Documento & " AND no_documento=" & NroDoc
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        ConvertirADocVenta = False
        MsgBox "Solicitar verificacion de folios...", vbInformation
        Exit Function
    End If

    LaFecha = Date
    If SP_Rut_Activo = "78.967.170-2" Or SP_Rut_Activo = "76.354.346-3" Then LaFecha = Me.DtFechaFac
    If IG_id_Bodega_Ventas = 0 Then IG_id_Bodega_Ventas = 1
'        Lp_Id_Unico_Venta = UltimoNro("ven_doc_venta", "id")
    Sql = "INSERT INTO ven_doc_venta " & _
              "(id,no_documento,fecha,rut_cliente,nombre_cliente,tipo_movimiento,neto,bruto,iva,comentario,CondicionPago," & _
              "ven_id,ven_nombre,ven_comision,forma_pago,tipo_doc,doc_id,suc_id,ven_fecha_vencimiento," & _
              "usu_nombre,id_ref,rut_emp,pla_id,are_id,cen_id,exento,ven_iva_retenido,bod_id,ven_plazo,ven_comentario," & _
              "ven_ordendecompra,ven_tipo_calculo,caj_id,sue_id," & _
              "gir_id,rso_id,tnc_id,ven_boleta_hasta,doc_id_indicio,doc_time,ven_sub_total,ven_ajuste_descuento,ven_ajuste_recargo,ven_descuento_valor,ven_descuentoxcien,ven_plazo_id) " & _
              "SELECT " & Id_Ventas_Unico & "," & NroDoc & ",'" & Fql(LaFecha) & "',v.rut_cliente,nombre_rsocial,'VD',nve_neto,nve_total,nve_iva,'VENTA POS','" & CondicionP & "',v.ven_id, " & _
                        "ven_nombre,'0','" & FormaS & "','" & Tipo_Documento & "'," & Id_Documento & "," & Ip_Suc_id & ",'" & Fql(Vencimiento) & "',nve_login,0,'" & SP_Rut_Activo & "',0,0,0,0,0," & _
                        IG_id_Bodega_Ventas & "," & Dias & ",'','',0," & LG_id_Caja & "," & IG_id_Sucursal_Empresa & ",0," & 0 & "," & 0 & "," & NroDoc & "," & Id_Documento & " "
              
              
            Sql = Sql & ",curtime(),nve_total,nve_valor_descuento_adicional,nve_valor_recargo,nve_valor_descuento,nve_descuentoxcien," & PlazoID & " " & _
              "FROM ven_nota_venta v " & _
                "LEFT JOIN maestro_clientes c ON v.rut_cliente=c.rut_cliente " & _
                "JOIN par_vendedores s ON v.ven_id=s.ven_id " & _
                "WHERE nve_id=" & LvDetalle.SelectedItem

         '   Sql = Sql & "," & PlazoID
        cn.Execute Sql
        
        If SP_Rut_Activo = "76.354.346-3" Then
            'si es mafil, ingresar las cuotas pendientes de pagos.
            If VenPosCaja.LvCuotas.ListItems.Count > 0 Then
                For J = 1 To VenPosCaja.LvCuotas.ListItems.Count
                     Sql = "INSERT INTO ven_doc_venta " & _
                          "(id,no_documento,fecha,rut_cliente,nombre_cliente,tipo_movimiento,neto,bruto,iva,comentario,CondicionPago," & _
                          "ven_id,ven_nombre,ven_comision,forma_pago,tipo_doc,doc_id,suc_id,ven_fecha_vencimiento," & _
                          "usu_nombre,id_ref,rut_emp,pla_id,are_id,cen_id,exento,ven_iva_retenido,bod_id,ven_plazo,ven_comentario," & _
                          "ven_ordendecompra,ven_tipo_calculo,caj_id,sue_id," & _
                          "gir_id,rso_id,tnc_id,ven_boleta_hasta,doc_id_indicio,doc_time,ven_sub_total,ven_ajuste_descuento,ven_ajuste_recargo,ven_descuento_valor,ven_descuentoxcien,ven_plazo_id) " & _
                          "SELECT " & UltimoNro("ven_doc_venta", "id") & "," & J & ",'" & Fql(LaFecha) & "',v.rut_cliente,nombre_rsocial,'VD',0," & CDbl(VenPosCaja.LvCuotas.ListItems(J).SubItems(2)) & ",0,'VENTA POS','" & CondicionP & "',v.ven_id, " & _
                                    "ven_nombre,'0','" & FormaS & "','" & "CUOTA:" & J & " " & Tipo_Documento & " Nro " & NroDoc & "'," & 100 & "," & Ip_Suc_id & ",'" & Fql(VenPosCaja.LvCuotas.ListItems(J).SubItems(1)) & "',nve_login,0,'" & SP_Rut_Activo & "',0,0,0,0,0," & _
                                    IG_id_Bodega_Ventas & "," & Dias & ",'','',0," & LG_id_Caja & "," & IG_id_Sucursal_Empresa & ",0," & 0 & "," & 0 & "," & NroDoc & "," & Id_Documento & " "
                          
                          
                    Sql = Sql & ",curtime()," & CDbl(VenPosCaja.LvCuotas.ListItems(J).SubItems(2)) & ",0,0,0,0," & PlazoID & " " & _
                          "FROM ven_nota_venta v " & _
                            "LEFT JOIN maestro_clientes c ON v.rut_cliente=c.rut_cliente " & _
                            "JOIN par_vendedores s ON v.ven_id=s.ven_id " & _
                            "WHERE nve_id=" & LvDetalle.SelectedItem
                    
                    cn.Execute Sql
            
            
                Next
            End If
        End If
        
        
        
        
        
        
        Sql = "INSERT INTO ven_detalle (codigo,descripcion,precio_real,descuento,unidades,precio_final,subtotal," & _
                                    "precio_costo,fecha,doc_id,no_documento,rut_emp,pla_id,ved_precio_venta_bruto," & _
                                    "ved_precio_venta_neto,ved_iva,ved_codigo_interno)  " & _
                                "SELECT codigo,nvd_descripcion,nvd_precio_unitario,0,nvd_cantidad,nvd_precio_unitario,nvd_precio_total,nvd_precio_costo,nve_fecha," & Id_Documento & "," & _
                                 NroDoc & ",'" & SP_Rut_Activo & "'," & 1 & ",nvd_precio_total,nvd_precio_total_venta_neto," & _
                                 "nvd_precio_total-nvd_precio_total_venta_neto,pro_codigo_interno " & _
                                "FROM ven_nota_venta_detalle d " & _
                                "JOIN ven_nota_venta v ON d.nve_id=v.nve_id " & _
                                "WHERE d.nve_id=" & LvDetalle.SelectedItem
                                
        
        cn.Execute Sql
        
        
      ConvertirADocVenta = True
      
      Exit Function
      
ProblemaConvertir:
    MsgBox "Problema al intentar generar el documento..." & vbNewLine & Err.Number & vbNewLine & Err.Description, vbInformation
 End Function

Private Sub DescargaForzadaDTE(nro_Doc As String)
  'Consulta electronica
        Dim obj2 As DTECloud.Integracion
        Set obj2 = New DTECloud.Integracion

        obj2.Productivo = True
        'obj.UrlServicioFacturacion = "http://wsdtedesa.dyndns.biz/WSDTE/Service.asmx"
        obj2.UrlServicioFacturacion = "http://wsdte.dyndns.biz/WSDTE/Service.asmx?WSDL"
        obj2.HabilitarDescarga = True
        
        If Dir("C:\FACTURAELECTRONICA\PDF\" & Replace(SP_Rut_Activo, ".", ""), vbDirectory) = "" Then
            'MsgBox "La carpeta no existe"
            MkDir ("C:\FACTURAELECTRONICA\PDF\" & Replace(SP_Rut_Activo, ".", ""))
        End If
        
        obj2.CarpetaDestino = "C:\FACTURAELECTRONICA\PDF\" & Replace(SP_Rut_Activo, ".", "")
        
        
        
        'obj.CarpetaDestino = "C:\FACTURAELECTRONICA\PDF"
        obj.Password = "1234"
        'resp = obj.DescargarPDF(rutempresa.Text, rutempresa.Text, Combo1.Text, folio.Text, "E", False, False)
        resp = obj.DescargarPDF(Replace(SP_Rut_Activo, ".", ""), Replace(SP_Rut_Activo, ".", ""), "33", Trim(nro_Doc), "E", False, True)
       ' MsgBox obj.URLPDF
  '      Call Shell(obj.RutaPDF)
        Archivo = obj.URLPDF
        ShellExecute Me.hWnd, "open", Archivo, "", "", 4
End Sub

Private Sub ImprimeFacturaTermica(Numero As String, Doc As String, Cedible As Boolean, firma As Boolean, NombreDocumento As String)
    Dim Cx As Double, Cy As Double, Sp_Fecha As String, Ip_Mes As Integer, Dp_S As Double, Sp_Letras As String
    Dim Sp_Neto As String * 12, Sp_IVA As String * 12, Sp_Total As String * 12
    Dim Sp_Cantidad As String * 7
    Dim Sp_Detalle As String * 40
    Dim Sp_Codigo As String * 10
    Dim Sp_Unitario As String * 11
    Dim Sp_Total_linea As String * 11
    Dim Sp_GuiasFacturadas As String
    Dim Sp_Segun_Guias As String
    Dim Sp_Sql As String
    Dim Sp_Neto_P As String * 10
    Dim Sp_IVA_P As String * 10
    Dim Sp_Total_P As String * 10
    Dim Rp_Fac As Recordset
    
    RSet Sp_Neto_P = "NETO    $:"
    RSet Sp_IVA_P = "IVA " & DG_IVA & "% $:"
    RSet Sp_Total_P = "TOTAL   $:"
    Printer.DrawWidth = 2
    Printer.FontName = "Courier New"
    Printer.FontBold = False
    Printer.ScaleMode = vbCentimeters
    Printer.FontSize = 10
    Printer.ScaleMode = 7
    Printer.FontBold = False
    Cx = 0.5
    'Cy = 5.9
    Cy = 1
    Dp_S = 0.17 'interlinea
    Printer.FontName = "Arial"
    Printer.FontSize = 11
    Printer.FontBold = True
    If SP_Rut_Activo = "3.630.608-4" Then
        Printer.Print "                   REPUESTOS BRIONES"
    Else
    
        Printer.Print "                     TOTALGOMAS"
    End If
    linini = Printer.CurrentY + 0.1
    Printer.CurrentY = Printer.CurrentY + 0.2
    Printer.CurrentX = Cx
    Printer.FontSize = 10
    Printer.FontName = "Courier"
    
    Printer.Print "                R.U.T.:" & SP_Rut_Activo
    Printer.CurrentY = Printer.CurrentY + 0.1
    Printer.CurrentX = Cx
    
    Printer.Print "       " & NombreDocumento
    Printer.CurrentY = Printer.CurrentY + 0.1
    Printer.CurrentX = Cx
    
    
    Printer.Print "                    N� " & Numero
    Printer.CurrentY = Printer.CurrentY + 0.1
    Printer.CurrentX = Cx
    
    linfinm = Printer.CurrentY
    
    Printer.Print "                      S.I.I. - TEMUCO"
    Printer.CurrentY = Printer.CurrentY + 0.1
    Printer.CurrentX = Cx
    
    
    Printer.Line (Cy, linini)-(6.7, linfinm), , B  'Rectangulo Encabezado y N� Nota de Venta
     
    
    Printer.FontBold = False
    Printer.FontSize = 9
     
    Printer.CurrentX = Cx
    Printer.CurrentY = linfinm + 0.7
    
    
    Printer.FontBold = True
    If SP_Rut_Activo = "3.630.608-4" Then
        Printer.Print " SERGIO ADOLFO BRIONES VALDERRAMA"
    Else
        Printer.Print " SOC. COMERCIAL TOTALGOMAS LTDA."
    End If
 '   Printer.CurrentY = Printer.CurrentY + 0.1
    Printer.CurrentX = Cx
    Printer.FontBold = False
    
        
    Printer.Print " VENTA DE PARTES, PIEZAS Y ACCESORIOS"
  '  Printer.CurrentY = Printer.CurrentY + 0.1
    Printer.CurrentX = Cx
        
    Printer.Print " DE VEHICULOS AUTOMOTORES."
  '  Printer.CurrentY = Printer.CurrentY + 0.1
    Printer.CurrentX = Cx
    If SP_Rut_Activo = "3.630.608-4" Then
        Printer.Print " MANUEL MONTT 1117 - TEMUCO - TEMUCO"
    Else
        Printer.Print " MANUEL MONTT 1299 - TEMUCO - TEMUCO"
    End If
  '  Printer.CurrentY = Printer.CurrentY + 0.1
    Printer.CurrentX = Cx
    If SP_Rut_Activo = "3.630.608-4" Then
    
    Else
        Printer.Print " FONO 0452730022"
    End If
   ' Printer.CurrentY = Printer.CurrentY + 0.1
    Printer.CurrentX = Cx
    
    If SP_Rut_Activo = "3.630.608-4" Then
        Printer.Print "repuestosbrionestemuco@gmail.com"
    Else
        Printer.Print " ventastotalgomas@gmail.com"
    End If
    'Printer.CurrentY = Printer.CurrentY + 0.1
    Printer.CurrentX = Cx
    
    
    Printer.FontName = "Arial"
    Printer.FontSize = "8"
    
    md = 1.3
    'pos = Printer.CurrentY
    Sp_Fecha = Date
    Printer.CurrentX = Cx
    Printer.CurrentY = Printer.CurrentY + 0.6
    Cy = Printer.CurrentY
    Printer.FontBold = True
    Printer.Print "FECHA:"
    Printer.FontBold = False
    Printer.CurrentX = Cx + md
    Printer.CurrentY = Cy
    
    Printer.Print Day(Date)
    Printer.CurrentX = Cx + md + 0.5
    Printer.CurrentY = Cy
    Printer.Print MonthName(Month(Date))
    Printer.CurrentX = Cx + md + 2.5
    Printer.CurrentY = Cy
    Printer.Print Year(Date)
    
    'Printer.Print Format(Sp_Fecha, "dd mmmm yyyy")
   ' Ip_Mes = Val(Mid(Sp_Fecha, 4, 2))
   ' Printer.Print Space(43) & Mid(Sp_Fecha, 1, 2) & Space(7) & UCase(MonthName(Ip_Mes))
   ' Printer.CurrentX = Cx + 6.7
   ' Printer.CurrentY = pos
   ' Printer.Print Space(49) & Mid(Sp_Fecha, 9, 2)  'A�O DE LA FECHA
   ' Printer.CurrentX = Cx + 0.6
   ' 'Printer.CurrentY = Printer.CurrentY + Dp_S
   ' Printer.CurrentY = Printer.CurrentY + Dp_S + 0.1
    
    
    
    
    
    
    'pos = Printer.CurrentY
    
    
     'Select datos cliente
    Sp_Sql = "SELECT nombre_rsocial,giro,direccion,comuna,ciudad " & _
                        "FROM maestro_clientes " & _
                        "WHERE rut_cliente='" & LvDetalle.SelectedItem.SubItems(3) & "'"
    Consulta Rp_Fac, Sp_Sql
    
    
    
    Cy = Printer.CurrentY
    Printer.CurrentX = Cx
    Printer.FontBold = True
    Printer.Print "RUT:"
    Printer.FontBold = False
    Printer.CurrentX = Cx + md
    Printer.CurrentY = Cy
    Printer.Print TxtRutCliente
    
    
    Cy = Printer.CurrentY
    Printer.CurrentX = Cx
    Printer.FontBold = True
    Printer.Print "R. Social:"
    Printer.FontBold = False
    Printer.CurrentX = Cx + md
    Printer.CurrentY = Cy
    Printer.Print Rp_Fac!nombre_rsocial
    
    
    
    Cy = Printer.CurrentY
    Printer.CurrentX = Cx
    Printer.FontBold = True
    Printer.Print "Giro.:"
    Printer.FontBold = False
    Printer.CurrentX = Cx + md
    Printer.CurrentY = Cy
    Printer.Print Rp_Fac!giro
        
    
    Cy = Printer.CurrentY
    Printer.CurrentX = Cx
    Printer.FontBold = True
    Printer.Print "Direcc.:"
    Printer.FontBold = False
    Printer.CurrentX = Cx + md
    Printer.CurrentY = Cy
    Printer.Print Rp_Fac!direccion
    
    Cy = Printer.CurrentY
    Printer.CurrentX = Cx
    Printer.FontBold = True
    Printer.Print "Comuna:"
    Printer.FontBold = False
    Printer.CurrentX = Cx + md
    Printer.CurrentY = Cy
    Printer.Print Rp_Fac!comuna
    
    

    
    Cy = Printer.CurrentY
    Printer.CurrentX = Cx
    Printer.FontBold = True
    Printer.Print "Ciudad:"
    Printer.FontBold = False
    Printer.CurrentX = Cx + md
    Printer.CurrentY = Cy
    Printer.Print Rp_Fac!ciudad
    
    
    Cy = Printer.CurrentY
    Printer.CurrentX = Cx
    Printer.FontBold = True
    Printer.Print "C. Pago:"
    Printer.FontBold = False
    Printer.CurrentX = Cx + md
    Printer.CurrentY = Cy
    Printer.Print Sm_Fpago
    
    
    Printer.CurrentY = Printer.CurrentY + 0.6
    
    
    Cy = Printer.CurrentY
    Printer.CurrentX = Cx
    Printer.FontBold = True
    Printer.Print "Cant   / Codigo / Descripcion        / Valor"
    Printer.FontBold = False
    
    
    Cy = Printer.CurrentY + 2
    
     Sql = "SELECT unidades,descripcion,precio_final,ved_codigo_interno " & _
            "FROM ven_detalle " & _
            "WHERE doc_id=" & Doc & " AND no_documento=" & Numero & " AND rut_emp='" & SP_Rut_Activo & "'"
    Consulta Rp_Fac, Sql
    If Rp_Fac.RecordCount > 0 Then
        Rp_Fac.MoveFirst
        Do While Not Rp_Fac.EOF
          '  For i = 1 To LvMateriales.ListItems.Count
                '6 - 3 - 7 - 8
                Printer.CurrentY = Printer.CurrentY + 0.11
                'Printer.CurrentX = Cx - 1.5
                
                Printer.CurrentX = Cx - 1
                
                
                'Printer.Print Right(Space(8) & LvMateriales.ListItems(i).SubItems(6), 8) & Space(7) & _
                        Left(LvMateriales.ListItems(i).SubItems(3) & Space(44), 44) & Space(3) & _
                        Right(Space(11) & NumFormat(LvMateriales.ListItems(i).SubItems(7)), 11) & Space(2) & _
                        Right(Space(11) & NumFormat(LvMateriales.ListItems(i).SubItems(8)), 11)
                RSet Sp_Cantidad = Rp_Fac!Unidades
                LSet Sp_Detalle = Rp_Fac!Descripcion
                LSet Sp_Codigo = Rp_Fac!ved_codigo_interno
                RSet Sp_Unitario = NumFormat(Rp_Fac!precio_final)
                RSet Sp_Total_linea = NumFormat(Round(Rp_Fac!precio_final * Rp_Fac!Unidades, 0))
                
                Printer.FontName = "Arial"
                Cy = Printer.CurrentY
                Printer.CurrentX = Cx - 0.3
                Printer.FontBold = True
                pos = Printer.CurrentY
                Printer.Print Sp_Cantidad & Space(2) & " x codigo:" & Sp_Codigo
                
                
                
                Printer.CurrentY = pos
                Printer.CurrentX = Cx + 4.1
                Printer.Print Sp_Unitario
                Printer.CurrentY = pos
                Printer.CurrentX = Cx + 5.4
                Printer.Print Sp_Total_linea
                
               
                Printer.FontBold = False
                Printer.CurrentX = Cx - 0.3
                Printer.CurrentY = Cy + 0.35
                pos = Printer.CurrentY
                Printer.Print Sp_Detalle
                
                
                
               ' Printer.Print Sp_Cantidad & Space(1) & Sp_Detalle & Space(1) & Sp_Codigo & Space(1) & Sp_Unitario & Space(1) & Sp_Total_linea
                 
                        
           ' Next
                Rp_Fac.MoveNext
         Loop
    End If
     Printer.FontSize = 8
    Printer.FontBold = True
    
    Printer.CurrentY = Printer.CurrentY + 0.7
     Printer.FontName = "Courier New"
     
     
     'Descunto
    If Val(LvDetalle.SelectedItem.SubItems(11)) + Val(LvDetalle.SelectedItem.SubItems(12)) - Val(LvDetalle.SelectedItem.SubItems(13)) > 0 Then
    
                     
            Printer.CurrentX = Cx + 2
            Printer.Print "Descuento $:" & " " & NumFormat(Val(LvDetalle.SelectedItem.SubItems(11)) + Val(LvDetalle.SelectedItem.SubItems(12)) - Val(LvDetalle.SelectedItem.SubItems(13)))
            Printer.CurrentY = Printer.CurrentY + 0.3
    
    
    End If
     
    
      'Totales
    Dp_Total = CDbl(TxtAPagar)
    Dp_Neto = Round(Dp_Total / Val("1." & DG_IVA), 0)
    Dp_Iva = Dp_Total - Dp_Neto
    
    Printer.FontBold = True
    
    RSet Sp_Neto = NumFormat(Dp_Neto)
    RSet Sp_IVA = NumFormat(Dp_Iva)
    RSet Sp_Total = NumFormat(Dp_Total)
    Printer.CurrentX = Cx + 1
    linini = Printer.CurrentY - 0.1
    Printer.Print Sp_Neto_P & " " & Sp_Neto   'SI ES GUIA NO IMPRIME NETO IVA TOTAL, SOLO NETO

        
    Printer.CurrentX = Cx + 1
    Printer.CurrentY = Printer.CurrentY + 0.2
    Printer.Print Sp_IVA_P & " " & Sp_IVA
    Printer.CurrentX = Cx + 1
    
    Printer.CurrentY = Printer.CurrentY + 0.2
    Printer.Print Sp_Total_P & " " & Sp_Total
    linfin = Printer.CurrentY + 0.1
    
    Printer.Line (Cx + 0.5, linini)-(6, linfin), , B 'Rectangulo Encabezado y N� Nota de Venta

    
    Printer.FontSize = 8
  
    Printer.CurrentY = Printer.CurrentY + 0.3
    pos = Printer.CurrentY
    Sp_Letras = UCase(BrutoAletras(CDbl(Dp_Total), True))
    mitad = Len(Sp_Letras) \ 2
    buscaespacio = InStr(mitad, Sp_Letras, " ")
    Printer.CurrentX = Cx
    If buscaespacio > 0 Then
            Printer.Print "Son: " & Mid(Sp_Letras, 1, buscaespacio)
            Printer.CurrentX = Cx + 1
            Printer.CurrentY = Printer.CurrentY + 0.1
            
            Printer.Print Mid(Sp_Letras, buscaespacio)
    Else
                
              Printer.Print "Son: " & Sp_Letras
    End If
    
    
    Printer.CurrentY = Printer.CurrentY + 0.3
     Printer.CurrentX = Cx
    
    Printer.PaintPicture LoadPicture(App.Path & "\timbre_pdf417.jpg"), Cx + 0.2, Printer.CurrentY
    Printer.CurrentY = Printer.CurrentY + 3.2
    Printer.CurrentX = Cx
    Printer.Print "Timbre Electronico S.I.I."
    Printer.CurrentX = Cx
    Printer.Print "Resolucion 80 del 22/08/2014"
    Printer.CurrentX = Cx
    Printer.Print "Verifique Documento: http://www.sii.cl"
    Printer.CurrentY = Printer.CurrentY + 0.3
    
    If Cedible Then
        Printer.FontSize = 10
        Printer.FontBold = True
        Printer.CurrentX = Cx
        Printer.Print "CEDIBLE"
        Printer.CurrentY = Printer.CurrentY + 0.2
        Printer.FontBold = False
    End If
    pos = Printer.CurrentY
    
    If firma Then
        Printer.FontName = "Arial"
        Coloca Cx, pos, "Nombre:", True, 9
        Coloca Cx, pos + 0.4, "RUT:", True, 9
        Coloca Cx, pos + 0.8, "Fecha:", True, 9
        Coloca Cx, pos + 1.2, "Recinto:", True, 9
        Coloca Cx, pos + 1.6, "Firma:", True, 9
        Coloca Cx, pos + 2, "El acuse de recibo que se declara en este acto, de  ", False, 8
        Coloca Cx, pos + 2.3, "acuerdo a lo dispuesto en la letra b) del Art 4�, y la", False, 8
        Coloca Cx, pos + 2.6, "letra c) del Art. 5� de la ley 19.983, acredita que la", False, 8
        Coloca Cx, pos + 2.9, "entrega de mercaderias o servicio(s) prestado(s) ", False, 8
        Coloca Cx, pos + 3.2, "ha(n) sido recibido(s)", False, 8
        
        Printer.Line (Cx - 0.3, pos - 0.2)-(7, pos + 3.6), , B 'Rectangulo Encabezado y N� Nota de Venta
        Printer.CurrentY = pos + 3.8
    
    End If
    
    
    
    Printer.FontName = "Arial"
    Printer.FontSize = "8"
    Printer.CurrentX = Cx + 0.5
    Printer.Print "Factura Electronica: ventas@alvamar.cl"
    
    Printer.CurrentY = Printer.CurrentY + 0.3
    'Printer.FontName = "Arial"
    'Printer.FontSize = "7"
    Printer.CurrentX = Cx + 0.5
    Printer.Print "Fue atendido por: " & LvDetalle.SelectedItem.SubItems(9)
    
    
    Printer.NewPage
    Printer.EndDoc
End Sub


Private Sub EstableImpresora(NombreImpresora As String)
   'Seteamos al impresora.
    Dim impresora As Printer
    ' Establece la impresora que se utilizar� para imprimir
    'Recorremos el objeto Printers
    For Each impresora In Printers
        'Si es igual al contenido se�alado en el combobox
        If UCase(impresora.DeviceName) = UCase(NombreImpresora) Then
            'Lo seteamos as�.
            Set Printer = impresora
            Exit For
        End If
    Next

End Sub
Private Sub EnviarDatosDePagoAlPos()
       'Enviar los datos al POS para emitir la boleta
       '  venPOS.FrmPago.Visible = True
        venPOS.TxtAPagar = Me.TxtAPagar
        venPOS.TxtSumaPagos = Me.TxtSumaPagos
        LG_Monto_Pagado_BF = CDbl(TxtSumaPagos)
        venPOS.TxtSaldoPago = Me.TxtSaldoPago
        venPOS.Refresh
        venPOS.LVFpago.ListItems.Clear
        
        
        
        
        If Val(TxtEfectivo) > 0 Then
            Sm_Fpago = "EFECTIVO"
            Im_Dias = 0
            TxtEfectivo = CDbl(TxtAPagar) - CDbl(TxtTC) - CDbl(TxtTD) - CDbl(TxtCh) - CDbl(TxtCredito)
        
            venPOS.LVFpago.ListItems.Add , , TxtEfectivo.Tag
            venPOS.LVFpago.ListItems(venPOS.LVFpago.ListItems.Count).SubItems(1) = Val(Me.TxtEfectivo)
            For i = 1 To LvPagos.ListItems.Count
                If LvPagos.ListItems(i) = TxtEfectivo.Tag Then
                    venPOS.LVFpago.ListItems(venPOS.LVFpago.ListItems.Count).SubItems(2) = LvPagos.ListItems(i).SubItems(2)
                     Exit For
                End If
            Next
        End If
        
        'If Val(TxtEfectivo) > 0 Then
        '    TxtEfectivo = CDbl(TxtAPagar) - CDbl(TxtTC) - CDbl(TxtTD) - CDbl(TxtCh) - CDbl(TxtCredito)
        '    venPOS.LVFpago.ListItems.Add , , TxtEfectivo.Tag
        '    venPOS.LVFpago.ListItems(venPOS.LVFpago.ListItems.Count).SubItems(1) = Val(Me.TxtEfectivo)
        'End If
        
        
      '  If Val(TxtTC) > 0 Then
      '      venPOS.LVFpago.ListItems.Add , , TxtTC.Tag
      '      venPOS.LVFpago.ListItems(venPOS.LVFpago.ListItems.Count).SubItems(1) = CDbl(TxtTC)
      '  End If
        
        If Val(TxtTC) > 0 Then
            Im_Dias = 0
            Sm_Fpago = "TARJETA CREDITO"
            venPOS.LVFpago.ListItems.Add , , TxtTC.Tag
            venPOS.LVFpago.ListItems(venPOS.LVFpago.ListItems.Count).SubItems(1) = CDbl(TxtTC)
            For i = 1 To LvPagos.ListItems.Count
                If LvPagos.ListItems(i) = TxtTC.Tag Then
                    venPOS.LVFpago.ListItems(venPOS.LVFpago.ListItems.Count).SubItems(2) = LvPagos.ListItems(i).SubItems(2)
                     Exit For
                End If
            Next
        End If
        
        
        
        
        
        
        'If Val(TxtTD) > 0 Then
        '    venPOS.LVFpago.ListItems.Add , , TxtTD.Tag
        '    venPOS.LVFpago.ListItems(venPOS.LVFpago.ListItems.Count).SubItems(1) = CDbl(TxtTD)
        'End If
        
        If Val(TxtTD) > 0 Then
            Im_Dias = 0
            Sm_Fpago = "TARJETA DEBITO"
            venPOS.LVFpago.ListItems.Add , , TxtTD.Tag
            venPOS.LVFpago.ListItems(venPOS.LVFpago.ListItems.Count).SubItems(1) = CDbl(TxtTD)
            For i = 1 To LvPagos.ListItems.Count
                If LvPagos.ListItems(i) = TxtTD.Tag Then
                    venPOS.LVFpago.ListItems(venPOS.LVFpago.ListItems.Count).SubItems(2) = LvPagos.ListItems(i).SubItems(2)
                     Exit For
                End If
            Next
        End If
 
        
        
        
        
        
        If Val(TxtDeposito) > 0 Then
            venPOS.LVFpago.ListItems.Add , , TxtDeposito.Tag
            venPOS.LVFpago.ListItems(venPOS.LVFpago.ListItems.Count).SubItems(1) = CDbl(TxtDeposito)
        
        End If
        
        
        If Val(TxtCh) > 0 Then
            venPOS.LvCheques.ListItems.Clear
            For i = 1 To LvCheques.ListItems.Count
                venPOS.LvCheques.ListItems.Add , , ""
                venPOS.LvCheques.ListItems(i).SubItems(1) = LvCheques.ListItems(i).SubItems(1)
                venPOS.LvCheques.ListItems(i).SubItems(2) = LvCheques.ListItems(i).SubItems(2)
                venPOS.LvCheques.ListItems(i).SubItems(3) = LvCheques.ListItems(i).SubItems(3)
                venPOS.LvCheques.ListItems(i).SubItems(4) = LvCheques.ListItems(i).SubItems(4)
                venPOS.LvCheques.ListItems(i).SubItems(5) = LvCheques.ListItems(i).SubItems(5)
                venPOS.LvCheques.ListItems(i).SubItems(6) = LvCheques.ListItems(i).SubItems(6)
                venPOS.LvCheques.ListItems(i).SubItems(8) = LvCheques.ListItems(i).SubItems(8)
            Next
            'venPOS.LVFpago.ListItems.Add , , TxtCh.Tag
            'venPOS.LVFpago.ListItems(venPOS.LVFpago.ListItems.Count).SubItems(1) = CDbl(TxtCh)
        End If
        
        
        If Val(TxtCh) > 0 Then
            Im_Dias = 1
            
            
            Ip_PlazoID = 1
            
            Sm_Fpago = "CHEQUE"
            venPOS.LVFpago.ListItems.Add , , TxtCh.Tag
            venPOS.LVFpago.ListItems(venPOS.LVFpago.ListItems.Count).SubItems(1) = CDbl(TxtCh)
            For i = 1 To LvPagos.ListItems.Count
                If LvPagos.ListItems(i) = TxtCh.Tag Then
                    venPOS.LVFpago.ListItems(venPOS.LVFpago.ListItems.Count).SubItems(2) = LvPagos.ListItems(i).SubItems(2)
                    Exit For
                End If
            Next
        End If
        
        
        
        
'        If Val(TxtCredito) > 0 Then
'            venPOS.LVFpago.ListItems.Add , , TxtCredito.Tag
'            venPOS.LVFpago.ListItems(venPOS.LVFpago.ListItems.Count).SubItems(1) = CDbl(TxtCredito)
'        End If


        If Val(TxtCredito) > 0 Then

            
            
            Ip_PlazoID = Right(CboPlazos.Text, 6)
            Im_Dias = CboPlazos.ItemData(CboPlazos.ListIndex)
            Sm_Fpago = "PENDIENTE"
            Sp_Condicion = "CREDITO"
            venPOS.LVFpago.ListItems.Add , , TxtCredito.Tag
            venPOS.LVFpago.ListItems(venPOS.LVFpago.ListItems.Count).SubItems(1) = CDbl(TxtCredito)
            Fp_FechaVencimiento = Date + CboPlazos.ItemData(CboPlazos.ListIndex)
        End If

       
        SG_Pago_Correcto = "SI"
        Me.Hide
        Unload Me
End Sub

Private Sub EmiteBoletaEpson()
                       Bm_BoletaEnProceso = True
                        CmdF5.Enabled = False
                        Frame1.Enabled = False
                        Me.Enabled = False
                        With vtaBoletaFiscalEpson.LvDetalle
                            .ListItems.Clear
'                            For d = 1 To Me.LvVenta.ListItems.Count
'                                .ListItems.Add , , LvVenta.ListItems(d).SubItems(3)
'                                .ListItems(d).SubItems(1) = LvVenta.ListItems(d).SubItems(2)
'                                .ListItems(d).SubItems(2) = CDbl(LvVenta.ListItems(d).SubItems(4))
'                                .ListItems(d).SubItems(3) = CDbl(LvVenta.ListItems(d).SubItems(1))
'
'
'                            Next
                            vtaBoletaFiscalEpson.TxtTotal = TxtTotal
                            vtaBoletaFiscalEpson.EmiteBoleta
                            
                    
                        End With
                        Bm_BoletaEnProceso = False
                        CmdF5.Enabled = True
                        Frame1.Enabled = True
                        Me.Enabled = True
End Sub
Private Sub EmiteBoletaSamsung()
                        Dim Sp_IdPagos As String
                        CmdF5.Enabled = False
                        'Sql = "SELECT unidades,descripcion,precio_final,ved_codigo_interno " & _
                                "FROM ven_detalle " & _
                                "WHERE doc_id=" & LvDetalle.SelectedItem.SubItems(8) & " AND no_documento=" & LvDetalle.SelectedItem.SubItems(1) & " AND rut_emp='" & SP_Rut_Activo & "'"
                                
                                
                        Sql = "SELECT nvd_cantidad unidades,nvd_descripcion descripcion,nvd_precio_unitario precio_final,pro_codigo_interno ved_codigo_interno " & _
                                "FROM ven_nota_venta_detalle " & _
                                "WHERE nve_id=" & LvDetalle.SelectedItem
                        Consulta RsTmp, Sql
                                
                        RsTmp.MoveFirst
                        
                        With vtaBoletaFiscalSamsumg
                            .TxtVendedor = LvDetalle.SelectedItem.SubItems(9)
                        
                            Do While Not RsTmp.EOF
                            'For d = 1 To LvMateriales.ListItems.Count
                                .LvDetalle.ListItems.Add , , CxP(RsTmp!Unidades)
                                .LvDetalle.ListItems(.LvDetalle.ListItems.Count).SubItems(1) = "COD:" & RsTmp!ved_codigo_interno & " - " & RsTmp!Descripcion
                                .LvDetalle.ListItems(.LvDetalle.ListItems.Count).SubItems(2) = RsTmp!precio_final
                            'Next
                                RsTmp.MoveNext
                            Loop
                            
                            .LvDescuentos.ListItems.Clear
                            If Val(Me.LvDetalle.SelectedItem.SubItems(11)) > 0 Then
                                .LvDescuentos.ListItems.Add , , ""
                                .LvDescuentos.ListItems(.LvDescuentos.ListItems.Count).SubItems(1) = "DESCUENTO"
                                .LvDescuentos.ListItems(.LvDescuentos.ListItems.Count).SubItems(2) = LvDetalle.SelectedItem.SubItems(11)
                            End If
                            If Val(Me.LvDetalle.SelectedItem.SubItems(12)) > 0 Then
                                .LvDescuentos.ListItems.Add , , ""
                                .LvDescuentos.ListItems(.LvDescuentos.ListItems.Count).SubItems(1) = "DESC. ADIC."
                                .LvDescuentos.ListItems(.LvDescuentos.ListItems.Count).SubItems(2) = LvDetalle.SelectedItem.SubItems(12)
                            End If
                            If Val(Me.LvDetalle.SelectedItem.SubItems(13)) > 0 Then
                                .LvDescuentos.ListItems.Add , , ""
                                .LvDescuentos.ListItems(.LvDescuentos.ListItems.Count).SubItems(1) = "AJUSTE SIMPLE"
                                .LvDescuentos.ListItems(.LvDescuentos.ListItems.Count).SubItems(2) = LvDetalle.SelectedItem.SubItems(13)
                            End If
                            
                            .LvPagos.ListItems.Clear
                           For i = 1 To Me.LVFpago.ListItems.Count
                                .LvPagos.ListItems.Add , , LVFpago.ListItems(i).SubItems(2)
                                If Val(LVFpago.ListItems(i).SubItems(2)) = 0 Then
                                     .LvPagos.ListItems(.LvPagos.ListItems.Count).SubItems(2) = TxtEfectivoReal
                                     
                                Else
                                
                                    .LvPagos.ListItems(.LvPagos.ListItems.Count).SubItems(2) = CDbl(LVFpago.ListItems(i).SubItems(1))
                                End If
                           Next
                        
                             .TxtRutCliente = Me.TxtRutCliente
                             .TxtNombreCliente = Me.TxtNombreCliente
                                                       
                           If VenPosCaja.LvCuotas.ListItems.Count > 0 Then
                           

                                For G = 1 To VenPosCaja.LvCuotas.ListItems.Count
                                    .LvCuotas.ListItems.Add , , VenPosCaja.LvCuotas.ListItems(G)
                                    .LvCuotas.ListItems(G).SubItems(1) = VenPosCaja.LvCuotas.ListItems(G).SubItems(1)
                                    .LvCuotas.ListItems(G).SubItems(2) = VenPosCaja.LvCuotas.ListItems(G).SubItems(2)
                                Next
                           End If
                           
                           
                            
                            .TxtTotal = Me.TxtAPagar
                            .EmiteBoleta
                            VenPosCaja.LvCuotas.ListItems.Clear
                        End With
End Sub
Private Sub EmitirFacturaManual()
                Dim Lp_Folio As Long
                Dim Sp_Condicion As String
                Dim Dp_FechaFac As Date
             '   FrmDte.Visible = True
               ' Sql = "SELECT IFNULL(MAX(no_documento),0) +1 numero " & _
                        "FROM ven_doc_venta " & _
                        "WHERE doc_id=" & LvDetalle.SelectedItem.SubItems(8) & " AND rut_emp='" & SP_Rut_Activo & "'"
                        
                Lp_Folio = AutoIncremento("LEE", LvDetalle.SelectedItem.SubItems(8), , IG_id_Sucursal_Empresa)
            
            
                Sql = "SELECT id " & _
                        "FROM ven_doc_venta " & _
                        "WHERE rut_emp='" & SP_Rut_Activo & "' AND doc_id=" & LvDetalle.SelectedItem.SubItems(8) & " AND no_documento=" & Lp_Folio
                Consulta RsTmp, Sql
                If RsTmp.RecordCount > 0 Then
                     Sql = "SELECT IFNULL(MAX(no_documento),0) +1 numero " & _
                        "FROM ven_doc_venta " & _
                        "WHERE doc_id=" & LvDetalle.SelectedItem.SubItems(8) & " AND rut_emp='" & SP_Rut_Activo & "'"
                    Consulta RsTmp, Sql
                    If RsTmp.RecordCount > 0 Then
                        Lp_Folio = RsTmp!Numero
                        
                    End If
                End If
                Lp_Nro_Documento = Lp_Folio
                DoEvents

                        
                
               ' Consulta RsTmp, Sql
            '    dte_id = RsTmp!dte_id
              '  Lp_Folio = RsTmp!Numero

               Dp_FechaFac = Date
                If SP_Rut_Activo = "78.967.170-2" Or SP_Rut_Activo = "76.169.962-8" Then
                    Dp_FechaFac = DtFechaFac
                    Lp_Folio = Me.TxtNextFactura
                    Lp_Nro_Documento = Lp_Folio
                End If
                
               ' FacturaElectronicaPOS 33, Lp_Folio, LvDetalle.SelectedItem.SubItems(8)
                If ConvertirADocVenta(Lp_Folio, Dp_FechaFac, Im_Dias, Sm_Fpago, Sp_Condicion, "1") Then
                
                        'cn.Execute "UPDATE dte_folios SET dte_disponible='NO' " & _
                                    "WHERE dte_id=" & dte_id
                                    
                                
                        cn.Execute "UPDATE ven_nota_venta SET nve_terminada='SI',id=" & Id_Ventas_Unico & " " & _
                                    "WHERE nve_id=" & LvDetalle.SelectedItem
                        
                        AutoIncremento "GUARDA", LvDetalle.SelectedItem.SubItems(8), Lp_Folio, IG_id_Sucursal_Empresa
                        
                        ImprimeFacturaAlcalde
                        
                        If Sm_Fpago = "TARJETA CREDITO" Or Sm_Fpago = "TARJETA DEBITO" Then
                            LP_FOLIOX = AutoIncremento("LEE", 40, , IG_id_Sucursal_Empresa)
                            AutoIncremento "GUARDA", 40, LP_FOLIOX, IG_id_Sucursal_Empresa
                        End If
                End If
                FrmDte.Visible = False
                
                
                MsgBox "Documento emitido...", vbInformation
                
                
    Exit Sub
RolBak:
  '  cn.RollbackTrans
 '   MsgBox "No fue posible completar el documento..."
    
End Sub


Private Sub LimpiaCheques()
    TxtRutCheque = ""
    TxtNroCheque = ""
    TxtPlaza = ""
    TxtMontoCheque = ""
    LvCheques.ListItems.Clear
    TxtTotalCheques = "0"



End Sub
Private Sub ImprimeBoletaKyR()
    Dim Cx As Double, Cy As Double, Sp_Fecha As String, Ip_Mes As Integer, Dp_S As Double, Sp_Letras As String
    Dim Sp_Neto As String * 12, Sp_IVA As String * 12, Sp_Total As String * 12
    Dim Sp_GuiasFacturadas As String
    
    Dim p_Codigo As String * 6
    Dim p_Cantidad As String * 7
    Dim p_UM As String * 4
    Dim p_Detalle As String * 40
    Dim p_Unitario As String * 10
    Dim p_Total As String * 11
    Dim p_Mes As String * 10
    Dim p_CiudadF As String * 20
    
    Dim Sp_Dia As String * 2
    Dim Sp_Mes As String * 10
    Dim Sp_Ano As String * 4
    
    On Error GoTo ProblemaImpresora
    Printer.FontName = "Courier New"
    Printer.FontSize = 10
    Printer.ScaleMode = 7
        
  'Aqui leemos el X y el Y iniciales de la tabla de sucursales
    Sql = "SELECT sue_cx,sue_cy " & _
            "FROM sis_empresas_sucursales " & _
            "WHERE sue_id=" & IG_id_Sucursal_Empresa
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        Cx = Val(RsTmp!sue_cx)
        Cy = Val(RsTmp!sue_cy)  'vertical)
    Else
        Cx = 1.7 'horizontal
        Cy = 1.8 'vertical
    
    End If
    Dp_S = 0.1
    SkImpresoraEnUso = Printer.DeviceName
    Printer.CurrentX = Cx
    Printer.CurrentY = Cy
    
    Sp_Dia = Right("00" & Day(Date), 2)
    Sp_Mes = UCase(MonthName(Month(Date)))
    Sp_Ano = Year(Date)
    'Sp_Fecha = Format(DtFecha.Value, "DD-MM-YYYY")
    'Ip_Mes = Val(Mid(Sp_Fecha, 4, 2))
    
    'Printer.CurrentY = Printer.CurrentY + 1.3
    Printer.CurrentX = Cx + 13.5
 '   p_Mes = UCase(MonthName(Ip_Mes))
    pos = Printer.CurrentY
    Printer.Print Sp_Dia & Space(18) & Sp_Ano
    Printer.CurrentY = pos
    Printer.CurrentX = Cx + 15
    Printer.Print Sp_Mes
    Printer.CurrentY = Printer.CurrentY + 0.8 + 1
    For i = 1 To LvProductos.ListItems.Count
        '6 - 3 - 7 - 8
        Printer.CurrentX = Printer.CurrentX + 0.2 'INTERLINIA ARTICULOS
       ' Printer.CurrentX = Cx - 2
        Printer.CurrentX = Cx - 2
        p_Codigo = LvProductos.ListItems(i).SubItems(1)
        
        If SP_Rut_Activo = "76.553.302-3" Then
            RSet p_Cantidad = Round(CDbl(LvProductos.ListItems(i).SubItems(3)), 0) 'Cantidad sin decimales para KyR
        Else
            RSet p_Cantidad = Trim(LvProductos.ListItems(i).SubItems(3))
            
        End If
        
       
       p_UM = " " 'LvMateriales.ListItems(i).SubItems(21)
        p_Detalle = LvProductos.ListItems(i).SubItems(2)
        RSet p_Unitario = LvProductos.ListItems(i).SubItems(4)
        RSet p_Total = NumFormat(LvProductos.ListItems(i).SubItems(3) * LvProductos.ListItems(i).SubItems(4))
        Printer.Print "   " & p_Cantidad & "    " & p_Codigo & "   " & p_Detalle & " " & p_Unitario & "   " & p_Total
    Next
    Printer.CurrentX = Cx - 1
    Printer.CurrentY = Cy + 10.2
    pos = Printer.CurrentY
    Printer.CurrentX = Cx + 1
    Printer.CurrentY = pos - 2.2
    Printer.CurrentX = Cx + 1
    RSet Sp_Total = LvDetalle.SelectedItem.SubItems(6)
    pos = pos - 1.6
    'pos = pos - 1.4
    Printer.CurrentY = pos - 0.5
    'Descuento en boleta impresa
    If Val(LvDetalle.SelectedItem.SubItems(11)) + Val(LvDetalle.SelectedItem.SubItems(12)) - Val(LvDetalle.SelectedItem.SubItems(13)) > 0 Then
            Printer.CurrentY = 11.4
            Printer.CurrentX = Cx + 10.9
            Printer.Print "DESCUENTO"
            Printer.CurrentY = 11.4
            Printer.CurrentX = Cx + 13.9
            Printer.Print NumFormat(Val(LvDetalle.SelectedItem.SubItems(11)) + Val(LvDetalle.SelectedItem.SubItems(12)) - Val(LvDetalle.SelectedItem.SubItems(13)))
    End If
    Printer.CurrentY = 12.2
    Printer.CurrentX = Cx + 13.9
    
    
    Printer.CurrentY = 12.2
    Printer.CurrentX = Cx + 13.9
    Printer.FontBold = True
    Printer.Print Sp_Total
    
    Printer.CurrentY = Printer.CurrentY + 1
    
    Printer.NewPage
    Printer.EndDoc
    Exit Sub
ProblemaImpresora:
    MsgBox "Ocurrio un error al intentar imprimir..." & vbNewLine & Err.Description & vbNewLine & Err.Number

End Sub

Private Sub ImprimeFacturaAlcalde()
    Dim Cx As Double, Cy As Double, Sp_Fecha As String, Ip_Mes As Integer, Dp_S As Double, Sp_Letras As String
    Dim Sp_Neto As String * 12, Sp_IVA As String * 12, Sp_Total As String * 12
    Dim Sp_GuiasFacturadas As String
    
    Dim p_Codigo As String * 6
    Dim p_Cantidad As String * 7
    Dim p_UM As String * 4
    Dim p_Detalle As String * 40
    Dim p_Unitario As String * 10
    Dim p_Total As String * 11
    Dim Rp_Fac As Recordset
    
    On Error GoTo ProblemaImpresora
    Printer.FontName = "Courier New"
    Printer.FontSize = 10
    Printer.ScaleMode = 7
       
    Sql = "SELECT sue_cx,sue_cy " & _
            "FROM sis_empresas_sucursales " & _
            "WHERE sue_id=" & IG_id_Sucursal_Empresa
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        Cx = Val(RsTmp!sue_cx) - 0.3
        Cy = Val(RsTmp!sue_cy) + 2.1 'vertical)
    Else
        Cx = 1.1 'horizontal
        Cy = 2.9 'vertical
    End If
    Dp_S = 0.2
    Printer.CurrentX = Cx
    Printer.CurrentY = Cy
    Sp_Fecha = Date
    Ip_Mes = Val(Mid(Sp_Fecha, 4, 2))
    
    'FECHA
    Printer.CurrentY = Cy + 2.5
    Printer.CurrentX = Cx + 1.2
    pos = Printer.CurrentY
    Printer.Print "       " & Mid(Sp_Fecha, 1, 2) & Space(4) & UCase(MonthName(Val(Mid(Sp_Fecha, 4, 2))))
    Printer.CurrentY = pos
    Printer.CurrentX = Cx + 9
    Printer.Print Mid(Sp_Fecha, 7, 4)
    Dim Sp_Sql As String
    
         'Select datos cliente
    Sp_Sql = "SELECT nombre_rsocial,giro,direccion,comuna,ciudad " & _
                        "FROM maestro_clientes " & _
                        "WHERE rut_cliente='" & LvDetalle.SelectedItem.SubItems(3) & "'"
    Consulta Rp_Fac, Sp_Sql
    
    'RUT
    Printer.CurrentX = Cx + 16
    Printer.CurrentY = pos
    pos = Printer.CurrentY
    Printer.Print LvDetalle.SelectedItem.SubItems(3)

    'RAZON SOCIAL
    Printer.CurrentX = Cx + 2
    Printer.CurrentY = Cy + 3
    pos = Printer.CurrentY
    Printer.Print Rp_Fac!nombre_rsocial
    
    'CIUDAD
    Printer.CurrentY = pos
    Printer.CurrentX = Cx + 16
    Printer.Print Rp_Fac!ciudad
   
    'DIRECCION
    Printer.CurrentX = Cx + 2
    Printer.CurrentY = Cy + 3.4
    pos = Printer.CurrentY
    posd = Printer.CurrentY
    Printer.Print Rp_Fac!direccion
    
    'TELEFONO
   ' Printer.CurrentY = pos
   ' Printer.CurrentX = Cx + 16
   ' Printer.Print Me.LbTelefono
    
    'GIRO
    Printer.CurrentY = Cy + 3.8
    Printer.CurrentX = Cx + 2
    pos = Printer.CurrentY
    Printer.Print Rp_Fac!giro  'CboGiros.Text ' MARIO ****   TxtGiro
   
    'COOMUNA
    Printer.CurrentY = pos
    Printer.CurrentX = Cx + 11
    Printer.Print Rp_Fac!comuna   'txtComuna
    
    'CONDICION DE VENTA
    Printer.CurrentY = pos
    Printer.CurrentX = Cx + 16.6
    'Printer.Print Mid(CboPlazos.Text, 1, 20)
    
    Printer.CurrentY = Printer.CurrentY + Dp_S - 0.3
    '***
    'Printer.CurrentY = Printer.CurrentY + Dp_S
    pos = Printer.CurrentY
    
    Printer.CurrentX = Cx + 8.5
    Printer.CurrentY = Printer.CurrentY + 1
    
   
    Printer.CurrentY = Printer.CurrentY + 0.4
    
    
    
    
   '   For i = 1 To LvProductos.ListItems.Count
   '     '6 - 3 - 7 - 8
   '     Printer.CurrentX = Printer.CurrentX + 0.2 'INTERLINIA ARTICULOS
   '    ' Printer.CurrentX = Cx - 2
   '     Printer.CurrentX = Cx - 2
   '     p_Codigo = LvProductos.ListItems(i).SubItems(1)
   '     RSet p_Cantidad = Trim(LvProductos.ListItems(i).SubItems(3))
   '    p_UM = " " 'LvMateriales.ListItems(i).SubItems(21)
    '    p_Detalle = LvProductos.ListItems(i).SubItems(2)
  '      RSet p_Unitario = LvProductos.ListItems(i).SubItems(4)
  '      RSet p_Total = NumFormat(LvProductos.ListItems(i).SubItems(3) * LvProductos.ListItems(i).SubItems(4))
  '      Printer.Print "   " & p_Cantidad & "    " & p_Codigo & "   " & p_Detalle & " " & p_Unitario & "   " & p_Total
  '  Next
    
    For i = 1 To LvProductos.ListItems.Count
        '6 - 3 - 7 - 8
        Printer.CurrentX = Cx - 0.5
        
        p_Codigo = LvProductos.ListItems(i).SubItems(1)
        RSet p_Cantidad = LvProductos.ListItems(i).SubItems(3)
        p_UM = "" 'LvProductos.ListItems(i).SubItems(21)
        p_Detalle = LvProductos.ListItems(i).SubItems(2)
        RSet p_Unitario = LvProductos.ListItems(i).SubItems(4)
        RSet p_Total = NumFormat(LvProductos.ListItems(i).SubItems(3) * LvProductos.ListItems(i).SubItems(4))
        
        Printer.Print p_Cantidad & "    " & p_Codigo & "      " & p_Detalle & "       " & p_Unitario & " " & p_Total
       
    Next
    
    
        
  '  If Bm_Factura_por_Guias Then LvMateriales.ListItems.Clear
    
    Printer.CurrentX = Cx - 1
    Printer.CurrentY = Cy + 14.2
    pos = Printer.CurrentY
        
    Printer.CurrentX = Cx + 1
    Printer.CurrentY = pos - 2.2
    Printer.Print "" ' TxtComentario
    
    Printer.CurrentX = Cx + 1
        
    Printer.CurrentY = pos - 1.4
    
    'Totales
    Dp_Total = CDbl(TxtAPagar)
    Dp_Neto = Round(Dp_Total / Val("1." & DG_IVA), 0)
    Dp_Iva = Dp_Total - Dp_Neto
    
    RSet Sp_Neto = NumFormat(Dp_Neto)
    RSet Sp_IVA = NumFormat(Dp_Iva)
    RSet Sp_Total = TxtAPagar
    pos = pos - 1.4
    
         'Descunto
    If Val(LvDetalle.SelectedItem.SubItems(11)) + Val(LvDetalle.SelectedItem.SubItems(12)) - Val(LvDetalle.SelectedItem.SubItems(13)) > 0 Then
    
                     
            Printer.CurrentX = Cx + 14
            Printer.CurrentY = Cy + 10.1
            Printer.Print "Descuento $:" & " " & NumFormat(Val(LvDetalle.SelectedItem.SubItems(11)) + Val(LvDetalle.SelectedItem.SubItems(12)) - Val(LvDetalle.SelectedItem.SubItems(13)))
           
    
    
    End If
    
    
    
    Printer.CurrentX = Cx + 9.5
    Printer.CurrentY = Cy + 10.8
    Printer.Print Sp_Neto
    
    Printer.CurrentX = Cx + 13
    Printer.CurrentY = Cy + 10.8
    Printer.Print Sp_IVA
    
    Printer.CurrentX = Cx + 16
    Printer.CurrentY = Cy + 10.8
    Printer.Print Sp_Total
    
    Printer.CurrentX = Cx + 1
    Printer.CurrentY = Cy + 11.4
    Sp_Letras = UCase(BrutoAletras(CDbl(TxtTotal), True))
    Printer.Print Sp_Letras
    Printer.NewPage
    Printer.EndDoc
    Exit Sub
ProblemaImpresora:
    MsgBox "Ocurrio un error al intentar imprimir..." & vbNewLine & Err.Description & vbNewLine & Err.Number

End Sub
Private Function La_Establecer_Impresora(ByVal NamePrinter As String) As Boolean
On Error GoTo errSub
      'Variable de referencia
  
    Dim obj_Impresora As Object
    'Creamos la referencia
    Set obj_Impresora = CreateObject("WScript.Network")
        obj_Impresora.setdefaultprinter NamePrinter
    Set obj_Impresora = Nothing
  
        'La funci�n devuelve true y se cambi� con �xito
  
       La_Establecer_Impresora = True
    Exit Function
'Error al cambiar la impresora
errSub:
If Err.Number = 0 Then Exit Function
   La_Establecer_Impresora = False
   MsgBox "error: " & Err.Number & Chr(13) & "Description: " & Err.Description
   On Error GoTo 0
End Function



Private Sub ObtenerProxNumeros()
    DoEvents
    FrmLoad.Visible = True
    If SP_Rut_Activo = "76.553.302-3" Or SP_Rut_Activo = "76.169.962-8" Or SP_Rut_Activo = "78.967.170-2" Or SP_Rut_Activo = "76.354.346-3" Or SP_Rut_Activo = "76.239.518-5" Then
        'Boleta
        Sql = "SELECT IFNULL(MAX(no_documento),0) +1 numero " & _
                    "FROM ven_doc_venta " & _
                    "WHERE doc_id=" & 2 & " AND rut_emp='" & SP_Rut_Activo & "'"
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
            TxtNextBoleta = RsTmp!Numero
            
        
        Else
            TxtNextBoleta = 1
        End If
        
           'factura
        Sql = "SELECT IFNULL(MAX(no_documento),0) +1 numero " & _
                    "FROM ven_doc_venta " & _
                    "WHERE doc_id=" & 1 & " AND rut_emp='" & SP_Rut_Activo & "'"
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
            TxtNextFactura = RsTmp!Numero
            
        
        Else
            TxtNextFactura = 1
        End If
        
        
        Dim Lp_Nvoucher As Long
        Lp_Nvoucher = AutoIncremento("LEE", 40, , IG_id_Sucursal_Empresa)
           'voucher transbank
        TxtNextVoucher = Lp_Nvoucher
        ' Comprobar si ya ha sido utilizado.
        
        Sql = "SELECT id " & _
                "FROM ven_doc_venta " & _
                "WHERE doc_id=40 AND no_documento=" & Lp_Nvoucher
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
                    Sql = "SELECT IFNULL(MAX(no_documento),0) +1 numero " & _
                                "FROM ven_doc_venta " & _
                                "WHERE doc_id=" & 40 & " AND rut_emp='" & SP_Rut_Activo & "'"
                    Consulta RsTmp, Sql
                    If RsTmp.RecordCount > 0 Then
                        TxtNextVoucher = RsTmp!Numero
                        
                    
                    Else
                        TxtNextVoucher = 1
                    End If
        End If
        
        
        
        
        
    End If
    FrmLoad.Visible = False
End Sub
