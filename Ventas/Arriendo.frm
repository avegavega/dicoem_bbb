VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{28D47522-CF84-11D1-834C-00A0249F0C28}#1.0#0"; "gif89.dll"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{DA729E34-689F-49EA-A856-B57046630B73}#1.0#0"; "progressbar-xp.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form Arriendo 
   Caption         =   "Consulta de Arriendo / Devolucion de maquinarias"
   ClientHeight    =   10440
   ClientLeft      =   3660
   ClientTop       =   2760
   ClientWidth     =   16395
   LinkTopic       =   "Form1"
   ScaleHeight     =   10440
   ScaleWidth      =   16395
   Begin VB.Timer Timer1 
      Interval        =   1000
      Left            =   16140
      Top             =   8970
   End
   Begin VB.Frame FrmLoad 
      Height          =   1950
      Left            =   4905
      TabIndex        =   52
      Top             =   4410
      Visible         =   0   'False
      Width           =   2805
      Begin GIF89LibCtl.Gif89a Gif89a1 
         Height          =   1425
         Left            =   210
         OleObjectBlob   =   "Arriendo.frx":0000
         TabIndex        =   53
         Top             =   285
         Width           =   2445
      End
   End
   Begin MSComDlg.CommonDialog Dialogo 
      Left            =   15
      Top             =   6735
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.CommandButton CmdAnularContrato 
      Caption         =   "Anular Contrato"
      Height          =   420
      Left            =   7755
      TabIndex        =   35
      Top             =   4575
      Width           =   1425
   End
   Begin VB.TextBox TxtEstado 
      Height          =   300
      Left            =   2000
      TabIndex        =   9
      Top             =   5130
      Width           =   5200
   End
   Begin VB.TextBox TxtFechaContrato 
      Enabled         =   0   'False
      Height          =   300
      Left            =   2000
      TabIndex        =   8
      Top             =   4815
      Width           =   5200
   End
   Begin VB.TextBox TxtContrato 
      Enabled         =   0   'False
      Height          =   300
      Left            =   2000
      TabIndex        =   7
      Top             =   4155
      Width           =   5200
   End
   Begin VB.Frame Frame1 
      Caption         =   "Arriendo"
      Height          =   10200
      Left            =   120
      TabIndex        =   0
      Top             =   60
      Width           =   15975
      Begin VB.CommandButton CmdLimpiar 
         Caption         =   "Limpiar"
         Height          =   465
         Left            =   8670
         TabIndex        =   28
         Top             =   1320
         Width           =   1470
      End
      Begin VB.CommandButton CmdExportar 
         Caption         =   "Exportar a Excel"
         Height          =   465
         Left            =   7065
         TabIndex        =   22
         Top             =   1320
         Width           =   1395
      End
      Begin VB.Frame Opciones 
         Caption         =   "Opciones"
         Height          =   765
         Left            =   6720
         TabIndex        =   29
         Top             =   1080
         Width           =   9090
         Begin VB.CommandButton CmdRetornar 
            Caption         =   "Retornar"
            Height          =   465
            Left            =   3600
            TabIndex        =   36
            Top             =   240
            Width           =   1335
         End
      End
      Begin VB.CommandButton Command1 
         Caption         =   "Rut"
         Height          =   465
         Left            =   4695
         TabIndex        =   30
         Top             =   465
         Width           =   1290
      End
      Begin VB.Frame Frame4 
         Caption         =   "Filtrar por Fechas"
         Height          =   930
         Left            =   6720
         TabIndex        =   14
         Top             =   165
         Width           =   9075
         Begin VB.OptionButton Option2 
            Caption         =   "Seleccionar fechas"
            Height          =   240
            Left            =   495
            TabIndex        =   16
            Top             =   555
            Width           =   1725
         End
         Begin VB.OptionButton Option1 
            Caption         =   "Todas las fechas"
            Height          =   240
            Left            =   495
            TabIndex        =   15
            Top             =   255
            Value           =   -1  'True
            Width           =   1740
         End
         Begin ACTIVESKINLibCtl.SkinLabel skIni 
            Height          =   240
            Left            =   2580
            OleObjectBlob   =   "Arriendo.frx":0086
            TabIndex        =   17
            Top             =   570
            Width           =   795
         End
         Begin MSComCtl2.DTPicker DTInicio 
            Height          =   300
            Left            =   3495
            TabIndex        =   18
            Top             =   465
            Width           =   1305
            _ExtentX        =   2302
            _ExtentY        =   529
            _Version        =   393216
            Enabled         =   0   'False
            Format          =   95289345
            CurrentDate     =   42747
         End
         Begin MSComCtl2.DTPicker DtHasta 
            Height          =   285
            Left            =   5520
            TabIndex        =   19
            Top             =   465
            Width           =   1305
            _ExtentX        =   2302
            _ExtentY        =   503
            _Version        =   393216
            Enabled         =   0   'False
            Format          =   95289345
            CurrentDate     =   42747
         End
         Begin ACTIVESKINLibCtl.SkinLabel skFin 
            Height          =   240
            Left            =   4845
            OleObjectBlob   =   "Arriendo.frx":00EE
            TabIndex        =   20
            Top             =   555
            Width           =   570
         End
      End
      Begin VB.Frame Frame3 
         Caption         =   "Listado de Productos en Contrato Seleccionado"
         Height          =   4530
         Left            =   120
         TabIndex        =   4
         Top             =   5520
         Width           =   15750
         Begin VB.CommandButton Command3 
            Caption         =   "Fecha"
            Height          =   225
            Left            =   11145
            TabIndex        =   56
            Top             =   2085
            Width           =   1140
         End
         Begin VB.CommandButton CmdCambiaHora 
            Caption         =   "Hora"
            Height          =   225
            Left            =   12480
            TabIndex        =   55
            Top             =   2070
            Width           =   1140
         End
         Begin VB.CommandButton CmdAnulaRecepcion 
            Caption         =   "Anula Recepcion"
            Height          =   240
            Left            =   13860
            TabIndex        =   54
            Top             =   2055
            Width           =   1620
         End
         Begin VB.Frame Frame6 
            Caption         =   "Observaciones"
            Height          =   1770
            Left            =   6285
            TabIndex        =   40
            Top             =   2685
            Width           =   9240
            Begin VB.TextBox TxtRecNroRecep 
               Height          =   300
               Left            =   6735
               TabIndex        =   47
               Text            =   "0"
               Top             =   255
               Width           =   1485
            End
            Begin VB.CommandButton CmdRecOk 
               Caption         =   "Ok"
               Height          =   300
               Left            =   8235
               TabIndex        =   46
               Top             =   270
               Width           =   450
            End
            Begin VB.ComboBox CboRecUtilizable 
               Height          =   315
               ItemData        =   "Arriendo.frx":0156
               Left            =   5565
               List            =   "Arriendo.frx":0160
               Style           =   2  'Dropdown List
               TabIndex        =   45
               Top             =   255
               Width           =   1185
            End
            Begin MSComCtl2.DTPicker DtRecHora 
               Height          =   300
               Left            =   4380
               TabIndex        =   44
               Top             =   255
               Width           =   1170
               _ExtentX        =   2064
               _ExtentY        =   529
               _Version        =   393216
               Format          =   95289346
               CurrentDate     =   43103
            End
            Begin MSComCtl2.DTPicker DtRecFecha 
               Height          =   300
               Left            =   3195
               TabIndex        =   43
               Top             =   255
               Width           =   1170
               _ExtentX        =   2064
               _ExtentY        =   529
               _Version        =   393216
               Format          =   95289345
               CurrentDate     =   43103
            End
            Begin VB.TextBox TxtRecObs 
               Height          =   315
               Left            =   300
               TabIndex        =   42
               Top             =   255
               Width           =   2895
            End
            Begin MSComctlLib.ListView LvRecepciones 
               Height          =   1140
               Left            =   285
               TabIndex        =   41
               Top             =   555
               Width           =   8610
               _ExtentX        =   15187
               _ExtentY        =   2011
               View            =   3
               LabelWrap       =   0   'False
               HideSelection   =   0   'False
               FullRowSelect   =   -1  'True
               GridLines       =   -1  'True
               _Version        =   393217
               ForeColor       =   -2147483640
               BackColor       =   -2147483643
               BorderStyle     =   1
               Appearance      =   1
               NumItems        =   6
               BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                  Object.Tag             =   "N109"
                  Text            =   "Id Rec"
                  Object.Width           =   0
               EndProperty
               BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                  SubItemIndex    =   1
                  Object.Tag             =   "T1000"
                  Text            =   "Observacion"
                  Object.Width           =   5106
               EndProperty
               BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                  SubItemIndex    =   2
                  Text            =   "Fecha"
                  Object.Width           =   2064
               EndProperty
               BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                  SubItemIndex    =   3
                  Text            =   "Hora"
                  Object.Width           =   2064
               EndProperty
               BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                  SubItemIndex    =   4
                  Text            =   "Utilizable"
                  Object.Width           =   2090
               EndProperty
               BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                  SubItemIndex    =   5
                  Object.Tag             =   "N109"
                  Text            =   "Nro Recepcion"
                  Object.Width           =   2540
               EndProperty
            End
         End
         Begin VB.TextBox TxtNombreReceptor 
            Height          =   315
            Left            =   1725
            TabIndex        =   59
            Top             =   3060
            Width           =   4050
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
            Height          =   255
            Left            =   300
            OleObjectBlob   =   "Arriendo.frx":016C
            TabIndex        =   33
            Top             =   3090
            Width           =   1350
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
            Height          =   255
            Left            =   45
            OleObjectBlob   =   "Arriendo.frx":01E8
            TabIndex        =   32
            Top             =   3435
            Width           =   1605
         End
         Begin VB.CommandButton CmdDevolucion 
            Caption         =   "Devoluci�n de Maquinaria"
            Height          =   480
            Left            =   3705
            TabIndex        =   62
            Top             =   3780
            Width           =   2100
         End
         Begin ACTIVESKINLibCtl.SkinLabel SknObservacion 
            Height          =   270
            Left            =   480
            OleObjectBlob   =   "Arriendo.frx":026C
            TabIndex        =   31
            Top             =   2385
            Width           =   1170
         End
         Begin VB.TextBox TxtObservacion 
            Height          =   315
            Left            =   1725
            TabIndex        =   57
            Text            =   "100"
            Top             =   2385
            Width           =   13740
         End
         Begin VB.TextBox TxtNumeroRecepcion 
            Height          =   315
            Left            =   1725
            TabIndex        =   58
            Top             =   2730
            Width           =   4050
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel6 
            Height          =   240
            Left            =   180
            OleObjectBlob   =   "Arriendo.frx":02E4
            TabIndex        =   11
            Top             =   2745
            Width           =   1470
         End
         Begin MSComctlLib.ListView LvProductos 
            Height          =   1680
            Left            =   105
            TabIndex        =   13
            Top             =   360
            Width           =   15390
            _ExtentX        =   27146
            _ExtentY        =   2963
            View            =   3
            LabelWrap       =   0   'False
            HideSelection   =   0   'False
            FullRowSelect   =   -1  'True
            GridLines       =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   12
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Text            =   "Id Detalle"
               Object.Width           =   1764
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Text            =   "Numero Contrato"
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   2
               Text            =   "Codigo Producto"
               Object.Width           =   2822
            EndProperty
            BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   3
               Text            =   "Codigo Empresa"
               Object.Width           =   2822
            EndProperty
            BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   4
               Text            =   "Descripcion"
               Object.Width           =   7056
            EndProperty
            BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   5
               Object.Tag             =   "N100"
               Text            =   "Valor"
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   6
               Text            =   "Estado Producto"
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   7
               Text            =   "Observacion"
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   8
               Text            =   "Numero Recepcion"
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   9
               Text            =   "Nombre Receptor"
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   10
               Text            =   "Fecha Recepcion"
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(12) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   11
               Object.Tag             =   "T1000"
               Text            =   "Hora Recepcion"
               Object.Width           =   2540
            EndProperty
         End
         Begin MSComCtl2.DTPicker FechaRecepcion 
            CausesValidation=   0   'False
            Height          =   285
            Left            =   1725
            TabIndex        =   60
            Top             =   3405
            Width           =   3015
            _ExtentX        =   5318
            _ExtentY        =   503
            _Version        =   393216
            Format          =   95289344
            CurrentDate     =   42768
         End
         Begin MSComCtl2.DTPicker HoraRecepcion 
            CausesValidation=   0   'False
            Height          =   285
            Left            =   4755
            TabIndex        =   61
            Top             =   3405
            Width           =   1035
            _ExtentX        =   1826
            _ExtentY        =   503
            _Version        =   393216
            Format          =   95289346
            CurrentDate     =   42768
         End
      End
      Begin VB.Frame Frame2 
         Caption         =   "Listado de Contratos"
         Height          =   3630
         Left            =   120
         TabIndex        =   3
         Top             =   1860
         Width           =   15750
         Begin VB.CommandButton CmdReimprimir 
            Caption         =   "Reimprimir Contrato"
            Height          =   465
            Left            =   9165
            TabIndex        =   39
            Top             =   2205
            Width           =   2070
         End
         Begin VB.CommandButton CmdEliminar 
            Caption         =   "Eliminar Contrato"
            Height          =   420
            Left            =   7515
            TabIndex        =   34
            Top             =   2220
            Width           =   1425
         End
         Begin VB.CommandButton CmdEstado 
            Caption         =   "Finalizar Contrato"
            Height          =   405
            Left            =   7515
            TabIndex        =   27
            Top             =   3135
            Width           =   1425
         End
         Begin ACTIVESKINLibCtl.SkinLabel SknEstado 
            Height          =   225
            Left            =   450
            OleObjectBlob   =   "Arriendo.frx":0362
            TabIndex        =   26
            Top             =   3210
            Width           =   1245
         End
         Begin ACTIVESKINLibCtl.SkinLabel SknFechaContrato 
            Height          =   270
            Left            =   570
            OleObjectBlob   =   "Arriendo.frx":03DE
            TabIndex        =   25
            Top             =   2895
            Width           =   1125
         End
         Begin ACTIVESKINLibCtl.SkinLabel SknNombreCliente 
            Height          =   300
            Left            =   555
            OleObjectBlob   =   "Arriendo.frx":0458
            TabIndex        =   24
            Top             =   2565
            Width           =   1140
         End
         Begin Proyecto2.XP_ProgressBar BarraProgreso 
            Height          =   495
            Left            =   3270
            TabIndex        =   23
            Top             =   1005
            Visible         =   0   'False
            Width           =   8460
            _ExtentX        =   14923
            _ExtentY        =   873
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BrushStyle      =   0
            Color           =   16777088
            Scrolling       =   1
            ShowText        =   -1  'True
         End
         Begin ACTIVESKINLibCtl.SkinLabel SknContrato 
            Height          =   255
            Left            =   480
            OleObjectBlob   =   "Arriendo.frx":04D2
            TabIndex        =   21
            Top             =   2235
            Width           =   1215
         End
         Begin VB.CommandButton Command2 
            Caption         =   "Cambiar Estado"
            Height          =   360
            Left            =   5505
            TabIndex        =   10
            Top             =   4230
            Width           =   1695
         End
         Begin VB.TextBox TxtNombreCliente 
            Enabled         =   0   'False
            Height          =   300
            Left            =   1755
            TabIndex        =   6
            Top             =   2565
            Width           =   5200
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel5 
            Height          =   240
            Left            =   150
            OleObjectBlob   =   "Arriendo.frx":054E
            TabIndex        =   5
            Top             =   3990
            Width           =   1305
         End
         Begin MSComctlLib.ListView LvContratos 
            Height          =   1725
            Left            =   105
            TabIndex        =   12
            Top             =   300
            Width           =   15555
            _ExtentX        =   27437
            _ExtentY        =   3043
            View            =   3
            LabelWrap       =   0   'False
            HideSelection   =   0   'False
            FullRowSelect   =   -1  'True
            GridLines       =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   12
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Text            =   "Numero Contrato"
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Text            =   "Rut Cliente"
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   2
               Text            =   "Nombre Cliente"
               Object.Width           =   6174
            EndProperty
            BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   3
               Text            =   "Fecha Contrato"
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   4
               Text            =   "Obra"
               Object.Width           =   3528
            EndProperty
            BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   5
               Object.Tag             =   "N109"
               Text            =   "Total"
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   6
               Text            =   "Estado"
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   7
               Text            =   "Obs Devolucion"
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   8
               Text            =   "N Recepcion"
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   9
               Text            =   "Nombre Receptor"
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   10
               Text            =   "Fecha Recepcion"
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(12) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   11
               Text            =   "Hora Recepcion"
               Object.Width           =   2540
            EndProperty
         End
      End
      Begin VB.CommandButton CmdBuscar 
         Caption         =   "Buscar"
         Height          =   480
         Left            =   3270
         TabIndex        =   2
         Top             =   450
         Width           =   1290
      End
      Begin VB.TextBox TxtConsulta 
         Height          =   330
         Left            =   1155
         TabIndex        =   1
         Top             =   585
         Width           =   1905
      End
      Begin VB.Frame Frame5 
         Caption         =   "Filtros de Busqueda"
         Height          =   1665
         Left            =   300
         TabIndex        =   37
         Top             =   165
         Width           =   6285
         Begin VB.Frame Frame7 
            Caption         =   "Buscar por Nro de Recepcion"
            Height          =   645
            Left            =   150
            TabIndex        =   48
            Top             =   870
            Width           =   5925
            Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
               Height          =   300
               Left            =   810
               OleObjectBlob   =   "Arriendo.frx":05CA
               TabIndex        =   51
               Top             =   270
               Width           =   1845
            End
            Begin VB.TextBox TxtNroRecepcion 
               Height          =   330
               Left            =   2835
               TabIndex        =   50
               Top             =   255
               Width           =   1905
            End
            Begin VB.CommandButton CmdRecepcionNro 
               Caption         =   "Consultar"
               Height          =   360
               Left            =   4815
               TabIndex        =   49
               Top             =   225
               Width           =   960
            End
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
            Height          =   270
            Left            =   75
            OleObjectBlob   =   "Arriendo.frx":0648
            TabIndex        =   38
            Top             =   480
            Width           =   735
         End
      End
   End
End
Attribute VB_Name = "Arriendo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim sue_id As Integer
Private Sub CmdAnularContrato_Click()
    If Me.LvContratos.ListItems.Count <= 0 Then
            MsgBox "Debe Seleccionar un contrato...", vbExclamation
            Exit Sub
    End If
    If MsgBox("Esta seguro que desa Anular contrato: " & Me.LvContratos.SelectedItem & "", vbOKCancel + vbQuestion) = vbCancel Then Exit Sub
    FrmLoad.Visible = True
    DoEvents
    
    contratoAnulado

   Sql = "UPDATE ven_arriendo " & _
       "SET arr_estado='NULO' " & _
       "WHERE sue_id='" & LogSueID & "' and arr_id='" & Me.LvContratos.SelectedItem & "'"
    cn.Execute Sql

''Deja Disponible la maquinaria
    For i = 1 To LvProductos.ListItems.Count
      '  If LvProductos.ListItems(i).Checked Then
            Sql = "UPDATE maestro_productos " & _
            "SET pro_estado = 'DISPONIBLE'" & _
            " WHERE  rut_emp='" & SP_Rut_Activo & "' AND pro_codigo_interno='" & Me.LvProductos.ListItems(i).SubItems(3) & "'"
            cn.Execute Sql
      '  End If
    Next
    FrmLoad.Visible = False
    LvContratos_DblClick
End Sub
Private Sub contratoAnulado()
    Dim Sp_Plantilla As String
    Dim Sp_PlantillaCpraVta As String
    Dim Sp_Contrato As String
    Dim Sp_ContratoCpraVta As String
    Dim Sp_TxtHolding(2) As String
    Dim Sp_Variedad As String
    Dim Plantilla As New Word.Application
    Dim PlantillaCpraVta As New Word.Application
    Dim contrato As New Word.Document
    Dim ContratoCpraVta As New Word.Document
    Sp_TxtHolding(0) = "A efectos de calcular las cantidades normalizadas totales respecto a acceder a la bonificaci�n por Volumen, se consideraran, en conjunto con el PRODUCTO que sea recepcionado por MALTEXCO asociado al presente contrato, aquella cebada normalizada resultante de los contratos que se identifican a continuaci�n, en la medida que estos hayan sido firmados por ambas partes y se cumplan copulativamente la totalidad de las cl�usulas establecidas en aquellos contratos:"
    Sp_TxtHolding(1) = "Asimismo, en caso de acceder a esta modalidad denominada �HOLDING�, a efectos de la determinaci�n de la bonificaci�n por antig�edad, se considerar� a partir del contrato m�s antiguo. Cabe destacar que las entregas de m�s de un AGRICULTOR en un mismo per�odo de cosecha no se suman para estos efectos."
    
    Sp_Plantilla = App.Path & "\word\" & "ContratoArriendoDICOEMAnulado.rtf"
Dim contra As Long
contra = Me.TxtContrato

    
    Dialogo.FileName = App.Path & "\word\" & contra
    
    'Dialogo.FileName = "C:" & "\word\" & Lp_Id_Unico_Venta
    '  Dialogo.ShowSave
    '  On Error GoTo problemaGenerar
     Dialogo.FileName = App.Path & "\word\"
    Sp_Contrato = Dialogo.FileName & contra & ".doc"
    'Sp_Contrato = App.Path & "\word\" & TxtRut & ".doc"
    
    FileCopy Sp_Plantilla, Sp_Contrato

    Set contrato = Plantilla.Documents.Open(Sp_Contrato)
        
        Sp_ContratoCpraVta = Dialogo.FileName & "_cpravta.doc"
        FileCopy Sp_Plantilla, Sp_ContratoCpraVta
        Set ContratoCpraVta = PlantillaCpraVta.Documents.Open(Sp_ContratoCpraVta)
'For i = 1 To Me.LvContratos.ListItems.Count
         With ContratoCpraVta.Bookmarks
            .Item("NUMERO_CONTRATO").Range.Text = contra ' Me.LvContratos.SelectedItem
''            .Item("NUMERO_CONTRATO").Range.Text = Me.LvContratos.ListItems(i).SubItems(1)
              .Item("FECHA").Range.Text = FormatDateTime(Date, vbLongDate)
''            .Item("NOMBRE_CLIENTE").Range.Text = TxtRazonSocial
''            .Item("RUT_CLIENTE").Range.Text = Me.LvContratos.SelectedItem(i).SubItems(1)
''            .Item("DIRECCION_CLIENTE").Range.Text = Me.TxtDireccion
''            .Item("GIRO_CLIENTE").Range.Text = Me.TxtGiro
''            .Item("COMUNA_CLIENTE").Range.Text = Me.txtComuna
''
''            .Item("CODIGO_PRODUCTO1").Range.Text = Me.LvVenta.ListItems(1).SubItems(1)
''            .Item("NOMBRE_PRODUCTO1").Range.Text = Me.LvVenta.ListItems(1).SubItems(2)
''            .Item("PRECIO_PRODUCTO1").Range.Text = Me.LvVenta.ListItems(1).SubItems(4)
''
''            If Me.LvVenta.ListItems.Count = 2 Then
''            .Item("CODIGO_PRODUCTO2").Range.Text = Me.LvVenta.ListItems(2).SubItems(1)
''            .Item("NOMBRE_PRODUCTO2").Range.Text = Me.LvVenta.ListItems(2).SubItems(2)
''            .Item("PRECIO_PRODUCTO2").Range.Text = Me.LvVenta.ListItems(2).SubItems(4)
''            Else
''
''            End If
''            If Me.LvVenta.ListItems.Count = 3 Then
''            .Item("CODIGO_PRODUCTO2").Range.Text = Me.LvVenta.ListItems(2).SubItems(1)
''            .Item("NOMBRE_PRODUCTO2").Range.Text = Me.LvVenta.ListItems(2).SubItems(2)
''            .Item("PRECIO_PRODUCTO2").Range.Text = Me.LvVenta.ListItems(2).SubItems(4)
''            .Item("CODIGO_PRODUCTO3").Range.Text = Me.LvVenta.ListItems(3).SubItems(1)
''            .Item("NOMBRE_PRODUCTO3").Range.Text = Me.LvVenta.ListItems(3).SubItems(2)
''            .Item("PRECIO_PRODUCTO3").Range.Text = Me.LvVenta.ListItems(3).SubItems(4)
''            Else
''
''            End If
''            If Me.LvVenta.ListItems.Count = 4 Then
''            .Item("CODIGO_PRODUCTO2").Range.Text = Me.LvVenta.ListItems(2).SubItems(1)
''            .Item("NOMBRE_PRODUCTO2").Range.Text = Me.LvVenta.ListItems(2).SubItems(2)
''            .Item("PRECIO_PRODUCTO2").Range.Text = Me.LvVenta.ListItems(2).SubItems(4)
''            .Item("CODIGO_PRODUCTO3").Range.Text = Me.LvVenta.ListItems(3).SubItems(1)
''            .Item("NOMBRE_PRODUCTO3").Range.Text = Me.LvVenta.ListItems(3).SubItems(2)
''            .Item("PRECIO_PRODUCTO3").Range.Text = Me.LvVenta.ListItems(3).SubItems(4)
''            .Item("CODIGO_PRODUCTO4").Range.Text = Me.LvVenta.ListItems(4).SubItems(1)
''            .Item("NOMBRE_PRODUCTO4").Range.Text = Me.LvVenta.ListItems(4).SubItems(2)
''            .Item("PRECIO_PRODUCTO4").Range.Text = Me.LvVenta.ListItems(4).SubItems(4)
''            Else
''
''            End If
''
''            .Item("OBSERVACION").Range.Text = Me.TxtObservacion
''            .Item("OBRA").Range.Text = Me.TxtObra
''            .Item("TELEFONO").Range.Text = Me.txtFono
''            .Item("NOMBRE_FIRMA").Range.Text = Me.TxtNombreFirma
''            .Item("RUT_FIRMA").Range.Text = Me.TxtRutFirma
''            '.Item("HORA").Range.Text = Time
''            .Item("GARANTIA").Range.Text = Me.TxtGarantia
''            .Item("FECHA_INICIO_CONTRATO").Range.Text = FormatDateTime(DtInicioC, vbLongDate)
''            .Item("HORA_INICIO_CONTRATO").Range.Text = Me.Hora_Inicio
''            .Item("ORDEN_DE_COMPRA").Range.Text = Me.TxtOC
''            .Item("NOMBRE_QUIEN_ENTREGA").Range.Text = Me.TxtNombreEntrega
''            .Item("RUT_QUIEN_ENTREGA").Range.Text = Me.TxtRutEntrega
''''Control Interno
''            .Item("NUMERO_CONTRATOI").Range.Text = Lp_Id_Unico_Venta
''            .Item("FECHAI").Range.Text = FormatDateTime(Date, vbLongDate)
''            .Item("NOMBRE_CLIENTEI").Range.Text = TxtRazonSocial
''            .Item("RUT_CLIENTEI").Range.Text = Me.TxtRut
''            .Item("DIRECCION_CLIENTEI").Range.Text = Me.TxtDireccion
''            .Item("GIRO_CLIENTEI").Range.Text = Me.TxtGiro
''            .Item("COMUNA_CLIENTEI").Range.Text = Me.txtComuna
''
''            .Item("CODIGO_PRODUCTOI1").Range.Text = Me.LvVenta.ListItems(1).SubItems(1)
''            .Item("NOMBRE_PRODUCTOI1").Range.Text = Me.LvVenta.ListItems(1).SubItems(2)
''            .Item("PRECIO_PRODUCTOI1").Range.Text = Me.LvVenta.ListItems(1).SubItems(4)
''
''            If Me.LvVenta.ListItems.Count = 2 Then
''            .Item("CODIGO_PRODUCTOI2").Range.Text = Me.LvVenta.ListItems(2).SubItems(1)
''            .Item("NOMBRE_PRODUCTOI2").Range.Text = Me.LvVenta.ListItems(2).SubItems(2)
''            .Item("PRECIO_PRODUCTOI2").Range.Text = Me.LvVenta.ListItems(2).SubItems(4)
''            Else
''
''            End If
''            If Me.LvVenta.ListItems.Count = 3 Then
''            .Item("CODIGO_PRODUCTOI2").Range.Text = Me.LvVenta.ListItems(2).SubItems(1)
''            .Item("NOMBRE_PRODUCTOI2").Range.Text = Me.LvVenta.ListItems(2).SubItems(2)
''            .Item("PRECIO_PRODUCTOI2").Range.Text = Me.LvVenta.ListItems(2).SubItems(4)
''            .Item("CODIGO_PRODUCTOI3").Range.Text = Me.LvVenta.ListItems(3).SubItems(1)
''            .Item("NOMBRE_PRODUCTOI3").Range.Text = Me.LvVenta.ListItems(3).SubItems(2)
''            .Item("PRECIO_PRODUCTOI3").Range.Text = Me.LvVenta.ListItems(3).SubItems(4)
''            Else
''
''            End If
''            If Me.LvVenta.ListItems.Count = 4 Then
''            .Item("CODIGO_PRODUCTOI2").Range.Text = Me.LvVenta.ListItems(2).SubItems(1)
''            .Item("NOMBRE_PRODUCTOI2").Range.Text = Me.LvVenta.ListItems(2).SubItems(2)
''            .Item("PRECIO_PRODUCTOI2").Range.Text = Me.LvVenta.ListItems(2).SubItems(4)
''            .Item("CODIGO_PRODUCTOI3").Range.Text = Me.LvVenta.ListItems(3).SubItems(1)
''            .Item("NOMBRE_PRODUCTOI3").Range.Text = Me.LvVenta.ListItems(3).SubItems(2)
''            .Item("PRECIO_PRODUCTOI3").Range.Text = Me.LvVenta.ListItems(3).SubItems(4)
''            .Item("CODIGO_PRODUCTOI4").Range.Text = Me.LvVenta.ListItems(4).SubItems(1)
''            .Item("NOMBRE_PRODUCTOI4").Range.Text = Me.LvVenta.ListItems(4).SubItems(2)
''            .Item("PRECIO_PRODUCTOI4").Range.Text = Me.LvVenta.ListItems(4).SubItems(4)
''            Else
''
''            End If
''
''            .Item("OBSERVACIONI").Range.Text = Me.TxtObservacion
''            .Item("OBRAI").Range.Text = Me.TxtObra
''            .Item("TELEFONOI").Range.Text = Me.txtFono
''            .Item("NOMBRE_FIRMAI").Range.Text = Me.TxtNombreFirma
''            .Item("RUT_FIRMAI").Range.Text = Me.TxtRutFirma
''            '.Item("HORA").Range.Text = Time
''            .Item("GARANTIAI").Range.Text = Me.TxtGarantia
''            .Item("FECHA_INICIO_CONTRATOI").Range.Text = FormatDateTime(DtInicioC, vbLongDate)
''            .Item("HORA_INICIO_CONTRATOI").Range.Text = Me.Hora_Inicio
''            .Item("ORDEN_DE_COMPRAI").Range.Text = Me.TxtOC
''            .Item("NOMBRE_QUIEN_ENTREGAI").Range.Text = Me.TxtNombreEntrega
''            .Item("RUT_QUIEN_ENTREGAI").Range.Text = Me.TxtRutEntrega
''''Cliente
''            .Item("NUMERO_CONTRATOC").Range.Text = Lp_Id_Unico_Venta
''            .Item("FECHAC").Range.Text = FormatDateTime(Date, vbLongDate)
''            .Item("NOMBRE_CLIENTEC").Range.Text = TxtRazonSocial
''            .Item("RUT_CLIENTEC").Range.Text = Me.TxtRut
''            .Item("DIRECCION_CLIENTEC").Range.Text = Me.TxtDireccion
''            .Item("GIRO_CLIENTEC").Range.Text = Me.TxtGiro
''            .Item("COMUNA_CLIENTEC").Range.Text = Me.txtComuna
''
''            .Item("CODIGO_PRODUCTOC1").Range.Text = Me.LvVenta.ListItems(1).SubItems(1)
''            .Item("NOMBRE_PRODUCTOC1").Range.Text = Me.LvVenta.ListItems(1).SubItems(2)
''            .Item("PRECIO_PRODUCTOC1").Range.Text = Me.LvVenta.ListItems(1).SubItems(4)
''
''            If Me.LvVenta.ListItems.Count = 2 Then
''            .Item("CODIGO_PRODUCTOC2").Range.Text = Me.LvVenta.ListItems(2).SubItems(1)
''            .Item("NOMBRE_PRODUCTOC2").Range.Text = Me.LvVenta.ListItems(2).SubItems(2)
''            .Item("PRECIO_PRODUCTOC2").Range.Text = Me.LvVenta.ListItems(2).SubItems(4)
''            Else
''
''            End If
''            If Me.LvVenta.ListItems.Count = 3 Then
''            .Item("CODIGO_PRODUCTOC2").Range.Text = Me.LvVenta.ListItems(2).SubItems(1)
''            .Item("NOMBRE_PRODUCTOC2").Range.Text = Me.LvVenta.ListItems(2).SubItems(2)
''            .Item("PRECIO_PRODUCTOC2").Range.Text = Me.LvVenta.ListItems(2).SubItems(4)
''            .Item("CODIGO_PRODUCTOC3").Range.Text = Me.LvVenta.ListItems(3).SubItems(1)
''            .Item("NOMBRE_PRODUCTOC3").Range.Text = Me.LvVenta.ListItems(3).SubItems(2)
''            .Item("PRECIO_PRODUCTOC3").Range.Text = Me.LvVenta.ListItems(3).SubItems(4)
''            Else
''
''            End If
''            If Me.LvVenta.ListItems.Count = 4 Then
''            .Item("CODIGO_PRODUCTOC2").Range.Text = Me.LvVenta.ListItems(2).SubItems(1)
''            .Item("NOMBRE_PRODUCTOC2").Range.Text = Me.LvVenta.ListItems(2).SubItems(2)
''            .Item("PRECIO_PRODUCTOC2").Range.Text = Me.LvVenta.ListItems(2).SubItems(4)
''            .Item("CODIGO_PRODUCTOC3").Range.Text = Me.LvVenta.ListItems(3).SubItems(1)
''            .Item("NOMBRE_PRODUCTOC3").Range.Text = Me.LvVenta.ListItems(3).SubItems(2)
''            .Item("PRECIO_PRODUCTOC3").Range.Text = Me.LvVenta.ListItems(3).SubItems(4)
''            .Item("CODIGO_PRODUCTOC4").Range.Text = Me.LvVenta.ListItems(4).SubItems(1)
''            .Item("NOMBRE_PRODUCTOC4").Range.Text = Me.LvVenta.ListItems(4).SubItems(2)
''            .Item("PRECIO_PRODUCTOC4").Range.Text = Me.LvVenta.ListItems(4).SubItems(4)
''            Else
''
''            End If
''
''            .Item("OBSERVACIONC").Range.Text = Me.TxtObservacion
''            .Item("OBRAC").Range.Text = Me.TxtObra
''            .Item("TELEFONOC").Range.Text = Me.txtFono
''            .Item("NOMBRE_FIRMAC").Range.Text = Me.TxtNombreFirma
''            .Item("RUT_FIRMAC").Range.Text = Me.TxtRutFirma
''            '.Item("HORA").Range.Text = Time
''            .Item("GARANTIAC").Range.Text = Me.TxtGarantia
''            .Item("FECHA_INICIO_CONTRATOC").Range.Text = FormatDateTime(DtInicioC, vbLongDate)
''            .Item("HORA_INICIO_CONTRATOC").Range.Text = Me.Hora_Inicio
''            .Item("ORDEN_DE_COMPRAC").Range.Text = Me.TxtOC
''            .Item("NOMBRE_QUIEN_ENTREGAC").Range.Text = Me.TxtNombreEntrega
''            .Item("RUT_QUIEN_ENTREGAC").Range.Text = Me.TxtRutEntrega

        End With
      '  Next
        PlantillaCpraVta.Visible = True
'
End Sub

Private Sub contratoReimpreso()
    Dim Sp_Plantilla As String
    Dim Sp_PlantillaCpraVta As String
    Dim Sp_Contrato As String
    Dim Sp_ContratoCpraVta As String
    Dim Sp_TxtHolding(2) As String
    Dim Sp_Variedad As String
    Dim Plantilla As New Word.Application
    Dim PlantillaCpraVta As New Word.Application
    Dim contrato As New Word.Document
    Dim ContratoCpraVta As New Word.Document
    Dim Rut As String
    Dim nombre As String
    Dim direccion As String
    Dim comuna As String
    Dim giro As String
    Dim fono As String
    Dim obra As String
    Dim fecha_contrato As String
    Dim fecha_inicio As String
    Dim fecha_termiino As String
    Dim Hora_Inicio As String
 ''   Dim obra As String
    Dim garantia As String
    Dim obs As String
    Dim oc As String

    Dim entrega As String
    Dim rutentrega As String
    Dim firma As String
    Dim firmaentrega As String
    
    Sp_TxtHolding(0) = "A efectos de calcular las cantidades normalizadas totales respecto a acceder a la bonificaci�n por Volumen, se consideraran, en conjunto con el PRODUCTO que sea recepcionado por MALTEXCO asociado al presente contrato, aquella cebada normalizada resultante de los contratos que se identifican a continuaci�n, en la medida que estos hayan sido firmados por ambas partes y se cumplan copulativamente la totalidad de las cl�usulas establecidas en aquellos contratos:"
    Sp_TxtHolding(1) = "Asimismo, en caso de acceder a esta modalidad denominada �HOLDING�, a efectos de la determinaci�n de la bonificaci�n por antig�edad, se considerar� a partir del contrato m�s antiguo. Cabe destacar que las entregas de m�s de un AGRICULTOR en un mismo per�odo de cosecha no se suman para estos efectos."
    
    If SP_Rut_Activo = "76.115.474-5" Then
        Sp_Plantilla = App.Path & "\wordr\" & "ContratoArriendoDICOEM.rtf"
    '    Dialogo.FileName = App.Path & "\wordr\" & Lp_Id_Unico_Venta
    Else
        Sp_Plantilla = App.Path & "\word\" & "ContratoArriendoDICOEM.rtf"
     '   Dialogo.FileName = App.Path & "\word\" & Lp_Id_Unico_Venta
    End If
    
  '  Sp_Plantilla = App.Path & "\word\" & "ContratoArriendoDICOEM.rtf"
    
    
    Dim contra As Long
    contra = Me.TxtContrato

    ''busca datos segun numero de contrato
    Sql = ""
        Sql = "SELECT arr_rut_cliente,arr_nombre_cliente,arr_direccion_cliente,arr_comuna_cliente, " & _
        "arr_giro_cliente,arr_fono_cliente, arr_direccion_obra, " & _
        "arr_garantia, arr_observacion, arr_fecha_contrato,arr_fecha_inicio,arr_fecha_termino, arr_hora_inicio, " & _
        "arr_estado,arr_obs_devolucion,arr_num_recepcion,arr_nombre_receptor,arr_fecha_recepcion,arr_hora_recepcion,arr_oc, " & _
        "arr_nombre_retira,arr_rut_retira,arr_nombre_digita,arr_rut_digita " & _
            "FROM ven_arriendo " & _
            "WHERE arr_id= '" & Me.TxtContrato & "'" & _
            "limit 1"


    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        Rut = RsTmp!arr_rut_cliente
        nombre = RsTmp!arr_nombre_cliente
        fecha_contrato = RsTmp!arr_fecha_contrato
        direccion = RsTmp!arr_direccion_cliente
        comuna = RsTmp!arr_comuna_cliente
     ''   giro = RsTmp!arr_giro_cliente
        giro = RsTmp!arr_giro_cliente
        fono = RsTmp!arr_fono_cliente
        obra = RsTmp!arr_direccion_obra
        obs = RsTmp!arr_observacion
        garantia = RsTmp!arr_garantia
        oc = RsTmp!arr_oc
        fecha_inicio = RsTmp!arr_fecha_inicio
        fecha_termiino = RsTmp!arr_fecha_termino
        Hora_Inicio = RsTmp!arr_hora_inicio
        entrega = "" & RsTmp!arr_nombre_digita
        rutentrega = "" & RsTmp!arr_rut_digita
        firma = "" & RsTmp!arr_nombre_retira
        rutfirma = "" & RsTmp!arr_rut_retira
        
    End If
    ''busca datos segun numero de contrato
    If SP_Rut_Activo = "76.115.474-5" Then
        Dialogo.FileName = App.Path & "\wordr\" & contra
        Dialogo.FileName = App.Path & "\wordr\"
    Else
        Dialogo.FileName = App.Path & "\word\" & contra
        Dialogo.FileName = App.Path & "\word\"
    End If
    'Dialogo.FileName = "C:" & "\word\" & Lp_Id_Unico_Venta
    '  Dialogo.ShowSave
    '  On Error GoTo problemaGenerar
     
    Sp_Contrato = Dialogo.FileName & ".doc"
    'Sp_Contrato = App.Path & "\word\" & TxtRut & ".doc"
    
    FileCopy Sp_Plantilla, Sp_Contrato

    Set contrato = Plantilla.Documents.Open(Sp_Contrato)
        
        Sp_ContratoCpraVta = Dialogo.FileName & "_cpravta.doc"
        FileCopy Sp_Plantilla, Sp_ContratoCpraVta
        Set ContratoCpraVta = PlantillaCpraVta.Documents.Open(Sp_ContratoCpraVta)
For i = 1 To RsTmp.RecordCount
         With ContratoCpraVta.Bookmarks
            .Item("NUMERO_CONTRATO").Range.Text = Me.TxtContrato
''            .Item("NUMERO_CONTRATO").Range.Text = Me.LvContratos.ListItems(i).SubItems(1)
            .Item("FECHA").Range.Text = FormatDateTime(fecha_contrato, vbLongDate)
            .Item("NOMBRE_CLIENTE").Range.Text = nombre
            .Item("RUT_CLIENTE").Range.Text = Rut
            .Item("DIRECCION_CLIENTE").Range.Text = direccion
            .Item("GIRO_CLIENTE").Range.Text = giro
            .Item("COMUNA_CLIENTE").Range.Text = comuna

            .Item("CODIGO_PRODUCTO1").Range.Text = Me.LvProductos.ListItems(1).SubItems(3)
            .Item("NOMBRE_PRODUCTO1").Range.Text = Me.LvProductos.ListItems(1).SubItems(4)
            .Item("PRECIO_PRODUCTO1").Range.Text = Replace(Me.LvProductos.ListItems(1).SubItems(5), ",", ".")

            If Me.LvProductos.ListItems.Count = 2 Then
            .Item("CODIGO_PRODUCTO2").Range.Text = Me.LvProductos.ListItems(2).SubItems(3)
            .Item("NOMBRE_PRODUCTO2").Range.Text = Me.LvProductos.ListItems(2).SubItems(4)
            .Item("PRECIO_PRODUCTO2").Range.Text = Replace(Me.LvProductos.ListItems(2).SubItems(5), ",", ".")
            Else

            End If
            If Me.LvProductos.ListItems.Count = 3 Then
            .Item("CODIGO_PRODUCTO2").Range.Text = Me.LvProductos.ListItems(2).SubItems(3)
            .Item("NOMBRE_PRODUCTO2").Range.Text = Me.LvProductos.ListItems(2).SubItems(4)
            .Item("PRECIO_PRODUCTO2").Range.Text = Replace(Me.LvProductos.ListItems(2).SubItems(5), ",", ".")
            .Item("CODIGO_PRODUCTO3").Range.Text = Me.LvProductos.ListItems(3).SubItems(3)
            .Item("NOMBRE_PRODUCTO3").Range.Text = Me.LvProductos.ListItems(3).SubItems(4)
            .Item("PRECIO_PRODUCTO3").Range.Text = Replace(Me.LvProductos.ListItems(3).SubItems(5), ",", ".")
            Else

            End If
            If Me.LvProductos.ListItems.Count = 4 Then
            .Item("CODIGO_PRODUCTO2").Range.Text = Me.LvProductos.ListItems(2).SubItems(3)
            .Item("NOMBRE_PRODUCTO2").Range.Text = Me.LvProductos.ListItems(2).SubItems(4)
            .Item("PRECIO_PRODUCTO2").Range.Text = Me.LvProductos.ListItems(2).SubItems(5)
            .Item("CODIGO_PRODUCTO3").Range.Text = Me.LvProductos.ListItems(3).SubItems(3)
            .Item("NOMBRE_PRODUCTO3").Range.Text = Me.LvProductos.ListItems(3).SubItems(4)
            .Item("PRECIO_PRODUCTO3").Range.Text = Me.LvProductos.ListItems(3).SubItems(5)
            .Item("CODIGO_PRODUCTO4").Range.Text = Me.LvProductos.ListItems(4).SubItems(3)
            .Item("NOMBRE_PRODUCTO4").Range.Text = Me.LvProductos.ListItems(4).SubItems(4)
            .Item("PRECIO_PRODUCTO4").Range.Text = Me.LvProductos.ListItems(4).SubItems(5)
            Else

            End If

            .Item("OBSERVACION").Range.Text = obs
            .Item("OBRA").Range.Text = obra
            .Item("TELEFONO").Range.Text = fono
            .Item("NOMBRE_FIRMA").Range.Text = firma
            .Item("RUT_FIRMA").Range.Text = rutfirma
          ''  .Item("HORA").Range.Text = Time
            .Item("GARANTIA").Range.Text = garantia
            .Item("FECHA_INICIO_CONTRATO").Range.Text = FormatDateTime(fecha_termiino, vbLongDate)
            .Item("HORA_INICIO_CONTRATO").Range.Text = Hora_Inicio
            .Item("ORDEN_DE_COMPRA").Range.Text = oc
            .Item("NOMBRE_QUIEN_ENTREGA").Range.Text = entrega
            .Item("RUT_QUIEN_ENTREGA").Range.Text = rutentrega
''Control Interno
            .Item("NUMERO_CONTRATOI").Range.Text = Me.TxtContrato
            .Item("FECHAI").Range.Text = FormatDateTime(fecha_contrato, vbLongDate)
            .Item("NOMBRE_CLIENTEI").Range.Text = nombre
            .Item("RUT_CLIENTEI").Range.Text = Rut
            .Item("DIRECCION_CLIENTEI").Range.Text = direccion
            .Item("GIRO_CLIENTEI").Range.Text = giro
            .Item("COMUNA_CLIENTEI").Range.Text = comuna
            
            
            .Item("CODIGO_PRODUCTOI1").Range.Text = Me.LvProductos.ListItems(1).SubItems(3)
            .Item("NOMBRE_PRODUCTOI1").Range.Text = Me.LvProductos.ListItems(1).SubItems(4)
            .Item("PRECIO_PRODUCTOI1").Range.Text = Me.LvProductos.ListItems(1).SubItems(5)

            If Me.LvProductos.ListItems.Count = 2 Then
            .Item("CODIGO_PRODUCTOI2").Range.Text = Me.LvProductos.ListItems(2).SubItems(3)
            .Item("NOMBRE_PRODUCTOI2").Range.Text = Me.LvProductos.ListItems(2).SubItems(4)
            .Item("PRECIO_PRODUCTOI2").Range.Text = Me.LvProductos.ListItems(2).SubItems(5)
            Else

            End If
            If Me.LvProductos.ListItems.Count = 3 Then
            .Item("CODIGO_PRODUCTOI2").Range.Text = Me.LvProductos.ListItems(2).SubItems(3)
            .Item("NOMBRE_PRODUCTOI2").Range.Text = Me.LvProductos.ListItems(2).SubItems(4)
            .Item("PRECIO_PRODUCTOI2").Range.Text = Me.LvProductos.ListItems(2).SubItems(5)
            .Item("CODIGO_PRODUCTOI3").Range.Text = Me.LvProductos.ListItems(3).SubItems(3)
            .Item("NOMBRE_PRODUCTOI3").Range.Text = Me.LvProductos.ListItems(3).SubItems(4)
            .Item("PRECIO_PRODUCTOI3").Range.Text = Me.LvProductos.ListItems(3).SubItems(5)
            Else

            End If
            If Me.LvProductos.ListItems.Count = 4 Then
            .Item("CODIGO_PRODUCTOI2").Range.Text = Me.LvProductos.ListItems(2).SubItems(3)
            .Item("NOMBRE_PRODUCTOI2").Range.Text = Me.LvProductos.ListItems(2).SubItems(4)
            .Item("PRECIO_PRODUCTOI2").Range.Text = Me.LvProductos.ListItems(2).SubItems(5)
            .Item("CODIGO_PRODUCTOI3").Range.Text = Me.LvProductos.ListItems(3).SubItems(3)
            .Item("NOMBRE_PRODUCTOI3").Range.Text = Me.LvProductos.ListItems(3).SubItems(4)
            .Item("PRECIO_PRODUCTOI3").Range.Text = Me.LvProductos.ListItems(3).SubItems(5)
            .Item("CODIGO_PRODUCTOI4").Range.Text = Me.LvProductos.ListItems(4).SubItems(3)
            .Item("NOMBRE_PRODUCTOI4").Range.Text = Me.LvProductos.ListItems(4).SubItems(4)
            .Item("PRECIO_PRODUCTOI4").Range.Text = Me.LvProductos.ListItems(4).SubItems(5)
            Else

            End If

            .Item("OBSERVACIONI").Range.Text = obs
            .Item("OBRAI").Range.Text = obra
            .Item("TELEFONOI").Range.Text = fono
            .Item("NOMBRE_FIRMAI").Range.Text = firma
            .Item("RUT_FIRMAI").Range.Text = rutfirma
          ''  .Item("HORA").Range.Text = Time
            .Item("GARANTIAI").Range.Text = garantia
            .Item("FECHA_INICIO_CONTRATOI").Range.Text = FormatDateTime(fecha_termiino, vbLongDate)
            .Item("HORA_INICIO_CONTRATOI").Range.Text = Hora_Inicio
            .Item("ORDEN_DE_COMPRAI").Range.Text = oc
            .Item("NOMBRE_QUIEN_ENTREGAI").Range.Text = entrega
            .Item("RUT_QUIEN_ENTREGAI").Range.Text = rutentrega

''''Cliente
            .Item("NUMERO_CONTRATOC").Range.Text = Me.TxtContrato
            .Item("FECHAC").Range.Text = FormatDateTime(fecha_contrato, vbLongDate)
            .Item("NOMBRE_CLIENTEC").Range.Text = nombre
            .Item("RUT_CLIENTEC").Range.Text = Rut
            .Item("DIRECCION_CLIENTEC").Range.Text = direccion
            .Item("GIRO_CLIENTEC").Range.Text = giro
            .Item("COMUNA_CLIENTEC").Range.Text = comuna
            
            .Item("CODIGO_PRODUCTOC1").Range.Text = Me.LvProductos.ListItems(1).SubItems(3)
            .Item("NOMBRE_PRODUCTOC1").Range.Text = Me.LvProductos.ListItems(1).SubItems(4)
            .Item("PRECIO_PRODUCTOC1").Range.Text = Me.LvProductos.ListItems(1).SubItems(5)

            If Me.LvProductos.ListItems.Count = 2 Then
            .Item("CODIGO_PRODUCTOC2").Range.Text = Me.LvProductos.ListItems(2).SubItems(3)
            .Item("NOMBRE_PRODUCTOC2").Range.Text = Me.LvProductos.ListItems(2).SubItems(4)
            .Item("PRECIO_PRODUCTOC2").Range.Text = Me.LvProductos.ListItems(2).SubItems(5)
            Else

            End If
            If Me.LvProductos.ListItems.Count = 3 Then
            .Item("CODIGO_PRODUCTOC2").Range.Text = Me.LvProductos.ListItems(2).SubItems(3)
            .Item("NOMBRE_PRODUCTOC2").Range.Text = Me.LvProductos.ListItems(2).SubItems(4)
            .Item("PRECIO_PRODUCTOC2").Range.Text = Me.LvProductos.ListItems(2).SubItems(5)
            .Item("CODIGO_PRODUCTOC3").Range.Text = Me.LvProductos.ListItems(3).SubItems(3)
            .Item("NOMBRE_PRODUCTOC3").Range.Text = Me.LvProductos.ListItems(3).SubItems(4)
            .Item("PRECIO_PRODUCTOC3").Range.Text = Me.LvProductos.ListItems(3).SubItems(5)
            Else

            End If
            If Me.LvProductos.ListItems.Count = 4 Then
            .Item("CODIGO_PRODUCTOC2").Range.Text = Me.LvProductos.ListItems(2).SubItems(3)
            .Item("NOMBRE_PRODUCTOC2").Range.Text = Me.LvProductos.ListItems(2).SubItems(4)
            .Item("PRECIO_PRODUCTOC2").Range.Text = Me.LvProductos.ListItems(2).SubItems(5)
            .Item("CODIGO_PRODUCTOC3").Range.Text = Me.LvProductos.ListItems(3).SubItems(3)
            .Item("NOMBRE_PRODUCTOC3").Range.Text = Me.LvProductos.ListItems(3).SubItems(4)
            .Item("PRECIO_PRODUCTOC3").Range.Text = Me.LvProductos.ListItems(3).SubItems(5)
            .Item("CODIGO_PRODUCTOC4").Range.Text = Me.LvProductos.ListItems(4).SubItems(3)
            .Item("NOMBRE_PRODUCTOC4").Range.Text = Me.LvProductos.ListItems(4).SubItems(4)
            .Item("PRECIO_PRODUCTOC4").Range.Text = Me.LvProductos.ListItems(4).SubItems(5)
            Else

            End If

            .Item("OBSERVACIONC").Range.Text = obs
            .Item("OBRAC").Range.Text = obra
            .Item("TELEFONOC").Range.Text = fono
            .Item("NOMBRE_FIRMAC").Range.Text = firma
            .Item("RUT_FIRMAC").Range.Text = rutfirma
          ''  .Item("HORA").Range.Text = Time
            .Item("GARANTIAC").Range.Text = garantia
            .Item("FECHA_INICIO_CONTRATOC").Range.Text = FormatDateTime(fecha_termiino, vbLongDate)
            .Item("HORA_INICIO_CONTRATOC").Range.Text = Hora_Inicio
            .Item("ORDEN_DE_COMPRAC").Range.Text = oc
            .Item("NOMBRE_QUIEN_ENTREGAC").Range.Text = entrega
            .Item("RUT_QUIEN_ENTREGAC").Range.Text = rutentrega

        End With
        Next
        PlantillaCpraVta.Visible = True

End Sub

Private Sub CmdAnulaRecepcion_Click()
    If LvProductos.SelectedItem Is Nothing Then Exit Sub
    If MsgBox("Est� seguro(a) de anular esta recepci�n...?", vbInformation + vbOKCancel) = vbOK Then
        '24-10-2018
        'anular recepcion
        Sql = "UPDATE ven_arr_detalle SET det_arr_num_recepcion=0,det_arr_nombre_receptor='',det_arr_fecha_recepcion=Null,det_arr_hora_recepcion=Null " & _
                "WHERE det_arr_id=" & LvProductos.SelectedItem
        cn.Execute Sql
        
        CargaRecepciones
        MsgBox "Recepcion actualizada...", vbInformation
        
    End If
End Sub

Private Sub CmdBuscar_Click()
    Dim Sp_SueId As String
    Me.FrmLoad.Visible = True

    DoEvents
    LvContratos.ListItems.Clear
    LvProductos.ListItems.Clear
    Sp_SueId = " sue_id=" & LogSueID & " "
    If Option1.Value Then
      If TxtConsulta = "" Then
                Sql = "SELECT arr_id,arr_rut_cliente,arr_nombre_cliente,DATE_FORMAT(arr_fecha_contrato,'%d-%m-%Y')arr_fecha_contrato,arr_direccion_obra,arr_valor,arr_estado,arr_obs_devolucion,arr_num_recepcion,arr_nombre_receptor,arr_fecha_recepcion,arr_hora_recepcion " & _
                "FROM ven_arriendo " & _
                "WHERE " & Sp_SueId & _
                "ORDER BY arr_id DESC"
       Else
                Sql = "SELECT arr_id,arr_rut_cliente,arr_nombre_cliente,DATE_FORMAT(arr_fecha_contrato,'%d-%m-%Y')arr_fecha_contrato,arr_direccion_obra,arr_valor,arr_estado,arr_obs_devolucion,arr_num_recepcion,arr_nombre_receptor,arr_fecha_recepcion,arr_hora_recepcion " & _
                "FROM ven_arriendo " & _
                "WHERE " & Sp_SueId & " and arr_id =  '" & TxtConsulta & "' " & _
                "ORDER BY arr_id DESC"
       End If
    Else
    Sql = ""
'                Sql = "SELECT despacho_id,rut_cliente,DATE_FORMAT(despacho_fecha,'%d-%m-%Y')despacho_fecha,despacho_direccion,nve_id " & _
'                "FROM ven_despacho " & _
'                "where despacho_fecha BETWEEN '" & Format(DTInicio.Value, "YYYY-MM-DD") & "' AND '" & Format(DtHasta.Value, "YYYY-MM-DD") & "'"
       If TxtConsulta = "" Then
               Sql = "SELECT arr_id,arr_rut_cliente,arr_nombre_cliente,DATE_FORMAT(arr_fecha_contrato,'%d-%m-%Y')arr_fecha_contrato,arr_direccion_obra,arr_valor,arr_estado,arr_obs_devolucion,arr_num_recepcion,arr_nombre_receptor,arr_fecha_recepcion,arr_hora_recepcion " & _
               "FROM ven_arriendo " & _
               "WHERE " & Sp_SueId & " AND arr_fecha_contrato BETWEEN '" & Format(DTInicio.Value, "YYYY-MM-DD") & "' AND '" & Format(DtHasta.Value, "YYYY-MM-DD") & "' " & _
               "ORDER BY arr_id DESC"
       Else
              Sql = "SELECT arr_id,arr_rut_cliente,arr_nombre_cliente,DATE_FORMAT(arr_fecha_contrato,'%d-%m-%Y')arr_fecha_contrato,arr_direccion_obra,arr_valor,arr_estado,arr_obs_devolucion,arr_num_recepcion,arr_nombre_receptor,arr_fecha_recepcion,arr_hora_recepcion " & _
               "FROM ven_arriendo " & _
               "WHERE " & Sp_SueId & "  AND arr_fecha_contrato BETWEEN '" & Format(DTInicio.Value, "YYYY-MM-DD") & "' AND '" & Format(DtHasta.Value, "YYYY-MM-DD") & "' and arr_id = '" & TxtConsulta & "' " & _
               "ORDER BY arr_id DESC"
       End If

    End If

    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvContratos, False, True, True, False
    FrmLoad.Visible = False
End Sub

Private Sub CmdConProductosDevuelto_Click()
'              Sql = "SELECT arr_id,arr_rut_cliente,arr_nombre_cliente,DATE_FORMAT(arr_fecha_contrato,'%d-%m-%Y')arr_fecha_contrato,arr_direccion_obra,arr_valor,arr_estado " & _
'               "FROM ven_arriendo " & _
'               "WHERE arr_fecha_contrato BETWEEN '" & Format(DTInicio.Value, "YYYY-MM-DD") & "' AND '" & Format(DtHasta.Value, "YYYY-MM-DD") & "' and arr_id =   TxtConsulta  "
'    Consulta RsTmp, Sql
'    LLenar_Grilla RsTmp, Me, LvContratos, False, True, True, False
End Sub

Private Sub CmdCambiaHora_Click()
    If LvProductos.SelectedItem Is Nothing Then Exit Sub
    SG_codigo = ""
    SG_codigo = InputBox("Ingrese nueva hora:", "actualizar hora", LvProductos.SelectedItem.SubItems(11))
    If Len(SG_codigo) > 0 Then
                Sql = "UPDATE ven_arr_detalle SET det_arr_hora_recepcion='" & SG_codigo & "' " & _
                "WHERE det_arr_id=" & LvProductos.SelectedItem
        cn.Execute Sql
        
        CargaRecepciones
    End If
End Sub

Private Sub CmdDevolucion_Click()




    If Me.LvProductos.ListItems.Count <= 0 Then
                MsgBox "Debe Seleccionar al menos una Maquinaria...", vbExclamation
              Exit Sub
    End If
    
    'Antes de continuar debemos saber si ya existe una recepcion previa
    For i = 1 To LvProductos.ListItems.Count
        If LvProductos.ListItems(i).Checked Then
                Sql = "SELECT IFNULL(det_arr_num_recepcion,0) nrorecep " & _
                        "FROM ven_arr_detalle " & _
                        "WHERE det_arr_id=" & LvProductos.ListItems(i)
                Consulta RsTmp, Sql
                If RsTmp.RecordCount > 0 Then
                    If Val(RsTmp!nrorecep) > 0 Then
                        MsgBox "El Producto:" & LvProductos.ListItems(i).SubItems(2) & vbNewLine & "Ya tiene una recepcion registrada" & vbNewLine & "Cambie su seleccion", vbInformation
                        Exit Sub
                    End If
                End If
        End If
    Next
    
    
    'Tambien debemos verificar que el nro de recepcion ingresado, no est� antes ingresado
 '   Sql = "SELECT arr_id " & _
 '           "FROM ven_arr_detalle " & _
 '           "WHERE det_arr_num_recepcion=" & Val(TxtNumeroRecepcion)
 '   Consulta RsTmp, Sql
 '   If RsTmp.RecordCount > 0 Then
 '       MsgBox "El Nro de Recepci�n ya ha sido utilizado antes...", vbInformation
 '       TxtNumeroRecepcion.SetFocus
 '       Exit Sub
 '   End If
    
    
    
    
    
    'Me.HoraRecepcion.Format
    If Me.TxtObservacion = "" Then
                MsgBox "Debe Ingresar Observacion...", vbExclamation
              Exit Sub
    End If
    
    If Val(TxtNumeroRecepcion) = 0 Then
            MsgBox "Debe ingresar Nro de Recepci�n", vbInformation
            TxtNumeroRecepcion.SetFocus
            Exit Sub
    End If
    
    If Len(TxtNombreReceptor) = 0 Then
            MsgBox "Debe ingresar nombre de receptor...", vbInformation
            TxtNombreReceptor.SetFocus
            Exit Sub
    End If
    
    For i = 1 To LvProductos.ListItems.Count
        If LvProductos.ListItems(i).Checked Then
            Sql = "UPDATE maestro_productos " & _
            "SET pro_estado = 'En Mantencion'" & _
            " WHERE  rut_emp='" & SP_Rut_Activo & "' AND pro_codigo_interno='" & Me.LvProductos.ListItems(i).SubItems(3) & "'"
            cn.Execute Sql
                          
            '' Actualiza el detalle del contrato por codigo de producto, sucursal y numero de contrato.
            Sql = "UPDATE ven_arr_detalle " & _
            "SET det_arr_estado = 'En Mantencion', det_arr_obs_devolucion='" & TxtObservacion & "', det_arr_num_recepcion='" & TxtNumeroRecepcion & "',  " & _
            "det_arr_nombre_receptor='" & TxtNombreReceptor & "',det_arr_fecha_recepcion='" & Fql(FechaRecepcion) & "',det_arr_hora_recepcion= '" & HoraRecepcion.Hour & ":" & HoraRecepcion.Minute & ":00' " & _
            " WHERE  sue_id='" & LogSueID & "' AND   det_arr_id='" & Me.LvProductos.ListItems(i) & "' AND   det_arr_codigo_interno='" & Me.LvProductos.ListItems(i).SubItems(3) & "'"
        
    
            cn.Execute Sql
                                    
        End If
    Next

    LvContratos_DblClick
    TxtObservacion = ""
    TxtNumeroRecepcion = ""
    TxtNombreReceptor = ""

End Sub

Private Sub CmdEliminar_Click()
    If Me.LvContratos.ListItems.Count <= 0 Then
                MsgBox "Debe Seleccionar un contrato...", vbExclamation
              Exit Sub
    End If
    If MsgBox("Esta seguro que desa Eliminar contrato: " & Me.LvContratos.SelectedItem & "", vbOKCancel + vbQuestion) = vbCancel Then Exit Sub
    Sql = ""
    Sql = "DELETE from ven_arriendo " & _
    "WHERE sue_id='" & LogSueID & "' and arr_id='" & TxtContrato & "'"
    cn.Execute Sql

    Sql = ""
       Sql = "DELETE from ven_arr_detalle " & _
       "WHERE sue_id='" & LogSueID & "' and arr_id='" & TxtContrato & "'"
    cn.Execute Sql

End Sub

Private Sub CmdEstado_Click()
  If Me.LvContratos.ListItems.Count <= 0 Then
              MsgBox "Debe Seleccionar un contrato...", vbExclamation
            Exit Sub
  End If
  
  '13Marz02019
  '
  
''If MsgBox("Esta seguro que desa finalizar contrato" & vbNewLine & vbYesNo + vbQuestion, "Finalizar Contrato") = vbNo Then Exit Sub
If MsgBox("Esta seguro que desa finalizar contrato: " & Me.LvContratos.SelectedItem & "", vbOKCancel + vbQuestion) = vbCancel Then Exit Sub
Sql = ""
   Sql = "UPDATE ven_arriendo " & _
   "SET arr_estado='Finalizado' " & _
   "WHERE sue_id='" & LogSueID & "' and arr_id='" & Me.LvContratos.SelectedItem & "'"
cn.Execute Sql
LvContratos_DblClick

End Sub

Private Sub CmdExportar_Click()
 Dim tit(2) As String
    If LvContratos.ListItems.Count = 0 Then Exit Sub
    BarraProgreso.Visible = True
    tit(0) = Me.Caption & " " & Date & " - " & Time
    tit(1) = ""
    tit(2) = ""
    ExportarNuevo LvContratos, tit, Me, BarraProgreso
    BarraProgreso.Visible = False
End Sub

Private Sub CmdLimpiar_Click()
Me.TxtConsulta = ""
LvContratos.ListItems.Clear
LvProductos.ListItems.Clear
Me.TxtContrato = ""
Me.TxtNombreCliente = ""
Me.TxtFechaContrato = ""
Me.TxtEstado = ""
''Me.TxtMontoDevuelto = ""
End Sub

Private Sub CmdSalir_Click()
    Unload Me
End Sub

Private Sub CmdRecepcionNro_Click()
    FrmLoad.Visible = True
    DoEvents
    LvProductos.ListItems.Clear
    Sp_Nro_Recepcion = " AND  det_arr_num_recepcion=" & Val(TxtNroRecepcion)
    '  Sql = "SELECT arr_id,arr_rut_cliente,arr_nombre_cliente,DATE_FORMAT(arr_fecha_contrato,'%d-%m-%Y')arr_fecha_contrato,arr_direccion_obra,arr_valor,arr_estado " & _
                "FROM ven_arriendo " & _
                "WHERE arr_rut_cliente =  '" & TxtConsulta & "'"
    
    Sql = "SELECT d.arr_id,arr_rut_cliente,arr_nombre_cliente,DATE_FORMAT(arr_fecha_contrato,'%d-%m-%Y')arr_fecha_contrato,arr_direccion_obra,arr_valor,arr_estado " & _
            "FROM ven_arr_detalle d " & _
            "JOIN ven_arriendo a ON d.arr_id=a.arr_id " & _
            "JOIN maestro_clientes c ON a.arr_rut_cliente=c.rut_cliente " & _
            "WHERE d.sue_id=" & LogSueID & " AND a.sue_id=" & LogSueID & " " & Sp_Nro_Recepcion & " " & _
            "GROUP BY a.arr_id " & _
            "ORDER BY d.det_arr_num_recepcion "

    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvContratos, False, True, True, False
    
  '  Sql = "SELECT sue_ciudad,sue_direccion " & _
            "FROM sis_empresas_sucursales " & _
            "WHERE sue_id=" & LogSueID
   ' Consulta RsTmp, Sql
    'If RsTmp.RecordCount > 0 Then
       ' SkSucursal = RsTmp!sue_ciudad & " - " & RsTmp!sue_direccion
        
    
    'End If
    
    
    FrmLoad.Visible = False
End Sub

Private Sub CmdRecOk_Click()
    Dim Sp_Elementos As String

    If Len(TxtRecObs) = 0 Then
        MsgBox "Debe ingresar una descripcion...", vbInformation
        TxtRecObs.SetFocus
        Exit Sub
    End If
    
  '  For i = 1 To LvProductos.ListItems.Count
  '      If LvProductos.ListItems(i).Checked Then
            Sp_Elementos = Sp_Elementos & LvProductos.SelectedItem.SubItems(4)
  ''      End If
    'Next
   '
    If MsgBox("Registrar este evento para: " & vbNewLine & vbNewLine & Sp_Elementos, vbOKCancel) = vbCancel Then Exit Sub
    
    Sql = "INSERT INTO ven_arr_detalle_recepciones (det_arr_id,rec_obs,rec_fecha,rec_hora,rec_utilizable,rec_nro_recepcion) " & _
                "VALUES(" & LvProductos.SelectedItem & ",'" & TxtRecObs & "','" & Fql(Me.DtRecFecha) & "','" & Format(DtRecHora, "HH:MM") & "','" & Me.CboRecUtilizable.Text & "'," & Me.TxtRecNroRecep & ")"
    
    cn.Execute Sql
    
    Sql = "SELECT rec_id,rec_obs,rec_fecha,rec_hora,rec_utilizable,rec_nro_recepcion " & _
            "FROM ven_arr_detalle_recepciones " & _
                "WHERE det_arr_id=" & LvProductos.SelectedItem
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, Me.LvRecepciones, False, True, True, False
    
End Sub

Private Sub CmdReimprimir_Click()
  If Me.LvContratos.ListItems.Count <= 0 Then
              MsgBox "Debe Seleccionar al menos un contrato...", vbExclamation
            Exit Sub
  End If
  If Val(TxtContrato) = 0 Then
    MsgBox "Haga doble clic en el contrato que desea re-imprimir...", vbInformation
    Exit Sub
End If
contratoReimpreso
End Sub

Private Sub CmdRetornar_Click()
Unload Me
End Sub

Private Sub Command1_Click()
    LvContratos.ListItems.Clear
    LvProductos.ListItems.Clear
    
        LlamaClienteDe = "VD"
    ClienteEncontrado = False
    BuscaCliente.Show 1
    TxtConsulta = SG_codigo
    ''TxtRut.Text = Replace(TxtRut.Text, ".", "")
    ''TxtRut.Text = Replace(TxtRut.Text, "-", "")
    TxtConsulta.SetFocus
    
    
    If Option1.Value Then
    
    
      Sql = ""
      
      If TxtConsulta = "" Then
                Sql = "SELECT arr_id,arr_rut_cliente,arr_nombre_cliente,DATE_FORMAT(arr_fecha_contrato,'%d-%m-%Y')arr_fecha_contrato,arr_direccion_obra,arr_valor,arr_estado " & _
                "FROM ven_arriendo "
               ' "WHERE arr_id =  '" & TxtConsulta & "'"
       Else
                Sql = "SELECT arr_id,arr_rut_cliente,arr_nombre_cliente,DATE_FORMAT(arr_fecha_contrato,'%d-%m-%Y')arr_fecha_contrato,arr_direccion_obra,arr_valor,arr_estado " & _
                "FROM ven_arriendo " & _
                "WHERE arr_rut_cliente =  '" & TxtConsulta & "'"
       End If
                
    Else
         Sql = ""
'                Sql = "SELECT despacho_id,rut_cliente,DATE_FORMAT(despacho_fecha,'%d-%m-%Y')despacho_fecha,despacho_direccion,nve_id " & _
'                "FROM ven_despacho " & _
'                "where despacho_fecha BETWEEN '" & Format(DTInicio.Value, "YYYY-MM-DD") & "' AND '" & Format(DtHasta.Value, "YYYY-MM-DD") & "'"
        If TxtConsulta = "" Then
               Sql = "SELECT arr_id,arr_rut_cliente,arr_nombre_cliente,DATE_FORMAT(arr_fecha_contrato,'%d-%m-%Y')arr_fecha_contrato,arr_direccion_obra,arr_valor,arr_estado " & _
               "FROM ven_arriendo " & _
               "WHERE arr_fecha_contrato BETWEEN '" & Format(DTInicio.Value, "YYYY-MM-DD") & "' AND '" & Format(DtHasta.Value, "YYYY-MM-DD") & "'"
       Else
              Sql = "SELECT arr_id,arr_rut_cliente,arr_nombre_cliente,DATE_FORMAT(arr_fecha_contrato,'%d-%m-%Y')arr_fecha_contrato,arr_direccion_obra,arr_valor,arr_estado " & _
               "FROM ven_arriendo " & _
               "WHERE arr_fecha_contrato BETWEEN '" & Format(DTInicio.Value, "YYYY-MM-DD") & "' AND '" & Format(DtHasta.Value, "YYYY-MM-DD") & "' and arr_rut_cliente = '" & TxtConsulta & "'"
       End If

    End If

    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvContratos, False, True, True, False
End Sub
        
Private Sub Command3_Click()
    If LvProductos.SelectedItem Is Nothing Then Exit Sub
    Compra_CambiaFecha.DtFecha = LvProductos.SelectedItem.SubItems(10)
    Compra_CambiaFecha.Show 1
    If SG_codigo2 = Empty Then Exit Sub
    
    Dim Fp_FechaN As Date
    
  
    If Len(SG_codigo2) > 0 Then
                'Fp_FechaN = SG_codigo2
                Sql = "UPDATE ven_arr_detalle SET det_arr_fecha_recepcion=" & SG_codigo2 & " " & _
                "WHERE det_arr_id=" & LvProductos.SelectedItem
        cn.Execute Sql
        
        CargaRecepciones
    End If
End Sub

Private Sub Form_Load()

    Centrar Me
    Me.Height = 10845
    Aplicar_skin Me
    Me.CmdEliminar.Visible = False
    
    If LogUsuario = "KAROL" Then
        Me.CmdEliminar.Visible = True
    Else
        Me.CmdEliminar.Visible = False
    End If
    

        Sql = ""
    Sql = "SELECT sue_id FROM sis_usuarios WHERE usu_login='" & LogUsuario & "'"
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
     sue_id = RsTmp!sue_id
   End If
   DtRecFecha.Value = Date
   FechaRecepcion.Value = Date
   CboRecUtilizable.ListIndex = 0
   DTInicio = Date
   DtHasta = Date
End Sub

Private Sub LvContratos_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    ordListView ColumnHeader, Me, LvContratos
End Sub

Private Sub LvContratos_DblClick()

    TxtObservacion = ""
    TxtNumeroRecepcion = ""
    Me.TxtNombreReceptor = ""
    CargaRecepciones
End Sub
Private Sub CargaRecepciones()
    FrmLoad.Visible = True
    DoEvents
    Sql = "SELECT det_arr_id,arr_id,det_arr_codigo,det_arr_codigo_interno,det_arr_descripcion,det_arr_total, det_arr_estado, " & _
                " det_arr_obs_devolucion,det_arr_num_recepcion,det_arr_nombre_receptor,det_arr_fecha_recepcion,CAST(det_arr_hora_recepcion AS CHAR(12)) hora " & _
                "FROM ven_arr_detalle " & _
                "WHERE sue_id='" & LogSueID & "' and  arr_id =  '" & LvContratos.SelectedItem & "'"
    
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvProductos, True, True, True, True
    
    Me.TxtContrato = LvContratos.SelectedItem
    Me.TxtNombreCliente = LvContratos.SelectedItem.SubItems(2)
    Me.TxtFechaContrato = LvContratos.SelectedItem.SubItems(3)
    Me.TxtEstado = LvContratos.SelectedItem.SubItems(6)
    FrmLoad.Visible = False
End Sub


Private Sub LvProductos_Click()
    If LvProductos.SelectedItem Is Nothing Then Exit Sub
    
    TxtObservacion = LvProductos.SelectedItem.SubItems(7)
    TxtNumeroRecepcion = LvProductos.SelectedItem.SubItems(8)
    Me.TxtNombreReceptor = LvProductos.SelectedItem.SubItems(9)
    If Len(LvProductos.SelectedItem.SubItems(10)) > 0 Then
        FechaRecepcion = LvProductos.SelectedItem.SubItems(10)
    End If
    If Len(LvProductos.SelectedItem.SubItems(11)) > 0 Then
        HoraRecepcion = LvProductos.SelectedItem.SubItems(11)
    End If
    Sql = "SELECT rec_id,rec_obs,rec_fecha,rec_hora,rec_utilizable,rec_nro_recepcion " & _
            "FROM ven_arr_detalle_recepciones " & _
                "WHERE   det_arr_id=" & LvProductos.SelectedItem
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, Me.LvRecepciones, False, True, True, False
    
End Sub

Private Sub Option1_Click()
    DTInicio.Enabled = False
    DtHasta.Enabled = False
End Sub

Private Sub Option2_Click()
    DTInicio.Enabled = True
    DtHasta.Enabled = True
End Sub

Private Sub Timer1_Timer()
    On Error Resume Next
    TxtConsulta.SetFocus
    Timer1.Enabled = False
End Sub

Private Sub TxtNombreReceptor_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub TxtNroRecepcion_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
End Sub

Private Sub TxtNumeroRecepcion_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
    If KeyAscii = 39 Then KeyAscii = 0
        On Error Resume Next
    If KeyAscii = 13 Then SendKeys ("{TAB}")
    If KeyAscii = 39 Then KeyAscii = 0
End Sub


Private Sub TxtObservacion_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub TxtRecNroRecep_GotFocus()
    En_Foco TxtRecNroRecep
End Sub

Private Sub TxtRecNroRecep_KeyPress(KeyAscii As Integer)
    KeyAscii = AceptaSoloNumeros(KeyAscii)
End Sub

Private Sub TxtRecNroRecep_Validate(Cancel As Boolean)
    If Val(TxtRecNroRecep) = 0 Then TxtRecNroRecep = 0
    
End Sub

Private Sub TxtRecObs_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub
