VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{DA729E34-689F-49EA-A856-B57046630B73}#1.0#0"; "progressbar-xp.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form VtaBoletas 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Ingreso de ventas con Boletas "
   ClientHeight    =   3375
   ClientLeft      =   2565
   ClientTop       =   1725
   ClientWidth     =   14145
   FillStyle       =   4  'Upward Diagonal
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3375
   ScaleWidth      =   14145
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox TxtExento 
      Height          =   285
      Left            =   8850
      TabIndex        =   33
      Text            =   "Text1"
      Top             =   4590
      Visible         =   0   'False
      Width           =   1365
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkVerf 
      Height          =   180
      Left            =   7935
      OleObjectBlob   =   "VtaBoletas.frx":0000
      TabIndex        =   32
      Top             =   2625
      Width           =   3195
   End
   Begin Proyecto2.XP_ProgressBar ProGres 
      Height          =   375
      Left            =   7875
      TabIndex        =   31
      Top             =   2790
      Width           =   3240
      _ExtentX        =   5715
      _ExtentY        =   661
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BrushStyle      =   0
      Color           =   12937777
      Scrolling       =   9
   End
   Begin VB.TextBox TxtIva 
      Height          =   285
      Left            =   10740
      TabIndex        =   27
      Text            =   "Text2"
      Top             =   3570
      Visible         =   0   'False
      Width           =   2325
   End
   Begin VB.TextBox TxtNeto 
      Height          =   285
      Left            =   8325
      TabIndex        =   26
      Text            =   "Text1"
      Top             =   3585
      Visible         =   0   'False
      Width           =   1800
   End
   Begin VB.Timer Timer1 
      Interval        =   10
      Left            =   360
      Top             =   5085
   End
   Begin VB.Frame FrameDatos 
      Caption         =   "Datos Contables"
      Height          =   1155
      Left            =   210
      TabIndex        =   18
      Top             =   1305
      Width           =   13785
      Begin VB.TextBox TxtSDNeto 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   12480
         TabIndex        =   8
         Tag             =   "N"
         Text            =   "0"
         Top             =   495
         Width           =   1230
      End
      Begin VB.ComboBox CboCentroCosto 
         Height          =   315
         Left            =   6375
         Style           =   2  'Dropdown List
         TabIndex        =   6
         Top             =   495
         Width           =   3150
      End
      Begin VB.ComboBox CboItemGasto 
         Height          =   315
         Left            =   9525
         Style           =   2  'Dropdown List
         TabIndex        =   7
         Top             =   495
         Visible         =   0   'False
         Width           =   2985
      End
      Begin VB.ComboBox CboCuenta 
         Height          =   315
         Left            =   105
         Style           =   2  'Dropdown List
         TabIndex        =   4
         Top             =   495
         Width           =   3150
      End
      Begin VB.ComboBox CboArea 
         Height          =   315
         Left            =   3240
         Style           =   2  'Dropdown List
         TabIndex        =   5
         Top             =   495
         Width           =   3150
      End
      Begin VB.CommandButton cmdCuenta 
         Caption         =   "Cuenta"
         Height          =   225
         Left            =   105
         TabIndex        =   19
         Top             =   255
         Width           =   870
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkItem 
         Height          =   180
         Left            =   9525
         OleObjectBlob   =   "VtaBoletas.frx":0074
         TabIndex        =   20
         Top             =   300
         Visible         =   0   'False
         Width           =   2055
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkCC 
         Height          =   180
         Left            =   6360
         OleObjectBlob   =   "VtaBoletas.frx":00EC
         TabIndex        =   21
         Top             =   300
         Width           =   2055
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkArea 
         Height          =   180
         Left            =   3285
         OleObjectBlob   =   "VtaBoletas.frx":0168
         TabIndex        =   22
         Top             =   300
         Width           =   2055
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel5 
         Height          =   180
         Left            =   12435
         OleObjectBlob   =   "VtaBoletas.frx":01CE
         TabIndex        =   23
         Top             =   300
         Width           =   1260
      End
   End
   Begin VB.Frame FrmProveedor 
      Caption         =   "Datos del documento"
      Height          =   1140
      Left            =   150
      TabIndex        =   14
      Top             =   150
      Width           =   11415
      Begin VB.TextBox TxtDesde 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   6840
         TabIndex        =   2
         Tag             =   "N"
         Text            =   "0"
         Top             =   495
         Width           =   1200
      End
      Begin VB.ComboBox CboTipoDoc 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         ItemData        =   "VtaBoletas.frx":0236
         Left            =   1575
         List            =   "VtaBoletas.frx":0243
         Style           =   2  'Dropdown List
         TabIndex        =   1
         ToolTipText     =   $"VtaBoletas.frx":0268
         Top             =   480
         Width           =   2865
      End
      Begin VB.TextBox TxtHasta 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   8085
         TabIndex        =   3
         Tag             =   "N"
         Text            =   "0"
         Top             =   495
         Width           =   1200
      End
      Begin VB.TextBox TxtUltimo_Numero 
         BackColor       =   &H00FFFF80&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   330
         Left            =   4425
         TabIndex        =   28
         Text            =   "Ultimo N�mero Ingresado--->"
         Top             =   495
         Width           =   2505
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   225
         Index           =   0
         Left            =   6750
         OleObjectBlob   =   "VtaBoletas.frx":02FD
         TabIndex        =   15
         Top             =   300
         Width           =   1290
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   225
         Index           =   0
         Left            =   1560
         OleObjectBlob   =   "VtaBoletas.frx":035E
         TabIndex        =   16
         Top             =   285
         Width           =   2250
      End
      Begin MSComCtl2.DTPicker DtFecha 
         Height          =   345
         Left            =   135
         TabIndex        =   0
         Top             =   480
         Width           =   1425
         _ExtentX        =   2514
         _ExtentY        =   609
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         CustomFormat    =   "dd-mm-yyyy hh:mm:ss"
         Format          =   92078081
         CurrentDate     =   39855
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   225
         Left            =   120
         OleObjectBlob   =   "VtaBoletas.frx":03D7
         TabIndex        =   17
         Top             =   255
         Width           =   1065
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   225
         Index           =   1
         Left            =   8400
         OleObjectBlob   =   "VtaBoletas.frx":0448
         TabIndex        =   24
         Top             =   300
         Width           =   870
      End
   End
   Begin VB.CommandButton CmdGuardar 
      Caption         =   "&Guardar"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   405
      Left            =   11385
      TabIndex        =   9
      ToolTipText     =   "Guarda documento"
      Top             =   2805
      Width           =   1215
   End
   Begin VB.CommandButton CmdSalir 
      Cancel          =   -1  'True
      Caption         =   "&Retornar"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   405
      Left            =   12750
      TabIndex        =   10
      Top             =   2805
      Width           =   1215
   End
   Begin VB.Frame FrmPeriodo 
      Caption         =   "Periodo Contable actual"
      Height          =   1155
      Left            =   11760
      TabIndex        =   11
      Top             =   150
      Width           =   2250
      Begin VB.TextBox TxtAnoContable 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1365
         Locked          =   -1  'True
         TabIndex        =   13
         TabStop         =   0   'False
         Text            =   "0"
         Top             =   480
         Width           =   780
      End
      Begin VB.TextBox TxtMesContable 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   75
         Locked          =   -1  'True
         TabIndex        =   12
         TabStop         =   0   'False
         Text            =   "0"
         Top             =   480
         Width           =   1275
      End
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   870
      OleObjectBlob   =   "VtaBoletas.frx":04A9
      Top             =   5070
   End
   Begin MSComctlLib.ListView LvDetalle 
      Height          =   5775
      Left            =   420
      TabIndex        =   29
      Top             =   4290
      Width           =   4440
      _ExtentX        =   7832
      _ExtentY        =   10186
      View            =   3
      LabelWrap       =   0   'False
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   2
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Object.Tag             =   "N109"
         Text            =   "Desde"
         Object.Width           =   2646
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Object.Tag             =   "N109"
         Text            =   "Hasta"
         Object.Width           =   2646
      EndProperty
   End
   Begin MSComctlLib.ListView LvDetalles 
      Height          =   5775
      Left            =   4965
      TabIndex        =   30
      Top             =   4275
      Width           =   2865
      _ExtentX        =   5054
      _ExtentY        =   10186
      View            =   3
      LabelWrap       =   0   'False
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   2
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Object.Tag             =   "N109"
         Text            =   "Desde"
         Object.Width           =   2646
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Object.Tag             =   "N109"
         Text            =   "Hasta"
         Object.Width           =   2646
      EndProperty
   End
   Begin VB.Label Label1 
      Caption         =   "Label1"
      Height          =   495
      Left            =   6465
      TabIndex        =   25
      Top             =   1770
      Width           =   1215
   End
End
Attribute VB_Name = "VtaBoletas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim Sm_Exento As String * 2

Private Sub CboTipoDoc_Validate(Cancel As Boolean)
    If CboTipoDoc.ListIndex = -1 Then Exit Sub
    Sql = "SELECT doc_solo_exento " & _
            "FROM sis_documentos " & _
            "WHERE doc_id=" & CboTipoDoc.ItemData(CboTipoDoc.ListIndex)
    Consulta RsTmp, Sql
    Sm_Exento = "SI"
    If RsTmp.RecordCount > 0 Then
        Sm_Exento = RsTmp!doc_solo_exento
    End If
    VerificaAfectos
    UltimaBoleta
End Sub

Private Sub cmdCuenta_Click()
    With BuscarSimple
        SG_codigo = Empty
        .Sm_Consulta = "SELECT pla_id, pla_nombre,tpo_nombre,det_nombre " & _
                       "FROM    con_plan_de_cuentas p,  con_tipo_de_cuenta t,   con_detalle_tipo_cuenta d " & _
                       "WHERE   p.tpo_id = t.tpo_id AND p.det_id = d.det_id "
        .Sm_CampoLike = "pla_nombre"
        .Im_Columnas = 2
        .Show 1
        If SG_codigo = Empty Then Exit Sub
        Busca_Id_Combo CboCuenta, Val(SG_codigo)
    End With
End Sub

Private Sub CmdGuardar_Click()
    Dim Lp_IdV As Long, Lp_pla As Long, Lp_Cen As Long, Lp_Are As Long, i As Long, X As Long
    
    Dim Bp_Utilizado As Boolean
    
    If CboTipoDoc.ListIndex = -1 Then Exit Sub
    
    If Not VerificaNros Then Exit Sub
    
    
    
    If Val(TxtSDNeto) = 0 Then
        MsgBox "Falta Valor...", vbInformation
        TxtSDNeto.SetFocus
        Exit Sub
    End If
    VerificaAfectos
    'Validar que las boletas que se estan ingresando
    'no se hayan ingresado antes.
    Sql = "SELECT no_documento, ven_boleta_hasta " & _
            "FROM ven_doc_venta " & _
            "WHERE doc_id=" & CboTipoDoc.ItemData(CboTipoDoc.ListIndex) & " AND rut_emp = '" & SP_Rut_Activo & "'"
    
    
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvDetalle, False, True, True, False
    SkVerf.Visible = True
    Me.ProGres.Visible = True
    DoEvents
    Bp_Utilizado = False
    If RsTmp.RecordCount > 0 Then
        'ReDim Lp_Posibles(Val(TxtHasta) - Val(TxtDesde))
        
        LvDetalles.ListItems.Clear
        For i = Val(TxtDesde) To Val(TxtHasta)
            LvDetalles.ListItems.Add , , i
        Next
    
        ProGres.Min = 1
        ProGres.Max = LvDetalle.ListItems.Count
        
        
        For i = 1 To LvDetalle.ListItems.Count
            ProGres.Value = (i * 100) / LvDetalle.ListItems.Count
            For X = 1 To LvDetalles.ListItems.Count
                If (Val(LvDetalles.ListItems(X)) >= Val(LvDetalle.ListItems(i))) And (Val(LvDetalles.ListItems(X)) <= Val(LvDetalle.ListItems(i).SubItems(1))) Then
                    MsgBox "Numeracion incorrecta, ya ha sido utilizada..." & vbNewLine & LvDetalles.ListItems(X), vbInformation
                    Bp_Utilizado = True
                    ProGres.Value = 1
                    SkVerf.Visible = False
                    Me.ProGres.Visible = False
                    Exit Sub
                    
                End If
            Next X
            If Bp_Utilizado Then
                Exit Sub
            End If
            
        Next i
    
    End If
    
    
    'Sql = "SELECT ven_boleta_hasta " & _
          "FROM ven_doc_venta " & _
          "WHERE rut_emp='" & SP_Rut_Activo & "' AND ven_boleta_hasta>=" & TxtDesde
  '  Consulta RsTmp, Sql
  '  If RsTmp.RecordCount > 0 Then
  '      MsgBox "Numeraci�n ya ha sido utilizada...", vbInformation
  '      TxtDesde.SetFocus
  '      Exit Sub
  '  End If
    
    
    
    
    On Error GoTo errorGraba
    cn.BeginTrans
    
        If CboCuenta.ListCount > 0 Then Lp_pla = CboCuenta.ItemData(CboCuenta.ListIndex)
        If CboArea.ListCount > 0 Then Lp_Are = CboArea.ItemData(CboArea.ListIndex)
        If Me.CboCentroCosto.ListCount > 0 Then Lp_Cen = CboCentroCosto.ItemData(CboCentroCosto.ListIndex)
        Lp_IdV = UltimoNro("ven_doc_venta", "id")
        Sql = "INSERT INTO ven_doc_venta(id,no_documento,fecha,rut_cliente,neto,iva,bruto,condicionpago,forma_pago,tipo_doc,doc_id,rut_emp,ven_boleta_hasta,exento,ven_simple,caj_id) VALUES(" & _
            Lp_IdV & "," & TxtDesde & ",'" & Fql(DtFecha) & "','11.111.111-1'," & CDbl(TxtNeto) & "," & CDbl(TxtIva) & "," & CDbl(TxtSDNeto) & ",'CONTADO','PENDIENTE','" & CboTipoDoc.Text & "'," & CboTipoDoc.ItemData(CboTipoDoc.ListIndex) & ",'" & SP_Rut_Activo & "'," & TxtHasta & "," & CDbl(TxtExento) & ",'SI'," & LG_id_Caja & ")"
        cn.Execute Sql
        
        
        
     '   Sql = "INSERT INTO ven_sin_detalle(id,vsd_neto,vsd_iva,pla_id,cen_id,are_id) VALUES(" & _
              Lp_IdV & "," & CDbl(TxtNeto) & "," & CDbl(TxtIva) & "," & Lp_pla & "," & Lp_Cen & "," & Lp_Are & ")"
        
     'aqui estabamos guardadndo id para igualarlo a ven_doc_venta, pero no sirve, era para solo contable, ahora utilizaermos no_doc y doc_id
    '    Sql = "INSERT INTO ven_detalle (id,rut_emp,pla_id,ved_precio_venta_bruto,ved_precio_venta_neto,ved_exento,cen_id,are_id,ved_iva) VALUES(" & _
                 Lp_IdV & ",'" & SP_Rut_Activo & "'," & Lp_pla & "," & CDbl(TxtSDNeto) & "," & CDbl(TxtNeto) & "," & CDbl(TxtExento) & "," & Lp_Cen & "," & Lp_Are & "," & CDbl(TxtIva) & ")"
        
        Sql = "INSERT INTO ven_detalle (rut_emp,pla_id,ved_precio_venta_bruto,ved_precio_venta_neto,ved_exento,cen_id,are_id,ved_iva,no_documento,doc_id) VALUES(" & _
                 "'" & SP_Rut_Activo & "'," & Lp_pla & "," & CDbl(TxtSDNeto) & "," & CDbl(TxtNeto) & "," & CDbl(TxtExento) & "," & Lp_Cen & "," & Lp_Are & "," & CDbl(TxtIva) & "," & TxtDesde & "," & CboTipoDoc.ItemData(CboTipoDoc.ListIndex) & ")"
        
        
        
        
        cn.Execute Sql
        
        
        
        
        'Si trabajamos con caja, incluir estas boletas, por ahora dejaremos exclusivo para _
        vegamodelo
        If SP_Rut_Activo = "96.803.210-0" Then 'vegamodelo
                      Dim Lp_IdAbo As Long
                      Dim Lp_Nro_Comprobante As Long
                     
                    '  Lp_Id_Nueva_Venta
                      TxtTotal = CDbl(TxtSDNeto)
                      TxtRut = "11.111.111-1"
                      Lp_Id_Unico_Venta = Lp_IdV
                      Lp_IdAbo = UltimoNro("cta_abonos", "abo_id")
                      Lp_Nro_Comprobante = AutoIncremento("lee", 100, , IG_id_Sucursal_Empresa)
                      If Len(TxtRut) = 0 Then TxtRut = "11.111.111-1"
                       
                      
                       
                       'Aqui registra el pago con su formas
                      Sql = "INSERT INTO cta_abonos (abo_id,abo_cli_pro,abo_rut,abo_fecha,abo_fecha_pago,abo_monto,abo_observacion,usu_nombre,suc_id,abo_obs_extra,rut_emp,abo_nro_comprobante,caj_id,abo_origen) " & _
                             "VALUES(" & Lp_IdAbo & ",'CLI','" & TxtRut & "','" & _
                             Fql(DtFecha) & "','" & Fql(DtFecha) & "'," & CDbl(TxtTotal) & _
                             ",'PAGO POS CAJA','" & LogUsuario & "'," & 0 & ",'OTRAS VENTAS','" & SP_Rut_Activo & "'," & Lp_Nro_Comprobante & "," & LG_id_Caja & ",'VENTA')"
                             
                      cn.Execute Sql
                           
                      Sql = "INSERT INTO cta_abono_documentos (abo_id,id,ctd_monto,rut_emp) VALUES "
                      ' For i = 1 To LVDetalle.ListItems.Count
                           Sql = Sql & "(" & Lp_IdAbo & "," & Lp_Id_Unico_Venta & "," & CDbl(TxtTotal) & ",'" & SP_Rut_Activo & "')"
                      ' Next
                       
                       cn.Execute Sql
                       
                       AutoIncremento "GUARDA", 100, Lp_Nro_Comprobante, IG_id_Sucursal_Empresa
                                   
                               
                       
                       'Grabaremos la(s) formas de pago en que se pago o pagaron los documentos
                       '26 Marzo 2012
                       Sql = "INSERT INTO abo_tipos_de_pagos (abo_id,mpa_id,pad_valor,caj_id) VALUES"
                       
           
                               Sql = Sql & "(" & Lp_IdAbo & ",1," & CDbl(TxtTotal) & "," & LG_id_Caja & ")"
                           
                      
                       cn.Execute Sql
                      
        End If
        
        
     
        
        
        
        
        
        
        
        
    cn.CommitTrans
    
    
    
    
    UltimaBoleta
    TxtHasta = 0
    TxtSDNeto = 0
    DtFecha.SetFocus
    ''''
    TxtUl_Nu = TxtDesde
    ''''
    SkVerf.Visible = False
    Me.ProGres.Visible = False
    Exit Sub
errorGraba:
    cn.RollbackTrans
    MsgBox "Ocurrio un error al intentar grabar..." & vbNewLine & Err.Number & " " & Err.Description, vbInformation
    SkVerf.Visible = False
    Me.ProGres.Visible = False
End Sub

Private Sub CmdSalir_Click()
    Unload Me
End Sub

Private Sub DtFecha_Validate(Cancel As Boolean)
    If Sm_PermiteFechaFueraPeriodo = "SI" Then Exit Sub
    If Month(DtFecha.Value) <> IG_Mes_Contable Or Year(DtFecha.Value) <> IG_Ano_Contable Then
        MsgBox "Fecha de venta no corresponde al periodo contable..."
        Cancel = True
    End If
End Sub

Private Sub Form_Load()
    Centrar Me, False
    Aplicar_skin Me
    DtFecha = Date
    TxtMesContable = Principal.CboMes.Text
    TxtAnoContable = Principal.CboAno.Text
    LLenarCombo CboTipoDoc, "doc_nombre", "doc_id", "sis_documentos", "doc_boleta_venta='SI' and doc_activo='SI'", "doc_orden"
   ' If CboTipoDoc.ListCount > 0 Then CboTipoDoc.ListIndex = 0
    CboTipoDoc.ListIndex = -1
    TxtDesde.Locked = False
    TxtHasta.Locked = False
    TxtSDNeto.Locked = False
    'DtFecha.Enabled = False
    CboTipoDoc.Locked = False
    CboArea.Locked = False
    CboCuenta.Locked = False
    CboCentroCosto.Locked = False
    SkVerf.Visible = False
    Me.ProGres.Visible = False
    
     LLenarCombo CboCentroCosto, "cen_nombre", "cen_id", "par_centros_de_costo", "cen_activo='SI' AND rut_emp='" & SP_Rut_Activo & "'"
    LLenarCombo CboArea, "are_nombre", "are_id", "par_areas", "are_activo='SI' AND rut_emp='" & SP_Rut_Activo & "'"
    
    LLenarCombo CboCuenta, "pla_nombre", "pla_id", "con_plan_de_cuentas", "pla_activo='SI'"
    CboCuenta.ListIndex = 0
    'CONSULTAMOS SI LA EMPRESA ACTIVA
    'LLEVA CENTRO DE COSTO, AREAS
    '24 Julio 2012
    Sql = "SELECT emp_centro_de_costos,emp_areas,emp_item_de_gastos,emp_cuenta, " & _
                "  emp_permite_venta_fuera_periodo " & _
          "FROM sis_empresas " & _
          "WHERE rut='" & SP_Rut_Activo & "'"
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
          Sm_PermiteFechaFueraPeriodo = RsTmp!emp_permite_venta_fuera_periodo
    
        If RsTmp!emp_cuenta = "NO" Then
            LLenarCombo CboCuenta, "pla_nombre", "pla_id", "con_plan_de_cuentas", "pla_id=99999"
            CboCuenta.ListIndex = 0
            CboCuenta.Visible = False
            CmdCuenta.Visible = False
        End If
        If RsTmp!emp_centro_de_costos = "NO" Then
            LLenarCombo CboCentroCosto, "cen_nombre", "cen_id", "par_centros_de_costo", "cen_id=99999"
            If CboCentroCosto.ListCount > 0 Then CboCentroCosto.ListIndex = 0
            CboCentroCosto.Visible = False
            SkCC.Visible = False
        End If
        If RsTmp!emp_areas = "NO" Then
            SkArea.Visible = False
            LLenarCombo CboArea, "are_nombre", "are_id", "par_areas", "are_id=99999"
            If CboArea.ListCount > 0 Then CboArea.ListIndex = 0
            CboArea.Visible = False
        End If
        If RsTmp!emp_item_de_gastos = "NO" Then
            SkItem.Visible = False
            LLenarCombo CboItemGasto, "gas_nombre", "gas_id", "par_item_gastos", "gas_id=99999"
            If CboItemGasto.ListCount > 0 Then CboItemGasto.ListIndex = 0
            CboItemGasto.Visible = False
        End If
    End If
    
    If DG_ID_Unico > 0 Then
        'Visualizar ingreso solamente, no permitir graba
        Me.CmdGuardar.Enabled = False
     '   Sql = "SELECT fecha,doc_id,no_documento,ven_boleta_hasta,s.pla_id,s.are_id,s.cen_id,bruto " & _
              "FROM ven_doc_venta v " & _
              "INNER JOIN ven_sin_detalle s USING(id) " & _
              "WHERE v.id=" & DG_ID_Unico
       '17 agosto 2015, ya no utilizamos ven_sin_detalle, solo ven_detalle
       Sql = "SELECT v.fecha,v.doc_id,v.no_documento,ven_boleta_hasta,s.pla_id,s.are_id,s.cen_id,bruto " & _
              "FROM ven_doc_venta v " & _
              "INNER JOIN ven_detalle s USING(id) /* ON v.no_documento=s.no_documento AND v.doc_id=s.doc_id  */ " & _
              "WHERE v.id=" & DG_ID_Unico
              
              
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
            DtFecha = RsTmp!Fecha
            Busca_Id_Combo Me.CboTipoDoc, Val(RsTmp!doc_id)
            TxtDesde = RsTmp!no_documento
            TxtHasta = RsTmp!ven_boleta_hasta
            TxtSDNeto = RsTmp!bruto
            Busca_Id_Combo CboCuenta, Val(RsTmp!pla_id)
            Busca_Id_Combo CboCentroCosto, Val(RsTmp!cen_id)
            Busca_Id_Combo CboArea, Val(RsTmp!are_id)
        End If
        TxtDesde.Locked = True
        TxtHasta.Locked = True
        TxtSDNeto.Locked = True
        'DtFecha.Enabled = False
        CboTipoDoc.Locked = True
        CboArea.Locked = True
        CboCuenta.Locked = True
        CboCentroCosto.Locked = True
    
    End If
    
    
End Sub


Private Sub Timer1_Timer()
    On Error Resume Next
    DtFecha.SetFocus
    Timer1.Enabled = False
End Sub

Private Sub TxtDesde_GotFocus()
    En_Foco Me.ActiveControl
End Sub

Private Sub TxtDesde_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
    If KeyAscii = 39 Then KeyAscii = 0
End Sub


Private Sub TxtHasta_GotFocus()
    En_Foco Me.ActiveControl
End Sub

Private Sub TxtHasta_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
    If KeyAscii = 39 Then KeyAscii = 0
End Sub

Function VerificaNros() As Boolean
    VerificaNros = True
    If Val(TxtDesde) > 0 And Val(TxtHasta) > 0 Then
        If Val(TxtHasta) < Val(TxtDesde) Then
            MsgBox "Numeraci�n incorrecta...", vbInformation
            TxtHasta.SetFocus
            VerificaNros = False
        End If
        
    Else
        MsgBox "Falta Numeraci�n...", vbInformation
        VerificaNros = False
        TxtDesde.SetFocus
    End If
End Function


Private Sub TxtSDNeto_GotFocus()
    En_Foco Me.ActiveControl
End Sub

Private Sub TxtSDNeto_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
End Sub

Private Sub TxtSDNeto_Validate(Cancel As Boolean)
    VerificaAfectos
End Sub

Private Sub VerificaAfectos()
    If Sm_Exento = "NO" Then
        If Val(TxtSDNeto) > 0 Then
            TxtNeto = NumFormat(Val(TxtSDNeto) / Val("1." & DG_IVA))
            TxtIva = NumFormat(Val(TxtSDNeto) - CDbl(TxtNeto))
            TxtExento = 0
        End If
    Else
        TxtNeto = 0
        TxtIva = 0
        TxtExento = TxtSDNeto
    End If
End Sub

Private Sub UltimaBoleta()
    'Consultamos Ultima boleta emitida y sumamos 1 como sugerencia para inicial

    If CboTipoDoc.ListIndex = -1 Then Exit Sub
    Sql = "SELECT IFNULL(ven_boleta_hasta,0) boleta " & _
          "FROM ven_doc_venta " & _
          "WHERE doc_id=" & CboTipoDoc.ItemData(CboTipoDoc.ListIndex) & " AND rut_emp='" & SP_Rut_Activo & "' AND ven_boleta_hasta>0 " & _
          "ORDER BY id DESC " & _
          "LIMIT 1"
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then TxtDesde = RsTmp!boleta + 1
End Sub

