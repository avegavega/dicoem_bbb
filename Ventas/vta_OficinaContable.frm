VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form vta_OficinaContable 
   Caption         =   "Venta Oficina Contable"
   ClientHeight    =   8310
   ClientLeft      =   1245
   ClientTop       =   1485
   ClientWidth     =   13305
   LinkTopic       =   "Form1"
   ScaleHeight     =   8310
   ScaleWidth      =   13305
   Begin VB.Timer Timer2 
      Interval        =   30
      Left            =   8760
      Top             =   7875
   End
   Begin VB.Timer Timer1 
      Interval        =   1
      Left            =   0
      Top             =   0
   End
   Begin VB.CommandButton CmdSalir 
      Cancel          =   -1  'True
      Caption         =   "Retornar"
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   11970
      TabIndex        =   90
      Top             =   7770
      Width           =   1140
   End
   Begin VB.Frame FrameDoc 
      Caption         =   "Documento"
      Height          =   1935
      Left            =   285
      TabIndex        =   58
      Top             =   5760
      Width           =   12810
      Begin VB.Frame FrmReferencia 
         Caption         =   "Referencia"
         Height          =   900
         Left            =   2460
         TabIndex        =   71
         Top             =   240
         Visible         =   0   'False
         Width           =   5175
         Begin VB.TextBox TxtDocReferencia 
            BackColor       =   &H00E0E0E0&
            Height          =   285
            Left            =   1095
            Locked          =   -1  'True
            TabIndex        =   77
            Top             =   510
            Width           =   2775
         End
         Begin VB.TextBox TxtNroReferencia 
            BackColor       =   &H00E0E0E0&
            Height          =   285
            Left            =   4305
            Locked          =   -1  'True
            TabIndex        =   76
            Tag             =   "0"
            Top             =   510
            Width           =   780
         End
         Begin VB.TextBox TxtMotivoRef 
            BackColor       =   &H00E0E0E0&
            Height          =   285
            Left            =   1110
            Locked          =   -1  'True
            TabIndex        =   75
            Text            =   "-"
            Top             =   180
            Width           =   4005
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkDocIdReferencia 
            Height          =   285
            Left            =   2400
            OleObjectBlob   =   "vta_OficinaContable.frx":0000
            TabIndex        =   72
            Top             =   1485
            Width           =   1485
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkFechaReferencia 
            Height          =   315
            Left            =   210
            OleObjectBlob   =   "vta_OficinaContable.frx":006C
            TabIndex        =   73
            Top             =   1425
            Width           =   1500
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkCodigoReferencia 
            Height          =   270
            Left            =   225
            OleObjectBlob   =   "vta_OficinaContable.frx":00DC
            TabIndex        =   74
            Top             =   1020
            Width           =   1800
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel6 
            Height          =   225
            Left            =   75
            OleObjectBlob   =   "vta_OficinaContable.frx":0168
            TabIndex        =   78
            Top             =   255
            Width           =   930
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel7 
            Height          =   225
            Left            =   3900
            OleObjectBlob   =   "vta_OficinaContable.frx":01D2
            TabIndex        =   79
            Top             =   540
            Width           =   345
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel8 
            Height          =   225
            Left            =   90
            OleObjectBlob   =   "vta_OficinaContable.frx":0236
            TabIndex        =   80
            Top             =   525
            Width           =   960
         End
      End
      Begin VB.TextBox TxtTotalDescuento 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   9045
         Locked          =   -1  'True
         TabIndex        =   70
         TabStop         =   0   'False
         Text            =   "0"
         Top             =   510
         Width           =   1065
      End
      Begin VB.TextBox TxtSubTotalMateriales 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   9045
         Locked          =   -1  'True
         TabIndex        =   69
         TabStop         =   0   'False
         Text            =   "0"
         Top             =   210
         Width           =   1065
      End
      Begin VB.CommandButton CmdEmiteDocumento 
         Caption         =   "Emitir Documento - F5"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   660
         Left            =   90
         TabIndex        =   13
         ToolTipText     =   "Si lleva control de INVENTARIO, no puede modifcar este documento. S�... puede eliminar con N.C. u otro documento"
         Top             =   1200
         Width           =   2385
      End
      Begin VB.TextBox SkTotalMateriales 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   11280
         Locked          =   -1  'True
         TabIndex        =   68
         TabStop         =   0   'False
         Text            =   "0"
         ToolTipText     =   "Nro de Documento"
         Top             =   450
         Width           =   1440
      End
      Begin VB.TextBox SkIvaMateriales 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   11280
         Locked          =   -1  'True
         TabIndex        =   67
         TabStop         =   0   'False
         Text            =   "0"
         ToolTipText     =   "Nro de Documento"
         Top             =   705
         Width           =   1440
      End
      Begin VB.TextBox SkBrutoMateriales 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   11280
         Locked          =   -1  'True
         TabIndex        =   66
         TabStop         =   0   'False
         Text            =   "0"
         ToolTipText     =   "Nro de Documento"
         Top             =   1320
         Width           =   1440
      End
      Begin VB.TextBox TxtExentos 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   11280
         Locked          =   -1  'True
         TabIndex        =   65
         TabStop         =   0   'False
         Text            =   "0"
         ToolTipText     =   "Nro de Documento"
         Top             =   180
         Width           =   1440
      End
      Begin VB.ComboBox CboFpago 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         ItemData        =   "vta_OficinaContable.frx":02A6
         Left            =   75
         List            =   "vta_OficinaContable.frx":02B0
         Style           =   2  'Dropdown List
         TabIndex        =   64
         Top             =   810
         Width           =   2355
      End
      Begin VB.TextBox TxtIVARetenido 
         Alignment       =   1  'Right Justify
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   11280
         TabIndex        =   63
         TabStop         =   0   'False
         Tag             =   "N"
         Text            =   "0"
         ToolTipText     =   "Valor IVA retenido"
         Top             =   1020
         Width           =   1440
      End
      Begin VB.ComboBox CboEntregaInmediata 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         ItemData        =   "vta_OficinaContable.frx":02C6
         Left            =   1530
         List            =   "vta_OficinaContable.frx":02D0
         Style           =   2  'Dropdown List
         TabIndex        =   62
         Top             =   270
         Visible         =   0   'False
         Width           =   870
      End
      Begin VB.TextBox txtOriginal 
         Height          =   390
         Left            =   4560
         TabIndex        =   61
         Text            =   "Text2"
         Top             =   2475
         Width           =   2160
      End
      Begin VB.TextBox txtNuevoDescuento 
         Height          =   390
         Left            =   6705
         TabIndex        =   59
         Text            =   "Text2"
         Top             =   2490
         Width           =   885
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel11 
         Height          =   195
         Left            =   7995
         OleObjectBlob   =   "vta_OficinaContable.frx":02DC
         TabIndex        =   60
         Top             =   990
         Visible         =   0   'False
         Width           =   1890
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel25 
         Height          =   255
         Left            =   7740
         OleObjectBlob   =   "vta_OficinaContable.frx":0358
         TabIndex        =   81
         Top             =   510
         Width           =   1335
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel17 
         Height          =   255
         Index           =   0
         Left            =   8955
         OleObjectBlob   =   "vta_OficinaContable.frx":03D6
         TabIndex        =   82
         Top             =   525
         Width           =   2295
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel26 
         Height          =   255
         Left            =   8355
         OleObjectBlob   =   "vta_OficinaContable.frx":0448
         TabIndex        =   83
         Top             =   210
         Width           =   735
      End
      Begin ACTIVESKINLibCtl.SkinLabel skLiva 
         Height          =   255
         Left            =   8955
         OleObjectBlob   =   "vta_OficinaContable.frx":04B6
         TabIndex        =   84
         Top             =   765
         Width           =   2295
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel17 
         Height          =   225
         Index           =   2
         Left            =   8955
         OleObjectBlob   =   "vta_OficinaContable.frx":051E
         TabIndex        =   85
         Top             =   1365
         Width           =   2295
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel17 
         Height          =   255
         Index           =   1
         Left            =   8955
         OleObjectBlob   =   "vta_OficinaContable.frx":0586
         TabIndex        =   86
         Top             =   225
         Width           =   2295
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   225
         Index           =   2
         Left            =   90
         OleObjectBlob   =   "vta_OficinaContable.frx":05F0
         TabIndex        =   87
         Top             =   615
         Width           =   1575
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel10 
         Height          =   255
         Left            =   8955
         OleObjectBlob   =   "vta_OficinaContable.frx":0661
         TabIndex        =   88
         Top             =   1095
         Width           =   2295
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   225
         Index           =   4
         Left            =   120
         OleObjectBlob   =   "vta_OficinaContable.frx":06D7
         TabIndex        =   89
         Top             =   285
         Visible         =   0   'False
         Width           =   1395
      End
   End
   Begin VB.Frame FrameCliente 
      Caption         =   "Datos del cliente"
      Height          =   1230
      Left            =   240
      TabIndex        =   36
      Top             =   1380
      Width           =   12810
      Begin VB.TextBox TxtDscto 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   11910
         MaxLength       =   6
         TabIndex        =   47
         Top             =   525
         Width           =   615
      End
      Begin VB.TextBox TxtGiro 
         Height          =   285
         Left            =   12870
         Locked          =   -1  'True
         TabIndex        =   46
         TabStop         =   0   'False
         Top             =   195
         Width           =   1815
      End
      Begin VB.CommandButton CmdBuscaCliente 
         Caption         =   "F1 - Buscar"
         Height          =   255
         Left            =   600
         TabIndex        =   45
         Top             =   510
         Width           =   1125
      End
      Begin VB.TextBox TxtCiudad 
         Height          =   285
         Left            =   9795
         Locked          =   -1  'True
         TabIndex        =   44
         TabStop         =   0   'False
         Text            =   " "
         Top             =   540
         Width           =   1335
      End
      Begin VB.TextBox TxtComuna 
         Height          =   285
         Left            =   7680
         Locked          =   -1  'True
         TabIndex        =   43
         TabStop         =   0   'False
         Text            =   " "
         Top             =   540
         Width           =   1425
      End
      Begin VB.TextBox TxtDireccion 
         Height          =   285
         Left            =   3720
         Locked          =   -1  'True
         TabIndex        =   42
         TabStop         =   0   'False
         Top             =   510
         Width           =   2775
      End
      Begin VB.TextBox TxtRazonSocial 
         BackColor       =   &H00E0E0E0&
         Height          =   285
         Left            =   3720
         Locked          =   -1  'True
         TabIndex        =   41
         TabStop         =   0   'False
         Top             =   210
         Width           =   3015
      End
      Begin VB.TextBox TxtRut 
         BackColor       =   &H0080FF80&
         Height          =   285
         Left            =   630
         TabIndex        =   5
         ToolTipText     =   "Ingrese el RUT sin puntos ni guion."
         Top             =   240
         Width           =   1575
      End
      Begin VB.ComboBox CboSucursal 
         Height          =   315
         Left            =   7680
         Style           =   2  'Dropdown List
         TabIndex        =   40
         Top             =   210
         Width           =   5070
      End
      Begin VB.TextBox TxtListaPrecio 
         BackColor       =   &H00E0E0E0&
         Height          =   285
         Left            =   135
         Locked          =   -1  'True
         TabIndex        =   39
         TabStop         =   0   'False
         Top             =   795
         Width           =   2775
      End
      Begin VB.ComboBox CboGiros 
         Height          =   315
         Left            =   3720
         Style           =   2  'Dropdown List
         TabIndex        =   38
         Top             =   825
         Width           =   9000
      End
      Begin VB.ComboBox CboRs 
         Height          =   315
         Left            =   3705
         Style           =   2  'Dropdown List
         TabIndex        =   37
         Top             =   195
         Visible         =   0   'False
         Width           =   3060
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   255
         Left            =   2025
         OleObjectBlob   =   "vta_OficinaContable.frx":0750
         TabIndex        =   48
         Top             =   540
         Width           =   1605
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   255
         Index           =   0
         Left            =   120
         OleObjectBlob   =   "vta_OficinaContable.frx":07C0
         TabIndex        =   49
         Top             =   240
         Width           =   495
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel16 
         Height          =   255
         Left            =   1935
         OleObjectBlob   =   "vta_OficinaContable.frx":0824
         TabIndex        =   50
         Top             =   240
         Width           =   1695
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel18 
         Height          =   255
         Left            =   3210
         OleObjectBlob   =   "vta_OficinaContable.frx":088E
         TabIndex        =   51
         Top             =   840
         Width           =   420
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel19 
         Height          =   255
         Left            =   6765
         OleObjectBlob   =   "vta_OficinaContable.frx":08F4
         TabIndex        =   52
         Top             =   555
         Width           =   855
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel20 
         Height          =   255
         Left            =   9135
         OleObjectBlob   =   "vta_OficinaContable.frx":095E
         TabIndex        =   53
         Top             =   570
         Width           =   735
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel21 
         Height          =   255
         Left            =   11280
         OleObjectBlob   =   "vta_OficinaContable.frx":09C8
         TabIndex        =   54
         Top             =   540
         Width           =   1395
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel5 
         Height          =   255
         Left            =   6750
         OleObjectBlob   =   "vta_OficinaContable.frx":0A52
         TabIndex        =   55
         Top             =   210
         Width           =   900
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Datos Venta"
      Height          =   1110
      Left            =   210
      TabIndex        =   28
      Top             =   270
      Width           =   12840
      Begin VB.Frame FraModalidad 
         Caption         =   "Modalidad"
         Height          =   1110
         Left            =   10710
         TabIndex        =   56
         Top             =   0
         Visible         =   0   'False
         Width           =   2070
         Begin VB.ComboBox CboModalidad 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   420
            ItemData        =   "vta_OficinaContable.frx":0AC0
            Left            =   135
            List            =   "vta_OficinaContable.frx":0ACA
            Style           =   2  'Dropdown List
            TabIndex        =   57
            Top             =   420
            Width           =   1785
         End
      End
      Begin VB.ComboBox CboVendedores 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   3450
         Style           =   2  'Dropdown List
         TabIndex        =   2
         Top             =   615
         Width           =   2880
      End
      Begin VB.Frame Frame1 
         Caption         =   "Documento de Venta"
         Height          =   1110
         Left            =   6360
         TabIndex        =   31
         Top             =   0
         Width           =   4440
         Begin VB.ComboBox CboDocVenta 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            ItemData        =   "vta_OficinaContable.frx":0ADF
            Left            =   210
            List            =   "vta_OficinaContable.frx":0AE1
            Style           =   2  'Dropdown List
            TabIndex        =   3
            ToolTipText     =   "Seleccione Documento de  venta. Ventas con Retenci�n, cuando el contribuyente recibe factura de terceros "
            Top             =   525
            Width           =   2370
         End
         Begin VB.TextBox TxtNroDocumento 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   2565
            TabIndex        =   4
            ToolTipText     =   "Nro de Documento"
            Top             =   525
            Width           =   1320
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
            Height          =   210
            Left            =   2565
            OleObjectBlob   =   "vta_OficinaContable.frx":0AE3
            TabIndex        =   32
            Top             =   300
            Width           =   1260
         End
      End
      Begin VB.Frame Frame3 
         Caption         =   "Bodega Entrega"
         Height          =   1110
         Left            =   12990
         TabIndex        =   29
         Top             =   -15
         Visible         =   0   'False
         Width           =   2490
         Begin VB.ComboBox CboBodega 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            ItemData        =   "vta_OficinaContable.frx":0B5D
            Left            =   90
            List            =   "vta_OficinaContable.frx":0B6A
            Style           =   2  'Dropdown List
            TabIndex        =   30
            ToolTipText     =   "Seleccione Bodega"
            Top             =   600
            Width           =   2355
         End
      End
      Begin VB.ComboBox CboPlazos 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         ItemData        =   "vta_OficinaContable.frx":0B8F
         Left            =   1575
         List            =   "vta_OficinaContable.frx":0B91
         Style           =   2  'Dropdown List
         TabIndex        =   1
         Top             =   615
         Width           =   1860
      End
      Begin ACTIVESKINLibCtl.SkinLabel Skvendedor 
         Height          =   180
         Left            =   3435
         OleObjectBlob   =   "vta_OficinaContable.frx":0B93
         TabIndex        =   33
         Top             =   450
         Width           =   810
      End
      Begin MSComCtl2.DTPicker DtFecha 
         Height          =   315
         Left            =   225
         TabIndex        =   0
         Top             =   615
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   556
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   273416193
         CurrentDate     =   39857
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   255
         Left            =   255
         OleObjectBlob   =   "vta_OficinaContable.frx":0BFA
         TabIndex        =   34
         Top             =   420
         Width           =   1035
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel9 
         Height          =   285
         Left            =   1605
         OleObjectBlob   =   "vta_OficinaContable.frx":0C67
         TabIndex        =   35
         Top             =   420
         Width           =   420
      End
   End
   Begin VB.Frame FrameContable 
      Caption         =   "Datos Contables"
      Height          =   3000
      Left            =   255
      TabIndex        =   14
      Top             =   2685
      Visible         =   0   'False
      Width           =   12840
      Begin VB.ComboBox CboCentroCosto 
         Height          =   315
         Left            =   4575
         Style           =   2  'Dropdown List
         TabIndex        =   8
         Top             =   555
         Width           =   1815
      End
      Begin VB.ComboBox CboCuenta 
         Height          =   315
         Left            =   60
         Style           =   2  'Dropdown List
         TabIndex        =   6
         ToolTipText     =   "Para cambiar CUENTA doble click en linea de detalle"
         Top             =   555
         Width           =   2805
      End
      Begin VB.ComboBox CboArea 
         Height          =   315
         Left            =   2835
         Style           =   2  'Dropdown List
         TabIndex        =   7
         Top             =   555
         Width           =   1755
      End
      Begin VB.CommandButton cmdCuenta 
         Caption         =   "Cuenta"
         Height          =   240
         Left            =   165
         TabIndex        =   19
         Top             =   285
         Width           =   960
      End
      Begin VB.TextBox TxtSdIva 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   11235
         TabIndex        =   11
         Tag             =   "N"
         Text            =   "0"
         Top             =   555
         Visible         =   0   'False
         Width           =   1050
      End
      Begin VB.TextBox TxtSDNeto 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   10200
         TabIndex        =   10
         Tag             =   "N"
         Text            =   "0"
         Top             =   555
         Visible         =   0   'False
         Width           =   1050
      End
      Begin VB.TextBox TxtSdExento 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   9165
         TabIndex        =   9
         Tag             =   "N"
         Text            =   "0"
         Top             =   555
         Visible         =   0   'False
         Width           =   1050
      End
      Begin VB.CommandButton CmdOkSd 
         Caption         =   "Ok"
         Height          =   300
         Left            =   12300
         TabIndex        =   12
         Top             =   555
         Visible         =   0   'False
         Width           =   405
      End
      Begin VB.CommandButton CmdModifica 
         Caption         =   "Modificar"
         Height          =   210
         Left            =   11550
         TabIndex        =   18
         ToolTipText     =   "Modifica datos contables de la linea"
         Top             =   135
         Width           =   1170
      End
      Begin VB.TextBox TxtNroCuenta 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   270
         Left            =   1155
         TabIndex        =   17
         Tag             =   "N2"
         ToolTipText     =   "Ingrese el codigo de la cuenta"
         Top             =   285
         Width           =   930
      End
      Begin VB.TextBox TxtCodigoActivo 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   6555
         Locked          =   -1  'True
         TabIndex        =   16
         TabStop         =   0   'False
         Tag             =   "N2"
         ToolTipText     =   "Codigo del activo inmovilizado"
         Top             =   555
         Width           =   615
      End
      Begin VB.TextBox TxtNombreActivo 
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   7140
         Locked          =   -1  'True
         TabIndex        =   15
         TabStop         =   0   'False
         Tag             =   "N2"
         ToolTipText     =   "Ingrese el codigo de la cuenta"
         Top             =   555
         Width           =   2040
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel14 
         Height          =   180
         Index           =   5
         Left            =   4560
         OleObjectBlob   =   "vta_OficinaContable.frx":0CC8
         TabIndex        =   20
         Top             =   360
         Width           =   2055
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel14 
         Height          =   180
         Index           =   2
         Left            =   2820
         OleObjectBlob   =   "vta_OficinaContable.frx":0D44
         TabIndex        =   21
         Top             =   360
         Width           =   2055
      End
      Begin ACTIVESKINLibCtl.SkinLabel SksdExento 
         Height          =   180
         Left            =   9195
         OleObjectBlob   =   "vta_OficinaContable.frx":0DAA
         TabIndex        =   22
         Top             =   360
         Visible         =   0   'False
         Width           =   990
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkSdneto 
         Height          =   180
         Left            =   10245
         OleObjectBlob   =   "vta_OficinaContable.frx":0E14
         TabIndex        =   23
         Top             =   360
         Visible         =   0   'False
         Width           =   990
      End
      Begin ACTIVESKINLibCtl.SkinLabel SksdIva 
         Height          =   180
         Left            =   11790
         OleObjectBlob   =   "vta_OficinaContable.frx":0E7A
         TabIndex        =   24
         Top             =   360
         Visible         =   0   'False
         Width           =   480
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkAi 
         Height          =   180
         Index           =   1
         Left            =   6615
         OleObjectBlob   =   "vta_OficinaContable.frx":0EDE
         TabIndex        =   25
         Top             =   375
         Width           =   540
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkAi 
         Height          =   180
         Index           =   2
         Left            =   7140
         OleObjectBlob   =   "vta_OficinaContable.frx":0F42
         TabIndex        =   26
         Top             =   375
         Width           =   1980
      End
      Begin MSComctlLib.ListView LvSinDetalle 
         Height          =   1965
         Left            =   45
         TabIndex        =   27
         Top             =   900
         Width           =   12630
         _ExtentX        =   22278
         _ExtentY        =   3466
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   0   'False
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   12
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "N100"
            Text            =   "id"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "T1000"
            Text            =   "Cuenta"
            Object.Width           =   3704
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "T1500"
            Text            =   "Area"
            Object.Width           =   3704
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Object.Tag             =   "T3000"
            Text            =   "Centro de Costo"
            Object.Width           =   3704
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Object.Tag             =   "T1000"
            Text            =   "Cod"
            Object.Width           =   1438
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Object.Tag             =   "T1000"
            Text            =   "Nombre Activo"
            Object.Width           =   3598
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   6
            Key             =   "otros"
            Object.Tag             =   "N100"
            Text            =   "Exento"
            Object.Width           =   1852
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   7
            Key             =   "neto"
            Object.Tag             =   "N100"
            Text            =   "Neto"
            Object.Width           =   1852
         EndProperty
         BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   8
            Key             =   "iva"
            Object.Tag             =   "N100"
            Text            =   "IVA"
            Object.Width           =   1852
         EndProperty
         BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   9
            Object.Tag             =   "N109"
            Text            =   "PLA_ID"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   10
            Object.Tag             =   "N109"
            Text            =   "ARE_ID"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(12) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   11
            Object.Tag             =   "N109"
            Text            =   "CEN_ID"
            Object.Width           =   0
         EndProperty
      End
   End
End
Attribute VB_Name = "vta_OficinaContable"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public S_Facturado As String
Dim Lm_Ids As String
Public iP_Tipo_NC As Integer
Public lP_Documento_Referenciado As Long
Public Im_Tcal As Integer
Dim Bm_Modifica_Inventario As Boolean
Dim Bm_Cambia_Precios As Boolean
Dim Sm_operador As String
Dim CodigoEncontrado As Boolean
Dim Fococodigo As Boolean
Dim Fococantidad As Boolean
Dim AdoTempVd As ADODB.Recordset
Dim RsVdirecta As Recordset
Dim Codigo As String
Dim MARCA As String
Dim Descripcion As String
Dim Descuento As String
Dim Unidades As String
Dim Precio As String
Dim PrecioRef As String
Dim SubTotal As String
Dim DescuentoTemp As String
Dim IdFamiliaR As String
Dim NomFamiliaR As String
Dim PCosto As String
Dim Bm_Requiere_Referencia As Boolean
Dim Bm_ReIntegraStock As Boolean
Dim bm_SoloVistaDoc As Boolean
Dim Bm_EditandoPorGuias As Boolean
Dim Lp_IdAbo As Long
Dim Sm_GuiasEditando As String
Dim Bm_Nueva_Ventas As Boolean
Dim Bm_Factura_por_Guias As Boolean
Dim Bm_Es_Nota_de_Credito As Boolean
Dim bm_Documento_Permite_Pago As Boolean
Dim Sm_SoloExento As String * 2
Dim Sm_RetieneIva As String * 2
Dim Bm_Cuenta As Boolean
Dim Sm_NotaDeVenta As String * 2
Dim Sm_Bitacora_Productos As String * 2
Dim Lp_Nro_Comprobante As Long
Dim Sm_Factor_IVA As Integer
Dim Sm_Con_Precios_Brutos As String * 2
Dim Sm_Avisa_Sin_Stock As String * 2
Dim Sm_PermiteFechaFueraPeriodo As String * 2
Dim Sm_SoloVendedorAsignado As String * 2
Dim Sm_PermiteCrearProductos As String * 2
Dim Lp_MontoCreditoAprobado As Long

Dim IM_TCALORIGINAL As Integer
Dim Sm_Actualiza_PrecioVenta As String * 2
Dim Lp_Id_Nueva_Venta As Long
Dim Sp_FacturaElectronica As String
Private Declare Function ShellExecute Lib "shell32.dll" Alias "ShellExecuteA" (ByVal hWnd As Long, ByVal lpOperation As String, ByVal lpFile As String, ByVal lpParameters As String, ByVal lpDirectory As String, ByVal nShowCmd As Long) As Long

Private Sub CalculaTotal()
    SkTotalMateriales = 0
    SkIvaMateriales = 0
    TxtExentos = 0
    SkBrutoMateriales = 0
        
        SkTotalMateriales = NumFormat(TotalizaColumna(LvSinDetalle, "neto"))
        SkIvaMateriales = NumFormat(TotalizaColumna(LvSinDetalle, "iva"))
        TxtExentos = NumFormat(TotalizaColumna(LvSinDetalle, "otros"))
        SkBrutoMateriales = NumFormat(CDbl(SkTotalMateriales) + CDbl(SkIvaMateriales) + CDbl(TxtExentos) - CDbl(TxtIVARetenido))
    
            


End Sub

Private Sub limpieza()
            TxtNroDocumento = Empty
            TxtRut = Empty
            TxtRazonSocial = Empty
            TxtGiro = Empty
            TxtCiudad = Empty
            TxtDireccion = Empty
            txtComuna = Empty
            CboSucursal.ListIndex = -1
            Me.LvSinDetalle.ListItems.Clear
            TxtExentos = 0
            SkTotalMateriales = 0
            SkIvaMateriales = 0
            SkBrutoMateriales = 0
            TxtMotivoRef = "-"
            TxtDocReferencia = Empty
            TxtNroReferencia = Empty
            DG_ID_Unico = 0
            bm_SoloVistaDoc = False
            CboDocVenta.ListIndex = -1
            TxtIVARetenido = 0
            DtFecha.SetFocus
End Sub

Private Sub TotalBruto()
     
     SkBrutoMateriales = NumFormat(CDbl(SkTotalMateriales) + CDbl(SkIvaMateriales) + CDbl(TxtExentos) - CDbl(TxtIVARetenido))
End Sub

Private Sub CboCentroCosto_LostFocus()
    If SP_Control_Inventario = "SI" Then If TxtCodigo.Enabled Then TxtCodigo.SetFocus Else TxtSdExento.SetFocus
    


    CmdBuscaActivo.Visible = False
    If CboCuenta.Visible And CboCuenta.ListIndex > -1 Then
        
        Sql = "SELECT pla_id " & _
                    "FROM con_plan_de_cuentas " & _
                    "WHERE tpo_id=1 AND det_id=4 AND pla_analisis='SI' AND pla_id=" & CboCuenta.ItemData(CboCuenta.ListIndex)
            Consulta RsTmp, Sql
            If RsTmp.RecordCount > 0 Then
               ' Me.CmdBuscaActivo.Visible = True
            End If
    
    End If
End Sub
Private Sub CboCuenta_Click()
        Dim Rp_Cuenta As Recordset
       ' CmdBuscaActivo.Visible = False
        If CboCuenta.Visible And CboCuenta.ListIndex > -1 Then
            TxtNroCuenta = CboCuenta.ItemData(CboCuenta.ListIndex)
            Sql = "SELECT pla_id " & _
                    "FROM con_plan_de_cuentas " & _
                    "WHERE tpo_id=1 AND det_id=4 AND pla_analisis='SI' AND pla_id=" & CboCuenta.ItemData(CboCuenta.ListIndex)
            Consulta Rp_Cuenta, Sql
            If Rp_Cuenta.RecordCount > 0 Then
                MsgBox "Venta de activo en m�dulo depreciaciones"
             '   TxtCodigo.Visible = False
            Else
              '  TxtCodigoActivo = ""
              '  TxtNombreActivo = ""
                
            End If
    
        End If
End Sub

Private Sub CboDocVenta_Click()
    FrmReferencia.Visible = False
    Bm_Requiere_Referencia = False
    Bm_ReIntegraStock = False
  
    Bm_Modifica_Inventario = True
    If CboDocVenta.ListIndex = -1 Then Exit Sub
    
    
    TxtNroDocumento = AutoIncremento("LEE", CboDocVenta.ItemData(CboDocVenta.ListIndex), , IG_id_Sucursal_Empresa)
       
   
    BuscaRefencia
    
End Sub
Private Sub CuentaLineas()
'    Me.SkCantLineas = "Linea " & LvMateriales.ListItems.Count & " / " & SkCantLineas.Tag

End Sub
Private Sub BuscaRefencia()
    Dim sp_Motivo As String
    Dim Sp_DocRefExento As String * 2
    Sp_DocRefExento = "NO"
    Bm_EditandoPorGuias = False
    CboFpago.Locked = False
    SkInfo = ""
    If bm_SoloVistaDoc Then Exit Sub
    If CboDocVenta.ListIndex = -1 Then Exit Sub
    Sql = "SELECT doc_requiere_referencia refe,doc_nota_de_credito,doc_permite_pago," & _
            "doc_solo_exento,doc_retiene_iva,doc_nota_de_venta,doc_factor_iva," & _
            "doc_cod_sii,doc_cantidad_lineas,doc_dte " & _
          "FROM sis_documentos " & _
          "WHERE doc_id=" & CboDocVenta.ItemData(CboDocVenta.ListIndex)
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
            
        Sm_Factor_IVA = RsTmp!doc_factor_iva
        If Sm_Factor_IVA = 0 Then
            'SkTotalMateriales = NUMFORMAT(CDbl(SkBrutoMateriales) - CDbl(SkIvaMateriales)
            SkIvaMateriales = 0
            SkBrutoMateriales = SkTotalMateriales
        Else
            If CDbl(SkTotalMateriales) > 0 Then
                SkBrutoMateriales = AgregarIVA(CDbl(SkTotalMateriales), Val(DG_IVA))
                SkIvaMateriales = NumFormat(CDbl(SkBrutoMateriales) - CDbl(SkTotalMateriales))
            End If
        End If
        
        Bm_Es_Nota_de_Credito = IIf(RsTmp!doc_nota_de_credito = "SI", True, False)
        bm_Documento_Permite_Pago = IIf(RsTmp!doc_permite_pago = "SI", True, False)
        Sm_SoloExento = RsTmp!doc_solo_exento
        Im_Tcal = IM_TCALORIGINAL
        
    
        
        If Sm_SoloExento = "SI" Then Im_Tcal = 3  '22 Nov 2012 si es solo exento la suma
        
        'de los productos sera exento
        Sm_NotaDeVenta = RsTmp!doc_nota_de_venta
        Sm_RetieneIva = RsTmp!doc_retiene_iva
        If Sm_SoloExento = "SI" Then
            TxtSDNeto.Enabled = False
            TxtSdIva.Enabled = False
        Else
            TxtSDNeto.Enabled = True
            TxtSdIva.Enabled = True
        End If
        
        If Sm_RetieneIva = "SI" Then
            Me.TxtIVARetenido.Enabled = True
            TxtIVARetenido.Locked = False
        Else
            TotalBruto
            TxtIVARetenido = 0
            TxtIVARetenido.Enabled = False
        End If
            
        
        If RsTmp!refe = "SI" Then
            Bm_Requiere_Referencia = True
            If CboDocVenta.ListIndex > -1 And Len(TxtRut) > 0 Then
                Sql = "SELECT v.id,fecha,CONCAT(tipo_doc,' ',IF(doc_factura_guias='SI','POR GUIA(S)','')) tipo_d ,no_documento,v.rut_cliente,IFNULL(cli_nombre_fantasia,nombre_rsocial) nombre,neto,iva,bruto " & _
                      "FROM ven_doc_venta v,sis_documentos d,maestro_clientes c " & _
                      "WHERE v.rut_emp='" & SP_Rut_Activo & "' AND c.rut_cliente=v.rut_cliente AND v.doc_id=d.doc_id AND doc_referenciable='SI' AND v.rut_cliente='" & TxtRut & "' " & _
                      "ORDER BY id DESC"
                Consulta RsTmp, Sql
                iP_Tipo_NC = 0
                If RsTmp.RecordCount > 0 Then
                    SG_codigo2 = CboDocVenta.ItemData(CboDocVenta.ListIndex)
                    vta_BuscaDocumentos.B_Seleccion = True
                    
                    If CboDocVenta.ItemData(CboDocVenta.ListIndex) = IG_id_Nota_Credito_Clientes Then
                        vta_BuscaDocumentos.B_Seleccion = False
                    End If
                    vta_BuscaDocumentos.Show 1
                    
                    
                End If
                If iP_Tipo_NC > 0 Then
                    
                    Sql = "SELECT tnc_nombre,tnc_modifica_inventario,tnc_anula_documento " & _
                          "FROM ntc_tipos " & _
                          "WHERE tnc_id=" & iP_Tipo_NC
                    Consulta RsTmp3, Sql
                    If RsTmp3.RecordCount > 0 Then
                        sp_Motivo = RsTmp3!tnc_nombre
                        TxtNroReferencia.Tag = lP_Documento_Referenciado
                        DG_ID_Unico = Me.lP_Documento_Referenciado
                        
                        'Ya tenemos identificado el numero unico del documento de venta
                        'que se esta referenciando-  Buscaremos el documento con el cual
                        'Se emitio y sabremos sus atributos.
                        
                        Sql = "SELECT doc_solo_exento,fecha,v.doc_id,doc_cod_sii " & _
                                "FROM ven_doc_venta v " & _
                                "INNER JOIN sis_documentos d USING(doc_id) " & _
                                "WHERE id=" & DG_ID_Unico
                        Consulta RsTmp, Sql
                        If RsTmp.RecordCount > 0 Then
                            Sp_DocRefExento = RsTmp!doc_solo_exento
                            SkCodigoReferencia = iP_Tipo_NC
                            If SkCodigoReferencia = 4 Then SkCodigoReferencia = 3
                            SkFechaReferencia = Format(RsTmp!Fecha, "YYYY-MM-DD")
                            SkDocIdReferencia = RsTmp!doc_cod_sii
                        End If
                        
                        
                        LaFecha = DtFecha.Value
                        'SkInfo = "POR ANULACION DE DOCUMENTO "
                        If RsTmp3!tnc_anula_documento = "SI" Then
                            'Se anulara el documento
                            'Devolver articulos al stock
                            'Abonar el total del documento a la NC
                            Bm_ReIntegraStock = True
                            bm_SoloVistaDoc = False
                            
                            CargaDocumento Sp_DocRefExento
                          
                        End If
                        If RsTmp3!tnc_anula_documento = "NO" And RsTmp3!tnc_modifica_inventario = "NO" Then
                            'ESTO SOLO CORRIGE PRECIOS NO CANTIDADES
                            Bm_Modifica_Inventario = False
                            Bm_Cambia_Precios = True
                            CargaDocumento Sp_DocRefExento
                            'Inhabilitamos entrada de nuevo codigo
                            TxtCodigo.Locked = True
                            CmdBuscaProducto.Enabled = False
                            TxtCantidad.Locked = True
                          '  If Me.CboDocVenta.ItemData(CboDocVenta.ListIndex) = IG_id_Nota_Credito_Clientes Then
                          '      Sm_operador = "<"
                          '  End If
                          '  If Me.CboDocVenta.ItemData(CboDocVenta.ListIndex) = IG_id_Nota_Debito_Clientes Then
                          '      Sm_operador = ">"
                          '  End If
                        End If
                        If RsTmp3!tnc_anula_documento = "NO" And RsTmp3!tnc_modifica_inventario = "SI" Then
                            'No anula pero modifica cantidades
                            Bm_Modifica_Inventario = True
                            Bm_Cambia_Precios = True
                            Bm_ReIntegraStock = True
                            CargaDocumento Sp_DocRefExento
                            TxtCodigo.Locked = True
                            CmdBuscaProducto.Enabled = False
                            TxtCantidad.Locked = False
                        End If
                        DtFecha = LaFecha
                    End If
                    Sql = "SELECT id, v.doc_id,no_documento,doc_nombre,doc_factura_guias,condicionpago,ven_iva_retenido,ven_plazo,ven_plazo_id " & _
                          "FROM ven_doc_venta v,sis_documentos d " & _
                          "WHERE  v.rut_emp='" & SP_Rut_Activo & "' AND v.doc_id=d.doc_id AND v.id=" & lP_Documento_Referenciado
                    Consulta RsTmp, Sql
                    If RsTmp.RecordCount > 0 Then
                        FrmReferencia.Visible = True
                        FrmReferencia.Tag = RsTmp!doc_id
                        TxtMotivoRef = sp_Motivo
                        TxtNroReferencia = RsTmp!no_documento
                        TxtDocReferencia = RsTmp!doc_nombre
                        TxtIVARetenido = RsTmp!ven_iva_retenido
                        CboFpago.Clear
                    '    Sql = "SELECT pla_id " & _
                    '            "FROM par_plazos_vencimiento " & _
                    '            "WHERE pla_dias=" & RsTmp!ven_plazo
                    '    Consulta RsTmp2, Sql
                        
                        
                        
                        'Busca_Id_Combo CboPlazos, Val(RsTmp!ven_plazo)
                        'If CboPlazos.ListIndex = -1 Then
                            For i = 0 To CboPlazos.ListCount - 1
                                If Val(RsTmp!ven_plazo_id) = Val(Right(CboPlazos.List(i), 5)) Then
                                    CboPlazos.ListIndex = i
                                    Exit For
                                End If
                            Next
                        'End If
                        If RsTmp!condicionpago = "CONTADO" Then CboFpago.AddItem "CONTADO"
                        If RsTmp!condicionpago = "CREDITO" Then CboFpago.AddItem "CREDITO"
                        CboPlazos.Enabled = False
                        CboFpago.Locked = True
                        'Aqui debemos consultar si el documento de referencia corresponde a guias en el caso que
                        'estemos haciendo una nota de credito por una factura que contiene guias.
                        If RsTmp!doc_factura_guias = "SI" Then
                            Bm_EditandoPorGuias = True
                            Sql = "SELECT CONCAT(CAST(doc_id AS CHAR),'-',CAST(no_documento AS CHAR)) guia   " & _
                                  "FROM ven_doc_venta " & _
                                  "WHERE  rut_emp='" & SP_Rut_Activo & "' AND doc_id_factura=" & RsTmp!doc_id & " AND nro_factura=" & RsTmp!no_documento
                            Consulta RsTmp2, Sql
                            If RsTmp2.RecordCount > 0 Then
                                With RsTmp2
                                    .MoveFirst
                                    Sm_GuiasEditando = "'0'"
                                    Do While Not .EOF
                                        Sm_GuiasEditando = Sm_GuiasEditando & ",'" & !guia & "'"
                                        .MoveNext
                                    Loop
                                End With
                                EditarVD RsTmp!Id
                            End If
                        End If
                        
                        
                    End If
                Else
                    Me.CboDocVenta.SetFocus
                End If
                
                
            End If
        End If
    End If
End Sub


Private Sub CboDocVenta_KeyPress(KeyAscii As Integer)
     If KeyAscii = 13 Then SendKeys ("{TAB}")
End Sub

Private Sub CboFpago_GotFocus()
    On Error Resume Next
    If Me.CboPlazos.ItemData(CboPlazos.ListIndex) = 0 Then
        CboFpago.Clear
        CboFpago.AddItem "CONTADO"
    Else
        CboFpago.Clear
        CboFpago.AddItem "CREDITO"
    End If
    CboFpago.ListIndex = 0
End Sub




Private Sub CboPlazos_Click()
    On Error Resume Next
    If Me.CboPlazos.ItemData(CboPlazos.ListIndex) = 0 Then
        CboFpago.Clear
        CboFpago.AddItem "CONTADO"
    Else
        CboFpago.Clear
        CboFpago.AddItem "CREDITO"
    End If
    CboFpago.ListIndex = 0
End Sub

Private Sub CboPlazos_KeyPress(KeyAscii As Integer)
     If KeyAscii = 13 Then SendKeys ("{TAB}")
End Sub


Private Sub CboPlazos_LostFocus()
    On Error Resume Next
    If Me.CboPlazos.ItemData(CboPlazos.ListIndex) = 0 Then
        CboFpago.Clear
        CboFpago.AddItem "CONTADO"
    Else
        CboFpago.Clear
        CboFpago.AddItem "CREDITO"
    End If
    CboFpago.ListIndex = 0
End Sub


Private Sub CboPlazos_Validate(Cancel As Boolean)
    On Error Resume Next
    If Me.CboPlazos.ItemData(CboPlazos.ListIndex) = 0 Then
        CboFpago.Clear
        CboFpago.AddItem "CONTADO"
    Else
        CboFpago.Clear
        CboFpago.AddItem "CREDITO"
    End If
    CboFpago.ListIndex = 0
End Sub


Private Sub CboRs_Click()
    If CboRs.ListIndex = -1 Then Exit Sub
    Me.TxtRazonSocial = CboRs.Text
    
End Sub

Private Sub CboSucursal_Click()
    Dim Ip_IdSuc As Integer
    If CboSucursal.ListIndex = -1 Then Exit Sub
    
    Ip_IdSuc = CboSucursal.ItemData(CboSucursal.ListIndex)
    If Ip_IdSuc = 0 Then
        Busca_Id_Combo CboGiros, 0
        Exit Sub
    End If
    Sql = "SELECT suc_direccion,suc_ciudad,suc_contacto suc_comuna,gir_id " & _
          "FROM par_sucursales " & _
          "WHERE suc_id=" & Ip_IdSuc
    Consulta RsTmp2, Sql
    If RsTmp2.RecordCount > 0 Then
        TxtDireccion = RsTmp2!suc_direccion
        
       ' If SP_Rut_Activo = "11.500.319-4" Then
            'No cambia , especial para Panaderia perla del sur
       ' Else
            'En cualquier otro caso si cambia la ciudad.
            TxtCiudad = RsTmp2!suc_ciudad
       ' End If
        txtComuna = RsTmp2!suc_comuna
         Busca_Id_Combo CboGiros, Val("" & RsTmp2!gir_id)
    End If
End Sub


Private Sub CboSucursal_GotFocus()
'    If Len(TxtRut) = 0 Then TxtRut.SetFocus
End Sub

Private Sub CboSucursal_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then TxtCodigo.SetFocus
End Sub

Private Sub CboVendedores_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then SendKeys ("{TAB}")
End Sub


Private Sub CmdAnularDocumento_Click()
    Dim Sp_Llave As String
    Dim Lp_IdMantemer As Long
    If TxtRut = "NULO" Then
            MsgBox "Documento esta NULO...", vbOKOnly + vbInformation
            Me.cmdSalir.SetFocus
            Exit Sub
    End If
    If CboDocVenta.ListIndex > -1 And Val(TxtNroDocumento) > 0 Then
        
        Sql = "SELECT tipo_movimiento " & _
                "FROM ven_doc_venta " & _
                "WHERE no_documento=" & TxtNroDocumento & " AND doc_id=" & CboDocVenta.ItemData(CboDocVenta.ListIndex) & " AND rut_emp='" & SP_Rut_Activo & "'"
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
            If RsTmp!tipo_movimiento = "NULO" Then
                MsgBox "Documento " & CboDocVenta.Text & " Nro " & TxtNroDocumento & " ya ha sido anulado...", vbInformation
                Exit Sub
            Else
               ' MsgBox "El documento existe..., buscar para anular...", vbInformation
               ' Exit Sub
            End If
        End If
        If DG_ID_Unico = 0 Then
        
                sis_InputBox.FramBox = "Ingrese llave para anulacion"
                sis_InputBox.Show 1
                Sp_Llave = UCase(SG_codigo2)
                If Len(Sp_Llave) = 0 Then Exit Sub
                
              '  If SP_Rut_Activo = "76.369.600-6" Then
              '      Sql = "SELECT usu_id,usu_nombre,usu_login " & _
                          "FROM sis_usuarios " & _
                          "WHERE usu_pwd = MD5('" & Sp_Llave & "') AND per_id=1 "
             '
             '   Else
                    Sql = "SELECT usu_id,usu_nombre,usu_login " & _
                      "FROM sis_usuarios " & _
                      "WHERE usu_pwd = MD5('" & Sp_Llave & "')"
             '   End If
                      
                Consulta RsTmp3, Sql
                If RsTmp3.RecordCount = 0 Then
                    MsgBox "Contrase�a incorrecta o su perfil no permite Anular documentos...", vbInformation
                    Exit Sub
                End If
                     
                     
              Sql = "INSERT INTO ven_doc_venta (id,fecha,doc_id,no_documento,rut_emp,rut_cliente,tipo_doc,nombre_cliente," & _
                                          "tipo_movimiento,usu_nombre,sue_id,caj_id) VALUES(" & _
                        Lp_IdMantemer & "," & _
                         "'" & Format(DtFecha.Value, "YYYY-MM-DD") & _
                         "'," & CboDocVenta.ItemData(CboDocVenta.ListIndex) & _
                         "," & TxtNroDocumento & _
                         ",'" & SP_Rut_Activo & _
                         "','NULO' " & _
                         ",'" & CboDocVenta.Text & _
                         "','ANULADO','NULO','" & Sp_Llave & "'," & IG_id_Sucursal_Empresa & "," & LG_id_Caja & ")"
            cn.Execute Sql
            AutoIncremento "GUARDA", CboDocVenta.ItemData(CboDocVenta.ListIndex), TxtNroDocumento, IG_id_Sucursal_Empresa
            Unload Me
            Exit Sub
            
        
        Else
        
    
            AnulacionDeDocumento
        
        End If
        
        Exit Sub
        
        
        'CODIGO OBSOLETO
    
        Sql = "SELECT v.id " & _
              "FROM ven_doc_venta v INNER JOIN ven_doc_venta d ON v.id=d.id_ref " & _
              "WHERE v.rut_emp='" & SP_Rut_Activo & "' AND " & _
                        "v.doc_id=" & CboDocVenta.ItemData(CboDocVenta.ListIndex) & " AND " & _
                        "v.no_documento=" & TxtNroDocumento & " AND v.rut_cliente='" & TxtRut & "'"
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
                MsgBox "Documento tiene Nota(s) de Credito o Nota(s) de debito..." & vbNewLine & "No es posible anular...", vbExclamation
                Exit Sub
        End If
        If MsgBox("Confirmar Anulacion de documento" & vbNewLine & Me.CboDocVenta.Text & vbNewLine & "Nro. " & Me.TxtNroDocumento, vbYesNo + vbQuestion) = vbNo Then Exit Sub
        
            'Anulando documento
            '27 Agosto 2011
                SG_codigo2 = Empty
                sis_InputBox.texto.PasswordChar = "*"
                sis_InputBox.FramBox = "Ingrese clave de usuario para anulacion"
                sis_InputBox.Show 1
                Sp_Llave = UCase(SG_codigo2)
                If Len(Sp_Llave) > 0 Then
                    Sql = "SELECT usu_nombre " & _
                         "FROM sis_usuarios " & _
                         "WHERE usu_pwd=MD5('" & Sp_Llave & "')"
                    Consulta RsTmp, Sql
                    If RsTmp.RecordCount > 0 Then
                        Sp_Llave = RsTmp!usu_nombre
                    Else
                        MsgBox "Llave no encontrada...", vbExclamation
                        Exit Sub
                    End If
                Else
                    Exit Sub
                End If
        'Regenerar Kardex
                AnulacionDeDocumento
                
                Exit Sub
        
        
        
        
        
         On Error GoTo Anulando
         cn.BeginTrans
                Sql = "SELECT c.id,  c.fecha,c.doc_id,doc_nombre,c.no_documento,d.codigo pro_codigo,m.descripcion,d.unidades cantidad,precio_costo/unidades unitario,doc_movimiento " & _
                      "FROM ven_detalle d " & _
                      "INNER JOIN ven_doc_venta c ON (d.doc_id=c.doc_id AND d.no_documento=c.no_documento) " & _
                      "INNER JOIN maestro_productos m ON m.codigo=d.codigo " & _
                      "INNER JOIN sis_documentos s ON c.doc_id=s.doc_id " & _
                      "WHERE c.rut_emp='" & SP_Rut_Activo & "'  AND c.no_documento=" & TxtNroDocumento & "  AND m.rut_emp='" & SP_Rut_Activo & "' AND d.rut_emp='" & SP_Rut_Activo & "' " & _
                          " AND c.doc_id=" & CboDocVenta.ItemData(CboDocVenta.ListIndex)
                Consulta RsTmp, Sql
                If RsTmp.RecordCount > 0 Then
                    Lp_IdMantemer = RsTmp!Id
                    RsTmp.MoveFirst
                    Do While Not RsTmp.EOF
                         Kardex Format(Date, "YYYY-MM-DD"), IIf(RsTmp!doc_movimiento = "ENTRADA", "SALIDA", "ENTRADA"), RsTmp!doc_id, RsTmp!no_documento, IG_id_Bodega_Ventas, RsTmp!pro_codigo, _
                          RsTmp!cantidad, "ANULA " & RsTmp!doc_nombre & " Nro:" & RsTmp!no_documento, RsTmp!unitario, RsTmp!unitario * RsTmp!cantidad, , , , , CboDocVenta.ItemData(CboDocVenta.ListIndex), , , , 0
                        RsTmp.MoveNext
                    Loop
                End If
            
            
                'CONSULTAMOS SALDO ACTUAL DE LA CTA CTE
                If DG_ID_Unico > 0 Then ctacte TxtRut, "CLI", CboDocVenta.ItemData(CboDocVenta.ListIndex), Format(DtFecha.Value, "YYYY-MM-DD"), "ANULACION " & CboDocVenta.Text & " " & TxtNroDocumento, TxtNroDocumento, CDbl(SkBrutoMateriales), True
       
            'Fin cta corriente
            
         'DEBEMOS CONSULTAR SI EXISTEN DOCUMENTOS QUE HACEN REFERENCIA AL QUE SE ESTA INGRESANDO
        Sql = "UPDATE ven_doc_venta c " & _
                     "SET id_ref=0,doc_id_factura=0,nro_factura=0 " & _
              "WHERE c.rut_emp='" & SP_Rut_Activo & "'  AND c.nro_factura=" & TxtNroDocumento & _
                          " AND c.doc_id_factura=" & CboDocVenta.ItemData(CboDocVenta.ListIndex)
        cn.Execute Sql
            
        Sql = "DELETE FROM ven_doc_venta,ven_detalle USING ven_doc_venta " & _
              "LEFT JOIN ven_detalle " & _
              "ON( ven_doc_venta.doc_id= ven_detalle.doc_id AND ven_doc_venta.no_documento= ven_detalle.no_documento) " & _
              "WHERE ven_doc_venta.rut_emp='" & SP_Rut_Activo & "' AND " & _
              "ven_doc_venta.doc_id=" & CboDocVenta.ItemData(CboDocVenta.ListIndex) & " AND ven_doc_venta.no_documento=" & TxtNroDocumento
              
         cn.Execute Sql
         
         Sql = "INSERT INTO ven_doc_venta (id,fecha,doc_id,no_documento,rut_emp,rut_cliente,tipo_doc,nombre_cliente," & _
                                          "tipo_movimiento,usu_nombre) VALUES(" & _
                        Lp_IdMantemer & "," & _
                         "'" & Format(DtFecha.Value, "YYYY-MM-DD") & _
                         "'," & CboDocVenta.ItemData(CboDocVenta.ListIndex) & _
                         "," & TxtNroDocumento & _
                         ",'" & SP_Rut_Activo & _
                         "','NULO' " & _
                         ",'" & CboDocVenta.Text & _
                         "','ANULADO','NULO','" & Sp_Llave & "')"
         cn.Execute Sql
         AutoIncremento "GUARDA", CboDocVenta.ItemData(CboDocVenta.ListIndex), TxtNroDocumento, IG_id_Sucursal_Empresa
         
        cn.CommitTrans
         MsgBox "Documento anulado " & vbNewLine & "AUTORIZADO POR:" & Sp_Llave
         Unload Me
         Exit Sub
    End If
    Exit Sub
Anulando:
    cn.RollbackTrans
    MsgBox "Hubo un error al intentar anular el documento..." & vbNewLine & Err.Number & vbNewLine & Err.Description, vbInformation
End Sub

Private Sub CmdBuscaActivo_Click()
    SG_codigo = Empty
    SG_codigo2 = CboCuenta.ItemData(CboCuenta.ListIndex)
    Con_ActivoInmovilizado.CboFiltroActivo.Enabled = False
    Con_ActivoInmovilizado.Sm_FiltroCta = " AND a.pla_id=" & SG_codigo2
    Con_ActivoInmovilizado.CmdSelecciona.Visible = True
    Con_ActivoInmovilizado.Show 1
    If Val(SG_codigo) > 0 Then
        Sql = "SELECT aim_nombre,aim_valor " & _
                "FROM con_activo_inmovilizado " & _
                "WHERE aim_id=" & Val(SG_codigo)
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
            If SP_Control_Inventario = "NO" Then
                TxtCodigoActivo = SG_codigo
                TxtNombreActivo = RsTmp!aim_nombre
                
                If TxtSDNeto.Enabled Then
                    TxtSDNeto = RsTmp!aim_valor
                    TxtSDNeto.SetFocus
                Else
                    TxtSdExento = RsTmp!aim_valor
                    TxtSdExento.SetFocus
                End If
            
            Else
                TxtCodigo = SG_codigo
                TxtDescrp = RsTmp!aim_nombre
                TxtCantidad = 1
                TxtPrecio = RsTmp!aim_valor
                txtPUni = RsTmp!aim_valor
                TxtSubTotal = RsTmp!aim_valor
            
            End If
        End If
    End If
    SG_codigo = Empty
    SG_codigo2 = Empty
End Sub

Private Sub CmdBuscaCliente_Click()
    LlamaClienteDe = "VD"
    'BuscaCliente.AdoCliente.Recordset.Filter = 0
    'Set BuscaCliente.MshClientes.DataSource = BuscaCliente.AdoCliente.Recordset
    ClienteEncontrado = False
    BuscaCliente.Show 1
    TxtRut = SG_codigo
  '  TxtDscto.SetFocus
   ' SendKeys ("{TAB}")
    
    CompruebaCambioCliente
    'Call SaldoCliente(TxtRut, TxtRazonSocial)
    TxtRut.SetFocus
End Sub


Private Sub cmdCancel_Click()
    frmEspecial.Visible = False
    VentaEspecial = False
    LvMateriales.ListItems.Clear
     frmEspecial.Visible = False
    CMDeliminaMaterial.Enabled = True
    CmdEspecial.Enabled = True
    
End Sub

Private Sub cmdCuenta_Click()
    With BuscarSimple
        SG_codigo = Empty
        .Sm_Consulta = "SELECT pla_id, pla_nombre,tpo_nombre,det_nombre " & _
                       "FROM    con_plan_de_cuentas p,  con_tipo_de_cuenta t,   con_detalle_tipo_cuenta d " & _
                       "WHERE   p.tpo_id = t.tpo_id AND p.det_id = d.det_id "
        .Sm_CampoLike = "pla_nombre"
        .Im_Columnas = 2
        .Show 1
        If SG_codigo = Empty Then Exit Sub
        Busca_Id_Combo CboCuenta, Val(SG_codigo)
    End With
End Sub




Private Sub CmdEmiteDocumento_Click()
  
    Dim ip_Sucursal_id As Integer, Ip_PlaId As Long, Ip_AreId As Long, Ip_CenId As Long, Dp_FactorComision As Double
    Dim Ip_Giro_Id As Integer, iP_RS As Integer
    Dim Sp_Modalidad As String, Ip_TipoNotaCredito As Integer
    Ip_TipoNotaCredito = 0
    Sp_Modalidad = IIf(CboModalidad.ItemData(CboModalidad.ListIndex) = 1, "VTA", "ARR")
    If CboFpago.ListIndex < 0 Then
        MsgBox "SELECCIONE FORMA DE PAGO...", vbOKOnly + vbInformation
        CboFpago.SetFocus
        Exit Sub
    End If
    
    If LvSinDetalle.ListItems.Count = 0 Then
        MsgBox "Ingrese valores...", vbInformation
        CboCuenta.SetFocus
        Exit Sub
    End If
    
    
    iP_RS = 0
    
    'validamos cantidad de lineas por documento
    
    If CboRs.Visible Then iP_RS = CboRs.ItemData(CboRs.ListIndex)
    
  
    
         If Sm_PermiteFechaFueraPeriodo = "NO" Then
            If Month(DtFecha.Value) <> IG_Mes_Contable Or Year(DtFecha.Value) <> IG_Ano_Contable Then
                 MsgBox "Fecha de venta no corresponde al periodo contable..."
                 DtFecha.SetFocus
                 Exit Sub
            End If
        End If
    
        sql2 = "SELECT pla_requiere_validacion valida " & _
            "FROM par_plazos_vencimiento " & _
            "WHERE pla_id=" & Val(Right(CboPlazos.Text, 10))
        Consulta RsTmp2, sql2
        If RsTmp2!valida = "SI" Then
            If CDbl(Me.SkTotalMateriales) > Lp_MontoCreditoAprobado Then
                MsgBox "El cliente seleccionado no esta habilitado para ventas al credito..."
                TxtRut = ""
                Exit Sub
            End If
        End If
 '       If Mid(CboPlazos.Text, 1, 7) <> "CONTADO" Then
'            If CDbl(Me.SkTotalMateriales) > Lp_MontoCreditoAprobado Then
'                MsgBox "El monto autorizado para credito es " & NumFormat(Lp_MontoCreditoAprobado) & vbNewLine & "Contacte con administracion"
'                Exit Sub
'            End If
'        End If
    
    
        On Error GoTo errorGrabar
        cn.BeginTrans
        If CboVendedores.ListIndex = -1 Then
            MsgBox "No ha seleccionado Vendedor..."
            CboVendedores.SetFocus
            cn.RollbackTrans
            Exit Sub
        End If
        
        If CboDocVenta.ListIndex = -1 Then
            MsgBox "Seleccione alg�n documento de venta...", vbOKOnly + vbInformation
            CboDocVenta.SetFocus
            cn.RollbackTrans
            Exit Sub
        ElseIf Val(TxtNroDocumento) = 0 Then
            MsgBox "No ha ingresado Nro de Documento...", vbOKOnly + vbInformation
            TxtNroDocumento.SetFocus
            cn.RollbackTrans
            Exit Sub
        End If
    

        If CboCuenta.ListIndex < 0 Or CboArea.ListIndex < 0 Or CboCentroCosto.ListIndex < 0 Then
                    MsgBox "Faltan datos contables..." & vbNewLine & "Cuenta - Area - Centro Costo", vbInformation
                    CboCuenta.SetFocus
                    cn.RollbackTrans
                    Exit Sub
        End If
 
        If CDbl(SkBrutoMateriales) < 1 Then
            MsgBox "Total de documento no es v�lido...", vbInformation
            If SP_Control_Inventario = "NO" Then SkTotalMateriales.SetFocus Else TxtCodigo.SetFocus
            cn.RollbackTrans
            Exit Sub
        End If
        
        'Calcular comision vendedor
        Dp_FactorComision = 0
        Sql = "SELECT ven_comision " & _
                "FROM par_vendedores " & _
                "WHERE ven_id=" & CboVendedores.ItemData(CboVendedores.ListIndex)
        Consulta RsTmp, Sql 'Extraemos factor
        If RsTmp.RecordCount > 0 Then Dp_FactorComision = RsTmp!ven_comision
        'Ahora viene el calculo
        If Dp_FactorComision > 0 Then
            TxtComisionVendedor = Int(SkTotalMateriales / 100 * Dp_FactorComision)
            Sql = "SELECT doc_signo_libro signo " & _
                  "FROM sis_documentos " & _
                  "WHERE doc_id=" & CboDocVenta.ItemData(CboDocVenta.ListIndex)
            Consulta RsTmp, Sql 'Si el documento es nota de credito debe dejar comision negativa
            If RsTmp.RecordCount > 0 Then If RsTmp!Signo = "-" Then TxtComisionVendedor = TxtComisionVendedor * -1
        Else
            TxtComisionVendedor = "0"
        End If
        
        
        Sql = "SELECT doc_requiere_rut,doc_mueve_inventario " & _
              "FROM sis_documentos " & _
              "WHERE doc_id=" & CboDocVenta.ItemData(CboDocVenta.ListIndex)
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
            Bm_Modifica_Inventario = IIf(RsTmp!doc_mueve_inventario = "SI", True, False)
            If RsTmp!doc_requiere_rut = "SI" Then
                If Len(TxtRut) = 0 Then
                    MsgBox "Necesita ingresar Cliente para este documento...", vbOKOnly + vbInformation
                    TxtRut.SetFocus
                    cn.RollbackTrans
                    Exit Sub
                End If
            End If
        End If
    
        If Sm_RetieneIva = "SI" Then
            If Val(TxtIVARetenido) = 0 Then
                MsgBox "Falta Valor IVA retenido...", vbExclamation + vbOKOnly
                TxtIVARetenido.SetFocus
                cn.RollbackTrans
                Exit Sub
            End If
        End If
        
    
         If CboSucursal.ListCount > 0 Then
             ip_Sucursal_id = CboSucursal.ItemData(CboSucursal.ListIndex)
         Else
             ip_Sucursal_id = 0
         End If
        
        If CboGiros.ListCount = 0 Then
            CboGiros.AddItem "PRINCIPAL"
            CboGiros.ListIndex = 0
        ElseIf CboGiros.ListIndex = -1 Then
            CboGiros.ListIndex = 0
        End If
         
         Dim Sp_Fpago As String
         Sp_Fpago = Me.CboFpago.Text
         
         
'         If SP_Control_Inventario = "NO" Then
             'sabemos que estamos editando un documento
             '25 Agosto 2011
             Ip_PlaId = CboCuenta.ItemData(CboCuenta.ListIndex)
             Ip_AreId = CboArea.ItemData(CboArea.ListIndex)
             Ip_CenId = CboCentroCosto.ItemData(CboCentroCosto.ListIndex)
             If Me.Tag = "VIEJA" Then
                Sql = "SELECT condicionpago " & _
                        "FROM ven_doc_venta " & _
                        "WHERE id=" & DG_ID_Unico & _
                        " AND rut_emp='" & SP_Rut_Activo & "'"
                Consulta RsTmp, Sql
                If RsTmp.RecordCount > 0 Then
                    If RsTmp!condicionpago = "CONTADO" Then
                        'SIGUEL
                        Sql = "SELECT abo_id " & _
                             "FROM cta_abonos a " & _
                             "INNER JOIN cta_abono_documentos d USING(abo_id) " & _
                             "WHERE d.rut_emp='" & SP_Rut_Activo & "' AND a.rut_emp='" & SP_Rut_Activo & "' AND abo_cli_pro='CLI' AND d.id=" & DG_ID_Unico
                        Consulta RsTmp, Sql
                        If RsTmp.RecordCount > 0 Then
                            'Eliminamos abnos asociados al documento cuando
                            'fue cancelado al contado
                            cn.Execute "DELETE FROM cta_abonos WHERE abo_id=" & RsTmp!abo_id
                            cn.Execute "DELETE FROM cta_abono_documentos WHERE abo_id=" & RsTmp!abo_id
                            cn.Execute "DELETE FROM abo_tipos_de_pagos WHERE abo_id=" & RsTmp!abo_id
                        End If
                    Else
                        If Mid(CboPlazos.Text, 1, 25) <> "CONTADO" Then
                            'SIGUE
                            
                        Else
                            cn.RollbackTrans
                            MsgBox "Este documento fue ingresado al CREDITO..." & vbNewLine & "No es posible cambiar su forma de pago, debe eliminarlo e ingresarlo nuevamente...", vbInformation + vbOKOnly
                            Exit Sub
                        End If
                    End If
                
                End If
             
                 Sql = "DELETE FROM ven_doc_venta " & _
                       "WHERE id=" & DG_ID_Unico
                       
                 cn.Execute Sql
                 
             End If
    
        Sql = "SELECT no_documento " & _
              "FROM ven_doc_venta " & _
              "WHERE rut_emp='" & SP_Rut_Activo & "' AND no_documento = " & TxtNroDocumento & " AND doc_id=" & CboDocVenta.ItemData(CboDocVenta.ListIndex)
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
            MsgBox "Este Nro de Documento ya ha sido utilizado ...", vbOKOnly + vbInformation
            TxtNroDocumento.SetFocus
            cn.RollbackTrans
            Exit Sub
        End If
        Timer1.Enabled = False
        bm_NoTimer = False
        
        Lp_Id_Nueva_Venta = UltimoNro("ven_doc_venta", "id")
        
        Sp_NC_Utilizada = "NO"
        If Bm_Es_Nota_de_Credito Then
            'Buscar el documento referenciado    /// 20-10-2013
            'y ver si su saldo.
            'si el saldo es mayor o igual a la NC abonar el monto de la nota de credito
            Lp_SaldoRef = SaldoDocumento(Val(lP_Documento_Referenciado), "CLI")
            
            '10-10-2013
            'si el saldo del documento es mayor a 0, abonar la nc parte de ella para completar el pago, si la
            'nc es mayor al saldo, el resto enviarlo al pozo, referenciando la nc, para poder eliminar ese
            'abono en caso que se elimine la nc
            
            '13'10'2014
            'definir el tipo de nota de credito (se utilizar en DTE)
            Ip_TipoNotaCredito = SkCodigoReferencia
            
            
            
            Dim Lp_Abono_a_Realizar As Long
            Dim Lp_Abono_a_Pozo As Long
        '    If Lp_SaldoRef >= CDbl(SkBrutoMateriales) Then
            If Lp_SaldoRef > 0 Then
                    Lp_Abono_a_Pozo = 0
                    If Lp_SaldoRef >= CDbl(SkBrutoMateriales) Then
                        Lp_Abono_a_Realizar = CDbl(SkBrutoMateriales)
                    Else
                        Lp_Abono_a_Realizar = Lp_SaldoRef
                        Lp_Abono_a_Pozo = CDbl(SkBrutoMateriales) - Lp_SaldoRef
                    End If
                    
                    'hacer abono
                    MsgBox "El monto " & NumFormat(Lp_Abono_a_Realizar) & " de esta NC se abon� al documento original...", vbInformation
                    Lp_Nro_Comprobante = AutoIncremento("lee", 200, , IG_id_Sucursal_Empresa)
                
                   
                    Bp_Comprobante_Pago = True
                    Lp_IdAbo = UltimoNro("cta_abonos", "abo_id")
                    PagoDocumento Lp_IdAbo, "CLI", TxtRut, DtFecha, CDbl(Lp_Abono_a_Realizar), 8888, "NC", "ABONO CON NC", LogUsuario, Lp_Nro_Comprobante, Lp_Id_Nueva_Venta, "VENTA"
                    Sql = "INSERT INTO cta_abono_documentos (abo_id,id,ctd_monto,rut_emp) VALUES "
                    Sql = Sql & "(" & Lp_IdAbo & "," & lP_Documento_Referenciado & "," & CDbl(Lp_Abono_a_Realizar) & ",'" & SP_Rut_Activo & "')"
                    cn.Execute Sql
                    
                    AutoIncremento "GUARDA", 200, Lp_Nro_Comprobante, IG_id_Sucursal_Empresa
                    
                    Sp_NC_Utilizada = "SI"
                    
                    
            
            Else
                    'Aqui detectamos que el el documento ya fue pagado
                    'por lo tanto el valor de esta factura pasa a favor del cliente
                    Lp_Abono_a_Pozo = CDbl(SkBrutoMateriales)
            End If
            'Aqui agragaremos al pozo el saldo de la nc
            Sql = "INSERT INTO cta_pozo (id_ref,pzo_fecha,rut_emp,pzo_cli_pro,pzo_rut,pzo_abono) " & _
                        "VALUES(" & Lp_Id_Nueva_Venta & ",'" & Fql(DtFecha) & "','" & SP_Rut_Activo & "','CLI','" & TxtRut & "'," & Lp_Abono_a_Pozo & ")"
            cn.Execute Sql
             Sp_NC_Utilizada = "SI"

            
            
            
                    'PagoDocumento Lp_Id, "PRO", Me.TxtRutProveedor, Me.DtFecha, Lp_SaldoRef, 8888, "DESCUENTA NC", "", LogUsuario,
        End If
    
            'Miercoles 2 de Febrero 2011
            'ahora procedemos a grabar el documento de venta
             
             ''aqui pregunto fomra de pago
        
        TipoDocumentoVenta = CboDocVenta.Text

        FormaDelPago = "PENDIENTE"
    
        If Val(Frame1.Tag) > 0 Then 'Si cargamos una Nota de Venta, la dejamos como terminada 5-1-2012
            cn.Execute "UPDATE ven_doc_venta SET ven_estado_nota_venta='TERMINADA' WHERE id=" & Frame1.Tag
            Frame1.Tag = 0
        End If
            
        AutoIncremento "GUARDA", CboDocVenta.ItemData(CboDocVenta.ListIndex), TxtNroDocumento, IG_id_Sucursal_Empresa
        If Len(TxtRut) = 0 Then TxtRut = "11.111.111-1"
        'aqui grabamos el documento de venta
        
        '14 Dic 2013
        'Id del plazo, identificar el id del plazo, ya que el id del combo plazo son los dias
            elplazo = Right(CboPlazos.Text, 5)
            
        '*********************************************
        
        Sql = "INSERT INTO ven_doc_venta " & _
              "(id,no_documento,fecha,rut_cliente,nombre_cliente,tipo_movimiento,neto,bruto,iva,comentario,CondicionPago," & _
              "ven_id,ven_nombre,ven_comision,forma_pago,tipo_doc,doc_id,suc_id,ven_fecha_vencimiento," & _
              "usu_nombre,id_ref,rut_emp,pla_id,are_id,cen_id,exento,ven_iva_retenido,bod_id,ven_plazo,ven_comentario," & _
              "ven_ordendecompra,ven_tipo_calculo,ven_venta_arriendo,ven_nc_utilizada,caj_id,sue_id,ven_plazo_id," & _
              "ven_entrega_inmediata,gir_id,rso_id,tnc_id,ven_simple,ven_oficina_contable) " & _
              "VALUES(" & Lp_Id_Nueva_Venta & "," & _
              CDbl(TxtNroDocumento) & ",'" & Format(DtFecha, "YYYY-MM-DD") & "','" & TxtRut & "','" & TxtRazonSocial & "','VD'," & _
              CDbl(SkTotalMateriales) & "," & CDbl(SkBrutoMateriales) & "," & CDbl(SkIvaMateriales) & "," & _
              "'" & TxtMotivoRef & "','" & Sp_Fpago & "'," & _
              CboVendedores.ItemData(CboVendedores.ListIndex) & ",'" & _
              CboVendedores.Text & "'," & _
              TxtComisionVendedor & ",'" & _
              FormaDelPago & "','" & _
              CboDocVenta.Text & "'," & _
              CboDocVenta.ItemData(CboDocVenta.ListIndex) & "," & _
              ip_Sucursal_id & ",'" & Format(DtFecha.Value + CboPlazos.ItemData(CboPlazos.ListIndex), "YYYY-MM-DD") & "','" & _
              LogUsuario & "'," & TxtNroReferencia.Tag & ",'" & SP_Rut_Activo & "'," & Ip_PlaId & "," & Ip_AreId & "," & _
              Ip_CenId & "," & CDbl(TxtExentos) & "," & CDbl(TxtIVARetenido) & "," & IG_id_Bodega_Ventas & _
              "," & CboPlazos.ItemData(CboPlazos.ListIndex) & ",'" & TxtComentario & "','" & "" & _
              "'," & Im_Tcal & ",'" & Sp_Modalidad & "','" & Sp_NC_Utilizada & "'," & LG_id_Caja & "," & IG_id_Sucursal_Empresa & "," & elplazo & ",'" & Me.CboEntregaInmediata.Text & "'," & CboGiros.ItemData(CboGiros.ListIndex) & "," & iP_RS & "," & Ip_TipoNotaCredito & ",'SI','SI')"
            
              cn.Execute Sql
              
        'Cta Cte
        'OBSOLETA no se utiliza en ningun lugar
        ' 10 Agosto 2013
        'CtaCte TxtRut, "CLI", CboDocVenta.ItemData(CboDocVenta.ListIndex), Format(DtFecha.Value, "YYYY-MM-DD"), "VENTA CON " & CboDocVenta.Text & " " & TxtNroDocumento, TxtNroDocumento, CDbl(SkBrutoMateriales), False
              
        Me.Tag = "NUEVA"
  
            'Si no lleva control de inventario limpiamos los
            'controles para ingrear un nuevo documento
            'Else ' '  ACA GRABAMOS EL DETALLE SIN DETALLE 6 SEPTIEMBRE 2011
           ' If Not Bm_Nueva_Ventas Then
           '     Sql = "DELETE FROM ven_sin_detalle WHERE id=" & Lp_Id_Nueva_Venta
           '     cn.Execute Sql
           ' End If
        
            Sql = "INSERT INTO ven_sin_detalle(id,vsd_exento,vsd_neto,vsd_iva,pla_id,are_id,cen_id,aim_id) VALUES "
            
            
            Sql = "INSERT INTO ven_detalle(no_documento,doc_id,ved_precio_venta_neto,ved_iva,ved_exento," & _
                                             "pla_id,are_id,cen_id,aim_id,rut_emp) VALUES "
            With LvSinDetalle
            
                For i = 1 To .ListItems.Count
                    sql_values = sql_values & "(" & TxtNroDocumento & "," & CboDocVenta.ItemData(CboDocVenta.ListIndex) & "," & _
                    CDbl(.ListItems(i).SubItems(7)) & "," & _
                    CDbl(.ListItems(i).SubItems(8)) & "," & _
                    CDbl(.ListItems(i).SubItems(6)) & "," & _
                    CDbl(.ListItems(i).SubItems(9)) & "," & _
                    CDbl(.ListItems(i).SubItems(10)) & "," & _
                    CDbl(.ListItems(i).SubItems(11)) & "," & _
                    Val(.ListItems(i).SubItems(4)) & ",'" & SP_Rut_Activo & "'),"
                    
                    'Tambien debemos darle la salida a los activos
                    'inmovilizados, fecha de salida y estado a inactivo.
                    
                    If Val(.ListItems(i).SubItems(4)) > 0 Then
                            cn.Execute "UPDATE con_activo_inmovilizado " & _
                                        "SET aim_fecha_egreso='" & Fql(DtFecha) & "',aim_habilitado='NO' " & _
                                        "WHERE aim_id=" & Val(.ListItems(i).SubItems(4))
                    End If
                    
                Next
            End With
            
            sql_values = Mid(sql_values, 1, Len(sql_values) - 1)
            cn.Execute Sql & sql_values
            
            
            'Exit Sub
  

'
        
        


         If CboFpago.Text = "CONTADO" Then
            
            'Aqui realizar abono a la cta cte
            '8 Octubre 2011
            'bar si este documento ya tiene un pago
            'Sql = "DELETE FROM cta_abonos WHERE "
            
            'Sql = " SELECT  v.id,   no_documento,doc_nombre,    v.rut_cliente,  IFNULL(cli_nombre_fantasia,nombre_rsocial)cliente, " & _
                    "IF(v.suc_id = 0,'CM',CONCAT(suc_ciudad,' - ',suc_direccion))sucursal,fecha, v.ven_fecha_vencimiento, " & _
                    "bruto,0 abonos, bruto saldo, 0 nuevo, v.suc_id " & _
                    "FROM ven_doc_venta v,maestro_clientes c,sis_documentos d,par_sucursales s " & _
                    "WHERE v.rut_emp = '" & SP_Rut_Activo & "' AND v.suc_id = s.suc_id AND v.rut_cliente = c.rut_cliente " & _
                    "AND v.doc_id = d.doc_id AND v.doc_id <> 11 AND v.rut_cliente = '" & TxtRut & "' AND v.id IN(" & Lp_Id_Nueva_Venta & ") " & _
                    "ORDER BY id "
            
            If LG_id_Caja = 0 Then
                
                Lp_Nro_Comprobante = AutoIncremento("LEE", 100, , IG_id_Sucursal_Empresa)
                Lp_IdAbo = UltimoNro("cta_abonos", "abo_id")
                PagoDocumento Lp_IdAbo, "CLI", Me.TxtRut, DtFecha, CDbl(Me.SkBrutoMateriales), 1, "EFECTIVO", "PAGO CONTADO", LogUsuario, Lp_Nro_Comprobante, 0, "VENTA"
                Sql = "INSERT INTO cta_abono_documentos (abo_id,id,ctd_monto,rut_emp) VALUES "
                Sql = Sql & "(" & Lp_IdAbo & "," & Lp_Id_Nueva_Venta & "," & CDbl(CDbl(Me.SkBrutoMateriales)) & ",'" & SP_Rut_Activo & "')"
                cn.Execute Sql
                AutoIncremento "GUARDAR", CboDocVenta.ItemData(CboDocVenta.ListIndex), Lp_Nro_Comprobante, IG_id_Sucursal_Empresa
            End If
        End If
        cn.CommitTrans
  
  
    
    
    'Aun no hay impresion de documento de venta sin inventario 22 Octubre 2011
  
        limpieza
        Exit Sub
  
fin:
    
    Im_Tcal = IM_TCALORIGINAL
    Unload Me
    
    Exit Sub
CancelaImpesionNV:
    'no imprime nota de venta
    Unload Me
    Exit Sub
errorGrabar:
    MsgBox "Ocurrio un error al grabar Doc. de Venta..." & vbNewLine & Err.Number & vbNewLine & Err.Description, vbInformation
    MsgBox Err.Source
    cn.RollbackTrans
End Sub
Private Sub EstableImpresora(NombreImpresora As String)
   'Seteamos al impresora.
    Dim impresora As Printer
    ' Establece la impresora que se utilizar� para imprimir
    'Recorremos el objeto Printers
    For Each impresora In Printers
        'Si es igual al contenido se�alado en el combobox
        If UCase(impresora.DeviceName) = UCase(NombreImpresora) Then
            'Lo seteamos as�.
            Set Printer = impresora
            Exit For
        End If
    Next

End Sub
Private Sub CmdModifica_Click()
    If LvMateriales.SelectedItem Is Nothing Or LvMateriales.ListItems.Count = 0 Then Exit Sub
    If Val(LvMateriales.SelectedItem) = 0 Then Exit Sub
    LvMateriales.SelectedItem.SubItems(17) = CboCuenta.ItemData(CboCuenta.ListIndex)
    LvMateriales.SelectedItem.SubItems(18) = CboArea.ItemData(CboArea.ListIndex)
    LvMateriales.SelectedItem.SubItems(19) = CboCentroCosto.ItemData(CboCentroCosto.ListIndex)
    
    
    If SP_Control_Inventario = "SI" Then
        
        Sql = "UPDATE ven_detalle " & _
              "SET pla_id=" & CboCuenta.ItemData(CboCuenta.ListIndex) & "," & _
                  "are_id=" & CboArea.ItemData(CboArea.ListIndex) & "," & _
                  "cen_id=" & CboCentroCosto.ItemData(CboCentroCosto.ListIndex) & " " & _
              "WHERE id=" & LvMateriales.SelectedItem
   Else
        Sql = "UPDATE ven_sin_detalle " & _
              "SET pla_id=" & CboCuenta.ItemData(CboCuenta.ListIndex) & "," & _
                  "are_id=" & CboArea.ItemData(CboArea.ListIndex) & "," & _
                  "cen_id=" & CboCentroCosto.ItemData(CboCentroCosto.ListIndex) & " " & _
              "WHERE vsd_id=" & LvMateriales.SelectedItem
   End If
        
    
    cn.Execute Sql
    MsgBox "Datos contables modificados...", vbInformation
    
End Sub

Private Sub CmdOkSd_Click()
    If CboCuenta.ListIndex = -1 Or CboArea.ListIndex = -1 Or CboCentroCosto.ListIndex = -1 Then
        MsgBox "Faltan datos contables...", vbInformation
        CboCuenta.SetFocus
        Exit Sub
    End If
    If CDbl(TxtSdExento) + CDbl(TxtSDNeto) + CDbl(TxtSdIvaRetenido) = 0 Then
        MsgBox "Faltan valores...", vbInformation
        TxtSDNeto.SetFocus
        Exit Sub
    End If
    
    
    
    LvSinDetalle.ListItems.Add
    LvSinDetalle.ListItems(LvSinDetalle.ListItems.Count).SubItems(1) = CboCuenta.Text
    LvSinDetalle.ListItems(LvSinDetalle.ListItems.Count).SubItems(2) = CboArea.Text
    LvSinDetalle.ListItems(LvSinDetalle.ListItems.Count).SubItems(3) = CboCentroCosto.Text
    
    LvSinDetalle.ListItems(LvSinDetalle.ListItems.Count).SubItems(4) = TxtCodigoActivo
    LvSinDetalle.ListItems(LvSinDetalle.ListItems.Count).SubItems(5) = TxtNombreActivo
    
    LvSinDetalle.ListItems(LvSinDetalle.ListItems.Count).SubItems(6) = TxtSdExento
    LvSinDetalle.ListItems(LvSinDetalle.ListItems.Count).SubItems(7) = TxtSDNeto
    LvSinDetalle.ListItems(LvSinDetalle.ListItems.Count).SubItems(8) = TxtSdIva
    
    LvSinDetalle.ListItems(LvSinDetalle.ListItems.Count).SubItems(9) = CboCuenta.ItemData(CboCuenta.ListIndex)
    LvSinDetalle.ListItems(LvSinDetalle.ListItems.Count).SubItems(10) = CboArea.ItemData(CboArea.ListIndex)
    LvSinDetalle.ListItems(LvSinDetalle.ListItems.Count).SubItems(11) = CboCentroCosto.ItemData(CboCentroCosto.ListIndex)
    
    
    TxtSdExento = 0
    TxtSDNeto = 0
    
    TxtSdIva = 0
    TxtCodigoActivo = ""
    TxtNombreActivo = ""
    'Me.CmdBuscaActivo.Visible = False
    
    CboCuenta.SetFocus
    
    SkTotalMateriales = NumFormat(TotalizaColumna(LvSinDetalle, "neto"))
    SkIvaMateriales = NumFormat(TotalizaColumna(LvSinDetalle, "iva"))
    TxtExentos = NumFormat(TotalizaColumna(LvSinDetalle, "otros"))
    SkBrutoMateriales = NumFormat(CDbl(SkTotalMateriales) + CDbl(SkIvaMateriales) + CDbl(TxtExentos) + CDbl(TxtIVARetenido))
    
    
'    calculaTotal
End Sub
Private Sub cmdSalir_Click()
    
    If MsgBox("�Esta seguro de salir de Venta?..." & vbNewLine & "Se perderan los movimientos...", vbQuestion + vbYesNo) = vbYes Then
        Unload Me
    End If
End Sub


Private Sub DtFecha_GotFocus()
    Me.DtFecha.CalendarBackColor = ClrCfoco
End Sub

Private Sub DtFecha_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then SendKeys ("{TAB}")
End Sub
Private Sub DtFecha_KeyUp(KeyCode As Integer, Shift As Integer)
    If KeyCode = 13 Then SendKeys ("{TAB}")
End Sub

Private Sub DtFecha_LostFocus()
    Me.DtFecha.CalendarBackColor = ClrSfoco
    DtSalidaMaterial = DtFecha
End Sub

Private Sub DtFecha_Validate(Cancel As Boolean)
    Exit Sub
    'no corre
    Sql = "SELECT fecha " & _
            "FROM ven_doc_venta " & _
            "WHERE rut_emp='" & SP_Rut_Activo & "' " & _
            "ORDER BY id DESC " & _
            "LIMIT 1"
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        If DtFecha.Value <> RsTmp!Fecha Then
            MsgBox "Fecha es distinta a la ultima venta ingresada.." & vbNewLine & vbNewLine & " ... (solo a modo de informacion)..", vbInformation
        End If
        
        
    End If
            
End Sub

Private Sub Form_KeyUp(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF5 Then CmdEmiteDocumento_Click
End Sub
Private Sub Form_Load()
    Sp_FacturaElectronica = ""
    CboModalidad.ListIndex = 0
    Centrar Me
    Bm_Cuenta = True
    CboEntregaInmediata.ListIndex = 0
    Bm_EditandoPorGuias = False
    
    If DG_ID_Unico > 0 Then
        'Editando
        bm_SoloVistaDoc = True
        Bm_Nueva_Ventas = False
    Else
        'nUEVO
        Bm_Nueva_Ventas = True
        bm_SoloVistaDoc = False
    End If
    If SP_Control_Inventario = "NO" Then Me.CmdModifica.Visible = False
    Bm_Factura_por_Guias = False
    Sm_operador = Empty
    S_Facturado = "NO"
    DM_Comision_Boton_Especial = False
    VentaEspecial = False
    Fococantidad = False
    Fococodigo = False
    QueActividad = 5 ' Informamos este valor a la forma de pago
                    ' para que sepa que se trata de una venta
    Aplicar_skin Me
    bm_NoTimer = True
    Me.DtFecha.Value = Date
    LaVentaEspecial = False
    LLenarCombo CboPlazos, "CONCAT(pla_nombre,'                                    ',CAST(pla_id AS CHAR))", "pla_dias", "par_plazos_vencimiento", "pla_activo='SI'", "pla_id"
    
    CboPlazos.ListIndex = 0

    LLenarCombo CboCuenta, "CONCAT(pla_nombre,'-',MID(tpo_nombre,1,2))", "pla_id", "con_plan_de_cuentas join con_tipo_de_cuenta USING(tpo_id) ", "pla_activo='SI'"
    LLenarCombo CboCentroCosto, "cen_nombre", "cen_id", "par_centros_de_costo", "cen_activo='SI' AND rut_emp='" & SP_Rut_Activo & "'"
    LLenarCombo CboArea, "are_nombre", "are_id", "par_areas", "are_activo='SI' AND rut_emp='" & SP_Rut_Activo & "'"
    LLenarCombo CboBodega, "bod_nombre", "bod_id", "par_bodegas", "bod_activo='SI' AND rut_emp='" & SP_Rut_Activo & "'", "bod_id"
    LLenarCombo CboVendedores, "ven_nombre", "ven_id", "par_vendedores", "ven_activo='SI' AND rut_emp='" & SP_Rut_Activo & "'", "ven_nombre"
    CboBodega.ListIndex = 0
    If CboVendedores.ListCount = 0 Then
        MsgBox "No hay vendedores creados, es necesario para realizar ventas...", vbInformation
        cmdSalir_Click
        Exit Sub
    End If
    
    
    CboVendedores.ListIndex = 0
    'buscamos vendedor asociado a la sucursal
    Sql = "SELECT ven_id " & _
            "FROM sis_empresas_sucursales " & _
            "WHERE sue_id=" & IG_id_Sucursal_Empresa
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then Busca_Id_Combo CboVendedores, Val(RsTmp!ven_id)
    
 
    
        CboArea.ListIndex = 0
    
        SkAi(1).Visible = False
        SkAi(2).Visible = False
        TxtCodigoActivo.Visible = False
        TxtNombreActivo.Visible = False
       
        
  '      CmdBuscaActivo.Top = CmdBuscaProducto.Top
  '      CmdBuscaActivo.Left = CmdBuscaProducto.Left
        
       '
    Me.Caption = "FORMULARIO DE VENTAS"
        
    ClienteEncontrado = False

    LLenarCombo Me.CboDocVenta, "doc_nombre", "doc_id", "sis_documentos", "doc_nota_de_credito='NO' AND doc_nota_de_debito='NO' AND doc_boleta_venta='NO' AND doc_activo='SI' AND doc_documento='VENTA' AND doc_contable='SI' AND doc_utiliza_en_caja='" & SG_Funcion_Equipo & "'", "doc_id"
    CargaDocumento 'Carga el documento
        'FrameMA.Visible = False
        'FrameDoc.Top = FrameMA.Top
        'CmdSalir.Top = FrameMA.Top + FrameDoc.Height + 200
        Me.SksdExento.Visible = True
        Me.SksdIva.Visible = True
        SkSdneto.Visible = True
        TxtSdExento.Visible = True
        TxtSDNeto.Visible = True
        TxtSdIva.Visible = True
        Me.CmdOkSd.Visible = True
        
        FrameContable.Height = 4050
       ' SkTotalMateriales.Locked = False
       ' SkBrutoMateriales.Locked = False
        Me.CmdEmiteDocumento.Caption = "Guardar Documento"
        'Me.Height = Me.CmdSalir.Top + Me.CmdSalir.Height * 2.3
        'CmdAnularDocumento.Top = CmdSalir.Top
        Centrar Me
    
    
    
    'CONSULTAMOS SI LA EMPRESA ACTIVA
    'LLEVA CENTRO DE COSTO, AREAS
    '8 OCTUBRE 2011
    Sql = "SELECT emp_centro_de_costos,emp_areas,emp_cuenta,emp_seleciona_datos_contables_venta datoscontables," & _
                  "emp_avisa_sin_stock,emp_fecha_salida_en_venta salida,emp_columna_marca_en_venta marca,emp_cuenta_ventas " & _
          "FROM sis_empresas " & _
          "WHERE rut='" & SP_Rut_Activo & "'"
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
    
        
        If RsTmp!emp_centro_de_costos = "NO" Then
            LLenarCombo CboCentroCosto, "cen_nombre", "cen_id", "par_centros_de_costo", "cen_id=99999"
            CboCentroCosto.ListIndex = 0
            LLenarCombo CboCentroCosto, "cen_nombre", "cen_id", "par_centros_de_costo", "cen_id=99999"
            CboCentroCosto.ListIndex = 0
            CboCentroCosto.Visible = False
            SkinLabel14(5).Visible = False
        Else
            FrameContable.Visible = True
        End If
        
        If RsTmp!emp_areas = "NO" Then
            LLenarCombo CboArea, "are_nombre", "are_id", "par_areas", "are_id=99999"
            CboArea.ListIndex = 0
            SkinLabel14(2).Visible = False
            
            CboArea.Visible = False
        Else
            FrameContable.Visible = True
        End If
        
        If RsTmp!emp_cuenta = "NO" Then
            LLenarCombo CboCuenta, "pla_nombre", "pla_id", "con_plan_de_cuentas", "pla_id=99999"
            CboCuenta.ListIndex = 0
            Bm_Cuenta = False
            CboCuenta.Visible = False
            cmdCuenta.Visible = False
        Else
            FrameContable.Visible = True
            
            
                Busca_Id_Combo CboCuenta, Val(RsTmp!emp_cuenta_ventas)
          If SP_Rut_Activo = "76.369.600-6" Then FrameContable.Visible = True
            
        End If
      '  If RsTmp!emp_cuenta = "NO" Then
      '      cmdCuenta.Visible = False
      '      LLenarCombo CboCuenta, "pla_nombre", "pla_id", "con_plan_de_cuentas", "pla_id=99999"
      '      CboCuenta.ListIndex = 0
      '      CboCuenta.Visible = False
      '  End If
    End If
    
  
   
   
    '2 Abril 2012 Seleccionamos documento de venta por defecto
    If Val(TxtNroDocumento) = 0 Then
    
        Sql = "SELECT par_valor " & _
              "FROM tabla_parametros " & _
              "WHERE par_id=200"
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
            If Me.CboDocVenta.ListCount > 0 Then Busca_Id_Combo CboDocVenta, Val(RsTmp!par_valor)
        End If
    End If
    '---------------------------------------------------------/
    bm_NoTimer = True
End Sub
Private Sub CargaDocumento(Optional ByVal Solo_Exento As String)
    If DG_ID_Unico > 0 Then
        EditarVD DG_ID_Unico, Solo_Exento
       ', VDdoc 'Extraemos los datos de la FACTURA o BOLETA
    ElseIf EstadoOT = "VentaDirecta" Then
     '   paso = UltimaFACTURAOBOLETA("FACTURA")
     '   paso = UltimaFACTURAOBOLETA("BOLETA")
    End If

End Sub















Private Sub Form_Unload(Cancel As Integer)
    SG_codigo = ""
    SG_codigo2 = ""
End Sub









Private Sub LvSinDetalle_DblClick()
    If LvSinDetalle.SelectedItem Is Nothing Then Exit Sub
    With LvSinDetalle.SelectedItem
        Busca_Id_Combo CboCuenta, .SubItems(9)
        Busca_Id_Combo CboArea, .SubItems(10)
        Busca_Id_Combo CboCentroCosto, .SubItems(11)
        TxtSdExento = .SubItems(6)
        TxtSDNeto = .SubItems(7)
        TxtSdIva = .SubItems(8)
        LvSinDetalle.ListItems.Remove .Index
        
        If CboArea.ListIndex = -1 Then CboArea.ListIndex = 0
        If CboCentroCosto.ListIndex = -1 Then CboCentroCosto.ListIndex = 0
    End With
    CalculaTotal
End Sub



Private Sub SkBrutoMateriales_GotFocus()
    En_Foco SkBrutoMateriales
End Sub

Private Sub SkBrutoMateriales_KeyPress(KeyAscii As Integer)
        KeyAscii = AceptaSoloNumeros(KeyAscii)
End Sub


Private Sub SkBrutoMateriales_Validate(Cancel As Boolean)
    If Val(SkBrutoMateriales) = 0 Then
        SkBrutoMateriales = 0
    Else
        SkTotalMateriales = Round(CDbl(SkBrutoMateriales) / Val("1." & DG_IVA))
        SkIvaMateriales = CDbl(SkBrutoMateriales) - CDbl(SkTotalMateriales)
        SkBrutoMateriales = NumFormat(SkBrutoMateriales)
        SkTotalMateriales = NumFormat(SkTotalMateriales)
        SkIvaMateriales = NumFormat(SkIvaMateriales)
        Me.CmdEmiteDocumento.SetFocus
    End If
End Sub

Private Sub SkIdReferencia_DragDrop(Source As Control, X As Single, Y As Single)

End Sub

Private Sub SkIvaMateriales_KeyPress(KeyAscii As Integer)
        KeyAscii = AceptaSoloNumeros(KeyAscii)
End Sub


Private Sub SkTotalMateriales_GotFocus()
    En_Foco SkTotalMateriales
End Sub

Private Sub SkTotalMateriales_KeyPress(KeyAscii As Integer)
    KeyAscii = AceptaSoloNumeros(KeyAscii)
End Sub


Private Sub SkTotalMateriales_Validate(Cancel As Boolean)
    If Val(SkTotalMateriales) = 0 Then
        SkTotalMateriales = 0
    Else
        SkBrutoMateriales = Round(CDbl(SkTotalMateriales) * Val("1." & DG_IVA))
        SkIvaMateriales = CDbl(SkBrutoMateriales) - CDbl(SkTotalMateriales)
        SkBrutoMateriales = NumFormat(SkBrutoMateriales)
        SkTotalMateriales = NumFormat(SkTotalMateriales)
        SkIvaMateriales = NumFormat(SkIvaMateriales)
    End If
End Sub


Private Sub Timer1_Timer()
    On Error GoTo ErrorTimer
    Timer1.Enabled = False
    If Not bm_NoTimer Then Exit Sub
    
    
    For Each CTEXTOMO In Controls
        If (TypeOf CTEXTOMO Is TextBox) Then
            If Me.ActiveControl.Name = CTEXTOMO.Name Then 'Foco activo
                CTEXTOMO.BackColor = IIf(CTEXTOMO.Locked, ClrDesha, ClrCfoco)
            Else
                CTEXTOMO.BackColor = IIf(CTEXTOMO.Locked, ClrDesha, ClrSfoco)
            End If
        End If
    Next
    
    'Ell timer controla la habilitacion de del boton guardar siempre y
    'cuando los campos tengan algo
    
    Exit Sub
ErrorTimer:
    'no se que pasa pero da un error
        
End Sub

Private Sub Timer2_Timer()
    On Error Resume Next
    If DG_ID_Unico > 0 Then
        cmdSalir.SetFocus
    Else
        DtFecha.SetFocus
    End If
    Timer2.Enabled = False
End Sub












Private Sub TxtDscto_GotFocus()
    En_Foco TxtDscto
End Sub

Private Sub TxtDscto_KeyPress(KeyAscii As Integer)
    KeyAscii = AceptaSoloNumeros(KeyAscii)
    If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtDscto_Validate(Cancel As Boolean)
    TxtNuevoTotal = 0
    TxtTotalDescuento = 0
    Me.SkTotalMateriales = 0
    Me.CompruebaCambioCliente

End Sub



Private Sub txtIvaRetenido_GotFocus()
    En_Foco TxtIVARetenido
End Sub


Private Sub TxtIVARetenido_KeyPress(KeyAscii As Integer)
    KeyAscii = AceptaSoloNumeros(KeyAscii)
    If KeyAscii = 39 Then KeyAscii = 0
End Sub


Private Sub txtIvaRetenido_Validate(Cancel As Boolean)
    If Val(TxtIVARetenido) = 0 Then
        TxtIVARetenido = 0
    Else
        TxtIVARetenido = NumFormat(TxtIVARetenido)
    End If
    TotalBruto
    

End Sub


Private Sub TxtNeto_Change()
    SkTotalMateriales = TxtNeto
End Sub

Private Sub TxtNeto_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
    
End Sub


Private Sub TxtNroCuenta_GotFocus()
    En_Foco TxtNroCuenta
End Sub
Private Sub TxtNroCuenta_KeyPress(KeyAscii As Integer)
    KeyAscii = AceptaSoloNumeros(KeyAscii)
    If KeyAscii = 39 Then KeyAscii = 0
End Sub
Private Sub TxtNroCuenta_Validate(Cancel As Boolean)
    If Val(TxtNroCuenta) = 0 Then Exit Sub
    Busca_Id_Combo CboCuenta, Val(TxtNroCuenta)
    If CboCuenta.ListIndex = -1 Then
        MsgBox "Cuenta no existe..."
    End If
End Sub
Private Sub TxtNroDocumento_GotFocus()
    En_Foco TxtNroDocumento
End Sub

Private Sub TxtNroDocumento_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
'    If KeyAscii = 13 Then SendKeys ("{TAB}")
    If KeyAscii = 39 Then KeyAscii = 0
End Sub








Private Sub TxtRut_GotFocus()
    En_Foco TxtRut
End Sub

Private Sub TxtRut_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
    If KeyAscii = 13 Then SendKeys ("{TAB}")
    If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtRut_KeyUp(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF1 Then CmdBuscaCliente_Click
End Sub

Private Sub TxtRut_LostFocus()
    If Len(TxtRut.Text) = 0 Then
        Me.TxtRazonSocial.Text = ""
        Me.TxtDireccion.Text = ""
        Me.TxtCiudad.Text = ""
        Me.TxtGiro.Text = ""
        Me.txtComuna.Text = ""
        Me.TxtDscto.Text = ""
        Exit Sub
    End If
    
    
    If ClienteEncontrado Then
     
        BuscaRefencia
        
    Else
       ' TxtRut.SetFocus
    End If
    
End Sub

Private Sub TxtRut_Validate(Cancel As Boolean)
    If Len(TxtRut.Text) = 0 Then Exit Sub
    If CboDocVenta.ListIndex = -1 Then
        TxtRut = ""
        MsgBox "Seleccione documento...", vbInformation
        On Error Resume Next
        CboDocVenta.SetFocus
        Exit Sub
    End If
    TxtRut.Text = Replace(TxtRut.Text, ".", "")
    TxtRut.Text = Replace(TxtRut.Text, "-", "")
    Respuesta = VerificaRut(TxtRut.Text, NuevoRut)
    CboSucursal.Clear
    
    If Respuesta = True Then
        Me.TxtRut.Text = NuevoRut
        'Buscar el cliente
        Sql = "SELECT rut_cliente,nombre_rsocial,direccion direccion_,ciudad,comuna,giro,descuento descuento_,fono," & _
                "IFNULL(lst_nombre,'LISTA PRECIO GENERAL') listaprecios,m.lst_id,ven_id,cli_monto_credito " & _
              "FROM maestro_clientes m " & _
              "INNER JOIN par_asociacion_ruts  a ON m.rut_cliente=a.rut_cli " & _
              "LEFT JOIN par_lista_precios l USING(lst_id) " & _
              "WHERE habilitado='SI' AND a.rut_emp='" & SP_Rut_Activo & "' AND rut_cliente = '" & Me.TxtRut.Text & "'"
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
                'Cliente encontrado
            With RsTmp
                Lp_MontoCreditoAprobado = !cli_monto_credito
                sql2 = "SELECT pla_requiere_validacion valida " & _
                        "FROM par_plazos_vencimiento " & _
                        "WHERE pla_id=" & Val(Right(CboPlazos.Text, 10))
                Consulta RsTmp2, sql2
                If RsTmp2!valida = "SI" Then
                    If !cli_monto_credito = 0 Then
                        If MsgBox("El cliente seleccionado no esta habilitado para ventas al credito..." & vbNewLine & _
                            "�Ver ficha ahora...?", vbQuestion + vbYesNo) = vbNo Then
                            
                            TxtRut = ""
                            Exit Sub
                        Else
                            SG_codigo = TxtRut
                            AgregoCliente.Show 1
                            Sql = "SELECT cli_monto_credito " & _
                                    "FROM maestro_clientes " & _
                                    "WHERE rut_cliente='" & TxtRut & "'"
                            Consulta RsTmp3, Sql
                            Lp_MontoCreditoAprobado = RsTmp3!cli_monto_credito
                        End If
                    End If
                End If
                TxtListaPrecio = !listaprecios
                TxtListaPrecio.Tag = !lst_id
                TxtRut.Text = !rut_cliente
                
                TxtRazonSocial.Text = !nombre_rsocial
                TxtDireccion.Text = "" & !direccion_
                TxtCiudad.Text = "" & !ciudad
                TxtCiudad.Tag = "" & !ciudad
                txtComuna.Text = "" & !comuna
                TxtGiro.Text = "" & !giro
                TxtDscto.Text = "" & !Descuento_
           '     LbTelefono.Caption = "" & !fono
                ClienteEncontrado = True
                If ClienteEncontrado = True Then
                    CompruebaCambioCliente
                    'Call SaldoCliente(TxtRut, TxtRazonSocial)
                End If
                
                Sql = "SELECT a.lst_id,lst_nombre " & _
                      "FROM par_asociacion_lista_precios  a " & _
                      "INNER JOIN par_lista_precios l USING(lst_id) " & _
                      "WHERE l.rut_emp='" & SP_Rut_Activo & "' AND cli_rut='" & TxtRut & "'"
                Consulta RsTmp2, Sql
                TxtListaPrecio.Tag = !lst_id
                TxtListaPrecio = "LISTA DE PRECIOS PRINCIPAL"
                If RsTmp2.RecordCount > 0 Then
                    TxtListaPrecio = RsTmp2!lst_nombre
                    TxtListaPrecio.Tag = RsTmp2!lst_id
                End If
                
                
                If Sm_SoloVendedorAsignado = "SI" Then
                    Busca_Id_Combo CboVendedores, Val(!ven_id)
                End If
                    
                     
                
                
                
                LLenarCombo CboSucursal, "CONCAT(suc_ciudad,' ',suc_direccion,' ',suc_contacto)", "suc_id", "par_sucursales", "suc_activo='SI' AND rut_cliente='" & TxtRut & "'", "suc_id"
                CboSucursal.AddItem "CASA MATRIZ"
                CboSucursal.ItemData(CboSucursal.ListCount - 1) = 0
                CboSucursal.ListIndex = CboSucursal.ListCount - 1
                
                LLenarCombo CboGiros, "gir_nombre", "gir_id", "par_clientes_giros", "cli_rut='" & TxtRut & "'"
                CboGiros.AddItem !giro, 0
                Busca_Id_Combo CboGiros, 0
        '        CboGiros.AddItem TxtGiro
        '        CboGiros.ItemData(CboGiros.ListCount - 1) = 0
                
                            CboRs.Clear
                        LLenarCombo CboRs, "rso_nombre", "rso_id", "par_razon_social_clientes", "rut_cliente='" & Me.TxtRut & "'"
                        If CboRs.ListCount > 0 Then
                           CboRs.Visible = True
                           CboRs.ListIndex = 0
                        Else
                            CboRs.Visible = False
                        End If
                
                If bm_SoloVistaDoc Then Exit Sub
                                
                Sql = "SELECT doc_id " & _
                      "FROM sis_documentos " & _
                      "WHERE doc_id=" & CboDocVenta.ItemData(CboDocVenta.ListIndex) & " AND doc_id IN(" & SP_Facturas_De_Ventas & ")"
                Consulta RsTmp, Sql
                If RsTmp.RecordCount > 0 Then
                    'Consultamos las guias por facturar
                    Sql = "SELECT id " & _
                             "FROM ven_doc_venta " & _
                             "WHERE rut_emp='" & SP_Rut_Activo & "' AND doc_id=" & CboDocVenta.ItemData(CboDocVenta.ListIndex) & " AND no_documento=" & TxtNroDocumento
                    Consulta RsTmp, Sql
                    If RsTmp.RecordCount > 0 Then
                        MsgBox "Nro de documento ya fue utilizado...", vbInformation
                        Exit Sub
                    End If
                        
                
                    SG_codigo2 = "SELECT id,fecha,no_documento,v.rut_cliente,nombre_cliente,neto,iva,bruto," & _
                            "IF(v.suc_id=0,'CM',CONCAT(suc_ciudad,' - ',suc_direccion)) sucursal, " & _
                            "v.suc_id " & _
                          "FROM ven_doc_venta v " & _
                          "INNER JOIN par_sucursales s USING(suc_id) " & _
                          "WHERE  rut_emp='" & SP_Rut_Activo & "' AND nro_factura=0 AND v.rut_cliente='" & TxtRut & "' AND doc_id IN(" & IG_id_GuiaClientes & ")"
                    Consulta RsTmp, SG_codigo2
                    If RsTmp.RecordCount > 0 Then
                        If MsgBox("Cliente tiene guias por facturar..." & vbNewLine & "Desea facturarlas?", vbOKCancel + vbQuestion) = vbCancel Then Exit Sub
                        
                    
                        
                        With vta_Facturacion_Guias
                            iP_RS = 0
                            If CboRs.Visible Then
                                If CboRs.ListIndex > -1 Then
                                    .I_ID_Rs = CboRs.ItemData(CboRs.ListIndex)
                                End If
                            
                            End If
                            SG_Presentacion_Factura = Empty
                            CboSucursal.Clear
                            LLenarCombo .CboSucursal, "CONCAT(suc_ciudad,' ',suc_direccion,' ',suc_contacto)", "suc_id", "par_sucursales", "suc_activo='SI' AND rut_cliente='" & TxtRut & "'", "suc_id"
                            .CboSucursal.AddItem "CASA MATRIZ"
                            .CboSucursal.ItemData(.CboSucursal.ListCount - 1) = 0
                            .CboSucursal.AddItem "TODAS"
                            .CboSucursal.ListIndex = .CboSucursal.ListCount - 1
                            
                            .S_NombreVendedor = CboVendedores.Text
                            .D_Fecha = DtFecha.Value
                            .L_Plazo = CboPlazos.ItemData(CboPlazos.ListIndex)
                            .I_Doc_Id = CboDocVenta.ItemData(CboDocVenta.ListIndex)
                            .L_NDoc = TxtNroDocumento
                            .S_rut = TxtRut
                            .S_Nombre = TxtRazonSocial
                            .I_Id_Vendedor = CboVendedores.ItemData(CboVendedores.ListIndex)
                            .S_documento = CboDocVenta.Text

                            
                            
                            vta_Facturacion_Guias.Show 1
                            If S_Facturado = "SI" Then
                                If MsgBox(CboDocVenta.Text & " Nro " & TxtNroDocumento & " Guardado correctamente" & vbNewLine & "�Imprimir? ", vbYesNo + vbQuestion) = vbYes Then
                                    Bm_Factura_por_Guias = True
                                    
                                    Dialogo.CancelError = True
                                    On Error GoTo CancelaImpesionFacturacion
                                    
                                    Dialogo.ShowPrinter
                                    
                                    
                                  
                                    
                                    
                                End If
                            End If
                            SG_codigo2 = Empty
                        End With
                    End If
                Else
                    
                End If
            End With
        Else
                'Cliente no existe aun �crearlo??
                Respuesta = MsgBox("Cliente no encontrado..." & Chr(13) & _
                "     �Desea Crear?    ", vbYesNo + vbQuestion)
                If Respuesta = 6 Then
                    'crear
                    AccionCliente = 4
                    AgregoCliente.TxtRut.Text = Me.TxtRut.Text
                    
                    AgregoCliente.TxtRut.Locked = True
                    ClienteEncontrado = False
                    AgregoCliente.Timer1.Enabled = True
            
                    AgregoCliente.Show 1
                    If ClienteEncontrado = True Then
                        TxtRut_Validate False
                        CompruebaCambioCliente
                        TxtCodigo.SetFocus
                    Else
                        TxtRut_Validate True
                    End If
                Else
                    'volver al rut
                    ClienteEncontrado = False
                    Me.TxtRut.Text = ""
                    SendKeys "+{TAB}" ' Shift TAB retrocede al control anterior segun el orden del tabindex
                End If
        
            End If
       
    Else
        Me.TxtRut.Text = ""
        On Error Resume Next
        SendKeys "+{TAB}" ' Shift TAB retrocede al control anterior segun el orden del tabindex
    End If
    Exit Sub
CancelaImpesionFacturacion:
    'No imprime
    Unload Me
End Sub



Public Sub CompruebaCambioCliente()
       '  Me.TxtPatente.SetFocus
       
      '  RecalculaGrillaMateriales
End Sub
Public Sub ActualizaNC()
        Dim lp_Promedio As Double
        With LvMateriales
            If .ListItems.Count > 0 Then
                For i = 1 To .ListItems.Count
                    If .ListItems(i).SubItems(1) <> "ZZZZZZZ1" Then
                        Sql = "UPDATE maestro_productos " & _
                              "SET stock_Actual = stock_actual -" & .ListItems(i).SubItems(6) & _
                              " WHERE  rut_emp='" & SP_Rut_Activo & "' AND  codigo='" & .ListItems(i).SubItems(1) & "'"
                        cn.Execute Sql
                        
                       '  = 0
                        lp_Promedio = CostoAVG(.ListItems(i).SubItems(1), IG_id_Bodega_Ventas)
                        
                        
                      ' MsgBox Val(CxP(Val(.ListItems(i).SubItems(6))))
                        Kardex Format(.ListItems(i).SubItems(14), "YYYY-MM-DD"), "ENTRADA", CboDocVenta.ItemData(CboDocVenta.ListIndex), _
                                       TxtNroDocumento, IG_id_Bodega_Ventas, .ListItems(i).SubItems(1), Val(CxP(Val(.ListItems(i).SubItems(6)))), _
                                       TxtMotivoRef & TxtDocReferencia & " " & TxtNroReferencia, lp_Promedio, lp_Promedio * CxP(Val(.ListItems(i).SubItems(6))), Me.TxtRut.Text, Me.TxtRazonSocial, "NO", 0, CboDocVenta.ItemData(CboDocVenta.ListIndex), , , , 0
                    End If
                Next
            End If
        End With
        '
End Sub

Function EditarVD(L_ID As Double, Optional ByVal B_SoloExento As String)
            Dim L_No_Doc As Long
            Dim Cod_Admin As String * 3
            Dim s_FacturaGuias As String * 2
            Dim Lp_Referencia As Long
            Dim Sp_Comentario As String
            Dim Ip_Suc_id As Integer
            Dim Sp_DRSoloExento As String * 2 'DR documento referenciado
            Dim Ip_GiroId As Integer, Ip_Rso As Integer
            
            
            Sp_DRSoloExento = "NO"
            
            If B_SoloExento <> Empty Then
                Sp_DRSoloExento = B_SoloExento
            End If
            
            
            FrameContable.Enabled = False
            Me.FrameDoc.Enabled = False
            FrameCliente.Enabled = False
            Frame2.Enabled = False
            
            
            If Not Bm_EditandoPorGuias Then
                Sql = "SELECT * " & _
                      "FROM ven_doc_venta " & _
                      "INNER JOIN sis_documentos d USING(doc_id) " & _
                      "WHERE id=" & L_ID
                      
            Else
                Sql = "SELECT * " & _
                      "FROM ven_doc_venta " & _
                      "INNER JOIN sis_documentos d USING(doc_id) " & _
                      "WHERE  rut_emp='" & SP_Rut_Activo & "' AND  doc_id=" & FrmReferencia.Tag & " AND no_documento=" & TxtNroReferencia
            End If
            Consulta RsTmp, Sql
            If RsTmp.RecordCount = 0 Then
                MsgBox "No se encontraron datos..."
                Exit Function
            Else
                CboRs.Visible = False
                Ip_Rso = RsTmp!rso_id
  
               
                If RsTmp!ven_venta_arriendo = "ARR" Then Me.CboModalidad.ListIndex = 1
                
                Im_Tcal = RsTmp!ven_tipo_calculo
                Busca_Id_Combo CboVendedores, Val(RsTmp!ven_id)
                TxtComentario = RsTmp!ven_comentario
                Sm_Factor_IVA = RsTmp!doc_factor_iva
                Sm_NotaDeVenta = RsTmp!doc_nota_de_venta
                If Sm_NotaDeVenta = "SI" Then Frame1.Tag = RsTmp!Id
                Ip_Suc_id = RsTmp!suc_id
                Ip_GiroId = Val("" & RsTmp!gir_id)
                L_No_Doc = RsTmp!no_documento
                IP_Doc_ID = RsTmp!doc_id
                
                If RsTmp!tnc_id > 0 Then
                    SkCodigoReferencia = RsTmp!tnc_id
                End If
             '   Me.TxtNroDocumento = L_No_Doc
             '   Busca_Id_Combo CboDocVenta, Val(IP_Doc_ID)
                
                s_FacturaGuias = RsTmp!doc_factura_guias
                If s_FacturaGuias = "SI" Then Bm_Factura_por_Guias = True Else Bm_Factura_por_Guias = False
                SkTotalMateriales.Tag = NumFormat(RsTmp!Neto)
                SkIvaMateriales = NumFormat(RsTmp!Iva)
                SkBrutoMateriales = NumFormat(RsTmp!bruto)
                TxtIVARetenido = NumFormat(RsTmp!ven_iva_retenido)
                Lp_Referencia = RsTmp!id_ref
                Sp_Comentario = "" & RsTmp!comentario
             '   Busca_Id_Combo CboPlazos, RsTmp!ven_plazo_id
                
              '  If CboPlazos.ListIndex = -1 Then
                    '8 Ene 2014
                    'Ajuste para buscar plazo de pago, despues de algunos cambios con el id y la cantidad de dias
                    For i = 0 To CboPlazos.ListCount - 1
                        If Val(Right(CboPlazos.List(i), 6)) = RsTmp!ven_plazo_id Then
                            CboPlazos.ListIndex = i
                            Exit For
                        End If
                    Next
               ' End If
            
                    Busca_Id_Combo CboCuenta, RsTmp!pla_id
                    Busca_Id_Combo CboArea, RsTmp!are_id
                    Busca_Id_Combo CboCentroCosto, RsTmp!cen_id
            
                
            End If
            DtFecha.Value = RsTmp!Fecha
            Me.TxtRut.Text = RsTmp!rut_cliente
            
            Me.TxtRazonSocial.Text = "" & RsTmp!nombre_cliente
            'para completar los datos del cliente validar el RUT
            'y mostrara los datos del cliente
            
            If RsTmp!condicionpago = "CONTADO" Then
                CboFpago.ListIndex = 0
            Else
                CboFpago.Clear
                CboFpago.AddItem "CREDITO"
                CboFpago.ListIndex = 0
            End If
            
            
            Busca_Id_Combo CboVendedores, RsTmp!ven_id


            
            
            If IP_Doc_ID = 0 Then
                TxtNroDocumento.Tag = dpago
                Sql = "SELECT * FROM ven_detalle " & _
                      "WHERE  rut_emp='" & SP_Rut_Activo & "' AND  no_documento = " & L_No_Doc & " and doc_id =" & IP_Doc_ID
            Else
                If bm_SoloVistaDoc Then
                    Busca_Id_Combo CboDocVenta, Val(IP_Doc_ID)
                    TxtNroDocumento = L_No_Doc
                    TxtRut_Validate (True)
                    CboSucursal.Clear
                    LLenarCombo CboSucursal, "CONCAT(suc_ciudad,' ',suc_direccion,' ',suc_contacto)", "suc_id", "par_sucursales", "suc_activo='SI' AND rut_cliente='" & TxtRut & "'", "suc_id"
                    CboSucursal.AddItem "CASA MATRIZ"
                    CboSucursal.ItemData(CboSucursal.ListCount - 1) = 0
                    Busca_Id_Combo CboSucursal, Val(Ip_Suc_id)
                    Busca_Id_Combo CboGiros, Val(Ip_GiroId)
                    
                End If
                
               
            End If
            
            Consulta RsTmp, Sql
                                    
            If Len(Me.TxtRut) > 0 Then
                'Datos del cliente
                Filtro = "Rut_Cliente = '" & Me.TxtRut.Text & "'"
                Sql = "SELECT rut_cliente,nombre_rsocial,direccion direccion_,ciudad,comuna,giro,descuento descuento_,fono telefono " & _
                "FROM maestro_clientes m INNER JOIN par_asociacion_ruts a ON m.rut_cliente=a.rut_cli " & _
                "WHERE a.rut_emp='" & SP_Rut_Activo & "' AND rut_cliente = '" & Me.TxtRut.Text & "'"
                Consulta RsTmp, Sql
                If RsTmp.RecordCount > 0 Then
                        'Cliente encontrado
                    With RsTmp
                        TxtRut.Text = !rut_cliente
                        
                        TxtRazonSocial.Text = !nombre_rsocial
                        TxtDireccion.Text = !direccion_
                        TxtCiudad.Text = !ciudad
                        txtComuna.Text = !comuna
                        TxtGiro.Text = !giro
                        
                        TxtDscto.Text = "" & !Descuento_
                        
                        ClienteEncontrado = True
                    End With
                End If
            End If
            
                          'Distinta razon social
                '3 mayo 2014
            LLenarCombo CboRs, "rso_nombre", "rso_id", "par_razon_social_clientes", "rut_cliente='" & TxtRut & "'"
            If CboRs.ListCount > 0 Then
                CboRs.Visible = True
                CboRs.ListIndex = 0
                Busca_Id_Combo CboRs, Val(Ip_Rso)
            End If
            
            
            

            'Sql = "SELECT  d.vsd_id,pla_nombre, are_nombre, cen_nombre,d.aim_id," & _
                        "IF(d.aim_id=0,'',(SELECT aim_nombre FROM con_activo_inmovilizado i WHERE d.aim_id=i.aim_id)) nombre_activo," & _
                        "d.vsd_exento,d.vsd_neto,d.vsd_iva,d.pla_id,d.are_id,d.cen_id " & _
                    "FROM    ven_sin_detalle d " & _
                    "left JOIN con_plan_de_cuentas c USING(pla_id) " & _
                    "left JOIN par_areas a USING(are_id) " & _
                    "left JOIN par_centros_de_costo o USING(cen_id) " & _
                    "WHERE   d.id =" & DG_ID_Unico
              Sql = "SELECT  d.id,pla_nombre, are_nombre, cen_nombre,d.aim_id," & _
                        "IF(d.aim_id=0,'',(SELECT aim_nombre FROM con_activo_inmovilizado i WHERE d.aim_id=i.aim_id)) nombre_activo," & _
                        "d.ved_exento,d.ved_precio_venta_neto,d.ved_iva,d.pla_id,d.are_id,d.cen_id " & _
                    "FROM    ven_detalle d " & _
                    "left JOIN con_plan_de_cuentas c USING(pla_id) " & _
                    "left JOIN par_areas a USING(are_id) " & _
                    "left JOIN par_centros_de_costo o USING(cen_id) " & _
                    "WHERE   d.no_documento=" & TxtNroDocumento & " AND doc_id=" & CboDocVenta.ItemData(CboDocVenta.ListIndex) & " AND d.rut_emp='" & SP_Rut_Activo & "'"
                    
                    
                Consulta RsTmp, Sql
                LLenar_Grilla RsTmp, Me, LvSinDetalle, False, True, True, False
                CalculaTotal
                Bm_Nueva_Ventas = False

            
            
            
            If bm_SoloVistaDoc Then
            
                TxtNroDocumento = L_No_Doc
                Busca_Id_Combo Me.CboDocVenta, Val(IP_Doc_ID)
                If Lp_Referencia > 0 Then
                    Sql = "SELECT no_documento,doc_nombre,comentario,doc_cod_sii,fecha " & _
                          "FROM ven_doc_venta v,sis_documentos d " & _
                          "WHERE  v.rut_emp='" & SP_Rut_Activo & "' AND v.doc_id=d.doc_id AND id=" & Lp_Referencia
                    Consulta RsTmp2, Sql
                    If RsTmp2.RecordCount > 0 Then
                        Me.FrmReferencia.Visible = True
                        TxtMotivoRef = Sp_Comentario
                        TxtNroReferencia = RsTmp2!no_documento
                        TxtDocReferencia = RsTmp2!doc_nombre
                        Me.SkDocIdReferencia = RsTmp2!doc_cod_sii
                        Me.SkFechaReferencia = Format(RsTmp2!Fecha, "YYYY-MM-DD")
                        'SkCodigoReferencia = RsTmp2!tnc_id
                    End If
                End If
                
                
                
                
            End If
            If Bm_EditandoPorGuias Then FramGuias.Visible = False
            TxtNeto = SkTotalMateriales
            
                If Sm_NotaDeVenta = "SI" Then
                    
                    CboDocVenta.Locked = False
                    TxtNroDocumento.Locked = False
                    Me.DtFecha.Enabled = True
                    
                    Me.FrameContable.Enabled = True
                    CboDocVenta.Locked = False
                    FrameCliente.Enabled = True
                    bm_SoloVistaDoc = False
                    Frame1.Caption = "Doc. Venta desde Nota de Venta " & TxtNroDocumento
                    
                Else
                    Me.FrameCliente.Enabled = False
                    Me.CboVendedores.Locked = True
                End If
            
            SkTotalMateriales = SkTotalMateriales.Tag
End Function
Function GrabaDocVd(Tabla As String, Campo As String, AnteriorNo As Double, NuevoNo As Double)
       
        Sql = "SELECT * FROM " & Tabla & " WHERE " & Campo & " = " & AnteriorNo
       paso = Consulta(AdoTempVd, Sql)
       
        Sql = "UPDATE " & Tabla & " Set " & Campo & " = " & NuevoNo & " WHERE " & Campo & " = " & AnteriorNo
        cn.Execute Sql
End Function








Private Sub TxtSdExento_GotFocus()
    En_Foco TxtSdExento
End Sub


Private Sub TxtSdExento_KeyPress(KeyAscii As Integer)
    KeyAscii = AceptaSoloNumeros(KeyAscii)
    If KeyAscii = 39 Then KeyAscii = 0
End Sub


Private Sub TxtSdExento_Validate(Cancel As Boolean)

    If SinDesborde(Me.ActiveControl) Then
        Cancel = False
    Else
        Cancel = True
        TxtSdExento = 0 'ESTE SE REEMPLAZA POR EL CONTROL ACTIVO
    End If

    If Val(TxtSdExento) = 0 Then TxtSdExento = "0" Else TxtSdExento = NumFormat(TxtSdExento)
End Sub

Private Sub TxtSdIva_GotFocus()
    En_Foco TxtSdIva
End Sub


Private Sub TxtSdIva_KeyPress(KeyAscii As Integer)
    KeyAscii = AceptaSoloNumeros(KeyAscii)
    If KeyAscii = 39 Then KeyAscii = 0
End Sub


Private Sub TxtSdIva_Validate(Cancel As Boolean)
    
    'ESTO ES PARA EVITAR ERROR POR DESBORDAMIENTO
    'EVITANDO LA CAIDA DEL SISTEMA
    If SinDesborde(Me.ActiveControl) Then
        Cancel = False
    Else
        Cancel = True
        TxtSdIva = 0 'ESTE SE REEMPLAZA POR EL CONTROL ACTIVO
    End If
    'REEMPLAZAR EL NOMBRE DEL CONTROL EJ: TXTSDIVA  , TXTNETO,
    '----------------------------------------
    
    
    If Val(TxtSdIva) = 0 Then TxtSdIva = NumFormat(TxtSdIva)
End Sub
Private Sub TxtSDNeto_GotFocus()
    En_Foco TxtSDNeto
End Sub
Private Sub TxtSDNeto_KeyPress(KeyAscii As Integer)
           KeyAscii = AceptaSoloNumeros(KeyAscii)
           If KeyAscii = 39 Then KeyAscii = 0
End Sub
Private Sub TxtSDNeto_Validate(Cancel As Boolean)
    If SinDesborde(Me.ActiveControl) Then
        Cancel = False
    Else
        Cancel = True
        TxtSDNeto = 0
    End If

    If Val(TxtSDNeto) = 0 Then
        TxtSDNeto = "0"
    Else
        TxtSDNeto = NumFormat(TxtSDNeto)
        TxtSdIva = NumFormat(CDbl(TxtSDNeto) * Val(("1." & DG_IVA)) - CDbl(TxtSDNeto))
    End If
End Sub


Private Sub DetalleParaFacturaGuias(sp_Guias As String)
    Sql = "SELECT v.id, v.codigo,'' marca,v.descripcion,v.precio_real,v.descuento,SUM(v.unidades),v.precio_final," & _
                             "SUM(v.subtotal),v.familia " & _
                      "FROM ven_detalle AS v " & _
                      "INNER JOIN maestro_productos AS p ON v.codigo = p.codigo " & _
                      "INNER JOIN ven_doc_venta d ON v.no_documento=d.no_documento AND v.doc_id=d.doc_id " & _
                      "WHERE v.rut_emp='" & SP_Rut_Activo & "' AND  d.rut_emp='" & SP_Rut_Activo & "' AND  p.rut_emp='" & SP_Rut_Activo & "' AND d.nro_factura=" & Me.TxtNroDocumento & " AND doc_id_factura=" & CboDocVenta.ItemData(CboDocVenta.ListIndex) & " " & _
                      "GROUP BY v.codigo,v.precio_final"
                          
    
     
     If Bm_Es_Nota_de_Credito Then
        
         Sql = "SELECT v.id, v.codigo,v.marca,v.descripcion,v.precio_real,v.descuento,SUM(v.unidades),v.precio_final," & _
                             "SUM(v.subtotal),v.familia " & _
                      "FROM ven_detalle AS v " & _
                      "INNER JOIN maestro_productos AS p ON v.codigo = p.codigo " & _
                      "WHERE v.rut_emp='" & SP_Rut_Activo & "' AND p.rut_emp='" & SP_Rut_Activo & "' AND no_documento=" & Me.TxtNroDocumento & " AND doc_id=" & CboDocVenta.ItemData(CboDocVenta.ListIndex) & " " & _
                      "GROUP BY v.codigo,v.precio_final"
     
     End If
    Consulta RsTmp3, Sql
     
'     LLenar_Grilla RsTmp3, Me, LvMateriales, False, True, True, False

End Sub


Private Sub AnulacionDeDocumento()
    Dim s_Inventario As String, L_Unico As Long, s_mov As String
    Dim rs_Detalle As Recordset, Sp_Llave As String, Sp_Bodega As Integer, Lp_IdAbono As Long
    Dim s_FacturaGuias As String * 2
    If Principal.CmdMenu(8).Visible = True Then
        If ConsultaCentralizado("2,10", IG_Mes_Contable, IG_Ano_Contable) Then
            MsgBox "periodo contable ya ha sido centralizado, " & vbNewLine & "No puede modificar"
            Exit Sub
        End If
    End If
    Dim Sp_Codigos As String
    
    L_Unico = DG_ID_Unico
    SG_codigo2 = Empty
    
    Sql = "SELECT nro_factura,doc_id_factura " & _
            "FROM ven_doc_venta " & _
            "WHERE id=" & L_Unico & " AND nro_factura>0 "
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        MsgBox "Es una guia facturada..." & vbNewLine & " Primero elimine la factura...", vbInformation
        Exit Sub
    End If
    
    Sql = "SELECT id_ref " & _
            "FROM ven_doc_venta " & _
            "WHERE id_ref=" & L_Unico
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        MsgBox "Documento tiene N.Credito asociada..." & vbNewLine & "No es posible eliminar...", vbInformation
        Exit Sub
    End If
    
    sis_InputBox.FramBox = "Ingrese llave para anulacion"
    sis_InputBox.Show 1
    Sp_Llave = UCase(SG_codigo2)
    If Len(Sp_Llave) = 0 Then Exit Sub
    
    
    
    
    
    Sql = "SELECT usu_id,usu_nombre,usu_login " & _
          "FROM sis_usuarios " & _
          "WHERE usu_pwd = MD5('" & Sp_Llave & "')"
    Consulta RsTmp3, Sql
    
    
    '1ro Extraeremos el abo_id en el caso que exista 21-01-2012
    Sql = "SELECT abo_id " & _
          "FROM cta_abono_documentos d " & _
          "INNER JOIN cta_abonos a USING(abo_id) " & _
          "WHERE abo_cli_pro='CLI' AND a.rut_emp ='" & SP_Rut_Activo & "' AND d.rut_emp ='" & SP_Rut_Activo & "' AND id=" & L_Unico
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
                '2DO SABREMOS QUE TIPO DE ABONO FUE,
            Lp_IdAbono = RsTmp!abo_id
            Sql = "SELECT abo_id,abo_fecha,mpa_nombre,usu_nombre,a.mpa_id,abo_fecha_pago " & _
                  "FROM cta_abonos a " & _
                  "INNER JOIN par_medios_de_pago m USING(mpa_id) " & _
                  "INNER JOIN cta_abono_documentos d USING(abo_id) " & _
                  "WHERE  abo_cli_pro='CLI' AND a.rut_emp='" & SP_Rut_Activo & "' AND d.rut_emp='" & SP_Rut_Activo & "' AND a.abo_id=" & Lp_IdAbono
            Consulta RsTmp, Sql
            If RsTmp.RecordCount > 0 Then
                If RsTmp.RecordCount > 1 Then
                    'Ojo que el abono de este documento fue junto con varios documento
                    'por lo tanto no debemos permitir eliminar este documento
                    MsgBox "No es posible eliminar este documento..." & vbNewLine & "Es parte de un abono de multiples documentos " & _
                    vbNewLine & "Cant. Documentos " & RsTmp.RecordCount & " Fecha Abono " & Format(RsTmp!abo_fecha_pago, "DD-MM-YYYY") & " Usuario:" & RsTmp!usu_nombre & " Medio de pago:" & RsTmp!mpa_nombre, vbExclamation
                    Exit Sub
                End If
                If RsTmp.RecordCount = 1 Then
                    If RsTmp!mpa_id = 5 Then
                        '19 Agosto de 2013
                        'aqui debieremos pasar no mas por q el abono es de una guia
                    ElseIf RsTmp!mpa_id <> 1 Then
                        MsgBox "No es posible eliminar este documento..." & vbNewLine & "Ya se realiz� abono ... " & _
                        vbNewLine & " Fecha Abono " & Format(RsTmp!abo_fecha_pago, "DD-MM-YYYY") & " Usuario:" & RsTmp!usu_nombre & " Medio de pago:" & RsTmp!mpa_nombre, vbExclamation
                        Exit Sub
                    Else
                        'El abono es solo a un documento y en contado(efectivo) por lo que no es problema eliminarlo
                    End If
                End If
            
            
            End If
            
            If MsgBox("El documento tiene un abono registrado ..." & vbNewLine & "Nro Comprobante XX " & vbNewLine & "�Continuar?", vbQuestion + vbYesNo) = vbNo Then Exit Sub
            
    Else
        'El documento es al credito pero no tiene abonos, es posible continuar, no afectara nada
    
    End If
    
    
    
    If RsTmp3.RecordCount > 0 Then
        X = InputBox("Usuario:" & RsTmp3!usu_nombre & vbNewLine & "Ingrese motivo", "Motivo de eliminaci�n")
        If Len(X) = 0 Then
            MsgBox "No ingreso motivo, no fue eliminado el documento"
            Exit Sub
        End If
        
      '  With LvDetalle.SelectedItem
             Sql = "INSERT INTO sis_eliminacion_documentos (id,eli_fecha,eli_motivo,usu_login,eli_ven_com) VALUES (" & _
                    DG_ID_Unico & ",'" & Fql(Date) & "','" & UCase(X) & "','" & RsTmp3!usu_login & "','VEN')"
            
            'Sql = "INSERT INTO com_historial_eliminaciones (doc_id,no_documento,rut_proveedor,eli_fecha,eli_motivo,usu_id) VALUES (" & _
                        .SubItems(7) & "," & .SubItems(4) & ",'" & .SubItems(8) & "','" & Format(Now, "YYYY-DD-MM HH:MM:SS") & "','" & X & "'," & RsTmp3!usu_id & ")"
            
       ' End With
            
    Else
        MsgBox "Contrase�a ingresada no fue encontrada...", vbInformation
        Exit Sub
    End If
    On Error GoTo ErrorElimina
    cn.BeginTrans 'COMENZAMOS LA TRANSACCION
    cn.Execute Sql
    
    Sql = "DELETE FROM cta_abonos " & _
          "WHERE abo_cli_pro='CLI' AND rut_emp='" & SP_Rut_Activo & "' AND abo_id=" & Lp_IdAbono
    cn.Execute Sql
    Sql = "DELETE FROM cta_abono_documentos " & _
          "WHERE rut_emp='" & SP_Rut_Activo & "' AND abo_id=" & Lp_IdAbono
    cn.Execute Sql
    
    Sql = "SELECT rut_cliente rut,nombre_cliente nombre_proveedor,doc_movimiento inventario," & _
                 "no_documento,c.doc_id,doc_nombre,bod_id,doc_factura_guias,ven_nc_utilizada,id " & _
          "FROM ven_doc_venta c,sis_documentos d " & _
          "WHERE d.doc_id=c.doc_id AND id=" & L_Unico
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        Sp_Bodega = RsTmp!bod_id
        s_FacturaGuias = RsTmp!doc_factura_guias
        If RsTmp!Inventario = "ENTRADA" Then s_mov = "SALIDA"
        If RsTmp!Inventario = "SALIDA" Then s_mov = "ENTRADA"
        
        Sql = "SELECT d.codigo pro_codigo,unidades cantidad," & _
              "IFNULL((SELECT pro_precio_neto*d.unidades " & _
               "FROM inv_kardex k " & _
               "WHERE k.rut_emp='" & SP_Rut_Activo & "' AND k.pro_codigo=d.codigo ORDER BY kar_id DESC LIMIT 1),0) total_linea " & _
              "FROM ven_detalle d " & _
              "INNER JOIN maestro_productos m ON d.codigo=m.codigo " & _
              "WHERE m.rut_emp='" & SP_Rut_Activo & "' AND  pro_inventariable='SI' AND d.rut_emp='" & SP_Rut_Activo & "' AND no_documento=" & RsTmp!no_documento & " AND doc_id=" & RsTmp!doc_id
        Consulta rs_Detalle, Sql
        If rs_Detalle.RecordCount > 0 Then
            rs_Detalle.MoveFirst
            Do While Not rs_Detalle.EOF
                Kardex Format(Date, "YYYY-MM-DD"), s_mov, RsTmp!doc_id, RsTmp!no_documento, Sp_Bodega, _
                    rs_Detalle!pro_codigo, rs_Detalle!cantidad, "ANULA " & RsTmp!doc_nombre & " Nro:" & RsTmp!no_documento, Round(rs_Detalle!total_linea / rs_Detalle!cantidad), _
                    rs_Detalle!total_linea, RsTmp!Rut, RsTmp!nombre_proveedor, "NO", Round(rs_Detalle!total_linea / rs_Detalle!cantidad), , , , , 0
                    
                rs_Detalle.MoveNext
            Loop
        End If
        
        '28 Sep 2013
        'DEBEMOS DETECTAR SI LA NOTA DE CREDITO SE UTILIZO PARA ABONAR A SU DOCUMENTO ORIGINAL
        If RsTmp!ven_nc_utilizada = "SI" Then
            'con esto sabemos q se trata de una nota de credito, y podemos referenciar a la tabla de abonos
            Sql = "DELETE FROM cta_abonos " & _
                    "WHERE abo_cli_pro='CLI' AND rut_emp='" & SP_Rut_Activo & "' AND id_ref=" & RsTmp!Id
            cn.Execute Sql
        End If
        
        'DEBEMOS CONSULTAR SI EXISTEN DOCUMENTOS QUE HACEN REFERENCIA AL QUE SE ESTA INGRESANDO
        Sql = "UPDATE ven_doc_venta " & _
                     "SET id_ref=0,doc_id_factura=0,nro_factura=0 " & _
              "WHERE id_ref=" & L_Unico
        cn.Execute Sql
        
                
        'DEBEMOS CONSULTAR SI EXISTEN DOCUMENTOS FACTURADOS CON ESTE DOCUMENTO PARA LIBERARLOS Abril 2012
        If s_FacturaGuias = "SI" Then
            Sql = "UPDATE ven_doc_venta " & _
                     "SET id_ref=0,doc_id_factura=0,nro_factura=0 " & _
                  "WHERE nro_factura=" & RsTmp!no_documento & " AND doc_id_factura=" & RsTmp!doc_id & " " & _
                       "AND rut_emp='" & SP_Rut_Activo & "'"
            cn.Execute Sql
        End If
                
                
                
                
        If RsTmp!Inventario = "NN" Then
            '28 Abril 2011
            'No modifica inventario
            'por lo tanto podriamos eliminar el documeneto sin tocar el
            'kardex ni el stock
        End If
        'multidelete
        
        If SP_Control_Inventario = "SI" Then
            'Antes de eliminar los registros
            'Debemos saber si hay algun activo inmovilizado
            Sql = "SELECT aim_id " & _
                    "FROM ven_detalle d " & _
                    "JOIN ven_doc_venta v ON (v.no_documento=d.no_documento AND v.doc_id=d.doc_id) " & _
                    "WHERE aim_id>0 AND v.rut_emp='" & SP_Rut_Activo & "' AND  v.id=" & L_Unico
            Consulta RsTmp, Sql
            If RsTmp.RecordCount > 0 Then
                RsTmp.MoveFirst
                Do While Not RsTmp.EOF
                    cn.Execute "UPDATE con_activo_inmovilizado " & _
                                    "SET aim_fecha_egreso=Null,aim_habilitado='SI' " & _
                                    "WHERE aim_id=" & RsTmp!aim_id
                    RsTmp.MoveNext
                Loop
            End If
        
        
            Sql = "DELETE FROM " & _
                  "ven_doc_venta, " & _
                  "ven_detalle " & _
                  "USING ven_doc_venta " & _
                  "LEFT JOIN ven_detalle ON (ven_doc_venta.no_documento=ven_detalle.no_documento AND ven_doc_venta.doc_id=ven_detalle.doc_id) " & _
                  "WHERE ven_doc_venta.rut_emp='" & SP_Rut_Activo & "' AND  ven_doc_venta.id=" & L_Unico
            'Consulta RsTmp, Sql
        Else
            'Antes de eliminar los registros
            'Debemos saber si hay algun activo inmovilizado
            Sql = "SELECT aim_id " & _
                    "FROM ven_sin_detalle " & _
                    "WHERE aim_id>0 AND id=" & L_Unico
            Consulta RsTmp, Sql
            If RsTmp.RecordCount > 0 Then
                RsTmp.MoveFirst
                Do While Not RsTmp.EOF
                    cn.Execute "UPDATE con_activo_inmovilizado " & _
                                    "SET aim_fecha_egreso=Null,aim_habilitado='SI' " & _
                                    "WHERE aim_id=" & RsTmp!aim_id
                    RsTmp.MoveNext
                Loop
            End If
            
        
        
            Sql = "DELETE FROM " & _
                  "ven_doc_venta, " & _
                  "ven_sin_detalle " & _
                  "USING ven_doc_venta " & _
                  "LEFT JOIN ven_sin_detalle USING(id) " & _
                  "WHERE ven_doc_venta.id=" & L_Unico
            'Consulta RsTmp, Sql
        End If
        cn.Execute Sql
    End If
    
    EliminaAbonosDeNC "CLI", Val(DG_ID_Unico)
    
    
    
                 Sql = "INSERT INTO ven_doc_venta (id,fecha,doc_id,no_documento,rut_emp,rut_cliente,tipo_doc,nombre_cliente," & _
                                          "tipo_movimiento,usu_nombre,sue_id,caj_id) VALUES(" & _
                        DG_ID_Unico & "," & _
                         "'" & Format(DtFecha.Value, "YYYY-MM-DD") & _
                         "'," & CboDocVenta.ItemData(CboDocVenta.ListIndex) & _
                         "," & TxtNroDocumento & _
                         ",'" & SP_Rut_Activo & _
                         "','NULO' " & _
                         ",'" & CboDocVenta.Text & _
                         "','ANULADO','NULO','" & Sp_Llave & "'," & IG_id_Sucursal_Empresa & "," & LG_id_Caja & ")"
             cn.Execute Sql
             AutoIncremento "GUARDA", CboDocVenta.ItemData(CboDocVenta.ListIndex), TxtNroDocumento, IG_id_Sucursal_Empresa
    
    
    cn.CommitTrans
    Unload Me
    Exit Sub
ErrorElimina:
    MsgBox "Ocurrio un error al eliminar documento"
    cn.RollbackTrans
End Sub





Private Sub TxtTotalDescuento_Validate(Cancel As Boolean)
    SkBrutoMateriales = txtOriginal - CDbl(Me.TxtTotalDescuento)
    
   txtNuevoDescuento = (Val(txtOriginal) - CDbl(SkBrutoMateriales)) / Val(txtOriginal) * 100
End Sub


