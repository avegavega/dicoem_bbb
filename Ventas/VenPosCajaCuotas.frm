VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form VenPosCajaCuotas 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Cuotas"
   ClientHeight    =   8265
   ClientLeft      =   5295
   ClientTop       =   1380
   ClientWidth     =   7920
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8265
   ScaleWidth      =   7920
   ShowInTaskbar   =   0   'False
   Begin VB.Frame FrmCambiaFecha 
      Caption         =   "Cambiar Fecha"
      Height          =   1815
      Left            =   4890
      TabIndex        =   16
      Top             =   4215
      Visible         =   0   'False
      Width           =   2880
      Begin VB.CommandButton CmdXfecha 
         Caption         =   "x"
         Height          =   225
         Left            =   2625
         TabIndex        =   19
         Top             =   120
         Width           =   210
      End
      Begin VB.CommandButton CmdCambiaFecha 
         Caption         =   "Cambiar"
         Height          =   330
         Left            =   840
         TabIndex        =   18
         Top             =   1035
         Width           =   1245
      End
      Begin MSComCtl2.DTPicker DtCambia 
         Height          =   315
         Left            =   765
         TabIndex        =   17
         Top             =   555
         Width           =   1440
         _ExtentX        =   2540
         _ExtentY        =   556
         _Version        =   393216
         Format          =   273416193
         CurrentDate     =   42618
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Datos del documento"
      Height          =   3120
      Left            =   495
      TabIndex        =   4
      Top             =   135
      Width           =   7305
      Begin VB.TextBox TxtNombreCliente 
         Appearance      =   0  'Flat
         BackColor       =   &H0080FF80&
         BeginProperty Font 
            Name            =   "Arial Narrow"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   465
         Left            =   1665
         Locked          =   -1  'True
         MultiLine       =   -1  'True
         TabIndex        =   10
         TabStop         =   0   'False
         Top             =   840
         Width           =   5190
      End
      Begin VB.TextBox TxtRutCliente 
         Appearance      =   0  'Flat
         BackColor       =   &H0080FF80&
         BeginProperty Font 
            Name            =   "Arial Narrow"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   1665
         Locked          =   -1  'True
         TabIndex        =   9
         TabStop         =   0   'False
         Top             =   405
         Width           =   1860
      End
      Begin VB.TextBox TxtDocumento 
         Appearance      =   0  'Flat
         BackColor       =   &H0080FF80&
         BeginProperty Font 
            Name            =   "Arial Narrow"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   465
         Left            =   1665
         Locked          =   -1  'True
         MultiLine       =   -1  'True
         TabIndex        =   8
         TabStop         =   0   'False
         Top             =   1815
         Width           =   5190
      End
      Begin VB.TextBox TxtMonto 
         Appearance      =   0  'Flat
         BackColor       =   &H0080FF80&
         BeginProperty Font 
            Name            =   "Arial Narrow"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   420
         Left            =   1665
         Locked          =   -1  'True
         TabIndex        =   7
         TabStop         =   0   'False
         Top             =   1350
         Width           =   1860
      End
      Begin VB.CommandButton CmdCalcular 
         Caption         =   "Calcular"
         Height          =   390
         Left            =   3015
         TabIndex        =   6
         Top             =   2385
         Width           =   1695
      End
      Begin VB.TextBox TxtCuotas 
         Appearance      =   0  'Flat
         BackColor       =   &H0080FF80&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00400000&
         Height          =   465
         Left            =   1665
         MaxLength       =   1
         MultiLine       =   -1  'True
         TabIndex        =   5
         TabStop         =   0   'False
         Top             =   2340
         Width           =   1305
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel6 
         Height          =   240
         Index           =   0
         Left            =   900
         OleObjectBlob   =   "VenPosCajaCuotas.frx":0000
         TabIndex        =   11
         Top             =   525
         Width           =   630
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel7 
         Height          =   240
         Index           =   0
         Left            =   465
         OleObjectBlob   =   "VenPosCajaCuotas.frx":0064
         TabIndex        =   12
         Top             =   960
         Width           =   1065
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel6 
         Height          =   240
         Index           =   1
         Left            =   900
         OleObjectBlob   =   "VenPosCajaCuotas.frx":00CE
         TabIndex        =   13
         Top             =   1500
         Width           =   630
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel7 
         Height          =   240
         Index           =   1
         Left            =   150
         OleObjectBlob   =   "VenPosCajaCuotas.frx":0136
         TabIndex        =   14
         Top             =   1935
         Width           =   1380
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel7 
         Height          =   240
         Index           =   2
         Left            =   60
         OleObjectBlob   =   "VenPosCajaCuotas.frx":01A6
         TabIndex        =   15
         Top             =   2430
         Width           =   1455
      End
   End
   Begin VB.Timer Timer1 
      Interval        =   30
      Left            =   6390
      Top             =   300
   End
   Begin VB.Frame Frame1 
      Caption         =   "Detalle de cuotas"
      Height          =   4590
      Left            =   480
      TabIndex        =   0
      Top             =   3540
      Width           =   7335
      Begin VB.CommandButton Command1 
         Caption         =   "Volver sin grabar"
         Height          =   360
         Left            =   5280
         TabIndex        =   3
         Top             =   4095
         Width           =   1605
      End
      Begin VB.CommandButton CmdOk 
         Caption         =   "Aceptar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   525
         Left            =   210
         TabIndex        =   2
         Top             =   3945
         Width           =   2025
      End
      Begin MSComctlLib.ListView LvDetalle 
         Height          =   3330
         Left            =   210
         TabIndex        =   1
         ToolTipText     =   "Doble clic para personalizar fecha."
         Top             =   450
         Width           =   6675
         _ExtentX        =   11774
         _ExtentY        =   5874
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   3
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Numero"
            Object.Width           =   3528
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Fecha"
            Object.Width           =   3528
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Monto"
            Object.Width           =   3528
         EndProperty
      End
   End
End
Attribute VB_Name = "VenPosCajaCuotas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub CmdCalcular_Click()
    Dim ValorCuota As Long
    Dim Lp_SumaCuotas As Long
    If Val(TxtCuotas) = 0 Or Val(TxtCuotas) > 100 Then
        MsgBox "Debe especificar cantidad de cuotas correctas...", vbInformation + vbOKOnly
        TxtCuotas.SetFocus
        Exit Sub
    End If
    
    LvDetalle.ListItems.Clear
    ValorCuota = Round(CDbl(TxtMonto) / Val(TxtCuotas), 0)
    Lp_SumaCuotas = 0
    For p = 1 To Val(TxtCuotas)
        LvDetalle.ListItems.Add , , p
        LvDetalle.ListItems(LvDetalle.ListItems.Count).SubItems(1) = Date + (30 * p)
        LvDetalle.ListItems(LvDetalle.ListItems.Count).SubItems(2) = NumFormat(ValorCuota)
        Lp_SumaCuotas = Lp_SumaCuotas + ValorCuota
    
        If p = Val(TxtCuotas) Then
            If Lp_SumaCuotas <> CDbl(TxtMonto) Then
                If Lp_SumaCuotas > CDbl(TxtMonto) Then
                    LvDetalle.ListItems(LvDetalle.ListItems.Count).SubItems(2) = NumFormat(ValorCuota - (Lp_SumaCuotas - CDbl(TxtMonto)))
                Else
                    LvDetalle.ListItems(LvDetalle.ListItems.Count).SubItems(2) = NumFormat(ValorCuota + (CDbl(TxtMonto) - Lp_SumaCuotas))
                End If
            End If
                
        
        End If
    Next
    
    
End Sub

Private Sub CmdCambiaFecha_Click()
    LvDetalle.SelectedItem.SubItems(1) = DtCambia
    Me.FrmCambiaFecha.Visible = False
    LvDetalle.Enabled = True
     Me.CmdCalcular.Enabled = True
 
End Sub

Private Sub CmdOk_Click()
    'Pasar los datos a la caja.
    If LvDetalle.ListItems.Count = 0 Then
        MsgBox "Debe calcular las cuotas...", vbInformation + vbOKOnly
        TxtCuotas.SetFocus
        Exit Sub
    End If
    
  '  With VenPosCaja.LvCuotas
        VenPosCaja.LvCuotas.ListItems.Clear
        For p = 1 To LvDetalle.ListItems.Count
            VenPosCaja.LvCuotas.ListItems.Add , , LvDetalle.ListItems(p)
            VenPosCaja.LvCuotas.ListItems(p).SubItems(1) = LvDetalle.ListItems(p).SubItems(1)
            VenPosCaja.LvCuotas.ListItems(p).SubItems(2) = LvDetalle.ListItems(p).SubItems(2)
        
        Next
'    End With
   ' Me.Hide
    Unload Me
End Sub

Private Sub CmdXfecha_Click()
    FrmCambiaFecha.Visible = False
    Me.CmdCalcular.Enabled = True
    LvDetalle.Enabled = True
End Sub

Private Sub Command1_Click()
    VenPosCaja.LvCuotas.ListItems.Clear
    LvDetalle.ListItems.Clear
    Unload Me
End Sub

Private Sub Form_Load()
    Aplicar_skin Me
    Centrar Me, False
End Sub
Private Sub LvDetalle_DblClick()
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    Me.FrmCambiaFecha.Visible = True
    DtCambia = LvDetalle.SelectedItem.SubItems(1)
    LvDetalle.Enabled = False
    Me.CmdCalcular.Enabled = False
End Sub

Private Sub Timer1_Timer()
    On Error Resume Next
    TxtCuotas.SetFocus
    Timer1.Enabled = False
End Sub

Private Sub TxtCuotas_KeyPress(KeyAscii As Integer)
    KeyAscii = AceptaSoloNumeros(KeyAscii)
End Sub

Private Sub TxtCuotas_Validate(Cancel As Boolean)
    TxtCuotas = Trim(TxtCuotas)
End Sub
