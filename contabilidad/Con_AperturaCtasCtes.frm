VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{DA729E34-689F-49EA-A856-B57046630B73}#1.0#0"; "progressbar-xp.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form Con_AperturaCtasCtes 
   Caption         =   "Abre Cta Cte. con Asiento de Apertura"
   ClientHeight    =   5115
   ClientLeft      =   1455
   ClientTop       =   2655
   ClientWidth     =   14985
   LinkTopic       =   "Form1"
   ScaleHeight     =   5115
   ScaleWidth      =   14985
   Begin VB.CommandButton cmdSalir 
      Cancel          =   -1  'True
      Caption         =   "&Retornar sin Grabar"
      Height          =   420
      Left            =   3345
      TabIndex        =   22
      ToolTipText     =   "No graba los datos y vuelve"
      Top             =   4440
      Width           =   2955
   End
   Begin VB.ComboBox CboSucursales 
      Height          =   315
      Left            =   11925
      Style           =   2  'Dropdown List
      TabIndex        =   20
      ToolTipText     =   "Sucursal de la empresa"
      Top             =   1020
      Width           =   2610
   End
   Begin VB.Frame FraProgreso 
      Caption         =   "Progreso exportaci�n"
      Height          =   795
      Left            =   975
      TabIndex        =   18
      Top             =   2490
      Visible         =   0   'False
      Width           =   11430
      Begin Proyecto2.XP_ProgressBar BarraProgreso 
         Height          =   330
         Left            =   150
         TabIndex        =   19
         Top             =   315
         Width           =   11130
         _ExtentX        =   19632
         _ExtentY        =   582
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BrushStyle      =   0
         Color           =   16777088
         Scrolling       =   1
         ShowText        =   -1  'True
      End
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Exportar"
      Height          =   420
      Left            =   2085
      TabIndex        =   17
      ToolTipText     =   "Exporta a excel"
      Top             =   4440
      Width           =   1245
   End
   Begin VB.CommandButton CmdGrabarV 
      Caption         =   "Graba y continua"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Left            =   135
      TabIndex        =   16
      ToolTipText     =   "Graba los datos y los agrega al asiento"
      Top             =   4440
      Width           =   1935
   End
   Begin VB.Timer Timer3 
      Interval        =   500
      Left            =   0
      Top             =   0
   End
   Begin VB.TextBox TxtTotalVoucher 
      Alignment       =   1  'Right Justify
      BackColor       =   &H8000000F&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   10710
      Locked          =   -1  'True
      TabIndex        =   14
      TabStop         =   0   'False
      ToolTipText     =   "Nro de Documento"
      Top             =   4440
      Width           =   1245
   End
   Begin VB.CommandButton CmdOk 
      Caption         =   "Ok"
      Height          =   285
      Left            =   14505
      TabIndex        =   5
      Top             =   1020
      Width           =   330
   End
   Begin VB.CommandButton CmdBuscaCliente 
      Caption         =   "RUT     F1:Buscar"
      Height          =   255
      Left            =   120
      TabIndex        =   13
      Top             =   765
      Width           =   1575
   End
   Begin VB.TextBox TxtRazonSocial 
      BackColor       =   &H8000000F&
      Height          =   315
      Left            =   1680
      Locked          =   -1  'True
      TabIndex        =   12
      TabStop         =   0   'False
      Top             =   1020
      Width           =   4125
   End
   Begin VB.TextBox TxtRut 
      BackColor       =   &H0080FF80&
      Height          =   315
      Left            =   105
      TabIndex        =   0
      ToolTipText     =   "Ingrese el RUT sin puntos ni guion."
      Top             =   1020
      Width           =   1575
   End
   Begin VB.TextBox TxtBruto 
      Alignment       =   1  'Right Justify
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   10680
      TabIndex        =   4
      ToolTipText     =   "Nro de Documento"
      Top             =   1020
      Width           =   1245
   End
   Begin VB.ComboBox CboTipoDoc 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      ItemData        =   "Con_AperturaCtasCtes.frx":0000
      Left            =   5790
      List            =   "Con_AperturaCtasCtes.frx":0002
      Style           =   2  'Dropdown List
      TabIndex        =   1
      ToolTipText     =   "Seleccione Documento de  venta. Ventas con Retenci�n, cuando el contribuyente recibe factura de terceros "
      Top             =   1020
      Width           =   2370
   End
   Begin VB.TextBox TxtNroDocumento 
      Alignment       =   1  'Right Justify
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   8145
      TabIndex        =   2
      ToolTipText     =   "Nro de Documento"
      Top             =   1020
      Width           =   1305
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
      Height          =   240
      Left            =   3060
      OleObjectBlob   =   "Con_AperturaCtasCtes.frx":0004
      TabIndex        =   7
      Top             =   780
      Width           =   1395
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
      Height          =   210
      Left            =   8010
      OleObjectBlob   =   "Con_AperturaCtasCtes.frx":007A
      TabIndex        =   8
      Top             =   795
      Width           =   1260
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
      Height          =   210
      Left            =   5940
      OleObjectBlob   =   "Con_AperturaCtasCtes.frx":00F4
      TabIndex        =   9
      Top             =   795
      Width           =   1260
   End
   Begin MSComCtl2.DTPicker DtFecha 
      Height          =   315
      Left            =   9435
      TabIndex        =   3
      Top             =   1020
      Width           =   1275
      _ExtentX        =   2249
      _ExtentY        =   556
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Format          =   94961665
      CurrentDate     =   39857
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel5 
      Height          =   255
      Left            =   9510
      OleObjectBlob   =   "Con_AperturaCtasCtes.frx":016E
      TabIndex        =   10
      Top             =   795
      Width           =   1035
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel6 
      Height          =   210
      Left            =   10260
      OleObjectBlob   =   "Con_AperturaCtasCtes.frx":01DF
      TabIndex        =   11
      Top             =   795
      Width           =   1260
   End
   Begin MSComctlLib.ListView LvDetalle 
      Height          =   2985
      Left            =   120
      TabIndex        =   6
      Top             =   1365
      Width           =   14730
      _ExtentX        =   25982
      _ExtentY        =   5265
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   0
      NumItems        =   11
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Object.Tag             =   "T500"
         Text            =   "RUT"
         Object.Width           =   2760
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Object.Tag             =   "T3000"
         Text            =   "Nombre"
         Object.Width           =   7267
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Key             =   "debe"
         Object.Tag             =   "T1500"
         Text            =   "Documento"
         Object.Width           =   4154
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   3
         Key             =   "haber"
         Object.Tag             =   "N109"
         Text            =   "Nro Documento"
         Object.Width           =   2275
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Object.Tag             =   "F1000"
         Text            =   "Fecha"
         Object.Width           =   2187
      EndProperty
      BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   5
         Key             =   "bruto"
         Object.Tag             =   "N100"
         Text            =   "Bruto"
         Object.Width           =   2231
      EndProperty
      BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   6
         Object.Tag             =   "N109"
         Text            =   "id documeneto"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   7
         Text            =   "abonos?"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   8
         Object.Tag             =   "N109"
         Text            =   "ID UNICO"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   9
         Object.Tag             =   "T2000"
         Text            =   "Sucursal"
         Object.Width           =   4463
      EndProperty
      BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   10
         Object.Tag             =   "N109"
         Text            =   "Suc. empresa id"
         Object.Width           =   0
      EndProperty
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
      Height          =   210
      Left            =   9390
      OleObjectBlob   =   "Con_AperturaCtasCtes.frx":0253
      TabIndex        =   15
      Top             =   4530
      Width           =   1260
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   15
      OleObjectBlob   =   "Con_AperturaCtasCtes.frx":02CB
      Top             =   780
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel7 
      Height          =   210
      Left            =   11850
      OleObjectBlob   =   "Con_AperturaCtasCtes.frx":04FF
      TabIndex        =   21
      Top             =   795
      Width           =   930
   End
End
Attribute VB_Name = "Con_AperturaCtasCtes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public Sm_CliPro As String




Private Sub CmdBuscaCliente_Click()
    If Sm_CliPro = "CLI" Then
        SG_codigo = Empty
        BuscaCliente.Show 1
        TxtRut = SG_codigo
    Else
        BuscaProveedor.Show 1
        TxtRut = SG_codigo
    End If
    TxtRut_Validate (True)
End Sub



Private Sub CmdGrabarV_Click()
    SG_codigo = TxtTotalVoucher
    If Sm_CliPro = "CLI" Then
        With Con_Vouchers
            .LvDetalleC.ListItems.Clear
            For i = 1 To LvDetalle.ListItems.Count
                .LvDetalleC.ListItems.Add , , LvDetalle.ListItems(i)
                .LvDetalleC.ListItems(i).SubItems(1) = LvDetalle.ListItems(i).SubItems(1)
                .LvDetalleC.ListItems(i).SubItems(2) = LvDetalle.ListItems(i).SubItems(2)
                .LvDetalleC.ListItems(i).SubItems(3) = LvDetalle.ListItems(i).SubItems(3)
                .LvDetalleC.ListItems(i).SubItems(4) = LvDetalle.ListItems(i).SubItems(4)
                .LvDetalleC.ListItems(i).SubItems(5) = LvDetalle.ListItems(i).SubItems(5)
                .LvDetalleC.ListItems(i).SubItems(6) = LvDetalle.ListItems(i).SubItems(6)
                .LvDetalleC.ListItems(i).SubItems(9) = LvDetalle.ListItems(i).SubItems(9)
                .LvDetalleC.ListItems(i).SubItems(10) = LvDetalle.ListItems(i).SubItems(10)
            Next
        End With
    Else
        With Con_Vouchers
            .LvDetalleP.ListItems.Clear
            For i = 1 To LvDetalle.ListItems.Count
                .LvDetalleP.ListItems.Add , , LvDetalle.ListItems(i)
                .LvDetalleP.ListItems(i).SubItems(1) = LvDetalle.ListItems(i).SubItems(1)
                .LvDetalleP.ListItems(i).SubItems(2) = LvDetalle.ListItems(i).SubItems(2)
                .LvDetalleP.ListItems(i).SubItems(3) = LvDetalle.ListItems(i).SubItems(3)
                .LvDetalleP.ListItems(i).SubItems(4) = LvDetalle.ListItems(i).SubItems(4)
                .LvDetalleP.ListItems(i).SubItems(5) = LvDetalle.ListItems(i).SubItems(5)
                .LvDetalleP.ListItems(i).SubItems(6) = LvDetalle.ListItems(i).SubItems(6)
            Next
        End With
    
    End If
    
    
    Unload Me
    
End Sub

Private Sub CmdOk_Click()
    If Len(TxtRut) = 0 Or Len(TxtRazonSocial) = 0 Then
        If Sm_CliPro = "CLI" Then MsgBox "Debe ingresar Cliente para el documento...", vbInformation
        If Sm_CliPro = "PRO" Then MsgBox "Debe ingresar Proveedor para el documento...", vbInformation
        TxtRut.SetFocus
        Exit Sub
    End If
    
    If CboTipoDoc.ListIndex = -1 Then
        MsgBox "Seleccione tipo documento...", vbInformation
        CboTipoDoc.SetFocus
        Exit Sub
    End If
    If Val(Me.TxtNroDocumento) = 0 Then
        MsgBox "Ingrese Nro de documento...", vbInformation
        TxtNroDocumento.SetFocus
        Exit Sub
    End If
    If Val(Me.TxtBruto) = 0 Then
        MsgBox "Ingrese Valor del documento...", vbInformation
        TxtBruto.SetFocus
        Exit Sub
    End If
    
    'Primero buscamos que el documento no se repita
    For i = 1 To LvDetalle.ListItems.Count
        If LvDetalle.ListItems(i) = TxtRut And CboTipoDoc.ItemData(CboTipoDoc.ListIndex) = LvDetalle.ListItems(i).SubItems(6) And Val(TxtNroDocumento) = Val(LvDetalle.ListItems(i).SubItems(3)) Then
            MsgBox "Documento ya se encuentra ingresado...", vbInformation
            Exit Sub
        End If
            
    
    
    Next
    
    
    
    LvDetalle.ListItems.Add , , TxtRut
    X = LvDetalle.ListItems.Count
    LvDetalle.ListItems(X).SubItems(1) = TxtRazonSocial
    LvDetalle.ListItems(X).SubItems(2) = CboTipoDoc.Text
    LvDetalle.ListItems(X).SubItems(3) = TxtNroDocumento
    LvDetalle.ListItems(X).SubItems(4) = DtFecha
    LvDetalle.ListItems(X).SubItems(5) = TxtBruto
    LvDetalle.ListItems(X).SubItems(6) = CboTipoDoc.ItemData(CboTipoDoc.ListIndex)
    LvDetalle.ListItems(X).SubItems(9) = CboSucursales.Text
    LvDetalle.ListItems(X).SubItems(10) = CboSucursales.ItemData(CboSucursales.ListIndex)
    
    
    TxtRut = Empty
    TxtRazonSocial = Empty
    TxtNroDocumento = Empty
    TxtBruto = Empty
    TxtRut.SetFocus
    Calcula
End Sub
Public Sub Calcula()
    TxtTotalVoucher = NumFormat(TotalizaColumna(LvDetalle, "bruto"))
End Sub

Private Sub CmdSalir_Click()
    Unload Me
End Sub

Private Sub Command1_Click()
    Dim tit(2) As String
    If LvDetalle.ListItems.Count = 0 Then Exit Sub
    FraProgreso.Visible = True
    tit(0) = Me.Caption & " " & DtFecha & " - " & Time
    tit(1) = ""
    tit(2) = ""
    ExportarNuevo LvDetalle, tit, Me, BarraProgreso
    FraProgreso.Visible = False
End Sub

Private Sub Form_Load()
    Centrar Me
    Skin2 Me, , 7
    LLenarCombo CboSucursales, "CONCAT(sue_direccion,'-',sue_ciudad)", "sue_id", "sis_empresas_sucursales", "sue_activa='SI' AND emp_id=" & IG_id_Empresa
    CboSucursales.AddItem "CASA MATRIZ"
    CboSucursales.ItemData(CboSucursales.ListCount - 1) = 1
    Busca_Id_Combo CboSucursales, Val(IG_id_Sucursal_Empresa)
    If Sm_CliPro = "CLI" Then
        Me.Caption = Me.Caption & " CLIENTES "
        If SP_Control_Inventario = "SI" Then
            LLenarCombo Me.CboTipoDoc, "doc_nombre", "doc_id", "sis_documentos", "doc_activo='SI' AND doc_documento='VENTA' AND doc_utiliza_en_caja='" & SG_Funcion_Equipo & "'", "doc_id"
        Else
            LLenarCombo Me.CboTipoDoc, "doc_nombre", "doc_id", "sis_documentos", "doc_boleta_venta='NO' AND doc_activo='SI' AND doc_documento='VENTA' AND doc_contable='SI' AND doc_utiliza_en_caja='" & SG_Funcion_Equipo & "'", "doc_id"
        End If
    ElseIf Sm_CliPro = "PRO" Then
        LLenarCombo Me.CboTipoDoc, "doc_nombre", "doc_id", "sis_documentos", "doc_activo='SI' AND doc_documento='COMPRA' AND doc_contable='SI'", "doc_id"
    
        Me.Caption = Me.Caption & " PROVEEDORES "
        If SP_Control_Inventario = "SI" Then LLenarCombo CboTipoDoc, "doc_nombre", "doc_id", "sis_documentos", "doc_libro_gastos='NO' AND doc_documento='COMPRA' AND doc_activo='SI'", "doc_orden"
        If SP_Control_Inventario = "NO" Then LLenarCombo CboTipoDoc, "doc_nombre", "doc_id", "sis_documentos", "doc_libro_gastos='NO' AND doc_documento='COMPRA' AND doc_activo='SI'", "doc_orden"
    End If
    CboTipoDoc.ListIndex = 0
    DtFecha = Date
    Calcula
End Sub


Private Sub Form_Unload(Cancel As Integer)
    SG_codigo = Empty
End Sub

Private Sub LvDetalle_DblClick()
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    If Val(LvDetalle.SelectedItem.SubItems(7)) > 0 Then
        MsgBox "No es posible modificar este item, se ha detectado" & vbNewLine & _
                "que tiene " & LvDetalle.SelectedItem.SubItems(7) & " Abonos.. " & _
                "Si quiere modificarlo debe eliminar 1ro sus abonos." & _
                " en el formulario INFORME DE ABONOS DE CLIENTES � " & _
                " en el formulario INFORME DE ABONOS DE PROVEEDORES ", vbExclamation
        Exit Sub
    End If
    TxtRut = LvDetalle.SelectedItem
    Me.TxtRazonSocial = LvDetalle.SelectedItem.SubItems(1)
    Busca_Id_Combo CboTipoDoc, Val(LvDetalle.SelectedItem.SubItems(6))
    TxtNroDocumento = LvDetalle.SelectedItem.SubItems(3)
    DtFecha = LvDetalle.SelectedItem.SubItems(4)
    TxtBruto = LvDetalle.SelectedItem.SubItems(5)
   
    Busca_Id_Combo CboSucursales, Val(LvDetalle.SelectedItem.SubItems(10))
    LvDetalle.ListItems.Remove LvDetalle.SelectedItem.Index
    Calcula
End Sub

Private Sub Timer3_Timer()
    On Error Resume Next
    TxtRut.SetFocus
    Timer3.Enabled = False
End Sub



Private Sub TxtBruto_GotFocus()
    En_Foco TxtBruto
End Sub

Private Sub TxtBruto_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
    If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtBruto_Validate(Cancel As Boolean)
    If Val(TxtBruto) = 0 Then
        TxtBruto = 0
    Else
        TxtBruto = NumFormat(TxtBruto)
    End If
End Sub

Private Sub TxtNroDocumento_GotFocus()
    En_Foco TxtNroDocumento
End Sub

Private Sub TxtNroDocumento_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
    If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtRut_GotFocus()
    En_Foco TxtRut
End Sub

Private Sub TxtRut_KeyUp(KeyCode As Integer, Shift As Integer)
        If KeyCode = vbKeyF1 Then CmdBuscaCliente_Click
End Sub

Private Sub TxtRut_Validate(Cancel As Boolean)
    If Len(TxtRut) = 0 Then Exit Sub
    Dim NuevoRut As String
    TxtRazonSocial = Empty
    TxtRut.Text = Replace(TxtRut.Text, ".", "")
    TxtRut.Text = Replace(TxtRut.Text, "-", "")
    If Not VerificaRut(TxtRut.Text, NuevoRut) Then Exit Sub
    TxtRut = NuevoRut
    If Sm_CliPro = "CLI" Then
        Sql = "SELECT nombre_rsocial AS nombre " & _
                   "FROM maestro_clientes m " & _
                    "JOIN par_asociacion_ruts a " & _
                    "ON m.rut_cliente=a.rut_cli " & _
                    "WHERE a.rut_emp='" & SP_Rut_Activo & "' AND m.rut_cliente='" & TxtRut & "'"
    Else
        Sql = "SELECT nombre_empresa AS nombre " & _
                   "FROM maestro_proveedores m " & _
                    "JOIN par_asociacion_ruts a " & _
                    "ON m.rut_proveedor=a.rut_pro " & _
                    "WHERE a.rut_emp='" & SP_Rut_Activo & "' AND m.rut_proveedor='" & TxtRut & "'"
    
    End If
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then Me.TxtRazonSocial = RsTmp!nombre
    
    
        
End Sub
