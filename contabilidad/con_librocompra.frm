VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{DA729E34-689F-49EA-A856-B57046630B73}#1.0#0"; "progressbar-xp.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form con_librocompra 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Libro de COMPRA/VENTA"
   ClientHeight    =   8130
   ClientLeft      =   3435
   ClientTop       =   2415
   ClientWidth     =   14085
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8130
   ScaleWidth      =   14085
   Begin VB.TextBox TxtFActuraElectronica 
      Height          =   285
      Left            =   11070
      TabIndex        =   31
      Text            =   "Text1"
      Top             =   165
      Visible         =   0   'False
      Width           =   2775
   End
   Begin VB.Frame FraProgreso 
      Height          =   540
      Left            =   555
      TabIndex        =   20
      Top             =   7500
      Visible         =   0   'False
      Width           =   9930
      Begin Proyecto2.XP_ProgressBar PBar 
         Height          =   255
         Left            =   105
         TabIndex        =   21
         Top             =   195
         Width           =   9585
         _ExtentX        =   16907
         _ExtentY        =   450
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BrushStyle      =   0
         Color           =   16777088
         Scrolling       =   4
         ShowText        =   -1  'True
      End
   End
   Begin VB.CommandButton CmdExportar 
      Caption         =   "Exportar Sin Nota de Credito"
      Height          =   390
      Left            =   720
      TabIndex        =   19
      ToolTipText     =   "Salir"
      Top             =   6900
      Width           =   2415
   End
   Begin VB.Frame Frame3 
      Caption         =   "ASIENTOS  CONTABLES CON NOTA DE CREDITO"
      Height          =   4575
      Left            =   7215
      TabIndex        =   17
      Top             =   2820
      Width           =   6630
      Begin VB.CommandButton CmdExportaCNC 
         Caption         =   "Exportar Con Nota de Credito"
         Height          =   390
         Left            =   180
         TabIndex        =   22
         ToolTipText     =   "Salir"
         Top             =   4080
         Width           =   2565
      End
      Begin MSComctlLib.ListView LvCDetalle 
         Height          =   3705
         Left            =   135
         TabIndex        =   18
         Top             =   315
         Width           =   6345
         _ExtentX        =   11192
         _ExtentY        =   6535
         View            =   3
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   5
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "T400"
            Text            =   "Cod"
            Object.Width           =   882
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "T3000"
            Text            =   "CUENTAS"
            Object.Width           =   4762
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   2
            Key             =   "debe"
            Object.Tag             =   "N100"
            Text            =   "DEBE"
            Object.Width           =   2381
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   3
            Key             =   "haber"
            Object.Tag             =   "N100"
            Text            =   "HABER"
            Object.Width           =   2381
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Key             =   "retenido"
            Object.Tag             =   "N100"
            Text            =   "retenido"
            Object.Width           =   0
         EndProperty
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "ASIENTOS  CONTABLES SIN NOTA DE CREDITO"
      Height          =   4575
      Left            =   600
      TabIndex        =   15
      Top             =   2820
      Width           =   6660
      Begin MSComctlLib.ListView LVDetalle 
         Height          =   3705
         Left            =   105
         TabIndex        =   16
         Top             =   330
         Width           =   6405
         _ExtentX        =   11298
         _ExtentY        =   6535
         View            =   3
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         Appearance      =   1
         NumItems        =   5
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "T500"
            Text            =   "Cod"
            Object.Width           =   1235
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "T3000"
            Text            =   "CUENTAS"
            Object.Width           =   4762
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   2
            Key             =   "debe"
            Object.Tag             =   "N102"
            Text            =   "DEBE"
            Object.Width           =   2381
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   3
            Key             =   "haber"
            Object.Tag             =   "N102"
            Text            =   "HABER"
            Object.Width           =   2381
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Key             =   "retenido"
            Object.Tag             =   "N100"
            Text            =   "Retenido"
            Object.Width           =   0
         EndProperty
      End
   End
   Begin MSComDlg.CommonDialog Dialogo 
      Left            =   75
      Top             =   2910
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      CancelError     =   -1  'True
   End
   Begin VB.CommandButton CmdSalir 
      Cancel          =   -1  'True
      Caption         =   "Retornar"
      Height          =   390
      Left            =   12495
      TabIndex        =   4
      ToolTipText     =   "Salir"
      Top             =   7695
      Width           =   1335
   End
   Begin VB.Frame FrmPeriodo 
      Caption         =   "Periodo Contable"
      Height          =   2385
      Left            =   585
      TabIndex        =   0
      Top             =   300
      Width           =   13260
      Begin VB.CheckBox ChkContabilizacion 
         Caption         =   "Contabilizacion por cada dcto."
         Height          =   210
         Left            =   10395
         TabIndex        =   33
         ToolTipText     =   "Esto hara que la carga de datos sea mas lenta."
         Top             =   1815
         Width           =   2535
      End
      Begin VB.CheckBox ChkAsientos 
         Caption         =   "Incluir Asientos"
         Height          =   210
         Left            =   8025
         TabIndex        =   32
         Top             =   1815
         Width           =   1470
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel6 
         Height          =   405
         Left            =   6210
         OleObjectBlob   =   "con_librocompra.frx":0000
         TabIndex        =   30
         Top             =   315
         Width           =   4260
      End
      Begin VB.ComboBox CboSucursales 
         Height          =   315
         Left            =   7995
         Style           =   2  'Dropdown List
         TabIndex        =   28
         ToolTipText     =   "Sucursal de la empresa"
         Top             =   1365
         Width           =   5205
      End
      Begin Proyecto2.XP_ProgressBar ProBar 
         Height          =   285
         Left            =   240
         TabIndex        =   27
         Top             =   2055
         Visible         =   0   'False
         Width           =   6795
         _ExtentX        =   11986
         _ExtentY        =   503
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BrushStyle      =   0
         Color           =   49152
         ShowText        =   -1  'True
      End
      Begin VB.CheckBox ChkFolio 
         Caption         =   "Ordenar por Folio"
         Height          =   240
         Left            =   5655
         TabIndex        =   25
         Top             =   1350
         Width           =   1620
      End
      Begin VB.CommandButton Command3 
         Caption         =   "Libro de Ventas"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   8010
         TabIndex        =   24
         Top             =   855
         Width           =   2820
      End
      Begin VB.CommandButton CmdLibroGastos 
         Caption         =   "Asiento Contable Libro Gastos"
         Height          =   495
         Left            =   10845
         TabIndex        =   23
         ToolTipText     =   "Genera Asiento Contable Libro Gastos"
         Top             =   855
         Width           =   2355
      End
      Begin VB.Frame Frame1 
         Caption         =   "Folios"
         Height          =   660
         Left            =   3300
         TabIndex        =   10
         Top             =   270
         Visible         =   0   'False
         Width           =   2280
         Begin VB.TextBox txtAl 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   1350
            TabIndex        =   13
            Top             =   315
            Width           =   855
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
            Height          =   195
            Left            =   1155
            OleObjectBlob   =   "con_librocompra.frx":0110
            TabIndex        =   12
            Top             =   360
            Width           =   180
         End
         Begin VB.TextBox txtDel 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   240
            TabIndex        =   11
            Top             =   315
            Width           =   855
         End
      End
      Begin VB.ComboBox CboTipoLibro 
         Height          =   315
         ItemData        =   "con_librocompra.frx":0172
         Left            =   525
         List            =   "con_librocompra.frx":0174
         Style           =   2  'Dropdown List
         TabIndex        =   9
         Top             =   585
         Width           =   2715
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   195
         Left            =   525
         OleObjectBlob   =   "con_librocompra.frx":0176
         TabIndex        =   6
         Top             =   1020
         Width           =   1320
      End
      Begin VB.CommandButton Command1 
         Caption         =   "Libro Compras"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   5625
         TabIndex        =   3
         ToolTipText     =   "Genera libro de compras"
         Top             =   870
         Width           =   2355
      End
      Begin VB.ComboBox CboMesContable 
         Height          =   315
         ItemData        =   "con_librocompra.frx":01DA
         Left            =   540
         List            =   "con_librocompra.frx":01DC
         Style           =   2  'Dropdown List
         TabIndex        =   1
         Top             =   1215
         Width           =   2715
      End
      Begin VB.ComboBox CboAnoContable 
         Height          =   315
         ItemData        =   "con_librocompra.frx":01DE
         Left            =   3240
         List            =   "con_librocompra.frx":01E0
         Style           =   2  'Dropdown List
         TabIndex        =   2
         Top             =   1215
         Width           =   1440
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   195
         Left            =   3270
         OleObjectBlob   =   "con_librocompra.frx":01E2
         TabIndex        =   7
         Top             =   1035
         Width           =   1320
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   195
         Left            =   525
         OleObjectBlob   =   "con_librocompra.frx":0246
         TabIndex        =   8
         Top             =   375
         Width           =   1320
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel5 
         Height          =   240
         Left            =   7425
         OleObjectBlob   =   "con_librocompra.frx":02D4
         TabIndex        =   29
         Top             =   1350
         Width           =   1650
      End
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   150
      OleObjectBlob   =   "con_librocompra.frx":0342
      Top             =   7485
   End
   Begin MSComctlLib.ListView LvLibro 
      Height          =   2415
      Left            =   495
      TabIndex        =   5
      Top             =   8385
      Width           =   13455
      _ExtentX        =   23733
      _ExtentY        =   4260
      View            =   3
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   17
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Object.Tag             =   "T1500"
         Text            =   "Fecha"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "T.D."
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Object.Tag             =   "T2000"
         Text            =   "SII"
         Object.Width           =   3528
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   3
         Object.Tag             =   "N109"
         Text            =   "Nro"
         Object.Width           =   1587
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Object.Tag             =   "T1500"
         Text            =   "Razon Social"
         Object.Width           =   3528
      EndProperty
      BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   5
         Object.Tag             =   "T1500"
         Text            =   "R.U.T."
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   6
         Object.Tag             =   "N1500"
         Text            =   "Exentas"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   7
         Object.Tag             =   "T800"
         Text            =   "Afectas"
         Object.Width           =   1411
      EndProperty
      BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   8
         Object.Tag             =   "N100"
         Text            =   "IVA"
         Object.Width           =   2117
      EndProperty
      BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   9
         Object.Tag             =   "N100"
         Text            =   "Ret. IVA"
         Object.Width           =   2117
      EndProperty
      BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   10
         Object.Tag             =   "N100"
         Text            =   "Imp Adic."
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(12) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   11
         Object.Tag             =   "N100"
         Text            =   "Imp. Espe."
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(13) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   12
         Object.Tag             =   "N100"
         Text            =   "Total"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(14) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   13
         Object.Tag             =   "N100"
         Text            =   "Comision Vendedor"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(15) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   14
         Object.Tag             =   "N100"
         Text            =   "Costo OT's"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(16) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   15
         Object.Tag             =   "N100"
         Text            =   "Total Costo"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(17) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   16
         Object.Tag             =   "N100"
         Text            =   "UTILIDAD"
         Object.Width           =   2540
      EndProperty
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkEmpresaActiva 
      Height          =   285
      Left            =   -210
      OleObjectBlob   =   "con_librocompra.frx":0576
      TabIndex        =   14
      Top             =   90
      Width           =   5685
   End
   Begin MSComctlLib.ListView LvTemp 
      Height          =   2100
      Left            =   3105
      TabIndex        =   26
      Top             =   8175
      Visible         =   0   'False
      Width           =   6225
      _ExtentX        =   10980
      _ExtentY        =   3704
      View            =   3
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   0
      NumItems        =   4
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Object.Tag             =   "T500"
         Text            =   "Cod"
         Object.Width           =   882
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Object.Tag             =   "T3000"
         Text            =   "CUENTAS"
         Object.Width           =   4762
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   2
         Key             =   "debe"
         Object.Tag             =   "N100"
         Text            =   "DEBE"
         Object.Width           =   2381
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   3
         Key             =   "haber"
         Object.Tag             =   "N100"
         Text            =   "HABER"
         Object.Width           =   2381
      EndProperty
   End
End
Attribute VB_Name = "con_librocompra"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim s_FiltroPeriodo As String
Dim s_Libro As String
Dim s_Declaradas As String
Dim Sp_BodSuc As String
Dim Sm_EsDistribuidorGas As String * 2
Dim CnV As ADODB.Connection
    


Private Sub BoletasVentaPorteriaVegaModelo()
    Sql = "SELECT r.ven_id,tur_apertura,'BOF' td,1 desde,CONCAT('a la ',COUNT(ven_id)) rs,'' rut," & _
            "SUM(ven_valor) exento,0 afectas,0 iva,0 retiva,0 impadic,0 impesp,SUM(ven_valor) total," & _
            "'50' grupo_id,'BOLETA FISCAL' grupo_nombre,'+' doc_signo_libro,'CONTADO' condicionpago,0 bod_id " & _
            "FROM ven_registro r " & _
            "JOIN ven_turno t ON r.tur_id=t.tur_id " & _
            "GROUP BY t.tur_apertura " & _
            "ORDER BY tur_apertura"
End Sub

Private Sub CmdGenera_Click()
    If CboMesContable.ListIndex > -1 And CboAnoContable > -1 Then
        s_FiltroPeriodo = " AND mes_contable=" & CboMesContable.ItemData(CboMesContable.ListIndex) & " AND ano_contable=" & CboAnoContable.Text & " "
        Sql = "SELECT " & _
                "r.fecha,l.doc_abreviado,l.doc_cod_sii sii,r.no_documento,   r.nombre_proveedor, r.rut," & _
                "r.com_exe_otros_imp exentas,r.neto, r.iva,iva_retenido ret_iva,0 imp_adc,0 imp_esp,r.total " & _
            "FROM " & _
                "compra_repuesto_documento As r, sis_documentos As l " & _
            "WHERE " & _
                "r.doc_id = l.doc_id And l.doc_grupo_libro_compra > 0 " & s_FiltroPeriodo & _
            "GROUP BY " & _
                "rut,doc_nombre " & _
            "ORDER BY l.doc_grupo_libro_compra,fecha"
        Consulta RsTmp, Sql
        Set grid.DataSource = RsTmp
        
        If RsTmp.RecordCount > 0 Then
            LvLibro.ListItems.Add , , "LIBRO DE COMPRAS " & Me.CboMesContable.Text & " " & Me.CboAnoContable.Text
            LvLibro.ListItems.Add , , "-"
            LvLibro.ListItems.Add , , " "
            LvLibro.ListItems(LvLibro.ListItems.Count).SubItems(3) = " COD"
            LvLibro.ListItems(LvLibro.ListItems.Count).SubItems(7) = "COMPRAS CON FACTURAS"
            LvLibro.ListItems(LvLibro.ListItems.Count).SubItems(11) = "TOTAL COMPRAS"
            LvLibro.ListItems.Add , , "Fecha"
            LvLibro.ListItems.Add , , "- "
            LvLibro.ListItems(LvLibro.ListItems.Count).SubItems(1) = "TD"
            LvLibro.ListItems(LvLibro.ListItems.Count).SubItems(2) = "SII"
            LvLibro.ListItems(LvLibro.ListItems.Count).SubItems(3) = "DOCUM"
            LvLibro.ListItems(LvLibro.ListItems.Count).SubItems(4) = "RAZON SOCIAL"
            LvLibro.ListItems(LvLibro.ListItems.Count).SubItems(5) = "RUT"
            LvLibro.ListItems(LvLibro.ListItems.Count).SubItems(6) = "EXENTAS"
            LvLibro.ListItems(LvLibro.ListItems.Count).SubItems(7) = "AFECTAS"
            LvLibro.ListItems(LvLibro.ListItems.Count).SubItems(8) = "I.V.A."
            LvLibro.ListItems(LvLibro.ListItems.Count).SubItems(9) = "RET.IVA"
            LvLibro.ListItems(LvLibro.ListItems.Count).SubItems(10) = "IMP.ADIC."
            LvLibro.ListItems(LvLibro.ListItems.Count).SubItems(11) = "IMP.ESPEC"
            'LvLibro.ListItems(LvLibro.ListItems.Count).SubItems(12) = "C/FACTURAS"
            LvLibro.ListItems.Add , , "- "
            With RsTmp
                .MoveFirst
                Do While Not .EOF
                    LvLibro.ListItems.Add , , !Fecha
                    LvLibro.ListItems(LvLibro.ListItems.Count).SubItems(1) = !doc_abreviado
                    LvLibro.ListItems(LvLibro.ListItems.Count).SubItems(2) = !sii
                    LvLibro.ListItems(LvLibro.ListItems.Count).SubItems(3) = !no_documento
                    LvLibro.ListItems(LvLibro.ListItems.Count).SubItems(4) = !nombre_proveedor
                    LvLibro.ListItems(LvLibro.ListItems.Count).SubItems(5) = !Rut
                    LvLibro.ListItems(LvLibro.ListItems.Count).SubItems(6) = !exentas
                    LvLibro.ListItems(LvLibro.ListItems.Count).SubItems(7) = !Neto
                    LvLibro.ListItems(LvLibro.ListItems.Count).SubItems(8) = !Iva
                    LvLibro.ListItems(LvLibro.ListItems.Count).SubItems(9) = !ret_iva
                    LvLibro.ListItems(LvLibro.ListItems.Count).SubItems(10) = !imp_adc
                    LvLibro.ListItems(LvLibro.ListItems.Count).SubItems(11) = !imp_esp
                    LvLibro.ListItems(LvLibro.ListItems.Count).SubItems(12) = !Total
                    .MoveNext
                Loop
            End With
            CreaArchivoPlano
        End If
    End If
End Sub
Private Sub CreaArchivoPlano()
    Dim c_Fecha As String * 10
    Dim c_TD As String * 3
    Dim c_Sii As String * 2
    Dim c_Nro As String * 12
    Dim c_RS As String * 20
    Dim c_RUT As String * 12
    Dim c_Exentas As String * 9
    Dim c_Afectas As String * 9
    Dim c_IVA As String * 10
    Dim c_RetIva As String * 9
    Dim c_ImpAd As String * 9
    Dim c_ImpEsp As String * 9
    Dim c_Total As String * 11
    X = FreeFile
    Open App.Path & "\libro.txt" For Output As X
        With LvLibro
            For i = 1 To .ListItems.Count
                c_Fecha = .ListItems(i)
                c_TD = .ListItems(i).SubItems(1)
                c_Sii = .ListItems(i).SubItems(2)
                c_Nro = .ListItems(i).SubItems(3)
                c_RS = .ListItems(i).SubItems(4)
                c_RUT = .ListItems(i).SubItems(5)
                RSet c_Exentas = .ListItems(i).SubItems(6)
                RSet c_Afectas = .ListItems(i).SubItems(7)
                RSet c_IVA = .ListItems(i).SubItems(8)
                RSet c_RetIva = .ListItems(i).SubItems(9)
                RSet c_ImpAd = .ListItems(i).SubItems(10)
                RSet c_ImpEsp = .ListItems(i).SubItems(11)
                RSet c_Total = .ListItems(i).SubItems(12)
                Print #X, c_Fecha & " " & c_TD & " " & c_Sii & " " & c_Nro & " " & c_RS & " " & c_RUT & _
                         " " & c_Exentas & " " & c_Afectas & " " & c_IVA & " " & c_RetIva & " " & c_ImpAd & _
                         " " & c_ImpEsp & " " & c_Total
            Next i
        End With
    Close #X
End Sub


Private Sub CboTipoLibro_Click()
    If CboTipoLibro.ListIndex = 2 Then
        Frame1.Visible = True
    Else
        Frame1.Visible = False
    End If
End Sub

Private Sub ChkFolio_Click()
    Command1.SetFocus
End Sub

Private Sub CmdExportaCNC_Click()
    Dim tit(2) As String
    FraProgreso.Visible = True
    tit(0) = "ASIENTO CONTABLE CON NOTA DE CREDITO"
    tit(1) = s_Libro & " " & Me.CboMesContable.Text & " " & Me.CboAnoContable.Text
    tit(2) = Time
    ExportarNuevo LvCDetalle, tit, Me, PBar
    FraProgreso.Visible = False
End Sub

Private Sub cmdExportar_Click()
    Dim tit(2) As String
    FraProgreso.Visible = True
    tit(0) = "ASIENTO CONTABLE SIN NOTA DE CREDITO"
    tit(1) = s_Libro & " " & Me.CboMesContable.Text & " " & Me.CboAnoContable.Text
    tit(2) = Time
    ExportarNuevo LvDetalle, tit, Me, PBar
    FraProgreso.Visible = False
End Sub

Private Sub CmdLibroGastos_Click()
    Dim Lp_Caja As Long, Sp_Fecha As String
    
    
    Sp_Fecha = " AND mes_contable=" & Me.CboMesContable.ItemData(CboMesContable.ListIndex) & " AND ano_contable=" & Me.CboAnoContable.Text & " "
    
    LLenar_Grilla AsientoContable(Sp_Fecha, 4), Me, LvDetalle, False, True, True, False
    LvDetalle.ListItems.Add , , ""
    LvDetalle.ListItems(LvDetalle.ListItems.Count).SubItems(1) = "TOTALES"
    LvDetalle.ListItems(LvDetalle.ListItems.Count).SubItems(2) = NumFormat(TotalizaColumna(LvDetalle, "debe"))
    LvDetalle.ListItems(LvDetalle.ListItems.Count).SubItems(3) = NumFormat(TotalizaColumna(LvDetalle, "haber"))
    LvDetalle.ListItems(LvDetalle.ListItems.Count).ListSubItems(1).Bold = True
    LvDetalle.ListItems(LvDetalle.ListItems.Count).ListSubItems(2).Bold = True
    LvDetalle.ListItems(LvDetalle.ListItems.Count).ListSubItems(3).Bold = True

End Sub

Private Sub cmdSalir_Click()
    Unload Me
End Sub
Private Sub LibroTimbraje()
    
    'Realiazar impresion por codigo de hojas para timbraje
    If Val(txtDel) > 0 And Val(txtAl) > 0 Then
        If Val(txtAl) >= Val(txtDel) Then
            Sql = "SELECT * " & _
                  "FROM sis_empresas " & _
                  "WHERE rut='" & SP_Rut_Activo & "'"
            Consulta RsTmp3, Sql
            If RsTmp3.RecordCount > 0 Then
                
            
                RsEmpresa = "EMPRESA    :" & RsTmp3!nombre_empresa
                DireccionEmpresa = "DIRECCION  :" & RsTmp3!direccion & " - " & RsTmp3!ciudad
                Sp_Giro = "GIRO       :" & RsTmp3!giro
                Sp_Rut = "RUT        :" & RsTmp3!Rut
                If Not IsNull(RsTmp3!rut_rl) Then sp_rlrut = "RUT RL     :" & RsTmp3!rut_rl
                If Not IsNull(RsTmp3!nombre_rl) Then sp_rlnombre = "NOMBRE RL  :" & RsTmp3!nombre_rl
                On Error GoTo canceloImpresion
                Me.Dialogo.ShowPrinter
                Printer.ScaleMode = 7
                For i = Val(txtDel) To Val(txtAl)
                    Printer.Orientation = vbPRORLandscape
                    Printer.ScaleMode = vbCentimeters
                    Printer.DrawMode = 1
                    Printer.FontName = "Courier New"
                    Printer.FontSize = 8
                    linea = 0.5
                    H = 1.3
                    interlineado = 0.3
                    Printer.CurrentY = linea
                    Printer.CurrentX = 18
                    Printer.Print "FOLIO :" & i
                    Printer.CurrentY = linea
                    Printer.CurrentX = H
                    Printer.Print RsEmpresa
                    Printer.CurrentX = H
                    linea = linea + interlineado
                    Printer.CurrentY = linea
                    Printer.Print Sp_Rut
                    Printer.CurrentX = H
                    linea = linea + interlineado
                    Printer.CurrentY = linea
                    Printer.Print DireccionEmpresa
                    Printer.CurrentX = H
                    linea = linea + interlineado
                    Printer.CurrentY = linea
                    Printer.Print Sp_Giro
                    Printer.CurrentX = H
                    linea = linea + interlineado
                    Printer.CurrentY = linea
                    Printer.Print sp_rlrut
                    Printer.CurrentX = H
                    linea = linea + interlineado
                    Printer.CurrentY = linea
                    Printer.Print sp_rlnombre
                    
                    
                    
                    Printer.NewPage 'activar lo enviado a la
                    Printer.EndDoc 'impresora, o sea , que imprima lo que le hemos enviado mediante los printer
                Next
                
            End If
        End If
    End If
    Exit Sub
canceloImpresion:
    'Solo cancelo la impresion
End Sub
Private Sub Command1_Click()
    
    Dim Lp_Debe As Long, Lp_LineaNC As Long, Lp_CreditoFiscal As Long, Lp_Haber As Long, Lp_Proveedores As Long
    Dim Lp_P As Long, Ip_Grupo As Integer, Sp_Grupo As String, Lp_ImpDC As Long
    Dim Lp_Total As Long, Lp_NroDoctos As Long, Lp_Neto As Long, Lp_Iva As Long, Lp_Ret As Long, Lp_Otros As Long, Lp_Exento As Long
    Dim Lp_TotalIvaSinCredito As Long, Sp_CampoExtra As String, Sp_FieldExtra As String
    Dim Lp_TotalIvaMargen As Long
    Dim Lp_MargenDistribuidor As Long
    Dim Rp_CamposExtras As Recordset
    Dim Lp_TCamposExtras(26) As Long
    
    If CboTipoLibro.ListIndex = 2 Then
        LibroTimbraje
        Exit Sub
    End If
    Sp_BodSuc = ""
    If Me.CboSucursales.Text <> "TODAS" Then
        Sql = "SELECT bod_id " & _
                "FROM par_bodegas " & _
                "WHERE sue_id=" & CboSucursales.ItemData(CboSucursales.ListIndex) & " AND rut_emp='" & SP_Rut_Activo & "'"
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
            Sp_BodSuc = " AND bod_id=" & RsTmp!bod_id & " "
        End If
    End If
    ProBar.Visible = True
    '***************************************************************************************************
    '     LIBRO DE COMPRA LISTADO - RESUMEN Y ASIENTO CONTABLE
    '***************************************************************************************************
    s_FiltroPeriodo = " AND mes_contable=" & CboMesContable.ItemData(CboMesContable.ListIndex) & " AND ano_contable=" & CboAnoContable.Text & " "
    s_Libro = "LIBRO DE COMPRAS"
    s_Declaradas = "Total Compras Declaradas"
    Sql = "TRUNCATE con_libro_compra_temporal"
    cn.Execute Sql
    
    
    Sql = "SELECT e.imp_id,imp_nombre " & _
            "FROM par_impuestos_empresas e " & _
            "JOIN par_impuestos i ON e.imp_id=i.imp_id " & _
            "WHERE  rut='" & SP_Rut_Activo & "'"
    Consulta Rp_CamposExtras, Sql
    
    If Rp_CamposExtras.RecordCount > 0 Then
        Rp_CamposExtras.MoveFirst
        m = 14
        
        c = 1
        Sp_CampoExtra = ""
        Sp_FieldExtra = ""
        Do While Not Rp_CamposExtras.EOF
            
            Sp_FieldExtra = Sp_FieldExtra & "imp_extra" & Trim(Str(c)) & ","
            c = c + 1
            Sp_CampoExtra = Sp_CampoExtra & "(SELECT SUM(coi_valor) " & _
                                "FROM com_impuestos i " & _
                                "INNER JOIN par_impuestos_empresas e USING(imp_id) " & _
                                "WHERE e.imp_id=" & Rp_CamposExtras!imp_id & " AND l.doc_orden_de_compra = 'NO' AND e.rut ='" & SP_Rut_Activo & "' AND i.id = r.id),"
            Rp_CamposExtras.MoveNext
        Loop
        If Len(Sp_CampoExtra) > 0 Then
            Sp_CampoExtra = "," & Mid(Sp_CampoExtra, 1, Len(Sp_CampoExtra) - 1)
            Sp_FieldExtra = "," & Mid(Sp_FieldExtra, 1, Len(Sp_FieldExtra) - 1)
        End If
    
    End If
    
  
        
    
    
    
            
    LvDetalle.ColumnHeaders.Add
    
    Sql = "INSERT INTO con_libro_compra_temporal (tmp_id,fecha,td,sii,nro,rs,rut,exentas,afectas,iva,retiva,impadic,impesp,total,grupo_id,grupo_nombre,signo,fpago,folio,doc_id,bod_id,tasa_iva,iva_sin_credito,iva_margen,margen_dist" & Sp_FieldExtra & ",monto_activo_fijo)" & _
          "SELECT " & _
            "r.id tmp_id,r.fecha,l.doc_abreviado td,l.doc_cod_sii sii,r.no_documento nro," & _
            "(SELECT nombre_empresa FROM maestro_proveedores p WHERE p.rut_proveedor=r.rut LIMIT 1) rs," & _
            "r.rut,r.com_exe_otros_imp exentas,r.neto afectas,r.iva,iva_retenido retiva," & _
            " (SELECT IF(e.ime_costo_credito='COSTO',SUM(coi_valor),0) impadic " & _
                    "FROM com_impuestos i " & _
                    "INNER JOIN par_impuestos_empresas e USING(imp_id) " & _
                    "WHERE  l.doc_orden_de_compra='NO' AND  e.rut='" & SP_Rut_Activo & "' AND i.id=r.id) impadic," & _
            "(SELECT IF(e.ime_costo_credito='CREDITO',SUM(coi_valor),0) impesp " & _
                    "FROM com_impuestos i " & _
                    "INNER JOIN par_impuestos_empresas e USING(imp_id) " & _
                    "WHERE   l.doc_orden_de_compra='NO' AND e.rut='" & SP_Rut_Activo & "' AND  i.id=r.id) impesp, " & _
            "r.total,l.doc_grupo_libro_compra  grupo_id, l.doc_nombre_grupo grupo_nombre,doc_signo_libro,pago,com_folio_texto,r.doc_id,bod_id,com_tasa_iva,com_iva_sin_credito,com_iva_margen,com_margen_distribuidor " & _
            Sp_CampoExtra & " " & _
            ",/*columna activo fijo comprado */ (SELECT SUM(cmd_costo_real_neto) FROM com_doc_compra_detalle d WHERE r.id=d.id AND " & _
                "(SELECT COUNT(pla_id) " & _
                    "FROM con_plan_de_cuentas p " & _
                    "WHERE tpo_id=1 AND det_id=4 AND pla_analisis='SI' AND p.pla_id=d.pla_id)>0) activo_fijoo " & _
            "FROM com_doc_compra As r, sis_documentos As l " & _
            "WHERE com_informa_compra='SI' AND doc_contable='SI' AND  l.doc_orden_de_compra='NO' AND r.rut_emp='" & SP_Rut_Activo & "' AND r.doc_id = l.doc_id  AND l.doc_grupo_libro_compra > 0    AND MONTH(fecha)=" & CboMesContable.ItemData(CboMesContable.ListIndex) & " " & _
            " AND YEAR(fecha)= " & CboAnoContable.Text & " " & s_FiltroPeriodo & Sp_BodSuc & _
            " ORDER BY l.doc_grupo_libro_compra,fecha "
    cn.Execute Sql
    
    
    'meses anteriores
    Sql = "INSERT INTO con_libro_compra_temporal (tmp_id,fecha,td,sii,nro,rs,rut,exentas,afectas,iva,retiva,impadic,impesp,total,grupo_id,grupo_nombre,signo,fpago,folio,doc_id,bod_id,tasa_iva,iva_sin_credito,iva_margen,margen_dist" & Sp_FieldExtra & ",monto_activo_fijo)" & _
            "SELECT r.id tmp_id,r.fecha ,l.doc_abreviado td, l.doc_cod_sii sii, r.no_documento nro," & _
        "(SELECT nombre_empresa FROM maestro_proveedores p WHERE p.rut_proveedor=r.rut LIMIT 1) rs," & _
        "r.rut,r.com_exe_otros_imp exentas,r.neto afectas, r.iva ,iva_retenido retiva," & _
            " (SELECT IF(e.ime_costo_credito='COSTO',SUM(coi_valor),0) impadic " & _
                    "FROM com_impuestos i " & _
                    "INNER JOIN par_impuestos_empresas e USING(imp_id) " & _
                    "WHERE  l.doc_orden_de_compra='NO' AND   e.rut='" & SP_Rut_Activo & "' AND  i.id=r.id) impadic," & _
            "(SELECT IF(e.ime_costo_credito='CREDITO',SUM(coi_valor),0) impesp " & _
                    "FROM com_impuestos i " & _
                    "INNER JOIN par_impuestos_empresas e USING(imp_id) " & _
                    "WHERE   l.doc_orden_de_compra='NO' AND  e.rut='" & SP_Rut_Activo & "' AND  i.id=r.id) impesp, " & _
        "r.total,(l.doc_grupo_libro_compra +100) grupo_id ,CONCAT('MESES ANTERIORES',' ',l.doc_nombre_grupo ) grupo_nombre,doc_signo_libro,pago,com_folio_texto,r.doc_id,bod_id,com_tasa_iva,com_iva_sin_credito,com_iva_margen,com_margen_distribuidor " & _
        Sp_CampoExtra & " " & _
          ",/*columna activo fijo comprado */ (SELECT SUM(cmd_costo_real_neto) FROM com_doc_compra_detalle d WHERE r.id=d.id AND " & _
                "(SELECT COUNT(pla_id) " & _
                    "FROM con_plan_de_cuentas p " & _
                    "WHERE tpo_id=1 AND det_id=4 AND pla_analisis='SI' AND p.pla_id=d.pla_id)>0) activo_fijoo " & _
        "FROM com_doc_compra As r, sis_documentos As l " & _
        "WHERE  com_informa_compra='SI' AND doc_contable='SI' AND l.doc_orden_de_compra='NO' AND  r.rut_emp='" & SP_Rut_Activo & "' AND " & _
            "r.doc_id = l.doc_id And l.doc_grupo_libro_compra > 0 And " & _
            "(Month(FECHA)<>" & CboMesContable.ItemData(CboMesContable.ListIndex) & " OR " & _
            "YEAR(fecha)<>" & CboAnoContable.Text & ") " & s_FiltroPeriodo & " " & Sp_BodSuc & _
        "ORDER BY  l.doc_grupo_libro_compra , fecha"
    cn.Execute Sql
    


    
 '   Sql = "SELECT " & _
                    "fecha,td doc_abreviado,sii,nro , rs,rut,IF(signo='+',exentas,exentas*-1) exentas," & _
                    "IF(signo='+',afectas,afectas*-1) neto ," & _
                    "IF(signo='+',iva,iva*-1) iva,IF(signo='+',retiva,retiva*-1) ret_iva," & _
                    "IF(signo='+',impadic +impesp,(impadic+impesp)*-1) otrosimp," & _
                    "IF(signo='+',total,total*-1) total,grupo_id grupo,folio,grupo_nombre,doc_id,tasa_iva,iva_sin_credito,iva_margen,margen_dist" & Sp_FieldExtra & _
                            " FROM " & _
                                "con_libro_compra_temporal "
                                
   Sql = "SELECT " & _
                    "fecha,td doc_abreviado,sii,nro , rs,rut,IF(signo='+',exentas,exentas*-1) exentas," & _
                    "IF(signo='+',afectas,afectas*-1) neto ," & _
                    "IF(signo='+',iva,iva*-1) iva,IF(signo='+',retiva,retiva*-1) ret_iva," & _
                    "0 otrosimp," & _
                    "IF(signo='+',total,total*-1) total,grupo_id grupo,folio,grupo_nombre,doc_id,tasa_iva,iva_sin_credito,iva_margen,margen_dist" & Sp_FieldExtra & ",monto_activo_fijo " & _
                            " FROM " & _
                                "con_libro_compra_temporal "

    If Me.ChkFolio.Value = 1 Then Sql = Sql & " ORDER BY folio"
        
    Consulta RsCom, Sql
    If RsCom.RecordCount = 0 Then
        MsgBox "No se encontraron registros en el periodo contable seleccionado...", vbInformation
    '    Exit Sub
    End If
    With Sis_PrevisualizarLibro.LvLibro
        Sis_PrevisualizarLibro.LvLibro.ListItems.Clear
        If Sm_EsDistribuidorGas = "SI" Then
                'habilitar columnas relacionadqaas con distribuidoras de gas
                .ColumnHeaders(13).Width = 1440
                .ColumnHeaders(14).Width = 1440
                .ColumnHeaders(28).Width = 1440
        End If
        If RsCom.RecordCount > 0 Then
             If Rp_CamposExtras.RecordCount > 0 Then
                    Rp_CamposExtras.MoveFirst
                    m = 15
                    
                  
                    Do While Not Rp_CamposExtras.EOF
                        .ColumnHeaders(m).Text = Rp_CamposExtras!imp_nombre
                        .ColumnHeaders(m).Width = 1440
                        m = m + 1
                        Rp_CamposExtras.MoveNext
                    Loop
                    
                   ' If m < 26 Then
                   '     For i = m To 26
                   '         .ColumnHeaders(i).Width = 0
                   '     Next i
                   ' End If
                    
            End If
            
            
            
            RsCom.MoveFirst
            Ip_Grupo = RsCom!grupo
            Sp_Grupo = RsCom!grupo_nombre
            Lp_Total = 0 ' RsCom!Total
            Lp_NroDoctos = 0
            Do While Not RsCom.EOF
                If Ip_Grupo <> RsCom!grupo And ChkFolio.Value = 0 Then
                    .ListItems.Add , , ""
                    Lp_P = .ListItems.Count
                    
                    .ListItems(Lp_P).SubItems(1) = "No Doctos:"
                    .ListItems(Lp_P).SubItems(2) = Lp_NroDoctos
                    .ListItems(Lp_P).SubItems(5) = "TOTAL " & Sp_Grupo
                    .ListItems(Lp_P).SubItems(7) = NumFormat(Lp_Exento)
                    .ListItems(Lp_P).SubItems(8) = NumFormat(Lp_Neto)
                    .ListItems(Lp_P).SubItems(9) = NumFormat(Lp_Iva)
                    .ListItems(Lp_P).SubItems(10) = NumFormat(Lp_Ret)
                    .ListItems(Lp_P).SubItems(11) = NumFormat(Lp_Otros)
                    .ListItems(Lp_P).SubItems(12) = NumFormat(Lp_TotalIvaSinCredito)
                    .ListItems(Lp_P).SubItems(13) = NumFormat(Lp_TotalIvaMargen)
                    
                    
                    'TOTALES campos extras en el list view
                        '8 12 2014
                    If Rp_CamposExtras.RecordCount > 0 Then
                        Rp_CamposExtras.MoveFirst
                        c = 14
                        C2 = 20
                        Do While Not Rp_CamposExtras.EOF
                            .ListItems(Lp_P).SubItems(c) = NumFormat(Lp_TCamposExtras(c))         'NumFormat(RsCom.Fields(c2))
                            C2 = C2 + 1
                            c = c + 1
                            Rp_CamposExtras.MoveNext
                        Loop
                        '***************************************************
                    End If
                    
                    .ListItems(Lp_P).SubItems(26) = NumFormat(Lp_Total)
                    .ListItems(Lp_P).SubItems(27) = NumFormat(Lp_MargenDistribuidor)
                    
                    Ip_Grupo = RsCom!grupo
                    Sp_Grupo = RsCom!grupo_nombre
                    Lp_Total = 0
                    Lp_Neto = 0
                    Lp_Iva = 0
                    Lp_Ret = 0
                    Lp_Otros = 0
                    Lp_Exento = 0
                    Lp_TotalIvaSinCredito = 0
                    Lp_TotalIvaMargen = 0
                    Lp_NroDoctos = 0
                    Lp_MargenDistribuidor = 0
                    
                    
                    'TOTALES EN 0 campos extras en el list view
                        '8 12 2014
                    If Rp_CamposExtras.RecordCount > 0 Then
                            Rp_CamposExtras.MoveFirst
                            c = 14
                            C2 = 20
                            Do While Not Rp_CamposExtras.EOF
                                Lp_TCamposExtras(c) = 0        'NumFormat(RsCom.Fields(c2))
                                C2 = C2 + 1
                                c = c + 1
                                Rp_CamposExtras.MoveNext
                            Loop
                    End If
                    '***************************************************
                    
                    
                    'ver
                    
                    For i = 1 To .ColumnHeaders.Count - 5
                        'negrita
                        .ListItems(Lp_P).ListSubItems(i).Bold = True
                    Next
                    .ListItems.Add , , ""
                End If
                Lp_NroDoctos = Lp_NroDoctos + 1
                
              'mario 17 de julio 2013 + Val(
              
                If IsNull(RsCom!Total) Then
                    Lp_Total = 0
                Else
                    Lp_Total = Lp_Total + Val(RsCom!Total)
                End If
                If IsNull(RsCom!Neto) Then
                    Lp_Neto = 0
                Else
                    Lp_Neto = Lp_Neto + Val(RsCom!Neto)
                End If
                If IsNull(RsCom!Iva) Then
                    Lp_Iva = 0
                Else
                    Lp_Iva = Lp_Iva + Val(RsCom!Iva)
                End If
                If IsNull(RsCom!ret_iva) Then
                    Lp_Ret = 0
                Else
                    Lp_Ret = Lp_Ret + Val(RsCom!ret_iva)
                End If
                If IsNull(RsCom!otrosimp) Then
                    Lp_Otros = 0
                Else
                    Lp_Otros = Lp_Otros + Val(RsCom!otrosimp)
                End If
                If IsNull(RsCom!exentas) Then
                    Lp_Exento = 0
                Else
                    Lp_Exento = Lp_Exento + Val(RsCom!exentas)
                End If
                
                If IsNull(RsCom!iva_sin_credito) Then
                   ' Lp_TotalIvaSinCredito = 0
                Else
                    Lp_TotalIvaSinCredito = Lp_TotalIvaSinCredito + Val(RsCom!iva_sin_credito)
                End If
                
                If IsNull(RsCom!iva_margen) Then
                   ' Lp_TotalIvaMargen = 0
                Else
                    Lp_TotalIvaMargen = Lp_TotalIvaMargen + Val(RsCom!iva_margen)
                End If
                
                If IsNull(RsCom!margen_dist) Then
                    '
                Else
                    Lp_MargenDistribuidor = Lp_MargenDistribuidor + Val(RsCom!margen_dist)
                End If
                
                'TOTALES ACUMULADOS campos extras en el list view
                '8 12 2014
                If Rp_CamposExtras.RecordCount > 0 Then
                    Rp_CamposExtras.MoveFirst
                    c = 14
                    C2 = 20
                    Do While Not Rp_CamposExtras.EOF
                         Lp_TCamposExtras(c) = Lp_TCamposExtras(c) + Val("" & RsCom.Fields(C2))
                        C2 = C2 + 1
                        c = c + 1
                        Rp_CamposExtras.MoveNext
                    Loop
                    '***************************************************
                End If
                
                
                
               
                .ListItems.Add , , RsCom!Folio
                Lp_P = .ListItems.Count
                .ListItems(Lp_P).SubItems(1) = Format(RsCom!Fecha, "DD-MM-YY")
                .ListItems(Lp_P).SubItems(2) = RsCom!doc_abreviado
                .ListItems(Lp_P).SubItems(3) = RsCom!sii
                .ListItems(Lp_P).SubItems(4) = RsCom!Nro
                .ListItems(Lp_P).SubItems(5) = NumFormat(RsCom!rs)
                .ListItems(Lp_P).SubItems(6) = RsCom!Rut
                .ListItems(Lp_P).SubItems(7) = NumFormat(RsCom!exentas)
                .ListItems(Lp_P).SubItems(8) = NumFormat(RsCom!Neto)
                .ListItems(Lp_P).SubItems(9) = NumFormat(RsCom!Iva)
                .ListItems(Lp_P).SubItems(10) = NumFormat(RsCom!ret_iva)
                .ListItems(Lp_P).SubItems(11) = NumFormat(RsCom!otrosimp)
                .ListItems(Lp_P).SubItems(12) = NumFormat(RsCom!iva_sin_credito)
                .ListItems(Lp_P).SubItems(13) = NumFormat(RsCom!iva_margen)
                .ListItems(Lp_P).SubItems(31) = NumFormat(RsCom!monto_activo_fijo)
                
                'campos extras en el list view
                '8 12 2014
                If Rp_CamposExtras.RecordCount > 0 Then
                    Rp_CamposExtras.MoveFirst
                    c = 14
                    C2 = 20
                    Do While Not Rp_CamposExtras.EOF
                        .ListItems(Lp_P).SubItems(c) = NumFormat(RsCom.Fields(C2))
                        C2 = C2 + 1
                        c = c + 1
                        Rp_CamposExtras.MoveNext
                    Loop
                End If               '***************************************************
                 
                .ListItems(Lp_P).SubItems(26) = NumFormat(RsCom!Total)
                .ListItems(Lp_P).SubItems(27) = NumFormat(RsCom!margen_dist)
                .ListItems(Lp_P).SubItems(29) = NumFormat(RsCom!doc_id)
                .ListItems(Lp_P).SubItems(30) = NumFormat(RsCom!tasa_iva)
                RsCom.MoveNext
            Loop
        
             If ChkFolio.Value = 0 Then ' Aqui es libro normal, cuando es 1 se ordena solo por folio
                 .ListItems.Add , , ""
                 Lp_P = .ListItems.Count
                 .ListItems(Lp_P).SubItems(1) = "No Doctos:"
                 .ListItems(Lp_P).SubItems(2) = Lp_NroDoctos
                 .ListItems(Lp_P).SubItems(5) = "TOTAL " & Sp_Grupo
                 .ListItems(Lp_P).SubItems(7) = NumFormat(Lp_Exento)
                 .ListItems(Lp_P).SubItems(8) = NumFormat(Lp_Neto)
                 .ListItems(Lp_P).SubItems(9) = NumFormat(Lp_Iva)
                 .ListItems(Lp_P).SubItems(10) = NumFormat(Lp_Ret)
                 .ListItems(Lp_P).SubItems(11) = NumFormat(Lp_Otros)
                 
               
                 For i = 12 To 27
                    .ListItems(Lp_P).SubItems(i) = 0
                 Next
                 .ListItems(Lp_P).SubItems(12) = NumFormat(Lp_TotalIvaSinCredito)
                 .ListItems(Lp_P).SubItems(13) = NumFormat(Lp_TotalIvaMargen)
                 .ListItems(Lp_P).SubItems(26) = NumFormat(Lp_Total)
                 .ListItems(Lp_P).SubItems(27) = NumFormat(Lp_MargenDistribuidor)
                 
                 'ver
                 
                 For i = 1 To .ColumnHeaders.Count - 5
                     .ListItems(Lp_P).ListSubItems(i).Bold = True
                 Next
             End If
             .ListItems.Add , , ""
             .ListItems.Add , , ""
             Lp_P = .ListItems.Count
             .ListItems(Lp_P).SubItems(5) = "TOTALES"
             
             For i = 7 To 27
                 .ListItems(Lp_P).SubItems(i) = 0
                 For X = 1 To .ListItems.Count - 2
                     If .ListItems(X).SubItems(1) <> "No Doctos:" And Len(.ListItems(X).SubItems(1)) > 0 Then
                            If Val(.ListItems(X).SubItems(i)) = 0 Then
                                .ListItems(X).SubItems(i) = 0
                            End If
                         .ListItems(Lp_P).SubItems(i) = .ListItems(Lp_P).SubItems(i) + CDbl(.ListItems(X).SubItems(i))
                     End If
                 Next
             Next
             For i = 7 To 26
             
                 .ListItems(Lp_P).SubItems(i) = NumFormat(.ListItems(Lp_P).SubItems(i))
             Next
            For i = 1 To 26
                 .ListItems(Lp_P).ListSubItems(i).Bold = True
            Next
            
            'Hasta aqui el libro de compras
            'Ahora ver las cuentas asociadas
            'INFORMACION DE CUENTAS CONTABLES
            ProBar.Max = .ListItems.Count
            If .ListItems.Count > 0 Then
                For i = 1 To .ListItems.Count
                    If Len(.ListItems(i)) > 0 Then
                        Sql = "SELECT pla_nombre,SUM(cmd_total_neto+cmd_exento) total_cuenta " & _
                                "FROM com_doc_compra_detalle d " & _
                                "JOIN con_plan_de_cuentas p ON d.pla_id=p.pla_id " & _
                                "JOIN com_doc_compra c ON d.id=c.id " & _
                                "WHERE rut_emp='" & SP_Rut_Activo & "' AND com_folio_texto='" & .ListItems(i) & "' AND mes_contable=" & CboMesContable.ItemData(CboMesContable.ListIndex) & " AND ano_contable=" & CboAnoContable.Text & " " & _
                                "GROUP BY d.pla_id"
                    
                    
                       ' Sql = "SELECT pla_nombre,total_cuenta " & _
                                "FROM vi_contabilidad_detalle_cuentas_libro " & _
                                "WHERE rut_emp='" & SP_Rut_Activo & "' AND com_folio_texto='" & .ListItems(i) & "' AND mes_contable=" & CboMesContable.ItemData(CboMesContable.ListIndex) & " AND ano_contable=" & CboAnoContable.Text
                        Consulta RsTmp, Sql
                        If RsTmp.RecordCount > 0 Then
                            detallecuenta = ""
                            Do While Not RsTmp.EOF
                                detallecuenta = detallecuenta & RsTmp!pla_nombre & " $" & RsTmp!total_cuenta & " - "
                                RsTmp.MoveNext
                            Loop
                            .ListItems(i).SubItems(28) = detallecuenta
                        Else
                            
                        
                        
                            Sql = "SELECT pla_nombre,SUM(cmd_total_neto) total_cuenta " & _
                                   "FROM com_doc_compra_detalle d " & _
                                   "JOIN con_plan_de_cuentas p ON d.pla_id = p.pla_id " & _
                                   "JOIN com_doc_compra v ON d.id=v.id " & _
                                   "WHERE   v.rut_emp = '" & SP_Rut_Activo & "' AND v.nro_factura=" & .ListItems(i).SubItems(4) & " AND v.doc_id_factura=" & .ListItems(i).SubItems(14) & " " & _
                                   " GROUP BY d.pla_id"
                                   Consulta RsTmp, Sql
                                   If RsTmp.RecordCount > 0 Then
                                       detallecuenta = ""
                                       Do While Not RsTmp.EOF
                                           detallecuenta = detallecuenta & RsTmp!pla_nombre & " $" & RsTmp!total_cuenta & " - "
                                           RsTmp.MoveNext
                                       Loop
                                      .ListItems(i).SubItems(28) = detallecuenta
                                   End If
                        End If
                    End If
                    ProBar.Value = i
                Next
            End If
              
      End If
        
    
                
    
    End With
    

    
    
    
    
    
    Sql = "SELECT * " & _
              "FROM sis_empresas " & _
              "WHERE rut='" & SP_Rut_Activo & "'"
    Consulta RsTmp3, Sql
    If RsTmp3.RecordCount > 0 Then
        RsEmpresa = RsTmp3!nombre_empresa
        DireccionEmpresa = RsTmp3!direccion
        Sp_Giro = RsTmp3!giro
        Sp_Ciudad = RsTmp3!ciudad
        Sp_Rut = RsTmp3!Rut
        If Not IsNull(RsTmp3!rut_rl) Then sp_rlrut = "RUT RL     :" & RsTmp3!rut_rl
        If Not IsNull(RsTmp3!nombre_rl) Then sp_rlnombre = "NOMBRE RL  :" & RsTmp3!nombre_rl
        
    End If
    With Sis_PrevisualizarLibro
        .LvDetalle.ListItems.Add , , "NOMBRE EMP.:" & RsEmpresa
        .LvDetalle.ListItems.Add , , "RUT EMP.   :" & Sp_Rut
        .LvDetalle.ListItems.Add , , "GIRO       :" & Sp_Giro
        .LvDetalle.ListItems.Add , , "DIRECCION  :" & DireccionEmpresa & " - " & Sp_Ciudad
        .LvDetalle.ListItems.Add , , sp_rlrut
        .LvDetalle.ListItems.Add , , sp_rlnombre
        .Caption = "LIBRO DE COMPRAS"
    End With

    Sis_PrevisualizarLibro.SkLibro.Caption = "LIBRO DE COMPRAS " & CboMesContable.Text & "  " & CboAnoContable.Text
    
    
    '*
    '***************************************************************
    
    'R E S U M E N     L I B R O    C O M P R A
    '**********************************************5******************
    
    
    
    If Rp_CamposExtras.RecordCount > 0 Then 'aqui se arman los campos extras
        Rp_CamposExtras.MoveFirst
        m = 14
        
        c = 1
        Sp_CampoExtra = ""
        Sp_FieldExtra = ""
        
      '  Do While Not Rp_CamposExtras.EOF
       '     Sis_PrevisualizarLibro.LvResumen.ColumnHeaders(9 + C).Width = 1500
        '    Sis_PrevisualizarLibro.LvResumen.ColumnHeaders(9 + C).Text = Rp_CamposExtras!imp_nombre
        '    Sp_FieldExtra = Sp_FieldExtra & "imp_extra" & Trim(Str(C)) & ","
        '    C = C + 1
        '    Sp_CampoExtra = Sp_CampoExtra & "(SELECT SUM(coi_valor) " & _
        '                        "FROM com_impuestos i " & _
        '                        "INNER JOIN par_impuestos_empresas e USING(imp_id) " & _
        '                        "WHERE e.imp_id=" & Rp_CamposExtras!imp_id & " AND d.doc_orden_de_compra = 'NO' AND e.rut ='" & SP_Rut_Activo & "' AND i.id = detalle.id),"
        '    Rp_CamposExtras.MoveNext
        '
        'Loop
        
        Do While Not Rp_CamposExtras.EOF
            Sis_PrevisualizarLibro.LvResumen.ColumnHeaders(9 + c).Width = 1500
            Sis_PrevisualizarLibro.LvResumen.ColumnHeaders(9 + c).Text = Rp_CamposExtras!imp_nombre
            Sp_FieldExtra = Sp_FieldExtra & "IF(signo='-',SUM(imp_extra" & Trim(Str(c)) & ")*-1,SUM(imp_extra" & Trim(Str(c)) & ")),"
            c = c + 1
            Sp_CampoExtra = Sp_CampoExtra & "(SELECT SUM(coi_valor) " & _
                                "FROM com_impuestos i " & _
                                "INNER JOIN par_impuestos_empresas e USING(imp_id) " & _
                                "WHERE e.imp_id=" & Rp_CamposExtras!imp_id & " AND d.doc_orden_de_compra = 'NO' AND e.rut ='" & SP_Rut_Activo & "' AND i.id = detalle.id),"
            Rp_CamposExtras.MoveNext
        
        Loop

        
        
        If c < 12 Then
            For i = c To 12
                Sp_CampoExtra = Sp_CampoExtra & "0,"
                Sp_FieldExtra = Sp_FieldExtra & "0,"
            Next
        End If
        
        If Len(Sp_CampoExtra) > 0 Then
            Sp_CampoExtra = "," & Mid(Sp_CampoExtra, 1, Len(Sp_CampoExtra) - 1)
            Sp_FieldExtra = "," & Mid(Sp_FieldExtra, 1, Len(Sp_FieldExtra) - 1)
        End If
    
    Else ' NO HAY CAMP0OS EXTRAS
            For c = 1 To 12
                Sp_CampoExtra = Sp_CampoExtra & "0,"
            Next
            Sp_CampoExtra = "," & Mid(Sp_CampoExtra, 1, Len(Sp_CampoExtra) - 1)
    End If
    
    Sis_PrevisualizarLibro.LvResumen.ColumnHeaders(7).Width = 1200
    
    If Sm_EsDistribuidorGas = "NO" Then
        Sis_PrevisualizarLibro.LvResumen.ColumnHeaders(8).Width = 1200
        Sis_PrevisualizarLibro.LvResumen.ColumnHeaders(9).Width = 0
        Sis_PrevisualizarLibro.LvResumen.ColumnHeaders(23).Width = 0
        
        
    End If
    
    
    
    Sql = "SELECT  doc_nombre documento,COUNT(*) cantidad,IF(signo='-',SUM(exentas)*1,SUM(exentas)) exentas," & _
                "IF(signo='-',SUM(afectas)*-1,SUM(afectas)) neto,IF(signo='-',SUM(iva)*-1,SUM(iva)) iva," & _
                 "IF(signo='-',SUM(retiva)*-1,SUM(retiva)) retiva,0 impesp,IF(signo='-',SUM(iva_sin_credito)*-1,SUM(iva_sin_credito)) iva_sin_credito," & _
                 "IF(signo='-',SUM(iva_margen)*-1,SUM(iva_margen))" & Sp_FieldExtra & ",IF(signo='-',SUM(total)*-1,SUM(total)) total," & _
                 "IF(signo='-',SUM(margen_dist)*-1,SUM(margen_dist)) margen_dist " & _
            "FROM con_libro_compra_temporal t " & _
            "JOIN sis_documentos d ON t.doc_id=d.doc_id " & _
            "GROUP BY t.doc_id"
            
    ' Sql = "SELECT doc_nombre documento,COUNT(*) cantidad," & _
                "IF(d.doc_signo_libro='-',SUM(com_exe_otros_imp)*-1,SUM(com_exe_otros_imp)) exentas," & _
                "IF(d.doc_signo_libro='-',SUM(neto)*-1,SUM(neto)) neto," & _
                "IF(d.doc_signo_libro='-',SUM(iva)*-1,SUM(iva)) iva," & _
                "IF(d.doc_signo_libro='-',SUM(iva_retenido)*-1,SUM(iva_retenido)) ret_iva," & _
                " /* SUM((SELECT IF(d.doc_signo_libro='-',SUM(coi_valor)*-1,SUM(coi_valor)) " & _
                "FROM            com_impuestos i " & _
                "INNER JOIN par_impuestos_empresas e USING(imp_id) " & _
                "WHERE e.rut = '" & SP_Rut_Activo & "' AND i.id = detalle.id)) */ 0 impesp, " & _
                "IF(d.doc_signo_libro='-',SUM(com_iva_sin_credito)*-1,SUM(com_iva_sin_credito)) iva_sin," & _
                "IF(d.doc_signo_libro='-',SUM(com_iva_margen)*-1,SUM(com_iva_margen)) iva_margen" & _
                Sp_CampoExtra & ", IF(d.doc_signo_libro='-',SUM(total)*-1,SUM(total)) total, " & _
                "IF(d.doc_signo_libro='-',SUM(com_margen_distribuidor)*-1,SUM(com_margen_distribuidor)) margen_dist " & " " & _
              "FROM com_doc_compra detalle  " & _
              "INNER JOIN sis_documentos d USING(doc_id) " & _
              "WHERE  com_informa_compra='SI' AND detalle.rut_emp='" & SP_Rut_Activo & "' AND doc_contable='SI' " & s_FiltroPeriodo & " " & Sp_BodSuc & " " & _
              "GROUP BY detalle.doc_id " & _
              "ORDER BY detalle.doc_id"
    
    Consulta RsTmp, Sql
    
    LLenar_Grilla RsTmp, Sis_PrevisualizarLibro, Sis_PrevisualizarLibro.LvResumen, False, True, True, False
    If RsTmp.RecordCount > 0 Then
        Sis_PrevisualizarLibro.LvResumen.ListItems.Add , , ""
        Sis_PrevisualizarLibro.LvResumen.ListItems.Add , , "TOTALES"
        Lp_P = Sis_PrevisualizarLibro.LvResumen.ListItems.Count
        
        For X = 1 To 22
            Sis_PrevisualizarLibro.LvResumen.ListItems(Lp_P).SubItems(X) = 0
            For i = 1 To Sis_PrevisualizarLibro.LvResumen.ListItems.Count - 2
                If Sis_PrevisualizarLibro.LvResumen.ListItems(i).SubItems(X) = "" Then Sis_PrevisualizarLibro.LvResumen.ListItems(i).SubItems(X) = 0
                Sis_PrevisualizarLibro.LvResumen.ListItems(Lp_P).SubItems(X) = NumFormat(CDbl(Sis_PrevisualizarLibro.LvResumen.ListItems(Lp_P).SubItems(X)) + CDbl(Sis_PrevisualizarLibro.LvResumen.ListItems(i).SubItems(X)))
            Next
        Next
        For X = 1 To 22
                Sis_PrevisualizarLibro.LvResumen.ListItems(Lp_P).SubItems(X) = NumFormat(Sis_PrevisualizarLibro.LvResumen.ListItems(Lp_P).SubItems(X))
                Sis_PrevisualizarLibro.LvResumen.ListItems(Lp_P).ListSubItems(X).Bold = True
        Next
        
    End If
    
    
    
    'Set Sis_PrevisualizarLibro.Msh.DataSource = RsCom
    

    
   'Exit Sub

    
  ' ArmaDataReport
   
   
   'ASIENTO CONTABLE
   'SIN NOTA DE CREIDTO
    
   'If SP_Control_Inventario = "SI" Then
        'YA NO TENEMOS TABLAS SEPARADAS PARA CONTROL DE INVENTARIO Y SIN INVENTARIO.
        '17 ENERO 2012
   s_FiltroPeriodo = " AND mes_contable=" & CboMesContable.ItemData(CboMesContable.ListIndex) & " AND ano_contable=" & CboAnoContable.Text & " "
   LLenar_Grilla AsientoContable(s_FiltroPeriodo, 1, CboMesContable.ItemData(CboMesContable.ListIndex), CboAnoContable.Text, Sp_BodSuc), Sis_PrevisualizarLibro, Sis_PrevisualizarLibro.LvAC, False, True, True, False
   TotalAsiento Sis_PrevisualizarLibro.LvAC
   
   'ASIENTO CONTABLE CON NOTA DE CREDITO
   's_FiltroPeriodo = " AND mes_contable=" & CboMesContable.ItemData(CboMesContable.ListIndex) & " AND ano_contable=" & CboAnoContable.Text & " "
   'LLenar_Grilla AsientoContable(s_FiltroPeriodo, 11), Sis_PrevisualizarLibro, Sis_PrevisualizarLibro.LvCDetalle, False, True, True, False
   'TotalAsiento Sis_PrevisualizarLibro.LvCDetalle
   
   s_FiltroPeriodo = " AND mes_contable=" & CboMesContable.ItemData(CboMesContable.ListIndex) & " AND ano_contable=" & CboAnoContable.Text & " "
   LLenar_Grilla AsientoContable(s_FiltroPeriodo, 11, CboMesContable.ItemData(CboMesContable.ListIndex), CboAnoContable.Text, Sp_BodSuc), Me, LvTemp, False, True, True, False
   
    
    
    '       Set RsTmp2 = AsientoContable(s_FiltroPeriodo, 10, Me.CboMesContable.ItemData(CboMesContable.ListIndex), CboAnoContable.Text)
    '    LLenar_Grilla RsTmp2, Me, LvTemp, False, True, True, False

    If LvTemp.ListItems.Count > 0 Then
        For i = LvTemp.ListItems.Count To 1 Step -1
            Sis_PrevisualizarLibro.LvCDetalle.ListItems.Add , , LvTemp.ListItems(i)
            Sis_PrevisualizarLibro.LvCDetalle.ListItems(Sis_PrevisualizarLibro.LvCDetalle.ListItems.Count).SubItems(1) = LvTemp.ListItems(i).SubItems(1)
            Sis_PrevisualizarLibro.LvCDetalle.ListItems(Sis_PrevisualizarLibro.LvCDetalle.ListItems.Count).SubItems(2) = LvTemp.ListItems(i).SubItems(3)
            Sis_PrevisualizarLibro.LvCDetalle.ListItems(Sis_PrevisualizarLibro.LvCDetalle.ListItems.Count).SubItems(3) = LvTemp.ListItems(i).SubItems(2)
        Next
    End If
    
   TotalAsiento Sis_PrevisualizarLibro.LvCDetalle
    
    
   'Otros Impuestos
   'Sql = "SELECT p.imp_nombre,IF(doc_signo_libro='-',SUM(i.coi_valor)*-1,SUM(i.coi_valor)) valor " & _
            "FROM com_impuestos i " & _
            "INNER JOIN par_impuestos_empresas e USING(imp_id) " & _
            "INNER JOIN par_impuestos p USING(imp_id) " & _
            "INNER JOIN com_doc_compra c USING(id) " & _
            "INNER JOIN sis_documentos d USING(doc_id) " & _
            "WHERE d.doc_orden_de_compra='NO' AND e.rut='" & SP_Rut_Activo & "' AND c.rut_emp='" & SP_Rut_Activo & "' AND ime_costo_credito='CREDITO' " & s_FiltroPeriodo & _
            "GROUP BY i.imp_id"
            
    'Otros IMPUESTOS CON DERECHO A CREDITO
   SG_codigo2 = " AND d.doc_orden_de_compra = 'NO' AND e.rut = '" & SP_Rut_Activo & "' " & _
                "AND c.rut_emp = '" & SP_Rut_Activo & "' " & _
                "AND ime_costo_credito = 'CREDITO' " & s_FiltroPeriodo & " "
   
   Sql = "SELECT p.imp_nombre," & _
                    "p.imp_nombre,IFNULL(SUM(IF(doc_signo_libro='+',i.coi_valor,0)),0)+IFNULL(SUM(IF(doc_signo_libro='-',i.coi_valor*-1,0)),0) valor " & _
                "From com_impuestos i " & _
                "INNER JOIN par_impuestos_empresas e USING(imp_id) " & _
                "INNER JOIN par_impuestos p USING(imp_id) " & _
                "INNER JOIN com_doc_compra c USING(id) " & _
                "INNER JOIN sis_documentos d USING(doc_id) " & _
                "WHERE 1 = 1 " & SG_codigo2 & " " & _
                "Group By i.imp_id "
            
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        Sis_PrevisualizarLibro.LvResumen.ListItems.Add , , ""
        Sis_PrevisualizarLibro.LvResumen.ListItems.Add , , ""
        Sis_PrevisualizarLibro.LvResumen.ListItems.Add , , "OTROS IMP.CON DERECHO A CREDITO"
        Lp_P = Sis_PrevisualizarLibro.LvResumen.ListItems.Count
        Sis_PrevisualizarLibro.LvResumen.ListItems(Lp_P).Bold = True
        
        Sis_PrevisualizarLibro.LvResumen.ListItems.Add , , ""
        Lp_P = Sis_PrevisualizarLibro.LvResumen.ListItems.Count
        RsTmp.MoveFirst
        Lp_ImpDC = 0
        Do While Not RsTmp.EOF
            Lp_ImpDC = Lp_ImpDC + RsTmp!Valor
            Sis_PrevisualizarLibro.LvResumen.ListItems.Add , , RsTmp!imp_nombre
            Lp_P = Sis_PrevisualizarLibro.LvResumen.ListItems.Count
            Sis_PrevisualizarLibro.LvResumen.ListItems(Lp_P).SubItems(2) = NumFormat(RsTmp!Valor)
            RsTmp.MoveNext
        Loop
        Sis_PrevisualizarLibro.LvResumen.ListItems.Add , , "TOTAL OTROS IMP.CON DERECHO A CREDITO"
        Lp_P = Sis_PrevisualizarLibro.LvResumen.ListItems.Count
        Sis_PrevisualizarLibro.LvResumen.ListItems(Lp_P).SubItems(2) = NumFormat(Lp_ImpDC)
        Sis_PrevisualizarLibro.LvResumen.ListItems(Lp_P).Bold = True
        Sis_PrevisualizarLibro.LvResumen.ListItems(Lp_P).ListSubItems(2).Bold = True
    End If
    
    'Otros IMPUESTOS DE COSTOS
    SG_codigo2 = " AND d.doc_orden_de_compra = 'NO' AND e.rut = '" & SP_Rut_Activo & "' " & _
                "AND c.rut_emp = '" & SP_Rut_Activo & "' " & _
                "AND ime_costo_credito = 'COSTO' " & s_FiltroPeriodo & " "
   
   Sql = "SELECT p.imp_nombre," & _
                    "p.imp_nombre,IFNULL(SUM(IF(doc_signo_libro='+',i.coi_valor,0)),0)+IFNULL(SUM(IF(doc_signo_libro='-',i.coi_valor*-1,0)),0) valor " & _
                "From com_impuestos i " & _
                "INNER JOIN par_impuestos_empresas e USING(imp_id) " & _
                "INNER JOIN par_impuestos p USING(imp_id) " & _
                "INNER JOIN com_doc_compra c USING(id) " & _
                "INNER JOIN sis_documentos d USING(doc_id) " & _
                "WHERE 1 = 1 " & SG_codigo2 & " " & _
                "Group By i.imp_id "
            
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        Sis_PrevisualizarLibro.LvResumen.ListItems.Add , , ""
        Sis_PrevisualizarLibro.LvResumen.ListItems.Add , , ""
        Sis_PrevisualizarLibro.LvResumen.ListItems.Add , , "OTROS IMP.SIN DERECHO A CREDITO"
        Lp_P = Sis_PrevisualizarLibro.LvResumen.ListItems.Count
        Sis_PrevisualizarLibro.LvResumen.ListItems(Lp_P).Bold = True
        
        Sis_PrevisualizarLibro.LvResumen.ListItems.Add , , ""
        Lp_P = Sis_PrevisualizarLibro.LvResumen.ListItems.Count
        RsTmp.MoveFirst
        Lp_ImpDC = 0
        Do While Not RsTmp.EOF
            Lp_ImpDC = Lp_ImpDC + RsTmp!Valor
            Sis_PrevisualizarLibro.LvResumen.ListItems.Add , , RsTmp!imp_nombre
            Lp_P = Sis_PrevisualizarLibro.LvResumen.ListItems.Count
            Sis_PrevisualizarLibro.LvResumen.ListItems(Lp_P).SubItems(2) = NumFormat(RsTmp!Valor)
            RsTmp.MoveNext
        Loop
        Sis_PrevisualizarLibro.LvResumen.ListItems.Add , , "TOTAL OTROS IMP.SIN DERECHO A CREDITO"
        Lp_P = Sis_PrevisualizarLibro.LvResumen.ListItems.Count
        Sis_PrevisualizarLibro.LvResumen.ListItems(Lp_P).SubItems(2) = NumFormat(Lp_ImpDC)
        Sis_PrevisualizarLibro.LvResumen.ListItems(Lp_P).Bold = True
        Sis_PrevisualizarLibro.LvResumen.ListItems(Lp_P).ListSubItems(2).Bold = True
    End If
    
    
    
    
    
    
    If Me.CboTipoLibro.ListIndex = 0 Then Sis_PrevisualizarLibro.Sm_TipoLibro = "BORRADOR"
    If Me.CboTipoLibro.ListIndex = 1 Then Sis_PrevisualizarLibro.Sm_TipoLibro = "LEGAL"
    Sis_PrevisualizarLibro.Bm_PorFolio = False
    If Me.ChkFolio.Value = 1 Then Sis_PrevisualizarLibro.Bm_PorFolio = True
    Sis_PrevisualizarLibro.Sm_Ano_Contable = CboAnoContable.Text
    Sis_PrevisualizarLibro.Sm_Mes_Contable = CboMesContable.Text
    Sis_PrevisualizarLibro.Sm_Periodo = CboAnoContable.Text & "-" & Right("00" & CboMesContable.ItemData(CboMesContable.ListIndex), 2)
    Sis_PrevisualizarLibro.Sm_CompraVenta = "COMPRA"
    If TxtFActuraElectronica = "SI" Then
        Sis_PrevisualizarLibro.ConsultaEstadoLibro
    End If
    Sis_PrevisualizarLibro.Show 1
    For i = 100 To 1 Step -1
        ProBar.Value = i
    Next
     ProBar.Visible = False
    'Set Form1.msh.DataSource = RsCom
End Sub
Private Sub ArmaDataReport()
    Dim l_Exentas As Long
    Dim l_Afectas As Long
    Dim l_IVA As Long
    Dim l_RetIVA As Long
    Dim l_ImpAdc As Long
    Dim l_Imp_Esp As Long
    Dim l_Total As Long
    
    LvDetalle.ListItems.Clear
    LvCDetalle.ListItems.Clear
    Sql = "SELECT SUM(exentas) exentas,SUM(afectas) afectas,SUM(iva) iva,SUM(retiva) retiva,SUM(impadic) impadic,SUM(impesp) impesp,SUM(total) total,signo " & _
          "FROM con_libro_compra_temporal " & _
          "GROUP BY signo "
    Consulta RsTmp2, Sql
    With RsTmp2
        If .RecordCount > 0 Then
            .MoveFirst
            Do While Not .EOF
                If !Signo = "+" Then
                    l_Exentas = l_Exentas + !exentas
                    l_Afectas = l_Afectas + (0 & !afectas)
                    l_IVA = l_IVA + (0 & !Iva)
                    l_RetIVA = l_RetIVA + (0 & !retiva)
                    l_ImpAdc = l_ImpAdc + (0 & !impadic)
                    l_Imp_Esp = l_Imp_Esp + (0 & !impesp)
                    l_Total = l_Total + (0 & !Total)
                End If
            
                If !Signo = "-" Then
                    l_Exentas = l_Exentas - !exentas
                    l_Afectas = l_Afectas - !afectas
                    l_IVA = l_IVA - !Iva
                    l_RetIVA = l_RetIVA - !retiva
                    l_ImpAdc = l_ImpAdc - !impadic
                    l_Imp_Esp = l_Imp_Esp - !impesp
                    l_Total = l_Total - !Total
                End If
                .MoveNext
            Loop
        End If
    End With
    Printer.Height = 210
 '  Printer.PaperSize = 1
    Set DR_Libro.DataSource = RsCom
    With DR_Libro.Sections("PieInforme")
        .Controls("etexentas").Caption = NumFormat(l_Exentas)
        .Controls("etafectas").Caption = NumFormat(l_Afectas)
        .Controls("etiva").Caption = NumFormat(l_IVA)
        .Controls("etretiva").Caption = NumFormat(l_RetIVA)
        '.Controls("etimpadc").Caption = NumFormat(l_impadic)
        .Controls("etimpesp").Caption = NumFormat(l_Imp_Esp)
        .Controls("ettotal").Caption = NumFormat(l_Total)
        .Controls("etDeclaradas").Caption = s_Declaradas
    
    End With
    
    'Datos de la empresa
    
    If CboTipoLibro.ListIndex = 0 Then 'Borrador
        Sql = "SELECT * " & _
              "FROM sis_empresas " & _
              "WHERE rut='" & SP_Rut_Activo & "'"
        Consulta RsTmp3, Sql
        If RsTmp3.RecordCount > 0 Then
            RsEmpresa = RsTmp3!nombre_empresa
            DireccionEmpresa = RsTmp3!direccion
            Sp_Giro = RsTmp3!giro
            Sp_Ciudad = RsTmp3!ciudad
            Sp_Rut = RsTmp3!Rut
            If Not IsNull(RsTmp3!rut_rl) Then sp_rlrut = "RUT RL     :" & RsTmp3!rut_rl
            If Not IsNull(RsTmp3!nombre_rl) Then sp_rlnombre = "NOMBRE RL  :" & RsTmp3!nombre_rl
            
        End If
    
        DR_Libro.Sections("encPagina").Controls("etEmpresa").Caption = "NOMBRE EMP.:" & RsEmpresa
        DR_Libro.Sections("encPagina").Controls("etrut").Caption = "RUT EMP.   :" & Sp_Rut
        DR_Libro.Sections("encPagina").Controls("etgiro").Caption = "GIRO       :" & Sp_Giro
        DR_Libro.Sections("encPagina").Controls("etdireccion").Caption = "DIRECCION  :" & DireccionEmpresa & " - " & Sp_Ciudad
        DR_Libro.Sections("encPagina").Controls("etrlrut").Caption = sp_rlrut
        DR_Libro.Sections("encPagina").Controls("etrlnombre").Caption = sp_rlnombre
        DR_Libro.Sections("encPagina").Controls("etfecha").Caption = Date
       
    End If
    If CboTipoLibro.ListIndex = 1 Then 'Legal SII
        DR_Libro.Sections("encPagina").Controls("etEmpresa").Visible = False
        DR_Libro.Sections("encPagina").Controls("etrut").Visible = False
        DR_Libro.Sections("encPagina").Controls("etgiro").Visible = False
        DR_Libro.Sections("encPagina").Controls("etdireccion").Visible = False
        DR_Libro.Sections("encPagina").Controls("etrlrut").Visible = False
        DR_Libro.Sections("encPagina").Controls("etrlnombre").Visible = False
        DR_Libro.Sections("encPagina").Controls("etfecha").Visible = False
        DR_Libro.Sections("encPagina").Controls("etnropagina").Visible = False
        DR_Libro.Sections("encPagina").Controls("etpagina").Visible = False
        
    End If
    
    DR_Libro.Sections("encPagina").Controls("etlibro").Caption = s_Libro & " " & Me.CboMesContable.Text & " " & Me.CboAnoContable.Text
    DR_Libro.Caption = DR_Libro.Sections("encPagina").Controls("etlibro").Caption
    DR_Libro.Show 1
    
    'AHORA VAMOS CON EL RESUMEN
    If s_Libro = "LIBRO DE COMPRAS" Then
        Sql = "SELECT doc_nombre documento,COUNT(*) cantidad," & _
                "IF(d.doc_signo_libro='-',SUM(neto)*-1,SUM(neto)) neto," & _
                "IF(d.doc_signo_libro='-',SUM(com_exe_otros_imp)*-1,SUM(com_exe_otros_imp)) exentas," & _
                "IF(d.doc_signo_libro='-',SUM(iva)*-1,SUM(iva)) iva," & _
                "SUM(iva_retenido)ret_iva," & _
                " SUM((SELECT IF(e.ime_costo_credito='COSTO',SUM(coi_valor),0) impadic " & _
                    "FROM com_impuestos i " & _
                    "INNER JOIN par_impuestos_empresas e USING(imp_id) " & _
                    "WHERE e.rut='" & SP_Rut_Activo & "' AND   i.id=detalle.id)) impadic," & _
            "SUM((SELECT IF(e.ime_costo_credito='CREDITO',SUM(coi_valor),0) impesp " & _
                    "FROM com_impuestos i " & _
                    "INNER JOIN par_impuestos_empresas e USING(imp_id) " & _
                    "WHERE e.rut='" & SP_Rut_Activo & "' AND   i.id=detalle.id)) impesp, " & _
                "IF(d.doc_signo_libro='-',SUM(total)*-1,SUM(total)) total " & _
              "FROM com_doc_compra detalle  " & _
              "INNER JOIN sis_documentos d USING(doc_id) " & _
              "WHERE detalle.rut_emp='" & SP_Rut_Activo & "' AND doc_contable='SI' " & s_FiltroPeriodo & " " & _
              "GROUP BY detalle.doc_id " & _
              "ORDER BY detalle.doc_id"
    Else 'libro venta
        Sql = "SELECT doc_nombre documento,COUNT(*) cantidad," & _
                "IF(d.doc_signo_libro='-',SUM(neto)*-1,SUM(neto)) neto," & _
                "IF(d.doc_signo_libro='-',SUM(exento)*-1,SUM(exento)) exentas," & _
                "IF(d.doc_signo_libro='-',SUM(iva)*-1,SUM(iva)) iva," & _
                "IF(d.doc_signo_libro='+',SUM(ven_iva_retenido)*-1,SUM(ven_iva_retenido)) ret_iva ," & _
                "0 impadic,0 impesp," & _
                "IF(d.doc_signo_libro='-',SUM(bruto)*-1,SUM(bruto)) total " & _
              "FROM ven_doc_venta detalle " & _
              "INNER JOIN sis_documentos d USING(doc_id) " & _
              "WHERE detalle.rut_emp='" & SP_Rut_Activo & "' AND doc_contable='SI' " & s_FiltroPeriodo & " " & _
              "GROUP BY detalle.doc_id " & _
              "ORDER BY detalle.doc_id"
    End If
    Consulta RsTmp, Sql
    
    With Dr_LibroResumen
        If CboTipoLibro.ListIndex = 0 Then 'Borrador
            Sql = "SELECT * " & _
                  "FROM sis_empresas " & _
                  "WHERE rut='" & SP_Rut_Activo & "'"
            Consulta RsTmp3, Sql
            If RsTmp3.RecordCount > 0 Then
                RsEmpresa = RsTmp3!nombre_empresa
                DireccionEmpresa = RsTmp3!direccion
                Sp_Giro = RsTmp3!giro
                Sp_Ciudad = RsTmp3!ciudad
                Sp_Rut = RsTmp3!Rut
                If Not IsNull(RsTmp3!rut_rl) Then sp_rlrut = "RUT RL     :" & RsTmp3!rut_rl
                If Not IsNull(RsTmp3!nombre_rl) Then sp_rlnombre = "NOMBRE RL  :" & RsTmp3!nombre_rl
            End If
            .Sections("encPagina").Controls("etEmpresa").Caption = "NOMBRE EMP.:" & RsEmpresa
            .Sections("encPagina").Controls("etrut").Caption = "RUT EMP.   :" & Sp_Rut
            .Sections("encPagina").Controls("etgiro").Caption = "GIRO       :" & Sp_Giro
            .Sections("encPagina").Controls("etdireccion").Caption = "DIRECCION  :" & DireccionEmpresa & " - " & Sp_Ciudad
            .Sections("encPagina").Controls("etrlrut").Caption = sp_rlrut
            .Sections("encPagina").Controls("etrlnombre").Caption = sp_rlnombre
            .Sections("encPagina").Controls("etfecha").Caption = Date
        End If
        If CboTipoLibro.ListIndex = 1 Then 'Legal SII
            .Sections("encPagina").Controls("etEmpresa").Visible = False
            .Sections("encPagina").Controls("etrut").Visible = False
            .Sections("encPagina").Controls("etgiro").Visible = False
            .Sections("encPagina").Controls("etdireccion").Visible = False
            .Sections("encPagina").Controls("etrlrut").Visible = False
            .Sections("encPagina").Controls("etrlnombre").Visible = False
            .Sections("encPagina").Controls("etfecha").Visible = False
            .Sections("encPagina").Controls("etnropagina").Visible = False
            .Sections("encPagina").Controls("etpagina").Visible = False
        End If
        .Sections("encPagina").Controls("etlibro").Caption = s_Libro & " " & Me.CboMesContable.Text & " " & Me.CboAnoContable.Text
        .Caption = DR_Libro.Sections("encPagina").Controls("etlibro").Caption
        Set .DataSource = RsTmp
        .Show 1
    End With
    
    
End Sub




Private Sub Command2_Click()
    Dim Lp_Debe As Long, Lp_LineaNC As Long, Lp_DebitoFiscal As Long, Lp_Haber As Long, Lp_Proveedores As Long, Lp_Clientes As Long
    Dim Lp_Retencion As Long
    'Libro de ventas
    s_FiltroPeriodo = " AND MONTH(fecha)=" & CboMesContable.ItemData(CboMesContable.ListIndex) & " AND YEAR(fecha)=" & CboAnoContable.Text
    s_Libro = "LIBRO DE VENTAS"
    s_Declaradas = "Total Ventas Declaradas"
    Sql = "TRUNCATE con_libro_compra_temporal"
    Consulta RsTmp, Sql
    Sql = "INSERT INTO con_libro_compra_temporal (tmp_id,fecha,td,sii,nro,rs,rut,exentas,afectas,iva,retiva,impadic,impesp,total,grupo_id,grupo_nombre,signo,fpago) " & _
          "SELECT v.id tmp_id,fecha,d.doc_abreviado td,d.doc_cod_sii sii,v.no_documento nro,v.nombre_cliente rz,v.rut_cliente rut," & _
            "exento exentas,neto afectas,iva,ven_iva_retenido retiva,0 impadic,0 impesp,bruto total,doc_grupo_libro_compra grupo_id," & _
            "doc_nombre_grupo grupo_nombre,doc_signo_libro,condicionpago " & _
          "FROM ven_doc_venta v,sis_documentos d " & _
          "WHERE v.rut_emp='" & SP_Rut_Activo & "' AND v.doc_id = d.doc_id AND d.doc_contable='SI' and v.doc_id<>2 " & s_FiltroPeriodo & _
          " ORDER BY no_documento "
    Consulta RsTmp, Sql
    
    'Agregamos la insercion de las boletas de venta
    Sql = "INSERT INTO con_libro_compra_temporal (tmp_id,fecha,td,sii,nro,rs,rut,exentas,afectas,iva,retiva,impadic,impesp,total,grupo_id,grupo_nombre,signo,fpago) " & _
          "SELECT id tmp_id,fecha,d.doc_abreviado td,'' sii, MIN(no_documento) nro,CONCAT('A la',' ',MAX(no_documento)) rs,'' rut,0 exentas,SUM(neto) afectas,SUM(iva) iva," & _
          "0 retiva,0 impadic,0 impesp,SUM(bruto) total,doc_grupo_libro_compra grupo_id," & _
            "doc_nombre_grupo grupo_nombre,doc_signo_libro,condicionpago " & _
           " FROM ven_doc_venta v,sis_documentos d " & _
           " WHERE v.rut_emp='" & SP_Rut_Activo & "' AND  v.doc_id=d.doc_id AND v.doc_id=2 " & s_FiltroPeriodo & _
           " GROUP BY fecha " & _
           " ORDER BY fecha "
    Consulta RsTmp, Sql
    
    
     Sql = "SHAPE {SELECT DISTINCT grupo_id grupo,grupo_nombre doc_nombre_grupo  " & _
                  "FROM con_libro_compra_temporal} " & _
                  "APPEND ({SELECT " & _
                    "fecha,td doc_abreviado,sii,nro , rs,rut,exentas,afectas neto ," & _
                    "iva,retiva ret_iva,impadic imp_adc,impesp imp_esp,total,grupo_id grupo,'' folio " & _
                            "FROM " & _
                                "con_libro_compra_temporal " & _
                            "ORDER BY grupo_id,fecha} AS detalle RELATE grupo TO grupo)"
    ConexionBdShape RsCom, Sql
    ArmaDataReport
    
    
   s_FiltroPeriodo = " AND MONTH(v.fecha)=" & CboMesContable.ItemData(CboMesContable.ListIndex) & " AND YEAR(v.fecha)=" & CboAnoContable.Text
   If SP_Control_Inventario = "SI" Then
        'ASIENTO CONTABLE LIBRO DE VENTAS
        'SIN NOTA DE CREIDTO
        'CON CONTROL DE INVENTARIO
        Sql = "SELECT d.pla_id,pla_nombre,0,SUM(d.subtotal) haber  " & _
              "FROM ven_detalle d " & _
              "INNER JOIN ven_doc_venta v ON (v.no_documento=d.no_documento AND v.doc_id=d.doc_id AND d.rut_emp=v.rut_emp) " & _
              "INNER JOIN con_plan_de_cuentas p ON d.pla_id=p.pla_id " & _
              "INNER JOIN sis_documentos x ON v.doc_id=x.doc_id " & _
              "WHERE v.rut_emp='" & SP_Rut_Activo & "' AND doc_contable='SI'  AND doc_nota_de_credito='NO' " & s_FiltroPeriodo & " " & _
              "GROUP BY d.pla_id " & _
              "UNION SELECT 0,'IVA DEBITO FISCAL',0,SUM(iva) " & _
              "FROM ven_doc_venta v " & _
              "INNER JOIN sis_documentos x ON v.doc_id=x.doc_id " & _
              "WHERE v.rut_emp='" & SP_Rut_Activo & "' AND doc_contable='SI'  AND doc_nota_de_credito='NO' " & s_FiltroPeriodo & " " & _
              "UNION SELECT 0,'IVA RETENIDO',SUM(ven_iva_retenido),0 " & _
              "FROM ven_doc_venta v " & _
              "INNER JOIN sis_documentos x ON v.doc_id=x.doc_id " & _
              "WHERE v.rut_emp='" & SP_Rut_Activo & "' AND doc_contable='SI'  AND doc_nota_de_credito='NO' " & s_FiltroPeriodo & " "

              
        sql2 = "SELECT  SUM(IF(v.condicionpago='CONTADO',v.bruto,0)) caja," & _
                       "SUM(IF(v.condicionpago='CREDITO',v.bruto,0)) clientes " & _
              "FROM ven_detalle d " & _
              "INNER JOIN ven_doc_venta v ON (v.no_documento=d.no_documento AND v.doc_id=d.doc_id AND d.rut_emp=v.rut_emp) " & _
              "INNER JOIN con_plan_de_cuentas p ON d.pla_id=p.pla_id " & _
              "INNER JOIN sis_documentos x ON v.doc_id=x.doc_id " & _
              "WHERE v.rut_emp='" & SP_Rut_Activo & "' AND doc_contable='SI' AND doc_nota_de_credito='NO' " & s_FiltroPeriodo
              
              
   Else
        'ASIENTO CONTABLE
        'SIN NOTA DE CREIDTO
        'S I N    CONTROL DE INVENTARIO  S I N
        Sql = "SELECT d.pla_id,pla_nombre,0,SUM(d.vsd_exento)+SUM(d.vsd_neto) haber " & _
              "FROM ven_sin_detalle d " & _
              "INNER JOIN ven_doc_venta v ON v.id=d.id " & _
              "INNER JOIN con_plan_de_cuentas p ON d.pla_id=p.pla_id " & _
              "INNER JOIN sis_documentos x ON v.doc_id=x.doc_id " & _
              "WHERE v.rut_emp='" & SP_Rut_Activo & "' AND doc_contable='SI'  AND doc_nota_de_credito='NO' " & s_FiltroPeriodo & " " & _
              "GROUP BY d.pla_id " & _
              "UNION SELECT 0,'IVA DEBITO FISCAL',0,SUM(iva) " & _
              "FROM ven_doc_venta v " & _
              "INNER JOIN sis_documentos x ON v.doc_id=x.doc_id " & _
              "WHERE v.rut_emp='" & SP_Rut_Activo & "' AND doc_contable='SI'  AND doc_nota_de_credito='NO' " & s_FiltroPeriodo & " " & _
              "UNION SELECT 0,'IVA RETENIDO',SUM(ven_iva_retenido),0 " & _
              "FROM ven_doc_venta v " & _
              "INNER JOIN sis_documentos x ON v.doc_id=x.doc_id " & _
              "WHERE v.rut_emp='" & SP_Rut_Activo & "' AND doc_contable='SI'  AND doc_nota_de_credito='NO' " & s_FiltroPeriodo & " "

        sql2 = "SELECT  SUM(IF(v.condicionpago='CONTADO',v.bruto,0)) caja," & _
                       "SUM(IF(v.condicionpago='CREDITO',v.bruto,0)) clientes " & _
              "FROM ven_sin_detalle d " & _
              "INNER JOIN ven_doc_venta v USING(id) " & _
              "INNER JOIN con_plan_de_cuentas p ON d.pla_id=p.pla_id " & _
              "INNER JOIN sis_documentos x ON v.doc_id=x.doc_id " & _
              "WHERE v.rut_emp='" & SP_Rut_Activo & "' AND doc_contable='SI' AND doc_nota_de_credito='NO' " & s_FiltroPeriodo
   End If
   Consulta RsTmp2, Sql
   LLenar_Grilla RsTmp2, Me, LvDetalle, False, True, True, False
   If LvDetalle.ListItems.Count > 0 Then
        Lp_Debe = 0
        Lp_CreditoFiscal = 0
        Lp_Retencion = 0
        Lp_Haber = 0
        Lp_Proveedores = 0
      '  For i = 1 To LvDetalle.ListItems.Count
      '      Lp_Debe = Lp_Debe + CDbl(LvDetalle.ListItems(i).SubItems(3))
      '      Lp_DebitoFiscal = Lp_DebitoFiscal + CDbl(LvDetalle.ListItems(i).SubItems(2))
      '      Lp_Retencion = Lp_Retencion + CDbl(LvDetalle.ListItems(i).SubItems(4))
      '      LvDetalle.ListItems(i).SubItems(2) = 0
      '  Next
        'Debito fiscal
      '  LvDetalle.ListItems.Add , , ""
      '  LvDetalle.ListItems(LvDetalle.ListItems.Count).SubItems(1) = "IVA DEBITO FISCAL"
       ' LvDetalle.ListItems(LvDetalle.ListItems.Count).SubItems(3) = NumFormat(Lp_DebitoFiscal)
      '  LvDetalle.ListItems(LvDetalle.ListItems.Count).SubItems(2) = 0
      ' Lp_Debe = Lp_Debe + Lp_DebitoFiscal
        
        'Iva retenido
      '  If Lp_Retencion > 0 Then
      '      LvDetalle.ListItems.Add , , ""
      '      LvDetalle.ListItems(LvDetalle.ListItems.Count).SubItems(1) = "IVA RETENIDO"
      '      LvDetalle.ListItems(LvDetalle.ListItems.Count).SubItems(2) = NumFormat(Lp_Retencion)
      '``      LvDetalle.ListItems(LvDetalle.ListItems.Count).SubItems(3) = 0
      '`'  End If
        
        
        
        'Caja
        Consulta RsTmp2, sql2
        If RsTmp2.RecordCount > 0 Then
            If RsTmp2!caja > 0 Then 'Caja
                
                LvDetalle.ListItems.Add , , ""
                LvDetalle.ListItems(LvDetalle.ListItems.Count).SubItems(1) = "CAJA"
                LvDetalle.ListItems(LvDetalle.ListItems.Count).SubItems(2) = NumFormat(RsTmp2!caja)
                LvDetalle.ListItems(LvDetalle.ListItems.Count).SubItems(3) = 0
            End If
            If RsTmp2!clientes > 0 Then 'Clientes
                LvDetalle.ListItems.Add , , ""
                LvDetalle.ListItems(LvDetalle.ListItems.Count).SubItems(1) = "CLIENTES"
                LvDetalle.ListItems(LvDetalle.ListItems.Count).SubItems(2) = NumFormat(RsTmp2!clientes)
                LvDetalle.ListItems(LvDetalle.ListItems.Count).SubItems(3) = 0
            End If
        End If
        
        LvDetalle.ListItems.Add , , ""
        LvDetalle.ListItems(LvDetalle.ListItems.Count).SubItems(1) = "TOTALES"
        LvDetalle.ListItems(LvDetalle.ListItems.Count).SubItems(2) = NumFormat(TotalizaColumna(LvDetalle, "debe"))   'NumFormat(Lp_Debe)
        LvDetalle.ListItems(LvDetalle.ListItems.Count).SubItems(3) = NumFormat(TotalizaColumna(LvDetalle, "haber")) ' NumFormat(Lp_Haber + Lp_Proveedores)
        LvDetalle.ListItems(LvDetalle.ListItems.Count).ListSubItems(1).Bold = True
        LvDetalle.ListItems(LvDetalle.ListItems.Count).ListSubItems(2).Bold = True
        LvDetalle.ListItems(LvDetalle.ListItems.Count).ListSubItems(3).Bold = True
   End If
    
    
    
   'ASIENTO CONTABLE CON NOTA DE CREDITO
   If SP_Control_Inventario = "SI" Then
 
        Sql = "SELECT d.pla_id,pla_nombre,SUM(d.subtotal) debe,SUM(v.iva) iva,SUM(ven_iva_retenido) retenido  " & _
              "FROM ven_detalle d " & _
              "INNER JOIN ven_doc_venta v USING(no_documento,doc_id,rut_emp) " & _
              "INNER JOIN con_plan_de_cuentas p ON d.pla_id=p.pla_id " & _
              "INNER JOIN sis_documentos x ON v.doc_id=x.doc_id " & _
              "WHERE v.rut_emp='" & SP_Rut_Activo & "' AND doc_contable='SI'  AND doc_nota_de_credito='SI' " & s_FiltroPeriodo & " " & _
              "GROUP BY d.pla_id "
              
        sql2 = "SELECT  SUM(IF(v.condicionpago='CONTADO',v.bruto,0)) caja," & _
                       "SUM(IF(v.condicionpago='CREDITO',v.bruto,0)) clientes " & _
              "FROM ven_detalle d " & _
              "INNER JOIN ven_doc_venta v USING(no_documento,doc_id,rut_emp) " & _
              "INNER JOIN con_plan_de_cuentas p ON d.pla_id=p.pla_id " & _
              "INNER JOIN sis_documentos x ON v.doc_id=x.doc_id " & _
              "WHERE v.rut_emp='" & SP_Rut_Activo & "' AND doc_contable='SI' AND doc_nota_de_credito='SI' " & s_FiltroPeriodo
              
   Else
        Sql = "SELECT d.pla_id,pla_nombre,SUM(d.vsd_exento)+SUM(d.vsd_neto) debe,SUM(iva) iva,SUM(ven_iva_retenido) retenido  " & _
              "FROM ven_sin_detalle d " & _
              "INNER JOIN ven_doc_venta v ON v.id=d.id " & _
              "INNER JOIN con_plan_de_cuentas p ON d.pla_id=p.pla_id " & _
              "INNER JOIN sis_documentos x ON v.doc_id=x.doc_id " & _
              "WHERE v.rut_emp='" & SP_Rut_Activo & "' AND doc_contable='SI'  AND doc_nota_de_credito='SI' " & s_FiltroPeriodo & " " & _
              "GROUP BY d.pla_id "
        sql2 = "SELECT  SUM(IF(v.condicionpago='CONTADO',v.bruto,0)) caja," & _
                       "SUM(IF(v.condicionpago='CREDITO',v.bruto,0)) clientes " & _
              "FROM ven_sin_detalle d " & _
              "INNER JOIN ven_doc_venta v USING(id) " & _
              "INNER JOIN con_plan_de_cuentas p ON d.pla_id=p.pla_id " & _
              "INNER JOIN sis_documentos x ON v.doc_id=x.doc_id " & _
              "WHERE v.rut_emp='" & SP_Rut_Activo & "' AND doc_contable='SI' AND doc_nota_de_credito='SI' " & s_FiltroPeriodo
              
              
   End If
   Consulta RsTmp2, Sql
   LLenar_Grilla RsTmp2, Me, LvCDetalle, False, True, True, False
   If LvCDetalle.ListItems.Count > 0 Then
        Lp_Debe = 0
        Lp_DebitoFiscal = 0
        Lp_Retencion = 0
        For i = 1 To LvCDetalle.ListItems.Count
            Lp_Debe = Lp_Debe + CDbl(LvCDetalle.ListItems(i).SubItems(2))
            Lp_DebitoFiscal = Lp_DebitoFiscal + CDbl(LvCDetalle.ListItems(i).SubItems(3))
            Lp_Retencion = Lp_Retencion + CDbl(LvCDetalle.ListItems(i).SubItems(4))
            LvCDetalle.ListItems(i).SubItems(3) = 0
        Next
        Lp_Debe = Lp_Debe + Lp_DebitoFiscal
        'Debito Fiscal
        LvCDetalle.ListItems.Add , , ""
        LvCDetalle.ListItems(LvCDetalle.ListItems.Count).SubItems(1) = "IVA DEBITO FISCAL"
        LvCDetalle.ListItems(LvCDetalle.ListItems.Count).SubItems(2) = NumFormat(Lp_DebitoFiscal)
        LvCDetalle.ListItems(LvCDetalle.ListItems.Count).SubItems(3) = 0
        
        'Debito Fiscal
        If Lp_Retencion > 0 Then
            LvCDetalle.ListItems.Add , , ""
            LvCDetalle.ListItems(LvCDetalle.ListItems.Count).SubItems(1) = "IVA RETENIDO"
            LvCDetalle.ListItems(LvCDetalle.ListItems.Count).SubItems(3) = NumFormat(Lp_Retencion)
            LvCDetalle.ListItems(LvCDetalle.ListItems.Count).SubItems(2) = 0
        End If
         'Caja
        Consulta RsTmp2, sql2
        If RsTmp2.RecordCount > 0 Then
            If RsTmp2!caja > 0 Then
                LvCDetalle.ListItems.Add , , ""
                LvCDetalle.ListItems(LvCDetalle.ListItems.Count).SubItems(1) = "CAJA"
                LvCDetalle.ListItems(LvCDetalle.ListItems.Count).SubItems(3) = NumFormat(RsTmp2!caja)
                LvCDetalle.ListItems(LvCDetalle.ListItems.Count).SubItems(2) = 0
            End If
            If RsTmp2!clientes > 0 Then
                LvCDetalle.ListItems.Add , , ""
                LvCDetalle.ListItems(LvCDetalle.ListItems.Count).SubItems(1) = "CLIENTES"
                LvCDetalle.ListItems(LvCDetalle.ListItems.Count).SubItems(3) = NumFormat(RsTmp2!clientes)
                LvCDetalle.ListItems(LvCDetalle.ListItems.Count).SubItems(2) = 0
            End If
        End If
        
        
        
        
        LvCDetalle.ListItems.Add , , ""
        LvCDetalle.ListItems(LvCDetalle.ListItems.Count).SubItems(1) = "TOTALES"
        LvCDetalle.ListItems(LvCDetalle.ListItems.Count).SubItems(2) = NumFormat(TotalizaColumna(LvCDetalle, "debe"))
        LvCDetalle.ListItems(LvCDetalle.ListItems.Count).SubItems(3) = NumFormat(TotalizaColumna(LvCDetalle, "haber"))
        LvCDetalle.ListItems(LvCDetalle.ListItems.Count).ListSubItems(1).Bold = True
        LvCDetalle.ListItems(LvCDetalle.ListItems.Count).ListSubItems(2).Bold = True
        LvCDetalle.ListItems(LvCDetalle.ListItems.Count).ListSubItems(3).Bold = True
   End If
     
End Sub



Private Sub Command3_Click()
 Dim Lp_Debe As Long, Lp_LineaNC As Long, Lp_CreditoFiscal As Long, Lp_Haber As Long, Lp_Proveedores As Long
    Dim Lp_P As Long, Ip_Grupo As Integer, Sp_Grupo As String, Lp_ImpDC As Long
    Dim Lp_Total As Long, Lp_NroDoctos As Long, Lp_Neto As Long, Lp_Iva As Long, Lp_Ret As Long, Lp_Otros As Long, Lp_Exento As Long
    Dim Sp_IdBoletas As String
    
    Dim Lp_CantidadBoletas As Long
    
    
    If CboTipoLibro.ListIndex = 2 Then
        LibroTimbraje
        Exit Sub
    End If
    
    Sp_BodSuc = ""
    If Me.CboSucursales.Text <> "TODAS" Then
        Sql = "SELECT bod_id " & _
                "FROM par_bodegas " & _
                "WHERE sue_id=" & CboSucursales.ItemData(CboSucursales.ListIndex) & " AND rut_emp='" & SP_Rut_Activo & "'"
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
            Sp_BodSuc = " AND bod_id=" & RsTmp!bod_id & " "
        End If
    End If
    
    Sp_IdBoletas = ""
    Sql = "SELECT doc_id " & _
            "FROM sis_documentos " & _
            "WHERE doc_documento='VENTA' AND doc_contable='SI' AND doc_boleta_venta='SI'"
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        RsTmp.MoveFirst
        Do While Not RsTmp.EOF
            Sp_IdBoletas = Sp_IdBoletas & RsTmp!doc_id & ","
            RsTmp.MoveNext
        Loop
        If Len(Sp_IdBoletas) > 0 Then Sp_IdBoletas = Mid(Sp_IdBoletas, 1, Len(Sp_IdBoletas) - 1)
    End If
    ProBar.Visible = True
    '***************************************************************************************************
    '     LIBRO DE VENTA LISTADO - RESUMEN Y ASIENTO CONTABLE
    '***************************************************************************************************
    s_FiltroPeriodo = " AND MONTH(v.fecha)=" & CboMesContable.ItemData(CboMesContable.ListIndex) & " AND YEAR(v.fecha)=" & CboAnoContable.Text & " "
    s_Libro = "LIBRO DE VENTA"
    s_Declaradas = "Total Ventas Declaradas"
    Sql = "TRUNCATE con_libro_compra_temporal"
    cn.Execute Sql
    'Consulta RsTmp, Sql
    If Len(Sp_IdBoletas) > 0 Then Sp_IdBoletas = "," & Sp_IdBoletas Else Sp_IdBoletas = "0"
    Sql = "INSERT INTO con_libro_compra_temporal (tmp_id,fecha,td,sii,nro,rs,rut,exentas,afectas,iva,retiva,impadic,impesp,total,grupo_id,grupo_nombre,signo,fpago,doc_id,bod_id) " & _
          "SELECT v.id tmp_id,fecha,d.doc_abreviado td,d.doc_cod_sii sii,v.no_documento nro,v.nombre_cliente rz,v.rut_cliente rut," & _
            "exento exentas,neto afectas,iva,ven_iva_retenido retiva,0 impadic,0 impesp,bruto total,doc_grupo_libro_compra grupo_id," & _
            "doc_nombre_grupo grupo_nombre,doc_signo_libro,condicionpago,v.doc_id,bod_id " & _
          "FROM ven_doc_venta v,sis_documentos d " & _
          "WHERE ven_informa_venta='SI' AND v.rut_emp='" & SP_Rut_Activo & "' AND v.doc_id = d.doc_id AND d.doc_contable='SI' and v.doc_id NOT IN(40" & Sp_IdBoletas & ") " & s_FiltroPeriodo & Sp_BodSuc & " " & _
          "ORDER BY no_documento"
          
      cn.Execute Sql
    
    'Agregamos la insercion de las boletas de venta
    If SP_Control_Inventario = "SI" Then
    
        
    
    
        Sql = "INSERT INTO con_libro_compra_temporal (boleta,tmp_id,fecha,td,sii,nro,rs,rut,exentas,afectas,iva,retiva,impadic,impesp,total,grupo_id,grupo_nombre,signo,fpago,bod_id,cantidad_boletas,doc_id) " & _
              "SELECT 'SI',id tmp_id,fecha,d.doc_abreviado td,'' sii, MIN(no_documento) nro,CONCAT('A la',' ',MAX(no_documento)) rs,'' rut, exento,SUM(neto) afectas,SUM(iva) iva," & _
              "0 retiva,0 impadic,0 impesp,SUM(bruto) total,doc_grupo_libro_compra grupo_id," & _
                "doc_nombre_grupo grupo_nombre,doc_signo_libro,condicionpago,bod_id ,/*(MAX(no_documento) - MIN(no_documento))+1*/ Count(id) CantBoletas,v.doc_id " & _
               " FROM ven_doc_venta v,sis_documentos d " & _
               " WHERE ven_simple='NO' AND v.rut_emp='" & SP_Rut_Activo & "' AND  v.doc_id=d.doc_id AND v.doc_id IN(0" & Sp_IdBoletas & ") " & s_FiltroPeriodo & Sp_BodSuc & " " & _
               " GROUP BY fecha,v.doc_id " & _
               " ORDER BY fecha "
                cn.Execute Sql
                
                
                
         '2 2 2015
         'VER DOCUMENTO PAGADOS CON VOUCHER TRANSBANK
         
         Sql = "INSERT INTO con_libro_compra_temporal (boleta,tmp_id,fecha,td,sii,nro,rs,rut,exentas,afectas,iva,retiva,impadic,impesp,total,grupo_id,grupo_nombre,signo,fpago,bod_id,cantidad_boletas,doc_id) " & _
              "SELECT 'SI', id tmp_id,fecha,d.doc_abreviado td,'' sii, MIN(no_documento) nro,CONCAT('A la',' ',MAX(no_documento)) rs,'' rut,0 exentas,SUM(neto) afectas,SUM(iva) iva," & _
              "0 retiva,0 impadic,0 impesp,SUM(bruto) total,doc_grupo_libro_compra grupo_id," & _
                "doc_nombre_grupo grupo_nombre,doc_signo_libro,condicionpago,bod_id,/*(MAX(no_documento) - MIN(no_documento))+1*/ Count(id)  CantBoletas,v.doc_id " & _
               " FROM ven_doc_venta v,sis_documentos d " & _
               " WHERE v.rut_emp='" & SP_Rut_Activo & "' AND  v.doc_id=d.doc_id AND v.doc_id=40 " & s_FiltroPeriodo & Sp_BodSuc & " " & _
               " GROUP BY fecha " & _
               " ORDER BY fecha "
                cn.Execute Sql
               
               
               'venta simple
        Sql = "INSERT INTO con_libro_compra_temporal (boleta,tmp_id,fecha,td,sii,nro,rs,rut,exentas,afectas,iva,retiva,impadic,impesp,total,grupo_id,grupo_nombre,signo,fpago,bod_id,cantidad_boletas,doc_id) " & _
            "SELECT 'SI',id tmp_id,fecha,d.doc_abreviado td,'' sii,no_documento,CONCAT('A la',' ',CAST(ven_boleta_hasta AS CHAR))rs," & _
            "'' rut, exento,neto afectas,iva iva,0 retiva,0 impadic,0 impesp,bruto total, doc_grupo_libro_compra grupo_id," & _
            "doc_nombre_grupo grupo_nombre,doc_signo_libro,condicionpago,bod_id ,(ven_boleta_hasta-no_documento)+1 CantBoletas,v.doc_id " & _
            "FROM  ven_doc_venta v, sis_documentos d " & _
            "WHERE v.rut_emp = '" & SP_Rut_Activo & "' " & _
            "AND ven_simple='SI' AND v.doc_id = d.doc_id AND v.doc_id IN(0" & Sp_IdBoletas & ") " & s_FiltroPeriodo & " " & Sp_BodSuc & " " & _
            "ORDER BY Fecha"
            cn.Execute Sql
            
        If SP_Rut_Activo = "96.803.210-0" Then
            'agregar registros sistema de porteria
            'VEGA MODELO
            ConexionBDPorteria
            Sql = "SELECT  r.ven_id id,  '' folio, t.tur_apertura fecha,'BOF' td,'' sii, COUNT(*) cant_boletas,'' rs,'' rut,SUM(r.ven_valor) exentas " & _
                    "FROM ven_registro r " & _
                    "JOIN ven_turno t ON r.tur_id=t.tur_id " & _
                    "WHERE MONTH(t.tur_apertura) = " & CboMesContable.ItemData(CboMesContable.ListIndex) & " AND YEAR(tur_apertura) = " & CboAnoContable.Text & " " & _
                    "GROUP BY tur_apertura"
            ConsultaV RsTmp, Sql
            If RsTmp.RecordCount > 0 Then
                Do While Not RsTmp.EOF
                    Sql = "INSERT INTO con_libro_compra_temporal (boleta,tmp_id,fecha,td,sii,nro,rs,rut,exentas,afectas,iva,retiva,impadic,impesp,total,grupo_id,grupo_nombre,signo,fpago,bod_id,cantidad_boletas,doc_id) VALUES "
                    
                    cn.Execute Sql & "('SI'," & RsTmp!Id & ",'" & Fql(RsTmp!Fecha) & "','BOF',38," & RsTmp!cant_boletas & ",'',''," & RsTmp!exentas & ",0,0,0,0,0," & RsTmp!exentas & ",8,'BOLETA EX. FISCAL','+','CONTADO',1," & RsTmp!cant_boletas & ",60)"
                        
                    RsTmp.MoveNext
                Loop
            End If
        End If
            
    Else 'Boletas sin inventario de tal nro a tal nro
        Sql = "INSERT INTO con_libro_compra_temporal (tmp_id,fecha,td,sii,nro,rs,rut,exentas,afectas,iva,retiva,impadic,impesp,total,grupo_id,grupo_nombre,signo,fpago,bod_id) " & _
            "SELECT id tmp_id,fecha,d.doc_abreviado td,'' sii,no_documento,CONCAT('A la',' ',CAST(ven_boleta_hasta AS CHAR))rs," & _
            "'' rut,0 exentas,neto afectas,iva iva,0 retiva,0 impadic,0 impesp,bruto total, doc_grupo_libro_compra grupo_id," & _
            "doc_nombre_grupo grupo_nombre,doc_signo_libro,condicionpago,bod_id " & _
            "FROM  ven_doc_venta v, sis_documentos d " & _
            "WHERE v.rut_emp = '" & SP_Rut_Activo & "' " & _
            "AND v.doc_id = d.doc_id AND v.doc_id IN(2,27) " & s_FiltroPeriodo & " " & Sp_BodSuc & " " & _
            "ORDER BY Fecha"
            cn.Execute Sql
    End If
    
    Sql = "SELECT " & _
                    "fecha,td doc_abreviado,sii,nro , rs,rut,IF(signo='+',exentas,exentas*-1) exentas," & _
                    "IF(signo='+',afectas,afectas*-1) neto ," & _
                    "IF(signo='+',iva,iva*-1) iva,IF(signo='+',retiva,retiva*-1) ret_iva," & _
                    "IF(signo='+',impadic +impesp,(impadic+impesp)*-1) otrosimp," & _
                    "IFNULL(IF(signo='+',total,total*-1),0) total,grupo_id grupo,folio,grupo_nombre,doc_id,cantidad_boletas,tasa_iva,boleta " & _
            "FROM con_libro_compra_temporal " & _
            "ORDER BY grupo_id,fecha,nro "
    Consulta RsCom, Sql
    If RsCom.RecordCount = 0 Then
        MsgBox "No se encontraron registros en el periodo contable seleccionado...", vbInformation
        'Exit Sub
    End If
    
    Sis_PrevisualizarLibro.LvLibro.Visible = False
    With Sis_PrevisualizarLibro.LvLibroVenta
        Sis_PrevisualizarLibro.LvLibroVenta.ListItems.Clear
        
        .ColumnHeaders(13).Width = 0
        .ColumnHeaders(14).Width = 0
        .ColumnHeaders(16).Width = 0
        If RsCom.RecordCount > 0 Then
             RsCom.MoveFirst
             Ip_Grupo = RsCom!grupo
             Sp_Grupo = RsCom!grupo_nombre
             Lp_Total = 0 ' RsCom!Total
             Lp_NroDoctos = 0
             Lp_CantidadBoletas = 0
             Do While Not RsCom.EOF
                 If Ip_Grupo <> RsCom!grupo Then
                     .ListItems.Add , , ""
                     Lp_P = .ListItems.Count
                     
                     .ListItems(Lp_P).SubItems(1) = "No Doctos:"
                     
                     If Sp_Grupo = "BOLETAS" Then
                        '  Sql = "SELECT SUM(cantidad_boletas) cantidad " & _
                                    "FROM  con_libro_compra_temporal " & _
                                    "WHERE doc_id=2"
                        
                        Sql = "SELECT COUNT(*)cantidad " & _
                                "FROM  ven_doc_venta v " & _
                                "INNER JOIN sis_documentos d USING(doc_id) " & _
                                "WHERE   v.doc_id IN(2) " & _
                                "AND v.rut_emp = '" & SP_Rut_Activo & "' AND doc_contable = 'SI' " & s_FiltroPeriodo & " " & Sp_BodSuc & " " & _
                                "GROUP BY v.doc_id"
                        Consulta RsTmp2, Sql
                        If RsTmp2.RecordCount > 0 Then .ListItems(Lp_P).SubItems(2) = RsTmp2!cantidad
                    
                    ElseIf Sp_Grupo = "BOLETA EXENTA" Then
                        
                         Sql = "SELECT SUM(cantidad_boletas) cantidad " & _
                                    "FROM  con_libro_compra_temporal " & _
                                    "WHERE doc_id IN(0" & Sp_IdBoletas & ")"
                                   
                        Consulta RsTmp2, Sql
                        If RsTmp2.RecordCount > 0 Then .ListItems(Lp_P).SubItems(2) = RsTmp2!cantidad
                    
                    ElseIf Sp_Grupo = "BOLETA EX. FISCAL" Then
                        
                          Sql = "SELECT  COUNT(*) cantidad " & _
                                "FROM ven_registro r " & _
                                "JOIN ven_turno t ON r.tur_id=t.tur_id " & _
                                "WHERE MONTH(t.tur_apertura) = " & CboMesContable.ItemData(CboMesContable.ListIndex) & " AND YEAR(tur_apertura) = " & CboAnoContable.Text & " " & _
                                "/* GROUP BY tur_apertura */ "
                                ConsultaV RsTmp2, Sql
                                If RsTmp2.RecordCount > 0 Then .ListItems(Lp_P).SubItems(2) = RsTmp2!cantidad
                    
                    Else
                        If Sp_Grupo = "BOLETAS TRANSBANK" Then
                              '  Sql = "SELECT SUM(cantidad_boletas) cantidad " & _
                                    "FROM  con_libro_compra_temporal " & _
                                    "WHERE doc_id=40"
                        '
                                Sql = "SELECT COUNT(*) cantidad " & _
                                        "FROM  ven_doc_venta v " & _
                                        "INNER JOIN sis_documentos d USING(doc_id) " & _
                                        "WHERE   v.doc_id IN(40) " & _
                                        "AND v.rut_emp = '" & SP_Rut_Activo & "' AND doc_contable = 'SI' " & s_FiltroPeriodo & " " & Sp_BodSuc & " " & _
                                        "GROUP BY v.doc_id"
                                Consulta RsTmp2, Sql
                                If RsTmp2.RecordCount > 0 Then .ListItems(Lp_P).SubItems(2) = RsTmp2!cantidad
                        Else
                            .ListItems(Lp_P).SubItems(2) = Lp_NroDoctos
                        End If
                    
                    
                    
                    '    If Sp_Grupo = "BOLETAS TRANSBANK" Then
                    '            Sql = "SELECT SUM(ven_boleta_hasta + 1)cantidad " & _
                    '                    "FROM  ven_doc_venta v " & _
                    '                    "INNER JOIN sis_documentos d USING(doc_id) " & _
                    '                    "WHERE   v.doc_id IN(40) " & _
                    '                    "AND v.rut_emp = '" & SP_Rut_Activo & "' AND doc_contable = 'SI' " & s_FiltroPeriodo & " " & Sp_BodSuc & " " & _
                    '                    "GROUP BY v.doc_id"
                    '            Consulta RsTmp2, Sql
                    '            If RsTmp2.RecordCount > 0 Then .ListItems(Lp_P).SubItems(2) = RsTmp2!cantidad
                    '    Else
                    '        .ListItems(Lp_P).SubItems(2) = Lp_NroDoctos
                    '    End If
                    End If
                     
                     .ListItems(Lp_P).SubItems(5) = "TOTAL " & Sp_Grupo
                     .ListItems(Lp_P).SubItems(7) = NumFormat(Lp_Exento)
                     .ListItems(Lp_P).SubItems(8) = NumFormat(Lp_Neto)
                     .ListItems(Lp_P).SubItems(9) = NumFormat(Lp_Iva)
                     .ListItems(Lp_P).SubItems(10) = NumFormat(Lp_Ret)
                     .ListItems(Lp_P).SubItems(11) = NumFormat(Lp_Otros)
                     .ListItems(Lp_P).SubItems(14) = NumFormat(Lp_Total)
                     
                     Ip_Grupo = RsCom!grupo
                     Sp_Grupo = RsCom!grupo_nombre
                     Lp_Total = 0
                     Lp_Neto = 0
                     Lp_Iva = 0
                     Lp_Ret = 0
                     Lp_Otros = 0
                     Lp_Exento = 0
                     Lp_NroDoctos = 0
                     Lp_CantidadBoletas = 0
                     For i = 1 To .ColumnHeaders.Count - 6
                         .ListItems(Lp_P).ListSubItems(i).Bold = True
                     Next
                     .ListItems.Add , , ""
                 End If
                 Lp_NroDoctos = Lp_NroDoctos + 1
                 Lp_Total = IIf(IsNull(RsCom!Total), 0, Lp_Total + RsCom!Total)
                 Lp_Neto = IIf(IsNull(RsCom!Neto), 0, Lp_Neto + RsCom!Neto)
                 Lp_Iva = IIf(IsNull(RsCom!Iva), 0, Lp_Iva + RsCom!Iva)
                 Lp_Ret = IIf(IsNull(RsCom!ret_iva), 0, Lp_Ret + RsCom!ret_iva)
                 Lp_Otros = IIf(IsNull(RsCom!otrosimp), 0, Lp_Otros + RsCom!otrosimp)
                 Lp_Exento = IIf(IsNull(RsCom!exentas), 0, Lp_Exento + RsCom!exentas)
                 Lp_CantidadBoletas = IIf(IsNull(RsCom!cantidad_boletas), 0, cantidad_boletas)
                 .ListItems.Add , , RsCom!Folio
                 Lp_P = .ListItems.Count
                 .ListItems(Lp_P).SubItems(1) = Format(RsCom!Fecha, "DD-MM-YY")
                 .ListItems(Lp_P).SubItems(2) = RsCom!doc_abreviado
                 .ListItems(Lp_P).SubItems(3) = RsCom!sii
                 .ListItems(Lp_P).SubItems(4) = RsCom!Nro
                 .ListItems(Lp_P).SubItems(5) = "" & RsCom!rs
                 .ListItems(Lp_P).SubItems(6) = RsCom!Rut
                 .ListItems(Lp_P).SubItems(7) = NumFormat(RsCom!exentas)
                 .ListItems(Lp_P).SubItems(8) = NumFormat(RsCom!Neto)
                 .ListItems(Lp_P).SubItems(9) = NumFormat(RsCom!Iva)
                 .ListItems(Lp_P).SubItems(10) = NumFormat(RsCom!ret_iva)
                 .ListItems(Lp_P).SubItems(11) = NumFormat(RsCom!otrosimp)
                 .ListItems(Lp_P).SubItems(12) = 0
                 .ListItems(Lp_P).SubItems(13) = 0
                 .ListItems(Lp_P).SubItems(14) = NumFormat(RsCom!Total)
                 .ListItems(Lp_P).SubItems(16) = RsCom!doc_id
                 .ListItems(Lp_P).SubItems(17) = RsCom!tasa_iva
                 .ListItems(Lp_P).SubItems(18) = "" & RsCom!cantidad_boletas
                  .ListItems(Lp_P).SubItems(19) = "" & RsCom!boleta
                 RsCom.MoveNext
             Loop
             .ListItems.Add , , ""
             Lp_P = .ListItems.Count
             .ListItems(Lp_P).SubItems(1) = "No Doctos:"
             
             
             If Sp_Grupo = "BOLETAS" Then
             
                        '        Sql = "SELECT SUM(cantidad_boletas) cantidad " & _
                                    "FROM  con_libro_compra_temporal " & _
                                    "WHERE doc_id=2"
             
           '     Sql = "SELECT " & _
                        "(SELECT IFNULL(ven_boleta_hasta,0) " & _
                        "FROM ven_doc_venta v " & _
                        "INNER JOIN sis_documentos d USING(doc_id) " & _
                        "WHERE   v.doc_id IN(2) AND v.rut_emp = '" & SP_Rut_Activo & "' AND doc_contable = 'SI' AND MONTH(v.fecha)=" & CboMesContable.ItemData(CboMesContable.ListIndex) & " AND YEAR(v.fecha)=" & CboAnoContable.Text & " " & _
                        "ORDER BY id DESC " & _
                        "LIMIT 1)-" & _
                        "(SELECT IFNULL(ven_boleta_hasta,0) " & _
                        "FROM ven_doc_venta v " & _
                        "INNER JOIN sis_documentos d USING(doc_id) " & _
                        "WHERE   v.doc_id IN(2) AND v.rut_emp = '" & SP_Rut_Activo & "' AND doc_contable = 'SI' AND MONTH(v.fecha)=" & CboMesContable.ItemData(CboMesContable.ListIndex) & " AND YEAR(v.fecha)=" & CboAnoContable.Text & " " & _
                        "ORDER BY id " & _
                        "LIMIT 1) cantidad"
             
             
                Sql = "SELECT COUNT(*) cantidad " & _
                        "FROM  ven_doc_venta v " & _
                        "INNER JOIN sis_documentos d USING(doc_id) " & _
                        "WHERE   v.doc_id IN(2) " & _
                        "AND v.rut_emp = '" & SP_Rut_Activo & "' AND doc_contable = 'SI' " & s_FiltroPeriodo & " " & Sp_BodSuc & " " & _
                        "GROUP BY v.doc_id"
                Consulta RsTmp2, Sql
                If RsTmp2.RecordCount > 0 Then .ListItems(Lp_P).SubItems(2) = RsTmp2!cantidad
            ElseIf Sp_Grupo = "BOLETA EXENTA" Then
                '20-03-2015
                'sumamos las boletas exentas ingresadas por venta simple boletas desde hasta
                 Sql = "SELECT SUM(cantidad_boletas) cantidad " & _
                        "FROM  con_libro_compra_temporal " & _
                        "WHERE doc_id IN(0" & Sp_IdBoletas & ")"
                Consulta RsTmp2, Sql
                If RsTmp2.RecordCount > 0 Then .ListItems(Lp_P).SubItems(2) = RsTmp2!cantidad
                
            ElseIf Sp_Grupo = "BOLETAS TRANSBANK" Then
                                 'Sql = "SELECT SUM(cantidad_boletas) cantidad " & _
                                    "FROM  con_libro_compra_temporal " & _
                                    "WHERE doc_id=40"
                                 Sql = "SELECT COUNT(*)cantidad " & _
                                        "FROM  ven_doc_venta v " & _
                                        "INNER JOIN sis_documentos d USING(doc_id) " & _
                                        "WHERE   v.doc_id IN(40) " & _
                                        "AND v.rut_emp = '" & SP_Rut_Activo & "' AND doc_contable = 'SI' " & s_FiltroPeriodo & " " & Sp_BodSuc & " " & _
                                        "GROUP BY v.doc_id"
                                Consulta RsTmp2, Sql
                                If RsTmp2.RecordCount > 0 Then .ListItems(Lp_P).SubItems(2) = RsTmp2!cantidad
             
             
                'Sql = "SELECT SUM((ven_boleta_hasta + 1))cantidad " & _
                        "FROM  ven_doc_venta v " & _
                        "INNER JOIN sis_documentos d USING(doc_id) " & _
                        "WHERE   v.doc_id IN(2) " & _
                        "AND v.rut_emp = '" & SP_Rut_Activo & "' AND doc_contable = 'SI' " & s_FiltroPeriodo & " " & Sp_BodSuc & " " & _
                        "GROUP BY v.doc_id"
                'Consulta RsTmp2, Sql
                'If RsTmp2.RecordCount > 0 Then .ListItems(Lp_P).SubItems(2) = RsTmp2!cantidad
            
            ElseIf Sp_Grupo = "BOLETA EX. FISCAL" Then
                
                Sql = "SELECT  COUNT(*) cantidad " & _
                    "FROM ven_registro r " & _
                    "JOIN ven_turno t ON r.tur_id=t.tur_id " & _
                    "WHERE MONTH(t.tur_apertura) = " & CboMesContable.ItemData(CboMesContable.ListIndex) & " AND YEAR(tur_apertura) = " & CboAnoContable.Text & " " & _
                    " /*GROUP BY tur_apertura */"
                    ConsultaV RsTmp2, Sql
                    If RsTmp2.RecordCount > 0 Then .ListItems(Lp_P).SubItems(2) = RsTmp2!cantidad
                
            Else
                .ListItems(Lp_P).SubItems(2) = Lp_NroDoctos
            End If
             
            ' .ListItems(Lp_P).SubItems(2) = Lp_NroDoctos
             .ListItems(Lp_P).SubItems(5) = "TOTAL " & Sp_Grupo
             .ListItems(Lp_P).SubItems(7) = NumFormat(Lp_Exento)
             .ListItems(Lp_P).SubItems(8) = NumFormat(Lp_Neto)
             .ListItems(Lp_P).SubItems(9) = NumFormat(Lp_Iva)
             .ListItems(Lp_P).SubItems(10) = NumFormat(Lp_Ret)
             .ListItems(Lp_P).SubItems(11) = NumFormat(Lp_Otros)
             .ListItems(Lp_P).SubItems(12) = 0
             .ListItems(Lp_P).SubItems(13) = 0
             .ListItems(Lp_P).SubItems(14) = NumFormat(Lp_Total)
             
             For i = 1 To .ColumnHeaders.Count - 6
                 .ListItems(Lp_P).ListSubItems(i).Bold = True
             Next
             .ListItems.Add , , ""
             .ListItems.Add , , ""
             Lp_P = .ListItems.Count
             .ListItems(Lp_P).SubItems(5) = "TOTALES"
             
             For i = 7 To 14
                 .ListItems(Lp_P).SubItems(i) = 0
                 For X = 1 To .ListItems.Count - 2
                     If .ListItems(X).SubItems(1) <> "No Doctos:" And Len(.ListItems(X).SubItems(1)) > 0 Then
                         .ListItems(Lp_P).SubItems(i) = .ListItems(Lp_P).SubItems(i) + CDbl(.ListItems(X).SubItems(i))
                     End If
                 Next
             Next
             For i = 7 To 14
                 .ListItems(Lp_P).SubItems(i) = NumFormat(.ListItems(Lp_P).SubItems(i))
             Next
            For i = 1 To 14
                 .ListItems(Lp_P).ListSubItems(i).Bold = True
            Next
        
                    'Hasta aqui el libro de VENTAS
            'Ahora ver las cuentas asociadas
            'INFORMACION DE CUENTAS CONTABLES
            'MARIO
            ProBar.Max = .ListItems.Count
            
            If Me.ChkContabilizacion.Value = 1 Then
            
                    If .ListItems.Count > 0 Then
                        
                        For i = 1 To .ListItems.Count
                            
                            If Len(.ListItems(i).SubItems(4)) > 0 Then
                                Sql = "SELECT pla_nombre,SUM(ved_precio_venta_neto) total_cuenta " & _
                                        "FROM ven_detalle d " & _
                                        "JOIN ven_doc_venta v ON d.doc_id=v.doc_id AND d.no_documento=v.no_documento " & _
                                        "JOIN con_plan_de_cuentas p ON d.pla_id = p.pla_id " & _
                                        "WHERE   d.rut_emp = '" & SP_Rut_Activo & "' AND MONTH(v.fecha)=" & CboMesContable.ItemData(CboMesContable.ListIndex) & " AND YEAR(v.fecha) =" & CboAnoContable.Text & " " & _
                                        "AND d.no_documento=" & .ListItems(i).SubItems(4) & " AND d.doc_id=" & .ListItems(i).SubItems(17) & " " & _
                                        " GROUP BY d.pla_id"
        '                        Sql = "SELECT pla_nombre,SUM(cmd_total_neto+cmd_exento) total_cuenta " & _
                                        "FROM com_doc_compra_detalle d " & _
                                        "JOIN con_plan_de_cuentas p ON d.pla_id=p.pla_id " & _
                                        "JOIN com_doc_compra c ON d.id=c.id " & _
                                        "WHERE rut_emp='" & SP_Rut_Activo & "' AND com_folio_texto='" & .ListItems(i) & "' AND mes_contable=" & CboMesContable.ItemData(CboMesContable.ListIndex) & " AND ano_contable=" & CboAnoContable.Text & " " & _
                                        "GROUP BY d.pla_id"
                            
                            
                               ' Sql = "SELECT pla_nombre,total_cuenta " & _
                                        "FROM vi_contabilidad_detalle_cuentas_libro " & _
                                        "WHERE rut_emp='" & SP_Rut_Activo & "' AND com_folio_texto='" & .ListItems(i) & "' AND mes_contable=" & CboMesContable.ItemData(CboMesContable.ListIndex) & " AND ano_contable=" & CboAnoContable.Text
                                'Sis_BarraProgreso.Show 1
                                Consulta RsTmp, Sql
                                If RsTmp.RecordCount > 0 Then
                                    detallecuenta = ""
                                    Do While Not RsTmp.EOF
                                        detallecuenta = detallecuenta & RsTmp!pla_nombre & " $" & RsTmp!total_cuenta & " - "
                                        RsTmp.MoveNext
                                    Loop
                                    .ListItems(i).SubItems(16) = detallecuenta
                                Else
                                    Sql = "SELECT pla_nombre,SUM(ved_precio_venta_neto) total_cuenta " & _
                                        "FROM ven_detalle d " & _
                                        "JOIN con_plan_de_cuentas p ON d.pla_id = p.pla_id " & _
                                        "JOIN ven_doc_venta v ON d.no_documento=v.no_documento AND d.doc_id=v.doc_id " & _
                                        "WHERE   d.rut_emp = '" & SP_Rut_Activo & "' AND v.nro_factura=" & .ListItems(i).SubItems(4) & " AND v.doc_id_factura=" & .ListItems(i).SubItems(17) & " " & _
                                        " GROUP BY d.pla_id"
                                        Consulta RsTmp, Sql
                                        If RsTmp.RecordCount > 0 Then
                                            detallecuenta = ""
                                            Do While Not RsTmp.EOF
                                                detallecuenta = detallecuenta & RsTmp!pla_nombre & " $" & RsTmp!total_cuenta & " - "
                                                RsTmp.MoveNext
                                            Loop
                                           .ListItems(i).SubItems(16) = detallecuenta
                                        End If
                                
                                End If
                           End If
                           ProBar.Value = i
                        Next
                    End If
            End If 'incluye contabilizacion
        End If
    End With
    
    
    
    
    
    
    
    
    
    Sql = "SELECT * " & _
              "FROM sis_empresas " & _
              "WHERE rut='" & SP_Rut_Activo & "'"
    Consulta RsTmp3, Sql
    If RsTmp3.RecordCount > 0 Then
        RsEmpresa = RsTmp3!nombre_empresa
        DireccionEmpresa = RsTmp3!direccion
        Sp_Giro = RsTmp3!giro
        Sp_Ciudad = RsTmp3!ciudad
        Sp_Rut = RsTmp3!Rut
        If Not IsNull(RsTmp3!rut_rl) Then sp_rlrut = "RUT RL     :" & RsTmp3!rut_rl
        If Not IsNull(RsTmp3!nombre_rl) Then sp_rlnombre = "NOMBRE RL  :" & RsTmp3!nombre_rl
        
    End If
    With Sis_PrevisualizarLibro
        .LvDetalle.ListItems.Add , , "NOMBRE EMP.:" & RsEmpresa
        .LvDetalle.ListItems.Add , , "RUT EMP.   :" & Sp_Rut
        .LvDetalle.ListItems.Add , , "GIRO       :" & Sp_Giro
        .LvDetalle.ListItems.Add , , "DIRECCION  :" & DireccionEmpresa & " - " & Sp_Ciudad
        .LvDetalle.ListItems.Add , , sp_rlrut
        .LvDetalle.ListItems.Add , , sp_rlnombre
        .LvDetalle.ListItems.Add , , "SUCURSAL   :" & CboSucursales.Text
        .Caption = "LIBRO DE VENTAS"
    End With

    Sis_PrevisualizarLibro.SkLibro.Caption = "LIBRO DE VENTAS " & CboMesContable.Text & "  " & CboAnoContable.Text
    
    'RESUMEN LIBRO VENTAS
    s_FiltroPeriodo = " AND MONTH(detalle.fecha)=" & CboMesContable.ItemData(CboMesContable.ListIndex) & " AND YEAR(detalle.fecha)=" & CboAnoContable.Text & " "
   If SP_Control_Inventario = "SI" Then
    
        '21 Marzo 2015
        'Se van a separar las ventas por boletas, y por otros documentos, usando UNION
        
        
        Sql = "SELECT doc_nombre documento,IF(ven_simple='SI',SUM((ven_boleta_hasta+1)-no_documento), COUNT(id)) cantidad," & _
                "IF(d.doc_signo_libro='-',SUM(exento)*-1,SUM(exento)) exentas," & _
                "IF(d.doc_signo_libro='-',SUM(neto)*-1,SUM(neto)) neto," & _
                "IF(d.doc_signo_libro='-',SUM(iva)*-1,SUM(iva)) iva," & _
                "IF(d.doc_signo_libro='+',SUM(ven_iva_retenido)*-1,SUM(ven_iva_retenido)) ret_iva ," & _
                "0 otros_imp,0,0," & _
                "IF(d.doc_signo_libro='-',SUM(bruto)*-1,SUM(bruto)) total,0 " & _
              "FROM ven_doc_venta detalle " & _
              "INNER JOIN sis_documentos d USING(doc_id) " & _
              "WHERE doc_id IN(0" & Sp_IdBoletas & ") AND ven_informa_venta='SI' " & Sp_BodSuc & " AND detalle.rut_emp='" & SP_Rut_Activo & "' AND doc_contable='SI' " & s_FiltroPeriodo & " " & _
              "GROUP BY detalle.doc_id " & _
              "/* ORDER BY detalle.doc_id */ UNION " & _
                "SELECT doc_nombre documento,COUNT(*) cantidad," & _
                "IF(d.doc_signo_libro='-',SUM(exento)*-1,SUM(exento)) exentas," & _
                "IF(d.doc_signo_libro='-',SUM(neto)*-1,SUM(neto)) neto," & _
                "IF(d.doc_signo_libro='-',SUM(iva)*-1,SUM(iva)) iva," & _
                "IF(d.doc_signo_libro='+',SUM(ven_iva_retenido)*-1,SUM(ven_iva_retenido)) ret_iva ," & _
                "0 otros_imp,0,0," & _
                "IF(d.doc_signo_libro='-',SUM(bruto)*-1,SUM(bruto)) total,0 " & _
              "FROM ven_doc_venta detalle " & _
              "INNER JOIN sis_documentos d USING(doc_id) " & _
              "WHERE  doc_id NOT IN(0" & Sp_IdBoletas & ") AND  ven_informa_venta='SI' " & Sp_BodSuc & " AND detalle.rut_emp='" & SP_Rut_Activo & "' AND doc_contable='SI' " & s_FiltroPeriodo & " " & _
              "GROUP BY detalle.doc_id " & _
              "/* ORDER BY detalle.doc_id */"
              
              
             If SP_Rut_Activo = "96.803.210-0" Then
                'vega modelo resumen de boletas
                Sql = Sql & "UNION SELECT doc_nombre,  SUM(nro) cantidad,SUM(exentas) exentas ,0 neto,0 iva,0 ret_iva,0 otros,0,01,SUM(total) total,sii " & _
                            "FROM con_libro_compra_temporal t " & _
                            "JOIN sis_documentos d ON t.doc_id=d.doc_id " & _
                            "Where sii = 38"
                
            End If
    Else
        Sql = "SELECT doc_nombre documento,COUNT(*) cantidad," & _
                 "IF(d.doc_signo_libro='-',SUM(exento)*-1,SUM(exento)) exentas," & _
                 "IF(d.doc_signo_libro='-',SUM(neto)*-1,SUM(neto)) neto," & _
                 "IF(d.doc_signo_libro='-',SUM(iva)*-1,SUM(iva)) iva," & _
                 "IF(d.doc_signo_libro='+',SUM(ven_iva_retenido)*-1,SUM(ven_iva_retenido)) ret_iva ," & _
                 "0 otros_imp," & _
                 "IF(d.doc_signo_libro='-',SUM(bruto)*-1,SUM(bruto)) total " & _
               "FROM ven_doc_venta detalle " & _
               "INNER JOIN sis_documentos d USING(doc_id) " & _
               "WHERE  ven_informa_venta='SI'  " & Sp_BodSuc & " AND  detalle.doc_id NOT IN(2) AND detalle.rut_emp='" & SP_Rut_Activo & "' AND doc_contable='SI' " & s_FiltroPeriodo & " " & _
               "GROUP BY detalle.doc_id " & _
               "UNION " & _
        "SELECT doc_nombre documento,SUM((ven_boleta_hasta+1)-no_documento) cantidad," & _
                 "IF(d.doc_signo_libro='-',SUM(exento)*-1,SUM(exento)) exentas," & _
                 "IF(d.doc_signo_libro='-',SUM(neto)*-1,SUM(neto)) neto," & _
                 "IF(d.doc_signo_libro='-',SUM(iva)*-1,SUM(iva)) iva," & _
                 "IF(d.doc_signo_libro='+',SUM(ven_iva_retenido)*-1,SUM(ven_iva_retenido)) ret_iva ," & _
                 "0 otros_imp," & _
                 "IF(d.doc_signo_libro='-',SUM(bruto)*-1,SUM(bruto)) total " & _
               "FROM ven_doc_venta detalle " & _
               "INNER JOIN sis_documentos d USING(doc_id) " & _
               "WHERE  ven_informa_venta='SI'  " & Sp_BodSuc & "  AND  detalle.doc_id IN(2)  AND detalle.rut_emp='" & SP_Rut_Activo & "' AND doc_contable='SI' " & s_FiltroPeriodo & " " & _
               "GROUP BY detalle.doc_id "
    End If
    Sis_PrevisualizarLibro.LvResumen.Visible = False
    Sis_PrevisualizarLibro.LvResumenVentas.ColumnHeaders(8).Width = 0
    Sis_PrevisualizarLibro.LvResumenVentas.ColumnHeaders(9).Width = 0
    'Sis_PrevisualizarLibro.LvResumenVentas.ColumnHeaders(11).Width = 0
   
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Sis_PrevisualizarLibro, Sis_PrevisualizarLibro.LvResumenVentas, False, True, True, False
    If RsTmp.RecordCount > 0 Then
        Sis_PrevisualizarLibro.LvResumenVentas.ListItems.Add , , ""
        Sis_PrevisualizarLibro.LvResumenVentas.ListItems.Add , , "TOTALES"
        Lp_P = Sis_PrevisualizarLibro.LvResumenVentas.ListItems.Count
        For X = 1 To 9
            Sis_PrevisualizarLibro.LvResumenVentas.ListItems(Lp_P).SubItems(X) = 0
            For i = 1 To Sis_PrevisualizarLibro.LvResumenVentas.ListItems.Count - 2
                If Sis_PrevisualizarLibro.LvResumenVentas.ListItems(i).SubItems(X) = "" Then Sis_PrevisualizarLibro.LvResumenVentas.ListItems(i).SubItems(X) = "0"
                Sis_PrevisualizarLibro.LvResumenVentas.ListItems(Lp_P).SubItems(X) = CDbl(Sis_PrevisualizarLibro.LvResumenVentas.ListItems(Lp_P).SubItems(X)) + CDbl(Sis_PrevisualizarLibro.LvResumenVentas.ListItems(i).SubItems(X))
            Next
        Next
        For X = 1 To 9
                Sis_PrevisualizarLibro.LvResumenVentas.ListItems(Lp_P).SubItems(X) = NumFormat(Sis_PrevisualizarLibro.LvResumenVentas.ListItems(Lp_P).SubItems(X))
                Sis_PrevisualizarLibro.LvResumenVentas.ListItems(Lp_P).ListSubItems(X).Bold = True
        Next
        
    End If
    
    
   
   'ASIENTO CONTABLE
   'SIN NOTA DE CREIDTO
 If SP_Control_Inventario = "SI" Then
        'ASIENTO CONTABLE LIBRO DE VENTAS
        'SIN NOTA DE CREIDTO
        'CON CONTROL DE INVENTARIO
        If Me.ChkAsientos.Value = 1 Then s_FiltroPeriodo = " AND MONTH(v.fecha)=" & CboMesContable.ItemData(CboMesContable.ListIndex) & " AND YEAR(v.fecha)=" & CboAnoContable.Text & " "
        
        
        'Sql = "SELECT d.pla_id,pla_nombre,0,SUM(d.ved_precio_venta_neto) haber  " & _
              "FROM ven_detalle d " & _
              "INNER JOIN ven_doc_venta v ON (v.no_documento=d.no_documento AND v.doc_id=d.doc_id AND d.rut_emp=v.rut_emp) " & _
              "INNER JOIN con_plan_de_cuentas p ON d.pla_id=p.pla_id " & _
              "INNER JOIN sis_documentos x ON v.doc_id=x.doc_id " & _
              "WHERE v.rut_emp='" & SP_Rut_Activo & "' AND doc_contable='SI'  AND doc_nota_de_credito='NO' " & s_FiltroPeriodo & " " & _
              "GROUP BY d.pla_id " & _
              "UNION SELECT 0,'IVA DEBITO FISCAL',0,SUM(iva) " & _
              "FROM ven_doc_venta v " & _
              "INNER JOIN sis_documentos x ON v.doc_id=x.doc_id " & _
              "WHERE v.rut_emp='" & SP_Rut_Activo & "' AND doc_contable='SI'  AND doc_nota_de_credito='NO' " & s_FiltroPeriodo & " " & _
              "UNION SELECT 0,'IVA RETENIDO',SUM(ven_iva_retenido),0 " & _
              "FROM ven_doc_venta v " & _
              "INNER JOIN sis_documentos x ON v.doc_id=x.doc_id " & _
              "WHERE v.rut_emp='" & SP_Rut_Activo & "' AND doc_contable='SI'  AND doc_nota_de_credito='NO' " & s_FiltroPeriodo & " "

              
        'sql2 = "SELECT  SUM(IF(v.condicionpago='CONTADO',v.bruto,0)) caja," & _
                       "SUM(IF(v.condicionpago='CREDITO',v.bruto,0)) clientes " & _
              "FROM ven_detalle d " & _
              "INNER JOIN ven_doc_venta v ON (v.no_documento=d.no_documento AND v.doc_id=d.doc_id AND d.rut_emp=v.rut_emp) " & _
              "INNER JOIN con_plan_de_cuentas p ON d.pla_id=p.pla_id " & _
              "INNER JOIN sis_documentos x ON v.doc_id=x.doc_id " & _
              "WHERE v.rut_emp='" & SP_Rut_Activo & "' AND doc_contable='SI' AND doc_nota_de_credito='NO' " & s_FiltroPeriodo
         'sql2 = "SELECT  SUM(IF(v.condicionpago='CONTADO',v.bruto,0)) caja," & _
                       "SUM(IF(v.condicionpago='CREDITO',v.bruto,0)) clientes " & _
              "FROM ven_doc_venta v " & _
              "INNER JOIN sis_documentos x ON v.doc_id=x.doc_id " & _
              "WHERE v.rut_emp='" & SP_Rut_Activo & "' AND doc_contable='SI' AND doc_nota_de_credito='NO' " & s_FiltroPeriodo
        'sql2 = "SELECT  SUM(IF(v.condicionpago='CONTADO',v.bruto,0)) caja," & _
                       "SUM(IF(v.condicionpago='CREDITO',v.bruto,0)) clientes " & _
              "FROM ven_doc_venta v" & _
              "INNER JOIN sis_documentos x ON v.doc_id=x.doc_id " & _
              "WHERE v.rut_emp='" & SP_Rut_Activo & "' AND doc_contable='SI' AND doc_nota_de_credito='NO' " & s_FiltroPeriodo
              
   Else
        'ASIENTO CONTABLE
        'SIN NOTA DE CREDITO
        'S I N    CONTROL DE INVENTARIO  S I N
        s_FiltroPeriodo = " AND MONTH(v.fecha)=" & CboMesContable.ItemData(CboMesContable.ListIndex) & " AND YEAR(v.fecha)=" & CboAnoContable.Text & " "
        Sql = "SELECT d.pla_id,pla_nombre,0,SUM(d.vsd_exento)+SUM(d.vsd_neto) haber " & _
              "FROM ven_sin_detalle d " & _
              "INNER JOIN ven_doc_venta v ON v.id=d.id " & _
              "INNER JOIN con_plan_de_cuentas p ON d.pla_id=p.pla_id " & _
              "INNER JOIN sis_documentos x ON v.doc_id=x.doc_id " & _
              "WHERE  ven_informa_venta='SI' AND v.rut_emp='" & SP_Rut_Activo & "' AND doc_contable='SI'  AND doc_nota_de_credito='NO' " & s_FiltroPeriodo & " " & _
              "GROUP BY d.pla_id " & _
              "UNION SELECT 0,'IVA DEBITO FISCAL',0,SUM(iva) " & _
              "FROM ven_doc_venta v " & _
              "INNER JOIN sis_documentos x ON v.doc_id=x.doc_id " & _
              "WHERE  ven_informa_venta='SI' AND  v.rut_emp='" & SP_Rut_Activo & "' AND doc_contable='SI'  AND doc_nota_de_credito='NO' " & s_FiltroPeriodo & " " & _
              "UNION SELECT 0,'IVA RETENIDO',SUM(ven_iva_retenido),0 " & _
              "FROM ven_doc_venta v " & _
              "INNER JOIN sis_documentos x ON v.doc_id=x.doc_id " & _
              "WHERE  ven_informa_venta='SI' AND  v.rut_emp='" & SP_Rut_Activo & "' AND doc_contable='SI'  AND doc_nota_de_credito='NO' " & s_FiltroPeriodo & " "

      '  sql2 = "SELECT  SUM(IF(v.condicionpago='CONTADO',v.bruto,0)) caja," & _
                       "SUM(IF(v.condicionpago='CREDITO',v.bruto,0)) clientes " & _
              "FROM ven_sin_detalle d " & _
              "INNER JOIN ven_doc_venta v USING(id) " & _
              "INNER JOIN con_plan_de_cuentas p ON d.pla_id=p.pla_id " & _
              "INNER JOIN sis_documentos x ON v.doc_id=x.doc_id " & _
              "WHERE v.rut_emp='" & SP_Rut_Activo & "' AND doc_contable='SI' AND doc_nota_de_credito='NO' " & s_FiltroPeriodo
           sql2 = "SELECT  SUM(IF(v.condicionpago='CONTADO',v.bruto,0)) caja," & _
                       "SUM(IF(v.condicionpago='CREDITO',v.bruto,0)) clientes " & _
              "FROM   ven_doc_venta v  " & _
              "INNER JOIN sis_documentos x ON v.doc_id=x.doc_id " & _
              "WHERE  ven_informa_venta='SI' AND  v.rut_emp='" & SP_Rut_Activo & "' AND doc_contable='SI' AND doc_nota_de_credito='NO' " & s_FiltroPeriodo
              
   End If
   
   
   
    If Me.ChkAsientos.Value = 1 Then
        Set RsTmp2 = AsientoContable(s_FiltroPeriodo, 2, Me.CboMesContable.ItemData(CboMesContable.ListIndex), CboAnoContable.Text, Sp_BodSuc)
        LLenar_Grilla RsTmp2, Sis_PrevisualizarLibro, Sis_PrevisualizarLibro.LvAC, False, True, True, False
        TotalAsiento Sis_PrevisualizarLibro.LvAC
    End If
    
    
    
   
   
   
   GoTo salto
   
   Consulta RsTmp2, Sql
   LLenar_Grilla RsTmp2, Sis_PrevisualizarLibro, Sis_PrevisualizarLibro.LvAC, False, True, True, False
   
   If Sis_PrevisualizarLibro.LvAC.ListItems.Count > 0 Then
        Lp_Debe = 0
        Lp_CreditoFiscal = 0
        Lp_Retencion = 0
        Lp_Haber = 0
        Lp_Proveedores = 0
      '  For i = 1 To LvDetalle.ListItems.Count
      '      Lp_Debe = Lp_Debe + CDbl(LvDetalle.ListItems(i).SubItems(3))
      '      Lp_DebitoFiscal = Lp_DebitoFiscal + CDbl(LvDetalle.ListItems(i).SubItems(2))
      '      Lp_Retencion = Lp_Retencion + CDbl(LvDetalle.ListItems(i).SubItems(4))
      '      LvDetalle.ListItems(i).SubItems(2) = 0
      '  Next
        'Debito fiscal
      '  LvDetalle.ListItems.Add , , ""
      '  LvDetalle.ListItems(LvDetalle.ListItems.Count).SubItems(1) = "IVA DEBITO FISCAL"
       ' LvDetalle.ListItems(LvDetalle.ListItems.Count).SubItems(3) = NumFormat(Lp_DebitoFiscal)
      '  LvDetalle.ListItems(LvDetalle.ListItems.Count).SubItems(2) = 0
      ' Lp_Debe = Lp_Debe + Lp_DebitoFiscal
        
        'Iva retenido
      '  If Lp_Retencion > 0 Then
      '      LvDetalle.ListItems.Add , , ""
      '      LvDetalle.ListItems(LvDetalle.ListItems.Count).SubItems(1) = "IVA RETENIDO"
      '      LvDetalle.ListItems(LvDetalle.ListItems.Count).SubItems(2) = NumFormat(Lp_Retencion)
      '``      LvDetalle.ListItems(LvDetalle.ListItems.Count).SubItems(3) = 0
      '`'  End If
        
        
        
        'Caja
        Consulta RsTmp2, sql2
        If RsTmp2.RecordCount > 0 Then
            If RsTmp2!caja > 0 Then 'Caja
                
                Sis_PrevisualizarLibro.LvAC.ListItems.Add , , ""
                Sis_PrevisualizarLibro.LvAC.ListItems(Sis_PrevisualizarLibro.LvAC.ListItems.Count).SubItems(1) = "CAJA"
                Sis_PrevisualizarLibro.LvAC.ListItems(Sis_PrevisualizarLibro.LvAC.ListItems.Count).SubItems(2) = NumFormat(RsTmp2!caja)
                Sis_PrevisualizarLibro.LvAC.ListItems(Sis_PrevisualizarLibro.LvAC.ListItems.Count).SubItems(3) = 0
            End If
            If RsTmp2!clientes > 0 Then 'Clientes
                Sis_PrevisualizarLibro.LvAC.ListItems.Add , , ""
                Sis_PrevisualizarLibro.LvAC.ListItems(Sis_PrevisualizarLibro.LvAC.ListItems.Count).SubItems(1) = "CLIENTES"
                Sis_PrevisualizarLibro.LvAC.ListItems(Sis_PrevisualizarLibro.LvAC.ListItems.Count).SubItems(2) = NumFormat(RsTmp2!clientes)
                Sis_PrevisualizarLibro.LvAC.ListItems(Sis_PrevisualizarLibro.LvAC.ListItems.Count).SubItems(3) = 0
            End If
        End If
        
        Sis_PrevisualizarLibro.LvAC.ListItems.Add , , ""
        Sis_PrevisualizarLibro.LvAC.ListItems(Sis_PrevisualizarLibro.LvAC.ListItems.Count).SubItems(1) = "TOTALES"
        Sis_PrevisualizarLibro.LvAC.ListItems(Sis_PrevisualizarLibro.LvAC.ListItems.Count).SubItems(2) = NumFormat(TotalizaColumna(Sis_PrevisualizarLibro.LvAC, "debe"))   'NumFormat(Lp_Debe)
        Sis_PrevisualizarLibro.LvAC.ListItems(Sis_PrevisualizarLibro.LvAC.ListItems.Count).SubItems(3) = NumFormat(TotalizaColumna(Sis_PrevisualizarLibro.LvAC, "haber")) ' NumFormat(Lp_Haber + Lp_Proveedores)
  
        Sis_PrevisualizarLibro.LvAC.ListItems(Sis_PrevisualizarLibro.LvAC.ListItems.Count).ListSubItems(1).Bold = True
        Sis_PrevisualizarLibro.LvAC.ListItems(Sis_PrevisualizarLibro.LvAC.ListItems.Count).ListSubItems(2).Bold = True
        Sis_PrevisualizarLibro.LvAC.ListItems(Sis_PrevisualizarLibro.LvAC.ListItems.Count).ListSubItems(3).Bold = True
   End If
    
salto:
If Me.ChkAsientos.Value = 1 Then
   'Detectamos si ocurre alguna diferencia en el AContable por los decimales que no se suman
    Sis_PrevisualizarLibro.SkDiferencia = CDbl(Sis_PrevisualizarLibro.LvAC.ListItems(Sis_PrevisualizarLibro.LvAC.ListItems.Count).SubItems(2)) - CDbl(Sis_PrevisualizarLibro.LvAC.ListItems(Sis_PrevisualizarLibro.LvAC.ListItems.Count).SubItems(3))
    If Val(Sis_PrevisualizarLibro.SkDiferencia) > 0 Then
        Sis_PrevisualizarLibro.LvAC.ListItems(1).SubItems(3) = NumFormat(CDbl(Sis_PrevisualizarLibro.LvAC.ListItems(1).SubItems(3)) + Val(Sis_PrevisualizarLibro.SkDiferencia))
        Sis_PrevisualizarLibro.LvAC.ListItems(Sis_PrevisualizarLibro.LvAC.ListItems.Count).SubItems(3) = _
        NumFormat(CDbl(Sis_PrevisualizarLibro.LvAC.ListItems(Sis_PrevisualizarLibro.LvAC.ListItems.Count).SubItems(3)) + Val(Sis_PrevisualizarLibro.SkDiferencia))
    End If
    'Fin Deteccion -
        
   'ASIENTO CONTABLE CON NOTA DE CREDITO
    
   
otrosalto:
   
       Set RsTmp2 = AsientoContable(s_FiltroPeriodo, 10, Me.CboMesContable.ItemData(CboMesContable.ListIndex), CboAnoContable.Text, Sp_BodSuc)
        LLenar_Grilla RsTmp2, Me, LvTemp, False, True, True, False

    If LvTemp.ListItems.Count > 0 Then
        For i = LvTemp.ListItems.Count To 1 Step -1
            Sis_PrevisualizarLibro.LvCDetalle.ListItems.Add , , LvTemp.ListItems(i)
            Sis_PrevisualizarLibro.LvCDetalle.ListItems(Sis_PrevisualizarLibro.LvCDetalle.ListItems.Count).SubItems(1) = LvTemp.ListItems(i).SubItems(1)
            Sis_PrevisualizarLibro.LvCDetalle.ListItems(Sis_PrevisualizarLibro.LvCDetalle.ListItems.Count).SubItems(2) = LvTemp.ListItems(i).SubItems(3)
            Sis_PrevisualizarLibro.LvCDetalle.ListItems(Sis_PrevisualizarLibro.LvCDetalle.ListItems.Count).SubItems(3) = LvTemp.ListItems(i).SubItems(2)
        Next
    End If
    

    
    
    
    TotalAsiento Sis_PrevisualizarLibro.LvCDetalle
   
End If
   
   If CboTipoLibro.ListIndex = 0 Then
        Sis_PrevisualizarLibro.Sm_TipoLibro = "BORRADOR"
    Else
        Sis_PrevisualizarLibro.Sm_TipoLibro = "�egal�"
    End If
    Sis_PrevisualizarLibro.Sm_Ano_Contable = CboAnoContable.Text
    Sis_PrevisualizarLibro.Sm_Mes_Contable = CboMesContable.Text
    Sis_PrevisualizarLibro.Sm_CompraVenta = "VENTAS"
    Sis_PrevisualizarLibro.Sm_Periodo = CboAnoContable.Text & "-" & Right("00" & CboMesContable.ItemData(CboMesContable.ListIndex), 2)
    If TxtFActuraElectronica = "SI" Then
        Sis_PrevisualizarLibro.ConsultaEstadoLibro
    End If
    Sis_PrevisualizarLibro.Show 1
    ProBar.Value = 1
    ProBar.Visible = False
End Sub

Private Sub Form_Load()
    Centrar Me
    Aplicar_skin Me, App.Path
    SkEmpresaActiva = "EMPRESA ACTIVA: " & SP_Empresa_Activa
    Sp_EsDistribuidorGas = "NO"
    For i = 1 To 12
        CboMesContable.AddItem UCase(MonthName(i, False))
        CboMesContable.ItemData(CboMesContable.ListCount - 1) = i
    Next
    Busca_Id_Combo CboMesContable, Val(IG_Mes_Contable)
    LLenaYears Me.CboAnoContable, 2010

    
    CboTipoLibro.AddItem "LIBRO BORRADOR"
    CboTipoLibro.AddItem "LIBRO LEGAL SII"
    CboTipoLibro.AddItem "HOJAS PARA TIMBRAR"
    CboTipoLibro.ListIndex = 0
    
    LLenarCombo CboSucursales, "CONCAT(sue_direccion,'-',sue_ciudad)", "sue_id", "sis_empresas_sucursales", "sue_activa='SI' AND emp_id=" & IG_id_Empresa
    CboSucursales.AddItem "CASA MATRIZ"
    CboSucursales.ItemData(CboSucursales.ListCount - 1) = 1
    CboSucursales.AddItem "TODAS"
    CboSucursales.ListIndex = CboSucursales.ListCount - 1
    
    Sql = "SELECT emp_factura_electronica dte,emp_distribuidora_gas gas " & _
                "FROM sis_empresas " & _
                "WHERE rut='" & SP_Rut_Activo & "'"
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        Me.TxtFActuraElectronica = RsTmp!Dte
        Sm_EsDistribuidorGas = RsTmp!gas
    End If
    
End Sub

Private Sub txtAl_KeyPress(KeyAscii As Integer)
    KeyAscii = AceptaSoloNumeros(KeyAscii)
End Sub


Private Sub txtDel_KeyPress(KeyAscii As Integer)
    KeyAscii = AceptaSoloNumeros(KeyAscii)
    If KeyAscii = 39 Then KeyAscii = 0
End Sub


Function ConexionBDPorteria()
            On Error GoTo FalloConexion
                Sp_Servidor = "vegamodelosa.dyndns.org"
                SG_BaseDato = "db_valley"
                'Connection_String = "PROVIDER=MSDASQL;dsn=citroen;uid=;pwd=;" '   ' le agrega el path de la base de datos al Connectionstring
                Connection_String = "PROVIDER=MSDASQL;dsn=db_redmar;uid=root;pwd=eunice;server=" & Sp_Servidor & ";database=" & SG_BaseDato & ";"
                '" '   ' le agrega el path de la base de datos al Connectionstring
                
'                Connection_String = "Driver={MySQL ODBC 5.2 Driver Unicode Driver};Server=" & Sp_Servidor & " ;Port=3306;" & _
                                    "Database=" & SG_BaseDato & ";User=root;Password=eunice;Option=3;"
                
                Set CnV = New ADODB.Connection '  ' Nuevo objeto Connection
                CnV.CursorLocation = adUseClient
                'cn.CommandTimeout = 30
                CnV.ConnectionTimeout = 30
                CnV.Open Connection_String '  'Abre la conexi�n
            Exit Function
FalloConexion:
    MsgBox "No fue posible establecer la conecci�n con el Servidor..." & vbNewLine & "Verifique que la Ip ingresada sea la correcta", vbInformation
    Exit Function
End Function

Function ConsultaV(RecSet As ADODB.Recordset, CSQL As String)
InicioConsulta:
            IS_IntentosConexion = IS_IntentosConexion + 1
             Set RecSet = New ADODB.Recordset '  'Nuevo recordset
            'Debug.Print CSQL
            On Error GoTo FalloConsulta
            
            RecSet.Open CSQL, CnV, adOpenStatic, adLockOptimistic '  ' abre
            IS_IntentosConexion = 0
            Exit Function
FalloConsulta:
    If IS_IntentosConexion < 2 Then
        ConexionBD
        GoTo InicioConsulta
    End If
        
    If EjecucionIDE Then
        MsgBox "Fallo la consulta a la base de datos " & vbNewLine & CSQL & vbNewLine & Err.Number & " - " & Err.Description
    Else
        MsgBox "Fallo la consulta a la base de datos " & vbNewLine & vbNewLine & Err.Number & " - " & Err.Description
    End If
    If IS_IntentosConexion = 2 Then End
End Function
