VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{DA729E34-689F-49EA-A856-B57046630B73}#1.0#0"; "progressbar-xp.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form Con_ListadoVouchers 
   Caption         =   "Informe de Vouchers Contabilizados"
   ClientHeight    =   8850
   ClientLeft      =   915
   ClientTop       =   645
   ClientWidth     =   13260
   LinkTopic       =   "Form1"
   ScaleHeight     =   8850
   ScaleWidth      =   13260
   Begin VB.CommandButton CmdEliminar 
      Caption         =   "F4 - Eliminar"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   525
      Left            =   5025
      TabIndex        =   8
      ToolTipText     =   "Exportar"
      Top             =   8130
      Width           =   1635
   End
   Begin VB.CommandButton CmdSeleccionar 
      Caption         =   "F2 - Ver"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   525
      Left            =   1770
      TabIndex        =   6
      ToolTipText     =   "Visualizar compra"
      Top             =   8130
      Width           =   1620
   End
   Begin VB.Frame FraProgreso 
      Caption         =   "Progreso exportaci�n"
      Height          =   795
      Left            =   1740
      TabIndex        =   14
      Top             =   4590
      Visible         =   0   'False
      Width           =   9510
      Begin Proyecto2.XP_ProgressBar BarraProgreso 
         Height          =   330
         Left            =   150
         TabIndex        =   15
         Top             =   270
         Width           =   9165
         _ExtentX        =   16166
         _ExtentY        =   582
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BrushStyle      =   0
         Color           =   16777088
         Scrolling       =   1
         ShowText        =   -1  'True
      End
   End
   Begin VB.CommandButton cmdExportar 
      Caption         =   "F3 - Excel"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   525
      Left            =   3405
      TabIndex        =   7
      ToolTipText     =   "Exportar"
      Top             =   8130
      Width           =   1620
   End
   Begin VB.CommandButton CmdNueva 
      Caption         =   "F1 - Nueva"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   525
      Left            =   135
      TabIndex        =   5
      ToolTipText     =   "Nueva compra"
      Top             =   8130
      Width           =   1620
   End
   Begin VB.CommandButton CmdSalir 
      Cancel          =   -1  'True
      Caption         =   "Esc - Retornar"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   525
      Left            =   11340
      TabIndex        =   9
      Top             =   8130
      Width           =   1620
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   660
      OleObjectBlob   =   "Con_ListadoVouchers.frx":0000
      Top             =   8700
   End
   Begin VB.Timer Timer1 
      Interval        =   10
      Left            =   15
      Top             =   8235
   End
   Begin VB.Frame frmOts 
      Caption         =   "Listado historico"
      Height          =   5745
      Left            =   30
      TabIndex        =   12
      Top             =   2310
      Width           =   12960
      Begin VB.TextBox TxtC1 
         Alignment       =   1  'Right Justify
         Height          =   330
         Index           =   3
         Left            =   11385
         Locked          =   -1  'True
         TabIndex        =   28
         Text            =   "Text1"
         Top             =   5310
         Width           =   1350
      End
      Begin VB.TextBox TxtC1 
         Alignment       =   1  'Right Justify
         Height          =   330
         Index           =   2
         Left            =   10080
         Locked          =   -1  'True
         TabIndex        =   27
         Text            =   "Text1"
         Top             =   5310
         Width           =   1320
      End
      Begin VB.TextBox TxtC1 
         Alignment       =   1  'Right Justify
         Height          =   330
         Index           =   1
         Left            =   8775
         Locked          =   -1  'True
         TabIndex        =   26
         Text            =   "Text1"
         Top             =   5310
         Width           =   1320
      End
      Begin VB.TextBox TxtC1 
         Alignment       =   1  'Right Justify
         Height          =   330
         Index           =   0
         Left            =   7470
         Locked          =   -1  'True
         TabIndex        =   25
         Text            =   "Text1"
         Top             =   5310
         Width           =   1305
      End
      Begin VB.CommandButton CmdModificaNro 
         Caption         =   "Modificar Nro Voucher"
         Height          =   255
         Left            =   195
         TabIndex        =   24
         Top             =   450
         Width           =   1785
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel7 
         Height          =   180
         Left            =   10860
         OleObjectBlob   =   "Con_ListadoVouchers.frx":0234
         TabIndex        =   23
         Top             =   195
         Width           =   1290
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel6 
         Height          =   270
         Left            =   8190
         OleObjectBlob   =   "Con_ListadoVouchers.frx":02AA
         TabIndex        =   22
         Top             =   195
         Width           =   1245
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   270
         Left            =   10695
         OleObjectBlob   =   "Con_ListadoVouchers.frx":0320
         TabIndex        =   21
         Top             =   435
         Width           =   1530
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   300
         Left            =   8040
         OleObjectBlob   =   "Con_ListadoVouchers.frx":03A4
         TabIndex        =   20
         Top             =   435
         Width           =   1830
      End
      Begin MSComctlLib.ListView LvDetalle 
         Height          =   4575
         Left            =   180
         TabIndex        =   13
         Top             =   720
         Width           =   12555
         _ExtentX        =   22146
         _ExtentY        =   8070
         View            =   3
         LabelWrap       =   0   'False
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   14
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "N109"
            Text            =   "Id Unico"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "N109"
            Text            =   "Numero"
            Object.Width           =   1411
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "T1000"
            Text            =   "Tipo Comprobante"
            Object.Width           =   1676
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Object.Tag             =   "F1000"
            Text            =   "Fecha"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Object.Tag             =   "T3000"
            Text            =   "Glosa"
            Object.Width           =   7937
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   5
            Key             =   "debitos"
            Object.Tag             =   "N100"
            Text            =   "D�bitos"
            Object.Width           =   2293
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   6
            Key             =   "creditos"
            Object.Tag             =   "N100"
            Text            =   "Cr�ditos"
            Object.Width           =   2293
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   7
            Key             =   "debitosn"
            Object.Tag             =   "N100"
            Text            =   "D�bitos"
            Object.Width           =   2293
         EndProperty
         BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   8
            Key             =   "creditosn"
            Object.Tag             =   "N100"
            Text            =   "Cr�ditos"
            Object.Width           =   2293
         EndProperty
         BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   9
            Object.Tag             =   "T100"
            Text            =   "de Centralizacion"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   10
            Object.Tag             =   "N109"
            Text            =   "Id Asiento"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(12) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   11
            Object.Tag             =   "N109"
            Text            =   "MES"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(13) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   12
            Object.Tag             =   "N109"
            Text            =   "ano"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(14) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   13
            Object.Tag             =   "T1000"
            Text            =   "APERTURA ?"
            Object.Width           =   2540
         EndProperty
      End
   End
   Begin VB.Frame Frame4 
      Caption         =   "Mes y A�o"
      Height          =   1155
      Left            =   345
      TabIndex        =   1
      Top             =   975
      Width           =   12645
      Begin VB.CommandButton CmdBusca 
         Caption         =   "Buscar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   435
         Left            =   10695
         TabIndex        =   4
         ToolTipText     =   "Busca texto ingresado"
         Top             =   540
         Width           =   1665
      End
      Begin VB.ComboBox CboTipo 
         Height          =   315
         ItemData        =   "Con_ListadoVouchers.frx":0428
         Left            =   2910
         List            =   "Con_ListadoVouchers.frx":042A
         Style           =   2  'Dropdown List
         TabIndex        =   3
         Top             =   450
         Width           =   2800
      End
      Begin VB.ComboBox comMes 
         Height          =   315
         ItemData        =   "Con_ListadoVouchers.frx":042C
         Left            =   135
         List            =   "Con_ListadoVouchers.frx":042E
         Style           =   2  'Dropdown List
         TabIndex        =   0
         Top             =   450
         Width           =   1575
      End
      Begin VB.ComboBox ComAno 
         Height          =   315
         Left            =   1695
         Style           =   2  'Dropdown List
         TabIndex        =   2
         Top             =   450
         Width           =   1215
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
         Height          =   255
         Left            =   120
         OleObjectBlob   =   "Con_ListadoVouchers.frx":0430
         TabIndex        =   10
         Top             =   255
         Width           =   375
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel5 
         Height          =   255
         Left            =   1725
         OleObjectBlob   =   "Con_ListadoVouchers.frx":0494
         TabIndex        =   11
         Top             =   255
         Width           =   375
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   210
         Left            =   2910
         OleObjectBlob   =   "Con_ListadoVouchers.frx":04F8
         TabIndex        =   19
         Top             =   240
         Width           =   2475
      End
   End
   Begin VB.Frame Frame3 
      Caption         =   "Empresa Activa"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   825
      Left            =   345
      TabIndex        =   16
      Top             =   150
      Width           =   12660
      Begin VB.TextBox SkEmpresa 
         BackColor       =   &H00FFFFC0&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00C00000&
         Height          =   435
         Left            =   2280
         TabIndex        =   18
         Text            =   "Text1"
         Top             =   345
         Width           =   7515
      End
      Begin VB.TextBox SkRut 
         BackColor       =   &H00FFFFC0&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00C00000&
         Height          =   435
         Left            =   195
         TabIndex        =   17
         Text            =   "Text1"
         Top             =   345
         Width           =   2085
      End
   End
End
Attribute VB_Name = "Con_ListadoVouchers"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim Sm_filtro As String

Private Sub CmdBusca_Click()
    If ComAno.ListCount = 0 Then Exit Sub
    Sm_filtro = " AND vou_ano_contable=" & ComAno.ItemData(ComAno.ListIndex) & " AND vou_mes_contable=" & comMes.ItemData(comMes.ListIndex) & " "
    If CboTipo.Text = "TODOS" Then
        '
    Else
        Sm_filtro = Sm_filtro & " AND vou_tipo=" & CboTipo.ItemData(CboTipo.ListIndex) & " "
    End If
    CargaV
End Sub

Private Sub CmdEliminar_Click()
    Dim Ip_IdV As Long, Ip_TA As Integer, Sp_DepreciacionVentaActivo As String * 2
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    If Val(LvDetalle.SelectedItem) = 0 Then Exit Sub
    'Primero identificamos si corresponde a centralizacion
    If MsgBox("Est� por eliminar el voucher Nro: " & vbNewLine & _
    LvDetalle.SelectedItem.SubItems(1) & vbNewLine & "�Continuar?..." & vbNewLine & _
    IIf(LvDetalle.SelectedItem.SubItems(9) = "SI", "ESTE ASIENTO PROVIENE DE CENTRALIZACION", ""), vbYesNo + vbQuestion) = vbNo Then Exit Sub
        
    Ip_IdV = LvDetalle.SelectedItem
    Ip_TA = LvDetalle.SelectedItem.SubItems(10)
    On Error GoTo ErroGraba
    cn.BeginTrans
            Sp_DepreciacionVentaActivo = ""
            'Ahora comprobamos si existe activo inomovilizado
            Sql = "SELECT r.aim_id,d.vod_debe,vod_haber " & _
                    "FROM con_vouchers_detalle d " & _
                    "JOIN con_relacion_activos r ON d.pla_id=r.pla_id AND d.vou_id=r.vou_id AND d.vod_id=r.vod_id " & _
                    "WHERE r.aim_id>0 AND  d.vou_id=" & Ip_IdV
            Consulta RsTmp, Sql
            If RsTmp.RecordCount > 0 Then
                RsTmp.MoveFirst
                Do While Not RsTmp.EOF
                
                
                
                    If RsTmp!vod_debe > 0 Then
                        'Antes de eliminar se debe consultar si este
                        'activo se encuenta en un voucher de egreso.
                        
                        Sql = "SELECT aim_id,vou_numero,vou_mes_contable,vou_ano_contable " & _
                            "FROM con_vouchers_detalle d " & _
                            "JOIN con_vouchers c ON d.vou_id=c.vou_id " & _
                            "WHERE rut_emp='" & SP_Rut_Activo & "' AND aim_id=" & RsTmp!aim_id & " AND vod_haber>0"
                        Consulta RsTmp2, Sql
                        If RsTmp2.RecordCount > 0 Then
                            MsgBox "El Activo contenido en este voucher se encuentra " & _
                            vbNewLine & " en un voucher de Egreso..." & vbNewLine & _
                            "Nro:" & RsTmp2!vou_numero & vbNewLine & "Periodo:" & RsTmp2!vou_mes_contable & "-" & RsTmp2!vou_ano_contable & vbNewLine & _
                            "Debe eliminar el voucher de egreso 1ro.", vbInformation
                            cn.RollbackTrans
                            Exit Sub
                        
                        End If
                        
                        
                    
                    
                        'Ingreso
                              '5 Marzo 2014 comentamos esto
                              'no sabemos por que cresta hacia esto. :/
                        'MsgBox "Se eliminar� activo codigo: " & RsTmp!aim_id
                        'cn.Execute "DELETE FROM con_activo_inmovilizado " & _
                        '            "WHERE aim_id=" & RsTmp!aim_id
                                
                        
                    ElseIf RsTmp!vod_haber > 0 Then
                        'Egreso
                        cn.Execute "UPDATE con_activo_inmovilizado " & _
                                    "SET aim_habilitado='SI',aim_fecha_egreso=Null " & _
                                    "WHERE aim_id=" & RsTmp!aim_id
                    
                    End If
                    RsTmp.MoveNext
                Loop
            
            
            End If
            
            
            If LvDetalle.SelectedItem.SubItems(10) = 14 Then
                Dim Sp_ActivosD As String
                'Aqui sabemos que el voucher corresponde a
                'Correcion monetearia
                '19 Abril 2013
                Sql = "SELECT vou1_id,vou2_id,vou3_id,vou4_id,vou5_id,vou6_id,vou7_id,dep_ano,dep_venta,aim_id " & _
                            "FROM con_depreciaciones " & _
                            "WHERE rut_emp='" & SP_Rut_Activo & "' AND ( vou1_id =" & Ip_IdV & " Or " & _
                            "vou2_id = " & Ip_IdV & " Or vou3_id = " & Ip_IdV & " Or vou4_id =" & Ip_IdV & " OR vou5_id=" & Ip_IdV & " OR vou6_id=" & Ip_IdV & " OR vou7_id=" & Ip_IdV & ") " & _
                            "GROUP BY vou1_id,vou2_id,vou3_id,vou4_id,vou5_id,vou6_id,vou7_id "
                Consulta RsTmp, Sql
                If RsTmp.RecordCount > 0 Then
                    Sp_DepreciacionVentaActivo = RsTmp!dep_venta
                    'Sabado 26 Julio 2014
                    'Verificar que el voucher que se quiere eliminar, las depreciaciones
                    'no tengan movimientos en a�os posteriores
                    Sql = "SELECT dep_id " & _
                            "FROM con_depreciaciones " & _
                            "WHERE rut_emp='" & SP_Rut_Activo & "' AND dep_ano>" & RsTmp!dep_ano
                    Consulta RsTmp3, Sql
                    If RsTmp3.RecordCount > 0 Then
                        MsgBox "Existen depreciaciones de a�os posteriores...", vbInformation
                        Exit Sub
                    
                    End If
                    
                    
                    If MsgBox("Este Voucher corresponde a Correcion Monetaria, y esta " & vbNewLine & _
                    "asociado a otros voucher's..." & vbNewLine & " Si lo elimnia, tambien se eliminan los vouchers asociados..." & vbNewLine & " �Continuar...?", vbQuestion + vbYesNo) = vbNo Then Exit Sub
                    
                    
                    
                    
                    
                    
                    
                      '30 Agosto 2014
                       'Si el activo fue vendido, solo lo marcamos como act_vendido='NO'
                       If Sp_DepreciacionVentaActivo = "SI" Then
                            '6 Sep 2014
                            'Eliminar el registro de venta
                            Sql = "SELECT no_documento,doc_id " & _
                                    "FROM ven_doc_venta " & _
                                    "WHERE rut_emp='" & SP_Rut_Activo & "' AND id IN(" & _
                                    "SELECT vdt_rel " & _
                                    "FROM con_voucher_detalle_auxiliar " & _
                                    "WHERE vdt_tipo='CLI' AND vod_id IN(" & _
                                                "SELECT vod_id " & _
                                                "FROM con_vouchers_detalle " & _
                                                "WHERE vou_id IN(" & RsTmp!vou1_id & "," & RsTmp!vou2_id & "," & RsTmp!vou3_id & "," & RsTmp!vou4_id & RsTmp!vou5_id & "," & RsTmp!vou6_id & "," & RsTmp!vou7_id & ")))"
                            Consulta RsTmp2, Sql
                            If RsTmp2.RecordCount > 0 Then
                                '6 Sep 2014
                                'Se encontr� documento de venta, debemos eliminarlo tambien
                                'No verificaremos si el documento tiene pagos, standby
                                'ya que es un procedimiento poco comun
                                cn.Execute "DELETE FROM ven_doc_venta " & _
                                            "WHERE rut_emp='" & SP_Rut_Activo & "' AND no_documento=" & RsTmp2!no_documento & " AND doc_id=" & RsTmp2!doc_id
                                cn.Execute "DELETE FROM ven_detalle " & _
                                            "WHERE rut_emp='" & SP_Rut_Activo & "' AND no_documento=" & RsTmp2!no_documento & " AND doc_id=" & RsTmp2!doc_id
                                
                            End If
                            
                       
                            Sql = "UPDATE con_activo_inmovilizado SET aim_vendido='NO' " & _
                                        "WHERE aim_id=" & RsTmp!aim_id
                       Else
                            'Devolver los meses que se han descontado del activo en la depreciacion
                            Sql = "UPDATE con_activo_inmovilizado a " & _
                                         "JOIN con_depreciaciones b USING(aim_id) " & _
                                         "SET a.aim_vida_util = a.aim_vida_util + b.dep_meses_depreciados " & _
                                         "WHERE b.dep_ano=" & ComAno.Text & " AND a.aim_id IN " & _
                                         "(SELECT aim_id " & _
                                         "FROM con_depreciaciones " & _
                                         "WHERE rut_emp='" & SP_Rut_Activo & "' AND ( vou1_id =" & Ip_IdV & " Or " & _
                                         "vou2_id = " & Ip_IdV & " Or vou3_id = " & Ip_IdV & " Or vou4_id =" & Ip_IdV & " OR vou5_id=" & Ip_IdV & ")) "
                        End If
                        cn.Execute Sql
                    
                    
                    
                    
                        'Aqui entonces se van a eliminar los vouchers asociados
                        'Ademas las depreciaciones guardas tambien se eliminaran
                        'cn.Execute "DELETE con_vouchers.*,con_vouchers_detalle.* " & _
                                "FROM con_vouchers " & _
                                "JOIN con_vouchers_detalle USING(vou_id) " & _
                                "WHERE rut_emp='" & SP_Rut_Activo & "' AND con_vouchers.vou_id IN(" & RsTmp!vou1_id & "," & RsTmp!vou2_id & "," & RsTmp!vou3_id & "," & RsTmp!vou4_id & ")"
                                
                        cn.Execute "DELETE FROM con_individualiza_valor_voucher_activo_inm " & _
                                    "WHERE vod_id IN (SELECT vod_id " & _
                                                        "FROM con_vouchers_detalle " & _
                                                        "WHERE vou_id=" & RsTmp!vou1_id & ")"
                                
                       cn.Execute "DELETE FROM con_vouchers " & _
                                    "WHERE rut_emp='" & SP_Rut_Activo & "' AND vou_id IN(" & RsTmp!vou1_id & "," & RsTmp!vou2_id & "," & RsTmp!vou3_id & "," & RsTmp!vou4_id & "," & RsTmp!vou5_id & "," & RsTmp!vou6_id & "," & RsTmp!vou7_id & ")"
                       
                       cn.Execute "DELETE FROM con_relacion_activos " & _
                                    "WHERE vod_id IN(" & _
                                                "SELECT vod_id " & _
                                                "FROM con_vouchers_detalle " & _
                                                "WHERE vou_id IN(" & RsTmp!vou1_id & "," & RsTmp!vou2_id & "," & RsTmp!vou3_id & "," & RsTmp!vou4_id & RsTmp!vou5_id & "," & RsTmp!vou6_id & "," & RsTmp!vou7_id & "))"
                                    
                       
                       
                       cn.Execute "DELETE FROM con_vouchers_detalle " & _
                                    "WHERE vou_id IN(" & RsTmp!vou1_id & "," & RsTmp!vou2_id & "," & RsTmp!vou3_id & "," & RsTmp!vou4_id & "," & RsTmp!vou5_id & ")"
                                
                                
                       
                       
                                
                                
                                
                     
                        Sql = "DELETE FROM con_depreciaciones " & _
                                    "WHERE dep_ano=" & Me.ComAno.Text & " AND rut_emp='" & SP_Rut_Activo & "' AND ( vou1_id =" & RsTmp!vou1_id & " Or " & _
                            "vou2_id = " & RsTmp!vou2_id & " Or vou3_id = " & RsTmp!vou3_id & " Or vou4_id =" & RsTmp!vou4_id & " OR vou5_id=" & RsTmp!vou5_id & " OR vou6_id=" & RsTmp!vou6_id & " OR vou7_id=" & RsTmp!vou7_id & ") "
                        cn.Execute Sql
                
                End If
            End If
            
            
            'Aqui comprobaremos si el voucher corresponde a asiento de apertura
            'Y ademas si estes tiene las ctas clietnes y/o proveedores
            'Si las tiene, comprobar que no tenga algun abono
            If LvDetalle.SelectedItem.SubItems(13) = "SI" Then
                Sql = "SELECT cant_abonos,vdt_tipo,vdt_rel " & _
                        "FROM vi_voucher_cantidad_abonos " & _
                        "WHERE vou_id=" & Ip_IdV & " AND " & _
                        "((pla_id=" & IG_IdCtaClientes & " AND vdt_tipo='CLI') OR (pla_id=" & IG_IdCtaProveedores & " AND vdt_tipo='PRO'))"
                Consulta RsTmp, Sql
                If RsTmp.RecordCount > 0 Then
                    RsTmp.MoveFirst
                    Do While Not RsTmp.EOF
                        
                        If RsTmp!cant_abonos > 0 Then
                            MsgBox "Este voucher tiene documentos  a los que se le han" & vbNewLine & _
                            "realizado abonos, no es posible eliminarlo "
                            cn.RollbackTrans
                            Exit Sub
                        End If
                        RsTmp.MoveNext
                    Loop
                End If
                                            
                If RsTmp.RecordCount > 0 Then                           'mario tambien se va a eliminar el documento de la cta cte
                    RsTmp.MoveFirst
                    Do While Not RsTmp.EOF
                        If RsTmp!vdt_tipo = "CLI" Then
                            Sql = "SELECT no_documento,doc_id " & _
                                    "FROM ven_doc_venta " & _
                                        "WHERE id=" & RsTmp!vdt_rel
                            Consulta RsTmp3, Sql
                            If RsTmp3.RecordCount > 0 Then
                                
                                    cn.Execute "DELETE FROM ven_doc_venta WHERE no_documento=" & RsTmp3!no_documento & " AND doc_id=" & RsTmp3!doc_id & " AND rut_emp='" & SP_Rut_Activo & "'"
                            End If
                            cn.Execute "DELETE FROM ven_doc_venta WHERE id=" & RsTmp!vdt_rel
                        End If
                        If RsTmp!vdt_tipo = "PRO" Then
                            cn.Execute "DELETE FROM com_doc_compra WHERE id=" & RsTmp!vdt_rel
                            cn.Execute "DELETE FROM com_doc_compra_detalle WHERE id=" & RsTmp!vdt_rel
                        End If
                        RsTmp.MoveNext
                    Loop
                End If
                
                
                
            End If
            
            
            
            
            
            'Ahora eliminar el vouchers y su detalle
            cn.Execute "DELETE FROM con_vouchers " & _
                        "WHERE vou_id=" & Ip_IdV
            cn.Execute "DELETE FROM con_vouchers_detalle " & _
                        "WHERE vou_id=" & Ip_IdV
            
            
            'Sql = "DELETE con_vouchers.*,con_vouchers_detalle.* " & _
            '        "FROM con_vouchers " & _
            '        "JOIN con_vouchers_detalle USING(vou_id) " & _
            '        "WHERE con_vouchers.vou_id=" & Ip_IdV
            'cn.Execute Sql
            
            If LvDetalle.SelectedItem.SubItems(9) = "SI" Then
                
                If Ip_TA = 2 Or Ip_TA = 10 Then
                    'Dejamos libres las ventas que esten marcadas con centralizaciones
                      Sql = "UPDATE ven_doc_venta SET ctl_id=0 " & _
                                   "WHERE ctl_id=(SELECT ctl_id " & _
                                    "FROM con_centralizaciones " & _
                                    "WHERE rut_emp='" & SP_Rut_Activo & "' AND ctl_mes=" & comMes.ItemData(comMes.ListIndex) & "  AND ctl_ano=" & ComAno.Text & " AND asi_id=" & Ip_TA & ")"
                     cn.Execute Sql
                ElseIf Ip_TA = 1 Or Ip_TA = 11 Then
                    'Dejamos libres las compras que esten marcadas con centralizaciones
                     cn.Execute "UPDATE com_doc_compra SET ctl_id=0 " & _
                                   "WHERE ctl_id=(SELECT ctl_id " & _
                                    "FROM con_centralizaciones " & _
                                    "WHERE rut_emp='" & SP_Rut_Activo & "' AND ctl_mes=" & comMes.ItemData(comMes.ListIndex) & "  AND ctl_ano=" & ComAno.Text & " AND asi_id=" & Ip_TA & ")"
                     
                End If
                'Aqui dejamos el perido libre de centralizacion de el asiento correspondiente
                cn.Execute "DELETE FROM con_centralizaciones " & _
                           "WHERE asi_id=" & LvDetalle.SelectedItem.SubItems(10) & " AND ctl_mes=" & LvDetalle.SelectedItem.SubItems(11) & " AND ctl_ano=" & LvDetalle.SelectedItem.SubItems(12) & " AND rut_emp='" & SP_Rut_Activo & "'"
                
            End If

    cn.CommitTrans
    
    
    CargaV
     
     
    Exit Sub
ErroGraba:
    
    cn.RollbackTrans
    MsgBox "Ocurrio un problema al intentar guardar los cambios..." & _
    vbNewLine & Err.Number & " " & Err.Description, vbInformation
     
        
End Sub

Private Sub CmdExportar_Click()
    Dim tit(2) As String
    If LvDetalle.ListItems.Count = 0 Then Exit Sub
    FraProgreso.Visible = True
    tit(0) = Me.Caption & " " & DtFecha & " - " & Time
    tit(1) = Principal.SkEmpresa
    tit(2) = "RUT:" & SP_Rut_Activo
    ExportarNuevo LvDetalle, tit, Me, BarraProgreso
    FraProgreso.Visible = False
End Sub

Private Sub CmdModificaNro_Click()
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    If Val(LvDetalle.SelectedItem) = 0 Then Exit Sub
    
    Dim Lp_NuevoNro As Variant
    Lp_NuevoNro = InputBox("Ingrese Nro de voucher", "Cambiar Nro", LvDetalle.SelectedItem.SubItems(1))
    '
    If Val(Lp_NuevoNro) > 0 Then
        'Buscamos si esta disponible
        If Not VoucherDisponible(Val(Lp_NuevoNro)) Then
            MsgBox "Nro de voucher no esta disponible...", vbInformation
            Exit Sub
        Else
            'Se procede a cambiar el nro de voucher
            Sql = "UPDATE con_vouchers SET vou_numero=" & Lp_NuevoNro & " " & _
                    "WHERE vou_id=" & LvDetalle.SelectedItem
            cn.Execute Sql
            CargaV
        End If
    End If
    
End Sub

Private Sub CmdNueva_Click()
    On Error GoTo fina
    DG_ID_Unico = 0
    Con_Vouchers.Sm_NroVEditado = 0
    Con_Vouchers.Tag = "NUEVA"
    Con_Vouchers.Sm_Centralizado = "NO"
    DG_ID_Unico = 0
    Con_Vouchers.Show 1
    CargaV
    LvDetalle.SetFocus
    Exit Sub
fina:
End Sub

Private Sub CmdSeleccionar_Click()
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    If Val(LvDetalle.SelectedItem.Text) = 0 Then Exit Sub
    
    DG_ID_Unico = LvDetalle.SelectedItem.Text
    Me.Hide
    If DG_ID_Unico > 0 Then
        Con_Vouchers.Sm_Centralizado = "NO"
        Con_Vouchers.Sm_NroVEditado = LvDetalle.SelectedItem.SubItems(1)
        If LvDetalle.SelectedItem.SubItems(9) = "SI" Then Con_Vouchers.Sm_Centralizado = "SI"
       Con_Vouchers.Show 1
       CargaV
    End If
    Me.Show 1
    
End Sub





Private Sub ComAno_Click()
    If ComAno.ListCount = 0 Then Exit Sub
    CmdBusca_Click
End Sub

Private Sub comMes_Click()
    If comMes.ListCount = 0 Then Exit Sub
    CmdBusca_Click
End Sub

Private Sub Form_KeyUp(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF1 Then CmdNueva_Click
    If KeyCode = vbKeyF2 Then CmdSeleccionar_Click
    If KeyCode = vbKeyF3 Then CmdExportar_Click
    If KeyCode = vbKeyF4 Then CmdEliminar_Click
End Sub
Private Sub cmdSalir_Click()
    On Error Resume Next
    Unload Me
End Sub

Private Sub Form_Load()
    SkRut = SP_Rut_Activo
    SkEmpresa = SP_Empresa_Activa
    Skin2 Me, , 7
    Centrar Me
    
    CboTipo.AddItem "INGRESO"
    CboTipo.AddItem "EGRESO"
    CboTipo.AddItem "TRASPASO"
    CboTipo.AddItem "TODOS"
    CboTipo.ItemData(0) = 1
    CboTipo.ItemData(1) = 2
    CboTipo.ItemData(2) = 3
    CboTipo.ListIndex = CboTipo.ListCount - 1
    For i = 1 To 12
        comMes.AddItem UCase(MonthName(i, False))
        comMes.ItemData(comMes.ListCount - 1) = i
    Next
    Busca_Id_Combo comMes, Val(IG_Mes_Contable)
    LLenaYears ComAno, 2010
   ' For i = Year(Date) To 2010 Step -1
   ' For i = Year(Date) To Year(Date) - 1 Step -1
   '     ComAno.AddItem i
   '     ComAno.ItemData(ComAno.ListCount - 1) = i
   ' Next
   ' Busca_Id_Combo ComAno, Val(IG_Ano_Contable)
    Sm_filtro = " AND vou_ano_contable=" & ComAno.ItemData(ComAno.ListIndex) & " AND vou_mes_contable=" & comMes.ItemData(comMes.ListIndex) & " "
    CargaV
End Sub
Private Sub CargaV()
    Sql = "SELECT con_vouchers.vou_id,vou_numero,CASE vou_tipo WHEN 1 THEN 'INGRESO' WHEN 2 THEN 'EGRESO' WHEN 3 THEN 'TRASPASO' END comprobante," & _
              "CAST(CONCAT_WS('-',vou_ano_contable,vou_mes_contable,vou_dia) AS DATE) fecha, vou_glosa, " & _
                "IF(vou_tiponiif=1 OR vou_tiponiif=2,SUM(vod_debe),0) debe," & _
                "IF(vou_tiponiif=1 OR vou_tiponiif=2,SUM(vod_haber),0) haber," & _
                "IF(vou_tiponiif=1 OR vou_tiponiif=3,SUM(vod_debe),0) debe_niif," & _
                "IF(vou_tiponiif=1 OR vou_tiponiif=3,SUM(vod_haber),0) haber_niif," & _
                "vou_centralizacion,vou_id_asiento,vou_mes_contable,vou_ano_contable,vou_apertura " & _
            "FROM con_vouchers " & _
            "INNER JOIN con_vouchers_detalle USING(vou_id) " & _
            "WHERE rut_emp='" & SP_Rut_Activo & "' " & Sm_filtro & _
            "GROUP BY con_vouchers.vou_id " & _
            "HAVING debe+haber+debe_niif+haber_niif>0 " & _
            "ORDER BY vou_numero "
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvDetalle, False, True, True, False
        
    With LvDetalle
        '.ListItems.Add , , "z"
        'i = .ListItems.Count
        '.ListItems(i).SubItems(4) = "TOTALES"
        '.ListItems(i).SubItems(5) = NumFormat(TotalizaColumna(LvDetalle, "debitos"))
        '.ListItems(i).SubItems(6) = NumFormat(TotalizaColumna(LvDetalle, "creditos"))
        '.ListItems(i).SubItems(7) = NumFormat(TotalizaColumna(LvDetalle, "debitosn"))
        ''.ListItems(i).SubItems(8) = NumFormat(TotalizaColumna(LvDetalle, "creditosn"))
        TxtC1(0) = NumFormat(TotalizaColumna(LvDetalle, "debitos"))
        TxtC1(1) = NumFormat(TotalizaColumna(LvDetalle, "creditos"))
        TxtC1(2) = NumFormat(TotalizaColumna(LvDetalle, "debitosn"))
        TxtC1(3) = NumFormat(TotalizaColumna(LvDetalle, "creditosn"))
        'For X = 1 To 8
        '    .ListItems(i).ListSubItems(X).Bold = True
        'Next
    End With
    
    
End Sub

Private Sub LvDetalle_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    ordListView ColumnHeader, Me, LvDetalle
End Sub

Private Sub LvDetalle_DblClick()
    CmdSeleccionar_Click
End Sub


Private Sub Timer1_Timer()
    On Error Resume Next
    LvDetalle.SetFocus
    Timer1.Enabled = False
End Sub
Private Function VoucherDisponible(Nro As Long) As Boolean
    Sql = "SELECT vou_id " & _
            "FROM con_vouchers " & _
            "WHERE vou_ano_contable=" & IG_Ano_Contable & " AND vou_mes_contable=" & IG_Mes_Contable & " AND  vou_numero=" & Nro & " AND rut_emp='" & SP_Rut_Activo & "'"
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        VoucherDisponible = False
    Else
        VoucherDisponible = True
    End If
End Function

