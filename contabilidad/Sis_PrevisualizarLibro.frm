VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{28D47522-CF84-11D1-834C-00A0249F0C28}#1.0#0"; "gif89.dll"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{DA729E34-689F-49EA-A856-B57046630B73}#1.0#0"; "progressbar-xp.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form Sis_PrevisualizarLibro 
   BackColor       =   &H00FFFFFF&
   Caption         =   "Form1"
   ClientHeight    =   10395
   ClientLeft      =   3150
   ClientTop       =   345
   ClientWidth     =   15360
   LinkTopic       =   "Form1"
   ScaleHeight     =   10395
   ScaleWidth      =   15360
   WindowState     =   2  'Maximized
   Begin VB.Frame FrmLoad 
      Height          =   1680
      Left            =   9360
      TabIndex        =   33
      Top             =   6675
      Visible         =   0   'False
      Width           =   2805
      Begin GIF89LibCtl.Gif89a Gif89a1 
         Height          =   1275
         Left            =   210
         OleObjectBlob   =   "Sis_PrevisualizarLibro.frx":0000
         TabIndex        =   34
         Top             =   285
         Width           =   2445
      End
   End
   Begin VB.CommandButton CmdLibroSinSII 
      Caption         =   "Opcion Compra SII"
      Height          =   285
      Left            =   4230
      TabIndex        =   31
      Top             =   9480
      Visible         =   0   'False
      Width           =   1710
   End
   Begin MSComctlLib.ListView LvCompra 
      Height          =   1245
      Left            =   15495
      TabIndex        =   30
      Top             =   4950
      Visible         =   0   'False
      Width           =   3420
      _ExtentX        =   6033
      _ExtentY        =   2196
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   29
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "tipo"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "folio"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "rut contraparte"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Text            =   "tasa impuesto"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Text            =   "razon social contraparte"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   5
         Text            =   "tipo impuesto"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   6
         Text            =   "fecha emision"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   7
         Text            =   "anulado"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   8
         Text            =   "monto exento"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   9
         Text            =   "monto neto"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   10
         Text            =   "monto iva recuperable"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(12) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   11
         Text            =   "cod iva nr"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(13) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   12
         Text            =   "monto iva nr"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(14) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   13
         Text            =   "iva uso comun"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(15) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   14
         Text            =   "cod otro impuesto"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(16) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   15
         Text            =   "tasa oto imp"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(17) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   16
         Text            =   "monto otro imp"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(18) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   17
         Text            =   "monto total"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(19) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   18
         Text            =   "monto otro imouesto sc"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(20) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   19
         Text            =   "monto activo fijo"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(21) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   20
         Text            =   "monto iva activo fijo"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(22) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   21
         Text            =   "iva no retenido"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(23) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   22
         Text            =   "puros"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(24) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   23
         Text            =   "cigarrillos"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(25) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   24
         Text            =   "elaborados"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(26) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   25
         Text            =   "imp a vehiculos"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(27) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   26
         Text            =   "codigo suc sii"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(28) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   27
         Text            =   "numero interno"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(29) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   28
         Text            =   "emisor/receptor"
         Object.Width           =   2540
      EndProperty
   End
   Begin VB.Frame FraProcesandoDTE 
      Height          =   1485
      Left            =   1470
      TabIndex        =   27
      Top             =   4305
      Visible         =   0   'False
      Width           =   13020
      Begin VB.Timer Timer2 
         Enabled         =   0   'False
         Interval        =   20
         Left            =   2940
         Top             =   930
      End
      Begin Proyecto2.XP_ProgressBar XP_ProgressBar1 
         Height          =   255
         Left            =   4050
         TabIndex        =   28
         Top             =   975
         Width           =   4335
         _ExtentX        =   7646
         _ExtentY        =   450
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BrushStyle      =   0
         Color           =   16750899
         Scrolling       =   2
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel13 
         Height          =   510
         Left            =   1350
         OleObjectBlob   =   "Sis_PrevisualizarLibro.frx":0086
         TabIndex        =   29
         Top             =   420
         Width           =   10275
      End
   End
   Begin MSComDlg.CommonDialog Dialogo 
      Left            =   12030
      Top             =   1905
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.CommandButton cmdVolver 
      Cancel          =   -1  'True
      Caption         =   "Retornar"
      Height          =   435
      Left            =   13320
      TabIndex        =   16
      Top             =   1755
      Width           =   1845
   End
   Begin VB.Frame FraProgreso 
      Height          =   540
      Left            =   2310
      TabIndex        =   12
      Top             =   8745
      Visible         =   0   'False
      Width           =   9930
      Begin Proyecto2.XP_ProgressBar PBar 
         Height          =   255
         Left            =   105
         TabIndex        =   13
         Top             =   195
         Width           =   9585
         _ExtentX        =   16907
         _ExtentY        =   450
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BrushStyle      =   0
         Color           =   16777088
         Scrolling       =   4
         ShowText        =   -1  'True
      End
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   7980
      Left            =   0
      TabIndex        =   2
      Top             =   2295
      Width           =   15315
      _ExtentX        =   27014
      _ExtentY        =   14076
      _Version        =   393216
      TabHeight       =   520
      TabCaption(0)   =   "Libro"
      TabPicture(0)   =   "Sis_PrevisualizarLibro.frx":014E
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "LvLibroVenta"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "LvLibro"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "CmdExportaLibro"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "CmdPrintLibro"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "CmdSubirLibroElectronico"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "TxtEstadoLibro"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).Control(6)=   "CmdEliminaLibroElectronico"
      Tab(0).Control(6).Enabled=   0   'False
      Tab(0).Control(7)=   "CmdRectificar"
      Tab(0).Control(7).Enabled=   0   'False
      Tab(0).Control(8)=   "CmdConsultarEstado"
      Tab(0).Control(8).Enabled=   0   'False
      Tab(0).Control(9)=   "CmdAcuse"
      Tab(0).Control(9).Enabled=   0   'False
      Tab(0).Control(10)=   "CmdAjustar"
      Tab(0).Control(10).Enabled=   0   'False
      Tab(0).ControlCount=   11
      TabCaption(1)   =   "Cuadro Resumen"
      TabPicture(1)   =   "Sis_PrevisualizarLibro.frx":016A
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "LvResumenVentas"
      Tab(1).Control(1)=   "LVResumen"
      Tab(1).Control(2)=   "CmdExportaResumen"
      Tab(1).Control(3)=   "CmdPrintResumen"
      Tab(1).ControlCount=   4
      TabCaption(2)   =   "Asiento Contable"
      TabPicture(2)   =   "Sis_PrevisualizarLibro.frx":0186
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "LvTemp"
      Tab(2).Control(1)=   "Frame2"
      Tab(2).Control(2)=   "Frame3"
      Tab(2).ControlCount=   3
      Begin VB.CommandButton CmdAjustar 
         Caption         =   "Ajustar"
         Height          =   300
         Left            =   13530
         TabIndex        =   35
         ToolTipText     =   "No mostrar columnas sin movimientos"
         Top             =   7170
         Width           =   1380
      End
      Begin VB.CommandButton CmdAcuse 
         Caption         =   "Dar Acuse Comercial (Incluido Acuse de Mercaderias)"
         Height          =   300
         Left            =   8115
         TabIndex        =   32
         ToolTipText     =   "Dar acuse comercial Aceptado a todos los documentos de su libro."
         Top             =   7170
         Width           =   5340
      End
      Begin VB.CommandButton CmdConsultarEstado 
         Caption         =   "Consultar estado"
         Height          =   300
         Left            =   6000
         TabIndex        =   26
         Top             =   7170
         Width           =   2085
      End
      Begin VB.CommandButton CmdRectificar 
         Caption         =   "-- - Rectificar Libro"
         Height          =   360
         Left            =   2010
         TabIndex        =   25
         ToolTipText     =   "Debe contar con codigo del SII"
         Top             =   7485
         Width           =   2070
      End
      Begin VB.CommandButton CmdEliminaLibroElectronico 
         Caption         =   "Elimina Libro"
         CausesValidation=   0   'False
         Height          =   255
         Left            =   14070
         TabIndex        =   22
         Top             =   7485
         Width           =   1035
      End
      Begin VB.TextBox TxtEstadoLibro 
         BackColor       =   &H00000000&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H0000FFFF&
         Height          =   285
         Left            =   4155
         Locked          =   -1  'True
         TabIndex        =   21
         TabStop         =   0   'False
         Text            =   "Text1"
         Top             =   7500
         Width           =   9885
      End
      Begin VB.CommandButton CmdSubirLibroElectronico 
         Caption         =   "F6 - Subir Libro Electronico "
         Height          =   360
         Left            =   2025
         TabIndex        =   20
         Top             =   7140
         Width           =   2055
      End
      Begin MSComctlLib.ListView LvTemp 
         Height          =   2100
         Left            =   -64245
         TabIndex        =   19
         Top             =   5610
         Visible         =   0   'False
         Width           =   6225
         _ExtentX        =   10980
         _ExtentY        =   3704
         View            =   3
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   4
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "T500"
            Text            =   "Cod"
            Object.Width           =   882
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "T3000"
            Text            =   "CUENTAS"
            Object.Width           =   4762
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   2
            Key             =   "debe"
            Object.Tag             =   "N100"
            Text            =   "DEBE"
            Object.Width           =   2381
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   3
            Key             =   "haber"
            Object.Tag             =   "N100"
            Text            =   "HABER"
            Object.Width           =   2381
         EndProperty
      End
      Begin VB.CommandButton CmdPrintResumen 
         Caption         =   "F4 - Imprimir"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   350
         Left            =   -72450
         TabIndex        =   17
         Top             =   7065
         Width           =   2130
      End
      Begin VB.CommandButton CmdPrintLibro 
         Caption         =   "F4 - Imprimir LIBROS"
         Height          =   345
         Left            =   105
         TabIndex        =   15
         Top             =   7485
         Width           =   1740
      End
      Begin VB.CommandButton CmdExportaLibro 
         Caption         =   "F3 - Exportar a Excel"
         Height          =   360
         Left            =   120
         TabIndex        =   14
         ToolTipText     =   "Salir"
         Top             =   7125
         Width           =   1725
      End
      Begin VB.CommandButton CmdExportaResumen 
         Caption         =   "F3 - Exportar a Excel"
         Height          =   350
         Left            =   -74595
         TabIndex        =   11
         ToolTipText     =   "Salir"
         Top             =   7065
         Width           =   2145
      End
      Begin VB.Frame Frame2 
         Caption         =   "ASIENTOS  CONTABLES SIN NOTA DE CREDITO"
         Height          =   4575
         Left            =   -74205
         TabIndex        =   8
         Top             =   1185
         Width           =   6660
         Begin ACTIVESKINLibCtl.SkinLabel SkDiferencia 
            Height          =   210
            Left            =   4995
            OleObjectBlob   =   "Sis_PrevisualizarLibro.frx":01A2
            TabIndex        =   18
            Top             =   4230
            Width           =   1530
         End
         Begin VB.CommandButton CmdExportar 
            Caption         =   "Exportar Sin Nota de Credito"
            Height          =   390
            Left            =   135
            TabIndex        =   10
            ToolTipText     =   "Salir"
            Top             =   4065
            Width           =   2415
         End
         Begin MSComctlLib.ListView LvAC 
            Height          =   3705
            Left            =   135
            TabIndex        =   9
            Top             =   330
            Width           =   6405
            _ExtentX        =   11298
            _ExtentY        =   6535
            View            =   3
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            FullRowSelect   =   -1  'True
            GridLines       =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   0
            NumItems        =   4
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Object.Tag             =   "T500"
               Text            =   "Cod"
               Object.Width           =   882
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Object.Tag             =   "T3000"
               Text            =   "CUENTAS"
               Object.Width           =   4762
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   2
               Key             =   "debe"
               Object.Tag             =   "N100"
               Text            =   "DEBE"
               Object.Width           =   2381
            EndProperty
            BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   3
               Key             =   "haber"
               Object.Tag             =   "N100"
               Text            =   "HABER"
               Object.Width           =   2381
            EndProperty
         End
      End
      Begin VB.Frame Frame3 
         Caption         =   "ASIENTOS  CONTABLES CON NOTA DE CREDITO"
         Height          =   4575
         Left            =   -67575
         TabIndex        =   5
         Top             =   1185
         Width           =   6630
         Begin VB.CommandButton CmdExportaCNC 
            Caption         =   "Exportar Con Nota de Credito"
            Height          =   390
            Left            =   195
            TabIndex        =   6
            ToolTipText     =   "Salir"
            Top             =   4080
            Width           =   2565
         End
         Begin MSComctlLib.ListView LvCDetalle 
            Height          =   3705
            Left            =   120
            TabIndex        =   7
            Top             =   315
            Width           =   6345
            _ExtentX        =   11192
            _ExtentY        =   6535
            View            =   3
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            FullRowSelect   =   -1  'True
            GridLines       =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   0
            NumItems        =   5
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Object.Tag             =   "T400"
               Text            =   "Cod"
               Object.Width           =   882
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Object.Tag             =   "T3000"
               Text            =   "CUENTAS"
               Object.Width           =   4762
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   2
               Key             =   "debe"
               Object.Tag             =   "N100"
               Text            =   "DEBE"
               Object.Width           =   2381
            EndProperty
            BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   3
               Key             =   "haber"
               Object.Tag             =   "N100"
               Text            =   "HABER"
               Object.Width           =   2381
            EndProperty
            BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   4
               Key             =   "retenido"
               Object.Tag             =   "N100"
               Text            =   "retenido"
               Object.Width           =   0
            EndProperty
         End
      End
      Begin MSComctlLib.ListView LvLibro 
         Height          =   6705
         Left            =   60
         TabIndex        =   3
         Top             =   390
         Width           =   14745
         _ExtentX        =   26009
         _ExtentY        =   11827
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         Appearance      =   0
         NumItems        =   33
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "T1000"
            Text            =   "Folio"
            Object.Width           =   1411
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "F1000"
            Text            =   "Fech"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "T600"
            Text            =   "TD"
            Object.Width           =   1058
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Object.Tag             =   "T400"
            Text            =   "SII"
            Object.Width           =   706
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   4
            Object.Tag             =   "N109"
            Text            =   "Nro Doc."
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Object.Tag             =   "T2500"
            Text            =   "Razon Social"
            Object.Width           =   4762
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   6
            Object.Tag             =   "T1100"
            Text            =   "R.U.T."
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   7
            Object.Tag             =   "N100"
            Text            =   "Exentas"
            Object.Width           =   1940
         EndProperty
         BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   8
            Object.Tag             =   "N100"
            Text            =   "Afectas"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   9
            Object.Tag             =   "N100"
            Text            =   "I.V.A."
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   10
            Object.Tag             =   "N100"
            Text            =   "Ret. I.V.A."
            Object.Width           =   1940
         EndProperty
         BeginProperty ColumnHeader(12) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   11
            Object.Tag             =   "N100"
            Text            =   "Otros Impuestos"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(13) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   12
            Object.Tag             =   "N100"
            Text            =   "IVA Sin Credito"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(14) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   13
            Object.Tag             =   "N100"
            Text            =   "IVA Margen"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(15) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   14
            Object.Tag             =   "N100"
            Text            =   "Imp Extra 1"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(16) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   15
            Object.Tag             =   "N100"
            Text            =   "Imp Extra 2"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(17) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   16
            Object.Tag             =   "N100"
            Text            =   "Imp Extra 3"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(18) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   17
            Object.Tag             =   "N100"
            Text            =   "Imp Extra4"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(19) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   18
            Object.Tag             =   "N100"
            Text            =   "Imp Extra5"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(20) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   19
            Object.Tag             =   "N100"
            Text            =   "Imp Extra6"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(21) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   20
            Object.Tag             =   "N100"
            Text            =   "Imp Extra7"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(22) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   21
            Object.Tag             =   "N100"
            Text            =   "Imp Extra8"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(23) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   22
            Object.Tag             =   "N100"
            Text            =   "Imp Extra9"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(24) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   23
            Object.Tag             =   "N100"
            Text            =   "Imp Extra10"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(25) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   24
            Object.Tag             =   "N100"
            Text            =   "Imp Extra11"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(26) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   25
            Object.Tag             =   "N100"
            Text            =   "Imp Extra12"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(27) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   26
            Object.Tag             =   "N100"
            Text            =   "Total"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(28) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   27
            Object.Tag             =   "N100"
            Text            =   "Margen Dist."
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(29) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   28
            Object.Tag             =   "T2000"
            Text            =   "Contabilizacion"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(30) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   29
            Object.Tag             =   "N109"
            Text            =   "DOC_ID"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(31) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   30
            Object.Tag             =   "T1000"
            Text            =   "Tasa IVA"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(32) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   31
            Object.Tag             =   "N100"
            Text            =   "Monto Activo Fijo"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(33) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   32
            Text            =   "ID unico"
            Object.Width           =   2540
         EndProperty
      End
      Begin MSComctlLib.ListView LVResumen 
         Height          =   6195
         Left            =   -74640
         TabIndex        =   4
         Top             =   840
         Width           =   14640
         _ExtentX        =   25823
         _ExtentY        =   10927
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         Appearance      =   0
         NumItems        =   23
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "T1500"
            Text            =   "Documento"
            Object.Width           =   7056
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "N109"
            Text            =   "Cantidad"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   2
            Object.Tag             =   "N100"
            Text            =   "Exentas"
            Object.Width           =   2293
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   3
            Object.Tag             =   "N100"
            Text            =   "Monto Neto"
            Object.Width           =   2293
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   4
            Object.Tag             =   "N100"
            Text            =   "I.V.A."
            Object.Width           =   2293
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   5
            Object.Tag             =   "N100"
            Text            =   "Ret. I.V.A."
            Object.Width           =   2293
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   6
            Object.Tag             =   "N100"
            Text            =   "Otros Imp."
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   7
            Object.Tag             =   "N100"
            Text            =   "IVA Sin Credito"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   8
            Object.Tag             =   "N100"
            Text            =   "IVA Margen"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   9
            Object.Tag             =   "N100"
            Text            =   "Imp Extra 1"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   10
            Object.Tag             =   "N100"
            Text            =   "Imp Extra 2"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(12) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   11
            Object.Tag             =   "N100"
            Text            =   "Imp Extra 3"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(13) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   12
            Object.Tag             =   "N100"
            Text            =   "Imp Extra 4"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(14) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   13
            Object.Tag             =   "N100"
            Text            =   "Imp Extra 5"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(15) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   14
            Object.Tag             =   "N100"
            Text            =   "Imp Extra 6"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(16) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   15
            Object.Tag             =   "N100"
            Text            =   "Imp Extra 7"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(17) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   16
            Object.Tag             =   "N100"
            Text            =   "Imp Extra 8"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(18) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   17
            Object.Tag             =   "N100"
            Text            =   "Imp Extra 9"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(19) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   18
            Object.Tag             =   "N100"
            Text            =   "Imp Extra 10"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(20) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   19
            Object.Tag             =   "N100"
            Text            =   "Imp Extra 11"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(21) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   20
            Object.Tag             =   "N100"
            Text            =   "Imp Extra 12"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(22) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   21
            Object.Tag             =   "N100"
            Text            =   "TOTAL"
            Object.Width           =   2293
         EndProperty
         BeginProperty ColumnHeader(23) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   22
            Object.Tag             =   "N100"
            Text            =   "Margen Distribuidor"
            Object.Width           =   2540
         EndProperty
      End
      Begin MSComctlLib.ListView LvLibroVenta 
         Height          =   6705
         Left            =   75
         TabIndex        =   23
         Top             =   390
         Width           =   14940
         _ExtentX        =   26353
         _ExtentY        =   11827
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         Appearance      =   0
         NumItems        =   33
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "T1000"
            Text            =   "Folio"
            Object.Width           =   1411
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "F1000"
            Text            =   "Fech"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "T600"
            Text            =   "TD"
            Object.Width           =   1058
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Object.Tag             =   "T400"
            Text            =   "SII"
            Object.Width           =   706
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   4
            Object.Tag             =   "N109"
            Text            =   "Nro Doc."
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Object.Tag             =   "T2500"
            Text            =   "Razon Social"
            Object.Width           =   4762
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   6
            Object.Tag             =   "T1100"
            Text            =   "R.U.T."
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   7
            Object.Tag             =   "N100"
            Text            =   "Exentas"
            Object.Width           =   1940
         EndProperty
         BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   8
            Object.Tag             =   "N100"
            Text            =   "Afectas"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   9
            Object.Tag             =   "N100"
            Text            =   "I.V.A."
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   10
            Object.Tag             =   "N100"
            Text            =   "Ret. I.V.A."
            Object.Width           =   1940
         EndProperty
         BeginProperty ColumnHeader(12) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   11
            Object.Tag             =   "N100"
            Text            =   "Otros Impuestos"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(13) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   12
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(14) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   13
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(15) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   14
            Object.Tag             =   "N100"
            Text            =   "Total"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(16) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   15
            Object.Tag             =   "T2000"
            Text            =   "Contabilizacion"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(17) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   16
            Object.Tag             =   "N109"
            Text            =   "DOC_ID"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(18) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   17
            Object.Tag             =   "T1000"
            Text            =   "Tasa IVA"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(19) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   18
            Object.Tag             =   "N109"
            Text            =   "Cant Boletas"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(20) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   19
            Object.Tag             =   "T1000"
            Text            =   "BOLETA"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(21) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   20
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(22) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   21
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(23) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   22
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(24) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   23
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(25) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   24
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(26) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   25
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(27) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   26
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(28) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   27
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(29) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   28
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(30) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   29
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(31) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   30
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(32) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   31
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(33) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   32
            Object.Tag             =   "N109"
            Object.Width           =   2540
         EndProperty
      End
      Begin MSComctlLib.ListView LvResumenVentas 
         Height          =   6195
         Left            =   -74610
         TabIndex        =   24
         Top             =   570
         Width           =   14640
         _ExtentX        =   25823
         _ExtentY        =   10927
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         Appearance      =   0
         NumItems        =   11
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "T1500"
            Text            =   "Documento"
            Object.Width           =   7056
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "N109"
            Text            =   "Cantidad"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   2
            Object.Tag             =   "N100"
            Text            =   "Exentas"
            Object.Width           =   2293
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   3
            Object.Tag             =   "N100"
            Text            =   "Monto Neto"
            Object.Width           =   2293
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   4
            Object.Tag             =   "N100"
            Text            =   "I.V.A."
            Object.Width           =   2293
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   5
            Object.Tag             =   "N100"
            Text            =   "Ret. I.V.A."
            Object.Width           =   2293
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   6
            Object.Tag             =   "N100"
            Text            =   "Otros Imp."
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   7
            Object.Tag             =   "N100"
            Text            =   "IVA Sin Credito"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   8
            Object.Tag             =   "N100"
            Text            =   "IVA Margen"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   9
            Object.Tag             =   "N100"
            Text            =   "TOTAL"
            Object.Width           =   2293
         EndProperty
         BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   10
            Object.Tag             =   "N109"
            Text            =   "Doc_id"
            Object.Width           =   2540
         EndProperty
      End
   End
   Begin VB.Timer Timer1 
      Interval        =   20
      Left            =   8880
      Top             =   900
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   8040
      OleObjectBlob   =   "Sis_PrevisualizarLibro.frx":0200
      Top             =   135
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkLibro 
      Height          =   420
      Left            =   4740
      OleObjectBlob   =   "Sis_PrevisualizarLibro.frx":0434
      TabIndex        =   1
      Top             =   1905
      Width           =   5820
   End
   Begin MSComctlLib.ListView LVDetalle 
      Height          =   1830
      Left            =   15
      TabIndex        =   0
      Top             =   -15
      Width           =   15420
      _ExtentX        =   27199
      _ExtentY        =   3228
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      HideColumnHeaders=   -1  'True
      FullRowSelect   =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   1
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Object.Tag             =   "T500"
         Text            =   "Cod"
         Object.Width           =   26379
      EndProperty
   End
End
Attribute VB_Name = "Sis_PrevisualizarLibro"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public Sm_TipoLibro As String, Bm_PorFolio As Boolean, Sm_Mes_Contable As String, Sm_Ano_Contable As String, Sm_CompraVenta As String, Sm_Periodo As String
Dim Sm_File As Integer
Dim PFolio As String * 6, Pfecha As String * 8, Ptd As String * 3, PcodSii As String * 2, PnroDoc As String * 11, _
Pnombre As String * 19, PRut As String * 12, PExenta As String * 12, PAfecto As String * 12, _
Piva As String * 11, PRet As String * 11, Potros As String * 11, PTotal As String * 12, PnombreL As String * 19
Dim Texenta As Long, TAfecto As Long, Tiva As Long, Tret As Long, Totros As Long, Ttotal As Long, PDocs As String * 40
Dim Lp_Linea As Long, Lp_Pagina As Long, Cx As Double, Cy As Double, Dp As Double
Dim Sm_ColImp(26) As String * 12
Dim Sm_ColImpValor(26) As Long
Private Declare Function ShellExecute Lib "shell32.dll" Alias "ShellExecuteA" (ByVal hWnd As Long, ByVal lpOperation As String, ByVal lpFile As String, ByVal lpParameters As String, ByVal lpDirectory As String, ByVal nShowCmd As Long) As Long



Private Sub CmdAcuse_Click()
  GoTo acuseComercial
        Exit Sub
acuseComercial:
        On Error GoTo ErrorAcuse
        FraProcesandoDTE.Visible = True
        'Dim resp As Boolean
        DoEvents
        'Consulta electronica
        Dim obj As DTECloud.Integracion
        Set obj = New DTECloud.Integracion
        Dim sRut As String
        Dim sRutE As String
        Dim sTipo As String
        Dim sFolio As Long
        Dim iCodigoAcuse As String
        Dim sGlosa As String
        Dim bProductivo As Boolean
        Dim I_Cn As Integer
       
        iCodigoAcuse = "0"
        sGlosa = "Aceptado Ok en LC"
        bProductivo = True
       
        FrmLoad.Visible = True
       
        DoEvents
        cnt = 0
        For i = 1 To LvLibro.ListItems.Count
            
            If Val(LvLibro.ListItems(i).SubItems(3)) > 0 Then
                sRut = Replace(SP_Rut_Activo, ".", "")
                sRutE = Replace(LvLibro.ListItems(i).SubItems(6), ".", "")
                sTipo = Trim(LvLibro.ListItems(i).SubItems(3))
                sFolio = LvLibro.ListItems(i).SubItems(4)
                'Los iCodigoAcuse son '
                '0: Aceptado OK
                '1: Aceptado con Reparo
                '2: Rechazo Comercial
                'iCodigoAcuse = Mid(Me.CboGlosaAcuse.Text, 1, 1)
                'sGlosa = "Aceptado OK"
                
                
                obj.UrlServicioFacturacion = SG_Url_Factura_Electronica
                obj.Productivo = bProductivo
                obj.Password = "1234"
                
                'obj.AcuseComercialDTE(
                If obj.AcuseComercialDTE(sRut, sRutE, sTipo, sFolio, bProductivo, iCodigoAcuse, sGlosa, "acuse@alvamar.cl") Then
                    cnt = cnt + 1
                    Sql = "INSERT INTO sii_acuse (rut_emp,acu_folio,acu_cod_sii,acu_rut_proveedor,acu_codigo_acuse,acu_glosa) " & _
                            "VALUES('" & SP_Rut_Activo & "'," & sFolio & "," & sTipo & ",'" & LvLibro.ListItems(i).SubItems(6) & "'," & iCodigoAcuse & ",'" & sGlosa & "')"
                    cn.Execute Sql
                    'LvLibro.ListItems(i).SubItems(12) = sGlosa
                    If obj.AcuseDeReciboDeMercaderias(sRut, sRutE, sFolio, sTipo, "DOMICILIO COMERCIAL", True, "acuse@alvamar.cl") Then
                         Sql = "INSERT INTO sii_acuse (rut_emp,acu_folio,acu_cod_sii,acu_rut_proveedor,acu_codigo_acuse,acu_glosa,acu_mercaderia) " & _
                            "VALUES('" & SP_Rut_Activo & "'," & sFolio & "," & sTipo & ",'" & LvLibro.ListItems(i).SubItems(6) & "'," & iCodigoAcuse & ",'" & sGlosa & "','SI')"
                        cn.Execute Sql
                    End If
                   
                Else
                    MsgBox "Folio: " & sFolio & vbNewLine & "RUT :" & sRutE & vbNewLine & "No se pudo dar acuse a este DTE" & vbNewLine & Err.Description
                End If
            End If
          
        Next
       
        Me.FrmLoad.Visible = False
        FraProcesandoDTE.Visible = False
        MsgBox "Se dio acuse de recibo a " & cnt & " DTEs", vbInformation
        
        Exit Sub
ErrorConexion:
    FrmLoad.Visible = False
Me.FraProcesandoDTE.Visible = False
    MsgBox "Algo fallo :(  ..." & vbNewLine & vbNewLine & Err.Description, vbExclamation
    Exit Sub
ErrorAcuse:

    FrmLoad.Visible = False
 FraProcesandoDTE.Visible = False
    MsgBox "Algo fallo durante el envio:(  ..." & vbNewLine & vbNewLine & Err.Description, vbExclamation
End Sub

Private Sub CmdAjustar_Click()
    If LvLibro.ListItems.Count = 0 Then Exit Sub
    
    For i = 7 To LvLibro.ColumnHeaders.Count - 1
        If Val(LvLibro.ListItems(LvLibro.ListItems.Count).SubItems(i)) = 0 Then
            LvLibro.ColumnHeaders(i + 1).Width = 0
            'Exit For
        End If
    Next
    
End Sub

Private Sub CmdConsultarEstado_Click()
    FraProcesandoDTE.Visible = True
    DoEvents
    ConsultaEstadoLibro
    FraProcesandoDTE.Visible = False
End Sub

Private Sub CmdEliminaLibroElectronico_Click()
        Dim Sp_RutEliminar As String
        Dim obj As DTECloud.Integracion
        Set obj = New DTECloud.Integracion
        
        If MsgBox("�Seguro(a) de eliminar Libro?", vbOKCancel + vbQuestion) = vbCancel Then Exit Sub
        obj.UrlServicioFacturacion = SG_Url_Factura_Electronica
        obj.Password = "1234"
        'obj.EliminaLVEMensual
        Sp_RutEliminar = Trim(Replace(SP_Rut_Activo, ".", ""))
        If Mid(Sm_CompraVenta, 1, 5) = "VENTA" Then
        
            
            If obj.EliminaLVEMensual(Sp_RutEliminar, Sm_Periodo, "TipoEnvio as String", True) Then
            
          
         
                 MsgBox obj.DescripcionResultado
             Else
                 MsgBox obj.DescripcionResultado
             End If

         ElseIf Mid(Sm_CompraVenta, 1, 6) = "COMPRA" Then
            If obj.EliminaLCEMensual(Sp_RutEliminar, Sm_Periodo, "X", True) Then
                ''Consulta Ejecutada OK, detalle del estado en obj.DescripcionResultado
                MsgBox obj.DescripcionResultado
            Else
                MsgBox obj.DescripcionResultado
            End If
   
        End If
        
        
        ConsultaEstadoLibro
End Sub

Private Sub CmdExportaCNC_Click()
    Dim tit(2) As String
    FraProgreso.Visible = True
    tit(0) = "ASIENTO CONTABLE CON NOTA DE CREDITO"
    
    tit(1) = s_Libro & " " & Sm_Mes_Contable & " " & Sm_Ano_Contable
    tit(2) = "" 'Time
    ExportarNuevo LvCDetalle, tit, Me, PBar
    FraProgreso.Visible = False
End Sub

Private Sub CmdExportaLibro_Click()
    Dim tit(2) As String
    FraProgreso.Visible = True
    tit(0) = LvDetalle.ListItems(1)
    tit(1) = LvDetalle.ListItems(2)
    tit(2) = s_Libro & " " & Sm_Mes_Contable & " " & Sm_Ano_Contable & " " & LvDetalle.ListItems(LvDetalle.ListItems.Count)
    If Mid(Sm_CompraVenta, 1, 5) = "VENTA" Then
        ExportarNuevo LvLibroVenta, tit, Me, PBar
    Else
        ExportarNuevo LvLibro, tit, Me, PBar
    End If
    FraProgreso.Visible = False
End Sub

Private Sub CmdExportar_Click()
    Dim tit(2) As String
    FraProgreso.Visible = True
    tit(0) = "ASIENTO CONTABLE SIN NOTA DE CREDITO"
    tit(1) = s_Libro & " " & Sm_Mes_Contable & " " & Sm_Ano_Contable
    tit(2) = "" 'Time
    ExportarNuevo LvAC, tit, Me, PBar
    FraProgreso.Visible = False
End Sub

Private Sub CmdExportaResumen_Click()
 Dim tit(2) As String
    FraProgreso.Visible = True
    tit(0) = "CUADRO RESUMEN"
    tit(1) = s_Libro & " " & Sm_Mes_Contable & " " & Sm_Ano_Contable
    tit(2) = "" 'Time
    
    If Mid(Sm_CompraVenta, 1, 5) = "VENTA" Then
        ExportarNuevo Me.LvResumenVentas, tit, Me, PBar
    Else
        ExportarNuevo Me.LVResumen, tit, Me, PBar
    End If
    
    FraProgreso.Visible = False
End Sub

Private Sub CmdLibroSinSII_Click()
    Dim Sp_Archivo As String
    Dim le_Tipo As String
    Dim le_Folio As String
    Dim le_Fecha As String
    Dim le_Rut As String
    Dim le_Rsocial As String
    Dim le_ImpEspecifico As String
    Dim le_Impuestos As String
    Dim le_Exento As String
    Dim le_Neto As String
    Dim le_IVA As String
    Dim le_TasaIva As String
    Dim le_IvaUsoComun As String
    Dim le_IvaNoRecuperable As String
    Dim le_IvaNoRetenido As String
    Dim le_IvaRetenido As String
    Dim le_Total As String
    Dim le_MontoActivoFijo As String
    Dim le_TipoImpuesto As String
    Dim le_CodigoImpuesto As String
    Dim le_TasaImpuesto As String
    Dim le_MontoImpuesto As String
    Dim le_CV As String
    Dim le_CantidadBoletas As String
    Dim le_IvafueraPlazo As String
    Dim le_Monto_Activo_Fijo As String
    ''

   '' Dim Sp_Archivo As String
    Dim le_TipoDoc As String
 ''   Dim le_Folio As String
    Dim le_RutProv As String
    Dim le_TasaI As String
    Dim le_RazonSocial As String
    Dim le_TipoI As String
    Dim le_FechaEmis As String
    Dim le_Anulado As String
    Dim le_MExento As String
    Dim le_MNeto As String
    Dim le_MIvaRec As String
    Dim le_CodINR As String
    Dim le_MIVAnoRe As String
    Dim le_IVAusoCo As String
    Dim le_COI As String
    Dim le_TaOI As String
    Dim le_MOtroImp As String
    Dim le_MontoTotal As String
    Dim le_MotrIScr As String
    Dim le_MActFijo As String
    Dim le_MivaAfij As String
    Dim le_IvaNORet As String
    Dim le_TabacoPu As String
    Dim le_TabacoCi As String
    Dim le_TabacoEl As String
    Dim le_ImpVehic As String
    Dim le_CodSuSii As String
    Dim le_NroInter As String
    Dim le_EmiRecep As String
    
'TIPO;FOLIO;FECHA;RUT;RAZON_SOCIAL;IMPUESTO_ESPECIFICO;IMPUESTOS;EXENTO;NETO;IVA;TASA_IVA"
';IVA_USO_COMUN;IVA_NO_RECUPERABLE;IVA_NO_RETENIDO;IVA_RETENIDO;TOTAL;MONTO_ACTIVO_FIJO;
'TIPO_IMPUESTO;CODIGO_IMPUESTO;TASA_IMPUESTO;MONTO_IMPUESTO"
        Sql = "SELECT sii,nro,rut,tasa_iva,rs,IFNULL(imp_extra12, 1),fecha,IFNULL(imp_extra12, ''),exentas,afectas,iva,'','','','','','',total,'',monto_activo_fijo,(monto_activo_fijo*0.19),'','','','','','','','' " & _
              "FROM con_libro_compra_temporal "
              ''& _
              ''"WHERE  pro_estado='En Mantencion'"


    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, Me.LvCompra, False, True, True, False
    
    '17 Septiembre 2018
    'Subir libro  electronico
    'Autor: alvamar
    
    FraProcesandoDTE.Visible = True
    DoEvents
    
    X = FreeFile
    On Error GoTo ErrorCreaLibro
    Sp_Archivo = "c:\facturaelectronica\libros\" & Sm_CompraVenta & "_" & Sm_Mes_Contable & "-" & Sm_Ano_Contable & "_" & Replace(SP_Rut_Activo, ".", "") & ".CSV"
    Open Sp_Archivo For Output As X
    
        
        If Mid(Sm_CompraVenta, 1, 5) = "VENTA" Then
        
            If 1 = 2 Then
            
                'quitaremos los campos que no corresponden
                le_CV = "VENTA"
                Print #X, "TIPO;FOLIO;FECHA;RUT;RAZON_SOCIAL;IMPUESTO_ESPECIFICO;IMPUESTOS;EXENTO;NETO;IVA;TASA_IVA;IVA_USO_COMUN;IVA_NO_RECUPERABLE;IVA_NO_RETENIDO;IVA_RETENIDO;TOTAL;CANTIDAD_BOLETAS;MONTO_ACTIVO_FIJO;TIPO_IMPUESTO;CODIGO_IMPUESTO;TASA_IMPUESTO;MONTO_IMPUESTO"
                With LvLibroVenta
                    For i = 1 To .ListItems.Count
                        If Val(.ListItems(i).SubItems(3)) > 0 And .ListItems(i).SubItems(19) = "NO" And .ListItems(i).SubItems(6) <> "NULO" Then
                            le_Tipo = .ListItems(i).SubItems(3)
                            le_Folio = .ListItems(i).SubItems(4)
                            le_Fecha = Format(.ListItems(i).SubItems(1), "DD/MM/YYYY")
                            le_Fecha = Replace(le_Fecha, "-", "/")
                            le_Rut = Replace(.ListItems(i).SubItems(6), ".", "")
                            le_Rsocial = Replace(.ListItems(i).SubItems(5), "�", "N")
                            le_ImpEspecifico = 0 '.ListItems(i).SubItems(4)
                            le_Impuestos = 0 '.ListItems(i).SubItems(4)
                            le_Exento = Abs(CDbl(.ListItems(i).SubItems(7)))
                            le_Neto = Abs(CDbl(.ListItems(i).SubItems(8)))
                            le_IVA = Abs(CDbl(.ListItems(i).SubItems(9)))
                            'Guardar la tasa en cada documento
                            le_TasaIva = .ListItems(i).SubItems(17)
                            le_IvaUsoComun = 0 '.ListItems(i).SubItems(4)
                            le_IvaNoRecuperable = 0 ' .ListItems(i).SubItems(4)
                            le_IvaNoRetenido = 0 '.ListItems(i).SubItems(4)
                            le_IvaRetenido = Abs(CDbl(.ListItems(i).SubItems(10)))
                            le_Total = Abs(CDbl(.ListItems(i).SubItems(14)))
                            le_CantidadBoletas = 0
                            'Consultar sobre estos campos
                            le_MontoActivoFijo = 0 '.ListItems(i).SubItems(4)
                            'IMPUESTOS ADICIONALES
                            'LIBRO ELECTRONICO DE VENTAS
                            '18 ABRIL 2017
                            'CONSULTAREMOS SI EL DOCUMENTO CONTIENE IMPUESTOS ADICIONALES PARA CARGARLOS ENLAS COLUMNAS CORREPONDIENTES.
                            Sql = "SELECT imp_adicional i_factor,imp_obs i_codigo,i.coi_valor i_valor " & _
                                    "FROM par_impuestos p " & _
                                    "JOIN ven_impuestos i ON p.imp_id=i.imp_id " & _
                                    "WHERE i.id=" & CDbl(.ListItems(i).SubItems(32))
                            Consulta RsTmp, Sql
                            le_CodigoImpuesto = "" ' .ListItems(i).SubItems(4)
                            le_TasaImpuesto = "" ' .ListItems(i).SubItems(4)
                            le_MontoImpuesto = "" ' .ListItems(i).SubItems(4)
                            If RsTmp.RecordCount > 0 Then
                                RsTmp.MoveFirst
                                Do While Not RsTmp.EOF
                                    le_CodigoImpuesto = le_CodigoImpuesto & RsTmp!i_codigo & "-"
                                    le_TasaImpuesto = le_TasaImpuesto & RsTmp!i_factor & "-"
                                    le_MontoImpuesto = le_MontoImpuesto & RsTmp!i_valor & "-"
                                    RsTmp.MoveNext
                                Loop
                                le_CodigoImpuesto = Mid(le_CodigoImpuesto, 1, Len(le_CodigoImpuesto) - 1)
                                le_TasaImpuesto = Mid(le_TasaImpuesto, 1, Len(le_TasaImpuesto) - 1)
                                le_MontoImpuesto = Mid(le_MontoImpuesto, 1, Len(le_MontoImpuesto) - 1)
                            End If
                            '****** fin impuestos adicionales
                            Print #X, le_Tipo & ";" & le_Folio & ";" & le_Fecha & ";" & le_Rut & ";" & le_Rsocial & ";" & le_ImpEspecifico & ";" & _
                                le_Impuestos & ";" & le_Exento & ";" & le_Neto & ";" & le_IVA & ";" & le_TasaIva & ";" & le_IvaUsoComun & ";" & _
                                le_IvaNoRecuperable & ";" & le_IvaNoRetenido & ";" & le_IvaRetenido & ";" & le_Total & ";" & le_CantidadBoletas & ";" & le_MontoActivoFijo & ";" & _
                                le_TipoImpuesto & ";" & le_CodigoImpuesto & ";" & le_TasaImpuesto & ";" & le_MontoImpuesto
                        End If
                        
                        
                    Next
                    cont = 0
                    '22 marzo ahora acumulamos solo las boletas
                    For i = 1 To LvResumenVentas.ListItems.Count
                        If LvResumenVentas.ListItems(i).SubItems(10) = "38" Or LvResumenVentas.ListItems(i).SubItems(10) = "35" Then
                            cont = cont + 1
                        
                        
                            le_Tipo = LvResumenVentas.ListItems(i).SubItems(10)
                            le_Folio = cont
                            
                            le_Fecha = Format(UltimoDiaMes(Sm_Periodo & "-01"), "DD/MM/YYYY")
                            le_Fecha = Replace(le_Fecha, "-", "/")
                            le_Rut = ""
                            le_Rsocial = ""
                            le_ImpEspecifico = 0 '.ListItems(i).SubItems(4)
                            le_Impuestos = 0 '.ListItems(i).SubItems(4)
                            le_Exento = Abs(CDbl(LvResumenVentas.ListItems(i).SubItems(2)))
                            le_Neto = 0
                            le_IVA = 0
                            'Guardar la tasa en cada documento
                            le_TasaIva = 0
                            le_IvaUsoComun = 0 '.ListItems(i).SubItems(4)
                            le_IvaNoRecuperable = 0 ' .ListItems(i).SubItems(4)
                            le_IvaNoRetenido = 0 '.ListItems(i).SubItems(4)
                            le_IvaRetenido = 0 ' Abs(CDbl(.ListItems(i).SubItems(10)))
                            le_Total = Abs(CDbl(LvResumenVentas.ListItems(i).SubItems(9)))
                            le_CantidadBoletas = LvResumenVentas.ListItems(i).SubItems(1)
                            'Consultar sobre estos campos
                            le_MontoActivoFijo = 0 '.ListItems(i).SubItems(4)
                            le_TipoImpuesto = "" '.ListItems(i).SubItems(4)
                            le_CodigoImpuesto = "" ' .ListItems(i).SubItems(4)
                            le_TasaImpuesto = "" ' .ListItems(i).SubItems(4)
                            le_MontoImpuesto = "" ' .ListItems(i).SubItems(4)
                            Print #X, le_Tipo & ";" & le_Folio & ";" & le_Fecha & ";" & le_Rut & ";" & le_Rsocial & ";" & le_ImpEspecifico & ";" & _
                                le_Impuestos & ";" & le_Exento & ";" & le_Neto & ";" & le_IVA & ";" & le_TasaIva & ";" & le_IvaUsoComun & ";" & _
                                le_IvaNoRecuperable & ";" & le_IvaNoRetenido & ";" & le_IvaRetenido & ";" & le_Total & ";" & le_CantidadBoletas & ";" & le_MontoActivoFijo & ";" & _
                                le_TipoImpuesto & ";" & le_CodigoImpuesto & ";" & le_TasaImpuesto & ";" & le_MontoImpuesto
                        End If
                        
                        
                    Next
                    
                End With
            End If
        
        
            
            
            
                'quitaremos los campos que no corresponden
                le_CV = "VENTA"
                'Print #X, "TIPO;FOLIO;FECHA;RUT;RAZON_SOCIAL;EXENTO;NETO;IVA;TASA_IVA;IVA_NO_RETENIDO;IVA_RETENIDO;TOTAL;CANTIDAD_BOLETAS;MONTO_ACTIVO_FIJO;TIPO_IMPUESTO;CODIGO_IMPUESTO;TASA_IMPUESTO;MONTO_IMPUESTO"
                Print #X, "TIPO;FOLIO;FECHA;RUT;RAZON_SOCIAL;EXENTO;NETO;IVA;TASA_IVA;IVA_FUERA_DE_PLAZO;IVA_NO_RETENIDO;IVA_RETENIDO;TOTAL;FOLIO_REF;ANULADO;CANTIDAD_BOLETAS;CODIGO_IMPUESTO;TASA_IMPUESTO;MONTO_IMPUESTO"
              '   Print #X, le_Tipo & ";" & le_Folio & ";" & le_Fecha & ";" & le_Rut & ";" & le_Rsocial & ";" & _
                                 le_Exento & ";" & le_Neto & ";" & le_IVA & ";" & le_TasaIva & ";" & le_IvafueraPlazo & ";" & _
                                 le_IvaNoRetenido & ";" & le_IvaRetenido & ";" & le_Total & ";;" & le_CantidadBoletas & ";" & _
                                le_TipoImpuesto & ";" & le_CodigoImpuesto & ";" & le_TasaImpuesto & ";" & le_MontoImpuesto
                With LvLibroVenta
                    For i = 1 To .ListItems.Count
                        If Val(.ListItems(i).SubItems(3)) > 0 And .ListItems(i).SubItems(19) = "NO" And .ListItems(i).SubItems(6) <> "NULO" Then
                            le_Tipo = .ListItems(i).SubItems(3)
                            le_Folio = .ListItems(i).SubItems(4)
                            le_Fecha = Format(.ListItems(i).SubItems(1), "DD/MM/YYYY")
                            le_Fecha = Replace(le_Fecha, "-", "/")
                            le_Rut = Replace(.ListItems(i).SubItems(6), ".", "")
                            le_Rsocial = Replace(.ListItems(i).SubItems(5), "�", "N")
                           ' le_ImpEspecifico = 0 '.ListItems(i).SubItems(4)
                        '    le_Impuestos = 0 '.ListItems(i).SubItems(4)
                            le_IvafueraPlazo = "0"
                            le_Exento = Abs(CDbl(.ListItems(i).SubItems(7)))
                            le_Neto = Abs(CDbl(.ListItems(i).SubItems(8)))
                            le_IVA = Abs(CDbl(.ListItems(i).SubItems(9)))
                            'Guardar la tasa en cada documento
                            le_TasaIva = .ListItems(i).SubItems(17)
                          '  le_IvaUsoComun = 0 '.ListItems(i).SubItems(4)
                            le_IvaNoRecuperable = 0 ' .ListItems(i).SubItems(4)
                            le_IvaNoRetenido = 0 '.ListItems(i).SubItems(4)
                            le_IvaRetenido = Abs(CDbl(.ListItems(i).SubItems(10)))
                            le_Total = Abs(CDbl(.ListItems(i).SubItems(14)))
                            le_CantidadBoletas = 0
                            'Consultar sobre estos campos
                            le_MontoActivoFijo = 0 '.ListItems(i).SubItems(4)
                            le_TipoImpuesto = "" '.ListItems(i).SubItems(4)
                            'IMPUESTOS ADICIONALES
                            'LIBRO ELECTRONICO DE COMPRAS
                            '18 ABRIL 2017
                            'CONSULTAREMOS SI EL DOCUMENTO CONTIENE IMPUESTOS ADICIONALES PARA CARGARLOS ENLAS COLUMNAS CORREPONDIENTES.
                            Sql = "SELECT imp_adicional i_factor,imp_obs i_codigo,i.coi_valor i_valor " & _
                                    "FROM par_impuestos p " & _
                                    "JOIN ven_impuestos i ON p.imp_id=i.imp_id " & _
                                    "WHERE i.id=" & CDbl(.ListItems(i).SubItems(32))
                            Consulta RsTmp, Sql
                            le_CodigoImpuesto = "" ' .ListItems(i).SubItems(4)
                            le_TasaImpuesto = "" ' .ListItems(i).SubItems(4)
                            le_MontoImpuesto = "" ' .ListItems(i).SubItems(4)
                            If RsTmp.RecordCount > 0 Then
                                RsTmp.MoveFirst
                                Do While Not RsTmp.EOF
                                    le_CodigoImpuesto = le_CodigoImpuesto & RsTmp!i_codigo & "-"
                                    le_TasaImpuesto = le_TasaImpuesto & RsTmp!i_factor & "-"
                                    le_MontoImpuesto = le_MontoImpuesto & RsTmp!i_valor & "-"
                                    RsTmp.MoveNext
                                Loop
                                le_CodigoImpuesto = Mid(le_CodigoImpuesto, 1, Len(le_CodigoImpuesto) - 1)
                                le_TasaImpuesto = Mid(le_TasaImpuesto, 1, Len(le_TasaImpuesto) - 1)
                                le_MontoImpuesto = Mid(le_MontoImpuesto, 1, Len(le_MontoImpuesto) - 1)
                            End If
                            '****** fin impuestos adicionales
                            
                            Print #X, le_Tipo & ";" & le_Folio & ";" & le_Fecha & ";" & le_Rut & ";" & le_Rsocial & ";" & _
                                 le_Exento & ";" & le_Neto & ";" & le_IVA & ";" & le_TasaIva & ";" & le_IvafueraPlazo & ";" & _
                                 le_IvaNoRetenido & ";" & le_IvaRetenido & ";" & le_Total & ";;" & le_CantidadBoletas & ";" & _
                                le_TipoImpuesto & ";" & le_CodigoImpuesto & ";" & le_TasaImpuesto & ";" & le_MontoImpuesto
                        End If
                        
                        
                    Next
                    
                    '22 marzo ahora acumulamos solo las boletas
                    For i = 1 To LvResumenVentas.ListItems.Count
                        If LvResumenVentas.ListItems(i).SubItems(10) = "38" Then
                        
                        
                        
                            le_Tipo = "38"
                            le_Folio = "1"
                            
                            le_Fecha = Format(UltimoDiaMes(Sm_Periodo & "-01"), "DD/MM/YYYY")
                            le_Fecha = Replace(le_Fecha, "-", "/")
                            le_Rut = ""
                            le_Rsocial = ""
                          '  le_ImpEspecifico = 0 '.ListItems(i).SubItems(4)
                            'le_Impuestos = 0 '.ListItems(i).SubItems(4)
                            le_Exento = Abs(CDbl(LvResumenVentas.ListItems(i).SubItems(2)))
                            le_Neto = 0
                            le_IVA = 0
                            'Guardar la tasa en cada documento
                            le_TasaIva = 0
                            le_IvaUsoComun = 0 '.ListItems(i).SubItems(4)
                            le_IvaNoRecuperable = 0 ' .ListItems(i).SubItems(4)
                            le_IvaNoRetenido = 0 '.ListItems(i).SubItems(4)
                            le_IvaRetenido = 0 ' Abs(CDbl(.ListItems(i).SubItems(10)))
                            le_Total = Abs(CDbl(LvResumenVentas.ListItems(i).SubItems(9)))
                            le_CantidadBoletas = LvResumenVentas.ListItems(i).SubItems(1)
                            'Consultar sobre estos campos
                            le_MontoActivoFijo = 0 '.ListItems(i).SubItems(4)
                            le_TipoImpuesto = "" '.ListItems(i).SubItems(4)
                            le_CodigoImpuesto = "" ' .ListItems(i).SubItems(4)
                            le_TasaImpuesto = "" ' .ListItems(i).SubItems(4)
                            le_MontoImpuesto = "" ' .ListItems(i).SubItems(4)
                            Print #X, le_Tipo & ";" & le_Folio & ";" & le_Fecha & ";" & le_Rut & ";" & le_Rsocial & ";" & _
                                 le_Exento & ";" & le_Neto & ";" & le_IVA & ";" & le_TasaIva & ";" & le_IvafueraPlazo & ";" & _
                                 le_IvaNoRetenido & ";" & le_IvaRetenido & ";" & le_Total & ";;;" & le_CantidadBoletas & ";" & _
                                le_TipoImpuesto & ";" & le_CodigoImpuesto & ";" & le_TasaImpuesto & ";" & le_MontoImpuesto
                            
                        End If
                        
                        
                    Next
                    
                End With
            
        
        
        
        ElseIf Mid(Sm_CompraVenta, 1, 6) = "COMPRA" Then
            le_CV = "COMPRA"
            ''Print #X, "TIPO;FOLIO;FECHA;RUT;RAZON_SOCIAL;IMPUESTO_ESPECIFICO;IMPUESTOS;EXENTO;NETO;IVA;TASA_IVA;IVA_USO_COMUN;IVA_NO_RECUPERABLE;IVA_NO_RETENIDO;IVA_RETENIDO;TOTAL;MONTO_ACTIVO_FIJO;TIPO_IMPUESTO;CODIGO_IMPUESTO;TASA_IMPUESTO;MONTO_IMPUESTO"
            Print #X, "Tipo Doc;Folio; Rut Prov;Tasa I;Razon Socia;Tipo I;Fecha Emis;Anulado[A];MExento;MNeto;MIvaRec;CodINR;MIVAnoRe;IVAusoCo;COI;TaOI;MOtroImp;MontoTotal;MotrIScr;MActFijo;MivaAfij;IvaNORet;TabacoPu;TabacoCi;TabacoEl;ImpVehic;CodSuSii;NroInter;EmiRecep"
                With Me.LvCompra
                    For i = 1 To .ListItems.Count
                        If Val(.ListItems(i).SubItems(3)) > 0 Then
                        
'                        le_Folio & ";" & le_RutProv & ";" & le_TasaI & ";" & le_RazonSocial & ";" & le_TipoI & ";" & le_FechaEmis & ";" & _
'                                le_Anulado & ";" & le_MExento & ";" & le_MNeto & ";" & le_MIvaRec & ";" & le_CodINR & ";" & le_MIVAnoRe & ";" & _
'                                le_IVAusoCo & ";" & le_COI & ";" & le_TaOI & ";" & le_MOtroImp & ";" & le_MontoTotal & ";" & _
'                                le_MotrIScr & ";" & le_MActFijo & ";" & le_MivaAfij & ";" & le_IvaNORet; ";" & le_TabacoPu & ";" & le_TabacoCi & ";" & _
'                                le_TabacoEl & ";" & le_ImpVehic & ";" & le_CodSuSii & ";" & le_NroInter & "; & le_EmiRecep"
                            le_TipoDoc = .SelectedItem
                            le_Folio = .ListItems(i).SubItems(1)
                            le_RutProv = .ListItems(i).SubItems(2)
                            le_TasaI = .ListItems(i).SubItems(3)
                            le_RazonSocial = .ListItems(i).SubItems(4)
                            le_TipoI = .ListItems(i).SubItems(5)
                            
                            le_FechaEmis = Format(.ListItems(i).SubItems(6), "DD/MM/YYYY")
                            le_FechaEmis = Replace(le_FechaEmis, "-", "/")
                            ''le_FechaEmis = .ListItems(i).SubItems(1)
                            le_Anulado = .ListItems(i).SubItems(7)
                            le_MExento = .ListItems(i).SubItems(8)
                            le_MNeto = .ListItems(i).SubItems(9)
                            le_MIvaRec = .ListItems(i).SubItems(10)
                            le_CodINR = .ListItems(i).SubItems(11)
                            le_MIVAnoRe = .ListItems(i).SubItems(12)
                            le_IVAusoCo = .ListItems(i).SubItems(13)
                            le_COI = .ListItems(i).SubItems(14)
                            le_TaOI = .ListItems(i).SubItems(15)
                            le_MOtroImp = .ListItems(i).SubItems(16)
                            le_MontoTotal = .ListItems(i).SubItems(17)
                            le_MotrIScr = .ListItems(i).SubItems(18)
                            le_MActFijo = .ListItems(i).SubItems(19)
                            le_MivaAfij = .ListItems(i).SubItems(20)
                            le_IvaNORet = .ListItems(i).SubItems(21)
                            le_TabacoPu = .ListItems(i).SubItems(22)
                            le_TabacoCi = .ListItems(i).SubItems(23)
                            le_TabacoEl = .ListItems(i).SubItems(24)
                            le_ImpVehic = .ListItems(i).SubItems(25)
                            le_CodSuSii = .ListItems(i).SubItems(26)
                            le_NroInter = .ListItems(i).SubItems(27)
                            le_EmiRecep = .ListItems(i).SubItems(28)
                            
''                            le_Fecha = Format(.ListItems(i).SubItems(6), "DD/MM/YYYY")
''                            le_Fecha = Replace(le_Fecha, "-", "/")
''                            le_Rut = Replace(.ListItems(i).SubItems(6), ".", "")
''                            le_Rsocial = .ListItems(i).SubItems(5)
''                            le_ImpEspecifico = 0 '.ListItems(i).SubItems(4)
''                            le_Impuestos = 0 '.ListItems(i).SubItems(4)
''                            le_Exento = Abs(CDbl(.ListItems(i).SubItems(7)))
''                            le_Neto = Abs(CDbl(.ListItems(i).SubItems(8)))
''                            le_IVA = Abs(CDbl(.ListItems(i).SubItems(9)))
''                            'Guardar la tasa en cada documento
''                            le_TasaIva = .ListItems(i).SubItems(30)
''                            le_IvaUsoComun = 0 '.ListItems(i).SubItems(4)
''                            le_IvaNoRecuperable = 0 ' .ListItems(i).SubItems(4)
''                            le_IvaNoRetenido = 0 '.ListItems(i).SubItems(4)
''                            le_IvaRetenido = Abs(CDbl(.ListItems(i).SubItems(10)))
''                            le_Total = Abs(CDbl(.ListItems(i).SubItems(26)))
                            'Consultar sobre estos campos
                         '  le_MontoActivoFijo = 0 '.ListItems(i).SubItems(4)
                            le_TipoImpuesto = "" '.ListItems(i).SubItems(4)
                            
                            
                            
                            
''''                            'IMPUESTOS ADICIONALES
''''                            'LIBRO ELECTRONICO DE COMPRAS
''''                            '18 ABRIL 2017
''''                            'CONSULTAREMOS SI EL DOCUMENTO CONTIENE IMPUESTOS ADICIONALES PARA CARGARLOS ENLAS COLUMNAS CORREPONDIENTES.
''''                            Sql = "SELECT imp_adicional i_factor,imp_obs i_codigo,i.coi_valor i_valor " & _
''''                                    "FROM par_impuestos p " & _
''''                                    "JOIN com_impuestos i ON p.imp_id=i.imp_id " & _
''''                                    "WHERE i.id=" & CDbl(.ListItems(i).SubItems(32))
''''                            Consulta RsTmp, Sql
''''                            le_CodigoImpuesto = ""
''''                            le_TasaImpuesto = ""
''''                            le_MontoImpuesto = ""
''''                            If RsTmp.RecordCount > 0 Then
''''                                RsTmp.MoveFirst
''''                                Do While Not RsTmp.EOF
''''                                    le_CodigoImpuesto = le_CodigoImpuesto & RsTmp!i_codigo & "-"
''''                                    le_TasaImpuesto = le_TasaImpuesto & RsTmp!i_factor & "-"
''''                                    le_MontoImpuesto = le_MontoImpuesto & RsTmp!i_valor & "-"
''''                                    RsTmp.MoveNext
''''                                Loop
''''                                le_CodigoImpuesto = Mid(le_CodigoImpuesto, 1, Len(le_CodigoImpuesto) - 1)
''''                                le_TasaImpuesto = Mid(le_TasaImpuesto, 1, Len(le_TasaImpuesto) - 1)
''''                                le_MontoImpuesto = Mid(le_MontoImpuesto, 1, Len(le_MontoImpuesto) - 1)
''''                            End If
''''                            '****** fin impuestos adicionales
''''                            le_Monto_Activo_Fijo = CDbl(.ListItems(i).SubItems(31))
''''                            ''    Dim le_TipoDoc As String


                            Print #X, le_TipoDoc & ";" & le_Folio & ";" & le_RutProv & ";" & le_TasaI & ";" & le_RazonSocial & ";" & le_TipoI & ";" & le_FechaEmis & ";" & _
                                le_Anulado & ";" & le_MExento & ";" & le_MNeto & ";" & le_MIvaRec & ";" & le_CodINR & ";" & le_MIVAnoRe & ";" & _
                                le_IVAusoCo & ";" & le_COI & ";" & le_TaOI & ";" & le_MOtroImp & ";" & le_MontoTotal & ";" & _
                                le_MotrIScr & ";" & le_MActFijo & ";" & le_MivaAfij & ";" & le_IvaNORet; ";" & le_TabacoPu & ";" & le_TabacoCi & ";" & _
                                le_TabacoEl & ";" & le_ImpVehic & ";" & le_CodSuSii & ";" & le_NroInter & ";" & le_EmiRecep
                        End If
                        
                        
                    Next
                End With
        
        
            
        End If
    Close #X
    
    ShellExecute Me.hWnd, "open", Sp_Archivo, "", "", 4
    
''    If MsgBox("Libro generado correctamente " & vbNewLine & Sp_Archivo & vbNewLine & vbNewLine & _
''            vbNewLine & "�Subir ahora?", vbQuestion + vbOKCancel) = vbCancel Then
''            FraProcesandoDTE.Visible = False
''            Exit Sub
''    End If
    
    'Procesamos Libro electronico
 '''''   ProcesaSubidaLibro le_CV, Sm_Periodo, Sp_Archivo, False, ""
    
    FraProcesandoDTE.Visible = False
    
    Exit Sub
    
ErrorCreaLibro:
    MsgBox "Ocurrio un error al generar CSV..." & vbNewLine & Err.Number & vbNewLine & Err.Description, vbExclamation
    FraProcesandoDTE.Visible = False

'''        Sql = "SELECT sii,nro,rut,tasa_iva,rs,IFNULL(imp_extra12, 1),fecha,IFNULL(imp_extra12, ''),exentas,afectas,iva,'','','','','','','',total,'','','','','','','','','','' " & _
'''              "FROM con_libro_compra_temporal "
'''              ''& _
'''              ''"WHERE  pro_estado='En Mantencion'"
'''
'''
'''    Consulta RsTmp, Sql
'''    LLenar_Grilla RsTmp, Me, Me.LvCompra, False, True, True, False
'''
'''
'''        Dim tit(2) As String
'''    FraProgreso.Visible = True
''' ''   tit(0) = "ASIENTO CONTABLE CON NOTA DE CREDITO"
'''
'''''    tit(1) = s_Libro & " " & Sm_Mes_Contable & " " & Sm_Ano_Contable
'''''    tit(2) = "" 'Time
'''    ExportarNuevoMSG LvCompra, tit, Me, PBar
'''    FraProgreso.Visible = False
    
End Sub

Private Sub CmdPrintLibro_Click()
    SG_codigo = Empty
    Sis_SeleccionaImpresion.Show 1
    If SG_codigo = Empty Then Exit Sub
   '� If LvLibro.ListItems.Count = 0 Then
   '     LvLibro.ListItems.Add , , ""
   '     LvLibro.ListItems(1).SubItems(5) = "SIN MOVIMIENTOS"
   ' End If
    For i = 1 To LvLibro.ListItems.Count
        LvLibro.ListItems(i).SubItems(6) = Replace(LvLibro.ListItems(i).SubItems(6), ".", "")
    Next
    If SG_codigo = "matriz" Then PrintMatriz
    If SG_codigo = "laser" Then PrintLaser
    
    
    Unload Me
    
    Exit Sub
ImpresoraError:
    'No imprime na
End Sub
Private Sub PrintMatriz()
    Dim Sp_Texto As String
    Dim Sp_Lh As String
   
    For i = 1 To 141
        Sp_Lh = Sp_Lh & "="
    Next
    On Error GoTo ErrorP
    Sm_File = FreeFile
    Open SG_codigo2 For Output As Sm_File
                
              '  Print #1, Chr(27) + "M"
                Lp_Linea = 0
                Lp_Pagina = 1
                Texenta = 0
                TAfecto = 0
                Tiva = 0
                Tret = 0
                Totros = 0
                Ttotal = 0
                EncabezadoLibro
                
                
                If LvLibro.ListItems.Count = 0 Then
                    Print #Sm_File, " "
                    Print #Sm_File, " "
                    Print #Sm_File, " "
                    Print #Sm_File, " S I N     M O V I M I E N T O S"
                    GoTo FinalArchivo
                End If
                '     LvLibro.ListItems(1).SubItems(5) = "SIN MOVIMIENTOS"
                For i = 1 To LvLibro.ListItems.Count
                    PFolio = LvLibro.ListItems(i)
                    Pfecha = LvLibro.ListItems(i).SubItems(1)
                    Ptd = LvLibro.ListItems(i).SubItems(2)
                    PcodSii = LvLibro.ListItems(i).SubItems(3)
                    RSet PnroDoc = LvLibro.ListItems(i).SubItems(4)
                    Pnombre = LvLibro.ListItems(i).SubItems(5)
                    PRut = LvLibro.ListItems(i).SubItems(6)
                    RSet PExenta = LvLibro.ListItems(i).SubItems(7)
                    RSet PAfecto = LvLibro.ListItems(i).SubItems(8)
                    RSet Piva = LvLibro.ListItems(i).SubItems(9)
                    RSet PRet = LvLibro.ListItems(i).SubItems(10)
                    RSet Potros = LvLibro.ListItems(i).SubItems(11)
                    RSet PTotal = LvLibro.ListItems(i).SubItems(12)
                    
                    Print #Sm_File, Chr$(&H1B); "a"; Chr$(0);
                    Print #Sm_File, Chr$(&H1B); "!"; Chr$(5);
                    
                    If Pfecha = "No Docto" Then
                        Print #Sm_File, Chr$(&H1B); "!"; Chr$(13);
                    End If
                    
                    If Trim(Pnombre) = "TOTALES" Then Print #Sm_File, Sp_Lh
                    Print #Sm_File, PFolio & " " & Pfecha & " " & Ptd & " " & PcodSii & " " & _
                    PnroDoc & " " & Pnombre & " " & PRut & " " & PExenta & " " & PAfecto & " " & Piva & _
                    " " & PRet & " " & Potros & " " & PTotal
                    If Trim(Pnombre) = "TOTALES" Then Print #Sm_File, Sp_Lh
                    If Pfecha <> "No Docto" And Trim(Pnombre) <> "TOTALES" And Len(Trim(Pnombre)) > 0 Then
                    
                        Texenta = Texenta + CDbl(PExenta)
                        TAfecto = TAfecto + CDbl(PAfecto)
                        Tiva = Tiva + CDbl(Piva)
                        Tret = Tret + CDbl(PRet)
                        Totros = Totros + CDbl(Potros)
                        Ttotal = Ttotal + CDbl(PTotal)
                    End If
                    
                    Lp_Linea = Lp_Linea + 1
                    
                    
                    If Lp_Linea = 45 Then
                     '   If Trim(Pnombre) <> "TOTALES" And Pfecha <> "No Docto" Then
                            'totalizar pagina
                            PFolio = ""
                            Pfecha = ""
                            Ptd = ""
                            PcodSii = ""
                            PnroDoc = ""
                            Pnombre = "HOJA SIGUIENTE"
                            PRut = "======>"
                            RSet PExenta = NumFormat(Texenta)
                            RSet PAfecto = NumFormat(TAfecto)
                            RSet Piva = NumFormat(Tiva)
                            RSet PRet = NumFormat(Tret)
                            RSet Potros = NumFormat(Totros)
                            RSet PTotal = NumFormat(Ttotal)
                            Print #Sm_File, Chr$(&H1B); "!"; Chr$(13);
                            Print #Sm_File, PFolio & " " & Pfecha & " " & Ptd & " " & PcodSii & " " & _
                            PnroDoc & " " & Pnombre & " " & PRut & " " & PExenta & " " & PAfecto & " " & Piva & _
                            " " & PRet & " " & Potros & " " & PTotal
                            
                           
                    '    End If
                        Lp_Linea = 0
                        Lp_Pagina = Lp_Pagina + 1
                        Print #Sm_File, Chr(12) 'SALTO DE PAGINA
                        
                        EncabezadoLibro
                    End If
                Next
FinalArchivo:
                Print #Sm_File, Chr(12) 'AVANCE DE PAGINA FINAL"
    Close #Sm_File
    
    
    
    Exit Sub
ErrorP:
    MsgBox "Problema al imprimir..." & vbNewLine & Err.Number & vbNewLine & Err.Description & vbNewLine & Err.Source
    Close #Sm_File
End Sub
Private Sub EncabezadoLibro()
    Dim Sp_Lh As String, Sp_Tit As String
    For i = 1 To 141
        Sp_Lh = Sp_Lh & "-"
    Next
    If Me.Sm_TipoLibro = "BORRADOR" Then ' Encabezado libro borrador
        Print #Sm_File, Chr$(&H1B); "!"; Chr$(12);
        Print #Sm_File, Chr$(&H1B); "a"; Chr$(0);
        Sp_Tit = Left(LvDetalle.ListItems(1) & Space(90), 90) & " " & Left(" Pagina:" & Lp_Pagina & Space(18), 18)
        Print #Sm_File, Sp_Tit
        Sp_Tit = Left(LvDetalle.ListItems(2) & Space(90), 90) & " " & Left(" Fecha :" & Date & Space(18), 18)
        Print #Sm_File, Sp_Tit
        For i = 3 To LvDetalle.ListItems.Count
            Print #Sm_File, LvDetalle.ListItems(i)
        Next
    Else
        For i = 1 To 10 'Espacios para libro legal
            Print #Sm_File, ""
        Next
    End If
    
    Print #Sm_File, Chr$(&H1B); "a"; Chr$(1);
    Print #Sm_File, Chr$(&H1B); "!"; Chr$(20);
    Print #Sm_File, SkLibro 'TITULO DEL LIBRO
    Print #Sm_File, Chr$(&H1B); "!"; Chr$(5);
    Print #Sm_File, Chr$(&H1B); "a"; Chr$(0);
    
    Print #Sm_File, Sp_Lh
    Print #Sm_File, "            COD                     COMPRAS CON FACTURAS     "
    
    PFolio = "FOLIO"
    Pfecha = "FECHA"
    Ptd = "TD"
    PcodSii = "SII"
    PnroDoc = "NRO DOCUM."
    Pnombre = "RAZON ROCIAL"
    PRut = "R.U.T."
    RSet PExenta = "EXENTAS"
    RSet PAfecto = "NETO"
    RSet Piva = "I.V.A."
    RSet PRet = "RET. I.V.A"
    RSet Potros = "OTROS IMP."
    RSet PTotal = "TOTAL"
    
    
    
    Print #Sm_File, PFolio & " " & Pfecha & " " & Ptd & " " & PcodSii & " " & _
                    PnroDoc & " " & Pnombre & " " & PRut & " " & PExenta & " " & PAfecto & " " & Piva & _
                    " " & PRet & " " & Potros & " " & PTotal
    Print #Sm_File, Sp_Lh
    If Ttotal > 0 Then
        PFolio = ""
        Pfecha = ""
        Ptd = ""
        PcodSii = ""
        PnroDoc = ""
        Pnombre = "DE HOJA ANTERIOR"
        PRut = "======>"
        RSet PExenta = NumFormat(Texenta)
        RSet PAfecto = NumFormat(TAfecto)
        RSet Piva = NumFormat(Tiva)
        RSet PRet = NumFormat(Tret)
        RSet Potros = NumFormat(Totros)
        RSet PTotal = NumFormat(Ttotal)
        Print #Sm_File, Chr$(&H1B); "!"; Chr$(13);
        Print #Sm_File, PFolio & " " & Pfecha & " " & Ptd & " " & PcodSii & " " & _
        PnroDoc & " " & Pnombre & " " & PRut & " " & PExenta & " " & PAfecto & " " & Piva & _
        " " & PRet & " " & Potros & " " & PTotal

       ' Texenta = 0
       ' TAfecto = 0
       ' Tiva = 0
       ' Tret = 0
       ' Totros = 0
       ' Ttotal = 0
        Lp_Linea = Lp_Linea + 1
    End If
   
End Sub
Private Sub PrintLaser()
    Dim Sp_Texto As String
    Dim Sp_Lh As String
    Super_Impresora Sm_ImpresoraSeleccionada
    ' Printer.Orientation = vbPRORLandscape
    
    If SG_BaseDato = "db_marcelapino" Then
        Printer.Orientation = 1
    Else
        Printer.Orientation = 1
    End If
    
    
    Printer.ScaleMode = vbCentimeters
    Printer.DrawMode = 1
    Dim CantCopias As Integer
    On Error GoTo CancelaImp
    Dialogo.CancelError = True
  '  Dialogo.ShowPrinter
    CantCopias = Dialogo.Copies

    For i = 1 To 147
        Sp_Lh = Sp_Lh & "="
    Next
    On Error GoTo ErrorP
    'Sm_File = FreeFile
    'Open App.Path & "\xxx.txt" For Output As Sm_File
                Lp_Linea = 0
                Lp_Pagina = 1
                Texenta = 0
                TAfecto = 0
                Tiva = 0
                Tret = 0
                Totros = 0
                Ttotal = 0
                Cx = 0
                Cy = 1
                EncabezadoLibroLaser
                If Mid(Sm_CompraVenta, 1, 6) = "COMPRA" Then
                    If LvLibro.ListItems.Count = 0 Then
                        Printer.Print " "
                        Printer.Print " "
                        Printer.Print " "
                        Printer.FontSize = 16
                        Printer.Print " "; " S I N     M O V I M I E N T O S"
                        GoTo FinalArchivo
                    End If
                Else
                    If LvLibroVenta.ListItems.Count = 0 Then
                        Printer.Print " "
                        Printer.Print " "
                        Printer.Print " "
                        Printer.FontSize = 16
                        Printer.Print " "; " S I N     M O V I M I E N T O S"
                        GoTo FinalArchivo
                    End If
                
                
                End If
                Printer.FontName = "Arial Narrow"
                
                Printer.FontSize = 10
                
                If Mid(Sm_CompraVenta, 1, 6) = "COMPRA" Then
                        For i = 1 To LvLibro.ListItems.Count
                            PFolio = LvLibro.ListItems(i)
                            If Mid(Sm_CompraVenta, 1, 6) = "COMPRA" Then
                                Pfecha = LvLibro.ListItems(i).SubItems(1)
                                Ptd = LvLibro.ListItems(i).SubItems(2)
                                PcodSii = LvLibro.ListItems(i).SubItems(3)
                                RSet PnroDoc = LvLibro.ListItems(i).SubItems(4)
                                PnombreL = LvLibro.ListItems(i).SubItems(5)
                                RSet PRut = LvLibro.ListItems(i).SubItems(6)
                                RSet PExenta = LvLibro.ListItems(i).SubItems(7)
                                RSet PAfecto = LvLibro.ListItems(i).SubItems(8)
                                RSet Piva = LvLibro.ListItems(i).SubItems(9)
                                RSet PRet = LvLibro.ListItems(i).SubItems(10)
                            Else
                                Pfecha = LvLibroVenta.ListItems(i).SubItems(1)
                                Ptd = LvLibroVenta.ListItems(i).SubItems(2)
                                PcodSii = LvLibroVenta.ListItems(i).SubItems(3)
                                RSet PnroDoc = LvLibroVenta.ListItems(i).SubItems(4)
                                PnombreL = LvLibroVenta.ListItems(i).SubItems(5)
                                RSet PRut = LvLibroVenta.ListItems(i).SubItems(6)
                                RSet PExenta = LvLibroVenta.ListItems(i).SubItems(7)
                                RSet PAfecto = LvLibroVenta.ListItems(i).SubItems(8)
                                RSet Piva = LvLibroVenta.ListItems(i).SubItems(9)
                                RSet PRet = LvLibroVenta.ListItems(i).SubItems(10)
                            
                            
                            End If
                                
                                
                                
                                
                            'RSet Potros = LvLibro.ListItems(i).SubItems(11)
                            If Mid(Sm_CompraVenta, 1, 6) = "COMPRA" Then
                                For Y = 11 To 25
                                    RSet Sm_ColImp(Y) = LvLibro.ListItems(i).SubItems(Y)
                                Next
                                
                                
                                RSet PTotal = LvLibro.ListItems(i).SubItems(26)
                            Else
                                ' VENTA
                                 RSet PTotal = LvLibro.ListItems(i).SubItems(14)
                                
                                
                            End If
                            
                            If Pfecha = "No Docto" Then
                                'coloca cx,cy,
                               ' Print #Sm_File, Chr$(&H1B); "!"; Chr$(13);
                            End If
                            
                            If Trim(PnombreL) = "TOTALES" Then
                                Cy = Cy + Dp
                                'Printer.Print Sp_Lh
                                Coloca Cx, Cy, Sp_Lh, False, 8
                            End If
                            Cy = Cy + Dp
                            If Pfecha = "No Docto" Or Trim(PnombreL) = "TOTALES" Then
                              '  Printer.Print PFolio & " " & Pfecha & " " & Ptd & " " & PcodSii & " " & _
                                PnroDoc & " " & PnombreL & " " & PRut & " " & PExenta & " " & PAfecto & " " & Piva & _
                                " " & PRet & " " & Potros & " " & PTotal
                           '         Coloca Cx, Cy, PFolio, True, 8
                                    If SG_BaseDato = "db_marcelapino" Then
                                        EnviaLineasMarcelaPino True
                                    Else
                                        EnviaLineas True
                                    End If
                                    
                            
                             '   Coloca Cx, Cy, PFolio & " " & Pfecha & " " & Ptd & " " & PcodSii & " " & _
                                PnroDoc & " " & PnombreL & " " & PRut & " " & PExenta & " " & PAfecto & " " & Piva & _
                                " " & PRet & " " & Potros & " " & PTotal, True, 8
                            Else
                              '  Printer.Print PFolio & " " & Pfecha & " " & Ptd & " " & PcodSii & " " & _
                                 PnroDoc & " " & PnombreL & " " & PRut & " " & PExenta & " " & PAfecto & " " & Piva & _
                                    " " & PRet & " " & Potros & " " & PTotal
                                
                             '  For p = 1 To LvLibro.ColumnHeaders.Count - 1
                            '         Coloca Cx, Cy, PFolio, False, 8
                                If SG_BaseDato = "db_marcelapino" Then
                                    EnviaLineasMarcelaPino False
                                Else
                                    EnviaLineas False
                                End If
                              ' Next
                                
                           '    Coloca Cx, Cy, PFolio & " " & Pfecha & " " & Ptd & " " & PcodSii & " " & _
                                 PnroDoc & " " & PnombreL & " " & PRut & " " & PExenta & " " & PAfecto & " " & Piva & _
                                    " " & PRet & " " & Potros & " " & PTotal, False, 8
                               
                               
                            End If
                            
                            If Trim(PnombreL) = "TOTALES" Then
                                Cy = Cy + Dp
                               ' Printer.Print Sp_Lh
                                Coloca Cx, Cy, Sp_Lh, False, 8
                            End If
                            If Pfecha <> "No Docto" And Trim(PnombreL) <> "TOTALES" And Len(Trim(PnombreL)) > 0 Then
                            
                                Texenta = Texenta + CDbl(PExenta)
                                TAfecto = TAfecto + CDbl(PAfecto)
                                Tiva = Tiva + CDbl(Piva)
                                Tret = Tret + CDbl(PRet)
                              '  Totros = Totros + CDbl(Potros)
                                Ttotal = Ttotal + CDbl(PTotal)
                                
                                If Mid(Sm_CompraVenta, 1, 6) = "COMPRA" Then
                                    For Y = 11 To 25
                                        Sm_ColImpValor(Y) = Sm_ColImpValor(Y) + CDbl(LvLibro.ListItems(i).SubItems(Y))
                                    Next
                                End If
                                
                            End If
                            
                            Lp_Linea = Lp_Linea + 1
                            
                            
                            If Lp_Linea = 42 And Lp_Linea <> LvLibro.ListItems.Count Then
                             '   If Trim(Pnombre) <> "TOTALES" And Pfecha <> "No Docto" Then
                                    'totalizar pagina
                                    PFolio = ""
                                    Pfecha = ""
                                    Ptd = ""
                                    PcodSii = ""
                                    PnroDoc = ""
                                    PnombreL = "HOJA SIGUIENTE"
                                    PRut = "======>"
                                    RSet PExenta = NumFormat(Texenta)
                                    RSet PAfecto = NumFormat(TAfecto)
                                    RSet Piva = NumFormat(Tiva)
                                    RSet PRet = NumFormat(Tret)
                                    RSet Potros = NumFormat(Totros)
                                    RSet PTotal = NumFormat(Ttotal)
                                    
                                    If Mid(Sm_CompraVenta, 1, 6) = "COMPRA" Then
                                        For Y = 11 To 25
                                            RSet Sm_ColImp(Y) = NumFormat(Sm_ColImpValor(Y))
                                        Next
                                    End If
                                    
                                    
                                    
                                    
                                    
                                    'Print #Sm_File, Chr$(&H1B); "!"; Chr$(13);
                                    Cy = Cy + Dp
                                '    Printer.Print PFolio & " " & Pfecha & " " & Ptd & " " & PcodSii & " " & _
                                    PnroDoc & " " & PnombreL & " " & PRut & " " & PExenta & " " & PAfecto & " " & Piva & _
                                    " " & PRet & " " & Potros & " " & PTotal
                                    If SG_BaseDato = "db_marcelapino" Then
                                        EnviaLineasMarcelaPino False
                                    Else
                                        EnviaLineas False
                                    End If
                                    
                                    'Coloca Cx, Cy, PFolio & " " & Pfecha & " " & Ptd & " " & PcodSii & " " & _
                                    PnroDoc & " " & PnombreL & " " & PRut & " " & PExenta & " " & PAfecto & " " & Piva & _
                                    " " & PRet & " " & Potros & " " & PTotal, True, 8
                                    Lp_Linea = 0
                                    Lp_Pagina = Lp_Pagina + 1
                                    Printer.NewPage
                                   ' Printer.EndDoc
                                    EncabezadoLibroLaser
                            End If
                        Next
                        
                Else
                    'esto es libro de ventas
                    
                    For i = 1 To LvLibroVenta.ListItems.Count
                          '  PFolio = LvLibro.ListItems(i)
                          
                                Pfecha = LvLibroVenta.ListItems(i).SubItems(1)
                                Ptd = LvLibroVenta.ListItems(i).SubItems(2)
                                PcodSii = LvLibroVenta.ListItems(i).SubItems(3)
                                RSet PnroDoc = LvLibroVenta.ListItems(i).SubItems(4)
                                PnombreL = LvLibroVenta.ListItems(i).SubItems(5)
                                RSet PRut = LvLibroVenta.ListItems(i).SubItems(6)
                                RSet PExenta = LvLibroVenta.ListItems(i).SubItems(7)
                                RSet PAfecto = LvLibroVenta.ListItems(i).SubItems(8)
                                RSet Piva = LvLibroVenta.ListItems(i).SubItems(9)
                                RSet PRet = LvLibroVenta.ListItems(i).SubItems(10)
                            
                            
                          
                                
                                
                                
                                
                        
                                ' VENTA
                                 RSet PTotal = LvLibroVenta.ListItems(i).SubItems(14)
                                
                                
                        
                            
                            If Pfecha = "No Docto" Then
                                'coloca cx,cy,
                               ' Print #Sm_File, Chr$(&H1B); "!"; Chr$(13);
                            End If
                            
                            If Trim(PnombreL) = "TOTALES" Then
                                Cy = Cy + Dp
                                'Printer.Print Sp_Lh
                                Coloca Cx, Cy, Sp_Lh, False, 8
                            End If
                            Cy = Cy + Dp
                            If Pfecha = "No Docto" Or Trim(PnombreL) = "TOTALES" Then
                              '  Printer.Print PFolio & " " & Pfecha & " " & Ptd & " " & PcodSii & " " & _
                                PnroDoc & " " & PnombreL & " " & PRut & " " & PExenta & " " & PAfecto & " " & Piva & _
                                " " & PRet & " " & Potros & " " & PTotal
                           '         Coloca Cx, Cy, PFolio, True, 8
                                    EnviaLineas True
                                    
                            
                             '   Coloca Cx, Cy, PFolio & " " & Pfecha & " " & Ptd & " " & PcodSii & " " & _
                                PnroDoc & " " & PnombreL & " " & PRut & " " & PExenta & " " & PAfecto & " " & Piva & _
                                " " & PRet & " " & Potros & " " & PTotal, True, 8
                            Else
                              '  Printer.Print PFolio & " " & Pfecha & " " & Ptd & " " & PcodSii & " " & _
                                 PnroDoc & " " & PnombreL & " " & PRut & " " & PExenta & " " & PAfecto & " " & Piva & _
                                    " " & PRet & " " & Potros & " " & PTotal
                                
                             '  For p = 1 To LvLibro.ColumnHeaders.Count - 1
                            '         Coloca Cx, Cy, PFolio, False, 8
                                  
                                    EnviaLineas False
                                    
                              ' Next
                                
                           '    Coloca Cx, Cy, PFolio & " " & Pfecha & " " & Ptd & " " & PcodSii & " " & _
                                 PnroDoc & " " & PnombreL & " " & PRut & " " & PExenta & " " & PAfecto & " " & Piva & _
                                    " " & PRet & " " & Potros & " " & PTotal, False, 8
                               
                               
                            End If
                            
                            If Trim(PnombreL) = "TOTALES" Then
                                Cy = Cy + Dp
                               ' Printer.Print Sp_Lh
                                Coloca Cx, Cy, Sp_Lh, False, 8
                            End If
                            If Pfecha <> "No Docto" And Trim(PnombreL) <> "TOTALES" And Len(Trim(PnombreL)) > 0 Then
                            
                                Texenta = Texenta + CDbl(PExenta)
                                TAfecto = TAfecto + CDbl(PAfecto)
                                Tiva = Tiva + CDbl(Piva)
                                Tret = Tret + CDbl(PRet)
                              '  Totros = Totros + CDbl(Potros)
                                Ttotal = Ttotal + CDbl(PTotal)
                            End If
                            
                            Lp_Linea = Lp_Linea + 1
                            
                            
                            If Lp_Linea = 42 And Lp_Linea <> Me.LvLibroVenta.ListItems.Count Then
                             '   If Trim(Pnombre) <> "TOTALES" And Pfecha <> "No Docto" Then
                                    'totalizar pagina
                                    PFolio = ""
                                    Pfecha = ""
                                    Ptd = ""
                                    PcodSii = ""
                                    PnroDoc = ""
                                    PnombreL = "HOJA SIGUIENTE"
                                    PRut = "======>"
                                    RSet PExenta = NumFormat(Texenta)
                                    RSet PAfecto = NumFormat(TAfecto)
                                    RSet Piva = NumFormat(Tiva)
                                    RSet PRet = NumFormat(Tret)
                                    RSet Potros = NumFormat(Totros)
                                    RSet PTotal = NumFormat(Ttotal)
                                    'Print #Sm_File, Chr$(&H1B); "!"; Chr$(13);
                                    Cy = Cy + Dp
                                '    Printer.Print PFolio & " " & Pfecha & " " & Ptd & " " & PcodSii & " " & _
                                    PnroDoc & " " & PnombreL & " " & PRut & " " & PExenta & " " & PAfecto & " " & Piva & _
                                    " " & PRet & " " & Potros & " " & PTotal
                                    EnviaLineas False
                                    
                                    'Coloca Cx, Cy, PFolio & " " & Pfecha & " " & Ptd & " " & PcodSii & " " & _
                                    PnroDoc & " " & PnombreL & " " & PRut & " " & PExenta & " " & PAfecto & " " & Piva & _
                                    " " & PRet & " " & Potros & " " & PTotal, True, 8
                                    Lp_Linea = 0
                                    Lp_Pagina = Lp_Pagina + 1
                                    Printer.NewPage
                                   ' Printer.EndDoc
                                    EncabezadoLibroLaser
                            End If
                        Next
                    
                    
                    
                
                End If
FinalArchivo:
                Printer.NewPage
                Printer.EndDoc
                'AVANCE DE PAGINA FINAL"
    Exit Sub
ErrorP:
    MsgBox "Problema al imprimir..." & vbNewLine & Err.Number & vbNewLine & Err.Description & vbNewLine & Err.Source
    Exit Sub
CancelaImp:
    'Cancelo impresion
End Sub
Private Function EnviaLineas(Negrita As Boolean)
        Dim avance As Double
        avance = 1.5
        RSet Ptd = Ptd
        Printer.FontSize = 10
        Printer.FontName = "Arial Narrow"
        Coloca Cx, Cy, PFolio, Negrita, 8
        Coloca Cx + 1.1, Cy, Pfecha, Negrita, 8
        Coloca Cx + 2.1, Cy, Ptd, Negrita, 8
        Coloca (Cx + 4) - Printer.TextWidth(PnroDoc), Cy, PnroDoc, Negrita, 8
        Coloca Cx + 4.2, Cy, PnombreL, Negrita, 8
        Coloca (Cx + 8.4) - Printer.TextWidth(PRut), Cy, PRut, Negrita, 8
        Coloca (Cx + 9.8) - Printer.TextWidth(PExenta), Cy, PExenta, Negrita, 8
        Coloca (Cx + 11.3) - Printer.TextWidth(PAfecto), Cy, PAfecto, Negrita, 8
        Coloca (Cx + 12.8) - Printer.TextWidth(Piva), Cy, Piva, Negrita, 8
        Coloca (Cx + 14.1) - Printer.TextWidth(PRet), Cy, PRet, Negrita, 8
        'Coloca (Cx + 14.8) - Printer.TextWidth(Potros), Cy, Potros, Negrita, 8
        If Mid(Sm_CompraVenta, 1, 6) = "COMPRA" Then
            For Y = 11 To 25
                If LvLibro.ColumnHeaders(Y + 1).Width > 0 Then
                    Coloca (Cx + (15.3) + avance) - Printer.TextWidth(Sm_ColImp(Y)), Cy, Sm_ColImp(Y), Negrita, 8
                    avance = avance + 1.5
                End If
                'RSet Sm_ColImp(Y) = LvLibro.ListItems(i).SubItems(Y)
            Next
        End If
        
        
        Coloca (Cx + 15.5 + avance) - Printer.TextWidth(PTotal), Cy, PTotal, Negrita, 8
End Function

Private Function EnviaLineasMarcelaPino(Negrita As Boolean)
        Dim avance As Double
        avance = 1.5
        RSet Ptd = Ptd
        Printer.FontSize = 10
        Printer.FontName = "Arial Narrow"
        Coloca Cx, Cy, PFolio, Negrita, 8
        Coloca Cx + 1.1, Cy, Pfecha, Negrita, 8
        Coloca Cx + 2.1, Cy, Ptd, Negrita, 8
        Coloca (Cx + 4) - Printer.TextWidth(PnroDoc), Cy, PnroDoc, Negrita, 8
        Coloca Cx + 4.2, Cy, PnombreL, Negrita, 8
        Coloca (Cx + 8.4) - Printer.TextWidth(PRut), Cy, PRut, Negrita, 8
        Coloca (Cx + 9.8) - Printer.TextWidth(PExenta), Cy, PExenta, Negrita, 8
        Coloca (Cx + 11.3) - Printer.TextWidth(PAfecto), Cy, PAfecto, Negrita, 8
        Coloca (Cx + 12.8) - Printer.TextWidth(Piva), Cy, Piva, Negrita, 8
        'Coloca (Cx + 14.1) - Printer.TextWidth(PRet), Cy, PRet, Negrita, 8
        'Coloca (Cx + 14.8) - Printer.TextWidth(Potros), Cy, Potros, Negrita, 8
        If Mid(Sm_CompraVenta, 1, 6) = "COMPRA" Then
            For Y = 11 To 25
                If LvLibro.ColumnHeaders(Y + 1).Width > 0 Then
                    Coloca (Cx + (13.3) + avance) - Printer.TextWidth(Sm_ColImp(Y)), Cy, Sm_ColImp(Y), Negrita, 8
                    avance = avance + 1.5
                End If
                'RSet Sm_ColImp(Y) = LvLibro.ListItems(i).SubItems(Y)
            Next
        End If
        
        
        Coloca (Cx + 13.3 + avance) - Printer.TextWidth(PTotal), Cy, PTotal, Negrita, 8
End Function


Private Sub EncabezadoLibroLaser()
    Dim Sp_Lh As String, Sp_Tit As String
    Dim Sp_EncabezadosExtras(26) As String
    
    For i = 1 To 120
        Sp_Lh = Sp_Lh & "-"
    Next
    Printer.FontName = "Arial Narrow"
    Cy = 2
    Dp = 0.35
    Cx = 0
    Cy = Dp * 10
    
    If Sm_TipoLibro = "BORRADOR" Then
        Printer.FontName = "Courier New"
        
        Cy = 2
       
        Sp_Tit = Left(LvDetalle.ListItems(1) & Space(90), 90) & " " & Left(" Pagina:" & Lp_Pagina & Space(18), 18)
        Coloca Cx, Cy, Sp_Tit, False, 8
        
        Sp_Tit = Left(LvDetalle.ListItems(2) & Space(90), 90) & " " & Left(" Fecha :" & Date & Space(18), 18)
        Cy = Cy + Dp
        Coloca Cx, Cy, Sp_Tit, False, 8
        Cy = Cy + Dp
        For i = 3 To LvDetalle.ListItems.Count
            
            Coloca Cx, Cy, LvDetalle.ListItems(i), False, 8
            Cy = Cy + Dp
        Next
        Cy = Cy + Dp
        Coloca Cx, Cy, "                " & SkLibro, True, 12 ' Titulo
        
    Else
        Printer.FontName = "Courier New"
        Cy = 2
        Cy = Cy + (Dp * 10)
        Coloca Cx, Cy, "                       " & SkLibro, True, 12 ' Titulo
    End If
    
    Cy = Cy + Dp
     Printer.FontName = "Courier New"
    Coloca Cx, Cy, Sp_Lh, False, 8
  '  Cy = Cy + Dp
  '  Coloca Cx, Cy, "            COD                     COMPRAS CON FACTURAS     ", False, 8
    
    PFolio = "Folio"
    Pfecha = "Fecha"
    Ptd = "TD"
    PcodSii = "SII"
    PnroDoc = "Nro Docum."
    PnombreL = "Razon Social"
    RSet PRut = "R.U.T."
    RSet PExenta = "Exentas"
    RSet PAfecto = "Neto"
    RSet Piva = "I.V.A."
    RSet PRet = "Ret. I.V.A"
    RSet Potros = "Otros Imp."
    RSet PTotal = "Total"
    
    If Mid(Sm_CompraVenta, 1, 6) = "COMPRA" Then
        For E = 11 To 22
            If LvLibro.ColumnHeaders(E + 1).Width > 0 Then
                RSet Sm_ColImp(E) = LvLibro.ColumnHeaders(E + 1).Text
            End If
         'zzzzzzzzzzzzz
        '      If LvLibro.ColumnHeaders(e + 1).Width > 0 Then
        '        Sm_ColImpValor(e) = 0
        '    End If
        Next
    End If
    
    
    Cy = Cy + Dp
    Printer.FontName = "Arial Narrow"
    
    If SG_BaseDato = "db_marcelapino" Then
        EnviaLineasMarcelaPino False
    Else
        EnviaLineas False
    End If
   
    'Coloca Cx, Cy, PFolio & " " & Pfecha & " " & Ptd & " " & PcodSii & " " & _
                    PnroDoc & " " & PnombreL & " " & PRut & " " & PExenta & " " & PAfecto & " " & Piva & _
                    " " & PRet & " " & Potros & " " & PTotal, False, 8
    Cy = Cy + Dp
    Printer.FontName = "Courier New"
    Coloca Cx, Cy, Sp_Lh, False, 8
    
    If Ttotal > 0 Then
        PFolio = ""
        Pfecha = ""
        Ptd = ""
        PcodSii = ""
        PnroDoc = ""
        PnombreL = "DE HOJA ANTERIOR"
        PRut = "======>"
        RSet PExenta = NumFormat(Texenta)
        RSet PAfecto = NumFormat(TAfecto)
        RSet Piva = NumFormat(Tiva)
        RSet PRet = NumFormat(Tret)
        RSet Potros = NumFormat(Totros)
        RSet PTotal = NumFormat(Ttotal)
        
        If Mid(Sm_CompraVenta, 1, 6) = "COMPRA" Then
            For Y = 11 To 25
                RSet Sm_ColImp(Y) = NumFormat(Sm_ColImpValor(Y))
            Next
        End If
                                    
        
        Cy = Cy + Dp
        
        If SG_BaseDato = "db_marcelapino" Then
             EnviaLineasMarcelaPino True
         Else
             EnviaLineas True
         End If
        
        
        'Coloca Cx, Cy, PFolio & " " & Pfecha & " " & Ptd & " " & PcodSii & " " & _
                    PnroDoc & " " & Pnombre & " " & PRut & " " & PExenta & " " & PAfecto & " " & Piva & _
                    " " & PRet & " " & Potros & " " & PTotal, True, 8
        
       ' Texenta = 0
       ' TAfecto = 0
       ' Tiva = 0
       ' Tret = 0
       ' Totros = 0
       ' Ttotal = 0
        Lp_Linea = Lp_Linea + 1
    End If
   
End Sub
Private Sub ResumenLaser()
    Dim Sp_Texto As String
    Dim Sp_Lh As String
    Dim Sp_Cetras As String
   On Error GoTo ErrorP
    Super_Impresora Sm_ImpresoraSeleccionada
    
    Printer.Orientation = vbPRORLandscape
    Printer.ScaleMode = vbCentimeters
    Cx = 2
    Cy = 1
    For i = 1 To 147
        Sp_Lh = Sp_Lh & "="
    Next
    
   
                Lp_Linea = 0
                Lp_Pagina = 1
                Texenta = 0
                TAfecto = 0
                Tiva = 0
                Tret = 0
                Totros = 0
                Ttotal = 0
                ResumenEncabezadoLaser
                If Mid(Sm_CompraVenta, 1, 6) = "COMPRA" Then 'RESUMEN LIBRO DE COMRAS
                        For i = 1 To LVResumen.ListItems.Count
                            PDocs = LVResumen.ListItems(i)
                            Pfecha = LVResumen.ListItems(i).SubItems(1)
                            RSet PExenta = LVResumen.ListItems(i).SubItems(2)
                            RSet PAfecto = LVResumen.ListItems(i).SubItems(3)
                            RSet Piva = LVResumen.ListItems(i).SubItems(4)
                            RSet PRet = LVResumen.ListItems(i).SubItems(5)
                            RSet Potros = LVResumen.ListItems(i).SubItems(6)
                            RSet PTotal = LVResumen.ListItems(i).SubItems(21)
                            Sp_Cetras = ""
                            For g = 8 To 19
                                If LVResumen.ColumnHeaders(g + 1).Width > 0 Then
                                    RSet Sm_ColImp(g) = LVResumen.ListItems(i).SubItems(g)
                                    Sp_Cetras = Sp_Cetras & " " & Sm_ColImp(g)
                                End If
                            Next
                            
                            
                            If Trim(PDocs) = "TOTALES" Then
                                Cy = Cy + Dp
                                Coloca Cx, Cy, Sp_Lh, False, 8
                            End If
                            Cy = Cy + Dp
                            
                            If Pfecha = "No Docto" Or Trim(PnombreL) = "TOTALES" Then
                                'Coloca Cx, Cy, PDocs & " " & Pfecha & " " & " " & PExenta & " " & PAfecto & " " & Piva & _
                                " " & PRet & " " & Potros & " " & PTotal, True, 8
                                Coloca Cx, Cy, PDocs & " " & Pfecha & " " & " " & PExenta & " " & PAfecto & " " & Piva & _
                                " " & PRet & " " & Sp_Cetras & " " & PTotal, True, 8
                                
                            Else
                                'Coloca Cx, Cy, PDocs & " " & Pfecha & " " & " " & PExenta & " " & PAfecto & " " & Piva & _
                            " " & PRet & " " & Potros & " " & PTotal, False, 8
                                Coloca Cx, Cy, PDocs & " " & Pfecha & " " & " " & PExenta & " " & PAfecto & " " & Piva & _
                                " " & PRet & " " & Sp_Cetras & " " & PTotal, False, 8
                               
                               
                            End If
                            
                            If Trim(PDocs) = "TOTALES" Then
                                Cy = Cy + Dp
                                Coloca Cx, Cy, Sp_Lh, False, 8
                            End If
                            
                            
                        Next
                Else 'RESUMEN VENTAS
                        For i = 1 To LvResumenVentas.ListItems.Count
                            PDocs = LvResumenVentas.ListItems(i)
                            Pfecha = LvResumenVentas.ListItems(i).SubItems(1)
                            RSet PExenta = LvResumenVentas.ListItems(i).SubItems(2)
                            RSet PAfecto = LvResumenVentas.ListItems(i).SubItems(3)
                            RSet Piva = LvResumenVentas.ListItems(i).SubItems(4)
                            RSet PRet = LvResumenVentas.ListItems(i).SubItems(5)
                            RSet Potros = LvResumenVentas.ListItems(i).SubItems(6)
                            RSet PTotal = LvResumenVentas.ListItems(i).SubItems(9)
                            Sp_Cetras = ""
                           ' For g = 8 To 19
                           '     If LVResumen.ColumnHeaders(g + 1).Width > 0 Then
                           '         RSet Sm_ColImp(g) = LVResumen.ListItems(i).SubItems(g)
                           '         Sp_Cetras = Sp_Cetras & " " & Sm_ColImp(g)
                           '     End If
                           ' Next
                            
                            
                            If Trim(PDocs) = "TOTALES" Then
                                Cy = Cy + Dp
                                Coloca Cx, Cy, Sp_Lh, False, 8
                            End If
                            Cy = Cy + Dp
                            
                            If Pfecha = "No Docto" Or Trim(PnombreL) = "TOTALES" Then
                                'Coloca Cx, Cy, PDocs & " " & Pfecha & " " & " " & PExenta & " " & PAfecto & " " & Piva & _
                                " " & PRet & " " & Potros & " " & PTotal, True, 8
                                Coloca Cx, Cy, PDocs & " " & Pfecha & " " & " " & PExenta & " " & PAfecto & " " & Piva & _
                                " " & PRet & " " & PTotal, True, 8
                                
                            Else
                                'Coloca Cx, Cy, PDocs & " " & Pfecha & " " & " " & PExenta & " " & PAfecto & " " & Piva & _
                            " " & PRet & " " & Potros & " " & PTotal, False, 8
                                Coloca Cx, Cy, PDocs & " " & Pfecha & " " & " " & PExenta & " " & PAfecto & " " & Piva & _
                                " " & PRet & " " & PTotal, False, 8
                               
                               
                            End If
                            
                            If Trim(PDocs) = "TOTALES" Then
                                Cy = Cy + Dp
                                Coloca Cx, Cy, Sp_Lh, False, 8
                            End If
                            
                            
                        Next
                
                End If
                Printer.NewPage
                Printer.EndDoc
                'AVANCE DE PAGINA FINAL"
    Exit Sub
ErrorP:
    MsgBox "Problema al imprimir..." & vbNewLine & Err.Number & vbNewLine & Err.Description & vbNewLine & Err.Source
  

End Sub
Private Sub ResumenEncabezadoLaser()
    Dim Sp_Lh As String, Sp_Tit As String
    Dim Sp_Cetras As String
    For i = 1 To 147
        Sp_Lh = Sp_Lh & "-"
    Next
    Printer.FontName = "Courier New"
    Cx = 2
    Cy = 1
    Dp = 0.35
    Sp_Tit = Left(LvDetalle.ListItems(1) & Space(90), 90) & " " & Left(" Pagina:" & Lp_Pagina & Space(18), 18)
    Coloca Cx, Cy, Sp_Tit, False, 8
    
    Sp_Tit = Left(LvDetalle.ListItems(2) & Space(90), 90) & " " & Left(" Fecha :" & Date & Space(18), 18)
    Cy = Cy + Dp
    Coloca Cx, Cy, Sp_Tit, False, 8
    Cy = Cy + Dp
    For i = 3 To LvDetalle.ListItems.Count
        
        Coloca Cx, Cy, LvDetalle.ListItems(i), False, 8
        Cy = Cy + Dp
    Next
    Cy = Cy + Dp
    Coloca Cx, Cy, "                " & "CUADRO RESUMEN " & SkLibro, True, 12 ' Titulo
    Cy = Cy + Dp
    Coloca Cx, Cy, Sp_Lh, False, 8
  '  Cy = Cy + Dp
  '  Coloca Cx, Cy, "            COD                     COMPRAS CON FACTURAS     ", False, 8
    
    PDocs = "DOCUMENTOS"
    Pfecha = "CANTIDAD"
    
    RSet PExenta = "EXENTAS"
    RSet PAfecto = "NETO"
    RSet Piva = "I.V.A."
    RSet PRet = "RET. I.V.A"
    RSet Potros = "OTROS IMP."
    RSet PTotal = "TOTAL"
    Sp_Cetras = ""
    If Mid(Sm_CompraVenta, 1, 6) = "COMPRA" Then
        Sp_Cetras = ""
        For g = 8 To 20
            If LVResumen.ColumnHeaders(g + 1).Width > 0 Then
                RSet Sm_ColImp(g) = LVResumen.ColumnHeaders(g + 1).Text
                Sp_Cetras = Sp_Cetras & " " & Sm_ColImp(g)
            End If
        Next
    End If
    Cy = Cy + Dp
    
    Coloca Cx, Cy, PDocs & " " & Pfecha & " " & PExenta & " " & PAfecto & " " & Piva & _
                    " " & PRet & " " & Sp_Cetras & " " & PTotal, False, 8
    Cy = Cy + Dp
    Coloca Cx, Cy, Sp_Lh, False, 8
    
End Sub



Private Sub MatrizResumen()

    Dim Sp_Texto As String
    Dim Sp_Lh As String
   
    For i = 1 To 141
        Sp_Lh = Sp_Lh & "="
    Next
    On Error GoTo ErrorP
    Sm_File = FreeFile
    Open SG_codigo2 For Output As Sm_File
                Lp_Linea = 0
                Lp_Pagina = 1
                Texenta = 0
                TAfecto = 0
                Tiva = 0
                Tret = 0
                Totros = 0
                Ttotal = 0
                EncabezadoResumenMatriz
                For i = 1 To LVResumen.ListItems.Count
                    'PFolio = LVResumen.ListItems(i)
                    PDocs = LVResumen.ListItems(i)
                    Pfecha = LVResumen.ListItems(i).SubItems(1) 'Cantidad
                   'Ptd = LVResumen.ListItems(i).SubItems(2)
                    'PcodSii = LVResumen.ListItems(i).SubItems(3)
                    'PnroDoc = LVResumen.ListItems(i).SubItems(4)
                    'Pnombre = LVResumen.ListItems(i).SubItems(5)
                    'PRut = LVResumen.ListItems(i).SubItems(6)
                    RSet PExenta = LVResumen.ListItems(i).SubItems(2)
                    RSet PAfecto = LVResumen.ListItems(i).SubItems(3)
                    RSet Piva = LVResumen.ListItems(i).SubItems(4)
                    RSet PRet = LVResumen.ListItems(i).SubItems(5)
                    RSet Potros = LVResumen.ListItems(i).SubItems(6)
                    RSet PTotal = LVResumen.ListItems(i).SubItems(7)
                    
                    Print #Sm_File, Chr$(&H1B); "a"; Chr$(0);
                    Print #Sm_File, Chr$(&H1B); "!"; Chr$(5);
                    
                    If Trim(PDocs) = "TOTALES" Then
                        Print #Sm_File, Chr$(&H1B); "!"; Chr$(13);
                    End If
                    
                    If Trim(PDocs) = "TOTALES" Then Print #Sm_File, Sp_Lh
                    Print #Sm_File, PDocs & " " & Pfecha & " " & PExenta & " " & PAfecto & " " & Piva & _
                    " " & PRet & " " & Potros & " " & PTotal
                    If Trim(PDocs) = "TOTALES" Then Print #Sm_File, Sp_Lh
                    
                 
                Next
                
                Print #Sm_File, Chr(12) 'AVANCE DE PAGINA FINAL"
    Close #Sm_File
    
    
    
    Exit Sub
ErrorP:
    MsgBox "Problema al imprimir..." & vbNewLine & Err.Number & vbNewLine & Err.Description & vbNewLine & Err.Source
    Close #Sm_File



End Sub
Private Sub EncabezadoResumenMatriz()
    Dim Sp_Lh As String, Sp_Tit As String
    For i = 1 To 141
        Sp_Lh = Sp_Lh & "-"
    Next
    Print #Sm_File, Chr$(&H1B); "!"; Chr$(12);
    Print #Sm_File, Chr$(&H1B); "a"; Chr$(0);
    Sp_Tit = Left(LvDetalle.ListItems(1) & Space(90), 90) & " " & Left(" Pagina:" & Lp_Pagina & Space(18), 18)
    Print #Sm_File, Sp_Tit
    Sp_Tit = Left(LvDetalle.ListItems(2) & Space(90), 90) & " " & Left(" Fecha :" & Date & Space(18), 18)
    Print #Sm_File, Sp_Tit
    For i = 3 To LvDetalle.ListItems.Count
        Print #Sm_File, LvDetalle.ListItems(i)
    Next
    Print #Sm_File, Chr$(&H1B); "a"; Chr$(1);
    Print #Sm_File, Chr$(&H1B); "!"; Chr$(20);
    Print #Sm_File, "CUADRO RESUMEN DE " & SkLibro 'TITULO DEL LIBRO
    Print #Sm_File, Chr$(&H1B); "!"; Chr$(5);
    Print #Sm_File, Chr$(&H1B); "a"; Chr$(0);
    
    Print #Sm_File, Sp_Lh
    'Print #Sm_File, "            COD                     COMPRAS CON FACTURAS     "
    PDocs = "DOCUMENTO"
    'PFolio = "FOLIO"
    Pfecha = "CANTIDAD"
    'Ptd = "TD"
    'PcodSii = "SII"
    'PnroDoc = "NRO DOCUM."
    'Pnombre = "RAZON ROCIAL"
    'PRut = "R.U.T."
    RSet PExenta = "EXENTAS"
    RSet PAfecto = "NETO"
    RSet Piva = "I.V.A."
    RSet PRet = "RET. I.V.A"
    RSet Potros = "OTROS IMP."
    RSet PTotal = "TOTAL"
    Print #Sm_File, PDocs & " " & Pfecha & " " & PExenta & " " & PAfecto & " " & Piva & _
                    " " & PRet & " " & Potros & " " & PTotal
    Print #Sm_File, Sp_Lh
   
   
End Sub









Private Sub CmdPrintResumen_Click()
    SG_codigo = Empty
    Sis_SeleccionaImpresion.Show 1
    If SG_codigo = Empty Then Exit Sub
    If SG_codigo = "matriz" Then MatrizResumen
    If SG_codigo = "laser" Then ResumenLaser
    
    Exit Sub
ImpresoraError:
    'No imprime na
End Sub

Private Sub CmdRectificar_Click()
 Dim Sp_Archivo As String
    Dim le_Tipo As String
    Dim le_Folio As String
    Dim le_Fecha As String
    Dim le_Rut As String
    Dim le_Rsocial As String
    Dim le_ImpEspecifico As String
    Dim le_Impuestos As String
    Dim le_Exento As String
    Dim le_Neto As String
    Dim le_IVA As String
    Dim le_TasaIva As String
    Dim le_IvaUsoComun As String
    Dim le_IvaNoRecuperable As String
    Dim le_IvaNoRetenido As String
    Dim le_IvaRetenido As String
    Dim le_Total As String
    Dim le_MontoActivoFijo As String
    Dim le_TipoImpuesto As String
    Dim le_CodigoImpuesto As String
    Dim le_TasaImpuesto As String
    Dim le_MontoImpuesto As String
    Dim le_CV As String
    Dim le_CantidadBoletas As String
    Dim le_IvafueraPlazo As String
    Dim le_Monto_Activo_Fijo As String
'TIPO;FOLIO;FECHA;RUT;RAZON_SOCIAL;IMPUESTO_ESPECIFICO;IMPUESTOS;EXENTO;NETO;IVA;TASA_IVA"
';IVA_USO_COMUN;IVA_NO_RECUPERABLE;IVA_NO_RETENIDO;IVA_RETENIDO;TOTAL;MONTO_ACTIVO_FIJO;
'TIPO_IMPUESTO;CODIGO_IMPUESTO;TASA_IMPUESTO;MONTO_IMPUESTO"


    '17 Septiembre 2018
    'Subir libro electronico
    'Autor: alvamar
    
    FraProcesandoDTE.Visible = True
    DoEvents
    
    X = FreeFile
    On Error GoTo ErrorCreaLibro
    Sp_Archivo = "c:\facturaelectronica\libros\" & Sm_CompraVenta & "_" & Sm_Mes_Contable & "-" & Sm_Ano_Contable & "_" & Replace(SP_Rut_Activo, ".", "") & "_rec.CSV"
    Open Sp_Archivo For Output As X
    
        
        If Mid(Sm_CompraVenta, 1, 5) = "VENTA" Then
        
            
            
            
                'quitaremos los campos que no corresponden
                le_CV = "VENTA"
                'Print #X, "TIPO;FOLIO;FECHA;RUT;RAZON_SOCIAL;EXENTO;NETO;IVA;TASA_IVA;IVA_NO_RETENIDO;IVA_RETENIDO;TOTAL;CANTIDAD_BOLETAS;MONTO_ACTIVO_FIJO;TIPO_IMPUESTO;CODIGO_IMPUESTO;TASA_IMPUESTO;MONTO_IMPUESTO"
                Print #X, "TIPO;FOLIO;FECHA;RUT;RAZON_SOCIAL;EXENTO;NETO;IVA;TASA_IVA;IVA_FUERA_DE_PLAZO;IVA_NO_RETENIDO;IVA_RETENIDO;TOTAL;FOLIO_REF;ANULADO;CANTIDAD_BOLETAS;CODIGO_IMPUESTO;TASA_IMPUESTO;MONTO_IMPUESTO"
                With LvLibroVenta
                    For i = 1 To .ListItems.Count
                        If Val(.ListItems(i).SubItems(3)) > 0 And .ListItems(i).SubItems(19) = "NO" And .ListItems(i).SubItems(6) <> "NULO" Then
                            le_Tipo = .ListItems(i).SubItems(3)
                            le_Folio = .ListItems(i).SubItems(4)
                            le_Fecha = Format(.ListItems(i).SubItems(1), "DD/MM/YYYY")
                            le_Fecha = Replace(le_Fecha, "-", "/")
                            le_Rut = Replace(.ListItems(i).SubItems(6), ".", "")
                            le_Rsocial = Replace(.ListItems(i).SubItems(5), "�", "N")
                           ' le_ImpEspecifico = 0 '.ListItems(i).SubItems(4)
                        '    le_Impuestos = 0 '.ListItems(i).SubItems(4)
                            le_IvafueraPlazo = "0"
                            le_Exento = Abs(CDbl(.ListItems(i).SubItems(7)))
                            le_Neto = Abs(CDbl(.ListItems(i).SubItems(8)))
                            le_IVA = Abs(CDbl(.ListItems(i).SubItems(9)))
                            'Guardar la tasa en cada documento
                            le_TasaIva = .ListItems(i).SubItems(17)
                          '  le_IvaUsoComun = 0 '.ListItems(i).SubItems(4)
                            le_IvaNoRecuperable = 0 ' .ListItems(i).SubItems(4)
                            le_IvaNoRetenido = 0 '.ListItems(i).SubItems(4)
                            le_IvaRetenido = Abs(CDbl(.ListItems(i).SubItems(10)))
                            le_Total = Abs(CDbl(.ListItems(i).SubItems(14)))
                            le_CantidadBoletas = 0
                            'Consultar sobre estos campos
                            le_MontoActivoFijo = 0 '.ListItems(i).SubItems(4)
                            le_TipoImpuesto = "" '.ListItems(i).SubItems(4)
                           'IMPUESTOS ADICIONALES
                            'LIBRO ELECTRONICO DE VENTAS
                            '18 ABRIL 2017
                            'CONSULTAREMOS SI EL DOCUMENTO CONTIENE IMPUESTOS ADICIONALES PARA CARGARLOS ENLAS COLUMNAS CORREPONDIENTES.
                            Sql = "SELECT imp_adicional i_factor,imp_obs i_codigo,i.coi_valor i_valor " & _
                                    "FROM par_impuestos p " & _
                                    "JOIN ven_impuestos i ON p.imp_id=i.imp_id " & _
                                    "WHERE i.id=" & CDbl(.ListItems(i).SubItems(32))
                            Consulta RsTmp, Sql
                            le_CodigoImpuesto = "" ' .ListItems(i).SubItems(4)
                            le_TasaImpuesto = "" ' .ListItems(i).SubItems(4)
                            le_MontoImpuesto = "" ' .ListItems(i).SubItems(4)
                            If RsTmp.RecordCount > 0 Then
                                RsTmp.MoveFirst
                                Do While Not RsTmp.EOF
                                    le_CodigoImpuesto = le_CodigoImpuesto & RsTmp!i_codigo & "-"
                                    le_TasaImpuesto = le_TasaImpuesto & RsTmp!i_factor & "-"
                                    le_MontoImpuesto = le_MontoImpuesto & RsTmp!i_valor & "-"
                                    RsTmp.MoveNext
                                Loop
                                le_CodigoImpuesto = Mid(le_CodigoImpuesto, 1, Len(le_CodigoImpuesto) - 1)
                                le_TasaImpuesto = Mid(le_TasaImpuesto, 1, Len(le_TasaImpuesto) - 1)
                                le_MontoImpuesto = Mid(le_MontoImpuesto, 1, Len(le_MontoImpuesto) - 1)
                            End If
                            '****** fin impuestos adicionales
                            Print #X, le_Tipo & ";" & le_Folio & ";" & le_Fecha & ";" & le_Rut & ";" & le_Rsocial & ";" & _
                                 le_Exento & ";" & le_Neto & ";" & le_IVA & ";" & le_TasaIva & ";" & le_IvafueraPlazo & ";" & _
                                 le_IvaNoRetenido & ";" & le_IvaRetenido & ";" & le_Total & ";;" & le_CantidadBoletas & ";" & _
                                le_TipoImpuesto & ";" & le_CodigoImpuesto & ";" & le_TasaImpuesto & ";" & le_MontoImpuesto
                        End If
                        
                        
                    Next
                    
                    '22 marzo ahora acumulamos solo las boletas
                    cont = 0
                    For i = 1 To LvResumenVentas.ListItems.Count
                        If LvResumenVentas.ListItems(i).SubItems(10) = "38" Or LvResumenVentas.ListItems(i).SubItems(10) = "35" Then
                        
                        
                            cont = cont + 1
                            le_Tipo = LvResumenVentas.ListItems(i).SubItems(10)
                            le_Folio = cont
                            
                            le_Fecha = Format(UltimoDiaMes(Sm_Periodo & "-01"), "DD/MM/YYYY")
                            le_Fecha = Replace(le_Fecha, "-", "/")
                            le_Rut = ""
                            le_Rsocial = ""
                          '  le_ImpEspecifico = 0 '.ListItems(i).SubItems(4)
                            'le_Impuestos = 0 '.ListItems(i).SubItems(4)
                            le_Exento = Abs(CDbl(LvResumenVentas.ListItems(i).SubItems(2)))
                            le_Neto = 0
                            le_IVA = 0
                            'Guardar la tasa en cada documento
                            le_TasaIva = 0
                            le_IvaUsoComun = 0 '.ListItems(i).SubItems(4)
                            le_IvaNoRecuperable = 0 ' .ListItems(i).SubItems(4)
                            le_IvaNoRetenido = 0 '.ListItems(i).SubItems(4)
                            le_IvaRetenido = 0 ' Abs(CDbl(.ListItems(i).SubItems(10)))
                            le_Total = Abs(CDbl(LvResumenVentas.ListItems(i).SubItems(9)))
                            le_CantidadBoletas = LvResumenVentas.ListItems(i).SubItems(1)
                            'Consultar sobre estos campos
                            le_MontoActivoFijo = 0 '.ListItems(i).SubItems(4)
                            le_TipoImpuesto = "" '.ListItems(i).SubItems(4)
                            le_CodigoImpuesto = "" ' .ListItems(i).SubItems(4)
                            le_TasaImpuesto = "" ' .ListItems(i).SubItems(4)
                            le_MontoImpuesto = "" ' .ListItems(i).SubItems(4)
                            Print #X, le_Tipo & ";" & le_Folio & ";" & le_Fecha & ";" & le_Rut & ";" & le_Rsocial & ";" & _
                                 le_Exento & ";" & le_Neto & ";" & le_IVA & ";" & le_TasaIva & ";" & le_IvafueraPlazo & ";" & _
                                 le_IvaNoRetenido & ";" & le_IvaRetenido & ";" & le_Total & ";;;" & le_CantidadBoletas & ";" & _
                                le_TipoImpuesto & ";" & le_CodigoImpuesto & ";" & le_TasaImpuesto & ";" & le_MontoImpuesto
                            
                        End If
                        
                        
                    Next
                    
                End With
            
        
        
        
        ElseIf Mid(Sm_CompraVenta, 1, 6) = "COMPRA" Then
            le_CV = "COMPRA"
            Print #X, "TIPO;FOLIO;FECHA;RUT;RAZON_SOCIAL;IMPUESTO_ESPECIFICO;IMPUESTOS;EXENTO;NETO;IVA;TASA_IVA;IVA_USO_COMUN;IVA_NO_RECUPERABLE;IVA_NO_RETENIDO;IVA_RETENIDO;TOTAL;MONTO_ACTIVO_FIJO;TIPO_IMPUESTO;CODIGO_IMPUESTO;TASA_IMPUESTO;MONTO_IMPUESTO"
                With LvLibro
                    For i = 1 To .ListItems.Count
                        If Val(.ListItems(i).SubItems(3)) > 0 Then
                            le_Tipo = .ListItems(i).SubItems(3)
                            le_Folio = .ListItems(i).SubItems(4)
                            le_Fecha = Format(.ListItems(i).SubItems(1), "DD/MM/YYYY")
                            le_Fecha = Replace(le_Fecha, "-", "/")
                            le_Rut = Replace(.ListItems(i).SubItems(6), ".", "")
                            le_Rsocial = .ListItems(i).SubItems(5)
                            le_ImpEspecifico = 0 '.ListItems(i).SubItems(4)
                            le_Impuestos = 0 '.ListItems(i).SubItems(4)
                            le_Exento = Abs(CDbl(.ListItems(i).SubItems(7)))
                            le_Neto = Abs(CDbl(.ListItems(i).SubItems(8)))
                            le_IVA = Abs(CDbl(.ListItems(i).SubItems(9)))
                            'Guardar la tasa en cada documento
                            le_TasaIva = .ListItems(i).SubItems(30)
                            le_IvaUsoComun = 0 '.ListItems(i).SubItems(4)
                            le_IvaNoRecuperable = 0 ' .ListItems(i).SubItems(4)
                            le_IvaNoRetenido = 0 '.ListItems(i).SubItems(4)
                            le_IvaRetenido = Abs(CDbl(.ListItems(i).SubItems(10)))
                            le_Total = Abs(CDbl(.ListItems(i).SubItems(26)))
                            'Consultar sobre estos campos
                         '  le_MontoActivoFijo = 0 '.ListItems(i).SubItems(4)
                            le_TipoImpuesto = "" '.ListItems(i).SubItems(4)
                            'IMPUESTOS ADICIONALES
                            'LIBRO ELECTRONICO DE COMPRAS
                            '18 ABRIL 2017
                            'CONSULTAREMOS SI EL DOCUMENTO CONTIENE IMPUESTOS ADICIONALES PARA CARGARLOS ENLAS COLUMNAS CORREPONDIENTES.
                            Sql = "SELECT imp_adicional i_factor,imp_obs i_codigo,i.coi_valor i_valor " & _
                                    "FROM par_impuestos p " & _
                                    "JOIN com_impuestos i ON p.imp_id=i.imp_id " & _
                                    "WHERE i.id=" & CDbl(.ListItems(i).SubItems(32))
                            Consulta RsTmp, Sql
                            le_CodigoImpuesto = "" ' .ListItems(i).SubItems(4)
                            le_TasaImpuesto = "" ' .ListItems(i).SubItems(4)
                            le_MontoImpuesto = "" ' .ListItems(i).SubItems(4)
                            If RsTmp.RecordCount > 0 Then
                                RsTmp.MoveFirst
                                Do While Not RsTmp.EOF
                                    le_CodigoImpuesto = le_CodigoImpuesto & RsTmp!i_codigo & "-"
                                    le_TasaImpuesto = le_TasaImpuesto & RsTmp!i_factor & "-"
                                    le_MontoImpuesto = le_MontoImpuesto & RsTmp!i_valor & "-"
                                    RsTmp.MoveNext
                                Loop
                                le_CodigoImpuesto = Mid(le_CodigoImpuesto, 1, Len(le_CodigoImpuesto) - 1)
                                le_TasaImpuesto = Mid(le_TasaImpuesto, 1, Len(le_TasaImpuesto) - 1)
                                le_MontoImpuesto = Mid(le_MontoImpuesto, 1, Len(le_MontoImpuesto) - 1)
                            End If
                            '****** fin impuestos adicionales
                            le_Monto_Activo_Fijo = CDbl(.ListItems(i).SubItems(31))
                            Print #X, le_Tipo & ";" & le_Folio & ";" & le_Fecha & ";" & le_Rut & ";" & le_Rsocial & ";" & le_ImpEspecifico & ";" & _
                                le_Impuestos & ";" & le_Exento & ";" & le_Neto & ";" & le_IVA & ";" & le_TasaIva & ";" & le_IvaUsoComun & ";" & _
                                le_IvaNoRecuperable & ";" & le_IvaNoRetenido & ";" & le_IvaRetenido & ";" & le_Total & ";" & le_Monto_Activo_Fijo & ";" & _
                                le_TipoImpuesto & ";" & le_CodigoImpuesto & ";" & le_TasaImpuesto & ";" & le_MontoImpuesto
                        End If
                        
                        
                    Next
                End With
        
        
            
        End If
    Close #X
    ShellExecute Me.hWnd, "open", Sp_Archivo, "", "", 4
    
    If MsgBox("Libro generado correctamente " & vbNewLine & Sp_Archivo & vbNewLine & vbNewLine & _
            vbNewLine & "�Subir ahora?", vbQuestion + vbOKCancel) = vbCancel Then
            FraProcesandoDTE.Visible = False
            Exit Sub
    End If
    
    'Procesamos Libro electronico
    Dim Scodigo As String
    Scodigo = InputBox("Digite el codigo de autorizacion de reemplazo", "Codigo de Autorizacion")
    If Len(Scodigo) = 0 Then
        MsgBox "Codigo de autorizacion no valido!!!"
        Exit Sub
    End If
    
    ProcesaSubidaLibro le_CV, Sm_Periodo, Sp_Archivo, True, UCase(Scodigo)
    
    FraProcesandoDTE.Visible = False
    
    Exit Sub
    
ErrorCreaLibro:
    MsgBox "Ocurrio un error al generar libro electronico..." & vbNewLine & Err.Number & vbNewLine & Err.Description, vbExclamation
    FraProcesandoDTE.Visible = False
        
End Sub


Private Sub CmdSubirLibroElectronico_Click()
 Dim Sp_Archivo As String
    Dim le_Tipo As String
    Dim le_Folio As String
    Dim le_Fecha As String
    Dim le_Rut As String
    Dim le_Rsocial As String
    Dim le_ImpEspecifico As String
    Dim le_Impuestos As String
    Dim le_Exento As String
    Dim le_Neto As String
    Dim le_IVA As String
    Dim le_TasaIva As String
    Dim le_IvaUsoComun As String
    Dim le_IvaNoRecuperable As String
    Dim le_IvaNoRetenido As String
    Dim le_IvaRetenido As String
    Dim le_Total As String
    Dim le_MontoActivoFijo As String
    Dim le_TipoImpuesto As String
    Dim le_CodigoImpuesto As String
    Dim le_TasaImpuesto As String
    Dim le_MontoImpuesto As String
    Dim le_CV As String
    Dim le_CantidadBoletas As String
    Dim le_IvafueraPlazo As String
    Dim le_Monto_Activo_Fijo As String
'TIPO;FOLIO;FECHA;RUT;RAZON_SOCIAL;IMPUESTO_ESPECIFICO;IMPUESTOS;EXENTO;NETO;IVA;TASA_IVA"
';IVA_USO_COMUN;IVA_NO_RECUPERABLE;IVA_NO_RETENIDO;IVA_RETENIDO;TOTAL;MONTO_ACTIVO_FIJO;
'TIPO_IMPUESTO;CODIGO_IMPUESTO;TASA_IMPUESTO;MONTO_IMPUESTO"


    '17 Septiembre 2018
    'Subir libro electronico
    'Autor: alvamar
    
    FraProcesandoDTE.Visible = True
    DoEvents
    
    X = FreeFile
    On Error GoTo ErrorCreaLibro
    Sp_Archivo = "c:\facturaelectronica\libros\" & Sm_CompraVenta & "_" & Sm_Mes_Contable & "-" & Sm_Ano_Contable & "_" & Replace(SP_Rut_Activo, ".", "") & "_rec.CSV"
    Open Sp_Archivo For Output As X
    
        
        If Mid(Sm_CompraVenta, 1, 5) = "VENTA" Then
        
            
            
            
                'quitaremos los campos que no corresponden
                le_CV = "VENTA"
                'Print #X, "TIPO;FOLIO;FECHA;RUT;RAZON_SOCIAL;EXENTO;NETO;IVA;TASA_IVA;IVA_NO_RETENIDO;IVA_RETENIDO;TOTAL;CANTIDAD_BOLETAS;MONTO_ACTIVO_FIJO;TIPO_IMPUESTO;CODIGO_IMPUESTO;TASA_IMPUESTO;MONTO_IMPUESTO"
                Print #X, "TIPO;FOLIO;FECHA;RUT;RAZON_SOCIAL;EXENTO;NETO;IVA;TASA_IVA;IVA_FUERA_DE_PLAZO;IVA_NO_RETENIDO;IVA_RETENIDO;TOTAL;FOLIO_REF;ANULADO;CANTIDAD_BOLETAS;CODIGO_IMPUESTO;TASA_IMPUESTO;MONTO_IMPUESTO"
                With LvLibroVenta
                    For i = 1 To .ListItems.Count
                        If Val(.ListItems(i).SubItems(3)) > 0 And .ListItems(i).SubItems(19) = "NO" And .ListItems(i).SubItems(6) <> "NULO" Then
                            le_Tipo = .ListItems(i).SubItems(3)
                            le_Folio = .ListItems(i).SubItems(4)
                            le_Fecha = Format(.ListItems(i).SubItems(1), "DD/MM/YYYY")
                            le_Fecha = Replace(le_Fecha, "-", "/")
                            le_Rut = Replace(.ListItems(i).SubItems(6), ".", "")
                            le_Rsocial = Replace(.ListItems(i).SubItems(5), "�", "N")
                           ' le_ImpEspecifico = 0 '.ListItems(i).SubItems(4)
                        '    le_Impuestos = 0 '.ListItems(i).SubItems(4)
                            le_IvafueraPlazo = "0"
                            le_Exento = Abs(CDbl(.ListItems(i).SubItems(7)))
                            le_Neto = Abs(CDbl(.ListItems(i).SubItems(8)))
                            le_IVA = Abs(CDbl(.ListItems(i).SubItems(9)))
                            'Guardar la tasa en cada documento
                            le_TasaIva = .ListItems(i).SubItems(17)
                          '  le_IvaUsoComun = 0 '.ListItems(i).SubItems(4)
                            le_IvaNoRecuperable = 0 ' .ListItems(i).SubItems(4)
                            le_IvaNoRetenido = 0 '.ListItems(i).SubItems(4)
                            le_IvaRetenido = Abs(CDbl(.ListItems(i).SubItems(10)))
                            le_Total = Abs(CDbl(.ListItems(i).SubItems(14)))
                            le_CantidadBoletas = 0
                            'Consultar sobre estos campos
                            le_MontoActivoFijo = 0 '.ListItems(i).SubItems(4)
                            le_TipoImpuesto = "" '.ListItems(i).SubItems(4)
                           'IMPUESTOS ADICIONALES
                            'LIBRO ELECTRONICO DE VENTAS
                            '18 ABRIL 2017
                            'CONSULTAREMOS SI EL DOCUMENTO CONTIENE IMPUESTOS ADICIONALES PARA CARGARLOS ENLAS COLUMNAS CORREPONDIENTES.
                            Sql = "SELECT imp_adicional i_factor,imp_obs i_codigo,i.coi_valor i_valor " & _
                                    "FROM par_impuestos p " & _
                                    "JOIN ven_impuestos i ON p.imp_id=i.imp_id " & _
                                    "WHERE i.id=" & CDbl(.ListItems(i).SubItems(32))
                            Consulta RsTmp, Sql
                            le_CodigoImpuesto = "" ' .ListItems(i).SubItems(4)
                            le_TasaImpuesto = "" ' .ListItems(i).SubItems(4)
                            le_MontoImpuesto = "" ' .ListItems(i).SubItems(4)
                            If RsTmp.RecordCount > 0 Then
                                RsTmp.MoveFirst
                                Do While Not RsTmp.EOF
                                    le_CodigoImpuesto = le_CodigoImpuesto & Val("" & RsTmp!i_codigo) & "-"
                                    le_TasaImpuesto = le_TasaImpuesto & Val("" & RsTmp!i_factor) & "-"
                                    le_MontoImpuesto = le_MontoImpuesto & Val("" & RsTmp!i_valor) & "-"
                                    RsTmp.MoveNext
                                Loop
                                le_CodigoImpuesto = Mid(le_CodigoImpuesto, 1, Len(le_CodigoImpuesto) - 1)
                                le_TasaImpuesto = Mid(le_TasaImpuesto, 1, Len(le_TasaImpuesto) - 1)
                                le_MontoImpuesto = Mid(le_MontoImpuesto, 1, Len(le_MontoImpuesto) - 1)
                            End If
                            '****** fin impuestos adicionales
                            Print #X, le_Tipo & ";" & le_Folio & ";" & le_Fecha & ";" & le_Rut & ";" & le_Rsocial & ";" & _
                                 le_Exento & ";" & le_Neto & ";" & le_IVA & ";" & le_TasaIva & ";" & le_IvafueraPlazo & ";" & _
                                 le_IvaNoRetenido & ";" & le_IvaRetenido & ";" & le_Total & ";;" & le_CantidadBoletas & ";" & _
                                le_TipoImpuesto & ";" & le_CodigoImpuesto & ";" & le_TasaImpuesto & ";" & le_MontoImpuesto
                        End If
                        
                        
                    Next
                    
                    '22 marzo ahora acumulamos solo las boletas
                    cont = 0
                    For i = 1 To LvResumenVentas.ListItems.Count
                        If LvResumenVentas.ListItems(i).SubItems(10) = "38" Or LvResumenVentas.ListItems(i).SubItems(10) = "35" Then
                        
                        
                            cont = cont + 1
                            le_Tipo = LvResumenVentas.ListItems(i).SubItems(10)
                            le_Folio = cont
                            
                            le_Fecha = Format(UltimoDiaMes(Sm_Periodo & "-01"), "DD/MM/YYYY")
                            le_Fecha = Replace(le_Fecha, "-", "/")
                            le_Rut = ""
                            le_Rsocial = ""
                          '  le_ImpEspecifico = 0 '.ListItems(i).SubItems(4)
                            'le_Impuestos = 0 '.ListItems(i).SubItems(4)
                            le_Exento = Abs(CDbl(LvResumenVentas.ListItems(i).SubItems(2)))
                            le_Neto = 0
                            le_IVA = 0
                            'Guardar la tasa en cada documento
                            le_TasaIva = 0
                            le_IvaUsoComun = 0 '.ListItems(i).SubItems(4)
                            le_IvaNoRecuperable = 0 ' .ListItems(i).SubItems(4)
                            le_IvaNoRetenido = 0 '.ListItems(i).SubItems(4)
                            le_IvaRetenido = 0 ' Abs(CDbl(.ListItems(i).SubItems(10)))
                            le_Total = Abs(CDbl(LvResumenVentas.ListItems(i).SubItems(9)))
                            le_CantidadBoletas = LvResumenVentas.ListItems(i).SubItems(1)
                            'Consultar sobre estos campos
                            le_MontoActivoFijo = 0 '.ListItems(i).SubItems(4)
                            le_TipoImpuesto = "" '.ListItems(i).SubItems(4)
                            le_CodigoImpuesto = "" ' .ListItems(i).SubItems(4)
                            le_TasaImpuesto = "" ' .ListItems(i).SubItems(4)
                            le_MontoImpuesto = "" ' .ListItems(i).SubItems(4)
                            Print #X, le_Tipo & ";" & le_Folio & ";" & le_Fecha & ";" & le_Rut & ";" & le_Rsocial & ";" & _
                                 le_Exento & ";" & le_Neto & ";" & le_IVA & ";" & le_TasaIva & ";" & le_IvafueraPlazo & ";" & _
                                 le_IvaNoRetenido & ";" & le_IvaRetenido & ";" & le_Total & ";;;" & le_CantidadBoletas & ";" & _
                                le_TipoImpuesto & ";" & le_CodigoImpuesto & ";" & le_TasaImpuesto & ";" & le_MontoImpuesto
                            
                        End If
                        
                        
                    Next
                    
                End With
            
        
        
        
        ElseIf Mid(Sm_CompraVenta, 1, 6) = "COMPRA" Then
            le_CV = "COMPRA"
            Print #X, "TIPO;FOLIO;FECHA;RUT;RAZON_SOCIAL;IMPUESTO_ESPECIFICO;IMPUESTOS;EXENTO;NETO;IVA;TASA_IVA;IVA_USO_COMUN;IVA_NO_RECUPERABLE;IVA_NO_RETENIDO;IVA_RETENIDO;TOTAL;MONTO_ACTIVO_FIJO;TIPO_IMPUESTO;CODIGO_IMPUESTO;TASA_IMPUESTO;MONTO_IMPUESTO"
                With LvLibro
                    For i = 1 To .ListItems.Count
                        If Val(.ListItems(i).SubItems(3)) > 0 Then
                            le_Tipo = .ListItems(i).SubItems(3)
                            le_Folio = .ListItems(i).SubItems(4)
                            le_Fecha = Format(.ListItems(i).SubItems(1), "DD/MM/YYYY")
                            le_Fecha = Replace(le_Fecha, "-", "/")
                            le_Rut = Replace(.ListItems(i).SubItems(6), ".", "")
                            le_Rsocial = .ListItems(i).SubItems(5)
                            le_ImpEspecifico = 0 '.ListItems(i).SubItems(4)
                            le_Impuestos = 0 '.ListItems(i).SubItems(4)
                            le_Exento = Abs(CDbl(.ListItems(i).SubItems(7)))
                            le_Neto = Abs(CDbl(.ListItems(i).SubItems(8)))
                            le_IVA = Abs(CDbl(.ListItems(i).SubItems(9)))
                            'Guardar la tasa en cada documento
                            le_TasaIva = .ListItems(i).SubItems(30)
                            le_IvaUsoComun = 0 '.ListItems(i).SubItems(4)
                            le_IvaNoRecuperable = 0 ' .ListItems(i).SubItems(4)
                            le_IvaNoRetenido = 0 '.ListItems(i).SubItems(4)
                            le_IvaRetenido = Abs(CDbl(.ListItems(i).SubItems(10)))
                            le_Total = Abs(CDbl(.ListItems(i).SubItems(26)))
                            'Consultar sobre estos campos
                         '  le_MontoActivoFijo = 0 '.ListItems(i).SubItems(4)
                            le_TipoImpuesto = "" '.ListItems(i).SubItems(4)
                            'IMPUESTOS ADICIONALES
                            'LIBRO ELECTRONICO DE COMPRAS
                            '18 ABRIL 2017
                            'CONSULTAREMOS SI EL DOCUMENTO CONTIENE IMPUESTOS ADICIONALES PARA CARGARLOS ENLAS COLUMNAS CORREPONDIENTES.
                            Sql = "SELECT imp_adicional i_factor,imp_obs i_codigo,i.coi_valor i_valor " & _
                                    "FROM par_impuestos p " & _
                                    "JOIN com_impuestos i ON p.imp_id=i.imp_id " & _
                                    "WHERE i.id=" & CDbl(.ListItems(i).SubItems(32))
                            Consulta RsTmp, Sql
                            le_CodigoImpuesto = "" ' .ListItems(i).SubItems(4)
                            le_TasaImpuesto = "" ' .ListItems(i).SubItems(4)
                            le_MontoImpuesto = "" ' .ListItems(i).SubItems(4)
                            If RsTmp.RecordCount > 0 Then
                                RsTmp.MoveFirst
                                Do While Not RsTmp.EOF
                                    le_CodigoImpuesto = le_CodigoImpuesto & RsTmp!i_codigo & "-"
                                    le_TasaImpuesto = le_TasaImpuesto & RsTmp!i_factor & "-"
                                    le_MontoImpuesto = le_MontoImpuesto & RsTmp!i_valor & "-"
                                    RsTmp.MoveNext
                                Loop
                                le_CodigoImpuesto = Mid(le_CodigoImpuesto, 1, Len(le_CodigoImpuesto) - 1)
                                le_TasaImpuesto = Mid(le_TasaImpuesto, 1, Len(le_TasaImpuesto) - 1)
                                le_MontoImpuesto = Mid(le_MontoImpuesto, 1, Len(le_MontoImpuesto) - 1)
                            End If
                            '****** fin impuestos adicionales
                            le_Monto_Activo_Fijo = CDbl(.ListItems(i).SubItems(31))
                            Print #X, le_Tipo & ";" & le_Folio & ";" & le_Fecha & ";" & le_Rut & ";" & le_Rsocial & ";" & le_ImpEspecifico & ";" & _
                                le_Impuestos & ";" & le_Exento & ";" & le_Neto & ";" & le_IVA & ";" & le_TasaIva & ";" & le_IvaUsoComun & ";" & _
                                le_IvaNoRecuperable & ";" & le_IvaNoRetenido & ";" & le_IvaRetenido & ";" & le_Total & ";" & le_Monto_Activo_Fijo & ";" & _
                                le_TipoImpuesto & ";" & le_CodigoImpuesto & ";" & le_TasaImpuesto & ";" & le_MontoImpuesto
                        End If
                        
                        
                    Next
                End With
        
        
            
        End If
    Close #X
    ShellExecute Me.hWnd, "open", Sp_Archivo, "", "", 4
    
    If MsgBox("Libro generado correctamente " & vbNewLine & Sp_Archivo & vbNewLine & vbNewLine & _
            vbNewLine & "�Subir ahora?", vbQuestion + vbOKCancel) = vbCancel Then
            FraProcesandoDTE.Visible = False
            Exit Sub
    End If

    
    ProcesaSubidaLibro le_CV, Sm_Periodo, Sp_Archivo, False, "0"
    
    FraProcesandoDTE.Visible = False
    
    Exit Sub
    
ErrorCreaLibro:
    MsgBox "Ocurrio un error al generar libro electronico..." & vbNewLine & Err.Number & vbNewLine & Err.Description, vbExclamation
    FraProcesandoDTE.Visible = False

End Sub
Private Sub ProcesaSubidaLibro(Tipo As String, Periodo As String, ElArchivo As String, BRectifica As Boolean, Scodigo As String)
        Dim objx As DTECloud.Integracion
        Set objx = New DTECloud.Integracion
        
        objx.UrlServicioFacturacion = SG_Url_Factura_Electronica
        objx.Password = "1234"
        
         On Error GoTo ProcesoLibro
  
        If Not BRectifica Then
                If Mid(Tipo, 1, 5) = "VENTA" Then
                    
                    If objx.ProcesaLVE_FormatoCSV(Replace(SP_Rut_Activo, ".", ""), Periodo, ElArchivo, True, True, True) Then
                        MsgBox objx.DescripcionResultado
                    Else
                        MsgBox objx.DescripcionResultado
                    End If
                
                ElseIf Mid(Tipo, 1, 6) = "COMPRA" Then
                    
                    If objx.ProcesaLCE_FormatoCSV(Replace(SP_Rut_Activo, ".", ""), Periodo, ElArchivo, False, True, True) Then
                
                    'If obj.ProcesaLCE_FormatoCSV(Replace(SP_Rut_Activo, ".", ""), Periodo, ElArchivo, True, True, True) Then
                        
                        MsgBox objx.DescripcionResultado
                    Else
                        MsgBox objx.DescripcionResultado
                    End If
                    
                End If
        Else
            'RECTIFICACION DE LIBROS
                If Mid(Tipo, 1, 5) = "VENTA" Then
                    
                    If objx.RectificaLVE_FormatoCSV(Replace(SP_Rut_Activo, ".", ""), Periodo, ElArchivo, True, True, Scodigo, True) Then
                    'If objx.ProcesaLVE_FormatoCSV(Replace(SP_Rut_Activo, ".", ""), Periodo, ElArchivo, True, True, True) Then
                        MsgBox objx.DescripcionResultado
                    Else
                        MsgBox objx.DescripcionResultado
                    End If
                
                ElseIf Mid(Tipo, 1, 6) = "COMPRA" Then
                    If objx.RectificaLCE_FormatoCSV(Replace(SP_Rut_Activo, ".", ""), Periodo, ElArchivo, True, True, Scodigo, True) Then
                    'If objx.ProcesaLCE_FormatoCSV(Replace(SP_Rut_Activo, ".", ""), Periodo, ElArchivo, True, True, True) Then
                    'If obj.ProcesaLCE_FormatoCSV(Replace(SP_Rut_Activo, ".", ""), Periodo, ElArchivo, True, True, True) Then
                        
                        MsgBox objx.DescripcionResultado
                    Else
                        MsgBox objx.DescripcionResultado
                    End If
                    
                End If
        
        
        End If
        
        ConsultaEstadoLibro
        
        
        Exit Sub
ProcesoLibro:
    MsgBox "Ocurrio un error al generar libro electronico..." & vbNewLine & Err.Number & vbNewLine & Err.Description, vbExclamation


End Sub





Private Sub cmdVolver_Click()
    Unload Me
End Sub
Private Sub Form_KeyUp(KeyCode As Integer, Shift As Integer)
   ' If KeyCode = vbKeyF1 Then CmdNueva_Click
   ' If KeyCode = vbKeyF2 Then CmdSeleccionar_Click
    If KeyCode = vbKeyF3 Then CmdExportaLibro_Click
    If KeyCode = vbKeyF4 Then CmdPrintLibro_Click
End Sub


Private Sub Form_Load()
    Skin2 Me, , 5
    Centrar Me
    FrmLoad.Visible = True
    DoEvents
'    MsgBox LvAC.ListItems(LvAC.ListItems.Count).SubItems(2)
'    MsgBox LvAC.ListItems(LvAC.ListItems.Count).SubItems(3)
    
    
    
    
   Sql = "SELECT emp_factura_electronica dte " & _
            "FROM sis_empresas " & _
            "WHERE rut='" & SP_Rut_Activo & "'"
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        If RsTmp!Dte = "NO" Then
            CmdSubirLibroElectronico.Visible = False
            TxtEstadoLibro.Visible = False
            CmdEliminaLibroElectronico.Visible = False
            Dim ax As String
          
            If Mid(Sm_CompraVenta, 1, 6) = "COMPRA" Then
            'CmdLibroSinSII.Visible = True
            Else
            CmdLibroSinSII.Visible = False
            End If
            
        Else
        
            If SG_Ver_Boton_Subir_Libros = "NO" Then
                CmdSubirLibroElectronico.Visible = False
                
                CmdEliminaLibroElectronico.Visible = False
                CmdLibroSinSII.Visible = False
    
            End If
        End If
    End If
    FrmLoad.Visible = False
End Sub
Public Sub ConsultaEstadoLibro()
        Dim obj As DTECloud.Integracion
        Dim Fp_Fin As Date
        TxtEstadoLibro = "Libros se deben consolidar en el registro de compra/ventas del SII"
        Fp_Fin = "2018-01-01"
        If Date > Fp_Fin Then Exit Sub
        Set obj = New DTECloud.Integracion
        obj.UrlServicioFacturacion = SG_Url_Factura_Electronica
        obj.Password = "1234"
        If Mid(Sm_CompraVenta, 1, 5) = "VENTA" Then
            resp = obj.ConsultaEstadoLibroElectronico(Replace(SP_Rut_Activo, ".", ""), Sm_Periodo, "VENTA", True)
        Else
            resp = obj.ConsultaEstadoLibroElectronico(Replace(SP_Rut_Activo, ".", ""), Sm_Periodo, "COMPRA", True)
        End If
        TxtEstadoLibro = obj.DescripcionResultado
        

End Sub


Private Sub Timer1_Timer()
    On Error Resume Next
    LvLibro.SetFocus
    Timer1.Enabled = False
End Sub
