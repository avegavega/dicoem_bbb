VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Begin VB.Form con_LibroVentas 
   Caption         =   "Libro Ventas"
   ClientHeight    =   2025
   ClientLeft      =   2385
   ClientTop       =   4290
   ClientWidth     =   6105
   LinkTopic       =   "Form1"
   ScaleHeight     =   2025
   ScaleWidth      =   6105
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   195
      OleObjectBlob   =   "con_LibroVentas.frx":0000
      Top             =   1725
   End
   Begin VB.Frame FrmPeriodo 
      Caption         =   "Periodo Contable"
      Height          =   1215
      Left            =   495
      TabIndex        =   0
      Top             =   525
      Width           =   5325
      Begin VB.ComboBox CboAnoContable 
         Height          =   315
         ItemData        =   "con_LibroVentas.frx":0234
         Left            =   2580
         List            =   "con_LibroVentas.frx":0236
         Style           =   2  'Dropdown List
         TabIndex        =   4
         Top             =   450
         Width           =   990
      End
      Begin VB.ComboBox CboMesContable 
         Height          =   315
         ItemData        =   "con_LibroVentas.frx":0238
         Left            =   285
         List            =   "con_LibroVentas.frx":023A
         Style           =   2  'Dropdown List
         TabIndex        =   3
         Top             =   450
         Width           =   2250
      End
      Begin VB.CommandButton Command1 
         Caption         =   "Libro Ventas"
         Height          =   495
         Left            =   3825
         TabIndex        =   2
         Top             =   210
         Width           =   1335
      End
      Begin VB.CommandButton CmdSalir 
         Caption         =   "&Cerrar"
         Height          =   390
         Left            =   3825
         TabIndex        =   1
         ToolTipText     =   "Salir"
         Top             =   735
         Width           =   1335
      End
   End
End
Attribute VB_Name = "con_LibroVentas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Form_Load()
    Aplicar_skin Me
End Sub
