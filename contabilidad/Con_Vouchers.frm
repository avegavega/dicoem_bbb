VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form Con_Vouchers 
   Caption         =   "Voucher Contable"
   ClientHeight    =   8415
   ClientLeft      =   7785
   ClientTop       =   1215
   ClientWidth     =   13695
   LinkTopic       =   "Form1"
   ScaleHeight     =   8415
   ScaleWidth      =   13695
   Begin VB.CommandButton Command1 
      Caption         =   "Calculadora"
      Height          =   360
      Left            =   12045
      TabIndex        =   48
      Top             =   60
      Width           =   1320
   End
   Begin MSComctlLib.ListView LvActivos 
      Height          =   1635
      Left            =   11025
      TabIndex        =   47
      Top             =   8490
      Width           =   2850
      _ExtentX        =   5027
      _ExtentY        =   2884
      View            =   3
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   3
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "aim_id"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "cta_id"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "aim_id"
         Object.Width           =   2540
      EndProperty
   End
   Begin VB.ComboBox CboTipoNiif 
      Height          =   315
      ItemData        =   "Con_Vouchers.frx":0000
      Left            =   6375
      List            =   "Con_Vouchers.frx":0002
      Style           =   2  'Dropdown List
      TabIndex        =   3
      ToolTipText     =   "Debe crear Areas si desea cambiar esta opci�n"
      Top             =   2070
      Width           =   1860
   End
   Begin VB.Frame FrmContable 
      Caption         =   "Contabilizaci�n"
      Height          =   4800
      Left            =   315
      TabIndex        =   22
      Top             =   3030
      Width           =   13080
      Begin VB.CommandButton CmdBuscaActivo 
         Caption         =   "A"
         Enabled         =   0   'False
         Height          =   315
         Left            =   5880
         TabIndex        =   39
         ToolTipText     =   "Buscar Activo Inmovilizado"
         Top             =   600
         Width           =   210
      End
      Begin VB.TextBox TxtCodActivo 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   6075
         Locked          =   -1  'True
         TabIndex        =   37
         TabStop         =   0   'False
         Top             =   585
         Width           =   480
      End
      Begin VB.TextBox TxtNombreActivo 
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   6555
         Locked          =   -1  'True
         TabIndex        =   35
         TabStop         =   0   'False
         Top             =   585
         Width           =   3135
      End
      Begin VB.CommandButton CmdCuenta 
         Caption         =   "Cuenta"
         Height          =   195
         Left            =   330
         TabIndex        =   31
         Top             =   375
         Width           =   930
      End
      Begin VB.CommandButton CmdGrabarV 
         Caption         =   "Grabar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   510
         Left            =   300
         TabIndex        =   9
         Top             =   4125
         Width           =   2310
      End
      Begin VB.TextBox TxtCodigoCuenta 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   345
         TabIndex        =   5
         Top             =   585
         Width           =   1000
      End
      Begin VB.TextBox TxtNombre 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   1350
         Locked          =   -1  'True
         TabIndex        =   25
         TabStop         =   0   'False
         Top             =   585
         Width           =   4515
      End
      Begin VB.TextBox TxtDebito 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   9690
         TabIndex        =   6
         Text            =   "0"
         Top             =   585
         Width           =   1380
      End
      Begin VB.TextBox TxtCredito 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   11070
         TabIndex        =   7
         Text            =   "0"
         Top             =   585
         Width           =   1380
      End
      Begin VB.CommandButton CmdOk 
         Caption         =   "Ok"
         Height          =   375
         Left            =   12435
         TabIndex        =   8
         Top             =   570
         Width           =   420
      End
      Begin VB.TextBox TxtHaber 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   11100
         Locked          =   -1  'True
         TabIndex        =   24
         TabStop         =   0   'False
         Text            =   "0"
         Top             =   4140
         Width           =   1380
      End
      Begin VB.TextBox TxtDebe 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   9705
         Locked          =   -1  'True
         TabIndex        =   23
         TabStop         =   0   'False
         Text            =   "0"
         Top             =   4140
         Width           =   1380
      End
      Begin MSComctlLib.ListView LvContable 
         Height          =   3105
         Left            =   330
         TabIndex        =   26
         Top             =   990
         Width           =   12540
         _ExtentX        =   22119
         _ExtentY        =   5477
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   7
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "N109"
            Text            =   "Id"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "N109"
            Text            =   "C�digo"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "T5000"
            Text            =   "Nombre de la Cuenta"
            Object.Width           =   7964
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Object.Tag             =   "T1000"
            Text            =   "Cod"
            Object.Width           =   1217
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Object.Tag             =   "T1000"
            Text            =   "Nombre Activo"
            Object.Width           =   5530
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   5
            Key             =   "debe"
            Object.Tag             =   "N100"
            Text            =   "D E B E"
            Object.Width           =   2434
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   6
            Key             =   "haber"
            Object.Tag             =   "N100"
            Text            =   "H A B E R"
            Object.Width           =   2434
         EndProperty
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel6 
         Height          =   240
         Left            =   7905
         OleObjectBlob   =   "Con_Vouchers.frx":0004
         TabIndex        =   27
         Top             =   4200
         Width           =   1665
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel7 
         Height          =   240
         Left            =   1365
         OleObjectBlob   =   "Con_Vouchers.frx":0070
         TabIndex        =   32
         Top             =   390
         Width           =   2475
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel8 
         Height          =   240
         Left            =   9345
         OleObjectBlob   =   "Con_Vouchers.frx":00F4
         TabIndex        =   33
         Top             =   390
         Width           =   1665
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel9 
         Height          =   240
         Left            =   10710
         OleObjectBlob   =   "Con_Vouchers.frx":0160
         TabIndex        =   34
         Top             =   390
         Width           =   1665
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel10 
         Height          =   240
         Left            =   6570
         OleObjectBlob   =   "Con_Vouchers.frx":01CE
         TabIndex        =   36
         Top             =   390
         Width           =   2475
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel12 
         Height          =   240
         Left            =   5790
         OleObjectBlob   =   "Con_Vouchers.frx":024E
         TabIndex        =   38
         Top             =   390
         Width           =   750
      End
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   60
      OleObjectBlob   =   "Con_Vouchers.frx":02B8
      Top             =   8295
   End
   Begin VB.Timer Timer1 
      Interval        =   10
      Left            =   555
      Top             =   8340
   End
   Begin VB.Frame Frame1 
      Caption         =   "VOUCHER - Ingresa -Edita -Elimina"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2610
      Left            =   300
      TabIndex        =   10
      Top             =   270
      Width           =   13050
      Begin VB.Frame Frame3 
         Caption         =   "Traspaso Apertura A�O ANTERIOR"
         Height          =   600
         Left            =   9060
         TabIndex        =   44
         Top             =   1485
         Width           =   3915
         Begin VB.ComboBox CboAno 
            Height          =   315
            ItemData        =   "Con_Vouchers.frx":04EC
            Left            =   2625
            List            =   "Con_Vouchers.frx":04EE
            Style           =   2  'Dropdown List
            TabIndex        =   46
            Top             =   225
            Width           =   1215
         End
         Begin VB.CommandButton CmdObtener 
            Caption         =   "Obtener datos periodo"
            Height          =   255
            Left            =   240
            TabIndex        =   45
            Top             =   255
            Width           =   2385
         End
      End
      Begin VB.Frame Frame2 
         Caption         =   "Asiento especial de APERTURA"
         Height          =   765
         Left            =   9060
         TabIndex        =   40
         ToolTipText     =   "Opci�n a utilizar al emplear el SISTEMA  por 1ra vez"
         Top             =   660
         Width           =   3900
         Begin VB.CheckBox CheckApertura 
            Caption         =   "Marque casilla si el SISTEMA se emplea por 1ra vez "
            Height          =   525
            Left            =   765
            TabIndex        =   41
            Top             =   195
            Width           =   3060
         End
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel13 
         Height          =   225
         Left            =   6390
         OleObjectBlob   =   "Con_Vouchers.frx":04F0
         TabIndex        =   30
         Top             =   1320
         Width           =   1080
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel11 
         Height          =   210
         Left            =   6090
         OleObjectBlob   =   "Con_Vouchers.frx":0566
         TabIndex        =   29
         Top             =   1560
         Width           =   1740
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkEditando 
         Height          =   195
         Left            =   4935
         OleObjectBlob   =   "Con_Vouchers.frx":05F2
         TabIndex        =   28
         Top             =   1215
         Width           =   2115
      End
      Begin VB.ComboBox CboTipo 
         Height          =   315
         ItemData        =   "Con_Vouchers.frx":0650
         Left            =   4185
         List            =   "Con_Vouchers.frx":0652
         Style           =   2  'Dropdown List
         TabIndex        =   2
         ToolTipText     =   "Debe crear Areas si desea cambiar esta opci�n"
         Top             =   1755
         Width           =   1860
      End
      Begin VB.TextBox TxtDia 
         Height          =   345
         Left            =   4185
         MaxLength       =   2
         TabIndex        =   1
         Top             =   1395
         Width           =   765
      End
      Begin VB.TextBox TxtGlosa 
         Height          =   345
         Left            =   4185
         TabIndex        =   4
         Top             =   2085
         Width           =   8730
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   315
         Left            =   795
         OleObjectBlob   =   "Con_Vouchers.frx":0654
         TabIndex        =   19
         Top             =   2070
         Width           =   3300
      End
      Begin VB.TextBox tXTNuevo 
         BackColor       =   &H00FFFFFF&
         Height          =   345
         Left            =   4185
         TabIndex        =   0
         Top             =   1050
         Width           =   765
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   315
         Left            =   165
         OleObjectBlob   =   "Con_Vouchers.frx":06DE
         TabIndex        =   18
         Top             =   1080
         Width           =   3930
      End
      Begin VB.TextBox TxtUltimo 
         BackColor       =   &H00E0E0E0&
         Height          =   345
         Left            =   4185
         Locked          =   -1  'True
         TabIndex        =   17
         TabStop         =   0   'False
         Top             =   705
         Width           =   750
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   330
         Left            =   105
         OleObjectBlob   =   "Con_Vouchers.frx":0774
         TabIndex        =   16
         Top             =   765
         Width           =   3990
      End
      Begin VB.TextBox TxtMes 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFFC0&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   435
         Left            =   11235
         Locked          =   -1  'True
         TabIndex        =   15
         TabStop         =   0   'False
         Top             =   270
         Width           =   1695
      End
      Begin VB.TextBox TxtAno 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFFC0&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   435
         Left            =   9525
         Locked          =   -1  'True
         TabIndex        =   14
         TabStop         =   0   'False
         Text            =   "Text1"
         Top             =   270
         Width           =   1725
      End
      Begin VB.TextBox skEmpresa 
         BackColor       =   &H00FFFFC0&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   435
         Left            =   2175
         Locked          =   -1  'True
         TabIndex        =   13
         TabStop         =   0   'False
         Text            =   "Text1"
         Top             =   270
         Width           =   7350
      End
      Begin VB.TextBox SkRut 
         BackColor       =   &H00FFFFC0&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   435
         Left            =   210
         Locked          =   -1  'True
         TabIndex        =   12
         TabStop         =   0   'False
         Text            =   "Text1"
         Top             =   270
         Width           =   1950
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
         Height          =   315
         Left            =   165
         OleObjectBlob   =   "Con_Vouchers.frx":0816
         TabIndex        =   20
         Top             =   1425
         Width           =   3930
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel5 
         Height          =   315
         Left            =   165
         OleObjectBlob   =   "Con_Vouchers.frx":087A
         TabIndex        =   21
         Top             =   1740
         Width           =   3930
      End
   End
   Begin VB.CommandButton CmdSalir 
      Cancel          =   -1  'True
      Caption         =   "&Retornar"
      Height          =   465
      Left            =   11745
      TabIndex        =   11
      Top             =   7920
      Width           =   1620
   End
   Begin MSComctlLib.ListView LvDetalleC 
      Height          =   1095
      Left            =   315
      TabIndex        =   42
      Top             =   7920
      Width           =   11145
      _ExtentX        =   19659
      _ExtentY        =   1931
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   0
      NumItems        =   11
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Object.Tag             =   "T500"
         Text            =   "RUT"
         Object.Width           =   2117
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Object.Tag             =   "T3000"
         Text            =   "Nombre"
         Object.Width           =   4762
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Key             =   "debe"
         Object.Tag             =   "T1500"
         Text            =   "Documento"
         Object.Width           =   2381
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   3
         Key             =   "haber"
         Object.Tag             =   "N109"
         Text            =   "Nro Documento"
         Object.Width           =   2381
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Object.Tag             =   "F1000"
         Text            =   "Fecha"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   5
         Key             =   "bruto"
         Object.Tag             =   "N100"
         Text            =   "Bruto"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   6
         Object.Tag             =   "N109"
         Text            =   "id documeneto"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   7
         Text            =   "TIENE ABONOS?"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   8
         Object.Tag             =   "N109"
         Text            =   "id unico"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   9
         Text            =   "SUCURAL"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   10
         Text            =   "ID SUCURSAL"
         Object.Width           =   2540
      EndProperty
   End
   Begin MSComctlLib.ListView LvDetalleP 
      Height          =   1440
      Left            =   270
      TabIndex        =   43
      Top             =   9045
      Width           =   11205
      _ExtentX        =   19764
      _ExtentY        =   2540
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   0
      NumItems        =   11
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Object.Tag             =   "T500"
         Text            =   "RUT"
         Object.Width           =   2117
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Object.Tag             =   "T3000"
         Text            =   "Nombre"
         Object.Width           =   4762
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Key             =   "debe"
         Object.Tag             =   "T1500"
         Text            =   "Documento"
         Object.Width           =   2381
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   3
         Key             =   "haber"
         Object.Tag             =   "N109"
         Text            =   "Nro Documento"
         Object.Width           =   2381
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Object.Tag             =   "F1000"
         Text            =   "Fecha"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   5
         Key             =   "bruto"
         Object.Tag             =   "N100"
         Text            =   "Bruto"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   6
         Object.Tag             =   "N109"
         Text            =   "doc_id"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   7
         Text            =   "TIENE ABONOS?"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   8
         Text            =   "id unico"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   9
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   10
         Object.Width           =   2540
      EndProperty
   End
End
Attribute VB_Name = "Con_Vouchers"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public Sm_Centralizado As String
Public Sm_NroVEditado As Long
Dim Sm_NroVOriginal As Long



Private Sub CmdBuscaActivo_Click()
    paso = "voucher"
    SG_codigo = TxtCodActivo
    SG_codigo2 = TxtCodigoCuenta
    Con_ActivoInmovilizado.Sm_AnoPedido = TxtAno
    Con_ActivoInmovilizado.Sm_Viene = "VOUCHER"
    Con_ActivoInmovilizado.CmdVoucher.Visible = True
    
    Con_ActivoInmovilizado.Sm_FiltroCta = " AND a.pla_id=" & TxtCodigoCuenta
    Con_ActivoInmovilizado.CboFiltroActivo.Enabled = False
    Con_ActivoInmovilizado.CmdSelecciona.Visible = True
    
    Con_ActivoInmovilizado.Show 1
    If Val(SG_codigo) > 0 Then
        
        TxtCodActivo = SG_codigo
    
        
        'Sql = "SELECT aim_nombre,aim_valor " & _
        '        "FROM con_activo_inmovilizado " & _
        '        "WHERE aim_id IN (" & SG_codigo & ")"
        'Consulta RsTmp, Sql
        
        'If RsTmp.RecordCount > 0 Then
        '    TxtCodActivo = SG_codigo
            TxtNombreActivo = "VARIOS"
            If Me.CboTipo.ListIndex = 0 Then
                Me.TxtDebito = SG_codigo2
                TxtDebito.SetFocus
            Else
                TxtCredito = SG_codigo2
                TxtCredito.SetFocus
            End If
       ' End If
    ElseIf SG_codigo = "RESUMEN" Then
            TxtCodActivo = -1
            If Me.CboTipo.ListIndex = 0 Or CboTipo.ListIndex = 2 Then
                Me.TxtDebito = SG_codigo2
                TxtDebito.SetFocus
            Else
                TxtCredito = SG_codigo2
                TxtCredito.SetFocus
            End If
    End If
    Con_ActivoInmovilizado.Sm_FiltroCta = Empty
    
    
    
End Sub

Private Sub cmdCuenta_Click()
     With BuscarSimple
        SG_codigo = Empty
        .Sm_Consulta = "SELECT pla_id, pla_nombre,tpo_nombre,det_nombre " & _
                       "FROM    con_plan_de_cuentas p,  con_tipo_de_cuenta t,   con_detalle_tipo_cuenta d " & _
                       "WHERE   p.tpo_id = t.tpo_id AND p.det_id = d.det_id "
        .Sm_CampoLike = "pla_nombre"
        .Im_Columnas = 2
        .CmdCuentasContables.Visible = True
        .Show 1
        If SG_codigo = Empty Then Exit Sub
        TxtCodigoCuenta = Val(SG_codigo)
        TxtCodigoCuenta.SetFocus
    End With
End Sub

Private Sub CmdGrabarV_Click()
    Dim Lp_Vou As Long, Lp_Vod As Long, Lp_VenId As Long, Sp_Apertura As String * 2
    
    If Val(tXTNuevo) = 0 Then
        MsgBox "Debe ingresar Nro de Vouchers..."
        tXTNuevo.SetFocus
        Exit Sub
    End If
    If LvContable.ListItems.Count = 0 Then
        MsgBox "No ha ingresado cuentas...", vbInformation
        TxtCodigoCuenta.SetFocus
        Exit Sub
    End If
    
    If CDbl(TxtDebe) <> CDbl(TxtHaber) Then
        MsgBox "Los totales de Debitos y Creditos no son iguales...", vbInformation
        LvContable.SetFocus
        Exit Sub
    End If
    
    'Buscar si el Nro de voucher esta disponible
    If Not VoucherDisponible(tXTNuevo) Then
        If Sm_NroVEditado = Val(tXTNuevo) Then
            'APROUEB
        Else
            MsgBox "Nro Voucher no disponible ...", vbInformation
            tXTNuevo.SetFocus
            Exit Sub
        End If
    End If
    On Error GoTo Grabando
    cn.BeginTrans
   '     Cx = DG_ID_Unico
    '    Sql = "SELECT vou_id " & _
                "FROM con_vouchers  " & _
                "WHERE vou_numero=" & Sm_NroVOriginal & " AND rut_emp='" & SP_Rut_Activo & "'"
    '    Consulta RsTmp, Sql
        
       If DG_ID_Unico > 0 Then
             '    eliminanos los vouchers previos si es que existen
            Lp_Vou = DG_ID_Unico
        '    Sql = "DELETE FROM con_vouchers " & _
                    "WHERE rut_emp='" & SP_Rut_Activo & "' AND vou_id=" & DG_ID_Unico
          '  cn.Execute Sql
            
         '   cn.Execute "DELETE FROM con_vouchers_detalle " & _
                       "WHERE  vou_id=" & Lp_Vou
                       
             '27 Nov. 2013
             'Cambiamos Delete por Update dejando el voucher sin rut, solo para que no se borre
                Sql = "UPDATE con_vouchers SET rut_emp='MODIF' " & _
                    "WHERE  vou_id=" & DG_ID_Unico
              cn.Execute Sql
            
            Lp_Vou = UltimoNro("con_vouchers", "vou_id")
        Else
            Lp_Vou = UltimoNro("con_vouchers", "vou_id")
        End If
        
       
        
        Sp_Apertura = IIf(CheckApertura.Value = 1, "SI", "NO")
        
        Sql4 = "INSERT INTO con_vouchers (vou_id,vou_numero,vou_dia,vou_mes_contable,vou_ano_contable,vou_fecha_ingreso,vou_glosa,rut_emp,vou_tipo,vou_tiponiif,vou_apertura) " & _
            "VALUES(" & Lp_Vou & "," & tXTNuevo & "," & TxtDia & "," & TxtMes.Tag & "," & IG_Ano_Contable & ",'" & Fql(Date) & "','" & TxtGlosa & "','" & SP_Rut_Activo & "'," & CboTipo.ItemData(CboTipo.ListIndex) & "," & CboTipoNiif.ItemData(CboTipoNiif.ListIndex) & ",'" & Sp_Apertura & "')"
        cn.Execute Sql4
        
      
        
        cn.Execute "DELETE FROM con_relacion_activos " & _
                            "WHERE vou_id=" & Lp_Vou
                            
                            
        'marquermos los documentos de venta con otro rut_emp, para que ya no pertenezcan aqui, y se crearan de nuevo
        Sql = ""
        Sql5 = ""
        'For i = 1 To LvContable.ListItems.Count
        Sql = "SELECT vod_id " & _
                "FROM con_vouchers v " & _
                "JOIN con_vouchers_detalle d USING(vou_id) " & _
                "WHERE v.vou_id=" & DG_ID_Unico
       Consulta RsTmp3, Sql
       If RsTmp3.RecordCount > 0 Then     '
        
            RsTmp3.MoveFirst
            Do While Not RsTmp3.EOF
                'documentos de clientes
               Sql = "SELECT no_documento,doc_id,id " & _
                       "FROM ven_doc_venta v " & _
                       "WHERE id IN (SELECT vdt_rel " & _
                                       "FROM con_voucher_detalle_auxiliar " & _
                                       "WHERE vdt_tipo='CLI' AND vod_id=" & RsTmp3!vod_id & ")"
                Consulta RsTmp, Sql
                Sql5 = ""
                If RsTmp.RecordCount > 0 Then
                    If SP_Control_Inventario = "NO" Then
                               cn.Execute "DELETE FROM ven_sin_detalle " & _
                                            "WHERE id IN (SELECT vdt_rel " & _
                                                        "FROM con_voucher_detalle_auxiliar " & _
                                                        "WHERE vdt_tipo='CLI' AND vod_id=" & RsTmp3!vod_id & ")"
                    End If
                    RsTmp.MoveFirst
                    Do While Not RsTmp.EOF
                        cn.Execute "UPDATE ven_doc_venta SET rut_emp='MODIF' " & _
                                    "WHERE no_documento=" & RsTmp!no_documento & " AND doc_id=" & RsTmp!doc_id & " AND rut_emp='" & SP_Rut_Activo & "'"
                        If SP_Control_Inventario = "SI" Then
                                cn.Execute "UPDATE ven_detalle SET rut_emp='MODIF' " & _
                                    "WHERE no_documento=" & RsTmp!no_documento & " AND doc_id=" & RsTmp!doc_id & " AND rut_emp='" & SP_Rut_Activo & "'"
                        End If
                        
                        
                        Sql5 = Sql5 & RsTmp!Id & ","
                        RsTmp.MoveNext
                    Loop
                End If
                If Len(Sql5) > 0 Then
                    Sql5 = Mid(Sql5, 1, Len(Sql5) - 1)
                    sql2 = "DELETE FROM con_voucher_detalle_auxiliar " & _
                            "WHERE vdt_tipo='CLI' AND vod_id=" & RsTmp3!vod_id & " AND  vdt_rel IN (" & Sql5 & ")"
                    cn.Execute sql2
                End If
                
                
                'Documentos de proveedores
               Sql = "SELECT no_documento,doc_id,id " & _
                       "FROM com_doc_compra v " & _
                       "WHERE id IN (SELECT vdt_rel " & _
                                       "FROM con_voucher_detalle_auxiliar " & _
                                       "WHERE vdt_tipo='PRO' AND vod_id=" & RsTmp3!vod_id & ")"
                Consulta RsTmp, Sql
                Sql5 = ""
                If RsTmp.RecordCount > 0 Then
                    RsTmp.MoveFirst
                    Do While Not RsTmp.EOF
                        cn.Execute "UPDATE com_doc_compra SET rut_emp='MODIF' " & _
                                    "WHERE no_documento=" & RsTmp!no_documento & " AND doc_id=" & RsTmp!doc_id & " AND rut_emp='" & SP_Rut_Activo & "'"
                    '    cn.Execute "UPDATE com_doc_compra_detalle SET rut_emp='MODIF' " & _
                                                                "WHERE no_documento=" & RsTmp!no_documento & " AND doc_id=" & RsTmp!doc_id & " AND rut_emp='" & SP_Rut_Activo & "'"
                        Sql5 = Sql5 & RsTmp!Id & ","
                        RsTmp.MoveNext
                    Loop
                End If
                If Len(Sql5) > 0 Then
                    Sql5 = Mid(Sql5, 1, Len(Sql5) - 1)
                    sql2 = "DELETE FROM con_voucher_detalle_auxiliar " & _
                            "WHERE vdt_tipo='PRO' AND vod_id=" & RsTmp3!vod_id & " AND  vdt_rel IN (" & Sql5 & ")"
                    cn.Execute sql2
                End If
                
                
                
                
                RsTmp3.MoveNext
            Loop
        End If
            'Next
        
        For i = 1 To LvContable.ListItems.Count
            
            Lp_Vod = UltimoNro("con_vouchers_detalle", "vod_id")
            cn.Execute "INSERT INTO con_vouchers_detalle (vod_id,vou_id,pla_id,vod_debe,vod_haber,aim_id) VALUES " & _
            "(" & Lp_Vod & "," & Lp_Vou & "," & LvContable.ListItems(i).SubItems(1) & "," & CDbl(LvContable.ListItems(i).SubItems(5)) & "," & CDbl(LvContable.ListItems(i).SubItems(6)) & "," & Val(LvContable.ListItems(i).SubItems(3)) & ")"
            
            
            If Val(LvContable.ListItems(i).SubItems(3)) > 0 Then
                
              
                
                'Debe identifcar los activos marcados de esta linea
                'y grabar su id en el campo vod_id de activos inmovilizados
                'para poder tenerlos relacionados e indentificados
                'MsgBox LvContable.ListItems(i).SubItems(3)
                'cn.Execute "UPDATE con_activo_inmovilizado SET vod_id=" & Lp_Vod & " " & _
                            "WHERE aim_id IN(" & LvContable.ListItems(i).SubItems(3) & ")"
                '14-09-2013
                Dim codigos() As String
                Dim codprecio() As String
                codigos = Split(LvContable.ListItems(i).SubItems(3), ",")
                
               
                
                For n = LBound(codigos) To UBound(codigos)
                    codprecio = Split(codigos(n), "/")
                
                
                    cn.Execute "INSERT INTO con_relacion_activos (pla_id,vou_id,vod_id,aim_id,aim_valor) " & _
                                "VALUES (" & LvContable.ListItems(i).SubItems(1) & "," & Lp_Vou & "," & Lp_Vod & "," & codprecio(0) & "," & codprecio(1) & ")"
                    
                    'List1.AddItem codigos(i).
                    
                          'aqui sabemos que se trata de un activo inmovilizado
                    '16 Enero 2013
                    'Entonces debemos saber si es Debe o Haber
                    If CDbl(LvContable.ListItems(i).SubItems(5)) > 0 Then
                        'Debito = Entrada Solo actualizaremos el precio y fecha
                        
                        'Sabado 26 de Julio 2014
                        'no actualizaremos el precio ya que lo obtenemos de los vouchers
                        'cn.Execute "UPDATE con_activo_inmovilizado " & _
                                    "SET /*aim_fecha_ingreso='" & Fql(Date) & "',*/ aim_valor=" & codprecio(1) & " " & _
                                    "WHERE rut_emp='" & SP_Rut_Activo & "' AND aim_id=" & codprecio(0)
                    
                    End If
                    If CDbl(LvContable.ListItems(i).SubItems(6)) > 0 Then
                        'Credito = Salida Actualizaremos fecha egreso y habilitado='NO'
                        cn.Execute "UPDATE con_activo_inmovilizado " & _
                                    "SET aim_fecha_egreso='" & Fql(Date) & "',aim_valor=" & codprecio(1) & ",aim_habilitado='NO' " & _
                                    "WHERE rut_emp='" & SP_Rut_Activo & "' AND aim_id=" & codprecio(0)
                    
                    End If
                    
                    
  
                Next
                
                
                
                
                
            End If
          
            
            
            'Debemos saber si es cuenta cliente, proveedor o activos e
            'para asociarlos al detalle del vocuher
            
          
            
            If IG_IdCtaClientes = Val(LvContable.ListItems(i).SubItems(1)) And CheckApertura.Value = 1 Then
                'Apertura cuando hay CLIENTES
                If LvDetalleC.ListItems.Count > 0 Then
                    Sql = "SELECT ven_id " & _
                            "FROM par_vendedores " & _
                            "WHERE rut_emp='" & SP_Rut_Activo & "' " & _
                            "LIMIT 1"
                            
                    Consulta RsTmp, Sql
                    vendersito = 0
                    If RsTmp.RecordCount > 0 Then vendersito = RsTmp!ven_id
                        
                    
                    
                    For Y = 1 To LvDetalleC.ListItems.Count
                        Lp_VenId = UltimoNro("ven_doc_venta", "id")
                        'Grabamos documento de venta para cuenta corriente
                        cn.Execute "INSERT INTO ven_doc_venta " & _
                        "(id,no_documento,fecha,rut_cliente,nombre_cliente,bruto,condicionpago," & _
                        "forma_pago,tipo_doc,doc_id,usu_nombre,rut_emp,ven_informa_venta,ven_id,ven_plazo_id,sue_id,suc_id) VALUES(" & Lp_VenId & "," & _
                        LvDetalleC.ListItems(Y).SubItems(3) & ",'" & Fql(LvDetalleC.ListItems(Y).SubItems(4)) & "',/*rut*/'" & LvDetalleC.ListItems(Y) & "'," & _
                        "/*nombre*/'" & LvDetalleC.ListItems(Y).SubItems(1) & "'," & _
                        "/*VALOR*/" & CDbl(LvDetalleC.ListItems(Y).SubItems(5)) & ",'CREDIT0','PENDIENTE',/*DOCUMENTO*/'" & LvDetalleC.ListItems(Y).SubItems(2) & "'," & _
                        "/*ID DOCUMENTO*/" & LvDetalleC.ListItems(Y).SubItems(6) & ",'" & LogUsuario & "','" & SP_Rut_Activo & "','NO'," & vendersito & ",30," & LvDetalleC.ListItems(Y).SubItems(10) & ",1)"
                        
                        'Agregaremos una linea de detalle
                        
                        InsertaProductoAsientoApertura CreaProductoAsientoApertura, CDbl(LvDetalleC.ListItems(Y).SubItems(5)), LvDetalleC.ListItems(Y).SubItems(3), LvDetalleC.ListItems(Y).SubItems(6), "CLI", Lp_VenId, SP_Control_Inventario
                        
                        cn.Execute "UPDATE maestro_clientes SET cli_monto_credito=cli_monto_credito+" & CDbl(LvDetalleC.ListItems(Y).SubItems(5)) & " " & _
                                    "WHERE rut_cliente='" & LvDetalleC.ListItems(Y) & "'"
                        
                        'Grabamos auxiliar con detalle
                        cn.Execute "INSERT INTO con_voucher_detalle_auxiliar (vdt_tipo,vod_id,vdt_rel) " & _
                                    "VALUES('CLI'," & Lp_Vod & "," & Lp_VenId & ")"
                    Next
                End If
            End If
        
        
                        'Debemos saber si es cuenta cliente, proveedor o activos e
            'para asociarlos al detalle del vocuher
            If IG_IdCtaProveedores = Val(LvContable.ListItems(i).SubItems(1)) And CheckApertura.Value = 1 Then
                'Apertura cuando hay PROVEEDORES
                If LvDetalleP.ListItems.Count > 0 Then
                    For Y = 1 To LvDetalleP.ListItems.Count
                        Lp_VenId = UltimoNro("com_doc_compra", "id")
                        'Grabamos documento de COMPRA para cuenta corriente
                        
                         cn.Execute "INSERT INTO com_doc_compra (id,rut,nombre_proveedor,documento,no_documento," & _
                            "doc_id,pago,total,fecha,rut_emp,com_informa_compra,mes_contable,ano_contable) VALUES(" & Lp_VenId & "," & _
                        "/*rut*/'" & LvDetalleP.ListItems(Y) & "',/*nombre*/'" & LvDetalleP.ListItems(Y).SubItems(1) & "'," & _
                        "    /*DOCUMENTO*/'" & LvDetalleP.ListItems(Y).SubItems(2) & "',/*NRO DOCUMENTO*/ " & LvDetalleP.ListItems(Y).SubItems(3) & "," & _
                        "/*ID DOCUMENTO*/" & LvDetalleP.ListItems(Y).SubItems(6) & ",'CREDITO'," & _
                        "/*VALOR*/" & CDbl(LvDetalleP.ListItems(Y).SubItems(5)) & ",'" & Fql(LvDetalleP.ListItems(Y).SubItems(4)) & "','" & SP_Rut_Activo & "'," & _
                        "/*informa compra*/ 'NO',/* mes_contable */ " & TxtMes.Tag & "," & TxtAno & ")"
                    
                    
                        'Agregaremos una linea de detalle
                        
                        InsertaProductoAsientoApertura CreaProductoAsientoApertura, CDbl(LvDetalleP.ListItems(Y).SubItems(5)), LvDetalleP.ListItems(Y).SubItems(3), LvDetalleP.ListItems(Y).SubItems(6), "PRO", Lp_VenId, SP_Control_Inventario
                        
                     
                        
                        'Grabamos auxiliar con detalle
                        cn.Execute "INSERT INTO con_voucher_detalle_auxiliar (vdt_tipo,vod_id,vdt_rel) " & _
                                    "VALUES('CLI'," & Lp_Vod & "," & Lp_VenId & ")"
                    
                        
                        'Grabamos auxiliar con detalle
                        cn.Execute "INSERT INTO con_voucher_detalle_auxiliar (vdt_tipo,vod_id,vdt_rel) " & _
                                    "VALUES('PRO'," & Lp_Vod & "," & Lp_VenId & ")"
                    Next
                End If
            End If

        
        Next
        
        
        
        
        
    cn.CommitTrans
    
    
    
    
    UltimoV
    LvContable.ListItems.Clear
    LvDetalleC.ListItems.Clear
    LvDetalleP.ListItems.Clear
    Exit Sub
Grabando:    MsgBox "Ocurrio un error al intentar grabar..." & vbNewLine & Err.Number & " " & Err.Description, vbInformation
    cn.RollbackTrans
    

End Sub

Private Sub CmdObtener_Click()


    'Detalle de cuentas
    Sql = "SELECT " & tXTNuevo & ",d.pla_id,c.pla_nombre,'','',IF(c.tpo_id = 1 OR c.tpo_id = 2," & _
        "IF(SUM(d.vod_debe) > SUM(d.vod_haber),SUM(d.vod_debe) - SUM(d.vod_haber),0),0)activo," & _
        "IF(c.tpo_id = 1 OR c.tpo_id = 2,IF(" & _
        "SUM(d.vod_debe) < SUM(d.vod_haber)," & _
        "SUM(d.vod_haber) - SUM(d.vod_debe),0),0) pasivo " & _
        "FROM con_vouchers_detalle d " & _
            "JOIN con_plan_de_cuentas c USING (pla_id) " & _
            "JOIN con_vouchers v USING (vou_id) " & _
            "JOIN con_tipo_de_cuenta t USING (tpo_id) " & _
        "WHERE v.vou_tiponiif IN (1, 2) AND t.tpo_id IN(1,2) " & _
        "AND vou_ano_contable = " & CboAno.Text & " AND rut_emp = '" & SP_Rut_Activo & "' " & _
        "GROUP BY d.pla_id " & _
        "HAVING activo + pasivo > 0 " & _
        "ORDER BY c.tpo_id,c.det_id "
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvContable, False, True, True, False
    
    
    
    '15 Marzo 2014
    '***************************************
    'Cargar los activos inmovilizados del periodo seleccionado
    For i = 1 To LvContable.ListItems.Count
        
        Sql = "SELECT r.aim_id,SUM(IF(d.vod_debe>0,r.aim_valor,0)) - SUM(IF(d.vod_haber>0,r.aim_valor,0)) valor " & _
                    "FROM con_vouchers_detalle d " & _
                    "JOIN con_relacion_activos r ON d.pla_id = r.pla_id  AND d.vou_id = r.vou_id AND d.vod_id = r.vod_id " & _
                    "JOIN con_vouchers v ON d.vou_id = v.vou_id " & _
                    "WHERE d.pla_id=" & LvContable.ListItems(i).SubItems(1) & "  AND v.rut_emp = '" & SP_Rut_Activo & "'  AND v.vou_ano_contable =" & CboAno.Text & " " & _
                    "GROUP BY r.aim_id"
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
            'relacionar activos
            With RsTmp
                .MoveFirst
                Do While Not .EOF
                    LvContable.ListItems(i).SubItems(3) = LvContable.ListItems(i).SubItems(3) & !aim_id & "/" & Abs(!Valor) & ","
                    .MoveNext
                Loop
            End With
            LvContable.ListItems(i).SubItems(3) = Mid(LvContable.ListItems(i).SubItems(3), 1, Len(LvContable.ListItems(i).SubItems(3)) - 1)
            
        End If
                    
    Next
    
    '***************************************
    
    
        
    TxtDebe = NumFormat(TotalizaColumna(LvContable, "debe"))
    TxtHaber = NumFormat(TotalizaColumna(LvContable, "haber"))
    




End Sub

Private Sub CmdOk_Click()
    Dim Ip_C As Integer, Bp_corregir As Boolean
    If Val(TxtCodigoCuenta) = 0 Then Exit Sub
    
    If Val(TxtCredito) > 0 And Val(TxtDebito) > 0 Then
        MsgBox "No puede tener Debitos y Creditos en una misma cuenta...", vbInformation
        TxtCredito.SetFocus
        Exit Sub
    End If
    
    
    If Val(TxtCredito) = 0 And Val(TxtDebito) = 0 Then
        MsgBox "Debe ingresar valores...", vbInformation
        TxtCredito.SetFocus
        Exit Sub
    End If
    
    If Me.CmdBuscaActivo.Enabled Then
        If Val(TxtCodActivo) = 0 Then
            MsgBox "Debe seleccionar Activo Inmovilizado..."
            CmdBuscaActivo.SetFocus
            Exit Sub
        End If
        If Val(TxtCodActivo) = -1 Then TxtCodActivo = ""
    End If
    
    Bp_corregir = False
    For i = 1 To LvContable.ListItems.Count
        If Val(LvContable.ListItems(i).SubItems(1)) = TxtCodigoCuenta Then
            If Me.CmdBuscaActivo.Enabled = True Then
                If Val(LvContable.ListItems(i).SubItems(3)) = Val(TxtCodActivo) Then
                    MsgBox "No se puede repetir el activo en un voucher...", vbInformation
                    Exit Sub
                End If
            
            Else
            
                If MsgBox("Cuenta ya ingresada..." & vbNewLine & " �Corregir?..", vbQuestion + vbYesNo) = vbNo Then Exit Sub
                Bp_corregir = True
                Ip_C = i
            End If
        End If
            
        
    Next
    
    'Verificar q si el voucher es de apertura
    'y es de clientes, ver que el valor ingresado corresponda a
    'la suma de la grilla de clientess
    If Val(TxtDebito) = 0 Then TxtDebito = "0"
    If Val(TxtCredito) = 0 Then TxtCredito = "0"
    If IG_IdCtaClientes = TxtCodigoCuenta Then
        If CDbl(TxtDebito) > 0 Then valorA = CDbl(TxtDebito)
        If CDbl(TxtCredito) > 0 Then valorA = CDbl(TxtCredito)
        If LvDetalleC.ListItems.Count > 0 Then
            If valorA <> TotalizaColumna(LvDetalleC, "bruto") Then
                MsgBox "Valores no coinciden con lo ingresado en clientes"
                Exit Sub
            End If
        End If
    End If
    
    If IG_IdCtaProveedores = TxtCodigoCuenta Then
        If CDbl(TxtDebito) > 0 Then valorA = CDbl(TxtDebito)
        If CDbl(TxtCredito) > 0 Then valorA = CDbl(TxtCredito)
        If LvDetalleP.ListItems.Count > 0 Then
            If valorA <> TotalizaColumna(LvDetalleP, "bruto") Then
                MsgBox "Valores no coinciden con lo ingresado en clientes"
                Exit Sub
            End If
        End If
    End If
    
    
    If Not Bp_corregir Then
        LvContable.ListItems.Add , , tXTNuevo
        Ip_C = LvContable.ListItems.Count
    End If
    LvContable.ListItems(Ip_C).SubItems(1) = TxtCodigoCuenta
    LvContable.ListItems(Ip_C).SubItems(2) = TxtNombre
    LvContable.ListItems(Ip_C).SubItems(3) = TxtCodActivo
    LvContable.ListItems(Ip_C).SubItems(4) = TxtNombreActivo
    LvContable.ListItems(Ip_C).SubItems(5) = NumFormat(TxtDebito)
    LvContable.ListItems(Ip_C).SubItems(6) = NumFormat(TxtCredito)
    
    
    TxtCodigoCuenta = ""
    TxtNombre = ""
    TxtCredito = 0
    TxtDebito = 0
    TxtCodActivo = ""
    TxtNombreActivo = ""
    Tvoucher
    CmdBuscaActivo.Enabled = False
    TxtCodigoCuenta.SetFocus
End Sub
Private Sub Tvoucher()
    TxtDebe = NumFormat(TotalizaColumna(LvContable, "debe"))
    TxtHaber = NumFormat(TotalizaColumna(LvContable, "haber"))
End Sub

Private Sub cmdSalir_Click()
     On Error Resume Next
     Unload Me
End Sub

Private Sub Command1_Click()
 Shell "calc.exe"
End Sub

Private Sub Form_Load()
    Centrar Me
    Skin2 Me, , 7
    'Aplicar_skin Me
    
    SkRut = SP_Rut_Activo
    SkEmpresa = SP_Empresa_Activa
    Skconsininventario = SP_Control_Inventario
    
    TxtDia = Day(Date)
    TxtAno = IG_Ano_Contable
    TxtMes = MonthName(IG_Mes_Contable)
    LLenaYears Me.CboAno, 2010
    'For i = Year(Date) To 2010 Step -1
    'For i = Year(Date) To Year(Date) - 1 Step -1
    '    CboAno.AddItem i
    '    CboAno.ItemData(CboAno.ListCount - 1) = i
    'Next
    'Busca_Id_Combo CboAno, Val(IG_Ano_Contable)
    
    CboTipo.AddItem "INGRESO"
    CboTipo.AddItem "EGRESO"
    CboTipo.AddItem "TRASPASO"
    CboTipo.ItemData(0) = 1
    CboTipo.ItemData(1) = 2
    CboTipo.ItemData(2) = 3
    CboTipo.ListIndex = 0
    
    CboTipoNiif.AddItem "TRIBUTARIA-N.I.I.F"
    CboTipoNiif.AddItem "TRIBUTARIA"
    CboTipoNiif.AddItem "N.I.I.F."
    CboTipoNiif.ItemData(0) = 1
    CboTipoNiif.ItemData(1) = 2
    CboTipoNiif.ItemData(2) = 3
    CboTipoNiif.ListIndex = 0
    
    'Buscar ultimo vouchers
    UltimoV
    
    If DG_ID_Unico > 0 Then
        'Editando Voucher
        Sql = "SELECT vou_id, vou_numero,vou_dia,vou_mes_contable,vou_ano_contable,vou_glosa,vou_tipo,vou_tiponiif,vou_apertura " & _
              "FROM con_vouchers " & _
              "WHERE vou_id=" & DG_ID_Unico
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
            With RsTmp
                SkEditando = "EDITANDO..."
                tXTNuevo.Locked = False
                
                
                tXTNuevo = !vou_numero
                Sm_NroVOriginal = !vou_numero
                TxtDia = !vou_dia
                Busca_Id_Combo CboTipo, Val(!vou_tipo)
                Busca_Id_Combo CboTipoNiif, Val(!vou_tiponiif)
                TxtGlosa = !vou_glosa
                TxtAno = !vou_ano_contable
                TxtMes = MonthName(!vou_mes_contable)
                TxtMes.Tag = !vou_mes_contable
                CheckApertura.Value = IIf(!vou_apertura = "SI", 1, 0)
                Sql = "SELECT d.vod_id,d.pla_id,pla_nombre,0 aim_id,'',vod_debe,vod_haber " & _
                      "FROM con_vouchers_detalle d " & _
                      "INNER JOIN con_plan_de_cuentas p USING(pla_id) " & _
                      "/*LEFT JOIN con_activo_inmovilizado a ON d.aim_id=a.aim_id */" & _
                      "WHERE vou_id=" & RsTmp!vou_id
                Consulta RsTmp, Sql
                If RsTmp.RecordCount > 0 Then
                    LLenar_Grilla RsTmp, Me, Me.LvContable, False, True, True, False
                    
                    For i = 1 To LvContable.ListItems.Count
                        activos = ""
                        Sql = "SELECT aim_id,aim_valor " & _
                                    "FROM con_relacion_activos " & _
                                    "WHERE vou_id=" & DG_ID_Unico & " AND  vod_id=" & LvContable.ListItems(i)
                        Consulta RsTmp, Sql
                        If RsTmp.RecordCount > 0 Then
                            RsTmp.MoveFirst
                            Do While Not RsTmp.EOF
                                activos = activos & RsTmp!aim_id & "/" & RsTmp!aim_valor & ","
                                RsTmp.MoveNext
                            Loop
                            activos = Mid(activos, 1, Len(activos) - 1)
                            LvContable.ListItems(i).SubItems(3) = activos
                        End If
                    Next
                    
                    
                    If CheckApertura.Value = 1 Then
                        'SI el voucher es de apertura, comprobar si existe la cuenta cliente, proveedores, o AI
                        'si es que tiene algun detalle
                        For i = 1 To LvContable.ListItems.Count
                            If LvContable.ListItems(i).SubItems(1) = IG_IdCtaClientes Then
                                Sql = "SELECT rut_cliente,nombre_cliente,tipo_doc,no_documento,fecha,bruto,doc_id, " & _
                                        "(SELECT COUNT(t.id) " & _
                                            "FROM vi_documento_tiene_abonos t " & _
                                            "WHERE t.rut_emp = v.rut_emp " & _
                                                        "AND t.abo_rut=v.rut_cliente " & _
                                                        "AND t.id=v.id " & _
                                                        "AND t.abo_cli_pro='CLI') abonos,v.id, " & _
                                                        "CONCAT(sue_direccion,'-',sue_ciudad) suc_empresa,v.sue_id " & _
                                        "FROM ven_doc_venta v " & _
                                        "LEFT JOIN sis_empresas_sucursales e ON v.sue_id=e.sue_id " & _
                                        "WHERE rut_emp='" & SP_Rut_Activo & "' AND id IN(SELECT vdt_rel " & _
                                                            "FROM con_voucher_detalle_auxiliar " & _
                                                            "WHERE vdt_tipo='CLI' AND vod_id=" & LvContable.ListItems(i) & ")"
                                Consulta RsTmp, Sql
                                LLenar_Grilla RsTmp, Me, Me.LvDetalleC, False, True, True, False
                            ElseIf LvContable.ListItems(i).SubItems(1) = IG_IdCtaProveedores Then
                                Sql = "SELECT rut,nombre_proveedor,documento,no_documento,fecha,total,doc_id, " & _
                                            "(SELECT COUNT(t.id) " & _
                                            "FROM vi_documento_tiene_abonos t " & _
                                            "WHERE t.rut_emp = v.rut_emp " & _
                                                        "AND t.abo_rut=v.rut " & _
                                                        "AND t.id=v.id " & _
                                                        "AND t.abo_cli_pro='PRO') abonos,v.id " & _
                                        "FROM com_doc_compra v " & _
                                        "WHERE id IN(SELECT vdt_rel " & _
                                                            "FROM con_voucher_detalle_auxiliar " & _
                                                            "WHERE vdt_tipo='PRO' AND vod_id=" & LvContable.ListItems(i) & ")"
                                Consulta RsTmp, Sql
                                LLenar_Grilla RsTmp, Me, Me.LvDetalleP, False, True, True, False
                            
                            End If
                        
                        
                        Next
                    End If
                Else
                    LvContable.ListItems.Clear
                End If
            End With
        End If
        If Me.Sm_Centralizado = "SI" Then
            Me.tXTNuevo.Locked = True
            Me.CmdGrabarV.Enabled = False
            Me.TxtCodigoCuenta.Locked = True
            '****************************
            'XXXXXX
        End If
        
    Else
        'Nuevo
        
    
    
    End If
    Tvoucher
    
End Sub
Private Sub UltimoV()
    Sql = "SELECT IFNULL(MAX(vou_numero),0) nrov " & _
          "FROM con_vouchers " & _
          "WHERE rut_emp='" & SP_Rut_Activo & "'"
          
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        Me.TxtUltimo = RsTmp!nrov
    Else
        TxtUltimo = 0
    End If
    tXTNuevo = Val(TxtUltimo) + 1
    SkEditando = ""
    tXTNuevo.Locked = False
    TxtMes.Tag = IG_Mes_Contable
    TxtGlosa = ""
    TxtDebe = 0
    TxtHaber = 0
End Sub





Private Sub Form_Unload(Cancel As Integer)
    paso = Empty
    SG_codigo2 = Empty
End Sub

Private Sub LvContable_DblClick()
    
    If LvContable.SelectedItem Is Nothing Then Exit Sub
    If Me.Sm_Centralizado = "SI" Then Exit Sub
    
    Dim Sp_EnQLado As String
    
    TxtCodigoCuenta = LvContable.SelectedItem.SubItems(1)
    TxtNombre = LvContable.SelectedItem.SubItems(2)
    TxtCodActivo = LvContable.SelectedItem.SubItems(3)
    TxtNombreActivo = LvContable.SelectedItem.SubItems(4)
    
    TxtDebito = CDbl(LvContable.SelectedItem.SubItems(5))
    TxtCredito = CDbl(LvContable.SelectedItem.SubItems(6))
    LvContable.ListItems.Remove LvContable.SelectedItem.Index
    If Val(TxtCodActivo) > 0 Then Me.CmdBuscaActivo.Enabled = True
    
    
    'Si estamos ingresando un asiento de apertura
    ' y la cta corresponde a clientes o proveedores
    'Abrir nuevamente la venta pero con los datos
    'que se habian cargado 22 Junio 2013
    'IG_IdCtaProveedores Or Val(TxtCodigoCuenta) = IG_IdCtaClientes
    If CheckApertura.Value = 1 Then
        If Val(TxtDebito) > 0 Then Sp_EnQLado = "DEBITO"
        If Val(TxtCredito) > 0 Then Sp_EnQLado = "CREDITO"
        If Val(TxtCodigoCuenta) = IG_IdCtaClientes Then
            Con_AperturaCtasCtes.Sm_CliPro = "CLI"
            With Con_AperturaCtasCtes.LvDetalle
                For i = 1 To LvDetalleC.ListItems.Count
                    .ListItems.Add , , LvDetalleC.ListItems(i)
                    For X = 1 To LvDetalleC.ColumnHeaders.Count - 1
                        .ListItems(i).SubItems(X) = LvDetalleC.ListItems(i).SubItems(X)
                    Next
                Next
                
            End With
            Con_AperturaCtasCtes.Calcula
            Con_AperturaCtasCtes.Show 1
            If Sp_EnQLado = "DEBITO" Then TxtDebito = TotalizaColumna(LvDetalleC, "bruto")
            If Sp_EnQLado = "CREDITO" Then TxtCredito = TotalizaColumna(LvDetalleC, "bruto")
            CmdOk_Click
        End If
        
        If Val(TxtCodigoCuenta) = IG_IdCtaProveedores Then
            Con_AperturaCtasCtes.Sm_CliPro = "PRO"
            With Con_AperturaCtasCtes.LvDetalle
                For i = 1 To LvDetalleP.ListItems.Count
                    .ListItems.Add , , LvDetalleP.ListItems(i)
                    For X = 1 To LvDetalleP.ColumnHeaders.Count - 1
                        .ListItems(i).SubItems(X) = LvDetalleP.ListItems(i).SubItems(X)
                    Next
                Next
            End With
            Con_AperturaCtasCtes.Calcula
            Con_AperturaCtasCtes.Show 1
            If Sp_EnQLado = "DEBITO" Then TxtDebito = TotalizaColumna(LvDetalleP, "bruto")
            If Sp_EnQLado = "CREDITO" Then TxtCredito = TotalizaColumna(LvDetalleP, "bruto")
            CmdOk_Click
        End If
        
        
        'If Val(SG_codigo) > 0 Or SG_codigo = "0" Then
            'If Val(TxtCodigoCuenta) = IG_IdCtaProveedores Then TxtCredito = SG_codigo
            'If Val(TxtCodigoCuenta) = IG_IdCtaClientes Then TxtDebito = SG_codigo
         
        'End If
    End If
    
    
    
        
    
    
    
    
    
    Tvoucher
    TxtCodigoCuenta.SetFocus
End Sub

Private Sub Timer1_Timer()
    On Error Resume Next
    TxtDia.SetFocus
    Timer1.Enabled = False
End Sub
Private Sub TxtCodigoCuenta_GotFocus()
    En_Foco TxtCodigoCuenta
End Sub

Private Sub TxtCodigoCuenta_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
    If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtCodigoCuenta_Validate(Cancel As Boolean)
    If Val(TxtCodigoCuenta) = 0 Then Exit Sub
    Sql = "SELECT pla_nombre,tpo_id,det_id,pla_analisis " & _
            "FROM con_plan_de_cuentas " & _
            "WHERE pla_id=" & TxtCodigoCuenta
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        TxtNombre = RsTmp!pla_nombre
        
        'Aqui comprobamos si corresponde a Activo inmovilizado
        If RsTmp!tpo_id = 1 And RsTmp!det_id = 4 And RsTmp!pla_analisis = "SI" Then
            CmdBuscaActivo.Enabled = True
        Else
            CmdBuscaActivo.Enabled = False
        End If
        
        'Aqui comprobaremos si la empresa tiene sistema de bancos-'
        'y ademas comprobaremos si la cuenta corresponde a el o los bancos que esta utilizando -
        'la empresa
        Sql = "SELECT rut " & _
                 "FROM sis_empresas " & _
                    "WHERE rut='" & SP_Rut_Activo & "' AND emp_modulo_banco='SI'"
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
            Sql = "SELECT cte_id " & _
                    "FROM ban_cta_cte " & _
                    "WHERE rut_emp='" & SP_Rut_Activo & "' AND ban_id=" & Val(TxtCodigoCuenta)
            Consulta RsTmp, Sql
            If RsTmp.RecordCount > 0 Then
                If Me.CheckApertura.Value = 1 Then
                    '4 de Mayo 2013
                    'Asiento de apertura banco
                    
                    
                    
                    MsgBox "_Aqui preguntamos que saldo tendra esta cuenta(positiva-negativa)" & vbNewLine & " Y agregamos el movimientos como saldo inicial"
                    
                    
                    
                
                Else
                    
                
                    MsgBox "La cuenta corresponde a uno de los Bancos que utiliza la empresa..." & _
                        "Estos movimientos debe realizarse a traves del Modulo de Banco, y luego centralizar...", vbInformation
                        TxtCodigoCuenta = ""
                        TxtNombre = ""
                        Cancel = True
                        Exit Sub
                End If
            End If
        End If
                    
        'Vamos a comprobar si la cta corresponde a clientes o proveedores
        If Val(TxtCodigoCuenta) = IG_IdCtaProveedores Or Val(TxtCodigoCuenta) = IG_IdCtaClientes Then
            If MsgBox("Cuentas reservadas..." & vbNewLine & "�Desea ingresar Asiento de apertura...?", vbQuestion + vbOKCancel) = vbCancel Then Exit Sub
            If Val(TxtCodigoCuenta) = IG_IdCtaProveedores Then Con_AperturaCtasCtes.Sm_CliPro = "PRO"
            If Val(TxtCodigoCuenta) = IG_IdCtaClientes Then Con_AperturaCtasCtes.Sm_CliPro = "CLI"
            Con_AperturaCtasCtes.Show 1
            If Val(SG_codigo) > 0 Then
                If Val(TxtCodigoCuenta) = IG_IdCtaProveedores Then TxtCredito = SG_codigo
                If Val(TxtCodigoCuenta) = IG_IdCtaClientes Then TxtDebito = SG_codigo
                CmdOk_Click
                
            End If
        End If

        
        
    Else
        MsgBox "Cuenta no encontrada...", vbInformation
        Cancel = True
    End If
End Sub

Private Sub TxtCredito_GotFocus()
    En_Foco Me.ActiveControl
End Sub
Private Sub TxtCredito_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
End Sub

Private Sub TxtDebito_GotFocus()
    En_Foco Me.ActiveControl
End Sub

Private Sub TxtDebito_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
    If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtDia_GotFocus()
    En_Foco TxtDia
End Sub

Private Sub TxtDia_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
    If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtDia_Validate(Cancel As Boolean)
    If Val(TxtDia) > 31 Then
        MsgBox "Fecha no valida"
        TxtDia = 1
    End If
End Sub

Private Sub TxtGlosa_GotFocus()
    En_Foco TxtGlosa
End Sub
Private Sub TxtGlosa_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
    If KeyAscii = 39 Then KeyAscii = 0
End Sub
Private Function VoucherDisponible(Nro As Long) As Boolean
    Sql = "SELECT vou_id " & _
            "FROM con_vouchers " & _
            "WHERE vou_ano_contable=" & IG_Ano_Contable & " AND vou_mes_contable=" & IG_Mes_Contable & " AND  vou_numero=" & Nro & " AND rut_emp='" & SP_Rut_Activo & "'"
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        VoucherDisponible = False
    Else
        VoucherDisponible = True
    End If
End Function

Private Sub TxtGlosa_Validate(Cancel As Boolean)
TxtGlosa = Replace(TxtGlosa, "'", "")
End Sub

Private Sub tXTNuevo_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
    If KeyAscii = 39 Then KeyAscii = 0
End Sub
Private Function CreaProductoAsientoApertura() As String

    Sql = "SELECT (SELECT mar_id FROM par_marcas WHERE rut_emp='" & SP_Rut_Activo & "' LIMIT 1) mar_id," & _
                    "(SELECT ume_id FROM sis_unidad_medida WHERE rut_emp='" & SP_Rut_Activo & "' LIMIT 1) ume_id," & _
                    "(SELECT tip_id FROM par_tipos_productos WHERE rut_emp='" & SP_Rut_Activo & "' LIMIT 1) tip_id"
    Consulta RsTmp, Sql
    Lp_NewId = UltimoNro("maestro_productos", "codigo")
    
    Sql = "INSERT INTO maestro_productos (" & _
          "codigo,marca,descripcion,precio_compra,porciento_utilidad,precio_venta,margen," & _
          "stock_actual,stock_critico,ubicacion_bodega,comentario,bod_id,mar_id,tip_id,pro_inventariable," & _
          "rut_emp,ume_id,pro_precio_sin_flete,pro_precio_flete,pro_activo) " & _
          "VALUES (" & Lp_NewId & ",'ASI. APERTURA','ASI. PARA APERTURA',1,1," & _
          "2,1,0,0,'SIN UBICACION','SOLO APERTURA',0" & _
           "," & RsTmp!mar_id & "," & RsTmp!tip_id & ",'NO'," & _
           "'" & SP_Rut_Activo & "'," & RsTmp!ume_id & ",1,1,'NO')"
    cn.Execute Sql

    CreaProductoAsientoApertura = Str(Lp_NewId)

End Function

Private Sub InsertaProductoAsientoApertura(Codigo As String, PrecioFinal As Long, nro_Doc As Long, Id_Doc As Integer, CliPro As String, IdUnico As Long, Inventario As String)
    If CliPro = "CLI" Then
        If Inventario = "SI" Then
            Sql = "INSERT INTO ven_detalle (codigo,marca,descripcion,precio_real," & _
                               "descuento,unidades,precio_final,subtotal,precio_costo,btn_especial," & _
                               "comision,fecha,no_documento,doc_id,rut_emp,pla_id,are_id,cen_id,aim_id," & _
                               "ved_precio_venta_bruto,ved_precio_venta_neto) VALUES"
              sql2 = "('" & _
                               Codigo & "','ASI. APERTURA','ASI. PARA APERTURA'," & _
                              1 & "," & 0 & "," & _
                               1 & "," & PrecioFinal & "," & _
                               PrecioFinal & "," & 1 & ",'" & _
                               "NO','NO','" & Fql(Date) & "'," & _
                               nro_Doc & "," & Id_Doc & ",'" & SP_Rut_Activo & "'," & _
                              0 & "," & 0 & "," & 0 & "," & _
                              0 & "," & 0 & "," & 0 & ")"
        Else
            'sin inventario
            Sql = "INSERT INTO ven_sin_detalle (id,vsd_neto,pla_id,cen_id,are_id) "
            sql2 = "VALUES(" & IdUnico & "," & PrecioFinal & ",99999,99999,99999)"
        End If
    Else
            Sql = "INSERT INTO com_doc_compra_detalle (id,pro_codigo,cmd_detalle,cmd_unitario_bruto," & _
                               "cmd_total_bruto,cmd_cantidad) VALUES"
            sql2 = "(" & _
                               IdUnico & ",'" & Codigo & "','ASI. PARA APERTURA'," & _
                               PrecioFinal & "," & _
                               PrecioFinal & ",1)"
    End If
    cn.Execute Sql & sql2
End Sub
