VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{DA729E34-689F-49EA-A856-B57046630B73}#1.0#0"; "progressbar-xp.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form con_PlanDeCuentas 
   Caption         =   "Plan de cuentas"
   ClientHeight    =   6960
   ClientLeft      =   165
   ClientTop       =   1710
   ClientWidth     =   15285
   LinkTopic       =   "Form1"
   ScaleHeight     =   6960
   ScaleWidth      =   15285
   Begin VB.PictureBox FrmDetalles 
      BackColor       =   &H00C0C000&
      Height          =   2910
      Left            =   1995
      ScaleHeight     =   2850
      ScaleWidth      =   6585
      TabIndex        =   28
      Top             =   195
      Visible         =   0   'False
      Width           =   6645
      Begin VB.CommandButton CmdCierraDetalleCheques 
         Caption         =   "x"
         Height          =   255
         Left            =   6315
         TabIndex        =   29
         ToolTipText     =   "Cierra detalle de cheques"
         Top             =   15
         Width           =   255
      End
      Begin MSComctlLib.ListView LvCuentas 
         Height          =   2280
         Left            =   75
         TabIndex        =   30
         ToolTipText     =   "Este es el detalle de cheque(s)"
         Top             =   435
         Width           =   6360
         _ExtentX        =   11218
         _ExtentY        =   4022
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   0   'False
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   3
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "N109"
            Text            =   "Id"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "T1000"
            Text            =   "Nombre de cuenta"
            Object.Width           =   5292
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "T1000"
            Text            =   "Tipo de Cuenta"
            Object.Width           =   3528
         EndProperty
      End
      Begin VB.Label Label1 
         BackStyle       =   0  'Transparent
         Caption         =   "Cuentas reservadas"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   -1  'True
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   75
         TabIndex        =   31
         Top             =   15
         Width           =   3015
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H80000003&
         BackStyle       =   1  'Opaque
         BorderStyle     =   0  'Transparent
         Height          =   285
         Left            =   -45
         Top             =   1605
         Width           =   8220
      End
   End
   Begin VB.CommandButton CmdReservadas 
      Caption         =   "Cuentas Reservadas"
      Height          =   255
      Left            =   270
      TabIndex        =   27
      Top             =   510
      Width           =   1650
   End
   Begin VB.Frame FraProgreso 
      Height          =   540
      Left            =   2085
      TabIndex        =   20
      Top             =   6315
      Visible         =   0   'False
      Width           =   8835
      Begin Proyecto2.XP_ProgressBar pbar 
         Height          =   255
         Left            =   150
         TabIndex        =   21
         Top             =   195
         Width           =   8565
         _ExtentX        =   15108
         _ExtentY        =   450
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BrushStyle      =   0
         Color           =   16777088
         Scrolling       =   1
         ShowText        =   -1  'True
      End
   End
   Begin VB.CommandButton cmdExportar 
      Caption         =   "Exportar a Excel"
      Height          =   360
      Left            =   255
      TabIndex        =   19
      Top             =   6390
      Width           =   1320
   End
   Begin VB.CommandButton CmdSalir 
      Cancel          =   -1  'True
      Caption         =   "&Retornar"
      Height          =   435
      Left            =   11490
      TabIndex        =   9
      Top             =   6450
      Width           =   1395
   End
   Begin VB.Timer Timer1 
      Interval        =   100
      Left            =   12750
      Top             =   -165
   End
   Begin VB.Frame FrmMantenedor 
      Caption         =   "Plan de cuentas"
      Height          =   5415
      Left            =   120
      TabIndex        =   0
      Top             =   840
      Width           =   14985
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
         Height          =   375
         Left            =   6525
         OleObjectBlob   =   "con_PlanDeCuentas.frx":0000
         TabIndex        =   34
         Top             =   4380
         Width           =   5835
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   30
         Left            =   10290
         OleObjectBlob   =   "con_PlanDeCuentas.frx":00D2
         TabIndex        =   33
         Top             =   3900
         Width           =   30
      End
      Begin VB.ComboBox CboCtaDepAcum 
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         Height          =   315
         ItemData        =   "con_PlanDeCuentas.frx":014C
         Left            =   11370
         List            =   "con_PlanDeCuentas.frx":0156
         Style           =   2  'Dropdown List
         TabIndex        =   32
         Top             =   555
         Width           =   3210
      End
      Begin VB.CommandButton CmdX 
         Caption         =   "x"
         Height          =   210
         Left            =   5235
         TabIndex        =   26
         ToolTipText     =   "Todas las cuentas"
         Top             =   4215
         Width           =   195
      End
      Begin VB.CommandButton CmdBusca 
         Caption         =   "Buscar"
         Height          =   300
         Left            =   4440
         TabIndex        =   25
         ToolTipText     =   "Buscar los datos ingresados"
         Top             =   4170
         Width           =   765
      End
      Begin VB.TextBox TxtBuscaCuenta 
         Height          =   315
         Left            =   1065
         TabIndex        =   23
         Tag             =   "T"
         ToolTipText     =   "Ingrese datos para buscar"
         Top             =   4155
         Width           =   3375
      End
      Begin VB.Frame Frame1 
         Caption         =   "Ayuda"
         Height          =   630
         Left            =   495
         TabIndex        =   17
         Top             =   4560
         Width           =   11820
         Begin ACTIVESKINLibCtl.SkinLabel SkInfo 
            Height          =   210
            Left            =   45
            OleObjectBlob   =   "con_PlanDeCuentas.frx":0162
            TabIndex        =   18
            Top             =   255
            Width           =   11565
         End
      End
      Begin VB.ComboBox CboAnalisis 
         Appearance      =   0  'Flat
         Height          =   315
         ItemData        =   "con_PlanDeCuentas.frx":01C5
         Left            =   4230
         List            =   "con_PlanDeCuentas.frx":01CF
         Style           =   2  'Dropdown List
         TabIndex        =   2
         Top             =   555
         Width           =   915
      End
      Begin VB.ComboBox CboDetalleCuenta 
         Appearance      =   0  'Flat
         Height          =   315
         ItemData        =   "con_PlanDeCuentas.frx":01DB
         Left            =   7800
         List            =   "con_PlanDeCuentas.frx":01E5
         Style           =   2  'Dropdown List
         TabIndex        =   4
         Top             =   555
         Width           =   2670
      End
      Begin VB.ComboBox CboTipoCuenta 
         Appearance      =   0  'Flat
         Height          =   315
         ItemData        =   "con_PlanDeCuentas.frx":01F1
         Left            =   5130
         List            =   "con_PlanDeCuentas.frx":01FB
         Style           =   2  'Dropdown List
         TabIndex        =   3
         Top             =   555
         Width           =   2670
      End
      Begin VB.CommandButton CmdOk 
         Caption         =   "Ok"
         Height          =   315
         Left            =   14565
         TabIndex        =   6
         Top             =   525
         Width           =   330
      End
      Begin VB.TextBox TxtNombre 
         Height          =   315
         Left            =   780
         TabIndex        =   1
         Tag             =   "T"
         Top             =   555
         Width           =   3465
      End
      Begin VB.ComboBox CboActivo 
         Appearance      =   0  'Flat
         Height          =   315
         ItemData        =   "con_PlanDeCuentas.frx":0207
         Left            =   10470
         List            =   "con_PlanDeCuentas.frx":0211
         Style           =   2  'Dropdown List
         TabIndex        =   5
         Top             =   555
         Width           =   915
      End
      Begin VB.TextBox TxtId 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         Height          =   315
         Left            =   150
         Locked          =   -1  'True
         TabIndex        =   10
         TabStop         =   0   'False
         Tag             =   "T"
         Top             =   555
         Width           =   660
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   255
         Left            =   5670
         OleObjectBlob   =   "con_PlanDeCuentas.frx":021D
         TabIndex        =   7
         Top             =   4170
         Width           =   6675
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   255
         Index           =   3
         Left            =   750
         OleObjectBlob   =   "con_PlanDeCuentas.frx":031B
         TabIndex        =   11
         Top             =   390
         Width           =   1095
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel5 
         Height          =   255
         Index           =   2
         Left            =   10440
         OleObjectBlob   =   "con_PlanDeCuentas.frx":0385
         TabIndex        =   12
         Top             =   360
         Width           =   1050
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   255
         Index           =   5
         Left            =   150
         OleObjectBlob   =   "con_PlanDeCuentas.frx":03FB
         TabIndex        =   13
         Top             =   390
         Width           =   615
      End
      Begin MSComctlLib.ListView LvDetalle 
         Height          =   3225
         Left            =   150
         TabIndex        =   8
         Top             =   855
         Width           =   14685
         _ExtentX        =   25903
         _ExtentY        =   5689
         View            =   3
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   10
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "N109"
            Text            =   "Id"
            Object.Width           =   1147
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "T3000"
            Text            =   "Nombre"
            Object.Width           =   6085
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "T800"
            Text            =   "Analisis"
            Object.Width           =   1614
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Object.Tag             =   "T2000"
            Text            =   "Tipo"
            Object.Width           =   4710
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Object.Tag             =   "T2000"
            Text            =   "Detalle"
            Object.Width           =   4710
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Object.Tag             =   "T800"
            Text            =   "Activo"
            Object.Width           =   1552
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   6
            Object.Tag             =   "N109"
            Text            =   "tp"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   7
            Object.Tag             =   "N109"
            Text            =   "det i"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   8
            Object.Tag             =   "T1000"
            Text            =   "Cuenta Relacionada de Dep. Acumulada"
            Object.Width           =   5556
         EndProperty
         BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   9
            Object.Tag             =   "N109"
            Text            =   "id cta dep acum"
            Object.Width           =   0
         EndProperty
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel5 
         Height          =   255
         Index           =   0
         Left            =   4230
         OleObjectBlob   =   "con_PlanDeCuentas.frx":045D
         TabIndex        =   14
         Top             =   360
         Width           =   735
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel5 
         Height          =   255
         Index           =   1
         Left            =   5145
         OleObjectBlob   =   "con_PlanDeCuentas.frx":04CB
         TabIndex        =   15
         Top             =   360
         Width           =   2520
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel5 
         Height          =   255
         Index           =   3
         Left            =   7800
         OleObjectBlob   =   "con_PlanDeCuentas.frx":0545
         TabIndex        =   16
         Top             =   360
         Width           =   2520
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   255
         Index           =   0
         Left            =   525
         OleObjectBlob   =   "con_PlanDeCuentas.frx":05D7
         TabIndex        =   24
         Top             =   4170
         Width           =   630
      End
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   0
      OleObjectBlob   =   "con_PlanDeCuentas.frx":0641
      Top             =   0
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkEmpresaActiva 
      Height          =   285
      Left            =   5415
      OleObjectBlob   =   "con_PlanDeCuentas.frx":0875
      TabIndex        =   22
      Top             =   510
      Width           =   5685
   End
End
Attribute VB_Name = "con_PlanDeCuentas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public B_Editable As Boolean, Sm_filtro As String
Dim Sp_TipoDepreciacion As String
Private Sub CboAnalisis_GotFocus()
    SkInfo = "Seleccione si la cuenta corresponde a Analisis."
End Sub

Private Sub CboAnalisis_LostFocus()
    SkInfo = Empty
End Sub



Private Sub CboDetalleCuenta_Click()
    VericaCtaActivoFijo
End Sub

Private Sub CboTipoCuenta_Click()
    If CboTipoCuenta.ListIndex > -1 Then
        LLenarCombo CboDetalleCuenta, "det_nombre", "det_id", "con_detalle_tipo_cuenta", "det_activo='SI' AND tpo_id=" & CboTipoCuenta.ItemData(CboTipoCuenta.ListIndex), "det_id"
        
    End If
    VericaCtaActivoFijo
End Sub
Private Sub VericaCtaActivoFijo()
    Me.CboCtaDepAcum.Enabled = False
    If CboTipoCuenta.ListIndex = -1 Then Exit Sub
    If Me.CboDetalleCuenta.ListIndex = -1 Then Exit Sub
    
    If CboTipoCuenta.ItemData(CboTipoCuenta.ListIndex) = 1 And CboDetalleCuenta.ItemData(CboDetalleCuenta.ListIndex) = 4 Then CboCtaDepAcum.Enabled = True

        
    
End Sub
Private Sub CmdBusca_Click()
    If Len(TxtBuscaCuenta) = 3 Then
        MsgBox "Ingrese mas caracteres para la busqueda...", vbInformation
        TxtBuscaCuenta.SetFocus
        Exit Sub
    End If
    Sm_filtro = " AND pla_nombre  LIKE '%" & TxtBuscaCuenta & "%' "
    GrillaPlan
        
        
End Sub

Private Sub CmdCierraDetalleCheques_Click()
    FrmDetalles.Visible = False
End Sub

Private Sub cmdExportar_Click()
Dim tit(2) As String
    FraProgreso.Visible = True
    tit(0) = Me.Caption
    tit(1) = "FECHA INFORME: " & Date & " - " & Time
    tit(2) = "" ' "                                        DESDE:" & DtInicio.Value & "   HASTA:" & Dtfin.Value
    ExportarNuevo LvDetalle, tit, Me, PBar
    PBar.Value = 1
    FraProgreso.Visible = False
End Sub

Private Sub CmdOk_Click()
    Dim Lp_CtaDepAcum As Long
    If Len(TxtNombre) = 0 Then
        MsgBox "Debe ingresar nombre de cuenta...", vbInformation
        TxtNombre.SetFocus
        Exit Sub
    End If
    If CboAnalisis.ListIndex = -1 Then
        MsgBox "Seleccionar si la cuenta es de analisis...", vbInformation
        CboAnalisis.SetFocus
        Exit Sub
    End If
    If CboTipoCuenta.ListIndex = -1 Then
        MsgBox "Seleccione tipo de cuenta", vbInformation
        CboTipoCuenta.SetFocus
        Exit Sub
    End If
    If Me.CboDetalleCuenta.ListIndex = -1 Then
        MsgBox "Seleccione detalle de la cuenta...", vbInformation
        CboDetalleCuenta.SetFocus
        Exit Sub
    End If
    
    If Sp_TipoDepreciacion = "INDIRECTA" Then
        '19 Abril 2016 _
        Si es DIRECTA DEBE PERMITIR GRABAR SIN ESTE DATO
        If CboCtaDepAcum.Enabled And Me.CboCtaDepAcum.ListIndex = -1 Then
            MsgBox "Seleccione o cree Cuenta Dep. Acumulada para esta cuenta de Activo Fijo...", vbInformation
            CboCtaDepAcum.SetFocus
            Exit Sub
        End If
    End If
    'Comprobamos si existe una cuenta con el mismo nombre
    If Sp_TipoDepreciacion = "INDIRECTA" Then
        If CboCtaDepAcum.Enabled Then
            Lp_CtaDepAcum = CboCtaDepAcum.ItemData(CboCtaDepAcum.ListIndex)
        Else
            Lp_CtaDepAcum = 0
        End If
    Else
        Lp_CtaDepAcum = 0
    End If
   
   
    If Val(TxtId) <> 0 Then
        Sql = "SELECT pla_id " & _
              "FROM con_plan_de_cuentas " & _
              "WHERE pla_nombre='" & TxtNombre & "' AND pla_id NOT IN(" & TxtId & ")"
    Else
        Sql = "SELECT pla_id " & _
              "FROM con_plan_de_cuentas " & _
              "WHERE pla_nombre='" & TxtNombre & "'"
    
    
    End If
    Consulta RsTmp, Sql
    
    
    
    
    
    If RsTmp.RecordCount > 0 Then
        MsgBox "Ya existe una cuenta con este nombre ...", vbOKOnly + vbInformation
        TxtNombre.SetFocus
        Exit Sub
    End If
    If Val(TxtId) <> 0 Then
        Sql = "SELECT pla_id " & _
              "FROM con_plan_de_cuentas " & _
              "WHERE pla_id=" & TxtId & " AND pla_id IN(" & SP_Cuentas_Reservadas & ")"
        Consulta RsTmp, Sql
        
        If RsTmp.RecordCount > 0 Then
            MsgBox "Cuenta Reservada del sistema ..." & vbNewLine & "Debe conservar este nombre....", vbInformation
            Limpia
            CboTipoCuenta.ListIndex = -1
            CboDetalleCuenta.ListIndex = -1
            TxtNombre.SetFocus
            Exit Sub
        End If
    End If
  '
    
    If Val(TxtId) = 0 Then
        'Agregar nueva cuenta
        Sql = "INSERT INTO con_plan_de_cuentas (pla_nombre,tpo_id,det_id,pla_analisis,pla_activo,pla_dep_acumulada) " & _
                                     "VALUES('" & TxtNombre & "'," & CboTipoCuenta.ItemData(CboTipoCuenta.ListIndex) & "," & _
                                     CboDetalleCuenta.ItemData(CboDetalleCuenta.ListIndex) & ",'" & CboAnalisis.Text & "','" & _
                                     CboActivo.Text & "'," & Lp_CtaDepAcum & ")"
    Else
        'Actualizar cuenta
        Sql = " UPDATE con_plan_de_cuentas SET pla_nombre='" & TxtNombre & "'," & _
                                              "pla_analisis='" & CboAnalisis.Text & "'," & _
                                              "tpo_id=" & CboTipoCuenta.ItemData(CboTipoCuenta.ListIndex) & "," & _
                                              "det_id=" & CboDetalleCuenta.ItemData(CboDetalleCuenta.ListIndex) & "," & _
                                              "pla_dep_acumulada=" & Lp_CtaDepAcum & ", " & _
                                              "pla_activo='" & CboActivo.Text & "' " & _
            "WHERE pla_id=" & TxtId
    End If
    Consulta RsTmp, Sql
    Limpia
    GrillaPlan
End Sub
Private Sub Limpia()
    TxtId = Empty
    TxtNombre = Empty
    CboAnalisis.ListIndex = -1
End Sub

Private Sub CmdReservadas_Click()
    Sql = "SELECT pla_id,pla_nombre,t.tpo_nombre " & _
           "FROM con_plan_de_cuentas p " & _
           "INNER JOIN con_tipo_de_cuenta t USING(tpo_id) " & _
           "WHERE p.pla_id IN(" & SP_Cuentas_Reservadas & ")"
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvCuentas, False, True, True, False
    FrmDetalles.Visible = True
    LvCuentas.SetFocus
End Sub

Private Sub CmdSalir_Click()
    Unload Me
End Sub



Private Sub CmdX_Click()
    TxtBuscaCuenta = Empty
    Sm_filtro = Empty
    GrillaPlan
End Sub

Private Sub Form_Load()
    B_Editable = True
    Centrar Me
    
    Sql = "SELECT emp_tipo_depreciacion " & _
            "FROM sis_empresas " & _
            "WHERE rut='" & SP_Rut_Activo & "'"
    Consulta RsTmp, Sql
    Sp_TipoDepreciacion = "INDIRECTA"
    If RsTmp.RecordCount > 0 Then
        Sp_TipoDepreciacion = RsTmp!emp_tipo_depreciacion
    End If
    
    SkEmpresaActiva = "EMPRESA ACTIVA: " & SP_Empresa_Activa
    Aplicar_skin Me
    CboActivo.ListIndex = 0
    LLenarCombo CboTipoCuenta, "tpo_nombre", "tpo_id", "con_tipo_de_cuenta", "1=1", "tpo_id"
    LLenarCombo Me.CboCtaDepAcum, "pla_nombre", "pla_id", "con_plan_de_cuentas", "pla_activo='SI' AND tpo_id=2 AND det_id<>8", "pla_nombre"
    Sm_filtro = Empty
    GrillaPlan
End Sub
Private Sub GrillaPlan()
    
    Sql = "SELECT pla_id,pla_nombre,pla_analisis,tpo_nombre,det_nombre,pla_activo,p.tpo_id,p.det_id, " & _
            "(SELECT px.pla_nombre FROM con_plan_de_cuentas px WHERE px.pla_id=p.pla_dep_acumulada) pla_nombre_acum,pla_dep_acumulada " & _
          "FROM con_plan_de_cuentas p,con_tipo_de_cuenta t,con_detalle_tipo_cuenta d " & _
          "WHERE p.tpo_id=t.tpo_id AND p.det_id=d.det_id " & Sm_filtro & " " & _
          "ORDER BY p.tpo_id,p.det_id"
    Consulta RsTmp2, Sql
    LLenar_Grilla RsTmp2, Me, LvDetalle, False, True, True, False
End Sub

Private Sub LvCuentas_LostFocus()
    FrmDetalles.Visible = False
End Sub

Private Sub LvDetalle_DblClick()
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    With LvDetalle.SelectedItem
        TxtId = .Text
     '   Sql = "SELECT pla_id " & _
              "FROM con_plan_de_cuentas " & _
              "WHERE pla_id=
        TxtNombre = .SubItems(1)
        CboAnalisis.ListIndex = IIf(.SubItems(2) = "SI", 0, 1)
        CboActivo.ListIndex = IIf(.SubItems(5) = "SI", 0, 1)
        Busca_Id_Combo CboTipoCuenta, .SubItems(6)
        Busca_Id_Combo CboDetalleCuenta, .SubItems(7)
        Busca_Id_Combo Me.CboCtaDepAcum, .SubItems(9)
        TxtNombre.SetFocus
    End With
End Sub
'Instrucción que ordena columnas
Private Sub LvDetalle_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    ordListView ColumnHeader, Me, LvDetalle
End Sub





Private Sub Timer1_Timer()
    If B_Editable Then
        CmdOk.Enabled = True
    Else
        CmdOk.Enabled = False
    End If
    TxtNombre.SetFocus
    Timer1.Enabled = False
End Sub

Private Sub TxtBuscaCuenta_GotFocus()
    En_Foco TxtBuscaCuenta
End Sub

Private Sub TxtBuscaCuenta_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub TxtNombre_GotFocus()
    En_Foco TxtNombre
    SkInfo = "CLIENTES-PROVEEDORES-IVA DEBITO FISCAL-IVA CREDITO FISCAL.....Son Cuentas propias del Sistema...."
End Sub

Private Sub TxtNombre_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
    If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtNombre_LostFocus()
    SkInfo = Empty
End Sub
