VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{DA729E34-689F-49EA-A856-B57046630B73}#1.0#0"; "progressbar-xp.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form Con_Depreciacion 
   Caption         =   "Depreciaciones"
   ClientHeight    =   10320
   ClientLeft      =   2430
   ClientTop       =   1590
   ClientWidth     =   15090
   LinkTopic       =   "Form1"
   ScaleHeight     =   10320
   ScaleWidth      =   15090
   Begin VB.VScrollBar VScroll1 
      Height          =   4635
      LargeChange     =   300
      Left            =   14820
      SmallChange     =   100
      TabIndex        =   46
      Top             =   5925
      Width           =   300
   End
   Begin VB.Frame Frame6 
      Caption         =   "Vender Activo"
      Height          =   1935
      Left            =   7170
      TabIndex        =   15
      Top             =   90
      Width           =   7515
      Begin VB.TextBox txtExento 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   5310
         Locked          =   -1  'True
         TabIndex        =   54
         TabStop         =   0   'False
         Text            =   "0"
         ToolTipText     =   "Nro de Documento"
         Top             =   1620
         Visible         =   0   'False
         Width           =   1365
      End
      Begin VB.TextBox txtElTotal 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   3480
         TabIndex        =   52
         TabStop         =   0   'False
         ToolTipText     =   "Nro de Documento"
         Top             =   1530
         Width           =   1365
      End
      Begin VB.TextBox txtElIVA 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   2100
         TabIndex        =   50
         TabStop         =   0   'False
         ToolTipText     =   "Nro de Documento"
         Top             =   1530
         Width           =   1365
      End
      Begin VB.TextBox TxtValorBruto 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   735
         TabIndex        =   29
         ToolTipText     =   "Nro de Documento"
         Top             =   1530
         Width           =   1365
      End
      Begin VB.CommandButton cmdBtnRut 
         Caption         =   "Rut"
         Height          =   225
         Left            =   1065
         TabIndex        =   28
         Top             =   1005
         Width           =   585
      End
      Begin VB.TextBox txtCliente 
         BackColor       =   &H00E0E0E0&
         Height          =   315
         Left            =   3240
         Locked          =   -1  'True
         TabIndex        =   27
         Top             =   960
         Width           =   4065
      End
      Begin VB.TextBox TxtRut 
         BackColor       =   &H0080FF80&
         Enabled         =   0   'False
         Height          =   315
         Left            =   1665
         TabIndex        =   26
         ToolTipText     =   "Ingrese el RUT sin puntos ni guion."
         Top             =   975
         Width           =   1560
      End
      Begin VB.ComboBox CboDocVenta 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         ItemData        =   "Con_Depreciacion.frx":0000
         Left            =   1665
         List            =   "Con_Depreciacion.frx":0002
         Style           =   2  'Dropdown List
         TabIndex        =   23
         ToolTipText     =   "Seleccione Documento de  venta. Ventas con Retenci�n, cuando el contribuyente recibe factura de terceros "
         Top             =   645
         Width           =   2370
      End
      Begin VB.TextBox TxtNroDocumento 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   5430
         TabIndex        =   22
         ToolTipText     =   "Nro de Documento"
         Top             =   660
         Width           =   1920
      End
      Begin VB.TextBox txtFactor 
         BackColor       =   &H8000000F&
         Height          =   285
         Left            =   5430
         Locked          =   -1  'True
         TabIndex        =   21
         TabStop         =   0   'False
         Top             =   360
         Width           =   1920
      End
      Begin VB.ComboBox CboMesContable 
         Height          =   315
         Left            =   1665
         Style           =   2  'Dropdown List
         TabIndex        =   17
         Top             =   330
         Width           =   2370
      End
      Begin VB.CommandButton CmdVende 
         Caption         =   "Calcular Asientos Venta"
         Height          =   300
         Left            =   5025
         TabIndex        =   16
         Top             =   1575
         Width           =   2085
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   255
         Left            =   420
         OleObjectBlob   =   "Con_Depreciacion.frx":0004
         TabIndex        =   19
         Top             =   405
         Width           =   1185
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   255
         Left            =   4275
         OleObjectBlob   =   "Con_Depreciacion.frx":007A
         TabIndex        =   20
         Top             =   375
         Width           =   1110
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel5 
         Height          =   210
         Left            =   4125
         OleObjectBlob   =   "Con_Depreciacion.frx":00E4
         TabIndex        =   24
         Top             =   660
         Width           =   1260
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel6 
         Height          =   210
         Left            =   45
         OleObjectBlob   =   "Con_Depreciacion.frx":015E
         TabIndex        =   25
         Top             =   675
         Width           =   1515
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel7 
         Height          =   210
         Index           =   0
         Left            =   810
         OleObjectBlob   =   "Con_Depreciacion.frx":01E0
         TabIndex        =   30
         Top             =   1335
         Width           =   1260
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel7 
         Height          =   210
         Index           =   1
         Left            =   2175
         OleObjectBlob   =   "Con_Depreciacion.frx":0246
         TabIndex        =   51
         Top             =   1335
         Width           =   1260
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel7 
         Height          =   210
         Index           =   2
         Left            =   3555
         OleObjectBlob   =   "Con_Depreciacion.frx":02AA
         TabIndex        =   53
         Top             =   1320
         Width           =   1260
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel7 
         Height          =   210
         Index           =   3
         Left            =   5475
         OleObjectBlob   =   "Con_Depreciacion.frx":0312
         TabIndex        =   55
         Top             =   1365
         Visible         =   0   'False
         Width           =   1260
      End
   End
   Begin VB.Frame FraProgreso 
      Caption         =   "Progreso exportaci�n"
      Height          =   795
      Left            =   1785
      TabIndex        =   13
      Top             =   3390
      Visible         =   0   'False
      Width           =   11430
      Begin Proyecto2.XP_ProgressBar BarraProgreso 
         Height          =   330
         Left            =   150
         TabIndex        =   14
         Top             =   315
         Width           =   11130
         _ExtentX        =   19632
         _ExtentY        =   582
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BrushStyle      =   0
         Color           =   16777088
         Scrolling       =   1
         ShowText        =   -1  'True
      End
   End
   Begin VB.CommandButton CmdExportar 
      Caption         =   "&Exportar"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   270
      Left            =   975
      TabIndex        =   12
      ToolTipText     =   "Exportar planilla a Excel"
      Top             =   5040
      Width           =   2490
   End
   Begin VB.CommandButton CmdSalir 
      Caption         =   "Retornar"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   390
      Left            =   13245
      TabIndex        =   11
      Top             =   5400
      Width           =   1455
   End
   Begin VB.ComboBox CboCuentaPasivo 
      Height          =   315
      ItemData        =   "Con_Depreciacion.frx":037A
      Left            =   6345
      List            =   "Con_Depreciacion.frx":037C
      Style           =   2  'Dropdown List
      TabIndex        =   9
      Top             =   225
      Visible         =   0   'False
      Width           =   3465
   End
   Begin MSComctlLib.ListView LvDep 
      Height          =   2565
      Left            =   1005
      TabIndex        =   7
      Top             =   2445
      Width           =   13500
      _ExtentX        =   23813
      _ExtentY        =   4524
      View            =   3
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      Checkboxes      =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   0
      NumItems        =   22
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Object.Tag             =   "N109"
         Text            =   "Id"
         Object.Width           =   1058
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Object.Tag             =   "F1000"
         Text            =   "Fecha Ingreso"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Object.Tag             =   "T1000"
         Text            =   "Cuenta"
         Object.Width           =   4410
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Object.Tag             =   "T3000"
         Text            =   "Nombre"
         Object.Width           =   4410
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   4
         Key             =   "inicial"
         Object.Tag             =   "N100"
         Text            =   "Valor Inicial"
         Object.Width           =   2258
      EndProperty
      BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   2
         SubItemIndex    =   5
         Object.Tag             =   "N103"
         Text            =   "Factor"
         Object.Width           =   2249
      EndProperty
      BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   6
         Key             =   "actualizado"
         Object.Tag             =   "N100"
         Text            =   "Valor Actualizado"
         Object.Width           =   1799
      EndProperty
      BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   7
         Key             =   "dacum"
         Object.Tag             =   "N100"
         Text            =   "Depreciacion Acumulada"
         Object.Width           =   2805
      EndProperty
      BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   2
         SubItemIndex    =   8
         Object.Tag             =   "N103"
         Text            =   "Factor"
         Object.Width           =   1614
      EndProperty
      BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   9
         Key             =   "dactual"
         Object.Tag             =   "N100"
         Text            =   "Deprec Actualizada"
         Object.Width           =   2275
      EndProperty
      BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   10
         Key             =   "vdepreciar"
         Object.Tag             =   "N100"
         Text            =   "Valor a depreciar"
         Object.Width           =   1958
      EndProperty
      BeginProperty ColumnHeader(12) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   11
         Object.Tag             =   "N100"
         Text            =   "Meses a depreciar"
         Object.Width           =   1764
      EndProperty
      BeginProperty ColumnHeader(13) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   12
         Object.Tag             =   "N100"
         Text            =   "Vida util"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(14) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   13
         Key             =   "dano"
         Object.Tag             =   "N100"
         Text            =   "Depreciacion del a�o"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(15) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   14
         Text            =   "Pla_Id"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(16) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   15
         Key             =   "tcreditoactivofijo"
         Object.Tag             =   "N100"
         Text            =   "Credito Activo Nuevo"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(17) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   16
         Object.Tag             =   "N109"
         Text            =   "Actualizacion Activo"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(18) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   17
         Object.Tag             =   "N109"
         Text            =   "Id Cuenta Dep Acumulada"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(19) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   18
         Object.Tag             =   "T1000"
         Text            =   "nuevo o viejo"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(20) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   19
         Object.Tag             =   "N109"
         Text            =   "Diferencia actualizdo-original "
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(21) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   20
         Object.Tag             =   "T1000"
         Text            =   "Termino vida utilil SI/NO"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(22) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   21
         Object.Tag             =   "N109"
         Text            =   "Si termina vida util resta 1 peso"
         Object.Width           =   2540
      EndProperty
   End
   Begin MSComctlLib.ListView LvHist 
      Height          =   2550
      Left            =   990
      TabIndex        =   6
      Top             =   2445
      Width           =   13425
      _ExtentX        =   23680
      _ExtentY        =   4498
      View            =   3
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   0
      NumItems        =   15
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Object.Tag             =   "N109"
         Text            =   "Id"
         Object.Width           =   1058
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Object.Tag             =   "F1000"
         Text            =   "Fecha Ingreso"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Object.Tag             =   "T1000"
         Text            =   "Cuenta"
         Object.Width           =   4410
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Object.Tag             =   "T3000"
         Text            =   "Nombre"
         Object.Width           =   4410
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Key             =   "inicial"
         Object.Tag             =   "N100"
         Text            =   "Valor Inicial"
         Object.Width           =   2258
      EndProperty
      BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   5
         Object.Tag             =   "N103"
         Text            =   "Factor"
         Object.Width           =   2249
      EndProperty
      BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   6
         Key             =   "actualizado"
         Object.Tag             =   "N100"
         Text            =   "Valor Actualizado"
         Object.Width           =   1799
      EndProperty
      BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   7
         Key             =   "dacum"
         Object.Tag             =   "N100"
         Text            =   "Depreciacion Acumulada"
         Object.Width           =   2805
      EndProperty
      BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   8
         Object.Tag             =   "N100"
         Text            =   "Factor"
         Object.Width           =   1614
      EndProperty
      BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   9
         Key             =   "dactual"
         Object.Tag             =   "N100"
         Text            =   "Deprec Actualizada"
         Object.Width           =   2275
      EndProperty
      BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   10
         Key             =   "vdepreciar"
         Object.Tag             =   "N100"
         Text            =   "Valor a depreciar"
         Object.Width           =   1958
      EndProperty
      BeginProperty ColumnHeader(12) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   11
         Object.Tag             =   "N100"
         Text            =   "Meses a depreciar"
         Object.Width           =   1764
      EndProperty
      BeginProperty ColumnHeader(13) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   12
         Object.Tag             =   "N100"
         Text            =   "Vida util"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(14) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   13
         Key             =   "dano"
         Object.Tag             =   "N100"
         Text            =   "Depreciacion del a�o"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(15) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   14
         Text            =   "Pla_Id"
         Object.Width           =   2540
      EndProperty
   End
   Begin VB.Frame Frame1 
      Caption         =   "A�o"
      Height          =   1500
      Left            =   645
      TabIndex        =   0
      Top             =   45
      Width           =   5475
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   285
         Left            =   210
         OleObjectBlob   =   "Con_Depreciacion.frx":037E
         TabIndex        =   18
         Top             =   375
         Width           =   945
      End
      Begin VB.ComboBox CboCuenta 
         Height          =   315
         ItemData        =   "Con_Depreciacion.frx":03F4
         Left            =   1950
         List            =   "Con_Depreciacion.frx":03F6
         Style           =   2  'Dropdown List
         TabIndex        =   5
         Top             =   900
         Width           =   3465
      End
      Begin VB.TextBox TxtNroCuenta 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1020
         TabIndex        =   4
         Tag             =   "N2"
         ToolTipText     =   "Ingrese el codigo de la cuenta"
         Top             =   900
         Width           =   930
      End
      Begin VB.CommandButton cmdCuenta 
         Caption         =   "Cuenta"
         Height          =   300
         Left            =   60
         TabIndex        =   3
         Top             =   915
         Width           =   960
      End
      Begin VB.CommandButton CmdBuscar 
         Caption         =   "Buscar"
         Height          =   300
         Left            =   3450
         TabIndex        =   2
         Top             =   360
         Width           =   1950
      End
      Begin VB.ComboBox ComAno 
         Height          =   315
         Left            =   1215
         Style           =   2  'Dropdown List
         TabIndex        =   1
         Top             =   360
         Width           =   2145
      End
   End
   Begin VB.Timer Timer3 
      Interval        =   500
      Left            =   0
      Top             =   0
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   15
      OleObjectBlob   =   "Con_Depreciacion.frx":03F8
      Top             =   780
   End
   Begin MSComctlLib.TabStrip TabS 
      Height          =   3450
      Left            =   720
      TabIndex        =   8
      Top             =   1935
      Width           =   13995
      _ExtentX        =   24686
      _ExtentY        =   6085
      TabWidthStyle   =   2
      MultiRow        =   -1  'True
      Separators      =   -1  'True
      _Version        =   393216
      BeginProperty Tabs {1EFB6598-857C-11D1-B16A-00C0F0283628} 
         NumTabs         =   2
         BeginProperty Tab1 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Actuales"
            Key             =   "uno"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab2 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Centralizadas"
            Key             =   "dos"
            ImageVarType    =   2
         EndProperty
      EndProperty
   End
   Begin VB.PictureBox Picture2 
      Appearance      =   0  'Flat
      BackColor       =   &H00C0FFC0&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   7680
      Left            =   120
      ScaleHeight     =   7680
      ScaleWidth      =   14625
      TabIndex        =   31
      Top             =   6345
      Width           =   14625
      Begin VB.CommandButton CmdAsiento 
         Caption         =   "Graba a Voucher"
         Height          =   660
         Left            =   6615
         TabIndex        =   48
         Top             =   2355
         Width           =   1380
      End
      Begin VB.CommandButton CmdCentralizarVenta 
         Caption         =   "Graba Vta a Voucher"
         Height          =   690
         Left            =   6600
         TabIndex        =   47
         Top             =   2460
         Visible         =   0   'False
         Width           =   1365
      End
      Begin VB.PictureBox Picture1 
         Appearance      =   0  'Flat
         BackColor       =   &H0080FF80&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   870
         Left            =   6480
         Picture         =   "Con_Depreciacion.frx":062C
         ScaleHeight     =   870
         ScaleWidth      =   1605
         TabIndex        =   49
         Top             =   2250
         Width           =   1605
      End
      Begin VB.Frame FraAsiento 
         Caption         =   "Cierre Dep Acumulada"
         Height          =   2415
         Index           =   3
         Left            =   135
         TabIndex        =   44
         Top             =   5205
         Width           =   6615
         Begin MSComctlLib.ListView LvCierreDepAcumulada 
            Height          =   2040
            Left            =   165
            TabIndex        =   45
            Top             =   225
            Width           =   6225
            _ExtentX        =   10980
            _ExtentY        =   3598
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            FullRowSelect   =   -1  'True
            GridLines       =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   0
            NumItems        =   5
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Object.Tag             =   "T500"
               Text            =   "Cod"
               Object.Width           =   882
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Object.Tag             =   "T3000"
               Text            =   "CUENTAS"
               Object.Width           =   4762
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   2
               Key             =   "debe"
               Object.Tag             =   "N100"
               Text            =   "DEBE"
               Object.Width           =   2381
            EndProperty
            BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   3
               Key             =   "haber"
               Object.Tag             =   "N100"
               Text            =   "HABER"
               Object.Width           =   2381
            EndProperty
            BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   4
               Object.Tag             =   "N109"
               Text            =   "cta dep acum"
               Object.Width           =   2540
            EndProperty
         End
      End
      Begin VB.Frame FraAsiento 
         Caption         =   "Actualizaci�n de Activos"
         Height          =   1815
         Index           =   0
         Left            =   135
         TabIndex        =   42
         Top             =   210
         Width           =   6525
         Begin MSComctlLib.ListView LvAsiento 
            Height          =   1485
            Left            =   165
            TabIndex        =   43
            Top             =   255
            Width           =   6225
            _ExtentX        =   10980
            _ExtentY        =   2619
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            FullRowSelect   =   -1  'True
            GridLines       =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   0
            NumItems        =   5
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Object.Tag             =   "T500"
               Text            =   "Cod"
               Object.Width           =   882
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Object.Tag             =   "T3000"
               Text            =   "CUENTAS"
               Object.Width           =   4762
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   2
               Key             =   "debe"
               Object.Tag             =   "N100"
               Text            =   "DEBE"
               Object.Width           =   2381
            EndProperty
            BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   3
               Key             =   "haber"
               Object.Tag             =   "N100"
               Text            =   "HABER"
               Object.Width           =   2381
            EndProperty
            BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   4
               Object.Width           =   2540
            EndProperty
         End
      End
      Begin VB.Frame FraAsiento 
         Caption         =   "Credito por activos nuevos"
         Height          =   1380
         Index           =   2
         Left            =   90
         TabIndex        =   40
         Top             =   3720
         Width           =   6555
         Begin MSComctlLib.ListView LvCreditoActivo 
            Height          =   1005
            Left            =   195
            TabIndex        =   41
            Top             =   285
            Width           =   6210
            _ExtentX        =   10954
            _ExtentY        =   1773
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            FullRowSelect   =   -1  'True
            GridLines       =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   0
            NumItems        =   5
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Object.Tag             =   "T500"
               Text            =   "Cod"
               Object.Width           =   882
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Object.Tag             =   "T3000"
               Text            =   "CUENTAS"
               Object.Width           =   4762
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   2
               Key             =   "debe"
               Object.Tag             =   "N100"
               Text            =   "DEBE"
               Object.Width           =   2381
            EndProperty
            BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   3
               Key             =   "haber"
               Object.Tag             =   "N100"
               Text            =   "HABER"
               Object.Width           =   2381
            EndProperty
            BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   4
               Object.Width           =   2540
            EndProperty
         End
      End
      Begin VB.Frame FraAsiento 
         Caption         =   "Actualizaci�n t�rmino vida util"
         Height          =   1425
         Index           =   1
         Left            =   165
         TabIndex        =   38
         Top             =   2265
         Width           =   6510
         Begin MSComctlLib.ListView LvActTerminoVidaUtil 
            Height          =   1065
            Left            =   120
            TabIndex        =   39
            Top             =   240
            Width           =   6225
            _ExtentX        =   10980
            _ExtentY        =   1879
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            FullRowSelect   =   -1  'True
            GridLines       =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   0
            NumItems        =   5
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Object.Tag             =   "T500"
               Text            =   "Cod"
               Object.Width           =   882
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Object.Tag             =   "T3000"
               Text            =   "CUENTAS"
               Object.Width           =   4762
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   2
               Key             =   "debe"
               Object.Tag             =   "N100"
               Text            =   "DEBE"
               Object.Width           =   2381
            EndProperty
            BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   3
               Key             =   "haber"
               Object.Tag             =   "N100"
               Text            =   "HABER"
               Object.Width           =   2381
            EndProperty
            BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   4
               Object.Tag             =   "N109"
               Text            =   "pla id"
               Object.Width           =   2540
            EndProperty
         End
      End
      Begin VB.Frame FraDep 
         Caption         =   "Cierre Activo Inmovilizado"
         Height          =   2415
         Index           =   2
         Left            =   7935
         TabIndex        =   36
         Top             =   4725
         Width           =   6615
         Begin MSComctlLib.ListView LvCierreActInmovilizado 
            Height          =   2040
            Left            =   180
            TabIndex        =   37
            Top             =   315
            Width           =   6225
            _ExtentX        =   10980
            _ExtentY        =   3598
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            FullRowSelect   =   -1  'True
            GridLines       =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   0
            NumItems        =   5
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Object.Tag             =   "T500"
               Text            =   "Cod"
               Object.Width           =   882
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Object.Tag             =   "T3000"
               Text            =   "CUENTAS"
               Object.Width           =   4762
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   2
               Key             =   "debe"
               Object.Tag             =   "N100"
               Text            =   "DEBE"
               Object.Width           =   2381
            EndProperty
            BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   3
               Key             =   "haber"
               Object.Tag             =   "N100"
               Text            =   "HABER"
               Object.Width           =   2381
            EndProperty
            BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   4
               Object.Tag             =   "N109"
               Text            =   "cta dep acum"
               Object.Width           =   2540
            EndProperty
         End
      End
      Begin VB.Frame FraDep 
         Caption         =   "Depreciaciones (Doble click para cambiar cuenta contable)"
         Height          =   2415
         Index           =   0
         Left            =   7920
         TabIndex        =   34
         Top             =   225
         Width           =   6615
         Begin MSComctlLib.ListView LvDepre 
            Height          =   2040
            Left            =   150
            TabIndex        =   35
            Top             =   240
            Width           =   6225
            _ExtentX        =   10980
            _ExtentY        =   3598
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            FullRowSelect   =   -1  'True
            GridLines       =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   0
            NumItems        =   5
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Object.Tag             =   "T500"
               Text            =   "Cod"
               Object.Width           =   882
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Object.Tag             =   "T3000"
               Text            =   "CUENTAS"
               Object.Width           =   4762
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   2
               Key             =   "debe"
               Object.Tag             =   "N100"
               Text            =   "DEBE"
               Object.Width           =   2381
            EndProperty
            BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   3
               Key             =   "haber"
               Object.Tag             =   "N100"
               Text            =   "HABER"
               Object.Width           =   2381
            EndProperty
            BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   4
               Object.Tag             =   "N109"
               Text            =   "cta dep acum"
               Object.Width           =   2540
            EndProperty
         End
      End
      Begin VB.Frame FraDep 
         Caption         =   "Actualizaci�n Depreciaciones Acumuladas(Doble click para cambiar cta contable)"
         Height          =   1890
         Index           =   1
         Left            =   7950
         TabIndex        =   32
         Top             =   2730
         Width           =   6615
         Begin MSComctlLib.ListView LvCorreccion 
            Height          =   1470
            Left            =   165
            TabIndex        =   33
            Top             =   285
            Width           =   6225
            _ExtentX        =   10980
            _ExtentY        =   2593
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            FullRowSelect   =   -1  'True
            GridLines       =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   0
            NumItems        =   4
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Object.Tag             =   "T500"
               Text            =   "Cod"
               Object.Width           =   882
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Object.Tag             =   "T3000"
               Text            =   "CUENTAS"
               Object.Width           =   4762
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   2
               Key             =   "debe"
               Object.Tag             =   "N100"
               Text            =   "DEBE"
               Object.Width           =   2381
            EndProperty
            BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   3
               Key             =   "haber"
               Object.Tag             =   "N100"
               Text            =   "HABER"
               Object.Width           =   2381
            EndProperty
         End
      End
   End
   Begin VB.Label Label1 
      Caption         =   "Plan de cuentas pasivo pero oculto para las depreciaciones"
      Height          =   225
      Left            =   6330
      TabIndex        =   10
      Top             =   60
      Width           =   6285
   End
End
Attribute VB_Name = "Con_Depreciacion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim Sm_Cuenta_Correccion_Perdida As String
Dim Sm_Cuenta_Correccion_Ganancia As String
Dim Sm_TipoDepreciacion As String
Dim Lp_TopBase As Long
Dim Lp_VodIdVentaActivo As Long


Private Sub VenderActivo()
   Dim Sp_Ano As String, Sp_FCuenta  As String, Sp_IN As String, Ip_RestaUno As Integer
    Sp_Ano = ComAno.Text
    
    'Debemos leer los activos que estan en el historico para no incluirlos
    'en la dep, leeremos los indices y los colocarems NOT IN en la query
    '17 Abril 2013
'    LvHist.ListItems.Clear
'    Sp_IN = Empty
'    Sql = "SELECT aim_id " & _
'            "FROM con_depreciaciones " & _
'            "WHERE rut_emp='" & SP_Rut_Activo & "' AND dep_ano=" & ComAno.Text
'    Consulta RsTmp, Sql
'    If RsTmp.RecordCount > 0 Then

    
    
    If CboCuenta.Text = "TODAS LAS CUENTAS" Then
        Sp_FCuenta = ""
    Else
        Sp_FCuenta = " AND p.pla_id=" & CboCuenta.ItemData(CboCuenta.ListIndex)
    End If
    
    LvDepre.ListItems.Clear
   ' LvDep.ListItems.Clear
    LvCorreccion.ListItems.Clear
    LvCreditoActivo.ListItems.Clear
    LvAsiento.ListItems.Clear
    Me.LvCierreActInmovilizado.ListItems.Clear
    Me.LvCierreDepAcumulada.ListItems.Clear
    Sql = "SELECT DISTINCT(a.pla_id), pla_nombre cuenta,0,0,pla_dep_acumulada " & _
            "FROM    con_activo_inmovilizado a " & _
            "JOIN con_plan_de_cuentas p ON a.pla_id = p.pla_id " & _
            "WHERE YEAR(aim_fecha_ingreso)<= " & ComAno.Text & Sp_FCuenta & " AND rut_emp = '" & SP_Rut_Activo & "' AND a.aim_id=" & LvDep.SelectedItem & " " & _
            "ORDER BY pla_nombre"
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        With RsTmp
            .MoveFirst
            LvDepre.ListItems.Clear
            LvDepre.ListItems.Add , , IG_Id_Depreciaciones
            LvDepre.ListItems(1).SubItems(1) = "DEPRECIACIONES"
            
            LvDepre.ListItems(1).SubItems(2) = 0
            LvDepre.ListItems(1).SubItems(3) = 0
            Do While Not .EOF
                LvAsiento.ListItems.Add , , !pla_id
                X = LvAsiento.ListItems.Count
                LvAsiento.ListItems(X).SubItems(1) = !cuenta
                LvAsiento.ListItems(X).SubItems(2) = 0
                LvAsiento.ListItems(X).SubItems(3) = 0
               
                
           '     LvDepre.ListItems.Add , , !pla_id
           '     LvDepre.ListItems.Add , , IG_Id_CuentaDepAcumulada
           '     Busca_Id_Combo Me.CboCuentaPasivo, Val(IG_Id_CuentaDepAcumulada)
           
                If Sm_TipoDepreciacion = "INDIRECTA" Then
                    LvDepre.ListItems.Add , , !pla_dep_acumulada
                    Busca_Id_Combo Me.CboCuentaPasivo, Val(!pla_dep_acumulada)
                    X = LvDepre.ListItems.Count
                    LvDepre.ListItems(X).SubItems(1) = CboCuentaPasivo.Text
                    LvDepre.ListItems(X).SubItems(2) = 0
                    LvDepre.ListItems(X).SubItems(3) = 0
                    LvDepre.ListItems(X).SubItems(4) = !pla_dep_acumulada
                Else
                    'Depreciacion DIRECTA
                    LvDepre.ListItems.Add , , !pla_id
                    Busca_Id_Combo Me.CboCuentaPasivo, Val(!pla_id)
                   ' Busca_Id_Combo Me.CboCuentaPasivo, Val(!pla_dep_acumulada)
                    X = LvDepre.ListItems.Count
                    LvDepre.ListItems(X).SubItems(1) = CboCuentaPasivo.Text
                    LvDepre.ListItems(X).SubItems(2) = 0
                    LvDepre.ListItems(X).SubItems(3) = 0
                    LvDepre.ListItems(X).SubItems(4) = !pla_id
                End If
           
           
           
           
'                LvDepre.ListItems.Add , , !pla_dep_acumulada
'                Busca_Id_Combo Me.CboCuentaPasivo, Val(!pla_dep_acumulada)
'                X = LvDepre.ListItems.Count
'                LvDepre.ListItems(X).SubItems(1) = CboCuentaPasivo.Text
'                LvDepre.ListItems(X).SubItems(2) = 0
'                LvDepre.ListItems(X).SubItems(3) = 0
'                LvDepre.ListItems(X).SubItems(4) = !pla_dep_acumulada
                
                If Sm_TipoDepreciacion = "INDIRECTA" Then
                        LvCierreDepAcumulada.ListItems.Add , , !pla_dep_acumulada
                        Busca_Id_Combo Me.CboCuentaPasivo, Val(!pla_dep_acumulada)
                        X = LvCierreDepAcumulada.ListItems.Count
                        LvCierreDepAcumulada.ListItems(X).SubItems(1) = CboCuentaPasivo.Text
                        LvCierreDepAcumulada.ListItems(X).SubItems(2) = 0
                        LvCierreDepAcumulada.ListItems(X).SubItems(3) = 0
                        LvCierreDepAcumulada.ListItems(X).SubItems(4) = !pla_dep_acumulada
                        LvCierreDepAcumulada.ListItems.Add , , !pla_id
                        Busca_Id_Combo Me.CboCuentaPasivo, Val(!pla_id)
                        X = LvCierreDepAcumulada.ListItems.Count
                        LvCierreDepAcumulada.ListItems(X).SubItems(1) = CboCuentaPasivo.Text
                        LvCierreDepAcumulada.ListItems(X).SubItems(2) = 0
                        LvCierreDepAcumulada.ListItems(X).SubItems(3) = 0
                End If
                            
                
                
                LvCierreActInmovilizado.ListItems.Add , , IG_IdCtaClientes
                Busca_Id_Combo Me.CboCuentaPasivo, Val(IG_IdCtaClientes)
                X = LvCierreActInmovilizado.ListItems.Count
                LvCierreActInmovilizado.ListItems(X).SubItems(1) = CboCuentaPasivo.Text
                LvCierreActInmovilizado.ListItems(X).SubItems(2) = 0
                LvCierreActInmovilizado.ListItems(X).SubItems(3) = 0
                'LvCierreActInmovilizado.ListItems(X).SubItems(4) = !pla_dep_acumulada
              
                
                'aQUI AGREGAR LA CUENTA DE UTILIDAD O PERDIDA
                ' SEGUN CORRESPONDA
                           
                
                
                
                
                
                .MoveNext
            Loop
            
            'Prepara asiento para correcion monetaria perdida
             .MoveFirst
            LvCorreccion.ListItems.Clear
            LvCorreccion.ListItems.Add , , IG_id_CorrecionPerdida
            Busca_Id_Combo CboCuentaPasivo, Val(IG_id_CorrecionPerdida)
            LvCorreccion.ListItems(1).SubItems(1) = CboCuentaPasivo.Text
            
            LvCorreccion.ListItems(1).SubItems(2) = 0
            LvCorreccion.ListItems(1).SubItems(3) = 0
            Do While Not .EOF
                LvCorreccion.ListItems.Add , , !pla_dep_acumulada
                Busca_Id_Combo CboCuentaPasivo, Val(!pla_dep_acumulada)
                X = LvCorreccion.ListItems.Count
                LvCorreccion.ListItems(X).SubItems(1) = CboCuentaPasivo.Text
                LvCorreccion.ListItems(X).SubItems(2) = 0
                LvCorreccion.ListItems(X).SubItems(3) = 0
                .MoveNext
            Loop
            
            
            LvAsiento.ListItems.Add , , IG_id_CorrecionGanancia
            X = LvAsiento.ListItems.Count
            LvAsiento.ListItems(X).SubItems(1) = Sm_Cuenta_Correccion_Ganancia
            LvAsiento.ListItems(X).SubItems(2) = 0
            LvAsiento.ListItems(X).SubItems(3) = 0
        End With
        
        'Aqui preparamos asiento para Credito de Activos Nuevos
        Busca_Id_Combo Me.CboCuentaPasivo, Val(IG_idCuentaActivoFijoNuevos)
        Sql = "SELECT " & IG_idCuentaActivoFijoNuevos & ",'" & CboCuentaPasivo.Text & "' pla_nombre,0,0 " & _
                "UNION " & _
                "SELECT DISTINCT (a.pla_id),pla_nombre cuenta,0,0 " & _
                "FROM con_activo_inmovilizado a " & _
                "JOIN con_plan_de_cuentas p ON a.pla_id = p.pla_id " & _
                "WHERE aim_nuevo='SI' AND YEAR(aim_fecha_ingreso)=" & ComAno.Text & " " & Sp_FCuenta & " AND rut_emp = '" & SP_Rut_Activo & "' AND a.aim_id=" & LvDep.SelectedItem & " " & _
                "ORDER BY pla_nombre "
        Consulta RsTmp, Sql
        LLenar_Grilla RsTmp, Me, LvCreditoActivo, False, True, True, False
        
    End If
    
    
    'Aqui comenzamos con la dep
    
    If sm_tipodrepreciacion = "INDIRECTA" Then
        'OBTENER valor inicial del activo
        SQL3 = "IFNULL((SELECT dep_valor_actualizado " & _
                "FROM con_depreciaciones k " & _
                "WHERE rut_emp='" & SP_Rut_Activo & "' AND dep_ano=" & Val(ComAno.Text) - 1 & " AND k.aim_id=a.aim_id)," & _
                " (SELECT  SUM(IF(d.vod_debe>0,r.aim_valor,0))-SUM(IF(d.vod_haber>0,r.aim_valor,0)) " & _
                        "FROM con_vouchers_detalle d " & _
                        "JOIN con_relacion_activos r ON d.pla_id = r.pla_id  AND d.vou_id = r.vou_id AND d.vod_id = r.vod_id " & _
                        "JOIN con_vouchers v ON d.vou_id = v.vou_id " & _
                        "WHERE r.aim_id=a.aim_id AND v.rut_emp = '" & SP_Rut_Activo & "'  AND v.vou_ano_contable =" & Sp_Ano & ") " & _
                                    ") aim_valor_C1,"
    Else
        SQL3 = "IFNULL((SELECT dep_valor_actualizado-dep_del_ano " & _
                "FROM con_depreciaciones k " & _
                "WHERE rut_emp='" & SP_Rut_Activo & "' AND dep_ano=" & Val(ComAno.Text) - 1 & " AND k.aim_id=a.aim_id)," & _
                " (SELECT  SUM(IF(d.vod_debe>0,r.aim_valor,0))-SUM(IF(d.vod_haber>0,r.aim_valor,0)) " & _
                        "FROM con_vouchers_detalle d " & _
                        "JOIN con_relacion_activos r ON d.pla_id = r.pla_id  AND d.vou_id = r.vou_id AND d.vod_id = r.vod_id " & _
                        "JOIN con_vouchers v ON d.vou_id = v.vou_id " & _
                        "WHERE r.aim_id=a.aim_id AND v.rut_emp = '" & SP_Rut_Activo & "'  AND v.vou_ano_contable =" & Sp_Ano & ") " & _
                                    ") aim_valor_C1,"
    
    End If
    Sql = "SELECT  aim_id,aim_fecha_ingreso,pla_nombre cuenta,aim_nombre," & _
             SQL3 & _
            "IF(YEAR(aim_fecha_ingreso)<" & Sp_Ano & ",IFNULL((SELECT fac_valor FROM con_factores WHERE fac_ano=" & Sp_Ano & " AND fac_mes=" & CboMesContable.ItemData(CboMesContable.ListIndex) & ")  ,1.000), " & _
            "IFNULL((SELECT fac_valor FROM con_factores WHERE fac_ano=" & Sp_Ano & " AND fac_mes=" & CboMesContable.ItemData(CboMesContable.ListIndex) & "),1)) AS factor_C2 " & _
            ",0,0,0,0,0,0,0,0,a.pla_id,0,0,pla_dep_acumulada " & _
            "FROM con_activo_inmovilizado a " & _
            "JOIN con_plan_de_cuentas p ON a.pla_id = p.pla_id " & _
            "WHERE YEAR(aim_fecha_ingreso)<=" & Sp_Ano & " AND rut_emp='" & SP_Rut_Activo & "' " & Sp_FCuenta & " AND a.aim_id=" & LvDep.SelectedItem & " " & _
            "ORDER BY pla_nombre;"
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvDep, False, True, True, False
    
    If RsTmp.RecordCount > 0 Then
        For i = 1 To LvDep.ListItems.Count
            'Columnas 3,4,5,6
                       'Columnas 3,4,5,6
            If Sm_TipoDepreciacion = "INDIRECTA" Then
                SQL3 = "IFNULL((SELECT dep_actualizada+dep_del_ano FROM con_depreciaciones d WHERE d.aim_id=" & LvDep.ListItems(i) & " AND dep_ano=" & Sp_Ano & "-1),aim_dacumu ) acumulada_C4 " & _
                    "," & CDbl(LvDep.ListItems(i).SubItems(5)) & " C5,IFNULL((SELECT dep_acumulada FROM con_depreciaciones d WHERE d.aim_id=" & LvDep.ListItems(i) & "  AND dep_ano=" & Sp_Ano & "-1),aim_dacumu)* " & CDbl(LvDep.ListItems(i).SubItems(5)) & " C6, "
            Else
                SQL3 = "0 acumulada_C4,0 C5,0 C6,"
            End If
            Sql = "SELECT " & Round(CDbl(LvDep.ListItems(i).SubItems(4)) * CDbl(LvDep.ListItems(i).SubItems(5)), 0) & " valor_Actualizado_C3," & _
                    SQL3 & _
                    "IF(aim_nuevo='SI' AND YEAR(aim_fecha_ingreso)=" & ComAno.Text & ",'NUEVO','VIEJO') edad " & _
            "FROM con_activo_inmovilizado " & _
            "WHERE aim_id =" & LvDep.ListItems(i)
            Consulta RsTmp, Sql
            'asignamos un 0 a la celda de credito
            LvDep.ListItems(i).SubItems(15) = "0"
            If RsTmp.RecordCount > 0 Then
                Ip_RestaUno = 0
                LvDep.ListItems(i).SubItems(18) = RsTmp!edad
                
                LvDep.ListItems(i).SubItems(6) = NumFormat(RsTmp!valor_Actualizado_C3)
                If RsTmp!edad = "VIEJO" Then
                    'nada
                Else
                    Sql = "SELECT fac_valor " & _
                            "FROM con_factores " & _
                            "WHERE fac_ano=" & ComAno.Text & " AND fac_mes=" & CboMesContable.ItemData(CboMesContable.ListIndex) & " "
                    Consulta RsTmp3, Sql
                    If RsTmp.RecordCount > 0 Then
                        anterior = LvDep.ListItems(i).SubItems(6)
                        
                        LvDep.ListItems(i).SubItems(6) = Round(RsTmp!valor_Actualizado_C3 * RsTmp3!fac_valor, 0)
                        LvDep.ListItems(i).SubItems(15) = NumFormat(CDbl(LvDep.ListItems(i).SubItems(6)) - anterior)
                        LvDep.ListItems(i).SubItems(6) = anterior - LvDep.ListItems(i).SubItems(15)
                    End If
                End If
                
                LvDep.ListItems(i).SubItems(7) = NumFormat(RsTmp!acumulada_C4)
                LvDep.ListItems(i).SubItems(8) = LvDep.ListItems(i).SubItems(5)
                LvDep.ListItems(i).SubItems(9) = NumFormat(CDbl(NumFormat(RsTmp!acumulada_C4)) * CDbl(LvDep.ListItems(i).SubItems(5)))
                LvDep.ListItems(i).SubItems(10) = NumFormat(CDbl(LvDep.ListItems(i).SubItems(6)) - CDbl(LvDep.ListItems(i).SubItems(9)))
            End If
            ' Revisra 15 Abril 2012 Sql = "SELECT IF(" & Sp_Ano & ">aim_fecha_ingreso,12,13-MONTH(aim_fecha_ingreso)) C8," & _
                    "IF(YEAR(aim_fecha_ingreso)<" & Sp_Ano & "," & _
                    "TIMESTAMPDIFF(MONTH,'" & Val(Sp_Ano - 1) & "-12-31',aim_fecha_ingreso),aim_vida_util) C9 " & _
                    "FROM con_activo_inmovilizado a " & _
                    "WHERE aim_id=" & LvDep.ListItems(i)
         '   Sql = "SELECT IF(" & Sp_Ano & ">YEAR(aim_fecha_ingreso),12,13-MONTH(aim_fecha_ingreso)) C8," & _
                    "aim_vida_util C9 " & _
                    "FROM con_activo_inmovilizado a " & _
                    "WHERE aim_id=" & LvDep.ListItems(i)
                    
            Sql = "SELECT " & CboMesContable.ItemData(CboMesContable.ListIndex) & " C8," & _
                    "aim_vida_util C9 " & _
                    "FROM con_activo_inmovilizado a " & _
                    "WHERE aim_id=" & LvDep.ListItems(i)
                    
            Consulta RsTmp, Sql
            If RsTmp.RecordCount > 0 Then
                LvDep.ListItems(i).SubItems(11) = RsTmp!C8
                LvDep.ListItems(i).SubItems(12) = RsTmp!C9
                
                'Si la vida util restante es menor a 13 (esta dentro de los doce meses)
                'igualar los meses a depreciar a la vida util
                '19-Ago-2014
                If RsTmp!C9 < 13 Then
                    LvDep.ListItems(i).SubItems(11) = RsTmp!C9
                End If
                
                '19 Junio 2014
                'Aqui detectamos si el activo termino su vida util
                
                
                
                
                If Val(LvDep.ListItems(i).SubItems(11)) >= Val(LvDep.ListItems(i).SubItems(12)) Then
                    If Val(LvDep.ListItems(i).SubItems(11)) <> 0 And Val(LvDep.ListItems(i).SubItems(12)) <> 0 Then
                        Ip_RestaUno = 1
                        LvDep.ListItems(i).SubItems(20) = "SI"
                    End If
                
                End If
            End If
            If CDbl(LvDep.ListItems(i).SubItems(12)) > 0 Then
                LvDep.ListItems(i).SubItems(13) = NumFormat(CDbl(LvDep.ListItems(i).SubItems(10)) / CDbl(LvDep.ListItems(i).SubItems(12)) * CDbl(LvDep.ListItems(i).SubItems(11)) - Ip_RestaUno)
                
                LvDep.ListItems(i).SubItems(21) = NumFormat(CDbl(LvDep.ListItems(i).SubItems(6)) - Ip_RestaUno)
            Else
                'Aqui evitamos la division por 0
                '13 Abril 2013
                LvDep.ListItems(i).SubItems(13) = 0
                LvDep.ListItems(i).SubItems(4) = 1 'VALOR INICIAL
                LvDep.ListItems(i).SubItems(6) = 1
                LvDep.ListItems(i).SubItems(7) = 0
                LvDep.ListItems(i).SubItems(8) = 0
                LvDep.ListItems(i).SubItems(9) = 0
                LvDep.ListItems(i).SubItems(10) = 0
                LvDep.ListItems(i).SubItems(11) = 0 'MESES A DEPRECIAR
            End If
            Ip_RestaUno = 0
        Next
    End If
    
    LvActTerminoVidaUtil.ListItems.Clear
    If Sm_TipoDepreciacion = "INDIRECTA" Then
            
            '28 Junio 2014
            'asiento termino vida util
            'me.LvActTerminoVidaUtil
            Dim Bp_SeEncuentraCuenta As Boolean
            For i = 1 To LvDep.ListItems.Count
                Bp_SeEncuentraCuenta = False
                If LvDep.ListItems(i).SubItems(20) = "SI" Then
                    For X = 1 To LvActTerminoVidaUtil.ListItems.Count
                        If Val(LvDep.ListItems(i).SubItems(17)) = Val(LvActTerminoVidaUtil.ListItems(X)) Then
                            'La cuenta ya esta
                            Bp_SeEncuentraCuenta = True
                            
                            'LvActTerminoVidaUtil.ListItems(X).SubItems(2) = CDbl(LvDep.ListItems(i).SubItems(13)) + _
                            CDbl(LvActTerminoVidaUtil.ListItems(X).SubItems(2))
                            LvActTerminoVidaUtil.ListItems(X).SubItems(2) = CDbl(LvDep.ListItems(i).SubItems(21)) + _
                            CDbl(LvActTerminoVidaUtil.ListItems(X).SubItems(2))
                            
                            Exit For
                        End If
                    Next
                    If Not Bp_SeEncuentraCuenta Then
                        'Agregamos cuenta
                        LvActTerminoVidaUtil.ListItems.Add , , LvDep.ListItems(i).SubItems(17)
                        Busca_Id_Combo CboCuentaPasivo, Val(LvDep.ListItems(i).SubItems(17))
                        LvActTerminoVidaUtil.ListItems(LvActTerminoVidaUtil.ListItems.Count).SubItems(1) = Me.CboCuentaPasivo.Text
                        'LvActTerminoVidaUtil.ListItems(LvActTerminoVidaUtil.ListItems.Count).SubItems(2) = CDbl(LvDep.ListItems(i).SubItems(13))
                        LvActTerminoVidaUtil.ListItems(LvActTerminoVidaUtil.ListItems.Count).SubItems(2) = CDbl(LvDep.ListItems(i).SubItems(21))
                        LvActTerminoVidaUtil.ListItems(LvActTerminoVidaUtil.ListItems.Count).SubItems(4) = LvDep.ListItems(i).SubItems(14)
                    End If
                End If
            Next
            
            'TAREA JULIO 2014
            'VERIFICAR LAS CUENTAS QUE ESTAN INCLUIDAS AQUI PARA TERMINAR EL ASIENTO
            For i = 1 To LvActTerminoVidaUtil.ListItems.Count
                
                Bp_SeEncuentraCuenta = False
                
                For X = 1 To LvDep.ListItems.Count
                    If Val(LvActTerminoVidaUtil.ListItems(i).SubItems(4)) = Val(LvDep.ListItems(X).SubItems(14)) And _
                        LvDep.ListItems(X).SubItems(20) = "SI" Then
                        For Z = 1 To LvActTerminoVidaUtil.ListItems.Count
                            If Val(LvActTerminoVidaUtil.ListItems(Z)) = Val(LvDep.ListItems(X).SubItems(14)) Then
                                Bp_SeEncuentraCuenta = True
                                
                                'LvActTerminoVidaUtil.ListItems(Z).SubItems(3) = CDbl(LvActTerminoVidaUtil.ListItems(Z).SubItems(3)) + _
                                (CDbl(LvDep.ListItems(X).SubItems(13)))
                                
                                LvActTerminoVidaUtil.ListItems(Z).SubItems(3) = CDbl(LvActTerminoVidaUtil.ListItems(Z).SubItems(3)) + _
                                (CDbl(LvDep.ListItems(X).SubItems(21)))
                                Exit For
                            End If
                        Next
                        If Not Bp_SeEncuentraCuenta Then
                            LvActTerminoVidaUtil.ListItems.Add , , Val(LvDep.ListItems(X).SubItems(14))
                            LvActTerminoVidaUtil.ListItems(LvActTerminoVidaUtil.ListItems.Count).SubItems(1) = LvDep.ListItems(X).SubItems(2)
                            'LvActTerminoVidaUtil.ListItems(LvActTerminoVidaUtil.ListItems.Count).SubItems(3) = CDbl(LvDep.ListItems(X).SubItems(13))
                            LvActTerminoVidaUtil.ListItems(LvActTerminoVidaUtil.ListItems.Count).SubItems(3) = CDbl(LvDep.ListItems(X).SubItems(21))
                        End If
                    End If
                Next
                
                
            Next
            
    End If
    TotalAsiento LvActTerminoVidaUtil
    
    'indiviualizacion activos
    '5 Feb 2014
    For i = 1 To LvDep.ListItems.Count
        If LvDep.ListItems(i).SubItems(18) = "NUEVO" Then
            LvDep.ListItems(i).SubItems(16) = CDbl(NumFormat((CDbl(LvDep.ListItems(i).SubItems(4)) * Val(CxP(LvDep.ListItems(i).SubItems(5)))) - CDbl(LvDep.ListItems(i).SubItems(4))))
        Else
            LvDep.ListItems(i).SubItems(16) = CDbl(LvDep.ListItems(i).SubItems(6)) - CDbl(LvDep.ListItems(i).SubItems(4))
        End If
        'actualizado - original
        '10 Mayo 2014
    '    LvDep.ListItems(i).SubItems(19) = CDbl(LvDep.ListItems(i).SubItems(6)) - CDbl(LvDep.ListItems(i).SubItems(4))
    Next
    
    If LvDep.ListItems.Count > 0 Then
        ''Totalizamos depreciaciones
        '12 Abril 2013-
        LvDep.ListItems.Add , , ""
        LvDep.ListItems(LvDep.ListItems.Count).SubItems(BuscaIn(LvDep, "inicial")) = NumFormat(TotalizaColumna(LvDep, "inicial"))
        LvDep.ListItems(LvDep.ListItems.Count).SubItems(BuscaIn(LvDep, "actualizado")) = NumFormat(TotalizaColumna(LvDep, "actualizado"))
        LvDep.ListItems(LvDep.ListItems.Count).SubItems(BuscaIn(LvDep, "dacum")) = NumFormat(TotalizaColumna(LvDep, "dacum"))
        LvDep.ListItems(LvDep.ListItems.Count).SubItems(BuscaIn(LvDep, "dactual")) = NumFormat(TotalizaColumna(LvDep, "dactual"))
        LvDep.ListItems(LvDep.ListItems.Count).SubItems(BuscaIn(LvDep, "vdepreciar")) = NumFormat(TotalizaColumna(LvDep, "vdepreciar"))
        LvDep.ListItems(LvDep.ListItems.Count).SubItems(BuscaIn(LvDep, "dano")) = NumFormat(TotalizaColumna(LvDep, "dano"))
        LvDep.ListItems(LvDep.ListItems.Count).SubItems(15) = NumFormat(TotalizaColumna(LvDep, "tcreditoactivofijo"))
        
        For i = 1 To LvDep.ColumnHeaders.Count - 7
            LvDep.ListItems(LvDep.ListItems.Count).ListSubItems(i).Bold = True
        Next
    End If
    
    'Cierre Dep Acumulada
    '23 Agosto 2014
    If Sm_TipoDepreciacion = "INDIRECTA" Then
            LvCierreDepAcumulada.ListItems(1).SubItems(2) = NumFormat(CDbl(LvDep.SelectedItem.SubItems(9)) + CDbl(LvDep.SelectedItem.SubItems(13)))
            LvCierreDepAcumulada.ListItems(2).SubItems(3) = NumFormat(CDbl(LvDep.SelectedItem.SubItems(9)) + CDbl(LvDep.SelectedItem.SubItems(13)))
    End If
    TotalAsiento LvCierreDepAcumulada
    
    'Cierre Act Inmovilizado
    '23-Agosto-2014
    LvCierreActInmovilizado.ListItems(1).SubItems(2) = NumFormat(TxtValorBruto)
    
    If Sm_TipoDepreciacion = "INDIRECTA" Then
        UTILIDADOPERDIDA = CDbl(TxtValorBruto) - CDbl(LvCierreDepAcumulada.ListItems(1).SubItems(2))
    Else
        UTILIDADOPERDIDA = CDbl(TxtValorBruto) - (CDbl(LvDep.SelectedItem.SubItems(6)) - CDbl(LvDep.SelectedItem.SubItems(13)))
    End If
    If UTILIDADOPERDIDA > 0 Then
            LvCierreActInmovilizado.ListItems.Add , , LvDep.SelectedItem.SubItems(14)
            Busca_Id_Combo Me.CboCuentaPasivo, Val(LvDep.SelectedItem.SubItems(14))
            X = LvCierreActInmovilizado.ListItems.Count
            LvCierreActInmovilizado.ListItems(X).SubItems(1) = CboCuentaPasivo.Text
            LvCierreActInmovilizado.ListItems(X).SubItems(3) = NumFormat(CDbl(LvDep.SelectedItem.SubItems(6)) - (CDbl(LvDep.SelectedItem.SubItems(9)) + CDbl(LvDep.SelectedItem.SubItems(13))))
            LvCierreActInmovilizado.ListItems(X).SubItems(2) = 0
            
            'cuenta utilidad
             LvCierreActInmovilizado.ListItems.Add , , IG_IdUtilidadVtaActivoInmovilizado
            Busca_Id_Combo Me.CboCuentaPasivo, Val(IG_IdUtilidadVtaActivoInmovilizado)
            X = LvCierreActInmovilizado.ListItems.Count
            LvCierreActInmovilizado.ListItems(X).SubItems(1) = CboCuentaPasivo.Text
            LvCierreActInmovilizado.ListItems(X).SubItems(3) = NumFormat(CDbl(TxtValorBruto) - CDbl(LvCierreActInmovilizado.ListItems(2).SubItems(3)))
            LvCierreActInmovilizado.ListItems(X).SubItems(2) = 0
    Else
            'cuenta perdida
            perdida = NumFormat((CDbl(LvDep.SelectedItem.SubItems(6)) - (CDbl(LvDep.SelectedItem.SubItems(9)) + CDbl(LvDep.SelectedItem.SubItems(13)))) - CDbl(TxtValorBruto))
             LvCierreActInmovilizado.ListItems.Add , , IG_IdPerdidaVtaActivoInmovilizado
            Busca_Id_Combo Me.CboCuentaPasivo, Val(IG_IdPerdidaVtaActivoInmovilizado)
            X = LvCierreActInmovilizado.ListItems.Count
            LvCierreActInmovilizado.ListItems(X).SubItems(1) = CboCuentaPasivo.Text
            LvCierreActInmovilizado.ListItems(X).SubItems(2) = perdida
            LvCierreActInmovilizado.ListItems(X).SubItems(3) = 0
    
            'cta Activo inmovilizado
            LvCierreActInmovilizado.ListItems.Add , , LvDep.SelectedItem.SubItems(14)
            Busca_Id_Combo Me.CboCuentaPasivo, Val(LvDep.SelectedItem.SubItems(14))
            X = LvCierreActInmovilizado.ListItems.Count
            LvCierreActInmovilizado.ListItems(X).SubItems(1) = CboCuentaPasivo.Text
            LvCierreActInmovilizado.ListItems(X).SubItems(3) = NumFormat(CDbl(perdida) + CDbl(LvCierreActInmovilizado.ListItems(1).SubItems(2)))
            LvCierreActInmovilizado.ListItems(X).SubItems(2) = 0
    End If
    
    If CDbl(txtElIVA) > 0 Then
        '30Ago2014
        'Si el IVA es >0 entonces sumar a clientes el valor de iva
        'y agregar la cuenta debito fiscal en el voucher al haber
        Me.LvCierreActInmovilizado.ListItems(1).SubItems(2) = NumFormat(CDbl(LvCierreActInmovilizado.ListItems(1).SubItems(2)) + CDbl(txtElIVA))
        
       'cta IVA debito fiscal
        LvCierreActInmovilizado.ListItems.Add , , IG_Id_CuentaDebitoFiscal
        Busca_Id_Combo Me.CboCuentaPasivo, Val(IG_Id_CuentaDebitoFiscal)
        X = LvCierreActInmovilizado.ListItems.Count
        LvCierreActInmovilizado.ListItems(X).SubItems(1) = CboCuentaPasivo.Text
        LvCierreActInmovilizado.ListItems(X).SubItems(2) = 0
        LvCierreActInmovilizado.ListItems(X).SubItems(3) = txtElIVA
        
    End If
    
    
    TotalAsiento LvCierreActInmovilizado
     'IG_IdUtilidadVtaActivoInmovilizado
    'IG_IdPerdidaVtaActivoInmovilizado
    
    
    AsientosContables
End Sub

Private Sub CboCuenta_Click()
    If CboCuenta.ListIndex = -1 Then Exit Sub
    TxtNroCuenta = CboCuenta.ItemData(CboCuenta.ListIndex)
End Sub

Private Sub AsientosContables()
    If LvDep.ListItems.Count = 0 Then Exit Sub
    
    
    Dim Lp_PlaIdActual As Long
    
    
    
    
    
    'Asiento correcion Ganancia
    For i = 1 To LvAsiento.ListItems.Count
        LvAsiento.ListItems(i).SubItems(2) = 0
        LvAsiento.ListItems(i).SubItems(3) = 0
        Lp_PlaIdActual = LvAsiento.ListItems(i)
        For X = 1 To LvDep.ListItems.Count - 1
            If Val(LvDep.ListItems(X).SubItems(14)) = Lp_PlaIdActual Then
                LvAsiento.ListItems(i).SubItems(2) = NumFormat(CDbl(LvAsiento.ListItems(i).SubItems(2)) + _
                (CDbl(LvDep.ListItems(X).SubItems(6)) + CDbl(LvDep.ListItems(X).SubItems(15)) - CDbl(LvDep.ListItems(X).SubItems(4))))
            End If
        Next
    Next
    
    TotalAsiento LvAsiento
    LvAsiento.ListItems(LvAsiento.ListItems.Count).SubItems(3) = LvAsiento.ListItems(LvAsiento.ListItems.Count).SubItems(2)
    LvAsiento.ListItems(LvAsiento.ListItems.Count - 1).SubItems(3) = LvAsiento.ListItems(LvAsiento.ListItems.Count).SubItems(2)
    
    If Sm_TipoDepreciacion = "INDIRECTA" Then
            'Asiento Depreciacion basandose en el asiento ganancia
            For i = 2 To LvDepre.ListItems.Count
                LvDepre.ListItems(i).SubItems(2) = 0
                LvDepre.ListItems(i).SubItems(3) = 0
                Lp_PlaIdActual = LvDepre.ListItems(i)
                For X = 1 To LvDep.ListItems.Count - 1
                    If Val(LvDep.ListItems(X).SubItems(17)) = Lp_PlaIdActual Then
                        LvDepre.ListItems(i).SubItems(3) = NumFormat(CDbl(LvDepre.ListItems(i).SubItems(3)) + _
                        CDbl(LvDep.ListItems(X).SubItems(13)))
                    End If
                Next
            Next
    Else 'DEPRECIACION DIRECTA
            'Asiento Depreciacion basandose en el asiento ganancia
            For i = 2 To LvDepre.ListItems.Count
                LvDepre.ListItems(i).SubItems(2) = 0
                LvDepre.ListItems(i).SubItems(3) = 0
                Lp_PlaIdActual = LvDepre.ListItems(i)
                For X = 1 To LvDep.ListItems.Count - 1
                    If Val(LvDep.ListItems(X).SubItems(14)) = Lp_PlaIdActual Then
                        LvDepre.ListItems(i).SubItems(3) = NumFormat(CDbl(LvDepre.ListItems(i).SubItems(3)) + _
                        CDbl(LvDep.ListItems(X).SubItems(13)))
                    End If
                Next
            Next
    End If
    TotalAsiento LvDepre
    
    LvDepre.ListItems(1).SubItems(2) = LvDepre.ListItems(LvDepre.ListItems.Count).SubItems(3)
    LvDepre.ListItems(LvDepre.ListItems.Count).SubItems(2) = LvDepre.ListItems(LvDepre.ListItems.Count).SubItems(3)
    
    'Asiento por credito de activos nuevos.
    
    For i = 2 To LvCreditoActivo.ListItems.Count
        LvCreditoActivo.ListItems(i).SubItems(2) = 0
        LvCreditoActivo.ListItems(i).SubItems(3) = 0
        Lp_PlaIdActual = LvCreditoActivo.ListItems(i)
        For X = 1 To LvDep.ListItems.Count - 1
            If LvDep.ListItems(X).SubItems(14) = Lp_PlaIdActual And Val(LvDep.ListItems(X).SubItems(15)) > 0 Then
                LvCreditoActivo.ListItems(i).SubItems(3) = CDbl(LvCreditoActivo.ListItems(i).SubItems(3)) + CDbl(LvDep.ListItems(X).SubItems(15))
                
            End If
        Next
    Next
    
   
    TotalAsiento LvCreditoActivo
    LvCreditoActivo.ListItems(1).SubItems(2) = LvCreditoActivo.ListItems(LvCreditoActivo.ListItems.Count).SubItems(3)
    LvCreditoActivo.ListItems(LvCreditoActivo.ListItems.Count).SubItems(2) = LvCreditoActivo.ListItems(LvCreditoActivo.ListItems.Count).SubItems(3)
    
    
    
     'Asiento Correccion monetaria perdida
     '4to Asiento
     '27 Abril 2013
    For i = 2 To LvCorreccion.ListItems.Count
        LvCorreccion.ListItems(i).SubItems(2) = 0
        LvCorreccion.ListItems(i).SubItems(3) = 0
        Lp_PlaIdActual = LvCorreccion.ListItems(i)
        For X = 1 To LvDep.ListItems.Count - 1
            If Val(LvDep.ListItems(X).SubItems(17)) = Lp_PlaIdActual Then
                LvCorreccion.ListItems(i).SubItems(3) = CDbl(LvCorreccion.ListItems(i).SubItems(3)) + _
                    (CDbl(LvDep.ListItems(X).SubItems(9)) - CDbl(LvDep.ListItems(X).SubItems(7)))
            End If
        Next
    Next
    
    TotalAsiento LvCorreccion
    LvCorreccion.ListItems(1).SubItems(2) = LvCorreccion.ListItems(LvCorreccion.ListItems.Count).SubItems(3)
    LvCorreccion.ListItems(LvCorreccion.ListItems.Count).SubItems(2) = LvCorreccion.ListItems(LvCorreccion.ListItems.Count).SubItems(3)
    
    
End Sub


Private Sub CboMesContable_Click()
    If CboMesContable.ListIndex = -1 Then Exit Sub
    'Buscar Factor del mes y a�o seleccionado
      Sql = "SELECT fac_mes,fac_valor " & _
            "FROM con_factores " & _
            "WHERE fac_ano=" & ComAno.Text & " AND fac_mes=" & CboMesContable.ItemData(CboMesContable.ListIndex) & " "
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
                txtFactor = CxP(Format(RsTmp!fac_valor, "#,##0.##0"))
    Else
        txtFactor = "1.000"
    End If
End Sub

Private Sub CmdAsiento_Click()
    Dim Ip_MesesD As Integer, Ip_MesesF As Integer, Lp_AsientoActivoNuevo As Long
    Dim Lp_Vouchers(7) As Long
    
    'Validaciones
    If LvDep.ListItems.Count = 0 Then
        MsgBox "No hay movimientos para centralizar...", vbInformation + vbOKOnly
        Exit Sub
    End If
    
    
    
    
    
    
    If MsgBox("Al centralizar a voucher queda cerrado el a�o ..." & vbNewLine & "                 �Continuar?", vbQuestion + vbYesNo) = vbNo Then Exit Sub
    For i = 1 To 7: Lp_Vouchers(i) = 0: Next
    'Generamos voucher contables
    'For i = 1 To LvAsiento.ListItems.Count
    '    LvFinal.ListItems.Add , , LvAsiento.ListItems(i)
    '    LvFinal.ListItems(i).SubItems(1) = LvAsiento.ListItems(i).SubItems(1)
    ''    LvFinal.ListItems(i).SubItems(2) = LvAsiento.ListItems(i).SubItems(2)
    '    LvFinal.ListItems(i).SubItems(3) = LvAsiento.ListItems(i).SubItems(3)
    'Next
    'For i = 1 To LvDepre.ListItems.Count
    '    LvFinal.ListItems.Add , , LvDepre.ListItems(i)
    '    LvFinal.ListItems(LvFinal.ListItems.Count).SubItems(1) = LvDepre.ListItems(i).SubItems(1)
    '    LvFinal.ListItems(LvFinal.ListItems.Count).SubItems(2) = LvDepre.ListItems(i).SubItems(2)
    '    LvFinal.ListItems(LvFinal.ListItems.Count).SubItems(3) = LvDepre.ListItems(i).SubItems(3)
    'Next
    
   Lp_Vouchers(1) = CreaVoucher(3, "CORRECCION MONETARIA DE ACTIVOS A�O " & ComAno.Text, LvAsiento)
   Lp_Vouchers(2) = CreaVoucher(3, "DEPRECIACION ACTIVO INMOVILIZADO A�O " & ComAno.Text, LvDepre)
   Lp_Vouchers(3) = CreaVoucher(3, "CREDITO ACTIVO INMOVILIZADO A�O " & ComAno.Text, LvCreditoActivo)
   Lp_Vouchers(4) = CreaVoucher(3, "CORRECCION MONETARIA DEPRECIACIONES A�O " & ComAno.Text, LvCorreccion)
   Lp_Vouchers(5) = CreaVoucher(3, "TERMINO DE VIDA UTIL A�O " & ComAno.Text, LvActTerminoVidaUtil)
    
    
    'Aqui guardaremos toda la depreciacion para tenerla como historica
    Sql = "INSERT INTO con_depreciaciones (dep_ano, pla_id,aim_id, dep_valor_inicial, dep_factor," & _
            "dep_valor_actualizado, dep_acumulada, dep_factor_2, dep_actualizada, dep_valor_a_depreciar," & _
            "dep_meses_depreciados, dep_vida_util, dep_del_ano, vou1_id,vou2_id,vou3_id,vou4_id,vou5_id,rut_emp) VALUES "
            
    For i = 1 To LvDep.ListItems.Count - 1
        Sql = Sql & "(" & ComAno.Text & "," & LvDep.ListItems(i).SubItems(14) & "," & LvDep.ListItems(i) & "," & _
                CDbl(LvDep.ListItems(i).SubItems(4)) & "," & CxP(LvDep.ListItems(i).SubItems(5)) & "," & CDbl(LvDep.ListItems(i).SubItems(6)) & "," & CDbl(LvDep.ListItems(i).SubItems(7)) & "," & _
                CxP(LvDep.ListItems(i).SubItems(8)) & "," & CDbl(LvDep.ListItems(i).SubItems(9)) & "," & _
                CDbl(LvDep.ListItems(i).SubItems(10)) & "," & CDbl(LvDep.ListItems(i).SubItems(11)) & "," & _
                CDbl(LvDep.ListItems(i).SubItems(12)) & "," & CDbl(LvDep.ListItems(i).SubItems(13)) & "," & _
                Lp_Vouchers(1) & "," & Lp_Vouchers(2) & "," & Lp_Vouchers(3) & "," & Lp_Vouchers(4) & "," & Lp_Vouchers(5) & ",'" & SP_Rut_Activo & "'),"
    Next
    Sql = Mid(Sql, 1, Len(Sql) - 1)
    On Error GoTo GuardaDepre
    cn.BeginTrans
        cn.Execute Sql
        
        
        'Tambien debemos restar los meses de vida util del activo
        For i = 1 To LvDep.ListItems.Count - 1
           ' Lp_MesesD = 12
           ' If Year(LvDep.ListItems(i).SubItems(1)) = ComAno.Text Then
           '     'Aqui sabemos que el activo ingreso el mismo a�o
           '     'restamos 12 - (o el n�mero de meses en que se compro)
           '     Ip_MesesD = 13 - Month(LvDep.ListItems(i).SubItems(1))
           '' End If
            
               
            '26 de Julio de 2014
            'En vez de hacer todos estos calculos, solo vamos a restar los meses a depreciar
            'ya que cuando se elimina una depreciacion estos meses se suman
               
            'Lp_MesesF = Val(LvDep.ListItems(i).SubItems(12)) - Val(LvDep.ListItems(i).SubItems(11))
            'If Lp_MesesF < 0 Then Lp_MesesF = 0
            'cn.Execute "UPDATE con_activo_inmovilizado SET aim_vida_util=" & Lp_MesesF & " " & _
                        "WHERE aim_id=" & LvDep.ListItems(i)
                        
            cn.Execute "UPDATE con_activo_inmovilizado SET aim_vida_util=aim_vida_util-" & LvDep.ListItems(i).SubItems(11) & " " & _
                        "WHERE aim_id=" & LvDep.ListItems(i)
             
        Next
    cn.CommitTrans
    MsgBox "Centralizacion Correcta...", vbInformation
    CmdBuscar_Click
    Exit Sub
GuardaDepre:
    cn.RollbackTrans
    MsgBox "Error al grabar Depreciacion historica..." & vbNewLine & Err.Number & vbNewLine & Err.Description
End Sub

Private Sub cmdBtnRut_Click()
    ClienteEncontrado = False
    SG_codigo = Empty
    TxtRut = ""
    txtCliente = ""
    BuscaCliente.Show 1
    TxtRut = SG_codigo
    If SG_codigo <> Empty Then
        TxtRut_Validate (True)
    End If
End Sub

Private Sub CmdBuscar_Click()
    Dim Sp_Ano As String, Sp_FCuenta  As String, Sp_IN As String, Ip_RestaUno As Integer
    Sp_Ano = ComAno.Text
    CmdAsiento.Visible = True
    CmdCentralizarVenta.Visible = False
    'Debemos leer los activos que estan en el historico para no incluirlos
    'en la dep, leeremos los indices y los colocarems NOT IN en la query
    '17 Abril 2013
    LvHist.ListItems.Clear
    Sp_IN = Empty
    Sql = "SELECT aim_id " & _
            "FROM con_depreciaciones " & _
            "WHERE rut_emp='" & SP_Rut_Activo & "' AND dep_ano=" & ComAno.Text
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        With RsTmp
            .MoveFirst
            
            Do While Not .EOF
                Sp_IN = Sp_IN & !aim_id & ","
                .MoveNext
            Loop
            Sp_IN = Mid(Sp_IN, 1, Len(Sp_IN) - 1) & ") "
        End With
        'Aqui cargarmos los que ya estan centralizados (historicos)
        'de esto no mostraremos los asientos.
        'Aqui ya sabemos que hay al menos 1 historico
        Sql = "SELECT d.aim_id,a.aim_fecha_ingreso,pla_nombre,aim_nombre,d.dep_valor_inicial,dep_factor,dep_valor_actualizado, " & _
                "dep_acumulada , dep_factor_2, dep_actualizada, dep_valor_a_depreciar, dep_meses_depreciados, dep_vida_util, dep_del_ano, d.pla_id " & _
                "FROM con_depreciaciones d " & _
                "JOIN con_activo_inmovilizado a ON d.aim_id=a.aim_id " & _
                "JOIN con_plan_de_cuentas p ON d.pla_id=p.pla_id " & _
            "WHERE d.rut_emp='" & SP_Rut_Activo & "' AND d.dep_ano=" & ComAno.Text
        Consulta RsTmp, Sql
        LLenar_Grilla RsTmp, Me, LvHist, True, True, True, False
        
        
        Sp_IN = " AND a.aim_id NOT IN(" & Sp_IN
    End If
    
    
    If CboCuenta.Text = "TODAS LAS CUENTAS" Then
        Sp_FCuenta = ""
    Else
        Sp_FCuenta = " AND p.pla_id=" & CboCuenta.ItemData(CboCuenta.ListIndex)
    End If
    
    LvDepre.ListItems.Clear
    LvDep.ListItems.Clear
    LvCorreccion.ListItems.Clear
    LvCreditoActivo.ListItems.Clear
    LvAsiento.ListItems.Clear
    Sql = "SELECT DISTINCT(a.pla_id), pla_nombre cuenta,0,0,pla_dep_acumulada " & _
            "FROM    con_activo_inmovilizado a " & _
            "JOIN con_plan_de_cuentas p ON a.pla_id = p.pla_id " & _
            "WHERE YEAR(aim_fecha_ingreso)<= " & ComAno.Text & Sp_FCuenta & " AND rut_emp = '" & SP_Rut_Activo & "'" & Sp_IN & _
            "ORDER BY pla_nombre"
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        With RsTmp
            .MoveFirst
            LvDepre.ListItems.Clear
            LvDepre.ListItems.Add , , IG_Id_Depreciaciones
            LvDepre.ListItems(1).SubItems(1) = "DEPRECIACIONES"
            
            LvDepre.ListItems(1).SubItems(2) = 0
            LvDepre.ListItems(1).SubItems(3) = 0
            Do While Not .EOF
                LvAsiento.ListItems.Add , , !pla_id
                X = LvAsiento.ListItems.Count
                LvAsiento.ListItems(X).SubItems(1) = !cuenta
                LvAsiento.ListItems(X).SubItems(2) = 0
                LvAsiento.ListItems(X).SubItems(3) = 0
               
                
           '     LvDepre.ListItems.Add , , !pla_id
           '     LvDepre.ListItems.Add , , IG_Id_CuentaDepAcumulada
           '     Busca_Id_Combo Me.CboCuentaPasivo, Val(IG_Id_CuentaDepAcumulada)
           '13-09-2014
           'Depreciacion indirecta
                If Sm_TipoDepreciacion = "INDIRECTA" Then
                    LvDepre.ListItems.Add , , !pla_dep_acumulada
                    Busca_Id_Combo Me.CboCuentaPasivo, Val(!pla_dep_acumulada)
                    X = LvDepre.ListItems.Count
                    LvDepre.ListItems(X).SubItems(1) = CboCuentaPasivo.Text
                    LvDepre.ListItems(X).SubItems(2) = 0
                    LvDepre.ListItems(X).SubItems(3) = 0
                    LvDepre.ListItems(X).SubItems(4) = !pla_id
                Else
                    'Depreciacion DIRECTA
                    LvDepre.ListItems.Add , , !pla_id
                    Busca_Id_Combo Me.CboCuentaPasivo, Val(!pla_id)
                    X = LvDepre.ListItems.Count
                    LvDepre.ListItems(X).SubItems(1) = CboCuentaPasivo.Text
                    LvDepre.ListItems(X).SubItems(2) = 0
                    LvDepre.ListItems(X).SubItems(3) = 0
                    LvDepre.ListItems(X).SubItems(4) = !pla_id
                End If
                
                '27 Jun 2015, CUALQUIER PROBLEMA VOLVER A ESTO
'                  If Sm_TipoDepreciacion = "INDIRECTA" Then
'                    LvDepre.ListItems.Add , , !pla_dep_acumulada
'                    Busca_Id_Combo Me.CboCuentaPasivo, Val(!pla_dep_acumulada)
'                    X = LvDepre.ListItems.Count
'                    LvDepre.ListItems(X).SubItems(1) = CboCuentaPasivo.Text
'                    LvDepre.ListItems(X).SubItems(2) = 0
'                    LvDepre.ListItems(X).SubItems(3) = 0
'                    LvDepre.ListItems(X).SubItems(4) = !pla_dep_acumulada
'                Else
'                    'Depreciacion DIRECTA
'                    LvDepre.ListItems.Add , , !pla_id
'                    Busca_Id_Combo Me.CboCuentaPasivo, Val(!pla_id)
'                    X = LvDepre.ListItems.Count
'                    LvDepre.ListItems(X).SubItems(1) = CboCuentaPasivo.Text
'                    LvDepre.ListItems(X).SubItems(2) = 0
'                    LvDepre.ListItems(X).SubItems(3) = 0
'                    LvDepre.ListItems(X).SubItems(4) = !pla_id
'                End If
                .MoveNext
            Loop
            
            'Prepara asiento para correcion monetaria perdida
             .MoveFirst
            LvCorreccion.ListItems.Clear
            LvCorreccion.ListItems.Add , , IG_id_CorrecionPerdida
            Busca_Id_Combo CboCuentaPasivo, Val(IG_id_CorrecionPerdida)
            LvCorreccion.ListItems(1).SubItems(1) = CboCuentaPasivo.Text
            
            LvCorreccion.ListItems(1).SubItems(2) = 0
            LvCorreccion.ListItems(1).SubItems(3) = 0
            Do While Not .EOF
                LvCorreccion.ListItems.Add , , !pla_dep_acumulada
                Busca_Id_Combo CboCuentaPasivo, Val(!pla_dep_acumulada)
                X = LvCorreccion.ListItems.Count
                LvCorreccion.ListItems(X).SubItems(1) = CboCuentaPasivo.Text
                LvCorreccion.ListItems(X).SubItems(2) = 0
                LvCorreccion.ListItems(X).SubItems(3) = 0
                .MoveNext
            Loop
            
            
            LvAsiento.ListItems.Add , , IG_id_CorrecionGanancia
            X = LvAsiento.ListItems.Count
            LvAsiento.ListItems(X).SubItems(1) = Sm_Cuenta_Correccion_Ganancia
            LvAsiento.ListItems(X).SubItems(2) = 0
            LvAsiento.ListItems(X).SubItems(3) = 0
        End With
        
        'Aqui preparamos asiento para Credito de Activos Nuevos
        Busca_Id_Combo Me.CboCuentaPasivo, Val(IG_idCuentaActivoFijoNuevos)
        Sql = "SELECT " & IG_idCuentaActivoFijoNuevos & ",'" & CboCuentaPasivo.Text & "' pla_nombre,0,0 " & _
                "UNION " & _
                "SELECT DISTINCT (a.pla_id),pla_nombre cuenta,0,0 " & _
                "FROM con_activo_inmovilizado a " & _
                "JOIN con_plan_de_cuentas p ON a.pla_id = p.pla_id " & _
                "WHERE aim_nuevo='SI' AND YEAR(aim_fecha_ingreso)=" & ComAno.Text & " " & Sp_FCuenta & " AND rut_emp = '" & SP_Rut_Activo & "'" & Sp_IN & _
                "ORDER BY pla_nombre "
        Consulta RsTmp, Sql
        LLenar_Grilla RsTmp, Me, LvCreditoActivo, False, True, True, False
        
    End If
    
    If Sm_TipoDepreciacion = "INDIRECTA" Then
        'OBTENER valor inicial del activo
        SQL3 = "IFNULL(IFNULL((SELECT dep_valor_actualizado " & _
                "FROM con_depreciaciones k " & _
                "WHERE rut_emp='" & SP_Rut_Activo & "' AND dep_ano=" & Val(ComAno.Text) - 1 & " AND k.aim_id=a.aim_id)," & _
                " (SELECT  SUM(IF(d.vod_debe>0,r.aim_valor,0))-SUM(IF(d.vod_haber>0,r.aim_valor,0)) " & _
                        "FROM con_vouchers_detalle d " & _
                        "JOIN con_relacion_activos r ON d.pla_id = r.pla_id  AND d.vou_id = r.vou_id AND d.vod_id = r.vod_id " & _
                        "JOIN con_vouchers v ON d.vou_id = v.vou_id " & _
                        "WHERE r.aim_id=a.aim_id AND v.rut_emp = '" & SP_Rut_Activo & "'  AND v.vou_ano_contable =" & Sp_Ano & ") " & _
                                    "),aim_valor) aim_valor_C1,"
    Else
        SQL3 = "IFNULL(IFNULL((SELECT dep_valor_actualizado-dep_del_ano " & _
                "FROM con_depreciaciones k " & _
                "WHERE rut_emp='" & SP_Rut_Activo & "' AND dep_ano=" & Val(ComAno.Text) - 1 & " AND k.aim_id=a.aim_id)," & _
                " (SELECT  SUM(IF(d.vod_debe>0,r.aim_valor,0))-SUM(IF(d.vod_haber>0,r.aim_valor,0)) " & _
                        "FROM con_vouchers_detalle d " & _
                        "JOIN con_relacion_activos r ON d.pla_id = r.pla_id  AND d.vou_id = r.vou_id AND d.vod_id = r.vod_id " & _
                        "JOIN con_vouchers v ON d.vou_id = v.vou_id " & _
                        "WHERE r.aim_id=a.aim_id AND v.rut_emp = '" & SP_Rut_Activo & "'  AND v.vou_ano_contable =" & Sp_Ano & ") " & _
                                    "),aim_valor) aim_valor_C1,"
    
    End If
    
    
    
    
    
    'Aqui comenzamos con la dep
    Sql = "SELECT  aim_id,aim_fecha_ingreso,pla_nombre cuenta,aim_nombre," & _
            SQL3 & " " & _
            "IF(YEAR(aim_fecha_ingreso)<" & Sp_Ano & ",IFNULL((SELECT fac_valor FROM con_factores WHERE fac_ano=" & Sp_Ano & " AND fac_mes=13),1.000), " & _
            "IFNULL((SELECT fac_valor FROM con_factores WHERE fac_ano=" & Sp_Ano & " AND fac_mes=MONTH(aim_fecha_ingreso)),1)) AS factor_C2 " & _
            ",0,0,0,0,0,0,0,0,a.pla_id,0,0,pla_dep_acumulada " & _
            "FROM con_activo_inmovilizado a " & _
            "JOIN con_plan_de_cuentas p ON a.pla_id = p.pla_id " & _
            "WHERE YEAR(aim_fecha_ingreso)<=" & Sp_Ano & " AND rut_emp='" & SP_Rut_Activo & "' " & Sp_FCuenta & Sp_IN & " " & _
            "ORDER BY pla_nombre;"
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvDep, False, True, True, False
    
    If RsTmp.RecordCount > 0 Then
        For i = 1 To LvDep.ListItems.Count
            'Columnas 3,4,5,6
            If Sm_TipoDepreciacion = "INDIRECTA" Then
                SQL3 = "IFNULL((SELECT dep_actualizada+dep_del_ano FROM con_depreciaciones d WHERE d.aim_id=" & LvDep.ListItems(i) & " AND dep_ano=" & Sp_Ano & "-1),aim_dacumu ) acumulada_C4 " & _
                    "," & CDbl(LvDep.ListItems(i).SubItems(5)) & " C5,IFNULL((SELECT dep_acumulada FROM con_depreciaciones d WHERE d.aim_id=" & LvDep.ListItems(i) & "  AND dep_ano=" & Sp_Ano & "-1),aim_dacumu)* " & CDbl(LvDep.ListItems(i).SubItems(5)) & " C6, "
            Else
                SQL3 = "0 acumulada_C4,0 C5,0 C6,"
            End If
            
            
            
            Sql = "SELECT " & Round(CDbl(LvDep.ListItems(i).SubItems(4)) * CDbl(LvDep.ListItems(i).SubItems(5)), 0) & " valor_Actualizado_C3," & _
                    SQL3 & _
                    "IF(aim_nuevo='SI' AND YEAR(aim_fecha_ingreso)=" & ComAno.Text & ",'NUEVO','VIEJO') edad " & _
            "FROM con_activo_inmovilizado " & _
            "WHERE aim_id =" & LvDep.ListItems(i)
            Consulta RsTmp, Sql
            'asignamos un 0 a la celda de credito
            LvDep.ListItems(i).SubItems(15) = "0"
            If RsTmp.RecordCount > 0 Then
                Ip_RestaUno = 0
                LvDep.ListItems(i).SubItems(18) = RsTmp!edad
                
                LvDep.ListItems(i).SubItems(6) = NumFormat(RsTmp!valor_Actualizado_C3)
                If RsTmp!edad = "VIEJO" Then
                    'nada
                Else
                    Sql = "SELECT fac_valor " & _
                            "FROM con_factores " & _
                            "WHERE fac_ano=" & ComAno.Text & " AND fac_mes=14 "
                    Consulta RsTmp3, Sql
                    If RsTmp.RecordCount > 0 Then
                        anterior = LvDep.ListItems(i).SubItems(6)
                        
                        LvDep.ListItems(i).SubItems(6) = Round(RsTmp!valor_Actualizado_C3 * RsTmp3!fac_valor, 0)
                        LvDep.ListItems(i).SubItems(15) = NumFormat(CDbl(LvDep.ListItems(i).SubItems(6)) - anterior)
                        LvDep.ListItems(i).SubItems(6) = anterior - LvDep.ListItems(i).SubItems(15)
                    End If
                End If
                
                LvDep.ListItems(i).SubItems(7) = NumFormat(RsTmp!acumulada_C4)
                LvDep.ListItems(i).SubItems(8) = LvDep.ListItems(i).SubItems(5)
                LvDep.ListItems(i).SubItems(9) = NumFormat(CDbl(NumFormat(RsTmp!acumulada_C4)) * CDbl(LvDep.ListItems(i).SubItems(5)))
                LvDep.ListItems(i).SubItems(10) = NumFormat(CDbl(LvDep.ListItems(i).SubItems(6)) - CDbl(LvDep.ListItems(i).SubItems(9)))
            End If
            ' Revisra 15 Abril 2012 Sql = "SELECT IF(" & Sp_Ano & ">aim_fecha_ingreso,12,13-MONTH(aim_fecha_ingreso)) C8," & _
                    "IF(YEAR(aim_fecha_ingreso)<" & Sp_Ano & "," & _
                    "TIMESTAMPDIFF(MONTH,'" & Val(Sp_Ano - 1) & "-12-31',aim_fecha_ingreso),aim_vida_util) C9 " & _
                    "FROM con_activo_inmovilizado a " & _
                    "WHERE aim_id=" & LvDep.ListItems(i)
            Sql = "SELECT IF(" & Sp_Ano & ">YEAR(aim_fecha_ingreso),12,13-MONTH(aim_fecha_ingreso)) C8," & _
                    "aim_vida_util C9 " & _
                    "FROM con_activo_inmovilizado a " & _
                    "WHERE aim_id=" & LvDep.ListItems(i)
            Consulta RsTmp, Sql
            If RsTmp.RecordCount > 0 Then
                LvDep.ListItems(i).SubItems(11) = RsTmp!C8
                LvDep.ListItems(i).SubItems(12) = RsTmp!C9
                
                'Si la vida util restante es menor a 13 (esta dentro de los doce meses)
                'igualar los meses a depreciar a la vida util
                '19-Ago-2014
                If RsTmp!C9 < 13 Then
                    LvDep.ListItems(i).SubItems(11) = RsTmp!C9
                End If
                
                '19 Junio 2014
                'Aqui detectamos si el activo termino su vida util
                
                
                
                
                If Val(LvDep.ListItems(i).SubItems(11)) >= Val(LvDep.ListItems(i).SubItems(12)) Then
                    If Val(LvDep.ListItems(i).SubItems(11)) <> 0 And Val(LvDep.ListItems(i).SubItems(12)) <> 0 Then
                        Ip_RestaUno = 1
                        LvDep.ListItems(i).SubItems(20) = "SI"
                    End If
                
                End If
            End If
            If CDbl(LvDep.ListItems(i).SubItems(12)) > 0 Then
                LvDep.ListItems(i).SubItems(13) = NumFormat(CDbl(LvDep.ListItems(i).SubItems(10)) / CDbl(LvDep.ListItems(i).SubItems(12)) * CDbl(LvDep.ListItems(i).SubItems(11)) - Ip_RestaUno)
                
                LvDep.ListItems(i).SubItems(21) = NumFormat(CDbl(LvDep.ListItems(i).SubItems(6)) - Ip_RestaUno)
            Else
                'Aqui evitamos la division por 0
                '13 Abril 2013
'                LvDep.ListItems(i).SubItems(13) = 0
'                LvDep.ListItems(i).SubItems(4) = 1 'VALOR INICIAL
'                LvDep.ListItems(i).SubItems(6) = 1
'                LvDep.ListItems(i).SubItems(7) = 0
'                LvDep.ListItems(i).SubItems(8) = 0
'                LvDep.ListItems(i).SubItems(9) = 0
'                LvDep.ListItems(i).SubItems(10) = 0
'                LvDep.ListItems(i).SubItems(11) = 0 'MESES A DEPRECIAR
                
                
            End If
            Ip_RestaUno = 0
        Next
    End If
    
    LvActTerminoVidaUtil.ListItems.Clear
    If Sm_TipoDepreciacion = "INDIRECTA" Then
            '28 Junio 2014
            'asiento termino vida util
            'me.LvActTerminoVidaUtil
            Dim Bp_SeEncuentraCuenta As Boolean
            For i = 1 To LvDep.ListItems.Count
                Bp_SeEncuentraCuenta = False
                If LvDep.ListItems(i).SubItems(20) = "SI" Then
                    For X = 1 To LvActTerminoVidaUtil.ListItems.Count
                        If Val(LvDep.ListItems(i).SubItems(17)) = Val(LvActTerminoVidaUtil.ListItems(X)) Then
                            'La cuenta ya esta
                            Bp_SeEncuentraCuenta = True
                            
                            'LvActTerminoVidaUtil.ListItems(X).SubItems(2) = CDbl(LvDep.ListItems(i).SubItems(13)) + _
                            CDbl(LvActTerminoVidaUtil.ListItems(X).SubItems(2))
                            LvActTerminoVidaUtil.ListItems(X).SubItems(2) = CDbl(LvDep.ListItems(i).SubItems(21)) + _
                            CDbl(LvActTerminoVidaUtil.ListItems(X).SubItems(2))
                            
                            Exit For
                        End If
                    Next
                    If Not Bp_SeEncuentraCuenta Then
                        'Agregamos cuenta
                        LvActTerminoVidaUtil.ListItems.Add , , LvDep.ListItems(i).SubItems(17)
                        Busca_Id_Combo CboCuentaPasivo, Val(LvDep.ListItems(i).SubItems(17))
                        LvActTerminoVidaUtil.ListItems(LvActTerminoVidaUtil.ListItems.Count).SubItems(1) = Me.CboCuentaPasivo.Text
                        'LvActTerminoVidaUtil.ListItems(LvActTerminoVidaUtil.ListItems.Count).SubItems(2) = CDbl(LvDep.ListItems(i).SubItems(13))
                        LvActTerminoVidaUtil.ListItems(LvActTerminoVidaUtil.ListItems.Count).SubItems(2) = CDbl(LvDep.ListItems(i).SubItems(21))
                        LvActTerminoVidaUtil.ListItems(LvActTerminoVidaUtil.ListItems.Count).SubItems(4) = LvDep.ListItems(i).SubItems(14)
                    End If
                End If
            Next
                    
            
            
            'TAREA JULIO 2014
            'VERIFICAR LAS CUENTAS QUE ESTAN INCLUIDAS AQUI PARA TERMINAR EL ASIENTO
            For i = 1 To LvActTerminoVidaUtil.ListItems.Count
                
                Bp_SeEncuentraCuenta = False
                
                For X = 1 To LvDep.ListItems.Count
                    If Val(LvActTerminoVidaUtil.ListItems(i).SubItems(4)) = Val(LvDep.ListItems(X).SubItems(14)) And _
                        LvDep.ListItems(X).SubItems(20) = "SI" Then
                        For Z = 1 To LvActTerminoVidaUtil.ListItems.Count
                            If Val(LvActTerminoVidaUtil.ListItems(Z)) = Val(LvDep.ListItems(X).SubItems(14)) Then
                                Bp_SeEncuentraCuenta = True
                                
                                'LvActTerminoVidaUtil.ListItems(Z).SubItems(3) = CDbl(LvActTerminoVidaUtil.ListItems(Z).SubItems(3)) + _
                                (CDbl(LvDep.ListItems(X).SubItems(13)))
                                
                                LvActTerminoVidaUtil.ListItems(Z).SubItems(3) = CDbl(LvActTerminoVidaUtil.ListItems(Z).SubItems(3)) + _
                                (CDbl(LvDep.ListItems(X).SubItems(21)))
                                Exit For
                            End If
                        Next
                        If Not Bp_SeEncuentraCuenta Then
                            LvActTerminoVidaUtil.ListItems.Add , , Val(LvDep.ListItems(X).SubItems(14))
                            LvActTerminoVidaUtil.ListItems(LvActTerminoVidaUtil.ListItems.Count).SubItems(1) = LvDep.ListItems(X).SubItems(2)
                            'LvActTerminoVidaUtil.ListItems(LvActTerminoVidaUtil.ListItems.Count).SubItems(3) = CDbl(LvDep.ListItems(X).SubItems(13))
                            LvActTerminoVidaUtil.ListItems(LvActTerminoVidaUtil.ListItems.Count).SubItems(3) = CDbl(LvDep.ListItems(X).SubItems(21))
                        End If
                    End If
                Next
                
                
            Next
            
    End If
    TotalAsiento LvActTerminoVidaUtil
    
    'indiviualizacion activos
    '5 Feb 2014
    For i = 1 To LvDep.ListItems.Count
        If LvDep.ListItems(i).SubItems(18) = "NUEVO" Then
            LvDep.ListItems(i).SubItems(16) = CDbl(NumFormat((CDbl(LvDep.ListItems(i).SubItems(4)) * Val(CxP(LvDep.ListItems(i).SubItems(5)))) - CDbl(LvDep.ListItems(i).SubItems(4))))
        Else
            LvDep.ListItems(i).SubItems(16) = CDbl(LvDep.ListItems(i).SubItems(6)) - CDbl(LvDep.ListItems(i).SubItems(4))
        End If
        'actualizado - original
        '10 Mayo 2014
    '    LvDep.ListItems(i).SubItems(19) = CDbl(LvDep.ListItems(i).SubItems(6)) - CDbl(LvDep.ListItems(i).SubItems(4))
    Next
    
    If LvDep.ListItems.Count > 0 Then
        ''Totalizamos depreciaciones
        '12 Abril 2013-
        LvDep.ListItems.Add , , ""
        LvDep.ListItems(LvDep.ListItems.Count).SubItems(BuscaIn(LvDep, "inicial")) = NumFormat(TotalizaColumna(LvDep, "inicial"))
        LvDep.ListItems(LvDep.ListItems.Count).SubItems(BuscaIn(LvDep, "actualizado")) = NumFormat(TotalizaColumna(LvDep, "actualizado"))
        LvDep.ListItems(LvDep.ListItems.Count).SubItems(BuscaIn(LvDep, "dacum")) = NumFormat(TotalizaColumna(LvDep, "dacum"))
        LvDep.ListItems(LvDep.ListItems.Count).SubItems(BuscaIn(LvDep, "dactual")) = NumFormat(TotalizaColumna(LvDep, "dactual"))
        LvDep.ListItems(LvDep.ListItems.Count).SubItems(BuscaIn(LvDep, "vdepreciar")) = NumFormat(TotalizaColumna(LvDep, "vdepreciar"))
        LvDep.ListItems(LvDep.ListItems.Count).SubItems(BuscaIn(LvDep, "dano")) = NumFormat(TotalizaColumna(LvDep, "dano"))
        LvDep.ListItems(LvDep.ListItems.Count).SubItems(15) = NumFormat(TotalizaColumna(LvDep, "tcreditoactivofijo"))
        
        For i = 1 To LvDep.ColumnHeaders.Count - 7
            LvDep.ListItems(LvDep.ListItems.Count).ListSubItems(i).Bold = True
        Next
    End If
    
    AsientosContables
End Sub
Private Function BuscaIn(LvB As ListView, Clave As String) As Integer
        For n = 1 To LvB.ColumnHeaders.Count - 1
            If LvB.ColumnHeaders(n).Key = Clave Then
                
                BuscaIn = n - 1
'                LvB.ListItems(LvB.ListItems.Count).ListSubItems(BuscaIn).Bold = True
                Exit For
            End If
        Next
End Function

Private Sub CmdCentralizarVenta_Click()
  Dim Ip_MesesD As Integer, Ip_MesesF As Integer, Lp_AsientoActivoNuevo As Long
    Dim Lp_Vouchers(7) As Long
    
    If Len(TxtRut) = 0 Then
        MsgBox "Debe seleccionar cliente...", vbInformation
        Me.cmdBtnRut.SetFocus
        Exit Sub
    End If
    
    If Val(TxtNroDocumento) = 0 Then
        MsgBox "Ingrese Nro de documento...", vbInformation
        TxtNroDocumento.SetFocus
        Exit Sub
    End If
    
    
    
    If Not ValidaNumero Then
        TxtNroDocumento.SetFocus
        Exit Sub
    End If
    'Validaciones
    If LvDep.ListItems.Count = 0 Then
        MsgBox "No hay movimientos para centralizar...", vbInformation + vbOKOnly
        Exit Sub
    End If
    If MsgBox("Centralizar� venta de Activo inmobilizado ..." & vbNewLine & "                 �Continuar?", vbQuestion + vbYesNo) = vbNo Then Exit Sub
    For i = 1 To 7: Lp_Vouchers(i) = 0: Next
    'Generamos voucher contables
    Lp_Vouchers(1) = CreaVoucherVenta(3, "CORRECCION MONETARIA DE ACTIVOS A�O " & ComAno.Text, LvAsiento)
    Lp_Vouchers(2) = CreaVoucherVenta(3, "DEPRECIACION ACTIVO INMOVILIZADO A�O " & ComAno.Text, LvDepre)
    Lp_Vouchers(3) = CreaVoucherVenta(3, "CREDITO ACTIVO INMOVILIZADO A�O " & ComAno.Text, LvCreditoActivo)
    Lp_Vouchers(4) = CreaVoucherVenta(3, "CORRECCION MONETARIA DEPRECIACIONES A�O " & ComAno.Text, LvCorreccion)
    Lp_Vouchers(5) = CreaVoucherVenta(3, "TERMINO DE VIDA UTIL A�O " & ComAno.Text, LvActTerminoVidaUtil)
    Lp_Vouchers(6) = CreaVoucherVenta(3, "CIERRA DEPRECIACION ACUMULADA " & ComAno.Text, LvCierreDepAcumulada)
    Lp_Vouchers(7) = CreaVoucherVenta(3, "CIERRE ACTIVO INMOVILIZADO " & ComAno.Text, LvCierreActInmovilizado)
    'Aqui guardaremos toda la depreciacion para tenerla como historica
    Sql = "INSERT INTO con_depreciaciones (dep_ano, pla_id,aim_id, dep_valor_inicial, dep_factor," & _
            "dep_valor_actualizado, dep_acumulada, dep_factor_2, dep_actualizada, dep_valor_a_depreciar," & _
            "dep_meses_depreciados, dep_vida_util, dep_del_ano, vou1_id,vou2_id,vou3_id,vou4_id,vou5_id,vou6_id,vou7_id,rut_emp,dep_venta) VALUES "
 '   For i = 1 To LvDep.ListItems.Count - 1
        Sql = Sql & "(" & ComAno.Text & "," & LvDep.SelectedItem.SubItems(14) & "," & LvDep.SelectedItem & "," & _
                CDbl(LvDep.SelectedItem.SubItems(4)) & "," & CxP(LvDep.SelectedItem.SubItems(5)) & "," & CDbl(LvDep.SelectedItem.SubItems(6)) & "," & CDbl(LvDep.SelectedItem.SubItems(7)) & "," & _
                CxP(LvDep.SelectedItem.SubItems(8)) & "," & CDbl(LvDep.SelectedItem.SubItems(9)) & "," & _
                CDbl(LvDep.SelectedItem.SubItems(10)) & "," & CDbl(LvDep.SelectedItem.SubItems(11)) & "," & _
                CDbl(LvDep.SelectedItem.SubItems(12)) & "," & CDbl(LvDep.SelectedItem.SubItems(13)) & "," & _
                Lp_Vouchers(1) & "," & Lp_Vouchers(2) & "," & Lp_Vouchers(3) & "," & Lp_Vouchers(4) & "," & Lp_Vouchers(5) & "," & Lp_Vouchers(6) & "," & Lp_Vouchers(7) & ",'" & SP_Rut_Activo & "','SI'),"
 '   Next
    Sql = Mid(Sql, 1, Len(Sql) - 1)
    On Error GoTo GuardaDepre
    cn.BeginTrans
        cn.Execute Sql
            'Marcamos el activo como "VENDIDO"
            cn.Execute "UPDATE con_activo_inmovilizado SET aim_vendido='SI' " & _
                        "WHERE aim_id=" & LvDep.SelectedItem
        
        '30Ago2014
        'Crearemos el registro de la venta
        Dim fechasa As Date, Lp_VenId As Long
         fechasa = UltimoDiaMes("01-" & CboMesContable.ItemData(CboMesContable.ListIndex) & "-" & ComAno.Text)
         
         elneto = CDbl(TxtValorBruto)
         
         If CDbl(TxtExento) > 0 Then
            elneto = 0
        End If
         Lp_VenId = UltimoNro("ven_doc_venta", "id")
         'Grabamos documento de venta para cuenta corriente
         cn.Execute "INSERT INTO ven_doc_venta " & _
         "(id,no_documento,fecha,rut_cliente,nombre_cliente,bruto,condicionpago," & _
         "forma_pago,tipo_doc,doc_id,usu_nombre,rut_emp,ven_informa_venta,ven_id,ven_plazo_id,sue_id,ven_solo_activo_inmobilizado,exento,iva,neto ) VALUES(" & Lp_VenId & "," & _
         TxtNroDocumento & ",'" & Fql(fechasa) & "',/*rut*/'" & TxtRut & "'," & _
         "/*nombre*/'" & TxtNombre & "'," & _
         "/*VALOR*/" & CDbl(txtElTotal) & ",'CREDIT0','PENDIENTE',/*DOCUMENTO*/'" & CboDocVenta.Text & "'," & _
         "/*ID DOCUMENTO*/" & CboDocVenta.ItemData(CboDocVenta.ListIndex) & ",'" & LogUsuario & "','" & SP_Rut_Activo & "','SI'," & 1 & ",30," & 1 & ",'SI'," & CDbl(TxtExento) & "," & CDbl(txtElIVA) & "," & elneto & ")"
         
         'Agregaremos una linea de detalle
         
         InsertaProductoAsientoApertura CreaProductoAsientoApertura, CDbl(txtElTotal), TxtNroDocumento, CboDocVenta.ItemData(CboDocVenta.ListIndex), "CLI", Lp_VenId, SP_Control_Inventario
         
                'Grabamos auxiliar con detalle
        cn.Execute "INSERT INTO con_voucher_detalle_auxiliar (vdt_tipo,vod_id,vdt_rel) " & _
                    "VALUES('CLI'," & Lp_VodIdVentaActivo & "," & Lp_VenId & ")"
        
    cn.CommitTrans
    MsgBox "Centralizacion Correcta...", vbInformation
    CmdBuscar_Click
    Exit Sub
GuardaDepre:
    cn.RollbackTrans
    MsgBox "Error al grabar Depreciacion historica..." & vbNewLine & Err.Number & vbNewLine & Err.Description
End Sub

Private Sub cmdCuenta_Click()
    With BuscarSimple
        SG_codigo = Empty
        .Sm_Consulta = "SELECT pla_id, pla_nombre,tpo_nombre,det_nombre " & _
                       "FROM    con_plan_de_cuentas p,  con_tipo_de_cuenta t,   con_detalle_tipo_cuenta d " & _
                       "WHERE   p.tpo_id = t.tpo_id AND p.det_id = d.det_id AND p.tpo_id=1 AND p.det_id=4 "
        .Sm_CampoLike = "pla_nombre"
        .Im_Columnas = 2
        .Show 1
        If SG_codigo = Empty Then Exit Sub
        Busca_Id_Combo CboCuenta, Val(SG_codigo)
        TxtNroCuenta = SG_codigo
    End With
End Sub

Private Sub CmdExportar_Click()
    Dim tit(2) As String
    
    FraProgreso.Visible = True
    tit(0) = Me.Caption & " " & DtFecha & " - " & Time
   
    tit(2) = ""
    
    If TabS.SelectedItem.Index = 1 Then
        If LvDep.ListItems.Count = 0 Then Exit Sub
         tit(1) = "ACTUALES"
        ExportarNuevo LvDep, tit, Me, BarraProgreso
    Else
        If LvHist.ListItems.Count = 0 Then Exit Sub
        tit(1) = "HISTORICOS"
        ExportarNuevo Me.LvHist, tit, Me, BarraProgreso
    End If
    FraProgreso.Visible = False
End Sub

Private Sub cmdSalir_Click()
    Unload Me
End Sub


Private Sub CmdVende_Click()
    If CboMesContable.ListIndex = -1 Then
        MsgBox "Seleccione mes de venta...", vbInformation
        Exit Sub
    End If
    If LvDep.SelectedItem Is Nothing Then
        MsgBox "No hay activo seleccionado...", vbInformation
        Exit Sub
    End If
    
    If Val(TxtValorBruto) < 1 Then
        MsgBox "Debe ingresar Valor de venta del Activo...", vbInformation
        TxtValorBruto.SetFocus
        Exit Sub
    End If
    
    LvAsiento.ListItems.Clear
    LvActTerminoVidaUtil.ListItems.Clear
    LvCorreccion.ListItems.Clear
    LvCreditoActivo.ListItems.Clear
    LvDepre.ListItems.Clear
    VenderActivo
    CmdAsiento.Visible = False
    CmdCentralizarVenta.Visible = True
    
End Sub
Private Sub ComAno_Click()
    If ComAno.ListIndex = -1 Then Exit Sub
    Exit Sub
    LvDetalle.ListItems.Clear
    Sql = "SELECT fac_ene,fac_feb,fac_mar,fac_abr,fac_may,fac_jun, " & _
            "fac_jul,fac_ago,fac_sep,fac_oct,fac_nov,fac_dic," & _
            "fac_capital_propio,fac_bien_nuevo " & _
        "FROM con_factores_sii " & _
        "WHERE fac_ano=" & ComAno.Text
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        RsTmp.MoveFirst
        For i = 0 To RsTmp.Fields.Count - 1
            LvDetalle.ListItems.Add , , RsTmp.Fields(i)
        Next
    Else
        For i = 1 To 14
            LvDetalle.ListItems.Add , , "1.000"
        Next
    End If
    
    Sql = "SELECT aim_id,aim_fecha_ingreso,pla_nombre cuenta ,aim_nombre, aim_valor," & _
            "IF(YEAR(aim_fecha_ingreso)<" & ComAno.Text & ", " & _
            LvDetalle.ListItems(13) & ", " & _
            "X" & ") " & _
            "FROM con_activo_inmovilizado a " & _
            "JOIN con_plan_de_cuentas p ON a.pla_id=p.pla_id " & _
            "ORDER BY pla_nombre"
    Consulta RsTmp, Sql
        LLenar_Grilla RsTmp, Me, LvDep, False, True, True, False

End Sub



Private Sub Form_Load()
    Centrar Me
    Skin2 Me, , 7
   ' Aplicar_skin Me
   LLenaYears ComAno, 2010
   Lp_TopBase = 225
   Picture2.Top = 5445
    For i = 1 To 12
        CboMesContable.AddItem UCase(MonthName(i, False))
        CboMesContable.ItemData(CboMesContable.ListCount - 1) = i
    Next
    
    LLenarCombo Me.CboDocVenta, "doc_nombre", "doc_id", "sis_documentos", "doc_activo='SI' AND doc_documento='VENTA' AND doc_utiliza_en_caja='" & SG_Funcion_Equipo & "'", "doc_id"
   
    
    LLenarCombo CboCuenta, "pla_nombre", "pla_id", "con_plan_de_cuentas a ", "tpo_id=1 AND det_id=4 "
    LLenarCombo CboCuentaPasivo, "pla_nombre", "pla_id", "con_plan_de_cuentas a ", "1=1 "
    CboCuenta.AddItem "TODAS LAS CUENTAS"
    CboCuenta.ListIndex = CboCuenta.ListCount - 1
    
    Sql = "SELECT pla_id,pla_nombre " & _
            "FROM con_plan_de_cuentas " & _
            "WHERE pla_id IN(" & IG_id_CorrecionPerdida & "," & IG_id_CorrecionGanancia & ")"
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        RsTmp.MoveFirst
        Do While Not RsTmp.EOF
            If RsTmp!pla_id = IG_id_CorrecionPerdida Then Sm_Cuenta_Correccion_Perdida = RsTmp!pla_nombre
            If RsTmp!pla_id = IG_id_CorrecionGanancia Then Sm_Cuenta_Correccion_Ganancia = RsTmp!pla_nombre
            RsTmp.MoveNext
        Loop
    End If
    
    Sql = "SELECT emp_tipo_depreciacion tipo " & _
            "FROM sis_empresas " & _
            "WHERE rut='" & SP_Rut_Activo & "'"
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        Sm_TipoDepreciacion = RsTmp!Tipo
    End If
    
End Sub








Private Sub LvAsiento_DblClick()
        If LvAsiento.SelectedItem Is Nothing Then Exit Sub

        With BuscarSimple
            SG_codigo = Empty
            .Sm_Consulta = "SELECT pla_id, pla_nombre,tpo_nombre,det_nombre " & _
                           "FROM    con_plan_de_cuentas p,  con_tipo_de_cuenta t,   con_detalle_tipo_cuenta d " & _
                           "WHERE   p.tpo_id = t.tpo_id AND p.det_id = d.det_id  /*AND p.tpo_id=2  AND p.det_id=4 */"
            .Sm_CampoLike = "pla_nombre"
            .Im_Columnas = 2
            .Show 1
            If SG_codigo = Empty Then Exit Sub
            
            For i = 1 To LvAsiento.ListItems.Count
                If Val(LvAsiento.ListItems(i)) = Val(SG_codigo) Then
                    MsgBox "Esta cuenta ya se encuentra en la lista...", vbInformation
                    Exit Sub
                End If
            Next
            
            Busca_Id_Combo CboCuentaPasivo, Val(SG_codigo)
            
            LvAsiento.SelectedItem = SG_codigo
            LvAsiento.SelectedItem.SubItems(1) = CboCuentaPasivo.Text
            
        End With
End Sub

Private Sub LvCorreccion_DblClick()
        If LvCorreccion.SelectedItem Is Nothing Then Exit Sub

        With BuscarSimple
            SG_codigo = Empty
            .Sm_Consulta = "SELECT pla_id, pla_nombre,tpo_nombre,det_nombre " & _
                           "FROM    con_plan_de_cuentas p,  con_tipo_de_cuenta t,   con_detalle_tipo_cuenta d " & _
                           "WHERE   p.tpo_id = t.tpo_id AND p.det_id = d.det_id AND p.tpo_id=2 /* AND p.det_id=4 */"
            .Sm_CampoLike = "pla_nombre"
            .Im_Columnas = 2
            .Show 1
            If SG_codigo = Empty Then Exit Sub
            
            For i = 1 To LvCorreccion.ListItems.Count
                If Val(LvCorreccion.ListItems(i)) = Val(SG_codigo) Then
                    MsgBox "Esta cuenta ya se encuentra en la lista...", vbInformation
                    Exit Sub
                End If
            Next
            
            Busca_Id_Combo CboCuentaPasivo, Val(SG_codigo)
            
            LvCorreccion.SelectedItem = SG_codigo
            LvCorreccion.SelectedItem.SubItems(1) = CboCuentaPasivo.Text
            
        End With
End Sub

Private Sub LvCreditoActivo_DblClick()
        If LvCreditoActivo.SelectedItem Is Nothing Then Exit Sub

        With BuscarSimple
            SG_codigo = Empty
            .Sm_Consulta = "SELECT pla_id, pla_nombre,tpo_nombre,det_nombre " & _
                           "FROM    con_plan_de_cuentas p,  con_tipo_de_cuenta t,   con_detalle_tipo_cuenta d " & _
                           "WHERE   p.tpo_id = t.tpo_id AND p.det_id = d.det_id /* and p.tpo_id=2 AND p.det_id=4 */"
            .Sm_CampoLike = "pla_nombre"
            .Im_Columnas = 2
            .Show 1
            If SG_codigo = Empty Then Exit Sub
            
            For i = 1 To LvCreditoActivo.ListItems.Count
                If Val(LvCreditoActivo.ListItems(i)) = Val(SG_codigo) Then
                    MsgBox "Esta cuenta ya se encuentra en la lista...", vbInformation
                    Exit Sub
                End If
            Next
            
            Busca_Id_Combo CboCuentaPasivo, Val(SG_codigo)
            
            LvCreditoActivo.SelectedItem = SG_codigo
            LvCreditoActivo.SelectedItem.SubItems(1) = CboCuentaPasivo.Text
            
        End With
End Sub

Private Sub LvDepre_DblClick()
        If LvDepre.SelectedItem Is Nothing Then Exit Sub

        With BuscarSimple
            SG_codigo = Empty
            .Sm_Consulta = "SELECT pla_id, pla_nombre,tpo_nombre,det_nombre " & _
                           "FROM    con_plan_de_cuentas p,  con_tipo_de_cuenta t,   con_detalle_tipo_cuenta d " & _
                           "WHERE   p.tpo_id = t.tpo_id AND p.det_id = d.det_id AND p.tpo_id=2 /* AND p.det_id=4 */"
            .Sm_CampoLike = "pla_nombre"
            .Im_Columnas = 2
            .Show 1
            If SG_codigo = Empty Then Exit Sub
            
            For i = 1 To LvDepre.ListItems.Count
                If Val(LvDepre.ListItems(i)) = Val(SG_codigo) Then
                    MsgBox "Esta cuenta ya se encuentra en la lista...", vbInformation
                    Exit Sub
                End If
            Next
            
            Busca_Id_Combo CboCuentaPasivo, Val(SG_codigo)
            
            LvDepre.SelectedItem = SG_codigo
            LvDepre.SelectedItem.SubItems(1) = CboCuentaPasivo.Text
            
        End With
End Sub




Private Sub TabS_Click()
    If TabS.SelectedItem.Index = 1 Then
        LvDep.Visible = True
        LvHist.Visible = False
    ElseIf TabS.SelectedItem.Index = 2 Then
        LvDep.Visible = False
        LvHist.Visible = True
    End If
End Sub

Private Sub Timer3_Timer()
    On Error Resume Next
    ComAno.SetFocus
    Timer3.Enabled = False

End Sub
Private Sub TxtNroCuenta_GotFocus()
    En_Foco TxtNroCuenta
End Sub
Private Sub TxtNroCuenta_KeyPress(KeyAscii As Integer)
    KeyAscii = AceptaSoloNumeros(KeyAscii)
End Sub
Private Sub TxtNroCuenta_Validate(Cancel As Boolean)
    If Val(TxtNroCuenta) = 0 Then Exit Sub
    Busca_Id_Combo CboCuenta, Val(TxtNroCuenta)
    If CboCuenta.ListIndex = -1 Then
        MsgBox "Cuenta no existe..."
    End If
End Sub

Function CreaVoucher(Tipo As Integer, Glosa As String, LVa As ListView) As Long
    Dim Sp_IndInm As String
    Dim Lp_Vou As Long, Lp_Nro_Voucher As Long, Sp_TipoV As Integer, Ip_dia As Integer
    Dim Lp_Vod As Long
    If CDbl(LVa.ListItems(LVa.ListItems.Count).SubItems(2)) <> CDbl(LVa.ListItems(LVa.ListItems.Count).SubItems(3)) Then
        MsgBox "Asiento descuadrado, corriga origen..." & vbNewLine & Glosa
        Exit Function
    End If
    'Aqui comprobamos si los totales estan en 0, para no generar vocuher.
    If CDbl(LVa.ListItems(LVa.ListItems.Count).SubItems(2)) = 0 And CDbl(LVa.ListItems(LVa.ListItems.Count).SubItems(3)) = 0 Then
        CreaVoucher = 0
        Exit Function
    End If
    On Error GoTo Grabando
    Ip_dia = Mid(UltimoDiaMes("01-12-" & ComAno.Text), 1, 2)
    cn.BeginTrans
        Lp_Vou = UltimoNro("con_vouchers", "vou_id")
        CreaVoucher = Lp_Vou
        Lp_Nro_Voucher = UltimoMayor("con_vouchers", "vou_numero", SP_Rut_Activo)
        Sql = "INSERT INTO con_vouchers (vou_id,vou_numero,vou_dia,vou_mes_contable,vou_ano_contable,vou_fecha_ingreso,vou_glosa,rut_emp,vou_tipo,vou_centralizacion,vou_id_asiento,vou_tiponiif) " & _
            "VALUES(" & Lp_Vou & "," & Lp_Nro_Voucher & "," & 31 & "," & 12 & "," & ComAno.Text & ",'" & Fql(Date) & "','" & Glosa & "','" & SP_Rut_Activo & "'," & Tipo & ",'SI'," & 14 & "," & 1 & ")"
        cn.Execute Sql
        Sql = "INSERT INTO con_vouchers_detalle (vod_id,vou_id,pla_id,vod_debe,vod_haber) VALUES "
        For i = 1 To LVa.ListItems.Count
            If Val(LVa.ListItems(i)) > 0 Then
                sql2 = ""
                Lp_Vod = UltimoNro("con_vouchers_detalle", "vod_id")
                sql2 = "(" & Lp_Vod & "," & Lp_Vou & "," & LVa.ListItems(i) & "," & CDbl(LVa.ListItems(i).SubItems(2)) & "," & CDbl(LVa.ListItems(i).SubItems(3)) & ")"
                cn.Execute Sql & sql2
                '5 Feb 2014'
                'Crear relacion de los activos inmobilizados con su actualizacion de activos,
                'individualizando cada uno de ellos.
                If LVa.Name = "LvAsiento" Then
                '    For i = 1 To LvAsiento.ListItems.Count
                        Sp_IndInm = ""
                        For X = 1 To LvDep.ListItems.Count
                            If LvAsiento.ListItems(i) = LvDep.ListItems(X).SubItems(14) Then
                                Sp_IndInm = Sp_IndInm & "(" & Lp_Vod & "," & LvDep.ListItems(X) & "," & LvDep.ListItems(X).SubItems(16) & "),"
                            End If
                        Next
                        If Len(Sp_IndInm) > 0 Then
                            cn.Execute "INSERT INTO con_individualiza_valor_voucher_activo_inm " & _
                            "(vod_id,aim_id,ind_valor) " & _
                            "VALUES " & Mid(Sp_IndInm, 1, Len(Sp_IndInm) - 1)
                        End If
                        '12-Julio-2014
                        'Tambien crear relacion en la tabla con_relacion_activos
                        'para vouchers
                        Sp_IndInm = ""
                        For X = 1 To LvDep.ListItems.Count - 1
                            If LvAsiento.ListItems(i) = LvDep.ListItems(X).SubItems(14) Then
                                Sp_IndInm = Sp_IndInm & "(" & Lp_Vou & "," & Lp_Vod & "," & LvDep.ListItems(X) & "," & LvDep.ListItems(X).SubItems(16) & "," & LvDep.ListItems(X).SubItems(14) & "),"
                            End If
                        Next
                        If Len(Sp_IndInm) > 0 Then
                            cn.Execute "INSERT INTO con_relacion_activos  " & _
                            "(vou_id,vod_id,aim_id,aim_valor,pla_id) " & _
                            "VALUES " & Mid(Sp_IndInm, 1, Len(Sp_IndInm) - 1)
                        End If
                End If
                
                If Sm_TipoDepreciacion = "DIRECTA" Then
                    'Crear relacion de tabla con_relacion_activos
                    If LVa.Name = "LvDepre" Then
                        '13-9-2014
                        Sp_IndInm = ""
                        For X = 1 To LvDep.ListItems.Count - 1
                            If LvDepre.ListItems(i) = LvDep.ListItems(X).SubItems(14) Then
                                Sp_IndInm = Sp_IndInm & "(" & Lp_Vou & "," & Lp_Vod & "," & LvDep.ListItems(X) & "," & CDbl(LvDep.ListItems(X).SubItems(13)) & "," & LvDep.ListItems(X).SubItems(14) & "),"
                            End If
                        Next
                        If Len(Sp_IndInm) > 0 Then
                            cn.Execute "INSERT INTO con_relacion_activos  " & _
                            "(vou_id,vod_id,aim_id,aim_valor,pla_id) " & _
                            "VALUES " & Mid(Sp_IndInm, 1, Len(Sp_IndInm) - 1)
                        End If
                    
                    End If
                Else 'INDIRECTA
                
                 'Crear relacion de tabla con_relacion_activos
                    If LVa.Name = "LvDepre" Then
                        '13-9-2014
                        Sp_IndInm = ""
                        For X = 1 To LvDep.ListItems.Count - 1
                            If LvDepre.ListItems(i).SubItems(4) = LvDep.ListItems(X).SubItems(14) Then
                                Sp_IndInm = Sp_IndInm & "(" & Lp_Vou & "," & Lp_Vod & "," & LvDep.ListItems(X) & "," & CDbl(LvDep.ListItems(X).SubItems(13)) & "," & LvDep.ListItems(X).SubItems(17) & "),"
                            End If
                        Next
                        If Len(Sp_IndInm) > 0 Then
                            cn.Execute "INSERT INTO con_relacion_activos  " & _
                            "(vou_id,vod_id,aim_id,aim_valor,pla_id) " & _
                            "VALUES " & Mid(Sp_IndInm, 1, Len(Sp_IndInm) - 1)
                        End If
                    
                    End If
                    
                    
                
                End If
                
                If LVa.Name = "LvActTerminoVidaUtil" Then


                        '12-Julio-2014
                        'Tambien crear relacion en la tabla con_relacion_activos
                        'para vouchers
                        Sp_IndInm = ""
                        For X = 1 To LvDep.ListItems.Count - 1
                            If LvActTerminoVidaUtil.ListItems(i) = LvDep.ListItems(X).SubItems(14) And LvDep.ListItems(X).SubItems(20) = "SI" Then
                                Sp_IndInm = Sp_IndInm & "(" & Lp_Vou & "," & Lp_Vod & "," & LvDep.ListItems(X) & "," & CDbl(LvDep.ListItems(X).SubItems(21)) & "," & LvDep.ListItems(X).SubItems(14) & "),"
                            End If
                        Next
                        If Len(Sp_IndInm) > 0 Then
                            cn.Execute "INSERT INTO con_relacion_activos  " & _
                            "(vou_id,vod_id,aim_id,aim_valor,pla_id) " & _
                            "VALUES " & Mid(Sp_IndInm, 1, Len(Sp_IndInm) - 1)
                        End If
                End If
                
                If Sm_TipoDepreciacion = "DIRECTA" Then
                    If LVa.Name = "LvCreditoActivo" Then
                            '01-Nov-2014
                            'Tambien crear relacion en la tabla con_relacion_activos
                            'para vouchers
                            Sp_IndInm = ""
                            For X = 1 To LvDep.ListItems.Count - 1
                                If LvCreditoActivo.ListItems(i) = LvDep.ListItems(X).SubItems(14) Then
                                    Sp_IndInm = Sp_IndInm & "(" & Lp_Vou & "," & Lp_Vod & "," & LvDep.ListItems(X) & "," & CDbl(LvCreditoActivo.ListItems(i).SubItems(2)) + CDbl(LvCreditoActivo.ListItems(i).SubItems(3)) & "," & LvDep.ListItems(X).SubItems(14) & "),"
                                End If
                            Next
                            If Len(Sp_IndInm) > 0 Then
                                cn.Execute "INSERT INTO con_relacion_activos  " & _
                                "(vou_id,vod_id,aim_id,aim_valor,pla_id) " & _
                                "VALUES " & Mid(Sp_IndInm, 1, Len(Sp_IndInm) - 1)
                            End If
                    End If
                End If
                
                
                
                
                
            End If
        Next
        'Aqui agregamos el voucher
        Sql = "INSERT INTO con_centralizaciones (asi_id,ctl_mes,ctl_ano,rut_emp,ctl_usuario,ctl_fecha) " & _
              "VALUES(" & 14 & "," & 12 & "," & ComAno.Text & ",'" & SP_Rut_Activo & "','" & LogUsuario & "','" & Fql(Date) & "')"
        cn.Execute Sql
    cn.CommitTrans
    Exit Function
Grabando:    MsgBox "Ocurrio un error al intentar grabar Voucher..." & vbNewLine & Err.Number & " " & Err.Description, vbInformation
    cn.RollbackTrans

End Function


Function CreaVoucherVenta(Tipo As Integer, Glosa As String, LVa As ListView) As Long
    'Solo para venta de activo inmobilizado
    '
    Dim Sp_IndInm As String
    Dim Lp_Vou As Long, Lp_Nro_Voucher As Long, Sp_TipoV As Integer, Ip_dia As Integer
    Dim Lp_Vod As Long
    If CDbl(LVa.ListItems(LVa.ListItems.Count).SubItems(2)) <> CDbl(LVa.ListItems(LVa.ListItems.Count).SubItems(3)) Then
        MsgBox "Asiento descuadrado, corriga origen..." & vbNewLine & Glosa
        Exit Function
    End If
    'Aqui comprobamos si los totales estan en 0, para no generar vocuher.
    If CDbl(LVa.ListItems(LVa.ListItems.Count).SubItems(2)) = 0 And CDbl(LVa.ListItems(LVa.ListItems.Count).SubItems(3)) = 0 Then
        CreaVoucherVenta = 0
        Exit Function
    End If
    On Error GoTo Grabando
    Ip_dia = Mid(UltimoDiaMes("01-" & CboMesContable.ItemData(CboMesContable.ListIndex) & "-" & ComAno.Text), 1, 2)
    cn.BeginTrans
        Lp_Vou = UltimoNro("con_vouchers", "vou_id")
        CreaVoucherVenta = Lp_Vou
        Lp_Nro_Voucher = UltimoMayor("con_vouchers", "vou_numero", SP_Rut_Activo)
        Sql = "INSERT INTO con_vouchers (vou_id,vou_numero,vou_dia,vou_mes_contable,vou_ano_contable,vou_fecha_ingreso,vou_glosa,rut_emp,vou_tipo,vou_centralizacion,vou_id_asiento,vou_tiponiif) " & _
            "VALUES(" & Lp_Vou & "," & Lp_Nro_Voucher & "," & Ip_dia & "," & CboMesContable.ItemData(CboMesContable.ListIndex) & "," & ComAno.Text & ",'" & Fql(Date) & "','" & Glosa & "','" & SP_Rut_Activo & "'," & Tipo & ",'SI'," & 14 & "," & 1 & ")"
        cn.Execute Sql
        Sql = "INSERT INTO con_vouchers_detalle (vod_id,vou_id,pla_id,vod_debe,vod_haber) VALUES "
        For i = 1 To LVa.ListItems.Count
            If Val(LVa.ListItems(i)) > 0 Then
                sql2 = ""
                Lp_Vod = UltimoNro("con_vouchers_detalle", "vod_id")
                sql2 = "(" & Lp_Vod & "," & Lp_Vou & "," & LVa.ListItems(i) & "," & CDbl(LVa.ListItems(i).SubItems(2)) & "," & CDbl(LVa.ListItems(i).SubItems(3)) & ")"
                cn.Execute Sql & sql2
                '5 Feb 2014'
                'Crear relacion de los activos inmobilizados con su actualizacion de activos,
                'individualizando cada uno de ellos.
                If LVa.Name = "LvAsiento" Then
                '    For i = 1 To LvAsiento.ListItems.Count
                        Sp_IndInm = ""
                       ' For X = 1 To LvDep.ListItems.Count
                            If LvAsiento.ListItems(i) = LvDep.SelectedItem.SubItems(14) Then
                                Sp_IndInm = Sp_IndInm & "(" & Lp_Vod & "," & LvDep.SelectedItem & "," & LvDep.SelectedItem.SubItems(16) & "),"
                            End If
                      '  Next
                        If Len(Sp_IndInm) > 0 Then
                            cn.Execute "INSERT INTO con_individualiza_valor_voucher_activo_inm " & _
                            "(vod_id,aim_id,ind_valor) " & _
                            "VALUES " & Mid(Sp_IndInm, 1, Len(Sp_IndInm) - 1)
                        End If
                        '12-Julio-2014
                        'Tambien crear relacion en la tabla con_relacion_activos
                        'para vouchers
                        Sp_IndInm = ""
                        'For X = 1 To LvDep.ListItems.Count - 1
                            If LvAsiento.ListItems(i) = LvDep.SelectedItem.SubItems(14) Then
                                Sp_IndInm = Sp_IndInm & "(" & Lp_Vou & "," & Lp_Vod & "," & LvDep.SelectedItem & "," & LvDep.SelectedItem.SubItems(16) & "," & LvDep.SelectedItem.SubItems(14) & "),"
                            End If
                        'Next
                        If Len(Sp_IndInm) > 0 Then
                            cn.Execute "INSERT INTO con_relacion_activos  " & _
                            "(vou_id,vod_id,aim_id,aim_valor,pla_id) " & _
                            "VALUES " & Mid(Sp_IndInm, 1, Len(Sp_IndInm) - 1)
                        End If
                End If
                
                
                If LVa.Name = "LvActTerminoVidaUtil" Then
                        '12-Julio-2014
                        'Tambien crear relacion en la tabla con_relacion_activos
                        'para vouchers
                        Sp_IndInm = ""
                    '    For X = 1 To LvDep.ListItems.Count - 1
                            If LvActTerminoVidaUtil.ListItems(i) = LvDep.SelectedItem.SubItems(14) And LvDep.SelectedItem.SubItems(20) = "SI" Then
                                Sp_IndInm = Sp_IndInm & "(" & Lp_Vou & "," & Lp_Vod & "," & LvDep.SelectedItem & "," & CDbl(LvDep.SelectedItem.SubItems(21)) & "," & LvDep.SelectedItem.SubItems(14) & "),"
                            End If
                    '    Next
                        If Len(Sp_IndInm) > 0 Then
                            cn.Execute "INSERT INTO con_relacion_activos  " & _
                            "(vou_id,vod_id,aim_id,aim_valor,pla_id) " & _
                            "VALUES " & Mid(Sp_IndInm, 1, Len(Sp_IndInm) - 1)
                        End If
                End If
                
                If LVa.Name = "LvCierreActInmovilizado" Then
                    Lp_VodIdVentaActivo = Lp_Vod
                    '6 sep 2014
                    'Tambien crear relacion en la tabla con_relacion_activos
                    'para vouchers
                    Sp_IndInm = ""
                '    For X = 1 To LvDep.ListItems.Count - 1
                        If LvCierreActInmovilizado.ListItems(i) = LvDep.SelectedItem.SubItems(14) Then
                            Sp_IndInm = Sp_IndInm & "(" & Lp_Vou & "," & Lp_Vod & "," & LvDep.SelectedItem & "," & CDbl(LvCierreActInmovilizado.ListItems(i).SubItems(3)) & "," & LvDep.SelectedItem.SubItems(14) & "),"
                        End If
                '    Next
                    If Len(Sp_IndInm) > 0 Then
                        cn.Execute "INSERT INTO con_relacion_activos  " & _
                        "(vou_id,vod_id,aim_id,aim_valor,pla_id) " & _
                        "VALUES " & Mid(Sp_IndInm, 1, Len(Sp_IndInm) - 1)
                    End If
                End If
                If LVa.Name = "LvCierreDepAcumulada" Then
                   
                    '6 sep 2014
                    'Tambien crear relacion en la tabla con_relacion_activos
                    'para vouchers
                    Sp_IndInm = ""
                   ' For X = 1 To LvDep.ListItems.Count - 1
                        If LvCierreDepAcumulada.ListItems(i) = LvDep.SelectedItem.SubItems(14) Then
                            Sp_IndInm = Sp_IndInm & "(" & Lp_Vou & "," & Lp_Vod & "," & LvDep.SelectedItem & "," & CDbl(LvCierreDepAcumulada.ListItems(i).SubItems(3)) & "," & LvDep.SelectedItem.SubItems(14) & "),"
                        End If
                   ' Next
                    If Len(Sp_IndInm) > 0 Then
                        cn.Execute "INSERT INTO con_relacion_activos  " & _
                        "(vou_id,vod_id,aim_id,aim_valor,pla_id) " & _
                        "VALUES " & Mid(Sp_IndInm, 1, Len(Sp_IndInm) - 1)
                    End If
                End If
                
                
                If LVa.Name = "LvDepre" Then
                   
                    '6 sep 2014
                    'Tambien crear relacion en la tabla con_relacion_activos
                    'para vouchers
                    Sp_IndInm = ""
                   ' For X = 1 To LvDep.ListItems.Count - 1
                        If LVa.ListItems(i) = LvDep.SelectedItem.SubItems(14) Then
                            Sp_IndInm = Sp_IndInm & "(" & Lp_Vou & "," & Lp_Vod & "," & LvDep.SelectedItem & "," & CDbl(LVa.ListItems(i).SubItems(3)) & "," & LvDep.SelectedItem.SubItems(14) & "),"
                        End If
                   ' Next
                    If Len(Sp_IndInm) > 0 Then
                        cn.Execute "INSERT INTO con_relacion_activos  " & _
                        "(vou_id,vod_id,aim_id,aim_valor,pla_id) " & _
                        "VALUES " & Mid(Sp_IndInm, 1, Len(Sp_IndInm) - 1)
                    End If
                End If
                
                
                
            End If
        Next
        'Aqui agregamos el voucher
        Sql = "INSERT INTO con_centralizaciones (asi_id,ctl_mes,ctl_ano,rut_emp,ctl_usuario,ctl_fecha) " & _
              "VALUES(" & 14 & "," & CboMesContable.ItemData(CboMesContable.ListIndex) & "," & ComAno.Text & ",'" & SP_Rut_Activo & "','" & LogUsuario & "','" & Fql(Date) & "')"
        cn.Execute Sql
    cn.CommitTrans
    Exit Function
Grabando:    MsgBox "Ocurrio un error al intentar grabar Voucher por venta de activo..." & vbNewLine & Err.Number & " " & Err.Description, vbInformation
    cn.RollbackTrans

End Function


Private Sub TxtNroDocumento_GotFocus()
    En_Foco TxtNroDocumento
End Sub

Private Sub TxtNroDocumento_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
    If KeyAscii = 39 Then KeyAscii = 0
End Sub


Private Function ValidaNumero() As Boolean
    Sql = "SELECT id " & _
            "FROM ven_doc_venta " & _
            "WHERE doc_id=" & CboDocVenta.ItemData(CboDocVenta.ListIndex) & " AND no_documento=" & TxtNroDocumento & " AND rut_emp='" & SP_Rut_Activo & "'"
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        MsgBox "Documento ya ha sido utilizado..."
        ValidaNumero = False
    Else
        ValidaNumero = True
    End If
End Function

Private Sub TxtRut_Validate(Cancel As Boolean)
    Dim Sp_CP As String
    txtCliente = ""
    If Len(TxtRut.Text) = 0 Then Exit Sub
    TxtRut.Text = Replace(TxtRut.Text, ".", "")
    TxtRut.Text = Replace(TxtRut.Text, "-", "")
    Respuesta = VerificaRut(TxtRut.Text, NuevoRut)
    If Respuesta = True Then
        Me.TxtRut.Text = NuevoRut
        Sql = "SELECT rut_cliente rut," & _
                        "m.nombre_rsocial nombre," & _
                        "fono,email " & _
                  "FROM maestro_clientes m " & _
                  "INNER JOIN par_asociacion_ruts a ON m.rut_cliente=a.rut_cli " & _
                  "WHERE  a.rut_emp='" & SP_Rut_Activo & "' AND  m.rut_cliente='" & TxtRut & "'"
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
            txtCliente = RsTmp!nombre
        End If
    End If
End Sub

Private Sub TxtValorBruto_GotFocus()
    En_Foco TxtValorBruto
End Sub

Private Sub TxtValorBruto_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
    If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtValorBruto_Validate(Cancel As Boolean)
    txtElIVA = 0
    txtElTotal = 0
    TxtExento = 0
    If CboDocVenta.ListIndex = -1 Then
        MsgBox "Seleccione documento de venta...", vbInformation
        CboDocVenta.SetFocus
        Exit Sub
    End If
    If Val(TxtValorBruto) > 0 Then
        'Buscar si el documento tiene iva
        Sql = "SELECT doc_factor_iva factor,doc_solo_exento " & _
                "FROM sis_documentos " & _
                "WHERE doc_id=" & CboDocVenta.ItemData(CboDocVenta.ListIndex)
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
            'multiplicar neto por iva
            If RsTmp!factor > 0 Then
                txtElTotal = NumFormat(Val(TxtValorBruto) * Val("1." & RsTmp!factor))
                TxtValorBruto = NumFormat(TxtValorBruto)
                txtElIVA = NumFormat(CDbl(txtElTotal) - CDbl(TxtValorBruto))
            Else
                txtElIVA = 0
                txtElTotal = NumFormat(TxtValorBruto)
                TxtValorBruto = txtElTotal
                If RsTmp!doc_solo_exento = "SI" Then TxtExento = TxtValorBruto
            End If
        End If
    End If
    
End Sub

Private Sub VScroll1_Change()
    
    FraDep(0).Top = Lp_TopBase - VScroll1.Value
    FraDep(1).Top = FraDep(0).Top + FraDep(0).Height + 100
    FraDep(2).Top = FraDep(1).Top + FraDep(1).Height + 100


    FraAsiento(0).Top = Lp_TopBase - VScroll1.Value
    FraAsiento(1).Top = FraAsiento(0).Top + FraAsiento(0).Height + 100
    FraAsiento(2).Top = FraAsiento(1).Top + FraAsiento(1).Height + 100
    FraAsiento(3).Top = FraAsiento(2).Top + FraAsiento(2).Height + 100



End Sub

Private Sub InsertaProductoAsientoApertura(Codigo As String, PrecioFinal As Long, nro_Doc As Long, Id_Doc As Integer, CliPro As String, IdUnico As Long, Inventario As String)
    
        If Inventario = "SI" Then
            Sql = "INSERT INTO ven_detalle (codigo,marca,descripcion,precio_real," & _
                               "descuento,unidades,precio_final,subtotal,precio_costo,btn_especial," & _
                               "comision,fecha,no_documento,doc_id,rut_emp,pla_id,are_id,cen_id,aim_id," & _
                               "ved_precio_venta_bruto,ved_precio_venta_neto) VALUES"
              sql2 = "('" & _
                               Codigo & "','ASI. APERTURA','" & LvDep.SelectedItem.SubItems(3) & "'," & _
                              1 & "," & 0 & "," & _
                               1 & "," & PrecioFinal & "," & _
                               PrecioFinal & "," & 1 & ",'" & _
                               "NO','NO','" & Fql(UltimoDiaMes("01-" & CboMesContable.ItemData(CboMesContable.ListIndex) & "-" & ComAno.Text)) & "'," & _
                               nro_Doc & "," & Id_Doc & ",'" & SP_Rut_Activo & "'," & LvDep.SelectedItem.SubItems(14) & _
                               "," & 0 & "," & 0 & "," & _
                              0 & "," & 0 & "," & PrecioFinal & ")"
        Else
            'sin inventario
            Sql = "INSERT INTO ven_sin_detalle (id,vsd_neto,pla_id,cen_id,are_id) "
            sql2 = "VALUES(" & IdUnico & "," & PrecioFinal & ",99999,99999,99999," & LvDep.SelectedItem & ")"
        End If
    
        cn.Execute Sql & sql2
End Sub
Private Function CreaProductoAsientoApertura() As String

    Sql = "SELECT (SELECT mar_id FROM par_marcas WHERE rut_emp='" & SP_Rut_Activo & "' LIMIT 1) mar_id," & _
                    "(SELECT ume_id FROM sis_unidad_medida WHERE rut_emp='" & SP_Rut_Activo & "' LIMIT 1) ume_id," & _
                    "(SELECT tip_id FROM par_tipos_productos WHERE rut_emp='" & SP_Rut_Activo & "' LIMIT 1) tip_id"
    Consulta RsTmp, Sql
    Lp_NewId = UltimoNro("maestro_productos", "codigo")
    
    Sql = "INSERT INTO maestro_productos (" & _
          "codigo,marca,descripcion,precio_compra,porciento_utilidad,precio_venta,margen," & _
          "stock_actual,stock_critico,ubicacion_bodega,comentario,bod_id,mar_id,tip_id,pro_inventariable," & _
          "rut_emp,ume_id,pro_precio_sin_flete,pro_precio_flete,pro_activo) " & _
          "VALUES (" & Lp_NewId & ",'ASI. APERTURA','ASI. PARA APERTURA',1,1," & _
          "2,1,0,0,'SIN UBICACION','SOLO APERTURA',0" & _
           "," & RsTmp!mar_id & "," & RsTmp!tip_id & ",'NO'," & _
           "'" & SP_Rut_Activo & "'," & RsTmp!ume_id & ",1,1,'NO')"
    cn.Execute Sql

    CreaProductoAsientoApertura = Str(Lp_NewId)

End Function

