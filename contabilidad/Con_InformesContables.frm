VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Object = "{DA729E34-689F-49EA-A856-B57046630B73}#1.0#0"; "progressbar-xp.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form Con_InformesContables 
   Caption         =   "Informes Contables"
   ClientHeight    =   9630
   ClientLeft      =   540
   ClientTop       =   1080
   ClientWidth     =   13800
   LinkTopic       =   "Form1"
   ScaleHeight     =   9630
   ScaleWidth      =   13800
   Begin MSComDlg.CommonDialog Dialogo 
      Left            =   7200
      Top             =   9135
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.Frame FraProgreso 
      Caption         =   "Progreso exportación"
      Height          =   795
      Left            =   2070
      TabIndex        =   17
      Top             =   5535
      Visible         =   0   'False
      Width           =   9495
      Begin Proyecto2.XP_ProgressBar BarraProgreso 
         Height          =   330
         Left            =   150
         TabIndex        =   18
         Top             =   270
         Width           =   9165
         _ExtentX        =   16166
         _ExtentY        =   582
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BrushStyle      =   0
         Color           =   16777088
         Scrolling       =   1
         ShowText        =   -1  'True
      End
   End
   Begin VB.CommandButton CmdPrintLibro 
      Caption         =   "F4 - Imprimir"
      Height          =   525
      Left            =   1965
      TabIndex        =   28
      Top             =   8985
      Width           =   1800
   End
   Begin MSComctlLib.ListView LvInv 
      Height          =   6045
      Left            =   375
      TabIndex        =   19
      Top             =   2880
      Width           =   13215
      _ExtentX        =   23310
      _ExtentY        =   10663
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   0   'False
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   9
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Object.Tag             =   "T1000"
         Text            =   "Cuentas"
         Object.Width           =   2646
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Object.Tag             =   "F1000"
         Text            =   "Fecha"
         Object.Width           =   1764
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   2
         SubItemIndex    =   2
         Object.Tag             =   "T1000"
         Text            =   "Nro"
         Object.Width           =   882
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Object.Tag             =   "T1000"
         Text            =   "Detalle"
         Object.Width           =   3528
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   4
         Object.Tag             =   "N100"
         Text            =   "Valor"
         Object.Width           =   2117
      EndProperty
      BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   5
         Key             =   "debe"
         Object.Tag             =   "N100"
         Text            =   "Debitos"
         Object.Width           =   2293
      EndProperty
      BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   6
         Key             =   "haber"
         Object.Tag             =   "N100"
         Text            =   "Creditos"
         Object.Width           =   2293
      EndProperty
      BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   7
         Object.Tag             =   "N100"
         Text            =   "Deudor"
         Object.Width           =   2293
      EndProperty
      BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   8
         Object.Tag             =   "N100"
         Text            =   "Acreedor"
         Object.Width           =   2293
      EndProperty
   End
   Begin VB.CommandButton cmdExportar 
      Caption         =   "F3 - Excel"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   525
      Left            =   330
      TabIndex        =   16
      ToolTipText     =   "Exportar"
      Top             =   8985
      Width           =   1620
   End
   Begin VB.CommandButton CmdSalir 
      Cancel          =   -1  'True
      Caption         =   "Esc - Retornar"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   525
      Left            =   11895
      TabIndex        =   15
      Top             =   9030
      Width           =   1620
   End
   Begin VB.Timer Timer1 
      Left            =   60
      Top             =   735
   End
   Begin VB.Frame Frame4 
      Caption         =   "Filtro periodo y tipo de balance"
      Height          =   1680
      Left            =   390
      TabIndex        =   3
      Top             =   1140
      Width           =   13185
      Begin VB.Frame FrmDetalle 
         Height          =   855
         Left            =   6780
         TabIndex        =   22
         Top             =   780
         Width           =   5490
         Begin VB.CommandButton cmdCuenta 
            Caption         =   "Cuenta"
            Height          =   240
            Left            =   30
            TabIndex        =   27
            Top             =   510
            Width           =   960
         End
         Begin VB.TextBox TxtNroCuenta 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   1035
            TabIndex        =   26
            Tag             =   "N2"
            ToolTipText     =   "Ingrese el codigo de la cuenta"
            Top             =   480
            Width           =   930
         End
         Begin VB.ComboBox CboCuenta 
            Height          =   315
            ItemData        =   "Con_InformesContables.frx":0000
            Left            =   1965
            List            =   "Con_InformesContables.frx":0002
            Style           =   2  'Dropdown List
            TabIndex        =   25
            Top             =   480
            Width           =   3465
         End
         Begin VB.ComboBox CboDetalle 
            Height          =   315
            ItemData        =   "Con_InformesContables.frx":0004
            Left            =   1050
            List            =   "Con_InformesContables.frx":0006
            Style           =   2  'Dropdown List
            TabIndex        =   23
            Top             =   120
            Width           =   4380
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkDetalle 
            Height          =   210
            Left            =   105
            OleObjectBlob   =   "Con_InformesContables.frx":0008
            TabIndex        =   24
            Top             =   195
            Width           =   705
         End
      End
      Begin VB.ComboBox CboNorma 
         Height          =   315
         ItemData        =   "Con_InformesContables.frx":0074
         Left            =   4860
         List            =   "Con_InformesContables.frx":0076
         Style           =   2  'Dropdown List
         TabIndex        =   20
         Top             =   450
         Width           =   2415
      End
      Begin VB.CommandButton CmdBusca 
         Caption         =   "Buscar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   420
         Left            =   12135
         TabIndex        =   8
         ToolTipText     =   "Busca texto ingresado"
         Top             =   255
         Width           =   900
      End
      Begin VB.ComboBox CboTipo 
         Height          =   315
         ItemData        =   "Con_InformesContables.frx":0078
         Left            =   7305
         List            =   "Con_InformesContables.frx":007A
         Style           =   2  'Dropdown List
         TabIndex        =   7
         Top             =   450
         Width           =   4665
      End
      Begin VB.ComboBox comMes 
         Height          =   315
         ItemData        =   "Con_InformesContables.frx":007C
         Left            =   135
         List            =   "Con_InformesContables.frx":007E
         Style           =   2  'Dropdown List
         TabIndex        =   6
         Top             =   450
         Width           =   1575
      End
      Begin VB.ComboBox ComAno 
         Height          =   315
         Left            =   3630
         Style           =   2  'Dropdown List
         TabIndex        =   5
         Top             =   450
         Width           =   1215
      End
      Begin VB.ComboBox ComMes2 
         Height          =   315
         ItemData        =   "Con_InformesContables.frx":0080
         Left            =   2010
         List            =   "Con_InformesContables.frx":0082
         Style           =   2  'Dropdown List
         TabIndex        =   4
         Top             =   450
         Width           =   1575
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
         Height          =   255
         Left            =   120
         OleObjectBlob   =   "Con_InformesContables.frx":0084
         TabIndex        =   9
         Top             =   255
         Width           =   375
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel5 
         Height          =   255
         Left            =   3660
         OleObjectBlob   =   "Con_InformesContables.frx":00E8
         TabIndex        =   10
         Top             =   255
         Width           =   375
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   210
         Left            =   8760
         OleObjectBlob   =   "Con_InformesContables.frx":014C
         TabIndex        =   11
         Top             =   225
         Width           =   2475
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel8 
         Height          =   255
         Left            =   2010
         OleObjectBlob   =   "Con_InformesContables.frx":01C2
         TabIndex        =   12
         Top             =   270
         Width           =   375
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel10 
         Height          =   255
         Left            =   1830
         OleObjectBlob   =   "Con_InformesContables.frx":0226
         TabIndex        =   13
         Top             =   510
         Width           =   300
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   210
         Left            =   4875
         OleObjectBlob   =   "Con_InformesContables.frx":0288
         TabIndex        =   21
         Top             =   255
         Width           =   1365
      End
   End
   Begin VB.Frame Frame3 
      Caption         =   "Empresa Activa"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   390
      TabIndex        =   0
      Top             =   195
      Width           =   13200
      Begin VB.TextBox SkEmpresa 
         BackColor       =   &H00FFFFC0&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00C00000&
         Height          =   435
         Left            =   2280
         TabIndex        =   2
         Text            =   "Text1"
         Top             =   345
         Width           =   10770
      End
      Begin VB.TextBox SkRut 
         BackColor       =   &H00FFFFC0&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00C00000&
         Height          =   435
         Left            =   135
         TabIndex        =   1
         Text            =   "Text1"
         Top             =   345
         Width           =   2145
      End
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   -30
      OleObjectBlob   =   "Con_InformesContables.frx":02F0
      Top             =   165
   End
   Begin MSComctlLib.ListView LvDetalle 
      Height          =   6135
      Left            =   375
      TabIndex        =   14
      Top             =   2850
      Width           =   13215
      _ExtentX        =   23310
      _ExtentY        =   10821
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   0   'False
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   9
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Object.Tag             =   "T1000"
         Text            =   "vou_id"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Object.Tag             =   "T1000"
         Text            =   "Fecha"
         Object.Width           =   1764
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   2
         Object.Tag             =   "N109"
         Text            =   "Numero"
         Object.Width           =   2293
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   2
         SubItemIndex    =   3
         Object.Tag             =   "T1000"
         Text            =   "Tipo"
         Object.Width           =   882
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   4
         Object.Tag             =   "N109"
         Text            =   "Cod."
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   5
         Object.Tag             =   "T2500"
         Text            =   "Descripcion"
         Object.Width           =   4410
      EndProperty
      BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   6
         Key             =   "debe"
         Object.Tag             =   "N100"
         Text            =   "Debe"
         Object.Width           =   2293
      EndProperty
      BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   7
         Key             =   "haber"
         Object.Tag             =   "N100"
         Text            =   "Haber"
         Object.Width           =   2293
      EndProperty
      BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   8
         Object.Tag             =   "T1500"
         Text            =   "Glosa"
         Object.Width           =   2646
      EndProperty
   End
End
Attribute VB_Name = "Con_InformesContables"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim Sp_Tipo As String
'Dim PFolio As String * 6, Pfecha As String * 8, Ptd As String * 3, PcodSii As String * 2, PnroDoc As String * 11, _
Pnombre As String * 19, PRut As String * 12, PExenta As String * 12, PAfecto As String * 12, _
Piva As String * 11, PRet As String * 11, Potros As String * 11, PTotal As String * 12, PnombreL As String * 19

Dim LFecha As String * 10, LNumero As String * 4, LTipo As String * 4, LLcodigo As String * 6, _
LDescripcion As String * 30, Ldebe As String * 12, Lhaber As String * 12, LGlosa As String * 30

Dim Cx As Double
Dim Cy As Double
Dim Dp As Double



Private Sub CboCuenta_Click()
    If CboCuenta.ListIndex = -1 Then Exit Sub
    TxtNroCuenta = CboCuenta.ItemData(CboCuenta.ListIndex)
End Sub

Private Sub CboTipo_Click()
    If CboTipo.ListIndex = -1 Then Exit Sub
    FrmDetalle.Visible = False
    'SkDetalle.Visible = False
    'CboDetalle.Visible = False
    If CboTipo.ItemData(CboTipo.ListIndex) = 2 Then
      '  SkDetalle.Visible = True
       ' CboDetalle.Visible = True
            FrmDetalle.Visible = True
            CboDetalle.ListIndex = 0
        
    
    End If
End Sub

'
Private Sub CmdBusca_Click()
        Dim Sp_FiltroCuenta As String
        Dim n As Long, Lp_Totales(2) As Double, Lp_idCuenta As Long, Lp_TotalesAnt(2) As Double, Sp_NombreCta As String, Lp_Linea_Pasivo As Long
        Dim Lp_SumaDebe As Double, Lp_SumaHaber As Double, Lp_TotalDeudor As Double, Lp_TotalAcreedor As Double, Bp_totalizarTipo As Boolean
        Dim Lp_TotalActivo As Double
        Dim Lp_TotalPasivo As Double
        Dim Rp_Rst4 As Recordset
    '    DoEvents
    ''    Timer2.Enabled = True
     '   Me.Bar_Cargando.Visible = True
      '  Bar_Cargando.Value = 1
        
        
        Sp_Periodo = " AND (vou_mes_contable BETWEEN " & comMes.ItemData(comMes.ListIndex) & " AND " & ComMes2.ItemData(ComMes2.ListIndex) & ") AND vou_ano_contable = " & ComAno.Text & " "
        LvDetalle.ListItems.Clear
        LvDetalle.Visible = True
        LvInv.Visible = False
        If Me.FrmDetalle.Visible Then
            If CboDetalle.ListIndex = 2 Then
                'Sigue
            Else
                
            
            
                'Exit Sub
            End If
        
        End If
        
        If CboNorma.ListIndex = 0 Then Sp_Tipo = "(1,2) "
        If CboNorma.ListIndex = 1 Then Sp_Tipo = "(1,3) "
        Select Case CboTipo.ItemData(CboTipo.ListIndex)
        
            Case 1
            
                '***********************************************************************************
                ' Inicio libro diario
                '************************************************************************************
                    Dim Lp_AcumuladoMes(2) As Double
                    Lp_AcumuladoMes(1) = 0
                    Lp_AcumuladoMes(2) = 0
                    For i = comMes.ItemData(comMes.ListIndex) To ComMes2.ItemData(ComMes2.ListIndex)
                            Sp_Periodo = " AND vou_mes_contable =" & i & " AND vou_ano_contable = " & ComAno.Text & " "
                            Sql = "SELECT vou_id,CAST(CONCAT_WS('-',vou_ano_contable,vou_mes_contable,vou_dia) AS DATE) fecha,vou_numero numero," & _
                                        "CASE vou_tipo WHEN 1 THEN 'I' WHEN 2 THEN 'E' WHEN 3 THEN 'T' END tipo," & _
                                        "0,'',0,0,vou_glosa glosa " & _
                                    "FROM con_vouchers " & _
                                    "WHERE vou_tiponiif IN " & Sp_Tipo & " AND  rut_emp='" & SP_Rut_Activo & "' " & Sp_Periodo & _
                                    "ORDER BY vou_mes_contable"
                            Consulta RsTmp, Sql
                            If RsTmp.RecordCount > 0 Then
                                With RsTmp
                                    .MoveFirst
                                    Do While Not .EOF
                                        LvDetalle.ListItems.Add , , !vou_id
                                        n = LvDetalle.ListItems.Count
                                        LvDetalle.ListItems(n).SubItems(1) = "" & !Fecha
                                        LvDetalle.ListItems(n).SubItems(2) = "" & !Numero
                                        LvDetalle.ListItems(n).SubItems(3) = "" & !Tipo
                                        LvDetalle.ListItems(n).SubItems(8) = "" & !Glosa
                                        
                                        'Aca viene el detalle del voucher
                                        Sql = "SELECT vou_id,'','','',d.pla_id,pla_nombre,vod_debe debe,vod_haber haber,'' " & _
                                                "FROM con_vouchers_detalle d " & _
                                                "JOIN con_plan_de_cuentas c USING(pla_id) " & _
                                                "WHERE vou_id=" & !vou_id & " " & _
                                                "HAVING debe+haber>0 " & _
                                                "UNION SELECT vou_id,'','','','' pla_id,'* * * TOTAL VOUCHER' pla_nombre,SUM(vod_debe) debe,SUM(vod_haber) haber,'' " & _
                                                "FROM con_vouchers_detalle " & _
                                                "WHERE vou_id=" & !vou_id & " " & _
                                                "GROUP BY vou_id " & _
                                                "HAVING debe+haber>0"
                                        Consulta RsTmp2, Sql
                                        
                                        If RsTmp2.RecordCount > 0 Then
                                            RsTmp2.MoveFirst
                                            Do While Not RsTmp2.EOF
                                                LvDetalle.ListItems(n).SubItems(4) = RsTmp2!pla_id
                                                LvDetalle.ListItems(n).SubItems(5) = RsTmp2!pla_nombre
                                                LvDetalle.ListItems(n).SubItems(6) = NumFormat(RsTmp2!debe)
                                                LvDetalle.ListItems(n).SubItems(7) = NumFormat(RsTmp2!haber)
                                                If RsTmp2!pla_nombre <> "* * * TOTAL VOUCHER" Then
                                                    Lp_Totales(1) = Lp_Totales(1) + CDbl(LvDetalle.ListItems(n).SubItems(6))
                                                    Lp_Totales(2) = Lp_Totales(2) + CDbl(LvDetalle.ListItems(n).SubItems(7))
                                                End If
                                                
                                                If Not RsTmp2.EOF Then
                                                    LvDetalle.ListItems.Add , , !vou_id
                                                    n = LvDetalle.ListItems.Count
                                                End If
                                                RsTmp2.MoveNext
                                            Loop
                                           ' Lp_Totales(1) = Lp_Totales(1) + CDbl(LvDetalle.ListItems(N).SubItems(6))
                                           ' Lp_Totales(2) = Lp_Totales(2) + CDbl(LvDetalle.ListItems(N).SubItems(7))
                                        End If
                                    
                                        .MoveNext
                                    Loop
                                    
                                    
                                    
                                    
                                    LvDetalle.ListItems.Add , , ""
                                    LvDetalle.ListItems.Add , , ""
                                    n = LvDetalle.ListItems.Count
                                    LvDetalle.ListItems(n).SubItems(5) = "* * * TOTAL DEL MES"
                                    LvDetalle.ListItems(n).SubItems(6) = NumFormat(Lp_Totales(1))
                                    LvDetalle.ListItems(n).SubItems(7) = NumFormat(Lp_Totales(2))
                                    
                                    LvDetalle.ListItems.Add , , ""
                                    LvDetalle.ListItems.Add , , ""
                                    n = LvDetalle.ListItems.Count
                                    LvDetalle.ListItems(n).SubItems(5) = "* * * TOTAL MESES ANTERIORES"
                                    
                                    If comMes.ItemData(comMes.ListIndex) = 1 Then
                                        LvDetalle.ListItems(n).SubItems(6) = NumFormat(Lp_AcumuladoMes(1))
                                        LvDetalle.ListItems(n).SubItems(7) = NumFormat(Lp_AcumuladoMes(2))
                                    Else
                                        Sql = "SELECT IFNULL(SUM(vod_debe),0) debe,IFNULL(SUM(vod_haber),0) haber " & _
                                                "FROM con_vouchers v " & _
                                               "JOIN con_vouchers_detalle d ON v.vou_id=d.vou_id " & _
                                                "WHERE vou_tiponiif IN " & Sp_Tipo & " AND v.rut_emp='" & SP_Rut_Activo & "' AND vou_ano_contable=" & ComAno.Text & " AND vou_mes_contable BETWEEN 0 AND " & i - 1
                                        Consulta RsTmp, Sql
                                        If RsTmp.RecordCount > 0 Then
                                            Lp_AcumuladoMes(1) = RsTmp!debe
                                            Lp_AcumuladoMes(2) = RsTmp!haber
                                        
                                            LvDetalle.ListItems(n).SubItems(6) = NumFormat(RsTmp!debe)
                                            LvDetalle.ListItems(n).SubItems(7) = NumFormat(RsTmp!haber)
                                        End If
                                    End If
                                    
                                    LvDetalle.ListItems.Add , , ""
                                    LvDetalle.ListItems.Add , , ""
                                    n = LvDetalle.ListItems.Count
                                    LvDetalle.ListItems(n).SubItems(5) = "* * * TOTAL ACUMULADO"
                                    
                                    'If comMes.ItemData(comMes.ListIndex) = 1 Then
                                        
                                        Lp_AcumuladoMes(1) = Lp_AcumuladoMes(1) + Lp_Totales(1)
                                        Lp_AcumuladoMes(2) = Lp_AcumuladoMes(2) + Lp_Totales(2)
                                    '    LvDetalle.ListItems(n).SubItems(6) = NumFormat(Lp_Totales(1))
                                    '    LvDetalle.ListItems(n).SubItems(7) = NumFormat(Lp_Totales(2))
                                        LvDetalle.ListItems(n).SubItems(6) = NumFormat(Lp_AcumuladoMes(1))
                                        LvDetalle.ListItems(n).SubItems(7) = NumFormat(Lp_AcumuladoMes(2))
                                    'Else
                                        'Cambiar esto
                                    '    LvDetalle.ListItems(n).SubItems(6) = NumFormat(Lp_Totales(1) + RsTmp!debe)
                                    '    LvDetalle.ListItems(n).SubItems(7) = NumFormat(Lp_Totales(2) + RsTmp!haber)
                                    'End If
                                    
                                    
                                End With
                            End If
                            LvDetalle.ListItems.Add , , ""
                            LvDetalle.ListItems.Add , , ""
                            Lp_Totales(1) = 0
                            Lp_Totales(2) = 0
                    Next
                '***********************************************************************************
                ' Fin libro diario
                '************************************************************************************
            Case 2, 3
                Dim Sp_Caja As String
                Dim Sp_FiltroCuenta2 As String
                Dim Ip_HastaMesContable As Integer
                Sp_Caja = ""
                If CboTipo.ItemData(CboTipo.ListIndex) = 3 Then Sp_Caja = " d.pla_id=" & IG_Id_CuentaCaja Else Sp_Caja = "1=1"
                
                If Me.FrmDetalle.Visible Then
                    
                    If CboCuenta.Text = "TODAS LAS CUENTAS" Then
                        Sp_FiltroCuenta = ""
                        Sp_FiltroCuenta2 = ""
                    Else
                        Sp_FiltroCuenta = " AND d.pla_id=" & CboCuenta.ItemData(CboCuenta.ListIndex) & " "
                        Sp_FiltroCuenta2 = " AND y.pla_id=" & CboCuenta.ItemData(CboCuenta.ListIndex) & " "
                    End If
                End If
                
                '***********************************************************************************
                ' Inicio Libro mayor
                '************************************************************************************
                LvDetalle.ColumnHeaders(9).Width = 0
                LvDetalle.ColumnHeaders(2).Width = 2500
                LvDetalle.ColumnHeaders(6).Width = 2500
                LvDetalle.ColumnHeaders.Item(6).Text = "GLOSA"
                
                
                For i = comMes.ItemData(comMes.ListIndex) To ComMes2.ItemData(ComMes2.ListIndex)
                            Sp_Periodo = " AND vou_mes_contable =" & i & " AND vou_ano_contable = " & ComAno.Text & " "
                            
                            Ip_HastaMesContable = i
                            If Me.FrmDetalle.Visible Then
                                If CboDetalle.ListIndex = 0 Then
                                    Ip_HastaMesContable = ComMes2.ItemData(ComMes2.ListIndex)
                                End If
                            End If
                            Sql = "SELECT d.pla_id vou_id, pla_nombre Fecha," & _
                                        "'' numero,'' tipo," & _
                                        "'A LA FECHA' glosa, " & _
                                        "(SELECT SUM(vod_debe) " & _
                                        "FROM con_vouchers x " & _
                                        "JOIN con_vouchers_detalle y ON x.vou_id = y.vou_id " & _
                                        "JOIN con_plan_de_cuentas z ON y.pla_id = z.pla_id " & _
                                        "WHERE vou_tiponiif IN " & Sp_Tipo & "  " & Sp_FiltroCuenta2 & " AND y.pla_id=d.pla_id   AND rut_emp = '" & SP_Rut_Activo & "' AND vou_ano_contable = " & ComAno.Text & " AND vou_mes_contable BETWEEN 0 AND " & Ip_HastaMesContable & " " & _
                                        "GROUP BY    d.pla_id) debe," & _
                                        "(SELECT SUM(vod_haber) " & _
                                        "FROM con_vouchers x " & _
                                        "JOIN con_vouchers_detalle y ON x.vou_id = y.vou_id " & _
                                        "JOIN con_plan_de_cuentas z ON y.pla_id = z.pla_id " & _
                                        "WHERE vou_tiponiif IN " & Sp_Tipo & " " & Sp_FiltroCuenta2 & " AND y.pla_id=d.pla_id   AND rut_emp = '" & SP_Rut_Activo & "' AND vou_ano_contable = " & ComAno.Text & "  AND vou_mes_contable BETWEEN 0 AND " & Ip_HastaMesContable & " " & _
                                        "GROUP BY    d.pla_id) haber " & _
                                        "FROM con_vouchers c " & _
                                        "JOIN con_vouchers_detalle d ON c.vou_id=d.vou_id " & _
                                        "JOIN con_plan_de_cuentas p ON d.pla_id=p.pla_id " & _
                                        "WHERE " & Sp_Caja & "  " & Sp_FiltroCuenta & " AND vou_tiponiif IN " & Sp_Tipo & " AND  rut_emp='" & SP_Rut_Activo & "' AND vou_ano_contable=" & Me.ComAno.Text & " AND vou_mes_contable BETWEEN 1 AND " & Ip_HastaMesContable & " " & _
                                        "GROUP BY d.pla_id " & _
                                        "HAVING debe>0 or haber>0 " & _
                                        "ORDER BY  p.tpo_id,p.det_id,vou_mes_contable,p.pla_nombre "
                                        
                            If CboTipo.ItemData(CboTipo.ListIndex) = 3 Then
                                   Sql = "SELECT d.pla_id vou_id, pla_nombre Fecha," & _
                                            "'' numero,'' tipo," & _
                                            "'' glosa, " & _
                                            "'' debe," & _
                                            "'' haber " & _
                                            "FROM con_vouchers c " & _
                                            "JOIN con_vouchers_detalle d ON c.vou_id=d.vou_id " & _
                                            "JOIN con_plan_de_cuentas p ON d.pla_id=p.pla_id " & _
                                            "WHERE " & Sp_Caja & " " & Sp_FiltroCuenta & " AND vou_tiponiif IN " & Sp_Tipo & " AND  rut_emp='" & SP_Rut_Activo & "' AND vou_ano_contable=" & Me.ComAno.Text & " AND vou_mes_contable BETWEEN 1 AND " & i & " " & _
                                            "GROUP BY d.pla_id " & _
                                            "/*HAVING debe>0 or haber>0 */" & _
                                            "ORDER BY  p.tpo_id,p.det_id,vou_mes_contable"
                            
                            
                            End If
                                        
                            If Me.FrmDetalle.Visible Then
                                If CboDetalle.ListIndex = 1 Then
                                    'nada
                                Else
                                    'La query seria sin a la fecha
                                    '4 -abril 2013
                                        Sql = "SELECT d.pla_id vou_id, pla_nombre Fecha," & _
                                            "'' numero,'' tipo," & _
                                            "'' glosa, " & _
                                            "'' debe," & _
                                            "'' haber " & _
                                            "FROM con_vouchers c " & _
                                            "JOIN con_vouchers_detalle d ON c.vou_id=d.vou_id " & _
                                            "JOIN con_plan_de_cuentas p ON d.pla_id=p.pla_id " & _
                                            "WHERE " & Sp_Caja & " " & Sp_FiltroCuenta & " AND vou_tiponiif IN " & Sp_Tipo & " AND  rut_emp='" & SP_Rut_Activo & "' AND vou_ano_contable=" & Me.ComAno.Text & " AND vou_mes_contable BETWEEN 1 AND " & Ip_HastaMesContable & " " & _
                                            "GROUP BY d.pla_id " & _
                                            "/*HAVING debe>0 or haber>0 */" & _
                                            "ORDER BY  p.tpo_id,p.det_id,vou_mes_contable"
                                        
                                    
                                
                                
                                End If
                            End If
                            
                                        
                                        
                                        
                                        
                                        
                            Consulta RsTmp, Sql
                            If RsTmp.RecordCount > 0 Then
                                With RsTmp
                                    .MoveFirst
                                    Lp_idCuenta = 0
                                       Do While Not .EOF
                                        'If LvDetalle.ListItems.Count = 0 Then
                                               ' If Lp_idCuenta <> !vou_id Then
                                                    LvDetalle.ListItems.Add , , !vou_id
                                                    n = LvDetalle.ListItems.Count
                                                    LvDetalle.ListItems(n).SubItems(1) = !Fecha
                                                    LvDetalle.ListItems(n).SubItems(2) = !Numero
                                                    LvDetalle.ListItems(n).SubItems(3) = !Tipo
                                                    LvDetalle.ListItems(n).SubItems(5) = !Glosa
                                                    If Len(!Glosa) = 0 Then
                                                    
                                                        LvDetalle.ListItems(n).SubItems(6) = ""
                                                        LvDetalle.ListItems(n).SubItems(7) = ""
                                                    Else
                                                        LvDetalle.ListItems(n).SubItems(6) = NumFormat(!debe)
                                                        LvDetalle.ListItems(n).SubItems(7) = NumFormat(!haber)
                                                    End If
                                                    Lp_TotalesAnt(1) = 0 & !debe
                                                    Lp_TotalesAnt(2) = 0 & !haber
                                                    Lp_Totales(1) = 0
                                                    Lp_Totales(2) = 0
                                                'End If
                                                'Lp_idCuenta = !vou_id
                                       ' End If
                                        'Aca viene el detalle del voucher

                                                
                                        Sql = "SELECT d.pla_id," & _
                                                "CAST(CONCAT_WS('-',vou_ano_contable,vou_mes_contable,vou_dia) AS CHAR) fecha, " & _
                                                "CAST(vou_numero AS CHAR) vou_numero,CASE vou_tipo WHEN 1 THEN 'I' WHEN 2 THEN 'E' WHEN 3 THEN 'T' END tipo, " & _
                                                "c.vou_glosa pla_nombre, vod_debe, vod_haber " & _
                                                "FROM con_vouchers c " & _
                                                "JOIN con_vouchers_detalle d ON c.vou_id=d.vou_id " & _
                                                "JOIN con_plan_de_cuentas p ON d.pla_id=p.pla_id "
                                                If Me.FrmDetalle.Visible Then
                                                    If CboDetalle.ListIndex = 1 Then
                                                        Sql = Sql & "WHERE  vou_tiponiif IN " & Sp_Tipo & " AND rut_emp='" & SP_Rut_Activo & "' AND vou_ano_contable=" & Me.ComAno.Text & " AND vou_mes_contable=" & i & " AND d.pla_id= " & !vou_id & " "
                                                    Else
                                                        Sql = Sql & "WHERE  vou_tiponiif IN " & Sp_Tipo & " AND rut_emp='" & SP_Rut_Activo & "' AND vou_ano_contable=" & Me.ComAno.Text & " AND vou_mes_contable BETWEEN " & comMes.ItemData(comMes.ListIndex) & " AND " & ComMes2.ItemData(ComMes2.ListIndex) & " AND d.pla_id= " & !vou_id & " "
                                                    End If
                                                Else
                                                    
                                                    Sql = Sql & "WHERE " & Sp_Caja & " AND vou_tiponiif IN " & Sp_Tipo & " AND rut_emp='" & SP_Rut_Activo & "' AND vou_ano_contable=" & Me.ComAno.Text & " AND vou_mes_contable=" & i & " AND d.pla_id= " & !vou_id & " "
                                                End If
                                                Sql = Sql & "UNION SELECT d.pla_id,'' fecha,'' vou_numero,'' tipo,'* * * TOTAL MES' pla_nombre,SUM(vod_debe) vod_debe,SUM(vod_haber) vod_haber " & _
                                                "FROM con_vouchers c " & _
                                                "JOIN con_vouchers_detalle d ON c.vou_id = d.vou_id " & _
                                                "JOIN con_plan_de_cuentas p ON d.pla_id = p.pla_id "
                                                If Me.FrmDetalle.Visible Then
                                                    If CboDetalle.ListIndex = 1 Then
                                                        Sql = Sql & " WHERE " & Sp_Caja & " AND vou_tiponiif IN " & Sp_Tipo & " AND  rut_emp = '" & SP_Rut_Activo & "'  AND vou_ano_contable =" & ComAno.Text & " AND  vou_mes_contable =" & i & " AND d.pla_id =" & !vou_id & " "
                                                    Else
                                                        Sql = Sql & " WHERE " & Sp_Caja & " AND vou_tiponiif IN " & Sp_Tipo & " AND  rut_emp = '" & SP_Rut_Activo & "'  AND vou_ano_contable =" & ComAno.Text & " AND  vou_mes_contable BETWEEN " & comMes.ItemData(comMes.ListIndex) & " AND " & ComMes2.ItemData(ComMes2.ListIndex) & " AND d.pla_id =" & !vou_id & " "
                                                    End If
                                                Else
                                                    Sql = Sql & " WHERE " & Sp_Caja & " AND vou_tiponiif IN " & Sp_Tipo & " AND  rut_emp = '" & SP_Rut_Activo & "'  AND vou_ano_contable =" & ComAno.Text & " AND  vou_mes_contable =" & i & " AND d.pla_id =" & !vou_id & " "
                                                
                                                End If
                                                Sql = Sql & "GROUP BY d.pla_id"
                                                
                                                
                                        Consulta RsTmp2, Sql
                                        
                                        If RsTmp2.RecordCount > 0 Then
                                            RsTmp2.MoveFirst
                                            Lp_idCuenta = RsTmp2!pla_id
                                            Do While Not RsTmp2.EOF
                                                LvDetalle.ListItems.Add , , !vou_id
                                                n = LvDetalle.ListItems.Count
                                                LvDetalle.ListItems(n).SubItems(2) = RsTmp2!vou_numero
                                                LvDetalle.ListItems(n).SubItems(1) = RsTmp2!Fecha
                                                LvDetalle.ListItems(n).SubItems(3) = "" & RsTmp2!Tipo
                                                LvDetalle.ListItems(n).SubItems(4) = RsTmp2!pla_id
                                                LvDetalle.ListItems(n).SubItems(5) = RsTmp2!pla_nombre
                                                LvDetalle.ListItems(n).SubItems(6) = NumFormat(RsTmp2!vod_debe)
                                                LvDetalle.ListItems(n).SubItems(7) = NumFormat(RsTmp2!vod_haber)
                                                If RsTmp2!pla_nombre = "* * * TOTAL MES" Then
                                                    Lp_Totales(1) = RsTmp2!vod_debe
                                                    Lp_Totales(2) = RsTmp2!vod_haber
                                                End If
                                                RsTmp2.MoveNext
                                            Loop
                                       '     LvDetalle.ListItems.Add , , !vou_id
                                       '     n = LvDetalle.ListItems.Count
                                       '     LvDetalle.ListItems(n).SubItems(6) = NumFormat(Lp_Totales(1))
                                       '     LvDetalle.ListItems(n).SubItems(7) = NumFormat(Lp_Totales(2))
                                        Else
                                            'Cuando no encuentra registros en el mes
                                            'De una cuenta con movimientos en meses anteriors
                                            'Debe totalizar con esos mismos datos 9 Enero 2013
                                            LvDetalle.ListItems.Add , , !vou_id
                                            n = LvDetalle.ListItems.Count
                                            LvDetalle.ListItems(n).SubItems(5) = "* * * TOTAL MES"
                                            LvDetalle.ListItems(n).SubItems(6) = 0 'NumFormat(Lp_Totales(1))
                                            LvDetalle.ListItems(n).SubItems(7) = 0 'NumFormat(Lp_Totales(2))
                                        End If
                                        LvDetalle.ListItems.Add , , !vou_id
                                        n = LvDetalle.ListItems.Count
                                        LvDetalle.ListItems(n).SubItems(5) = "* * * TOTAL ACUMULADO"
                                        LvDetalle.ListItems(n).SubItems(6) = NumFormat(Lp_Totales(1) + Lp_TotalesAnt(1))
                                        LvDetalle.ListItems(n).SubItems(7) = NumFormat(Lp_Totales(2) + Lp_TotalesAnt(2))
                                        LvDetalle.ListItems.Add , , !vou_id
                                        n = LvDetalle.ListItems.Count
                                        LvDetalle.ListItems(n).SubItems(5) = "* * * SALDO"
                                        LvDetalle.ListItems(n).SubItems(6) = 0
                                        LvDetalle.ListItems(n).SubItems(7) = 0
                                        If CDbl(LvDetalle.ListItems(n - 1).SubItems(6)) > CDbl(LvDetalle.ListItems(n - 1).SubItems(7)) Then
                                            LvDetalle.ListItems(n).SubItems(7) = NumFormat(CDbl(LvDetalle.ListItems(n - 1).SubItems(6)) - CDbl(LvDetalle.ListItems(n - 1).SubItems(7)))
                                        ElseIf CDbl(LvDetalle.ListItems(n - 1).SubItems(7)) > CDbl(LvDetalle.ListItems(n - 1).SubItems(6)) Then
                                            LvDetalle.ListItems(n).SubItems(6) = NumFormat(CDbl(LvDetalle.ListItems(n - 1).SubItems(7)) - CDbl(LvDetalle.ListItems(n - 1).SubItems(6)))
                                        End If
                                        
                                        .MoveNext
                                        LvDetalle.ListItems.Add , , ""
                                        n = LvDetalle.ListItems.Count

                                    Loop
                                    
                                    
                                    
                                    
                     
                                    
                                    
                                End With
                            End If
                            LvDetalle.ListItems.Add , , ""
                            LvDetalle.ListItems.Add , , ""
                            Lp_Totales(1) = 0
                            Lp_Totales(2) = 0
                            If Me.FrmDetalle.Visible Then
                                If CboDetalle.ListIndex = 1 Then
                                   ' Sql = Sql & " WHERE vou_tiponiif IN " & Sp_Tipo & " AND  rut_emp = '" & SP_Rut_Activo & "'  AND vou_ano_contable =" & ComAno.Text & " AND  vou_mes_contable =" & i & " AND d.pla_id =" & !vou_id & " "
                                Else
                                    Exit For
                                   ' Sql = Sql & " WHERE vou_tiponiif IN " & Sp_Tipo & " AND  rut_emp = '" & SP_Rut_Activo & "'  AND vou_ano_contable =" & ComAno.Text & " AND  vou_mes_contable BETWEEN " & comMes.ItemData(comMes.ListIndex) & " AND " & ComMes2.ItemData(ComMes2.ListIndex) & " AND d.pla_id =" & !vou_id & " "
                                End If
                            End If
                            'If CboTipo.ItemData(CboTipo.ListIndex) = 3 Then Exit For
                            
                    Next
                    If CboTipo.ItemData(CboTipo.ListIndex) = 3 Then
                         If LvDetalle.ListItems.Count > 0 Then
                            Dim Lp_Acumulado As Double, Lp_Acumulado2 As Double
                            Lp_Acumulado = 0
                            Lp_Acumulado2 = 0
                            For i = 1 To LvDetalle.ListItems.Count
                                If LvDetalle.ListItems(i).SubItems(5) = "* * * TOTAL MES" Then
                                    Lp_Acumulado = Lp_Acumulado + CDbl(LvDetalle.ListItems(i).SubItems(6))
                                    Lp_Acumulado2 = Lp_Acumulado2 + CDbl(LvDetalle.ListItems(i).SubItems(7))
                                    LvDetalle.ListItems(i + 1).SubItems(6) = NumFormat(Lp_Acumulado)
                                    LvDetalle.ListItems(i + 1).SubItems(7) = NumFormat(Lp_Acumulado2)
                                    
                                    
                                    LvDetalle.ListItems(i + 2).SubItems(7) = 0
                                    LvDetalle.ListItems(i + 2).SubItems(6) = 0
                                    If CDbl(LvDetalle.ListItems(i + 1).SubItems(6)) > CDbl(LvDetalle.ListItems(i + 1).SubItems(7)) Then
                                            LvDetalle.ListItems(i + 2).SubItems(7) = NumFormat(CDbl(LvDetalle.ListItems(i + 1).SubItems(6)) - CDbl(LvDetalle.ListItems(i + 1).SubItems(7)))
                                        ElseIf CDbl(LvDetalle.ListItems(i + 1).SubItems(7)) > CDbl(LvDetalle.ListItems(i + 1).SubItems(6)) Then
                                            LvDetalle.ListItems(i + 2).SubItems(6) = NumFormat(CDbl(LvDetalle.ListItems(i + 1).SubItems(7)) - CDbl(LvDetalle.ListItems(i + 1).SubItems(6)))
                                        End If
                                    
                                    End If
                            
                            Next
                         End If
                    End If
                    
                
            Case 3
                    '***********************************************************************************
                    ' Inicio Caja
                    '***********************************************************************************
                    
                
                
            
                
                
                
            Case 4
                    '***********************************************************************************
                    ' Inicio Libro INVENTARIO Y BALANCE
                    '***********************************************************************************
                    LvDetalle.Visible = False
                    LvInv.Visible = True
                    LvInv.ListItems.Clear
                    Sp_Periodo = " (vou_mes_contable BETWEEN " & comMes.ItemData(comMes.ListIndex) & " AND " & ComMes2.ItemData(ComMes2.ListIndex) & ") AND vou_ano_contable=" & ComAno.Text & " "
                    

                    'ACTIVOS
                    Sql = "SELECT d.tpo_id,tpo_nombre,d.det_id,d.det_nombre," & _
                            "(SELECT SUM(vod_debe)+SUM(vod_haber) " & _
                            "FROM con_vouchers v " & _
                            "JOIN con_vouchers_detalle r ON v.vou_id=r.vou_id " & _
                            "JOIN con_plan_de_cuentas p ON r.pla_id=p.pla_id " & _
                            "WHERE " & Sp_Periodo & " AND rut_emp='" & SP_Rut_Activo & "' AND d.tpo_id=p.tpo_id AND d.det_id=p.det_id AND v.vou_tiponiif IN " & Sp_Tipo & "  " & _
                            "GROUP BY p.tpo_id,p.det_id) movio,t.tpo_id " & _
                            "FROM con_tipo_de_cuenta t " & _
                            "JOIN con_detalle_tipo_cuenta d ON t.tpo_id=d.tpo_id " & _
                            "WHERE t.tpo_id IN (1,2) " & _
                            "HAVING movio<>0;"
                    Consulta RsTmp, Sql
                    If RsTmp.RecordCount > 0 Then
                        RsTmp.MoveFirst
                        Sp_NombreCta = ""
                        Bp_totalizarTipo = False
                        Do While Not RsTmp.EOF
                            If RsTmp!tpo_nombre <> Sp_NombreCta Then

                                If RsTmp!tpo_nombre <> Sp_NombreCta And Len(Sp_NombreCta) > 0 Then Bp_totalizarTipo = True
                                    
                                If Bp_totalizarTipo Then
                                       'SUMAR DEUDOR Y ACREEDOR
                                       For i = 1 To LvInv.ListItems.Count
                                           If Val(LvInv.ListItems(i).SubItems(7)) > 0 Then Lp_TotalDeudor = Lp_TotalDeudor + CDbl(LvInv.ListItems(i).SubItems(7))
                                           If Val(LvInv.ListItems(i).SubItems(8)) > 0 Then Lp_TotalAcreedor = Lp_TotalAcreedor + CDbl(LvInv.ListItems(i).SubItems(8))
                                           'Lp_TotalAcreedor = Lp_TotalAcreedor
                                       Next
                                       
                                       LvInv.ListItems.Add , , ""
                                       LvInv.ListItems.Add , , ""
                                       n = LvInv.ListItems.Count
                                       LvInv.ListItems(n).SubItems(3) = "TOTAL ACTIVOS---->"
                                       LvInv.ListItems(n).SubItems(7) = NumFormat(Lp_TotalDeudor)
                                       LvInv.ListItems(n).SubItems(8) = NumFormat(Lp_TotalAcreedor)
                                       LvInv.ListItems.Add , , ""
                                       n = LvInv.ListItems.Count
                                       LvInv.ListItems(n).SubItems(3) = "SALDO ACTIVOS---->"
                                       LvInv.ListItems(n).SubItems(7) = NumFormat(Lp_TotalDeudor - Lp_TotalAcreedor)
                                       Lp_TotalActivo = Lp_TotalDeudor - Lp_TotalAcreedor
                                'Else
                                       LvInv.ListItems.Add , , ""
                                       n = LvInv.ListItems.Count
                                       Lp_Linea_Pasivo = n
                                End If
                                Bp_totalizarTipo = False
                                LvInv.ListItems.Add , , RsTmp!tpo_nombre
                                n = LvInv.ListItems.Count
                                LvInv.ListItems(n).Bold = True
                                    
                            End If
                            Sp_NombreCta = RsTmp!tpo_nombre
                            LvInv.ListItems.Add , , RsTmp!det_nombre
                            n = LvInv.ListItems.Count
                            LvInv.ListItems(n).ForeColor = vbBlue
                            
                            Sql = "SELECT r.pla_id,pla_nombre,SUM(vod_debe) debe, SUM(vod_haber) haber,pla_analisis,tpo_id,det_id " & _
                                    "FROM con_vouchers v " & _
                                    "JOIN con_vouchers_detalle r ON v.vou_id = r.vou_id " & _
                                    "JOIN con_plan_de_cuentas p ON r.pla_id = p.pla_id " & _
                                    "WHERE " & Sp_Periodo & " AND rut_emp = '" & SP_Rut_Activo & "' AND p.tpo_id=" & RsTmp!tpo_id & " AND det_id=" & RsTmp!det_id & " AND v.vou_tiponiif IN " & Sp_Tipo & " " & _
                                    "GROUP BY r.pla_id "
                            Consulta RsTmp2, Sql
                            If RsTmp2.RecordCount > 0 Then
                                With RsTmp2
                                    .MoveFirst
                                    
                                    Do While Not .EOF
                                        Sql = ""
                                        LvInv.ListItems.Add , , !pla_nombre
                                        n = LvInv.ListItems.Count
                                        If !pla_analisis = "NO" Then
                                            'Esto es sin analisis comprobar si es deudor o acreedor
                                            '18 Enero 2013
                                            LvInv.ListItems(n).SubItems(3) = "TOTAL CUENTA---->"
                                            LvInv.ListItems(n).SubItems(7) = 0
                                            LvInv.ListItems(n).SubItems(8) = 0
                                            If RsTmp!tpo_id = 1 Then 'Activo
                                                If !debe > !haber Then
                                                    LvInv.ListItems(n).SubItems(7) = NumFormat(!debe - !haber)
                                                Else
                                                    LvInv.ListItems(n).SubItems(8) = NumFormat(!haber - !debe)
                                                End If
                                            ElseIf RsTmp!tpo_id = 2 Then ' Pasivo
                                                If !haber > !debe Then
                                                    LvInv.ListItems(n).SubItems(8) = NumFormat(!haber - !debe)
                                                Else
                                                    LvInv.ListItems(n).SubItems(7) = NumFormat(!debe - !haber)
                                                End If
                                            End If
                                        Else
                                            'Aqui sabemos que es de analisis "ANALISIS"
                                            '19 Enero 2013
                                            

                                            'Aca comprobaremos si se trata de Activos Fijos con Analisis
                                            If !tpo_id = 1 And !det_id = 4 And !pla_analisis = "SI" Then
                                                'Analisis de Activo Fijo
                                                       
                                                'Iremos a buscar la query a una funcio ya que es muy extensa
                                                Sql = QueryActivosNueva(!pla_id)
                                              
                                            
                                            
                                            ElseIf !tpo_id = 1 And !pla_id = IG_IdCtaClientes Then
                                                'Aqui identificamos que el activo corresponde a
                                                'clientes con analisis.
                                                                                               
                                                    Sql = "SELECT CONCAT('" & Mid(comMes.Text, 1, 3) & "','-','" & ComAno.Text & "') fecha,v.rut_cliente vou_numero,nombre_cliente vou_glosa,  " & _
                                                                "SUM(bruto)- IFNULL((SELECT  SUM(t.ctd_monto) " & _
                                                                            "FROM cta_abono_documentos t,cta_abonos a " & _
                                                                            "WHERE t.rut_emp = '" & SP_Rut_Activo & "' " & _
                                                                            "AND a.rut_emp = '" & SP_Rut_Activo & "' " & _
                                                                            "AND t.abo_id = a.abo_id AND a.abo_cli_pro = 'CLI' AND a.abo_rut = v.rut_cliente " & _
                                                                            "AND YEAR(a.abo_fecha_pago)= " & ComAno.Text & " " & _
                                                                            "AND (MONTH(a.abo_fecha_pago )BETWEEN " & comMes.ItemData(comMes.ListIndex) & " AND " & ComMes2.ItemData(ComMes2.ListIndex) & ")),0) valor " & _
                                                    "FROM ven_doc_venta v " & _
                                                    "INNER JOIN sis_documentos USING(doc_id), maestro_clientes c " & _
                                                    "WHERE (ven_informa_venta='NO' OR ctl_id>0) AND  v.rut_emp = '" & SP_Rut_Activo & "' " & _
                                                    "AND v.rut_cliente = c.rut_cliente " & _
                                                    "AND YEAR(v.fecha) = " & ComAno.Text & _
                                                    " AND(MONTH(v.fecha) BETWEEN " & comMes.ItemData(comMes.ListIndex) & " AND " & ComMes2.ItemData(ComMes2.ListIndex) & ") " & _
                                                    "GROUP BY v.rut_cliente " & _
                                                    "HAVING valor<>0 "
                                                    Sql = Sql & _
                                                        "UNION " & _
                                                        "SELECT '" & Mid(comMes.Text, 1, 3) & "-" & ComAno.Text & "' fecha,/* CONTROL SALDO CTA FAVOR */ " & _
                                                        "pzo_rut,maestro_clientes.nombre_rsocial, " & _
                                                        "SUM(pzo_cargo) - SUM(pzo_abono) valor  " & _
                                                        "FROM cta_pozo " & _
                                                        "JOIN maestro_clientes ON cta_pozo.pzo_rut=maestro_clientes.rut_cliente " & _
                                                        "WHERE pzo_cli_pro='CLI' AND cta_pozo.rut_emp='" & SP_Rut_Activo & "' " & _
                                                        "AND YEAR(cta_pozo.pzo_fecha)=" & ComAno.Text & " AND (MONTH(cta_pozo.pzo_fecha) BETWEEN " & comMes.ItemData(comMes.ListIndex) & " AND " & ComMes2.ItemData(ComMes2.ListIndex) & ") " & _
                                                        "GROUP BY cta_pozo.pzo_rut " & _
                                                        "HAVING Valor <> 0"
                                                    
                                                
                                                
                                                
                                            ElseIf !tpo_id = 2 And !pla_id = IG_IdCtaProveedores Then
                                                'Aqui identificamos a Proveedores con Analisis
                                                Sql = "SELECT CONCAT('" & Mid(comMes.Text, 1, 3) & "','-','" & ComAno.Text & "') fecha, " & _
                                                            "v.rut vou_numero,nombre_proveedor vou_glosa, " & _
                                                            "SUM(IF(doc_nota_de_credito = 'NO',total,0))-IFNULL(( " & _
                                                                "SELECT SUM(t.ctd_monto) " & _
                                                                "FROM cta_abono_documentos t,cta_abonos a " & _
                                                                "WHERE t.rut_emp = '" & SP_Rut_Activo & "' AND a.rut_emp = '" & SP_Rut_Activo & "' AND t.abo_id = a.abo_id AND a.abo_cli_pro = 'PRO'   AND a.abo_rut = v.rut " & _
                                                                "AND YEAR(a.abo_fecha_pago)=" & ComAno.Text & " AND (MONTH(a.abo_fecha_pago) BETWEEN " & comMes.ItemData(comMes.ListIndex) & " AND " & ComMes2.ItemData(ComMes2.ListIndex) & ") " & _
                                                            "),0 " & _
                                                            ")-SUM(IF(doc_nota_de_credito = 'SI',total,0)) valor " & _
                                                        "FROM com_doc_compra v " & _
                                                        "INNER JOIN sis_documentos USING(doc_id), maestro_proveedores c " & _
                                                        "WHERE doc_contable='SI' AND doc_orden_de_compra = 'NO' AND v.com_nc_utilizada='NO' AND v.rut_emp = '" & SP_Rut_Activo & "' AND v.doc_id_factura = 0 AND v.Rut = c.rut_proveedor " & _
                                                        "AND v.ano_contable=" & ComAno.Text & " AND (mes_contable BETWEEN " & comMes.ItemData(comMes.ListIndex) & " AND " & ComMes2.ItemData(ComMes2.ListIndex) & ") " & _
                                                        "GROUP BY v.rut " & _
                                                        "HAVING valor > 0 " & _
                                                        "/* ORDER BY v.rut */ "
                                                        Sql = Sql & _
                                                        "UNION " & _
                                                        "SELECT '" & Mid(comMes.Text, 1, 3) & "-" & ComAno.Text & "' fecha,/* CONTROL SALDO CTA FAVOR */ " & _
                                                        "pzo_rut,maestro_proveedores.nombre_empresa, " & _
                                                        "SUM(pzo_cargo) - SUM(pzo_abono) valor  " & _
                                                        "FROM cta_pozo " & _
                                                        "JOIN maestro_proveedores ON cta_pozo.pzo_rut=maestro_proveedores.rut_proveedor " & _
                                                        "WHERE pzo_cli_pro='PRO' AND cta_pozo.rut_emp='" & SP_Rut_Activo & "' " & _
                                                        "AND YEAR(cta_pozo.pzo_fecha)=" & ComAno.Text & " AND (MONTH(cta_pozo.pzo_fecha) BETWEEN " & comMes.ItemData(comMes.ListIndex) & " AND " & ComMes2.ItemData(ComMes2.ListIndex) & ") " & _
                                                        "GROUP BY cta_pozo.pzo_rut " & _
                                                        "HAVING Valor <> 0"
                                            
                                            
                                            Else
                                                If RsTmp!tpo_id = 1 Then 'Activos
                                                    Sql = "SELECT CAST(CONCAT_WS('-',vou_ano_contable,vou_mes_contable,vou_dia) AS DATE) Fecha," & _
                                                                "v.vou_numero,v.vou_glosa,(vod_debe+vod_haber)*IF(vod_debe>0,1,-1) valor " & _
                                                            "FROM    con_vouchers v " & _
                                                            "JOIN con_vouchers_detalle r ON v.vou_id = r.vou_id " & _
                                                            "JOIN con_plan_de_cuentas p ON r.pla_id = p.pla_id " & _
                                                            "WHERE " & Sp_Periodo & " AND rut_emp = '" & SP_Rut_Activo & "' AND r.pla_id=" & !pla_id & " AND v.vou_tiponiif IN " & Sp_Tipo
                                                ElseIf RsTmp!tpo_id = 2 Then 'Pasivos
                                                    Sql = "SELECT CAST(CONCAT_WS('-',vou_ano_contable,vou_mes_contable,vou_dia) AS DATE) Fecha," & _
                                                                "v.vou_numero,v.vou_glosa,(vod_debe+vod_haber)*IF(vod_debe>0,-1,1) valor " & _
                                                            "FROM    con_vouchers v " & _
                                                            "JOIN con_vouchers_detalle r ON v.vou_id = r.vou_id " & _
                                                            "JOIN con_plan_de_cuentas p ON r.pla_id = p.pla_id " & _
                                                            "WHERE " & Sp_Periodo & " AND rut_emp = '" & SP_Rut_Activo & "' AND r.pla_id=" & !pla_id & " AND v.vou_tiponiif IN " & Sp_Tipo
                                                End If
                                            End If
                                          '  RsTmp3.Close
                                            Consulta Rp_Rst4, Sql
                                            If Rp_Rst4.RecordCount > 0 Then
                                                Rp_Rst4.MoveFirst
                                                Lp_SumaDebe = 0
                                                Lp_SumaHaber = 0
                                                Do While Not Rp_Rst4.EOF
                                                    LvInv.ListItems.Add , , ""
                                                    n = LvInv.ListItems.Count
                                                    LvInv.ListItems(n).SubItems(1) = "" & Rp_Rst4!Fecha
                                                    LvInv.ListItems(n).SubItems(2) = "" & Rp_Rst4!vou_numero
                                                    LvInv.ListItems(n).SubItems(3) = "" & Rp_Rst4!vou_glosa
                                                    LvInv.ListItems(n).SubItems(4) = "" & NumFormat(Rp_Rst4!Valor)
                                                    If Rp_Rst4!Valor > 0 Then
                                                        Lp_SumaDebe = Lp_SumaDebe + Rp_Rst4!Valor
                                                    Else
                                                        Lp_SumaHaber = Lp_SumaHaber + Rp_Rst4!Valor
                                                    End If
                                                    Rp_Rst4.MoveNext
                                                Loop
                                                
                                                If RsTmp!tpo_id = 1 Then
                                                    LvInv.ListItems(n).SubItems(5) = NumFormat(Lp_SumaDebe)
                                                    LvInv.ListItems(n).SubItems(6) = NumFormat(Lp_SumaHaber)
                                                ElseIf RsTmp!tpo_id = 2 Then
                                                    LvInv.ListItems(n).SubItems(6) = NumFormat(Lp_SumaDebe)
                                                    LvInv.ListItems(n).SubItems(5) = NumFormat(Lp_SumaHaber)
                                                End If
                                                LvInv.ListItems.Add , , ""
                                                n = LvInv.ListItems.Count
                                                LvInv.ListItems(n).SubItems(3) = "TOTAL CUENTA---->"
                                                LvInv.ListItems(n).SubItems(7) = 0
                                                LvInv.ListItems(n).SubItems(8) = 0
                                                
                                                
                                                If RsTmp!tpo_id = 1 Then
                                                    If Lp_SumaDebe > Abs(Lp_SumaHaber) Then
                                                        LvInv.ListItems(n).SubItems(7) = NumFormat(Lp_SumaDebe - Abs(Lp_SumaHaber))
                                                    Else
                                                        LvInv.ListItems(n).SubItems(8) = NumFormat(Abs(Lp_SumaHaber) - Lp_SumaDebe)
                                                    End If
                                                ElseIf RsTmp!tpo_id = 2 Then
                                                    If Abs(Lp_SumaHaber) > Abs(Lp_SumaDebe) Then
                                                        LvInv.ListItems(n).SubItems(7) = NumFormat(Abs(Lp_SumaHaber) - Abs(Lp_SumaDebe))
                                                    Else
                                                        LvInv.ListItems(n).SubItems(8) = NumFormat(Abs(Lp_SumaDebe) - Abs(Lp_SumaHaber))
                                                    End If
                                                End If
                                            End If
                                        End If
                                        
                                    
                                    
                                        .MoveNext
                                    Loop
                    
                                End With
                            End If
                    
                            RsTmp.MoveNext
                            LvInv.ListItems.Add , , ""
                            

                            
                        Loop
                        
                        'resumen para separar Pasivos
                          'SUMAR DEUDOR Y ACREEDOR
'                                If RsTmp!tpo_id = 2 Then
                        If Lp_Linea_Pasivo > 0 Then
                                        Lp_TotalDeudor = 0
                                        Lp_TotalAcreedor = 0
                                       For i = Lp_Linea_Pasivo To LvInv.ListItems.Count
                                           If Val(LvInv.ListItems(i).SubItems(7)) > 0 Then Lp_TotalDeudor = Lp_TotalDeudor + CDbl(LvInv.ListItems(i).SubItems(7))
                                           If Val(LvInv.ListItems(i).SubItems(8)) > 0 Then Lp_TotalAcreedor = Lp_TotalAcreedor + CDbl(LvInv.ListItems(i).SubItems(8))
                                           'Lp_TotalAcreedor = Lp_TotalAcreedor
                                       Next
                                       
                                       LvInv.ListItems.Add , , ""
                                       LvInv.ListItems.Add , , ""
                                       n = LvInv.ListItems.Count
                                       LvInv.ListItems(n).SubItems(3) = "TOTAL PASIVOS---->"
                                       LvInv.ListItems(n).SubItems(7) = NumFormat(Lp_TotalDeudor)
                                       LvInv.ListItems(n).SubItems(8) = NumFormat(Lp_TotalAcreedor)
                                       LvInv.ListItems.Add , , ""
                                       n = LvInv.ListItems.Count
                                       LvInv.ListItems(n).SubItems(3) = "SALDO PASIVOS---->"
                                       LvInv.ListItems(n).SubItems(8) = NumFormat(Lp_TotalAcreedor - Lp_TotalDeudor)
                                       LvInv.ListItems.Add , , ""
                                       n = LvInv.ListItems.Count
                                       Lp_TotalPasivo = Lp_TotalAcreedor - Lp_TotalDeudor
                             '   End If
                                        LvInv.ListItems.Add , , ""
                                        LvInv.ListItems.Add , , ""
                                        n = LvInv.ListItems.Count
                                        If Lp_TotalActivo > Lp_TotalPasivo Then
                                            LvInv.ListItems(n).SubItems(3) = "UTILIDAD DEL BALANCE---->"
                                            LvInv.ListItems(n).SubItems(7) = NumFormat(Lp_TotalActivo - Lp_TotalPasivo)
                                        ElseIf Lp_TotalPasivo > Lp_TotalActivo Then
                                            LvInv.ListItems(n).SubItems(3) = "PERDIDA DEL BALANCE---->"
                                             LvInv.ListItems(n).SubItems(7) = NumFormat(Lp_TotalPasivo - Lp_TotalActivo)
                                        Else
                                              LvInv.ListItems(n).SubItems(3) = "UTILIDAD DEL BALANCE---->"
                                             LvInv.ListItems(n).SubItems(7) = 0
                                        End If
                            
                             End If
                             
                             
                    End If
                    
                    

                    '***********************************************************************************
                    ' FIN LIBRO INVENTARIO Y BALANCE
                    '***********************************************************************************
        End Select
     '    Timer2.Enabled = False
    '    Me.Bar_Cargando.Visible = False
End Sub

Private Sub cmdCuenta_Click()
    With BuscarSimple
        SG_codigo = Empty
        .Sm_Consulta = "SELECT pla_id, pla_nombre,tpo_nombre,det_nombre " & _
                       "FROM    con_plan_de_cuentas p,  con_tipo_de_cuenta t,   con_detalle_tipo_cuenta d " & _
                       "WHERE   p.tpo_id = t.tpo_id AND p.det_id = d.det_id "
        .Sm_CampoLike = "pla_nombre"
        .Im_Columnas = 2
        .Show 1
        If SG_codigo = Empty Then Exit Sub
        Busca_Id_Combo CboCuenta, Val(SG_codigo)
        TxtNroCuenta = SG_codigo
    End With
End Sub

Private Sub CmdExportar_Click()
    Dim tit(2) As String
    If LvDetalle.Visible = True Then
        If LvDetalle.ListItems.Count = 0 Then Exit Sub
    Else
        If LvInv.ListItems.Count = 0 Then Exit Sub
    End If
    FraProgreso.Visible = True
    tit(0) = Me.Caption & " " & DtFecha & " - " & Time
    tit(1) = Principal.SkEmpresa
    tit(2) = "RUT:" & SP_Rut_Activo
    If LvDetalle.Visible = True Then
        ExportarNuevo LvDetalle, tit, Me, BarraProgreso
    Else
        ExportarNuevo LvInv, tit, Me, BarraProgreso
    End If
    FraProgreso.Visible = False
End Sub

Private Sub CmdPrintLibro_Click()
    SG_codigo = Empty
    Sis_SeleccionaImpresion.Show 1
    If SG_codigo = Empty Then Exit Sub
   'ˇ If LvLibro.ListItems.Count = 0 Then
   '     LvLibro.ListItems.Add , , ""
   '     LvLibro.ListItems(1).SubItems(5) = "SIN MOVIMIENTOS"
   ' End If
  '  If SG_codigo = "matriz" Then PrintMatriz
    
    
    
    If SG_codigo = "laser" Then       'DABA ERROR AL COMPILAR..........
        If CboTipo.ItemData(CboTipo.ListIndex) = 1 Then
            PrintLaserLibroDiario
        End If
        
        
    End If
    Unload Me
    
    Exit Sub
ImpresoraError:
    'No imprime na
End Sub

Private Sub cmdSalir_Click()
    On Error Resume Next
    Unload Me
End Sub


Private Sub Form_Load()
    SkRut = SP_Rut_Activo
    SkEmpresa = SP_Empresa_Activa
    Skin2 Me, , 7
    Centrar Me
    LLenarCombo CboTipo, "tib_nombre", "tib_numero", "con_tipo_balance", "tib_activo='SI' AND tib_tipo='LIBROS'", "tib_id"
    LLenarCombo CboCuenta, "pla_nombre", "pla_id", "con_plan_de_cuentas", "pla_activo='SI'"
    CboCuenta.AddItem "TODAS LAS CUENTAS"
    CboCuenta.ListIndex = CboCuenta.ListCount - 1
    CboTipo.ListIndex = 0
        
    'CboDetalle.AddItem "Por cuentas"
    CboDetalle.AddItem "ACUMULADO"
    CboDetalle.AddItem "MES A MES"
        
     For i = 1 To 12
        comMes.AddItem UCase(MonthName(i, False))
        comMes.ItemData(comMes.ListCount - 1) = i
        ComMes2.AddItem UCase(MonthName(i, False))
        ComMes2.ItemData(comMes.ListCount - 1) = i
    Next
    Busca_Id_Combo comMes, Val(IG_Mes_Contable)
    Busca_Id_Combo ComMes2, Val(IG_Mes_Contable)
    LLenaYears ComAno, 2010
   ' For i = Year(Date) To 2010 Step -1
   'For i = Year(Date) To Year(Date) - 1 Step -1
   '     ComAno.AddItem i
   '     ComAno.ItemData(ComAno.ListCount - 1) = i
       
   ' Next
   ' Busca_Id_Combo ComAno, Val(IG_Ano_Contable)
    CboNorma.AddItem "TRIBUTARIO"
    CboNorma.AddItem "FINANCIERO"
    CboNorma.ListIndex = 0
    
End Sub


Private Sub Timer1_Timer()
    On Error Resume Next
    comMes.SetFocus
End Sub
Private Function QueryActivos(Sp_IdC As String) As String

Dim Sp_Filtro1 As String, Sp_Filtro2 As String, Sp_Filtro3 As String
Dim Sp_Periodo1 As String, Sp_Periodo2 As String, Sp_Periodo3 As String
'''^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
'   QUERY PARA ACTIVOS INMOVILIZADOS
'***********************************************************************
Sp_Filtro1 = " AND c.rut_emp='" & SP_Rut_Activo & "' AND v.rut_emp='" & SP_Rut_Activo & "'AND a.rut_emp='" & SP_Rut_Activo & "' "
Sp_Filtro2 = "AND v.rut_emp='" & SP_Rut_Activo & "'AND a.rut_emp='" & SP_Rut_Activo & "' "
Sp_Periodo1 = "AND vou_ano_contable=" & ComAno.Text & " AND (vou_mes_contable BETWEEN " & comMes.ItemData(comMes.ListIndex) & " AND " & ComMes2.ItemData(ComMes2.ListIndex) & ") "
Sp_Periodo2 = "AND ano_contable=" & ComAno.Text & " AND (mes_contable BETWEEN " & comMes.ItemData(comMes.ListIndex) & " AND " & ComMes2.ItemData(ComMes2.ListIndex) & ") "
Sp_Periodo3 = "AND YEAR(c.fecha)=" & ComAno.Text & "  AND (MONTH(c.fecha) BETWEEN " & comMes.ItemData(comMes.ListIndex) & " AND " & ComMes2.ItemData(ComMes2.ListIndex) & ") "
sql2 = ""
sql2 = "SELECT CAST(CONCAT_WS('-',vou_ano_contable,vou_mes_contable,vou_dia) AS DATE) Fecha," & _
            "vou_numero,a.aim_nombre vou_glosa,d.cmd_unitario_neto valor " & _
            "FROM con_vouchers v " & _
            "JOIN con_vouchers_detalle vd ON v.vou_id=vd.vou_id " & _
            "JOIN com_doc_compra c " & _
            "JOIN com_doc_compra_detalle d ON c.id=d.id AND d.vod_id=vd.vod_id " & _
            "JOIN con_activo_inmovilizado a ON d.pro_codigo=a.aim_id " & _
            "WHERE vou_tiponiif IN " & Sp_Tipo & " AND vou_id_asiento IN(1,11) AND d.pla_id=vd.pla_id AND d.pla_id=" & Sp_IdC & " " & _
            Sp_Periodo1 & _
            Sp_Periodo2 & _
            Sp_Filtro1 & " " & _
            "UNION /*VENTA SIN INVENTARIO */ "
sql2 = sql2 & "SELECT CAST(CONCAT_WS('-',vou_ano_contable,vou_mes_contable,vou_dia) AS DATE) Fecha,vou_numero,a.aim_nombre,d.vsd_neto*-1 " & _
            "FROM con_vouchers v " & _
            "JOIN con_vouchers_detalle vd ON v.vou_id=vd.vou_id " & _
            "JOIN ven_doc_venta c " & _
            "JOIN ven_sin_detalle d ON c.id=d.id " & _
            "JOIN con_activo_inmovilizado a ON d.aim_id=a.aim_id " & _
            "WHERE vou_tiponiif IN " & Sp_Tipo & " AND  vou_id_asiento IN(2,10) AND d.pla_id=vd.pla_id AND    d.pla_id=" & Sp_IdC & " " & _
            Sp_Periodo1 & _
            Sp_Periodo3 & _
            Sp_Filtro1 & " " & _
            "UNION /*VENTA CON INVENTARIO */ "
sql2 = sql2 & "SELECT CAST(CONCAT_WS('-',vou_ano_contable,vou_mes_contable,vou_dia) AS DATE) Fecha,vou_numero,a.aim_nombre,d.ved_precio_venta_neto*-1 " & _
            "FROM con_vouchers v " & _
            "JOIN con_vouchers_detalle vd ON v.vou_id=vd.vou_id " & _
            "JOIN ven_doc_venta c " & _
            "JOIN ven_detalle d ON c.no_documento=d.no_documento AND c.doc_id=d.doc_id AND c.rut_emp=d.rut_emp " & _
            "JOIN con_activo_inmovilizado a ON d.aim_id=a.aim_id " & _
            "WHERE vou_tiponiif IN " & Sp_Tipo & " AND   vou_id_asiento IN(2,10) AND d.pla_id=vd.pla_id AND    d.pla_id=" & Sp_IdC & " " & _
            Sp_Periodo1 & _
            Sp_Periodo3 & _
             Sp_Filtro1 & " " & _
            "UNION /* CON VOUCHERS */"
sql2 = sql2 & "SELECT CAST(CONCAT_WS('-',vou_ano_contable,vou_mes_contable,vou_dia) AS DATE) Fecha, " & _
            "vou_numero,a.aim_nombre," & _
                "IF(IFNULL((SELECT dep_valor_actualizado FROM con_depreciaciones n " & _
                    "WHERE n.aim_id=r.aim_id AND rut_emp='" & SP_Rut_Activo & "' AND dep_ano=" & ComAno.Text & "),0)=0, " & _
            "IF(r.aim_valor > 0, r.aim_valor,r.aim_valor *- 1), " & _
      "IFNULL((SELECT dep_valor_actualizado FROM con_depreciaciones n " & _
                    "WHERE n.aim_id=r.aim_id AND rut_emp='" & SP_Rut_Activo & "' AND dep_ano=" & ComAno.Text & "),0)) " & _
            "FROM con_vouchers v " & _
            "JOIN con_vouchers_detalle vd ON v.vou_id=vd.vou_id " & _
            "JOIN con_relacion_activos r ON v.vou_id=r.vou_id AND vd.vod_id=r.vod_id AND r.pla_id=vd.pla_id " & _
            "JOIN con_activo_inmovilizado a ON r.aim_id = a.aim_id " & _
            "WHERE vou_tiponiif IN " & Sp_Tipo & " AND vd.pla_id =" & Sp_IdC & " " & _
            Sp_Periodo1 & _
            Sp_Filtro2
 QueryActivos = sql2
End Function

Private Function QueryActivosNueva(Sp_IdC As String) As String

Dim Sp_Filtro1 As String, Sp_Filtro2 As String, Sp_Filtro3 As String
Dim Sp_Periodo1 As String, Sp_Periodo2 As String, Sp_Periodo3 As String
'''^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
'   QUERY PARA ACTIVOS INMOVILIZADOS
'***********************************************************************
Sp_Filtro1 = " AND c.rut_emp='" & SP_Rut_Activo & "' AND v.rut_emp='" & SP_Rut_Activo & "'AND a.rut_emp='" & SP_Rut_Activo & "' "
Sp_Filtro2 = "AND v.rut_emp='" & SP_Rut_Activo & "'AND a.rut_emp='" & SP_Rut_Activo & "' "
Sp_Periodo1 = "AND vou_ano_contable=" & ComAno.Text & " AND (vou_mes_contable BETWEEN " & comMes.ItemData(comMes.ListIndex) & " AND " & ComMes2.ItemData(ComMes2.ListIndex) & ") "
Sp_Periodo2 = "AND ano_contable=" & ComAno.Text & " AND (mes_contable BETWEEN " & comMes.ItemData(comMes.ListIndex) & " AND " & ComMes2.ItemData(ComMes2.ListIndex) & ") "
Sp_Periodo3 = "AND YEAR(c.fecha)=" & ComAno.Text & "  AND (MONTH(c.fecha) BETWEEN " & comMes.ItemData(comMes.ListIndex) & " AND " & ComMes2.ItemData(ComMes2.ListIndex) & ") "
sql2 = ""
sql2 = "SELECT CAST(CONCAT_WS('-',vou_ano_contable,vou_mes_contable,vou_dia) AS DATE) Fecha," & _
            "vou_numero,a.aim_nombre vou_glosa,d.cmd_unitario_neto valor " & _
            "FROM con_vouchers v " & _
            "JOIN con_vouchers_detalle vd ON v.vou_id=vd.vou_id " & _
            "JOIN com_doc_compra c " & _
            "JOIN com_doc_compra_detalle d ON c.id=d.id AND d.vod_id=vd.vod_id " & _
            "JOIN con_activo_inmovilizado a ON d.pro_codigo=a.aim_id " & _
            "WHERE vou_tiponiif IN " & Sp_Tipo & " AND vou_id_asiento IN(1,11) AND d.pla_id=vd.pla_id AND d.pla_id=" & Sp_IdC & " " & _
            Sp_Periodo1 & _
            Sp_Periodo2 & _
            Sp_Filtro1 & " " & _
            "UNION /*VENTA SIN INVENTARIO */ "
sql2 = sql2 & "SELECT CAST(CONCAT_WS('-',vou_ano_contable,vou_mes_contable,vou_dia) AS DATE) Fecha,vou_numero,a.aim_nombre,d.vsd_neto*-1 " & _
            "FROM con_vouchers v " & _
            "JOIN con_vouchers_detalle vd ON v.vou_id=vd.vou_id " & _
            "JOIN ven_doc_venta c " & _
            "JOIN ven_sin_detalle d ON c.id=d.id " & _
            "JOIN con_activo_inmovilizado a ON d.aim_id=a.aim_id " & _
            "WHERE vou_tiponiif IN " & Sp_Tipo & " AND  vou_id_asiento IN(2,10) AND d.pla_id=vd.pla_id AND    d.pla_id=" & Sp_IdC & " " & _
            Sp_Periodo1 & _
            Sp_Periodo3 & _
            Sp_Filtro1 & " " & _
            "UNION /*VENTA CON INVENTARIO */ "
sql2 = sql2 & "SELECT CAST(CONCAT_WS('-',vou_ano_contable,vou_mes_contable,vou_dia) AS DATE) Fecha,vou_numero,a.aim_nombre,d.ved_precio_venta_neto*-1 " & _
            "FROM con_vouchers v " & _
            "JOIN con_vouchers_detalle vd ON v.vou_id=vd.vou_id " & _
            "JOIN ven_doc_venta c " & _
            "JOIN ven_detalle d ON c.no_documento=d.no_documento AND c.doc_id=d.doc_id AND c.rut_emp=d.rut_emp " & _
            "JOIN con_activo_inmovilizado a ON d.aim_id=a.aim_id " & _
            "WHERE vou_tiponiif IN " & Sp_Tipo & " AND   vou_id_asiento IN(2,10) AND d.pla_id=vd.pla_id AND    d.pla_id=" & Sp_IdC & " " & _
            Sp_Periodo1 & _
            Sp_Periodo3 & _
             Sp_Filtro1 & " " & _
            "UNION /* CON VOUCHERS */"
sql2 = sql2 & "SELECT CAST(CONCAT_WS('-',vou_ano_contable,vou_mes_contable,vou_dia) AS DATE) Fecha, " & _
            "vou_numero,a.aim_nombre,IF(vd.vod_haber>0,r.aim_valor*-1,r.aim_valor) aim_valor " & _
            "FROM con_vouchers v " & _
            "JOIN con_vouchers_detalle vd ON v.vou_id=vd.vou_id " & _
            "JOIN con_relacion_activos r ON v.vou_id=r.vou_id AND vd.vod_id=r.vod_id AND r.pla_id=vd.pla_id " & _
            "JOIN con_activo_inmovilizado a ON r.aim_id = a.aim_id " & _
            "WHERE vou_tiponiif IN " & Sp_Tipo & " AND vd.pla_id =" & Sp_IdC & " " & _
            Sp_Periodo1 & _
            Sp_Filtro2
 QueryActivosNueva = sql2
End Function


Private Sub TxtNroCuenta_GotFocus()
    En_Foco TxtNroCuenta
End Sub
Private Sub TxtNroCuenta_KeyPress(KeyAscii As Integer)
    KeyAscii = AceptaSoloNumeros(KeyAscii)
End Sub
Private Sub TxtNroCuenta_Validate(Cancel As Boolean)
    If Val(TxtNroCuenta) = 0 Then Exit Sub
    Busca_Id_Combo CboCuenta, Val(TxtNroCuenta)
    If CboCuenta.ListIndex = -1 Then
        MsgBox "Cuenta no existe..."
    End If
End Sub
Private Sub PrintLaserLibroDiario()
    Dim Sp_Texto As String
    Dim Sp_Lh As String
 '  Establecer_Impresora Sm_ImpresoraSeleccionada
    For Each pr In Printers
        If pr.DeviceName = Sm_ImpresoraSeleccionada Then
            Establecer_Impresora pr.DeviceName
            Set Printer = pr 'Cambiamos la impresora por defecto
            Exit For        ' a la tengamos configurada en los parametros
        End If               'para las FACTURAs
    Next
   ' Printer.Orientation = vbPRORLandscape
    Printer.Orientation = 1
    Printer.ScaleMode = vbCentimeters
    Printer.DrawMode = 1
    Dim CantCopias As Integer
    On Error GoTo CancelaImp
    Dialogo.CancelError = True
  '  Dialogo.ShowPrinter
    CantCopias = Dialogo.Copies
    
    
    

    For i = 1 To 147
        Sp_Lh = Sp_Lh & "="
    Next
    On Error GoTo ErrorP
    'Sm_File = FreeFile
    'Open App.Path & "\xxx.txt" For Output As Sm_File
                Lp_Linea = 0
                Lp_Pagina = 1
              
                Cx = 1
                Cy = 0
                
                LFecha = ""
                LNumero = ""
                LTipo = ""
                LLcodigo = ""
                LDescripcion = ""
                RSet Ldebe = ""
                RSet Lhaber = ""
                LGlosa = ""
                    
                
                EncabezadoLibroDiario
                Cy = Cy + Dp
                    If LvDetalle.ListItems.Count = 0 Then
                        Printer.Print " "
                        Printer.Print " "
                        Printer.Print " "
                        Printer.FontSize = 16
                        Printer.Print " "; " S I N     M O V I M I E N T O S"
                        GoTo FinalArchivo
                    End If
              
                Printer.FontName = "Arial Narrow"
                
                Printer.FontSize = 10
                
               
                        For i = 1 To LvDetalle.ListItems.Count
                          '  PFolio = LvLibro.ListItems(i)
                          
                          
                                LFecha = LvDetalle.ListItems(i).SubItems(1)
                                LNumero = LvDetalle.ListItems(i).SubItems(2)
                                LTipo = LvDetalle.ListItems(i).SubItems(3)
                                LLcodigo = LvDetalle.ListItems(i).SubItems(4)
                                LSet LDescripcion = LvDetalle.ListItems(i).SubItems(5)
                                RSet Ldebe = LvDetalle.ListItems(i).SubItems(6)
                                RSet Lhaber = LvDetalle.ListItems(i).SubItems(7)
                                LSet LGlosa = LvDetalle.ListItems(i).SubItems(8)
                          
                                
                                
                                
                                
                            'RSet Potros = LvLibro.ListItems(i).SubItems(11)
'                            If Sm_CompraVenta = "COMPRA" Then
'                                For Y = 11 To 25
'                                    RSet Sm_ColImp(Y) = LvLibro.ListItems(i).SubItems(Y)
'                                Next
'
'
'                                RSet PTotal = LvLibro.ListItems(i).SubItems(26)
'                            Else
'                                ' VENTA
'                                 RSet PTotal = LvLibro.ListItems(i).SubItems(14)
'
'
'                            End If
                            
                            If Pfecha = "No Docto" Then
                                'coloca cx,cy,
                               ' Print #Sm_File, Chr$(&H1B); "!"; Chr$(13);
                            End If
                            
                            If Trim(PnombreL) = "TOTALES" Then
                                Cy = Cy + Dp
                                'Printer.Print Sp_Lh
                                Coloca Cx, Cy, Sp_Lh, False, 8
                            End If
                            Cy = Cy + Dp
                            If Pfecha = "No Docto" Or Trim(PnombreL) = "TOTALES" Then
                              '  Printer.Print PFolio & " " & Pfecha & " " & Ptd & " " & PcodSii & " " & _
                                PnroDoc & " " & PnombreL & " " & PRut & " " & PExenta & " " & PAfecto & " " & Piva & _
                                " " & PRet & " " & Potros & " " & PTotal
                           '         Coloca Cx, Cy, PFolio, True, 8
                                    EnviaLineasLibroDiario True
                                    
                            
                             '   Coloca Cx, Cy, PFolio & " " & Pfecha & " " & Ptd & " " & PcodSii & " " & _
                                PnroDoc & " " & PnombreL & " " & PRut & " " & PExenta & " " & PAfecto & " " & Piva & _
                                " " & PRet & " " & Potros & " " & PTotal, True, 8
                            Else
                              '  Printer.Print PFolio & " " & Pfecha & " " & Ptd & " " & PcodSii & " " & _
                                 PnroDoc & " " & PnombreL & " " & PRut & " " & PExenta & " " & PAfecto & " " & Piva & _
                                    " " & PRet & " " & Potros & " " & PTotal
                                
                             '  For p = 1 To LvLibro.ColumnHeaders.Count - 1
                            '         Coloca Cx, Cy, PFolio, False, 8
                                  
                                    EnviaLineasLibroDiario False
                               '     Cy = Cy + Dp
                              ' Next
                                
                           '    Coloca Cx, Cy, PFolio & " " & Pfecha & " " & Ptd & " " & PcodSii & " " & _
                                 PnroDoc & " " & PnombreL & " " & PRut & " " & PExenta & " " & PAfecto & " " & Piva & _
                                    " " & PRet & " " & Potros & " " & PTotal, False, 8
                               
                               
                            End If
                            
                            If Trim(PnombreL) = "TOTALES" Then
                                Cy = Cy + Dp
                               ' Printer.Print Sp_Lh
                                Coloca Cx, Cy, Sp_Lh, False, 8
                            End If
                            If Pfecha <> "No Docto" And Trim(PnombreL) <> "TOTALES" And Len(Trim(PnombreL)) > 0 Then
                            
                                Texenta = Texenta + CDbl(PExenta)
                                TAfecto = TAfecto + CDbl(PAfecto)
                                Tiva = Tiva + CDbl(Piva)
                                Tret = Tret + CDbl(PRet)
                              '  Totros = Totros + CDbl(Potros)
                                Ttotal = Ttotal + CDbl(PTotal)
                            End If
                            
                            Lp_Linea = Lp_Linea + 1
                            
                            
                            If Lp_Linea = 400 Then
                             '   If Trim(Pnombre) <> "TOTALES" And Pfecha <> "No Docto" Then
                                    'totalizar pagina
                                    PFolio = ""
                                    Pfecha = ""
                                    Ptd = ""
                                    PcodSii = ""
                                    PnroDoc = ""
                                    PnombreL = "HOJA SIGUIENTE"
                                    PRut = "======>"
                                    RSet PExenta = NumFormat(Texenta)
                                    RSet PAfecto = NumFormat(TAfecto)
                                    RSet Piva = NumFormat(Tiva)
                                    RSet PRet = NumFormat(Tret)
                                    RSet Potros = NumFormat(Totros)
                                    RSet PTotal = NumFormat(Ttotal)
                                    'Print #Sm_File, Chr$(&H1B); "!"; Chr$(13);
                                    Cy = Cy + Dp
                                '    Printer.Print PFolio & " " & Pfecha & " " & Ptd & " " & PcodSii & " " & _
                                    PnroDoc & " " & PnombreL & " " & PRut & " " & PExenta & " " & PAfecto & " " & Piva & _
                                    " " & PRet & " " & Potros & " " & PTotal
                                    EnviaLineasLibroDiario False
                                    
                                    'Coloca Cx, Cy, PFolio & " " & Pfecha & " " & Ptd & " " & PcodSii & " " & _
                                    PnroDoc & " " & PnombreL & " " & PRut & " " & PExenta & " " & PAfecto & " " & Piva & _
                                    " " & PRet & " " & Potros & " " & PTotal, True, 8
                                    Lp_Linea = 0
                                    Lp_Pagina = Lp_Pagina + 1
                                    Printer.NewPage
                                   ' Printer.EndDoc
                                    EncabezadoLibroDiario
                            End If
                        Next
                   
FinalArchivo:
                Printer.NewPage
                Printer.EndDoc
                'AVANCE DE PAGINA FINAL"
    Exit Sub
ErrorP:
    MsgBox "Problema al imprimir..." & vbNewLine & Err.Number & vbNewLine & Err.Description & vbNewLine & Err.Source
    Exit Sub
CancelaImp:
    'Cancelo impresion
End Sub

Private Function EnviaLineasLibroDiario(Negrita As Boolean)
        Dim avance As Double
        avance = 1.5
        Printer.FontSize = 10
        Printer.FontName = "Arial Narrow"
        
         
        
     
        
        
        
        Coloca Cx, Cy, LFecha, Negrita, 8
        Coloca Cx + 1.5, Cy, LNumero, Negrita, 8
        Coloca (Cx + 3) - Printer.TextWidth(LTipo), Cy, LTipo, Negrita, 8
        Coloca Cx + 3.2, Cy, LLcodigo, Negrita, 8
        'Coloca (Cx + 8) - Printer.TextWidth(LDescripcion), Cy, LDescripcion, Negrita, 8
        Coloca (Cx + 4.7), Cy, LDescripcion, Negrita, 8
        Coloca (Cx + 11) - Printer.TextWidth(Ldebe), Cy, Ldebe, Negrita, 8
        Coloca (Cx + 12.5) - Printer.TextWidth(Lhaber), Cy, Lhaber, Negrita, 8
        'Coloca (Cx + 17) - Printer.TextWidth(LGlosa), Cy, LGlosa, Negrita, 8
        Coloca (Cx + 13.2), Cy, LGlosa, Negrita, 8
        'Coloca (Cx + 13.3) - Printer.TextWidth(PRet), Cy, PRet, Negrita, 8
        'Coloca (Cx + 14.8) - Printer.TextWidth(Potros), Cy, Potros, Negrita, 8
'        If Sm_CompraVenta = "COMPRA" Then
'            For Y = 11 To 25
'                If LvLibro.ColumnHeaders(Y + 1).Width > 0 Then
'                    Coloca (Cx + (13.3) + avance) - Printer.TextWidth(Sm_ColImp(Y)), Cy, Sm_ColImp(Y), Negrita, 8
'                    avance = avance + 1.5
'                End If
'                'RSet Sm_ColImp(Y) = LvLibro.ListItems(i).SubItems(Y)
'            Next
'        End If
        
        
        'Coloca (Cx + 13.3 + avance) - Printer.TextWidth(PTotal), Cy, PTotal, Negrita, 8
End Function


Private Sub EncabezadoLibroDiario()
    Dim Sp_Lh As String, Sp_Tit As String
    Dim Sp_EncabezadosExtras(26) As String
    
    For i = 1 To 147
        Sp_Lh = Sp_Lh & "-"
    Next
    Printer.FontName = "Arial Narrow"
    Cy = 4
    Dp = 0.35
    Cx = 1
    Cy = Dp * 10
    
'    If Sm_TipoLibro = "BORRADOR" Then
'        Printer.FontName = "Courier New"
'
'        Cy = 0
'
'        Sp_Tit = Left(LvDetalle.ListItems(1) & Space(90), 90) & " " & Left(" Pagina:" & Lp_Pagina & Space(18), 18)
'        Coloca Cx, Cy, Sp_Tit, False, 8
'
'        Sp_Tit = Left(LvDetalle.ListItems(2) & Space(90), 90) & " " & Left(" Fecha :" & Date & Space(18), 18)
'        Cy = Cy + Dp
'        Coloca Cx, Cy, Sp_Tit, False, 8
'        Cy = Cy + Dp
'        For i = 3 To LvDetalle.ListItems.Count
'
'            Coloca Cx, Cy, LvDetalle.ListItems(i), False, 8
'            Cy = Cy + Dp
'        Next
'
'    End If
    Cy = Cy + Dp
    Coloca Cx, Cy, "L I B R O    D I A R I O     Periodo:" & comMes.Text & " al " & ComMes2.Text & " del " & ComAno.Text, True, 12  ' Titulo
    Cy = Cy + Dp
     Printer.FontName = "Courier New"
    Coloca Cx, Cy, Sp_Lh, False, 8
    Cy = Cy + Dp
  '  Coloca Cx, Cy, "            COD                     COMPRAS CON FACTURAS     ", False, 8
    
   ' Dim LFecha As String * 10, LNumero As String * 4, LTipo As String * 4, Lcodigo As String * 6, _
LDescripcion As String * 30, Ldebe As String * 12, Lhaber As String * 12, LGlosa As String * 30

    
    LFecha = "FECHA"
    LNumero = "NUMERO"
    LTipo = "TIPO"
    LLcodigo = "CODIGO"
    LDescripcion = "DESCRIPCION"
    RSet Ldebe = "DEBE"
    RSet Lhaber = "HABER"
    LGlosa = "GLOSA"
    
    
'    If Sm_CompraVenta = "COMPRA" Then
'        For e = 11 To 22
'            If LvLibro.ColumnHeaders(e + 1).Width > 0 Then
'                RSet Sm_ColImp(e) = LvLibro.ColumnHeaders(e + 1).Text
'            End If
'        Next
'    End If
    
    
    Cy = Cy + Dp
    Printer.FontName = "Arial Narrow"
    
    
    EnviaLineasLibroDiario False
   
    'Coloca Cx, Cy, PFolio & " " & Pfecha & " " & Ptd & " " & PcodSii & " " & _
                    PnroDoc & " " & PnombreL & " " & PRut & " " & PExenta & " " & PAfecto & " " & Piva & _
                    " " & PRet & " " & Potros & " " & PTotal, False, 8
    Cy = Cy + Dp
    Printer.FontName = "Courier New"
    Coloca Cx, Cy, Sp_Lh, False, 8
    
    If Ttotal > 0 Then
        PFolio = ""
        Pfecha = ""
        Ptd = ""
        PcodSii = ""
        PnroDoc = ""
        Pnombre = "DE HOJA ANTERIOR"
        PRut = "======>"
        RSet PExenta = NumFormat(Texenta)
        RSet PAfecto = NumFormat(TAfecto)
        RSet Piva = NumFormat(Tiva)
        RSet PRet = NumFormat(Tret)
        RSet Potros = NumFormat(Totros)
        RSet PTotal = NumFormat(Ttotal)
        
        Cy = Cy + Dp
        
        EnviaLineasLibroDiario True
        
        'Coloca Cx, Cy, PFolio & " " & Pfecha & " " & Ptd & " " & PcodSii & " " & _
                    PnroDoc & " " & Pnombre & " " & PRut & " " & PExenta & " " & PAfecto & " " & Piva & _
                    " " & PRet & " " & Potros & " " & PTotal, True, 8
        
       ' Texenta = 0
       ' TAfecto = 0
       ' Tiva = 0
       ' Tret = 0
       ' Totros = 0
       ' Ttotal = 0
        Lp_Linea = Lp_Linea + 1
    End If
   
End Sub

