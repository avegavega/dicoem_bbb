VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{DA729E34-689F-49EA-A856-B57046630B73}#1.0#0"; "progressbar-xp.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form con_CpraVtaAI 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Compra Venta Activo Inmovilizado"
   ClientHeight    =   8490
   ClientLeft      =   450
   ClientTop       =   1335
   ClientWidth     =   13935
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8490
   ScaleWidth      =   13935
   Begin VB.CommandButton CmdSalir 
      Cancel          =   -1  'True
      Caption         =   "&Retornar"
      Height          =   465
      Left            =   12165
      TabIndex        =   16
      Top             =   7800
      Width           =   1620
   End
   Begin VB.Frame frmOts 
      Caption         =   "Detalle"
      Height          =   5745
      Left            =   195
      TabIndex        =   12
      Top             =   1950
      Width           =   13590
      Begin VB.Frame FraProgreso 
         Caption         =   "Progreso exportaci�n"
         Height          =   795
         Left            =   2115
         TabIndex        =   13
         Top             =   2685
         Visible         =   0   'False
         Width           =   9510
         Begin Proyecto2.XP_ProgressBar BarraProgreso 
            Height          =   330
            Left            =   150
            TabIndex        =   14
            Top             =   315
            Width           =   9165
            _ExtentX        =   16166
            _ExtentY        =   582
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BrushStyle      =   0
            Color           =   16777088
            Scrolling       =   1
            ShowText        =   -1  'True
         End
      End
      Begin MSComctlLib.ListView LvDetalle 
         Height          =   5325
         Left            =   195
         TabIndex        =   15
         Top             =   270
         Width           =   13260
         _ExtentX        =   23389
         _ExtentY        =   9393
         View            =   3
         LabelWrap       =   0   'False
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   6
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "T1000"
            Text            =   "Periodo Contable"
            Object.Width           =   2293
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "F1000"
            Text            =   "Fecha Doc."
            Object.Width           =   1940
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "T2000"
            Text            =   "Proveedor/Cliente"
            Object.Width           =   4762
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Object.Tag             =   "T2000"
            Text            =   "Cuenta"
            Object.Width           =   4762
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Object.Tag             =   "T2000"
            Text            =   "Detalle"
            Object.Width           =   6174
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   5
            Key             =   "neto"
            Object.Tag             =   "N100"
            Text            =   "Neto"
            Object.Width           =   2293
         EndProperty
      End
   End
   Begin VB.CommandButton cmdExportar 
      Caption         =   "F3 - Excel"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   525
      Left            =   180
      TabIndex        =   11
      ToolTipText     =   "Exportar"
      Top             =   7770
      Width           =   1620
   End
   Begin VB.Frame Frame4 
      Caption         =   "Filtro periodo y Cuenta Contable"
      Height          =   1155
      Left            =   180
      TabIndex        =   0
      Top             =   630
      Width           =   13620
      Begin VB.CommandButton CmdBusca 
         Caption         =   "Buscar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   435
         Left            =   11205
         TabIndex        =   5
         ToolTipText     =   "Busca texto ingresado"
         Top             =   360
         Width           =   1665
      End
      Begin VB.ComboBox CboCta 
         Height          =   315
         ItemData        =   "con_CpraVtaAI.frx":0000
         Left            =   6090
         List            =   "con_CpraVtaAI.frx":0002
         Style           =   2  'Dropdown List
         TabIndex        =   4
         ToolTipText     =   "Debe crear Areas si desea cambiar esta opci�n"
         Top             =   435
         Width           =   5055
      End
      Begin VB.ComboBox comMes 
         Height          =   315
         ItemData        =   "con_CpraVtaAI.frx":0004
         Left            =   135
         List            =   "con_CpraVtaAI.frx":0006
         Style           =   2  'Dropdown List
         TabIndex        =   3
         Top             =   450
         Width           =   1575
      End
      Begin VB.ComboBox ComAno 
         Height          =   315
         Left            =   3630
         Style           =   2  'Dropdown List
         TabIndex        =   2
         Top             =   450
         Width           =   1215
      End
      Begin VB.ComboBox ComMes2 
         Height          =   315
         ItemData        =   "con_CpraVtaAI.frx":0008
         Left            =   2010
         List            =   "con_CpraVtaAI.frx":000A
         Style           =   2  'Dropdown List
         TabIndex        =   1
         Top             =   450
         Width           =   1575
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
         Height          =   255
         Left            =   120
         OleObjectBlob   =   "con_CpraVtaAI.frx":000C
         TabIndex        =   6
         Top             =   255
         Width           =   375
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel5 
         Height          =   255
         Left            =   3660
         OleObjectBlob   =   "con_CpraVtaAI.frx":0070
         TabIndex        =   7
         Top             =   255
         Width           =   375
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   210
         Left            =   6090
         OleObjectBlob   =   "con_CpraVtaAI.frx":00D4
         TabIndex        =   8
         Top             =   240
         Width           =   2475
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel8 
         Height          =   255
         Left            =   2010
         OleObjectBlob   =   "con_CpraVtaAI.frx":013E
         TabIndex        =   9
         Top             =   270
         Width           =   375
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel10 
         Height          =   255
         Left            =   1830
         OleObjectBlob   =   "con_CpraVtaAI.frx":01A2
         TabIndex        =   10
         Top             =   510
         Width           =   300
      End
   End
   Begin VB.Timer Timer1 
      Interval        =   10
      Left            =   495
      Top             =   45
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   0
      OleObjectBlob   =   "con_CpraVtaAI.frx":0204
      Top             =   0
   End
End
Attribute VB_Name = "con_CpraVtaAI"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub CmdBusca_Click()
    Dim Sp_Periodo As String, Sp_Ctas As String
    Sp_Periodo = " AND (mes_contable BETWEEN " & comMes.ItemData(comMes.ListIndex) & " AND " & ComMes2.ItemData(ComMes2.ListIndex) & ") AND ano_contable = " & ComAno.Text & " "
    If CboCta.Text <> "TODAS" Then
        Sp_Ctas = " AND  d.pla_id=" & CboCta.ItemData(CboCta.ListIndex)
    Else
        Sp_Ctas = ""
    End If
    
    LvDetalle.ListItems.Clear
    
    Sql = "SELECT d.pla_id,pla_nombre,SUM(cmd_total_neto)+SUM(cmd_exento) totalcuenta " & _
            "FROM com_doc_compra_detalle d " & _
            "JOIN com_doc_compra c USING(id) " & _
            "JOIN con_plan_de_cuentas p ON d.pla_id=p.pla_id " & _
            "WHERE d.pla_id IN (" & _
            "SELECT pla_id " & _
            "FROM con_plan_de_cuentas p " & _
            "WHERE p.tpo_id=1 and p.det_id=4) " & Sp_Periodo & " AND c.rut_emp='" & SP_Rut_Activo & "' " & Sp_Ctas & " " & _
            "GROUP BY d.pla_id " & _
            "/*ORDER BY pla_nombre */"
    Sp_Periodo = " AND (MONTH(c.fecha) BETWEEN " & comMes.ItemData(comMes.ListIndex) & " AND " & ComMes2.ItemData(ComMes2.ListIndex) & ") AND YEAR(c.fecha)= " & ComAno.Text & " "
    Sql = Sql & "UNION SELECT d.pla_id,pla_nombre,SUM(ved_precio_venta_neto) totalcuenta " & _
            "FROM ven_detalle d " & _
            "JOIN ven_doc_venta c ON d.no_documento=c.no_documento AND d.doc_id=c.doc_id  " & _
            "JOIN con_plan_de_cuentas p ON d.pla_id=p.pla_id " & _
            "WHERE d.pla_id IN (" & _
            "SELECT pla_id " & _
            "FROM con_plan_de_cuentas p " & _
            "WHERE p.tpo_id=1 and p.det_id=4) " & Sp_Periodo & " AND c.rut_emp='" & SP_Rut_Activo & "' " & Sp_Ctas & " " & _
            "GROUP BY d.pla_id " & _
            "ORDER BY pla_nombre "
            
            
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        RsTmp.MoveFirst
        Do While Not RsTmp.EOF
            DetalleCta RsTmp!pla_id, Sp_Periodo
            With RsTmp2
                If .RecordCount > 0 Then
                    'Incorporamos del detalle de la cta
                    .MoveFirst
                    Do While Not .EOF
                        LvDetalle.ListItems.Add , , !Periodo
                        X = LvDetalle.ListItems.Count
                        LvDetalle.ListItems(X).SubItems(1) = !Fecha
                        LvDetalle.ListItems(X).SubItems(2) = !nombre_empresa
                        LvDetalle.ListItems(X).SubItems(3) = !pla_nombre
                        LvDetalle.ListItems(X).SubItems(4) = !cmd_detalle
                        LvDetalle.ListItems(X).SubItems(5) = NumFormat(!tneto)
                    
                    
                        .MoveNext
                    Loop
                    LvDetalle.ListItems.Add , , ""
                    X = LvDetalle.ListItems.Count
                    LvDetalle.ListItems(X).SubItems(2) = "TOTAL CUENTA"
                    LvDetalle.ListItems(X).SubItems(3) = RsTmp!pla_nombre
                    LvDetalle.ListItems(X).SubItems(5) = NumFormat(RsTmp!totalcuenta)
                    For i = 1 To LvDetalle.ColumnHeaders.Count - 1
                        LvDetalle.ListItems(X).ListSubItems(i).Bold = True
                    Next
                    
                    LvDetalle.ListItems.Add , , ""
                End If
        
                RsTmp.MoveNext
            End With
         Loop
    End If
    
    
    'LLenar_Grilla RsTmp, Me, LvDetalle, False, True, True, False
        
End Sub
Private Sub DetalleCta(IdCuenta As Long, Periodo As String)
    Sql = "SELECT CAST(CONCAT_WS('-',mes_contable,ano_contable) AS CHAR) periodo,fecha,m.nombre_empresa " & _
                                    ",pla_nombre,cmd_detalle,cmd_total_neto+cmd_exento tneto " & _
            "FROM com_doc_compra_detalle d " & _
            "JOIN com_doc_compra c USING(id) " & _
            "JOIN con_plan_de_cuentas p ON d.pla_id=p.pla_id " & _
            "JOIN maestro_proveedores m ON c.rut=m.rut_proveedor " & _
            "WHERE d.pla_id IN (" & _
            "SELECT pla_id " & _
            "FROM con_plan_de_cuentas p " & _
            "WHERE     p.tpo_id=1 and p.det_id=4) " & Periodo & " AND c.rut_emp='" & SP_Rut_Activo & "' AND  d.pla_id=" & IdCuenta & " " & _
            "/*ORDER BY pla_nombre */"
    Periodo = " AND (MONTH(c.fecha) BETWEEN " & comMes.ItemData(comMes.ListIndex) & " AND " & ComMes2.ItemData(ComMes2.ListIndex) & ") AND YEAR(c.fecha)= " & ComAno.Text & " "
            
    Sql = Sql & "UNION SELECT 'FECHA' periodo,c.fecha,m.nombre_rsocial " & _
                                    ",pla_nombre,descripcion,ved_precio_venta_neto*unidades " & _
            "FROM ven_detalle d " & _
            "JOIN ven_doc_venta c ON d.no_documento=c.no_documento AND d.doc_id=c.doc_id  " & _
            "JOIN con_plan_de_cuentas p ON d.pla_id=p.pla_id " & _
            "JOIN maestro_clientes m ON c.rut_cliente=m.rut_cliente " & _
            "WHERE d.pla_id IN (" & _
            "SELECT pla_id " & _
            "FROM con_plan_de_cuentas p " & _
            "WHERE     p.tpo_id=1 and p.det_id=4) " & Periodo & " AND c.rut_emp='" & SP_Rut_Activo & "' AND  d.pla_id=" & IdCuenta & " " & _
            "ORDER BY pla_nombre "
                    
            
    Consulta RsTmp2, Sql
   ' Set DetalleCta = RsTmp2
End Sub
Private Sub cmdExportar_Click()
    Dim tit(2) As String
    If LvDetalle.ListItems.Count = 0 Then Exit Sub
    FraProgreso.Visible = True
    tit(0) = Me.Caption & " " & DtFecha & " - " & Time
    tit(1) = Principal.SkEmpresa
    tit(2) = "RUT:" & SP_Rut_Activo
    ExportarNuevo LvDetalle, tit, Me, BarraProgreso
    FraProgreso.Visible = False
End Sub

Private Sub CmdSalir_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    Centrar Me
    Skin2 Me, , 7
    'Aplicar_skin Me
    LLenarCombo CboCta, "pla_nombre", "pla_id", "con_plan_de_cuentas", "tpo_id=1 AND det_id=4 AND pla_activo='SI'"
    CboCta.AddItem "TODAS"
    CboCta.ListIndex = CboCta.ListCount - 1
    
    For i = 1 To 12
        comMes.AddItem UCase(MonthName(i, False))
        comMes.ItemData(comMes.ListCount - 1) = i
        ComMes2.AddItem UCase(MonthName(i, False))
        ComMes2.ItemData(comMes.ListCount - 1) = i
    Next
    Busca_Id_Combo comMes, Val(IG_Mes_Contable)
    Busca_Id_Combo ComMes2, Val(IG_Mes_Contable)
    
    LLenaYears ComAno, 2010
    'For i = Year(Date) To Year(Date) - 1 Step -1
    '    ComAno.AddItem i
    '    ComAno.ItemData(ComAno.ListCount - 1) = i
       
    'Next
    'Busca_Id_Combo ComAno, Val(IG_Ano_Contable)
End Sub
