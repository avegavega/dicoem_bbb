VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{DA729E34-689F-49EA-A856-B57046630B73}#1.0#0"; "progressbar-xp.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form Con_Asiento_Contable_Caja 
   Caption         =   "Asiento Contable Caja"
   ClientHeight    =   6900
   ClientLeft      =   960
   ClientTop       =   2790
   ClientWidth     =   13995
   LinkTopic       =   "Form1"
   ScaleHeight     =   6900
   ScaleWidth      =   13995
   Begin VB.Frame FraProgreso 
      Height          =   735
      Left            =   1815
      TabIndex        =   17
      Top             =   3270
      Visible         =   0   'False
      Width           =   3855
      Begin Proyecto2.XP_ProgressBar BarraProgreso 
         Height          =   375
         Left            =   105
         TabIndex        =   18
         Top             =   225
         Width           =   3540
         _ExtentX        =   6244
         _ExtentY        =   661
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BrushStyle      =   0
         Color           =   16777088
         Scrolling       =   1
         ShowText        =   -1  'True
      End
   End
   Begin VB.Timer Timer1 
      Interval        =   10
      Left            =   11670
      Top             =   0
   End
   Begin VB.Frame Frame2 
      Caption         =   "ASIENTO  CONTABLE CAJA A CLIENTES"
      Height          =   4575
      Left            =   390
      TabIndex        =   10
      Top             =   1515
      Width           =   6660
      Begin VB.CommandButton cmdExportar 
         Caption         =   "Exportar Lista a Excel"
         Height          =   495
         Left            =   150
         TabIndex        =   15
         ToolTipText     =   "Exportar"
         Top             =   4005
         Width           =   2055
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkDiferencia 
         Height          =   210
         Left            =   6255
         OleObjectBlob   =   "Con_Asiento_Contable_Caja.frx":0000
         TabIndex        =   11
         Top             =   4230
         Width           =   270
      End
      Begin MSComctlLib.ListView LvCargos 
         Height          =   3705
         Left            =   165
         TabIndex        =   12
         Top             =   270
         Width           =   6405
         _ExtentX        =   11298
         _ExtentY        =   6535
         View            =   3
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   4
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "T500"
            Text            =   "Cod"
            Object.Width           =   882
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "T3000"
            Text            =   "CUENTAS"
            Object.Width           =   4762
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   2
            Key             =   "debe"
            Object.Tag             =   "N100"
            Text            =   "DEBE"
            Object.Width           =   2381
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   3
            Key             =   "haber"
            Object.Tag             =   "N100"
            Text            =   "HABER"
            Object.Width           =   2381
         EndProperty
      End
   End
   Begin VB.CommandButton cmdSalir 
      Cancel          =   -1  'True
      Caption         =   "&Retornar"
      Height          =   405
      Left            =   12390
      TabIndex        =   9
      Top             =   6420
      Width           =   1350
   End
   Begin VB.Frame Frame1 
      Caption         =   "ASIENTO  CONTABLE PROVEEDORES A CAJA"
      Height          =   4575
      Left            =   7125
      TabIndex        =   6
      Top             =   1515
      Width           =   6660
      Begin VB.CommandButton Command1 
         Caption         =   "Exportar Lista a Excel"
         Height          =   495
         Left            =   135
         TabIndex        =   16
         ToolTipText     =   "Exportar"
         Top             =   4005
         Width           =   2055
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   210
         Left            =   6255
         OleObjectBlob   =   "Con_Asiento_Contable_Caja.frx":005E
         TabIndex        =   7
         Top             =   4230
         Width           =   270
      End
      Begin MSComctlLib.ListView LvDeposito 
         Height          =   3705
         Left            =   135
         TabIndex        =   8
         Top             =   270
         Width           =   6405
         _ExtentX        =   11298
         _ExtentY        =   6535
         View            =   3
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   4
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "T500"
            Text            =   "Cod"
            Object.Width           =   882
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "T3000"
            Text            =   "CUENTAS"
            Object.Width           =   4762
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   2
            Key             =   "debe"
            Object.Tag             =   "N100"
            Text            =   "DEBE"
            Object.Width           =   2381
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   3
            Key             =   "haber"
            Object.Tag             =   "N100"
            Text            =   "HABER"
            Object.Width           =   2381
         EndProperty
      End
   End
   Begin VB.Frame Frame3 
      Caption         =   "Seleccione Cuenta y Periodo"
      Height          =   1125
      Left            =   360
      TabIndex        =   0
      Top             =   240
      Width           =   13380
      Begin VB.Frame FrmOrigen 
         Caption         =   "Origen de Datos"
         Height          =   675
         Left            =   7590
         TabIndex        =   13
         Top             =   315
         Width           =   5505
         Begin VB.TextBox TxtOrigen 
            BackColor       =   &H00FFFFC0&
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FF0000&
            Height          =   330
            Left            =   165
            TabIndex        =   14
            Text            =   "  Provenientes de Ingresos-Pagos de Tesorer�a"
            Top             =   225
            Width           =   5190
         End
      End
      Begin VB.CommandButton cmdConsultar 
         Caption         =   "&Consultar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   420
         Left            =   5400
         TabIndex        =   3
         Top             =   555
         Width           =   1365
      End
      Begin VB.ComboBox CboAnoContable 
         Height          =   315
         ItemData        =   "Con_Asiento_Contable_Caja.frx":00BC
         Left            =   2925
         List            =   "Con_Asiento_Contable_Caja.frx":00BE
         Style           =   2  'Dropdown List
         TabIndex        =   2
         Top             =   630
         Width           =   2370
      End
      Begin VB.ComboBox CboMesContable 
         Height          =   315
         ItemData        =   "Con_Asiento_Contable_Caja.frx":00C0
         Left            =   225
         List            =   "Con_Asiento_Contable_Caja.frx":00C2
         Style           =   2  'Dropdown List
         TabIndex        =   1
         Top             =   630
         Width           =   2715
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   195
         Left            =   210
         OleObjectBlob   =   "Con_Asiento_Contable_Caja.frx":00C4
         TabIndex        =   4
         Top             =   435
         Width           =   1320
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   195
         Left            =   2955
         OleObjectBlob   =   "Con_Asiento_Contable_Caja.frx":0128
         TabIndex        =   5
         Top             =   450
         Width           =   1320
      End
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   11655
      OleObjectBlob   =   "Con_Asiento_Contable_Caja.frx":018C
      Top             =   7410
   End
End
Attribute VB_Name = "Con_Asiento_Contable_Caja"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdConsultar_Click()
    Dim Sp_Fechas As String
    Sp_Fechas = " AND MONTH(a.abo_fecha_pago)= " & CboMesContable.ItemData(CboMesContable.ListIndex) & " AND YEAR(a.abo_fecha_pago)=" & CboAnoContable.Text & " "

    LLenar_Grilla AsientoContable(Sp_Fechas, 5), Me, LvCargos, False, True, True, False
    TotalAsiento LvCargos
    LLenar_Grilla AsientoContable(Sp_Fechas, 6), Me, LvDeposito, False, True, True, False
    TotalAsiento LvDeposito
    
End Sub

Private Sub CmdExportar_Click()
    Dim tit(2) As String
    If LvCargos.ListItems.Count = 0 Then Exit Sub
    FraProgreso.Visible = True
    tit(0) = Me.Caption & " " & DtFecha & " - " & Time
    tit(1) = ""
    tit(2) = ""
    ExportarNuevo LvCargos, tit, Me, BarraProgreso
    FraProgreso.Visible = False
End Sub

Private Sub cmdSalir_Click()
    Unload Me
End Sub



Private Sub Command1_Click()
    Dim tit(2) As String
    If LvDeposito.ListItems.Count = 0 Then Exit Sub
    FraProgreso.Visible = True
    tit(0) = Me.Caption & " " & DtFecha & " - " & Time
    tit(1) = ""
    tit(2) = ""
    ExportarNuevo LvDeposito, tit, Me, BarraProgreso
    FraProgreso.Visible = False
End Sub

Private Sub Form_Load()
    Aplicar_skin Me
    Centrar Me
    For i = 1 To 12
        CboMesContable.AddItem UCase(MonthName(i, False))
        CboMesContable.ItemData(CboMesContable.ListCount - 1) = i
    Next
    Busca_Id_Combo CboMesContable, Val(IG_Mes_Contable)
    LLenaYears Me.CboAnoContable, 2010
    'For i = Year(Date) To Year(Date) - 1 Step -1
    '    CboAnoContable.AddItem i
    '    CboAnoContable.ItemData(CboAnoContable.ListCount - 1) = i
    'Next
    'Busca_Id_Combo CboAnoContable, Val(IG_Ano_Contable)
 

End Sub

Private Sub Timer1_Timer()
    On Error Resume Next
    CboCuenta.SetFocus
    Timer1.Enabled = False
End Sub
