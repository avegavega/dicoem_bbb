VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Begin VB.Form con_factores_sii 
   Caption         =   "Tabla Factores SII"
   ClientHeight    =   6825
   ClientLeft      =   3240
   ClientTop       =   2085
   ClientWidth     =   7200
   LinkTopic       =   "Form1"
   ScaleHeight     =   6825
   ScaleWidth      =   7200
   Begin VB.CommandButton CmdSalir 
      Caption         =   "Retornar"
      Height          =   390
      Left            =   5655
      TabIndex        =   29
      Top             =   6330
      Width           =   1365
   End
   Begin VB.CommandButton CmdGuardar 
      Caption         =   "Actualizar"
      Height          =   390
      Left            =   5655
      TabIndex        =   28
      Top             =   5895
      Width           =   1365
   End
   Begin VB.TextBox TxtMes 
      Alignment       =   1  'Right Justify
      Height          =   285
      Index           =   13
      Left            =   1710
      TabIndex        =   27
      Top             =   5985
      Width           =   1140
   End
   Begin VB.TextBox TxtMes 
      Alignment       =   1  'Right Justify
      Height          =   285
      Index           =   12
      Left            =   3030
      TabIndex        =   24
      Top             =   5145
      Width           =   1110
   End
   Begin VB.TextBox TxtMes 
      Alignment       =   1  'Right Justify
      Height          =   285
      Index           =   11
      Left            =   3030
      TabIndex        =   23
      Top             =   4860
      Width           =   1110
   End
   Begin VB.TextBox TxtMes 
      Alignment       =   1  'Right Justify
      Height          =   285
      Index           =   10
      Left            =   3030
      TabIndex        =   22
      Top             =   4575
      Width           =   1110
   End
   Begin VB.TextBox TxtMes 
      Alignment       =   1  'Right Justify
      Height          =   285
      Index           =   9
      Left            =   3030
      TabIndex        =   21
      Top             =   4320
      Width           =   1110
   End
   Begin VB.TextBox TxtMes 
      Alignment       =   1  'Right Justify
      Height          =   285
      Index           =   8
      Left            =   3030
      TabIndex        =   20
      Top             =   4035
      Width           =   1110
   End
   Begin VB.TextBox TxtMes 
      Alignment       =   1  'Right Justify
      Height          =   285
      Index           =   7
      Left            =   3030
      TabIndex        =   19
      Top             =   3750
      Width           =   1110
   End
   Begin VB.TextBox TxtMes 
      Alignment       =   1  'Right Justify
      Height          =   285
      Index           =   6
      Left            =   3030
      TabIndex        =   18
      Top             =   3465
      Width           =   1110
   End
   Begin VB.TextBox TxtMes 
      Alignment       =   1  'Right Justify
      Height          =   285
      Index           =   5
      Left            =   3030
      TabIndex        =   17
      Top             =   3165
      Width           =   1110
   End
   Begin VB.TextBox TxtMes 
      Alignment       =   1  'Right Justify
      Height          =   285
      Index           =   4
      Left            =   3030
      TabIndex        =   16
      Top             =   2895
      Width           =   1110
   End
   Begin VB.TextBox TxtMes 
      Alignment       =   1  'Right Justify
      Height          =   285
      Index           =   3
      Left            =   3030
      TabIndex        =   15
      Top             =   2580
      Width           =   1110
   End
   Begin VB.TextBox TxtMes 
      Alignment       =   1  'Right Justify
      Height          =   285
      Index           =   2
      Left            =   3030
      TabIndex        =   14
      Top             =   2265
      Width           =   1110
   End
   Begin VB.TextBox TxtMes 
      Alignment       =   1  'Right Justify
      Height          =   285
      Index           =   14
      Left            =   3375
      TabIndex        =   13
      Top             =   5985
      Width           =   1110
   End
   Begin VB.TextBox TxtMes 
      Alignment       =   1  'Right Justify
      Height          =   285
      Index           =   1
      Left            =   3030
      TabIndex        =   12
      Top             =   1965
      Width           =   1110
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
      Height          =   225
      Index           =   0
      Left            =   1995
      OleObjectBlob   =   "con_factores_sii.frx":0000
      TabIndex        =   2
      Top             =   2010
      Width           =   1035
   End
   Begin VB.Frame Frame1 
      Caption         =   "A�o"
      Height          =   930
      Left            =   990
      TabIndex        =   0
      Top             =   570
      Width           =   4860
      Begin VB.ComboBox ComAno 
         Height          =   315
         ItemData        =   "con_factores_sii.frx":0068
         Left            =   1215
         List            =   "con_factores_sii.frx":006A
         Style           =   2  'Dropdown List
         TabIndex        =   1
         Top             =   360
         Width           =   2145
      End
   End
   Begin VB.Timer Timer3 
      Interval        =   500
      Left            =   45
      Top             =   870
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   60
      OleObjectBlob   =   "con_factores_sii.frx":006C
      Top             =   1650
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
      Height          =   225
      Index           =   1
      Left            =   1995
      OleObjectBlob   =   "con_factores_sii.frx":02A0
      TabIndex        =   3
      Top             =   2310
      Width           =   1035
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
      Height          =   225
      Index           =   2
      Left            =   1995
      OleObjectBlob   =   "con_factores_sii.frx":030C
      TabIndex        =   4
      Top             =   2640
      Width           =   1035
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
      Height          =   225
      Index           =   3
      Left            =   1995
      OleObjectBlob   =   "con_factores_sii.frx":0374
      TabIndex        =   5
      Top             =   4335
      Width           =   1035
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
      Height          =   225
      Index           =   4
      Left            =   1995
      OleObjectBlob   =   "con_factores_sii.frx":03E6
      TabIndex        =   6
      Top             =   3795
      Width           =   1035
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
      Height          =   225
      Index           =   5
      Left            =   1995
      OleObjectBlob   =   "con_factores_sii.frx":044E
      TabIndex        =   7
      Top             =   3210
      Width           =   1035
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
      Height          =   225
      Index           =   6
      Left            =   1995
      OleObjectBlob   =   "con_factores_sii.frx":04B4
      TabIndex        =   8
      Top             =   2940
      Width           =   1035
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
      Height          =   225
      Index           =   7
      Left            =   1995
      OleObjectBlob   =   "con_factores_sii.frx":051C
      TabIndex        =   9
      Top             =   4605
      Width           =   1035
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
      Height          =   225
      Index           =   8
      Left            =   1995
      OleObjectBlob   =   "con_factores_sii.frx":0588
      TabIndex        =   10
      Top             =   4065
      Width           =   1035
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
      Height          =   225
      Index           =   9
      Left            =   1995
      OleObjectBlob   =   "con_factores_sii.frx":05F2
      TabIndex        =   11
      Top             =   3510
      Width           =   1035
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
      Height          =   225
      Index           =   10
      Left            =   1995
      OleObjectBlob   =   "con_factores_sii.frx":065A
      TabIndex        =   25
      Top             =   4890
      Width           =   1035
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
      Height          =   225
      Index           =   11
      Left            =   1995
      OleObjectBlob   =   "con_factores_sii.frx":06CA
      TabIndex        =   26
      Top             =   5175
      Width           =   1035
   End
   Begin VB.Frame Frame2 
      Caption         =   "Factor de Capital          Factor Bi�n Nuevo"
      Height          =   735
      Left            =   1470
      TabIndex        =   30
      Top             =   5745
      Width           =   3480
   End
   Begin VB.Frame Frame3 
      Caption         =   "Factores de Correcci�n"
      Height          =   4080
      Left            =   1455
      TabIndex        =   31
      Top             =   1545
      Width           =   3480
   End
End
Attribute VB_Name = "con_factores_sii"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False


Private Sub CmdGuardar_Click()
    
    On Error GoTo errorGrabar
    cn.BeginTrans
    
        cn.Execute "DELETE FROM con_factores " & _
                    "WHERE fac_ano=" & ComAno.Text
        Sql = ""
        For i = 1 To 14
            Sql = Sql & "(" & ComAno.Text & "," & i & "," & CxP(Format(TxtMes(i), "#0,##0")) & "),"
        Next
        Sql = "INSERT INTO con_factores (fac_ano,fac_mes,fac_valor) VALUES " & Mid(Sql, 1, Len(Sql) - 1)
        cn.Execute Sql
    cn.CommitTrans
    Exit Sub
errorGrabar:
    cn.RollbackTrans
    MsgBox "Problema al grabar..." & vbNewLine & Err.Number & vbNewLine & Err.Description, vbInformation
        
End Sub

Private Sub CmdSalir_Click()
    Unload Me
End Sub

Private Sub ComAno_Click()
    If ComAno.ListIndex = -1 Then Exit Sub
    
    Sql = "SELECT fac_mes,fac_valor " & _
            "FROM con_factores " & _
            "WHERE fac_ano=" & ComAno.Text & " " & _
            "ORDER BY fac_mes"
            
    Consulta RsTmp, Sql
            
    If RsTmp.RecordCount > 0 Then
        RsTmp.MoveFirst
        With RsTmp
            For i = 1 To 14
                TxtMes(i) = CxP(Format(!fac_valor, "#,##0.##0"))
                RsTmp.MoveNext
            Next
        End With
    Else
        For i = 1 To 14
            TxtMes(i) = "1.000"
        Next
    End If
        
    
    
End Sub

Private Sub Form_Load()
    Centrar Me
    Skin2 Me, , 7
    'Aplicar_skin Me
    For i = 1 To 12
        TxtMes(i) = "1.000"
    Next
    TxtNuevos = "1.000"
    TxtCapital = "1.000"
    LLenaYears ComAno, 2010
    'For i = Year(Date) To Year(Date) - 1 Step -1
    '    ComAno.AddItem i
    '    ComAno.ItemData(ComAno.ListCount - 1) = i
    'Next
    'Busca_Id_Combo ComAno, Val(IG_Ano_Contable)
End Sub


Private Sub Timer3_Timer()
    On Error Resume Next
    ComAno.SetFocus
    Timer3.Enabled = False
End Sub

Private Sub TxtMes_GotFocus(Index As Integer)
    En_Foco TxtMes(Index)
End Sub



Private Sub TxtMes_Validate(Index As Integer, Cancel As Boolean)
    If Val(TxtMes(Index)) = 0 Then TxtMes(Index) = 0
End Sub
