VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{DA729E34-689F-49EA-A856-B57046630B73}#1.0#0"; "progressbar-xp.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form Con_BalanceComprobacion 
   Caption         =   "Informes de Balances"
   ClientHeight    =   9000
   ClientLeft      =   615
   ClientTop       =   1335
   ClientWidth     =   14055
   LinkTopic       =   "Form1"
   ScaleHeight     =   9000
   ScaleWidth      =   14055
   Begin VB.CommandButton cmdExportar 
      Caption         =   "F3 - Excel"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   525
      Left            =   420
      TabIndex        =   17
      ToolTipText     =   "Exportar"
      Top             =   7995
      Width           =   1620
   End
   Begin VB.CommandButton CmdSalir 
      Cancel          =   -1  'True
      Caption         =   "Esc - Retornar"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   525
      Left            =   12195
      TabIndex        =   16
      Top             =   7965
      Width           =   1620
   End
   Begin VB.Frame Frame3 
      Caption         =   "Empresa Activa"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   420
      TabIndex        =   10
      Top             =   30
      Width           =   10020
      Begin VB.TextBox SkRut 
         BackColor       =   &H00FFFFC0&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00C00000&
         Height          =   435
         Left            =   195
         TabIndex        =   12
         Text            =   "Text1"
         Top             =   345
         Width           =   2085
      End
      Begin VB.TextBox SkEmpresa 
         BackColor       =   &H00FFFFC0&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00C00000&
         Height          =   435
         Left            =   2280
         TabIndex        =   11
         Text            =   "Text1"
         Top             =   345
         Width           =   7515
      End
   End
   Begin VB.Frame Frame4 
      Caption         =   "Filtro periodo y tipo de balance"
      Height          =   1155
      Left            =   420
      TabIndex        =   2
      Top             =   975
      Width           =   12945
      Begin VB.ComboBox ComMes2 
         Height          =   315
         ItemData        =   "Con_BalanceComprobacion.frx":0000
         Left            =   2010
         List            =   "Con_BalanceComprobacion.frx":0002
         Style           =   2  'Dropdown List
         TabIndex        =   13
         Top             =   450
         Width           =   1575
      End
      Begin VB.ComboBox ComAno 
         Height          =   315
         Left            =   3630
         Style           =   2  'Dropdown List
         TabIndex        =   6
         Top             =   450
         Width           =   1215
      End
      Begin VB.ComboBox comMes 
         Height          =   315
         ItemData        =   "Con_BalanceComprobacion.frx":0004
         Left            =   135
         List            =   "Con_BalanceComprobacion.frx":0006
         Style           =   2  'Dropdown List
         TabIndex        =   5
         Top             =   450
         Width           =   1575
      End
      Begin VB.ComboBox CboTipo 
         Height          =   315
         ItemData        =   "Con_BalanceComprobacion.frx":0008
         Left            =   6090
         List            =   "Con_BalanceComprobacion.frx":000A
         Style           =   2  'Dropdown List
         TabIndex        =   4
         ToolTipText     =   "Debe crear Areas si desea cambiar esta opci�n"
         Top             =   435
         Width           =   5055
      End
      Begin VB.CommandButton CmdBusca 
         Caption         =   "Buscar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   435
         Left            =   11205
         TabIndex        =   3
         ToolTipText     =   "Busca texto ingresado"
         Top             =   360
         Width           =   1665
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
         Height          =   255
         Left            =   120
         OleObjectBlob   =   "Con_BalanceComprobacion.frx":000C
         TabIndex        =   7
         Top             =   255
         Width           =   375
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel5 
         Height          =   255
         Left            =   3660
         OleObjectBlob   =   "Con_BalanceComprobacion.frx":0070
         TabIndex        =   8
         Top             =   255
         Width           =   375
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   210
         Left            =   8115
         OleObjectBlob   =   "Con_BalanceComprobacion.frx":00D4
         TabIndex        =   9
         Top             =   225
         Width           =   2475
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel8 
         Height          =   255
         Left            =   2010
         OleObjectBlob   =   "Con_BalanceComprobacion.frx":0150
         TabIndex        =   14
         Top             =   270
         Width           =   375
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel10 
         Height          =   255
         Left            =   1830
         OleObjectBlob   =   "Con_BalanceComprobacion.frx":01B4
         TabIndex        =   15
         Top             =   510
         Width           =   300
      End
   End
   Begin VB.Frame frmOts 
      Caption         =   "Listado historico"
      Height          =   5745
      Left            =   435
      TabIndex        =   0
      Top             =   2190
      Width           =   13365
      Begin VB.Frame FraProgreso 
         Caption         =   "Progreso exportaci�n"
         Height          =   795
         Left            =   2550
         TabIndex        =   18
         Top             =   2145
         Visible         =   0   'False
         Width           =   9510
         Begin Proyecto2.XP_ProgressBar BarraProgreso 
            Height          =   330
            Left            =   150
            TabIndex        =   19
            Top             =   315
            Width           =   9165
            _ExtentX        =   16166
            _ExtentY        =   582
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BrushStyle      =   0
            Color           =   16777088
            Scrolling       =   1
            ShowText        =   -1  'True
         End
      End
      Begin MSComctlLib.ListView LvDetalle 
         Height          =   4680
         Left            =   180
         TabIndex        =   1
         Top             =   555
         Width           =   13185
         _ExtentX        =   23257
         _ExtentY        =   8255
         View            =   3
         LabelWrap       =   0   'False
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   13
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "N109"
            Text            =   "Cuenta"
            Object.Width           =   1235
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "T1000"
            Text            =   "Cuenta contable"
            Object.Width           =   3528
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   2
            Key             =   "debitos"
            Object.Tag             =   "N100"
            Text            =   "D�bitos"
            Object.Width           =   2293
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   3
            Key             =   "creditos"
            Object.Tag             =   "N100"
            Text            =   "Cr�ditos"
            Object.Width           =   2293
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   4
            Key             =   "debitosn"
            Object.Tag             =   "N100"
            Text            =   "Deudor"
            Object.Width           =   2293
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   5
            Key             =   "creditosn"
            Object.Tag             =   "N100"
            Text            =   "Acreedor"
            Object.Width           =   2293
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   6
            Key             =   "activo"
            Object.Tag             =   "N100"
            Text            =   "Activo"
            Object.Width           =   2293
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   7
            Key             =   "pasivo"
            Object.Tag             =   "N100"
            Text            =   "Pasivo"
            Object.Width           =   2293
         EndProperty
         BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   8
            Key             =   "perdida"
            Object.Tag             =   "N100"
            Text            =   "Perdidas"
            Object.Width           =   2293
         EndProperty
         BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   9
            Key             =   "ganancia"
            Object.Tag             =   "N100"
            Text            =   "Ganancias"
            Object.Width           =   2293
         EndProperty
         BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   10
            Object.Tag             =   "N109"
            Text            =   "Id Asiento"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(12) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   11
            Object.Tag             =   "N109"
            Text            =   "MES"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(13) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   12
            Object.Tag             =   "N109"
            Text            =   "ano"
            Object.Width           =   0
         EndProperty
      End
   End
   Begin VB.Timer Timer1 
      Left            =   90
      Top             =   570
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   0
      OleObjectBlob   =   "Con_BalanceComprobacion.frx":0216
      Top             =   0
   End
End
Attribute VB_Name = "Con_BalanceComprobacion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub CmdBusca_Click()
    CargaBalance
End Sub
Private Sub CmdExportar_Click()
    Dim tit(2) As String
    If LvDetalle.ListItems.Count = 0 Then Exit Sub
    FraProgreso.Visible = True
    tit(0) = Principal.SkEmpresa
    tit(1) = "RUT  :" & SP_Rut_Activo & " - " & Time
   ' tit(2) = "GIRO :" & giro
    tit(2) = CboTipo.Text & " Desde el 01 de " & comMes & " Hasta el 30 de " & ComMes2 & " del a�o " & ComAno
    ExportarNuevo LvDetalle, tit, Me, BarraProgreso
    FraProgreso.Visible = False
End Sub

Private Sub cmdSalir_Click()
    On Error Resume Next
    Unload Me
End Sub



Private Sub Form_Load()
    SkRut = SP_Rut_Activo
    SkEmpresa = SP_Empresa_Activa
    Skin2 Me, , 7
    Centrar Me
    LLenarCombo CboTipo, "tib_nombre", "tib_numero", "con_tipo_balance", "tib_activo='SI' AND tib_tipo='BALANCE'", "tib_id"
    CboTipo.ListIndex = 0
    
    
     For i = 1 To 12
        comMes.AddItem UCase(MonthName(i, False))
        comMes.ItemData(comMes.ListCount - 1) = i
        ComMes2.AddItem UCase(MonthName(i, False))
        ComMes2.ItemData(comMes.ListCount - 1) = i
    Next
    Busca_Id_Combo comMes, Val(IG_Mes_Contable)
    Busca_Id_Combo ComMes2, Val(IG_Mes_Contable)
    LLenaYears ComAno, 2010
   ' For i = Year(Date) To 2010 Step -1
   'For i = Year(Date) To Year(Date) - 1 Step -1
   '     ComAno.AddItem i
   '     ComAno.ItemData(ComAno.ListCount - 1) = i
       
   ' Next
   ' Busca_Id_Combo ComAno, Val(IG_Ano_Contable)
    
    
End Sub



Private Sub Timer1_Timer()
    On Error Resume Next
    CboCuenta.SetFocus
    Timer1.Enabled = False
End Sub
Private Sub CargaBalance()
    Dim Sp_Tipo As String, Sp_Periodo As String, Ip_Total As Integer
    
    Select Case CboTipo.ItemData(CboTipo.ListIndex)
        Case 1
            Sp_Tipo = "(1,2) "
            For i = 7 To 10
                LvDetalle.ColumnHeaders(i).Width = 0
            Next
        Case 2
            Sp_Tipo = "(1,3) "
            For i = 7 To 10
                LvDetalle.ColumnHeaders(i).Width = 0
            Next
        Case 3
            Sp_Tipo = "(1,2) "
            For i = 7 To 10
                LvDetalle.ColumnHeaders(i).Width = 1300
            Next
       Case 4
            Sp_Tipo = "(1,3) "
            For i = 7 To 10
                LvDetalle.ColumnHeaders(i).Width = 1300
            Next
            
            
            
    End Select
    Sp_Periodo = " AND (vou_mes_contable BETWEEN " & comMes.ItemData(comMes.ListIndex) & " AND " & ComMes2.ItemData(ComMes2.ListIndex) & ") AND vou_ano_contable = " & ComAno.Text & " "
    
 '   Sql = "SELECT pla_id,pla_nombre,debitos,credito,deudor,acreedor,activo,pasivo,perdida,ganancia " & _
            "FROM  vi_contabilidad_balance_trib_niif " & _
            "WHERE vou_tiponiif IN" & Sp_Tipo & Sp_Periodo & " AND rut_emp='" & SP_Rut_Activo & "' " & _
            "GROUP BY pla_id"
   Sql = "SELECT d.pla_id,c.pla_nombre,SUM(d.vod_debe)Debitos,SUM(d.vod_haber)Credito," & _
            "IF(SUM(d.vod_debe)> SUM(d.vod_haber),SUM(d.vod_debe)- SUM(d.vod_haber),0) Deudor," & _
            "IF(SUM(d.vod_debe)< SUM(d.vod_haber),SUM(d.vod_haber)- SUM(d.vod_debe),0) Acreedor," & _
            "IF(c.tpo_id=1 OR c.tpo_id=2," & _
                "IF(SUM(d.vod_debe)> SUM(d.vod_haber)," & _
                "SUM(d.vod_debe)- SUM(d.vod_haber),0),0) activo," & _
            "IF(c.tpo_id=1 OR c.tpo_id=2," & _
                "IF( SUM(d.vod_debe)< SUM(d.vod_haber)," & _
                "SUM(d.vod_haber)- SUM(d.vod_debe),0),0) pasivo," & _
            "IF(c.tpo_id=3 OR c.tpo_id=4," & _
                "IF(SUM(d.vod_debe)> SUM(d.vod_haber)," & _
                "SUM(d.vod_debe)- SUM(d.vod_haber),0),0) perdida," & _
            "IF(c.tpo_id=3 OR c.tpo_id=4," & _
                "IF(  SUM(d.vod_debe)< SUM(d.vod_haber)," & _
                "SUM(d.vod_haber)- SUM(d.vod_debe),0),0) ganancia " & _
        " FROM con_vouchers_detalle d " & _
            "JOIN con_plan_de_cuentas c USING(pla_id) " & _
            "JOIN con_vouchers v USING(vou_id) " & _
            "/*JOIN con_tipo_de_cuenta t USING(tpo_id) */ " & _
            " WHERE v.vou_tiponiif IN" & Sp_Tipo & Sp_Periodo & " AND rut_emp='" & SP_Rut_Activo & "' " & _
            " GROUP BY d.pla_id " & _
            "ORDER BY  c.tpo_id,c.det_id"
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvDetalle, False, True, True, False
    
      With LvDetalle
        .ListItems.Add , , ""
        i = .ListItems.Count
        Ip_Total = i
        .ListItems(i).SubItems(1) = "TOTALES"
        .ListItems(i).SubItems(2) = NumFormat(TotalizaColumna(LvDetalle, "debitos"))
        .ListItems(i).SubItems(3) = NumFormat(TotalizaColumna(LvDetalle, "creditos"))
        .ListItems(i).SubItems(4) = NumFormat(TotalizaColumna(LvDetalle, "debitosn"))
        .ListItems(i).SubItems(5) = NumFormat(TotalizaColumna(LvDetalle, "creditosn"))
        .ListItems(i).SubItems(6) = NumFormat(TotalizaColumna(LvDetalle, "activo"))
        .ListItems(i).SubItems(7) = NumFormat(TotalizaColumna(LvDetalle, "pasivo"))
        .ListItems(i).SubItems(8) = NumFormat(TotalizaColumna(LvDetalle, "perdida"))
        .ListItems(i).SubItems(9) = NumFormat(TotalizaColumna(LvDetalle, "ganancia"))
        For X = 1 To 9
            .ListItems(i).ListSubItems(X).Bold = True
        Next
    
        Select Case CboTipo.ItemData(CboTipo.ListIndex)
            Case 3, 4
                If CDbl(.ListItems(i).SubItems(6)) > CDbl(.ListItems(i).SubItems(7)) Then
                        .ListItems.Add , , ""
                        X = .ListItems.Count
                        .ListItems(X).SubItems(1) = "UTILIDAD DEL EJERCICIO"
                        .ListItems(X).SubItems(7) = NumFormat(CDbl(.ListItems(i).SubItems(6)) - CDbl(.ListItems(i).SubItems(7)))
                        .ListItems(X).SubItems(8) = NumFormat(CDbl(.ListItems(i).SubItems(9)) - CDbl(.ListItems(i).SubItems(8)))
                End If
                If CDbl(.ListItems(i).SubItems(6)) < CDbl(.ListItems(i).SubItems(7)) Then
                        .ListItems.Add , , ""
                        X = .ListItems.Count
                        .ListItems(X).SubItems(1) = "PERDIDA DEL EJERCICIO"
                        .ListItems(X).SubItems(6) = NumFormat(CDbl(.ListItems(i).SubItems(7)) - CDbl(.ListItems(i).SubItems(6)))
                        .ListItems(X).SubItems(9) = NumFormat(CDbl(.ListItems(i).SubItems(8)) - CDbl(.ListItems(i).SubItems(9)))
                End If
                If .ListItems.Count > 1 Then
                    For X = 1 To 9
                        .ListItems(i).ListSubItems(X).Bold = True
                    Next
                
                
                
                    .ListItems.Add , , ""
                    i = .ListItems.Count
                    .ListItems(i).SubItems(1) = "TOTALES GENERALES"
                    .ListItems(i).SubItems(2) = .ListItems(Ip_Total).SubItems(2)
                    .ListItems(i).SubItems(3) = .ListItems(Ip_Total).SubItems(3)
                    .ListItems(i).SubItems(4) = .ListItems(Ip_Total).SubItems(4)
                    .ListItems(i).SubItems(5) = .ListItems(Ip_Total).SubItems(5)
                    
                    If CDbl(.ListItems(Ip_Total).SubItems(6)) = CDbl(.ListItems(Ip_Total).SubItems(7)) Then
                        .ListItems(i).SubItems(6) = .ListItems(Ip_Total).SubItems(6)
                        .ListItems(i).SubItems(7) = .ListItems(Ip_Total).SubItems(7)
                        .ListItems(i).SubItems(8) = .ListItems(Ip_Total).SubItems(8)
                        .ListItems(i).SubItems(9) = .ListItems(Ip_Total).SubItems(9)
                    Else
                        
                        .ListItems(i).SubItems(6) = NumFormat(CDbl(.ListItems(Ip_Total).SubItems(6)) + CDbl(IIf(Val(.ListItems(i - 1).SubItems(6)) = 0, 0, .ListItems(i - 1).SubItems(6))))
                        .ListItems(i).SubItems(7) = NumFormat(CDbl(.ListItems(Ip_Total).SubItems(7)) + CDbl(IIf(Val(.ListItems(i - 1).SubItems(7)) = 0, 0, .ListItems(i - 1).SubItems(7))))
                        .ListItems(i).SubItems(8) = NumFormat(CDbl(.ListItems(Ip_Total).SubItems(8)) + CDbl(IIf(Val(.ListItems(i - 1).SubItems(8)) = 0, 0, .ListItems(i - 1).SubItems(8))))
                        .ListItems(i).SubItems(9) = NumFormat(CDbl(.ListItems(Ip_Total).SubItems(9)) + CDbl(IIf(Val(.ListItems(i - 1).SubItems(9)) = 0, 0, .ListItems(i - 1).SubItems(9))))
                    End If
                    For X = 1 To 9
                        .ListItems(i).ListSubItems(X).Bold = True
                    Next

                End If
        End Select
    End With
End Sub
