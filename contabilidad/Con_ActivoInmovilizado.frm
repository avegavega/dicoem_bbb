VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{DA729E34-689F-49EA-A856-B57046630B73}#1.0#0"; "progressbar-xp.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form Con_ActivoInmovilizado 
   Caption         =   "Activo Inmovilizado"
   ClientHeight    =   8370
   ClientLeft      =   -75
   ClientTop       =   1485
   ClientWidth     =   15420
   LinkTopic       =   "Form1"
   ScaleHeight     =   8370
   ScaleWidth      =   15420
   Begin VB.Frame FraHistoria 
      Caption         =   "Historia Activo"
      Height          =   2055
      Left            =   3660
      TabIndex        =   53
      Top             =   -150
      Visible         =   0   'False
      Width           =   10245
      Begin ACTIVESKINLibCtl.SkinLabel SkActivio 
         Height          =   300
         Left            =   1710
         OleObjectBlob   =   "Con_ActivoInmovilizado.frx":0000
         TabIndex        =   56
         Top             =   120
         Width           =   6945
      End
      Begin VB.CommandButton CmdCierraHistoria 
         Caption         =   "x"
         Height          =   225
         Left            =   9885
         TabIndex        =   55
         Top             =   120
         Width           =   270
      End
      Begin MSComctlLib.ListView LvHistoriaActivo 
         Height          =   1545
         Left            =   135
         TabIndex        =   54
         Top             =   420
         Width           =   9900
         _ExtentX        =   17463
         _ExtentY        =   2725
         View            =   3
         LabelWrap       =   0   'False
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   7
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "N109"
            Text            =   "Id Unico"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "N109"
            Text            =   "Mes"
            Object.Width           =   1235
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "N109"
            Text            =   "A�o"
            Object.Width           =   1323
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   2
            SubItemIndex    =   3
            Object.Tag             =   "N109"
            Text            =   "Nro Voucher"
            Object.Width           =   1940
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   4
            Object.Tag             =   "N100"
            Text            =   "Debe"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   5
            Object.Tag             =   "N100"
            Text            =   "Haber"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   6
            Object.Tag             =   "T3000"
            Text            =   "Glosa"
            Object.Width           =   7056
         EndProperty
      End
   End
   Begin VB.ComboBox ComAno 
      Height          =   315
      Left            =   1875
      Style           =   2  'Dropdown List
      TabIndex        =   39
      Top             =   555
      Width           =   1215
   End
   Begin VB.TextBox TxtTotal2 
      Alignment       =   1  'Right Justify
      BackColor       =   &H8000000F&
      Height          =   285
      Left            =   12315
      Locked          =   -1  'True
      TabIndex        =   38
      TabStop         =   0   'False
      Tag             =   "T"
      Top             =   6255
      Width           =   1125
   End
   Begin VB.Frame FraProgreso 
      Height          =   735
      Left            =   1950
      TabIndex        =   29
      Top             =   2700
      Visible         =   0   'False
      Width           =   11475
      Begin Proyecto2.XP_ProgressBar BarraProgreso 
         Height          =   375
         Left            =   105
         TabIndex        =   30
         Top             =   225
         Width           =   11205
         _ExtentX        =   19764
         _ExtentY        =   661
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BrushStyle      =   0
         Color           =   16777088
         Scrolling       =   1
         ShowText        =   -1  'True
      End
   End
   Begin VB.CommandButton cmdExportar 
      Caption         =   "Exportar Lista a Excel"
      Height          =   390
      Left            =   165
      TabIndex        =   28
      ToolTipText     =   "Exportar"
      Top             =   7875
      Width           =   2055
   End
   Begin VB.CommandButton cmdSalir 
      Cancel          =   -1  'True
      Caption         =   "&Retornar"
      Height          =   405
      Left            =   13500
      TabIndex        =   27
      Top             =   7905
      Width           =   1350
   End
   Begin VB.Timer Timer1 
      Left            =   1365
      Top             =   45
   End
   Begin VB.Frame FrmMantenedor 
      Caption         =   "Activos Inmovilizados"
      Height          =   6405
      Left            =   195
      TabIndex        =   9
      Top             =   1440
      Width           =   15180
      Begin VB.TextBox txtTotalSeleccionado 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         Height          =   285
         Left            =   10845
         Locked          =   -1  'True
         TabIndex        =   51
         TabStop         =   0   'False
         Tag             =   "T"
         Top             =   5580
         Width           =   1275
      End
      Begin VB.CommandButton CmdVoucher 
         Caption         =   "Voucher por el total"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   435
         Left            =   10845
         TabIndex        =   50
         ToolTipText     =   "Total del Voucher"
         Top             =   5100
         Visible         =   0   'False
         Width           =   2415
      End
      Begin VB.Frame Frame1 
         Caption         =   "Filtros"
         Height          =   855
         Left            =   120
         TabIndex        =   41
         Top             =   5040
         Width           =   7575
         Begin VB.CommandButton CmdX 
            Caption         =   "x"
            Height          =   210
            Left            =   7035
            TabIndex        =   46
            ToolTipText     =   "Eliminar filtro- Mostrar todos"
            Top             =   495
            Width           =   195
         End
         Begin VB.CommandButton CmdBusca 
            Caption         =   "Buscar"
            Height          =   315
            Left            =   6240
            TabIndex        =   45
            ToolTipText     =   "Buscar los datos ingresados"
            Top             =   435
            Width           =   765
         End
         Begin VB.TextBox TxtBuscaCuenta 
            Height          =   315
            Left            =   2640
            TabIndex        =   44
            Tag             =   "T"
            ToolTipText     =   "Ingrese datos para buscar"
            Top             =   435
            Width           =   2490
         End
         Begin VB.ComboBox CboFiltro 
            Height          =   315
            Left            =   120
            Style           =   2  'Dropdown List
            TabIndex        =   43
            ToolTipText     =   "Para cambiar CUENTA doble click en linea de detalle"
            Top             =   435
            Width           =   2500
         End
         Begin VB.ComboBox CboFiltroActivo 
            Appearance      =   0  'Flat
            Height          =   315
            ItemData        =   "Con_ActivoInmovilizado.frx":007A
            Left            =   5115
            List            =   "Con_ActivoInmovilizado.frx":0084
            Style           =   2  'Dropdown List
            TabIndex        =   42
            Top             =   435
            Width           =   1110
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
            Height          =   255
            Index           =   0
            Left            =   2640
            OleObjectBlob   =   "Con_ActivoInmovilizado.frx":0090
            TabIndex        =   47
            Top             =   240
            Width           =   1965
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
            Height          =   210
            Index           =   2
            Left            =   120
            OleObjectBlob   =   "Con_ActivoInmovilizado.frx":0126
            TabIndex        =   48
            Top             =   240
            Width           =   2355
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel5 
            Height          =   255
            Index           =   3
            Left            =   5145
            OleObjectBlob   =   "Con_ActivoInmovilizado.frx":01A8
            TabIndex        =   49
            Top             =   240
            Width           =   1050
         End
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
         Height          =   180
         Left            =   12045
         OleObjectBlob   =   "Con_ActivoInmovilizado.frx":0222
         TabIndex        =   37
         Top             =   420
         Width           =   1110
      End
      Begin VB.TextBox TxtDacumu 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   12030
         TabIndex        =   6
         Tag             =   "N"
         Text            =   "0"
         Top             =   600
         Width           =   990
      End
      Begin VB.TextBox TxtTotal 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         Height          =   285
         Left            =   10890
         Locked          =   -1  'True
         TabIndex        =   33
         TabStop         =   0   'False
         Tag             =   "T"
         Top             =   4815
         Width           =   1200
      End
      Begin VB.CommandButton CmdSelecciona 
         Caption         =   "Seleccionar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   405
         Left            =   10830
         TabIndex        =   31
         ToolTipText     =   "Seleccionar marcados, valores que pasan al Voucher"
         Top             =   5880
         Visible         =   0   'False
         Width           =   2430
      End
      Begin VB.TextBox TxtTipoDepreciacion 
         BackColor       =   &H8000000F&
         Height          =   285
         Left            =   8940
         Locked          =   -1  'True
         TabIndex        =   26
         TabStop         =   0   'False
         Tag             =   "T"
         Top             =   600
         Width           =   1065
      End
      Begin VB.TextBox TxtEgreso 
         BackColor       =   &H8000000F&
         Height          =   285
         Left            =   6990
         Locked          =   -1  'True
         TabIndex        =   25
         TabStop         =   0   'False
         Tag             =   "T"
         Top             =   600
         Width           =   1275
      End
      Begin VB.ComboBox CboCuenta 
         Height          =   315
         Left            =   720
         Style           =   2  'Dropdown List
         TabIndex        =   0
         ToolTipText     =   "Para cambiar CUENTA doble click en linea de detalle"
         Top             =   600
         Width           =   2505
      End
      Begin VB.CommandButton cmdCuenta 
         Caption         =   "Codigo Cuenta"
         Height          =   225
         Left            =   720
         TabIndex        =   24
         Top             =   375
         Width           =   1425
      End
      Begin VB.TextBox TxtNroCuenta 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   270
         Left            =   2145
         TabIndex        =   23
         Tag             =   "N2"
         ToolTipText     =   "Unidad medida"
         Top             =   345
         Width           =   885
      End
      Begin VB.TextBox TxtNeto 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   10920
         TabIndex        =   5
         Tag             =   "N"
         Text            =   "0"
         Top             =   600
         Width           =   1125
      End
      Begin VB.TextBox TxtVidaUtil 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   8280
         TabIndex        =   3
         Tag             =   "T"
         Text            =   "0"
         ToolTipText     =   "Expresar en meses "
         Top             =   600
         Width           =   660
      End
      Begin VB.TextBox TxtId 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         Height          =   315
         Left            =   120
         Locked          =   -1  'True
         TabIndex        =   10
         TabStop         =   0   'False
         Tag             =   "T"
         Top             =   600
         Width           =   600
      End
      Begin VB.ComboBox CboActivo 
         Appearance      =   0  'Flat
         Height          =   315
         ItemData        =   "Con_ActivoInmovilizado.frx":0296
         Left            =   14085
         List            =   "Con_ActivoInmovilizado.frx":02A0
         Style           =   2  'Dropdown List
         TabIndex        =   7
         Top             =   585
         Width           =   690
      End
      Begin VB.TextBox TxtNombre 
         Height          =   300
         Left            =   3210
         TabIndex        =   1
         Tag             =   "T"
         Top             =   600
         Width           =   2500
      End
      Begin VB.CommandButton CmdOk 
         Caption         =   "Ok"
         Height          =   270
         Left            =   14760
         TabIndex        =   8
         Top             =   600
         Width           =   345
      End
      Begin VB.ComboBox CboNuevo 
         Appearance      =   0  'Flat
         Height          =   315
         ItemData        =   "Con_ActivoInmovilizado.frx":02AC
         Left            =   10020
         List            =   "Con_ActivoInmovilizado.frx":02B6
         Style           =   2  'Dropdown List
         TabIndex        =   4
         Top             =   600
         Width           =   915
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   225
         Left            =   120
         OleObjectBlob   =   "Con_ActivoInmovilizado.frx":02C2
         TabIndex        =   11
         Top             =   4800
         Width           =   6675
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   255
         Index           =   3
         Left            =   3225
         OleObjectBlob   =   "Con_ActivoInmovilizado.frx":03C0
         TabIndex        =   12
         Top             =   420
         Width           =   1095
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel5 
         Height          =   255
         Index           =   2
         Left            =   14100
         OleObjectBlob   =   "Con_ActivoInmovilizado.frx":042A
         TabIndex        =   13
         Top             =   420
         Width           =   1020
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   255
         Index           =   5
         Left            =   120
         OleObjectBlob   =   "Con_ActivoInmovilizado.frx":0496
         TabIndex        =   14
         Top             =   405
         Width           =   615
      End
      Begin MSComctlLib.ListView LvDetalle 
         Height          =   3810
         Left            =   75
         TabIndex        =   15
         Top             =   930
         Width           =   15060
         _ExtentX        =   26564
         _ExtentY        =   6720
         View            =   3
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   13
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "N109"
            Text            =   "Id"
            Object.Width           =   1058
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "T1000"
            Text            =   "Cuenta"
            Object.Width           =   4410
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "T3000"
            Text            =   "Nombre"
            Object.Width           =   4392
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Object.Tag             =   "F1000"
            Text            =   "Fecha Ingreso"
            Object.Width           =   2240
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Object.Tag             =   "F1000"
            Text            =   "Fecha Egreso"
            Object.Width           =   2240
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Object.Tag             =   "N109"
            Text            =   "Vida Util"
            Object.Width           =   1199
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   6
            Object.Tag             =   "T1500"
            Text            =   "Depreciacion"
            Object.Width           =   1905
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   7
            Object.Tag             =   "T800"
            Text            =   "Nuevo"
            Object.Width           =   1587
         EndProperty
         BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   8
            Key             =   "valor"
            Object.Tag             =   "N100"
            Text            =   "Valor"
            Object.Width           =   2081
         EndProperty
         BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   9
            Key             =   "Dep.Acumul."
            Object.Tag             =   "N100"
            Text            =   "Dep.Acumul."
            Object.Width           =   1923
         EndProperty
         BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   10
            Object.Tag             =   "T100"
            Text            =   "Habilitado"
            Object.Width           =   1535
         EndProperty
         BeginProperty ColumnHeader(12) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   11
            Object.Tag             =   "F1000"
            Text            =   "Fec.Adquis."
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(13) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   12
            Object.Tag             =   "N109"
            Text            =   "Cuenta Id"
            Object.Width           =   0
         EndProperty
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel5 
         Height          =   255
         Index           =   0
         Left            =   10125
         OleObjectBlob   =   "Con_ActivoInmovilizado.frx":04F8
         TabIndex        =   16
         Top             =   405
         Width           =   735
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel5 
         Height          =   240
         Index           =   1
         Left            =   9000
         OleObjectBlob   =   "Con_ActivoInmovilizado.frx":0560
         TabIndex        =   17
         Top             =   435
         Width           =   1335
      End
      Begin MSComCtl2.DTPicker DtIngreso 
         Height          =   300
         Left            =   5730
         TabIndex        =   2
         Top             =   600
         Width           =   1275
         _ExtentX        =   2249
         _ExtentY        =   529
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         CustomFormat    =   "dd-mm-yyyy hh:mm:ss"
         Format          =   273416193
         CurrentDate     =   39855
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   225
         Left            =   3420
         OleObjectBlob   =   "Con_ActivoInmovilizado.frx":05D6
         TabIndex        =   19
         Top             =   660
         Width           =   1065
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   255
         Index           =   1
         Left            =   5730
         OleObjectBlob   =   "Con_ActivoInmovilizado.frx":0647
         TabIndex        =   20
         Top             =   405
         Width           =   1095
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   255
         Index           =   4
         Left            =   8205
         OleObjectBlob   =   "Con_ActivoInmovilizado.frx":06BF
         TabIndex        =   21
         ToolTipText     =   "Expresar en meses"
         Top             =   405
         Width           =   645
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   255
         Index           =   6
         Left            =   10545
         OleObjectBlob   =   "Con_ActivoInmovilizado.frx":072F
         TabIndex        =   22
         Top             =   420
         Width           =   1080
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   255
         Index           =   7
         Left            =   9525
         OleObjectBlob   =   "Con_ActivoInmovilizado.frx":0799
         TabIndex        =   34
         Top             =   4845
         Width           =   1290
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   255
         Index           =   10
         Left            =   8865
         OleObjectBlob   =   "Con_ActivoInmovilizado.frx":0805
         TabIndex        =   52
         Top             =   5595
         Width           =   1920
      End
      Begin MSComCtl2.DTPicker DtFechaAdquisicion 
         Height          =   300
         Left            =   13020
         TabIndex        =   57
         ToolTipText     =   "Fecha de adquisicion del bien."
         Top             =   585
         Width           =   1080
         _ExtentX        =   1905
         _ExtentY        =   529
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         CustomFormat    =   "dd-mm-yyyy hh:mm:ss"
         Format          =   273416193
         CurrentDate     =   39855
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   255
         Index           =   11
         Left            =   13095
         OleObjectBlob   =   "Con_ActivoInmovilizado.frx":088B
         TabIndex        =   58
         Top             =   390
         Width           =   960
      End
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkEmpresaActiva 
      Height          =   285
      Left            =   7665
      OleObjectBlob   =   "Con_ActivoInmovilizado.frx":08FD
      TabIndex        =   18
      Top             =   465
      Width           =   6285
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   0
      OleObjectBlob   =   "Con_ActivoInmovilizado.frx":0977
      Top             =   0
   End
   Begin MSComctlLib.ListView LvExcel 
      Height          =   3810
      Left            =   225
      TabIndex        =   32
      Top             =   8385
      Width           =   14460
      _ExtentX        =   25506
      _ExtentY        =   6720
      View            =   3
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   0
      NumItems        =   7
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Object.Tag             =   "N109"
         Text            =   "Id"
         Object.Width           =   1058
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Object.Tag             =   "T3000"
         Text            =   "Nombre"
         Object.Width           =   5292
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Object.Tag             =   "F1000"
         Text            =   "Fecha Ingreso"
         Object.Width           =   2258
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Object.Tag             =   "F1000"
         Text            =   "Fecha Egreso"
         Object.Width           =   2249
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Object.Tag             =   "T1000"
         Text            =   "Vida Util"
         Object.Width           =   1799
      EndProperty
      BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   5
         Object.Tag             =   "T800"
         Text            =   "Nuevo"
         Object.Width           =   1614
      EndProperty
      BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   6
         Object.Tag             =   "N100"
         Text            =   "Valor"
         Object.Width           =   2275
      EndProperty
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
      Height          =   255
      Index           =   8
      Left            =   0
      OleObjectBlob   =   "Con_ActivoInmovilizado.frx":0BAB
      TabIndex        =   35
      Top             =   0
      Width           =   1080
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
      Height          =   255
      Index           =   9
      Left            =   0
      OleObjectBlob   =   "Con_ActivoInmovilizado.frx":0C15
      TabIndex        =   36
      Top             =   0
      Width           =   1080
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel5 
      Height          =   255
      Index           =   5
      Left            =   1935
      OleObjectBlob   =   "Con_ActivoInmovilizado.frx":0C7F
      TabIndex        =   40
      Top             =   330
      Width           =   375
   End
End
Attribute VB_Name = "Con_ActivoInmovilizado"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public Sm_FiltroCta As String, Sm_Viene As String, Sm_AnoPedido As String
Dim Sm_filtro As String, Sm_Filtro_Activo As String

Private Sub CmdBusca_Click()
    Sm_filtro = ""
    Sm_Filtro_Activo = ""
    If Len(TxtBuscaCuenta) > 0 Then
        Sm_filtro = " AND aim_nombre LIKE '" & TxtBuscaCuenta & "%' "
    End If
    If CboFiltro.Text <> "TODAS" And CboFiltro.ListIndex > -1 Then
    
        Sm_filtro = Sm_filtro & " AND a.pla_id=" & CboFiltro.ItemData(CboFiltro.ListIndex) & " "
    End If
    If CboFiltroActivo.Text <> "TODOS" Then
        Sm_Filtro_Activo = " AND aim_habilitado='" & CboFiltroActivo.Text & "' "
    End If
    
    
    CargaActivos
End Sub

Private Sub CmdCierraHistoria_Click()
    CmdCierraHistoria.Visible = False
End Sub

Private Sub cmdCuenta_Click()
    With BuscarSimple
        SG_codigo = Empty
        .Sm_Consulta = "SELECT pla_id, pla_nombre,tpo_nombre,det_nombre " & _
                       "FROM    con_plan_de_cuentas p,  con_tipo_de_cuenta t,   con_detalle_tipo_cuenta d " & _
                       "WHERE   p.tpo_id = t.tpo_id AND p.det_id = d.det_id AND p.tpo_id=1 AND p.det_id=4 "
        .Sm_CampoLike = "pla_nombre"
        .Im_Columnas = 2
        .Show 1
        If SG_codigo = Empty Then Exit Sub
        Busca_Id_Combo CboCuenta, Val(SG_codigo)
    End With
End Sub

Private Sub CmdExportar_Click()
      
    Dim tit(2) As String, H As Long
    
    If LvDetalle.ListItems.Count = 0 Then Exit Sub
    LvExcel.ListItems.Clear
    Sql = "SELECT DISTINCTROW(a.pla_id) pla_id,pla_nombre " & _
            "FROM con_activo_inmovilizado a " & _
            "JOIN con_plan_de_cuentas p ON a.pla_id=p.pla_id " & _
            "WHERE rut_emp='" & SP_Rut_Activo & "' " & Sm_filtro & Sm_FiltroCta & Sm_Filtro_Activo
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        RsTmp.MoveFirst
        Do While Not RsTmp.EOF
            
            
        
            LvExcel.ListItems.Add , , ""
            H = LvExcel.ListItems.Count
            LvExcel.ListItems(H).SubItems(1) = RsTmp!pla_nombre
            
            Sql = "SELECT aim_id,aim_nombre,aim_fecha_ingreso,aim_fecha_egreso,aim_vida_util,aim_nuevo,aim_valor,aim_dacumu,aim_habilitado " & _
                        "FROM con_activo_inmovilizado a " & _
                        "JOIN con_plan_de_cuentas p ON a.pla_id=p.pla_id " & _
                        "WHERE rut_emp='" & SP_Rut_Activo & "' " & Sm_filtro & Sm_FiltroCta & Sm_Filtro_Activo & " AND a.pla_id=" & RsTmp!pla_id
            Consulta RsTmp2, Sql
            If RsTmp2.RecordCount > 0 Then
                RsTmp2.MoveFirst
                Do While Not RsTmp2.EOF
                    LvExcel.ListItems.Add , , RsTmp2!aim_id
                    H = LvExcel.ListItems.Count
                    LvExcel.ListItems(H).SubItems(1) = RsTmp2!aim_nombre
                    LvExcel.ListItems(H).SubItems(2) = RsTmp2!aim_fecha_ingreso
                    LvExcel.ListItems(H).SubItems(3) = "" & RsTmp2!aim_fecha_egreso
                    LvExcel.ListItems(H).SubItems(4) = RsTmp2!aim_vida_util
                    LvExcel.ListItems(H).SubItems(5) = RsTmp2!aim_nuevo
                    LvExcel.ListItems(H).SubItems(6) = RsTmp2!aim_valor
                   ' LvExcel.ListItems(H).SubItems(7) = RsTmp2!aim_dacumu
                                   
                    RsTmp2.MoveNext
                Loop
            End If
            
            LvExcel.ListItems.Add , , ""
            H = LvExcel.ListItems.Count
            LvExcel.ListItems(H).SubItems(1) = "TOTAL CUENTA"
            
            Sql = "SELECT IFNULL(SUM(aim_valor),0) Valor " & _
                        "FROM con_activo_inmovilizado a " & _
                        "JOIN con_plan_de_cuentas p ON a.pla_id=p.pla_id " & _
                        "WHERE rut_emp='" & SP_Rut_Activo & "' " & Sm_filtro & Sm_FiltroCta & Sm_Filtro_Activo & " AND a.pla_id=" & RsTmp!pla_id
            Consulta RsTmp3, Sql
            If RsTmp3.RecordCount > 0 Then
                LvExcel.ListItems(H).SubItems(6) = RsTmp3!Valor
            End If
            LvExcel.ListItems.Add , , ""
            
            
            '''mario
         '  Sql = "SELECT IFNULL(SUM(aim_dacumu),0) Valor " & _
         '               "FROM con_activo_inmovilizado a " & _
         '               "JOIN con_plan_de_cuentas p ON a.pla_id=p.pla_id " & _
         '               "WHERE rut_emp='" & SP_Rut_Activo & "' " & Sm_Filtro & Sm_FiltroCta & Sm_Filtro_Activo & " AND a.pla_id=" & RsTmp!pla_id
         '   Consulta RsTmp3, Sql
         '   If RsTmp3.RecordCount > 0 Then
         '       LvExcel.ListItems(H).SubItems(6) = RsTmp3!Valor
         '   End If
         '   LvExcel.ListItems.Add , , ""
            '''mario
            
        
            RsTmp.MoveNext
        Loop
        
        LvExcel.ListItems.Add , , ""
        H = LvExcel.ListItems.Count
        LvExcel.ListItems(H).SubItems(1) = "TOTAL ACTIVO INMOVILIZADO"
        LvExcel.ListItems(H).SubItems(6) = txtTotal
       ' LvExcel.ListItems(H).SubItems(7) = TxtTotal2
        
    End If
    

    
    
    
    FraProgreso.Visible = True
    tit(0) = Me.Caption & " " & Date & " - " & Time
   ' tit(1) = "TIPO DEPRECIACION " & TxtTipoDepreciacion
    
    ''''mario
   ' tit(0) = Principal.SkEmpresa
    tit(1) = "RUT  :" & SP_Rut_Activo & " - " & Time & " TIPO DEPRECIACION " & TxtTipoDepreciacion & " " & ComAno
    'tit(2) = CboTipo.Text & " Desde el 01 de " & comMes & " Hasta el 30 de " & ComMes2 & " del a�o " & ComAno
    '''mario
    
    tit(2) = ""
    ExportarNuevo LvExcel, tit, Me, BarraProgreso
    FraProgreso.Visible = False
End Sub

Private Sub CmdOk_Click()
    If Len(TxtNombre) = 0 Then
        MsgBox "Debe ingresar nombre de Activo", vbInformation
        TxtNombre.SetFocus
        Exit Sub
    End If
    
    '''mario Existen bienes que no tienen vida �til
    If Val(TxtVidaUtil) = 0 Then
           '''sigue
           ' If Val(TxtNeto) <> 1 Then
           '     MsgBox "Debe ingresar vida util", vbInformation
           '     TxtVidaUtil.SetFocus
           '     Exit Sub
           ' End If
           TxtVidaUtil = "0"
    End If
    '''mario
    
    If Val(TxtNeto) = 0 Then
        
        MsgBox "Debe ingresar valor...", vbInformation
        TxtNeto.SetFocus
        Exit Sub
    End If
    
    Sql = "SELECT aim_id " & _
            "FROM con_activo_inmovilizado " & _
            "WHERE rut_emp='" & SP_Rut_Activo & "' AND aim_nombre='" & TxtNombre & "' AND aim_id<>" & Val(TxtId)
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        MsgBox "Ya existe un activo con el nombre que ha ingresado... (Codigo " & RsTmp!aim_id & " )", vbInformation
        TxtNombre.SetFocus
        Exit Sub
    End If
    
    If Val(TxtId) = 0 Then
        Sql = "INSERT INTO con_activo_inmovilizado (pla_id,aim_nombre,aim_fecha_ingreso,aim_vida_util,aim_tipo_depreciacion,aim_nuevo,aim_valor,aim_dacumu,rut_emp,aim_fecha_adquisicion) " & _
                "VALUES(" & CboCuenta.ItemData(CboCuenta.ListIndex) & ",'" & TxtNombre & "','" & Fql(DtIngreso) & "'," & TxtVidaUtil & ",'" & TxtTipoDepreciacion & "','" & CboNuevo.Text & "'," & CDbl(Me.TxtNeto) & "," & CDbl(Me.TxtDacumu) & ",'" & SP_Rut_Activo & "','" & Fql(DtFechaAdquisicion) & "')"
    
    Else
        Sql = "UPDATE con_activo_inmovilizado SET " & _
                        "aim_nombre='" & TxtNombre & "',aim_fecha_ingreso='" & Fql(DtIngreso) & "'," & _
                        "aim_vida_util=" & TxtVidaUtil & ",aim_tipo_depreciacion='" & TxtTipoDepreciacion & "'," & _
                        "aim_nuevo='" & CboNuevo.Text & "',aim_valor=" & CDbl(TxtNeto) & ",aim_dacumu=" & CDbl(TxtDacumu) & ",aim_habilitado='" & CboActivo.Text & "'  " & _
                        ",aim_fecha_adquisicion='" & Fql(DtFechaAdquisicion) & "' " & _
                "WHERE aim_id=" & Val(TxtId)
    End If
    cn.Execute Sql
    
    TxtNombre = ""
    TxtEgreso = ""
    TxtVidaUtil = ""
   ' TxtTipoDepreciacion = ""
    CboNuevo.ListIndex = 0
    TxtNeto = 0
    TxtDacumu = 0
    CboActivo.ListIndex = 0
    TxtId = ""
    CargaActivos
End Sub

Private Sub cmdSalir_Click()
    Unload Me
End Sub

Private Sub CmdSelecciona_Click()
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    SG_codigo = Empty
    If paso = "voucher" Then
    
        For i = 1 To LvDetalle.ListItems.Count
            If LvDetalle.ListItems(i).Checked Then SG_codigo = SG_codigo & LvDetalle.ListItems(i) & "/" & CDbl(LvDetalle.ListItems(i).SubItems(8)) & ","
        Next
        If Len(SG_codigo) = 0 Then
            MsgBox "No ha seleccionado ning�n activo...", vbExclamation
            Exit Sub
        End If
        SG_codigo = Mid(SG_codigo, 1, Len(SG_codigo) - 1)
    Else
        SG_codigo = LvDetalle.SelectedItem
    End If
    SG_codigo2 = txtTotalSeleccionado
    Unload Me
    
End Sub

Private Sub CmdVoucher_Click()
    SG_codigo = ""
    SG_codigo2 = txtTotal
    For i = 1 To LvDetalle.ListItems.Count
        SG_codigo = SG_codigo & LvDetalle.ListItems(i) & "/" & CDbl(LvDetalle.ListItems(i).SubItems(8)) & ","

    Next
    If Len(SG_codigo) = 0 Then
        MsgBox "No hay activos...", vbExclamation
        Exit Sub
    End If
    SG_codigo = Mid(SG_codigo, 1, Len(SG_codigo) - 1)
    Unload Me
End Sub

Private Sub CmdX_Click()
    Sm_filtro = ""
    CargaActivos
End Sub





Private Sub ComAno_Click()
    CargaActivos
End Sub

Private Sub Form_Load()
    Dim Sp_FiltroCta As String
    

    SkRut = SP_Rut_Activo
    SkEmpresaActiva = SP_Empresa_Activa
    Skin2 Me, , 7
    'Aplicar_skin Me
    
    Centrar Me
    Sp_extra = ""
    Sql = "SELECT men_infoextra " & _
          "FROM sis_menu " & _
          "WHERE men_id=" & IP_IDMenu
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        Sp_extra = RsTmp!men_infoextra
    End If
    'Si viene desde el menu
    'liberamos las variables publicas de este modulo
    If Sp_extra = "MENU" Then
        Sm_FiltroCta = Empty
        Sm_Viene = Empty
    End If
    
    '*************************************************
    'Mario Mario Mario
    LLenaYears ComAno, 2010
    'dejar comentado esto
 '   For i = Year(Date) To 2010 Step -1
 '       ComAno.AddItem i
 '       ComAno.ItemData(ComAno.ListCount - 1) = i
 '   Next
 '   Busca_Id_Combo ComAno, Val(IG_Ano_Contable)
    '***********************************************
    
    DtIngreso = Date
    DtEgreso = Date
  '  CboNuevo.AddItem "SI"
  '  CboNuevo.AddItem "NO"
    CboNuevo.ListIndex = 1
   ' CboActivo.AddItem "SI"
    'CboActivo.AddItem "NO"
    CboActivo.ListIndex = 0
    
    CboFiltroActivo.AddItem "TODOS"
    CboFiltroActivo.ListIndex = 2
    
    Sql = "SELECT emp_tipo_depreciacion amp_tipo " & _
            "FROM sis_empresas " & _
            "WHERE rut='" & SP_Rut_Activo & "'"
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then TxtTipoDepreciacion = RsTmp!amp_tipo
    
    'Aqui detectamos que viene de otra parte buscando informacion
    'puede de ser de compra de venta o de voucher
    '12 Enero 2013
    Sp_FiltroCta = ""
    If Val(SG_codigo2) > 0 Then Sm_FiltroCta = " AND a.pla_id=" & Val(SG_codigo2)
    
        
    Sm_Filtro_Activo = " AND aim_habilitado='SI' "
    
    LLenarCombo CboCuenta, "pla_nombre", "pla_id", "con_plan_de_cuentas a ", "tpo_id=1 AND det_id=4 " & Sm_FiltroCta
    LLenarCombo CboFiltro, "pla_nombre", "pla_id", "con_plan_de_cuentas a ", "tpo_id=1 AND det_id=4" & Sm_FiltroCta
    CboFiltro.AddItem "TODAS"
    CboCuenta.ListIndex = 0
    CargaActivos
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
        Sp_extra = Empty
        Sm_FiltroCta = Empty
        Sm_Viene = Empty
        
        paso = Empty
End Sub

Private Sub LvDetalle_Click()
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
  
    Sql = "SELECT v.vou_id,v.vou_mes_contable,vou_ano_contable,vou_numero, IF(d.vod_debe>0,r.aim_valor,0),IF(vod_haber>0,r.aim_valor,0),vou_glosa " & _
                    "FROM con_vouchers_detalle d " & _
                    "JOIN con_relacion_activos r ON d.pla_id = r.pla_id  AND d.vou_id = r.vou_id AND d.vod_id = r.vod_id " & _
                    "JOIN con_vouchers v ON d.vou_id = v.vou_id " & _
                    "WHERE r.aim_id=" & Me.LvDetalle.SelectedItem & "  AND v.rut_emp = '" & SP_Rut_Activo & "'"
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        FraHistoria.Visible = True
        SkActivio = LvDetalle.SelectedItem.SubItems(2)
        LLenar_Grilla RsTmp, Me, LvHistoriaActivo, False, True, True, False
        LvHistoriaActivo.SetFocus
    End If
End Sub

Private Sub LvDetalle_DblClick()
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    
    TxtId = LvDetalle.SelectedItem
    Busca_Id_Combo CboCuenta, Val(LvDetalle.SelectedItem.SubItems(11))
    TxtNombre = LvDetalle.SelectedItem.SubItems(2)
    DtIngreso = LvDetalle.SelectedItem.SubItems(3)
    TxtEgreso = LvDetalle.SelectedItem.SubItems(4)
    TxtVidaUtil = LvDetalle.SelectedItem.SubItems(5)
    
    CboNuevo.ListIndex = IIf(LvDetalle.SelectedItem.SubItems(7) = "SI", 0, 1)
    TxtNeto = LvDetalle.SelectedItem.SubItems(8)
    TxtDacumu = LvDetalle.SelectedItem.SubItems(9)
    Me.DtFechaAdquisicion = LvDetalle.SelectedItem.SubItems(10)
    CboActivo.ListIndex = IIf(LvDetalle.SelectedItem.SubItems(11) = "SI", 0, 1)
        
    
End Sub
Private Sub CargaActivos()
    Dim Lineas() As String
    Dim Bp_check As Boolean
    Dim Sp_Anito As String
    If paso = "voucher" Then
        Bp_check = True
      ' Le pasa el textobx (el dato ), y el caracter delimitador
        Lineas = Split(SG_codigo, ",")
        Sp_Anito = Sm_AnoPedido
          
    

    
        
        
    Else
        If Sp_extra = "MENU" Then
    
            Bp_check = False
        Else
            Bp_check = True
        End If
        Sp_Anito = ComAno.Text
    End If
    Sql = "SELECT aim_id,pla_nombre,aim_nombre,aim_fecha_ingreso,aim_fecha_egreso,aim_vida_util,aim_tipo_depreciacion,aim_nuevo," & _
            "/*aim_valor*/ " & _
            "/*(SELECT SUM(vod_debe)-SUM(vod_haber) " & _
                "FROM con_vouchers_detalle d " & _
                "JOIN con_vouchers v ON d.vou_id=v.vou_id " & _
                "WHERE d.aim_id=a.aim_id AND v.rut_emp='" & SP_Rut_Activo & "' AND v.vou_ano_contable=" & Sp_Anito & ") */ " & _
                "IF((SELECT COUNT(r.rea_id) " & _
                    "FROM con_vouchers_detalle d " & _
                    "JOIN con_relacion_activos r ON d.pla_id = r.pla_id  AND d.vou_id = r.vou_id AND d.vod_id = r.vod_id " & _
                    "JOIN con_vouchers v ON d.vou_id = v.vou_id " & _
                    "WHERE r.aim_id=a.aim_id AND v.rut_emp = '" & SP_Rut_Activo & "')=0,aim_valor, " & _
                    "(SELECT SUM(IF(d.vod_debe>0,r.aim_valor,0)) - SUM(IF(d.vod_haber>0,r.aim_valor,0)) " & _
                    "FROM con_vouchers_detalle d " & _
                    "JOIN con_relacion_activos r ON d.pla_id = r.pla_id  AND d.vou_id = r.vou_id AND d.vod_id = r.vod_id " & _
                    "JOIN con_vouchers v ON d.vou_id = v.vou_id " & _
                    "WHERE r.aim_id=a.aim_id AND v.rut_emp = '" & SP_Rut_Activo & "'  AND v.vou_ano_contable =" & Sp_Anito & ")) valor " & _
            ",aim_dacumu,aim_fecha_adquisicion,aim_habilitado,a.pla_id " & _
            "FROM con_activo_inmovilizado a " & _
            "JOIN con_plan_de_cuentas p ON a.pla_id=p.pla_id " & _
            "WHERE rut_emp='" & SP_Rut_Activo & "' " & Sm_filtro & Sm_FiltroCta & Sm_Filtro_Activo
            
            
            
            
    Consulta RsTmp, Sql
    
    LLenar_Grilla RsTmp, Me, LvDetalle, Bp_check, True, True, False
    
    
    txtTotal = NumFormat(TotalizaColumna(LvDetalle, "valor"))
    TxtTotal2 = NumFormat(TotalizaColumna(LvDetalle, "Dep.Acumul."))
    
    If paso = "voucher" Then
        
            For X = 1 To LvDetalle.ListItems.Count
                For i = LBound(Lineas) To UBound(Lineas)
                    If Val(LvDetalle.ListItems(X)) = Val(Lineas(i)) Then LvDetalle.ListItems(X).Checked = True
                    
                
                Next i
            Next X
            CalcularChekados
    End If
End Sub

Private Sub LvDetalle_ItemCheck(ByVal Item As MSComctlLib.ListItem)
    CalcularChekados
End Sub
Private Sub CalcularChekados()
    totalseleccionado = 0
    For i = 1 To LvDetalle.ListItems.Count
        If LvDetalle.ListItems(i).Checked Then totalseleccionado = totalseleccionado + CDbl(LvDetalle.ListItems(i).SubItems(8))
    Next
    txtTotalSeleccionado = NumFormat(totalseleccionado)
    
End Sub

Private Sub LvHistoriaActivo_LostFocus()
    FraHistoria.Visible = False
End Sub

Private Sub Timer1_Timer()
    On Error Resume Next
    TxtNombre.SetFocus
End Sub

Private Sub TxtBuscaCuenta_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
    If KeyAscii = 39 Then KeyAscii = 0
End Sub


Private Sub TxtNeto_GotFocus()
    En_Foco TxtNeto
End Sub
Private Sub TxtDacumu_GotFocus()
    En_Foco TxtDacumu
End Sub

Private Sub TxtNeto_KeyPress(KeyAscii As Integer)
    KeyAscii = AceptaSoloNumeros(KeyAscii)
    If KeyAscii = 39 Then KeyAscii = 0
End Sub
Private Sub TxtDacumu_KeyPress(KeyAscii As Integer)
    KeyAscii = AceptaSoloNumeros(KeyAscii)
End Sub

Private Sub TxtNeto_Validate(Cancel As Boolean)
    If Val(TxtNeto) > 0 Then TxtNeto = NumFormat(TxtNeto) Else TxtNeto = "0"
End Sub
Private Sub TxtDacumu_Validate(Cancel As Boolean)
    If Val(TxtDacumu) > 0 Then TxtDacumu = NumFormat(TxtDacumu) Else TxtDacumu = "0"
End Sub

Private Sub TxtNombre_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
    If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtNroCuenta_GotFocus()
    En_Foco TxtNroCuenta
End Sub
Private Sub TxtNroCuenta_KeyPress(KeyAscii As Integer)
    KeyAscii = AceptaSoloNumeros(KeyAscii)
End Sub
Private Sub TxtNroCuenta_Validate(Cancel As Boolean)
    If Val(TxtNroCuenta) = 0 Then Exit Sub
    Busca_Id_Combo CboCuenta, Val(TxtNroCuenta)
    If CboCuenta.ListIndex = -1 Then
        MsgBox "Cuenta no existe..."
    End If
End Sub
Private Sub TxtVidaUtil_GotFocus()
    En_Foco TxtVidaUtil
End Sub

Private Sub TxtVidaUtil_KeyPress(KeyAscii As Integer)
    KeyAscii = AceptaSoloNumeros(KeyAscii)
    If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtVidaUtil_Validate(Cancel As Boolean)
    If Val(TxtVidaUtil) = 0 Then TxtVidaUtil = "0"
End Sub
