VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{DA729E34-689F-49EA-A856-B57046630B73}#1.0#0"; "progressbar-xp.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form Con_Centralizar 
   Caption         =   "Centralizacion"
   ClientHeight    =   9585
   ClientLeft      =   525
   ClientTop       =   1350
   ClientWidth     =   13050
   LinkTopic       =   "Form1"
   ScaleHeight     =   9585
   ScaleWidth      =   13050
   Begin MSComctlLib.ListView LvTemp 
      Height          =   2100
      Left            =   11475
      TabIndex        =   19
      Top             =   8925
      Visible         =   0   'False
      Width           =   6225
      _ExtentX        =   10980
      _ExtentY        =   3704
      View            =   3
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   0
      NumItems        =   4
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Object.Tag             =   "T500"
         Text            =   "Cod"
         Object.Width           =   882
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Object.Tag             =   "T3000"
         Text            =   "CUENTAS"
         Object.Width           =   4762
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   2
         Key             =   "debe"
         Object.Tag             =   "N100"
         Text            =   "DEBE"
         Object.Width           =   2381
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   3
         Key             =   "haber"
         Object.Tag             =   "N100"
         Text            =   "HABER"
         Object.Width           =   2381
      EndProperty
   End
   Begin VB.CommandButton cmdExportar 
      Caption         =   "F3 - Excel"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   450
      Left            =   4905
      TabIndex        =   18
      ToolTipText     =   "Exportar"
      Top             =   8460
      Width           =   1620
   End
   Begin VB.Frame FraProgreso 
      Caption         =   "Progreso exportaci�n"
      Height          =   795
      Left            =   5970
      TabIndex        =   16
      Top             =   7215
      Visible         =   0   'False
      Width           =   4170
      Begin Proyecto2.XP_ProgressBar BarraProgreso 
         Height          =   330
         Left            =   150
         TabIndex        =   17
         Top             =   315
         Width           =   3840
         _ExtentX        =   6773
         _ExtentY        =   582
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BrushStyle      =   0
         Color           =   16777088
         Scrolling       =   1
         ShowText        =   -1  'True
      End
   End
   Begin MSComctlLib.ListView LvAsiento 
      Height          =   2025
      Left            =   4905
      TabIndex        =   10
      Top             =   6390
      Width           =   6225
      _ExtentX        =   10980
      _ExtentY        =   3572
      View            =   3
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   0
      NumItems        =   5
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Object.Tag             =   "T500"
         Text            =   "Cod"
         Object.Width           =   882
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Object.Tag             =   "T3000"
         Text            =   "CUENTAS"
         Object.Width           =   4762
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   2
         Key             =   "debe"
         Object.Tag             =   "N100"
         Text            =   "DEBE"
         Object.Width           =   2381
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   3
         Key             =   "haber"
         Object.Tag             =   "N100"
         Text            =   "HABER"
         Object.Width           =   2381
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Object.Tag             =   "N109"
         Text            =   "vod_id"
         Object.Width           =   2540
      EndProperty
   End
   Begin VB.Frame Frame2 
      Caption         =   "Seleccione Periodo a Centralizar"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   975
      Left            =   600
      TabIndex        =   5
      Top             =   75
      Width           =   12000
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel6 
         Height          =   510
         Left            =   6915
         OleObjectBlob   =   "Con_Centralizar.frx":0000
         TabIndex        =   22
         Top             =   345
         Width           =   4725
      End
      Begin VB.ComboBox comMes 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         ItemData        =   "Con_Centralizar.frx":0154
         Left            =   1725
         List            =   "Con_Centralizar.frx":0156
         Style           =   2  'Dropdown List
         TabIndex        =   7
         Top             =   435
         Width           =   2085
      End
      Begin VB.ComboBox ComAno 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   5355
         Style           =   2  'Dropdown List
         TabIndex        =   6
         Top             =   435
         Width           =   1215
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
         Height          =   255
         Left            =   1350
         OleObjectBlob   =   "Con_Centralizar.frx":0158
         TabIndex        =   8
         Top             =   495
         Width           =   375
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel5 
         Height          =   255
         Left            =   4890
         OleObjectBlob   =   "Con_Centralizar.frx":01BC
         TabIndex        =   9
         Top             =   495
         Width           =   375
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Seleccione Asientos Contables a centralizar"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   7980
      Left            =   660
      TabIndex        =   1
      Top             =   1065
      Width           =   11970
      Begin VB.Frame Frame3 
         Caption         =   "Ayuda"
         Height          =   2490
         Left            =   105
         TabIndex        =   23
         Top             =   5280
         Width           =   3720
         Begin VB.TextBox txtAyuda 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFC0&
            ForeColor       =   &H00FF0000&
            Height          =   2115
            Left            =   135
            Locked          =   -1  'True
            MultiLine       =   -1  'True
            TabIndex        =   24
            TabStop         =   0   'False
            Top             =   285
            Width           =   3495
         End
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   210
         Left            =   8670
         OleObjectBlob   =   "Con_Centralizar.frx":0220
         TabIndex        =   21
         Top             =   150
         Visible         =   0   'False
         Width           =   1800
      End
      Begin VB.ComboBox CboNiif 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         ItemData        =   "Con_Centralizar.frx":02A8
         Left            =   8655
         List            =   "Con_Centralizar.frx":02AA
         Style           =   2  'Dropdown List
         TabIndex        =   20
         ToolTipText     =   "Seleccione cuenta"
         Top             =   630
         Visible         =   0   'False
         Width           =   1935
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   195
         Left            =   5850
         OleObjectBlob   =   "Con_Centralizar.frx":02AC
         TabIndex        =   12
         Top             =   4320
         Width           =   4920
      End
      Begin VB.Frame FrmAsiento 
         Caption         =   "Vista Asiento"
         Height          =   3285
         Left            =   4005
         TabIndex        =   11
         Top             =   4620
         Width           =   6570
         Begin VB.ComboBox CboCuentas 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            ItemData        =   "Con_Centralizar.frx":0344
            Left            =   3300
            List            =   "Con_Centralizar.frx":0346
            Style           =   2  'Dropdown List
            TabIndex        =   14
            ToolTipText     =   "Seleccione cuenta"
            Top             =   300
            Width           =   3090
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkCuenta 
            Height          =   195
            Left            =   375
            OleObjectBlob   =   "Con_Centralizar.frx":0348
            TabIndex        =   15
            Top             =   345
            Width           =   2850
         End
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   210
         Left            =   300
         TabIndex        =   4
         Top             =   420
         Width           =   195
      End
      Begin VB.CommandButton CmdCentralizar 
         Caption         =   "&Comenzar Centralizacion"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   465
         Left            =   270
         TabIndex        =   3
         Tag             =   "Centraliza Asientos marcados"
         Top             =   4710
         Width           =   2310
      End
      Begin MSComctlLib.ListView LvDetalle 
         Height          =   3840
         Left            =   240
         TabIndex        =   2
         Top             =   420
         Width           =   11400
         _ExtentX        =   20108
         _ExtentY        =   6773
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   7
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "N109"
            Object.Width           =   529
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "T4000"
            Text            =   "Nombre Asiento"
            Object.Width           =   7056
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "T4000"
            Text            =   "Glosa sugerida (doble click para cambiar)"
            Object.Width           =   7056
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Object.Tag             =   "T1000"
            Text            =   "Contab.-Trib. y � Niif"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Object.Tag             =   "N109"
            Text            =   "Id Nif"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Object.Tag             =   "T1000"
            Text            =   "Centralizado"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   6
            Object.Tag             =   "T1000"
            Text            =   "Ayuda"
            Object.Width           =   0
         EndProperty
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   195
         Left            =   345
         OleObjectBlob   =   "Con_Centralizar.frx":03B8
         TabIndex        =   13
         Top             =   4320
         Width           =   3690
      End
   End
   Begin VB.CommandButton CmdSalir 
      Caption         =   "&Retornar"
      Height          =   465
      Left            =   10965
      TabIndex        =   0
      Top             =   9105
      Width           =   1620
   End
   Begin VB.Timer Timer1 
      Interval        =   10
      Left            =   495
      Top             =   45
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   0
      OleObjectBlob   =   "Con_Centralizar.frx":0456
      Top             =   0
   End
End
Attribute VB_Name = "Con_Centralizar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public Sp_glosa As String



Private Sub CboCuentas_Click()
    If CboCuentas.ListIndex > -1 Then ProcederAsiento LvDetalle.SelectedItem, CboCuentas.ItemData(CboCuentas.ListIndex)
End Sub



Private Sub CboNiif_Click()
 '   If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    'LvDetalle.ListItems(BuscaLinea(CboNiif.Tag)).SubItems(3) = CboNiif.Text
    'LvDetalle.ListItems(BuscaLinea(CboNiif.Tag)).SubItems(4) = CboNiif.ListIndex + 1
    'CboNiif.Tag = 0
    'CboNiif.Visible = False
End Sub

Private Sub CboNiif_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        LvDetalle.ListItems(BuscaLinea(CboNiif.Tag)).SubItems(3) = CboNiif.Text
        LvDetalle.ListItems(BuscaLinea(CboNiif.Tag)).SubItems(4) = CboNiif.ListIndex + 1
        CboNiif.Tag = 0
        CboNiif.Visible = False
        
    ElseIf KeyAscii = 27 Then
        LvDetalle.ListItems(BuscaLinea(CboNiif.Tag)).SubItems(3) = ""
        LvDetalle.ListItems(BuscaLinea(CboNiif.Tag)).SubItems(4) = ""
        CboNiif.Tag = 0
        CboNiif.Visible = False
    End If
    
    
        


End Sub
Private Function BuscaLinea(IdA As Integer) As Integer
    For Y = 1 To LvDetalle.ListItems.Count
        If Val(LvDetalle.ListItems(Y)) = IdA Then
            BuscaLinea = Y
            Exit For
        End If
    Next
End Function

Private Sub CboNiif_LostFocus()
    If Val(CboNiif.Tag) > 0 Then
        CboNiif.Tag = 0
        CboNiif.Visible = False
    End If
End Sub

Private Sub Check1_Click()
    For i = 1 To LvDetalle.ListItems.Count
        If LvDetalle.ListItems(i).Checked Then
            LvDetalle.ListItems(i).Checked = False
        Else
            LvDetalle.ListItems(i).Checked = True
        End If
    Next
End Sub

Private Sub CmdCentralizar_Click()
    Dim Bp_Hace As Boolean
    If MsgBox("�Va a comenzar la centralizacion?, continuar ?", vbQuestion + vbYesNo) = vbNo Then Exit Sub
    
    'Comenzamos la centralizacion
    Bp_Hace = False
      
    For i = 1 To LvDetalle.ListItems.Count
        
        If LvDetalle.ListItems(i).Checked Then
            'Consultar si el asiento ya fue
            'centralizado
            
            
            If ConsultaCentralizado(LvDetalle.ListItems(i), comMes.ItemData(comMes.ListIndex), ComAno.Text) Then
                MsgBox "ASIENTO DE " & LvDetalle.ListItems(i).SubItems(1) & vbNewLine & " YA ESTA CENTRALIZADO PARA ESTE PERIODO...", vbInformation
            ElseIf Val(LvDetalle.ListItems(i).SubItems(4)) = 0 Then
                MsgBox "Seleccion Niif para el asiento " & vbNewLine & LvDetalle.ListItems(i).SubItems(1)
            
            Else
                
                
                Sp_glosa = LvDetalle.ListItems(i).SubItems(2) & " " & comMes.Text
                If LvDetalle.ListItems(i) = 7 Or LvDetalle.ListItems(i) = 8 Then
                    'En los asientos de bancos debemos repetir el proceso por cada cuenta
                    'bancaria
                    Sql = "SELECT cte_id " & _
                            "FROM ban_cta_cte c JOIN con_plan_de_cuentas ON pla_id=ban_id " & _
                            "WHERE cte_activo='SI' AND rut_emp='" & SP_Rut_Activo & "'"
                    Consulta RsTmp, Sql
                    If RsTmp.RecordCount > 0 Then
                        RsTmp.MoveFirst
                        Do While Not RsTmp.EOF
                            ProcederAsiento LvDetalle.ListItems(i), RsTmp!cte_id
                            Hace LvDetalle.ListItems(i), Sp_glosa
                            RsTmp.MoveNext
                        Loop
                        Bp_Hace = False
                    End If
                Else
                    ProcederAsiento LvDetalle.ListItems(i)
                    Bp_Hace = True
                End If
                
                FrmAsiento.Caption = LvDetalle.ListItems(i).SubItems(1)
                
                'Creacion de vouchers
                If Bp_Hace Then Hace LvDetalle.ListItems(i), Sp_glosa
            End If
        End If
    
    Next
    MarcaCentralizados
    MsgBox "Centralizacion de los asientos marcados ha finalizado correctamente..."
    LvAsiento.ListItems.Clear
End Sub
Private Sub Hace(Identifica As Integer, Glosa As String)
    Dim Ip_TipoC As Integer
    Select Case Identifica
            Case 1, 10, 7, 4, 6 'Identificacion del tipo de voucher
                Ip_TipoC = 2 'Egres
            Case 11, 2, 8, 9, 5
                Ip_TipoC = 1 'Ingreso
            Case 12, 13
                Ip_TipoC = 3 'Traspaso
    End Select
    If LvAsiento.ListItems.Count > 1 Then
        
        CreaVoucher Ip_TipoC, Glosa & " " & comMes.Text, Identifica
    End If
End Sub


Private Sub CreaVoucher(Tipo As Integer, Glosa As String, IdAsiento As Integer)
    Dim Lp_Vou As Long, Lp_Nro_Voucher As Long, Sp_TipoV As Integer, Ip_dia As Integer, Lp_IdCentralizacion As Long
    Dim Lp_Vod As Long
    
    If CDbl(LvAsiento.ListItems(LvAsiento.ListItems.Count).SubItems(2)) <> CDbl(LvAsiento.ListItems(LvAsiento.ListItems.Count).SubItems(3)) Then
        
        MsgBox "Asiento descuadrado, corriga origen..." & vbNewLine & Glosa
        
        Exit Sub
    End If
    
    If CDbl(LvAsiento.ListItems(LvAsiento.ListItems.Count).SubItems(2)) + CDbl(LvAsiento.ListItems(LvAsiento.ListItems.Count).SubItems(3)) = 0 Then
        'asiento en 0
        Lp_IdCentralizacion = UltimoNro("con_centralizaciones", "ctl_id")
        Sql = "INSERT INTO con_centralizaciones (ctl_id,asi_id,ctl_mes,ctl_ano,rut_emp,ctl_usuario,ctl_fecha) " & _
              "VALUES(" & Lp_IdCentralizacion & "," & IdAsiento & "," & comMes.ItemData(comMes.ListIndex) & "," & ComAno.Text & ",'" & SP_Rut_Activo & "','" & LogUsuario & "','" & Fql(Date) & "')"
        cn.Execute Sql
        Exit Sub
    End If
    
    
    On Error GoTo Grabando
    Ip_dia = Mid(UltimoDiaMes("01-" & comMes.ItemData(comMes.ListIndex) & "-" & ComAno.Text), 1, 2)
    cn.BeginTrans
        
        Lp_Vou = UltimoNro("con_vouchers", "vou_id")
        Lp_Nro_Voucher = UltimoMayor("con_vouchers", "vou_numero", SP_Rut_Activo)
        Sql = "INSERT INTO con_vouchers (vou_id,vou_numero,vou_dia,vou_mes_contable,vou_ano_contable,vou_fecha_ingreso,vou_glosa,rut_emp,vou_tipo,vou_centralizacion,vou_id_asiento,vou_tiponiif) " & _
            "VALUES(" & Lp_Vou & "," & Lp_Nro_Voucher & "," & Ip_dia & "," & comMes.ItemData(comMes.ListIndex) & "," & ComAno.Text & ",'" & Fql(Date) & "','" & Glosa & "','" & SP_Rut_Activo & "'," & Tipo & ",'SI'," & IdAsiento & "," & LvDetalle.ListItems(BuscaLinea(IdAsiento)).SubItems(4) & ")"
        cn.Execute Sql
        
        
        For i = 1 To LvAsiento.ListItems.Count - 1
            Sql = "INSERT INTO con_vouchers_detalle (vod_id,vou_id,pla_id,vod_debe,vod_haber) VALUES "
        
            Lp_Vod = UltimoNro("con_vouchers_detalle", "vod_id")
        
            Sql = Sql & "(" & Lp_Vod & "," & Lp_Vou & "," & LvAsiento.ListItems(i) & "," & CDbl(LvAsiento.ListItems(i).SubItems(2)) & "," & CDbl(LvAsiento.ListItems(i).SubItems(3)) & ")"
            
            cn.Execute Sql
            Sql = ""
            LvAsiento.ListItems(i).SubItems(4) = Lp_Vod
            If IdAsiento = 1 Or IdAsiento = 11 Then
                cn.Execute "UPDATE com_doc_compra v " & _
                            "INNER JOIN sis_documentos x ON v.doc_id = x.doc_id " & _
                            "INNER JOIN com_doc_compra_detalle d ON v.id=d.id " & _
                            "SET vod_id = " & Lp_Vod & " " & _
                            "WHERE d.pla_id=" & LvAsiento.ListItems(i) & " AND com_informa_compra= 'SI' " & _
                            "AND rut <> 'NULO' " & _
                            "AND v.rut_emp = '" & SP_Rut_Activo & "' " & _
                            "AND(doc_contable = 'SI') " & _
                            "AND doc_nota_de_credito = 'NO' " & _
                            "AND mes_contable=" & comMes.ItemData(comMes.ListIndex) & " " & _
                            "AND ano_contable=" & ComAno.Text
                            
                            
                '29 Marzo 2014
                'Creamos relacion de activo con el voucher
                'Verificamos si corresponde a AI
                Sql = "SELECT pla_id " & _
                        "FROM con_plan_de_cuentas " & _
                        "WHERE tpo_id=1 AND det_id=4 AND pla_analisis='SI' AND pla_id=" & LvAsiento.ListItems(i)
                Consulta RsTmp, Sql
                If RsTmp.RecordCount > 0 Then 'buscamos los activos comprados
                    'Sql = "SELECT d.pla_id,pla_nombre,SUM(d.cmd_total_neto + d.cmd_exento)debe,d.pro_codigo "
                    Sql = "SELECT d.pla_id,pla_nombre,d.cmd_total_neto + d.cmd_exento debe,d.pro_codigo " & _
                            "FROM    com_doc_compra_detalle d " & _
                            "INNER JOIN com_doc_compra c ON d.id = c.id " & _
                            "INNER JOIN con_plan_de_cuentas p ON d.pla_id = p.pla_id " & _
                            "INNER JOIN sis_documentos x ON c.doc_id = x.doc_id " & _
                            "WHERE   com_informa_compra = 'SI' " & _
                            "AND c.rut_emp = '" & SP_Rut_Activo & "' " & _
                            "AND tpo_id=1 AND det_id=4 " & _
                            "AND((doc_contable = 'SI' AND doc_nota_de_credito = 'NO' AND mes_contable =" & comMes.ItemData(comMes.ListIndex) & " AND ano_contable =" & ComAno.Text & ") " & _
                                "OR(doc_id_factura > 0 AND( " & _
                                        "SELECT mes_contable " & _
                                        "FROM com_doc_compra p " & _
                                        "JOIN sis_documentos k " & _
                                        "WHERE doc_nota_de_credito = 'NO' AND p.no_documento = c.nro_factura AND p.doc_id = c.doc_id_factura " & _
                                        "LIMIT 1)=" & comMes.ItemData(comMes.ListIndex) & " " & _
                                    "AND(SELECT ano_contable " & _
                                        "FROM com_doc_compra p " & _
                                        "JOIN sis_documentos k USING(doc_id) " & _
                                        "WHERE doc_nota_de_credito = 'NO' AND p.no_documento = c.nro_factura AND p.doc_id = c.doc_id_factura " & _
                                        "LIMIT 1)= " & ComAno.Text & ")) " & _
                            "/*GROUP BY    d.pla_id */"
                    Consulta RsTmp3, Sql
                    If RsTmp3.RecordCount > 0 Then
                        With RsTmp3
                            .MoveFirst
                            Do While Not .EOF
                                cn.Execute "INSERT INTO con_relacion_activos (pla_id,vou_id,vod_id,aim_id,aim_valor) " & _
                                 "VALUES (" & !pla_id & "," & Lp_Vou & "," & Lp_Vod & "," & !pro_codigo & "," & !debe & ")"
                                .MoveNext
                            Loop
                        End With
                    End If
                End If
            End If
        Next
        'Sql = Mid(Sql, 1, Len(Sql) - 1)
        'cn.Execute Sql
        
        
        'Aqui agregamos el voucher
        Lp_IdCentralizacion = UltimoNro("con_centralizaciones", "ctl_id")
        Sql = "INSERT INTO con_centralizaciones (ctl_id,asi_id,ctl_mes,ctl_ano,rut_emp,ctl_usuario,ctl_fecha) " & _
              "VALUES(" & Lp_IdCentralizacion & "," & IdAsiento & "," & comMes.ItemData(comMes.ListIndex) & "," & ComAno.Text & ",'" & SP_Rut_Activo & "','" & LogUsuario & "','" & Fql(Date) & "')"
        cn.Execute Sql
        
        Select Case IdAsiento
            Case 2, 10
                cn.Execute "UPDATE ven_doc_venta v " & _
                    "INNER JOIN sis_documentos x ON v.doc_id = x.doc_id " & _
                    "SET ctl_id = " & Lp_IdCentralizacion & " " & _
                    "WHERE   ven_informa_venta = 'SI' " & _
                    "AND rut_cliente <> 'NULO' " & _
                    "AND v.rut_emp = '" & SP_Rut_Activo & "' " & _
                    "AND(doc_contable = 'SI') " & _
                    "AND doc_nota_de_credito = 'NO' " & _
                    "AND MONTH(v.fecha)=" & comMes.ItemData(comMes.ListIndex) & " " & _
                    "AND YEAR(v.fecha)=" & ComAno.Text
            Case 1, 11
                'ACTUALIZAMOS ID EN COMPRA
                 cn.Execute "UPDATE com_doc_compra v " & _
                    "INNER JOIN sis_documentos x ON v.doc_id = x.doc_id " & _
                    "SET ctl_id = " & Lp_IdCentralizacion & " " & _
                    "WHERE   com_informa_compra= 'SI' " & _
                    "AND rut <> 'NULO' " & _
                    "AND v.rut_emp = '" & SP_Rut_Activo & "' " & _
                    "AND(doc_contable = 'SI') " & _
                    "AND doc_nota_de_credito = 'NO' " & _
                    "AND mes_contable=" & comMes.ItemData(comMes.ListIndex) & " " & _
                    "AND ano_contable=" & ComAno.Text
                
                
                
        End Select
        
        
        
    cn.CommitTrans
    
    
    Exit Sub
Grabando:    MsgBox "Ocurrio un error al intentar grabar..." & vbNewLine & Err.Number & " " & Err.Description, vbInformation
    cn.RollbackTrans
    

End Sub



Private Sub ProcederAsiento(Id_Asiento As Integer, Optional ByVal Id_CtaCte As Integer)
    Dim Sp_Fechas As String, Rp_A As Recordset
    LvAsiento.ListItems.Clear
    Sp_Fechas = " AND MONTH(a.abo_fecha_pago)= " & Me.comMes.ItemData(comMes.ListIndex) & " AND YEAR(a.abo_fecha_pago)=" & Me.ComAno.Text & " "
    
    
    'Sp_Fecha = " AND MONTH(a.abo_fecha_pago)=" & IG_Mes_Contable & " AND YEAR(a.abo_fecha_pago)=" & IG_Ano_Contable

    Select Case Id_Asiento
        Case 1, 11, 18
            Sp_Fechas = " AND mes_contable=" & comMes.ItemData(comMes.ListIndex) & " AND ano_contable=" & ComAno.Text & " "
            
        Case 2
            Sp_Fechas = " AND MONTH(v.fecha)= " & Me.comMes.ItemData(comMes.ListIndex) & " AND YEAR(v.fecha)=" & Me.ComAno.Text & " "
            
        Case 4
            Sp_Fechas = " AND mes_contable=" & comMes.ItemData(comMes.ListIndex) & " AND ano_contable=" & ComAno.Text & " "
    
        
        Case 5, 6, 12, 13
        
        
        Case 7, 8 'Bancos
                Sp_Fechas = " AND MONTH(m.mov_fecha)= " & comMes.ItemData(comMes.ListIndex) & " AND YEAR(m.mov_fecha)=" & ComAno.Text & " AND m.cte_id=" & Id_CtaCte & " "

      
            
        Case 9
        
        Case 10
            Sp_Fechas = " AND MONTH(v.fecha)= " & Me.comMes.ItemData(comMes.ListIndex) & " AND YEAR(v.fecha)=" & Me.ComAno.Text & " "
        
            
            
    
    End Select
    
    Set Rp_A = AsientoContable(Sp_Fechas, Id_Asiento, comMes.ItemData(comMes.ListIndex), ComAno.Text)
    
    If Id_Asiento = 11 Or Id_Asiento = 10 Then
        LLenar_Grilla Rp_A, Me, LvTemp, False, True, True, False
        If LvTemp.ListItems.Count > 0 Then
            For i = LvTemp.ListItems.Count To 1 Step -1
                LvAsiento.ListItems.Add , , LvTemp.ListItems(i)
                LvAsiento.ListItems(LvAsiento.ListItems.Count).SubItems(1) = LvTemp.ListItems(i).SubItems(1)
                LvAsiento.ListItems(LvAsiento.ListItems.Count).SubItems(2) = LvTemp.ListItems(i).SubItems(3)
                LvAsiento.ListItems(LvAsiento.ListItems.Count).SubItems(3) = LvTemp.ListItems(i).SubItems(2)
            Next
        End If
    Else
        LLenar_Grilla Rp_A, Me, LvAsiento, False, True, True, False
    End If
    TotalAsiento LvAsiento
End Sub
Private Sub CmdExportar_Click()
    Dim tit(2) As String
    If LvAsiento.ListItems.Count = 0 Then Exit Sub
    FraProgreso.Visible = True
    tit(0) = Me.Caption & " " & DtFecha & " - " & Time
    tit(1) = Principal.SkEmpresa
    tit(2) = "RUT:" & SP_Rut_Activo
    ExportarNuevo LvAsiento, tit, Me, BarraProgreso
    FraProgreso.Visible = False
End Sub



Private Sub cmdSalir_Click()
    On Error Resume Next
    Unload Me
End Sub


Private Sub Form_Load()
    Centrar Me
    Skin2 Me, , 7
    
    For i = 1 To 12
        comMes.AddItem UCase(MonthName(i, False))
        comMes.ItemData(comMes.ListCount - 1) = i
    Next
    Busca_Id_Combo comMes, Val(IG_Mes_Contable)
    LLenaYears ComAno, 2010
   ' For i = Year(Date) To 2010 Step -1
   ' For i = Year(Date) To Year(Date) - 1 Step -1
   '     ComAno.AddItem i
   '     ComAno.ItemData(ComAno.ListCount - 1) = i
   ' Next
   ' Busca_Id_Combo ComAno, Val(IG_Ano_Contable)
    
    CboNiif.AddItem "TRIBUTARIA-N.I.I.F"
    CboNiif.AddItem "TRIBUTARIA"
    CboNiif.AddItem "N.I.I.F."
    CboNiif.ItemData(0) = 1
    CboNiif.ItemData(1) = 2
    CboNiif.ItemData(2) = 3
    CboNiif.ListIndex = 0
    MarcaCentralizados
End Sub

Private Sub MarcaCentralizados()
    Sql = "SELECT asi_id,asi_nombre,asi_glosa_defecto,'','1', " & _
            "(SELECT IF(ctl_id>0,'SI','NO') " & _
                        " FROM con_centralizaciones  c " & _
                        "WHERE ctl_mes = " & comMes.ItemData(comMes.ListIndex) & " " & _
                        "AND ctl_ano=" & ComAno.Text & " AND rut_emp='" & SP_Rut_Activo & "' AND a.asi_id=c.asi_id) " & _
                        ",asi_ayuda " & _
            "FROM con_asientos_centralizar a " & _
            "WHERE asi_activo='SI' " & _
            "ORDER BY asi_orden"
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvDetalle, True, True, True, False
End Sub



Private Sub LvDetalle_Click()
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    LvAsiento.ListItems.Clear
    txtAyuda = ""
    txtAyuda = LvDetalle.SelectedItem.SubItems(6)
'    If Me.LvDetalle.SelectedItem = 1 Then
'        txtAyuda = "CENTRALIZA LAS COMPRAS DIGITADAS EN EL  PERIODO CONTABLE SELECCIONADO..."
'
'    ElseIf LvDetalle.SelectedItem = 11 Then
'        txtAyuda = "CENTRALIZA LAS NOTAS DE CREDITO DE PROVEEDORES INGRESADAS EN PERIODO CONTABLE SELECCIONADO..."
'
'    ElseIf LvDetalle.SelectedItem = 2 Then
'         txtAyuda = "CENTRALIZA LAS VENTAS REALIZADAS EN EL PERIODO CONTABLE SELECCIONADO..."
'
'    ElseIf LvDetalle.SelectedItem = 10 Then
'        txtAyuda = "CENTRALIZA LAS NOTAS DE CREDITO DE CLIENTES INGRESADAS EN PERIODO CONTABLE SELECCIONADO..."
'
'    ElseIf LvDetalle.SelectedItem = 4 Then
'        txtAyuda = "CENTRALIZA LOS GASTOS CON BOLETAS REALIZADAS EN EL PERIODO CONTABLE SELECCIONADO..."
'    ElseIf LvDetalle.SelectedItem = 5 Then
'        txtAyuda = "CENTRALIZA LOS PAGOS EN EFECTIVO REALIZADOS POR LOS CLIENTES EN EL PERIODO CONTABLE SELECCIONADO..."
'    ElseIf LvDetalle.SelectedItem = 6 Then
'        txtAyuda = "CENTRALIZA LOS PAGOS EN EFECTIVO REALIZADOS A LOS PROVEEDORES EN EL PERIODO CONTABLE SELECCIONADO..."
'    ElseIf LvDetalle.SelectedItem = 7 Then
'        txtAyuda = "CENTRALIZA LOS GIROS Y CARGOS BANCARIOS DEL PERIODO CONTABLE SELECCIONADO..."
'    ElseIf LvDetalle.SelectedItem = 8 Then
'        txtAyuda = "CENTRALIZA LOS DEPOSITOS Y TRANSFERENCIAS BANCARIAS DEL PERIODO CONTABLE SELECCIONADO..."
'    ElseIf LvDetalle.SelectedItem = 9 Then
'        txtAyuda = "CENTRALIZA EL SALDO DE CAJA SOBRANTE POR CONCEPTO DE FONDOS POR RENDIR DEL PERIODO CONTABLE SELECCIONADO. SE ORIGINA EN OPCION DE BANCOS Y BOTON ESTADO DE CHEQUES POR RENDIR..."
'    End If
    
   
    
   
    
    SkCuenta.Visible = False
    CboCuentas.Visible = False
    If LvDetalle.SelectedItem = 7 Or LvDetalle.SelectedItem = 8 Then
        LLenarCombo CboCuentas, "CONCAT(cte_numero,' ',pla_nombre)", "cte_id", "ban_cta_cte c JOIN con_plan_de_cuentas ON pla_id=ban_id", "cte_activo='SI' AND rut_emp='" & SP_Rut_Activo & "'"
        If CboCuentas.ListCount > 0 Then
            CboCuentas.Visible = True
            SkCuenta.Visible = True
            LvDetalle.SetFocus
        End If
    End If
    
    If CboCuentas.Visible Then
    
    Else
        ProcederAsiento LvDetalle.SelectedItem
    End If
End Sub

Private Sub LvDetalle_DblClick()
    Dim Sp_glosa As String
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    Sp_glosa = InputBox("Ingrese Glosa...", "Glosa Asiento", LvDetalle.SelectedItem.SubItems(2))
    If Len(Sp_glosa) > 0 Then
        LvDetalle.SelectedItem.SubItems(2) = UCase(Sp_glosa)
    End If
    
End Sub

Private Sub LvDetalle_KeyDown(KeyCode As Integer, Shift As Integer)
    Exit Sub
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    If KeyCode = 13 Then
            CboNiif.Visible = True
           ' TxtTemp = LvDetalle.SelectedItem.SubItems(2)
           ' TxtTemp = CDbl(TxtAbonos) - CDbl(Me.TxtSumaPagos)
            CboNiif.Tag = LvDetalle.SelectedItem
            CboNiif.Top = LvDetalle.Top + LvDetalle.SelectedItem.Top
            CboNiif.SetFocus
    End If
        
End Sub


