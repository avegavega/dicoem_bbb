VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Begin VB.Form AgregoProveedor 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Ingresa Nuevo Proveedor"
   ClientHeight    =   6510
   ClientLeft      =   4485
   ClientTop       =   795
   ClientWidth     =   8730
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6510
   ScaleWidth      =   8730
   Begin VB.ComboBox CboBanco 
      Appearance      =   0  'Flat
      Height          =   315
      ItemData        =   "AgregoProveedor.frx":0000
      Left            =   1680
      List            =   "AgregoProveedor.frx":000A
      Style           =   2  'Dropdown List
      TabIndex        =   27
      Top             =   3060
      Width           =   3375
   End
   Begin VB.TextBox txtCtaCte 
      Height          =   315
      Left            =   1680
      MaxLength       =   50
      TabIndex        =   25
      Top             =   3375
      Width           =   3375
   End
   Begin VB.Frame Frame1 
      Caption         =   "Solo para subdistribuidores de gas"
      Height          =   705
      Left            =   465
      TabIndex        =   22
      Top             =   4125
      Width           =   4890
      Begin VB.ComboBox CboGas 
         Appearance      =   0  'Flat
         Height          =   315
         ItemData        =   "AgregoProveedor.frx":0016
         Left            =   2685
         List            =   "AgregoProveedor.frx":0020
         Style           =   2  'Dropdown List
         TabIndex        =   24
         Top             =   315
         Width           =   915
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel9 
         Height          =   315
         Left            =   375
         OleObjectBlob   =   "AgregoProveedor.frx":002C
         TabIndex        =   23
         Top             =   360
         Width           =   2265
      End
   End
   Begin VB.ComboBox CboActivo 
      Appearance      =   0  'Flat
      Height          =   315
      ItemData        =   "AgregoProveedor.frx":00CA
      Left            =   1680
      List            =   "AgregoProveedor.frx":00D4
      Style           =   2  'Dropdown List
      TabIndex        =   20
      Top             =   3705
      Width           =   915
   End
   Begin VB.Timer Timer2 
      Interval        =   40
      Left            =   7365
      Top             =   4590
   End
   Begin VB.PictureBox PicEdit 
      Height          =   3015
      Left            =   5400
      Picture         =   "AgregoProveedor.frx":00E0
      ScaleHeight     =   2955
      ScaleWidth      =   2955
      TabIndex        =   19
      Top             =   120
      Width           =   3015
   End
   Begin VB.PictureBox PicNew 
      AutoSize        =   -1  'True
      Height          =   3060
      Left            =   5400
      Picture         =   "AgregoProveedor.frx":26D7
      ScaleHeight     =   3000
      ScaleWidth      =   3045
      TabIndex        =   18
      Top             =   120
      Visible         =   0   'False
      Width           =   3105
   End
   Begin VB.Timer Timer1 
      Interval        =   100
      Left            =   7200
      Top             =   5235
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   7680
      OleObjectBlob   =   "AgregoProveedor.frx":4E5E
      Top             =   5235
   End
   Begin VB.CommandButton CmdSale 
      Cancel          =   -1  'True
      Caption         =   "Cancelar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   3915
      TabIndex        =   9
      Top             =   5235
      Width           =   1455
   End
   Begin VB.CommandButton CmdGuarda 
      Caption         =   "&Guardar"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   1575
      TabIndex        =   8
      ToolTipText     =   "Graba proveedor"
      Top             =   5235
      Width           =   2115
   End
   Begin VB.TextBox TxtMail 
      Height          =   315
      Left            =   1680
      MaxLength       =   50
      TabIndex        =   7
      Top             =   2730
      Width           =   3375
   End
   Begin VB.TextBox TxtFax 
      Height          =   315
      Left            =   1680
      MaxLength       =   15
      TabIndex        =   6
      Top             =   2430
      Width           =   3375
   End
   Begin VB.TextBox TxtFono 
      Height          =   315
      Left            =   1680
      MaxLength       =   15
      TabIndex        =   5
      Top             =   2115
      Width           =   3375
   End
   Begin VB.TextBox TxtCiudad 
      Height          =   315
      Left            =   1680
      MaxLength       =   25
      TabIndex        =   4
      Top             =   1800
      Width           =   3375
   End
   Begin VB.TextBox TxtComuna 
      Height          =   315
      Left            =   1680
      MaxLength       =   25
      TabIndex        =   3
      Top             =   1470
      Width           =   3375
   End
   Begin VB.TextBox TxtDirec 
      Height          =   315
      Left            =   1680
      MaxLength       =   50
      TabIndex        =   2
      Top             =   1140
      Width           =   3375
   End
   Begin VB.TextBox TxtRz 
      Height          =   315
      Left            =   1680
      MaxLength       =   50
      TabIndex        =   1
      Top             =   810
      Width           =   3375
   End
   Begin VB.TextBox TxtRut 
      Alignment       =   1  'Right Justify
      Height          =   315
      Left            =   1680
      TabIndex        =   0
      Text            =   " "
      Top             =   480
      Width           =   2055
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel8 
      Height          =   225
      Index           =   0
      Left            =   210
      OleObjectBlob   =   "AgregoProveedor.frx":5092
      TabIndex        =   17
      Top             =   2820
      Width           =   1395
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel7 
      Height          =   225
      Left            =   195
      OleObjectBlob   =   "AgregoProveedor.frx":50F1
      TabIndex        =   16
      Top             =   2490
      Width           =   1395
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel6 
      Height          =   270
      Left            =   195
      OleObjectBlob   =   "AgregoProveedor.frx":514E
      TabIndex        =   15
      Top             =   2190
      Width           =   1395
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel5 
      Height          =   255
      Left            =   195
      OleObjectBlob   =   "AgregoProveedor.frx":51AD
      TabIndex        =   14
      Top             =   1875
      Width           =   1395
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
      Height          =   255
      Left            =   195
      OleObjectBlob   =   "AgregoProveedor.frx":5210
      TabIndex        =   13
      Top             =   1545
      Width           =   1395
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
      Height          =   255
      Left            =   195
      OleObjectBlob   =   "AgregoProveedor.frx":5273
      TabIndex        =   12
      Top             =   1185
      Width           =   1395
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
      Height          =   255
      Left            =   195
      OleObjectBlob   =   "AgregoProveedor.frx":52DC
      TabIndex        =   11
      Top             =   855
      Width           =   1395
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
      Height          =   300
      Left            =   195
      OleObjectBlob   =   "AgregoProveedor.frx":534B
      TabIndex        =   10
      Top             =   525
      Width           =   1395
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel14 
      Height          =   375
      Left            =   -720
      OleObjectBlob   =   "AgregoProveedor.frx":53A8
      TabIndex        =   21
      Top             =   3750
      Width           =   2355
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel8 
      Height          =   225
      Index           =   1
      Left            =   195
      OleObjectBlob   =   "AgregoProveedor.frx":5413
      TabIndex        =   26
      Top             =   3435
      Width           =   1395
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel10 
      Height          =   375
      Left            =   -720
      OleObjectBlob   =   "AgregoProveedor.frx":547E
      TabIndex        =   28
      Top             =   3090
      Width           =   2355
   End
End
Attribute VB_Name = "AgregoProveedor"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim NoGuardar As Boolean
Dim Bm_Nuevo As Boolean
Private Sub CmdGuarda_Click()
        If CboBanco.ListIndex = -1 Then
            MsgBox "Seleccione banco...", vbInformation
            CboBanco.SetFocus
            Exit Sub
        End If

        Sql = "SELECT rut_proveedor " & _
              "FROM maestro_proveedores " & _
              "WHERE rut_proveedor='" & TxtRut & "'"
        Consulta RsTmp2, Sql
        If RsTmp2.RecordCount Then
            If Me.Caption = "Ingresa" Then
                MsgBox "Ya existe un proveedor con este RUT...", vbExclamation
                Me.TxtRut.SetFocus
                Exit Sub
            End If
        End If
        Filtro = "rut_proveedor = '" & Me.TxtRut.Text & "'"
        If Bm_Nuevo Then
            Sql = "INSERT INTO par_asociacion_ruts (rut_emp,rut_pro) " & _
                   "VALUES('" & SP_Rut_Activo & "','" & TxtRut & "')"
            Consulta RsTmp2, Sql
        
            Sql = "INSERT INTO maestro_proveedores (rut_proveedor,nombre_empresa,direccion,comuna,ciudad," & _
                                                    "fono,fax,email,habilitado,prv_distribuidor_de_gas,ban_id,prv_ctacte) VALUES ('" & _
                    TxtRut & "','" & TxtRz & "','" & TxtDirec & "','" & txtComuna & "','" & TxtCiudad & "','" & IIf(Len(Me.txtFono.Text) > 0, Me.txtFono.Text, "F") & "','" & _
                    IIf(Len(Me.TxtFax.Text) > 0, Me.TxtFax.Text, "S/Fax") & "','" & IIf(Len(Me.TxtMail.Text) > 0, Me.TxtMail.Text, "@") & "','" & CboActivo.Text & "','" & CboGas.Text & "'," & CboBanco.ItemData(CboBanco.ListIndex) & ",'" & txtCtaCte & "')"
        Else
            Sql = "UPDATE maestro_proveedores SET " & _
                        "nombre_empresa='" & TxtRz & "'," & _
                        "direccion='" & TxtDirec & "'," & _
                        "comuna='" & txtComuna & "'," & _
                        "ciudad='" & TxtCiudad & "'," & _
                        "fono='" & IIf(Len(Me.txtFono.Text) > 0, Me.txtFono.Text, "F") & "'," & _
                        "fax='" & IIf(Len(Me.TxtFax.Text) > 0, Me.TxtFax.Text, "S/Fax") & "'," & _
                        "email='" & IIf(Len(Me.TxtMail.Text) > 0, Me.TxtMail.Text, "@") & "', " & _
                        "habilitado='" & CboActivo.Text & "', " & _
                        "prv_distribuidor_de_gas='" & CboGas.Text & "', " & _
                        "ban_id=" & CboBanco.ItemData(CboBanco.ListIndex) & ", " & _
                        "prv_ctacte ='" & txtCtaCte & "' " & _
                    "WHERE rut_proveedor = '" & TxtRut & "'"
        End If
        cn.Execute Sql
        rutProveedor = True
        SG_codigo = TxtRut
        Unload Me
End Sub

Private Sub CmdSale_Click()
    SG_codigo = Empty
    Unload Me
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    On Error Resume Next
    If KeyAscii = 13 Then SendKeys "{Tab}"
End Sub

Private Sub Form_Load()
    On Error GoTo siga
    NoGuardar = False
    Centrar Me
    Aplicar_skin Me
    CboActivo.ListIndex = 0
    LLenarCombo CboBanco, "ban_nombre", "ban_id", "par_bancos", "ban_activo='SI'"
    CboBanco.ListIndex = 0
    If SG_codigo = Empty Then
        Bm_Nuevo = True
        CboGas.ListIndex = 1
    Else
        Bm_Nuevo = False
        Sql = "SELECT rut_proveedor,nombre_empresa,direccion,comuna,ciudad,fono,fax,email,habilitado,prv_distribuidor_de_gas,ban_id,prv_ctacte " & _
              "FROM maestro_proveedores " & _
              "WHERE rut_proveedor='" & SG_codigo & "'"
        Consulta RsTmp3, Sql
        If RsTmp3.RecordCount > 0 Then
            With RsTmp3
                TxtRut = !rut_proveedor
                TxtRz = !nombre_empresa
                TxtDirec = !direccion
                txtComuna = !comuna
                TxtCiudad = !ciudad
                txtFono = !fono
                TxtFax = !fax
                TxtMail = !Email
                CboActivo.ListIndex = IIf(!habilitado = "SI", 0, 1)
                CboGas.ListIndex = IIf(!prv_distribuidor_de_gas = "SI", 0, 1)
                Busca_Id_Combo CboBanco, Val(!ban_id)
                txtCtaCte = "" & !prv_ctacte
            End With
            TxtRut.Locked = True
            TxtRz.SetFocus
        End If
    End If
    Exit Sub
siga:

End Sub

Private Sub Timer1_Timer()
    If Len(Me.TxtRut.Text) > 0 And _
       Len(Me.TxtRz.Text) > 0 And _
       Len(Me.TxtCiudad.Text) > 0 And _
       Len(Me.txtComuna.Text) > 0 Then
            If NoGuardar = False Then Me.CmdGuarda.Enabled = True
    Else
        Me.CmdGuarda.Enabled = False
    End If
    For Each CTxt In Controls
        If (TypeOf CTxt Is TextBox) Then
            If Me.ActiveControl.Name = CTxt.Name Then 'Foco activo
                CTxt.BackColor = IIf(CTxt.Locked, ClrDesha, ClrCfoco)
            Else
                CTxt.BackColor = IIf(CTxt.Locked, ClrDesha, ClrSfoco)
            End If
        End If
    Next
End Sub


Private Sub Timer2_Timer()
    On Error Resume Next
    If Bm_Nuevo Then
        TxtRut.SetFocus
    Else
        TxtRz.SetFocus
    End If
    Timer2.Enabled = False
    
    Exit Sub

    If TxtRut.Locked Then
        TxtRz.SetFocus
    Else
        Me.TxtRut.SetFocus
    End If
    
End Sub

Private Sub TxtCiudad_KeyPress(KeyAscii As Integer)
KeyAscii = Asc(UCase(Chr(KeyAscii)))
 If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtCiudad_Validate(Cancel As Boolean)
TxtCiudad = Replace(TxtCiudad, "'", "")
End Sub

Private Sub txtComuna_KeyPress(KeyAscii As Integer)
KeyAscii = Asc(UCase(Chr(KeyAscii)))
 If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtComuna_Validate(Cancel As Boolean)
txtComuna = Replace(txtComuna, "'", "")
End Sub



Private Sub TxtDirec_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
     If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtDirec_Validate(Cancel As Boolean)
TxtDirec = Replace(TxtDirec, "'", "")
End Sub

Private Sub txtFax_KeyPress(KeyAscii As Integer)
KeyAscii = Asc(UCase(Chr(KeyAscii)))
 If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtFax_Validate(Cancel As Boolean)
TxtFax = Replace(TxtFax, "'", "")
End Sub

Private Sub TxtFono_KeyPress(KeyAscii As Integer)
KeyAscii = Asc(UCase(Chr(KeyAscii)))
 If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtFono_Validate(Cancel As Boolean)
txtFono = Replace(txtFono, "'", "")
End Sub

Private Sub TxtMail_KeyPress(KeyAscii As Integer)
KeyAscii = Asc(LCase(Chr(KeyAscii)))
 If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtMail_Validate(Cancel As Boolean)
TxtMail = Replace(TxtMail, "'", "")
End Sub

Private Sub TxtRut_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
     If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtRut_Validate(Cancel As Boolean)
    If Len(Me.TxtRut.Text) = 0 Then Exit Sub
    If AccionProveedor = 3 Then Exit Sub
    Me.TxtRut.Text = Replace(Me.TxtRut.Text, ".", "")
    Me.TxtRut.Text = Replace(Me.TxtRut.Text, "-", "")
    Respuesta = VerificaRut(Me.TxtRut.Text, NuevoRut)
    If Respuesta = True Then
        Me.TxtRut.Text = NuevoRut
        
        Dim VeriRut As Recordset
        paso = Consulta(VeriRut, "SELECT * FROM maestro_proveedores WHERE Rut_Proveedor = '" & NuevoRut & "'")
        If VeriRut.RecordCount > 0 Then
            MsgBox "Proveedor ya existe...", vbExclamation + vbOKOnly
            With VeriRut
                TxtRz = !nombre_empresa
                Me.TxtDirec = "" & !direccion
                Me.TxtCiudad = "" & !ciudad
                Me.txtComuna = "" & !comuna
                Me.txtFono = "" & !fono
                Me.TxtFax = "" & !fax
                Me.TxtMail = "" & !Email
            End With
            Me.CmdGuarda.Enabled = False
            NoGuardar = True
             InhabilitaCajas (True)
             If MsgBox("Desea incorporar a este Proveedor a la empresa ACTIVA ?", vbQuestion + vbYesNo) = vbNo Then Exit Sub

             On Error GoTo Fallo
             cn.BeginTrans
                 Sql = "DELETE FROM par_asociacion_ruts " & _
                       "WHERE rut_emp='" & SP_Rut_Activo & "' AND rut_pro='" & TxtRut & "'"
                 cn.Execute Sql
                 Sql = "DELETE FROM par_asociacion_ruts " & _
                       "WHERE rut_emp='" & SP_Rut_Activo & "' AND rut_emp='" & TxtRut & "'"
                 cn.Execute Sql
                 Sql = "INSERT INTO par_asociacion_ruts (rut_emp,rut_pro) " & _
                       "VALUES('" & SP_Rut_Activo & "','" & TxtRut & "')"
                 cn.Execute Sql
                 Sql = "UPDATE maestro_proveedores " & _
                        "SET habilitado='SI' " & _
                        "WHERE rut_proveedor='" & TxtRut & "'"
                 cn.Execute Sql
              cn.CommitTrans
            
            'Unload Me
        Else
            InhabilitaCajas (False)
            NoGuardar = False
        End If
    Else
         InhabilitaCajas (False)
        NoGuardar = False
        TxtRut.Text = ""
    End If
    Exit Sub
Fallo:
    MsgBox "Ocurrio un error al intentar grabar los datos..." & vbNewLine & Err.Number & vbNewLine & Err.Description, vbInformation
    cn.RollbackTrans
End Sub


Private Sub TxtRz_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
     If KeyAscii = 39 Then KeyAscii = 0
End Sub

Public Sub InhabilitaCajas(SI As Boolean)
    For Each CTxt In Controls
        If (TypeOf CTxt Is TextBox) Then
            If SI = True Then
                If CTxt.Name <> "TxtRut" Then CTxt.Locked = True
            Else
                If CTxt.Name <> "TxtRut" Then CTxt.Text = ""
                CTxt.Locked = False
            End If
        End If
    Next
End Sub

Private Sub TxtRz_Validate(Cancel As Boolean)
TxtRz = Replace(TxtRz, "'", "")
End Sub
