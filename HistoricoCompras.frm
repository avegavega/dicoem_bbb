VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{0ECD9B60-23AA-11D0-B351-00A0C9055D8E}#6.0#0"; "MSHFLXGD.OCX"
Begin VB.Form HistoricoCompras 
   Caption         =   "Historico Compras"
   ClientHeight    =   8130
   ClientLeft      =   60
   ClientTop       =   390
   ClientWidth     =   11445
   LinkTopic       =   "Form1"
   ScaleHeight     =   8130
   ScaleWidth      =   11445
   StartUpPosition =   2  'CenterScreen
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   3360
      OleObjectBlob   =   "HistoricoCompras.frx":0000
      Top             =   7680
   End
   Begin VB.CommandButton CmdSalir 
      Caption         =   "&Salir"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   9600
      TabIndex        =   12
      Top             =   7200
      Width           =   1215
   End
   Begin VB.CommandButton CmdSeleccionar 
      Caption         =   "Seleccionar"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   120
      TabIndex        =   11
      Top             =   7080
      Width           =   1935
   End
   Begin VB.Frame Frame2 
      Caption         =   "Ordenar Por"
      Height          =   1215
      Left            =   7680
      TabIndex        =   7
      Top             =   600
      Width           =   3255
      Begin VB.ComboBox CboDireccion 
         Height          =   315
         ItemData        =   "HistoricoCompras.frx":0234
         Left            =   1320
         List            =   "HistoricoCompras.frx":023E
         Style           =   2  'Dropdown List
         TabIndex        =   9
         Top             =   720
         Width           =   1695
      End
      Begin VB.ComboBox CboOrden 
         Height          =   315
         Left            =   480
         Style           =   2  'Dropdown List
         TabIndex        =   8
         Top             =   240
         Width           =   2415
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   255
         Left            =   240
         OleObjectBlob   =   "HistoricoCompras.frx":025B
         TabIndex        =   10
         Top             =   720
         Width           =   855
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Filtro"
      Height          =   1215
      Left            =   120
      TabIndex        =   1
      Top             =   600
      Width           =   6855
      Begin VB.CommandButton CmdTodos 
         Caption         =   "Todos"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   5640
         TabIndex        =   5
         Top             =   480
         Width           =   1095
      End
      Begin VB.CommandButton CmdBusca 
         Caption         =   "Buscar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   4320
         TabIndex        =   4
         Top             =   480
         Width           =   1095
      End
      Begin VB.OptionButton OpCliente 
         Caption         =   "Proveedor"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   600
         TabIndex        =   3
         Top             =   840
         Value           =   -1  'True
         Width           =   1695
      End
      Begin VB.TextBox TxtBusqueda 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   600
         TabIndex        =   2
         Top             =   380
         Width           =   3615
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   255
         Left            =   720
         OleObjectBlob   =   "HistoricoCompras.frx":02CB
         TabIndex        =   6
         Top             =   120
         Width           =   2295
      End
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
      Height          =   375
      Left            =   360
      OleObjectBlob   =   "HistoricoCompras.frx":032E
      TabIndex        =   0
      Top             =   0
      Width           =   8415
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
      Height          =   255
      Left            =   2280
      OleObjectBlob   =   "HistoricoCompras.frx":039A
      TabIndex        =   13
      Top             =   7200
      Width           =   6615
   End
   Begin MSHierarchicalFlexGridLib.MSHFlexGrid MshProvedor 
      Bindings        =   "HistoricoCompras.frx":047E
      Height          =   4095
      Left            =   240
      TabIndex        =   14
      Top             =   2760
      Width           =   10815
      _ExtentX        =   19076
      _ExtentY        =   7223
      _Version        =   393216
      FixedCols       =   0
      HighLight       =   2
      SelectionMode   =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _NumberOfBands  =   1
      _Band(0).Cols   =   2
   End
End
Attribute VB_Name = "HistoricoCompras"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim RSHisCompras As Recordset
Private Sub CboDireccion_Click()
  If CboOrden.ListIndex = -1 Then Exit Sub
    With Me.MshProvedor
        If Me.CboOrden.ListIndex = 0 Then
            Me.OrdenPorFecha
        Else
        
            .Col = CboOrden.ListIndex
            .Sort = Me.CboDireccion.ListIndex + 1
        End If
    End With
    
End Sub



Private Sub CboOrden_Click()
    With Me.MshProvedor
        If CboOrden.ListIndex = 0 Then
          
            Me.OrdenPorFecha
        
        
        Else
        
        
            .Col = CboOrden.ListIndex
            .Sort = Me.CboDireccion.ListIndex + 1
        End If
    End With
End Sub




Private Sub CmdSalir_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    Aplicar_skin Me
    Sql = "SELECT fecha,rut,nombre_proveedor,documento,no_documento,pago,neto, iva, total FROM compra_repuesto_documento"
    paso = AbreConsulta(RSHisCompras, Sql)
    Set Me.MshProvedor.DataSource = RSHisCompras
    EncabezadoMSH
End Sub

Public Sub EncabezadoMSH()
 Dim Encabezados(8) As String
    Encabezados(0) = "Fecha"
    Encabezados(1) = "R.U.T."
    Encabezados(2) = "Nombre Proveedor"
    Encabezados(3) = "Documento"
    Encabezados(4) = "N�"
    Encabezados(5) = "Forma Pago"
    Encabezados(6) = "NETO"
    Encabezados(7) = "I.V.A."
    Encabezados(8) = "Total"
    With Me.MshProvedor
        .ColWidth(1) = 1500
        .ColWidth(2) = 3000
        .Row = 0
        For I = 0 To .Cols - 1
            .Col = I
            .Text = Encabezados(I)
            Me.CboOrden.AddItem .Text
        Next
        
        For I = 1 To .Rows - 1
            .Row = I
            .Col = 0
            .Text = Format(.Text, "DD-MM-YYYY")
            .Col = 5
            '.Text = FormatNumber(.Text, 1, vbFalse)
            .Col = 6
            
           ' .Text = Format(.Text, "###,##.#")
            
            .Col = 7
            '.Text = Format(Replace(.Text, ".", ""), "###,##")
        Next
        
        
    End With
End Sub


Public Sub OrdenPorFecha()
        Dim Orden As String
        If Me.CboDireccion.ListIndex = 1 Then Orden = "DESC"
        
        Dim cn As ADODB.Connection 'variable para la base de datos y el recordset
        Dim AdoHis As ADODB.Recordset
        Connection_String = "PROVIDER=MSDASQL;dsn=citroen;uid=;pwd=;" '   ' le agrega el path de la base de datos al Connectionstring
        'Sql = "SELECT  FROM OT WHERE No_Orden = " & NumeroOrden
       ' Sql = "SELECT No_Orden,Fecha,Nombre_Cliente,Patente,Marca,Modelo,Ano, No_Factura, No_Boleta FROM OT ORDER BY No_Orden desc"
        Sql = "SELECT DISTINCTROW compra_stock_repuestos.fecha_compra, compra_stock_repuestos.rut_proveedor, compra_stock_repuestos.nombre_proveedor, compra_stock_repuestos.documento_ingreso, compra_stock_repuestos.numero_documento, Sum(compra_stock_repuestos.total) AS [Neto]," & _
        "Sum(compra_stock_repuestos.total *19/100) AS [IVA]," & _
        "Sum(compra_stock_repuestos.total * 1.19) As TOTAL " & _
        "FROM compra_stock_repuestos" & _
        " GROUP BY compra_stock_repuestos.fecha_compra, compra_stock_repuestos.rut_proveedor, compra_stock_repuestos.nombre_proveedor, compra_stock_repuestos.documento_ingreso, compra_stock_repuestos.numero_documento " & _
         "ORDER BY compra_stock_repuestos.fecha_compra " & IIf(Me.CboDireccion.ListIndex = 1, "Desc", "")
        
        
        Set cn = New ADODB.Connection '  ' Nuevo objeto Connection
        cn.CursorLocation = adUseClient
        cn.Open Connection_String '  'Abre la conexi�n
        Set AdoHis = New ADODB.Recordset '  'Nuevo recordset
        AdoHis.Open Sql, cn, adOpenStatic, adLockOptimistic '  ' abre el recordset
        
        
        'Me.AdoHistorico.RecordSource = Sql

        Set Me.MshProvedor.DataSource = AdoHis
    
        Me.EncabezadoMSH
End Sub
