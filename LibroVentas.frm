VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "MSDATGRD.OCX"
Begin VB.Form LibroVentas 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Libro Ventas"
   ClientHeight    =   10020
   ClientLeft      =   -1440
   ClientTop       =   645
   ClientWidth     =   14445
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   10020
   ScaleWidth      =   14445
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame FraProgreso 
      Height          =   735
      Left            =   7680
      TabIndex        =   40
      Top             =   9120
      Visible         =   0   'False
      Width           =   4215
      Begin VB.PictureBox Picture1 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         ForeColor       =   &H80000008&
         Height          =   375
         Left            =   3360
         ScaleHeight     =   345
         ScaleWidth      =   705
         TabIndex        =   41
         Top             =   240
         Width           =   735
         Begin ACTIVESKINLibCtl.SkinLabel SkProgreso 
            Height          =   255
            Left            =   -120
            OleObjectBlob   =   "LibroVentas.frx":0000
            TabIndex        =   42
            Top             =   45
            Width           =   735
         End
      End
      Begin MSComctlLib.ProgressBar BarraProgreso 
         Height          =   375
         Left            =   120
         TabIndex        =   43
         Top             =   240
         Width           =   3135
         _ExtentX        =   5530
         _ExtentY        =   661
         _Version        =   393216
         Appearance      =   1
      End
   End
   Begin MSComDlg.CommonDialog dialogo 
      Left            =   13680
      Top             =   9360
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.Frame Frame4 
      Caption         =   "Leyendas Accion"
      Height          =   855
      Left            =   120
      TabIndex        =   13
      Top             =   9000
      Width           =   8295
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   255
         Index           =   0
         Left            =   240
         OleObjectBlob   =   "LibroVentas.frx":0062
         TabIndex        =   14
         Top             =   360
         Width           =   7935
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   255
         Index           =   1
         Left            =   240
         OleObjectBlob   =   "LibroVentas.frx":0172
         TabIndex        =   15
         Top             =   600
         Width           =   7935
      End
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   0
      OleObjectBlob   =   "LibroVentas.frx":027E
      Top             =   7320
   End
   Begin VB.CommandButton CmdSalir 
      Caption         =   "&Salir"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   11760
      TabIndex        =   2
      Top             =   9240
      Width           =   2175
   End
   Begin VB.Frame Frame2 
      Caption         =   "Detalle"
      Height          =   7575
      Left            =   120
      TabIndex        =   1
      Top             =   1320
      Width           =   13815
      Begin MSComctlLib.ListView LvLibro 
         Height          =   1815
         Left            =   0
         TabIndex        =   39
         Top             =   2160
         Visible         =   0   'False
         Width           =   13575
         _ExtentX        =   23945
         _ExtentY        =   3201
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   11
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Fecha"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Documento"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Nro Documento"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "RUT Cliente"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Text            =   "R.Social"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Text            =   "Movimiento"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   6
            Object.Tag             =   "N100"
            Text            =   "Neto"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   7
            Object.Tag             =   "N100"
            Text            =   "IVA"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   8
            Object.Tag             =   "N100"
            Text            =   "total"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   9
            Text            =   "Forma de pago"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   10
            Object.Tag             =   "N100"
            Text            =   "Doc_id"
            Object.Width           =   0
         EndProperty
      End
      Begin VB.TextBox txtIvaTotal 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   8880
         Locked          =   -1  'True
         TabIndex        =   37
         Top             =   7200
         Width           =   1575
      End
      Begin VB.TextBox txtBrutototal 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   10560
         Locked          =   -1  'True
         TabIndex        =   36
         Top             =   7200
         Width           =   1575
      End
      Begin VB.TextBox txtNetoTotal 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   7200
         Locked          =   -1  'True
         TabIndex        =   35
         Top             =   7200
         Width           =   1575
      End
      Begin VB.TextBox txtNetoNC 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   7200
         Locked          =   -1  'True
         TabIndex        =   31
         Top             =   6600
         Width           =   1575
      End
      Begin VB.TextBox txtBrutoNC 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   10560
         Locked          =   -1  'True
         TabIndex        =   30
         Top             =   6600
         Width           =   1575
      End
      Begin VB.TextBox txtIvaNC 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   8880
         Locked          =   -1  'True
         TabIndex        =   29
         Top             =   6600
         Width           =   1575
      End
      Begin MSDataGridLib.DataGrid GridNC 
         Height          =   1695
         Left            =   120
         TabIndex        =   28
         Top             =   4680
         Width           =   13575
         _ExtentX        =   23945
         _ExtentY        =   2990
         _Version        =   393216
         HeadLines       =   1
         RowHeight       =   15
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Caption         =   "Notas de Cr�dito"
         ColumnCount     =   2
         BeginProperty Column00 
            DataField       =   ""
            Caption         =   ""
            BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
               Type            =   0
               Format          =   ""
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   3082
               SubFormatType   =   0
            EndProperty
         EndProperty
         BeginProperty Column01 
            DataField       =   ""
            Caption         =   ""
            BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
               Type            =   0
               Format          =   ""
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   3082
               SubFormatType   =   0
            EndProperty
         EndProperty
         SplitCount      =   1
         BeginProperty Split0 
            BeginProperty Column00 
            EndProperty
            BeginProperty Column01 
            EndProperty
         EndProperty
      End
      Begin VB.CommandButton cmdHistorial 
         Caption         =   "Historial"
         Height          =   255
         Left            =   3240
         TabIndex        =   27
         Top             =   4200
         Width           =   1215
      End
      Begin VB.CommandButton cmdEliminarDocumento 
         Caption         =   "Eliminar Documento Seleccionado"
         Height          =   255
         Left            =   120
         TabIndex        =   26
         ToolTipText     =   "de Venta"
         Top             =   4185
         Width           =   3015
      End
      Begin VB.CommandButton PicExcel 
         Caption         =   "Excel"
         Height          =   255
         Left            =   12960
         TabIndex        =   23
         ToolTipText     =   "de Ventas"
         Top             =   4200
         Width           =   735
      End
      Begin VB.TextBox TxtIva 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   8880
         Locked          =   -1  'True
         TabIndex        =   19
         Top             =   4320
         Width           =   1575
      End
      Begin VB.TextBox TxtBruto 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   10560
         Locked          =   -1  'True
         TabIndex        =   18
         Top             =   4320
         Width           =   1575
      End
      Begin VB.TextBox TxtNeto 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   7200
         Locked          =   -1  'True
         TabIndex        =   17
         Top             =   4320
         Width           =   1575
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
         Height          =   255
         Left            =   7800
         OleObjectBlob   =   "LibroVentas.frx":04B2
         TabIndex        =   20
         Top             =   4080
         Width           =   975
      End
      Begin MSDataGridLib.DataGrid GridVentas 
         Height          =   3735
         Left            =   120
         TabIndex        =   12
         Top             =   360
         Width           =   13575
         _ExtentX        =   23945
         _ExtentY        =   6588
         _Version        =   393216
         AllowUpdate     =   0   'False
         HeadLines       =   1
         RowHeight       =   15
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Caption         =   "Ventas"
         ColumnCount     =   2
         BeginProperty Column00 
            DataField       =   ""
            Caption         =   ""
            BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
               Type            =   0
               Format          =   ""
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   13322
               SubFormatType   =   0
            EndProperty
         EndProperty
         BeginProperty Column01 
            DataField       =   ""
            Caption         =   ""
            BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
               Type            =   0
               Format          =   ""
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   13322
               SubFormatType   =   0
            EndProperty
         EndProperty
         SplitCount      =   1
         BeginProperty Split0 
            BeginProperty Column00 
            EndProperty
            BeginProperty Column01 
            EndProperty
         EndProperty
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel6 
         Height          =   255
         Left            =   11280
         OleObjectBlob   =   "LibroVentas.frx":0510
         TabIndex        =   24
         Top             =   4080
         Width           =   855
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel7 
         Height          =   255
         Left            =   9720
         OleObjectBlob   =   "LibroVentas.frx":0570
         TabIndex        =   25
         Top             =   4080
         Width           =   735
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel8 
         Height          =   255
         Index           =   0
         Left            =   7800
         OleObjectBlob   =   "LibroVentas.frx":05D2
         TabIndex        =   32
         Top             =   6360
         Width           =   975
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel9 
         Height          =   255
         Left            =   11280
         OleObjectBlob   =   "LibroVentas.frx":0630
         TabIndex        =   33
         Top             =   6360
         Width           =   855
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel10 
         Height          =   255
         Left            =   9720
         OleObjectBlob   =   "LibroVentas.frx":0690
         TabIndex        =   34
         Top             =   6360
         Width           =   735
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel8 
         Height          =   255
         Index           =   1
         Left            =   5760
         OleObjectBlob   =   "LibroVentas.frx":06F2
         TabIndex        =   38
         Top             =   7200
         Width           =   1335
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Opciones del libro"
      Height          =   1095
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   13815
      Begin VB.CommandButton CmdFiltro 
         Caption         =   "Filtrar"
         Height          =   375
         Left            =   12600
         TabIndex        =   16
         Top             =   360
         Width           =   1095
      End
      Begin VB.Frame Frame3 
         Caption         =   "Dctos. de Venta"
         Height          =   735
         Left            =   4560
         TabIndex        =   7
         Top             =   240
         Width           =   7815
         Begin VB.ComboBox CboAccion 
            Height          =   315
            ItemData        =   "LibroVentas.frx":0756
            Left            =   6600
            List            =   "LibroVentas.frx":0772
            Style           =   2  'Dropdown List
            TabIndex        =   21
            Top             =   360
            Width           =   1095
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel5 
            Height          =   255
            Left            =   6000
            OleObjectBlob   =   "LibroVentas.frx":079F
            TabIndex        =   22
            Top             =   360
            Width           =   1095
         End
         Begin VB.CheckBox Check4 
            Caption         =   "Otros."
            Enabled         =   0   'False
            Height          =   255
            Left            =   4920
            TabIndex        =   11
            Top             =   360
            Width           =   855
         End
         Begin VB.CheckBox Check3 
            Caption         =   "Boletas"
            Enabled         =   0   'False
            Height          =   255
            Left            =   3480
            TabIndex        =   10
            Top             =   360
            Width           =   1215
         End
         Begin VB.CheckBox Check2 
            Caption         =   "Facturas"
            Enabled         =   0   'False
            Height          =   255
            Left            =   1920
            TabIndex        =   9
            Top             =   360
            Width           =   1215
         End
         Begin VB.CheckBox Check1 
            Caption         =   "Todos"
            Height          =   255
            Left            =   360
            TabIndex        =   8
            Top             =   360
            Value           =   1  'Checked
            Width           =   975
         End
      End
      Begin MSComCtl2.DTPicker Dtfin 
         Height          =   375
         Left            =   3000
         TabIndex        =   3
         Top             =   360
         Width           =   1455
         _ExtentX        =   2566
         _ExtentY        =   661
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   57999361
         CurrentDate     =   39916
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   255
         Left            =   2280
         OleObjectBlob   =   "LibroVentas.frx":0809
         TabIndex        =   4
         Top             =   480
         Width           =   705
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   255
         Left            =   120
         OleObjectBlob   =   "LibroVentas.frx":086A
         TabIndex        =   5
         Top             =   480
         Width           =   705
      End
      Begin MSComCtl2.DTPicker DtInicio 
         Height          =   375
         Left            =   840
         TabIndex        =   6
         Top             =   360
         Width           =   1455
         _ExtentX        =   2566
         _ExtentY        =   661
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   57999361
         CurrentDate     =   39916
      End
   End
End
Attribute VB_Name = "LibroVentas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim RsVentas As Recordset
Dim rs_NotaCredito As Recordset
Dim SqlNC As String
Dim Motivo As String
Private Sub cmdEliminarDocumento_Click()
    If RsVentas.RecordCount = 0 Then Exit Sub
    Dim i_IdDocumento As Double
    Dim Documento As String
    Dim RutPrv As String
    Dim NumDoc As Double
    Dim Tipo_Mov As String
    Dim ValorDoc As Double
    Dim NombreProveedor As String
    Dim fPago As String
    Dim VehRecDoc As String
    Dim VehRecNum As Double
    Dim RsComprasRep As Recordset
    Dim Lfecha As String
    Lfecha = Format(Date, "yyyy-mm-dd")
    With GridVentas 'S    on compras
        If .Row = -1 Then Exit Sub
        .Col = 1: Documento = .Text
        .Col = 2: NumDoc = Val(.Text)
        .Col = 3: RutPrv = .Text
        .Col = 4: NombreProveedor = .Text
        .Col = 5: Tipo_Mov = .Text
        .Col = 6: ValorDoc = Val(.Text)
        .Col = 9: fPago = .Text
        .Col = 10: i_IdDocumento = .Text
        '.Col = 10: VehRecDoc = .Text
        '.Col = 11: VehRecNum = Val(.Text)
    End With
    If MsgBox("Esta a punto de eliminar " & Documento & " N� " & NumDoc & vbNewLine & _
              "Cliente " & NombreProveedor & vbNewLine & _
              "Por $" & NumFormat(ValorDoc) & " Neto" & vbNewLine & _
              "Forma de pago:" & fPago & vbNewLine & _
              "Por concepto de " & Tipo_Mov & vbNewLine & _
              "�Confirma eliminaci�n?", vbYesNo) = vbYes Then
                Motivo = InputBox("Describa el motivo de la eliminacion")
                If Len(Motivo) = 0 Then
                    MsgBox "Debe ingresar motivo de eliminaci�n"
                    Exit Sub
                End If
                If Tipo_Mov = "ARR" Then
                    Documento = "FACTURA"
                    Sql = "DELETE FROM arriendos WHERE no_factura = " & NumDoc
                    Call AbreConsulta(RsTmp, Sql)
                    Sql = "DELETE FROM doc_venta WHERE tipo_doc='" & Documento & "' AND no_documento =" & NumDoc
                    Call AbreConsulta(RsTmp, Sql)
                    'Borramos los documentos de la cta cte
                    Filtro = "RUT='" & RutPrv & "' AND TIPO_DOC='" & Documento & "' AND NO_DOC=" & NumDoc
                    Sql = "DELETE FROM ctacte_clientes WHERE " & Filtro
                    Call AbreConsulta(RsTmp, Sql)
                    'Borramos los pagos del documento
                    Sql = "DELETE FROM pagos_clientes WHERE " & Filtro
                    Call AbreConsulta(RsTmp, Sql)
                End If
                If Tipo_Mov = "VD" Then
                        Filtro = " doc_id=" & i_IdDocumento & " AND no_documento=" & NumDoc
                        'If Documento = "FACTURA" Then
                        '    Filtro = "NO_FACTURA = " & NumDoc
                        'Else
                        '    Filtro = "NO_BOLETA = " & NumDoc
                        'End If
                        Sql = "SELECT codigo,unidades FROM vd_materiales WHERE " & Filtro
                        Call AbreConsulta(RsComprasRep, Sql)
                        
                        If RsComprasRep.RecordCount > 0 Then
                            With RsComprasRep
                                .MoveFirst
                                Do While Not .EOF
                                    'Actualizamos Stock
                                    'Eliminando documento
                                    '1ro Abril 2011
                                     Sql = "SELECT ROUND(kar_cantidad_valor/kar_cantidad,0) valor " & _
                                            "FROM kardex " & _
                                            "WHERE kar_documento=" & i_IdDocumento & " AND kar_numero=" & NumDoc & " " & _
                                            "ORDER BY kar_id DESC " & _
                                            "LIMIT 1"
                              
                                            AbreConsulta RsTmp, Sql
                                            If RsTmp.RecordCount > 0 Then
                                                l_Pcompra = RsTmp!valor
                                            Else
                                                l_Pcompra = 0
                                            End If
                                            'KARDEX  PARA ENTRADA AL ELIMINAR doc de venta
                                            Kardex Format(Date, "YYYY-MM-DD"), _
                                                           "ENTRADA", IG_id_OT, i_IdDocumento, 1, !Codigo, _
                                                           !Unidades, "ELIMINA: " & Documento & " " & NumDoc, _
                                                           l_Pcompra, l_Pcompra * !Unidades
                                                           
                                                        
                                                        'Sql = "UPDATE maestro_productos SET stock_actual = stock_actual +" & Replace$(Str(!Unidades), ",", ".") & " WHERE codigo = '" & !Codigo & "'"
                                                    'C'all AbreConsulta(RsTmp, Sql)
                                    .MoveNext
                                Loop
                            End With
                        End If
                        'Borra materiales de Venta directa
                        Sql = "DELETE FROM vd_materiales WHERE " & Filtro
                        Call AbreConsulta(RsComprasRep, Sql)
                        
                        Sql = "DELETE FROM doc_venta WHERE tipo_doc='" & Documento & "' AND no_documento =" & NumDoc
                        Call AbreConsulta(RsTmp, Sql)
                        
                        'Borramos los documentos de la cta cte
                        Filtro = "RUT='" & RutPrv & "' AND TIPO_DOC='" & Documento & "' AND NO_DOC=" & NumDoc
                        Sql = "DELETE FROM ctacte_clientes WHERE " & Filtro
                        Call AbreConsulta(RsTmp, Sql)
                        
                        'Borramos los pagos del documento
                        Sql = "DELETE FROM pagos_clientes WHERE " & Filtro
                        Call AbreConsulta(RsTmp, Sql)
                        
                        'Borramos los zzz del documento
                        Sql = "DELETE FROM detalle_zzz WHERE tipo_documento='" & Documento & "' AND no_documento=" & NumDoc
                        Call AbreConsulta(RsTmp, Sql)
                        
                End If
                
                If Tipo_Mov = "CVU" Then
                    Sql = "UPDATE doc_venta " & _
                          "SET tipo_doc='PENDIENTE', forma_pago='',no_documento=0 " & _
                          "WHERE tipo_doc ='" & Documento & "' " & _
                          "AND no_documento =" & NumDoc & _
                          " AND tipo_doc<>'PENDIENTE'"
                          
                    Call AbreConsulta(RsTmp, Sql)
                    'Borramos los documentos de la cta cte
                    Filtro = "RUT='" & RutPrv & "' AND TIPO_DOC='" & Documento & "' AND NO_DOC=" & NumDoc
                    Sql = "DELETE FROM ctacte_clientes WHERE " & Filtro
                    Call AbreConsulta(RsTmp, Sql)
                    
                    'Borramos los pagos del documento
                    Sql = "DELETE FROM pagos_clientes WHERE " & Filtro
                    Call AbreConsulta(RsTmp, Sql)
                    
                
                End If
                
                If Tipo_Mov = "OT" Then
                
                    If Documento = "FACTURA" Then
                         Filtro = "A.NO_FACTURA = " & NumDoc
                    Else
                        Filtro = "A.NO_BOLETA = " & NumDoc
                    End If
                  '0  Filtro = "doc_id = " & i_IdDocumento & " AND no_documento=" & NumDoc
                                       
                    'Obtenemos los materiales de la OT para devolverlos al Stock
                    Sql = "SELECT A.no_orden, B.ordentrabajo,B.codigo,B.unidades " & _
                          "FROM ot A,ot_materiales B " & _
                          "WHERE A.no_orden=B.ordentrabajo AND " & Filtro
                    Call AbreConsulta(RsComprasRep, Sql)
                    
                    If RsComprasRep.RecordCount > 0 Then
                        With RsComprasRep
                            .MoveFirst
                            Do While Not .EOF
                            
                                'KARDEX
        

                                    'Eliminando documento
                                    '1ro Abril 2011
                                     Sql = "SELECT ROUND(kar_cantidad_valor/kar_cantidad,0) valor " & _
                                            "FROM Kardex " & _
                                            "WHERE kar_documento=" & i_IdDocumento & " AND kar_numero=" & NumDoc & " " & _
                                            "ORDER BY kar_id DESC " & _
                                            "LIMIT 1"
                              
                                            AbreConsulta RsTmp, Sql
                                            If RsTmp.RecordCount > 0 Then
                                                l_Pcompra = RsTmp!valor
                                            Else
                                                l_Pcompra = 0
                                            End If
                                            'KARDEX  PARA ENTRADA AL ELIMINAR doc de venta
                                            Kardex Format(Date, "YYYY-MM-DD"), _
                                                           "ENTRADA", IG_id_OT, i_IdDocumento, 1, !Codigo, _
                                                           !Unidades, "ELIMINA: " & Documento & " " & NumDoc, _
                                                           l_Pcompra, l_Pcompra * !Unidades
                                                           
                                                        
                                                        'Sql = "UPDATE maestro_productos SET stock_actual = stock_actual +" & Replace$(Str(!Unidades), ",", ".") & " WHERE codigo = '" & !Codigo & "'"
                                                    'C'all AbreConsulta(RsTmp, Sql)
                                .MoveNext
                            Loop
                        End With
                    End If
                    'Borramos el Doc de venta
                    'Sql = "DELETE FROM doc_venta WHERE tipo_doc='" & Documento & "' AND no_documento =" & NumDoc
                    Sql = "DELETE FROM doc_venta WHERE doc_id=" & i_IdDocumento & " AND no_documento =" & NumDoc
                    Call AbreConsulta(RsTmp, Sql)
                    
                    'Borramos los documentos de la cta cte
                    Filtro = "RUT='" & RutPrv & "' AND TIPO_DOC='" & Documento & "' AND NO_DOC=" & NumDoc
                    Sql = "DELETE FROM ctacte_clientes WHERE " & Filtro
                    Call AbreConsulta(RsTmp, Sql)
                    
                    'Borramos los pagos del documento
                    Sql = "DELETE FROM pagos_clientes WHERE " & Filtro
                    Call AbreConsulta(RsTmp, Sql)
                    
                    'Obtenemos el Nro de orden
                     If Documento = "FACTURA" Then
                         Filtro = "NO_FACTURA = " & NumDoc
                    Else
                         Filtro = "NO_BOLETA = " & NumDoc
                    End If
                    Dim nOrden As Double
                    nOrden = 0
                    Sql = "SELECT no_orden FROM ot WHERE " & Filtro
                    Call AbreConsulta(RsTmp, Sql)
                    If RsTmp.RecordCount > 0 Then
                        nOrden = RsTmp!no_orden
                    End If
                    RsTmp.close
                    'Borramos materiales de la ot
                    Sql = "DELETE FROM ot_materiales WHERE ordentrabajo = " & nOrden
                    Call AbreConsulta(RsTmp, Sql)
                    'Borramos manos de obras de la ot
                    Sql = "DELETE FROM ot_manos_obra WHERE ordentrabajo = " & nOrden
                    Call AbreConsulta(RsTmp, Sql)
                    'Borramos la ot
                    Sql = "DELETE FROM ot WHERE no_orden =  " & nOrden
                    Call AbreConsulta(RsTmp, Sql)
                End If
                
                
                If Tipo_Mov = "VVN" Or Tipo_Mov = "VVU" Then 'Eliminacion Venta vehiculo nuevo
                    Dim Lchasis As String, Lpatente As String
                    
                    '1ro consultamos si el documento tiene asociado algun vehiculo en parte de pago
                    Filtro = "rut='" & RutPrv & "' AND partepagodoc='" & Documento & "' AND partepagondoc=" & NumDoc
                    Sql = "SELECT documento,no_documento " & _
                         "FROM compra_repuesto_documento " & _
                         "WHERE " & Filtro
                    Call AbreConsulta(RsTmp, Sql)
                    If RsTmp.RecordCount > 0 Then
                        MsgBox "ESTE DOCUMENTO TIENE ASOCIADO UN VEH�CULO RECIBIO EN PARTE PAGO" & vbNewLine & _
                               "-PRIMERO DEBE ELIMINAR " & vbNewLine & _
                               RsTmp!Documento & " N� " & RsTmp!No_Documento, vbOKOnly + vbInformation
                        Exit Sub
                    End If
                    
                    
                    Filtro = "rut_cliente='" & RutPrv & "' AND tipo_documento='" & Documento & "' AND no_documento=" & NumDoc
                    Sql = "SELECT chasis,patente FROM venta_vehiculos WHERE " & Filtro
                    Call AbreConsulta(RsTmp, Sql)
                    If RsTmp.RecordCount > 0 Then
                        Lchasis = "" & RsTmp!chasis
                        Lpatente = "" & RsTmp!patente
                    End If
                    
                    
                    
                    
                    
                    
                    
                    'Devolvemos el vehiculo al stock
                    If Tipo_Mov = "VVN" Then 'vehiculo nuevo
                        Sql = "UPDATE vehiculos SET estado='EN STOCK' WHERE chasis = '" & Lchasis & "'"
                    Else 'vehiculo usado
                        'Debemos consultar los documentos de comisiones
                        Sql = "SELECT tipo_doc,no_documento,bruto,tipo_movimiento FROM doc_venta " & _
                            "WHERE doc_afecto_comision='" & Documento & "' AND num_afecto_comision=" & NumDoc
                        Call AbreConsulta(RsTmp, Sql)
                        If RsTmp.RecordCount > 0 Then
                            Dim mensaje As String
                            RsTmp.MoveFirst
                            Do While Not RsTmp.EOF
                                mensaje = mensaje & " - " & RsTmp!tipo_doc & " N� " & RsTmp!No_Documento & " $ " & NumFormat(RsTmp!Bruto) & vbNewLine
                                RsTmp.MoveNext
                            Loop
                            If MsgBox("Al eliminar " & Documento & " N� " & NumDoc & vbNewLine & _
                                   "Se eliminaran los siguientes documentos de comisi�n " & vbNewLine & _
                                   mensaje & vbNewLine & "�Desea continuar?", vbYesNo + vbQuestion) = vbNo Then
                                   Exit Sub 'No continua
                            Else
                                Dim RsTmpX As Recordset, filtroX As String, filtroY As String
                                RsTmp.MoveFirst
                                Do While Not RsTmp.EOF
                                    Sql = "INSERT INTO eliminacion (tipo_movimiento,fecha,tipo_doc,no_documento,nombre,valor_neto,usuario,motivo,concepto) VALUES(" & _
                                    "'VENTA','" & Lfecha & "','" & RsTmp!tipo_doc & "'," & RsTmp!No_Documento & ",'" & NombreProveedor & "'," & RsTmp!Bruto & ",'" & LogUsuario & "','" & Motivo & "','" & RsTmp!Tipo_Movimiento & "')"
                                    cn.Execute Sql 'inserscion directa
                                    filtroX = "tipo_doc='" & RsTmp!tipo_doc & "' AND no_documento=" & RsTmp!No_Documento
                                    'Borramos los documentos de la cta cte
                                    filtroY = "RUT='" & RutPrv & "' AND TIPO_DOC='" & RsTmp!tipo_doc & "' AND NO_DOC=" & RsTmp!No_Documento
                                    Sql = "DELETE FROM ctacte_clientes WHERE " & filtroY
                                    Call AbreConsulta(RsTmpX, Sql)
                                    'Borramos los pagos del documento
                                    Sql = "DELETE FROM pagos_clientes WHERE " & filtroY
                                    Call AbreConsulta(RsTmpX, Sql)
                                    'Al final borramos el documento
                                    Sql = "DELETE FROM doc_venta WHERE " & filtroX
                                    Call AbreConsulta(RsTmpX, Sql)
                                    RsTmp.MoveNext
                                Loop
                            End If
                        End If
                        Sql = "UPDATE vehiculos SET estado='EN STOCK' WHERE patente = '" & Lpatente & "'"
                    End If
                    Call AbreConsulta(RsTmp, Sql)
                                        
                    'Eliminamos el registro de venta del vehiculo
                    Sql = "DELETE FROM venta_vehiculos WHERE " & Filtro
                    Call AbreConsulta(RsTmp, Sql)
                    
                    'Eliminamos el doc de venta
                    Sql = "DELETE FROM doc_venta WHERE tipo_doc='" & Documento & "' AND no_documento =" & NumDoc
                    Call AbreConsulta(RsTmp, Sql)
                    
                    'Borramos los documentos de la cta cte
                    Filtro = "RUT='" & RutPrv & "' AND TIPO_DOC='" & Documento & "' AND NO_DOC=" & NumDoc
                    Sql = "DELETE FROM ctacte_clientes WHERE " & Filtro
                    Call AbreConsulta(RsTmp, Sql)
                    
                    'Borramos los pagos del documento
                    Sql = "DELETE FROM pagos_clientes WHERE " & Filtro
                    Call AbreConsulta(RsTmp, Sql)
                End If
                
                
                
                
                
                
                If Tipo_Mov = "VEHICULO NUEVO" Or Tipo_Mov = "VEHICULO USADO" Then
                        'Eliminamos el vehiculo del maestro vehiculos
                        Filtro = "PROVEEDOR = '" & RutPrv & "' AND tipo_documento ='" & Documento & "' AND NO_DOCUMENTO = " & NumDoc
                        
                        If Tipo_Mov = "VEHICULO USADO" Then
                            If fPago = "Vehiculo Rec. P.pago" Then
                               'Eliminar pago en el doc de compra
                                Sql = "DELETE FROM pagos_clientes WHERE forma_pago ='VEHICULO EN PARTE PAGO' AND tipo_doc ='" & VehRecDoc & "' AND no_doc = " & VehRecNum
                                Debug.Print Sql
                                Call AbreConsulta(RsTmp, Sql)
                                
                                Sql = "DELETE FROM ctacte_clientes WHERE tipo_doc ='" & VehRecDoc & "' AND no_doc = " & VehRecNum & " AND HABER = " & ValorDoc
                                Debug.Print Sql
                                Call AbreConsulta(RsTmp, Sql)
                            End If
                        
                            Sql = "SELECT patente FROM vehiculos WHERE " & Filtro
                            Call AbreConsulta(RsTmp, Sql)
                            If RsTmp.RecordCount > 0 Then
                                Lpatente = RsTmp!patente
                                RsTmp.close 'eliminammos la cta cte del vehiculo
                                Sql = "DELETE FROM cta_cte_vehiculos WHERE patente_chasis ='" & Lpatente & "'"
                                Call AbreConsulta(RsTmp, Sql)
                            End If
                        End If
                        
                        Sql = "DELETE FROM vehiculos WHERE " & Filtro
                        Call AbreConsulta(RsTmp, Sql)
                         
                End If
                'Ahora eliminar el Doc de compra
'                Filtro = "RUT = '" & RutPrv & "' AND documento ='" & Documento & "' AND NO_DOCUMENTO = " & NumDoc
'                Sql = "DELETE FROM Compra_Repuesto_Documento WHERE " & Filtro
'                Call AbreConsulta(RsTmp, Sql)
'
'                'Ahora eliminamos Cta Cte. del proveedor
'                Filtro = "RUT = '" & RutPrv & "' AND tipo_doc ='" & Documento & "' AND NO_DOC = " & NumDoc
'                Sql = "DELETE FROM CTACTE_Proveedores WHERE " & Filtro
'                Call AbreConsulta(RsTmp, Sql)
'
'                'tambien eliminamos los pagos
'                Sql = "DELETE FROM pagos_proveedores WHERE " & Filtro
'                Call AbreConsulta(RsTmp, Sql)
                 Sql = "INSERT INTO eliminacion (tipo_movimiento,fecha,tipo_doc,no_documento,nombre,valor_neto,usuario,motivo,concepto) VALUES(" & _
                "'VENTA','" & Lfecha & "','" & Documento & "'," & NumDoc & ",'" & NombreProveedor & "'," & ValorDoc & ",'" & LogUsuario & "','" & Motivo & "','" & Tipo_Mov & "')"
              
                Debug.Print Sql
                
                cn.Execute Sql
                If MsgBox("�Imprimir combrante de anulaci�n?", vbYesNo + vbQuestion) = vbYes Then
                    'Imprime combrobante anulacion
                    Me.EmiteComprobanteEliminacion
                End If
    End If
    CmdFiltro_Click
'    Filtrar
End Sub
Private Sub CmdFiltro_Click()
    txtNeto = 0
    TxtIva = 0
    TxtBruto = 0
    LfINI = Format(DTInicio.Value, "yyyy-mm-dd")
    LfFIN = Format(Dtfin.Value, "yyyy-mm-dd")
    Filtro = ""
    
    
    If Check1.Value = 0 Then
        If Check2.Value = 1 And Check3.Value = 0 And Check4.Value = 0 Then
            Filtro = "Tipo_Doc = 'FACTURA'"
        End If
        If Check2.Value = 0 And Check3.Value = 1 And Check4.Value = 0 Then
            Filtro = "Tipo_Doc = 'BOLETA'"
        End If
        If Check2.Value = 0 And Check3.Value = 0 And Check4.Value = 1 Then
            Filtro = "(Tipo_Doc <> 'BOLETA' AND TIPO_DOC <> 'FACTURA')"
        End If
        If Check2.Value = 1 And Check3.Value = 1 And Check4.Value = 0 Then
            Filtro = "(Tipo_Doc = 'BOLETA' OR TIPO_DOC = 'FACTURA')"
        End If
        
        If Len(Filtro) > 0 Then Filtro = " AND " & Filtro
        
        Sql = "SELECT fecha,tipo_doc,no_documento as No,rut_cliente,nombre_cliente,tipo_movimiento as Accion,Neto,Iva,Bruto,Forma_pago,doc_id " & _
              "FROM doc_venta " & _
              " WHERE (fecha BETWEEN '" & LfINI & "' AND '" & LfFIN & "')" & Filtro & _
              " ORDER BY fecha" 'Desc"
        paso = AbreConsulta(RsVentas, Sql)
        '
        
        If Me.CboAccion.ListIndex > 0 Then
            RsVentas.Filter = "ACCION = '" & Me.CboAccion.Text & "'"
        End If
        
    Else
         Sql = "SELECT fecha,tipo_doc,no_documento as No,rut_cliente,nombre_cliente,tipo_movimiento as Accion,Neto,Iva,Bruto,Forma_pago,doc_id " & _
               "FROM doc_venta" & _
               " WHERE (fecha BETWEEN '" & LfINI & "' AND '" & LfFIN & "') ORDER BY Fecha" ' Desc"
        paso = AbreConsulta(RsVentas, Sql)
    End If
    
    LLenar_Grilla RsVentas, Me, LvLibro, False, True, True, True
    
    
    
    SqlNC = "SELECT fecha,no_nc as Nro_Nc,tipo_doc,no_doc,rut,razon_social,neto,bruto-neto as IVA,bruto " & _
           "FROM nc_clientes " & _
           "WHERE (fecha BETWEEN '" & LfINI & "' AND '" & LfFIN & "') ORDER BY Fecha" ' Desc
    
    
    ElGrid
    
    With LvLibro
        .ListItems.Add , , "Totales"
        .ListItems(.ListItems.Count).SubItems(6) = txtNeto
        .ListItems(.ListItems.Count).SubItems(7) = TxtIva
        .ListItems(.ListItems.Count).SubItems(8) = TxtBruto
        
         If rs_NotaCredito.RecordCount > 0 Then
            .ListItems.Add , , "NOTAS DE CREDITO"
            .ListItems(.ListItems.Count).SubItems(6) = " "
            .ListItems(.ListItems.Count).SubItems(7) = " "
            .ListItems(.ListItems.Count).SubItems(8) = " "
            
            rs_NotaCredito.MoveFirst
            
            .ListItems.Add , , " "
            .ListItems(.ListItems.Count).SubItems(1) = "Nro NC"
            .ListItems(.ListItems.Count).SubItems(2) = "Doc.Ref"
            .ListItems(.ListItems.Count).SubItems(3) = "Nro Doc. Ref"
            .ListItems(.ListItems.Count).SubItems(4) = "RUT"
            .ListItems(.ListItems.Count).SubItems(5) = "R.Social"
            .ListItems(.ListItems.Count).SubItems(6) = "Neto"
            .ListItems(.ListItems.Count).SubItems(7) = "IVA"
            .ListItems(.ListItems.Count).SubItems(8) = "Bruto"
        
            Do While Not rs_NotaCredito.EOF
                .ListItems.Add , , rs_NotaCredito!FECHA
                .ListItems(.ListItems.Count).SubItems(1) = rs_NotaCredito!NRO_NC
                .ListItems(.ListItems.Count).SubItems(2) = rs_NotaCredito!tipo_doc
                .ListItems(.ListItems.Count).SubItems(3) = rs_NotaCredito!no_doc
                .ListItems(.ListItems.Count).SubItems(4) = rs_NotaCredito!Rut
                .ListItems(.ListItems.Count).SubItems(5) = rs_NotaCredito!razon_social
                .ListItems(.ListItems.Count).SubItems(6) = NumFormat(rs_NotaCredito!Neto)
                .ListItems(.ListItems.Count).SubItems(7) = NumFormat(rs_NotaCredito!IVA)
                .ListItems(.ListItems.Count).SubItems(8) = NumFormat(rs_NotaCredito!Bruto)
            
                rs_NotaCredito.MoveNext
            Loop
            .ListItems.Add , , "Totales"
            .ListItems(.ListItems.Count).SubItems(6) = txtNetoNC
            .ListItems(.ListItems.Count).SubItems(7) = txtIvaNC
            .ListItems(.ListItems.Count).SubItems(8) = txtBrutoNC
            
            .ListItems.Add , , " "
            .ListItems.Add , , "Totales"
            .ListItems(.ListItems.Count).SubItems(6) = txtNetoTotal
            .ListItems(.ListItems.Count).SubItems(7) = txtIvaTotal
            .ListItems(.ListItems.Count).SubItems(8) = txtBrutototal
        End If
        
    End With
   
End Sub

Private Sub CmdHistorial_Click()
    HistorialEliminacion.Show
End Sub

Private Sub CmdSalir_Click()
    Unload Me
End Sub



Private Sub Check1_Click()
    If Check1.Value = 1 Then
        Check2.Value = 0
        Check3.Value = 0
        Check4.Value = 0
        Check2.Enabled = False
        Check3.Enabled = False
        Check4.Enabled = False
    Else
        Check2.Enabled = True
        Check3.Enabled = True
        Check4.Enabled = True
        
        
    End If
    
        
End Sub

Private Sub Form_Load()
    Aplicar_skin Me
    Sql = "SELECT fecha,tipo_doc,no_documento as No,rut_cliente,nombre_cliente,tipo_movimiento as Accion,Neto,Iva,Bruto,Forma_pago FROM doc_venta ORDER BY fecha desc"
    paso = AbreConsulta(RsVentas, Sql)
    Me.DTInicio.Value = Date
    Me.Dtfin.Value = Date
  '  ElGrid
    Me.CboAccion.ListIndex = 0
        
End Sub


Public Sub ElGrid()
   
   Set GridVentas.DataSource = RsVentas
    
    With GridVentas
        .Columns(0).Width = 1000 'fecha
        .Columns(1).Width = 1200 'tipo
        .Columns(2).Width = 800 'no
        .Columns(3).Width = 1500 'rut
        .Columns(4).Width = 2800 'nombre
        .Columns(5).Width = 700 'tipo mov
        .Columns(6).Alignment = dbgRight
        .Columns(7).Alignment = dbgRight
        .Columns(8).Alignment = dbgRight
        
        '.Columns(6).NumberFormat=
    End With
    With RsVentas
        If .RecordCount > 0 Then
            .MoveFirst
            txtNeto = 0
            TxtBruto = 0
            TxtIva = 0
            For I = 1 To .RecordCount
                txtNeto = Val(txtNeto) + IIf(IsNull(!Neto) = False, !Neto, 0)
                TxtIva = Val(TxtIva) + IIf(IsNull(!IVA) = False, !IVA, 0)
               
                'TxtBruto = Val(CDbl(TxtBruto)) + IIf(IsNull(CDbl(!Bruto)) = False, CDbl(!Bruto), 0)
                TxtBruto = Val(CDbl(TxtBruto)) + (0 & !Bruto) 'IIf(IsNull(CDbl(!Bruto)) = False, CDbl(!Bruto), 0)
                .MoveNext
            Next
            txtNeto = NumFormat(txtNeto)
            TxtIva = NumFormat(TxtIva)
            TxtBruto = NumFormat(TxtBruto)
        End If
    End With
            
    txtNetoNC = 0
    txtIvaNC = 0
    txtBrutoNC = 0
    Call AbreConsulta(rs_NotaCredito, SqlNC)
    
    If rs_NotaCredito.RecordCount > 0 Then
        
        Set GridNC.DataSource = rs_NotaCredito
        With rs_NotaCredito
            If .RecordCount > 0 Then
                .MoveFirst
        
                For I = 1 To .RecordCount
                    txtNetoNC = Val(txtNetoNC) + IIf(IsNull(!Neto) = False, !Neto, 0)
                    txtIvaNC = Val(txtIvaNC) + IIf(IsNull(!IVA) = False, !IVA, 0)
                    txtBrutoNC = Val(txtBrutoNC) + IIf(IsNull(!Bruto) = False, !Bruto, 0)
                    .MoveNext
                Next
                txtNetoNC = NumFormat(txtNetoNC)
                txtIvaNC = NumFormat(txtIvaNC)
                txtBrutoNC = NumFormat(txtBrutoNC)
            End If
        End With
        GridNC.Columns(5).Width = 2800
    End If
                   
    txtNetoTotal = NumFormat(NumReal(txtNeto) - NumReal(txtNetoNC))
    txtIvaTotal = NumFormat(NumReal(TxtIva) - NumReal(txtIvaNC))
    txtBrutototal = NumFormat(NumReal(TxtBruto) - NumReal(txtBrutoNC))
    
        
        
        
End Sub

Private Sub PicExcel_Click()

    Dim tit(2) As String
    FraProgreso.Visible = True
    tit(0) = "LIBRO VENTAS"
    tit(1) = "FECHA INFORME: " & Date & " - " & Time
    tit(2) = "                                        DESDE:" & DTInicio.Value & "   HASTA:" & Dtfin.Value
    Exportar LvLibro, tit, Me, BarraProgreso, SkProgreso
    BarraProgreso.Value = 1
    SkProgreso = "0%"
    FraProgreso.Visible = False
Exit Sub



    Set ExportarExel.AdoTemp.Recordset = RsVentas
    ExportarExel.Show 1
End Sub
Public Sub EmiteComprobanteEliminacion()
    Dim Documento As String
    Dim RutPrv As String
    Dim NumDoc As Double
    Dim Tipo_Mov As String
    Dim ValorDoc As Double
    Dim NombreProveedor As String
    Dim fPago As String
    Dim RsComprasRep As Recordset
    Dim Lfecha As String
    
   
    Lfecha = Format(Date, "yyyy-mm-dd")
    
    With GridVentas 'S    on compras
        If .Row = -1 Then Exit Sub
        .Col = 1: Documento = .Text
        .Col = 2: NumDoc = Val(.Text)
        .Col = 3: RutPrv = .Text
        .Col = 4: NombreProveedor = .Text
        .Col = 5: Tipo_Mov = .Text
        .Col = 6: ValorDoc = Val(.Text)
        .Col = 9: fPago = .Text
    End With

    Dim CantCopias As Integer
    Dialogo.Copies = 1
    
    
    Dialogo.ShowPrinter
    
    'llamamos al dialogo de impresion
    CantCopias = Dialogo.Copies
    
    Dim PosicionMo, PosicionMA, PosicionOT
    Dim cEspacios As Integer
    cEspacios = 10
    F = FreeFile '
    Dim Linea As String
   
        Printer.FontName = "Courier New"
        Printer.CurrentY = Printer.CurrentY + (1000 - Printer.CurrentY Mod 1000)
        Call TipoLetra(8, True, False, False)
        
        
        Printer.Print Space(cEspacios) & RsEmpresa  'aqui la enviamos a la impresora
        Printer.Print Space(cEspacios) & DireccionEmpresa  ' direccion
        Printer.Print Space(cEspacios) & GiroEmpresa  'giro
        Printer.Print Space(cEspacios) & RutEmpresa 'rut
        
        Call TipoLetra(14, True, False, False)
        Printer.Print Space(cEspacios) & Space(15) & "COMPROBANTE   ELIMINACION" & Space(20)  'titulo
        Call TipoLetra(12, True, False, False)
        Printer.Print Space(cEspacios) & Space(23) & "DOCUMENTO DE  V E N T A" & Space(20)  'titulo
        Call TipoLetra(10, True, False, False)
        Printer.Print Space(cEspacios) & Space(30) & Now
        
        paso = TipoLetra(8, False, False, False)
        Printer.CurrentY = Printer.CurrentY + 80
    
        Printer.Print Space(cEspacios) & "PROVEEDOR    :"; NombreProveedor
        Printer.Print Space(cEspacios) & "RUT PROVEEDOR:"; RutPrv
        Printer.Print Space(cEspacios) & "DOCUMENTO    :"; Documento
        Printer.Print Space(cEspacios) & "N� DOCUMENTO :"; NumDoc
        Printer.Print Space(cEspacios) & "VALOR NETO   :"; NumFormat(ValorDoc)
        Printer.Print Space(cEspacios) & "CONCEPTO     :"; Tipo_Mov
        Printer.Print Space(cEspacios) & "USUARIO      :"; LogUsuario
        Call TipoLetra(8, True, False, False)
        Printer.Print ""
        Printer.Print ""
        Printer.Print Space(cEspacios) & "MOTIVO ELIMINACION:" & Motivo
        
        Printer.CurrentY = Printer.CurrentY + 100
        
        
    
        paso = TipoLetra(10, True, False, False)
        
        
        Printer.CurrentY = Printer.CurrentY + 100
        
        
       Printer.Print Space(cEspacios) & "La eliminacion actualiza el stock y elimina todos los documentos"
       Printer.Print Space(cEspacios) & "asociados a la venta incluidos los abonos."
                                                                      
    
    
    
    
    Printer.DrawWidth = 1
    
    Printer.Line (400, 700)-(11000, 2550), , B 'Rectangulo Encabezado y N� Orden
    Printer.Line (400, 2550)-(11000, 3900), , B 'Rectangulo Datos del cliente
    Printer.Line (400, 3900)-(11000, 6150), , B ' Rectangulo Datos del vehiculo
    Printer.DrawWidth = 3
    Printer.NewPage 'activar lo enviado a la
    Printer.EndDoc 'impresora, o sea , que imprima lo que le hemos enviado mediante los printer
    
    
    
    
    
    Exit Sub
    
    


ErrHandler:
    ' El usuario ha hecho clic en el bot�n Cancelar
    MsgBox "La impresion ha sido cancelada"

    Exit Sub


End Sub

