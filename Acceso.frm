VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Begin VB.Form Acceso 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "ACCESO"
   ClientHeight    =   4665
   ClientLeft      =   3675
   ClientTop       =   1740
   ClientWidth     =   6900
   Icon            =   "Acceso.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Picture         =   "Acceso.frx":0442
   ScaleHeight     =   2756.232
   ScaleMode       =   0  'User
   ScaleWidth      =   6478.728
   ShowInTaskbar   =   0   'False
   Begin VB.ComboBox Combo1 
      Height          =   315
      ItemData        =   "Acceso.frx":1084
      Left            =   2250
      List            =   "Acceso.frx":1118
      TabIndex        =   16
      Text            =   "Combo1"
      Top             =   5175
      Width           =   2580
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Command1"
      Height          =   195
      Left            =   -60
      TabIndex        =   15
      Top             =   4245
      Width           =   105
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkVersion 
      Height          =   375
      Left            =   255
      OleObjectBlob   =   "Acceso.frx":138E
      TabIndex        =   14
      Top             =   4290
      Width           =   6300
   End
   Begin VB.PictureBox PicLogo 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      DrawMode        =   11  'Not Xor Pen
      DrawWidth       =   2
      ForeColor       =   &H80000008&
      Height          =   900
      Left            =   -45
      ScaleHeight     =   870
      ScaleWidth      =   6840
      TabIndex        =   13
      Top             =   15
      Width           =   6870
   End
   Begin VB.Frame LogoEmpresa 
      Caption         =   "INGRESE SUS DATOS"
      Height          =   3270
      Left            =   -30
      TabIndex        =   9
      Top             =   915
      Width           =   6825
      Begin VB.Frame Frame3 
         Caption         =   "Tipo de Conexion"
         Height          =   1485
         Left            =   1530
         TabIndex        =   12
         Top             =   1290
         Width           =   4080
         Begin VB.TextBox TxtRed 
            Height          =   285
            Left            =   1500
            TabIndex        =   4
            Text            =   "192.168.1.100"
            Top             =   690
            Width           =   2025
         End
         Begin VB.OptionButton Opt_Red 
            Caption         =   "Red Local"
            Height          =   195
            Left            =   360
            TabIndex        =   3
            Top             =   705
            Width           =   1230
         End
         Begin VB.OptionButton Opt_Local 
            Caption         =   "Local (localhost)"
            Height          =   255
            Left            =   345
            TabIndex        =   2
            Top             =   375
            Value           =   -1  'True
            Width           =   1935
         End
         Begin VB.OptionButton Opt_Remota 
            Caption         =   "Remota"
            Height          =   255
            Left            =   345
            TabIndex        =   5
            Top             =   1080
            Width           =   1080
         End
         Begin VB.TextBox TxtRemota 
            Height          =   285
            Left            =   1500
            TabIndex        =   6
            Text            =   "www.empresa.cl"
            Top             =   1005
            Width           =   2025
         End
      End
      Begin VB.TextBox txtUserName 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         Height          =   285
         Left            =   3270
         TabIndex        =   0
         Top             =   420
         Width           =   2325
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   255
         Left            =   1140
         OleObjectBlob   =   "Acceso.frx":1408
         TabIndex        =   11
         Top             =   885
         Width           =   2055
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   255
         Left            =   1065
         OleObjectBlob   =   "Acceso.frx":1472
         TabIndex        =   10
         Top             =   420
         Width           =   2145
      End
      Begin VB.TextBox txtPassword 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         Height          =   270
         IMEMode         =   3  'DISABLE
         Left            =   3285
         PasswordChar    =   "*"
         TabIndex        =   1
         Top             =   900
         Width           =   2325
      End
      Begin VB.CommandButton cmdCancel 
         Cancel          =   -1  'True
         Caption         =   "Cancelar"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   390
         Left            =   4245
         TabIndex        =   8
         Top             =   2820
         Width           =   1380
      End
      Begin VB.CommandButton cmdOK 
         Caption         =   "Aceptar"
         Default         =   -1  'True
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   390
         Left            =   1530
         TabIndex        =   7
         Top             =   2775
         Width           =   1380
      End
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   555
      OleObjectBlob   =   "Acceso.frx":14EA
      Top             =   1365
   End
End
Attribute VB_Name = "Acceso"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmdCancel_Click()
    'establecer la variable global a false
    'para indicar un inicio de sesi�n fallido
    LoginSucceeded = False
    Me.Hide
    End
End Sub

Private Sub CmdOk_Click()
    UsuarioGerencia = False
    If Len(txtPassword) = 0 Then txtPassword.SetFocus
    'comprobar si la contrase�a es correcta
    Dim Ip_Version_Actual As Long
    Ip_Version_Actual = App.Major & App.Minor & App.Revision
    SkVersion = "Version " & Ip_Version_Actual
    If Len(Me.txtUserName) = 0 Or Len(Me.txtPassword) = 0 Then Exit Sub
     
    If Me.Opt_Red.Value Or Me.Opt_Remota Then
        If Opt_Red.Value Then
            If Len(TxtRed) = 0 Then
                MsgBox "No ha ingresado una IP valida...", vbInformation
                TxtRed.SetFocus
            End If
        Else
            If Len(TxtRemota) = 0 Then
                MsgBox "No ha ingresado una IP valida...", vbInformation
                TxtRed.SetFocus
            End If
        End If
    End If
    
    If Me.Opt_Local.Value Then Sp_Servidor = "localhost"
    If Me.Opt_Red.Value Then Sp_Servidor = TxtRed
    If Me.Opt_Remota.Value Then Sp_Servidor = TxtRemota
    If Len(SG_BaseDato) = 0 Then SG_BaseDato = Combo1.Text
    'Cargamos los parametros
    Main
    
    
    If BG_FalloConexion Then
        BG_FalloConexion = False
        Exit Sub
    End If
    
    If Ip_Version_Actual < IG_Version Then
        'MsgBox "Esta utilizando una version anterior del sistema"
        Sis_NecesarioActualizar.Show 1
        End
    End If
    If Len(Me.txtUserName) > 0 And Len(Me.txtPassword) > 0 Then
        
        Sql = "SELECT usu_login,usu_nombre,u.per_id,p.per_nombre,usu_id,usu_ver_precios_de_costo vepcosto,sue_id " & _
              "FROM sis_usuarios u INNER JOIN sis_perfiles p USING(per_id) " & _
              "WHERE usu_activo='SI' AND usu_login = '" & Me.txtUserName & "' and usu_pwd = MD5('" & Me.txtPassword & "')"
    
        Consulta RSUsuarios, Sql
        If RSUsuarios.RecordCount > 0 Then
            'Usuario aceptado
            LoginSucceeded = True
          
            LogUsuario = Me.txtUserName
            LogPass = Me.txtPassword
            LogPerfil = RSUsuarios!per_id
            LogIdUsuario = RSUsuarios!usu_id
            LogSueID = RSUsuarios!sue_id
            SG_Usuario_Ve_Pcosto = RSUsuarios!vepcosto
            '14 12 2018 si es usuario gerencia puede seleccionar que sucursal trabajar
            
            If RSUsuarios!vepcosto = "NO" Then UsuarioGerencia = True
            
            Me.Hide
            
            
            
           Principal.TxtNombre = RSUsuarios!usu_nombre
           Principal.TxtPerfil = RSUsuarios!per_nombre
            
            Sp_Archivo = App.Path & "\conecta.ini"
            X = FreeFile
            Open Sp_Archivo For Output As X
                If Opt_Local.Value Then Print #X, "1"
                If Opt_Red.Value Then Print #X, "2"
                If Opt_Remota.Value Then Print #X, "3"
                Print #X, TxtRed
                Print #X, TxtRemota
                
            Close X
            
            Principal.Show
        Else
            MsgBox "La contrase�a no es v�lida. Vuelva a intentarlo", , "Inicio de sesi�n"
            txtPassword.SetFocus
            On Error Resume Next
            SendKeys "{Home}+{End}"
        End If
    End If
End Sub

Private Sub Command1_Click()
    Me.Height = Me.Height + 1000
    'Sis_BarraProgreso.Show 1
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        On Error Resume Next
        SendKeys vbKeyTab
    End If

End Sub

Private Sub Form_KeyUp(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF11 Then
        Me.txtPassword = "CLAUDIA"
        Me.txtUserName = "CLAUDIA"
       CmdOk_Click
    End If
End Sub

Private Sub Form_Load()
     
    Dim Sp_Archivo As String
    Centrar Me
    ConexionEspecial = ""
    SG_Sucursal_Activa = "TEMUCO"
    PicLogo = LoadPicture(App.Path & "\REDMAROK.JPG")
    
    Ip_Version_Actual = App.Major & "." & App.Minor & "." & App.Revision
    SkVersion = "Versi�n " & Ip_Version_Actual
    
    
    'Instruccion para Dell Maggio
    '0 empresas que ya tengan servidor MySql con otras contrase�as y usuarios
    If Dir$(App.Path & "\empresa.ini") <> "" Then
        X = FreeFile
        Open App.Path & "\empresa.ini" For Input As X
           Line Input #X, ConexionEspecial
        Close #X
    End If
    
    'Aqui colocaremos el nombre de la bd por defecto,
    'tambien se puede seleccionar del combo
    If Dir$(App.Path & "\bd.ini") <> "" Then
        X = FreeFile
        Open App.Path & "\bd.ini" For Input As X
               Line Input #X, SG_BaseDato
               Line Input #X, cualquira
                Line Input #X, SG_Sucursal_Activa
                   
        Close #X
    End If
    
    
    Text1 = Get_User_Name
    SG_Funcion_Equipo = "SI"
    Sp_Archivo = Dir(App.Path & "\funcion.ini")
    If Sp_Archivo = "" Then
        MsgBox "Falta archivo de funcion, imposible continuar...", vbExclamation
        End
    Else
        X = FreeFile
        On Error GoTo sigue
        Open App.Path & "\funcion.ini" For Input As X
            Line Input #X, Sp_Archivo
            If Len(Sp_Archivo) = 0 Then
                MsgBox "Fallo en archivo de funcion..., imposible continuar", vbExclamation
                End
            Else
                SG_Funcion_Equipo = Sp_Archivo
            End If
            Sp_Archivo = ""
        Close X
    End If
    
    
    
    
    Sp_Archivo = Dir(App.Path & "\conecta.ini")
    If Sp_Archivo = "" Then
        Opt_Local.Value = True
        'No existe
    Else
        Sp_Archivo = App.Path & "\conecta.ini"
        X = FreeFile
        On Error GoTo sigue
        Open Sp_Archivo For Input As X
            Line Input #X, Sp_Archivo
            If Val(Sp_Archivo) = 1 Then Opt_Local.Value = True
            If Val(Sp_Archivo) = 2 Then Opt_Red.Value = True
            If Val(Sp_Archivo) = 3 Then Opt_Remota.Value = True
            Line Input #X, Sp_Archivo
            TxtRed = Sp_Archivo
            Line Input #X, Sp_Archivo
            TxtRemota = Sp_Archivo
        Close X
    End If
    Close X
    
    
    Centrar Me
    Combo1.ListIndex = 0
    
    'colocaremos la direccion de la bd amazon
    'y el nombre de la bd
    Sp_Archivo = Dir(App.Path & "\cnx\bd.ini")
    If Sp_Archivo = "" Then
        'nada de nada, seguimos tal cual
    Else
        Sp_Archivo = App.Path & "\cnx\bd.ini"
        X = FreeFile
        On Error GoTo sigue
        Opt_Remota.Value = True
        Open Sp_Archivo For Input As X
            Line Input #X, Sp_Archivo
            Combo1.Text = Sp_Archivo
            Line Input #X, Sp_Archivo
            TxtRemota = Sp_Archivo
            Line Input #X, SG_Sucursal_Activa
            SG_Sucursal_Activa = UCase(SG_Sucursal_Activa)

        Close X
    
    End If
    
    
    
    'Para pruebas, colocaresmos las credenciales de la bd paara leerlas de un archivo
    '22 Nov 2016
    UserDb = "fadmin_fadmin"
    PwdDb = "livesoft01"
  '  Sp_Archivo = Dir(App.Path & "\cnx\bdx.ini")
  '  If Sp_Archivo = "" Then
        'nada de nada, seguimos tal cual
 '   Else
 '       Sp_Archivo = App.Path & "\cnx\bdx.ini"
 '       X = FreeFile
 '       On Error GoTo sigue
 '
 '       Open Sp_Archivo For Input As X
     '       Line Input #X, UserDb
  '          Line Input #X, PwdDb
  '      Close X
  '
  '  End If
    
    
    
    Aplicar_skin Me
    
    Exit Sub
sigue:
    MsgBox Err.Description & vbNewLine & Err.Number
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    End
End Sub




Private Sub txtPassword_GotFocus()
    En_Foco Me.ActiveControl
End Sub

Private Sub TxtPassword_KeyPress(KeyAscii As Integer)
     KeyAscii = Asc(UCase(Chr(KeyAscii)))
     If KeyAscii = 39 Then KeyAscii = 0
End Sub
Private Sub TxtRemota_GotFocus()
    En_Foco Me.ActiveControl
End Sub

Private Sub txtUserName_GotFocus()
    En_Foco Me.ActiveControl
End Sub

Private Sub txtUserName_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        SendKeys vbKeyTab
    End If
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
    If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub txtUserName_KeyUp(KeyCode As Integer, Shift As Integer)
    If KeyCode = 13 Then
        SendKeys vbKeyTab
    End If
End Sub
