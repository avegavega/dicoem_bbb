VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{0ECD9B60-23AA-11D0-B351-00A0C9055D8E}#6.0#0"; "MSHFLXGD.OCX"
Begin VB.Form HistoricoFacturas 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Historial de Facturas Ventas Directas"
   ClientHeight    =   7680
   ClientLeft      =   480
   ClientTop       =   1575
   ClientWidth     =   12045
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   ScaleHeight     =   7680
   ScaleWidth      =   12045
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton Command1 
      Caption         =   "Exportar"
      Height          =   1215
      Left            =   10800
      Picture         =   "HistoricoFacturas.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   9
      Top             =   720
      Width           =   975
   End
   Begin VB.Frame Frame3 
      Caption         =   "Tipo documento"
      Height          =   1455
      Left            =   4560
      TabIndex        =   19
      Top             =   600
      Width           =   2895
      Begin VB.OptionButton Option3 
         Caption         =   "S�lo Facturas"
         Height          =   255
         Left            =   480
         TabIndex        =   6
         Top             =   1080
         Width           =   2055
      End
      Begin VB.OptionButton Option2 
         Caption         =   "S�lo Boletas"
         Height          =   255
         Left            =   480
         TabIndex        =   5
         Top             =   720
         Width           =   2055
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Todos"
         Height          =   255
         Left            =   480
         TabIndex        =   4
         Top             =   360
         Value           =   -1  'True
         Width           =   2055
      End
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   9285
      OleObjectBlob   =   "HistoricoFacturas.frx":0E54
      Top             =   7050
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
      Height          =   375
      Left            =   840
      OleObjectBlob   =   "HistoricoFacturas.frx":1088
      TabIndex        =   18
      Top             =   0
      Width           =   8415
   End
   Begin VB.Frame Frame1 
      Caption         =   "Filtro"
      Height          =   1455
      Left            =   120
      TabIndex        =   16
      Top             =   600
      Width           =   4455
      Begin VB.TextBox TxtBusqueda 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   600
         TabIndex        =   0
         Top             =   420
         Width           =   3615
      End
      Begin VB.OptionButton OpCliente 
         Caption         =   "Cliente"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   600
         TabIndex        =   1
         Top             =   840
         Value           =   -1  'True
         Width           =   1215
      End
      Begin VB.CommandButton CmdBusca 
         Caption         =   "Buscar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   1920
         TabIndex        =   2
         Top             =   840
         Width           =   1095
      End
      Begin VB.CommandButton CmdTodos 
         Caption         =   "Todos"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   3120
         TabIndex        =   3
         Top             =   840
         Width           =   1095
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   255
         Left            =   1080
         OleObjectBlob   =   "HistoricoFacturas.frx":1116
         TabIndex        =   17
         Top             =   200
         Width           =   2295
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Ordenar Por"
      Height          =   1455
      Left            =   7440
      TabIndex        =   14
      Top             =   600
      Width           =   3255
      Begin VB.ComboBox CboOrden 
         Height          =   315
         Left            =   480
         Style           =   2  'Dropdown List
         TabIndex        =   7
         Top             =   240
         Width           =   2415
      End
      Begin VB.ComboBox CboDireccion 
         Height          =   315
         ItemData        =   "HistoricoFacturas.frx":1195
         Left            =   1320
         List            =   "HistoricoFacturas.frx":119F
         Style           =   2  'Dropdown List
         TabIndex        =   8
         Top             =   720
         Width           =   1695
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   255
         Left            =   240
         OleObjectBlob   =   "HistoricoFacturas.frx":11BC
         TabIndex        =   15
         Top             =   720
         Width           =   855
      End
   End
   Begin VB.CommandButton CmdSeleccionar 
      Caption         =   "Seleccionar"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   240
      TabIndex        =   11
      Top             =   7080
      Width           =   1935
   End
   Begin VB.CommandButton CmdSalir 
      Caption         =   "&Salir"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   10680
      TabIndex        =   13
      Top             =   7080
      Width           =   1215
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
      Height          =   255
      Left            =   2400
      OleObjectBlob   =   "HistoricoFacturas.frx":122C
      TabIndex        =   12
      Top             =   7080
      Width           =   6615
   End
   Begin MSHierarchicalFlexGridLib.MSHFlexGrid MshOt 
      Height          =   4815
      Left            =   240
      TabIndex        =   10
      Top             =   2160
      Width           =   11655
      _ExtentX        =   20558
      _ExtentY        =   8493
      _Version        =   393216
      FixedCols       =   0
      HighLight       =   2
      SelectionMode   =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _NumberOfBands  =   1
      _Band(0).Cols   =   2
   End
End
Attribute VB_Name = "HistoricoFacturas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim AdoOrden As Recordset
Dim ConsultaActual As String

Private Sub CboDireccion_Click()
    If CboOrden.ListIndex = -1 Then Exit Sub
    MshOt.Col = CboOrden.ListIndex
    MshOt.Sort = Me.CboDireccion.ListIndex + 1
End Sub

Private Sub CboOrden_Click()
    If Me.CboOrden.ListIndex = 1 Then
        paso = FiltraRSFecha("x")
    Else
    
        
        MshOt.Col = CboOrden.ListIndex
        MshOt.Sort = Me.CboDireccion.ListIndex + 1
    End If
End Sub

Private Sub CmdBusca_Click()
        
    If Len(Me.txtBusqueda.Text) = 0 Then Exit Sub
    
    paso = FiltraRS(Me.txtBusqueda.Text)
    
    Exit Sub
    Filtro = IIf(Me.OpCliente.Value = True, "Nombre_Cliente", "Patente") & " LIKE '" & Me.txtBusqueda.Text & "*'"
    AdoOrden.Filter = 0
    AdoOrden.Filter = Filtro
    
        
                          
                
            
            
    
End Sub

Private Sub CmdSalir_Click()
    Unload Me
End Sub

Private Sub CmdSeleccionar_Click()
    MshOt_DblClick
End Sub



Private Sub CmdTodos_Click()
        paso = CargarOTS("No_Factura")
End Sub

Private Sub Command1_Click()
    Set ExportarExel.AdoTemp.Recordset = AdoOrden
    ExportarExel.Show 1
End Sub

Private Sub Form_Load()
    Aplicar_skin Me
    Me.CboDireccion.ListIndex = 0
    paso = CargarOTS("No_Factura")
        
    MshOt.Row = 0
    For I = 0 To Me.MshOt.Cols - 1
        MshOt.Col = I
        Me.CboOrden.AddItem MshOt.Text
    Next
End Sub



Private Sub MshOt_DblClick()
    
    
    With MshOt
        If .Row = 0 Then Exit Sub
        .Col = 0
       Nfac = Val(.Text)
       Nbol = Val(.Text)
        .Col = 2
        
        If .Text = "Nula" Then
            MsgBox "Esta Orden fue eliminada, no es posible editarla", vbInformation
            Exit Sub
        End If
        If FormularioCargado("vtadirecta") = True Then
            MsgBox "Ya est� creando o revisando una " & Chr(34) & " Venta directa" & Chr(34)
            Exit Sub
        End If
        
        
        EstadoOT = "VistaVD"
        VDdoc = .TextMatrix(.Row, 7)
    End With
    
    
    
    'VDdoc = "Factura"
    EstadoOT = "VistaVD"
    
    
    VtaDirecta.Caption = VDdoc & " emitida N�" & Nfac
  
    VtaDirecta.Show
                
    paso = CargarOTS("No_Factura", ConsultaActual) 'Renueva la lista
    
End Sub


Private Sub Option1_Click()
    ConsultaActual = "SELECT No_Documento,Fecha,Rut_Cliente,Nombre_Cliente,Neto,Iva,Bruto,Tipo_doc,Forma_pago,Cod_Vendedor,Nombre_Vendedor,Comision_Vendedor,Nombre_Admin,Comision_Admin,doc_id " & _
    "FROM DOC_VENTA WHERE Tipo_Movimiento = 'VD' ORDER BY NO_documento desc"
   Call CargarOTS(X, ConsultaActual)
   
End Sub

Private Sub Option2_Click()
    ConsultaActual = "SELECT no_documento,fecha,rut_cliente,nombre_cliente,neto,iva,bruto,tipo_doc,forma_pago,cod_vendedor,nombre_vendedor,comision_vendedor,nombre_admin,comision_admin,doc_id " & _
    "FROM doc_venta WHERE tipo_movimiento = 'VD' AND tipo_doc = 'BOLETA' ORDER BY no_documento desc"
    Call CargarOTS(X, ConsultaActual)
End Sub

Private Sub Option3_Click()
    ConsultaActual = "SELECT no_documento,fecha,rut_cliente,nombre_cliente,neto,iva,bruto,tipo_doc,forma_pago,cod_vendedor,nombre_vendedor,comision_vendedor,nombre_admin,comision_admin,doc_id " & _
    "FROM doc_venta WHERE tipo_movimiento = 'VD' AND tipo_doc = 'FACTURA' ORDER BY no_documento desc"
    Call CargarOTS(X, ConsultaActual)
End Sub

Private Sub TxtBusqueda_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
  
End Sub

Function CargarOTS(Campo, Optional ByVal Consultax As String)
            
            If Consultax <> Empty Then
                Sql = Consultax
            Else
            'Sql = "SELECT  FROM OT WHERE No_Orden = " & NumeroOrden
            
            Sql = "SELECT no_documento,fecha,rut_cliente,nombre_cliente,neto,iva,bruto,tipo_doc,forma_pago,cod_vendedor,nombre_vendedor,comision_vendedor,nombre_admin,comision_admin,doc_id " & _
            "FROM doc_venta WHERE tipo_movimiento = 'VD' ORDER BY no_documento desc"
            ConsultaActual = Sql
            End If
            
            
            Consulta AdoOrden, Sql
           
            
            With MshOt
                .FixedCols = 0
                Set .DataSource = AdoOrden
                .Col = 0
                For I = 1 To .Rows - 1
                    .Row = I
                    .CellFontBold = True
                    .CellBackColor = &HFFFF80
                    .CellFontSize = .CellFontSize + 2
                Next
                .Col = 1
                For I = 1 To .Rows - 1
                    .Row = I
                    .Text = Format(.Text, "DD-MM-YYYY")
                Next
                .FixedCols = 1
                .ColWidth(2) = 1200
                .ColWidth(3) = 2800
                .ColWidth(4) = 1000
                .ColWidth(5) = 1500
               
            End With
End Function
Function FiltraRS(Nombre As String)
            
          '  Sql = "SELECT * FROM DOC_VENTA WHERE Tipo_Movimiento = 'VD' AND Nombre_Cliente Like '" & Nombre & "%' ORDER BY No_documento desc"
            
            Sql = "SELECT no_documento,fecha,rut_cliente,nombre_cliente,neto,iva,bruto,tipo_doc,forma_pago,cod_vendedor,nombre_vendedor,comision_vendedor,nombre_admin,comision_admin,doc_id " & _
            "FROM doc_venta WHERE tipo_movimiento = 'VD' AND nombre_cliente Like '" & Nombre & "%' ORDER BY no_documento desc"
            paso = Consulta(AdoOrden, Sql)
            With MshOt
                .ClearStructure
               
                .FixedCols = 0
                Set .DataSource = AdoOrden
                .Col = 0
                For I = 1 To .Rows - 1
                    .Row = I
                    .CellFontBold = True
                    .CellBackColor = &HFFFF80
                    .CellFontSize = .CellFontSize + 2
                Next
                .Col = 1
                For I = 1 To .Rows - 1
                    .Row = I
                    .Text = Format(.Text, "DD-MM-YYYY")
                Next
                .FixedCols = 1
                .ColWidth(2) = 1200
                .ColWidth(3) = 2800
                .ColWidth(4) = 1
                .ColWidth(5) = 1500
               
            End With
End Function

Function FiltraRSFecha(Nombre As String)
            
          '  Sql = "SELECT * FROM DOC_VENTA WHERE Tipo_Movimiento = 'VD' ORDER BY Fecha desc"
            
            Sql = "SELECT no_documento,fecha,rut_cliente,nombre_cliente,neto,iva,bruto,tipo_doc,forma_pago,cod_vendedor,nombre_vendedor,comision_vendedor,nombre_admin,comision_admin,doc_id " & _
            "FROM doc_venta WHERE tipo_movimiento = 'VD' ORDER BY fecha desc"
            paso = Consulta(AdoOrden, Sql)
            
            With MshOt
                .ClearStructure
            
                .FixedCols = 0
                Set .DataSource = AdoOrden
                .Col = 0
                For I = 1 To .Rows - 1
                    .Row = I
                    .CellFontBold = True
                    .CellBackColor = &HFFFF80
                    .CellFontSize = .CellFontSize + 2
                Next
               .Col = 1
                For I = 1 To .Rows - 1
                    .Row = I
                    .Text = Format(.Text, "DD-MM-YYYY")
                Next
               
                .FixedCols = 1
                .ColWidth(2) = 1200
                .ColWidth(3) = 2800
                .ColWidth(4) = 1
                .ColWidth(5) = 1500
               
            End With
End Function

