VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form dicoem_mantenimiento_equipos 
   Caption         =   "Mantenimiento Preventivo / Rutina de trabajo"
   ClientHeight    =   10530
   ClientLeft      =   2625
   ClientTop       =   1785
   ClientWidth     =   16380
   LinkTopic       =   "Form1"
   ScaleHeight     =   10530
   ScaleWidth      =   16380
   Begin VB.Frame Frame1 
      Caption         =   "Identificacion"
      Height          =   10080
      Left            =   105
      TabIndex        =   0
      Top             =   150
      Width           =   15645
      Begin VB.CommandButton Command1 
         Caption         =   "Generar Mantencion"
         Height          =   390
         Left            =   210
         TabIndex        =   40
         Top             =   9615
         Width           =   2805
      End
      Begin VB.Frame Frame4 
         Caption         =   "Frame4"
         Height          =   3135
         Left            =   195
         TabIndex        =   33
         Top             =   6405
         Width           =   15195
         Begin VB.ComboBox CboInsumos 
            Height          =   315
            Left            =   2280
            TabIndex        =   39
            Text            =   "Podria buscar por descripcion aqui."
            Top             =   210
            Width           =   9900
         End
         Begin VB.CommandButton CmdInsOk 
            Caption         =   "Ok"
            Height          =   405
            Left            =   14445
            TabIndex        =   38
            Top             =   510
            Width           =   600
         End
         Begin VB.TextBox TxtInsCantidad 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   390
            Left            =   12195
            TabIndex        =   37
            Top             =   525
            Width           =   2220
         End
         Begin VB.TextBox TxtInsDescripcion 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   390
            Left            =   2280
            TabIndex        =   36
            Top             =   525
            Width           =   9900
         End
         Begin VB.TextBox TxtInsCodigo 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   390
            Left            =   210
            TabIndex        =   35
            Top             =   525
            Width           =   2040
         End
         Begin MSComctlLib.ListView LvInsumos 
            Height          =   2040
            Left            =   210
            TabIndex        =   34
            Top             =   915
            Width           =   14850
            _ExtentX        =   26194
            _ExtentY        =   3598
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            FullRowSelect   =   -1  'True
            GridLines       =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            NumItems        =   4
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Text            =   "Id"
               Object.Width           =   0
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Text            =   "Codigo"
               Object.Width           =   3607
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   2
               Text            =   "Nombre"
               Object.Width           =   17462
            EndProperty
            BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   3
               Object.Tag             =   "N109"
               Text            =   "Cantidad"
               Object.Width           =   3916
            EndProperty
         End
      End
      Begin VB.Frame Frame2 
         Caption         =   "Actividades a realizar"
         Height          =   2835
         Left            =   210
         TabIndex        =   31
         Top             =   3735
         Width           =   15180
         Begin MSComctlLib.ListView LvDetalle 
            Height          =   2385
            Left            =   180
            TabIndex        =   32
            Top             =   300
            Width           =   14880
            _ExtentX        =   26247
            _ExtentY        =   4207
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            FullRowSelect   =   -1  'True
            GridLines       =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   0
            NumItems        =   2
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Object.Width           =   794
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Text            =   "Nombre"
               Object.Width           =   21167
            EndProperty
         End
      End
      Begin VB.Frame Frame3 
         Caption         =   "Otros datos - tiempos y horas"
         Height          =   2160
         Left            =   210
         TabIndex        =   9
         Top             =   1680
         Width           =   15180
         Begin VB.ComboBox CboClaseMantenimiento 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            ItemData        =   "dicoem_mantenimiento_equipos.frx":0000
            Left            =   6885
            List            =   "dicoem_mantenimiento_equipos.frx":000D
            Style           =   2  'Dropdown List
            TabIndex        =   43
            ToolTipText     =   "Seleccione Documento de  venta. Ventas con Retenci�n, cuando el contribuyente recibe factura de terceros "
            Top             =   1230
            Width           =   2280
         End
         Begin VB.ComboBox CboTipoTrabajo 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            ItemData        =   "dicoem_mantenimiento_equipos.frx":0024
            Left            =   6885
            List            =   "dicoem_mantenimiento_equipos.frx":0031
            Style           =   2  'Dropdown List
            TabIndex        =   42
            ToolTipText     =   "Seleccione Documento de  venta. Ventas con Retenci�n, cuando el contribuyente recibe factura de terceros "
            Top             =   855
            Width           =   2280
         End
         Begin VB.TextBox Text7 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   390
            Left            =   12315
            TabIndex        =   30
            Top             =   1380
            Width           =   2040
         End
         Begin VB.TextBox Text2 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   390
            Left            =   12315
            TabIndex        =   29
            Top             =   975
            Width           =   2040
         End
         Begin VB.ComboBox CboPrioridad 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            ItemData        =   "dicoem_mantenimiento_equipos.frx":0048
            Left            =   6870
            List            =   "dicoem_mantenimiento_equipos.frx":0055
            Style           =   2  'Dropdown List
            TabIndex        =   20
            ToolTipText     =   "Seleccione Documento de  venta. Ventas con Retenci�n, cuando el contribuyente recibe factura de terceros "
            Top             =   450
            Width           =   2295
         End
         Begin VB.TextBox Text6 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H00E0E0E0&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   390
            Left            =   2880
            MultiLine       =   -1  'True
            TabIndex        =   15
            TabStop         =   0   'False
            Top             =   1635
            Width           =   2040
         End
         Begin VB.TextBox Text5 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H00E0E0E0&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   390
            Left            =   2880
            MultiLine       =   -1  'True
            TabIndex        =   11
            TabStop         =   0   'False
            Top             =   1230
            Width           =   2040
         End
         Begin VB.TextBox Text3 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   390
            Left            =   2865
            TabIndex        =   10
            Top             =   420
            Width           =   2040
         End
         Begin ACTIVESKINLibCtl.SkinLabel Sknnum 
            Height          =   300
            Index           =   4
            Left            =   975
            OleObjectBlob   =   "dicoem_mantenimiento_equipos.frx":006C
            TabIndex        =   12
            Top             =   495
            Width           =   1875
         End
         Begin ACTIVESKINLibCtl.SkinLabel Sknnum 
            Height          =   300
            Index           =   5
            Left            =   420
            OleObjectBlob   =   "dicoem_mantenimiento_equipos.frx":00E2
            TabIndex        =   13
            Top             =   870
            Width           =   2430
         End
         Begin ACTIVESKINLibCtl.SkinLabel Sknnum 
            Height          =   300
            Index           =   6
            Left            =   390
            OleObjectBlob   =   "dicoem_mantenimiento_equipos.frx":015E
            TabIndex        =   14
            Top             =   1260
            Width           =   2460
         End
         Begin ACTIVESKINLibCtl.SkinLabel Sknnum 
            Height          =   300
            Index           =   7
            Left            =   465
            OleObjectBlob   =   "dicoem_mantenimiento_equipos.frx":01E2
            TabIndex        =   16
            Top             =   1680
            Width           =   2385
         End
         Begin ACTIVESKINLibCtl.SkinLabel Sknnum 
            Height          =   300
            Index           =   8
            Left            =   4950
            OleObjectBlob   =   "dicoem_mantenimiento_equipos.frx":0266
            TabIndex        =   17
            Top             =   525
            Width           =   1875
         End
         Begin ACTIVESKINLibCtl.SkinLabel Sknnum 
            Height          =   300
            Index           =   9
            Left            =   4395
            OleObjectBlob   =   "dicoem_mantenimiento_equipos.frx":02DA
            TabIndex        =   18
            Top             =   900
            Width           =   2430
         End
         Begin ACTIVESKINLibCtl.SkinLabel Sknnum 
            Height          =   300
            Index           =   10
            Left            =   4365
            OleObjectBlob   =   "dicoem_mantenimiento_equipos.frx":035A
            TabIndex        =   19
            Top             =   1290
            Width           =   2460
         End
         Begin ACTIVESKINLibCtl.SkinLabel Sknnum 
            Height          =   300
            Index           =   11
            Left            =   10350
            OleObjectBlob   =   "dicoem_mantenimiento_equipos.frx":03D6
            TabIndex        =   21
            Top             =   210
            Width           =   1875
         End
         Begin ACTIVESKINLibCtl.SkinLabel Sknnum 
            Height          =   300
            Index           =   13
            Left            =   9825
            OleObjectBlob   =   "dicoem_mantenimiento_equipos.frx":0450
            TabIndex        =   22
            Top             =   525
            Width           =   2460
         End
         Begin MSComCtl2.DTPicker DtRecHora 
            Height          =   360
            Left            =   13755
            TabIndex        =   23
            Top             =   180
            Width           =   1365
            _ExtentX        =   2408
            _ExtentY        =   635
            _Version        =   393216
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Format          =   94699522
            CurrentDate     =   43103
         End
         Begin MSComCtl2.DTPicker DtRecFecha 
            Height          =   375
            Left            =   12345
            TabIndex        =   24
            Top             =   180
            Width           =   1380
            _ExtentX        =   2434
            _ExtentY        =   661
            _Version        =   393216
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Format          =   94699521
            CurrentDate     =   43103
         End
         Begin MSComCtl2.DTPicker DTPicker1 
            Height          =   345
            Left            =   13755
            TabIndex        =   25
            Top             =   585
            Width           =   1395
            _ExtentX        =   2461
            _ExtentY        =   609
            _Version        =   393216
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Format          =   94699522
            CurrentDate     =   43103
         End
         Begin MSComCtl2.DTPicker DTPicker2 
            Height          =   360
            Left            =   12330
            TabIndex        =   26
            Top             =   585
            Width           =   1395
            _ExtentX        =   2461
            _ExtentY        =   635
            _Version        =   393216
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Format          =   94699521
            CurrentDate     =   43103
         End
         Begin ACTIVESKINLibCtl.SkinLabel Sknnum 
            Height          =   300
            Index           =   15
            Left            =   9735
            OleObjectBlob   =   "dicoem_mantenimiento_equipos.frx":04CC
            TabIndex        =   27
            Top             =   945
            Width           =   2460
         End
         Begin ACTIVESKINLibCtl.SkinLabel Sknnum 
            Height          =   300
            Index           =   16
            Left            =   9810
            OleObjectBlob   =   "dicoem_mantenimiento_equipos.frx":0538
            TabIndex        =   28
            Top             =   1335
            Width           =   2385
         End
         Begin MSComCtl2.DTPicker DtFechaProg 
            Height          =   375
            Left            =   2865
            TabIndex        =   41
            Top             =   825
            Width           =   1380
            _ExtentX        =   2434
            _ExtentY        =   661
            _Version        =   393216
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Format          =   94699521
            CurrentDate     =   43103
         End
      End
      Begin VB.TextBox TxtOT 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   18
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   465
         Left            =   420
         TabIndex        =   7
         Top             =   750
         Width           =   2910
      End
      Begin VB.TextBox Text1 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   15.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   465
         Left            =   7980
         MultiLine       =   -1  'True
         TabIndex        =   3
         TabStop         =   0   'False
         Top             =   1155
         Width           =   6855
      End
      Begin VB.TextBox TxtDescripcion 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   15.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   465
         Left            =   7980
         MultiLine       =   -1  'True
         TabIndex        =   2
         TabStop         =   0   'False
         Top             =   675
         Width           =   6855
      End
      Begin VB.TextBox TxtCodigo 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   18
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   465
         Left            =   7980
         TabIndex        =   1
         Top             =   195
         Width           =   2910
      End
      Begin ACTIVESKINLibCtl.SkinLabel Sknnum 
         Height          =   300
         Index           =   0
         Left            =   5970
         OleObjectBlob   =   "dicoem_mantenimiento_equipos.frx":05C0
         TabIndex        =   4
         Top             =   270
         Width           =   1875
      End
      Begin ACTIVESKINLibCtl.SkinLabel Sknnum 
         Height          =   300
         Index           =   1
         Left            =   5415
         OleObjectBlob   =   "dicoem_mantenimiento_equipos.frx":063C
         TabIndex        =   5
         Top             =   735
         Width           =   2430
      End
      Begin ACTIVESKINLibCtl.SkinLabel Sknnum 
         Height          =   300
         Index           =   2
         Left            =   6225
         OleObjectBlob   =   "dicoem_mantenimiento_equipos.frx":06C0
         TabIndex        =   6
         Top             =   1185
         Width           =   1620
      End
      Begin ACTIVESKINLibCtl.SkinLabel Sknnum 
         Height          =   300
         Index           =   3
         Left            =   1410
         OleObjectBlob   =   "dicoem_mantenimiento_equipos.frx":072A
         TabIndex        =   8
         Top             =   435
         Width           =   1875
      End
   End
   Begin VB.Timer Timer1 
      Interval        =   100
      Left            =   525
      Top             =   15
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   0
      OleObjectBlob   =   "dicoem_mantenimiento_equipos.frx":079A
      Top             =   0
   End
End
Attribute VB_Name = "dicoem_mantenimiento_equipos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub CboInsumos_Click()
    If CboInsumos.ListIndex = -1 Then Exit Sub
    
    Sql = "SELECT ins_codigo1 " & _
            "FROM inv_insumos " & _
            "WHERE ins_id=" & CboInsumos.ItemData(CboInsumos.ListIndex)
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        TxtInsCodigo.Tag = CboInsumos.ItemData(CboInsumos.ListIndex)
        TxtInsCodigo = RsTmp!ins_codigo1
        TxtInsDescripcion = CboInsumos.Text
        TxtInsCantidad.SetFocus
    End If
    
End Sub

Private Sub CmdInsOk_Click()
    LvInsumos.ListItems.Add , , TxtInsCodigo.Tag
    LvInsumos.ListItems(LvInsumos.ListItems.Count).SubItems(1) = TxtInsCodigo
     LvInsumos.ListItems(LvInsumos.ListItems.Count).SubItems(2) = TxtInsDescripcion
     LvInsumos.ListItems(LvInsumos.ListItems.Count).SubItems(3) = TxtInsCantidad
End Sub

Private Sub Form_Load()
    Centrar Me
    Skin2 Me, , 4
    Sql = "SELECT act_id,act_nombre " & _
            "FROM inv_actividades_mantenciones " & _
            "WHERE act_activo='SI'"
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LVDetalle, True, True, True, False
    LLenarCombo CboInsumos, "ins_nombre", "ins_id", "inv_insumos", "ins_activo='SI'", "ins_nombre"
    LLenarCombo Me.CboTipoTrabajo, "tra_nombre", "tra_id", "dicoem_tipo_trabajo", "tra_activo='SI'"
    CboTipoTrabajo.ListIndex = 0
    LLenarCombo Me.CboClaseMantenimiento, "tra_nombre", "tra_id", "dicoem_tipo_trabajo", "tra_activo='SI'"
    CboClaseMantenimiento.ListIndex = 0
End Sub

Private Sub Timer1_Timer()
    On Error Resume Next
    TxtOT.SetFocus
    Timer1.Enabled = False
End Sub



Private Sub TxtCodigo_Validate(Cancel As Boolean)
    Sql = "SELECT descripcion " & _
            "FROM maestro_productos " & _
            "WHERE pro_codigo_interno='" & TxtCodigo & "'"
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        TxtDescripcion = RsTmp!Descripcion
    End If
    
    'Ahora buscar si est� disponible o en alguna obra
    
End Sub

Private Sub TxtInsCantidad_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
End Sub

Private Sub TxtInsCodigo_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
    
End Sub

Private Sub TxtInsCodigo_Validate(Cancel As Boolean)
    If Len(TxtInsCodigo) > 0 Then
        Sql = "SELECT ins_id, ins_nombre " & _
                "FROM inv_insumos " & _
                "WHERE ins_codigo1='" & TxtInsCodigo & "' OR ins_codigo2='" & TxtInsCodigo & "'"
                
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
            TxtInsDescripcion = RsTmp!ins_nombre
            TxtInsCodigo.Tag = RsTmp!ins_id
        Else
            
        End If
    
    End If
End Sub
