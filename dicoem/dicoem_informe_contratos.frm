VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{28D47522-CF84-11D1-834C-00A0249F0C28}#1.0#0"; "gif89.dll"
Object = "{DA729E34-689F-49EA-A856-B57046630B73}#1.0#0"; "progressbar-xp.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form dicoem_informe_recepciones 
   Caption         =   "Informe Recepciones"
   ClientHeight    =   9810
   ClientLeft      =   2160
   ClientTop       =   945
   ClientWidth     =   19170
   LinkTopic       =   "Form1"
   ScaleHeight     =   9810
   ScaleWidth      =   19170
   Begin Proyecto2.XP_ProgressBar BarraProgreso 
      Height          =   495
      Left            =   6285
      TabIndex        =   15
      Top             =   4185
      Visible         =   0   'False
      Width           =   8460
      _ExtentX        =   14923
      _ExtentY        =   873
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BrushStyle      =   0
      Color           =   16777088
      Scrolling       =   1
      ShowText        =   -1  'True
   End
   Begin VB.CommandButton CmdRetornar 
      Caption         =   "Retornar"
      Height          =   405
      Left            =   17385
      TabIndex        =   7
      Top             =   9405
      Width           =   1590
   End
   Begin VB.Frame FrmLoad 
      Height          =   1950
      Left            =   9060
      TabIndex        =   2
      Top             =   3870
      Visible         =   0   'False
      Width           =   2805
      Begin GIF89LibCtl.Gif89a Gif89a1 
         Height          =   1425
         Left            =   210
         OleObjectBlob   =   "dicoem_informe_contratos.frx":0000
         TabIndex        =   3
         Top             =   285
         Width           =   2445
      End
   End
   Begin VB.Timer Timer1 
      Interval        =   100
      Left            =   525
      Top             =   15
   End
   Begin VB.Frame Frame2 
      Caption         =   "Recepciones"
      Height          =   8745
      Left            =   540
      TabIndex        =   0
      Top             =   555
      Width           =   18420
      Begin VB.CommandButton cmdExportar 
         Caption         =   "Excel"
         Height          =   315
         Left            =   16005
         TabIndex        =   14
         Top             =   5490
         Width           =   1950
      End
      Begin VB.Frame Frame3 
         Caption         =   "Por rangos de fecha"
         Height          =   1365
         Left            =   14895
         TabIndex        =   10
         Top             =   285
         Width           =   2925
         Begin VB.CommandButton CmdRango 
            Caption         =   "Consultar"
            Height          =   480
            Left            =   150
            TabIndex        =   11
            Top             =   795
            Width           =   2700
         End
         Begin MSComCtl2.DTPicker DtDesde 
            CausesValidation=   0   'False
            Height          =   285
            Left            =   150
            TabIndex        =   12
            Top             =   420
            Width           =   1350
            _ExtentX        =   2381
            _ExtentY        =   503
            _Version        =   393216
            Format          =   94830593
            CurrentDate     =   41001
         End
         Begin MSComCtl2.DTPicker DtHasta 
            Height          =   285
            Left            =   1500
            TabIndex        =   13
            Top             =   420
            Width           =   1305
            _ExtentX        =   2302
            _ExtentY        =   503
            _Version        =   393216
            Format          =   94830593
            CurrentDate     =   41001
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "Articulos de la recepcion"
         Height          =   2715
         Left            =   240
         TabIndex        =   8
         Top             =   5910
         Width           =   17745
         Begin MSComctlLib.ListView LvProductos 
            Height          =   2205
            Left            =   195
            TabIndex        =   9
            Top             =   330
            Width           =   17325
            _ExtentX        =   30559
            _ExtentY        =   3889
            View            =   3
            LabelWrap       =   0   'False
            HideSelection   =   0   'False
            FullRowSelect   =   -1  'True
            GridLines       =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   12
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Text            =   "Id Detalle"
               Object.Width           =   1764
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Text            =   "Numero Contrato"
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   2
               Text            =   "Codigo Producto"
               Object.Width           =   0
            EndProperty
            BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   3
               Text            =   "Codigo"
               Object.Width           =   2822
            EndProperty
            BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   4
               Text            =   "Descripcion"
               Object.Width           =   7056
            EndProperty
            BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   5
               Object.Tag             =   "N100"
               Text            =   "Valor"
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   6
               Text            =   "Estado Producto"
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   7
               Text            =   "Observacion"
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   8
               Text            =   "Numero Recepcion"
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   9
               Text            =   "Nombre Receptor"
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   10
               Text            =   "Fecha Recepcion"
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(12) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   11
               Text            =   "Hora Recepcion"
               Object.Width           =   2540
            EndProperty
         End
      End
      Begin VB.CommandButton CmdBuscaCliente 
         Caption         =   "Buscar Recepcion"
         Height          =   345
         Left            =   2040
         TabIndex        =   6
         Top             =   555
         Width           =   2580
      End
      Begin VB.TextBox TxtNroRecepcion 
         Height          =   345
         Left            =   405
         TabIndex        =   5
         Top             =   540
         Width           =   1605
      End
      Begin MSComctlLib.ListView LvDetalle 
         Height          =   3705
         Left            =   225
         TabIndex        =   1
         Top             =   1725
         Width           =   17730
         _ExtentX        =   31274
         _ExtentY        =   6535
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   7
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "N109"
            Text            =   "Nro Recepcion"
            Object.Width           =   1235
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "F1000"
            Text            =   "Fecha Recep."
            Object.Width           =   2293
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "T1000"
            Text            =   "Hora Recep"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   3
            Object.Tag             =   "N109"
            Text            =   "Nro Contrato"
            Object.Width           =   2293
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Object.Tag             =   "T1000"
            Text            =   "Nombre Cliente"
            Object.Width           =   7056
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Object.Tag             =   "T4000"
            Text            =   "Obra"
            Object.Width           =   7056
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   6
            Object.Tag             =   "T1000"
            Text            =   "Observaciones"
            Object.Width           =   7056
         EndProperty
      End
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   0
      OleObjectBlob   =   "dicoem_informe_contratos.frx":0086
      Top             =   0
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkSucursal 
      Height          =   375
      Left            =   11100
      OleObjectBlob   =   "dicoem_informe_contratos.frx":02BA
      TabIndex        =   4
      Top             =   120
      Width           =   6705
   End
End
Attribute VB_Name = "dicoem_informe_recepciones"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim Sp_Nro_Recepcion As String

Private Sub CmdBuscaCliente_Click()
    If Val(TxtNroRecepcion) = 0 Then
        MsgBox "Nro no v�lido...", vbInformation
        TxtNroRecepcion.SetFocus
        Exit Sub
    End If
         
    Sp_Nro_Recepcion = " AND d.det_arr_num_recepcion=" & TxtNroRecepcion & " "
   CargaRecepciones
End Sub

Private Sub CmdExportar_Click()
    Dim tit(2) As String
    If LVDetalle.ListItems.Count = 0 Then Exit Sub
    BarraProgreso.Visible = True
    tit(0) = Me.Caption & " " & Date & " - " & Time
    tit(1) = Me.SkSucursal
    tit(2) = ""
  
    
    ExportarNuevo LVDetalle, tit, Me, BarraProgreso
    BarraProgreso.Visible = False
End Sub

Private Sub CmdRango_Click()
   ' If Val(TxtNroRecepcion) = 0 Then
   '     MsgBox "Nro no v�lido...", vbInformation
   ''     TxtNroRecepcion.SetFocus
    '    Exit Sub
   '' End If
         
    Sp_Nro_Recepcion = " AND det_arr_fecha_recepcion BETWEEN '" & Fql(DtDesde) & "' AND '" & Fql(DtHasta) & "'"
   CargaRecepciones
End Sub

Private Sub CmdRetornar_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    DtDesde = Date - 30
    DtHasta = Date
    Aplicar_skin Me
    Centrar Me
End Sub



Private Sub LvDetalle_Click()
    If LVDetalle.SelectedItem Is Nothing Then Exit Sub
    FrmLoad.Visible = True
    DoEvents
    Sql = "SELECT det_arr_id,arr_id,det_arr_codigo,det_arr_codigo_interno,det_arr_descripcion,det_arr_total, det_arr_estado, " & _
                " det_arr_obs_devolucion,det_arr_num_recepcion,det_arr_nombre_receptor,det_arr_fecha_recepcion,det_arr_hora_recepcion " & _
                "FROM ven_arr_detalle " & _
                "WHERE sue_id='" & LogSueID & "' and  arr_id =  '" & LVDetalle.SelectedItem.SubItems(3) & "'"
    
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvProductos, False, True, True, False
    FrmLoad.Visible = False
End Sub

Private Sub LvDetalle_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    ordListView ColumnHeader, Me, LVDetalle
End Sub

Private Sub Timer1_Timer()
    On Error Resume Next
    LVDetalle.SetFocus
    Timer1.Enabled = False
    CargaRecepciones
End Sub
Private Sub CargaRecepciones()
    FrmLoad.Visible = True
    DoEvents
    LvProductos.ListItems.Clear
    Sql = "SELECT d.det_arr_num_recepcion,d.det_arr_fecha_recepcion, " & _
            "CAST(d.det_arr_hora_recepcion AS CHAR) hora , d.arr_id, nombre_rsocial, a.arr_direccion_obra, a.arr_observacion " & _
            "FROM ven_arr_detalle d " & _
            "JOIN ven_arriendo a ON d.arr_id=a.arr_id " & _
            "JOIN maestro_clientes c ON a.arr_rut_cliente=c.rut_cliente " & _
            "WHERE d.sue_id=" & LogSueID & " AND a.sue_id=" & LogSueID & " " & Sp_Nro_Recepcion & " " & _
            "ORDER BY d.det_arr_num_recepcion"
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LVDetalle, False, True, True, False
    
    Sql = "SELECT sue_ciudad,sue_direccion " & _
            "FROM sis_empresas_sucursales " & _
            "WHERE sue_id=" & LogSueID
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        SkSucursal = RsTmp!sue_ciudad & " - " & RsTmp!sue_direccion
        
    
    End If
    
    
    FrmLoad.Visible = False
    
    
End Sub
