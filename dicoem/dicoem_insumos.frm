VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{28D47522-CF84-11D1-834C-00A0249F0C28}#1.0#0"; "gif89.dll"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form dicoem_insumos 
   Caption         =   "Insumos"
   ClientHeight    =   9150
   ClientLeft      =   1770
   ClientTop       =   1020
   ClientWidth     =   17790
   LinkTopic       =   "Form1"
   ScaleHeight     =   9150
   ScaleWidth      =   17790
   Begin VB.Frame FrmLoad 
      Height          =   1950
      Left            =   9240
      TabIndex        =   25
      Top             =   3060
      Visible         =   0   'False
      Width           =   2805
      Begin GIF89LibCtl.Gif89a Gif89a1 
         Height          =   1425
         Left            =   210
         OleObjectBlob   =   "dicoem_insumos.frx":0000
         TabIndex        =   26
         Top             =   285
         Width           =   2445
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Tipos de insumos"
      Height          =   8340
      Left            =   285
      TabIndex        =   18
      Top             =   480
      Width           =   11775
      Begin VB.TextBox TxtPrecio 
         Alignment       =   1  'Right Justify
         Height          =   315
         Left            =   9150
         TabIndex        =   4
         Tag             =   "N"
         Text            =   "0"
         Top             =   615
         Width           =   1125
      End
      Begin VB.TextBox TxtInsumoCodigo2 
         Height          =   315
         Left            =   8025
         TabIndex        =   3
         Tag             =   "T"
         Top             =   615
         Width           =   1125
      End
      Begin VB.TextBox TxtInsumoCodigo1 
         Height          =   315
         Left            =   6900
         TabIndex        =   2
         Tag             =   "T"
         Top             =   615
         Width           =   1125
      End
      Begin VB.ComboBox CboTipos 
         Appearance      =   0  'Flat
         Height          =   315
         ItemData        =   "dicoem_insumos.frx":0086
         Left            =   4560
         List            =   "dicoem_insumos.frx":0090
         Style           =   2  'Dropdown List
         TabIndex        =   1
         Top             =   615
         Width           =   2340
      End
      Begin VB.TextBox TxtIdInusmo 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         Height          =   315
         Left            =   495
         Locked          =   -1  'True
         TabIndex        =   19
         TabStop         =   0   'False
         Tag             =   "T"
         Top             =   615
         Width           =   615
      End
      Begin VB.ComboBox CboInsumoActivo 
         Appearance      =   0  'Flat
         Height          =   315
         ItemData        =   "dicoem_insumos.frx":009C
         Left            =   10275
         List            =   "dicoem_insumos.frx":00A6
         Style           =   2  'Dropdown List
         TabIndex        =   5
         Top             =   615
         Width           =   915
      End
      Begin VB.TextBox TxtNombreInsumo 
         Height          =   315
         Left            =   1110
         TabIndex        =   0
         Tag             =   "T"
         Top             =   615
         Width           =   3450
      End
      Begin VB.CommandButton CmdOkInsumo 
         Caption         =   "Ok"
         Height          =   300
         Left            =   11190
         TabIndex        =   6
         Top             =   615
         Width           =   405
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   210
         Left            =   390
         OleObjectBlob   =   "dicoem_insumos.frx":00B2
         TabIndex        =   20
         Top             =   8010
         Width           =   6675
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   255
         Index           =   0
         Left            =   1095
         OleObjectBlob   =   "dicoem_insumos.frx":01B0
         TabIndex        =   21
         Top             =   405
         Width           =   1095
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel5 
         Height          =   255
         Index           =   0
         Left            =   10290
         OleObjectBlob   =   "dicoem_insumos.frx":021A
         TabIndex        =   22
         Top             =   270
         Width           =   1065
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   255
         Index           =   1
         Left            =   495
         OleObjectBlob   =   "dicoem_insumos.frx":0294
         TabIndex        =   23
         Top             =   405
         Width           =   615
      End
      Begin MSComctlLib.ListView LvInsumos 
         Height          =   6900
         Left            =   495
         TabIndex        =   24
         Top             =   975
         Width           =   11100
         _ExtentX        =   19579
         _ExtentY        =   12171
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   9
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Id"
            Object.Width           =   1058
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Nombre"
            Object.Width           =   6085
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "T1000"
            Text            =   "Tipo"
            Object.Width           =   4128
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Object.Tag             =   "T1000"
            Text            =   "Codigo 1"
            Object.Width           =   1984
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Object.Tag             =   "T1000"
            Text            =   "Codigo 2"
            Object.Width           =   1984
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   5
            Object.Tag             =   "N100"
            Text            =   "Precio"
            Object.Width           =   1984
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   6
            Text            =   "Habilitado"
            Object.Width           =   1614
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   7
            Object.Tag             =   "T1000"
            Text            =   "Fecha Creacion"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   8
            Object.Tag             =   "N109"
            Text            =   "Id Tipo"
            Object.Width           =   0
         EndProperty
      End
   End
   Begin VB.Frame FrmMantenedor 
      Caption         =   "Tipos de insumos"
      Height          =   4335
      Left            =   12150
      TabIndex        =   8
      Top             =   480
      Width           =   5550
      Begin VB.CommandButton CmdOk 
         Caption         =   "Ok"
         Height          =   300
         Left            =   4785
         TabIndex        =   13
         Top             =   615
         Width           =   405
      End
      Begin VB.TextBox TxtNombre 
         Height          =   315
         Left            =   1110
         TabIndex        =   12
         Tag             =   "T"
         Top             =   615
         Width           =   2700
      End
      Begin VB.ComboBox CboActivo 
         Appearance      =   0  'Flat
         Height          =   315
         ItemData        =   "dicoem_insumos.frx":02F6
         Left            =   3825
         List            =   "dicoem_insumos.frx":0300
         Style           =   2  'Dropdown List
         TabIndex        =   11
         Top             =   615
         Width           =   960
      End
      Begin VB.TextBox TxtId 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         Height          =   315
         Left            =   495
         Locked          =   -1  'True
         TabIndex        =   10
         TabStop         =   0   'False
         Tag             =   "T"
         Top             =   615
         Width           =   615
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   585
         Left            =   510
         OleObjectBlob   =   "dicoem_insumos.frx":030C
         TabIndex        =   9
         Top             =   3690
         Width           =   3855
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   255
         Index           =   3
         Left            =   1095
         OleObjectBlob   =   "dicoem_insumos.frx":040A
         TabIndex        =   14
         Top             =   405
         Width           =   1095
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel5 
         Height          =   255
         Index           =   2
         Left            =   3720
         OleObjectBlob   =   "dicoem_insumos.frx":0474
         TabIndex        =   15
         Top             =   420
         Width           =   1065
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   255
         Index           =   5
         Left            =   495
         OleObjectBlob   =   "dicoem_insumos.frx":04EE
         TabIndex        =   16
         Top             =   405
         Width           =   615
      End
      Begin MSComctlLib.ListView LvDetalle 
         Height          =   2730
         Left            =   495
         TabIndex        =   17
         Top             =   960
         Width           =   4755
         _ExtentX        =   8387
         _ExtentY        =   4815
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   3
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Id"
            Object.Width           =   1058
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Nombre"
            Object.Width           =   4762
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Habilitado"
            Object.Width           =   1614
         EndProperty
      End
   End
   Begin VB.CommandButton cmdSalir 
      Cancel          =   -1  'True
      Caption         =   "&Retornar"
      Height          =   465
      Left            =   16380
      TabIndex        =   7
      Top             =   8610
      Width           =   1185
   End
   Begin VB.Timer Timer1 
      Interval        =   100
      Left            =   -15
      Top             =   5025
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   15
      OleObjectBlob   =   "dicoem_insumos.frx":0550
      Top             =   4245
   End
End
Attribute VB_Name = "dicoem_insumos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub CmdOk_Click()
    If Len(TxtNombre) = 0 Then Exit Sub
    
    If Val(TxtId) = 0 Then
        Sql = "INSERT INTO inv_insumos_tipos (tip_nombre) " & _
                "VALUES ('" & TxtNombre & "')"
    Else
        Sql = "UPDATE inv_insumos_tipos " & _
                "SET tip_nombre='" & TxtNombre & "',tip_activo='" & CboActivo.Text & "' " & _
                "WHERE tip_id=" & TxtId
    End If
    cn.Execute Sql
    CargaTipos
    TxtId = ""
    TxtNombre = ""
    LLenarCombo Me.CboTipos, "tip_nombre", "tip_id", "inv_insumos_tipos", "tip_activo='SI'", "tip_nombre"
    TxtNombre.SetFocus
End Sub

Private Sub CmdOkInsumo_Click()
    If Len(Me.TxtNombreInsumo) = 0 Then
        MsgBox "Falta ingresar nombre del insumo...", vbInformation
        TxtNombreInsumo.SetFocus
        Exit Sub
    End If
    If Len(Me.TxtInsumoCodigo1) = 0 And Len(Me.TxtInsumoCodigo2) = 0 Then
        MsgBox "Debe ingresar al menos un codigo...", vbInformation
        TxtInsumoCodigo1.SetFocus
        Exit Sub
    End If
    
    If Val(TxtPrecio) = 0 Then TxtPrecio = "0"
    
    If Val(Me.TxtIdInusmo) = 0 Then
        'create
        Sql = "INSERT INTO inv_insumos (ins_nombre,tip_id,ins_codigo1,ins_codigo2,ins_precio,ins_activo,ins_fecha_creacion) " & _
            "VALUES('" & TxtNombreInsumo & "'," & CboTipos.ItemData(CboTipos.ListIndex) & ",'" & Me.TxtInsumoCodigo1 & "','" & Me.TxtInsumoCodigo2 & "'," & CDbl(Me.TxtPrecio) & ",'" & Me.CboInsumoActivo.Text & "','" & Now & "')"
        
        
    Else
        'update
        Sql = "UPDATE inv_insumos SET ins_nombre='" & TxtNombreInsumo & "',tip_id=" & CboTipos.ItemData(CboTipos.ListIndex) & _
                ",ins_codigo1='" & Me.TxtInsumoCodigo1 & "',ins_codigo2='" & Me.TxtInsumoCodigo2 & "',ins_precio=" & CDbl(TxtPrecio) & " " & _
                ",ins_activo='" & Me.CboInsumoActivo.Text & "' " & _
                "WHERE ins_id=" & Me.TxtIdInusmo
                    
    
    End If
    cn.Execute Sql
    
    Me.TxtIdInusmo = ""
    TxtNombreInsumo = ""
    Me.TxtInsumoCodigo1 = ""
    Me.TxtInsumoCodigo2 = ""
    TxtPrecio = ""
    CboInsumoActivo.ListIndex = 0
    
    CargaInsumos
    
    TxtNombreInsumo.SetFocus
End Sub

Private Sub CmdSalir_Click()
    Unload Me
End Sub



Private Sub Form_Load()
    Centrar Me
    Skin2 Me, , 2
    CboActivo.ListIndex = 0
    Me.CboInsumoActivo.ListIndex = 0
End Sub
Private Sub CargaDatos()
    FrmLoad.Visible = True
    DoEvents
    LLenarCombo Me.CboTipos, "tip_nombre", "tip_id", "inv_insumos_tipos", "tip_activo='SI'", "tip_nombre"
    CboTipos.ListIndex = 0
    CargaTipos
    CargaInsumos
    FrmLoad.Visible = False
End Sub
Private Sub CargaTipos()
    Sql = "SELECT tip_id,tip_nombre,tip_activo " & _
            "FROM inv_insumos_tipos " & _
            "ORDER BY tip_nombre"
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvDetalle, False, True, True, False
End Sub

Private Sub LvDetalle_DblClick()
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    TxtId = LvDetalle.SelectedItem
    Me.TxtNombre = LvDetalle.SelectedItem.SubItems(1)
    If LvDetalle.SelectedItem.SubItems(2) = "SI" Then CboActivo.ListIndex = 0
    If LvDetalle.SelectedItem.SubItems(2) = "NO" Then CboActivo.ListIndex = 1
    TxtNombre.SetFocus
End Sub

Private Sub LvInsumos_DblClick()
    
    If LvInsumos.SelectedItem Is Nothing Then Exit Sub
    Me.TxtIdInusmo = LvInsumos.SelectedItem
    TxtNombreInsumo = LvInsumos.SelectedItem.SubItems(1)
    Busca_Id_Combo CboTipos, Val(LvInsumos.SelectedItem.SubItems(8))
    Me.TxtInsumoCodigo1 = LvInsumos.SelectedItem.SubItems(3)
    Me.TxtInsumoCodigo2 = LvInsumos.SelectedItem.SubItems(4)
    Me.TxtPrecio = LvInsumos.SelectedItem.SubItems(5)
    If LvInsumos.SelectedItem.SubItems(6) = "SI" Then CboInsumoActivo.ListIndex = 0 Else CboInsumoActivo.ListIndex = 1
    TxtNombreInsumo.SetFocus
End Sub


Private Sub Timer1_Timer()
    CargaDatos
    Timer1.Enabled = False
End Sub

Private Sub TxtInsumoCodigo1_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub TxtInsumoCodigo2_GotFocus()
    En_Foco TxtInsumoCodigo2
End Sub

Private Sub TxtInsumoCodigo1_GotFocus()
    En_Foco TxtInsumoCodigo1
End Sub

Private Sub TxtInsumoCodigo2_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub TxtNombre_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub TxtNombreInsumo_GotFocus()
    En_Foco TxtNombreInsumo
End Sub

Private Sub TxtNombreInsumo_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub
Private Sub TxtPrecio_GotFocus()
    En_Foco TxtPrecio
End Sub

Private Sub TxtPrecio_KeyPress(KeyAscii As Integer)
    KeyAscii = AceptaSoloNumeros(KeyAscii)
End Sub

Private Sub TxtPrecio_Validate(Cancel As Boolean)
    If Val(TxtPrecio) = 0 Then TxtPrecio = "0"
End Sub
Private Sub CargaInsumos()
    Sql = "SELECT ins_id,ins_nombre,tip_nombre,ins_codigo1,ins_codigo2,ins_precio,ins_activo,ins_fecha_creacion,i.tip_id " & _
            "FROM inv_insumos i " & _
            "JOIN inv_insumos_tipos t ON i.tip_id=t.tip_id"
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, Me.LvInsumos, False, True, True, False
End Sub
