VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{DA729E34-689F-49EA-A856-B57046630B73}#1.0#0"; "progressbar-xp.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form Control_Ingresos_Egresos 
   Caption         =   "Control de Ingresos"
   ClientHeight    =   8790
   ClientLeft      =   2790
   ClientTop       =   1185
   ClientWidth     =   15390
   LinkTopic       =   "Form1"
   ScaleHeight     =   8790
   ScaleWidth      =   15390
   Begin MSComDlg.CommonDialog Dialogo 
      Left            =   11250
      Top             =   8295
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.Frame Frame1 
      Caption         =   "Cliente"
      Height          =   960
      Left            =   135
      TabIndex        =   2
      Top             =   255
      Width           =   6600
      Begin VB.TextBox txtDireccion 
         Height          =   285
         Left            =   6795
         TabIndex        =   44
         Text            =   "Text1"
         Top             =   600
         Width           =   3135
      End
      Begin VB.TextBox txtCiudad 
         Height          =   300
         Left            =   6795
         TabIndex        =   43
         Text            =   "Text1"
         Top             =   270
         Width           =   3165
      End
      Begin VB.TextBox TxtMail 
         BackColor       =   &H00E0E0E0&
         Height          =   315
         Left            =   2940
         Locked          =   -1  'True
         TabIndex        =   9
         Top             =   585
         Width           =   3480
      End
      Begin VB.TextBox TxtFono 
         BackColor       =   &H00E0E0E0&
         Height          =   315
         Left            =   720
         Locked          =   -1  'True
         TabIndex        =   7
         Top             =   585
         Width           =   1560
      End
      Begin VB.CommandButton cmdBtnRut 
         Caption         =   "Rut"
         Height          =   285
         Left            =   90
         TabIndex        =   6
         Top             =   300
         Width           =   585
      End
      Begin VB.TextBox txtCliente 
         BackColor       =   &H00E0E0E0&
         Height          =   315
         Left            =   2940
         Locked          =   -1  'True
         TabIndex        =   4
         Top             =   285
         Width           =   3480
      End
      Begin VB.TextBox TxtRut 
         BackColor       =   &H0080FF80&
         Height          =   315
         Left            =   720
         TabIndex        =   3
         ToolTipText     =   "Ingrese el RUT sin puntos ni guion."
         Top             =   300
         Width           =   1560
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel16 
         Height          =   270
         Left            =   2325
         OleObjectBlob   =   "Control_Ingresos_Egresos.frx":0000
         TabIndex        =   5
         Top             =   285
         Width           =   540
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   195
         Left            =   90
         OleObjectBlob   =   "Control_Ingresos_Egresos.frx":006C
         TabIndex        =   8
         Top             =   600
         Width           =   540
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   270
         Left            =   2325
         OleObjectBlob   =   "Control_Ingresos_Egresos.frx":00D2
         TabIndex        =   10
         Top             =   585
         Width           =   540
      End
   End
   Begin VB.CommandButton CmdLaserTinta 
      BackColor       =   &H00FFFFFF&
      Height          =   540
      Left            =   3465
      Picture         =   "Control_Ingresos_Egresos.frx":013C
      Style           =   1  'Graphical
      TabIndex        =   42
      Top             =   7080
      Width           =   1245
   End
   Begin VB.CheckBox Check1 
      Caption         =   "Omitir Guias"
      Height          =   270
      Left            =   2220
      TabIndex        =   41
      Top             =   7140
      Width           =   1410
   End
   Begin VB.Timer Timer3 
      Interval        =   500
      Left            =   150
      Top             =   8355
   End
   Begin VB.Timer Timer2 
      Enabled         =   0   'False
      Interval        =   10
      Left            =   9780
      Top             =   8220
   End
   Begin Proyecto2.XP_ProgressBar Bar_Cargando 
      Height          =   330
      Left            =   570
      TabIndex        =   40
      ToolTipText     =   "Cargando"
      Top             =   8325
      Width           =   8490
      _ExtentX        =   14975
      _ExtentY        =   582
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BrushStyle      =   0
      Color           =   12937777
      Scrolling       =   3
   End
   Begin VB.CheckBox ChkTodos 
      Caption         =   "Check1"
      Height          =   195
      Left            =   165
      TabIndex        =   39
      Top             =   1320
      Width           =   195
   End
   Begin VB.Frame FrmSaldo 
      Caption         =   "Saldo a favor"
      Height          =   540
      Left            =   7140
      TabIndex        =   37
      Top             =   7110
      Width           =   2760
      Begin ACTIVESKINLibCtl.SkinLabel SkSaldo 
         Height          =   240
         Left            =   540
         OleObjectBlob   =   "Control_Ingresos_Egresos.frx":05D9
         TabIndex        =   38
         Top             =   210
         Width           =   1590
      End
   End
   Begin VB.CommandButton CmdFactoring 
      Caption         =   "Factoring"
      Height          =   540
      Left            =   5640
      TabIndex        =   35
      Top             =   7095
      Visible         =   0   'False
      Width           =   1320
   End
   Begin VB.CommandButton cmdExportar 
      Caption         =   "Excel"
      Height          =   300
      Left            =   14445
      TabIndex        =   32
      Top             =   7095
      Width           =   660
   End
   Begin VB.Frame FraProgreso 
      Height          =   540
      Left            =   375
      TabIndex        =   25
      Top             =   5415
      Visible         =   0   'False
      Width           =   14670
      Begin Proyecto2.XP_ProgressBar PBar 
         Height          =   255
         Left            =   135
         TabIndex        =   34
         Top             =   195
         Width           =   14415
         _ExtentX        =   25426
         _ExtentY        =   450
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BrushStyle      =   0
         Color           =   16777088
         Scrolling       =   4
         ShowText        =   -1  'True
      End
   End
   Begin VB.Frame Frame6 
      Caption         =   "Estado"
      Height          =   960
      Left            =   12945
      TabIndex        =   24
      Top             =   255
      Width           =   2235
      Begin VB.ComboBox CboEstado 
         Height          =   315
         ItemData        =   "Control_Ingresos_Egresos.frx":0639
         Left            =   195
         List            =   "Control_Ingresos_Egresos.frx":0646
         Style           =   2  'Dropdown List
         TabIndex        =   33
         Top             =   450
         Width           =   1860
      End
   End
   Begin VB.Frame Frame5 
      Caption         =   "Doc. Vencidos"
      Height          =   960
      Left            =   11175
      TabIndex        =   21
      Top             =   255
      Width           =   1785
      Begin VB.CommandButton CmdVencidas 
         Caption         =   "Ver"
         Height          =   210
         Left            =   1200
         TabIndex        =   26
         ToolTipText     =   "Filtrar hasta la fecha seleccionada"
         Top             =   285
         Width           =   450
      End
      Begin MSComCtl2.DTPicker DtVencidas 
         Height          =   285
         Left            =   315
         TabIndex        =   22
         Top             =   525
         Width           =   1350
         _ExtentX        =   2381
         _ExtentY        =   503
         _Version        =   393216
         Format          =   91815937
         CurrentDate     =   40628
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
         Height          =   240
         Left            =   150
         OleObjectBlob   =   "Control_Ingresos_Egresos.frx":0670
         TabIndex        =   23
         Top             =   315
         Width           =   570
      End
   End
   Begin VB.Frame Frame4 
      Caption         =   "Informacion"
      Height          =   960
      Left            =   6645
      TabIndex        =   18
      Top             =   255
      Width           =   4560
      Begin VB.CommandButton CmdVer 
         Caption         =   "Ver"
         Height          =   210
         Left            =   4125
         TabIndex        =   27
         ToolTipText     =   "Filtrar por las fecha seleccionadas"
         Top             =   570
         Visible         =   0   'False
         Width           =   360
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Todas las fechas"
         Height          =   240
         Left            =   300
         TabIndex        =   20
         Top             =   255
         Value           =   -1  'True
         Width           =   1740
      End
      Begin VB.OptionButton Option2 
         Caption         =   "Seleccionar fechas"
         Height          =   240
         Left            =   300
         TabIndex        =   19
         Top             =   525
         Width           =   1725
      End
      Begin ACTIVESKINLibCtl.SkinLabel skIni 
         Height          =   240
         Left            =   1980
         OleObjectBlob   =   "Control_Ingresos_Egresos.frx":06D8
         TabIndex        =   28
         Top             =   225
         Width           =   795
      End
      Begin MSComCtl2.DTPicker DTInicio 
         Height          =   300
         Left            =   2820
         TabIndex        =   29
         Top             =   210
         Width           =   1305
         _ExtentX        =   2302
         _ExtentY        =   529
         _Version        =   393216
         Enabled         =   0   'False
         Format          =   91815937
         CurrentDate     =   40628
      End
      Begin MSComCtl2.DTPicker DtHasta 
         Height          =   285
         Left            =   2820
         TabIndex        =   30
         Top             =   510
         Width           =   1305
         _ExtentX        =   2302
         _ExtentY        =   503
         _Version        =   393216
         Enabled         =   0   'False
         Format          =   91815937
         CurrentDate     =   40628
      End
      Begin ACTIVESKINLibCtl.SkinLabel skFin 
         Height          =   240
         Left            =   1980
         OleObjectBlob   =   "Control_Ingresos_Egresos.frx":0740
         TabIndex        =   31
         Top             =   495
         Width           =   795
      End
   End
   Begin VB.CommandButton CmdTodos 
      Caption         =   "Todos"
      Height          =   210
      Left            =   5970
      TabIndex        =   17
      ToolTipText     =   "Todos los clientes"
      Top             =   105
      Width           =   690
   End
   Begin VB.Frame Frame2 
      Caption         =   "Totales"
      Height          =   600
      Left            =   10005
      TabIndex        =   13
      Top             =   7050
      Width           =   4350
      Begin VB.TextBox txtSaldos 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00E0E0E0&
         Height          =   285
         Left            =   2955
         Locked          =   -1  'True
         TabIndex        =   16
         Top             =   270
         Width           =   1350
      End
      Begin VB.TextBox TxtAbonos 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00E0E0E0&
         Height          =   285
         Left            =   1605
         Locked          =   -1  'True
         TabIndex        =   15
         Top             =   270
         Width           =   1350
      End
      Begin VB.TextBox TxtTotales 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00E0E0E0&
         Height          =   285
         Left            =   255
         Locked          =   -1  'True
         TabIndex        =   14
         Top             =   270
         Width           =   1350
      End
   End
   Begin VB.CommandButton CmdCobranza 
      Caption         =   "Gestion de Cobranzas"
      Height          =   390
      Left            =   2430
      TabIndex        =   12
      Top             =   7695
      Visible         =   0   'False
      Width           =   2145
   End
   Begin VB.CommandButton CmdGestionPagos 
      Caption         =   "GESTION DE PAGOS"
      Height          =   540
      Left            =   225
      TabIndex        =   11
      Top             =   7080
      Width           =   1905
   End
   Begin VB.CommandButton cmdSalir 
      Cancel          =   -1  'True
      Caption         =   "&Retornar"
      Height          =   405
      Left            =   13830
      TabIndex        =   1
      Top             =   8205
      Width           =   1350
   End
   Begin VB.Timer Timer1 
      Interval        =   10
      Left            =   600
      Top             =   7515
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   105
      OleObjectBlob   =   "Control_Ingresos_Egresos.frx":07A8
      Top             =   7485
   End
   Begin MSComctlLib.ListView LvDetalle 
      Height          =   5760
      Left            =   135
      TabIndex        =   0
      Top             =   1290
      Width           =   15045
      _ExtentX        =   26538
      _ExtentY        =   10160
      View            =   3
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   0
      NumItems        =   15
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Object.Tag             =   "T1000"
         Object.Width           =   529
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Object.Tag             =   "N109"
         Text            =   "Nro Doc."
         Object.Width           =   1587
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Object.Tag             =   "T1500"
         Text            =   "Documento"
         Object.Width           =   2646
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Object.Tag             =   "T1300"
         Text            =   "Rut"
         Object.Width           =   2346
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Object.Tag             =   "T2000"
         Text            =   "Nombre"
         Object.Width           =   3528
      EndProperty
      BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   5
         Object.Tag             =   "T1500"
         Text            =   "Sucursal"
         Object.Width           =   2646
      EndProperty
      BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   6
         Object.Tag             =   "F1000"
         Text            =   "Emision"
         Object.Width           =   2205
      EndProperty
      BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   7
         Object.Tag             =   "F1000"
         Text            =   "Vencimiento"
         Object.Width           =   2205
      EndProperty
      BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   8
         Key             =   "total"
         Object.Tag             =   "N100"
         Text            =   "Total"
         Object.Width           =   2381
      EndProperty
      BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   9
         Key             =   "abono"
         Object.Tag             =   "N100"
         Text            =   "Abonos"
         Object.Width           =   2381
      EndProperty
      BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   10
         Key             =   "saldo"
         Object.Tag             =   "N100"
         Text            =   "Saldo"
         Object.Width           =   2381
      EndProperty
      BeginProperty ColumnHeader(12) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   11
         Object.Tag             =   "T400"
         Text            =   "Cob."
         Object.Width           =   917
      EndProperty
      BeginProperty ColumnHeader(13) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   12
         Object.Tag             =   "T100"
         Text            =   "SIGNO"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(14) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   13
         Object.Tag             =   "N109"
         Text            =   "suc_id"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(15) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   14
         Object.Tag             =   "T1000"
         Text            =   "Folio"
         Object.Width           =   2117
      EndProperty
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkEmpresaActiva 
      Height          =   285
      Left            =   6720
      OleObjectBlob   =   "Control_Ingresos_Egresos.frx":09DC
      TabIndex        =   36
      Top             =   0
      Width           =   5685
   End
End
Attribute VB_Name = "Control_Ingresos_Egresos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public Spm_Pago As String

Dim sm_FiltroFecha As String
Dim Sm_FiltroVencidas As String
Dim Sm_FiltroEstado As String
Dim sp_Documentos As String

Dim Lp_ValueBar As Long
Dim Sm_FiltroSucursal As String
Dim Sm_Fguias As String

Dim P_Fecha As String * 10
Dim P_Documento As String * 25
Dim P_Numero As String * 11
Dim P_Vence As String * 10
Dim P_Venta As String * 12
Dim P_Pago As String * 12
Dim P_Saldo As String * 12
Dim Cx As Double
Dim Cy As Double
Dim Dp As Double
Dim Im_Paginas As Integer


Private Sub FormatoGrilla()
    
    If LvDetalle.ListItems.Count > 0 Then
       ' DoEvents
        'Buscar documento vencidos y marcarlos con rojo
        For i = 1 To LvDetalle.ListItems.Count
            If LvDetalle.ListItems(i).SubItems(12) = "-" Then
                LvDetalle.ListItems(i).SubItems(8) = NumFormat( _
                    CDbl(LvDetalle.ListItems(i).SubItems(8)) * -1)
                LvDetalle.ListItems(i).SubItems(10) = NumFormat( _
                    CDbl(LvDetalle.ListItems(i).SubItems(10)) * -1)
            End If
            If Len(LvDetalle.ListItems(i).SubItems(7)) > 0 Then
                If DateDiff("d", Date, LvDetalle.ListItems(i).SubItems(7)) < 0 Then
                    For X = 1 To LvDetalle.ColumnHeaders.Count - 2
                        LvDetalle.ListItems(i).ListSubItems(X).ForeColor = vbRed
                        LvDetalle.ListItems(i).ListSubItems(X).Bold = True
                    Next
                End If
            End If
            
        Next
        TxtTotales = NumFormat(TotalizaColumna(LvDetalle, "total"))
        TxtAbonos = NumFormat(TotalizaColumna(LvDetalle, "abono"))
        txtSaldos = NumFormat(TotalizaColumna(LvDetalle, "saldo"))
    End If
End Sub

Private Sub CboEstado_Click()
    If CboEstado.ListIndex = -1 Then Exit Sub
    If CboEstado.ListIndex = 0 Then Sm_FiltroEstado = " HAVING saldo>0 "
    If CboEstado.ListIndex = 1 Then Sm_FiltroEstado = " HAVING saldo=0 "
    If CboEstado.ListIndex = 2 Then Sm_FiltroEstado = Empty
    If Sp_extra = "CLIENTES" Then CargaClientes Else CargaProveedores
End Sub

Private Sub Check1_Click()
    Sm_Fguias = ""
    If Check1.Value = 1 Then Sm_Fguias = " AND doc_nombre<>'GUIA' "
    If Sp_extra = "CLIENTES" Then
        CargaClientes
    Else
        CargaProveedores
    End If
    LvDetalle.SetFocus
End Sub

Private Sub ChkTodos_Click()
    If ChkTodos.Value = 1 Then
        For i = 1 To LvDetalle.ListItems.Count
            LvDetalle.ListItems(i).Checked = True
        Next
    Else
        For i = 1 To LvDetalle.ListItems.Count
            LvDetalle.ListItems(i).Checked = False
        Next
    End If
End Sub

Private Sub CmdCobranza_Click()
    Dim sp_Documentos As String
    sp_Documentos = "0"
    If Len(TxtRut) = 0 Then
        MsgBox "Seleccione Cliente, antes de gestionar Cobranza...", vbInformation
        TxtRut.SetFocus
        Exit Sub
    End If
    For i = 1 To LvDetalle.ListItems.Count
        If LvDetalle.ListItems(i).Checked Then sp_Documentos = sp_Documentos & "," & LvDetalle.ListItems(i)
    Next
    If Len(sp_Documentos) > 1 Then
        Sql = "SELECT v.id,no_documento,CONCAT(doc_nombre,IF(doc_factura_guias='SI',' Por guias','')) doc_nombre,v.rut_cliente,IFNULL(cli_nombre_fantasia,nombre_rsocial) cliente," & _
                 "IF(v.suc_id=0,'CM',CONCAT(suc_ciudad,' - ',suc_direccion)) sucursal," & _
                 "fecha,v.ven_fecha_vencimiento,bruto," & _
                "IFNULL((SELECT SUM(t.ctd_monto) FROM cta_abono_documentos t,cta_abonos a WHERE t.abo_id=a.abo_id AND a.abo_cli_pro='CLI' AND t.id=v.id),0) abonos, " & _
                 "bruto-" & _
                 "IFNULL((SELECT SUM(t.ctd_monto) FROM cta_abono_documentos t,cta_abonos a WHERE t.abo_id=a.abo_id AND a.abo_cli_pro='CLI' AND t.id=v.id),0) saldo " & _
            "FROM ven_doc_venta v,maestro_clientes c,sis_documentos d,par_sucursales s " & _
            "WHERE  v.rut_emp='" & SP_Rut_Activo & "' AND  v.doc_id_factura=0 AND v.suc_id = s.suc_id And v.rut_cliente = c.rut_cliente AND v.doc_id = d.doc_id AND v.doc_id<>" & IG_id_Nota_Credito_Clientes & " " & Filtro & _
            " AND v.id IN (" & sp_Documentos & ") " & _
            "HAVING saldo > 0 " & _
            "ORDER BY id"
            ctacte_GestionDeCobranza.Show 1
            CargaClientes
            
    Else
        MsgBox "No ha seleccionado ning�n documento...", vbInformation
    End If

End Sub

Private Sub CmdExportar_Click()
    Dim tit(2) As String
    FraProgreso.Visible = True
    tit(0) = Me.Caption
    tit(1) = "FECHA INFORME: " & Date & " - " & Time
    tit(2) = "" ' "                                        DESDE:" & DtInicio.Value & "   HASTA:" & Dtfin.Value
    ExportarNuevo LvDetalle, tit, Me, PBar
    PBar.Value = 1
    FraProgreso.Visible = False
End Sub

Private Sub CmdFactoring_Click()
    sp_Documentos = "0"
    For i = 1 To LvDetalle.ListItems.Count
        If LvDetalle.ListItems(i).Checked Then
            If CDbl(LvDetalle.ListItems(i).SubItems(8)) = CDbl(LvDetalle.ListItems(i).SubItems(10)) Then
                sp_Documentos = sp_Documentos & "," & LvDetalle.ListItems(i)
            Else
                MsgBox "Doc.   :" & LvDetalle.ListItems(i).SubItems(2) & vbNewLine & _
                       "Nro.   :" & LvDetalle.ListItems(i).SubItems(1) & vbNewLine & _
                       "Nombre :" & LvDetalle.ListItems(i).SubItems(4) & vbNewLine & _
                       "Ya tiene abono(s), no es posible incluirla en el Factoring...", vbInformation
               
            End If
        End If
        
    Next
    
    If Len(sp_Documentos) > 1 Then
        ctacte_Factoring.Sm_DocumentosMarcados = sp_Documentos
        ctacte_Factoring.Show 1
        CargaClientes
    Else
        MsgBox "No ha seleccionado ning�n documento...", vbInformation
    End If
End Sub

Private Sub CmdGestionPagos_Click()
    Dim Lp_SucTemp As Long
    sp_Documentos = "0"
    If Len(TxtRut) = 0 Then
        MsgBox "Seleccione Cliente, antes de gestionar Pagos...", vbInformation
        TxtRut.SetFocus
        Exit Sub
    End If
    
    For i = 1 To LvDetalle.ListItems.Count
        If LvDetalle.ListItems(i).Checked Then
            Lp_SucTemp = Val(LvDetalle.ListItems(i).SubItems(13))
            Exit For
        End If
    Next
    
    
    For i = 1 To LvDetalle.ListItems.Count
        'If LvDetalle.ListItems(i).Checked And CDbl(LvDetalle.ListItems(i).SubItems(10)) > 0 Then
        If LvDetalle.ListItems(i).Checked Then
            If Lp_SucTemp <> Val(LvDetalle.ListItems(i).SubItems(13)) Then
                MsgBox "Debe seleccionar por sucursales...", vbInformation
                Exit Sub
            End If
            sp_Documentos = sp_Documentos & "," & LvDetalle.ListItems(i)
        End If
    Next
    
    
    
    
    
    
    
    If Len(sp_Documentos) > 1 Then
        If Sp_extra = "CLIENTES" Then
            
            Sql = "SELECT v.id,no_documento,CONCAT(doc_nombre,IF(doc_factura_guias='SI',' Por guias','')) doc_nombre,v.rut_cliente,IFNULL(cli_nombre_fantasia,nombre_rsocial) cliente," & _
                     "IF(v.suc_id=0,'CM',CONCAT(suc_ciudad,' - ',suc_direccion)) sucursal," & _
                     "fecha,v.ven_fecha_vencimiento,bruto," & _
                    "IFNULL((SELECT SUM(t.ctd_monto) FROM cta_abono_documentos t,cta_abonos a WHERE t.abo_id=a.abo_id AND a.abo_cli_pro='CLI' AND t.id=v.id),0) abonos, " & _
                     "bruto-" & _
                     "IFNULL((SELECT SUM(t.ctd_monto) FROM cta_abono_documentos t,cta_abonos a WHERE t.abo_id=a.abo_id AND a.abo_cli_pro='CLI' AND t.id=v.id),0) saldo,0 nuevo,v.suc_id " & _
                "FROM ven_doc_venta v,maestro_clientes c,sis_documentos d,par_sucursales s " & _
                "WHERE  v.rut_emp='" & SP_Rut_Activo & "' AND   v.doc_id_factura=0 AND v.suc_id = s.suc_id And v.rut_cliente = c.rut_cliente AND v.doc_id = d.doc_id AND v.doc_id<>" & IG_id_Nota_Credito_Clientes & " " & Filtro & _
                " AND v.id IN (" & sp_Documentos & ") " & _
                "HAVING saldo > 0 " & _
                "ORDER BY id"
                ctacte_GestionDePagos.Im_Fpago_Defecto = 1
                ctacte_GestionDePagos.Sm_NombreAbono = ""
                ctacte_GestionDePagos.Sm_RutAbono = ""
                ctacte_GestionDePagos.Sm_RelacionMP = ""
                

                ctacte_GestionDePagos.Sm_Cli_Pro = "CLI"
        Else
            Sm_FiltroEstado = " HAVING saldo <> 0 "
            Sql = "SELECT v.id,no_documento," & _
                  "CAST(IF(id_ref=0,CONCAT(doc_nombre,IF(doc_factura_guias = 'SI',' Por guias',''))," & _
                  "CONCAT(d.doc_nombre,' Ref. '," & _
    "(SELECT CONCAT(y.doc_abreviado,' ',no_documento) FROM com_doc_compra z INNER JOIN sis_documentos y USING(doc_id) WHERE z.id IN(v.id_ref))) " & _
    ") AS CHAR) doc_nombre " & _
    ",v.rut,nombre_proveedor,'',fecha, v.doc_fecha_vencimiento, total * IF(doc_signo_libro='-',-1,1) total,IF(doc_signo_libro='-',-1,1) *   IFNULL((" & _
                  "SELECT SUM(t.ctd_monto) FROM cta_abono_documentos t,cta_abonos a " & _
                        "WHERE t.abo_id = a.abo_id AND a.abo_cli_pro = 'PRO' AND t.id = v.id),0)" & _
                        " abonos," & _
                   "(total - (IFNULL((SELECT SUM(t.ctd_monto) FROM cta_abono_documentos t,cta_abonos a " & _
                                        "WHERE t.abo_id = a.abo_id AND a.abo_cli_pro = 'PRO' AND t.id = v.id ),0)" & _
                    ")) * IF(doc_signo_libro='-',-1,1) " & _
                        " saldo " & _
          "FROM    com_doc_compra v,   maestro_proveedores c,  sis_documentos d " & _
          "WHERE v.id IN (" & sp_Documentos & ") AND v.com_nc_utilizada='NO' AND v.rut_emp='" & SP_Rut_Activo & "' AND id_ref=0 AND  v.doc_id_factura = 0 AND v.Rut = c.rut_proveedor AND v.doc_id = d.doc_id  " & _
          Filtro & sm_FiltroFecha & Sm_FiltroVencidas & _
          " " & Sm_FiltroEstado & " " & _
          "ORDER BY id"
            
            
            
            
            
            '    Sql = "SELECT v.id,no_documento," & _
                  "CAST(IF(id_ref=0,CONCAT(doc_nombre,IF(doc_factura_guias = 'SI',' Por guias',''))," & _
                  "CONCAT(d.doc_nombre,' Ref. '," & _
    "(SELECT CONCAT(y.doc_abreviado,' ',no_documento) FROM com_doc_compra z INNER JOIN sis_documentos y USING(doc_id) WHERE z.id IN(v.id_ref))) " & _
    ") AS CHAR) doc_nombre " & _
    ",v.rut,nombre_proveedor,'',fecha, v.doc_fecha_vencimiento, total, IFNULL((" & _
                  "SELECT SUM(t.ctd_monto) FROM cta_abono_documentos t,cta_abonos a " & _
                        "WHERE t.abo_id = a.abo_id AND a.abo_cli_pro = 'PRO' AND t.id = v.id),0)" & _
                        "+IFNULL((SELECT SUM(ctd_monto) FROM cta_abonos z " & _
                        "INNER JOIN cta_abono_documentos y USING(abo_id) " & _
                        "WHERE z.rut_emp='" & SP_Rut_Activo & "' AND y.rut_emp='" & SP_Rut_Activo & "' " & _
                        "AND y.id IN(SELECT id FROM com_doc_compra w WHERE w.id_ref=v.id)),0) abonos," & _
                   "total - (IFNULL((SELECT SUM(t.ctd_monto) FROM cta_abono_documentos t,cta_abonos a " & _
                                        "WHERE t.abo_id = a.abo_id AND a.abo_cli_pro = 'PRO' AND t.id = v.id ),0)" & _
                    "+IFNULL((SELECT SUM(ctd_monto) FROM cta_abonos z " & _
                        "INNER JOIN cta_abono_documentos y USING(abo_id) " & _
                        "WHERE z.rut_emp='" & SP_Rut_Activo & "' AND y.rut_emp='" & SP_Rut_Activo & "' " & _
                        "AND y.id IN(SELECT id FROM com_doc_compra w WHERE w.id_ref=v.id)),0)) saldo " & _
          "FROM    com_doc_compra v,   maestro_proveedores c,  sis_documentos d " & _
          "WHERE /*v.doc_id<>23  AND*/  v.rut_emp='" & SP_Rut_Activo & "' AND   v.doc_id_factura = 0 AND v.Rut = c.rut_proveedor AND v.doc_id = d.doc_id  " & _
          " AND v.id IN (" & sp_Documentos & ") " & _
          Filtro & sm_FiltroFecha & Sm_FiltroVencidas & _
          " " & Sm_FiltroEstado & " " & _
          "ORDER BY id"
            
            
            '    Sql = "SELECT v.id,no_documento,CONCAT(doc_nombre,IF(doc_factura_guias='SI',' Por guias','')) doc_nombre,v.rut,nombre_empresa cliente," & _
                     "'' sucursal," & _
                     "fecha,v.doc_fecha_vencimiento,total," & _
                    "IFNULL((SELECT SUM(t.ctd_monto) FROM cta_abono_documentos t,cta_abonos a WHERE t.abo_id=a.abo_id AND a.abo_cli_pro='PRO' AND t.id=v.id),0) abonos, " & _
                     "total-" & _
                     "IFNULL((SELECT SUM(t.ctd_monto) FROM cta_abono_documentos t,cta_abonos a WHERE t.abo_id=a.abo_id AND a.abo_cli_pro='PRO' AND t.id=v.id),0) saldo " & _
                "FROM com_doc_compra v,maestro_proveedores c,sis_documentos d " & _
                "WHERE  v.rut_emp='" & SP_Rut_Activo & "' AND   v.doc_id_factura=0 AND v.rut = c.rut_proveedor AND v.doc_id = d.doc_id AND (v.doc_id<>9 AND v.doc_id<>15) " & Filtro & _
                " AND v.id IN (" & sp_Documentos & ") " & _
                "HAVING saldo > 0 " & _
                "ORDER BY id"
                ctacte_GestionDePagos.Sm_Cli_Pro = "PRO"
            End If
     '       ctacte_GestionDePagos.Im_Fpago_Defecto = 1
            ctacte_GestionDePagos.Sm_NombreAbono = ""
            ctacte_GestionDePagos.Sm_RutAbono = ""
            ctacte_GestionDePagos.Sm_RelacionMP = ""
            ctacte_GestionDePagos.Im_Fpago_Defecto = 0
            ctacte_GestionDePagos.Sm_NombreAbono = txtCliente
            ctacte_GestionDePagos.Sm_RutAbono = TxtRut
            ctacte_GestionDePagos.Show 1
            
            If Sp_extra = "CLIENTES" Then CargaClientes Else CargaProveedores
    Else
        MsgBox "No ha seleccionado ning�n documento con deuda...", vbInformation
    End If
End Sub

Private Sub CmdLaserTinta_Click()
    If Len(TxtRut) = 0 Then
        MsgBox "Seleccione cliente...", vbInformation
        Exit Sub
    End If

    Dialogo.CancelError = True
    On Error GoTo CancelaImpesion
    Dialogo.ShowPrinter
    
    
    'Sql = "SELECT rut,nombre_empresa,direccion,ciudad,giro " & _
    '        "FROM sis_empresas " & _
    '        "WHERE rut='" & SP_Rut_Activo & "'"
    'Consulta RsTmp, Sql
    'LLenar_Grilla RsTmp, Me, LvEmpresa, True, True, False, False
    
    Printer.Orientation = vbPRORPortrait
    Printer.ScaleMode = vbCentimeters
    ImprimeCartola
    
    Exit Sub
CancelaImpesion:
    'NO QUISO IMPRIMIR
End Sub

Private Sub cmdSalir_Click()
    Unload Me
End Sub



Private Sub CmdTodos_Click()
    Filtro = ""
    TxtRut = Empty
    txtCliente = Empty
    txtFono = Empty
    TxtMail = Empty
    If Sp_extra = "CLIENTES" Then CargaClientes Else CargaProveedores
End Sub

Private Sub CmdVencidas_Click()
    If Sp_extra = "CLIENTES" Then
        Sm_FiltroVencidas = " AND (v.ven_fecha_vencimiento <= '" & Format(DtVencidas.Value, "YYYY-MM-DD") & "') "
        CargaClientes
    Else
        Sm_FiltroVencidas = " AND (v.doc_fecha_vencimiento <= '" & Format(DtVencidas.Value, "YYYY-MM-DD") & "') "
        CargaProveedores
    End If
End Sub

Private Sub CmdVer_Click()
    DtVencidas.Value = DtHasta.Value
    sm_FiltroFecha = " AND (v.fecha BETWEEN '" & Format(DTInicio.Value, "YYYY-MM-DD") & "' AND '" & Format(DtHasta.Value, "YYYY-MM-DD") & "') "
    If Sp_extra = "CLIENTES" Then CargaClientes Else CargaProveedores
End Sub




Private Sub Form_Load()
    Centrar Me
    Aplicar_skin Me
    Sm_Fguias = ""
    SkEmpresaActiva = "EMPRESA ACTIVA: " & SP_Empresa_Activa
    Sql = "SELECT men_infoextra " & _
          "FROM sis_menu " & _
          "WHERE men_id=" & IP_IDMenu
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        Sp_extra = RsTmp!men_infoextra
    End If
    
    If Sp_extra = "CLIENTES" Then
        Frame1.Caption = "CLIENTE"
        Me.Caption = "CONTROL DE INGRESOS DE CLIENTES"
                'Comenzamos a separar las sucurales en los informes
        'si corresponde
        Sql = "SELECT IFNULL(COUNT(sue_id),0) cant " & _
                "FROM sis_empresas_sucursales " & _
                "WHERE emp_id=" & IG_id_Empresa
        Consulta RsTmp, Sql
        Sm_FiltroSucursal = ""
        If RsTmp.RecordCount > 0 Then
            If RsTmp!cant > 0 Then
                'Aplicar filtro de sucursal
                Sm_FiltroSucursal = " AND v.sue_id=" & IG_id_Sucursal_Empresa
            End If
        End If
            
    Else
        Frame1.Caption = "PROVEEDOR"
        Me.Caption = "CONTROL PAGO A PROVEEDORES"
        CmdCobranza.Visible = False
        CmdFactoring.Visible = False
    End If
    
    
    
    Filtro = Empty
    DTInicio.Value = Date - 30
    DtHasta.Value = Date
    DtVencidas.Value = Date
    Sm_FiltroEstado = " HAVING saldo > 0   "
    
 
    
    sm_FiltroFecha = Empty
    Sm_FiltroVencidas = Empty
    
  
End Sub

Private Sub CargaClientes()
    Me.Bar_Cargando.Visible = True
   ' Timer2.Enabled = True
    
   ' DoEvents

    TxtTotales = 0
    TxtAbonos = 0
    txtSaldos = 0
    Normal = "CONCAT(doc_nombre,IF(doc_factura_guias='SI',' Por guias',''))"
    If SP_Rut_Activo = "76.354.346-3" Then
        Normal = "tipo_doc"
    End If
    Sql = "SELECT v.id,no_documento," & Normal & " doc_nombre,v.rut_cliente,nombre_rsocial cliente," & _
                 "IF(v.suc_id=0,'CM',CONCAT(suc_ciudad,' - ',suc_direccion)) sucursal," & _
                 "fecha,v.ven_fecha_vencimiento,bruto," & _
                 "IFNULL((SELECT SUM(t.ctd_monto) FROM cta_abono_documentos t ,cta_abonos a WHERE t.rut_emp='" & SP_Rut_Activo & "' AND t.abo_id=a.abo_id AND a.abo_cli_pro='CLI' AND t.id=v.id),0) abonos, " & _
                 "bruto-" & _
                 "IFNULL((SELECT SUM(t.ctd_monto) FROM cta_abono_documentos t ,cta_abonos a WHERE t.rut_emp='" & SP_Rut_Activo & "' AND t.abo_id=a.abo_id AND a.abo_cli_pro='CLI' AND t.id=v.id),0) saldo, " & _
                 " " & _
                 "IF(" & _
                 "(SELECT COUNT(*) FROM cta_cobranza_documentos r,cta_cobranza z WHERE r.cbr_id=z.cbr_id AND r.id=v.id)>0,'C','') cobranza,doc_signo_libro,v.suc_id " & _
          "FROM ven_doc_venta v,maestro_clientes c,sis_documentos d,par_sucursales s " & _
          "WHERE v.rut_cliente<>'11.111.111-1' AND  ven_nc_utilizada='NO' /*AND doc_informa_venta='SI'*/ AND doc_nota_de_venta='NO' AND v.rut_emp='" & SP_Rut_Activo & "' AND  v.doc_id_factura=0 AND v.suc_id = s.suc_id And v.rut_cliente = c.rut_cliente AND v.doc_id = d.doc_id " & " " & _
          Filtro & sm_FiltroFecha & Sm_FiltroVencidas & _
          Sm_FiltroSucursal & Sm_FiltroEstado & Sm_Fguias & _
          " ORDER BY id"
          '27 Sep 2014
          ' se comento doc_informa_venta,
    Sis_BarraProgreso.Show 1
    'Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvDetalle, True, True, True, False
    FormatoGrilla
    Timer2.Enabled = False
    Me.Bar_Cargando.Visible = False
End Sub
Private Sub CargaProveedores()
   ' Me.Bar_Cargando.Visible = True
   ' Timer2.Enabled = True
    
    
    
    TxtTotales = 0
    TxtAbonos = 0
    txtSaldos = 0


  Sql = "SELECT v.id,no_documento," & _
                  "CAST(IF(id_ref=0,CONCAT(doc_nombre,IF(doc_factura_guias = 'SI',' Por guias',''))," & _
                  "CONCAT(d.doc_nombre,' Ref. '," & _
    "(SELECT CONCAT(y.doc_abreviado,' ',no_documento) FROM com_doc_compra z INNER JOIN sis_documentos y USING(doc_id) WHERE z.id IN(v.id_ref))) " & _
    ") AS CHAR) doc_nombre " & _
    ",v.rut,nombre_proveedor,'',fecha, v.doc_fecha_vencimiento, total, IFNULL((" & _
                  "SELECT SUM(t.ctd_monto) FROM cta_abono_documentos t,cta_abonos a " & _
                        "WHERE t.rut_emp='" & SP_Rut_Activo & "' AND t.abo_id = a.abo_id AND a.abo_cli_pro = 'PRO' AND t.id = v.id),0)" & _
                        "/*+IFNULL((SELECT SUM(ctd_monto) FROM cta_abonos z " & _
                        "INNER JOIN cta_abono_documentos y USING(abo_id) " & _
                        "WHERE z.rut_emp='" & SP_Rut_Activo & "' AND y.rut_emp='" & SP_Rut_Activo & "' " & _
                        "AND y.id IN(SELECT id FROM com_doc_compra w WHERE w.id_ref=v.id)),0)*/ abonos," & _
                   "total - (IFNULL((SELECT SUM(t.ctd_monto) FROM cta_abono_documentos t,cta_abonos a " & _
                                        "WHERE t.rut_emp='" & SP_Rut_Activo & "' AND t.abo_id = a.abo_id AND a.abo_cli_pro = 'PRO' AND t.id = v.id ),0)" & _
                    "/*+IFNULL((SELECT SUM(ctd_monto) FROM cta_abonos z " & _
                        "INNER JOIN cta_abono_documentos y USING(abo_id) " & _
                        "WHERE z.rut_emp='" & SP_Rut_Activo & "' AND y.rut_emp='" & SP_Rut_Activo & "' " & _
                        "AND y.id IN(SELECT id FROM com_doc_compra w WHERE w.id_ref=v.id)),0)*/) " & _
                        " saldo,'' cobranza,doc_signo_libro,0,com_folio_texto " & _
          "FROM    com_doc_compra v,   maestro_proveedores c,  sis_documentos d " & _
          "WHERE v.com_nc_utilizada='NO' AND v.rut_emp='" & SP_Rut_Activo & "' AND id_ref=0 AND  v.doc_id_factura = 0 AND v.Rut = c.rut_proveedor AND v.doc_id = d.doc_id  " & _
          Filtro & sm_FiltroFecha & Sm_FiltroVencidas & Sm_Fguias & _
          " " & Sm_FiltroEstado & " " & _
          "ORDER BY id"
    'DoEvents
  '  Sis_Cargando.ConsultaRescata RsTmp, Sql
    Sis_BarraProgreso.Show 1
    'Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvDetalle, True, True, True, False
    LvDetalle.ColumnHeaders(3).Width = LvDetalle.ColumnHeaders(3).Width + (LvDetalle.ColumnHeaders(6).Width / 2)
    LvDetalle.ColumnHeaders(5).Width = LvDetalle.ColumnHeaders(5).Width + (LvDetalle.ColumnHeaders(6).Width / 2) + LvDetalle.ColumnHeaders(12).Width
    LvDetalle.ColumnHeaders(6).Width = 0
    LvDetalle.ColumnHeaders(12).Width = 0
    FormatoGrilla
   ' Timer2.Enabled = False
   ' Me.Bar_Cargando.Visible = False
End Sub

Private Sub LvDetalle_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    ordListView ColumnHeader, Me, LvDetalle
End Sub

Private Sub LvDetalle_DblClick()
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    'Filtrar lista por el rut seleccionado
    TxtRut = LvDetalle.SelectedItem.SubItems(3)
    TxtRut_Validate True


End Sub

Private Sub Option1_Click()
    If Option1.Value Then
        sm_FiltroFecha = Empty
        DTInicio.Enabled = False
        DtHasta.Enabled = False
        CmdVer.Visible = False
        If Sp_extra = "CLIENTES" Then CargaClientes Else CargaProveedores
    Else
        DTInicio.Enabled = True
        DtHasta.Enabled = True
    End If
        
End Sub

Private Sub Option2_Click()
    DTInicio.Enabled = True
    DtHasta.Enabled = True
    CmdVer.Visible = True
End Sub

Private Sub Timer1_Timer()
    On Error Resume Next
    LvDetalle.SetFocus
    Timer1.Enabled = False
End Sub
Private Sub cmdBtnRut_Click()
    ClienteEncontrado = False
    SG_codigo = Empty
    
    If Sp_extra = "CLIENTES" Then
        BuscaCliente.Show 1
    Else
        BuscaProveedor.Show 1
    End If
    
    TxtRut = SG_codigo
    If SG_codigo <> Empty Then
        TxtRut_Validate (True)
    End If
End Sub


Private Sub Timer2_Timer()
    Lp_ValueBar = Lp_ValueBar + 1
    Bar_Cargando.Value = Lp_ValueBar
    If Lp_ValueBar = 99 Then Lp_ValueBar = 1
End Sub

Private Sub Timer3_Timer()
    If Sp_extra = "CLIENTES" Then
        CargaClientes
    Else
        CargaProveedores
    End If
    Timer3.Enabled = False
End Sub







Private Sub TxtRut_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
    If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtRut_Validate(Cancel As Boolean)
    Dim Sp_CP As String
    If Len(TxtRut.Text) = 0 Then Exit Sub
    TxtRut.Text = Replace(TxtRut.Text, ".", "")
    TxtRut.Text = Replace(TxtRut.Text, "-", "")
    Respuesta = VerificaRut(TxtRut.Text, NuevoRut)
    If Respuesta = True Then
        Me.TxtRut.Text = NuevoRut
        'Buscar el cliente
        If Sp_extra = "CLIENTES" Then
            Sql = "SELECT rut_cliente rut," & _
                        "m.nombre_rsocial nombre," & _
                        "fono,email,direccion,ciudad " & _
                  "FROM maestro_clientes m " & _
                  "INNER JOIN par_asociacion_ruts a ON m.rut_cliente=a.rut_cli " & _
                  "WHERE  a.rut_emp='" & SP_Rut_Activo & "' AND  m.rut_cliente='" & TxtRut & "'"
        Else
             Sql = " SELECT p.rut_proveedor rut, nombre_empresa nombre,fono,email,direccion,ciudad " & _
                    "FROM maestro_proveedores p " & _
                    "INNER JOIN par_asociacion_ruts a ON p.rut_proveedor=a.rut_pro " & _
                    "WHERE a.rut_emp='" & SP_Rut_Activo & "' AND rut_proveedor='" & TxtRut & "'"
        
        End If
                    
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
                'Cliente encontrado
            With RsTmp
                TxtRut.Text = !rut
                txtCliente = !nombre
                txtFono = "" & !fono
                TxtMail = "" & !Email
                TxtDireccion = "" & !direccion
                TxtCiudad = "" & !ciudad
            End With
            If Sp_extra = "CLIENTES" Then
                Filtro = " AND v.rut_cliente='" & TxtRut & "' "
            Else
                Filtro = " AND v.rut='" & TxtRut & "' "
            End If
        End If
        If Sp_extra = "CLIENTES" Then
            CargaClientes
            Sp_CP = "CLI"
        Else
            CargaProveedores
            Sp_CP = "PRO"
        End If
        'Buscaremos si el rut tiene saldo a favor y lo mostraremos
        '10 Nov 2012
        Sql = "SELECT IFNULL(SUM(pzo_abono)- SUM(pzo_cargo),0) saldo " & _
                "FROM cta_pozo " & _
                "WHERE rut_emp = '" & SP_Rut_Activo & "' AND pzo_rut = '" & TxtRut & "' AND pzo_cli_pro = '" & Sp_CP & "'"
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
            If (RsTmp!saldo) > 0 Then
                Me.FrmSaldo.Visible = True
                SkSaldo = NumFormat(RsTmp!saldo)
            Else
                Me.FrmSaldo.Visible = False
            End If
        End If
    End If
    
End Sub
Private Sub ImprimeCartola()
    Dim Sp_Texto As String
    Dim Sp_Lh As String
    Dim Lp_Contador As Long
    Dim Ip_LineasPorPaginas As Integer
    
    On Error GoTo ErrorP
    Ip_LineasPorPaginas = 50
    Cx = 2
    Cy = 1
    For i = 1 To 147
        Sp_Lh = Sp_Lh & "="
    Next
    
 
        
        Lp_Contador = 1
        Im_Paginas = 1
        Ip_Paginas = Int(LvDetalle.ListItems.Count / Ip_LineasPorPaginas)
        If (LvDetalle.ListItems.Count Mod Ip_LineasPorPaginas) > 0 Then
            Ip_Paginas = Ip_Paginas + 1
        End If
        ResumenEncabezadoLaser
        Cy = Cy + Dp
        
        For i = 1 To Me.LvDetalle.ListItems.Count
            P_Fecha = LvDetalle.ListItems(i).SubItems(6)
            P_Documento = LvDetalle.ListItems(i).SubItems(2)
            P_Numero = LvDetalle.ListItems(i).SubItems(1)
            P_Vence = LvDetalle.ListItems(i).SubItems(7)
            RSet P_Venta = LvDetalle.ListItems(i).SubItems(8)
            RSet P_Pago = LvDetalle.ListItems(i).SubItems(9)
            RSet P_Saldo = LvDetalle.ListItems(i).SubItems(10)
            EnviaLineasC False
            Cy = Cy + Dp
            Lp_Contador = Lp_Contador + 1
            If Lp_Contador = Ip_LineasPorPaginas Then
                Printer.NewPage
                Im_Paginas = Im_Paginas + 1
                ResumenEncabezadoLaser
                Cy = Cy + Dp
                Lp_Contador = 1
            End If
        Next
        PieDeCartola
        Printer.NewPage
        Printer.EndDoc
        'FIN DEL DOCUMETNO"
    Exit Sub
ErrorP:
    MsgBox "Problema al imprimir..." & vbNewLine & Err.Number & vbNewLine & Err.Description & vbNewLine & Err.Source
  

End Sub


Private Sub ResumenEncabezadoLaser()
    Dim Sp_Lh As String, Sp_Tit As String
    Dim Sp_Cetras As String
    
    
    For i = 1 To 100
        Sp_Lh = Sp_Lh & "_"
    Next
    Printer.FontName = "Courier New"
    

    Dp = 0.35
    Cx = 2
    Cy = 1.5
    Coloca Cx + 15, Cy, "P�gina:" & Im_Paginas & " de " & Ip_Paginas, False, 8
    Cy = Cy + Dp
    Coloca Cx + 15, Cy, "Fecha :" & Date, False, 8
    
    Cx = 2
    Cy = 2
   
    
    Cy = Cy + Dp
    'Datos de la empresa
    Sql = "SELECT direccion,giro,comuna,ciudad " & _
            "FROM sis_empresas " & _
            "WHERE rut='" & SP_Rut_Activo & "'"
    Consulta RsTmp, Sql
    
    
    '2-Abril-2015
    Coloca Cx, Cy, Principal.SkEmpresa, True, 10 'Nombre empresa
    Cy = Cy + Dp
    Coloca Cx, Cy, Principal.SkRut, False, 10 'rut
    Cy = Cy + Dp
    Coloca Cx, Cy, RsTmp!direccion & " - " & RsTmp!comuna & " - " & RsTmp!ciudad, False, 10 'direccion
    Cy = Cy + Dp
    Coloca Cx, Cy, RsTmp!giro, False, 10 'giro
    Cy = Cy + Dp
    Coloca Cx, Cy, Sp_Lh, False, 8
    Cy = Cy + Dp
    Coloca Cx, Cy, "                " & "CARTOLA CLIENTE", True, 14 ' Titulo
    Cy = Cy + Dp
    Coloca Cx, Cy, Sp_Lh, False, 8
    
 
    'Ahora datos del cliente
    Cy = Cy + (Dp * 2)
    Coloca Cx, Cy, "R.U.T.:" & TxtRut & "       NOMBRE:" & Me.txtCliente, True, 10 'Nombre empresa
    Cy = Cy + Dp + Dp
    Coloca Cx, Cy, "DIRECCION:" & TxtDireccion & "       CIUDAD:" & TxtCiudad, False, 10 'rut
    Cy = Cy + Dp + Dp
    Coloca Cx, Cy, Sp_Lh, False, 8
  
    P_Fecha = "Fecha"
    P_Documento = "Documento"
    P_Numero = "Numero"
    P_Vence = "Venc."
    RSet P_Venta = "Monto Vta"
    RSet P_Pago = "Abono"
    RSet P_Saldo = "Saldo"
    Cy = Cy + Dp
    EnviaLineasC True
    Cy = Cy + Dp
     Printer.FontName = "Courier New"
    Coloca Cx, Cy, Sp_Lh, False, 8
     
    
    
    
    
End Sub

Private Function EnviaLineasC(Negrita As Boolean)
        Dim avance As Double
        avance = 1.5
        Printer.FontSize = 10
        Printer.FontName = "Arial Narrow"
        
        Coloca Cx, Cy, P_Fecha, Negrita, 10
        Coloca Cx + 1.7, Cy, P_Documento, Negrita, 10
        Coloca Cx + 7, Cy, P_Numero, Negrita, 10
        Coloca Cx + 8.6, Cy, P_Vence, Negrita, 10
        Coloca (Cx + 12) - Printer.TextWidth(P_Venta), Cy, P_Venta, Negrita, 10
        Coloca (Cx + 14.2) - Printer.TextWidth(P_Pago), Cy, P_Pago, Negrita, 10
        Coloca (Cx + 17) - Printer.TextWidth(P_Saldo), Cy, P_Saldo, Negrita, 10
       
       
End Function


Private Sub PieDeCartola()
    Dim Sp_Lh As String, Sp_Tit As String
    Dim Sp_Cetras As String
    For i = 1 To 100
        Sp_Lh = Sp_Lh & "_"
    Next
    Printer.FontName = "Courier New"
    Cy = Cy + Dp
    P_Fecha = ""
    P_Documento = "TOTALES:"
    P_Numero = ""
    P_Vence = ""
    RSet P_Venta = TxtTotales
    RSet P_Pago = TxtAbonos
    RSet P_Saldo = txtSaldos
    Coloca Cx, Cy, Sp_Lh, False, 8
    Cy = Cy + Dp
    EnviaLineasC True
    Cy = Cy + Dp
     Printer.FontName = "Courier New"
    Coloca Cx, Cy, Sp_Lh, False, 8
End Sub

