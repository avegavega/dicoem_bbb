VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form gestion_controlpresupuesto 
   Caption         =   "Control de Presupuesto"
   ClientHeight    =   7740
   ClientLeft      =   3390
   ClientTop       =   3210
   ClientWidth     =   10395
   LinkTopic       =   "Form1"
   ScaleHeight     =   7740
   ScaleWidth      =   10395
   Begin VB.CommandButton CmdSalir 
      Cancel          =   -1  'True
      Caption         =   "Retorna"
      Height          =   450
      Left            =   8865
      TabIndex        =   13
      Top             =   6975
      Width           =   1215
   End
   Begin VB.Timer Timer1 
      Left            =   1440
      Top             =   255
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   915
      OleObjectBlob   =   "gestion_controlpresupuesto.frx":0000
      Top             =   240
   End
   Begin VB.Frame Frame1 
      Caption         =   "Asignar Presupuesto"
      Height          =   5820
      Left            =   360
      TabIndex        =   0
      Top             =   1080
      Width           =   9720
      Begin VB.TextBox TxtNombre 
         BackColor       =   &H8000000F&
         Height          =   315
         Left            =   810
         Locked          =   -1  'True
         TabIndex        =   12
         TabStop         =   0   'False
         Tag             =   "T"
         Top             =   915
         Width           =   4560
      End
      Begin VB.CommandButton CmdOk 
         Caption         =   "Ok"
         Height          =   300
         Left            =   8925
         TabIndex        =   11
         Top             =   900
         Width           =   525
      End
      Begin VB.TextBox Text3 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         Height          =   315
         Left            =   7725
         Locked          =   -1  'True
         TabIndex        =   10
         TabStop         =   0   'False
         Tag             =   "T"
         Top             =   915
         Width           =   1185
      End
      Begin VB.TextBox Text2 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         Height          =   315
         Left            =   6540
         Locked          =   -1  'True
         TabIndex        =   9
         TabStop         =   0   'False
         Tag             =   "T"
         Top             =   915
         Width           =   1185
      End
      Begin VB.TextBox TxtAsignado 
         Alignment       =   1  'Right Justify
         Height          =   315
         Left            =   5370
         TabIndex        =   8
         Tag             =   "T"
         Top             =   915
         Width           =   1185
      End
      Begin VB.TextBox TxtId 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         Height          =   315
         Left            =   195
         Locked          =   -1  'True
         TabIndex        =   7
         TabStop         =   0   'False
         Tag             =   "T"
         Top             =   915
         Width           =   615
      End
      Begin VB.CommandButton CmdMostrar 
         Caption         =   "Ver"
         Height          =   300
         Left            =   3120
         TabIndex        =   5
         Top             =   435
         Width           =   1935
      End
      Begin VB.ComboBox ComAno 
         Height          =   315
         Left            =   1905
         Style           =   2  'Dropdown List
         TabIndex        =   2
         Top             =   435
         Width           =   1215
      End
      Begin VB.ComboBox comMes 
         Height          =   315
         ItemData        =   "gestion_controlpresupuesto.frx":0234
         Left            =   330
         List            =   "gestion_controlpresupuesto.frx":0236
         Style           =   2  'Dropdown List
         TabIndex        =   1
         Top             =   435
         Width           =   1575
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
         Height          =   255
         Left            =   315
         OleObjectBlob   =   "gestion_controlpresupuesto.frx":0238
         TabIndex        =   3
         Top             =   240
         Width           =   375
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel5 
         Height          =   255
         Left            =   1920
         OleObjectBlob   =   "gestion_controlpresupuesto.frx":029C
         TabIndex        =   4
         Top             =   240
         Width           =   375
      End
      Begin MSComctlLib.ListView LvDetalle 
         Height          =   4395
         Left            =   210
         TabIndex        =   6
         Top             =   1245
         Width           =   9270
         _ExtentX        =   16351
         _ExtentY        =   7752
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   5
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Id"
            Object.Width           =   1058
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Nombre"
            Object.Width           =   8043
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   2
            Object.Tag             =   "N100"
            Text            =   "Asignado"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   3
            Object.Tag             =   "N100"
            Text            =   "Utilizado"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   4
            Object.Tag             =   "N100"
            Text            =   "Saldo"
            Object.Width           =   2117
         EndProperty
      End
   End
End
Attribute VB_Name = "gestion_controlpresupuesto"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub CmdMostrar_Click()
    CargaItems
End Sub

Private Sub CmdOk_Click()
        
    
    On Error GoTo errorGraba
    cn.BeginTrans
        cn.Execute "DELETE FROM ges_presupuesto_mensual " & _
                    "WHERE gas_id=" & TxtId & " AND pre_mes=" & comMes.ItemData(comMes.ListIndex) & " AND pre_ano=" & ComAno.Text & " AND rut_emp='" & SP_Rut_Activo & "'"
        cn.Execute "INSERT INTO ges_presupuesto_mensual (pre_mes,pre_ano,pre_valor,rut_emp,usu_asigna,gas_id) " & _
                "VALUES(" & comMes.ItemData(comMes.ListIndex) & "," & ComAno.Text & "," & CDbl(TxtAsignado) & ",'" & SP_Rut_Activo & "','" & LG_USUARIO & "'," & TxtId & ")"
             
    cn.CommitTrans
    TxtId = ""
    TxtNombre = ""
    TxtAsignado = "0"
    CargaItems
    Exit Sub
errorGraba:
    MsgBox "Error: " & Err.Description
    cn.RollbackTrans
End Sub

Private Sub CmdSalir_Click()
    Unload Me
End Sub

Private Sub Command1_Click()

End Sub

Private Sub Form_Load()
    Aplicar_skin Me
    Centrar Me
    
    For i = 1 To 12
        comMes.AddItem UCase(MonthName(i, False))
        comMes.ItemData(comMes.ListCount - 1) = i
    Next
    Busca_Id_Combo comMes, Val(IG_Mes_Contable)
    LLenaYears ComAno, 2010
    CargaItems
End Sub

Private Sub LvDetalle_DblClick()
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    
    TxtId = LvDetalle.SelectedItem
    TxtNombre = LvDetalle.SelectedItem.SubItems(1)
    TxtAsignado = LvDetalle.SelectedItem.SubItems(2)
    
End Sub

Private Sub Timer1_Timer()
    On Error Resume Next
    comMes.SetFocus
    Timer1.Enabled = False
End Sub
Private Sub CargaItems()
    Sql = "SELECT gas_id,gas_nombre," & _
            "(SELECT pre_valor FROM ges_presupuesto_mensual m WHERE m.gas_id=g.gas_id AND pre_mes=" & comMes.ItemData(comMes.ListIndex) & " AND pre_ano=" & ComAno.Text & " AND rut_emp='" & SP_Rut_Activo & "')" & _
            ",(SELECT SUM(d.cmd_total_neto)total_gasto " & _
                    "FROM com_doc_compra_detalle d " & _
                    "JOIN com_doc_compra c ON d.id = c.id " & _
                    "JOIN sis_documentos x ON c.doc_id = x.doc_id " & _
                    "WHERE   c.rut_emp = '76.361.539-1' " & _
                    "AND fecha BETWEEN '2015-04-01' AND '2015-04-15' " & _
                    "AND x.doc_orden_de_compra = 'NO' AND d.gas_id=g.gas_id " & _
                    "GROUP BY d.gas_id) " & _
            ",0 disponible " & _
            "FROM par_item_gastos g " & _
            "WHERE rut_emp='" & SP_Rut_Activo & "'"
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvDetalle, False, True, True, False
    For i = 1 To LvDetalle.ListItems.Count
        LvDetalle.ListItems(i).SubItems(4) = NumFormat(CDbl(LvDetalle.ListItems(i).SubItems(2)) - CDbl(LvDetalle.ListItems(i).SubItems(3)))
        If CDbl(LvDetalle.ListItems(i).SubItems(4)) < 0 Then
            For L = 1 To LvDetalle.ColumnHeaders.Count - 1
                LvDetalle.ListItems(i).ListSubItems(L).ForeColor = vbRed
            Next
        End If
    Next
End Sub
Private Sub TxtAsignado_GotFocus()
    En_Foco TxtAsignado
End Sub

Private Sub TxtAsignado_KeyPress(KeyAscii As Integer)
    KeyAscii = AceptaSoloNumeros(KeyAscii)
End Sub

Private Sub TxtAsignado_Validate(Cancel As Boolean)
    If Val(TxtAsignado) = 0 Then TxtAsignado = 0
End Sub
