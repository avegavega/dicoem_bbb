VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form Ban_SeleccionaChequeRendir 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Cheques disponibles fondos por rendir"
   ClientHeight    =   5880
   ClientLeft      =   2115
   ClientTop       =   885
   ClientWidth     =   11910
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5880
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton cmdSalir 
      Cancel          =   -1  'True
      Caption         =   "&No seguir"
      Height          =   405
      Left            =   10290
      TabIndex        =   4
      Top             =   5415
      Width           =   1350
   End
   Begin VB.Frame Frame1 
      Caption         =   "Cheques en fondos por rendir"
      Height          =   5145
      Left            =   300
      TabIndex        =   0
      Top             =   210
      Width           =   11355
      Begin VB.TextBox TxtSaldo 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   9555
         Locked          =   -1  'True
         TabIndex        =   9
         TabStop         =   0   'False
         Text            =   "0"
         Top             =   4695
         Width           =   1290
      End
      Begin VB.TextBox TxtRequerido 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   9555
         Locked          =   -1  'True
         TabIndex        =   7
         TabStop         =   0   'False
         Text            =   "0"
         Top             =   4365
         Width           =   1290
      End
      Begin VB.TextBox TxtSeleccion 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   9555
         Locked          =   -1  'True
         TabIndex        =   5
         TabStop         =   0   'False
         Text            =   "0"
         Top             =   4035
         Width           =   1290
      End
      Begin VB.CommandButton CmdContinua 
         Caption         =   "Continuar"
         Height          =   390
         Left            =   135
         TabIndex        =   1
         ToolTipText     =   "Salir"
         Top             =   4065
         Width           =   1440
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   210
         Left            =   6255
         OleObjectBlob   =   "Ban_SeleccionaChequeRendir.frx":0000
         TabIndex        =   2
         Top             =   4230
         Width           =   270
      End
      Begin MSComctlLib.ListView LvDetalle 
         Height          =   3705
         Left            =   195
         TabIndex        =   3
         Top             =   300
         Width           =   10965
         _ExtentX        =   19341
         _ExtentY        =   6535
         View            =   3
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   7
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "N109"
            Object.Width           =   529
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "N109"
            Text            =   "NRO CHEQUE"
            Object.Width           =   2293
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Key             =   "debe"
            Object.Tag             =   "T3000"
            Text            =   "BANCO"
            Object.Width           =   5292
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Object.Tag             =   "T1500"
            Text            =   "A"
            Object.Width           =   2646
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Object.Tag             =   "T1500"
            Text            =   "Concepto"
            Object.Width           =   2646
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   5
            Key             =   "haber"
            Object.Tag             =   "N100"
            Text            =   "DISPONIBLE"
            Object.Width           =   2469
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   6
            Object.Tag             =   "N100"
            Text            =   "UTILIZAR"
            Object.Width           =   2293
         EndProperty
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   225
         Left            =   8055
         OleObjectBlob   =   "Ban_SeleccionaChequeRendir.frx":005E
         TabIndex        =   6
         Top             =   4080
         Width           =   1440
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   225
         Left            =   8055
         OleObjectBlob   =   "Ban_SeleccionaChequeRendir.frx":00D3
         TabIndex        =   8
         Top             =   4410
         Width           =   1440
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
         Height          =   225
         Left            =   8055
         OleObjectBlob   =   "Ban_SeleccionaChequeRendir.frx":0148
         TabIndex        =   10
         Top             =   4740
         Width           =   1440
      End
   End
   Begin VB.Timer Timer1 
      Interval        =   30
      Left            =   495
      Top             =   30
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   0
      OleObjectBlob   =   "Ban_SeleccionaChequeRendir.frx":01A9
      Top             =   0
   End
End
Attribute VB_Name = "Ban_SeleccionaChequeRendir"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public Sm_EnviarA As String
Public Sm_Rut_Anticipo As String

Private Sub CmdContinua_Click()
    Dim Ip_CantSeleccionada As Integer
    If CDbl(TxtSeleccion) = 0 Then
        MsgBox "No selecciono ning�n cheque...", vbInformation
        Exit Sub
    End If
    
    If CDbl(TxtSaldo) > 0 Then
        MsgBox "Cheques no alcanzan a cubrir el documento..." & vbNewLine & "Debe abonarlo desde desde Control de Ingresos", vbInformation
        Exit Sub
    End If
    
    'Contando la cantidad de cheques seleccionados
    Ip_CantSeleccionada = 0
    For i = 1 To LvDetalle.ListItems.Count
        If LvDetalle.ListItems(i).Checked Then
            Ip_CantSeleccionada = Ip_CantSeleccionada + 1
        End If
    Next
    
    'Verificando si el monto de un cheque cubre lo requerido
    'If Ip_CantSeleccionada > 1 Then
    '    For i = 1 To LvDetalle.ListItems.Count
    '        If LvDetalle.ListItems(i).Checked Then
    '            If CDbl(LvDetalle.ListItems(i).SubItems(3)) >= CDbl(TxtRequerido) Then
    '                MsgBox "La seleccion no corresponde..." & vbNewLine & "Con solo un documento seleccionado alcanza a cubrir el monto requerido...", vbInformation
    '                Exit Sub
    '            End If
    '        End If
   '     Next
    
  '  End If
    SG_codigo = ""
    compra_Ingreso.LvDetalle.ListItems.Clear
    For i = 1 To LvDetalle.ListItems.Count
    
        If LvDetalle.ListItems(i).Checked Then
            SG_codigo = SG_codigo & LvDetalle.ListItems(i) & ","
            
            If Sm_EnviarA = "GASTOS" Then
                ComBoletasCompra.LvDetalle.ListItems.Add , , LvDetalle.ListItems(i)
                X = ComBoletasCompra.LvDetalle.ListItems.Count
                ComBoletasCompra.LvDetalle.ListItems(X).SubItems(1) = LvDetalle.ListItems(i).SubItems(1)
                ComBoletasCompra.LvDetalle.ListItems(X).SubItems(2) = LvDetalle.ListItems(i).SubItems(2)
                ComBoletasCompra.LvDetalle.ListItems(X).SubItems(3) = LvDetalle.ListItems(i).SubItems(5)
                ComBoletasCompra.LvDetalle.ListItems(X).SubItems(4) = LvDetalle.ListItems(i).SubItems(6)
            
            
            
            ElseIf Sm_EnviarA = "COMPRAS" Then
            
                compra_Ingreso.LvDetalle.ListItems.Add , , LvDetalle.ListItems(i)
                X = compra_Ingreso.LvDetalle.ListItems.Count
                compra_Ingreso.LvDetalle.ListItems(X).SubItems(1) = LvDetalle.ListItems(i).SubItems(1)
                compra_Ingreso.LvDetalle.ListItems(X).SubItems(2) = LvDetalle.ListItems(i).SubItems(2)
                compra_Ingreso.LvDetalle.ListItems(X).SubItems(3) = LvDetalle.ListItems(i).SubItems(5)
                compra_Ingreso.LvDetalle.ListItems(X).SubItems(4) = LvDetalle.ListItems(i).SubItems(6)
                        
            ElseIf Sm_EnviarA = "TESORERIA" Then
            
            
            
            End If
        End If
    Next
    SG_codigo = Mid(SG_codigo, 1, Len(SG_codigo) - 1)
    
    
    
    
    
    
    
    Unload Me
End Sub
Private Sub cmdSalir_Click()
    SG_codigo = Empty
    Unload Me
End Sub

Private Sub Form_Load()
    Centrar Me
    Aplicar_skin Me
End Sub
Private Sub CargaC()
    Dim Sp_FiltroAnticipo As String
    If Len(Sm_Rut_Anticipo) > 0 Then
        Sp_FiltroAnticipo = " AND f.pro_rut='" & Sm_Rut_Anticipo & "' "
    End If
    Sql = "SELECT f.che_id,c.che_numero,t.cte_nombre_banco," & _
            "(SELECT des_nombre FROM ban_destinatarios d WHERE m.con_id=d.des_id) destinatario," & _
             "(SELECT con_nombre FROM ban_conceptos d WHERE m.des_id=d.con_id) concepto, " & _
            "SUM(f.chf_haber-f.chf_debe) disponible,0 utilizar " & _
        "FROM ban_cheques_fondos_rendir f " & _
        "LEFT JOIN ban_cheques c ON f.che_id=c.che_id " & _
        "LEFT  JOIN ban_chequera r USING(chc_id) " & _
        "LEFT JOIN ban_cta_cte t USING(cte_id) " & _
        "LEFT JOIN ban_movimientos m ON f.mov_id=m.mov_id " & _
        "WHERE t.rut_emp='" & SP_Rut_Activo & "' " & Sp_FiltroAnticipo & " " & _
        "GROUP BY f.che_id " & _
        "HAVING disponible>0"
      Sql = Sql & " UNION SELECT f.che_id,0,cte_nombre_banco,(SELECT  des_nombre " & _
                                        "FROM ban_destinatarios d " & _
                                        "WHERE   m.des_id = d.des_id)destinatario,  " & _
                                        "(SELECT con_nombre " & _
                                            "FROM ban_conceptos d " & _
                                            "WHERE m.con_id = d.con_id)concepto " & _
                ",m.mov_valor-(SELECT SUM(chf_debe) FROM ban_cheques_fondos_rendir q WHERE q.che_id=f.che_id)  disponible,0 utilizar " & _
            "FROM ban_cheques_fondos_rendir f " & _
            "LEFT JOIN ban_movimientos m ON f.mov_id = m.mov_id " & _
            "LEFT JOIN ban_cta_cte c ON c.cte_id=m.cte_id " & _
            "WHERE m.mov_identificacion<>'CHEQUE' AND c.rut_emp = '" & SP_Rut_Activo & "' " & _
            "GROUP BY f.che_id " & _
            "HAVING disponible > 0"
        
        
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvDetalle, True, True, False, False
End Sub

Private Sub LvDetalle_ItemCheck(ByVal Item As MSComctlLib.ListItem)
    TxtSeleccion = 0
    If CDbl(TxtSaldo) > 0 Then
        If CDbl(Item.SubItems(5)) >= CDbl(TxtSaldo) Then
            Item.SubItems(6) = CDbl(TxtSaldo)
        Else
            Item.SubItems(6) = Item.SubItems(5)
        End If
        If Not Item.Checked Then Item.SubItems(6) = 0
    End If
    
    For i = 1 To LvDetalle.ListItems.Count
        If LvDetalle.ListItems(i).Checked Then TxtSeleccion = CDbl(TxtSeleccion) + CDbl(LvDetalle.ListItems(i).SubItems(6))
    Next
    TxtSeleccion = NumFormat(TxtSeleccion)
    TxtSaldo = NumFormat(CDbl(TxtRequerido) - CDbl(TxtSeleccion))
    
    If Not Item.Checked Then
        Item.SubItems(6) = 0
    End If
End Sub

Private Sub Timer1_Timer()
    On Error Resume Next
    LvDetalle.SetFocus
    Timer1.Enabled = False
    CargaC
End Sub
