VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Begin VB.Form Ban_SeleccionaCuenta 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Seleccione Cuenta"
   ClientHeight    =   8310
   ClientLeft      =   3915
   ClientTop       =   3015
   ClientWidth     =   7380
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8310
   ScaleWidth      =   7380
   Begin VB.CommandButton cmdSalir 
      Cancel          =   -1  'True
      Caption         =   "&Retornar"
      Height          =   465
      Left            =   6045
      TabIndex        =   2
      ToolTipText     =   "Salir sin hacer cambios"
      Top             =   7710
      Width           =   1050
   End
   Begin VB.Timer Timer1 
      Interval        =   10
      Left            =   480
      Top             =   30
   End
   Begin VB.Frame FrmSub 
      Caption         =   "Seleccione Cuenta"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   6885
      Left            =   405
      TabIndex        =   0
      Top             =   750
      Width           =   6705
      Begin VB.CommandButton cmdSub 
         Caption         =   "Command6"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   570
         Index           =   0
         Left            =   810
         TabIndex        =   1
         Top             =   -210
         Visible         =   0   'False
         Width           =   5250
      End
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   0
      OleObjectBlob   =   "Ban_SeleccionaCuenta.frx":0000
      Top             =   0
   End
End
Attribute VB_Name = "Ban_SeleccionaCuenta"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdSalir_Click()
    SG_codigo = Empty
    Unload Me
End Sub
Private Sub cmdSub_Click(Index As Integer)
    Ban_Movimientos.Sm_Cuenta = cmdSub(Index).Caption
    Ban_Movimientos.Sp_IdCuenta = cmdSub(Index).Tag
    Ban_Movimientos.Lp_SaldoInicial = cmdSub(Index).ToolTipText
    Ban_Movimientos.Show 1
End Sub

Private Sub Form_Load()
    If cmdSub.Count > 2 Then
        For i = cmdSub.Count - 1 To 1 Step -1
            Unload cmdSub(i)
        Next
    ElseIf cmdSub.Count = 2 Then
        Unload cmdSub(1)
    End If
    Sql = "SELECT cte_id men_id,CONCAT(pla_nombre,'  Nro ',cte_numero) men_nombre,cte_saldo_inicial men_tooltips " & _
            "FROM ban_cta_cte c " & _
            "INNER JOIN con_plan_de_cuentas b ON b.pla_id=c.ban_id " & _
            "WHERE cte_activo='SI' AND rut_emp='" & SP_Rut_Activo & "'"
    Consulta RsTmp, Sql
    
    If RsTmp.RecordCount > 0 Then
        With RsTmp
            i = 1
            .MoveFirst
            Do While Not .EOF
                 Load cmdSub(i)
                 cmdSub(i).Caption = !men_nombre
                 cmdSub(i).Height = cmdSub(0).Height
                 cmdSub(i).Top = cmdSub(i - 1).Top + cmdSub(0).Height + 100
                 cmdSub(i).ToolTipText = !men_tooltips
                 cmdSub(i).Visible = True
                 cmdSub(i).Tag = !men_id
                 i = i + 1
                .MoveNext
            Loop
        End With
    End If
    Centrar Me
    Aplicar_skin Me
End Sub
    
    
Private Sub Timer1_Timer()
    On Error Resume Next
    Me.SetFocus
    Timer1.Enabled = False
End Sub
