VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form Ban_EstadodeCheques 
   Caption         =   "Estado de Cheques de Fondos por Rendir"
   ClientHeight    =   9975
   ClientLeft      =   -345
   ClientTop       =   780
   ClientWidth     =   14925
   LinkTopic       =   "Form1"
   ScaleHeight     =   9975
   ScaleWidth      =   14925
   Begin VB.CommandButton CmdSalir 
      Caption         =   "&Retornar"
      Height          =   375
      Left            =   13050
      TabIndex        =   4
      Top             =   9525
      Width           =   1095
   End
   Begin VB.Frame Frame1 
      Caption         =   "Listado cheques de Fondos por rendir"
      Height          =   9165
      Left            =   300
      TabIndex        =   0
      Top             =   315
      Width           =   14520
      Begin VB.CommandButton CmdAsiento 
         Caption         =   "Asiento Contable Caja / Fondos por rendir."
         Height          =   435
         Left            =   240
         TabIndex        =   16
         Top             =   8190
         Width           =   3540
      End
      Begin VB.ComboBox CboEstado 
         Appearance      =   0  'Flat
         Height          =   315
         ItemData        =   "Ban_EstadodeCheques.frx":0000
         Left            =   3795
         List            =   "Ban_EstadodeCheques.frx":0002
         Style           =   2  'Dropdown List
         TabIndex        =   12
         Top             =   570
         Width           =   1980
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   195
         Left            =   3810
         OleObjectBlob   =   "Ban_EstadodeCheques.frx":0004
         TabIndex        =   11
         Top             =   360
         Width           =   1740
      End
      Begin VB.CommandButton CmdBusca 
         Caption         =   "Destinatarios"
         Height          =   270
         Left            =   255
         TabIndex        =   10
         Top             =   285
         Width           =   1680
      End
      Begin VB.ComboBox CboDestinatarios 
         Appearance      =   0  'Flat
         Height          =   315
         ItemData        =   "Ban_EstadodeCheques.frx":006E
         Left            =   255
         List            =   "Ban_EstadodeCheques.frx":0070
         Style           =   2  'Dropdown List
         TabIndex        =   9
         Top             =   570
         Width           =   3555
      End
      Begin VB.Frame Frame2 
         Caption         =   "Documentos pagados con cheque seleccionado"
         Height          =   3600
         Left            =   180
         TabIndex        =   2
         Top             =   4515
         Width           =   14175
         Begin VB.Frame Frame3 
            Caption         =   "Fecha"
            Height          =   705
            Left            =   135
            TabIndex        =   13
            Top             =   2820
            Width           =   6330
            Begin MSComCtl2.DTPicker DtFecha 
               Height          =   300
               Left            =   1290
               TabIndex        =   15
               Top             =   270
               Width           =   1335
               _ExtentX        =   2355
               _ExtentY        =   529
               _Version        =   393216
               Format          =   273416193
               CurrentDate     =   41181
            End
            Begin VB.CommandButton CmdRinde 
               Caption         =   "�&Rinde saldo a  Caja con esta fecha?"
               Height          =   375
               Left            =   2775
               TabIndex        =   14
               Top             =   240
               Width           =   3300
            End
         End
         Begin VB.TextBox TxtUtilizado 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   9360
            Locked          =   -1  'True
            TabIndex        =   6
            TabStop         =   0   'False
            Text            =   "0"
            Top             =   2880
            Width           =   1290
         End
         Begin VB.TextBox TxtSaldo 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   9360
            Locked          =   -1  'True
            TabIndex        =   5
            TabStop         =   0   'False
            Text            =   "0"
            Top             =   3210
            Width           =   1290
         End
         Begin MSComctlLib.ListView LvD 
            Height          =   2400
            Left            =   150
            TabIndex        =   3
            Top             =   315
            Width           =   13890
            _ExtentX        =   24500
            _ExtentY        =   4233
            View            =   3
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            FullRowSelect   =   -1  'True
            GridLines       =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   0
            NumItems        =   5
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Object.Tag             =   "T3000"
               Text            =   "Documento"
               Object.Width           =   4410
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Object.Tag             =   "N109"
               Text            =   "Nro"
               Object.Width           =   2646
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   2
               Object.Tag             =   "T1500"
               Text            =   "R.U.T."
               Object.Width           =   2646
            EndProperty
            BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   3
               Object.Tag             =   "T3500"
               Text            =   "Proveedor"
               Object.Width           =   6174
            EndProperty
            BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   4
               Key             =   "total"
               Object.Tag             =   "N100"
               Text            =   "Valor"
               Object.Width           =   2646
            EndProperty
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
            Height          =   225
            Left            =   7860
            OleObjectBlob   =   "Ban_EstadodeCheques.frx":0072
            TabIndex        =   7
            Top             =   2925
            Width           =   1440
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
            Height          =   225
            Left            =   7860
            OleObjectBlob   =   "Ban_EstadodeCheques.frx":00E7
            TabIndex        =   8
            Top             =   3255
            Width           =   1440
         End
      End
      Begin MSComctlLib.ListView LvDetalle 
         Height          =   3300
         Left            =   210
         TabIndex        =   1
         Top             =   1140
         Width           =   14175
         _ExtentX        =   25003
         _ExtentY        =   5821
         View            =   3
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   10
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "N109"
            Text            =   "che id"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "N109"
            Text            =   "Nro Cheque"
            Object.Width           =   2293
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "T2000"
            Text            =   "Destinatario"
            Object.Width           =   3528
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Object.Tag             =   "F1000"
            Text            =   "Fecha Emision"
            Object.Width           =   2381
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Object.Tag             =   "T3500"
            Text            =   "Banco"
            Object.Width           =   3528
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   5
            Object.Tag             =   "N100"
            Text            =   "Valor"
            Object.Width           =   2293
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   6
            Object.Tag             =   "N100"
            Text            =   "Utilizado"
            Object.Width           =   2293
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   7
            Object.Tag             =   "N100"
            Text            =   "Por Rendir"
            Object.Width           =   2293
         EndProperty
         BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   8
            Object.Tag             =   "T1500"
            Text            =   "Tipo"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   9
            Object.Tag             =   "T2000"
            Text            =   "Proveedor"
            Object.Width           =   3528
         EndProperty
      End
   End
   Begin VB.Timer Timer1 
      Interval        =   10
      Left            =   540
      Top             =   8295
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   -45
      OleObjectBlob   =   "Ban_EstadodeCheques.frx":015E
      Top             =   8310
   End
End
Attribute VB_Name = "Ban_EstadodeCheques"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim Sm_Having As String, Sm_filtro As String

Private Sub CboDestinatarios_Click()
    CargaCheques

End Sub

Private Sub CboEstado_Click()
    CargaCheques
    
End Sub

Private Sub CmdAsiento_Click()
    Ban_AsientoContableFondoRendir.Show 1
End Sub

Private Sub CmdBusca_Click()
    With BuscarSimple
        SG_codigo = Empty
        
        .Sm_Consulta = "SELECT che_id,che_destinatario " & _
                        "FROM ban_cheques INNER JOIN ban_chequera USING(chc_id) INNER JOIN ban_cta_cte USING(cte_id) " & _
                        "WHERE pla_id=10 AND LENGTH(che_destinatario)>0 and rut_emp='" & SP_Rut_Activo & "' "
                                                
        
        '.Sm_Consulta = "SELECT pla_id, pla_nombre,tpo_nombre,det_nombre " & _
                       "FROM    con_plan_de_cuentas p,  con_tipo_de_cuenta t,   con_detalle_tipo_cuenta d " & _
                       "WHERE   p.tpo_id = t.tpo_id AND p.det_id = d.det_id "
                       
        .Sm_CampoLike = "che_destinatario"
        .Sm_Group = " GROUP BY che_destinatario "
        .Sm_Order = " ORDER BY che_destinatario "
        .Im_Columnas = 2
        .Show 1
        If SG_codigo = Empty Then Exit Sub
        Busca_Id_Combo CboDestinatarios, Val(SG_codigo)
    End With
End Sub

Private Sub CmdRinde_Click()
    Dim Lp_IdAbo As Long, Lp_Nro_Comprobante As Long, Lp_Saldo As Long
    If CDbl(TxtSaldo) = 0 Then Exit Sub
    If MsgBox("�Confirma Rendir a Caja el saldo?", vbQuestion + vbOKCancel) = vbNo Then Exit Sub
    
    'Aqui saldamos el disponible     29-Sep-2012
    'y hacemos cuadrar el chque
    'Ingresaromos como si fuera una compra, para saldar
    
    On Error GoTo errorGraba
            
    cn.BeginTrans
        Lp_IdCompra = UltimoNro("com_doc_compra", "id")
        Lp_Nro_Comprobante = AutoIncremento("lee", 200, , IG_id_Sucursal_Empresa)
        Lp_IdAbo = UltimoNro("cta_abonos", "abo_id")
        Lp_Saldo = CDbl(TxtSaldo)
        PagoDocumento Lp_IdAbo, "PRO", SP_Rut_Activo, DtFecha, Lp_Saldo, 9999, _
        "RENDICION", "SALDO FONDOS A RENDIR", LogUsuario, Lp_Nro_Comprobante, 0, "DEV"
        Sql = "INSERT INTO cta_abono_documentos (abo_id,id,ctd_monto,rut_emp) VALUES "
        Sql = Sql & "(" & Lp_IdAbo & "," & Lp_IdCompra & "," & Lp_Saldo & ",'" & SP_Rut_Activo & "')"
        cn.Execute Sql
        
        AutoIncremento "GUARDA", 200, Lp_Nro_Comprobante, IG_id_Sucursal_Empresa
        
        Sql = "INSERT INTO com_doc_compra (id,total,fecha,doc_id,mes_contable,ano_contable,rut_emp,rut) " & _
              "VALUES (" & Lp_IdCompra & "," & CDbl(TxtSaldo) & ",'" & Format(DtFecha.Value, "YYYY-MM-DD") & "'," & _
              IG_Id_DocFondosRendir & "," & IG_Mes_Contable & "," & IG_Ano_Contable & ",'" & SP_Rut_Activo & "','" & SP_Rut_Activo & "')"
        cn.Execute Sql
            
            
        cn.Execute "INSERT INTO ban_cheques_fondos_rendir (che_id,chf_debe,abo_id) " & _
                    "VALUES(" & LvDetalle.SelectedItem.Text & "," & CDbl(TxtSaldo) & "," & Lp_IdAbo & ")"
            
            
    cn.CommitTrans
    CargaCheques
    Exit Sub
errorGraba:
    
    MsgBox "Ocurrio un error al intentar grabar" & vbNewLine & Err.Number & " " & Err.Description, vbInformation
    cn.RollbackTrans
    
End Sub

Private Sub cmdSalir_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    Centrar Me
    Aplicar_skin Me
    Sm_Having = " HAVING disponible>0"
    DtFecha = Date
    
    LLenarCombo Me.CboDestinatarios, UCase("che_destinatario"), "che_id", _
    "ban_cheques INNER JOIN ban_chequera USING(chc_id) INNER JOIN ban_cta_cte USING(cte_id)", _
    "LENGTH(che_destinatario)>0 AND rut_emp='" & SP_Rut_Activo & "' " & "GROUP BY che_destinatario", "che_destinatario"
    Me.CboDestinatarios.AddItem "TODOS"
    Me.CboDestinatarios.ItemData(Me.CboDestinatarios.ListCount - 1) = 99999
    Me.CboDestinatarios.ListIndex = CboDestinatarios.ListCount - 1
    
    CboEstado.AddItem "POR RENDIR"
    CboEstado.AddItem "RENDIDOS"
    CboEstado.AddItem "TODOS"
    CboEstado.ListIndex = 0
    
   ' CargaCheques
    VerDetalle
    
End Sub

Private Sub LvDetalle_Click()
    VerDetalle
End Sub
Private Sub AsientoContable()



End Sub
Private Sub VerDetalle()
    Dim Lp_CheId As Long, Lp_Monto As Long
    LvD.ListItems.Clear
    TxtUtilizado = 0
    TxtSaldo = 0
    
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    Lp_Monto = LvDetalle.SelectedItem.SubItems(5)
    TxtUtilizado = LvDetalle.SelectedItem.SubItems(6)
    TxtSaldo = LvDetalle.SelectedItem.SubItems(7)
    Lp_CheId = LvDetalle.SelectedItem.Text
    Sql = "SELECT s.doc_nombre,c.no_documento nro,rut,p.nombre_empresa,c.total " & _
            "FROM com_doc_compra c " & _
            "INNER JOIN cta_abono_documentos d ON c.id=d.id " & _
            "LEFT JOIN maestro_proveedores p ON c.rut=p.rut_proveedor " & _
            "INNER JOIN sis_documentos s ON c.doc_id=s.doc_id " & _
            "WHERE abo_id IN (SELECT abo_id FROM ban_cheques_fondos_rendir WHERE che_id=" & Lp_CheId & " AND abo_id>0) "
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvD, False, True, True, False
End Sub

Private Sub LvDetalle_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    ordListView ColumnHeader, Me, LvDetalle
End Sub

Private Sub LvDetalle_KeyDown(KeyCode As Integer, Shift As Integer)
    VerDetalle
End Sub

Private Sub LvDetalle_KeyUp(KeyCode As Integer, Shift As Integer)
    VerDetalle
End Sub
Private Sub Timer1_Timer()
    On Error Resume Next
    LvDetalle.SetFocus
    Timer1.Enabled = False
End Sub
Private Sub CargaCheques()
    If Me.CboDestinatarios.ItemData(Me.CboDestinatarios.ListIndex) = 99999 Then
        Sm_filtro = Empty
    Else
        Sm_filtro = "AND c.che_destinatario='" & Me.CboDestinatarios.Text & "' "
    End If
    
    Select Case CboEstado.ListIndex
        Case 0
            Sm_Having = " HAVING disponible>0"
        Case 1
            Sm_Having = " HAVING disponible=0"
        Case 2
            Sm_Having = Empty
    End Select
    
    LvD.ListItems.Clear
    TxtSaldo = 0
    TxtUtilizado = 0
    
    Sql = "SELECT f.che_id, c.che_numero,c.che_destinatario,c.che_fecha_emision emision, p.pla_nombre banco, c.che_valor,SUM(chf_debe) utilizado,c.che_valor-SUM(chf_debe) disponible,f.chf_tipo," & _
            "(SELECT nombre_empresa FROM maestro_proveedores m WHERE f.pro_rut=m.rut_proveedor) " & _
            "FROM ban_cheques_fondos_rendir f " & _
            "INNER JOIN ban_cheques c USING(che_id) " & _
            "INNER JOIN ban_chequera r USING(chc_id) " & _
            "INNER JOIN ban_cta_cte e USING(cte_id) " & _
            "INNER JOIN con_plan_de_cuentas p ON e.ban_id=p.pla_id " & _
            "WHERE  /*f.abo_id>0 AND */ e.rut_emp='" & SP_Rut_Activo & "' " & Sm_filtro & _
            "GROUP BY f.che_id " & _
            Sm_Having
    Sql = Sql & " UNION SELECT f.che_id,0,(SELECT  des_nombre " & _
                                        "FROM ban_destinatarios d " & _
                                        "WHERE   m.des_id = d.des_id)destinatario,m.mov_fecha,cte_nombre_banco,m.mov_valor, " & _
                "(SELECT SUM(chf_debe) FROM ban_cheques_fondos_rendir q WHERE q.che_id=f.che_id) utilizado,m.mov_valor-(SELECT SUM(chf_debe) FROM ban_cheques_fondos_rendir q WHERE q.che_id=f.che_id)  disponible,chf_tipo,(SELECT con_nombre " & _
                                                                                "FROM ban_conceptos d " & _
                                                                "WHERE m.con_id = d.con_id)concepto " & _
            "FROM ban_cheques_fondos_rendir f " & _
            "LEFT JOIN ban_movimientos m ON f.mov_id = m.mov_id " & _
            "LEFT JOIN ban_cta_cte c ON c.cte_id=m.cte_id " & _
            "WHERE m.mov_identificacion<>'CHEQUE' AND c.rut_emp = '" & SP_Rut_Activo & "' " & _
            "GROUP BY f.che_id " & _
            Sm_Having
    
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvDetalle, False, True, True, False

End Sub
