VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{DA729E34-689F-49EA-A856-B57046630B73}#1.0#0"; "progressbar-xp.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form Ban_ConciliacionHistorica 
   Caption         =   "Conciliacion Historica-"
   ClientHeight    =   7515
   ClientLeft      =   2130
   ClientTop       =   1380
   ClientWidth     =   9180
   LinkTopic       =   "Form1"
   ScaleHeight     =   7515
   ScaleWidth      =   9180
   Begin VB.CommandButton cmdExportar 
      Caption         =   "Exportar Lista a Excel"
      Height          =   345
      Left            =   180
      TabIndex        =   10
      ToolTipText     =   "Exportar"
      Top             =   6840
      Width           =   1920
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Retornar"
      Height          =   360
      Left            =   7380
      TabIndex        =   7
      Top             =   6840
      Width           =   1515
   End
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Interval        =   50
      Left            =   30
      Top             =   615
   End
   Begin VB.Frame Frame1 
      Caption         =   "Historico"
      Height          =   5415
      Left            =   180
      TabIndex        =   5
      Top             =   1425
      Width           =   8730
      Begin VB.Frame FraProgreso 
         Caption         =   "Progreso exportaci�n"
         Height          =   795
         Left            =   360
         TabIndex        =   8
         Top             =   2295
         Visible         =   0   'False
         Width           =   7980
         Begin Proyecto2.XP_ProgressBar BarraProgreso 
            Height          =   330
            Left            =   150
            TabIndex        =   9
            Top             =   315
            Width           =   11130
            _ExtentX        =   19632
            _ExtentY        =   582
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BrushStyle      =   0
            Color           =   16777088
            Scrolling       =   1
            ShowText        =   -1  'True
         End
      End
      Begin MSComctlLib.ListView LvDetalle 
         Height          =   4980
         Left            =   210
         TabIndex        =   6
         Top             =   300
         Width           =   8415
         _ExtentX        =   14843
         _ExtentY        =   8784
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   4
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "T3000"
            Text            =   "Nombre"
            Object.Width           =   5292
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "F1000"
            Text            =   "Fecha"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   2
            Object.Tag             =   "N100"
            Text            =   "Valores"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   3
            Object.Tag             =   "N100"
            Text            =   "SubTotales"
            Object.Width           =   1764
         EndProperty
      End
   End
   Begin VB.Frame Frame4 
      Caption         =   "Mes y A�o"
      Height          =   1050
      Left            =   5325
      TabIndex        =   0
      Top             =   300
      Width           =   3555
      Begin VB.ComboBox ComAno 
         Height          =   315
         Left            =   2160
         Style           =   2  'Dropdown List
         TabIndex        =   2
         Top             =   600
         Width           =   1215
      End
      Begin VB.ComboBox comMes 
         Height          =   315
         ItemData        =   "Ban_ConciliacionHistorica.frx":0000
         Left            =   285
         List            =   "Ban_ConciliacionHistorica.frx":0002
         Style           =   2  'Dropdown List
         TabIndex        =   1
         Top             =   600
         Width           =   1890
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
         Height          =   255
         Left            =   285
         OleObjectBlob   =   "Ban_ConciliacionHistorica.frx":0004
         TabIndex        =   3
         Top             =   405
         Width           =   375
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel5 
         Height          =   255
         Left            =   2160
         OleObjectBlob   =   "Ban_ConciliacionHistorica.frx":0068
         TabIndex        =   4
         Top             =   420
         Width           =   375
      End
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   0
      OleObjectBlob   =   "Ban_ConciliacionHistorica.frx":00CC
      Top             =   0
   End
End
Attribute VB_Name = "Ban_ConciliacionHistorica"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub ComAno_Click()
    CargaHis
End Sub

Private Sub Command1_Click()
 Unload Me
End Sub

Private Sub CmdExportar_Click()
    Dim tit(2) As String
    If LvDetalle.ListItems.Count = 0 Then Exit Sub
    FraProgreso.Visible = True
    tit(0) = "MOVIMIENTO HISTORICO"
    tit(1) = "MES MOVIMIENTO : " & comMes
    tit(2) = "A�O MOVIMIENTO : " & ComAno
    ExportarNuevo LvDetalle, tit, Me, BarraProgreso
    FraProgreso.Visible = False
End Sub

Private Sub comMes_Click()
    CargaHis
End Sub

Private Sub Form_Load()
    
    Aplicar_skin Me
    
    For i = 1 To 12
        comMes.AddItem UCase(MonthName(i, False))
        comMes.ItemData(comMes.ListCount - 1) = i
    Next
    Busca_Id_Combo comMes, Val(IG_Mes_Contable)
    LLenaYears ComAno, 2010
    'For i = Year(Date) To Year(Date) - 1 Step -1
    '    ComAno.AddItem i
    '    ComAno.ItemData(ComAno.ListCount - 1) = i
    'Next
    'Busca_Id_Combo ComAno, Val(IG_Ano_Contable)
End Sub
Private Sub CargaHis()
    LvDetalle.ListItems.Clear
    If ComAno.ListIndex = -1 Or comMes.ListIndex = -1 Then Exit Sub
    Sql = "SELECT cbd_detalle,cbd_fecha,cbd_valores,cbd_subtotales " & _
            "FROM ban_conciliacion_historica h " & _
            "INNER JOIN ban_conciliacion_historica_detalle d ON h.cob_id=d.cob_id " & _
            "WHERE h.cob_mes=" & comMes.ItemData(comMes.ListIndex) & " AND cob_ano=" & Me.ComAno.Text & " AND rut_emp='" & SP_Rut_Activo & "'"
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvDetalle, False, True, True, False

End Sub
