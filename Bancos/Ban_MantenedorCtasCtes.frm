VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form Ban_MantenedorCtasCtes 
   Caption         =   "Mantenedor Ctas Ctes"
   ClientHeight    =   6825
   ClientLeft      =   615
   ClientTop       =   2325
   ClientWidth     =   12795
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   ScaleHeight     =   6825
   ScaleWidth      =   12795
   Begin VB.CommandButton cmdSalir 
      Cancel          =   -1  'True
      Caption         =   "&Retornar"
      Height          =   465
      Left            =   11160
      TabIndex        =   15
      Top             =   6225
      Width           =   1185
   End
   Begin VB.Timer Timer1 
      Interval        =   10
      Left            =   645
      Top             =   0
   End
   Begin VB.Frame Frame5 
      Caption         =   "Mantenedor Ctas Ctes."
      Height          =   5430
      Left            =   660
      TabIndex        =   6
      Top             =   750
      Width           =   11700
      Begin VB.CommandButton cmdCuenta 
         Caption         =   "Cuenta"
         Height          =   225
         Left            =   840
         TabIndex        =   16
         Top             =   465
         Width           =   870
      End
      Begin VB.ComboBox CboBanco 
         Appearance      =   0  'Flat
         Height          =   315
         ItemData        =   "Ban_MantenedorCtasCtes.frx":0000
         Left            =   825
         List            =   "Ban_MantenedorCtasCtes.frx":000A
         Style           =   2  'Dropdown List
         TabIndex        =   0
         Top             =   690
         Width           =   3300
      End
      Begin VB.TextBox TxtCuenta 
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   4125
         TabIndex        =   1
         Tag             =   "T"
         Top             =   690
         Width           =   1515
      End
      Begin VB.TextBox TxtEjecutivo 
         Appearance      =   0  'Flat
         Height          =   315
         IMEMode         =   3  'DISABLE
         Left            =   5640
         TabIndex        =   2
         Tag             =   "t"
         Top             =   690
         Width           =   2955
      End
      Begin VB.ComboBox ComActivo 
         Appearance      =   0  'Flat
         Height          =   315
         ItemData        =   "Ban_MantenedorCtasCtes.frx":0016
         Left            =   9825
         List            =   "Ban_MantenedorCtasCtes.frx":0020
         Style           =   2  'Dropdown List
         TabIndex        =   4
         Top             =   690
         Width           =   1140
      End
      Begin VB.CommandButton CmdOk 
         Caption         =   "Ok"
         Height          =   300
         Left            =   10965
         TabIndex        =   5
         Top             =   690
         Width           =   495
      End
      Begin VB.TextBox SkId 
         Appearance      =   0  'Flat
         BackColor       =   &H80000004&
         Height          =   315
         Left            =   240
         Locked          =   -1  'True
         TabIndex        =   7
         TabStop         =   0   'False
         Tag             =   "T"
         Top             =   690
         Width           =   585
      End
      Begin VB.TextBox TxtSaldoInicial 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         Height          =   315
         Left            =   8595
         Locked          =   -1  'True
         TabIndex        =   3
         Tag             =   "N"
         Text            =   "0.00"
         ToolTipText     =   "Para ingresar un  Saldo Inicial, debe hacer el m�dulo de movimientos."
         Top             =   690
         Width           =   1230
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel6 
         Height          =   255
         Left            =   270
         OleObjectBlob   =   "Ban_MantenedorCtasCtes.frx":002C
         TabIndex        =   8
         Top             =   5085
         Width           =   4095
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
         Height          =   255
         Left            =   5640
         OleObjectBlob   =   "Ban_MantenedorCtasCtes.frx":00B1
         TabIndex        =   9
         Top             =   510
         Width           =   2025
      End
      Begin MSComctlLib.ListView LvCuentas 
         Height          =   4020
         Left            =   240
         TabIndex        =   10
         Top             =   1035
         Width           =   11235
         _ExtentX        =   19817
         _ExtentY        =   7091
         View            =   3
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         MousePointer    =   1
         NumItems        =   7
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "N109"
            Text            =   "Id"
            Object.Width           =   1058
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "T3000"
            Text            =   "Banco"
            Object.Width           =   5821
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "T1000"
            Text            =   "Nro Cta"
            Object.Width           =   2646
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Object.Tag             =   "T2000"
            Text            =   "Ejecutivo"
            Object.Width           =   5203
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   4
            Object.Tag             =   "N100"
            Text            =   "Saldo Inicial"
            Object.Width           =   2205
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Object.Tag             =   "T1000"
            Text            =   "Habilitada"
            Object.Width           =   1940
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   6
            Object.Tag             =   "N109"
            Text            =   "Ban_Id"
            Object.Width           =   0
         EndProperty
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   255
         Index           =   1
         Left            =   4125
         OleObjectBlob   =   "Ban_MantenedorCtasCtes.frx":0121
         TabIndex        =   11
         Top             =   510
         Width           =   1095
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel5 
         Height          =   210
         Index           =   0
         Left            =   8895
         OleObjectBlob   =   "Ban_MantenedorCtasCtes.frx":0195
         TabIndex        =   12
         Top             =   495
         Width           =   915
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel5 
         Height          =   255
         Index           =   1
         Left            =   9870
         OleObjectBlob   =   "Ban_MantenedorCtasCtes.frx":020D
         TabIndex        =   13
         Top             =   495
         Width           =   1095
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   255
         Index           =   2
         Left            =   240
         OleObjectBlob   =   "Ban_MantenedorCtasCtes.frx":0287
         TabIndex        =   14
         Top             =   510
         Width           =   615
      End
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   -15
      OleObjectBlob   =   "Ban_MantenedorCtasCtes.frx":02E9
      Top             =   30
   End
End
Attribute VB_Name = "Ban_MantenedorCtasCtes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False


Private Sub cmdCuenta_Click()
    With BuscarSimple
        SG_codigo = Empty
        .Sm_Consulta = "SELECT pla_id, pla_nombre,tpo_nombre,det_nombre " & _
                       "FROM    con_plan_de_cuentas p,  con_tipo_de_cuenta t,   con_detalle_tipo_cuenta d " & _
                       "WHERE   p.tpo_id = t.tpo_id AND p.det_id = d.det_id AND p.tpo_id=1 "
        .Sm_CampoLike = "pla_nombre"
        .Im_Columnas = 2
        .Show 1
        If SG_codigo = Empty Then Exit Sub
        Busca_Id_Combo CboBanco, Val(SG_codigo)
    End With
End Sub

Private Sub CmdOk_Click()
    If CboBanco.ListIndex = -1 Or Len(Me.TxtCuenta) = 0 Then
        MsgBox "Seleccione banco, e ingrese Nro de cuenta...", vbInformation
        CboBanco.SetFocus
        Exit Sub
    End If
    
    If Val(SkId) = 0 Then 'Nuevo
        If LvCuentas.ListItems.Count > 0 Then
            For i = 1 To LvCuentas.ListItems.Count
                If LvCuentas.ListItems(i).SubItems(6) = CboBanco.ItemData(CboBanco.ListIndex) And LvCuentas.ListItems(i).SubItems(2) = TxtCuenta Then
                    MsgBox "Cuenta ingresada ya existe...", vbInformation
                    CboBanco.SetFocus
                    Exit Sub
                End If
            Next
        End If
        Sql = "INSERT INTO ban_cta_cte (ban_id,cte_numero,cte_ejecutivo,cte_saldo_inicial,rut_emp,cte_nombre_banco)  " & _
             "VALUES(" & CboBanco.ItemData(CboBanco.ListIndex) & ",'" & TxtCuenta & "','" & TxtEjecutivo & "'," & CDbl(TxtSaldoInicial) & ",'" & SP_Rut_Activo & "','" & CboBanco.Text & "')"
    Else 'Editando
       If LvCuentas.ListItems.Count > 0 Then
            For i = 1 To LvCuentas.ListItems.Count
                If LvCuentas.ListItems(i).SubItems(6) = CboBanco.ItemData(CboBanco.ListIndex) And LvCuentas.ListItems(i).SubItems(2) = TxtCuenta And SkId <> LvCuentas.ListItems(i) Then
                    MsgBox "Cuenta ingresada ya existe...", vbInformation
                    CboBanco.SetFocus
                    Exit Sub
                End If
            Next
        End If

    
    
    
        Sql = "UPDATE ban_cta_cte SET ban_id=" & CboBanco.ItemData(CboBanco.ListIndex) & "," & _
                                  "cte_numero='" & TxtCuenta & "'," & _
                          "cte_saldo_inicial=" & CDbl(TxtSaldoInicial) & "," & _
                          "cte_ejecutivo='" & TxtEjecutivo & "'," & _
                            "cte_activo='" & ComActivo.Text & "', " & _
                            "cte_nombre_banco='" & CboBanco.Text & "' " & _
              "WHERE cte_id=" & SkId
    End If
    cn.Execute Sql
    TxtEjecutivo = Empty
    TxtCuenta = Empty
    TxtSaldoInicial = 0
    ComActivo.ListIndex = 0
    
    CargaCtas
End Sub
Private Sub CargaCtas()
    On Error Resume Next
    Sql = "SELECT cte_id,pla_nombre,cte_numero,cte_ejecutivo,cte_saldo_inicial,cte_activo,c.ban_id " & _
          "FROM ban_cta_cte c INNER JOIN con_plan_de_cuentas  b ON b.pla_id=c.ban_id " & _
          "WHERE c.rut_emp='" & SP_Rut_Activo & "'"
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvCuentas, False, True, True, False
    CboBanco.SetFocus
End Sub
Private Sub cmdSalir_Click()
    Unload Me
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        SendKeys "{tab}"
        KeyAscii = 0
    End If
    
End Sub

Private Sub Form_Load()
    Centrar Me
    Aplicar_skin Me
    ComActivo.ListIndex = 0
    LLenarCombo CboBanco, "pla_nombre", "pla_id", "con_plan_de_cuentas", "pla_activo='SI' AND tpo_id=1"
    CargaCtas
End Sub

Private Sub LvCuentas_DblClick()
    If LvCuentas.SelectedItem Is Nothing Then Exit Sub
    SkId = LvCuentas.SelectedItem
    With LvCuentas.SelectedItem
        Busca_Id_Combo CboBanco, .SubItems(6)
        TxtCuenta = .SubItems(2)
        TxtEjecutivo = .SubItems(3)
        TxtSaldoInicial = .SubItems(4)
        ComActivo.ListIndex = IIf(.SubItems(5) = "SI", 0, 1)
    End With
    CboBanco.SetFocus
End Sub

Private Sub Timer1_Timer()
    On Error Resume Next
    CboBanco.SetFocus
    Timer1.Enabled = False
End Sub

Private Sub TxtCuenta_GotFocus()
    En_Foco Me.ActiveControl
End Sub



Private Sub TxtEjecutivo_GotFocus()
    En_Foco Me.ActiveControl
End Sub

Private Sub TxtEjecutivo_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub TxtEjecutivo_Validate(Cancel As Boolean)
TxtEjecutivo = Replace(TxtEjecutivo, "'", "")
End Sub

Private Sub TxtSaldoInicial_GotFocus()
    En_Foco Me.ActiveControl
End Sub

Private Sub TxtSaldoInicial_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
End Sub

Private Sub TxtSaldoInicial_Validate(Cancel As Boolean)
    TxtSaldoInicial = NumFormat(TxtSaldoInicial)
End Sub
