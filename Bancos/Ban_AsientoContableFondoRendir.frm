VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{DA729E34-689F-49EA-A856-B57046630B73}#1.0#0"; "progressbar-xp.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form Ban_AsientoContableFondoRendir 
   Appearance      =   0  'Flat
   BackColor       =   &H80000005&
   Caption         =   "ASIENTO CONTABLE"
   ClientHeight    =   5580
   ClientLeft      =   4620
   ClientTop       =   2970
   ClientWidth     =   7350
   LinkTopic       =   "Form1"
   ScaleHeight     =   5580
   ScaleWidth      =   7350
   Begin VB.Frame FraProgreso 
      Caption         =   "Progreso exportación"
      Height          =   795
      Left            =   2100
      TabIndex        =   5
      Top             =   2355
      Visible         =   0   'False
      Width           =   3180
      Begin Proyecto2.XP_ProgressBar BarraProgreso 
         Height          =   330
         Left            =   180
         TabIndex        =   6
         Top             =   360
         Width           =   2835
         _ExtentX        =   5001
         _ExtentY        =   582
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BrushStyle      =   0
         Color           =   16777088
         Scrolling       =   1
         ShowText        =   -1  'True
      End
   End
   Begin VB.Timer Timer1 
      Interval        =   10
      Left            =   0
      Top             =   15
   End
   Begin VB.CommandButton cmdSalir 
      Cancel          =   -1  'True
      Caption         =   "&Retornar"
      Height          =   405
      Left            =   5730
      TabIndex        =   4
      ToolTipText     =   "Salir"
      Top             =   5055
      Width           =   1350
   End
   Begin VB.Frame Frame2 
      Caption         =   "ASIENTO  CONTABLE FONDOS POR RENDIR"
      Height          =   4575
      Left            =   420
      TabIndex        =   0
      Top             =   300
      Width           =   6660
      Begin VB.CommandButton CmdExportar 
         Caption         =   "Exportar"
         Height          =   390
         Left            =   135
         TabIndex        =   1
         ToolTipText     =   "Exporta a excel"
         Top             =   4065
         Width           =   1575
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkDiferencia 
         Height          =   210
         Left            =   6255
         OleObjectBlob   =   "Ban_AsientoContableFondoRendir.frx":0000
         TabIndex        =   2
         Top             =   4230
         Width           =   270
      End
      Begin MSComctlLib.ListView LvDetalle 
         Height          =   3705
         Left            =   135
         TabIndex        =   3
         Top             =   330
         Width           =   6405
         _ExtentX        =   11298
         _ExtentY        =   6535
         View            =   3
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   4
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "T500"
            Text            =   "Cod"
            Object.Width           =   882
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "T3000"
            Text            =   "CUENTAS"
            Object.Width           =   4762
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   2
            Key             =   "debe"
            Object.Tag             =   "N100"
            Text            =   "DEBE"
            Object.Width           =   2381
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   3
            Key             =   "haber"
            Object.Tag             =   "N100"
            Text            =   "HABER"
            Object.Width           =   2381
         EndProperty
      End
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   510
      OleObjectBlob   =   "Ban_AsientoContableFondoRendir.frx":005E
      Top             =   0
   End
End
Attribute VB_Name = "Ban_AsientoContableFondoRendir"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub CmdExportar_Click()
    Dim tit(2) As String
    If LvDetalle.ListItems.Count = 0 Then Exit Sub
    FraProgreso.Visible = True
    tit(0) = Me.Caption & " " & DtFecha & " - " & Time
    tit(1) = "ASIENTO CONTABLE FONDOS POR RENDIR"
    tit(2) = ""
    ExportarNuevo LvDetalle, tit, Me, BarraProgreso
    FraProgreso.Visible = False
End Sub

Private Sub cmdSalir_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    Dim Sp_Fecha As String
    Centrar Me, False
    Aplicar_skin Me

    Sp_Fecha = " AND MONTH(a.abo_fecha_pago)=" & IG_Mes_Contable & " AND YEAR(a.abo_fecha_pago)=" & IG_Ano_Contable

    
    LLenar_Grilla AsientoContable(Sp_Fecha, 9), Me, LvDetalle, False, True, True, False
    TotalAsiento LvDetalle
        
End Sub

Private Sub Timer1_Timer()
    On Error Resume Next
    LvDetalle.SetFocus
    Timer1.Enabled = False
End Sub
