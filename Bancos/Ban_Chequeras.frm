VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form Ban_Chequeras 
   Caption         =   "Chequeras"
   ClientHeight    =   6690
   ClientLeft      =   1935
   ClientTop       =   2385
   ClientWidth     =   11085
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   ScaleHeight     =   6690
   ScaleWidth      =   11085
   Begin VB.Frame Frame5 
      Caption         =   "Mantenedor Chequeras"
      Height          =   5430
      Left            =   330
      TabIndex        =   6
      Top             =   570
      Width           =   10515
      Begin VB.TextBox TxtCantidad 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   6180
         TabIndex        =   2
         Tag             =   "N"
         Text            =   "0"
         Top             =   690
         Width           =   1230
      End
      Begin VB.TextBox SkId 
         Appearance      =   0  'Flat
         BackColor       =   &H80000004&
         Height          =   315
         Left            =   240
         Locked          =   -1  'True
         TabIndex        =   7
         TabStop         =   0   'False
         Tag             =   "T"
         Top             =   690
         Width           =   585
      End
      Begin VB.CommandButton CmdOk 
         Caption         =   "Ok"
         Height          =   300
         Left            =   9750
         TabIndex        =   4
         Top             =   705
         Width           =   495
      End
      Begin VB.TextBox TxtSerieInicial 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         Height          =   315
         IMEMode         =   3  'DISABLE
         Left            =   5100
         MaxLength       =   12
         TabIndex        =   1
         Tag             =   "t"
         Top             =   690
         Width           =   1095
      End
      Begin VB.TextBox TxtComentario 
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   7395
         TabIndex        =   3
         Tag             =   "T"
         Top             =   690
         Width           =   2340
      End
      Begin VB.ComboBox CboCuenta 
         Appearance      =   0  'Flat
         Height          =   315
         ItemData        =   "Ban_Chequeras.frx":0000
         Left            =   825
         List            =   "Ban_Chequeras.frx":0002
         Style           =   2  'Dropdown List
         TabIndex        =   0
         Top             =   675
         Width           =   4290
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel6 
         Height          =   255
         Left            =   270
         OleObjectBlob   =   "Ban_Chequeras.frx":0004
         TabIndex        =   8
         Top             =   5085
         Width           =   4095
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
         Height          =   255
         Left            =   5100
         OleObjectBlob   =   "Ban_Chequeras.frx":0089
         TabIndex        =   9
         Top             =   510
         Width           =   1080
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   255
         Index           =   0
         Left            =   840
         OleObjectBlob   =   "Ban_Chequeras.frx":00FF
         TabIndex        =   10
         Top             =   495
         Width           =   1095
      End
      Begin MSComctlLib.ListView LvCuentas 
         Height          =   4020
         Left            =   225
         TabIndex        =   11
         Top             =   1035
         Width           =   10050
         _ExtentX        =   17727
         _ExtentY        =   7091
         View            =   3
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         MousePointer    =   1
         NumItems        =   6
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "N109"
            Text            =   "Id"
            Object.Width           =   1058
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "T3000"
            Text            =   "Nro Cuenta - Banco"
            Object.Width           =   7549
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "N100"
            Text            =   "Serie Inicial"
            Object.Width           =   1914
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   3
            Object.Tag             =   "T2000"
            Text            =   "Cantidad"
            Object.Width           =   2152
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Object.Tag             =   "T2000"
            Text            =   "Comentario"
            Object.Width           =   4136
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Object.Tag             =   "N109"
            Text            =   "Cta id"
            Object.Width           =   0
         EndProperty
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   255
         Index           =   1
         Left            =   7410
         OleObjectBlob   =   "Ban_Chequeras.frx":0169
         TabIndex        =   12
         Top             =   510
         Width           =   1095
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel5 
         Height          =   210
         Index           =   0
         Left            =   6465
         OleObjectBlob   =   "Ban_Chequeras.frx":01DB
         TabIndex        =   13
         Top             =   510
         Width           =   915
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   255
         Index           =   2
         Left            =   240
         OleObjectBlob   =   "Ban_Chequeras.frx":0249
         TabIndex        =   14
         Top             =   510
         Width           =   615
      End
   End
   Begin VB.Timer Timer1 
      Interval        =   10
      Left            =   660
      Top             =   0
   End
   Begin VB.CommandButton cmdSalir 
      Cancel          =   -1  'True
      Caption         =   "&Retornar"
      Height          =   465
      Left            =   9660
      TabIndex        =   5
      ToolTipText     =   "Salir"
      Top             =   6090
      Width           =   1185
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   0
      OleObjectBlob   =   "Ban_Chequeras.frx":02AB
      Top             =   30
   End
End
Attribute VB_Name = "Ban_Chequeras"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdSalir_Click()
    Unload Me
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        SendKeys "{tab}"
        KeyAscii = 0
    End If
    
End Sub

Private Sub Form_Load()
    Centrar Me
    Aplicar_skin Me

    LLenarCombo CboCuenta, "CONCAT(cte_numero,' ',pla_nombre)", "cte_id", "ban_cta_cte c INNER JOIN con_plan_de_cuentas ON pla_id=ban_id", "cte_activo='SI' AND rut_emp='" & SP_Rut_Activo & "'"
    CargaCtas
End Sub
Private Sub TxtCuenta_GotFocus()
En_Foco Me.ActiveControl
End Sub
Private Sub TxtEjecutivo_GotFocus()
    En_Foco Me.ActiveControl
End Sub
Private Sub CmdOk_Click()
    Dim Lp_ChequeraID As Long
    Dim Sp_Nros As String
    
    
    If CboCuenta.ListIndex = -1 Or Val(TxtSerieInicial) = 0 Or Val(TxtCantidad) = 0 Then
        MsgBox "Seleccione Cuenta, e ingrese Serie Inicial y Cantidad de cheques...", vbInformation
        CboCuenta.SetFocus
        Exit Sub
    End If
    sql2 = Empty
    
    On Error GoTo CrearChequera
    
    
    Lp_ChequeraID = UltimoNro("ban_chequera", "chc_id")
    cn.BeginTrans ' Comienzo de  transaccion
        If Val(SkId) = 0 Then 'Nuevo
        
            For i = CDbl(TxtSerieInicial) To CDbl(TxtSerieInicial) + CDbl(TxtCantidad)
                Sp_Nros = Sp_Nros & Trim(Str(i)) & ","
            
            Next
            Sp_Nros = Mid(Sp_Nros, 1, Len(Sp_Nros) - 1)
            
            Sql = "SELECT chc_id " & _
                  "FROM ban_cheques " & _
                  "INNER JOIN ban_chequera USING(chc_id) " & _
                  "WHERE cte_id=" & CboCuenta.ItemData(CboCuenta.ListIndex) & " AND che_numero IN(" & Sp_Nros & ")"
            Consulta RsTmp, Sql
            If RsTmp.RecordCount > 0 Then
                MsgBox "Uno o m�s Nros de serie ya han sido utilizados...", vbInformation
                cn.RollbackTrans
                CboCuenta.SetFocus
                Exit Sub
            End If
            'Aqui comprobamos las series de los cheques
            Sql = "INSERT INTO ban_chequera (chc_id,cte_id,chc_serie_inicial,chc_cantidad,chc_comentario)  " & _
                 "VALUES(" & Lp_ChequeraID & "," & CboCuenta.ItemData(CboCuenta.ListIndex) & "," & CDbl(TxtSerieInicial) & "," & CDbl(TxtCantidad) & ",'" & TxtComentario & "')"
                 
            sql2 = "INSERT INTO ban_cheques (chc_id,che_numero) VALUES "
            For i = CDbl(TxtSerieInicial) To CDbl(TxtSerieInicial) + CDbl(TxtCantidad) - 1
                sql2 = sql2 & "(" & Lp_ChequeraID & "," & i & "),"
            Next
            sql2 = Mid(sql2, 1, Len(sql2) - 1)
                 
        Else 'Editando
           'If LvCuentas.ListItems.Count > 0 Then
            '    For i = 1 To LvCuentas.ListItems.Count
             '       If LvCuentas.ListItems(i).SubItems(6) = CboBanco.ItemData(CboBanco.ListIndex) And LvCuentas.ListItems(i).SubItems(2) = TxtCuenta And SkId <> LvCuentas.ListItems(i) Then
              '          MsgBox "Cuenta ingresada ya existe...", vbInformation
              '          CboBanco.SetFocus
              '          Exit Sub
              '      End If
            '    Next
           ' End If
    
        
        
        
            Sql = "UPDATE ban_chequera SET cte_id='" & CboCuenta.ItemData(CboCuenta.ListIndex) & "'," & _
                              "chc_serie_inicial=" & CDbl(TxtSerieInicial) & "," & _
                              "chc_cantidad=" & CDbl(TxtCantidad) & "," & _
                                "chc_comentario='" & Me.TxtComentario & "' " & _
                  "WHERE chc_id=" & SkId
                  
                  
            sql2 = "DELETE FROM ban_cheques " & _
                   "WHERE chc_id=" & SkId
            cn.Execute sql2
             sql2 = "INSERT INTO ban_cheques (chc_id,che_numero) VALUES "
            For i = CDbl(TxtSerieInicial) To CDbl(TxtSerieInicial) + CDbl(TxtCantidad) - 1
                sql2 = sql2 & "(" & SkId & "," & i & "),"
            Next
            sql2 = Mid(sql2, 1, Len(sql2) - 1)
        
        
        End If
        cn.Execute Sql
        
        If sql2 <> Empty Then cn.Execute sql2
        
        
        
    cn.CommitTrans 'Confirma transaccion
    TxtSerieInicial = Empty
    TxtCantidad = 0
    TxtComentario = Empty
    
    
    CargaCtas
    
    Exit Sub
CrearChequera:
    cn.RollbackTrans
    MsgBox "No se cre� la chequera ..." & vbNewLine & Err.Number & vbNewLine & Err.Description, vbInformation
End Sub
Private Sub CargaCtas()
    On Error Resume Next
    Sql = "SELECT chc_id,CONCAT(cte_numero,' ',pla_nombre) cuenta ,chc_serie_inicial,chc_cantidad,chc_comentario,c.cte_id " & _
          "FROM ban_chequera c " & _
          "INNER JOIN ban_cta_cte t USING(cte_id) " & _
          "INNER JOIN con_plan_de_cuentas ON pla_id=ban_id " & _
          "WHERE t.rut_emp='" & SP_Rut_Activo & "'"
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvCuentas, False, True, True, False
    CboCuenta.SetFocus
End Sub
Private Sub TxtSaldoInicial_GotFocus()
    En_Foco Me.ActiveControl
End Sub

Private Sub LvCuentas_DblClick()
    If LvCuentas.SelectedItem Is Nothing Then Exit Sub
        
    Sql = "SELECT che_estado " & _
          "FROM ban_cheques " & _
          "WHERE che_estado<>'SIN EMITIR' AND chc_id=" & LvCuentas.SelectedItem & " AND che_numero BETWEEN " & CDbl(LvCuentas.SelectedItem.SubItems(2)) & " AND " & CDbl(LvCuentas.SelectedItem.SubItems(2)) + CDbl(LvCuentas.SelectedItem.SubItems(3)) - 1
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        MsgBox "No es posible editar chequera..., " & vbNewLine & " Algunas series ya han sido utilizadas...", vbInformation
        Exit Sub
    End If
    
    SkId = LvCuentas.SelectedItem
    Busca_Id_Combo CboCuenta, LvCuentas.SelectedItem.SubItems(5)
    TxtSerieInicial = LvCuentas.SelectedItem.SubItems(2)
    TxtCantidad = LvCuentas.SelectedItem.SubItems(3)
    TxtComentario = LvCuentas.SelectedItem.SubItems(4)
    CboCuenta.SetFocus
End Sub

Private Sub txtCantidad_GotFocus()
    En_Foco Me.ActiveControl
End Sub

Private Sub txtCantidad_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
End Sub

Private Sub txtCantidad_Validate(Cancel As Boolean)
    TxtCantidad = NumFormat(TxtCantidad)
End Sub

Private Sub TxtComentario_GotFocus()
    En_Foco Me.ActiveControl
End Sub

Private Sub TxtComentario_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub TxtSerieIncial_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
End Sub

Private Sub TxtSerieInicial_GotFocus()
    En_Foco Me.ActiveControl
End Sub

Private Sub TxtSerieInicial_Validate(Cancel As Boolean)
    TxtSerieInicial = NumFormat(TxtSerieInicial)
End Sub
