VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{DA729E34-689F-49EA-A856-B57046630B73}#1.0#0"; "progressbar-xp.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form Ban_Movimientos 
   Caption         =   "Movimientos en cuentas"
   ClientHeight    =   9855
   ClientLeft      =   345
   ClientTop       =   1335
   ClientWidth     =   13755
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   ScaleHeight     =   9855
   ScaleWidth      =   13755
   Begin MSComctlLib.ListView LvConciliacion 
      Height          =   2460
      Left            =   345
      TabIndex        =   65
      Top             =   9435
      Visible         =   0   'False
      Width           =   13215
      _ExtentX        =   23310
      _ExtentY        =   4339
      View            =   3
      LabelWrap       =   0   'False
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   5
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Object.Tag             =   "T1500"
         Text            =   "Detalle"
         Object.Width           =   9702
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Object.Tag             =   "F1000"
         Text            =   "Fecha Emision"
         Object.Width           =   2646
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Object.Tag             =   "T1500"
         Text            =   "Valores"
         Object.Width           =   2646
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   3
         Object.Tag             =   "T1500"
         Text            =   "SubTotales"
         Object.Width           =   2293
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Object.Tag             =   "F1000"
         Text            =   "Fecha Vencimiento."
         Object.Width           =   2646
      EndProperty
   End
   Begin VB.CommandButton CmdExportar 
      Caption         =   "Exportar"
      Height          =   210
      Left            =   12210
      TabIndex        =   79
      Top             =   1335
      Width           =   1290
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Command1"
      Enabled         =   0   'False
      Height          =   210
      Left            =   8865
      TabIndex        =   78
      Top             =   1215
      Visible         =   0   'False
      Width           =   1305
   End
   Begin VB.TextBox TxtIngresado 
      Alignment       =   1  'Right Justify
      Height          =   300
      Left            =   11715
      TabIndex        =   77
      TabStop         =   0   'False
      Text            =   "0"
      Top             =   8670
      Width           =   1290
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel19 
      Height          =   225
      Left            =   10545
      OleObjectBlob   =   "Ban_Movimientos.frx":0000
      TabIndex        =   76
      Top             =   8700
      Width           =   1065
   End
   Begin VB.TextBox TxtValor 
      Alignment       =   1  'Right Justify
      Height          =   300
      Left            =   11700
      TabIndex        =   6
      Tag             =   "N"
      Top             =   7290
      Width           =   1290
   End
   Begin VB.CommandButton CmdOkCuenta 
      Caption         =   "ok"
      Height          =   300
      Left            =   12975
      Style           =   1  'Graphical
      TabIndex        =   7
      Top             =   7290
      Width           =   300
   End
   Begin VB.TextBox TxtNroCuenta 
      Alignment       =   1  'Right Justify
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   10185
      TabIndex        =   75
      Tag             =   "N2"
      ToolTipText     =   "Ingrese el codigo de la cuenta"
      Top             =   6975
      Width           =   945
   End
   Begin MSComctlLib.ListView LvCue 
      Height          =   1065
      Left            =   8835
      TabIndex        =   74
      Top             =   7590
      Width           =   4485
      _ExtentX        =   7911
      _ExtentY        =   1879
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   0
      NumItems        =   4
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Object.Tag             =   "N109"
         Text            =   "Id"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Object.Tag             =   "T4000"
         Text            =   "Cuenta Contable"
         Object.Width           =   5133
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   2
         Key             =   "monto"
         Object.Tag             =   "N100"
         Text            =   "Monto"
         Object.Width           =   2275
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Object.Tag             =   "T1000"
         Text            =   "Rut de anticipo proveedores"
         Object.Width           =   2540
      EndProperty
   End
   Begin VB.CheckBox ChkApertura 
      Alignment       =   1  'Right Justify
      Caption         =   "Asiento Apertura"
      Height          =   255
      Left            =   11730
      TabIndex        =   73
      Top             =   7020
      Width           =   1575
   End
   Begin MSComctlLib.ListView LvCuentas 
      Height          =   3705
      Left            =   10020
      TabIndex        =   70
      Top             =   9975
      Width           =   7500
      _ExtentX        =   13229
      _ExtentY        =   6535
      View            =   3
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   0
      NumItems        =   5
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Object.Tag             =   "N109"
         Object.Width           =   617
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Object.Tag             =   "N109"
         Text            =   "NRO CHEQUE"
         Object.Width           =   2293
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Key             =   "debe"
         Object.Tag             =   "T3000"
         Text            =   "BANCO"
         Object.Width           =   5292
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   3
         Key             =   "haber"
         Object.Tag             =   "N100"
         Text            =   "DISPONIBLE"
         Object.Width           =   2469
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Object.Tag             =   "N100"
         Text            =   "Utilizar"
         Object.Width           =   2293
      EndProperty
   End
   Begin VB.CommandButton cmdMultiCuenta 
      Caption         =   "Cuenta Contable"
      Height          =   255
      Left            =   10950
      TabIndex        =   69
      Top             =   6690
      Visible         =   0   'False
      Width           =   1440
   End
   Begin VB.TextBox Text5 
      BackColor       =   &H00C0FFC0&
      ForeColor       =   &H00004000&
      Height          =   315
      Left            =   255
      TabIndex        =   68
      Text            =   "Lineas en  VERDE =  valores meses anteriores no conciliados"
      Top             =   9075
      Width           =   4605
   End
   Begin VB.Frame FraProgreso 
      Caption         =   "Progreso exportaci�n"
      Height          =   795
      Left            =   960
      TabIndex        =   66
      Top             =   4800
      Visible         =   0   'False
      Width           =   11430
      Begin Proyecto2.XP_ProgressBar BarraProgreso 
         Height          =   330
         Left            =   150
         TabIndex        =   67
         Top             =   315
         Width           =   11130
         _ExtentX        =   19632
         _ExtentY        =   582
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BrushStyle      =   0
         Color           =   16777088
         Scrolling       =   1
         ShowText        =   -1  'True
      End
   End
   Begin VB.CommandButton CmdEliminar 
      Caption         =   "&Eliminar"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Left            =   4530
      TabIndex        =   55
      Top             =   8550
      Width           =   2000
   End
   Begin VB.CommandButton CmdModificar 
      Caption         =   "&Modificar"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Left            =   2445
      TabIndex        =   54
      Top             =   8550
      Width           =   2000
   End
   Begin VB.CommandButton CmdNuevo 
      Caption         =   "&Nuevo"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Left            =   360
      TabIndex        =   53
      Top             =   8550
      Width           =   2000
   End
   Begin VB.ComboBox CboCuenta 
      Height          =   315
      Left            =   8835
      Style           =   2  'Dropdown List
      TabIndex        =   5
      Top             =   7290
      Width           =   2880
   End
   Begin VB.CommandButton CmdCuenta 
      Caption         =   "Cuenta Contable"
      Height          =   195
      Left            =   8850
      TabIndex        =   37
      Top             =   7080
      Width           =   1320
   End
   Begin VB.ComboBox CboDestinatarios 
      Height          =   315
      Left            =   480
      TabIndex        =   14
      Tag             =   "T"
      Text            =   "CboDestinatarios"
      Top             =   7875
      Width           =   4350
   End
   Begin VB.ComboBox CboConcepto 
      Height          =   315
      Left            =   4815
      TabIndex        =   16
      Tag             =   "T"
      Text            =   "CboConcepto"
      Top             =   7875
      Width           =   4020
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel5 
      Height          =   195
      Left            =   4830
      OleObjectBlob   =   "Ban_Movimientos.frx":0066
      TabIndex        =   36
      Top             =   7650
      Width           =   2055
   End
   Begin VB.CommandButton cmdSalir 
      Cancel          =   -1  'True
      Caption         =   "&Retornar"
      Height          =   420
      Left            =   12510
      TabIndex        =   22
      ToolTipText     =   "Salir sin hacer cambios"
      Top             =   9420
      Width           =   1050
   End
   Begin VB.Frame Frame1 
      Caption         =   "Cuenta Seleccionada"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1260
      Left            =   135
      TabIndex        =   20
      Top             =   60
      Width           =   13410
      Begin VB.TextBox TxtInicialContable 
         Height          =   240
         Left            =   9510
         TabIndex        =   81
         Text            =   "Text2"
         Top             =   255
         Width           =   1980
      End
      Begin VB.Frame Frame3 
         Caption         =   "Per�odo Contable"
         Height          =   1005
         Left            =   11640
         TabIndex        =   58
         Top             =   165
         Width           =   1710
         Begin VB.TextBox TxtMesContable 
            Alignment       =   2  'Center
            BackColor       =   &H00FFFF80&
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FF0000&
            Height          =   330
            Left            =   60
            TabIndex        =   60
            Text            =   "Text6"
            Top             =   570
            Width           =   1620
         End
         Begin VB.TextBox TxtAnoContable 
            Alignment       =   2  'Center
            BackColor       =   &H00FFFF80&
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FF0000&
            Height          =   330
            Left            =   60
            TabIndex        =   59
            Text            =   "Text5"
            Top             =   240
            Width           =   1620
         End
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel7 
         Height          =   300
         Left            =   6075
         OleObjectBlob   =   "Ban_Movimientos.frx":00D4
         TabIndex        =   32
         Top             =   592
         Width           =   3405
      End
      Begin VB.TextBox TxtSaldoInicial 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFC0&
         BorderStyle     =   0  'None
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   14.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   465
         Left            =   9510
         TabIndex        =   31
         Text            =   "0"
         Top             =   510
         Width           =   2010
      End
      Begin VB.TextBox TxtCuenta 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFF00&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   14.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   465
         Left            =   240
         TabIndex        =   21
         Text            =   "Text1"
         Top             =   510
         Width           =   5685
      End
   End
   Begin VB.Timer Timer1 
      Interval        =   10
      Left            =   45
      Top             =   510
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   0
      OleObjectBlob   =   "Ban_Movimientos.frx":0164
      Top             =   0
   End
   Begin VB.Frame Frame2 
      Caption         =   "Movimientos"
      Height          =   8055
      Left            =   150
      TabIndex        =   23
      Top             =   1365
      Width           =   13395
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel20 
         Height          =   240
         Left            =   2070
         OleObjectBlob   =   "Ban_Movimientos.frx":0398
         TabIndex        =   83
         Top             =   180
         Width           =   330
      End
      Begin VB.ComboBox CboEstado 
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   2400
         Style           =   2  'Dropdown List
         TabIndex        =   82
         Top             =   150
         Width           =   2775
      End
      Begin VB.TextBox txtInfoApertura 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         Height          =   285
         Left            =   7665
         TabIndex        =   80
         Text            =   "0"
         Top             =   4845
         Width           =   2790
      End
      Begin VB.CommandButton CmdHistorico 
         Caption         =   "Conciliaciones Historicas"
         Height          =   300
         Left            =   5055
         TabIndex        =   72
         Top             =   4785
         Width           =   2325
      End
      Begin VB.TextBox Text1 
         BackColor       =   &H00FFFFC0&
         ForeColor       =   &H00FF0000&
         Height          =   315
         Left            =   4695
         TabIndex        =   71
         Text            =   "CLIENTES-PROVEEDORES-->SE PROCESA EN TESORERIA"
         Top             =   7710
         Width           =   4605
      End
      Begin VB.CommandButton CmdPrev 
         Caption         =   "Ver Conciliacion"
         Height          =   300
         Left            =   3300
         TabIndex        =   64
         Top             =   4792
         Width           =   1725
      End
      Begin VB.CheckBox ChkTodos 
         Height          =   210
         Left            =   210
         TabIndex        =   63
         Top             =   495
         Width           =   180
      End
      Begin VB.CommandButton CmdConciliar 
         Caption         =   "No Conciliado"
         Height          =   300
         Index           =   1
         Left            =   1275
         TabIndex        =   62
         ToolTipText     =   "Dejar como no conciliados los items marcados"
         Top             =   4777
         Width           =   1110
      End
      Begin VB.CommandButton CmdConciliar 
         Caption         =   "Conciliar"
         Height          =   300
         Index           =   0
         Left            =   105
         TabIndex        =   61
         ToolTipText     =   "Conciliar los Items marcados."
         Top             =   4777
         Width           =   1110
      End
      Begin VB.TextBox txtSaldo 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         Height          =   285
         Left            =   10440
         TabIndex        =   52
         Text            =   "0"
         Top             =   4845
         Width           =   2790
      End
      Begin TabDlg.SSTab StabMov 
         Height          =   2445
         Left            =   60
         TabIndex        =   25
         Top             =   5235
         Width           =   13140
         _ExtentX        =   23178
         _ExtentY        =   4313
         _Version        =   393216
         Tabs            =   4
         TabsPerRow      =   4
         TabHeight       =   520
         TabCaption(0)   =   "Cheque"
         TabPicture(0)   =   "Ban_Movimientos.frx":03FC
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "SkinLabel4"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "SkinLabel6"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "SkinLabel3"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "DtCobro"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "SkinLabel2"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "SkinLabel1"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "SkArea"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "CboCheques"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "DtEmision"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "TxtValorCheque"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "CmdIngresaCheque"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "CboChequeras"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "TxtChNro"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "CmdAnularCheque"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).ControlCount=   14
         TabCaption(1)   =   "Dep�sito"
         TabPicture(1)   =   "Ban_Movimientos.frx":0418
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "CmdDeposito"
         Tab(1).Control(1)=   "TxtNroComprobante"
         Tab(1).Control(2)=   "TxtValorDeposito"
         Tab(1).Control(3)=   "DtDeposito"
         Tab(1).Control(4)=   "SkinLabel8"
         Tab(1).Control(5)=   "SkinLabel9"
         Tab(1).Control(6)=   "SkinLabel12"
         Tab(1).Control(7)=   "SkinLabel10"
         Tab(1).ControlCount=   8
         TabCaption(2)   =   "Transferencia a"
         TabPicture(2)   =   "Ban_Movimientos.frx":0434
         Tab(2).ControlEnabled=   0   'False
         Tab(2).Control(0)=   "TxtValorTransferencia"
         Tab(2).Control(1)=   "CmdIngresaTransferencia"
         Tab(2).Control(2)=   "CboCuentasPropias"
         Tab(2).Control(3)=   "OptTerceros"
         Tab(2).Control(4)=   "OptPropias"
         Tab(2).Control(5)=   "SkinLabel11"
         Tab(2).Control(6)=   "DtTransferencia"
         Tab(2).Control(7)=   "SkinLabel13"
         Tab(2).Control(8)=   "SkinLabel14"
         Tab(2).ControlCount=   9
         TabCaption(3)   =   "Cargo a cuenta"
         TabPicture(3)   =   "Ban_Movimientos.frx":0450
         Tab(3).ControlEnabled=   0   'False
         Tab(3).Control(0)=   "SkinLabel18"
         Tab(3).Control(1)=   "SkinLabel17"
         Tab(3).Control(2)=   "SkinLabel16"
         Tab(3).Control(3)=   "SkinLabel15"
         Tab(3).Control(4)=   "DtCargo"
         Tab(3).Control(5)=   "TxtNroComprobanteCargo"
         Tab(3).Control(6)=   "TxtValorCargo"
         Tab(3).Control(7)=   "CmdIngresaCargo"
         Tab(3).ControlCount=   8
         Begin VB.CommandButton CmdAnularCheque 
            Caption         =   "Anular Cheque"
            Height          =   240
            Left            =   2550
            TabIndex        =   57
            Top             =   945
            Width           =   1905
         End
         Begin VB.TextBox TxtChNro 
            Alignment       =   2  'Center
            BackColor       =   &H8000000F&
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Left            =   2550
            Locked          =   -1  'True
            TabIndex        =   56
            Top             =   660
            Visible         =   0   'False
            Width           =   1890
         End
         Begin VB.CommandButton CmdIngresaCargo 
            Caption         =   "&Ingresa Cargo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   480
            Left            =   -68600
            TabIndex        =   51
            Top             =   1770
            Width           =   2100
         End
         Begin VB.TextBox TxtValorCargo 
            Alignment       =   1  'Right Justify
            BackColor       =   &H8000000E&
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   -73395
            TabIndex        =   13
            Tag             =   "N"
            Text            =   "0"
            Top             =   645
            Width           =   1320
         End
         Begin VB.TextBox TxtNroComprobanteCargo 
            Alignment       =   1  'Right Justify
            BackColor       =   &H8000000E&
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   -72075
            TabIndex        =   15
            Tag             =   "N"
            Top             =   645
            Width           =   1680
         End
         Begin VB.TextBox TxtValorTransferencia 
            Alignment       =   1  'Right Justify
            BackColor       =   &H8000000E&
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   -73470
            TabIndex        =   10
            Tag             =   "N"
            Text            =   "0"
            Top             =   690
            Width           =   1320
         End
         Begin VB.CommandButton CmdIngresaTransferencia 
            Caption         =   "&Ingresa Transferencia"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   480
            Left            =   -68600
            TabIndex        =   44
            Top             =   1770
            Width           =   2100
         End
         Begin VB.ComboBox CboCuentasPropias 
            Enabled         =   0   'False
            Height          =   315
            Left            =   -70515
            Style           =   2  'Dropdown List
            TabIndex        =   11
            Top             =   690
            Width           =   4125
         End
         Begin VB.OptionButton OptTerceros 
            Caption         =   "A Terceros"
            Height          =   210
            Left            =   -70500
            TabIndex        =   43
            Top             =   465
            Value           =   -1  'True
            Width           =   2100
         End
         Begin VB.OptionButton OptPropias 
            Alignment       =   1  'Right Justify
            Caption         =   "Cuentas Propias"
            Height          =   210
            Left            =   -68145
            TabIndex        =   42
            Top             =   465
            Width           =   1650
         End
         Begin VB.CommandButton CmdDeposito 
            Caption         =   "&Ingresa Dep�sito"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   480
            Left            =   -68600
            TabIndex        =   38
            Top             =   1770
            Width           =   2100
         End
         Begin VB.TextBox TxtNroComprobante 
            Alignment       =   1  'Right Justify
            BackColor       =   &H8000000E&
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   -72210
            TabIndex        =   19
            Tag             =   "N"
            Top             =   675
            Width           =   1620
         End
         Begin VB.TextBox TxtValorDeposito 
            Alignment       =   1  'Right Justify
            BackColor       =   &H8000000E&
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   -73515
            TabIndex        =   18
            Tag             =   "N"
            Text            =   "0"
            Top             =   675
            Width           =   1320
         End
         Begin VB.ComboBox CboChequeras 
            Height          =   315
            Left            =   135
            Style           =   2  'Dropdown List
            TabIndex        =   0
            Top             =   660
            Width           =   2400
         End
         Begin VB.CommandButton CmdIngresaCheque 
            Caption         =   "&Ingresa Cheque"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   480
            Left            =   6400
            TabIndex        =   8
            Top             =   1770
            Width           =   2100
         End
         Begin VB.TextBox TxtValorCheque 
            Alignment       =   1  'Right Justify
            BackColor       =   &H8000000E&
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   7110
            TabIndex        =   4
            Tag             =   "N"
            Text            =   "0"
            Top             =   660
            Width           =   1500
         End
         Begin MSComCtl2.DTPicker DtEmision 
            Height          =   300
            Left            =   4470
            TabIndex        =   2
            Top             =   660
            Width           =   1320
            _ExtentX        =   2328
            _ExtentY        =   529
            _Version        =   393216
            Format          =   7864321
            CurrentDate     =   40963
         End
         Begin VB.ComboBox CboCheques 
            Height          =   315
            Left            =   2550
            Style           =   2  'Dropdown List
            TabIndex        =   1
            Top             =   675
            Width           =   1920
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkArea 
            Height          =   195
            Left            =   2565
            OleObjectBlob   =   "Ban_Movimientos.frx":046C
            TabIndex        =   26
            Top             =   480
            Width           =   1815
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
            Height          =   180
            Left            =   4485
            OleObjectBlob   =   "Ban_Movimientos.frx":04F0
            TabIndex        =   27
            Top             =   480
            Width           =   1245
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
            Height          =   180
            Left            =   5790
            OleObjectBlob   =   "Ban_Movimientos.frx":0568
            TabIndex        =   28
            Top             =   480
            Width           =   1185
         End
         Begin MSComCtl2.DTPicker DtCobro 
            Height          =   300
            Left            =   5790
            TabIndex        =   3
            Top             =   660
            Width           =   1320
            _ExtentX        =   2328
            _ExtentY        =   529
            _Version        =   393216
            Format          =   7864321
            CurrentDate     =   40963
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
            Height          =   180
            Left            =   7410
            OleObjectBlob   =   "Ban_Movimientos.frx":05DC
            TabIndex        =   29
            Top             =   480
            Width           =   1185
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel6 
            Height          =   195
            Left            =   165
            OleObjectBlob   =   "Ban_Movimientos.frx":0644
            TabIndex        =   30
            Top             =   480
            Width           =   2055
         End
         Begin MSComCtl2.DTPicker DtDeposito 
            Height          =   300
            Left            =   -74850
            TabIndex        =   17
            Top             =   675
            Width           =   1320
            _ExtentX        =   2328
            _ExtentY        =   529
            _Version        =   393216
            Format          =   7864321
            CurrentDate     =   40963
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel8 
            Height          =   180
            Left            =   -74865
            OleObjectBlob   =   "Ban_Movimientos.frx":06B4
            TabIndex        =   33
            Top             =   480
            Width           =   1365
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel9 
            Height          =   180
            Left            =   -73410
            OleObjectBlob   =   "Ban_Movimientos.frx":072E
            TabIndex        =   34
            Top             =   480
            Width           =   1185
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel12 
            Height          =   180
            Left            =   -72270
            OleObjectBlob   =   "Ban_Movimientos.frx":0796
            TabIndex        =   35
            Top             =   480
            Width           =   1665
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
            Height          =   195
            Left            =   165
            OleObjectBlob   =   "Ban_Movimientos.frx":0812
            TabIndex        =   39
            Top             =   1095
            Width           =   2055
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel10 
            Height          =   195
            Left            =   -74850
            OleObjectBlob   =   "Ban_Movimientos.frx":0888
            TabIndex        =   40
            Top             =   1095
            Width           =   2055
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel11 
            Height          =   195
            Left            =   -74835
            OleObjectBlob   =   "Ban_Movimientos.frx":08EA
            TabIndex        =   41
            Top             =   1095
            Width           =   2055
         End
         Begin MSComCtl2.DTPicker DtTransferencia 
            Height          =   300
            Left            =   -74850
            TabIndex        =   9
            Top             =   690
            Width           =   1365
            _ExtentX        =   2408
            _ExtentY        =   529
            _Version        =   393216
            Format          =   7864321
            CurrentDate     =   40963
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel13 
            Height          =   180
            Left            =   -74850
            OleObjectBlob   =   "Ban_Movimientos.frx":0960
            TabIndex        =   45
            Top             =   510
            Width           =   1365
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel14 
            Height          =   180
            Left            =   -73365
            OleObjectBlob   =   "Ban_Movimientos.frx":09DA
            TabIndex        =   46
            Top             =   510
            Width           =   1185
         End
         Begin MSComCtl2.DTPicker DtCargo 
            Height          =   300
            Left            =   -74745
            TabIndex        =   12
            Top             =   645
            Width           =   1320
            _ExtentX        =   2328
            _ExtentY        =   529
            _Version        =   393216
            Format          =   7864321
            CurrentDate     =   40963
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel15 
            Height          =   180
            Left            =   -74730
            OleObjectBlob   =   "Ban_Movimientos.frx":0A42
            TabIndex        =   47
            Top             =   420
            Width           =   1365
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel16 
            Height          =   180
            Left            =   -73275
            OleObjectBlob   =   "Ban_Movimientos.frx":0AB6
            TabIndex        =   48
            Top             =   435
            Width           =   1185
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel17 
            Height          =   180
            Left            =   -72075
            OleObjectBlob   =   "Ban_Movimientos.frx":0B1E
            TabIndex        =   49
            Top             =   435
            Width           =   1665
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel18 
            Height          =   195
            Left            =   -74805
            OleObjectBlob   =   "Ban_Movimientos.frx":0B9A
            TabIndex        =   50
            Top             =   1065
            Width           =   2055
         End
      End
      Begin MSComctlLib.ListView LvDetalle 
         Height          =   4290
         Left            =   135
         TabIndex        =   24
         Top             =   480
         Width           =   13215
         _ExtentX        =   23310
         _ExtentY        =   7567
         View            =   3
         LabelWrap       =   0   'False
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   17
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "N109"
            Object.Width           =   529
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "N109"
            Text            =   "Cta Cte ID"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   2
            Object.Tag             =   "N109"
            Text            =   "Numero"
            Object.Width           =   2646
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Object.Tag             =   "F1000"
            Text            =   "Fecha"
            Object.Width           =   2646
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Object.Tag             =   "T3000"
            Text            =   "Destino"
            Object.Width           =   8819
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   5
            Object.Tag             =   "N100"
            Text            =   "Pago/Cargo"
            Object.Width           =   2293
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   6
            Object.Tag             =   "N100"
            Text            =   "Deposito"
            Object.Width           =   2293
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   7
            Object.Tag             =   "N100"
            Text            =   "Saldo"
            Object.Width           =   2293
         EndProperty
         BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   8
            Object.Tag             =   "N109"
            Text            =   "Mov_id"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   9
            Object.Tag             =   "N109"
            Text            =   "a terceros Id de la cuenta destino"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   10
            Object.Tag             =   "T1000"
            Text            =   "De Tesoreeria ???"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(12) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   11
            Object.Tag             =   "T250"
            Text            =   "C"
            Object.Width           =   794
         EndProperty
         BeginProperty ColumnHeader(13) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   12
            Object.Tag             =   "T1000"
            Text            =   "Color"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(14) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   13
            Object.Tag             =   "N109"
            Text            =   "mov_id_unico_entre_cuentas"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(15) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   14
            Object.Tag             =   "N109"
            Text            =   "Id Plan de cuentras"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(16) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   15
            Object.Tag             =   "T300"
            Text            =   "Apertura?"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(17) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   16
            Object.Tag             =   "T1000"
            Text            =   "Fecha Cobro"
            Object.Width           =   0
         EndProperty
      End
   End
End
Attribute VB_Name = "Ban_Movimientos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public Sm_Cuenta As String
Public Sp_IdCuenta As Integer
Public Lp_SaldoInicial As Long
Dim Lp_IdMov As Long
Dim Lp_idsC As Long, Lp_idsD As Long
Dim Lp_Cuenta As Long
Dim Dm_Fecha As Date
Dim Li_CtaBanco As Integer
Dim Sp_Concilidados As String
Dim Bp_CargoFormulario As Boolean

Private Sub CboChequeras_Click()
    CboCheques.Clear
    If CboChequeras.ListIndex = -1 Then Exit Sub
    LLenarCombo CboCheques, "che_numero", "che_id", "ban_cheques", "che_estado='SIN EMITIR' AND chc_id=" & CboChequeras.ItemData(CboChequeras.ListIndex)
    If CboCheques.ListCount = 0 Then Exit Sub
    CboCheques.ListIndex = 0
End Sub



Private Sub CboConcepto_GotFocus()
    On Error Resume Next
    SendKeys "{F4}" 'envia un F4
End Sub



Private Sub CboCuenta_Click()
    If CboCuenta.ListIndex = -1 Then Exit Sub
    TxtNroCuenta = CboCuenta.ItemData(CboCuenta.ListIndex)
    Sql = "SELECT pla_id " & _
            "FROM con_plan_de_cuentas " & _
            "WHERE tpo_id=1 AND det_id=4 AND pla_analisis='SI' AND pla_id=" & CboCuenta.ItemData(CboCuenta.ListIndex)
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        MsgBox "La cuenta seleccionada corresponde a " & vbNewLine & "Activo Inmovilizado " & vbNewLine & "Debe ingresarlos en el m�dulo de compras..."
        TxtNroCuenta = 0
        CboCuenta.ListIndex = -1
    End If
    
End Sub

Private Sub CboCuenta_LostFocus()
   ' CboDestinatarios.SetFocus
End Sub

Private Sub CboCuentasPropias_Click()
    'MsgBox CboCuentasPropias.ItemData(CboCuentasPropias.ListIndex)
    If CboCuentasPropias.ListIndex = -1 Then Exit Sub
    
  '  LvCue.ListItems.Clear
  '  LvCue.ListItems.Add , , CboCuentasPropias.ItemData(CboCuentasPropias.ListIndex)
  '  LvCue.ListItems(1).SubItems(1) = CboCuentasPropias.Text
  '  LvCue.ListItems(1).SubItems(2) = TxtValorTransferencia
  '  SumaG
End Sub

Private Sub CboDestinatarios_GotFocus()
    On Error Resume Next
    SendKeys "{F4}" 'envia un F4
End Sub

Private Sub CboDestinatarios_LostFocus()
    CboConcepto.SetFocus
End Sub

Private Sub CboEstado_Click()
    If Not Bp_CargoFormulario Then Exit Sub
    Detalle
End Sub

Private Sub ChkApertura_Click()
   If ChkApertura.Value = 1 Then LaApertura 1
   If ChkApertura.Value = 0 Then LaApertura 0
   
End Sub
Private Sub LaApertura(Apertura As Integer)
 If Apertura = 1 Then
        CboCuenta.Enabled = False
        txtValor.Enabled = False
        CmdOkCuenta.Enabled = False
        TxtNroCuenta.Enabled = False
        CmdCuenta.Enabled = False
        LvCue.Enabled = False
    Else
        CboCuenta.Enabled = True
        txtValor.Enabled = True
        CmdOkCuenta.Enabled = True
        TxtNroCuenta.Enabled = True
        CmdCuenta.Enabled = True
        LvCue.Enabled = True
    End If
End Sub




Private Sub ChkTodos_Click()
    For i = 1 To LvDetalle.ListItems.Count
        If LvDetalle.ListItems(i).Checked Then LvDetalle.ListItems(i).Checked = False Else LvDetalle.ListItems(i).Checked = True
    Next
End Sub

Private Sub CmdAnularCheque_Click()
    Dim Sp_Nro_Cheque As Long, Sp_id_Cheque As Long, Lp_id_Mov As Long
    If TxtChNro.Visible Then
        Sp_Nro_Cheque = TxtChNro
        Sp_id_Cheque = TxtValorCheque.Tag
    Else
        Sp_Nro_Cheque = CboCheques.Text
        Sp_id_Cheque = CboCheques.ItemData(CboCheques.ListIndex)
    End If
    
    If TxtChNro.Visible = True Then
        If LvDetalle.SelectedItem.SubItems(10) = "SI" Then
            MsgBox "Este movimiento fue realizado en el M�dulo Tesoreria..." & vbNewLine & "No es posible modificarlo aqu�...", vbInformation
            Exit Sub
        End If
    End If
    
    If MsgBox(" Confirme anulacion del cheque Nro " & Sp_Nro_Cheque, vbQuestion + vbOKCancel) = vbCancel Then Exit Sub
    On Error GoTo ErrorAnula
    cn.BeginTrans
        
        If TxtChNro.Visible Then
            Lp_id_Mov = TxtChNro.Tag
            Sql = "UPDATE ban_movimientos SET mov_valor=0,mov_detalle='ANULADO',mov_fecha='" & Fql(Date) & "' " & _
                  "WHERE mov_id=" & TxtChNro.Tag
        Else
            Lp_id_Mov = UltimoNro("ban_movimientos", "mov_id")
            Sql = "INSERT INTO ban_movimientos (mov_id,cte_id,mov_tipo,mov_identificacion,mov_detalle,mov_identificacion_id,mov_fecha) " & _
                  "VALUES(" & Lp_id_Mov & "," & Sp_IdCuenta & ",'CARGO','CHEQUE','ANULADO',1,'" & Fql(Date) & "')"
        
        End If
        cn.Execute Sql
    
        Sql = "UPDATE ban_cheques SET che_estado='NULO',che_valor=0,che_destinatario='',che_concepto='',mov_id=" & Lp_id_Mov & _
                ",che_fecha_emision='" & Fql(Date) & "',che_fecha_cobro='" & Fql(Date) & "',mes_contable=" & IG_Mes_Contable & ",ano_contable=" & IG_Ano_Contable & "  " & _
              "WHERE che_id=" & Sp_id_Cheque
        cn.Execute Sql
    cn.CommitTrans
    
    CargaDatos
    CboChequeras_Click
    Exit Sub
ErrorAnula:
    cn.RollbackTrans
    MsgBox "Ocurrio un problema al intentar anular cheque" & vbNewLine & Err.Number & vbNewLine & Err.Description, vbInformation
    
End Sub





Private Sub CmdConciliar_Click(Index As Integer)
    Dim Sp_Marcados As String, Sp_Conciliado As String
    Sp_Marcados = Empty
    For i = 1 To LvDetalle.ListItems.Count
        If LvDetalle.ListItems(i).Checked Then
            Sp_Marcados = Sp_Marcados & LvDetalle.ListItems(i) & ","
            
            
        End If
    Next
    
    If Len(Sp_Marcados) > 0 Then
        Sp_Marcados = Mid(Sp_Marcados, 1, Len(Sp_Marcados) - 1)
        If Index = 0 Then
            Sp_Conciliado = "SI"
            
            'Marcar conciliados
        Else
            'Marcar como NO conciliados '21-08-2012
            Sp_Conciliado = "NO"
        End If
        cn.Execute "UPDATE ban_movimientos SET mov_mes_conciliacion=" & IG_Mes_Contable & ",mov_ano_conciliacion=" & IG_Ano_Contable & ",mov_conciliado='" & Sp_Conciliado & "',mov_fecha_conciliacion='" & Fql(Date) & "',mov_usuario_conciliacion='" & LogUsuario & "' WHERE mov_id IN(" & Sp_Marcados & ")"
    End If
    Detalle
    
    
    
    
End Sub

Private Sub cmdCuenta_Click()
    With BuscarSimple
        SG_codigo = Empty
        .Sm_Consulta = "SELECT pla_id, pla_nombre,tpo_nombre,det_nombre " & _
                       "FROM    con_plan_de_cuentas p,  con_tipo_de_cuenta t,   con_detalle_tipo_cuenta d " & _
                       "WHERE   p.tpo_id = t.tpo_id AND p.det_id = d.det_id "
        .Sm_CampoLike = "pla_nombre"
        .Im_Columnas = 2
        .Show 1
        If SG_codigo = Empty Then Exit Sub
        Busca_Id_Combo CboCuenta, Val(SG_codigo)
    End With
End Sub

Private Sub CmdDeposito_Click()
    'Validamos 1ro 25 Febro 2012
    Dim Sp_Apertura As String * 2
    If Principal.CmdMenu(8).Visible = True Then
        If ConsultaCentralizado("7,8", IG_Mes_Contable, IG_Ano_Contable) Then
            MsgBox "periodo contable ya ha sido centralizado, " & vbNewLine & "No puede modificar"
            Exit Sub
        End If
    End If
    
    
    Dim Lp_id_Mov As Long
    If Val(Me.TxtValorDeposito) = 0 Then
        MsgBox "Falta valor del deposito...", vbInformation
        TxtValorDeposito.SetFocus
        Exit Sub
    End If
    Lp_Cuenta = 0
    If CboCuenta.Visible And CboCuenta.ListIndex = -1 And ChkApertura.Value = 0 Then
        MsgBox "Seleccione Cuenta Contable...", vbInformation
        CboCuenta.SetFocus
        Exit Sub
    End If
    
    If Month(DtDeposito) <> IG_Mes_Contable Or Year(DtDeposito) <> IG_Ano_Contable Then
        MsgBox "Fecha no corresponde al periodo contable...", vbInformation
        DtDeposito.SetFocus
        Exit Sub
    End If
    If (CDbl(TxtValorDeposito) <> CDbl(TxtIngresado)) And ChkApertura.Value = 0 Then
        MsgBox "Valor cuenta(s) contable(s) debe ser igual al valor del Deposito...", vbInformation
        CboCuenta.SetFocus
        Exit Sub
    End If
    
    
    'Asiganmos cuenta contable si corresponmde
    If ChkApertura.Value = 0 Then
        If CboCuenta.Visible Then
            Lp_Cuenta = CboCuenta.ItemData(CboCuenta.ListIndex)
        Else
            Lp_Cuenta = 0
        End If
    End If
    'Proceder a grabar deposito
    
    On Error GoTo GrabaCheque
    cn.BeginTrans
        
        
               
       
    
        
        
        
        
        Lp_idsD = 0
        Lp_idsC = 0
        DestinosConceptos
        If LvDetalle.ListItems.Count > 0 Then
            If LvDetalle.SelectedItem.SubItems(10) = "SI" And Val(StabMov.Tag) > 0 Then
                MsgBox "Este cheque fue ingresado desde tesoreria..." & vbNewLine & "No es posible modificarlo en este m�dulo...", vbInformation
                cn.RollbackTrans
                Exit Sub
            End If
        End If
        If Val(StabMov.Tag) = 0 Then
            Lp_id_Mov = UltimoNro("ban_movimientos", "mov_id")
        Else
            Lp_id_Mov = StabMov.Tag
        End If
        
        
         'Aqui Grabamos la cuentas "multicuantas"

        
        Sql = "DELETE FROM com_con_multicuenta " & _
              "WHERE dcu_tipo='DEPOSITO' AND id_unico=" & Lp_id_Mov
        cn.Execute Sql
        
        If LvCue.ListItems.Count > 0 Then
        '    'Grabamos las cuentas en tabla de multicuantas para una boleta
        '
            Sql = "INSERT INTO com_con_multicuenta (dcu_tipo,id_unico,pla_id,dcu_valor) VALUES"
            For i = 1 To LvCue.ListItems.Count
                Sql = Sql & "('DEPOSITO'," & Lp_id_Mov & "," & LvCue.ListItems(i) & "," & CDbl(LvCue.ListItems(i).SubItems(2)) & "),"
            Next
            Sql = Mid(Sql, 1, Len(Sql) - 1)
            cn.Execute Sql
        End If
        
        
        
        Sql = "DELETE FROM ban_movimientos " & _
              "WHERE mov_id=" & Lp_id_Mov
        
        Sp_Apertura = "NO"
        If ChkApertura.Value = 1 Then Sp_Apertura = "SI"
        
        cn.Execute Sql
        Sql = "INSERT INTO ban_movimientos (mov_id,cte_id,mov_detalle,mov_fecha,mov_valor,mov_identificacion,mov_tipo,mov_nro_comprobante,mov_identificacion_id,con_id,des_id,pla_id,mov_apertura) " & _
              "VALUES(" & Lp_id_Mov & "," & Sp_IdCuenta & ",'" & CboDestinatarios.Text & " POR  " & CboConcepto.Text & "','" & Fql(DtDeposito) & "'," & CDbl(TxtValorDeposito) & ",'DEPOSITO','INGRESO'," & Val(TxtNroComprobante) & ",2," & Lp_idsC & "," & Lp_idsD & "," & Lp_Cuenta & ",'" & Sp_Apertura & "')"
        cn.Execute Sql
    
    cn.CommitTrans
    StabMov.Tag = 0
    LvCue.ListItems.Clear
    TxtValorDeposito = "0"
    TxtNroComprobante = ""
    Detalle
    DtDeposito.SetFocus
    Exit Sub
GrabaCheque:
    cn.RollbackTrans
    MsgBox "Ocurrio un problema al grabar..." & vbNewLine & Err.Number & vbNewLine & Err.Description, vbInformation
    
End Sub

Private Sub CmdEliminar_Click()
    Dim Ip_Movimiento As Integer
    If Val(StabMov.Tag) > 0 Then
        If LvDetalle.SelectedItem.SubItems(10) = "SI" Then
                MsgBox "Este movimiento fue realizado en el M�dulo Tesoreria..." & vbNewLine & "No es posible modificarlo aqu�...", vbInformation
                Exit Sub
        End If
    
        If MsgBox("�Est� seguro(a) de eliminar este movimiento...?", vbQuestion + vbYesNo) = vbNo Then Exit Sub
        Ip_Movimiento = LvDetalle.SelectedItem.SubItems(8)
        On Error GoTo Elininar
        cn.BeginTrans
            Select Case Ip_Movimiento
                Case 1
                    Sql = "SELECT che_id " & _
                          "FROM ban_cheques_fondos_rendir " & _
                          "WHERE che_id=" & Val(TxtValorCheque.Tag)
                    Consulta RsTmp, Sql
                    If RsTmp.RecordCount > 1 Then
                        MsgBox "Este Cheque ha sido utilizado para pagar documentos..." & _
                        vbNewLine & "No es posible eliminarlo...", vbInformation
                        Exit Sub
                    End If
                    
                    cn.Execute "UPDATE ban_cheques SET che_fecha_emision=Null," & _
                                                      "che_fecha_cobro=Null," & _
                                                      "che_valor=0," & _
                                                      "che_destinatario=''," & _
                                                      "che_concepto=''," & _
                                                      "che_estado='SIN EMITIR'," & _
                                                      "pla_id=0," & _
                                                      "mes_contable=0," & _
                                                      "ano_contable=0," & _
                                                     "mov_id=0 " & _
                               "WHERE mov_id=" & Val(StabMov.Tag)
                    cn.Execute "DELETE FROM ban_movimientos " & _
                               "WHERE mov_id=" & Val(StabMov.Tag)
                               
                    cn.Execute "DELETE FROM ban_cheques_fondos_rendir " & _
                               "WHERE che_id=" & TxtValorCheque.Tag
                               
                    cn.Execute "DELETE FROM com_con_multicuenta " & _
                               "WHERE dcu_tipo='CHEQUE' AND id_unico=" & TxtValorCheque.Tag
                               
                    CboChequeras_Click
                    MsgBox "El cheque puede ser ingresado nuevamente...", vbInformation
                Case 2 'deposito
                    cn.Execute "DELETE FROM ban_movimientos " & _
                               "WHERE mov_id=" & Val(StabMov.Tag)
                Case 3 'trnaferencioa
                    'Comprobaremos si la transferencia se realizo a cta propia
                   
                        cn.Execute "DELETE FROM ban_movimientos " & _
                               "WHERE mov_id=" & Val(StabMov.Tag)
                       cn.Execute "DELETE FROM ban_movimientos " & _
                               "WHERE mov_id=" & Val(LvDetalle.SelectedItem.SubItems(13))
                        
                        
                   
                Case 4 'cargo a cuaenta
                    cn.Execute "DELETE FROM ban_movimientos " & _
                               "WHERE mov_id=" & Val(StabMov.Tag)
            End Select
        cn.CommitTrans
        TxtValorCheque = 0
        TxtValorDeposito = 0
        TxtValorTransferencia = 0
        TxtValorCargo = 0
        TxtNroComprobanteCargo = ""
        TxtNroComprobante = ""
        TxtChNro.Visible = False
    
    End If
    CargaDatos
    Exit Sub
Elininar:
    MsgBox "Ocurrio un error al intentar eliminar..." & vbNewLine & Err.Number & " " & Err.Description
    cn.RollbackTrans
End Sub

Private Sub cmdExportar_Click()

    
    Dim tit(2) As String
    If LvDetalle.ListItems.Count = 0 Then Exit Sub
    FraProgreso.Visible = True
    tit(0) = "EMPRESA: " & Principal.SkEmpresa & "  R.U.T.:" & SP_Rut_Activo
    tit(1) = "MOVIMEINTOS EN CUENTA " & Date
    tit(2) = TxtCuenta
    ExportarNuevo LvDetalle, tit, Me, BarraProgreso
    FraProgreso.Visible = False
End Sub

Private Sub CmdHistorico_Click()
    Ban_ConciliacionHistorica.Show 1
End Sub

Private Sub CmdIngresaCargo_Click()
    'Validamos 1ro 8 Marzo 2012
    
    
    If Principal.CmdMenu(8).Visible = True Then
        If ConsultaCentralizado("7,8", IG_Mes_Contable, IG_Ano_Contable) Then
            MsgBox "periodo contable ya ha sido centralizado, " & vbNewLine & "No puede modificar"
            Exit Sub
        End If
    End If
    
    Dim Lp_id_Mov As Long
    If Val(Me.TxtValorCargo) = 0 Then
        MsgBox "Falta valor del cargo...", vbInformation
        TxtValorCargo.SetFocus
        Exit Sub
    End If
    
    Lp_Cuenta = 0
    If CboCuenta.Visible And CboCuenta.ListIndex = -1 Then
        MsgBox "Seleccione Cuenta Contable...", vbInformation
        CboCuenta.SetFocus
        Exit Sub
    End If
    
       
    If Month(DtCargo) <> IG_Mes_Contable Or Year(DtCargo) <> IG_Ano_Contable Then
        MsgBox "Fecha no corresponde al periodo contable...", vbInformation
        DtEmision.SetFocus
        Exit Sub
    End If
    
    If (CDbl(TxtValorCargo) <> CDbl(TxtIngresado)) And ChkApertura.Value = 0 Then
        MsgBox "Valor cuenta(s) contable(s) debe ser igual al valor del Cargo...", vbInformation
        CboCuenta.SetFocus
        Exit Sub
    End If
    
    
        'Asiganmos cuenta contable si corresponmde
    If CboCuenta.Visible Then Lp_Cuenta = CboCuenta.ItemData(CboCuenta.ListIndex)
    
     'Proceder a grabar deposito
    On Error GoTo Grabacargo
    cn.BeginTrans
        If LvDetalle.ListItems.Count > 0 Then
            If LvDetalle.SelectedItem.SubItems(10) = "SI" And Val(StabMov.Tag) > 0 Then
                MsgBox "Este cheque fue ingresado desde tesoreria..." & vbNewLine & "No es posible modificarlo en este m�dulo...", vbInformation
                cn.RollbackTrans
                Exit Sub
            End If
        End If
        Lp_idsD = 0
        Lp_idsC = 0
        DestinosConceptos
        
                
        If Val(StabMov.Tag) = 0 Then
            Lp_id_Mov = UltimoNro("ban_movimientos", "mov_id")
        Else
            Lp_id_Mov = StabMov.Tag
        End If
        
        'Aqui Grabamos la cuentas "multicuantas"
        Sql = "DELETE FROM com_con_multicuenta " & _
              "WHERE dcu_tipo='CARGO' AND id_unico=" & Lp_id_Mov
        cn.Execute Sql
        If LvCue.ListItems.Count > 0 Then
        '    'Grabamos las cuentas en tabla de multicuantas para una boleta
        '
            Sql = "INSERT INTO com_con_multicuenta (dcu_tipo,id_unico,pla_id,dcu_valor) VALUES"
            For i = 1 To LvCue.ListItems.Count
                Sql = Sql & "('CARGO'," & Lp_id_Mov & "," & LvCue.ListItems(i) & "," & CDbl(LvCue.ListItems(i).SubItems(2)) & "),"
            Next
            Sql = Mid(Sql, 1, Len(Sql) - 1)
            cn.Execute Sql
        End If
        
        
        
        Sql = "DELETE FROM ban_movimientos " & _
       "WHERE mov_id=" & Lp_id_Mov

        cn.Execute Sql
        
        
        
        Sql = "INSERT INTO ban_movimientos (mov_id,cte_id,mov_detalle,mov_fecha,mov_valor,mov_identificacion,mov_tipo,mov_nro_comprobante,mov_identificacion_id,con_id,des_id,pla_id) " & _
              "VALUES(" & Lp_id_Mov & "," & Sp_IdCuenta & ",'" & CboDestinatarios.Text & " POR  " & CboConcepto.Text & "','" & Fql(DtCargo) & "'," & CDbl(TxtValorCargo) & ",'CARGO','CARGO'," & Val(TxtNroComprobanteCargo) & ",4," & Lp_idsC & "," & Lp_idsD & "," & Lp_Cuenta & ")"
        cn.Execute Sql
    
    cn.CommitTrans
    LvCue.ListItems.Clear
    StabMov.Tag = 0
    TxtValorCargo = 0
    Me.TxtNroComprobanteCargo = 0
    Detalle
    DtDeposito.SetFocus
    Exit Sub
Grabacargo:
    cn.RollbackTrans
    MsgBox "Ocurrio un problema al grabar..." & vbNewLine & Err.Number & vbNewLine & Err.Description, vbInformation
End Sub

Private Sub CmdIngresaCheque_Click()
    Dim Lp_NroCheque As Long, Lp_idActual As Long, Sp_Where As String, Sp_Apertura As String * 2
    'Validaciones
    
    
    If Principal.CmdMenu(8).Visible = True Then
        If ConsultaCentralizado("7,8", IG_Mes_Contable, IG_Ano_Contable) Then
            MsgBox "periodo contable ya ha sido centralizado, " & vbNewLine & "No puede modificar"
            Exit Sub
        End If
    End If
    
    
    
    If CboChequeras.ListIndex = -1 Then
        MsgBox "Seleccione Chequera...", vbInformation
        CboChequeras.SetFocus
        Exit Sub
    ElseIf CboCheques.ListIndex = -1 Then
        MsgBox "Seleccione Cheque...", vbInformation
        CboCheques.SetFocus
        Exit Sub
    ElseIf CDbl(TxtValorCheque) = 0 Then
        MsgBox "Ingrese Valor...", vbInformation
        If TxtValorCheque.Enabled Then TxtValorCheque.SetFocus
        Exit Sub
    End If
    Lp_Cuenta = 0
    If CboCuenta.Visible And CboCuenta.ListIndex = -1 And ChkApertura.Value = 0 Then
        MsgBox "Seleccione Cuenta Contable...", vbInformation
        CboCuenta.SetFocus
        Exit Sub
    End If
    
    If (CDbl(TxtValorCheque) <> CDbl(TxtIngresado)) And ChkApertura.Value = 0 Then
        MsgBox "Valor cuenta(s) contable(s) debe ser igual al valor del cheque...", vbInformation
        CboCuenta.SetFocus
        Exit Sub
    End If
    
    If Month(DtEmision) <> IG_Mes_Contable Or Year(DtEmision) <> IG_Ano_Contable Then
        MsgBox "Fecha no corresponde al periodo contable...", vbInformation
        DtEmision.SetFocus
        Exit Sub
    End If
    If ChkApertura.Value = 0 Then
        If CboCuenta.ItemData(CboCuenta.ListIndex) = IG_IdCtaFondo Then
            'Si se detecta que el cheque sera por fondos por rendir
            'Debe seleccionar un beneficiarios <=>
            If Me.CboDestinatarios.Text = "Destinatario" Then
                MsgBox "Para cheques de Fondos por Rendir debe ingresar o seleccioar un destinatario...", vbInformation
                Me.CboDestinatarios.SetFocus
                Exit Sub
            End If
        End If
    End If
    
    
    
    
    
    
    
    
    
    
    'Asiganmos cuenta contable si corresponmde
    If CboCuenta.Visible And ChkApertura.Value = 0 Then
        Lp_Cuenta = CboCuenta.ItemData(CboCuenta.ListIndex)
    Else
        Lp_Cuenta = 0
    End If
    
    
    'Proceder a grabar chque
    On Error GoTo GrabaCheque
    cn.BeginTrans
        Lp_IdMov = UltimoNro("ban_movimientos", "mov_id")
        Lp_NroCheque = CboCheques.ItemData(CboCheques.ListIndex)
        
        Sp_Where = "WHERE che_id=" & CboCheques.ItemData(CboCheques.ListIndex)
        
        If TxtChNro.Visible = True Then
            Sql = "SELECT che_id " & _
                  "FROM ban_cheques " & _
                  "WHERE mov_id=" & TxtChNro.Tag
            Consulta RsTmp, Sql
            If RsTmp.RecordCount > 0 Then Lp_NroCheque = RsTmp!che_id
            Lp_IdMov = TxtChNro.Tag
            Sp_Where = "WHERE mov_id=" & Lp_IdMov
        End If
        
        
        
        'Aqui Grabamos la cuentas "multicuantas"

        
        Sql = "DELETE FROM com_con_multicuenta " & _
              "WHERE dcu_tipo='CHEQUE' AND id_unico=" & Lp_NroCheque
        cn.Execute Sql
        
        If LvCue.ListItems.Count > 0 Then
        '    'Grabamos las cuentas en tabla de multicuantas para una boleta
        '
            Sql = "INSERT INTO com_con_multicuenta (dcu_tipo,id_unico,pla_id,dcu_valor) VALUES"
            For i = 1 To LvCue.ListItems.Count
                Sql = Sql & "('CHEQUE'," & Lp_NroCheque & "," & LvCue.ListItems(i) & "," & CDbl(LvCue.ListItems(i).SubItems(2)) & "),"
            Next
            Sql = Mid(Sql, 1, Len(Sql) - 1)
            cn.Execute Sql
        End If
    

        Lp_idsD = 0
        Lp_idsC = 0
        If LvDetalle.ListItems.Count > 0 Then
            If LvDetalle.SelectedItem.SubItems(10) = "SI" And Val(StabMov.Tag) > 0 Then
                MsgBox "Este cheque fue ingresado desde tesoreria..." & vbNewLine & "No es posible modificarlo en este m�dulo...", vbInformation
                cn.RollbackTrans
                Exit Sub
            End If
        End If
        DestinosConceptos

        cn.Execute "DELETE FROM ban_cheques_fondos_rendir " & _
                    "WHERE chf_haber>0 AND che_id=" & Lp_NroCheque
        
        
        Sql = "UPDATE ban_cheques SET che_fecha_emision='" & Fql(DtEmision) & "'," & _
                                      "che_fecha_cobro='" & Fql(DtCobro) & "'," & _
                                      "che_valor=" & CDbl(TxtValorCheque) & "," & _
                                      "che_destinatario='" & CboDestinatarios.Text & "'," & _
                                      "che_concepto='" & CboConcepto & "'," & _
                                      "che_estado='POR COBRAR'," & _
                                      "pla_id=" & Lp_Cuenta & "," & _
                                      "mes_contable=" & IG_Mes_Contable & "," & _
                                      "ano_contable=" & IG_Ano_Contable & "," & _
                                      "mov_id=" & Lp_IdMov & " " & _
                Sp_Where
        cn.Execute Sql
        
        If TxtChNro.Visible = True Then
            cn.Execute "DELETE FROM ban_movimientos " & _
                       "WHERE mov_id=" & Lp_IdMov
        End If
        Sp_Apertura = "NO"
        If ChkApertura.Value = 1 Then Sp_Apertura = "SI"
        Sql = "INSERT INTO ban_movimientos (mov_id,cte_id,mov_detalle,mov_fecha,mov_valor,mov_identificacion,mov_identificacion_id,con_id,des_id,pla_id,mov_apertura) " & _
                "VALUES(" & Lp_IdMov & "," & Sp_IdCuenta & ",'" & CboDestinatarios.Text & " POR  " & CboConcepto.Text & "','" & Fql(DtEmision) & "'," & CDbl(TxtValorCheque) & ",'CHEQUE',1," & Lp_idsD & "," & Lp_idsC & "," & Lp_Cuenta & ",'" & Sp_Apertura & "')"
        cn.Execute Sql
        
        If LvCue.ListItems.Count > 0 Then
            'Cheque por fondos por rendir
            If Val(LvCue.ListItems(1)) = IG_IdCtaFondo Then
                cn.Execute "INSERT INTO ban_cheques_fondos_rendir (che_id,chf_haber,mov_id) " & _
                           "VALUES(" & Lp_NroCheque & "," & CDbl(TxtValorCheque) & "," & Lp_IdMov & ")"
            End If
            
            '5 abril 2014
            'Cheque por Anticipo proveedores
            If Val(LvCue.ListItems(1)) = IG_Id_CuentaAnticipoProveedores Then
                cn.Execute "INSERT INTO ban_cheques_fondos_rendir (che_id,chf_haber,chf_tipo,pro_rut,mov_id) " & _
                            "VALUES(" & Lp_NroCheque & "," & CDbl(TxtValorCheque) & ",'ANTICIPO','" & LvCue.ListItems(1).SubItems(3) & "'," & Lp_IdMov & ")"
            End If
            
        End If
        
   
        txtValor = 0
        LvCue.ListItems.Clear
        TxtIngresado = 0
        
        TxtValorCheque = 0
        TxtChNro.Visible = False
    
    cn.CommitTrans
    StabMov.Tag = 0
    CargaDatos
    CboChequeras_Click
    Exit Sub
GrabaCheque:
    cn.RollbackTrans
    MsgBox "Ocurrio un problema al grabar..." & vbNewLine & Err.Number & vbNewLine & Err.Description, vbInformation
    
End Sub
Private Sub DestinosConceptos()
        If Len(CboDestinatarios.Text) > 0 Then
            Sql = "SELECT des_id " & _
                  "FROM ban_destinatarios " & _
                  "WHERE des_nombre='" & CboDestinatarios.Text & "'"
            Consulta RsTmp, Sql
            If RsTmp.RecordCount = 0 Then
                Lp_idsD = UltimoNro("ban_destinatarios", "des_id ")
                cn.Execute "INSERT INTO ban_destinatarios (des_id,des_nombre) VALUES(" & Lp_idsD & ",'" & CboDestinatarios.Text & "')"
            Else
                Lp_idsD = RsTmp!des_id
            End If
        End If
        If Len(CboConcepto.Text) > 0 Then
            Sql = "SELECT con_id " & _
                  "FROM ban_conceptos " & _
                  "WHERE con_nombre='" & CboConcepto.Text & "'"
            Consulta RsTmp, Sql
            If RsTmp.RecordCount = 0 Then
                Lp_idsC = UltimoNro("ban_conceptos", "con_id ")
                cn.Execute "INSERT INTO ban_conceptos (con_id,con_nombre) VALUES(" & Lp_idsC & ",'" & CboConcepto.Text & "')"
            Else
                Lp_idsC = RsTmp!con_id
            End If
        End If
End Sub

Private Sub CmdIngresaTransferencia_Click()
    Dim Ip_Cta_Propia As Integer, Lp_id_Mov As Long, Lp_id_Mov2 As Long
    
    'Validamos 1ro 25 Febro 2012
    
    
    If Principal.CmdMenu(8).Visible = True Then
        If ConsultaCentralizado("7,8", IG_Mes_Contable, IG_Ano_Contable) Then
            MsgBox "periodo contable ya ha sido centralizado, " & vbNewLine & "No puede modificar"
            Exit Sub
        End If
    End If
    
    
    If Val(Me.TxtValorTransferencia) = 0 Then
        MsgBox "Falta valor de transferencia...", vbInformation
        TxtValorTransferencia.SetFocus
        Exit Sub
    End If
    
    
    Lp_Cuenta = 0
    If CboCuenta.Visible And CboCuenta.ListIndex = -1 Then
            If Not OptPropias Then
                MsgBox "Seleccione Cuenta Contable...", vbInformation
                CboCuenta.SetFocus
                Exit Sub
            End If
    End If
    
    If Month(DtTransferencia) <> IG_Mes_Contable Or Year(DtTransferencia) <> IG_Ano_Contable Then
        MsgBox "Fecha no corresponde al periodo contable...", vbInformation
        DtDeposito.SetFocus
        Exit Sub
    End If
    
    If (CDbl(TxtValorTransferencia) <> CDbl(TxtIngresado)) And ChkApertura.Value = 0 Then
        If Not OptPropias Then
            MsgBox "Valor cuenta(s) contable(s) debe ser igual al valor de la transferencia...", vbInformation
            CboCuenta.SetFocus
            Exit Sub
        End If
    End If
    
    
    'Asiganmos cuenta contable si corresponmde
    Lp_Cuenta = 0
    If CboCuenta.Visible Then
        If Not OptPropias Then Lp_Cuenta = CboCuenta.ItemData(CboCuenta.ListIndex)
    End If
    
    Ip_Cta_Propia = 0
    If Me.OptPropias = True Then
        If CboCuentasPropias.ListIndex = -1 Then
            MsgBox "Seleccione cuenta propia para transferencia..."
            CboCuentasPropias.SetFocus
            Exit Sub
        Else
            Ip_Cta_Propia = CboCuentasPropias.ItemData(CboCuentasPropias.ListIndex)
        End If
        If Val(StabMov.Tag) > 0 Then
            If CDbl(LvDetalle.SelectedItem.SubItems(6)) > 0 Then
                MsgBox "Este moviento debe modificarse desde Cuenta de Origen...", vbInformation
                Exit Sub
            End If
        End If
    End If
        
     'Proceder a grabar Transferencia
    On Error GoTo GrabaCheque
    cn.BeginTrans
        
        If LvDetalle.ListItems.Count > 0 Then
            If LvDetalle.SelectedItem.SubItems(10) = "SI" And Val(StabMov.Tag) > 0 Then
                MsgBox "Este cheque fue ingresado desde tesoreria..." & vbNewLine & "No es posible modificarlo en este m�dulo...", vbInformation
                cn.RollbackTrans
                Exit Sub
            End If
        End If
        Lp_idsD = 0
        Lp_idsC = 0
        DestinosConceptos
        
        
        If Val(Me.StabMov.Tag) = 0 Then
            Lp_id_Mov = UltimoNro("ban_movimientos", "mov_id")
            If OptPropias.Value Then
                Lp_id_Mov2 = Lp_id_Mov + 1
            Else
                Lp_id_Mov2 = 0
            End If
        Else
            Lp_id_Mov = StabMov.Tag
            Sql = "SELECT mov_id_unico_entre_cuentas id_unico " & _
                  "FROM ban_movimientos " & _
                  "WHERE mov_id=" & Lp_id_Mov
            Consulta RsTmp, Sql
            If RsTmp.RecordCount > 0 Then
                Lp_id_Mov2 = RsTmp!id_unico
                Sql = "DELETE FROM ban_movimientos " & _
                "WHERE mov_id=" & Lp_id_Mov2
                cn.Execute Sql
            End If
            
        End If
        
                
        'Aqui Grabamos la cuentas "multicuantas"
        Sql = "DELETE FROM com_con_multicuenta " & _
              "WHERE dcu_tipo='TRANSFERENCIA' AND id_unico=" & Lp_id_Mov
        cn.Execute Sql
        
        '    'Grabamos las cuentas en tabla de multicuantas para una transferencia
        If OptPropias.Value Then
              Sql = "SELECT pla_id,pla_nombre " & _
            "FROM ban_cta_cte e " & _
            "JOIN con_plan_de_cuentas c ON e.ban_id=c.pla_id " & _
            "WHERE cte_id=" & Me.CboCuentasPropias.ItemData(CboCuentasPropias.ListIndex)
            
            Consulta RsTmp, Sql
            If RsTmp.RecordCount > 0 Then ' Li_CtaBanco = RsTmp!ban_id            '
            
               LvCue.ListItems.Clear
               LvCue.ListItems.Add , , RsTmp!pla_id
               LvCue.ListItems(1).SubItems(1) = RsTmp!pla_nombre
               LvCue.ListItems(1).SubItems(2) = TxtValorTransferencia
            End If
        End If
        If LvCue.ListItems.Count > 0 Then
            Sql = "INSERT INTO com_con_multicuenta (dcu_tipo,id_unico,pla_id,dcu_valor) VALUES"
            For i = 1 To LvCue.ListItems.Count
                Sql = Sql & "('TRANSFERENCIA'," & Lp_id_Mov & "," & LvCue.ListItems(i) & "," & CDbl(LvCue.ListItems(i).SubItems(2)) & "),"
            Next
            Sql = Mid(Sql, 1, Len(Sql) - 1)
            cn.Execute Sql
        End If
        
        
        
        
        
        
        
        Sql = "DELETE FROM ban_movimientos " & _
        "WHERE mov_id=" & Lp_id_Mov

        cn.Execute Sql
        
        Sql = "INSERT INTO ban_movimientos (mov_id,cte_id,mov_detalle,mov_fecha,mov_valor,mov_identificacion," & _
        "mov_tipo,mov_transferencia_a_cuenta_id,mov_identificacion_id,con_id,des_id,pla_id,mov_id_unico_entre_cuentas) " & _
              "VALUES(" & Lp_id_Mov & "," & Sp_IdCuenta & ",'" & _
              CboDestinatarios.Text & " POR  " & CboConcepto.Text & "','" & Fql(DtTransferencia) & "'," & _
              CDbl(TxtValorTransferencia) & ",'TRANSFERENCIA','CARGO'," & _
              Ip_Cta_Propia & ",3," & Lp_idsC & "," & Lp_idsD & "," & Lp_Cuenta & "," & Lp_id_Mov2 & ")"
        cn.Execute Sql
        
       '**********************************************************-
       '12 Abrio 2014
       'intentaremos hacer con una transferencia lo mismo
       'que hacemos con los cheques a fondos por rendir o anticipo proveedores
       '***********************************************************
       If LvCue.ListItems.Count > 0 Then
            Dim Lp_NCheque As Long
            
            'debemos crear un cheque ficticio para relacionarlo con los pagos
            '19 Abril 2014
            Lp_NCheque = UltimoNro("ban_cheques", "che_id")
            Sql = "INSERT INTO ban_cheques (che_id) VALUES(" & Lp_NCheque & ")"
            cn.Execute Sql
            
       
            'Transferencia por fondos por rendir
            If Val(LvCue.ListItems(1)) = IG_IdCtaFondo Then
                cn.Execute "INSERT INTO ban_cheques_fondos_rendir (che_id,chf_haber,mov_id) " & _
                           "VALUES(" & Lp_NCheque & "," & CDbl(TxtValorTransferencia) & "," & Lp_id_Mov & ")"
            End If
            
            '5 abril 2014
            'Transferencia por Anticipo proveedores
            If Val(LvCue.ListItems(1)) = IG_Id_CuentaAnticipoProveedores Then
            
            
                cn.Execute "INSERT INTO ban_cheques_fondos_rendir (che_id,chf_haber,chf_tipo,pro_rut,mov_id) " & _
                            "VALUES(" & Lp_NCheque & "," & CDbl(TxtValorTransferencia) & ",'ANTICIPO','" & LvCue.ListItems(1).SubItems(3) & "'," & Lp_id_Mov & ")"
            End If
            
        End If
        
        
        
        
        
        
        
        If OptPropias.Value Then
        
            If Val(Me.StabMov.Tag) = 0 Then
                '
            Else
            
            End If
        
            'Agregar movimiento en la otra cuenta Deposito
            Sql = "INSERT INTO ban_movimientos (mov_id,cte_id,mov_detalle,mov_fecha,mov_valor,mov_identificacion,mov_tipo,mov_transferencia_a_cuenta_id,mov_identificacion_id,con_id,des_id,pla_id,mov_id_unico_entre_cuentas) " & _
                    "VALUES(" & Lp_id_Mov2 & "," & Ip_Cta_Propia & ",'" & CboDestinatarios.Text & " POR  " & CboConcepto.Text & "','" & Fql(DtTransferencia) & "'," & CDbl(TxtValorTransferencia) & ",'TRANSFERENCIA DE  " & TxtCuenta & "','INGRESO'," & Sp_IdCuenta & ",3," & Lp_idsC & "," & Lp_idsD & "," & Lp_Cuenta & "," & Lp_id_Mov & ")"
            cn.Execute Sql
            
            
                '    'Grabamos las cuentas en tabla de multicuantas para una boleta
             Sql = "SELECT pla_id,pla_nombre " & _
                "FROM ban_cta_cte e " & _
                "JOIN con_plan_de_cuentas c ON e.ban_id=c.pla_id " & _
                "WHERE cte_id=" & Sp_IdCuenta
          
            Consulta RsTmp, Sql
            If RsTmp.RecordCount > 0 Then ' Li_CtaBanco = RsTmp!ban_id            '
            
               LvCue.ListItems.Clear
               LvCue.ListItems.Add , , RsTmp!pla_id
               LvCue.ListItems(1).SubItems(1) = RsTmp!pla_nombre
               LvCue.ListItems(1).SubItems(2) = TxtValorTransferencia
            End If
     
            If LvCue.ListItems.Count > 0 Then
                Sql = "INSERT INTO com_con_multicuenta (dcu_tipo,id_unico,pla_id,dcu_valor) VALUES"
                For i = 1 To LvCue.ListItems.Count
                    Sql = Sql & "('TRANSFERENCIA'," & Lp_id_Mov2 & "," & LvCue.ListItems(i) & "," & CDbl(LvCue.ListItems(i).SubItems(2)) & "),"
                Next
                Sql = Mid(Sql, 1, Len(Sql) - 1)
                cn.Execute Sql
            End If
            
            
            
            
            
            
        End If
        
        
        
    
    
    cn.CommitTrans
    LvCue.ListItems.Clear
    StabMov.Tag = 0
    TxtValorTransferencia = "0"
    Detalle
    DtTransferencia.SetFocus
    Exit Sub
GrabaCheque:
    cn.RollbackTrans
    MsgBox "Ocurrio un problema al grabar..." & vbNewLine & Err.Number & vbNewLine & Err.Description, vbInformation
    
End Sub

Private Sub CmdModificar_Click()
    'Exit Sub
    'StabMov.Tag = 0
    'Modifica movimeintos
    Select Case StabMov.Tab
        Case 0
            StabMov.ToolTipText = "Modificando  cheque"
            'TxtValorCheque = 0
            CboChequeras.SetFocus
            TxtValorCheque.Enabled = True
            CmdOkCuenta.Enabled = True
            ChkApertura.Enabled = True
        Case 1
            StabMov.ToolTipText = "Ingresando Deposito"
            'TxtValorDeposito = 0
            TxtNroComprobante = ""
            DtDeposito.SetFocus
            TxtValorDeposito.Enabled = True
            ChkApertura.Enabled = True
        Case 2
            StabMov.ToolTipText = "Ingresando transferencia"
            'TxtValorTransferencia = 0
            DtTransferencia.SetFocus
            TxtValorTransferencia.Enabled = True
        Case 3
            StabMov.ToolTipText = "Ingresando Cargo"
'            TxtValorCargo = 0
            TxtNroComprobanteCargo = ""
            DtCargo.SetFocus
            TxtValorCargo.Enabled = True
    End Select
    CmdOkCuenta.Enabled = True
End Sub

Private Sub cmdMultiCuenta_Click()
    Compra_ContaMulticuenta.Sm_Formulario = "CHEQUES"
    Compra_ContaMulticuenta.TxtMonto = TxtValorCheque
    Compra_ContaMulticuenta.Show 1
End Sub

Private Sub CmdNuevo_Click()
    StabMov.Tag = 0
    'Nuevo movimeintos
    LvCue.ListItems.Clear
    CmdOkCuenta.Enabled = True
    Select Case StabMov.Tab
        
        Case 0
            StabMov.ToolTipText = "Ingresando nuevo cheque"
            TxtValorCheque = 0
            TxtChNro.Visible = False
            CboChequeras_Click
            'CboChequeras.SetFocus
            TxtValorCheque.Enabled = True
            CmdOkCuenta.Enabled = True
            LvCue.ListItems.Clear
            ChkApertura.Enabled = True
        Case 1
            StabMov.ToolTipText = "Ingresando Deposito"
            TxtValorDeposito = 0
            TxtNroComprobante = ""
            DtDeposito.SetFocus
            TxtValorDeposito.Enabled = True
            ChkApertura.Enabled = True
        Case 2
            StabMov.ToolTipText = "Ingresando transferencia"
            TxtValorTransferencia = 0
            DtTransferencia.SetFocus
            TxtValorTransferencia.Enabled = True
        Case 3
            StabMov.ToolTipText = "Ingresando Cargo"
            TxtValorCargo = 0
            TxtNroComprobanteCargo = ""
            DtCargo.SetFocus
            TxtValorCargo.Enabled = True
    End Select
End Sub

Private Sub CmdOkCuenta_Click()
    Dim Bp_Ingresada As Boolean, Sp_rut_Anticipo As String
    Dim Ip_P As Integer
    Sp_rut_Anticipo = ""
    If CboCuenta.ListIndex = -1 Then
        MsgBox "Debe seleccionar cuenta contable...", vbInformation
        CboCuenta.SetFocus
        Exit Sub
    End If
    If Val(txtValor) = 0 Then
        MsgBox "Debe ingresar Valor...", vbInformation
        txtValor.SetFocus
        Exit Sub
    End If
    Bp_Ingresada = False
    
    
    'Verificamos que sea Anticipo Proveedores
    '5-4-2014
    If CboCuenta.ItemData(CboCuenta.ListIndex) = IG_Id_CuentaAnticipoProveedores Then
            SG_codigo = Empty
            BuscaProveedor.Show 1
            If Len(SG_codigo) > 0 Then
                Sp_rut_Anticipo = SG_codigo
            End If
    End If
    
    If LvCue.ListItems.Count > 0 Then
        If Val(LvCue.ListItems(1)) = IG_IdCtaFondo Then
            MsgBox "Cuenta de: Fondos por rendir, se ingresa como cuenta unica..."
            CboCuenta.SetFocus
            Exit Sub
        End If
        
       
    End If
    

    
    
    If CboCuenta.ItemData(CboCuenta.ListIndex) = IG_IdCtaFondo Then
        If LvCue.ListItems.Count > 0 Then
            MsgBox "La cuenta Fondos por rendir, no puede combinarse con otras cuentas...", vbInformation
            CboCuenta.SetFocus
            Exit Sub
        End If
    End If
    
    For i = 1 To LvCue.ListItems.Count
        If CboCuenta.ItemData(CboCuenta.ListIndex) = LvCue.ListItems(i) Then
            MsgBox "Esta cuenta ya est� ingresada...", vbInformation
            Bp_Ingresada = True
            Exit For
        End If
    Next
    If Bp_Ingresada Then
        CboCuenta.SetFocus
        Exit Sub
    End If
    
    'Ya esta validado ahora agregamos
    LvCue.ListItems.Add , , CboCuenta.ItemData(CboCuenta.ListIndex)
    
    Ip_P = LvCue.ListItems.Count
    LvCue.ListItems(Ip_P).SubItems(1) = CboCuenta.Text
    LvCue.ListItems(Ip_P).SubItems(2) = NumFormat(txtValor)
    LvCue.ListItems(Ip_P).SubItems(3) = Sp_rut_Anticipo
    
    SumaG
    
    txtValor = 0
    If CboCuenta.Visible Then CboCuenta.SetFocus
End Sub

Private Sub CmdPrev_Click()
    Dim Lp_C As Long, Lp_T_CN As Long, Bp_Dnc As Boolean, Lp_T_D As Long
    Dim Lp_Id_Historica As Long, Sp_Fecha_His As String, Lp_Valores_His As Long, Lp_Sub_His As Long
    LvConciliacion.ListItems.Clear
    LvConciliacion.ListItems.Add , , "SALDO CONTABLE"
    Lp_C = LvConciliacion.ListItems.Count
    LvConciliacion.ListItems(Lp_C).SubItems(1) = LvDetalle.ListItems(LvDetalle.ListItems.Count).SubItems(3)
    LvConciliacion.ListItems(Lp_C).SubItems(3) = TxtSaldo.Tag
    
    LvConciliacion.ListItems.Add , , "MAS VALORES NO COBRADOS"
    Lp_C = LvConciliacion.ListItems.Count
    'filanegra LvConciliacion, Lp_C
    Lp_T_CN = 0
    For i = 1 To LvDetalle.ListItems.Count
        If LvDetalle.ListItems(i).SubItems(8) = 1 And LvDetalle.ListItems(i).SubItems(11) = "NO" Then
            LvConciliacion.ListItems.Add , , "NRO " & LvDetalle.ListItems(i).SubItems(2) & " " & LvDetalle.ListItems(i).SubItems(4)
            Lp_C = LvConciliacion.ListItems.Count
            LvConciliacion.ListItems(Lp_C).SubItems(1) = LvDetalle.ListItems(i).SubItems(3)
            LvConciliacion.ListItems(Lp_C).SubItems(2) = CDbl(LvDetalle.ListItems(i).SubItems(5))
            LvConciliacion.ListItems(Lp_C).SubItems(4) = LvDetalle.ListItems(i).SubItems(16)
            Lp_T_CN = Lp_T_CN + CDbl(LvDetalle.ListItems(i).SubItems(5))
        ElseIf (LvDetalle.ListItems(i).SubItems(8) = 3 Or LvDetalle.ListItems(i).SubItems(8) = 4) And LvDetalle.ListItems(i).SubItems(11) = "NO" Then
            LvConciliacion.ListItems.Add , , " " & LvDetalle.ListItems(i).SubItems(2) & " " & LvDetalle.ListItems(i).SubItems(4)
            Lp_C = LvConciliacion.ListItems.Count
            LvConciliacion.ListItems(Lp_C).SubItems(1) = LvDetalle.ListItems(i).SubItems(3)
            LvConciliacion.ListItems(Lp_C).SubItems(2) = CDbl(LvDetalle.ListItems(i).SubItems(5))
            LvConciliacion.ListItems(Lp_C).SubItems(4) = LvDetalle.ListItems(i).SubItems(16)
            Lp_T_CN = Lp_T_CN + CDbl(LvDetalle.ListItems(i).SubItems(5))
        End If
        
    Next
    LvConciliacion.ListItems.Add , , "TOTAL VALORES NO COBRADOS"
    Lp_C = LvConciliacion.ListItems.Count
    'LvConciliacion.ListItems(Lp_C).SubItems(3) = Lp_T_CN * -1
    LvConciliacion.ListItems(Lp_C).SubItems(3) = Lp_T_CN
'
   ' filanegra LvConciliacion, Lp_C
    
    
    'DEPOSITOS NO CONCILIADOS 23 Agosto 2012
    LvConciliacion.ListItems.Add , , ""
    Bp_Dnc = False
    For i = 1 To LvDetalle.ListItems.Count
        If LvDetalle.ListItems(i).SubItems(8) = 2 And LvDetalle.ListItems(i).SubItems(11) = "NO" Then
            Bp_Dnc = True
            Exit For
        End If
    Next
    
    If Bp_Dnc Then
        LvConciliacion.ListItems.Add , , "MENOS DEPOSITOS NO CONCILIADOS"
        Lp_C = LvConciliacion.ListItems.Count
        'filanegra LvConciliacion, Lp_C
        Lp_T_D = 0
        For i = 1 To LvDetalle.ListItems.Count
            If LvDetalle.ListItems(i).SubItems(8) = 2 And LvDetalle.ListItems(i).SubItems(11) = "NO" Then
                LvConciliacion.ListItems.Add , , "DEPOSITO NRO " & LvDetalle.ListItems(i).SubItems(2) & " " & LvDetalle.ListItems(i).SubItems(4)
                Lp_C = LvConciliacion.ListItems.Count
                LvConciliacion.ListItems(Lp_C).SubItems(1) = LvDetalle.ListItems(i).SubItems(3)
                LvConciliacion.ListItems(Lp_C).SubItems(2) = CDbl(LvDetalle.ListItems(i).SubItems(6))
                Lp_T_D = Lp_T_D + CDbl(LvDetalle.ListItems(i).SubItems(6))
            End If
        Next
        LvConciliacion.ListItems.Add , , "TOTAL DEPOSITOS NO CONCILIADOS"
        Lp_C = LvConciliacion.ListItems.Count
        LvConciliacion.ListItems(Lp_C).SubItems(3) = Lp_T_D
        
        'filanegra LvConciliacion, Lp_C
    End If
    
    
    LvConciliacion.ListItems.Add , , ""
    LvConciliacion.ListItems.Add , , "SALDO CONTABLE MAS"
    LvConciliacion.ListItems.Add , , "CHEQUES NO CONCILIADOS MENOS"
    LvConciliacion.ListItems.Add , , "DEPOSITOS NO CONCILIADOS  "
    LvConciliacion.ListItems.Add , , "ES IGUAL A SALDO CARTOLA BANCO"
    Lp_C = LvConciliacion.ListItems.Count
    'LvConciliacion.ListItems(Lp_C).SubItems(3) = NumFormat(CDbl(txtSaldo.Tag) - Lp_T_CN + Lp_T_D)
    LvConciliacion.ListItems(Lp_C).SubItems(3) = CDbl(TxtSaldo.Tag + Lp_T_CN - Lp_T_D)

    'Guardar conciliacion historica
    '27-Abril-2013
    
    '1ro eliminamos historico exixstente respecto al periodo
      cn.Execute "DELETE ban_conciliacion_historica.*,ban_conciliacion_historica_detalle.* " & _
                                "FROM ban_conciliacion_historica " & _
                                "JOIN ban_conciliacion_historica_detalle USING(cob_id) " & _
                                "WHERE rut_emp='" & SP_Rut_Activo & "' AND cob_mes=" & IG_Mes_Contable & " AND cob_ano=" & IG_Ano_Contable
    
    
    
    
    Lp_Id_Historica = UltimoNro("ban_conciliacion_historica", "cob_id")
    'On Error GoTo ExortaLv
    
    cn.Execute "INSERT INTO ban_conciliacion_historica (cob_id,cob_mes,cob_ano,rut_emp) " & _
                "VALUES(" & Lp_Id_Historica & "," & IG_Mes_Contable & "," & IG_Ano_Contable & ",'" & SP_Rut_Activo & "')"
    Sql = "INSERT INTO ban_conciliacion_historica_detalle (cob_id,cbd_detalle,cbd_fecha,cbd_valores,cbd_subtotales) VALUES "
    For i = 1 To LvConciliacion.ListItems.Count
        Lp_Valores_His = Val(LvConciliacion.ListItems(i).SubItems(2))
        Lp_Sub_His = Val(LvConciliacion.ListItems(i).SubItems(3))
        Sp_Fecha_His = ""
        If Len(LvConciliacion.ListItems(i).SubItems(1)) > 0 Then Sp_Fecha_His = LvConciliacion.ListItems(i).SubItems(1)
        
        Sql = Sql & "(" & Lp_Id_Historica & ",'" & LvConciliacion.ListItems(i) & "','" & Sp_Fecha_His & "'," & _
            Lp_Valores_His & "," & Lp_Sub_His & "),"
    
    Next
    Sql = Mid(Sql, 1, Len(Sql) - 1)
    cn.Execute Sql
    
    
    
    
    
    
ExortaLv:
    
    Dim tit(2) As String
    If LvConciliacion.ListItems.Count = 0 Then Exit Sub
    FraProgreso.Visible = True
    tit(0) = "EMPRESA: " & Principal.SkEmpresa & "  R.U.T.:" & SP_Rut_Activo
    tit(1) = "CONCILIACION BANCARIA AL " & Date
    tit(2) = ""
    ExportarNuevo LvConciliacion, tit, Me, BarraProgreso
    FraProgreso.Visible = False
End Sub



Private Sub CmdSalir_Click()
    Unload Me
End Sub

Private Sub Command1_Click()
    Unload Me
End Sub


Private Sub Form_KeyPress(KeyAscii As Integer)
    If Me.ActiveControl.Tag = "T" Then
        KeyAscii = Asc(UCase(Chr(KeyAscii)))
    ElseIf Me.ActiveControl.Tag = "N" Then
        KeyAscii = SoloNumeros(KeyAscii)
    ElseIf KeyAscii = 13 Then
        SendKeys "{TAB}" 'envia un tab
    End If
End Sub
Private Sub SumaG()
    TxtIngresado = NumFormat(TotalizaColumna(LvCue, "monto"))
    If CDbl(TxtIngresado) = CDbl(TxtValorCheque) Then
        TxtIngresado.ForeColor = vbBlue
    Else
        TxtIngresado.ForeColor = vbRed
    End If
End Sub
Private Sub Form_Load()
    Dim Sp_Che As String * 1
    Bp_CargoFormulario = False
   ' TxtSaldoInicial = NumFormat(Me.Lp_SaldoInicial)
    TxtCuenta = Sm_Cuenta
    Aplicar_skin Me
    Centrar Me
    DtEmision = Date
    DtCobro = Date
    DtDeposito = Date
    DtTransferencia = Date
    DtCargo = Date
    
    LLenarCombo CboCuenta, "pla_nombre", "pla_id", "con_plan_de_cuentas", "pla_activo='SI' AND pla_id NOT IN(" & IG_IdCtaProveedores & "," & IG_IdCtaClientes & ")", "pla_nombre"
    DestinatariosConceptos
    
    'Sql = "SELECT emp_centro_de_costos,emp_areas,emp_item_de_gastos,emp_cuenta " & _
    '      "FROM sis_empresas " & _
   '       "WHERE rut='" & SP_Rut_Activo & "'"
   ' Consulta RsTmp, Sql
   '
    ''''''

    ''''''
        
    CboEstado.AddItem "Operaciones sin Conciliar"
    CboEstado.ItemData(CboEstado.ListCount - 1) = 1
    CboEstado.AddItem "Operaciones conciliadas"
    CboEstado.ItemData(CboEstado.ListCount - 1) = 2
    CboEstado.AddItem "Todas las operaciones"
    CboEstado.ItemData(CboEstado.ListCount - 1) = 3
    CboEstado.ListIndex = CboEstado.ListCount - 1
    
    'Debiera tener cuenta si o si 22 Junio 2013
  '  If RsTmp!emp_cuenta = "NaaaaaaaO" Then
  '          cmdCuenta.Visible = False
  '          LLenarCombo CboCuenta, "pla_nombre", "pla_id", "con_plan_de_cuentas", "pla_id=99999 "
  '          CboCuenta.ListIndex = 0
  '          CboCuenta.Visible = False
  '  End If
    
  '  LLenarCombo Me.CboCheques,CONCAT('Cheque Nro ', &
    
    CargaDatos
    
    Bp_CargoFormulario = True
End Sub
Private Sub CargaDatos()
    TxtIngresado = 0
    LvCue.ListItems.Clear
    TxtMesContable = UCase(MonthName(IG_Mes_Contable))
    TxtAnoContable = IG_Ano_Contable
    LLenarCombo CboChequeras, "CONCAT(CAST(chc_serie_inicial AS CHAR),' AL ',CAST(chc_serie_inicial + chc_cantidad - 1 AS CHAR)) chequera", "chc_id", "ban_chequera c ", "cte_id=" & Sp_IdCuenta & " AND " & "(SELECT COUNT(che_id) FROM ban_cheques s WHERE (che_estado='SIN EMITIR' OR che_estado='NULO') AND (SELECT count(che_id)  FROM ban_cheques x WHERE che_estado='SIN EMITIR' AND x.chc_id=c.chc_id)>0 AND  s.chc_id=c.chc_id)>0"
    If CboChequeras.ListCount > 0 Then CboChequeras.ListIndex = 0
    
    LLenarCombo CboCuentasPropias, "CONCAT(cte_numero,' ',pla_nombre)", "cte_id", "ban_cta_cte c INNER JOIN con_plan_de_cuentas ON pla_id=ban_id", "cte_activo='SI' AND rut_emp='" & SP_Rut_Activo & "' AND cte_id NOT IN(" & Me.Sp_IdCuenta & ")"

    SaldoInicial
    
    Detalle
    
    ConsultaMovsApertura
End Sub





Private Sub SaldoInicial()
    'es el saldo final del mes anterior
    Dim Ip_Ano As Integer, Ip_Mes As Integer, Dp_Fecha As Date
    Ip_Mes = IG_Mes_Contable - 1
    Ip_Ano = IG_Ano_Contable
    If Ip_Mes < 1 Then
        Ip_Mes = 12
        Ip_Ano = Ip_Ano - 1
    End If
    
    'Para asiento de apertura debe seleccionar cte_saldo_inicial
    
    
    '2 Agosto 2014
    'Se haran dos saldos, inicial conciliacion e inicial contable, el inicial conciliacion
    'sera el que esta hasta ahora.
    Dp_Fecha = DateSerial(Ip_Ano, Ip_Mes + 1, 0)
    Dm_Fecha = DateSerial(IG_Ano_Contable, IG_Mes_Contable + 1, 0)
    Sql = "SELECT  SUM(IF(mov_tipo='INGRESO',mov_valor,0)) + SUM(IF(mov_tipo = 'CARGO',mov_valor *- 1,0)) inicial " & _
            "FROM ban_movimientos m " & _
            "WHERE m.cte_id =" & Sp_IdCuenta & " AND mov_fecha <='" & Fql(Dp_Fecha) & "' "
    Consulta RsTmp, Sql
    TxtSaldoInicial = 0
    If RsTmp.RecordCount > 0 Then TxtSaldoInicial = NumFormat(RsTmp!inicial)
    TxtSaldo = TxtSaldoInicial
    
    'Inicial contable excluyendo los mov de apertura
    Sql = "SELECT  SUM(IF(mov_tipo='INGRESO',mov_valor,0)) + SUM(IF(mov_tipo = 'CARGO',mov_valor *- 1,0)) inicial " & _
            "FROM ban_movimientos m " & _
            "WHERE mov_apertura='NO' AND m.cte_id =" & Sp_IdCuenta & " AND mov_fecha <='" & Fql(Dp_Fecha) & "' "
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then TxtInicialContable = NumFormat(RsTmp!inicial) Else TxtInicialContable = 0
    
    
End Sub


Private Sub DestinatariosConceptos()
    LLenarCombo CboDestinatarios, "des_nombre", "des_id", "ban_destinatarios", "des_activo='SI'", "des_nombre"
    LLenarCombo CboConcepto, "con_nombre", "con_id", "ban_conceptos", "con_activo='SI'", "con_nombre"
    CboDestinatarios.AddItem "Destinatario"
   ' CboDestinatarios.Sorted
    CboConcepto.AddItem "Concepto"
    CboDestinatarios.ListIndex = CboDestinatarios.ListCount - 1
    CboConcepto.ListIndex = CboConcepto.ListCount - 1

End Sub


Private Sub Detalle()
    Dim Sp_Fechas As String, Bp_Union As Boolean
    'Se mostraran las de apertura q no se han cobrado
    '26 Nov de 2013 (ver por que no estaba asi????????????)
    Bp_Union = False
    
    If Me.CboEstado.Text = "Todas las operaciones" Then
        Sp_Conciliados = Empty
    Else
        If CboEstado.ItemData(CboEstado.ListIndex) = 1 Then
            Sp_Conciliados = " AND mov_conciliado='NO' "
        Else
            Sp_Conciliados = " AND mov_conciliado='SI' "
            Bp_Union = True
        End If
    End If
    
    
    
    Sp_Fechas = " AND ((MONTH(mov_fecha)=" & IG_Mes_Contable & " AND YEAR(mov_fecha)=" & IG_Ano_Contable & ") /*mov_apertura='NO'*/ OR (mov_conciliado='NO'  AND mov_fecha<'" & Fql(Dm_Fecha + 1) & "') ) "
    
    Sql = "SELECT m.mov_id,m.cte_id,CAST(IF(mov_identificacion='CHEQUE',che_numero,m.mov_nro_comprobante) AS CHAR) nro," & _
            "m.mov_fecha,CONCAT(MID(m.mov_identificacion,1,12),' ', m.mov_detalle) detalle," & _
            "IF(m.mov_tipo='CARGO',m.mov_valor,0) pago,IF(m.mov_tipo='INGRESO',m.mov_valor,0) deposito,0," & _
            "mov_identificacion_id,mov_transferencia_a_cuenta_id,mov_de_tesoreria,mov_conciliado," & _
            "IF(mov_identificacion='CHEQUE' AND mov_conciliado='NO' AND (MONTH(mov_fecha)<>" & IG_Mes_Contable & " OR YEAR(mov_fecha)<>" & IG_Ano_Contable & "),'TENUE','NORMAL') color, " & _
            "mov_id_unico_entre_cuentas,m.pla_id,mov_apertura, " & _
            "c.che_fecha_cobro fcobro " & _
            "FROM ban_movimientos m " & _
            "LEFT JOIN ban_cheques c USING(mov_id) " & _
            "WHERE m.cte_id= " & Sp_IdCuenta & " " & Sp_Conciliados & " " & Sp_Fechas & _
            " GROUP BY mov_id "
    If Bp_Union Then
        Sp_Fechas = " AND mov_mes_conciliacion=" & IG_Mes_Contable & " AND mov_ano_conciliacion=" & IG_Ano_Contable & " "
        Sql = Sql & "UNION SELECT m.mov_id,m.cte_id,CAST(IF(mov_identificacion='CHEQUE',che_numero,m.mov_nro_comprobante) AS CHAR) nro," & _
            "m.mov_fecha,CONCAT(MID(m.mov_identificacion,1,12),' ', m.mov_detalle) detalle," & _
            "IF(m.mov_tipo='CARGO',m.mov_valor,0) pago,IF(m.mov_tipo='INGRESO',m.mov_valor,0) deposito,0," & _
            "mov_identificacion_id,mov_transferencia_a_cuenta_id,mov_de_tesoreria,mov_conciliado," & _
            "IF(mov_identificacion='CHEQUE' AND mov_conciliado='NO' AND (MONTH(mov_fecha)<>" & IG_Mes_Contable & " OR YEAR(mov_fecha)<>" & IG_Ano_Contable & "),'TENUE','NORMAL') color, " & _
            "mov_id_unico_entre_cuentas,m.pla_id,mov_apertura, " & _
            "c.che_fecha_cobro fcobro " & _
            "FROM ban_movimientos m " & _
            "LEFT JOIN ban_cheques c USING(mov_id) " & _
            "WHERE m.cte_id= " & Sp_IdCuenta & " " & Sp_Conciliados & " " & Sp_Fechas & _
            " GROUP BY mov_id "
    End If
            
    Sis_BarraProgreso.Show 1
    'Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvDetalle, True, True, True, False
    For i = 1 To LvDetalle.ListItems.Count
        If LvDetalle.ListItems(i).SubItems(12) = "TENUE" Then
            
            For X = 1 To LvDetalle.ColumnHeaders.Count - 2
                LvDetalle.ListItems(i).ListSubItems(X).ForeColor = vbGreen
            Next
        End If
    Next
    
    
    CalculaSaldo
    DestinatariosConceptos
End Sub
Private Sub CalculaSaldo()
    Dim Lp_Cargo As Long, Lp_Abono As Long, Lp_Actual
    If LvDetalle.ListItems.Count = 0 Then Exit Sub
    Lp_Cargo = 0
    Lp_Abono = 0
    Lp_Actual = CDbl(TxtSaldoInicial)
    For i = 1 To LvDetalle.ListItems.Count
        If LvDetalle.ListItems(i).SubItems(12) = "NORMAL" Then
            Lp_Cargo = Lp_Cargo + CDbl(LvDetalle.ListItems(i).SubItems(5))
            Lp_Abono = Lp_Abono + CDbl(LvDetalle.ListItems(i).SubItems(6))
            LvDetalle.ListItems(i).SubItems(7) = NumFormat(CDbl(TxtSaldoInicial) + Lp_Abono - Lp_Cargo)
            Lp_Actual = CDbl(LvDetalle.ListItems(i).SubItems(7))
            If Lp_Actual < 0 Then
                LvDetalle.ListItems(i).ListSubItems(7).ForeColor = vbRed
            Else
                LvDetalle.ListItems(i).ListSubItems(7).ForeColor = vbBlack
            End If
        End If
    Next
    TxtSaldo.Tag = Lp_Actual
    If Len(txtInfoApertura) > 0 Then
        TxtSaldo = "Saldo Conciliacion ($)" & NumFormat(Lp_Actual)
    Else
        TxtSaldo = "Saldo Contable Final ($)" & NumFormat(Lp_Actual)
    End If
End Sub
Private Sub LvCue_DblClick()
    If LvCue.SelectedItem Is Nothing Then Exit Sub
    If CmdOkCuenta.Enabled = False Then Exit Sub
    Busca_Id_Combo CboCuenta, Val(LvCue.SelectedItem.Text)
    txtValor = LvCue.SelectedItem.SubItems(2)
    LvCue.ListItems.Remove LvCue.SelectedItem.Index
    SumaG
    CboCuenta.SetFocus
End Sub

Private Sub LvDetalle_Click()
    TxtChNro.Visible = False
    CargaMovimiento
End Sub

Private Sub LvDetalle_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
     TxtChNro.Visible = False
     ordListView ColumnHeader, Me, LvDetalle
     CalculaSaldo
End Sub

Private Sub CargaMovimiento()
 
    Dim Lp_IdUnico As Long, Ip_IdMov As Integer, Sp_Ap As String * 2
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    LvCue.ListItems.Clear
    CmdOkCuenta.Enabled = False
    TxtChNro.Tag = 0
    Lp_IdUnico = LvDetalle.SelectedItem
    Ip_IdMov = LvDetalle.SelectedItem.SubItems(8)
    Sp_Ap = LvDetalle.SelectedItem.SubItems(15)
    StabMov.Tag = Lp_IdUnico
    
    Me.StabMov.Tab = Ip_IdMov - 1
    
    Sql = "SELECT * " & _
          "FROM ban_movimientos " & _
          "WHERE mov_id=" & Lp_IdUnico
    Consulta RsTmp3, Sql
    If RsTmp3.RecordCount = 0 Then Exit Sub
    Busca_Id_Combo CboCuenta, Val(RsTmp3!pla_id)
    'Busca_Id_Combo CboCuenta, Val(RsTmp2!pla_id)
    Select Case RsTmp3!mov_identificacion_id
        
        Case 1 'Cheque
            
            If Sp_Ap = "SI" Then
                ChkApertura.Value = 1
                LaApertura 1
            Else
                ChkApertura.Value = 0
                LaApertura 0
            End If
            sql2 = "SELECT * " & _
                  "FROM ban_cheques " & _
                  "WHERE mov_id=" & Lp_IdUnico
            Consulta RsTmp2, sql2
            If RsTmp2.RecordCount = 0 Then Exit Sub
            DtEmision = RsTmp2!che_fecha_emision
            DtCobro = RsTmp2!che_fecha_cobro
            TxtValorCheque = NumFormat(RsTmp2!che_valor)
            TxtValorCheque.Tag = RsTmp2!che_id
            CboDestinatarios.Text = RsTmp2!che_destinatario
            CboConcepto.Text = RsTmp2!che_concepto
            
            TxtChNro.Visible = True
            TxtChNro = LvDetalle.SelectedItem.SubItems(2)
            TxtChNro.Tag = LvDetalle.SelectedItem
            TxtValorCheque.Enabled = False
            'Cargar cuenta(s)
            Sql = "SELECT m.pla_id,p.pla_nombre,dcu_valor " & _
                  "FROM com_con_multicuenta m " & _
                  "INNER JOIN con_plan_de_cuentas p USING(pla_id) " & _
                  "WHERE dcu_tipo='CHEQUE' AND m.id_unico IN(" & TxtValorCheque.Tag & ") "
            Consulta RsTmp, Sql
            LLenar_Grilla RsTmp, Me, LvCue, False, True, True, False
            SumaG
            CmdOkCuenta.Enabled = False
            ChkApertura.Enabled = False
            
        Case 2 ' Deposito
            If RsTmp3!mov_apertura = "SI" Then
                ChkApertura.Value = 1
                LaApertura 1
            Else
                ChkApertura.Value = 0
                LaApertura 0
            End If
            DtDeposito = RsTmp3!mov_fecha
            TxtValorDeposito = RsTmp3!mov_valor
            Me.TxtNroComprobante = RsTmp3!mov_nro_comprobante
            Busca_Id_Combo CboDestinatarios, RsTmp3!des_id
            Busca_Id_Combo CboConcepto, RsTmp3!con_id
            TxtValorDeposito.Enabled = False
             ChkApertura.Enabled = False
             'Cargar cuenta(s)
            Sql = "SELECT m.pla_id,p.pla_nombre,dcu_valor " & _
                  "FROM com_con_multicuenta m " & _
                  "INNER JOIN con_plan_de_cuentas p USING(pla_id) " & _
                  "WHERE m.id_unico=" & Me.LvDetalle.SelectedItem & " AND dcu_tipo='DEPOSITO'"
            Consulta RsTmp, Sql
            LLenar_Grilla RsTmp, Me, LvCue, False, True, True, False
            SumaG
        Case 3 'Transferencia
            Me.DtTransferencia = RsTmp3!mov_fecha
            TxtValorTransferencia = RsTmp3!mov_valor
            Busca_Id_Combo CboDestinatarios, RsTmp3!des_id
            Busca_Id_Combo CboConcepto, RsTmp3!con_id
            OptTerceros.Value = True
            CboCuentasPropias.Enabled = False
            If Val(LvDetalle.SelectedItem.SubItems(9)) > 0 Then
                CboCuentasPropias.Enabled = True
                OptPropias.Value = True
                Busca_Id_Combo Me.CboCuentasPropias, Val(LvDetalle.SelectedItem.SubItems(9))
            End If
            TxtValorTransferencia.Enabled = False
            'Cargar cuenta(s)
            Sql = "SELECT m.pla_id,p.pla_nombre,dcu_valor " & _
                  "FROM com_con_multicuenta m " & _
                  "INNER JOIN con_plan_de_cuentas p USING(pla_id) " & _
                  "WHERE m.id_unico=" & Me.LvDetalle.SelectedItem & " AND dcu_tipo='TRANSFERENCIA'"
            Consulta RsTmp, Sql
            LLenar_Grilla RsTmp, Me, LvCue, False, True, True, False
            SumaG
        Case 4 '
            Me.DtCargo = RsTmp3!mov_fecha
            TxtValorCargo = RsTmp3!mov_valor
            Me.TxtNroComprobanteCargo = RsTmp3!mov_nro_comprobante
            Busca_Id_Combo CboDestinatarios, RsTmp3!des_id
            Busca_Id_Combo CboConcepto, RsTmp3!con_id
            TxtValorCargo.Enabled = False
            'Cargar cuenta(s)
            Sql = "SELECT m.pla_id,p.pla_nombre,dcu_valor " & _
                  "FROM com_con_multicuenta m " & _
                  "INNER JOIN con_plan_de_cuentas p USING(pla_id) " & _
                  "WHERE m.id_unico=" & Me.LvDetalle.SelectedItem & " AND dcu_tipo='CARGO'"
            Consulta RsTmp, Sql
            LLenar_Grilla RsTmp, Me, LvCue, False, True, True, False
            SumaG
            
            
    End Select
    CmdOkCuenta.Enabled = False
End Sub




Private Sub LvDetalle_KeyDown(KeyCode As Integer, Shift As Integer)
    '�If LvDetalle.SelectedItem Is Nothing Then Exit Sub

End Sub

Private Sub LvDetalle_KeyUp(KeyCode As Integer, Shift As Integer)
    CargaMovimiento
End Sub

Private Sub OptPropias_Click()
    CboCuentasPropias.Enabled = True
    CmdOkCuenta.Enabled = False
End Sub
Private Sub OptTerceros_Click()
    Me.CboCuentasPropias.Enabled = False
    CmdOkCuenta.Enabled = True
End Sub

Private Sub StabMov_Click(PreviousTab As Integer)
    If StabMov.Tab <> 2 Or (StabMov.Tab = 2 And OptTerceros.Value) Then
        CmdOkCuenta.Enabled = True
    Else
        CmdOkCuenta.Enabled = False
    End If
    
    If StabMov.Tab = 0 Or StabMov.Tab = 1 Then
        ChkApertura.Visible = True
        If ChkApertura.Value = 1 Then LaApertura 1
        If ChkApertura.Value = 0 Then LaApertura 0
        
    Else
        ChkApertura.Visible = False
        CboCuenta.Enabled = True
        TxtNroCuenta.Enabled = True
        Me.CmdCuenta.Enabled = True
    End If
    If PreviousTab <> StabMov.Tab Then CmdNuevo_Click
    
End Sub

Private Sub StabMov_LostFocus()
    StabMov.ToolTipText = ""
End Sub
Private Sub TxtNroComprobante_GotFocus()
    On Error Resume Next
     En_Foco Me.ActiveControl
End Sub
Private Sub TxtNroComprobante_LostFocus()
    On Error Resume Next
    If CboCuenta.Visible Then CboCuenta.SetFocus
End Sub

Private Sub TxtNroComprobanteCargo_GotFocus()
    On Error Resume Next
    En_Foco Me.ActiveControl
End Sub



Private Sub TxtNroCuenta_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
End Sub

Private Sub TxtNroCuenta_Validate(Cancel As Boolean)
    Busca_Id_Combo CboCuenta, Val(TxtNroCuenta)
    txtValor.SetFocus
End Sub

Private Sub TxtValor_GotFocus()
    En_Foco txtValor
End Sub

Private Sub TxtValor_KeyPress(KeyAscii As Integer)
'    KeyAscii = SoloNumeros(KeyAscii)
End Sub

Private Sub TxtValorCargo_GotFocus()
    On Error Resume Next
    En_Foco Me.ActiveControl
End Sub
Private Sub TxtValorCheque_GotFocus()
    On Error Resume Next
    En_Foco Me.ActiveControl
End Sub
Private Sub TxtValorCheque_LostFocus()
    On Error Resume Next
    txtValor = TxtValorCheque
    If CboCuenta.Visible Then CboCuenta.SetFocus
End Sub
Private Sub TxtValorCheque_Validate(Cancel As Boolean)
    If Val(TxtValorCheque) = 0 Then TxtValorCheque = 0
    TxtValorCheque = NumFormat(TxtValorCheque)
End Sub
Private Sub TxtValorDeposito_GotFocus()
    On Error Resume Next
    En_Foco Me.ActiveControl
End Sub
Private Sub TxtValorTransferencia_GotFocus()
    On Error Resume Next
    En_Foco Me.ActiveControl
End Sub
Private Sub Timer1_Timer()
    On Error Resume Next
    Me.SetFocus
    Timer1.Enabled = False
End Sub


Private Sub ConsultaMovsApertura()
    'Fecha          :2 -Ago- 2014
    'Author         :Alvaro Vega
    'Tema           :Consultar movimientos de apertura, la suma
    '                   de los no conciliados
    'Revisado por   :Mario Veloso
    
    Sql = "SELECT IFNULL(SUM(mov_valor),0) valor " & _
            "FROM ban_movimientos m " & _
            "JOIN ban_cheques c ON m.mov_id=c.mov_id " & _
            "JOIN ban_cta_cte e ON m.cte_id=e.cte_id " & _
            "WHERE e.rut_emp='" & SP_Rut_Activo & "' AND mov_apertura='SI'"
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        If RsTmp!Valor > 0 Then
            'Mostrar la info de estos valores
            txtInfoApertura = ":" & NumFormat(RsTmp!Valor)
            CalculaSaldoConMovApertura
        Else
            txtInfoApertura = ""
        
        End If
        
    End If

End Sub

Private Sub CalculaSaldoConMovApertura()
    'Fecha          :2 -Ago- 2014
    'Author         :Alvaro Vega
    'Tema           :Calcula el saldo contable excluyendo los mov. de apertura
    'Revisado por   :Mario Veloso



    Dim Lp_Cargo As Long, Lp_Abono As Long, Lp_Actual
    If LvDetalle.ListItems.Count = 0 Then Exit Sub
    Lp_Cargo = 0
    Lp_Abono = 0
    Lp_Actual = CDbl(TxtInicialContable)
    For i = 1 To LvDetalle.ListItems.Count
        If LvDetalle.ListItems(i).SubItems(12) = "NORMAL" And LvDetalle.ListItems(i).SubItems(15) = "NO" Then
            Lp_Cargo = Lp_Cargo + CDbl(LvDetalle.ListItems(i).SubItems(5))
            Lp_Abono = Lp_Abono + CDbl(LvDetalle.ListItems(i).SubItems(6))
            LvDetalle.ListItems(i).SubItems(7) = NumFormat(CDbl(TxtInicialContable) + Lp_Abono - Lp_Cargo)
            Lp_Actual = CDbl(LvDetalle.ListItems(i).SubItems(7))
            If Lp_Actual < 0 Then
                LvDetalle.ListItems(i).ListSubItems(7).ForeColor = vbRed
            Else
                LvDetalle.ListItems(i).ListSubItems(7).ForeColor = vbBlack
            End If
        End If
    Next
    txtInfoApertura = "Saldo Contable:" & NumFormat(Lp_Actual)
    'txtSaldo.Tag = Lp_Actual
    'txtSaldo = "Saldo Contable Final ($)" & NumFormat(Lp_Actual)
End Sub

