VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{DA729E34-689F-49EA-A856-B57046630B73}#1.0#0"; "progressbar-xp.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form Ban_OperacionesEnCtasCtes 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Informe de Operaciones en Cuentas Corrientes"
   ClientHeight    =   8565
   ClientLeft      =   -120
   ClientTop       =   2385
   ClientWidth     =   14070
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8565
   ScaleWidth      =   14070
   Begin VB.Frame FraProgreso 
      Caption         =   "Progreso exportaci�n"
      Height          =   795
      Left            =   1800
      TabIndex        =   22
      Top             =   6150
      Visible         =   0   'False
      Width           =   11430
      Begin Proyecto2.XP_ProgressBar BarraProgreso 
         Height          =   330
         Left            =   150
         TabIndex        =   23
         Top             =   315
         Width           =   11130
         _ExtentX        =   19632
         _ExtentY        =   582
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BrushStyle      =   0
         Color           =   16777088
         Scrolling       =   1
         ShowText        =   -1  'True
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Informe"
      Height          =   5820
      Left            =   210
      TabIndex        =   19
      Top             =   2280
      Width           =   13605
      Begin VB.CommandButton cmdExportar 
         Caption         =   "Exportar Lista a Excel"
         Height          =   345
         Left            =   180
         TabIndex        =   21
         ToolTipText     =   "Exportar"
         Top             =   5415
         Width           =   2055
      End
      Begin MSComctlLib.ListView LvDetalle 
         Height          =   4980
         Left            =   195
         TabIndex        =   20
         Top             =   390
         Width           =   13215
         _ExtentX        =   23310
         _ExtentY        =   8784
         View            =   3
         LabelWrap       =   0   'False
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   7
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "N109"
            Text            =   "Id Unico"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   1
            Object.Tag             =   "N109"
            Text            =   "Numero"
            Object.Width           =   2646
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "F1000"
            Text            =   "Fecha Emisi�n"
            Object.Width           =   2646
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Object.Tag             =   "T3000"
            Text            =   "Movimiento"
            Object.Width           =   5997
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Object.Tag             =   "T2500"
            Text            =   "Beneficiario"
            Object.Width           =   5997
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   5
            Object.Tag             =   "N100"
            Text            =   "Valor"
            Object.Width           =   2646
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   6
            Object.Tag             =   "T1000"
            Text            =   "Fecha COBRO"
            Object.Width           =   2646
         EndProperty
      End
   End
   Begin VB.CommandButton CmdSalir 
      Cancel          =   -1  'True
      Caption         =   "&Retornar"
      Height          =   315
      Left            =   12330
      TabIndex        =   18
      ToolTipText     =   "Salir"
      Top             =   8190
      Width           =   1485
   End
   Begin VB.Frame Frame4 
      Caption         =   "Mes y A�o"
      Height          =   1830
      Left            =   4905
      TabIndex        =   4
      Top             =   210
      Width           =   2970
      Begin VB.ComboBox comMes 
         Height          =   315
         ItemData        =   "Ban_OperacionesEnCtasCtes.frx":0000
         Left            =   135
         List            =   "Ban_OperacionesEnCtasCtes.frx":0002
         Style           =   2  'Dropdown List
         TabIndex        =   8
         Top             =   450
         Width           =   1575
      End
      Begin VB.ComboBox ComAno 
         Height          =   315
         Left            =   1695
         Style           =   2  'Dropdown List
         TabIndex        =   7
         Top             =   450
         Width           =   1215
      End
      Begin VB.CheckBox ChkFecha 
         Height          =   225
         Left            =   210
         TabIndex        =   5
         Top             =   1035
         Width           =   225
      End
      Begin MSComCtl2.DTPicker DtDesde 
         CausesValidation=   0   'False
         Height          =   285
         Left            =   480
         TabIndex        =   6
         Top             =   1005
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   503
         _Version        =   393216
         Enabled         =   0   'False
         Format          =   273416193
         CurrentDate     =   41001
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
         Height          =   255
         Left            =   120
         OleObjectBlob   =   "Ban_OperacionesEnCtasCtes.frx":0004
         TabIndex        =   9
         Top             =   255
         Width           =   375
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel5 
         Height          =   255
         Left            =   1725
         OleObjectBlob   =   "Ban_OperacionesEnCtasCtes.frx":0068
         TabIndex        =   10
         Top             =   255
         Width           =   375
      End
      Begin MSComCtl2.DTPicker DtHasta 
         Height          =   285
         Left            =   1680
         TabIndex        =   11
         Top             =   1005
         Width           =   1200
         _ExtentX        =   2117
         _ExtentY        =   503
         _Version        =   393216
         Enabled         =   0   'False
         Format          =   273416193
         CurrentDate     =   41001
      End
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   255
      OleObjectBlob   =   "Ban_OperacionesEnCtasCtes.frx":00CC
      Top             =   6840
   End
   Begin VB.Timer Timer1 
      Interval        =   10
      Left            =   1065
      Top             =   6855
   End
   Begin VB.Frame Frame3 
      Caption         =   "Seleccione Cuenta y Periodo"
      Height          =   1830
      Left            =   195
      TabIndex        =   0
      Top             =   210
      Width           =   13620
      Begin VB.Frame FramF 
         Caption         =   "Por cobrar en"
         Height          =   600
         Left            =   10455
         TabIndex        =   24
         ToolTipText     =   "Esta opcion ignora la fecha de emision"
         Top             =   165
         Visible         =   0   'False
         Width           =   2985
         Begin VB.CheckBox Chk 
            Height          =   195
            Left            =   105
            TabIndex        =   25
            Top             =   285
            Width           =   240
         End
         Begin MSComCtl2.DTPicker DtInicioC 
            CausesValidation=   0   'False
            Height          =   285
            Left            =   450
            TabIndex        =   26
            Top             =   240
            Width           =   1200
            _ExtentX        =   2117
            _ExtentY        =   503
            _Version        =   393216
            Enabled         =   0   'False
            Format          =   273416193
            CurrentDate     =   41001
         End
         Begin MSComCtl2.DTPicker DtFinC 
            Height          =   285
            Left            =   1650
            TabIndex        =   27
            Top             =   240
            Width           =   1200
            _ExtentX        =   2117
            _ExtentY        =   503
            _Version        =   393216
            Enabled         =   0   'False
            Format          =   273416193
            CurrentDate     =   41001
         End
      End
      Begin VB.ComboBox CboEstado 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         ItemData        =   "Ban_OperacionesEnCtasCtes.frx":0300
         Left            =   7845
         List            =   "Ban_OperacionesEnCtasCtes.frx":0302
         Style           =   2  'Dropdown List
         TabIndex        =   16
         Top             =   960
         Width           =   2580
      End
      Begin VB.TextBox TxtBusqueda 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   10410
         TabIndex        =   15
         ToolTipText     =   "Ingrese un Nro de cheque, puede buscar varios separandolos por comas"
         Top             =   960
         Width           =   3015
      End
      Begin VB.ComboBox CboTipoMov 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         ItemData        =   "Ban_OperacionesEnCtasCtes.frx":0304
         Left            =   7845
         List            =   "Ban_OperacionesEnCtasCtes.frx":0306
         Style           =   2  'Dropdown List
         TabIndex        =   12
         Top             =   405
         Width           =   2580
      End
      Begin VB.CommandButton cmdConsultar 
         Caption         =   "&Consultar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   420
         Left            =   12135
         TabIndex        =   2
         Top             =   1335
         Width           =   1365
      End
      Begin VB.ComboBox CboCuenta 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         ItemData        =   "Ban_OperacionesEnCtasCtes.frx":0308
         Left            =   180
         List            =   "Ban_OperacionesEnCtasCtes.frx":030A
         Style           =   2  'Dropdown List
         TabIndex        =   1
         Top             =   600
         Width           =   4530
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkCuenta 
         Height          =   180
         Left            =   195
         OleObjectBlob   =   "Ban_OperacionesEnCtasCtes.frx":030C
         TabIndex        =   3
         Top             =   420
         Width           =   1650
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   180
         Left            =   7860
         OleObjectBlob   =   "Ban_OperacionesEnCtasCtes.frx":037A
         TabIndex        =   13
         Top             =   195
         Width           =   1650
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   210
         Left            =   10425
         OleObjectBlob   =   "Ban_OperacionesEnCtasCtes.frx":03FC
         TabIndex        =   14
         Top             =   765
         Width           =   1650
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   180
         Left            =   7860
         OleObjectBlob   =   "Ban_OperacionesEnCtasCtes.frx":0474
         TabIndex        =   17
         Top             =   750
         Width           =   1650
      End
   End
End
Attribute VB_Name = "Ban_OperacionesEnCtasCtes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub CboTipoMov_Click()
    If CboTipoMov.ListIndex = -1 Then Exit Sub
    FramF.Visible = False
    If CboTipoMov.ItemData(CboTipoMov.ListIndex) = 1 Then Me.FramF.Visible = True

End Sub

Private Sub Chk_Click()
    If Chk.Value = 1 Then
        DtInicioC.Enabled = True
        DtFinC.Enabled = True
    Else
        DtInicioC.Enabled = False
        DtFinC.Enabled = False
    End If
End Sub

Private Sub ChkFecha_Click()
    If ChkFecha.Value = 1 Then
        DtDesde.Enabled = True
        DtHasta.Enabled = True
        comMes.Enabled = False
        ComAno.Enabled = False
    Else
        DtDesde.Enabled = False
        DtHasta.Enabled = False
        comMes.Enabled = True
        ComAno.Enabled = True
    End If
End Sub

Private Sub cmdConsultar_Click()
    Dim Sp_Tipo As String, Sp_Fechas As String, Sp_NrosCheques As String, Sp_Conciliados As String
    
    
    If CboCuenta.ListIndex = -1 Then
        MsgBox "Debe seleccionar una cuenta...", vbInformation + vbOKOnly
        CboCuenta.SetFocus
        Exit Sub
    End If
    
    If ChkFecha.Value = 1 Then
        Sp_Fecha = " AND mov_fecha BETWEEN '" & Fql(DtDesde) & "' AND '" & Fql(DtHasta) & "' "
    Else
        Sp_Fecha = " AND MONTH(mov_fecha)=" & comMes.ItemData(comMes.ListIndex) & " AND YEAR(mov_fecha)=" & ComAno.Text & " "
    End If
    
    If CboTipoMov.Text = "TODOS" Then
        Sp_Tipo = Empty
    Else
        Sp_Tipo = " AND mov_identificacion_id=" & CboTipoMov.ItemData(CboTipoMov.ListIndex) & " "
    End If
    
    If Len(TxtBusqueda) = 0 Then
        Sp_NrosCheques = Empty
    Else
        Sp_NrosCheques = " AND che_numero=" & TxtBusqueda & " "
    End If
    
    If Me.CboEstado.Text = "Todas las operaciones" Then
        Sp_Conciliados = Empty
    Else
        If CboEstado.ItemData(CboEstado.ListIndex) = 1 Then
            Sp_Conciliados = " AND mov_conciliado='NO' "
        Else
            Sp_Conciliados = " AND mov_conciliado='SI' "
        End If
    End If
    
    
    
    If Me.FramF.Visible = False Or (FramF.Visible = True And Chk.Value = 0) Then
        Sql = "SELECT m.mov_id,IF(mov_identificacion_id=1,(SELECT che_numero FROM ban_cheques b WHERE b.mov_id=m.mov_id " & Sp_NrosCheques & "),0) numero," & _
                "mov_fecha,mov_identificacion,mov_detalle,mov_valor *IF(mov_tipo='CARGO',-1,1) valor," & _
                "IF(mov_identificacion_id=1,(SELECT CAST(DATE_FORMAT(che_fecha_cobro,'%d-%m-%Y') AS CHAR) FROM ban_cheques b WHERE b.mov_id=m.mov_id " & Sp_NrosCheques & "),'') fecha_cobro " & _
            "FROM ban_movimientos m " & _
            "/*INNER JOIN ban_conceptos c ON m.con_id=c.con_id  " & _
            "INNER JOIN ban_destinatarios d ON m.des_id=d.des_id */" & _
            "WHERE m.cte_id=" & CboCuenta.ItemData(CboCuenta.ListIndex) & Sp_Tipo & Sp_Fecha & Sp_Conciliados
            Consulta RsTmp, Sql
    Else
        Sp_Fecha = " AND che_fecha_cobro BETWEEN '" & Fql(DtInicioC) & "' AND '" & Fql(DtFinC) & "' "
        Sql = "SELECT m.mov_id,IF(mov_identificacion_id=1,(SELECT che_numero FROM ban_cheques b WHERE b.mov_id=m.mov_id " & Sp_NrosCheques & "),0) numero," & _
                "mov_fecha,mov_identificacion,mov_detalle,mov_valor *IF(mov_tipo='CARGO',-1,1) valor," & _
                "IF(mov_identificacion_id=1,(SELECT CAST(DATE_FORMAT(che_fecha_cobro,'%d-%m-%Y') AS CHAR) FROM ban_cheques b WHERE b.mov_id=m.mov_id " & Sp_NrosCheques & "),'') fecha_cobro " & _
            "FROM ban_movimientos m " & _
            "/*INNER JOIN ban_conceptos c ON m.con_id=c.con_id  " & _
            "INNER JOIN ban_destinatarios d ON m.des_id=d.des_id */ " & _
            "INNER JOIN ban_cheques s ON m.mov_id=s.mov_id " & _
            "WHERE m.cte_id=" & CboCuenta.ItemData(CboCuenta.ListIndex) & Sp_Tipo & Sp_Fecha & Sp_Conciliados
            Consulta RsTmp, Sql
    
    End If
    LLenar_Grilla RsTmp, Me, LvDetalle, False, True, True, False
    
    For i = 1 To LvDetalle.ListItems.Count
        If Val(LvDetalle.ListItems(i).SubItems(5)) < 0 Then
            For X = 1 To LvDetalle.ColumnHeaders.Count - 1
                LvDetalle.ListItems(i).ListSubItems(X).ForeColor = vbRed
            Next
        End If
    Next
End Sub

Private Sub CmdExportar_Click()
    Dim tit(2) As String
    If LvDetalle.ListItems.Count = 0 Then Exit Sub
    FraProgreso.Visible = True
    tit(0) = "OPERACION EN CUENTA"
    tit(1) = CboCuenta.Text
    tit(2) = "TIPO MOVIMIENTO: " & CboTipoMov.Text
    ExportarNuevo LvDetalle, tit, Me, BarraProgreso
    FraProgreso.Visible = False
End Sub

Private Sub cmdSalir_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    Aplicar_skin Me
    Centrar Me
    For i = 1 To 12
        comMes.AddItem UCase(MonthName(i, False))
        comMes.ItemData(comMes.ListCount - 1) = i
    Next
    Busca_Id_Combo comMes, Val(IG_Mes_Contable)
    LLenaYears ComAno, 2010
    'For i = Year(Date) To Year(Date) - 1 Step -1
    '    ComAno.AddItem i
    '    ComAno.ItemData(ComAno.ListCount - 1) = i
    'Next
    'Busca_Id_Combo ComAno, Val(IG_Ano_Contable)
    LLenarCombo CboCuenta, "CONCAT(cte_numero,' ',pla_nombre)", "cte_id", "ban_cta_cte c INNER JOIN con_plan_de_cuentas ON pla_id=ban_id", "cte_activo='SI' AND rut_emp='" & SP_Rut_Activo & "'"

'    LLenarCombo CboCuenta, "CONCAT(cte_numero,' ',ban_nombre)", "cte_id", "ban_cta_cte c INNER JOIN par_bancos USING(ban_id)", "cte_activo='SI' AND rut_emp='" & SP_Rut_Activo & "'"
    If CboCuenta.ListCount > 0 Then
        CboCuenta.ListIndex = 0
    Else
    End If
    
    CboTipoMov.AddItem "CHEQUES"
    CboTipoMov.ItemData(CboTipoMov.ListCount - 1) = 1
    CboTipoMov.AddItem "DEPOSITOS"
    CboTipoMov.ItemData(CboTipoMov.ListCount - 1) = 2
    CboTipoMov.AddItem "TRANSFERENCIAS"
    CboTipoMov.ItemData(CboTipoMov.ListCount - 1) = 3
    CboTipoMov.AddItem "CARGOS"
    CboTipoMov.ItemData(CboTipoMov.ListCount - 1) = 4
    CboTipoMov.AddItem "TODOS"
    CboTipoMov.ItemData(CboTipoMov.ListCount - 1) = 5
    CboTipoMov.ListIndex = CboTipoMov.ListCount - 1
    
    CboEstado.AddItem "Operaciones sin Conciliar"
    CboEstado.ItemData(CboEstado.ListCount - 1) = 1
    CboEstado.AddItem "Operaciones conciliadas"
    CboEstado.ItemData(CboEstado.ListCount - 1) = 2
    CboEstado.AddItem "Todas las operaciones"
    CboEstado.ItemData(CboEstado.ListCount - 1) = 3
    CboEstado.ListIndex = CboEstado.ListCount - 1
End Sub
Private Sub LvDetalle_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    ordListView ColumnHeader, Me, LvDetalle
End Sub

Private Sub Timer1_Timer()
    On Error Resume Next
    CboCuenta.SetFocus
    Timer1.Enabled = False
End Sub
Private Sub TxtBusqueda_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
End Sub

Private Sub TxtBusqueda_Validate(Cancel As Boolean)
    If Val(TxtBusqueda) = 0 Then TxtBusqueda = ""
End Sub
