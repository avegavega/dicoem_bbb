VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{DA729E34-689F-49EA-A856-B57046630B73}#1.0#0"; "progressbar-xp.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form Ban_AsientosContables 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Asiento Contable Banco - Originado de Banco y Tesorer�a"
   ClientHeight    =   7110
   ClientLeft      =   390
   ClientTop       =   735
   ClientWidth     =   14280
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7110
   ScaleWidth      =   14280
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Frame3 
      Caption         =   "Seleccione Cuenta y Periodo"
      Height          =   1125
      Left            =   450
      TabIndex        =   11
      Top             =   285
      Width           =   13380
      Begin VB.ComboBox CboCuenta 
         Appearance      =   0  'Flat
         Height          =   315
         ItemData        =   "Ban_AsientosContables.frx":0000
         Left            =   195
         List            =   "Ban_AsientosContables.frx":0002
         Style           =   2  'Dropdown List
         TabIndex        =   15
         Top             =   600
         Width           =   5100
      End
      Begin VB.ComboBox CboMesContable 
         Height          =   315
         ItemData        =   "Ban_AsientosContables.frx":0004
         Left            =   5310
         List            =   "Ban_AsientosContables.frx":0006
         Style           =   2  'Dropdown List
         TabIndex        =   14
         Top             =   600
         Width           =   2715
      End
      Begin VB.ComboBox CboAnoContable 
         Height          =   315
         ItemData        =   "Ban_AsientosContables.frx":0008
         Left            =   8010
         List            =   "Ban_AsientosContables.frx":000A
         Style           =   2  'Dropdown List
         TabIndex        =   13
         Top             =   600
         Width           =   2370
      End
      Begin VB.CommandButton cmdConsultar 
         Caption         =   "&Consultar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   420
         Left            =   11880
         TabIndex        =   12
         Top             =   540
         Width           =   1365
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkCuenta 
         Height          =   180
         Left            =   195
         OleObjectBlob   =   "Ban_AsientosContables.frx":000C
         TabIndex        =   16
         Top             =   420
         Width           =   1650
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   195
         Left            =   5295
         OleObjectBlob   =   "Ban_AsientosContables.frx":007A
         TabIndex        =   17
         Top             =   405
         Width           =   1320
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   195
         Left            =   8040
         OleObjectBlob   =   "Ban_AsientosContables.frx":00DE
         TabIndex        =   18
         Top             =   420
         Width           =   1320
      End
   End
   Begin VB.Frame FraProgreso 
      Caption         =   "Progreso exportaci�n"
      Height          =   795
      Left            =   480
      TabIndex        =   9
      Top             =   6180
      Visible         =   0   'False
      Width           =   11430
      Begin Proyecto2.XP_ProgressBar BarraProgreso 
         Height          =   330
         Left            =   150
         TabIndex        =   10
         Top             =   315
         Width           =   11130
         _ExtentX        =   19632
         _ExtentY        =   582
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BrushStyle      =   0
         Color           =   16777088
         Scrolling       =   1
         ShowText        =   -1  'True
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "ASIENTO  CONTABLE POR  DEPOSITOS / TRANSFERENCIAS RECIBIDAS"
      Height          =   4575
      Left            =   7200
      TabIndex        =   5
      Top             =   1560
      Width           =   6660
      Begin VB.CommandButton CmdExportar2 
         Caption         =   "Exportar"
         Height          =   390
         Left            =   135
         TabIndex        =   6
         ToolTipText     =   "Exporta a excel"
         Top             =   4065
         Width           =   1440
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   210
         Left            =   6255
         OleObjectBlob   =   "Ban_AsientosContables.frx":0142
         TabIndex        =   7
         Top             =   4230
         Width           =   270
      End
      Begin MSComctlLib.ListView LvDeposito 
         Height          =   3705
         Left            =   135
         TabIndex        =   8
         Top             =   330
         Width           =   6405
         _ExtentX        =   11298
         _ExtentY        =   6535
         View            =   3
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   4
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "T500"
            Text            =   "Cod"
            Object.Width           =   882
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "T3000"
            Text            =   "CUENTAS"
            Object.Width           =   4762
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   2
            Key             =   "debe"
            Object.Tag             =   "N100"
            Text            =   "DEBE"
            Object.Width           =   2381
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   3
            Key             =   "haber"
            Object.Tag             =   "N100"
            Text            =   "HABER"
            Object.Width           =   2381
         EndProperty
      End
   End
   Begin VB.CommandButton cmdSalir 
      Cancel          =   -1  'True
      Caption         =   "&Retornar"
      Height          =   405
      Left            =   12465
      TabIndex        =   4
      ToolTipText     =   "Salir"
      Top             =   6465
      Width           =   1350
   End
   Begin VB.Frame Frame2 
      Caption         =   "ASIENTO  CONTABLE POR GIROS  / CARGOS / CHEQUES/TRANSF. REALIZADAS"
      Height          =   4575
      Left            =   465
      TabIndex        =   0
      Top             =   1560
      Width           =   6660
      Begin VB.CommandButton CmdExportar 
         Caption         =   "Exportar"
         Height          =   390
         Left            =   135
         TabIndex        =   2
         ToolTipText     =   "Exporta a excel"
         Top             =   4065
         Width           =   1575
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkDiferencia 
         Height          =   210
         Left            =   6255
         OleObjectBlob   =   "Ban_AsientosContables.frx":01A0
         TabIndex        =   1
         Top             =   4230
         Width           =   270
      End
      Begin MSComctlLib.ListView LvCargos 
         Height          =   3705
         Left            =   135
         TabIndex        =   3
         Top             =   330
         Width           =   6405
         _ExtentX        =   11298
         _ExtentY        =   6535
         View            =   3
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   4
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "T500"
            Text            =   "Cod"
            Object.Width           =   882
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "T3000"
            Text            =   "CUENTAS"
            Object.Width           =   4762
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   2
            Key             =   "debe"
            Object.Tag             =   "N100"
            Text            =   "DEBE"
            Object.Width           =   2381
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   3
            Key             =   "haber"
            Object.Tag             =   "N100"
            Text            =   "HABER"
            Object.Width           =   2381
         EndProperty
      End
   End
   Begin VB.Timer Timer1 
      Interval        =   10
      Left            =   11745
      Top             =   45
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   11730
      OleObjectBlob   =   "Ban_AsientosContables.frx":01FE
      Top             =   7455
   End
End
Attribute VB_Name = "Ban_AsientosContables"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False




Private Sub cmdConsultar_Click()
    Dim Sp_Fechas As String, Ip_C As Integer
    Dim Rp_A As Recordset
        
    If CboCuenta.ListIndex = -1 Then
        MsgBox "Necesita seleccionar una cuenta antes de consultar...", vbInformation
        CboCuenta.SetFocus
        Exit Sub
    End If
    
    Sp_Fechas = " AND MONTH(m.mov_fecha)= " & CboMesContable.ItemData(CboMesContable.ListIndex) & " AND YEAR(m.mov_fecha)=" & CboAnoContable.Text & " AND m.cte_id=" & CboCuenta.ItemData(CboCuenta.ListIndex) & " "
    
    'ASIENTO CHEQUES CARGOS
    Set Rp_A = AsientoContable(Sp_Fechas, 7, CboMesContable.ItemData(CboMesContable.ListIndex), CboAnoContable.Text)
    LLenar_Grilla Rp_A, Me, LvCargos, False, True, True, False
    TotalAsiento LvCargos
    'ASIENTO DEPOSITOS
    Set Rp_A = AsientoContable(Sp_Fechas, 8, CboMesContable.ItemData(CboMesContable.ListIndex), CboAnoContable.Text)
    
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvDeposito, False, True, True, False
    TotalAsiento LvDeposito
 
    
End Sub

Private Sub CmdExportar_Click()
    Dim tit(2) As String
    If LvCargos.ListItems.Count = 0 Then Exit Sub
    FraProgreso.Visible = True
    tit(0) = Me.Caption & " MES " & CboMesContable.Text & "   A�O " & CboAnoContable.Text
    tit(1) = CboCuenta.Text
    tit(2) = "POR GIROS/CARGOS/CHEQUES"
    ExportarNuevo LvCargos, tit, Me, BarraProgreso
    FraProgreso.Visible = False
End Sub

Private Sub CmdExportar2_Click()
    Dim tit(2) As String
    If LvDeposito.ListItems.Count = 0 Then Exit Sub
    FraProgreso.Visible = True
    tit(0) = Me.Caption & " MES " & CboMesContable.Text & "   A�O " & CboAnoContable.Text
    tit(1) = CboCuenta.Text
    tit(2) = "POR DEPOSITOS/TRANSFERENCIAS"
    ExportarNuevo LvDeposito, tit, Me, BarraProgreso
    FraProgreso.Visible = False
End Sub

Private Sub cmdSalir_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    Aplicar_skin Me
    Centrar Me
    For i = 1 To 12
        CboMesContable.AddItem UCase(MonthName(i, False))
        CboMesContable.ItemData(CboMesContable.ListCount - 1) = i
    Next
    Busca_Id_Combo CboMesContable, Val(IG_Mes_Contable)
    LLenaYears CboAnoContable, 2010
   ' For i = Year(Date) To Year(Date) - 1 Step -1
   '     CboAnoContable.AddItem i
   '     CboAnoContable.ItemData(CboAnoContable.ListCount - 1) = i
   ' Next
   ' Busca_Id_Combo CboAnoContable, Val(IG_Ano_Contable)
    LLenarCombo CboCuenta, "CONCAT(cte_numero,' ',pla_nombre)", "cte_id", "ban_cta_cte c JOIN con_plan_de_cuentas ON pla_id=ban_id", "cte_activo='SI' AND rut_emp='" & SP_Rut_Activo & "'"

    'LLenarCombo CboCuenta, "CONCAT(cte_numero,' ',ban_nombre)", "cte_id", "ban_cta_cte c INNER JOIN par_bancos USING(ban_id)", "cte_activo='SI' AND rut_emp='" & SP_Rut_Activo & "'"
    If CboCuenta.ListCount > 0 Then
        CboCuenta.ListIndex = 0
    Else
    End If

End Sub

Private Sub Timer1_Timer()
    On Error Resume Next
    CboCuenta.SetFocus
    Timer1.Enabled = False
End Sub
