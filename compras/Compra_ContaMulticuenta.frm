VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form Compra_ContaMulticuenta 
   Caption         =   "Multicuenta"
   ClientHeight    =   5130
   ClientLeft      =   1365
   ClientTop       =   1125
   ClientWidth     =   8010
   LinkTopic       =   "Form1"
   ScaleHeight     =   5130
   ScaleWidth      =   8010
   Begin VB.Frame Frame2 
      Caption         =   "Seleccione cuenta e ingrese monto"
      Height          =   3420
      Left            =   600
      TabIndex        =   6
      Top             =   960
      Width           =   7140
      Begin VB.CommandButton CmdContinuar 
         Caption         =   "Continuar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   240
         TabIndex        =   11
         Top             =   3015
         Width           =   1515
      End
      Begin VB.ComboBox CboCuenta 
         Height          =   315
         Left            =   240
         Style           =   2  'Dropdown List
         TabIndex        =   1
         Top             =   510
         Width           =   3990
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   195
         Left            =   2430
         OleObjectBlob   =   "Compra_ContaMulticuenta.frx":0000
         TabIndex        =   9
         Top             =   3000
         Width           =   1740
      End
      Begin VB.TextBox TxtIngresado 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   4245
         Locked          =   -1  'True
         TabIndex        =   8
         TabStop         =   0   'False
         Top             =   2970
         Width           =   1815
      End
      Begin VB.CommandButton cmdCuenta 
         Caption         =   "Cuenta"
         Height          =   225
         Left            =   240
         TabIndex        =   7
         Top             =   285
         Width           =   870
      End
      Begin VB.CommandButton CmdOk 
         Caption         =   "Ok"
         Height          =   300
         Left            =   6165
         TabIndex        =   3
         Top             =   495
         Width           =   810
      End
      Begin VB.TextBox TxtValor 
         Alignment       =   1  'Right Justify
         Height          =   300
         Left            =   4230
         TabIndex        =   2
         Top             =   510
         Width           =   1935
      End
      Begin MSComctlLib.ListView LvDetalle 
         Height          =   2100
         Left            =   240
         TabIndex        =   10
         Top             =   825
         Width           =   6750
         _ExtentX        =   11906
         _ExtentY        =   3704
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   3
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "N109"
            Text            =   "Id"
            Object.Width           =   882
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "T4000"
            Text            =   "Cuenta Contable"
            Object.Width           =   7038
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   2
            Key             =   "monto"
            Object.Tag             =   "N100"
            Text            =   "Monto"
            Object.Width           =   3413
         EndProperty
      End
   End
   Begin VB.CommandButton cmdSalir 
      Cancel          =   -1  'True
      Caption         =   "&Retornar"
      Height          =   405
      Left            =   6345
      TabIndex        =   5
      Top             =   4575
      Width           =   1350
   End
   Begin VB.Frame Frame1 
      Caption         =   "Valor a completar"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   720
      Left            =   5655
      TabIndex        =   0
      Top             =   180
      Width           =   2070
      Begin VB.TextBox TxtMonto 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   105
         Locked          =   -1  'True
         TabIndex        =   4
         TabStop         =   0   'False
         Top             =   315
         Width           =   1815
      End
   End
   Begin VB.Timer Timer1 
      Interval        =   20
      Left            =   510
      Top             =   4410
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   -15
      OleObjectBlob   =   "Compra_ContaMulticuenta.frx":007C
      Top             =   4395
   End
End
Attribute VB_Name = "Compra_ContaMulticuenta"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public Sm_Formulario As String
Private Sub CmdContinuar_Click()
    If CDbl(TxtIngresado) <> CDbl(TxtMonto) Then
        MsgBox "Valor no cuadra con ingreso...", vbInformation
        CboCuenta.SetFocus
        Exit Sub
    End If
    If Sm_Formulario = "BOLETA" Then
        ComBoletasCompra.LvDetalle.ListItems.Clear
        For i = 1 To LvDetalle.ListItems.Count
            ComBoletasCompra.LvDetalle.ListItems.Add , , LvDetalle.ListItems(i)
            ComBoletasCompra.LvDetalle.ListItems(i).SubItems(1) = LvDetalle.ListItems(i).SubItems(1)
            ComBoletasCompra.LvDetalle.ListItems(i).SubItems(2) = LvDetalle.ListItems(i).SubItems(2)
        Next
    End If
    If Sm_Formulario = "CHEQUES" Then
        Ban_Movimientos.LvCuentas.ListItems.Clear
        For i = 1 To LvDetalle.ListItems.Count
            Ban_Movimientos.LvCuentas.ListItems.Add , , LvDetalle.ListItems(i)
            Ban_Movimientos.LvCuentas.ListItems(i).SubItems(1) = LvDetalle.ListItems(i).SubItems(1)
            Ban_Movimientos.LvCuentas.ListItems(i).SubItems(2) = LvDetalle.ListItems(i).SubItems(2)
        Next
    End If
    
    Me.Hide
    
    
    
    
End Sub

Private Sub cmdCuenta_Click()
    With BuscarSimple
        SG_codigo = Empty
        .Sm_Consulta = "SELECT pla_id, pla_nombre,tpo_nombre,det_nombre " & _
                       "FROM    con_plan_de_cuentas p,  con_tipo_de_cuenta t,   con_detalle_tipo_cuenta d " & _
                       "WHERE   p.tpo_id = t.tpo_id AND p.det_id = d.det_id "
        .Sm_CampoLike = "pla_nombre"
        .Im_Columnas = 2
        .Show 1
        If SG_codigo = Empty Then Exit Sub
        Busca_Id_Combo CboCuenta, Val(SG_codigo)
    End With
End Sub

Private Sub CmdOk_Click()
    Dim Bp_Ingresada As Boolean
    Dim Ip_P As Integer
    If CboCuenta.ListIndex = -1 Then
        MsgBox "Debe seleccionar cuenta contable...", vbInformation
        CboCuenta.SetFocus
        Exit Sub
    End If
    If Val(txtValor) = 0 Then
        MsgBox "Debe ingresar Valor...", vbInformation
        txtValor.SetFocus
        Exit Sub
    End If
    Bp_Ingresada = False
    For i = 1 To LvDetalle.ListItems.Count
        If CboCuenta.ItemData(CboCuenta.ListIndex) = LvDetalle.ListItems(i) Then
            MsgBox "Esta cuenta ya est� ingresada...", vbInformation
            Bp_Ingresada = True
            Exit For
        End If
    Next
    If Bp_Ingresada Then
        CboCuenta.SetFocus
        Exit Sub
    End If
    
    'Ya esta validado ahora agregamos
    LvDetalle.ListItems.Add , , CboCuenta.ItemData(CboCuenta.ListIndex)
    
    Ip_P = LvDetalle.ListItems.Count
    LvDetalle.ListItems(Ip_P).SubItems(1) = CboCuenta.Text
    LvDetalle.ListItems(Ip_P).SubItems(2) = NumFormat(txtValor)
    
    SumaG
    
    txtValor = 0
    CboCuenta.SetFocus
    
    
    
End Sub
Private Sub SumaG()
    TxtIngresado = NumFormat(TotalizaColumna(LvDetalle, "monto"))
    If CDbl(TxtIngresado) = CDbl(TxtMonto) Then
        TxtIngresado.ForeColor = vbBlue
    Else
        TxtIngresado.ForeColor = vbRed
    End If
End Sub

Private Sub cmdSalir_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    Aplicar_skin Me
    Centrar Me
    LLenarCombo CboCuenta, "pla_nombre", "pla_id", "con_plan_de_cuentas", "pla_activo='SI'"

End Sub
Private Sub LvDetalle_DblClick()
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    
    Busca_Id_Combo CboCuenta, Val(LvDetalle.SelectedItem.Text)
    txtValor = LvDetalle.SelectedItem.SubItems(2)
    LvDetalle.ListItems.Remove LvDetalle.SelectedItem.Index
    SumaG
    CboCuenta.SetFocus
End Sub

Private Sub Timer1_Timer()
    On Error Resume Next
    CboCuenta.SetFocus
    Timer1.Enabled = False
End Sub

Private Sub TxtValor_GotFocus()
    En_Foco txtValor
End Sub

Private Sub TxtValor_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
End Sub

Private Sub TxtValor_Validate(Cancel As Boolean)
    If Val(txtValor) = 0 Then txtValor = "0"
End Sub
