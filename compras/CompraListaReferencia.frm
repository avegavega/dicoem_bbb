VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form CompraListaReferencia 
   Caption         =   "Seleccionar documento"
   ClientHeight    =   4845
   ClientLeft      =   540
   ClientTop       =   2250
   ClientWidth     =   11100
   LinkTopic       =   "Form1"
   ScaleHeight     =   4845
   ScaleWidth      =   11100
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   540
      OleObjectBlob   =   "CompraListaReferencia.frx":0000
      Top             =   4380
   End
   Begin VB.Timer Timer1 
      Interval        =   20
      Left            =   15
      Top             =   4395
   End
   Begin VB.CommandButton CmdSalir 
      Cancel          =   -1  'True
      Caption         =   "&Retornar"
      Height          =   495
      Left            =   9735
      TabIndex        =   3
      Top             =   4215
      Width           =   1095
   End
   Begin VB.Frame Frame1 
      Caption         =   "Documentos disponibles"
      Height          =   3825
      Left            =   210
      TabIndex        =   0
      Top             =   360
      Width           =   10620
      Begin VB.CommandButton CmdSeleccionar 
         Caption         =   "&Seleccionar"
         Height          =   495
         Left            =   165
         TabIndex        =   2
         Top             =   3240
         Width           =   1455
      End
      Begin MSComctlLib.ListView LvDetalle 
         Height          =   2880
         Left            =   180
         TabIndex        =   1
         Top             =   330
         Width           =   10215
         _ExtentX        =   18018
         _ExtentY        =   5080
         View            =   3
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   6
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "T1000"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "F1000"
            Text            =   "Fecha"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "T1500"
            Text            =   "Proveedor"
            Object.Width           =   5292
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Object.Tag             =   "T1300"
            Text            =   "Documento"
            Object.Width           =   3704
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   2
            SubItemIndex    =   4
            Object.Tag             =   "N109"
            Text            =   "Nro"
            Object.Width           =   1940
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   5
            Object.Tag             =   "N100"
            Text            =   "Valor Bruto"
            Object.Width           =   2293
         EndProperty
      End
   End
End
Attribute VB_Name = "CompraListaReferencia"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public Rut As String
Public Sm_Nota As String
Private Sub CmdSAlir_Click()
        Unload Me
        SG_codigo = emtpy
End Sub
Private Sub CmdSeleccionar_Click()
    If LvDetalle.SelectedItem Is Nothing Then
        MsgBox "Seleccione documento ", vbInformation
        LvDetalle.SetFocus
        Exit Sub
    End If
    SG_codigo = LvDetalle.SelectedItem
    Unload Me
End Sub
Private Sub Form_Load()
    Skin2 Me, , 5
    Centrar Me
    Sql = "SELECT id,fecha,nombre_proveedor,doc_nombre, no_documento,total " & _
          "FROM com_doc_compra c " & _
          "INNER JOIN sis_documentos USING(doc_id) " & _
          "WHERE doc_orden_de_compra='NO' AND rut_emp='" & SP_Rut_Activo & "' AND rut='" & Rut & "' AND " & Sm_Nota & _
          "ORDER BY id DESC "
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvDetalle, True, True, True, False
    BoldColuna LvDetalle, 6
End Sub

Private Sub LvDetalle_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    ordListView ColumnHeader, Me, LvDetalle
End Sub

Private Sub LvDetalle_DblClick()
    CmdSeleccionar_Click
End Sub

Private Sub LvDetalle_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 13 Then CmdSeleccionar_Click
        
End Sub

Private Sub Timer1_Timer()
    LvDetalle.SetFocus
    Timer1.Enabled = False
End Sub
