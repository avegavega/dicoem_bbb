VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{28D47522-CF84-11D1-834C-00A0249F0C28}#1.0#0"; "gif89.dll"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{DA729E34-689F-49EA-A856-B57046630B73}#1.0#0"; "progressbar-xp.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form Compra_Listado 
   Caption         =   "Compras"
   ClientHeight    =   8820
   ClientLeft      =   3660
   ClientTop       =   345
   ClientWidth     =   15690
   LinkTopic       =   "Form1"
   ScaleHeight     =   8820
   ScaleWidth      =   15690
   Begin VB.Timer Timer1 
      Interval        =   1000
      Left            =   8145
      Top             =   1020
   End
   Begin VB.Frame FrmLoad 
      Height          =   1680
      Left            =   5145
      TabIndex        =   32
      Top             =   5730
      Visible         =   0   'False
      Width           =   2805
      Begin GIF89LibCtl.Gif89a Gif89a1 
         Height          =   1275
         Left            =   210
         OleObjectBlob   =   "Compra_Listado.frx":0000
         TabIndex        =   33
         Top             =   285
         Width           =   2445
      End
   End
   Begin VB.TextBox txtXmlBasico 
      Height          =   285
      Left            =   2535
      TabIndex        =   29
      Top             =   9885
      Visible         =   0   'False
      Width           =   4695
   End
   Begin VB.CommandButton CmdGeneraOc 
      Caption         =   "Generar OC"
      Height          =   330
      Left            =   105
      TabIndex        =   28
      ToolTipText     =   "Exportar"
      Top             =   8415
      Width           =   2025
   End
   Begin VB.CommandButton CmdCompraSimple 
      Caption         =   "F4 - Nueva (Simple)"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   4290
      TabIndex        =   27
      ToolTipText     =   "Nueva compra"
      Top             =   8145
      Width           =   2415
   End
   Begin VB.CommandButton cmdFolio 
      Caption         =   "Cambiar Folio"
      Height          =   285
      Left            =   105
      TabIndex        =   26
      Top             =   7755
      Width           =   2025
   End
   Begin VB.Frame Frame2 
      Caption         =   "Filtro Nro Documento"
      Height          =   1050
      Left            =   9255
      TabIndex        =   21
      Top             =   405
      Width           =   1995
      Begin VB.TextBox TxtNroDocumento 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   240
         TabIndex        =   22
         Top             =   585
         Width           =   1425
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   255
         Left            =   195
         OleObjectBlob   =   "Compra_Listado.frx":0086
         TabIndex        =   23
         Top             =   330
         Width           =   1770
      End
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkEmpresaActiva 
      Height          =   285
      Left            =   6345
      OleObjectBlob   =   "Compra_Listado.frx":00F7
      TabIndex        =   20
      Top             =   105
      Width           =   5685
   End
   Begin VB.Frame FraProgreso 
      Caption         =   "Progreso exportaci�n"
      Height          =   795
      Left            =   690
      TabIndex        =   18
      Top             =   4695
      Visible         =   0   'False
      Width           =   11430
      Begin Proyecto2.XP_ProgressBar BarraProgreso 
         Height          =   330
         Left            =   150
         TabIndex        =   19
         Top             =   285
         Width           =   11130
         _ExtentX        =   19632
         _ExtentY        =   582
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BrushStyle      =   0
         Color           =   16777088
         Scrolling       =   1
         ShowText        =   -1  'True
      End
   End
   Begin VB.CommandButton CmdEliminaDocumento 
      Caption         =   "Eliminar Documento "
      Height          =   330
      Left            =   105
      TabIndex        =   7
      ToolTipText     =   "Exportar"
      Top             =   8055
      Width           =   2025
   End
   Begin VB.CommandButton CmdNueva 
      Caption         =   "F1 - Nueva"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   6855
      TabIndex        =   8
      ToolTipText     =   "Nueva compra"
      Top             =   8160
      Width           =   2175
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   -45
      OleObjectBlob   =   "Compra_Listado.frx":0171
      Top             =   7410
   End
   Begin VB.Frame Frame1 
      Caption         =   "Filtro"
      Height          =   1050
      Left            =   120
      TabIndex        =   15
      Top             =   420
      Width           =   7680
      Begin VB.TextBox TxtBusqueda 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   750
         TabIndex        =   1
         ToolTipText     =   "Ingrese RUT (separado por puntos y guion) o Razon Social"
         Top             =   600
         Width           =   3855
      End
      Begin VB.CommandButton CmdBusca 
         Caption         =   "Buscar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   5385
         TabIndex        =   2
         ToolTipText     =   "Busca texto ingresado"
         Top             =   585
         Width           =   1095
      End
      Begin VB.CommandButton CmdTodos 
         Caption         =   "Todos"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   6465
         TabIndex        =   3
         ToolTipText     =   "Muestra todas las compras"
         Top             =   585
         Width           =   1095
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   255
         Left            =   720
         OleObjectBlob   =   "Compra_Listado.frx":03A5
         TabIndex        =   16
         Top             =   330
         Width           =   3060
      End
   End
   Begin VB.CommandButton CmdSeleccionar 
      Caption         =   "F2  - Ver"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   9030
      TabIndex        =   9
      ToolTipText     =   "Visualizar compra"
      Top             =   8160
      Width           =   2175
   End
   Begin VB.CommandButton CmdSalir 
      Cancel          =   -1  'True
      Caption         =   "ESC - &Retornar"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   13245
      TabIndex        =   11
      ToolTipText     =   "Salir"
      Top             =   8160
      Width           =   2055
   End
   Begin VB.Frame frmOts 
      Caption         =   "Listado historico"
      Height          =   6210
      Left            =   105
      TabIndex        =   14
      Top             =   1515
      Width           =   15120
      Begin MSComctlLib.ListView LvDetalle 
         Height          =   5715
         Left            =   150
         TabIndex        =   6
         Top             =   315
         Width           =   14820
         _ExtentX        =   26141
         _ExtentY        =   10081
         View            =   3
         LabelWrap       =   0   'False
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   12
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "N109"
            Text            =   "Id Unico"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "T1200"
            Text            =   "Folio"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "F1000"
            Text            =   "Fecha"
            Object.Width           =   1940
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Object.Tag             =   "T3000"
            Text            =   "Nombre Proveedor"
            Object.Width           =   5292
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Object.Tag             =   "T2500"
            Text            =   "Tipo Documento"
            Object.Width           =   4939
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   5
            Object.Tag             =   "T800"
            Text            =   "Nro Doc"
            Object.Width           =   1940
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   6
            Object.Tag             =   "T1500"
            Text            =   "Movimiento"
            Object.Width           =   3528
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   7
            Key             =   "total"
            Object.Tag             =   "N100"
            Text            =   "Total"
            Object.Width           =   2646
         EndProperty
         BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   8
            Object.Tag             =   "N109"
            Text            =   "Doc_id"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   9
            Object.Tag             =   "T1000"
            Text            =   "rut_prvoeedodr"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   10
            Key             =   "retencion"
            Object.Tag             =   "N100"
            Text            =   "Impuesto"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(12) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   11
            Key             =   "totalb"
            Object.Tag             =   "N100"
            Text            =   "Neto"
            Object.Width           =   2540
         EndProperty
      End
   End
   Begin VB.CommandButton cmdExportar 
      Caption         =   "F3 - Exportar a Excel"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   11190
      TabIndex        =   10
      ToolTipText     =   "Exportar"
      Top             =   8160
      Width           =   2055
   End
   Begin VB.Frame Frame4 
      Caption         =   "Mes y A�o"
      Height          =   1050
      Left            =   11655
      TabIndex        =   0
      Top             =   375
      Width           =   3555
      Begin VB.ComboBox comMes 
         Height          =   315
         ItemData        =   "Compra_Listado.frx":0420
         Left            =   285
         List            =   "Compra_Listado.frx":0422
         Style           =   2  'Dropdown List
         TabIndex        =   4
         Top             =   600
         Width           =   1890
      End
      Begin VB.ComboBox ComAno 
         Height          =   315
         Left            =   2175
         Style           =   2  'Dropdown List
         TabIndex        =   5
         Top             =   615
         Width           =   1215
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
         Height          =   255
         Left            =   285
         OleObjectBlob   =   "Compra_Listado.frx":0424
         TabIndex        =   12
         Top             =   405
         Width           =   375
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel5 
         Height          =   255
         Left            =   2160
         OleObjectBlob   =   "Compra_Listado.frx":0488
         TabIndex        =   13
         Top             =   420
         Width           =   375
      End
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
      Height          =   255
      Left            =   3990
      OleObjectBlob   =   "Compra_Listado.frx":04EC
      TabIndex        =   17
      Top             =   7695
      Width           =   8055
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel6 
      Height          =   255
      Left            =   195
      OleObjectBlob   =   "Compra_Listado.frx":05D6
      TabIndex        =   24
      Top             =   660
      Width           =   1140
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel7 
      Height          =   255
      Left            =   150
      OleObjectBlob   =   "Compra_Listado.frx":063F
      TabIndex        =   25
      Top             =   645
      Width           =   1650
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   0
      Top             =   0
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.Frame Frame3 
      Caption         =   "Importar Compras desde CSV"
      Height          =   1035
      Left            =   210
      TabIndex        =   30
      Top             =   8790
      Visible         =   0   'False
      Width           =   13275
      Begin VB.CommandButton CmdEliminarDocPantalla 
         Caption         =   "Eliminar Documentos en Pantalla"
         Height          =   315
         Left            =   75
         TabIndex        =   39
         Top             =   630
         Width           =   2520
      End
      Begin VB.CommandButton CmdImportar 
         Caption         =   "Importar Compras"
         Default         =   -1  'True
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   10755
         TabIndex        =   38
         ToolTipText     =   "Nueva compra"
         Top             =   465
         Width           =   2415
      End
      Begin VB.TextBox TxtNombre 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   4140
         Locked          =   -1  'True
         TabIndex        =   37
         TabStop         =   0   'False
         Top             =   615
         Width           =   3375
      End
      Begin VB.TextBox TxtCodigoCuenta 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   3150
         TabIndex        =   35
         Top             =   615
         Width           =   1000
      End
      Begin VB.CommandButton CmdCuenta 
         Caption         =   "Cuenta"
         Height          =   225
         Left            =   3195
         TabIndex        =   34
         Top             =   345
         Width           =   990
      End
      Begin VB.ComboBox Cmb_tipoDoc 
         Height          =   315
         ItemData        =   "Compra_Listado.frx":06A8
         Left            =   7650
         List            =   "Compra_Listado.frx":06B2
         TabIndex        =   31
         Text            =   "Seleccione Tipo Documento"
         Top             =   645
         Visible         =   0   'False
         Width           =   2955
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel12 
         Height          =   180
         Left            =   4305
         OleObjectBlob   =   "Compra_Listado.frx":06E8
         TabIndex        =   36
         Top             =   390
         Width           =   1575
      End
   End
End
Attribute VB_Name = "Compra_Listado"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim Sm_LibroGastos As String
Dim Sp_Condicion As String
Dim Sp_CondicionGasto As String
Dim Sm_Numero As String
Dim Sp_Bodega As Integer
Private Sub cmdMesAno_Click()
    CargaDatos
End Sub

Private Sub Cmb_tipoDoc_Click()
Dim iiiii As String
iiiii = Cmb_tipoDoc.Text ''(Cmb_tipoDoc.ListIndex)
End Sub


Private Sub CmdBusca_Click()
    If Len(TxtBusqueda) = 0 Then Exit Sub
    Sp_Condicion = " AND (nombre_proveedor LIKE '%" & TxtBusqueda & "%' OR c.rut LIKE '%" & TxtBusqueda & "%') "
    CargaDatos
End Sub

Private Sub CmdCompraSimple_Click()
    Compra_Ingreso_OficinaContable.Show 1
    Me.SetFocus
    CargaDatos
    
End Sub

Private Sub cmdCuenta_Click()
    With BuscarSimple
        SG_codigo = Empty
        .Sm_Consulta = "SELECT pla_id, pla_nombre,tpo_nombre,det_nombre " & _
                       "FROM    con_plan_de_cuentas p,  con_tipo_de_cuenta t,   con_detalle_tipo_cuenta d " & _
                       "WHERE   p.tpo_id = t.tpo_id AND p.det_id = d.det_id "
        .Sm_CampoLike = "pla_nombre"
        .Im_Columnas = 2
        .CmdCuentasContables.Visible = True
        .Show 1
        If SG_codigo = Empty Then Exit Sub
        TxtCodigoCuenta = Val(SG_codigo)
        TxtCodigoCuenta_Validate True
        TxtCodigoCuenta.SetFocus
        
    End With
End Sub

Private Sub CmdEliminarDocPantalla_Click()
    Dim Sp_IdesCompra As String
    If LvDetalle.ListItems.Count = 0 Then Exit Sub
    
    If MsgBox("Esta Seguro de Eliminar los documentos del periodo" & vbNewLine & comMes.Text & vbNewLine & ComAno.Text, vbQuestion + vbOKCancel) = vbOK Then
            
            '02-11-2017
            'avv
            'Eliminacion de documentos que estan en la lista, eliminar tambien los com_impuestos.
            
            FrmLoad.Visible = True
            DoEvents
            
            Sp_IdesCompra = ""
            For i = 1 To LvDetalle.ListItems.Count
                Sp_IdesCompra = Sp_IdesCompra & LvDetalle.ListItems(i) & ","
                
                
            
            Next
            Sp_IdesCompra = Mid(Sp_IdesCompra, 1, Len(Sp_IdesCompra) - 1)
            
            cn.Execute "DELETE FROM com_impuestos " & _
                        "WHERE id IN(" & Sp_IdesCompra & ")"
                        
            cn.Execute "DELETE FROM com_doc_compra_detalle " & _
                        "WHERE id IN(" & Sp_IdesCompra & ")"
            
            cn.Execute "DELETE FROM com_doc_compra " & _
                        "WHERE id IN(" & Sp_IdesCompra & ")"
            
            FrmLoad.Visible = False
            CargaDatos
    End If
End Sub

Private Sub Timer1_Timer()
    On Error Resume Next
    Me.Show
    Timer1.Enabled = False
End Sub

Private Sub TxtCodigoCuenta_Validate(Cancel As Boolean)
    If Len(TxtCodigoCuenta) = 0 Then
        TxtNombre = 0
        Exit Sub
    End If
    Sql = "SELECT pla_nombre,tpo_id,det_id,pla_analisis " & _
            "FROM con_plan_de_cuentas " & _
            "WHERE pla_id='" & TxtCodigoCuenta & "'"
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        TxtNombre = RsTmp!pla_nombre
        
        'Aqui comprobamos si corresponde a Activo inmovilizado
        If RsTmp!tpo_id = 1 And RsTmp!det_id = 4 And RsTmp!pla_analisis = "SI" Then
            'Activo inmobilizado de ser ingresado en formulario de compra normal
            MsgBox "Para ingresar Activos Fijos, debe ir a bot�n COMPRAS--F1 NUEVA..."
            TxtCodigoCuenta = ""
            TxtNombre = ""
            Cancel = True
            Exit Sub
        End If
      
        Cancel = False
    Else
        MsgBox "Cuenta no encontrada...", vbInformation
        TxtCodigoCuenta = ""
        TxtNombre = ""
        Cancel = True
    End If
End Sub
Private Sub CmdEliminaDocumento_Click()
    Dim s_Inventario As String, L_Unico As Long, s_mov As String, rs_Detalle As Recordset
    Dim Sp_Llave As String, Lp_IdAbono As Long, Sp_EliminaActivos As String
    Dim Lp_NroC As Long
    Dim Fecha As Date
    If Principal.CmdMenu(8).Visible = True Then
        If ConsultaCentralizado("1,11", IG_Mes_Contable, IG_Ano_Contable) Then
            MsgBox "periodo contable ya ha sido centralizado, " & vbNewLine & "No puede modificar"
            Exit Sub
        End If
    End If
    
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    L_Unico = LvDetalle.SelectedItem.Text
    
    
    Sql = "SELECT nro_factura,doc_id_factura " & _
            "FROM com_doc_compra " & _
            "WHERE id=" & L_Unico & " AND nro_factura>0 "
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        MsgBox "Es una guia facturada..." & vbNewLine & " Primero elimine la factura...", vbInformation
        Exit Sub
    End If
    
     Sql = "SELECT com_id_ref_de_nota_debito " & _
            "FROM com_doc_compra " & _
            "WHERE com_id_ref_de_nota_debito=" & L_Unico
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        MsgBox "Documento tiene N.Credito o N.Debito asociada..." & vbNewLine & "No es posible eliminar...", vbInformation
        Exit Sub
    End If
    
    
    
    
    SG_codigo2 = Empty
    sis_InputBox.FramBox = "Ingrese llave para anulacion"
    sis_InputBox.Show 1
    Sp_Llave = UCase(SG_codigo2)
    If Len(Sp_Llave) = 0 Then Exit Sub
    Sql = "SELECT usu_id,usu_nombre,usu_login " & _
          "FROM sis_usuarios " & _
          "WHERE usu_pwd = MD5('" & Sp_Llave & "')"
    Consulta RsTmp3, Sql
    
    
    '1ro Extraeremos el abo_id en el caso que exista 21-01-2012
    Sql = "SELECT abo_id,abo_nro_comprobante " & _
          "FROM cta_abono_documentos d " & _
          "INNER JOIN cta_abonos a USING(abo_id) " & _
          "WHERE abo_cli_pro='PRO' AND a.rut_emp ='" & SP_Rut_Activo & "' AND d.rut_emp ='" & SP_Rut_Activo & "' AND id=" & L_Unico
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
            Lp_NroC = RsTmp!abo_nro_comprobante
                '2DO SABREMOS QUE TIPO DE ABONO FUE,
            Lp_IdAbono = RsTmp!abo_id
            Sql = "SELECT abo_id,abo_fecha,mpa_nombre,usu_nombre,a.mpa_id,abo_fecha_pago " & _
                  "FROM cta_abonos a " & _
                  "INNER JOIN par_medios_de_pago m USING(mpa_id) " & _
                  "INNER JOIN cta_abono_documentos d USING(abo_id) " & _
                  "WHERE abo_cli_pro='PRO' AND a.rut_emp='" & SP_Rut_Activo & "' AND d.rut_emp='" & SP_Rut_Activo & "' AND a.abo_id=" & Lp_IdAbono
            Consulta RsTmp, Sql
            If RsTmp.RecordCount > 0 Then
                If RsTmp.RecordCount > 1 Then
                    'Ojo que el abono de este documento fue junto con varios documento
                    'por lo tanto no debemos permitir eliminar este documento
                    MsgBox "No es posible eliminar este documento..." & vbNewLine & "Es parte de un abono de multiples documentos " & _
                    vbNewLine & "Cant. Documentos " & RsTmp.RecordCount & " Fecha Abono " & Format(RsTmp!abo_fecha_pago, "DD-MM-YYYY") & " Usuario:" & RsTmp!usu_nombre & " Medio de pago:" & RsTmp!mpa_nombre, vbExclamation
                    Exit Sub
                End If
                If RsTmp.RecordCount = 1 Then
                    If RsTmp!mpa_id <> 1 Then
                        MsgBox "No es posible eliminar este documento..." & vbNewLine & "Ya se realiz� abono ... " & _
                        vbNewLine & " Fecha Abono " & Format(RsTmp!abo_fecha_pago, "DD-MM-YYYY") & " Usuario:" & RsTmp!usu_nombre & " Medio de pago:" & RsTmp!mpa_nombre, vbExclamation
                        Exit Sub
                    Else
                        'El abono es solo a un documento y en contado(efectivo) por lo que no es problema eliminarlo
                    End If
                End If
            
            
            End If
            
            If MsgBox("El documento tiene un abono registrado ..." & vbNewLine & "Nro Comprobante " & Lp_NroC & vbNewLine & "�Continuar?", vbQuestion + vbYesNo) = vbNo Then Exit Sub
            
    Else
        'El documento es al credito pero no tiene abonos, es posible continuar, no afectara nada
    
    End If
    
    
    
    If RsTmp3.RecordCount > 0 Then
        X = InputBox("Usuario:" & RsTmp3!usu_nombre & vbNewLine & "Ingrese motivo", "Motivo de eliminaci�n")
        If Len(X) = 0 Then
            MsgBox "No ingreso motivo, no fue eliminado el documento"
            Exit Sub
        End If
        
        With LvDetalle.SelectedItem
             Sql = "INSERT INTO sis_eliminacion_documentos (id,eli_fecha,eli_motivo,usu_login,eli_ven_com) VALUES (" & _
                   LvDetalle.SelectedItem & ",'" & Fql(Date) & "','" & UCase(X) & "','" & RsTmp3!usu_login & "','COM')"
            
            'Sql = "INSERT INTO com_historial_eliminaciones (doc_id,no_documento,rut_proveedor,eli_fecha,eli_motivo,usu_id) VALUES (" & _
                        .SubItems(7) & "," & .SubItems(4) & ",'" & .SubItems(8) & "','" & Format(Now, "YYYY-DD-MM HH:MM:SS") & "','" & X & "'," & RsTmp3!usu_id & ")"
                         
            
        End With
            
    Else
        MsgBox "Contrase�a ingresada no fue encontrada...", vbInformation
        Exit Sub
    End If
    On Error GoTo ErrorElimina
    cn.BeginTrans 'COMENZAMOS LA TRANSACCION
        
        
        'Aqui detectamos si la empresa
        'Utiliza cuenta contable-
        'Entonces detectamos si en el documento que esta eliminado existen
        'activos inmovilizados
        Sql = "SELECT emp_cuenta " & _
                "FROM sis_empresas " & _
                "WHERE rut='" & SP_Rut_Activo & "' AND emp_cuenta='SI'"
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
            Sql = "SELECT a.aim_id " & _
                    "FROM com_doc_compra_detalle c " & _
                    "JOIN con_plan_de_cuentas p ON c.pla_id=p.pla_id " & _
                    "JOIN con_activo_inmovilizado a ON c.pro_codigo=a.aim_id " & _
                    "WHERE p.tpo_id=1 AND p.det_id=4 AND p.pla_analisis='SI' AND c.id=" & L_Unico
            Consulta RsTmp, Sql
            If RsTmp.RecordCount > 0 Then
                If MsgBox("En el documento que se est� eliminando existen " & _
                        "Activos Inmovilizados" & vbNewLine & "� Eliminar ?" & vbNewLine & " (tambien es posible modificar estos activos en el mantenedor de Activos Inmovilizados) ", vbYesNo + vbQuestion) = vbYes Then
                        
                        With RsTmp
                            .MoveFirst
                            Sp_EliminaActivos = ""
                            Do While Not .EOF
                                Sp_EliminaActivos = Sp_EliminaActivos & RsTmp!aim_id & ","
                                .MoveNext
                            Loop
                            Sp_EliminaActivos = Mid(Sp_EliminaActivos, 1, Len(Sp_EliminaActivos) - 1)
                            cn.Execute "DELETE FROM con_activo_inmovilizado " & _
                                    "WHERE aim_id IN(" & Sp_EliminaActivos & ")"
                        End With
                    
                End If
            
            
            End If
            
        
        
        End If
    
    
       ' cn.Execute Sql
        
        Sql = "DELETE FROM cta_abonos " & _
              "WHERE rut_emp='" & SP_Rut_Activo & "' AND abo_id=" & Lp_IdAbono
        cn.Execute Sql
        Sql = "DELETE FROM cta_abono_documentos " & _
              "WHERE rut_emp='" & SP_Rut_Activo & "' AND abo_id=" & Lp_IdAbono
        cn.Execute Sql
        
        If Lp_IdAbono > 0 Then
            cn.Execute "DELETE FROM ban_cheques_fondos_rendir " & _
                   "WHERE abo_id=" & Lp_IdAbono
        End If
        
        Sql = "SELECT rut,nombre_proveedor,doc_movimiento inventario,no_documento,c.doc_id,doc_nombre,bod_id,com_nc_utilizada,id " & _
              "FROM com_doc_compra c,sis_documentos d " & _
              "WHERE d.doc_id=c.doc_id AND id=" & L_Unico
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
            Sp_Bodega = RsTmp!bod_id
            If RsTmp!Inventario = "ENTRADA" Then s_mov = "SALIDA"
            If RsTmp!Inventario = "SALIDA" Then s_mov = "ENTRADA"
            If RsTmp!Inventario <> "NN" Then
            
                Sql = "SELECT cmd_id,pro_codigo,cmd_cantidad cantidad,cmd_costo_real_neto total_linea " & _
                      "FROM com_doc_compra_detalle " & _
                      "WHERE id=" & L_Unico
                Consulta rs_Detalle, Sql
                If rs_Detalle.RecordCount > 0 Then
                    rs_Detalle.MoveFirst
                    Do While Not rs_Detalle.EOF
                        Kardex Format(Date, "YYYY-MM-DD"), s_mov, RsTmp!doc_id, RsTmp!no_documento, Sp_Bodega, _
                            rs_Detalle!pro_codigo, rs_Detalle!cantidad, "ELIMINA " & RsTmp!doc_nombre & " Nro:" & RsTmp!no_documento, Round(rs_Detalle!total_linea / rs_Detalle!cantidad), _
                            rs_Detalle!total_linea, RsTmp!Rut, RsTmp!nombre_proveedor, "NO", Round(rs_Detalle!total_linea / rs_Detalle!cantidad), , , , , 0
                            
                        
                        
                        
                        cn.Execute "DELETE FROM com_doc_compra_detalle_impuestos " & _
                                    "WHERE cmd_id=" & rs_Detalle!cmd_id & " AND emp_id=" & IG_id_Empresa
                        
                        rs_Detalle.MoveNext
                    Loop
                End If
            End If
            'DEBEMOS CONSULTAR SI EXISTEN DOCUMENTOS QUE HACEN REFERENCIA AL QUE SE ESTA INGRESANDO
            Sql = "UPDATE com_doc_compra " & _
                         "SET id_ref=0,doc_id_factura=0,nro_factura=0,com_estado_oc='PROCESO' " & _
                  "WHERE id_ref=" & L_Unico
            cn.Execute Sql
                    
            If RsTmp!Inventario = "NN" Then
                '28 Abril 2011
                'No modifica inventario
                'por lo tanto podriamos eliminar el documeneto sin tocar el
                'kardex ni el stock
            End If
            
            '28 Sep 2013
            'DEBEMOS DETECTAR SI LA NOTA DE CREDITO SE UTILIZO PARA ABONAR A SU DOCUMENTO ORIGINAL
            If RsTmp!com_nc_utilizada = "SI" Then
                'con esto sabemos q se trata de una nota de credito, y podemos referenciar a la tabla de abonos
                Sql = "DELETE FROM cta_abonos " & _
                        "WHERE abo_cli_pro='PRO' AND rut_emp='" & SP_Rut_Activo & "' AND id_ref=" & RsTmp!ID
                cn.Execute Sql
            End If
            'multidelete
            Sql = "DELETE FROM com_impuestos " & _
                      "WHERE id=" & L_Unico
            cn.Execute Sql
            'If SP_Control_Inventario = "SI" Then
              '  Sql = "DELETE FROM " & _
                      "com_doc_compra, " & _
                      "com_doc_compra_detalle " & _
                      "USING com_doc_compra " & _
                      "LEFT JOIN com_doc_compra_detalle ON (com_doc_compra.id=com_doc_compra_detalle.id) " & _
                      "WHERE com_doc_compra.id=" & L_Unico
                Sql = "DELETE FROM com_doc_compra " & _
                        "WHERE id=" & L_Unico
                cn.Execute Sql
                Sql = "DELETE FROM com_doc_compra_detalle " & _
                        "WHERE id=" & L_Unico
                
                      
                    
                'Consulta RsTmp, Sql
            'Else
            '    Sql = "DELETE FROM " & _
            '          "com_doc_compra, " & _
            '          "com_sin_detalle " & _
            '          "USING com_doc_compra " & _
            '          "LEFT JOIN com_sin_detalle USING(id) " & _
            '          "WHERE com_doc_compra.id=" & L_Unico
                'Consulta RsTmp, Sql
            'End If
            cn.Execute Sql
            cn.Execute "DELETE FROM com_detalle_honorarios " & _
                                "WHERE com_id=" & L_Unico
            
         
        End If
    EliminaAbonosDeNC "PRO", LvDetalle.SelectedItem
    
    
    
    
    cn.CommitTrans
    CargaDatos
    Exit Sub
ErrorElimina:
    MsgBox "Ocurrio un error al eliminar documento" & vbNewLine & Err.Description & vbNewLine & Err.Number
    cn.RollbackTrans
    
End Sub

Private Sub CmdExportar_Click()
    Dim tit(2) As String
    If LvDetalle.ListItems.Count = 0 Then Exit Sub
    FraProgreso.Visible = True
    tit(0) = Me.Caption & " " & DtFecha & " - " & Time
    tit(1) = Principal.SkEmpresa
    tit(2) = SP_Rut_Activo
    ExportarNuevo LvDetalle, tit, Me, BarraProgreso
    FraProgreso.Visible = False
End Sub

Private Sub cmdFolio_Click()
    Dim Lp_NroC As Long
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    If Principal.CmdMenu(8).Visible = True Then
        If ConsultaCentralizado("1,11", IG_Mes_Contable, IG_Ano_Contable) Then
            MsgBox "Periodo contable ya ha sido centralizado, " & vbNewLine & "No puede modificar"
            Exit Sub
        End If
    End If
    L_Unico = LvDetalle.SelectedItem.Text
    
    
   ' If RsTmp2.RecordCount > 0 Then
               ' Sp_FolioNro = RsTmp2!folio
                Sp_Folio = LvDetalle.SelectedItem.SubItems(1)
                SG_codigo2 = Empty
                sis_InputBox.texto.PasswordChar = ""
                sis_InputBox.texto.Locked = False
                sis_InputBox.Caption = "Tome nota"
                sis_InputBox.FramBox = "Folio para libro contable"
                sis_InputBox.CmdCancelar.Visible = False
              '  Sp_Folio = Right("00" & IG_Mes_Contable, 2) & Right("000" & Sp_Folio, 3)
                sis_InputBox.texto = Sp_Folio
                sis_InputBox.Show 1
                
    If Len(SG_codigo2) = 0 Then Exit Sub
    
    Sql = "SELECT com_folio " & _
            "FROM com_doc_compra " & _
            "WHERE com_folio_texto='" & SG_codigo2 & "' AND rut_emp='" & SP_Rut_Activo & "' AND ano_contable=" & ComAno.Text
    
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        MsgBox "Folio ya existe...", vbInformation + vbOKOnly
        Exit Sub
    End If
    cn.Execute "UPDATE com_doc_compra SET com_folio=" & Val(Right(SG_codigo2, 3)) & ",com_folio_texto='" & SG_codigo2 & "' " & _
                "WHERE id=" & L_Unico
   CargaDatos
    
    '        End If
End Sub

Private Sub CmdGeneraOc_Click()
    busca_productos_OC.Show 1
End Sub

Private Sub CmdImportar_Click()
    Dim ff As Integer
    Dim xxxx() As String
    Dim linea, TxtMontoNeto, TxtMontoIVA, TxtExento As String
    Dim i, ii, montoExento, montoNeto, montoIva, montoNetoActivoFijo, CodOImp, ValOImp, TasaOImp, TPuros, TCigarrillos, TElaborados As Long
    Dim Sp_infoLinea As String, Sp_RutFormat As String
    Dim Rut, nombre, Tipo, Folio, Fecha As Date, monto, nombre_sii  As String
    Dim Lp_Id As Long, Sp_SoloUnidades As String, Sp_SoloValores As String, mFinal As String, Sp_Folio As String, Sp_FolioNro As Long
    Dim MesContable, AnoContable As String
    Dim Lp_ID_Compra, Sm_Plazo As String
    Dim Sp_NombreProveedor As String * 50
    Dim cnc As New ADODB.Connection
    Dim rsc As New ADODB.Recordset
    Dim Sp_Folio2 As String
                                    
    'Items detalle
    Dim Lp_Id_Detalle As Long
    Dim Ip_CenId As Long, Ip_PlaId As Long, Ip_GasId As Long, Ip_AreId As Long
    
    ' cursor del lado del cliente
    cnc.CursorLocation = adUseClient
    ' abre el archivo de texto datos.txt ubicado en el app.path
    cnc.Open "Provider=Microsoft.Jet.OLEDB.4.0;" _
                     & "Data Source=" & App.Path & ";" _
                    & "Extended Properties='text;FMT=Delimited'"
      
    ' enlaza el recordset con el DataGrid
    MesContable = UCase(MonthName(IG_Mes_Contable))
    AnoContable = IG_Ano_Contable
    TxtExento = 0
    
    If Me.TxtCodigoCuenta = "" Then
        MsgBox "Seleccione cuenta para importar compras..." & vbNewLine & vbNewLine & "Nota: Puede modificar cuentas posteriormente a la importacion si fuera necesario", vbInformation
        TxtCodigoCuenta.SetFocus
        Exit Sub
    
    End If
    
    If MsgBox("Est� seguro de agregar esta importaci�n al periodo contable:" & vbNewLine & MesContable & "-" & AnoContable, vbYesNo + vbQuestion) = vbNo Then Exit Sub
    
 
    
    Sql = "SELECT bod_id " & _
            "FROM par_bodegas " & _
            "WHERE rut_emp='" & SP_Rut_Activo & "'"
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        Sp_Bodega = RsTmp!bod_id
    Else
        Sp_Bodega = 1
    End If
        
    
    ''folio de compra
    Sql = "SELECT IFNULL(MAX(com_folio)+1,1) folio " & _
          "FROM com_doc_compra " & _
          "WHERE rut_emp='" & SP_Rut_Activo & "' AND ano_contable=" & IG_Ano_Contable & " AND  mes_contable=" & IG_Mes_Contable
    Consulta RsTmp2, Sql
    If RsTmp2.RecordCount > 0 Then
        Sp_FolioNro = RsTmp2!Folio
        Sp_Folio = RsTmp2!Folio
        SG_codigo2 = Empty
        Sp_Folio = Right("00" & IG_Mes_Contable, 2) & Right("000" & Sp_Folio, 3)
    End If
     
     On Error GoTo problemaImportar
    ff = FreeFile 'Sets to next available file number
    With CommonDialog1
        .FileName = ""
        .Filter = "All files (*.*) |*.*|" 'Sets the filter
        FrmLoad.Visible = False
        .ShowOpen
    End With
    txtXmlBasico.Text = CommonDialog1.FileName
    If Len(txtXmlBasico.Text) > 0 Then
        FrmLoad.Visible = True
        DoEvents
            ' Ejecuta la consulta sql para llenar el recordset
        FileCopy txtXmlBasico, "C:\FACTURAELECTRONICA\compras.txt"
        
        
        
        Reemplazar_Texto "C:\FACTURAELECTRONICA\compras.txt", ",", "."
        
        Reemplazar_Texto "C:\FACTURAELECTRONICA\compras.txt", "'", "."
        
        
        rsc.Open "Select * from C:\FACTURAELECTRONICA\compras.txt", cnc, adOpenStatic
    
        If rsc.RecordCount = 0 Then
            MsgBox "No se puedieron cargar los registros...", vbInformation
            FrmLoad.Visible = False
            Exit Sub
        End If
        sp_folios2 = "0"
        Ip_PlaId = Me.TxtCodigoCuenta
      '  Open txtXmlBasico For Input As ff
      '      Do While Not EOF(ff)
            rsc.MoveFirst
            Do While Not rsc.EOF
                
                Sp_infoLinea = "" & rsc.Fields.Item(0)
                If Len(Sp_infoLinea) = 0 Then
                    GoTo finimportacion
                End If
                If InStr(1, Sp_infoLinea, ";") > 0 Then
                        xxxx = Split(Sp_infoLinea, ";")
                     '   i = 0
                     '   linea = xxxx(i)
                  '  If linea <> "Linea" Then
                     '   For i = 0 To UBound(xxxx)
                            'Me.Print xxxx(i)
                            Rut = xxxx(3)
                            If Rut = "76833720-9" Then
                                MsgBox "x"
                            End If
                            
                            Sp_NombreProveedor = xxxx(4)
                            If Rut = "" Then
                               MsgBox "Importacion Completada...", vbInformation + vbOKOnly
                                Me.TxtCodigoCuenta = ""
                                Me.TxtNombre = ""
                                CargaDatos
                                Exit Sub
                            End If
                            Dim Respuesta, textorut As String
                            textorut = Rut
                            Respuesta = ""
                            Respuesta = VerificaRut(textorut, NuevoRut)
                            Rut = NuevoRut
                            
                            
                            
                            
                          
                            If Len(Rut) > 0 Then
                                  ''   nombre = xxxx(i)
                                  ''Inicia Graba proveedor si no existe
                                    Sql = ""
                                    Sql = "SELECT rut_proveedor " & _
                                    "FROM maestro_proveedores " & _
                                    "WHERE rut_proveedor='" & Rut & "'"
        
                                    Consulta RsTmp, Sql
                                    
                                    If RsTmp.RecordCount > 0 Then
                                    'Codigo ingresado ha sido encontrado
        
                                    Else
                                        Sql = ""
                                         Sql = "INSERT INTO maestro_proveedores (rut_proveedor,nombre_empresa,direccion,comuna,ciudad," & _
                                                        "fono,fax,email,habilitado,prv_distribuidor_de_gas,ban_id,prv_ctacte) VALUES ('" & _
                                         Rut & "','" & Sp_NombreProveedor & "','" & "NN" & "','" & "NN" & "','" & "NN" & "','" & "F" & "','" & _
                                         "S/Fax" & "','" & "@" & "','" & "NN" & "','" & "NN" & "','" & 0 & "','" & "NN" & "')"
                                        cn.Execute Sql
                                    End If
'''                        ''Fin Graba Proveedor si no existe
                             
                            End If
                           ' If i = 0 Then
                           ' '    Lp_ID_Compra = xxxx(i)
                           ' End If
                            Tipo = xxxx(1)
                            Folio = xxxx(5)
                            Fecha = xxxx(6)
                            
                            If Val(Sp_Folio2) <> Val(Folio) Then
                                Lp_ID_Compra = UltimoNro("com_doc_compra", "id")
                            End If
                            Sp_Folio2 = Folio
                            
                            
                            
                            montoExento = Val(xxxx(9))
                            montoNeto = Val(xxxx(10))
                            montoIva = Val(xxxx(11)) + Val(xxxx(12))
                            monto = Val(xxxx(14))
                            
                            If Lp_ID_Compra = "" Then
                                Sql = "SELECT id " & _
                                "FROM com_impuestos "
                                Consulta RsTmp2, Sql
                                If RsTmp2.RecordCount > 0 Then
                                    Lp_ID_Compra = RsTmp2!ID
                                End If
                            
                            End If
                            
                            

                            ''20-09-2017 TPuros, TCigarrillos, TElaborados
                            
                            
                            ''Codigo Tabacos Puros
                                TPuros = Val("" & xxxx(20))
                            
                            
                            ''Valor Tabacos Cigarrillos
                            TCigarrillos = 0
                            TCigarrillos = Val("" & xxxx(21))
                            'If Val(xxxx(13)) = 9 Then
                            '    montoExento = xxxx(14)
                            '    montoNeto = 0
                            '    montoIva = 0
                            '    TCigarrillos = Val("" & xxxx(12))
                            'End If
                            If TCigarrillos > 0 Then
                                montoExento = xxxx(14)
                                montoNeto = 0
                                montoIva = 0
                            End If
                            
                            ''Tasa Tabacos Elaborados
                                TElaborados = Val("" & xxxx(22))
                             ''Inserta impuestos
                            
                            ''20-09-2017
                            
                            
                            
                            ''Codigo Otro Impuesto
                                CodOImp = Val("" & xxxx(24))
                             
                            
                            
                            'If i = 14 Then
                                montoNetoActivoFijo = Val(xxxx(15))
                               ' If montoNetoActivoFijo > 0 Then
                               '      Sql = ""
                               '      Sql = "INSERT INTO com_impuestos(id,imp_id,coi_valor) VALUES (" & _
                               '       Lp_ID_Compra & ",'" & 1111 & "'," & CDbl(montoNetoActivoFijo) & ")"
                               '
                               '    ''  Sql = "INSERT INTO sis_eliminacion_documentos (id,eli_fecha,eli_motivo,usu_login,eli_ven_com) VALUES (" & _
                               ''    ''  LvDetalle.SelectedItem & ",'" & Fql(Date) & "','" & UCase(X) & "','" & RsTmp3!usu_login & "','COM')"
                                '   cn.Execute Sql
                                'End If
                            'End If
                            
                            
                            
                          '  If i = 24 Then
                            ''Valor Otro Impuesto
                                     ValOImp = Val("" & xxxx(25))
                                    'Impuestos adicionales
                                    ''If SkTotalAdicionales > 0 Then
                                    If CodOImp > 0 Then
                                        ''If CodOImp > 0 And Tipo <> 61 Then
                                        '' Id Lp_ID_Compra
                                            '11-20-2017 _
                                            Se debe buscar el "imp_id" del impuesto,
                                            
                                            If SG_BaseDato = "alvaro_flor" Then
                                                    Sql = "SELECT imp_id " & _
                                                            "FROM par_impuestos " & _
                                                            "WHERE imp_obs=" & CodOImp
                                                    Consulta RsTmp, Sql
                                                    If RsTmp.RecordCount > 0 Then
                                                            
                                                           Sql = "INSERT INTO com_impuestos(id,imp_id,coi_valor) VALUES (" & _
                                                           Lp_ID_Compra & "," & RsTmp!imp_id & "," & CDbl(ValOImp) & ")"
                                                          
                                                        ''  Sql = "INSERT INTO sis_eliminacion_documentos (id,eli_fecha,eli_motivo,usu_login,eli_ven_com) VALUES (" & _
                                                        ''  LvDetalle.SelectedItem & ",'" & Fql(Date) & "','" & UCase(X) & "','" & RsTmp3!usu_login & "','COM')"
                                                            cn.Execute Sql
                                                    End If
                                            Else
                                                    Sql = "SELECT imp_id " & _
                                                            "FROM par_impuestos " & _
                                                            "WHERE imp_obs=" & CodOImp
                                                    Consulta RsTmp, Sql
                                                    If RsTmp.RecordCount > 0 Then
                                                            
                                                           Sql = "INSERT INTO com_impuestos(id,imp_id,coi_valor) VALUES (" & _
                                                           Lp_ID_Compra & "," & RsTmp!imp_id & "," & CDbl(ValOImp) & ")"
                                                          
                                                        ''  Sql = "INSERT INTO sis_eliminacion_documentos (id,eli_fecha,eli_motivo,usu_login,eli_ven_com) VALUES (" & _
                                                        ''  LvDetalle.SelectedItem & ",'" & Fql(Date) & "','" & UCase(X) & "','" & RsTmp3!usu_login & "','COM')"
                                                            cn.Execute Sql
                                                    End If
                                            End If
                                    End If
                           ' End If
                            ''montoIva, CodOImp, ValOImp, TasaOImp, , ,
                            If TPuros > 0 And Tipo <> 61 Then
                            '' Id TPuros
                                Sql = ""
                                Sql = "INSERT INTO com_impuestos(id,imp_id,coi_valor) VALUES (" & _
                                 Lp_ID_Compra & ",'" & 5 & "'," & CDbl(TPuros) & ")"
                                cn.Execute Sql
                            End If
                            If TCigarrillos > 0 And Tipo <> 61 Then
                            '' Id TCigarrillos
                                Sql = ""
                          '      Sql = "INSERT INTO com_impuestos(id,imp_id,coi_valor) VALUES (" & _
                          '       Lp_ID_Compra & ",'" & 9 & "'," & CDbl(TCigarrillos) & ")"
                           '     cn.Execute Sql
                            End If
                            If TElaborados > 0 And Tipo <> 61 Then
                            '' Id TElaborados
                                Sql = ""
                                Sql = "INSERT INTO com_impuestos(id,imp_id,coi_valor) VALUES (" & _
                                 Lp_ID_Compra & ",'" & 7 & "'," & CDbl(TElaborados) & ")"
                                cn.Execute Sql
                          
                            End If
                            If i = 26 Then
                            ''Tasa Otro Impuesto
                             TasaOImp = Val("" & xxxx(26))
                             ''Inserta impuestos
                        ''    Sql = ""
                        ''        Sql = "INSERT INTO com_impuestos(id,imp_id,coi_valor) VALUES (" & _
                        ''         Lp_ID_Compra & "," & CodOImp & "," & CDbl(ValOImp) & ")"
                        ''    cn.Execute Sql
                            End If

                      '   Next i
                         
                        Dim Id_Doc As String
                        ''If Tipo = "FACTURA ELECTRONICA" Then
                        If Tipo = "33" Or Tipo = "30" Then
                        
                            ''obtiene codigo
                            Sql = "SELECT doc_id, doc_nombre " & _
                                  "FROM sis_documentos " & _
                                  "WHERE doc_documento='COMPRA' AND doc_cod_sii =" & Tipo
                            Consulta RsTmp2, Sql
                            If RsTmp2.RecordCount > 0 Then
                                Id_Doc = RsTmp2!doc_id
                                nombre_sii = RsTmp2!doc_nombre
                            End If
                        
                        End If
                        ''If Tipo = "NOTA DE CREDITO ELECTRONICA" Then
                        If Tipo = "61" Then
                        ''obtiene codigo
                            Sql = "SELECT doc_id, doc_nombre " & _
                                  "FROM sis_documentos " & _
                                  "WHERE doc_documento='COMPRA' AND doc_cod_sii =61"
                            Consulta RsTmp2, Sql
                            If RsTmp2.RecordCount > 0 Then
                                Id_Doc = RsTmp2!doc_id
                                nombre_sii = RsTmp2!doc_nombre
                            End If
                        End If
                        ''If Tipo = "FACTURA ELECTRONICA EXENTA" Then
                        If Tipo = "34" Then
                              ''obtiene codigo
                            Sql = "SELECT doc_id, doc_nombre " & _
                                  "FROM sis_documentos " & _
                                  "WHERE doc_documento='COMPRA' AND doc_cod_sii =34"
                            Consulta RsTmp2, Sql
                            If RsTmp2.RecordCount > 0 Then
                                Id_Doc = RsTmp2!doc_id
                                nombre_sii = RsTmp2!doc_nombre
                            End If
                        End If
                        ''Obtine Montos
                     ''   montoNeto = Round(CDbl(monto) / 1.19, 0)
                     ''   montoIva = NumFormat(CDbl(monto) - CDbl(montoNeto))

                        ''Inserta Linea a Linea
                        
                           If Folio = "" Then
                            ''                              MsgBox "Importacion Completada...", vbInformation + vbOKOnly
                            ''   Me.TxtCodigoCuenta = ""
                            ''   Me.TxtNombre = ""
                               CargaDatos
                            ''                        ''    MsgBox "Seleccione cuenta para importar compras..." & vbNewLine & vbNewLine & "Nota: Puede modificar cuentas posteriormente a la importacion si fuera necesario", vbInformation
                            Exit Sub
                        End If
                        
                       

                        
                        Sql = ""
                        Sql = "SELECT id " & _
                                "FROM com_doc_compra " & _
                                "WHERE rut='" & Rut & "' AND no_documento=" & Folio & " AND doc_id=" & Id_Doc & " AND rut_emp='" & SP_Rut_Activo & "'"
                        Consulta RsTmp, Sql
                        If RsTmp.RecordCount = 0 Then
                            Sql = "SELECT IFNULL(MAX(com_folio)+1,1) folio " & _
                                  "FROM com_doc_compra " & _
                                  "WHERE rut_emp='" & SP_Rut_Activo & "' AND ano_contable=" & IG_Ano_Contable & " AND  mes_contable=" & IG_Mes_Contable
                            Consulta RsTmp2, Sql
                            If RsTmp2.RecordCount > 0 Then
                                Sp_FolioNro = RsTmp2!Folio
                                Sp_Folio = RsTmp2!Folio
                                SG_codigo2 = Empty
                                Sp_Folio = Right("00" & IG_Mes_Contable, 2) & Right("000" & Sp_Folio, 3)
                            End If
                            
                            
                            Sql = "INSERT INTO com_doc_compra (rut,nombre_proveedor,documento,no_documento,pago,com_exe_otros_imp,neto,iva,total," & _
                                "fecha,doc_id,mes_contable,ano_contable,tipo_movimiento,id_ref,inventario,iva_retenido," & _
                                "doc_fecha_vencimiento,doc_factura_guias,rut_emp,com_folio,com_folio_texto,bod_id) " & _
                                "VALUES ('" & Rut & "','" & Mid(Sp_NombreProveedor, 1, 50) & "','" & UCase(nombre_sii) & "'," & Folio & ",'CONTADO'," & _
                                CDbl(montoExento) & "," & CDbl(montoNeto) & "," & CDbl(montoIva) & "," & CDbl(monto) & ",'" & Fql(Fecha) & "'," & _
                                Id_Doc & "," & IG_Mes_Contable & "," & IG_Ano_Contable & _
                                ",'COMPRA PRODUCTO'," & 0 & ",'" & "NN" & "',0,'" & _
                                Fql(Date) & "','NO','" & SP_Rut_Activo & "'," & Sp_FolioNro & ",'" & Sp_Folio & "'," & Sp_Bodega & ")"
                            cn.Execute Sql

                            ' For i = 1 To LvCuentaContable.ListItems.Count
                              Sql = ""
                              
                              'Activo inmovilizado
                              codigoaim = 0
                              ctactacta = Ip_PlaId
                               ctaorigen = ctactacta
                            If Val(montoNetoActivoFijo) > 0 Then
                                    ctactacta = 100112
                                    codigoaim = UltimoNro("con_activo_inmovilizado", "aim_id")
                                      Sql = "INSERT INTO con_activo_inmovilizado (aim_id,pla_id,aim_nombre,aim_fecha_ingreso,aim_vida_util,aim_tipo_depreciacion,aim_nuevo,aim_valor,aim_dacumu,rut_emp,aim_fecha_adquisicion) " & _
                                    "VALUES(" & codigoaim & "," & 100112 & ",'ACTIVO FIJO','" & Fql(Date) & "'," & 1 & ",'" & 1 & "','SI'," & CDbl(montoNetoActivoFijo) & "," & 0 & ",'" & SP_Rut_Activo & "','" & Fql(Date) & "')"
                                    cn.Execute Sql
                            End If
                            
                            If Val(TCigarrillos) > 0 Then
                                ctaorigen = ctactacta
                                ctactacta = 100113
                            
                            End If

                              
                            Sql = "INSERT INTO com_doc_compra_detalle (cmd_id,id,pro_codigo,cmd_cantidad,cmd_unitario_neto,cmd_unitario_bruto, " & _
                            "cmd_total_neto,cmd_total_bruto,pla_id,cen_id,gas_id,are_id,cmd_detalle,cmd_costo_real_neto,cmd_exento,cmd_nro_linea,aim_id) VALUES "
                            
                            Lp_Id_Detalle = UltimoNro("com_doc_compra_detalle", "cmd_id")
                            Ip_CenId = 99999
                            
                            Ip_GasId = 99999
                            Ip_AreId = 99999
                            
                            Sql = Sql & "(" & Lp_Id_Detalle & "," & Lp_ID_Compra & "," & codigoaim & ",1," & CDbl(montoNeto) & "," & _
                            0 & "," & CDbl(montoNeto) & "," & 0 & "," & ctactacta & "," & Ip_CenId & "," & Ip_GasId & "," & Ip_AreId & ",'" & _
                            1 & "'," & CDbl(montoNeto) & "," & CDbl(montoExento) & "," & 1 & "," & codigoaim & ")"
    
                            cn.Execute Sql
                             ctactacta = ctaorigen

                    Else
                        'MsgBox "Documento ya est� registrado:" & vbNewLine & vbNewLine & Rut & " " & nombre & " " & Tipo & " " & Folio
                        
                    
                    End If
                    rsc.MoveNext
                End If
                
            Loop
finimportacion:
        
       rsc.Close
    End If
    
   FrmLoad.Visible = False
   CargaDatos
   MsgBox "Importacion Completada...", vbInformation + vbOKOnly
   Me.TxtCodigoCuenta = ""
   Me.TxtNombre = ""
   Exit Sub
problemaImportar:
    MsgBox "Problema al importar..." & vbNewLine & Err.Description & " Nro:" & Err.Number
    
   
End Sub
Private Sub CmdNueva_Click()
    DG_ID_Unico = 0
    
    If Sp_extra = "GASTOS" Then
        If Principal.CmdMenu(8).Visible = True Then
            If ConsultaCentralizado("4", IG_Mes_Contable, IG_Ano_Contable) Then
                MsgBox "periodo contable ya ha sido centralizado, " & vbNewLine & "No puede modificar"
                Exit Sub
            End If
        End If
    ElseIf Sp_extra = "HONO" Then
        
    
    
    Else
    'MARIO
            If Principal.CmdMenu(8).Visible = True Then
                If ConsultaCentralizado("1,11", IG_Mes_Contable, IG_Ano_Contable) Then
                    MsgBox "periodo contable ya ha sido centralizado, " & vbNewLine & "No puede modificar"
                    Exit Sub
                End If
            End If
    End If
    
    
   ' foto.Visible = True
'    Me.Hide
 
    If Sp_extra = "GASTOS" Then
    
        ComBoletasCompra.Show 1
    ElseIf Sp_extra = "HONO" Then
        com_boletashonorarios.Show 1
    Else
        compra_Ingreso.Show 1
        Unload compra_detalle
    End If
 '   Me.Show
    Me.SetFocus
   ' foto.Visible = False
  
    CargaDatos
End Sub
Private Sub CmdSalir_Click()
    Unload Me
End Sub

Private Sub CmdSeleccionar_Click()
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    DG_ID_Unico = LvDetalle.SelectedItem.Text
    'Me.Hide
    If Sp_extra = "GASTOS" Then
        ComBoletasCompra.Show 1
    ElseIf Sp_extra = "HONO" Then
        
        com_boletashonorarios.Show 1
    Else
        compra_Visualizar.Lp_ID_Compra = DG_ID_Unico
        compra_Visualizar.Show 1
    End If
    'Me.Show
    Me.SetFocus
    'Unload compra_detalle
   ' CargaDatos
End Sub

Private Sub CmdTodos_Click()
    Sp_Condicion = ""
    TxtNroDocumento = ""
    CargaDatos
End Sub

Private Sub ComAno_Click()
    CargaDatos
End Sub

Private Sub comMes_Click()
    CargaDatos
End Sub
Private Sub Form_KeyUp(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF1 Then CmdNueva_Click
    If KeyCode = vbKeyF2 Then CmdSeleccionar_Click
    If KeyCode = vbKeyF3 Then CmdExportar_Click
End Sub
Private Sub Form_Load()
    Centrar Me
    SkEmpresaActiva = "EMPRESA ACTIVA: " & SP_Empresa_Activa
    Skin2 Me, , 4
    Sp_Condicion = ""
    For i = 1 To 12
        comMes.AddItem UCase(MonthName(i, False))
        comMes.ItemData(comMes.ListCount - 1) = i
    Next
    'if ig_mes_contable =
    Busca_Id_Combo comMes, Val(IG_Mes_Contable)
    
    If comMes.ListIndex = -1 Then
        comMes.ListIndex = Val(IG_Mes_Contable) - 1
    End If
    LLenaYears ComAno, 2010
    
    
    Sql = "SELECT par_valor " & _
            "FROM tabla_parametros " & _
            "WHERE par_id=4000"
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        If RsTmp!par_valor = "SI" Then
            Me.Frame3.Visible = True
            Me.Height = 10245
        End If
    End If
   ' For i = Year(Date) To 2010 Step -1
   '     ComAno.AddItem i
   '     ComAno.ItemData(ComAno.ListCount - 1) = i
   ' Next
   ' Busca_Id_Combo ComAno, Val(IG_Ano_Contable)
    'For i = 0 To ComAno.ListCount - 1
    '    If ComAno.List(i) = IG_Ano_Contable Then
    '        ComAno.ListIndex = i
    '        Exit For
    '    End If
    'Next
 
    
    Sql = "SELECT men_infoextra " & _
          "FROM sis_menu " & _
          "WHERE men_id=" & IP_IDMenu
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        Sp_extra = RsTmp!men_infoextra
    End If
    
   ' ComAno.ListIndex = 0
    Sp_CondicionGasto = " AND d.doc_libro_gastos='NO' AND doc_honorarios='NO' "
    If Sp_extra = "GASTOS" Then
        'estamos llamando el formulario desde el menu GASTOS MENORES 22 OCTRUBRE 2011
        CmdCompraSimple.Visible = False
        Sm_LibroGastos = "SI"
        Sp_CondicionGasto = " AND d.doc_libro_gastos='SI' AND doc_honorarios='NO' "
        Me.Caption = "GASTOS MENORES"
    ElseIf Sp_extra = "HONO" Then
        CmdCompraSimple.Visible = False
        Me.Caption = "BOLETAS DE HONORARIOS"
        Me.LvDetalle.ColumnHeaders(2).Width = 1
        Me.LvDetalle.ColumnHeaders(7).Width = 1
        Me.LvDetalle.ColumnHeaders(8).Text = "a Pagar"
        Sp_CondicionGasto = " AND doc_honorarios='SI' "
        CmdSeleccionar.Enabled = False
        cmdFolio.Enabled = False
    Else
    
    End If
    
    
    
    CargaDatos
    
End Sub
Private Sub CargaDatos()
    If comMes.ListIndex = -1 Then comMes.ListIndex = Month(IG_Mes_Contable + 1)
    If Val(TxtNroDocumento) = 0 Then
        Sm_Numero = ""
    Else
        Sm_Numero = " AND no_documento IN(" & TxtNroDocumento & ") "
    End If
    
    FrmLoad.Visible = True
    DoEvents
    
    Sql = "SELECT c.id,com_folio_texto,DATE_FORMAT(fecha,'%d-%m-%Y')fecha,p.nombre_empresa,CONCAT(doc_nombre,' ',IF( doc_factura_guias='SI','{ Por Guia(s) }',''),IF(nro_factura>0,' {FACTURADA} ','')),no_documento,tipo_movimiento,total * IF(doc_signo_libro='-',-1,1) ,c.doc_id,rut,iva,neto  " & _
          "FROM com_doc_compra c " & _
          "INNER JOIN sis_documentos d USING(doc_id) " & _
          "INNER JOIN maestro_proveedores p ON c.rut=p.rut_proveedor " & _
          "WHERE com_informa_compra='SI' AND  com_estado_oc='PROCESO' AND c.rut_emp='" & SP_Rut_Activo & "' AND mes_contable =" & comMes.ItemData(comMes.ListIndex) & " AND ano_contable =" & IIf(ComAno.ListCount = 0, Year(Date), ComAno.Text) & Sp_Condicion & Sp_CondicionGasto & Sm_Numero
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvDetalle, False, True, True, False
    If Sp_extra = "HONO" Then
        If LvDetalle.ListItems.Count > 0 Then
            LvDetalle.ListItems.Add , , ""
            LvDetalle.ListItems(LvDetalle.ListItems.Count).SubItems(7) = NumFormat(TotalizaColumna(LvDetalle, "total"))
            LvDetalle.ListItems(LvDetalle.ListItems.Count).SubItems(10) = NumFormat(TotalizaColumna(LvDetalle, "retencion"))
            LvDetalle.ListItems(LvDetalle.ListItems.Count).SubItems(11) = NumFormat(TotalizaColumna(LvDetalle, "totalb"))
            LvDetalle.ListItems(LvDetalle.ListItems.Count).ListSubItems(7).Bold = True
            LvDetalle.ListItems(LvDetalle.ListItems.Count).ListSubItems(10).Bold = True
            LvDetalle.ListItems(LvDetalle.ListItems.Count).ListSubItems(11).Bold = True
        End If
    End If
    
    FrmLoad.Visible = False
End Sub
Private Sub LvDetalle_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    ordListView ColumnHeader, Me, LvDetalle
End Sub
Private Sub LvDetalle_DblClick()
    If Sp_extra = "HONO" Then Exit Sub
    CmdSeleccionar_Click
End Sub



Private Sub TxtBusqueda_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
     If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtBusqueda_Validate(Cancel As Boolean)
TxtBusqueda = Replace(TxtBusqueda, "'", "")
End Sub

Private Sub TxtNroDocumento_Change()
    CargaDatos
End Sub



Private Sub TxtNroDocumento_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
     If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtNroDocumento_Validate(Cancel As Boolean)
TxtNroDocumento = Replace(TxtNroDocumento, "'", "")
End Sub
Private Sub Reemplazar_Texto(ByVal El_Archivo As String, _
                            ByVal La_cadena As String, _
                            ByVal Nueva_Cadena As String)
  
On Error GoTo errSub
Dim F As Integer
Dim Contenido As String
  
      
    F = FreeFile
      
    'Abre el archivo para leer los datos
    Open El_Archivo For Input As F
      
    'carga el contenido del archivo en la variable
    Contenido = Input$(LOF(F), #F)
      
    'Cierra el archivo
    Close #F
      
    ' Ejecuta la funci�n Replace, pasandole los datos
    Contenido = Replace(Contenido, La_cadena, Nueva_Cadena)
  
      
    F = FreeFile
    'Abre un nuevo archivo
    Open El_Archivo For Output As F
    'Graba los nuevos datos
    Print #F, Contenido
      
    'cierra el archivo
    Close #F
      
   ' MsgBox " Archivo modificado ", vbInformation
Exit Sub
  
'Error
  
errSub:
MsgBox Err.Description, vbCritical
Close
End Sub

