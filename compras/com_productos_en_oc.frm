VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form com_productos_en_oc 
   Caption         =   "Productos en OC"
   ClientHeight    =   8835
   ClientLeft      =   4065
   ClientTop       =   1785
   ClientWidth     =   14310
   LinkTopic       =   "Form1"
   ScaleHeight     =   8835
   ScaleWidth      =   14310
   Begin VB.Timer Timer1 
      Interval        =   50
      Left            =   1365
      Top             =   6870
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   180
      OleObjectBlob   =   "com_productos_en_oc.frx":0000
      Top             =   6840
   End
   Begin VB.Frame Frame1 
      Caption         =   "Seleccione los Productos de su factura"
      Height          =   8385
      Left            =   375
      TabIndex        =   0
      ToolTipText     =   "Solo si es nota de credito/debito"
      Top             =   330
      Width           =   14940
      Begin VB.CheckBox ChkTodos 
         Caption         =   "Check1"
         Height          =   195
         Left            =   315
         TabIndex        =   5
         Top             =   1080
         Width           =   240
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   240
         Left            =   2385
         OleObjectBlob   =   "com_productos_en_oc.frx":0234
         TabIndex        =   4
         Top             =   345
         Width           =   1875
      End
      Begin VB.TextBox TxtBusquedaCodigo 
         Height          =   315
         Left            =   2250
         TabIndex        =   3
         ToolTipText     =   "Despues de ingresar presione enter."
         Top             =   600
         Width           =   1650
      End
      Begin VB.CommandButton CmdPasar 
         Caption         =   "Pasar Seleccionados"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   600
         Left            =   285
         TabIndex        =   2
         Top             =   7710
         Width           =   2520
      End
      Begin MSComctlLib.ListView LvDetalle 
         Height          =   6615
         Left            =   285
         TabIndex        =   1
         Top             =   1050
         Width           =   14385
         _ExtentX        =   25374
         _ExtentY        =   11668
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   0   'False
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   23
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "N109"
            Text            =   "Nro Orden"
            Object.Width           =   529
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "T1000"
            Text            =   "Codigo Empresa"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "T1000"
            Text            =   "Codigo Proveedor"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Object.Tag             =   "T1000"
            Text            =   "Codigo"
            Object.Width           =   2328
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Object.Tag             =   "T3000"
            Text            =   "Descripcion"
            Object.Width           =   6703
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Object.Tag             =   "N102"
            Text            =   "Tipo"
            Object.Width           =   3881
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   6
            Object.Tag             =   "N102"
            Text            =   "Precio"
            Object.Width           =   2646
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   7
            Object.Tag             =   "N102"
            Text            =   "Cant."
            Object.Width           =   2293
         EndProperty
         BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   8
            Object.Tag             =   "T1000"
            Text            =   "U.Medida"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   9
            Key             =   "total"
            Object.Tag             =   "N100"
            Text            =   "Total Neto"
            Object.Width           =   2646
         EndProperty
         BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   10
            Object.Tag             =   "N109"
            Text            =   "Cuenta"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(12) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   11
            Object.Tag             =   "N109"
            Text            =   "CentroCosto"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(13) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   12
            Object.Tag             =   "N109"
            Text            =   "gasid"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(14) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   13
            Object.Tag             =   "N109"
            Text            =   "Area"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(15) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   14
            Object.Tag             =   "N102"
            Text            =   "Factor Dscto"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(16) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   15
            Object.Tag             =   "N102"
            Text            =   "Valor Dscto"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(17) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   16
            Object.Tag             =   "N102"
            Text            =   "Unitario Con Dscto"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(18) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   17
            Object.Tag             =   "N100"
            Text            =   "Total Linea C/dscto"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(19) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   18
            Object.Tag             =   "N102"
            Text            =   "CostoLinea"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(20) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   19
            Object.Tag             =   "N102"
            Text            =   "Unitario/Final"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(21) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   2
            SubItemIndex    =   20
            Object.Tag             =   "T500"
            Text            =   "Inventario"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(22) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   21
            Object.Tag             =   "N109"
            Text            =   "Ord Seleccion"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(23) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   22
            Object.Tag             =   "N109"
            Text            =   "x"
            Object.Width           =   0
         EndProperty
      End
   End
End
Attribute VB_Name = "com_productos_en_oc"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public Lm_id_Compra As Long
Public Sm_Codigo_Proveedor As String

Private Sub ChkTodos_Click()
    If ChkTodos.Value = 1 Then
        For i = 1 To LvDetalle.ListItems.Count
            LvDetalle.ListItems(i).Checked = True
        Next
    Else
        For i = 1 To LvDetalle.ListItems.Count
            LvDetalle.ListItems(i).Checked = False
        Next
    End If
            
End Sub

Private Sub CmdPasar_Click()
    SG_codigo3 = ""
    For H = 1 To LvDetalle.ListItems.Count
        If LvDetalle.ListItems(H).Checked Then
            SG_codigo3 = SG_codigo3 & LvDetalle.ListItems(H) & ","
            Sql = "UPDATE com_doc_compra_detalle " & _
                    "SET cmd_seleccion_oc=" & Val(LvDetalle.ListItems(H).SubItems(21)) & " " & _
                    "WHERE cmd_id=" & LvDetalle.ListItems(H)
            cn.Execute Sql
        End If
    Next
    If Len(SG_codigo3) > 0 Then
        SG_codigo3 = Mid(SG_codigo3, 1, Len(SG_codigo3) - 1)
    End If
    
    
    
    
    Unload Me
End Sub

Private Sub Form_Load()
    Centrar Me
    Skin2 Me, , 4
    CargaDetalle
    For b = 1 To LvDetalle.ListItems.Count
        LvDetalle.ListItems(b).SubItems(21) = 0
    Next
End Sub
Private Sub CargaDetalle()
    Sql = "SELECT cmd_id,pro_codigo_interno,"
    Sql = Sql & "(SELECT cpv_codigo_proveedor FROM par_codigos_proveedor WHERE rut_proveedor='" & Sm_Codigo_Proveedor & "' AND pro_codigo=m.codigo LIMIT 1) codprov "
    Sql = Sql & ",pro_codigo,IFNULL(descripcion,cmd_detalle),tip_nombre,cmd_unitario_neto,cmd_cantidad,ume_nombre,cmd_total_neto+cmd_exento, " & _
                        "d.pla_id,d.cen_id,d.gas_id,d.are_id,0,0,0,0,0,0,pro_inventariable " & _
                      "FROM com_doc_compra_detalle d " & _
                      "INNER JOIN com_doc_compra c USING(id) " & _
                      "LEFT JOIN maestro_productos m ON m.codigo = d.pro_codigo AND m.rut_emp=c.rut_emp " & _
                      "LEFT JOIN par_tipos_productos t USING(tip_id) " & _
                      "LEFT JOIN sis_unidad_medida u USING(ume_id) " & _
                      "WHERE cmd_oc_facturada='NO' AND c.rut_emp='" & SP_Rut_Activo & "' AND d.id=" & Lm_id_Compra
                      
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvDetalle, True, True, True, False
End Sub




Private Sub LvDetalle_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    ordListView ColumnHeader, Me, LvDetalle
End Sub

Private Sub LvDetalle_ItemCheck(ByVal Item As MSComctlLib.ListItem)
    Dim Lp_NroG As Long
    Lp_NroG = 0
    For c = 1 To LvDetalle.ListItems.Count
        
        If Val(LvDetalle.ListItems(c).SubItems(21)) > Lp_NroG Then
            Lp_NroG = Val(LvDetalle.ListItems(c).SubItems(21))
        End If
    Next
    Item.SubItems(21) = Lp_NroG + 1
End Sub

Private Sub TxtBusquedaCodigo_KeyPress(KeyAscii As Integer)
    'KeyAscii = SoloNumeros(KeyAscii)
    If KeyAscii = 13 Then
            SkScan = KeyAscii
            If Len(TxtBusquedaCodigo) = 0 Then Exit Sub
            Dim ret As Long
   
             ret = Buscar(LvDetalle, TxtBusquedaCodigo, 2, True)
            
             If ret <> 0 Then
                LvDetalle.ListItems(ret).Selected = True
                LvDetalle.SetFocus
              '  LvProductos_KeyDown 13, 1
             Else
             
                    ret = Buscar(LvDetalle, TxtBusquedaCodigo, 5, True)
            
                    If ret <> 0 Then
                       LvDetalle.ListItems(ret).Selected = True
                       LvDetalle.SetFocus
                     '  LvProductos_KeyDown 13, 1
                    Else
                
                    
             
                            MsgBox "Dato no encontrado", vbExclamation
                    End If
             End If
    
    
    End If

End Sub

Function Buscar(LV As ListView, _
                Cadena As String, _
                nCol As Integer, _
                bFraseCompleta As Boolean) As Long
   
   
    Dim i As Long
    Dim iStart As Long
    Dim oItem As ListItem
    
    
    'If Lv.SelectedItem Is Nothing Then
    '   i = 1
    If Not LV.SelectedItem Is Nothing Then
       iStart = LV.SelectedItem.Index + 1
    Else
       iStart = 1
    End If
    
    With LV
    For i = 1 To LV.ListItems.Count
        
        Set oItem = LV.ListItems(i)
        
        Dim sItem As String
        
        If nCol = 0 Then
            sItem = LV.ListItems(i)
        Else
            sItem = oItem.SubItems(nCol)
        End If
        If bFraseCompleta = False Then
           Dim nPos As Integer
           
           nPos = InStr(LCase(sItem), LCase(Cadena))
           If nPos <> 0 Then
              Buscar = oItem.Index
              oItem.EnsureVisible
              Exit For
           End If
        ElseIf bFraseCompleta = True Then
             If LCase(sItem) = LCase(Cadena) Then
              Buscar = oItem.Index
              oItem.EnsureVisible
              Exit For
             End If
        End If
    Next
    End With
    
    Set LV.SelectedItem = Nothing
    
End Function

