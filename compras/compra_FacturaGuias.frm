VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form compra_FacturaGuias 
   Caption         =   "Guias pendientes de Facturacion"
   ClientHeight    =   7080
   ClientLeft      =   1935
   ClientTop       =   1575
   ClientWidth     =   13110
   LinkTopic       =   "Form1"
   ScaleHeight     =   7080
   ScaleWidth      =   13110
   Begin VB.Timer Timer1 
      Interval        =   10
      Left            =   60
      Top             =   6285
   End
   Begin VB.CommandButton CmdSalir 
      Cancel          =   -1  'True
      Caption         =   "&Retornar"
      Height          =   375
      Left            =   11670
      TabIndex        =   4
      Top             =   6600
      Width           =   1095
   End
   Begin VB.Frame Frame2 
      Caption         =   "Guias pendientes de facturacion"
      Height          =   5880
      Left            =   435
      TabIndex        =   0
      Top             =   660
      Width           =   12405
      Begin VB.CheckBox Check 
         Caption         =   "Check1"
         Height          =   195
         Left            =   300
         TabIndex        =   8
         Top             =   435
         Width           =   180
      End
      Begin VB.TextBox txtTotalNeto 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         Height          =   315
         Left            =   8070
         Locked          =   -1  'True
         TabIndex        =   7
         TabStop         =   0   'False
         Tag             =   "N"
         Text            =   "0"
         Top             =   5190
         Width           =   1185
      End
      Begin VB.TextBox TxtIVA 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         Height          =   315
         Left            =   9255
         Locked          =   -1  'True
         TabIndex        =   6
         TabStop         =   0   'False
         Tag             =   "N"
         Text            =   "0"
         Top             =   5190
         Width           =   1185
      End
      Begin VB.TextBox TxtTotal 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         Height          =   315
         Left            =   10440
         Locked          =   -1  'True
         TabIndex        =   3
         TabStop         =   0   'False
         Tag             =   "N"
         Text            =   "0"
         Top             =   5190
         Width           =   1185
      End
      Begin VB.CommandButton cmdFacturar 
         Caption         =   "&Guardar Factura por Guias"
         Height          =   375
         Left            =   240
         TabIndex        =   2
         Top             =   5235
         Width           =   2730
      End
      Begin MSComctlLib.ListView LvDetalle 
         Height          =   4725
         Left            =   270
         TabIndex        =   1
         Top             =   390
         Width           =   11835
         _ExtentX        =   20876
         _ExtentY        =   8334
         View            =   3
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   8
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "T1000"
            Object.Width           =   529
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "T1300"
            Text            =   "Fecha"
            Object.Width           =   2293
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "N109"
            Text            =   "Nro Guia"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Object.Tag             =   "T800"
            Text            =   "Rut"
            Object.Width           =   2469
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Object.Tag             =   "T3000"
            Text            =   "Nombre Proveedor"
            Object.Width           =   6703
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   5
            Object.Tag             =   "N100"
            Text            =   "Neto"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   6
            Object.Tag             =   "N100"
            Text            =   "IVA"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   7
            Object.Tag             =   "N100"
            Text            =   "Total"
            Object.Width           =   2117
         EndProperty
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel14 
         Height          =   180
         Index           =   4
         Left            =   6105
         OleObjectBlob   =   "compra_FacturaGuias.frx":0000
         TabIndex        =   5
         Top             =   5220
         Width           =   1860
      End
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   -45
      OleObjectBlob   =   "compra_FacturaGuias.frx":007E
      Top             =   7380
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkEmpresaActiva 
      Height          =   285
      Left            =   7260
      OleObjectBlob   =   "compra_FacturaGuias.frx":02B2
      TabIndex        =   9
      Top             =   120
      Width           =   5685
   End
End
Attribute VB_Name = "compra_FacturaGuias"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public Im_Id_Factura As Integer
Public Sm_TipoDoc As String, Sm_NroDoc As String, Sm_Fecha As String
Public Sm_Rsocial As String, Sm_Exento As String, Sm_Plazo As String

Private Sub Check_Click()
    For i = 1 To LvDetalle.ListItems.Count
        If LvDetalle.ListItems(i).Checked Then LvDetalle.ListItems(i).Checked = False Else LvDetalle.ListItems(i).Checked = True
    Next
    SumaGuias
End Sub

Private Sub CmdFacturar_Click()
    Dim Lp_ID_Compra As Long
    Dim sp_Guias As String
    
    If Val(TxtTotal) = 0 Then
        MsgBox "Debe seleccionar al menos una guia...", vbOKOnly + vbInformation
        Exit Sub
    End If
    sp_Guias = "(0"
    
    
    On Error GoTo FallaFacturacion
    cn.BeginTrans
  '  With compra_Ingreso ' 4 Enero 2012
     'Este el documento de compra 4-marzo - 2011
    Sql = "SELECT doc_contable,doc_orden_de_compra  " & _
          "FROM sis_documentos " & _
          "WHERE doc_id=" & Im_Id_Factura
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        'Sp_OC = RsTmp!doc_orden_de_compra
        If RsTmp!doc_contable = "SI" Then
            
              
            Sql = "SELECT IFNULL(MAX(com_folio)+1,1) folio " & _
                  "FROM com_doc_compra " & _
                  "WHERE rut_emp='" & SP_Rut_Activo & "' AND ano_contable=" & IG_Ano_Contable & " AND  mes_contable=" & IG_Mes_Contable
            Consulta RsTmp, Sql
            If RsTmp.RecordCount > 0 Then
                Sp_FolioNro = RsTmp!Folio
                Sp_Folio = RsTmp!Folio
                SG_codigo2 = Empty
                sis_InputBox.texto.PasswordChar = ""
                sis_InputBox.texto.Locked = True
                sis_InputBox.Caption = "Tome nota"
                sis_InputBox.FramBox = "Folio para libro contable"
                sis_InputBox.CmdCancelar.Visible = False
                Sp_Folio = Right("00" & IG_Mes_Contable, 2) & Right("000" & Sp_Folio, 3)
                sis_InputBox.texto = Sp_Folio
                sis_InputBox.Show 1
            End If
        End If
    End If
     
     
     
    Lp_ID_Compra = UltimoNro("com_doc_compra", "id")
    Sql = "INSERT INTO com_doc_compra (id,rut,nombre_proveedor,documento,no_documento,pago,com_exe_otros_imp,neto,iva,total," & _
                                      "fecha,doc_id,mes_contable,ano_contable,tipo_movimiento,id_ref,inventario,iva_retenido," & _
                                      "doc_fecha_vencimiento,doc_factura_guias,rut_emp,com_folio,com_folio_texto,bod_id) " & _
          "VALUES (" & Lp_ID_Compra & ",'" & RutBuscado & "','" & Sm_Rsocial & "','" & Sm_TipoDoc & "'," & Sm_NroDoc & ",'CREDITO'," & _
          CDbl(Sm_Exento) & "," & CDbl(txtTotalNeto) & "," & CDbl(TxtIva) & "," & CDbl(TxtTotal) & ",'" & Sm_Fecha & "'," & _
          Im_Id_Factura & "," & IG_Mes_Contable & "," & IG_Ano_Contable & _
          ",'COMPRA PRODUCTO'," & 0 & ",'" & "NN" & "',0,'" & _
          Sm_Plazo & "','SI','" & SP_Rut_Activo & "'," & Sp_FolioNro & ",'" & Sp_Folio & "'," & compra_Ingreso.CboBodega.ItemData(compra_Ingreso.CboBodega.ListIndex) & ")"
   ' End With
    cn.Execute Sql
  
    For i = 1 To LvDetalle.ListItems.Count
        If LvDetalle.ListItems(i).Checked Then sp_Guias = sp_Guias & "," & LvDetalle.ListItems(i)
    Next
    sp_Guias = sp_Guias & ")"
    Sql = "UPDATE com_doc_compra " & _
          "SET doc_id_factura=" & Im_Id_Factura & "," & _
          "nro_factura=" & Sm_NroDoc & ", " & _
          "id_ref=" & Lp_ID_Compra & " " & _
          "WHERE id IN " & sp_Guias
    cn.Execute Sql
  
    
    



    'Cta Cte 30 de Junio de 2011
    '28 Junio 2014 traspasado a Compras desde ventas
        'Debemos consultar los abonos que tengan estas guias y abonarlas a la factura que se esta creando
        'Solo tomar el valor
        
        Sql = "SELECT sum(abo_monto) abonos " & _
              "FROM cta_abonos a,cta_abono_documentos d " & _
              "WHERE d.rut_emp='" & SP_Rut_Activo & "' AND a.rut_emp='" & SP_Rut_Activo & "' AND a.abo_id=d.abo_id AND abo_cli_pro='PRO' AND abo_rut='" & RutBuscado & "' AND d.id IN " & sp_Guias & "  " & _
              "GROUP BY abo_rut "
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
            
            L_Abo_id = UltimoNro("cta_abonos", "abo_id")
        
            Sql = "INSERT INTO cta_abonos (abo_id,abo_cli_pro,abo_rut,abo_fecha,abo_monto,mpa_id,abo_observacion,usu_nombre,rut_emp,caj_id,abo_origen) " & _
                  "VALUES(" & L_Abo_id & ",'PRO','" & RutBuscado & "','" & Format(Sm_Fecha, "YYYY-MM-DD") & "'," & RsTmp!abonos & ",5,'Abonos en guias','" & LogUsuario & "','" & SP_Rut_Activo & "'," & LG_id_Caja & ",'FCT')"
            cn.Execute Sql
            Sql = "INSERT INTO cta_abono_documentos (abo_id,id,ctd_monto,rut_emp) " & _
                  "VALUES(" & L_Abo_id & "," & Lp_ID_Compra & "," & RsTmp!abonos & ",'" & SP_Rut_Activo & "')"
            cn.Execute Sql
            '19 Agosto 2013
            Sql = "INSERT INTO abo_tipos_de_pagos  (abo_id,mpa_id,pad_valor) " & _
                    "VALUES(" & L_Abo_id & ",5," & RsTmp!abonos & ")"
            cn.Execute Sql
            
        End If


cn.CommitTrans
    MsgBox Sm_TipoDoc & " Nro " & Sm_NroDoc & vbNewLine & " Guardado correctamente...", vbInformation
    
    Im_Id_Factura = 0
    SG_codigo2 = "grabado"
    
    
    Unload Me
    Exit Sub
FallaFacturacion:
    MsgBox "Hubo un problema al intentar facturar..." & vbNewLine & Err.Number & vbNewLine & Err.Description
    cn.RollbackTrans
End Sub

Private Sub CmdSAlir_Click()
    SG_codigo2 = ""
    Unload Me
End Sub



Private Sub Form_Load()
    Centrar Me
    SkEmpresaActiva = SP_Empresa_Activa
    Skin2 Me, , 5
    Consulta RsTmp2, Sql
    LLenar_Grilla RsTmp2, Me, LvDetalle, True, True, True, False
    
End Sub




Private Sub LvDetalle_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    ordListView ColumnHeader, Me, LvDetalle
End Sub

Private Sub LvDetalle_ItemCheck(ByVal Item As MSComctlLib.ListItem)
    SumaGuias
End Sub
Private Sub SumaGuias()
    If LvDetalle.ListItems.Count = 0 Then Exit Sub
    txtTotalNeto = 0
    TxtIva = 0
    TxtTotal = 0
    For i = 1 To LvDetalle.ListItems.Count
        If LvDetalle.ListItems(i).Checked Then
            
            txtTotalNeto = CDbl(txtTotalNeto) + CDbl(LvDetalle.ListItems(i).SubItems(5))
            TxtIva = CDbl(TxtIva) + CDbl(LvDetalle.ListItems(i).SubItems(6))
            TxtTotal = CDbl(TxtTotal) + CDbl(LvDetalle.ListItems(i).SubItems(7))
        End If
    Next
    
    txtTotalNeto = Format(txtTotalNeto, "#,##0")
    TxtIva = Format(TxtIva, "#,#")
    TxtTotal = Format(TxtTotal, "#,#")
    
    TxtTotalFacturaGuias = TxtTotal
End Sub



Private Sub Timer1_Timer()
    On Error Resume Next
    CmdFacturar.SetFocus
    Timer1.Enabled = False
End Sub
