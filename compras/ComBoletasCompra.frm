VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form ComBoletasCompra 
   Caption         =   "Boletas de compra"
   ClientHeight    =   3900
   ClientLeft      =   195
   ClientTop       =   2910
   ClientWidth     =   14940
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   3900
   ScaleWidth      =   14940
   Begin VB.Frame FrmPeriodo 
      Caption         =   "Periodo Contable actual"
      Height          =   1155
      Left            =   11865
      TabIndex        =   28
      Top             =   195
      Width           =   2250
      Begin VB.TextBox TxtMesContable 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   75
         Locked          =   -1  'True
         TabIndex        =   30
         TabStop         =   0   'False
         Text            =   "0"
         Top             =   480
         Width           =   1275
      End
      Begin VB.TextBox TxtAnoContable 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1365
         Locked          =   -1  'True
         TabIndex        =   29
         TabStop         =   0   'False
         Text            =   "0"
         Top             =   480
         Width           =   780
      End
   End
   Begin VB.CommandButton CmdSalir 
      Cancel          =   -1  'True
      Caption         =   "&Retornar"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   405
      Left            =   13665
      TabIndex        =   13
      Top             =   3165
      Width           =   1215
   End
   Begin VB.CommandButton CmdGuardar 
      Caption         =   "&Guardar"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   405
      Left            =   12345
      TabIndex        =   12
      ToolTipText     =   "Guarda documento"
      Top             =   3180
      Width           =   1215
   End
   Begin VB.Frame FrmProveedor 
      Caption         =   "Datos del documento"
      Height          =   1140
      Left            =   1140
      TabIndex        =   19
      Top             =   195
      Width           =   10530
      Begin VB.TextBox TxtRutProveedor 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   5520
         TabIndex        =   3
         Tag             =   "T"
         Top             =   480
         Width           =   1485
      End
      Begin VB.CommandButton CmdBuscaProv 
         Caption         =   "Buscar"
         Height          =   255
         Left            =   5505
         TabIndex        =   22
         Top             =   795
         Width           =   660
      End
      Begin VB.TextBox TxtRsocial 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   7005
         TabIndex        =   4
         Top             =   480
         Width           =   3405
      End
      Begin VB.CommandButton CmdNuevoProveedor 
         Caption         =   "Crear"
         Height          =   255
         Left            =   6165
         TabIndex        =   21
         Top             =   795
         Width           =   675
      End
      Begin VB.TextBox TxtNDoc 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   4305
         TabIndex        =   2
         Tag             =   "N"
         Text            =   "0"
         Top             =   480
         Width           =   1200
      End
      Begin VB.ComboBox CboTipoDoc 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         ItemData        =   "ComBoletasCompra.frx":0000
         Left            =   1440
         List            =   "ComBoletasCompra.frx":000D
         Style           =   2  'Dropdown List
         TabIndex        =   1
         ToolTipText     =   $"ComBoletasCompra.frx":0032
         Top             =   480
         Width           =   2865
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel10 
         Height          =   225
         Left            =   7005
         OleObjectBlob   =   "ComBoletasCompra.frx":00C7
         TabIndex        =   20
         Top             =   255
         Width           =   3180
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   225
         Index           =   0
         Left            =   4155
         OleObjectBlob   =   "ComBoletasCompra.frx":0136
         TabIndex        =   23
         Top             =   255
         Width           =   1290
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   225
         Index           =   0
         Left            =   1425
         OleObjectBlob   =   "ComBoletasCompra.frx":0193
         TabIndex        =   24
         Top             =   285
         Width           =   2850
      End
      Begin MSComCtl2.DTPicker DtFecha 
         Height          =   345
         Left            =   135
         TabIndex        =   0
         Top             =   480
         Width           =   1290
         _ExtentX        =   2275
         _ExtentY        =   609
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         CustomFormat    =   "dd-mm-yyyy hh:mm:ss"
         Format          =   273416193
         CurrentDate     =   39855
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   225
         Left            =   120
         OleObjectBlob   =   "ComBoletasCompra.frx":020C
         TabIndex        =   25
         Top             =   255
         Width           =   1065
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
         Height          =   225
         Left            =   5505
         OleObjectBlob   =   "ComBoletasCompra.frx":027D
         TabIndex        =   26
         Top             =   255
         Width           =   1470
      End
   End
   Begin VB.Frame FrameDatos 
      Caption         =   "Datos Contables"
      Height          =   2235
      Left            =   45
      TabIndex        =   14
      Top             =   1470
      Width           =   14835
      Begin VB.TextBox TxtNroCuenta 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   960
         TabIndex        =   5
         Tag             =   "N2"
         ToolTipText     =   "Ingrese el codigo de la cuenta"
         Top             =   240
         Width           =   945
      End
      Begin VB.Frame Frame2 
         Caption         =   "Forma de pago"
         Height          =   570
         Left            =   12900
         TabIndex        =   31
         Top             =   345
         Width           =   1785
         Begin VB.ComboBox CboFpago 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            ItemData        =   "ComBoletasCompra.frx":02F2
            Left            =   60
            List            =   "ComBoletasCompra.frx":0302
            Style           =   2  'Dropdown List
            TabIndex        =   11
            ToolTipText     =   "Seleccione Bodega"
            Top             =   195
            Width           =   1695
         End
      End
      Begin VB.CommandButton cmdCuenta 
         Caption         =   "Cuenta"
         Height          =   240
         Left            =   90
         TabIndex        =   15
         Top             =   240
         Width           =   870
      End
      Begin VB.ComboBox CboArea 
         Height          =   315
         Left            =   3360
         Style           =   2  'Dropdown List
         TabIndex        =   7
         Top             =   495
         Width           =   2685
      End
      Begin VB.ComboBox CboCuenta 
         Height          =   315
         Left            =   105
         Style           =   2  'Dropdown List
         TabIndex        =   6
         Top             =   495
         Width           =   3255
      End
      Begin VB.ComboBox CboItemGasto 
         Height          =   315
         Left            =   8700
         Style           =   2  'Dropdown List
         TabIndex        =   9
         Top             =   495
         Width           =   2400
      End
      Begin VB.ComboBox CboCentroCosto 
         Height          =   315
         Left            =   6045
         Style           =   2  'Dropdown List
         TabIndex        =   8
         Top             =   495
         Width           =   2685
      End
      Begin VB.TextBox TxtSDNeto 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   11100
         TabIndex        =   10
         Tag             =   "N"
         Text            =   "0"
         Top             =   495
         Width           =   1050
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkItem 
         Height          =   180
         Left            =   8700
         OleObjectBlob   =   "ComBoletasCompra.frx":032B
         TabIndex        =   16
         Top             =   285
         Width           =   1605
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkCC 
         Height          =   180
         Left            =   6030
         OleObjectBlob   =   "ComBoletasCompra.frx":03A3
         TabIndex        =   17
         Top             =   285
         Width           =   1800
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkArea 
         Height          =   180
         Left            =   3405
         OleObjectBlob   =   "ComBoletasCompra.frx":041F
         TabIndex        =   18
         Top             =   285
         Width           =   1905
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel5 
         Height          =   165
         Left            =   11055
         OleObjectBlob   =   "ComBoletasCompra.frx":0485
         TabIndex        =   27
         Top             =   300
         Width           =   1080
      End
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   1050
      OleObjectBlob   =   "ComBoletasCompra.frx":04EF
      Top             =   6615
   End
   Begin VB.Timer Timer1 
      Left            =   540
      Top             =   6630
   End
   Begin MSComctlLib.ListView LvDetalle 
      Height          =   3705
      Left            =   3225
      TabIndex        =   32
      Top             =   3795
      Visible         =   0   'False
      Width           =   7500
      _ExtentX        =   13229
      _ExtentY        =   6535
      View            =   3
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   0
      NumItems        =   5
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Object.Tag             =   "N109"
         Object.Width           =   617
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Object.Tag             =   "N109"
         Text            =   "NRO CHEQUE"
         Object.Width           =   2293
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Key             =   "debe"
         Object.Tag             =   "T3000"
         Text            =   "BANCO"
         Object.Width           =   5292
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   3
         Key             =   "haber"
         Object.Tag             =   "N100"
         Text            =   "DISPONIBLE"
         Object.Width           =   2469
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Object.Tag             =   "N100"
         Text            =   "Utilizar"
         Object.Width           =   2293
      EndProperty
   End
End
Attribute VB_Name = "ComBoletasCompra"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub CboCuenta_Validate(Cancel As Boolean)
    If CboCuenta.ListIndex = -1 Then Exit Sub
    TxtNroCuenta = CboCuenta.ItemData(CboCuenta.ListIndex)
    Sql = "SELECT pla_id " & _
            "FROM con_plan_de_cuentas " & _
            "WHERE tpo_id=1 AND det_id=4 AND pla_analisis='SI' AND pla_id=" & CboCuenta.ItemData(CboCuenta.ListIndex)
    Consulta RsTmp3, Sql
    If RsTmp3.RecordCount > 0 Then
        MsgBox "La cuenta seleccionada corresponde a " & vbNewLine & "Activo Inmovilizado " & vbNewLine & "Debe ingresarlos en el m�dulo de compras..."
        TxtNroCuenta = 0
        CboCuenta.ListIndex = -1
    End If
End Sub

Private Sub CmdBuscaProv_Click()
    AccionProveedor = 0
    BuscaProveedor.Show 1
    txtRutProveedor = RutBuscado
    txtRutProveedor_Validate True
End Sub



Private Sub cmdCuenta_Click()
    With BuscarSimple
        SG_codigo = Empty
        .Sm_Consulta = "SELECT pla_id, pla_nombre,tpo_nombre,det_nombre " & _
                       "FROM    con_plan_de_cuentas p,  con_tipo_de_cuenta t,   con_detalle_tipo_cuenta d " & _
                       "WHERE   p.tpo_id = t.tpo_id AND p.det_id = d.det_id "
        .Sm_CampoLike = "pla_nombre"
        .Im_Columnas = 2
        .Show 1
        If SG_codigo = Empty Then Exit Sub
        Busca_Id_Combo CboCuenta, Val(SG_codigo)
    End With
End Sub

Private Sub CmdGuardar_Click()
     Dim Ip_CenId As Long, Ip_PlaId As Long, Ip_GasId As Long, Ip_AreId As Long, Lp_IdCompra As Long
     Dim Lp_Nro_Comprobante As Long
     
    
     
     
    If CboCuenta.ListIndex = -1 Then
        MsgBox "Seleccione Cuenta contable...", vbExclamation + vbOKOnly
        CboCuenta.SetFocus
        Exit Sub
    End If
    If CboArea.Visible And CboArea.ListIndex = -1 Then
        MsgBox "Seleccione Area...", vbExclamation + vbOKOnly
        CboArea.SetFocus
        Exit Sub
    End If
    If CboCentroCosto.Visible And CboCentroCosto.ListIndex = -1 Then
        MsgBox "Seleccione Centro de Costo...", vbExclamation + vbOKOnly
        CboCentroCosto.SetFocus
        Exit Sub
    End If
    If CboItemGasto.Visible And CboItemGasto.ListIndex = -1 Then
        MsgBox "Seleccione Item de Gasto...", vbExclamation + vbOKOnly
        CboItemGasto.SetFocus
        Exit Sub
    End If
    If Val(Me.TxtSDNeto) = 0 Then
        MsgBox "Debe ingresar un Valor"
        TxtSDNeto.SetFocus
        Exit Sub
    End If

        
  
  
    If Len(TxtRsocial) = 0 Then
        MsgBox "Debe ingresar algun proveedor para el documento...", vbExclamation + vbOKOnly
        TxtRsocial.SetFocus
        Exit Sub
    End If

    If CboFpago.ListIndex = -1 Then
        MsgBox "Seleccione forma de pago...", vbInformation
        CboFpago.SetFocus
        Exit Sub
    End If
    
    ''Control de date distinta de per�odo contable
    If Month(DtFecha.Value) <> IG_Mes_Contable Or Year(DtFecha.Value) <> IG_Ano_Contable Then
         MsgBox "Fecha no corresponde al periodo contable..."
         DtFecha.SetFocus
         Exit Sub
    End If
    ''
    
    
    
    SG_codigo2 = Empty
    'Si selecciona Fondos por rendir como medio de pago
    'Mostrar ventana para buscar documentos emitidos con cta fondos por rendir
    On Error GoTo FalloGrabo
    cn.BeginTrans
        
        
        
        If DG_ID_Unico > 0 Then
            Sql = "DELETE FROM com_doc_compra " & _
                  "WHERE id=" & DG_ID_Unico
            cn.Execute Sql
            Lp_IdCompra = DG_ID_Unico
        Else
            Lp_IdCompra = 1
            
            
            'Sql = "SELECT max(id) elid " & _
            '      "FROM com_doc_compra "
           ' Consulta RsTmp, Sql
            'If RsTmp.RecordCount Then lp_IdCompra = (0 & RsTmp!elid) + 1
            Lp_IdCompra = UltimoNro("com_doc_compra", "id")
        End If
        
        If CboFpago.Text <> "CREDITO" Then
            'Aqui realizar abono a la cta cte
            '8 Octubre 2011
            'Comprobar si este documento ya tiene un pago
            'Sql = "DELETE FROM cta_abonos WHERE "
            
             Lp_Nro_Comprobante = AutoIncremento("lee", 200, , IG_id_Sucursal_Empresa)
        
             Dim Lp_IdAbo As Long
             Bp_Comprobante_Pago = True
             Lp_IdAbo = UltimoNro("cta_abonos", "abo_id")
             PagoDocumento Lp_IdAbo, "PRO", Me.txtRutProveedor, DtFecha, Val(CDbl(TxtSDNeto)), CboFpago.ItemData(CboFpago.ListIndex), "PAGADO", CboFpago.Text, LogUsuario, Lp_Nro_Comprobante, 0, "COMPRA"
             Sql = "INSERT INTO cta_abono_documentos (abo_id,id,ctd_monto,rut_emp) VALUES "
             Sql = Sql & "(" & Lp_IdAbo & "," & Lp_IdCompra & "," & CDbl(TxtSDNeto) & ",'" & SP_Rut_Activo & "')"
             cn.Execute Sql
             AutoIncremento "GUARDA", 200, Lp_Nro_Comprobante, IG_id_Sucursal_Empresa
        End If
        
        
        
        If CboFpago.ItemData(CboFpago.ListIndex) = 9999 Then
            Ban_SeleccionaChequeRendir.Sm_Rut_Anticipo = ""
            Ban_SeleccionaChequeRendir.Sm_EnviarA = "GASTOS"
            Ban_SeleccionaChequeRendir.TxtRequerido = TxtSDNeto
            Ban_SeleccionaChequeRendir.TxtSaldo = TxtSDNeto
            Ban_SeleccionaChequeRendir.Show 1
            If SG_codigo = Empty Then
                cn.RollbackTrans
                Exit Sub
            End If
            Sql = "INSERT INTO ban_cheques_fondos_rendir (che_id,chf_debe,abo_id) VALUES"
            For i = 1 To LvDetalle.ListItems.Count
                Sql = Sql & "(" & LvDetalle.ListItems(i) & "," & CDbl(LvDetalle.ListItems(i).SubItems(4)) & "," & Lp_IdAbo & "),"
            Next
            Sql = Mid(Sql, 1, Len(Sql) - 1)
            cn.Execute Sql
        End If
        
           
                       
        'Listo para grabar el documento
        'ESTAMOS LISTOS PARA PROCEDER A GRABAR EL DOCUMENTO DE COMPRA
        'Consulta RsTmp, "BEGIN"
    
        
        
        
            
        Ip_CenId = Me.CboCentroCosto.ItemData(Me.CboCentroCosto.ListIndex)
        Ip_PlaId = Me.CboCuenta.ItemData(CboCuenta.ListIndex)
        Ip_GasId = CboItemGasto.ItemData(CboItemGasto.ListIndex)
        Ip_AreId = CboArea.ItemData(CboArea.ListIndex)
    
        Dim Sp_Fpago As String
        Sp_Fpago = CboFpago.Text
        
                
        '9 Mayo de 2015
        lacajita = 0
        If SG_Modulo_Caja = "SI" Then
            If Mid(CboFpago.Text, 1, 7) = "CONTADO" Then
                lacajita = LG_id_Caja
            End If
        End If
        
        
        'Insertamos doc de compra con id's 24 Agosto 2011
         Sql = "INSERT INTO com_doc_compra (id,rut,nombre_proveedor,documento,no_documento,pago,com_exe_otros_imp,neto,iva,especifico,total,fecha,doc_id,mes_contable,ano_contable,tipo_movimiento,id_ref,inventario,iva_retenido,doc_fecha_vencimiento,rut_emp,pla_id,cen_id,gas_id,are_id,caj_id) " & _
              "VALUES (" & Lp_IdCompra & ",'" & txtRutProveedor & "','" & TxtRsocial & "','" & CboTipoDoc.Text & "'," & TxtNDoc & ",'" & Sp_Fpago & "'," & _
              0 & "," & 0 & "," & 0 & "," & 0 & "," & CDbl(TxtSDNeto) & ",'" & Format(DtFecha.Value, "YYYY-MM-DD") & "'," & _
              CboTipoDoc.ItemData(CboTipoDoc.ListIndex) & "," & IG_Mes_Contable & "," & IG_Ano_Contable & ",'GASTOS MENORES'," & 0 & ",'NN'," & 0 & ",'" & Format(DtFecha.Value, "YYYY-MM-DD") & _
              "','" & SP_Rut_Activo & "'," & Ip_PlaId & "," & Ip_CenId & "," & Ip_GasId & "," & Ip_AreId & "," & lacajita & ")"
        cn.Execute Sql
        
        
        
        'If Option2.Value Then
        '    'Grabamos las cuentas en tabla de multicuantas para una boleta
        '
        '    Sql = "INSERT INTO com_con_multicuenta (dcu_tipo,id_unico,pla_id,dcu_valor) VALUES"
        '    For i = 1 To LvDetalle.ListItems.Count
        '        Sql = Sql & "('BOLETA'," & Lp_IdCompra & "," & LvDetalle.ListItems(i) & "," & CDbl(LvDetalle.ListItems(i).SubItems(2)) & "),"
        '    Next
        '    Sql = Mid(Sql, 1, Len(Sql) - 1)
        ''    cn.Execute Sql
        'End If

       
       
       
       
       
       
       
       
       
       
        If MsgBox("Ingreso grabado correctamente..." & vbNewLine & "� Ingresar Otro ?", vbQuestion + vbYesNo) = vbNo Then Unload Me
        TxtNDoc = 0
        Me.txtRutProveedor = Empty
        TxtRsocial = Empty
        TxtSDNeto = 0
        LvDetalle.ListItems.Clear
    cn.CommitTrans
    Exit Sub
FalloGrabo:
    MsgBox "Ocurrio un error al intentar grabar..." & vbNewLine & Err.Number & "  " & Err.Description, vbInformation + vbExclamation
    cn.RollbackTrans
End Sub

Private Sub CmdNuevoProveedor_Click()
    SG_codigo = Empty
    AgregoProveedor.Show 1
    If SG_codigo <> Empty Then txtRutProveedor = SG_codigo
    
End Sub



Private Sub cmdSalir_Click()
    If MsgBox("� Confirma salir ?...", vbQuestion + vbYesNo) = vbYes Then Unload Me
End Sub

Private Sub Form_Load()

    Centrar Me, False
    Aplicar_skin Me
    DtFecha = Date
    LLenarCombo CboTipoDoc, "doc_nombre", "doc_id", "sis_documentos", "doc_libro_gastos='SI' and doc_activo='SI'", "doc_orden"
    TxtMesContable = UCase(MonthName(IG_Mes_Contable))
    TxtAnoContable = IG_Ano_Contable
    LLenarCombo CboCuenta, "pla_nombre", "pla_id", "con_plan_de_cuentas", "pla_activo='SI'"
    LLenarCombo CboCentroCosto, "cen_nombre", "cen_id", "par_centros_de_costo", "cen_activo='SI' AND rut_emp='" & SP_Rut_Activo & "'"
    LLenarCombo Me.CboItemGasto, "gas_nombre", "gas_id", "par_item_gastos", "gas_activo='SI' AND rut_emp='" & SP_Rut_Activo & "'"
    LLenarCombo CboArea, "are_nombre", "are_id", "par_areas", "are_activo='SI' AND rut_emp='" & SP_Rut_Activo & "'"
    CboTipoDoc.ListIndex = 0
    CboFpago.ItemData(0) = 1
    CboFpago.ItemData(1) = 2
    CboFpago.ItemData(2) = 9999
    
         'CONSULTAMOS SI LA EMPRESA ACTIVA
    'LLEVA CENTRO DE COSTO, AREAS
    '8 OCTUBRE 2011
    Sql = "SELECT emp_centro_de_costos,emp_areas,emp_item_de_gastos,emp_cuenta " & _
          "FROM sis_empresas " & _
          "WHERE rut='" & SP_Rut_Activo & "'"
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        If RsTmp!emp_cuenta = "NO" Then
            LLenarCombo CboCuenta, "pla_nombre", "pla_id", "con_plan_de_cuentas", "pla_id=99999"
            CboCuenta.ListIndex = 0
            CboCuenta.Visible = False
            cmdCuenta.Visible = False
        End If
        If RsTmp!emp_centro_de_costos = "NO" Then
            LLenarCombo CboCentroCosto, "cen_nombre", "cen_id", "par_centros_de_costo", "cen_id=99999"
            CboCentroCosto.ListIndex = 0
            CboCentroCosto.Visible = False
            SkCC.Visible = False
        End If
        If RsTmp!emp_areas = "NO" Then
            SkArea.Visible = False
            LLenarCombo CboArea, "are_nombre", "are_id", "par_areas", "are_id=99999"
            CboArea.ListIndex = 0
            CboArea.Visible = False
        End If
        If RsTmp!emp_item_de_gastos = "NO" Then
            SkItem.Visible = False
            LLenarCombo CboItemGasto, "gas_nombre", "gas_id", "par_item_gastos", "gas_id=99999"
            CboItemGasto.ListIndex = 0
            CboItemGasto.Visible = False
        End If
    End If

    If DG_ID_Unico > 0 Then
        'Editando
        Sql = "SELECT fecha,rut,nombre_proveedor,total,pla_id,cen_id,are_id,gas_id,doc_id,no_documento, CASE pago WHEN 'CONTADO' THEN 1 WHEN 'CREDITO' THEN 2 ELSE 9999 END AS fpago " & _
              "FROM com_doc_compra " & _
              "WHERE rut_emp='" & SP_Rut_Activo & "' AND  id=" & DG_ID_Unico
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
            With RsTmp
                txtRutProveedor = !Rut
                TxtRsocial = !nombre_proveedor
                TxtNDoc = !no_documento
                Busca_Id_Combo CboCuenta, !pla_id
                Busca_Id_Combo CboCentroCosto, !cen_id
                Busca_Id_Combo CboItemGasto, !gas_id
                Busca_Id_Combo CboArea, !are_id
                TxtSDNeto = NumFormat(!Total)
                Me.Caption = "MODIFICANDO " & Me.Caption
                Busca_Id_Combo CboFpago, Val(!fpago)
            End With
        End If
    End If
End Sub
Private Sub Check1_Click()
    If Check1.Value = 1 Then
        Option1.Enabled = True
        Option2.Enabled = True
    Else
        Option1.Enabled = False
        Option2.Enabled = False
    End If
    CmdNueva.SetFocus
End Sub


Private Sub Form_Unload(Cancel As Integer)
    Unload Compra_ContaMulticuenta
End Sub

Private Sub Option2_Click()
    Compra_ContaMulticuenta.Sm_Formulario = "BOLETA"
    Compra_ContaMulticuenta.TxtMonto = TxtSDNeto
    Compra_ContaMulticuenta.Show 1
End Sub

Private Sub TxtNDoc_GotFocus()
    En_Foco TxtNDoc
End Sub
Private Sub TxtNDoc_KeyPress(KeyAscii As Integer)
    KeyAscii = AceptaSoloNumeros(KeyAscii)
End Sub

Private Sub TxtNroCuenta_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
End Sub

Private Sub TxtNroCuenta_Validate(Cancel As Boolean)
    If Val(TxtNroCuenta) = 0 Then Exit Sub
    Busca_Id_Combo CboCuenta, Val(TxtNroCuenta)
    If CboCuenta.ListIndex = -1 Then
        MsgBox "Cuenta no existe..."
    End If

End Sub

Private Sub TxtRsocial_GotFocus()
    En_Foco TxtRsocial
End Sub

Private Sub TxtRsocial_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
     If KeyAscii = 39 Then KeyAscii = 0
End Sub



Private Sub txtRutProveedor_Validate(Cancel As Boolean)
     If Len(txtRutProveedor) > 0 Then
        Respuesta = VerificaRut(txtRutProveedor, NuevoRut)
        txtRutProveedor = NuevoRut
        Sql = "SELECT nombre_empresa " & _
              "FROM maestro_proveedores " & _
              "WHERE rut_proveedor='" & txtRutProveedor & "' "
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
            TxtRsocial = RsTmp!nombre_empresa
        Else
            MsgBox "Proveedor no encontrado... ", vbOKOnly + vbInformation
            TxtRsocial = ""
        End If
    End If
End Sub

Private Sub TxtSDNeto_GotFocus()
    En_Foco TxtSDNeto
End Sub

Private Sub TxtSDNeto_KeyPress(KeyAscii As Integer)
    KeyAscii = AceptaSoloNumeros(KeyAscii)
End Sub


Private Sub TxtSDNeto_Validate(Cancel As Boolean)
    If Val(TxtSDNeto) = 0 Then
        TxtSDNeto = 0
    Else
        TxtSDNeto = NumFormat(TxtSDNeto)
    End If
    CmdGuardar.SetFocus
End Sub

