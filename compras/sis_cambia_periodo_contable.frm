VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Begin VB.Form sis_cambia_periodo_contable 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Cambiar periodo contable"
   ClientHeight    =   2610
   ClientLeft      =   12975
   ClientTop       =   3450
   ClientWidth     =   7200
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2610
   ScaleWidth      =   7200
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton CmdSalir 
      Caption         =   "Cerrar"
      Height          =   285
      Left            =   6195
      TabIndex        =   6
      Top             =   2265
      Width           =   795
   End
   Begin VB.Frame Frame1 
      Caption         =   "Periodo actual"
      Height          =   1590
      Left            =   870
      TabIndex        =   0
      Top             =   435
      Width           =   5625
      Begin VB.CommandButton CmdCambiar 
         Caption         =   "Cambiar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   390
         Left            =   1215
         TabIndex        =   5
         Top             =   1080
         Width           =   3300
      End
      Begin VB.ComboBox CboMes 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         ItemData        =   "sis_cambia_periodo_contable.frx":0000
         Left            =   2475
         List            =   "sis_cambia_periodo_contable.frx":0002
         Style           =   2  'Dropdown List
         TabIndex        =   3
         Top             =   270
         Width           =   1725
      End
      Begin VB.ComboBox CboAno 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         ItemData        =   "sis_cambia_periodo_contable.frx":0004
         Left            =   2475
         List            =   "sis_cambia_periodo_contable.frx":0006
         Style           =   2  'Dropdown List
         TabIndex        =   2
         Top             =   660
         Width           =   1260
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   240
         Left            =   780
         OleObjectBlob   =   "sis_cambia_periodo_contable.frx":0008
         TabIndex        =   1
         Top             =   330
         Width           =   1650
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   240
         Left            =   660
         OleObjectBlob   =   "sis_cambia_periodo_contable.frx":007E
         TabIndex        =   4
         Top             =   720
         Width           =   1770
      End
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   0
      OleObjectBlob   =   "sis_cambia_periodo_contable.frx":00F4
      Top             =   0
   End
End
Attribute VB_Name = "sis_cambia_periodo_contable"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub CmdCambiar_Click()
    'A�� Contable
    If Me.CboMes.ListIndex = -1 Then
        MsgBox "Seleccione mes", vbExclamation
        CboMes.SetFocus
        Exit Sub
    End If
        
        
    Sql = "UPDATE tabla_parametros " & _
          "SET par_valor='" & CboAno.Text & "' " & _
          "WHERE par_id=13"
          IG_Ano_Contable = CboAno.Text
    cn.Execute Sql
    'Mes Contable
    Sql = "UPDATE tabla_parametros " & _
          "SET par_valor='" & CboMes.ListIndex + 1 & "' " & _
          "WHERE par_id=14"
          IG_Mes_Contable = CboMes.ListIndex + 1
    cn.Execute Sql
    IG_Ano_Contable = CboAno.Text
    IG_Mes_Contable = CboMes.ListIndex + 1
    Principal.CboMes = UCase(MonthName(IG_Mes_Contable))
    Principal.CboAno = IG_Ano_Contable
    Unload Me
End Sub

Private Sub CmdSalir_Click()
    Unload Me
End Sub
Private Sub Form_Load()
    For i = 1 To 12
        CboMes.AddItem UCase(MonthName(i))
        CboMes.ItemData(i - 1) = i
    Next
    Busca_Id_Combo CboMes, Val(IG_Mes_Contable)
    LLenaYears CboAno, 2010
    Skin2 Me, , 4
End Sub
