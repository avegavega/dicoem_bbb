VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form LineasItemsGastos 
   Caption         =   "Plan de cuentas"
   ClientHeight    =   6555
   ClientLeft      =   2430
   ClientTop       =   2025
   ClientWidth     =   8265
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   6555
   ScaleWidth      =   8265
   Begin VB.Timer Timer1 
      Left            =   2715
      Top             =   105
   End
   Begin ACTIVESKINLibCtl.SkinLabel skIdLinea 
      Height          =   375
      Left            =   0
      OleObjectBlob   =   "LineasItemsGastos.frx":0000
      TabIndex        =   12
      Top             =   720
      Width           =   375
   End
   Begin ACTIVESKINLibCtl.SkinLabel skItemID 
      Height          =   255
      Left            =   0
      OleObjectBlob   =   "LineasItemsGastos.frx":0060
      TabIndex        =   11
      Top             =   3600
      Width           =   375
   End
   Begin VB.CommandButton cmdOut 
      Cancel          =   -1  'True
      Caption         =   "&Retornar"
      Height          =   375
      Left            =   6120
      TabIndex        =   8
      Top             =   6120
      Width           =   1815
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   120
      OleObjectBlob   =   "LineasItemsGastos.frx":00C0
      Top             =   6600
   End
   Begin VB.Frame FrmItems 
      Caption         =   "Items de cuenta"
      Height          =   2775
      Left            =   360
      TabIndex        =   10
      Top             =   3240
      Width           =   7575
      Begin VB.TextBox txtItems 
         Height          =   285
         Left            =   120
         TabIndex        =   4
         Top             =   360
         Width           =   5055
      End
      Begin VB.CommandButton OkItems 
         Caption         =   "Ok"
         Height          =   285
         Left            =   6840
         TabIndex        =   6
         Top             =   360
         Width           =   495
      End
      Begin VB.ComboBox ComActivoItem 
         Height          =   315
         ItemData        =   "LineasItemsGastos.frx":02F4
         Left            =   5160
         List            =   "LineasItemsGastos.frx":02FE
         Style           =   2  'Dropdown List
         TabIndex        =   5
         Top             =   360
         Width           =   1695
      End
      Begin MSComctlLib.ListView LVItemsGastos 
         Height          =   1695
         Left            =   90
         TabIndex        =   7
         Top             =   720
         Width           =   7215
         _ExtentX        =   12726
         _ExtentY        =   2990
         View            =   3
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         HotTracking     =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   4
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "id"
            Object.Width           =   2
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "idLineaGasto"
            Object.Width           =   2
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Descripion Item"
            Object.Width           =   8916
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "Activo"
            Object.Width           =   2990
         EndProperty
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Lista de cuentas"
      Height          =   2775
      Left            =   360
      TabIndex        =   0
      Top             =   360
      Width           =   7575
      Begin VB.ComboBox comActivoLinea 
         Height          =   315
         ItemData        =   "LineasItemsGastos.frx":030A
         Left            =   5160
         List            =   "LineasItemsGastos.frx":0314
         Style           =   2  'Dropdown List
         TabIndex        =   2
         Top             =   360
         Width           =   1695
      End
      Begin VB.CommandButton OkLinea 
         Caption         =   "Ok"
         Height          =   285
         Left            =   6840
         TabIndex        =   3
         Top             =   360
         Width           =   495
      End
      Begin VB.TextBox txtLineaGasto 
         Height          =   285
         Left            =   120
         TabIndex        =   1
         Top             =   360
         Width           =   5055
      End
      Begin MSComctlLib.ListView LvLineaGastos 
         Height          =   1695
         Left            =   100
         TabIndex        =   9
         Top             =   705
         Width           =   7215
         _ExtentX        =   12726
         _ExtentY        =   2990
         View            =   3
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         HotTracking     =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   3
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "id"
            Object.Width           =   2
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Descripcion Cuenta"
            Object.Width           =   8916
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Activo"
            Object.Width           =   2822
         EndProperty
      End
   End
End
Attribute VB_Name = "LineasItemsGastos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub CmdOut_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    Aplicar_skin Me
    Me.ComActivoItem.ListIndex = 0
    Me.comActivoLinea.ListIndex = 0
    CargaGastos
    LvLineaGastos_Click
    
End Sub
Private Sub CargaGastos()
    LVItemsGastos.ListItems.Clear
    Sql = "SELECT * FROM lineas_de_gastos WHERE linea_gasto_activo='SI'"
    Call Consulta(RsTmp, Sql)
    If RsTmp.RecordCount > 0 Then
        LLenar_Grilla RsTmp, Me, LvLineaGastos, False, True, True, False
        
    End If
    comActivoLinea.ListIndex = 0
End Sub

Private Sub LVItemsGastos_DblClick()
    If LVItemsGastos.SelectedItem Is Nothing Then Exit Sub
    
    skItemID = LVItemsGastos.SelectedItem.Text
    txtItems = LVItemsGastos.SelectedItem.SubItems(2)
    ComActivoItem.ListIndex = 0
    LVItemsGastos.ListItems.Remove LVItemsGastos.SelectedItem.Index
    
End Sub

Private Sub LvLineaGastos_Click()
    CargaItems
End Sub
Private Sub CargaItems()
    LVItemsGastos.ListItems.Clear
    FrmItems.Caption = "Item's de Cuentas"
    If LvLineaGastos.SelectedItem Is Nothing Then Exit Sub
    
    i = LvLineaGastos.SelectedItem.Text
    
    FrmItems.Caption = "Item's de Cuenta de: {" & LvLineaGastos.SelectedItem.SubItems(1) & "}"
    Sql = "SELECT item_gasto_id,g.linea_gasto_id,item_gasto_nombre,item_gasto_activo " & _
          "FROM items_gastos i,lineas_de_gastos g " & _
          "WHERE i.linea_gasto_id=g.linea_gasto_id AND item_gasto_activo='SI' AND i.linea_gasto_id=" & i
    Call Consulta(RsTmp, Sql)
    LLenar_Grilla RsTmp, Me, LVItemsGastos, False, True, True, False

End Sub



Private Sub LvLineaGastos_DblClick()
    If LvLineaGastos.SelectedItem Is Nothing Then Exit Sub
    i = LvLineaGastos.SelectedItem.Text
    Me.skIdLinea = i
    txtLineaGasto = LvLineaGastos.SelectedItem.SubItems(1)
    comActivoLinea.ListIndex = 0
    LvLineaGastos.ListItems.Remove LvLineaGastos.SelectedItem.Index
End Sub

Private Sub OkItems_Click()
    If LvLineaGastos.SelectedItem Is Nothing Then
        MsgBox "Debe seleccionar Linea de Gasto...", vbOKOnly + vbInformation
        Exit Sub
    End If
    If Len(txtItems) = 0 Then Exit Sub
    
    'Agui grabamos
    If Me.skItemID = 0 Then
        Sql = "INSERT INTO items_gastos " & _
              "(item_gasto_nombre,linea_gasto_id,item_gasto_activo) " & _
              "VALUES('" & txtItems & "'," & LvLineaGastos.SelectedItem.Text & ",'" & ComActivoItem.Text & "')"
    Else
        Sql = "UPDATE items_gastos " & _
              "SET item_gasto_nombre='" & txtItems & "',linea_gasto_id=" & LvLineaGastos.SelectedItem.Text & ",item_gasto_activo='" & Me.ComActivoItem.Text & "' " & _
              "WHERE item_gasto_id=" & skItemID
    End If
    Call Consulta(RsTmp, Sql)
    txtItems = ""
    skItemID = 0
    
    CargaItems
    
End Sub

Private Sub OkLinea_Click()
    If Len(txtLineaGasto) = 0 Then
        For i = 1 To LvLineaGastos.ListItems.Count
            If LvLineaGastos.ListItems(i).SubItems(1) = txtLineaGasto Then Exit Sub
        Next
    End If
    
    If skIdLinea = 0 Then
        Sql = "INSERT INTO lineas_de_gastos (linea_gasto_nombre,linea_gasto_activo) " & _
              "VALUES('" & txtLineaGasto & "','" & comActivoLinea.Text & "')"
    Else
        Sql = "UPDATE lineas_de_gastos SET linea_gasto_nombre='" & txtLineaGasto & "',linea_gasto_activo='" & comActivoLinea.Text & "' " & _
              "WHERE linea_gasto_id=" & skIdLinea
    End If
    Call Consulta(RsTmp, Sql)
    txtLineaGasto = ""
    skIdLinea = 0
    CargaGastos
End Sub

Private Sub Timer1_Timer()
    Me.LvLineaGastos.SetFocus
    Timer1.Enabled = False
End Sub

Private Sub txtItems_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
    If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub txtLineaGasto_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
    If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub txtLineaGasto_Validate(Cancel As Boolean)
txtLineaGasto = Replace(txtLineaGasto, "'", "")
End Sub
