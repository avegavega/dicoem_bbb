VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form Com_OC_Pendientes 
   Caption         =   "Ordenes de compra"
   ClientHeight    =   8070
   ClientLeft      =   1530
   ClientTop       =   1590
   ClientWidth     =   13035
   LinkTopic       =   "Form1"
   ScaleHeight     =   8070
   ScaleWidth      =   13035
   Begin VB.PictureBox PicLogo 
      Height          =   915
      Left            =   2325
      ScaleHeight     =   855
      ScaleWidth      =   8010
      TabIndex        =   10
      Top             =   6570
      Width           =   8070
   End
   Begin VB.Frame Frame1 
      Caption         =   "Seleccione OC"
      Height          =   6210
      Left            =   315
      TabIndex        =   1
      Top             =   210
      Width           =   12375
      Begin VB.CommandButton CmdCarga 
         Caption         =   "Carga Ordenes Seleccionadas"
         Height          =   375
         Left            =   270
         TabIndex        =   12
         Top             =   5760
         Width           =   2835
      End
      Begin VB.CheckBox Check1 
         Height          =   195
         Left            =   300
         TabIndex        =   11
         Top             =   1035
         Width           =   180
      End
      Begin VB.CommandButton CmdEliminar 
         Caption         =   "&Eliminar OC"
         Height          =   390
         Left            =   10500
         TabIndex        =   9
         Top             =   5745
         Width           =   1635
      End
      Begin VB.CommandButton CmdVerOCs 
         Caption         =   "Buscar Ordenes de Compra"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   9225
         TabIndex        =   8
         Top             =   510
         Width           =   2955
      End
      Begin VB.TextBox TxtRsocial 
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   5040
         Locked          =   -1  'True
         TabIndex        =   6
         TabStop         =   0   'False
         Top             =   510
         Width           =   4155
      End
      Begin VB.CommandButton CmdBuscaProv 
         Caption         =   "Buscar"
         Height          =   330
         Left            =   3090
         TabIndex        =   4
         ToolTipText     =   "Bucar Proveedor"
         Top             =   510
         Width           =   660
      End
      Begin VB.TextBox TxtRutProveedor 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   3765
         TabIndex        =   3
         Tag             =   "T"
         Top             =   510
         Width           =   1275
      End
      Begin MSComctlLib.ListView LvDetalle 
         Height          =   4665
         Left            =   285
         TabIndex        =   2
         Top             =   1005
         Width           =   11820
         _ExtentX        =   20849
         _ExtentY        =   8229
         View            =   3
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         Checkboxes      =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   8
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "T1000"
            Text            =   "Id Unico"
            Object.Width           =   529
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "N109"
            Text            =   "Nro OC"
            Object.Width           =   2293
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "F1000"
            Text            =   "Fecha"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Object.Tag             =   "T800"
            Text            =   "Rut"
            Object.Width           =   2469
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Object.Tag             =   "T3000"
            Text            =   "Nombre Proveedor"
            Object.Width           =   6703
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   5
            Object.Tag             =   "N100"
            Text            =   "Neto"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   6
            Object.Tag             =   "N100"
            Text            =   "IVA"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   7
            Object.Tag             =   "N100"
            Text            =   "Total"
            Object.Width           =   2117
         EndProperty
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
         Height          =   225
         Left            =   3765
         OleObjectBlob   =   "Com_OC_Pendientes.frx":0000
         TabIndex        =   5
         Top             =   315
         Width           =   1140
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel10 
         Height          =   225
         Left            =   4935
         OleObjectBlob   =   "Com_OC_Pendientes.frx":0071
         TabIndex        =   7
         Top             =   330
         Width           =   1020
      End
   End
   Begin VB.CommandButton CmdOut 
      Cancel          =   -1  'True
      Caption         =   "&Retornar"
      Height          =   375
      Left            =   10845
      TabIndex        =   0
      Top             =   6810
      Width           =   1710
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   915
      OleObjectBlob   =   "Com_OC_Pendientes.frx":00E0
      Top             =   5775
   End
   Begin VB.Timer Timer1 
      Interval        =   10
      Left            =   360
      Top             =   5835
   End
End
Attribute VB_Name = "Com_OC_Pendientes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public Sm_Sql As String

Private Sub Check1_Click()
    For i = 1 To LvDetalle.ListItems.Count
        If LvDetalle.ListItems(i).Checked Then LvDetalle.ListItems(i).Checked = False Else LvDetalle.ListItems(i).Checked = True
    Next
End Sub

Private Sub CmdBuscaProv_Click()
    AccionProveedor = 0
    BuscaProveedor.Show 1
    TxtRutProveedor = RutBuscado

    TxtRutProveedor_Validate True
End Sub

Private Sub CmdCarga_Click()
    SG_codigo = Empty
    For i = 1 To LvDetalle.ListItems.Count
        If LvDetalle.ListItems(i).Checked Then SG_codigo = SG_codigo & LvDetalle.ListItems(i) & ","
    
    Next
    If Len(SG_codigo) > 1 Then SG_codigo = Mid(SG_codigo, 1, Len(SG_codigo) - 1)
    Unload Me
End Sub

Private Sub CmdEliminar_Click()
    Dim L_Unico As Long
    'DEBEMOS CONSULTAR SI EXISTEN DOCUMENTOS QUE HACEN REFERENCIA AL QUE SE ESTA INGRESANDO
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    If MsgBox("Esta seguro de eliminar " & vbNewLine & "Orden de compra Nro " & LvDetalle.SelectedItem.SubItems(1), vbQuestion + vbYesNo) = vbNo Then Exit Sub
    
    On Error GoTo ErrEliminar
    cn.BeginTrans
        L_Unico = LvDetalle.SelectedItem
        'multidelete
        Sql = "DELETE FROM com_impuestos " & _
                  "WHERE id=" & L_Unico
        cn.Execute Sql
        Sql = "DELETE FROM " & _
              "com_doc_compra, " & _
              "com_doc_compra_detalle " & _
              "USING com_doc_compra " & _
              "LEFT JOIN com_doc_compra_detalle ON (com_doc_compra.id=com_doc_compra_detalle.id) " & _
              "WHERE com_doc_compra.id=" & L_Unico
        cn.Execute Sql
    cn.CommitTrans
    CargaOC
    MsgBox "Orden Eliminada correctamente...", vbInformation
    LvDetalle.SetFocus
    Exit Sub
ErrEliminar:
    MsgBox "Ocurrio un error al OC"
    cn.RollbackTrans
    
End Sub

Private Sub CmdOk_Click()
    If LvDetalle.SelectedItem Is Nothing Then
        MsgBox "Seleccione Orden de Compra ...", vbInformation
        LvDetalle.SetFocus
        Exit Sub
    End If
    SG_codigo = LvDetalle.SelectedItem
    SG_codigo2 = LvDetalle.SelectedItem.SubItems(1)
    Unload Me
End Sub
Private Sub CmdOut_Click()
    SG_codigo = Empty
    Unload Me
End Sub

Private Sub CmdVerOCs_Click()
    If Len(TxtRutProveedor) = 0 Then Exit Sub
    Sm_Sql = "SELECT id,no_documento nro_oc,fecha,rut,nombre_proveedor,neto,iva,total " & _
          "FROM com_doc_compra c " & _
          "WHERE com_estado_oc='PROCESO' AND  rut_emp='" & SP_Rut_Activo & "' AND doc_id=" & IG_id_OrdenCompra & " AND rut='" & TxtRutProveedor & "'"
    CargaOC
End Sub

Private Sub Form_Load()
    Skin2 Me, , 5
    Centrar Me, True
        PicLogo.Picture = LoadPicture(App.Path & "\REDMAROK.jpg")

    CargaOC
    PicLogo.Picture = LoadPicture(App.Path & "\REDMAROK.jpg")

End Sub
Private Sub CargaOC()
    If Len(Sm_Sql) = 0 Then Exit Sub
    Consulta RsTmp, Sm_Sql
    LLenar_Grilla RsTmp, Me, LvDetalle, True, True, True, False
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Sm_Sql = Empty
End Sub

Private Sub LvDetalle_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    ordListView ColumnHeader, Me, LvDetalle
End Sub
Private Sub LvDetalle_DblClick()
    'CmdOk_Click
End Sub

Private Sub Text1_Change()

End Sub

Private Sub Timer1_Timer()
    On Error Resume Next
    LvDetalle.SetFocus
    Timer1.Enabled = False
End Sub


Private Sub TxtRutProveedor_Validate(Cancel As Boolean)
    If Len(TxtRutProveedor) = 0 Then Exit Sub
    Sql = "SELECT nombre_empresa " & _
          "FROM maestro_proveedores " & _
          "WHERE rut_proveedor='" & TxtRutProveedor & "' "
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        TxtRsocial = RsTmp!nombre_empresa
    Else
        TxtRsocial = Empty
    End If
End Sub
