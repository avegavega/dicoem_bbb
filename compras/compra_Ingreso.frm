VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{28D47522-CF84-11D1-834C-00A0249F0C28}#1.0#0"; "gif89.dll"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{DE4917CD-D9B7-45E2-B0C7-DA1DB0A76109}#1.0#0"; "jhTextBoxM.ocx"
Begin VB.Form compra_Ingreso 
   Appearance      =   0  'Flat
   BackColor       =   &H80000005&
   Caption         =   "Ingreso de Compra"
   ClientHeight    =   9810
   ClientLeft      =   6420
   ClientTop       =   345
   ClientWidth     =   8460
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   ScaleHeight     =   9810
   ScaleWidth      =   8460
   Begin VB.Frame FrmProveedor 
      Caption         =   "Datos del documento"
      Height          =   8760
      Left            =   240
      TabIndex        =   12
      Top             =   855
      Width           =   7995
      Begin VB.CommandButton CmdPeriodoContable 
         Caption         =   "Cambiar periodo"
         Height          =   255
         Left            =   5715
         TabIndex        =   93
         Top             =   165
         Width           =   1560
      End
      Begin VB.Frame FrmPeriodo 
         Caption         =   "Periodo Contable"
         Height          =   780
         Left            =   5610
         TabIndex        =   22
         Top             =   195
         Width           =   2250
         Begin VB.TextBox TxtAnoContable 
            Alignment       =   1  'Right Justify
            BackColor       =   &H8000000F&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   1365
            Locked          =   -1  'True
            TabIndex        =   24
            TabStop         =   0   'False
            Text            =   "0"
            Top             =   390
            Width           =   780
         End
         Begin VB.TextBox TxtMesContable 
            Alignment       =   1  'Right Justify
            BackColor       =   &H8000000F&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   60
            Locked          =   -1  'True
            TabIndex        =   23
            TabStop         =   0   'False
            Text            =   "0"
            Top             =   390
            Width           =   1275
         End
      End
      Begin VB.Frame FrmDiconor 
         Caption         =   "Diconor Recargo Flete"
         Height          =   705
         Left            =   4455
         TabIndex        =   91
         Top             =   4575
         Visible         =   0   'False
         Width           =   3405
         Begin jhTextBoxM.TextBoxM TxtRecargoxFlete 
            Height          =   315
            Left            =   1305
            TabIndex        =   92
            ToolTipText     =   "valor de recargo por flete"
            Top             =   255
            Width           =   1890
            _ExtentX        =   3334
            _ExtentY        =   556
            TipoDeDato      =   1
            Text            =   "0"
            PasarElFoco     =   0   'False
            Alignment       =   1
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
      Begin VB.Frame FrmCaja 
         Caption         =   "Afecta Caja Actual"
         Height          =   795
         Left            =   6000
         TabIndex        =   85
         Top             =   7815
         Width           =   1920
         Begin VB.ComboBox CboCaja 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            ItemData        =   "compra_Ingreso.frx":0000
            Left            =   135
            List            =   "compra_Ingreso.frx":000A
            Style           =   2  'Dropdown List
            TabIndex        =   86
            ToolTipText     =   "Seleccione Bodega"
            Top             =   300
            Width           =   1695
         End
      End
      Begin VB.TextBox txtMargenDist 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2160
         Locked          =   -1  'True
         TabIndex        =   82
         TabStop         =   0   'False
         Text            =   "0"
         ToolTipText     =   "Dato referencial, no suma"
         Top             =   4785
         Width           =   1290
      End
      Begin VB.TextBox txtIVAMargen 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2160
         Locked          =   -1  'True
         TabIndex        =   80
         TabStop         =   0   'False
         Text            =   "0"
         Top             =   4470
         Width           =   1290
      End
      Begin VB.TextBox txtIvaSinCredito 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2160
         TabIndex        =   78
         TabStop         =   0   'False
         Text            =   "0"
         Top             =   4140
         Width           =   1290
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel23 
         Height          =   255
         Left            =   3735
         OleObjectBlob   =   "compra_Ingreso.frx":0016
         TabIndex        =   77
         Top             =   3645
         Width           =   420
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkTasaIVA 
         Height          =   255
         Left            =   3495
         OleObjectBlob   =   "compra_Ingreso.frx":0076
         TabIndex        =   76
         Top             =   3645
         Width           =   240
      End
      Begin VB.Frame fraSaldoOC 
         Caption         =   "Abonos OC"
         Height          =   1065
         Left            =   6345
         TabIndex        =   73
         Top             =   1275
         Visible         =   0   'False
         Width           =   1500
         Begin VB.CheckBox ChkUtilizaOC 
            Caption         =   "Utilizar"
            Height          =   195
            Left            =   120
            TabIndex        =   75
            Top             =   750
            Value           =   1  'Checked
            Width           =   1155
         End
         Begin VB.TextBox txtSaldoOC 
            Alignment       =   1  'Right Justify
            BackColor       =   &H8000000F&
            Height          =   285
            Left            =   120
            Locked          =   -1  'True
            TabIndex        =   74
            Text            =   "0"
            ToolTipText     =   "Abonos realizados a la(s) OC's"
            Top             =   360
            Width           =   1260
         End
      End
      Begin VB.CommandButton Command2 
         Caption         =   "Command2"
         Height          =   225
         Left            =   7005
         TabIndex        =   72
         Top             =   1005
         Visible         =   0   'False
         Width           =   285
      End
      Begin VB.CommandButton CmdNueva 
         Caption         =   "Continuar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   435
         Left            =   5880
         TabIndex        =   68
         ToolTipText     =   "Nueva compra"
         Top             =   7275
         Width           =   990
      End
      Begin VB.CommandButton CmdSalir 
         Cancel          =   -1  'True
         Caption         =   "&Retornar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   435
         Left            =   6900
         TabIndex        =   67
         ToolTipText     =   "Salir"
         Top             =   7275
         Width           =   990
      End
      Begin VB.TextBox TxtRetiradoPor 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2145
         TabIndex        =   62
         Tag             =   "T"
         Top             =   8370
         Width           =   3675
      End
      Begin VB.TextBox TxtAutorizadoPor 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2145
         TabIndex        =   60
         Tag             =   "T"
         Top             =   8085
         Width           =   3675
      End
      Begin VB.TextBox TxtSolicitadoPor 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2145
         TabIndex        =   58
         Tag             =   "T"
         Top             =   7785
         Width           =   3675
      End
      Begin VB.CommandButton CmdLlamaOC 
         Caption         =   "Cargar Orden Compra"
         Height          =   285
         Left            =   3450
         TabIndex        =   57
         Top             =   1365
         Width           =   2835
      End
      Begin VB.Frame Frame2 
         Caption         =   "Forma de pago"
         Height          =   570
         Left            =   5970
         TabIndex        =   55
         Top             =   6570
         Width           =   1785
         Begin VB.ComboBox CboFpago 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            ItemData        =   "compra_Ingreso.frx":00D6
            Left            =   60
            List            =   "compra_Ingreso.frx":00E0
            Style           =   2  'Dropdown List
            TabIndex        =   56
            ToolTipText     =   "Seleccione Bodega"
            Top             =   195
            Width           =   1695
         End
      End
      Begin VB.Frame FrameRef 
         Caption         =   "Seleccionar Documento"
         Height          =   2070
         Left            =   4425
         TabIndex        =   47
         Top             =   2430
         Visible         =   0   'False
         Width           =   3435
         Begin VB.TextBox TxtTotalRef 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00E0E0E0&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   1725
            Locked          =   -1  'True
            TabIndex        =   53
            TabStop         =   0   'False
            Top             =   1200
            Width           =   1590
         End
         Begin VB.TextBox TxtNroRef 
            BackColor       =   &H00E0E0E0&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   90
            Locked          =   -1  'True
            TabIndex        =   52
            TabStop         =   0   'False
            Top             =   1200
            Width           =   1620
         End
         Begin VB.TextBox TxtDocumentoRef 
            BackColor       =   &H00E0E0E0&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   90
            Locked          =   -1  'True
            TabIndex        =   51
            TabStop         =   0   'False
            Top             =   615
            Width           =   3225
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel18 
            Height          =   240
            Left            =   105
            OleObjectBlob   =   "compra_Ingreso.frx":00F6
            TabIndex        =   50
            Top             =   1020
            Width           =   1500
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel17 
            Height          =   195
            Left            =   105
            OleObjectBlob   =   "compra_Ingreso.frx":0160
            TabIndex        =   49
            Top             =   420
            Width           =   1365
         End
         Begin VB.CommandButton CmdBuscar 
            Caption         =   "Buscar"
            Height          =   345
            Left            =   2430
            TabIndex        =   48
            Top             =   225
            Width           =   900
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel19 
            Height          =   240
            Left            =   1740
            OleObjectBlob   =   "compra_Ingreso.frx":01D0
            TabIndex        =   54
            Top             =   1020
            Width           =   1500
         End
      End
      Begin VB.ComboBox CboBodega 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         ItemData        =   "compra_Ingreso.frx":0238
         Left            =   2160
         List            =   "compra_Ingreso.frx":0245
         Style           =   2  'Dropdown List
         TabIndex        =   45
         ToolTipText     =   "Seleccione Bodega"
         Top             =   6705
         Width           =   3705
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkTotalAdicionales 
         Height          =   300
         Left            =   5625
         OleObjectBlob   =   "compra_Ingreso.frx":026A
         TabIndex        =   44
         Top             =   6495
         Width           =   1035
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkImpCosto 
         Height          =   180
         Left            =   4425
         OleObjectBlob   =   "compra_Ingreso.frx":02CA
         TabIndex        =   43
         Top             =   6495
         Width           =   1185
      End
      Begin VB.TextBox TxtSubTotal 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   2160
         Locked          =   -1  'True
         TabIndex        =   41
         TabStop         =   0   'False
         Text            =   "0"
         Top             =   5070
         Width           =   1290
      End
      Begin VB.TextBox TxtNeto 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2160
         Locked          =   -1  'True
         TabIndex        =   39
         TabStop         =   0   'False
         Text            =   "0"
         Top             =   3270
         Width           =   1290
      End
      Begin VB.TextBox TxtDescuentoNeto 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2160
         TabIndex        =   7
         Text            =   "0"
         Top             =   2955
         Width           =   1290
      End
      Begin VB.TextBox TxtImpuesto 
         BackColor       =   &H00E0E0E0&
         Height          =   285
         Left            =   2160
         Locked          =   -1  'True
         TabIndex        =   37
         TabStop         =   0   'False
         Top             =   5400
         Width           =   2940
      End
      Begin VB.CommandButton CmdOkImp 
         Caption         =   "Ok"
         Height          =   270
         Left            =   6345
         TabIndex        =   36
         Top             =   5400
         Width           =   345
      End
      Begin VB.TextBox TxtValorImp 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   5085
         TabIndex        =   35
         Tag             =   "N"
         Text            =   "0"
         Top             =   5400
         Width           =   1260
      End
      Begin VB.TextBox TxtExento 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         Height          =   300
         Left            =   2160
         TabIndex        =   5
         Tag             =   "N"
         Text            =   "0"
         Top             =   2370
         Width           =   1290
      End
      Begin VB.TextBox txtIvaRetenido 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         Enabled         =   0   'False
         Height          =   285
         Left            =   2160
         TabIndex        =   9
         Tag             =   "N"
         Text            =   "0"
         Top             =   3885
         Width           =   1290
      End
      Begin VB.Frame Frame1 
         Height          =   540
         Left            =   2160
         TabIndex        =   30
         Top             =   7260
         Width           =   3690
         Begin VB.OptionButton Option2 
            Caption         =   "Valores Brutos"
            Height          =   225
            Left            =   2025
            TabIndex        =   32
            Top             =   255
            Width           =   1440
         End
         Begin VB.OptionButton Option1 
            Caption         =   "Valores Netos"
            Height          =   225
            Left            =   135
            TabIndex        =   31
            Top             =   225
            Value           =   -1  'True
            Width           =   1680
         End
      End
      Begin VB.CheckBox Check1 
         Alignment       =   1  'Right Justify
         Caption         =   "Ingresar Detalle"
         Height          =   195
         Left            =   2160
         TabIndex        =   11
         Top             =   7080
         Value           =   1  'Checked
         Width           =   1455
      End
      Begin VB.TextBox txtIVA 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2160
         TabIndex        =   8
         TabStop         =   0   'False
         Text            =   "0"
         Top             =   3585
         Width           =   1290
      End
      Begin VB.TextBox txtSubNeto 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2160
         TabIndex        =   6
         Text            =   "0"
         Top             =   2655
         Width           =   1290
      End
      Begin VB.TextBox txtTotal 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2160
         Locked          =   -1  'True
         TabIndex        =   25
         TabStop         =   0   'False
         Text            =   "0"
         Top             =   6375
         Width           =   1485
      End
      Begin VB.TextBox TxtRutProveedor 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2160
         TabIndex        =   4
         Tag             =   "T"
         Top             =   1680
         Width           =   1290
      End
      Begin VB.CommandButton CmdBuscaProv 
         Caption         =   "Buscar"
         Height          =   255
         Left            =   3465
         TabIndex        =   16
         Top             =   1725
         Width           =   660
      End
      Begin VB.TextBox TxtRsocial 
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2160
         Locked          =   -1  'True
         TabIndex        =   15
         TabStop         =   0   'False
         Top             =   2010
         Width           =   4155
      End
      Begin VB.CommandButton CmdNuevoProveedor 
         Caption         =   "Crear"
         Height          =   255
         Left            =   4125
         TabIndex        =   14
         Top             =   1725
         Width           =   675
      End
      Begin VB.TextBox TxtNDoc 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2160
         TabIndex        =   3
         Tag             =   "N"
         Text            =   "0"
         Top             =   1350
         Width           =   1290
      End
      Begin VB.ComboBox CboTipoDoc 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         ItemData        =   "compra_Ingreso.frx":032A
         Left            =   2160
         List            =   "compra_Ingreso.frx":0337
         Style           =   2  'Dropdown List
         TabIndex        =   2
         ToolTipText     =   $"compra_Ingreso.frx":035C
         Top             =   990
         Width           =   4155
      End
      Begin VB.ComboBox CboPlazos 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         ItemData        =   "compra_Ingreso.frx":0406
         Left            =   2130
         List            =   "compra_Ingreso.frx":0408
         Style           =   2  'Dropdown List
         TabIndex        =   1
         Top             =   630
         Width           =   3465
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel10 
         Height          =   225
         Left            =   1035
         OleObjectBlob   =   "compra_Ingreso.frx":040A
         TabIndex        =   13
         Top             =   2055
         Width           =   1020
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   225
         Index           =   0
         Left            =   585
         OleObjectBlob   =   "compra_Ingreso.frx":0479
         TabIndex        =   17
         Top             =   1410
         Width           =   1485
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   225
         Index           =   0
         Left            =   480
         OleObjectBlob   =   "compra_Ingreso.frx":04EA
         TabIndex        =   18
         Top             =   1050
         Width           =   1575
      End
      Begin MSComCtl2.DTPicker DtFecha 
         Height          =   345
         Left            =   2160
         TabIndex        =   0
         Top             =   285
         Width           =   1605
         _ExtentX        =   2831
         _ExtentY        =   609
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         CustomFormat    =   "dd-mm-yyyy hh:mm:ss"
         Format          =   95617025
         CurrentDate     =   39855
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   225
         Left            =   1005
         OleObjectBlob   =   "compra_Ingreso.frx":0563
         TabIndex        =   19
         Top             =   330
         Width           =   1065
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
         Height          =   225
         Left            =   570
         OleObjectBlob   =   "compra_Ingreso.frx":05D4
         TabIndex        =   20
         Top             =   1725
         Width           =   1500
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel9 
         Height          =   195
         Left            =   1470
         OleObjectBlob   =   "compra_Ingreso.frx":0645
         TabIndex        =   21
         Top             =   660
         Width           =   615
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel8 
         Height          =   195
         Left            =   945
         OleObjectBlob   =   "compra_Ingreso.frx":06A6
         TabIndex        =   26
         Top             =   6390
         Width           =   1140
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel6 
         Height          =   195
         Left            =   615
         OleObjectBlob   =   "compra_Ingreso.frx":070E
         TabIndex        =   27
         Top             =   2715
         Width           =   1455
      End
      Begin MSComctlLib.ListView LvImpuestos 
         Height          =   675
         Left            =   2160
         TabIndex        =   10
         Top             =   5685
         Width           =   4545
         _ExtentX        =   8017
         _ExtentY        =   1191
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   5
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "N109"
            Text            =   "Id"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "T5000"
            Text            =   "Impuesto"
            Object.Width           =   5186
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   2
            Object.Tag             =   "N100"
            Text            =   "Valor"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Object.Tag             =   "N102"
            Text            =   "FACTOR"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Object.Tag             =   "T1000"
            Text            =   "costo/credito"
            Object.Width           =   0
         EndProperty
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel5 
         Height          =   195
         Left            =   660
         OleObjectBlob   =   "compra_Ingreso.frx":0788
         TabIndex        =   28
         Top             =   5400
         Width           =   1455
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel7 
         Height          =   195
         Left            =   615
         OleObjectBlob   =   "compra_Ingreso.frx":07F8
         TabIndex        =   29
         Top             =   3645
         Width           =   1455
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel13 
         Height          =   195
         Left            =   795
         OleObjectBlob   =   "compra_Ingreso.frx":0862
         TabIndex        =   33
         Top             =   3915
         Width           =   1275
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel11 
         Height          =   195
         Left            =   585
         OleObjectBlob   =   "compra_Ingreso.frx":08D8
         TabIndex        =   34
         Top             =   2415
         Width           =   1455
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel14 
         Height          =   195
         Left            =   600
         OleObjectBlob   =   "compra_Ingreso.frx":0942
         TabIndex        =   38
         Top             =   3030
         Width           =   1455
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel15 
         Height          =   195
         Index           =   0
         Left            =   615
         OleObjectBlob   =   "compra_Ingreso.frx":09B2
         TabIndex        =   40
         Top             =   3330
         Width           =   1455
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel16 
         Height          =   195
         Left            =   615
         OleObjectBlob   =   "compra_Ingreso.frx":0A18
         TabIndex        =   42
         Top             =   5145
         Width           =   1455
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   225
         Index           =   1
         Left            =   510
         OleObjectBlob   =   "compra_Ingreso.frx":0A88
         TabIndex        =   46
         Top             =   6750
         Width           =   1575
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel20 
         Height          =   225
         Left            =   570
         OleObjectBlob   =   "compra_Ingreso.frx":0AFF
         TabIndex        =   59
         Top             =   7830
         Width           =   1500
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel21 
         Height          =   225
         Left            =   255
         OleObjectBlob   =   "compra_Ingreso.frx":0B72
         TabIndex        =   61
         Top             =   8115
         Width           =   1815
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel22 
         Height          =   225
         Left            =   150
         OleObjectBlob   =   "compra_Ingreso.frx":0BE5
         TabIndex        =   63
         Top             =   8415
         Width           =   1905
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel15 
         Height          =   195
         Index           =   1
         Left            =   630
         OleObjectBlob   =   "compra_Ingreso.frx":0C54
         TabIndex        =   79
         Top             =   4845
         Width           =   1455
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel15 
         Height          =   195
         Index           =   2
         Left            =   420
         OleObjectBlob   =   "compra_Ingreso.frx":0CCA
         TabIndex        =   81
         Top             =   4215
         Width           =   1650
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel15 
         Height          =   195
         Index           =   3
         Left            =   600
         OleObjectBlob   =   "compra_Ingreso.frx":0D46
         TabIndex        =   83
         Top             =   4515
         Width           =   1455
      End
   End
   Begin VB.Frame FrmLoad 
      Height          =   1680
      Left            =   2520
      TabIndex        =   89
      Top             =   6840
      Visible         =   0   'False
      Width           =   2805
      Begin GIF89LibCtl.Gif89a Gif89a1 
         Height          =   1275
         Left            =   210
         OleObjectBlob   =   "compra_Ingreso.frx":0DB8
         TabIndex        =   90
         Top             =   285
         Width           =   2445
      End
   End
   Begin VB.Frame FraProcesandoDTE 
      Height          =   1230
      Left            =   -150
      TabIndex        =   87
      Top             =   4275
      Visible         =   0   'False
      Width           =   8640
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel12 
         Height          =   510
         Left            =   450
         OleObjectBlob   =   "compra_Ingreso.frx":0E3E
         TabIndex        =   88
         Top             =   390
         Width           =   7590
      End
   End
   Begin VB.TextBox Text2 
      Height          =   420
      Left            =   3975
      TabIndex        =   84
      Text            =   "Text2"
      Top             =   10020
      Width           =   1155
   End
   Begin VB.Frame Frame3 
      Caption         =   "Empresa Activa"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   795
      Left            =   255
      TabIndex        =   64
      Top             =   15
      Width           =   7920
      Begin VB.TextBox SkRut 
         BackColor       =   &H00FFFFC0&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00C00000&
         Height          =   420
         Left            =   120
         TabIndex        =   70
         Text            =   "Text1"
         Top             =   315
         Width           =   2100
      End
      Begin VB.TextBox SkEmpresa 
         BackColor       =   &H00FFFFC0&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00C00000&
         Height          =   420
         Left            =   2235
         TabIndex        =   69
         Text            =   "Text1"
         Top             =   315
         Width           =   5505
      End
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Command1"
      Height          =   300
      Left            =   10755
      TabIndex        =   71
      Top             =   660
      Width           =   195
   End
   Begin VB.TextBox Text1 
      Height          =   480
      Left            =   8685
      TabIndex        =   66
      Text            =   "Text1"
      Top             =   9690
      Visible         =   0   'False
      Width           =   750
   End
   Begin MSComDlg.CommonDialog Dialogo 
      Left            =   0
      Top             =   6525
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   60
      OleObjectBlob   =   "compra_Ingreso.frx":0F06
      Top             =   7425
   End
   Begin VB.Timer Timer1 
      Interval        =   30
      Left            =   555
      Top             =   7455
   End
   Begin MSComctlLib.ListView LvDetalle 
      Height          =   3705
      Left            =   10050
      TabIndex        =   65
      Top             =   9705
      Visible         =   0   'False
      Width           =   7500
      _ExtentX        =   13229
      _ExtentY        =   6535
      View            =   3
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   0
      NumItems        =   5
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Object.Tag             =   "N109"
         Object.Width           =   617
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Object.Tag             =   "N109"
         Text            =   "NRO CHEQUE"
         Object.Width           =   2293
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Key             =   "debe"
         Object.Tag             =   "T3000"
         Text            =   "BANCO"
         Object.Width           =   5292
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   3
         Key             =   "haber"
         Object.Tag             =   "N100"
         Text            =   "DISPONIBLE"
         Object.Width           =   2469
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Object.Tag             =   "N100"
         Text            =   "Utilizar"
         Object.Width           =   2293
      EndProperty
   End
End
Attribute VB_Name = "compra_Ingreso"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public Smp_ActualizaPventa As String
Dim Ip_CenId As Long, Ip_PlaId As Long, Ip_GasId As Long, Ip_AreId As Long
Dim Lp_Neto As String, Lp_NetoTotal As String, Lp_Bruto As String, Lp_BrutoTotal As String
Dim Sm_Nota As String, Bm_Permite_Documento As Boolean, bm_Documento_Permite_Pago As Boolean
Dim Sm_MueveInventario As String * 2, Bm_Neto As Boolean, Lp_Nro_Comprobante As Long
Dim Lm_Top1 As Long, Lm_Top2 As Long
Dim Sm_EsOC As String
Dim Lp_TopFrmProveedor As Long
Dim Lp_Id As Long
Private Declare Function ShellExecute Lib "shell32.dll" Alias "ShellExecuteA" (ByVal hWnd As Long, ByVal lpOperation As String, ByVal lpFile As String, ByVal lpParameters As String, ByVal lpDirectory As String, ByVal nShowCmd As Long) As Long
Private Sub CboFpago_GotFocus()
        On Error Resume Next
    If Me.CboPlazos.ItemData(CboPlazos.ListIndex) = 0 Then
        CboFpago.Clear
        CboFpago.AddItem "CONTADO"
    Else
        CboFpago.Clear
        CboFpago.AddItem "CREDITO"
    End If
    CboFpago.ListIndex = 0
End Sub
Private Sub CboPlazos_Validate(Cancel As Boolean)
        On Error Resume Next
        If Me.CboPlazos.ItemData(CboPlazos.ListIndex) = 0 Then
            CboFpago.Clear
            CboFpago.AddItem "CONTADO"
        ElseIf CboPlazos.ItemData(CboPlazos.ListIndex) = 9999 Then
            CboFpago.Clear
            CboFpago.AddItem "FONDOS POR RENDIR"
        ElseIf CboPlazos.ItemData(CboPlazos.ListIndex) = 4444 Then
            CboFpago.Clear
            CboFpago.AddItem "CON ANTICIPO PROVEEDOR"
        Else
            CboFpago.Clear
            CboFpago.AddItem "CREDITO"
        End If
        CboFpago.ListIndex = 0
End Sub

Private Sub CboTipoDoc_Click()
    CboPlazos.Enabled = True
     txtSubNeto.Locked = False
    If CboTipoDoc.ListIndex > -1 Then
        'TipoDoc
       
        TipoDoc
        If CboTipoDoc.Text = "INVENTARIO INICIAL" Then
            CboPlazos.ListIndex = 0
            CboPlazos.Enabled = False
        End If
    End If
    VerificarUsado
    If Me.CboPlazos.ItemData(CboPlazos.ListIndex) = 0 Then
        CboFpago.Clear
        CboFpago.AddItem "CONTADO"
    ElseIf CboPlazos.ItemData(CboPlazos.ListIndex) = 9999 Then
            CboFpago.Clear
            CboFpago.AddItem "FONDOS POR RENDIR"
    ElseIf CboPlazos.ItemData(CboPlazos.ListIndex) = 4444 Then
            CboFpago.Clear
            CboFpago.AddItem "ANTICIPO PROVEEDOR"
    Else
        CboFpago.Clear
        CboFpago.AddItem "CREDITO"
    End If
    
    CboFpago.ListIndex = 0
End Sub
Private Sub TipoDoc()
'    If Not Bm_Permite_Documento Then Exit Sub
        CboBodega.Enabled = True
        Sm_Nota = Empty
        CboPlazos.Enabled = True
        CboFpago.Enabled = True
        FrameRef.Visible = False
        Sql = "SELECT doc_retiene_iva,doc_permite_pago,doc_movimiento,doc_nota_de_debito debito," & _
                     "doc_nota_de_credito credito,doc_mueve_inventario,doc_orden_de_compra,doc_solo_exento,doc_cod_sii, " & _
                     "(SELECT emp_factura_electronica FROM sis_empresas WHERE rut='" & SP_Rut_Activo & "') fe " & _
              "FROM sis_documentos " & _
              "WHERE doc_id=" & CboTipoDoc.ItemData(CboTipoDoc.ListIndex)
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then 'Consultamos si el documento corresponde
            Sm_EsOC = "NO"
            If RsTmp!doc_orden_de_compra = "SI" Then
                Sm_EsOC = "SI"
                'CORRELATIVO FERTIQUIMICA
                'DEBE SER UNICO PARA TODA LA COMPA�IA, NO POR SUCURSALES
                'EN CUANTO A LAS OC SE REFIERE
                If SP_Rut_Activo = "76.369.600-6" Then
                    TxtNDoc = AutoIncremento("LEE", CboTipoDoc.ItemData(CboTipoDoc.ListIndex), , 1)
                Else
                    TxtNDoc = AutoIncremento("LEE", CboTipoDoc.ItemData(CboTipoDoc.ListIndex), , IG_id_Sucursal_Empresa)
                End If
            End If
            bm_Documento_Permite_Pago = IIf(RsTmp!doc_permite_pago = "SI", True, False)
            Sm_MueveInventario = RsTmp!doc_mueve_inventario
            
            txtSubNeto.Locked = False
            
            If RsTmp!doc_solo_exento = "SI" Then
                txtSubNeto.Locked = True
            End If
            
            CboTipoDoc.Tag = RsTmp!doc_movimiento
            
            
            If RsTmp!doc_retiene_iva = "SI" Then ' IVA retenido
                 txtIvaRetenido.Locked = False
                 txtIvaRetenido.Enabled = True
                 txtIvaRetenido = txtIVA
            Else
                 txtIvaRetenido.Locked = True
                 txtIvaRetenido.Enabled = False
                 txtIvaRetenido = 0
            End If
            If RsTmp!debito = "SI" Or RsTmp!Credito = "SI" Then
                If RsTmp!debito = "SI" Then Sm_Nota = "DEBITO"
                If RsTmp!Credito = "SI" Then Sm_Nota = "CREDITO"
                CboPlazos.Enabled = False
                CboFpago.Enabled = False
                FrameRef.Visible = True
                CboBodega.Enabled = False
             '   CmdBuscar_Click
                TxtNDoc.SetFocus
            End If
            
            If RsTmp!doc_cod_sii = 46 And RsTmp!fe = "SI" Then
                'Buscar folio disponible Fac de Compra
                Sql = "SELECT dte_id,dte_folio numero " & _
                    "FROM dte_folios " & _
                    "WHERE rut_emp='" & SP_Rut_Activo & "' AND doc_id=46 AND dte_venta_compra='COMPRA' AND dte_disponible='SI' " & _
                    "ORDER BY dte_folio " & _
                    "LIMIT 1"
                Consulta RsTmp, Sql
                TxtNDoc = 0
                If RsTmp.RecordCount > 0 Then
                    TxtNDoc = RsTmp!Numero
                End If
            
            End If
            
        End If
End Sub

Private Sub CboTipoDoc_GotFocus()
    CmdLlamaOC.Tag = ""
End Sub

Private Sub CboTipoDoc_KeyDown(KeyCode As Integer, Shift As Integer)
    'TipoDoc
End Sub

Private Sub Check1_Click()
    If Check1.Value = 1 Then
        Option1.Enabled = True
        Option2.Enabled = True
    Else
        Option1.Enabled = False
        Option2.Enabled = False
    End If
    CmdNueva.SetFocus
End Sub

Private Sub CmdBuscaProv_Click()
    AccionProveedor = 0
    BuscaProveedor.Show 1
    TxtRutProveedor = RutBuscado

    TxtRutProveedor_Validate True
End Sub

Private Sub CmdBuscar_Click()
    SG_codigo = Empty
    CompraListaReferencia.Rut = Me.TxtRutProveedor
    TxtDocumentoRef = ""
    TxtNroRef = ""
    TxtTotalRef = 0
    TxtNroRef.Tag = ""
    If Sm_Nota = "DEBITO" Then CompraListaReferencia.Sm_Nota = " doc_nota_de_debito='NO' "
    If Sm_Nota = "CREDITO" Then CompraListaReferencia.Sm_Nota = " doc_nota_de_credito='NO' "
    CompraListaReferencia.Show 1
    If SG_codigo <> Empty Then
        Sql = "SELECT c.id,c.doc_id,doc_nombre,no_documento,total,iva_retenido,bod_id,pago,com_plazo " & _
              "FROM com_doc_compra c " & _
              "INNER JOIN sis_documentos d USING(doc_id) " & _
              "WHERE  id=" & SG_codigo
        Consulta RsTmp2, Sql
        If RsTmp2.RecordCount > 0 Then
            TxtDocumentoRef = RsTmp2!doc_nombre
            TxtDocumentoRef.Tag = RsTmp2!ID
            FrameRef.Tag = RsTmp2!doc_id
            TxtNroRef = RsTmp2!no_documento
            TxtTotalRef = NumFormat(RsTmp2!Total)
            TxtNroRef.Tag = SG_codigo
            If RsTmp2!iva_retenido > 0 Then
                txtIvaRetenido.Enabled = True
                txtIvaRetenido.Locked = False
            End If
            Busca_Id_Combo CboBodega, Val(RsTmp2!bod_id)
            Busca_Id_Combo CboPlazos, Val(RsTmp2!com_plazo)
            CboPlazos_Validate True
            TxtExento.SetFocus
            'Check1.Value = 0
        End If
    End If
End Sub

Private Sub cmdCuenta_Click()
    With BuscarSimple
        SG_codigo = Empty
        .Sm_Consulta = "SELECT pla_id, pla_nombre,tpo_nombre,det_nombre " & _
                       "FROM    con_plan_de_cuentas p,  con_tipo_de_cuenta t,   con_detalle_tipo_cuenta d " & _
                       "WHERE   p.tpo_id = t.tpo_id AND p.det_id = d.det_id "
        .Sm_CampoLike = "pla_nombre"
        .Im_Columnas = 2
        .CmdCuentasContables.Visible = True
        .Show 1
        If SG_codigo = Empty Then Exit Sub
        TxtCodigoCuenta = Val(SG_codigo)
        TxtCodigoCuenta.SetFocus
    End With
End Sub

Private Sub CmdLlamaOC_Click()
    Dim Sm_Bruto_Neto As String
    fraSaldoOC.Visible = False
    If Len(TxtRutProveedor) = 0 Then
        MsgBox "Debe Seleccionar Proveedor...", vbInformation
        TxtRutProveedor.SetFocus
        Exit Sub
    End If
    'Buscamos OC del proveedor
    Sql = "SELECT id,no_documento nro_oc,fecha,rut,nombre_proveedor,neto,iva,total " & _
          "FROM com_doc_compra c " & _
          "WHERE com_estado_oc='PROCESO' AND  rut_emp='" & SP_Rut_Activo & "' AND doc_id=" & IG_id_OrdenCompra & " AND rut='" & TxtRutProveedor & "'"
    SG_codigo = Empty
    Com_OC_Pendientes.Sm_Sql = Sql
    Com_OC_Pendientes.TxtRutProveedor = TxtRutProveedor
    Com_OC_Pendientes.TxtRsocial = TxtRsocial
    Com_OC_Pendientes.TxtRutProveedor.Locked = True
    Com_OC_Pendientes.CmdBuscaProv.Enabled = False
    
    Com_OC_Pendientes.Show 1
    If SG_codigo = Empty Then
        CmdLlamaOC.Tag = Empty
        CmdLlamaOC.Caption = "Carga Orden de Compra"
        sg_codig2 = Empty
        Exit Sub
    End If
    CmdLlamaOC.Tag = SG_codigo
    fraSaldoOC.Visible = True
    txtSaldoOC = NumFormat(SaldoDocumentoOC(Me.CmdLlamaOC.Tag, "PRO"))
    If CDbl(txtSaldoOC) > 0 Then MsgBox "Existen abonos en OC(s), para utilizar este abono, el tipo de pago debe ser CREDITO...", vbInformation
    
    'CmdLlamaOC.Caption = "ORDEN DE COMPRA NRO " & SG_codigo2
    
    
    'Cargamos la OC seleccionada
    Sql = "SELECT pago,SUM(neto) Neto,SUM(iva) IVA,SUM(total),SUM(com_exe_otros_imp) Exento,SUM(especifico),SUM(iva_retenido) iva_retenido,SUM(com_bruto_neto) com_bruto_neto," & _
            "com_solicitado_por,com_autorizado_por " & _
          "FROM  com_doc_compra " & _
          "WHERE id IN(" & SG_codigo & ")"
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        TxtExento = NumFormat(RsTmp!Exento)
        TxtNeto = NumFormat(RsTmp!Neto)
        txtIVA = NumFormat(RsTmp!Iva)
        txtIvaRetenido = NumFormat(RsTmp!iva_retenido)
        TxtSubTotal = NumFormat(CDbl(TxtExento) + CDbl(TxtNeto) + CDbl(txtIVA) - CDbl(txtIvaRetenido))
        If RsTmp!pago = "CONTADO" Then CboFpago.ListIndex = 0 Else CboFpago.ListIndex = 0
        Sm_Bruto_Neto = RsTmp!com_bruto_neto
        TxtSolicitadoPor = RsTmp!com_solicitado_por
        TxtAutorizadoPor = RsTmp!com_autorizado_por
        If Sm_Bruto_Neto = "NETO" Then
            Bm_Neto = True
        Else
            Bm_Neto = False
        End If
    End If
    'Cargamos Impuestos del documento seleccionado
    Sql = "SELECT e.imp_id,CONCAT(imp_nombre,'    (',LCASE(ime_costo_credito),')') ," & _
           "(SELECT SUM(coi_valor) coi_valor FROM com_impuestos c WHERE c.imp_id=i.imp_id AND c.id IN(" & SG_codigo & ")) valor," & _
           "0 factor,ime_costo_credito  " & _
          "FROM par_impuestos_empresas e " & _
          "INNER JOIN par_impuestos i USING(imp_id) " & _
          "WHERE e.rut='" & SP_Rut_Activo & "'"
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvImpuestos, False, True, True, False
    For i = 1 To LvImpuestos.ListItems.Count
        If LvImpuestos.ListItems(i).SubItems(4) = "COSTO" Then
            SkImpCosto = SkImpCosto + CDbl(LvImpuestos.ListItems(i).SubItems(2))
        End If
        SkTotalAdicionales = SkTotalAdicionales + CDbl(LvImpuestos.ListItems(i).SubItems(2))
    Next
    LvImpuestos.Tag = SkTotalAdicionales
   'TxtImpuesto.Tag = 1
  '  CalculaAdicionales
    
    
    Factores
    CTotal
  '  With compra_detalle
        
  
  '      compra_detalle.Show 1
        'compra_detalle.Hide
  '  End With
    
End Sub

Private Sub CmdNueva_Click()
    If Len(TxtRutProveedor) = 0 Then
        MsgBox "Falta ingresar proveedor..", vbInformation
        TxtRutProveedor.SetFocus
        Exit Sub
    End If
    If CboTipoDoc.ListIndex = -1 Then
        MsgBox "Seleccione documento..", vbInformation
        CboTipoDoc.SetFocus
        Exit Sub
    End If
    If Val(TxtNDoc) = 0 Then
        MsgBox "Falta Nro Documento..", vbInformation
        TxtNDoc.SetFocus
        Exit Sub
    End If
    If Val(txtTotal) < 1 Then
        If Sm_Nota <> "CREDITO" Then
            MsgBox "Faltan Valores...", vbInformation
            Exit Sub
        End If
    End If
       
        'validacion de la fecha
    If Val(Year(DtFecha)) < IG_Ano_Contable Then
       
    Else
        If (Month(DtFecha) + Year(DtFecha)) > (IG_Ano_Contable + IG_Mes_Contable) Then
            MsgBox "No puede ingresar documentos con FECHA posterior al periodo contable...", vbInformation
            DtFecha.SetFocus
            Exit Sub
        End If
    End If
    If CboFpago.ListIndex = -1 Then
        MsgBox "Seleccione forma de pago...", vbInformation
        CboFpago.SetFocus
        Exit Sub
    End If
    
    If Not Bm_Permite_Documento Then Exit Sub
    
    If SG_Modulo_Caja = "SI" Then
        If FrmCaja.Visible = False Then
        
            If Mid(CboPlazos.Text, 1, 7) = "CONTADO" Then MsgBox " Este valor se ver� reflejado en el resumen de su caja...  ", vbInformation
        End If
    End If

    
    If Check1.Value = 1 Then
        With compra_detalle
             
            .TxtNeto = NumFormat(CDbl(TxtNeto) + CDbl(TxtExento) + CDbl(Me.TxtRecargoxFlete))
            .txtIVA = txtIVA
            .txtIvaRetenido = txtIvaRetenido
            .TxtDSubTotal = TxtSubTotal
            .TxtImp = NumFormat(LvImpuestos.Tag)
            .TxtDTotal = txtTotal
            .SkImpCosto = SkImpCosto
            .TxtExento = TxtExento
            .txtIvaSinCredito = txtIvaSinCredito
            .txtIVAMargen = txtIVAMargen
            .txtMargenDist = Me.txtMargenDist
            .Neto = False
            If Option1.Value Then
                .Neto = True
                Bm_Neto = True
                .SkNetoBrutos = "USTED ESTA INGRESANDO VALORES NETOS"
            Else
                Bm_Neto = False
                .Neto = False
                .SkNetoBrutos = "USTED ESTA INGRESANDO VALORES BRUTOS"
            End If
            
            .Sm_Nota = Empty
            If Sm_Nota = "DEBITO" Or Sm_Nota = "CREDITO" Then
                If Val(TxtNroRef) = 0 Then
                    MsgBox "Debe seleccionar documento al que quiere hacer una Nota de Debito ", vbInformation
                    Me.CmdBuscar.SetFocus
                    Exit Sub
                End If
                .Sm_Modifica_Unidades = "NO"
                .Sm_Modifica_Valores = "NO"
                .Sm_Nota = Sm_Nota
                .Sm_Referencia = TxtDocumentoRef.Tag
                .CargaReferencia
                If Sm_Nota = "CREDITO" Then .FrameNC.Visible = True
                .Sm_Tipo_Nota_Credito = Empty
            End If
            If Val(CmdLlamaOC.Tag) > 0 Then
                If Bm_Neto Then
                    If SP_Rut_Activo = "76.178.895-7" Then
                        'TotalGomas
                        'Aqui vendran solo los seleccionados.
                        SG_codigo3 = ""
                        com_productos_en_oc.Sm_Codigo_Proveedor = TxtRutProveedor
                        com_productos_en_oc.Lm_id_Compra = SG_codigo
                        com_productos_en_oc.Show 1
                        If Len(SG_codigo3) = 0 Then
                            SG_codigo3 = "0"
                        End If
                        
                        Sql = "SELECT cmd_id,pro_codigo," & _
                        "CONCAT(((SELECT cpv_codigo_proveedor FROM par_codigos_proveedor WHERE rut_proveedor='" & TxtRutProveedor & "' AND pro_codigo=m.codigo LIMIT 1)),' ',    IFNULL(descripcion, cmd_detalle)) DETALLE, " & _
                                "tip_nombre,cmd_unitario_neto,cmd_cantidad,ume_nombre,cmd_total_neto+cmd_exento, " & _
                            "d.pla_id,d.cen_id,d.gas_id,d.are_id,0,0,0,cmd_total_neto,0,0,pro_inventariable, " & _
                            "    cmd_total_neto + cmd_exento netoreal," & _
                            "cmd_total_neto + cmd_exento costofinal,0,'' ccc,0,0," & _
                            "(SELECT precio_venta FROM maestro_productos p WHERE d.pro_codigo=p.codigo) " & _
                          "FROM com_doc_compra_detalle d " & _
                          "INNER JOIN com_doc_compra c USING(id) " & _
                          "LEFT JOIN maestro_productos m ON m.codigo = d.pro_codigo AND m.rut_emp=c.rut_emp " & _
                          "LEFT JOIN par_tipos_productos t USING(tip_id) " & _
                          "LEFT JOIN sis_unidad_medida u USING(ume_id) " & _
                          "WHERE d.cmd_id IN(" & SG_codigo3 & ") " & _
                          "ORDER BY cmd_seleccion_oc"
                    Else
                    
                        Sql = "SELECT cmd_id,pro_codigo,IFNULL(descripcion,cmd_detalle),tip_nombre,cmd_unitario_neto,cmd_cantidad,ume_nombre,cmd_total_neto+cmd_exento, " & _
                            "d.pla_id,d.cen_id,d.gas_id,d.are_id,0,0,0,cmd_total_neto,0,0,pro_inventariable, " & _
                            "    cmd_total_neto + cmd_exento netoreal," & _
                            "cmd_total_neto + cmd_exento costofinal,0,'' ccc,0,0," & _
                            "(SELECT precio_venta FROM maestro_productos p WHERE d.pro_codigo=p.codigo) " & _
                          "FROM com_doc_compra_detalle d " & _
                          "INNER JOIN com_doc_compra c USING(id) " & _
                          "LEFT JOIN maestro_productos m ON m.codigo = d.pro_codigo AND m.rut_emp=c.rut_emp " & _
                          "LEFT JOIN par_tipos_productos t USING(tip_id) " & _
                          "LEFT JOIN sis_unidad_medida u USING(ume_id) " & _
                          "WHERE d.id IN(" & SG_codigo & ")"
                    End If
                Else
                    Sql = "SELECT cmd_id,pro_codigo,IFNULL(descripcion,cmd_detalle),tip_nombre,cmd_unitario_bruto,cmd_cantidad,ume_nombre,cmd_total_bruto+cmd_exento, " & _
                                     "d.pla_id,d.cen_id,d.gas_id,d.are_id,0,0,0,cmd_total_bruto,0,0,pro_inventariable " & _
                              "FROM com_doc_compra_detalle d " & _
                              "INNER JOIN com_doc_compra c USING(id) " & _
                              "LEFT JOIN maestro_productos m ON m.codigo = d.pro_codigo AND m.rut_emp=c.rut_emp " & _
                              "LEFT JOIN par_tipos_productos t USING(tip_id) " & _
                              "LEFT JOIN sis_unidad_medida u USING(ume_id) " & _
                              "WHERE d.id IN (" & SG_codigo & ")"
                End If
                Consulta RsTmp, Sql
                LLenar_Grilla RsTmp, Me, .LvDetalle, False, True, True, False
                .txtTotal = NumFormat(TotalizaColumna(.LvDetalle, "total"))
                .TxtGtotal = NumFormat(CDbl(.txtTotal) - CDbl(.txtDescuento))
            End If
            .Show 1
            If compra_detalle.Sp_Accion = "GRABAR" Then GrabarCompra
            If compra_detalle.Sp_Accion = "SALIR" Then
                Unload compra_detalle
                Unload Me
            End If
        End With
    End If
End Sub
Private Sub GrabarCompra()
    Dim Sp_SoloUnidades As String, Sp_SoloValores As String, mFinal As String, Sp_Folio As String, Sp_FolioNro As Long
    Dim Bp_Comprobante_Pago As Boolean, Sp_OC As String, Sp_ImpCompPago As String * 2, Lp_SaldoRef As Long, Sp_NC_Utilizada As String * 2
    Dim Sp_NroIdRef As String, Lp_SaldoOC As Long
    Dim Bp_NC As Boolean
    
    Bp_Comprobante_Pago = False
    Sp_SoloUnidades = "NO"
    Sp_SoloValores = "NO"
    mFinal = Empty
    Bp_NC = False
    Lp_Id = 0
    Lp_Id = UltimoNro("com_doc_compra", "id")
    Sp_Folio = "0"
    Sp_FolioNro = 0
    
    On Error GoTo ErrorGrabacion
    cn.BeginTrans
    'Documento de compra
    Sp_NC_Utilizada = "NO"
    SG_codigo2 = Empty
    Sp_NroIdRef = "0"
    'Si selecciona Fondos por rendir como medio de pago
    'Mostrar ventana para buscar documentos emitidos con cta fondos por rendir
    FrmLoad.Visible = True
    DoEvents
    If Val(Me.CmdLlamaOC.Tag) > 0 Then
    '    bm_Documento_Permite_Pago = False
        
        If SP_Rut_Activo = "76.178.895-7" Then
            'totalgomas
            '03-mayo-2017
                'ordendecompra
            Dim Sp_ProductosOC As String
            For X = 1 To compra_detalle.LvDetalle.ListItems.Count
                Sp_ProductosOC = Sp_ProductosOC & compra_detalle.LvDetalle.ListItems(X).SubItems(1) & ","
            Next
            Sp_ProductosOC = Mid(Sp_ProductosOC, 1, Len(Sp_ProductosOC) - 1)
            cn.Execute "UPDATE  com_doc_compra_detalle set cmd_oc_facturada='SI' " & _
                    "WHERE  pro_codigo IN(" & Sp_ProductosOC & ") AND id=" & CmdLlamaOC.Tag
        Else
            Sql = "UPDATE com_doc_compra SET com_estado_oc='TERMINADA',id_ref=" & Lp_Id & " " & _
                "WHERE id IN(" & CmdLlamaOC.Tag & ")"
            cn.Execute Sql
        End If
        
        If CDbl(txtSaldoOC) > 0 And ChkUtilizaOC.Value = 1 Then
            'Debemos abonar este valor al documento
            'si sobra, se debe incluir en el pozo,
            'si el pago es contado, se debe restar este monto tambien
            '3 Marz 2014
          
                Lp_Nro_Comprobante = AutoIncremento("lee", 200, , IG_id_Sucursal_Empresa)
            
                Dim Lp_IdAbo As Long
                Bp_Comprobante_Pago = True
                Lp_IdAbo = UltimoNro("cta_abonos", "abo_id")
                PagoDocumento Lp_IdAbo, "PRO", Me.TxtRutProveedor, DtFecha, CDbl(txtSaldoOC), 999, "ABONOS EN OC", "PAGADO", LogUsuario, Lp_Nro_Comprobante, 0, "COMPRA"
                Sql = "INSERT INTO cta_abono_documentos (abo_id,id,ctd_monto,rut_emp) VALUES "
                Sql = Sql & "(" & Lp_IdAbo & "," & Lp_Id & "," & CDbl(txtSaldoOC) & ",'" & SP_Rut_Activo & "')"
                cn.Execute Sql
                AutoIncremento "GUARDA", 200, Lp_Nro_Comprobante, IG_id_Sucursal_Empresa
        End If
        
    End If
   'SI es nota de debito, indicar cual sera el nro de ref
    If Sm_Nota = "DEBITO" Or Sm_Nota = "CREDITO" And Val(TxtDocumentoRef.Tag) > 0 Then Sp_NroIdRef = TxtDocumentoRef.Tag
    
    If bm_Documento_Permite_Pago And CboFpago.Text <> "CREDITO" Then
    
        'Aqui realizar abono a la cta cte
        '8 Octubre 2011
        'Comprobar si este documento ya tiene un pago
        'Sql = "DELETE FROM cta_abonos WHERE "
        Dim Lp_RestaOC As Long
        Dim Lp_MontoPagar As Long
        Dim Lp_EnviarAPozo As Long
        Lp_MontoPagar = CDbl(txtTotal)
        Lp_EnviarAPozo = 0
        If Val(txtSaldoOC) > 0 And ChkUtilizaOC.Value = 1 And fraSaldoOC.Visible Then
            Lp_RestaOC = CDbl(txtSaldoOC)
            If CDbl(txtTotal) - Lp_RestaOC >= 0 Then
                Lp_MontoPagar = Lp_MontoPagar - Lp_RestaOC
            Else
                Lp_MontoPagar = CDbl(txtTotal)
                Lp_EnviarAPozo = Lp_RestaOC - Lp_MontoPagar
            End If
        End If
         
        Lp_Nro_Comprobante = AutoIncremento("lee", 200, , IG_id_Sucursal_Empresa)
    
     '   Dim lp_IDAbo As Long
        Bp_Comprobante_Pago = True
        Lp_IdAbo = UltimoNro("cta_abonos", "abo_id")
        PagoDocumento Lp_IdAbo, "PRO", Me.TxtRutProveedor, DtFecha, Lp_MontoPagar, IIf(Mid(CboPlazos.Text, 1, 7) = "CONTADO", 1, Right(CboPlazos.ItemData(CboPlazos.ListIndex), 5)), CboFpago.Text, "PAGADO", LogUsuario, Lp_Nro_Comprobante, 0, "COMPRA"
        Sql = "INSERT INTO cta_abono_documentos (abo_id,id,ctd_monto,rut_emp) VALUES "
        Sql = Sql & "(" & Lp_IdAbo & "," & Lp_Id & "," & Lp_MontoPagar & ",'" & SP_Rut_Activo & "')"
        cn.Execute Sql
        
        'Si por alguna razon nos sobra para el pozo, lo insertamos oqui
        If Lp_EnviarAPozo > 0 Then
            
            Sql = "INSERT INTO cta_pozo (id_ref,pzo_fecha,rut_emp,pzo_cli_pro,pzo_rut,pzo_abono) " & _
                "VALUES(" & Lp_Id & ",'" & Fql(DtFecha) & "','" & SP_Rut_Activo & "','PRO','" & TxtRutProveedor & "'," & Lp_EnviarAPozo & ")"
            cn.Execute Sql
        
        End If
        
        AutoIncremento "GUARDA", 200, Lp_Nro_Comprobante, IG_id_Sucursal_Empresa
        
        If CboPlazos.ItemData(CboPlazos.ListIndex) = 9999 Then
            Ban_SeleccionaChequeRendir.Sm_Rut_Anticipo = ""
            Ban_SeleccionaChequeRendir.Sm_EnviarA = "COMPRAS"
            Ban_SeleccionaChequeRendir.TxtRequerido = txtTotal
            Ban_SeleccionaChequeRendir.TxtSaldo = txtTotal
            
            Ban_SeleccionaChequeRendir.Show 1
            If SG_codigo = Empty Then
                cn.RollbackTrans
                Exit Sub
            End If
            Sql = "INSERT INTO ban_cheques_fondos_rendir (che_id,chf_debe,abo_id) VALUES"
            For i = 1 To LvDetalle.ListItems.Count
                Sql = Sql & "(" & LvDetalle.ListItems(i) & "," & CDbl(LvDetalle.ListItems(i).SubItems(4)) & "," & Lp_IdAbo & "),"
            Next
            Sql = Mid(Sql, 1, Len(Sql) - 1)
            cn.Execute Sql
        End If
        
        'Anticipo de proveedor, siempre que tenga disponible
        '5-4-2014
        If CboPlazos.ItemData(CboPlazos.ListIndex) = 4444 Then
            Ban_SeleccionaChequeRendir.Sm_EnviarA = "COMPRAS"
            Ban_SeleccionaChequeRendir.Sm_Rut_Anticipo = TxtRutProveedor
            Ban_SeleccionaChequeRendir.TxtRequerido = txtTotal
            Ban_SeleccionaChequeRendir.TxtSaldo = txtTotal
            Ban_SeleccionaChequeRendir.Show 1
            If SG_codigo = Empty Then
                cn.RollbackTrans
                Exit Sub
            End If
            Sql = "INSERT INTO ban_cheques_fondos_rendir (che_id,chf_debe,abo_id) VALUES"
            For i = 1 To LvDetalle.ListItems.Count
                Sql = Sql & "(" & LvDetalle.ListItems(i) & "," & CDbl(LvDetalle.ListItems(i).SubItems(4)) & "," & Lp_IdAbo & "),"
            Next
            Sql = Mid(Sql, 1, Len(Sql) - 1)
            cn.Execute Sql
        End If
    End If
    Sql = "SELECT doc_contable,doc_orden_de_compra,doc_nota_de_credito  " & _
          "FROM sis_documentos " & _
          "WHERE doc_id=" & CboTipoDoc.ItemData(CboTipoDoc.ListIndex)
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        Sp_OC = RsTmp!doc_orden_de_compra
        
        'If RsTmp!doc_contable = "SI" Then
              
            Sql = "SELECT IFNULL(MAX(com_folio)+1,1) folio " & _
                  "FROM com_doc_compra " & _
                  "WHERE rut_emp='" & SP_Rut_Activo & "' AND ano_contable=" & TxtAnoContable & " AND  mes_contable=" & IG_Mes_Contable
            Consulta RsTmp2, Sql
            If RsTmp2.RecordCount > 0 Then
                Sp_FolioNro = RsTmp2!Folio
                Sp_Folio = RsTmp2!Folio
                SG_codigo2 = Empty
                sis_InputBox.texto.PasswordChar = ""
                sis_InputBox.texto.Locked = True
                sis_InputBox.Caption = "Tome nota"
                sis_InputBox.FramBox = "Folio para libro contable"
                sis_InputBox.CmdCancelar.Visible = False
                Sp_Folio = Right("00" & IG_Mes_Contable, 2) & Right("000" & Sp_Folio, 3)
                sis_InputBox.texto = Sp_Folio
                FrmLoad.Visible = False
                sis_InputBox.Show 1
                FrmLoad.Visible = True
            End If
    '    End If
        
        If RsTmp!doc_nota_de_credito = "SI" Then
            Bp_NC = True
            'Buscar el documento referenciado    /// 20-10-2012
            'y ver si su saldo.
            'si el saldo es mayor o igual a la NC abonar el monto de la nota de credito
            
            Lp_SaldoRef = SaldoDocumento(Str(Val(Me.TxtNroRef.Tag)), "PRO")
            
            Dim Lp_Abono_a_Realizar As Long
            Dim Lp_Abono_a_Pozo As Long
            
            'If Lp_SaldoRef >= CDbl(TxtTotal) Then
            If Lp_SaldoRef > 0 Then
                    'hacer abono
                    lp_abono_a_plazo = 0
                    
                    If Lp_SaldoRef >= CDbl(txtTotal) Then
                        Lp_Abono_a_Realizar = CDbl(txtTotal)
                    Else
                        Lp_Abono_a_Realizar = Lp_SaldoRef
                        Lp_Abono_a_Pozo = CDbl(txtTotal) - Lp_SaldoRef
                    End If

                    
                    
                    MsgBox "El monto " & NumFormat(Lp_Abono_a_Realizar) & " de esta NC se abono al documento original...", vbInformation
                    Lp_Nro_Comprobante = AutoIncremento("lee", 200, , IG_id_Sucursal_Empresa)
                
                    'Dim lp_IDAbo As Long
                    Bp_Comprobante_Pago = True
                    Lp_IdAbo = UltimoNro("cta_abonos", "abo_id")
                    PagoDocumento Lp_IdAbo, "PRO", Me.TxtRutProveedor, DtFecha, CDbl(Lp_Abono_a_Realizar), 8888, "NC", "ABONO CON NC", LogUsuario, Lp_Nro_Comprobante, Lp_Id, "ncompra"
                    Sql = "INSERT INTO cta_abono_documentos (abo_id,id,ctd_monto,rut_emp) VALUES "
                    Sql = Sql & "(" & Lp_IdAbo & "," & Val(Me.TxtNroRef.Tag) & "," & CDbl(Lp_Abono_a_Realizar) & ",'" & SP_Rut_Activo & "')"
                    cn.Execute Sql
                    
                    AutoIncremento "GUARDA", 200, Lp_Nro_Comprobante, IG_id_Sucursal_Empresa
                    
                    Sp_NC_Utilizada = "SI"
                    If Lp_Abono_a_Pozo > 0 Then
                        
                        Sql = "INSERT INTO cta_pozo (id_ref,pzo_fecha,rut_emp,pzo_cli_pro,pzo_rut,pzo_abono) " & _
                            "VALUES(" & Lp_Id & ",'" & Fql(DtFecha) & "','" & SP_Rut_Activo & "','PRO','" & TxtRutProveedor & "'," & Lp_Abono_a_Pozo & ")"
                        cn.Execute Sql
                    
                    End If
                    
                    
            Else
                    Lp_Abono_a_Pozo = CDbl(txtTotal)
                    'PagoDocumento Lp_Id, "PRO", Me.TxtRutProveedor, Me.DtFecha, Lp_SaldoRef, 8888, "DESCUENTA NC", "", LogUsuario,
                    Sql = "INSERT INTO cta_pozo (id_ref,pzo_fecha,rut_emp,pzo_cli_pro,pzo_rut,pzo_abono) " & _
                        "VALUES(" & Lp_Id & ",'" & Fql(DtFecha) & "','" & SP_Rut_Activo & "','PRO','" & TxtRutProveedor & "'," & Lp_Abono_a_Pozo & ")"
                    cn.Execute Sql
                    Sp_NC_Utilizada = "SI"
            End If
            
        
        
        End If
        
        
    End If

    
    If Sp_OC = "SI" Then
        If SP_Rut_Activo = "76.369.600-6" Then
            'SOLO PARA FERTIQUIMICA
            AutoIncremento "GUARDA", Me.CboTipoDoc.ItemData(CboTipoDoc.ListIndex), TxtNDoc, 1
        Else
        
            AutoIncremento "GUARDA", Me.CboTipoDoc.ItemData(CboTipoDoc.ListIndex), TxtNDoc, IG_id_Sucursal_Empresa
        End If
    
    End If
    
    lacajita = 0
    If SG_Modulo_Caja = "SI" Then
        If Mid(CboPlazos.Text, 1, 7) = "CONTADO" Then
            If CboCaja.Text = "SI" Then
                lacajita = LG_id_Caja
            End If
        End If
    End If
    
    If CboBodega.ListIndex = -1 Then CboBodega.ListIndex = 0

    
    
    Sql = "INSERT INTO com_doc_compra (id,doc_id,no_documento,rut,nombre_proveedor,neto,iva,total,iva_retenido,total_impuestos,fecha,tipo_movimiento,mes_contable,ano_contable,doc_fecha_vencimiento,rut_emp,com_bruto_neto,bod_id,com_exe_otros_imp," & _
                      "pago,com_folio,com_folio_texto,com_plazo,com_solicitado_por,com_autorizado_por,com_nc_utilizada,com_id_ref_de_nota_debito,caj_id,com_pla_id,com_retirado_por,com_tasa_iva,com_iva_sin_credito,com_precios_venta_revisados) " & _
          "VALUES(" & Lp_Id & "," & CboTipoDoc.ItemData(CboTipoDoc.ListIndex) & "," & TxtNDoc & ",'" & TxtRutProveedor & "','" & TxtRsocial & "'," & CDbl(TxtNeto) & "," & CDbl(txtIVA) & "," & CDbl(txtTotal) & "," & _
           CDbl(txtIvaRetenido) & "," & CDbl(SkTotalAdicionales) & ",'" & Fql(DtFecha.Value) & "','INGRESO COMPRA'," & IG_Mes_Contable & "," & IG_Ano_Contable & ",'" & Fql(DtFecha + CboPlazos.ItemData(CboPlazos.ListIndex)) & _
           "','" & SP_Rut_Activo & "','" & IIf(Option1.Value, "NETO", "BRUTO") & "'," & CboBodega.ItemData(CboBodega.ListIndex) & "," & CDbl(TxtExento) & ",'" & CboFpago.Text & "'," & Sp_FolioNro & ",'" & Sp_Folio & "'," & CboPlazos.ItemData(CboPlazos.ListIndex) & ",'" & _
           TxtSolicitadoPor & "','" & TxtAutorizadoPor & "','" & Sp_NC_Utilizada & "'," & Sp_NroIdRef & "," & lacajita & "," & Val(Right(CboPlazos.Text, 5)) & ",'" & Me.TxtRetiradoPor & "','" & SkTasaIVA & "'," & CDbl(Me.txtIvaSinCredito) & ",'NO')"
    cn.Execute Sql
    'Impuestos adicionales
    If SkTotalAdicionales > 0 Then
        Sql = "INSERT INTO com_impuestos(id,imp_id,coi_valor) VALUES  "
        For i = 1 To Me.LvImpuestos.ListItems.Count
            If CDbl(LvImpuestos.ListItems(i).SubItems(2)) > 0 Then Sql = Sql & "(" & Lp_Id & "," & LvImpuestos.ListItems(i) & "," & CDbl(LvImpuestos.ListItems(i).SubItems(2)) & "),"
        Next
        Sql = Mid(Sql, 1, Len(Sql) - 1)
        cn.Execute Sql
    End If
    'Items detalle
    With compra_detalle.LvDetalle
        Dim Lp_Id_Detalle As Long
        
       
        For i = 1 To compra_detalle.LvDetalle.ListItems.Count
             Sql = "INSERT INTO com_doc_compra_detalle (cmd_id,id,pro_codigo,cmd_cantidad,cmd_unitario_neto,cmd_unitario_bruto, " & _
                    "cmd_total_neto,cmd_total_bruto,pla_id,cen_id,gas_id,are_id,cmd_detalle,cmd_costo_real_neto,cmd_exento,cmd_nro_linea,aim_id) VALUES "
                    
            Lp_Id_Detalle = UltimoNro("com_doc_compra_detalle", "cmd_id")
            
            Ip_CenId = .ListItems(i).SubItems(9)
            Ip_PlaId = .ListItems(i).SubItems(8)
            Ip_GasId = .ListItems(i).SubItems(10)
            Ip_AreId = .ListItems(i).SubItems(11)
            If Option1.Value Then
                Lp_Neto = CxP(CDbl(.ListItems(i).SubItems(17)) / CDbl(.ListItems(i).SubItems(5)))
                Lp_NetoTotal = CxP(CDbl(.ListItems(i).SubItems(17)))
                Lp_Bruto = "0"
                Lp_BrutoTotal = "0"
            Else
                'Aunque ingresamos valores Brutos y descuentos, igual guardaremos el valor neto o costo real
                '21 Diciembre 2011
                Lp_Neto = CxP(CDbl(.ListItems(i).SubItems(17)) / CDbl(.ListItems(i).SubItems(5)))
                Lp_NetoTotal = CxP(CDbl(.ListItems(i).SubItems(17)))
                'Lp_Neto = CxP(.ListItems(i).SubItems(19))
                'L'p_NetoTotal = CDbl(.ListItems(i).SubItems(19)) * CDbl(.ListItems(i).SubItems(5))
                'Lp_Neto = "0"
                'Lp_NetoTotal = "0"
                Lp_Bruto = CxP(CDbl(.ListItems(i).SubItems(4)))
                Lp_BrutoTotal = CxP(CDbl(.ListItems(i).SubItems(7)))
            End If
            codigoaim = 0
            If .ListItems(i).SubItems(26) = "SI" Then codigoaim = .ListItems(i).SubItems(1)
            Lp_Exento = CxP(CDbl(.ListItems(i).SubItems(21)))
            Sql = Sql & "(" & Lp_Id_Detalle & "," & Lp_Id & ",'" & .ListItems(i).SubItems(1) & "'," & CxP(CDbl(.ListItems(i).SubItems(5))) & "," & Lp_Neto & "," & _
            Lp_Bruto & "," & Lp_NetoTotal & "," & Lp_BrutoTotal & "," & Ip_PlaId & "," & Ip_CenId & "," & Ip_GasId & "," & Ip_AreId & ",'" & _
            .ListItems(i).SubItems(2) & "'," & CxP(CDbl(.ListItems(i).SubItems(20))) & "," & CxP(CDbl(.ListItems(i).SubItems(21))) & "," & i & "," & codigoaim & "),"
            
            '25 Octubre 2014
            'Grabaremos la penderacion de los impuestos linea a linea, (harina, carnes, etc)
            If compra_detalle.LvImpuestosInd.ListItems.Count > 0 Then
                SQL4 = ""
                For p = 1 To compra_detalle.LvImpuestosInd.ListItems.Count
                    If compra_detalle.LvImpuestosInd.ListItems(p) = i Then
                        SQL4 = SQL4 & "(" & Lp_Id_Detalle & "," & compra_detalle.LvImpuestosInd.ListItems(p).SubItems(1) & "," & _
                        IG_id_Empresa & "," & compra_detalle.LvImpuestosInd.ListItems(p).SubItems(2) & "),"
                    End If
                Next
                If Len(SQL4) > 0 Then
                    SQL4 = "INSERT INTO com_doc_compra_detalle_impuestos (cmd_id,imp_id,emp_id,cdi_valor) VALUES " & Mid(SQL4, 1, Len(SQL4) - 1)
                    cn.Execute SQL4
                End If
                
            End If
            
            If .ListItems(i).SubItems(26) = "NO" Then 'no es AIM
                SQL4 = "UPDATE maestro_productos SET pro_ped_id=0,pro_ped_cantidad=0,pro_ped_fecha_expira='1900-01-01' " & _
                            "WHERE codigo= " & .ListItems(i).SubItems(1)
                cn.Execute SQL4
            End If
            If Smp_ActualizaPventa = "SI" And Not Bp_NC And .ListItems(i).SubItems(26) <> "SI" Then
                
                If Val(.ListItems(i).SubItems(1)) > 0 Then
                    If Val(.ListItems(i).SubItems(23)) = 0 Then .ListItems(i).SubItems(23) = 0
                    
                    precio_compra_neto = Lp_Neto
                    precio_compra_bruto = Val(Lp_Neto) * Val("1." & DG_IVA)
                    precio_venta_neto = CDbl(.ListItems(i).SubItems(25)) / (Val("1." & DG_IVA))
                    precio_venta_bruto = CDbl(.ListItems(i).SubItems(25))
                    Margen = precio_venta_neto - precio_compra_neto
                    Sql3 = "UPDATE maestro_productos SET porciento_utilidad=" & CxP(CDbl(.ListItems(i).SubItems(23))) & "," & _
                                                        "margen=" & CxP(Margen) & "," & _
                                                        "precio_venta=" & CDbl(.ListItems(i).SubItems(25)) & ", " & _
                                                        "precio_compra=" & CxP(Lp_Neto) & ", " & _
                                                        "pro_precio_venta_neto=" & CxP(precio_venta_neto) & ", pro_precio_costo_neto=" & CxP(precio_compra_neto) & ",pro_precio_venta_bruto=" & CDbl(.ListItems(i).SubItems(25)) & "," & _
                                                        "pro_precio_costo_bruto=" & CxP(precio_compra_bruto) & " " & _
                            "WHERE codigo='" & .ListItems(i).SubItems(1) & "'"
                    cn.Execute Sql3
                End If
            End If
   
            Sql = Mid(Sql, 1, Len(Sql) - 1)
            cn.Execute Sql
            
            
            
            'Si es activo inmobilizado, actualizar el precio del mismo
            '29-11-2014
            If .ListItems(i).SubItems(26) = "SI" Then
                cn.Execute "UPDATE con_activo_inmovilizado SET aim_valor=" & CDbl(Lp_Neto) + CDbl(Lp_Exento) & " " & _
                            "WHERE rut_emp='" & SP_Rut_Activo & "' AND aim_id=" & .ListItems(i).SubItems(1)
                
            End If
            
            If SP_Rut_Activo = "76.553.302-3" Then
                'KyR quita de la lista de azules
                '29 Agosto 2019
                 Sql3 = "UPDATE maestro_productos SET pro_codigo_original2=0 " & _
                            "WHERE codigo='" & .ListItems(i).SubItems(1) & "'"
                    cn.Execute Sql3
            
            End If
        Next
        If Sm_MueveInventario = "SI" Then
            'Ahora Kardex e inventario
            If Sm_Nota = "DEBITO" Then
                SalidaNotaDebito IIf(Sm_Nota = "DEBITO", "SALIDA", "ENTRADA")
            End If
            
            For i = 1 To .ListItems.Count
                If .ListItems(i).SubItems(26) <> "SI" Then
                        If Option1.Value Then
                            Lp_Neto = CxP(CDbl(.ListItems(i).SubItems(4)))
                            Lp_NetoTotal = CxP(CDbl(.ListItems(i).SubItems(7)))
                            Lp_Bruto = "0"
                            Lp_BrutoTotal = "0"
                        Else
                            Lp_Neto = CxP(.ListItems(i).SubItems(19))
                            Lp_NetoTotal = CDbl(.ListItems(i).SubItems(19)) * CDbl(.ListItems(i).SubItems(5))
                            Lp_Bruto = CxP(CDbl(.ListItems(i).SubItems(4)))
                            Lp_BrutoTotal = CxP(CDbl(.ListItems(i).SubItems(7)))
                        End If
                        If CDbl(.ListItems(i).SubItems(14)) > 0 And CDbl(.ListItems(i).SubItems(15)) > 0 Then
                            Lp_Neto = CxP(CDbl(.ListItems(i).SubItems(14)))
                            Lp_NetoTotal = CxP(CDbl(.ListItems(i).SubItems(15)))
                        
                        End If
                        If Sm_Nota = "DEBITO" Then
                            'mFinal = "NND"
                            Lp_NetoTotal = CxP(.ListItems(i).SubItems(7) + ValorOriginal("Neto", .ListItems(i).SubItems(1)))
                            Lp_Neto = CDbl(Lp_NetoTotal) / CDbl(.ListItems(i).SubItems(5))
                            If Option2.Value Then
                                Lp_BrutoTotal = CxP(CDbl(.ListItems(i).SubItems(19)) + ValorOriginal("Bruto", .ListItems(i).SubItems(1)))
                                Lp_Bruto = CDbl(Lp_BrutoTotal) / CDbl(.ListItems(i).SubItems(5))
                            End If
                        ElseIf Sm_Nota = "CREDITO" Then
                            mFinal = "NNC"
                            If compra_detalle.Sm_Modifica_Unidades = "SI" And compra_detalle.Sm_Modifica_Valores = "NO" Then
                                Sp_SoloUnidades = "SI"
                            End If
                            If compra_detalle.Sm_Modifica_Unidades = "NO" And compra_detalle.Sm_Modifica_Valores = "SI" Then
                                Sp_SoloValores = "SI"
                            End If
                        End If
                        act_precio_compra = "SI"
                        '27Jun2019
                        If SP_Rut_Activo = "76.978.874-3" Then
                            'dist aldunate
                            act_precio_compra = "NO"
                        End If
                        
                        
                        
                        Kardex Fql(DtFecha.Value), CboTipoDoc.Tag, CboTipoDoc.ItemData(CboTipoDoc.ListIndex), TxtNDoc, CboBodega.ItemData(CboBodega.ListIndex), _
                                 .ListItems(i).SubItems(1), CDbl(.ListItems(i).SubItems(5)), "COMPRA " & CboTipoDoc.Text & " Nro:" & TxtNDoc, Val(Lp_Neto), _
                               Val(Lp_NetoTotal), TxtRutProveedor, TxtRsocial, act_precio_compra, Val(Lp_Neto), CboTipoDoc.ItemData(CboTipoDoc.ListIndex), Sp_SoloUnidades, Sp_SoloValores, mFinal, 0
                End If
                    
            Next
        End If
    End With
    'faltaria actualizar el precio del ACTIVO INMOBILIZADO COMPRADO
    '29-11-2014
    '******************************************************************
    'Se completaron todos los procesos, Confirmamos la grabacion en todas
    'las tablas
    cn.CommitTrans
    CmdLlamaOC.Tag = Empty
    CmdLlamaOC.Caption = "Carga Orden de Compra"
    sg_codig2 = Empty
    SG_codigo = Empty
    Sql = "SELECT emp_comprobantes_pagos_contado comp " & _
         "FROM sis_empresas " & _
         "WHERE rut='" & SP_Rut_Activo & "'"
    Consulta RsTmp, Sql
    Sp_ImpCompPago = "SI"
    If RsTmp.RecordCount > 0 Then
        Sp_ImpCompPago = RsTmp!comp
    End If
    Sql = "SELECT doc_retiene_iva,doc_permite_pago,doc_movimiento,doc_nota_de_debito debito," & _
                    "doc_nota_de_credito credito,doc_mueve_inventario,doc_orden_de_compra,doc_solo_exento,doc_cod_sii, " & _
                    "(SELECT emp_factura_electronica FROM sis_empresas WHERE rut='" & SP_Rut_Activo & "') fe " & _
             "FROM sis_documentos " & _
             "WHERE doc_id=" & CboTipoDoc.ItemData(CboTipoDoc.ListIndex)
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        If RsTmp!doc_cod_sii = 46 And RsTmp!fe = "SI" Then
            FraProcesandoDTE.Visible = True
            DoEvents
            FacturaElectronicaC "46"
        Else
            '04 dic 2018 solo si es dte da acuse
            If RsTmp!fe = "SI" Then
                FrmLoad.Visible = False
                If MsgBox("Dar Acuse de Recibo Comercial y Mercaderia de este documento?", vbOKCancel + vbQuestion) = vbOK Then
                    AcuseRecibo
                End If
                FrmLoad.Visible = True
            End If
        End If
    End If
    FrmLoad.Visible = False
    If MsgBox("Grabacion correcta..." & vbNewLine & "� Ingresa nuevo documento ?", vbYesNo + vbQuestion) = vbNo Then
        If Bp_Comprobante_Pago And Sp_ImpCompPago = "SI" Then Previsualiza
        If IG_id_OrdenCompra = Me.CboTipoDoc.ItemData(CboTipoDoc.ListIndex) Then
            On Error GoTo ImpresoraError1
            Dialogo.CancelError = True
            Dialogo.ShowPrinter
            ' O R D E N    D E   C O M P R A
            FrmLoad.Visible = True
            DoEvents
            ImprimeOC
        End If
         FrmLoad.Visible = False
        
        Unload compra_detalle
        Unload Me
    Else
        'Limpiar datos
        If Bp_Comprobante_Pago And Sp_ImpCompPago = "SI" Then Previsualiza
        If IG_id_OrdenCompra = Me.CboTipoDoc.ItemData(CboTipoDoc.ListIndex) Then
            On Error GoTo ImpresoraError2
            Dialogo.CancelError = True
            Dialogo.ShowPrinter
            FrmLoad.Visible = True
            DoEvents
            ImprimeOC
        End If
        If Mid(CboPlazos.Text, 1, 7) <> "CONTADO" Then
            FrmLoad.Visible = False
            If MsgBox("Desea registrar el pago($) del documento ahora?...", vbOKCancel + vbQuestion) = vbOK Then
                        FrmLoad.Visible = True
                        DoEvents
                        Sql = "SELECT v.id,no_documento,doc_nombre " & _
                                ",v.rut,nombre_proveedor,'',fecha, v.doc_fecha_vencimiento, total,0 abonos," & _
                                               "total saldo " & _
                                      "FROM    com_doc_compra v,   maestro_proveedores c,  sis_documentos d " & _
                                      "WHERE v.id IN (" & Lp_Id & ") AND v.com_nc_utilizada='NO' AND v.rut_emp='" & SP_Rut_Activo & "' AND id_ref=0 AND  v.doc_id_factura = 0 AND v.Rut = c.rut_proveedor AND v.doc_id = d.doc_id  " & _
                                       "ORDER BY id"
                        Consulta RsTmp, Sql
                        If RsTmp.RecordCount > 0 Then
                               ctacte_GestionDePagos.Sm_Cli_Pro = "PRO"
                               ctacte_GestionDePagos.Sm_NombreAbono = TxtRsocial 'NOMBRE PROVEEDOR
                               ctacte_GestionDePagos.Sm_RutAbono = TxtRutProveedor
                              ' ctacte_GestionDePagos.Sm_RelacionMP = Right(CboPlazos.Text, 3)
                               ctacte_GestionDePagos.CmdSalir.Visible = False
                               ctacte_GestionDePagos.SkORIGEN = "COMPRA"
                               ctacte_GestionDePagos.txtSuperMensaje.Top = 6870
                               ctacte_GestionDePagos.CmdSalir.Visible = True
                               FrmLoad.Visible = False
                               ctacte_GestionDePagos.Show 1
                               FrmLoad.Visible = True
                               DoEvents
                        End If
            End If
        End If
        FrmLoad.Visible = False
        Unload compra_detalle
        LimpiaDatos
    End If
     FrmLoad.Visible = False
    Exit Sub
ErrorGrabacion:
    cn.RollbackTrans
    FrmLoad.Visible = False
    MsgBox "Hubo un error al intentar grabar..." & vbNewLine & "Nro Error: " & Err.Number & vbNewLine & Err.Description
    Exit Sub
ImpresoraError1:
    FrmLoad.Visible = False
    Unload compra_detalle
    
    Unload Me
    Exit Sub
ImpresoraError2:
    FrmLoad.Visible = False
    Unload compra_detalle
    LimpiaDatos
End Sub

Private Sub Previsualiza()
   Dim Cx As Double, Cy As Double, Sp_Fecha As String, Ip_Mes As Integer, Dp_S As Double, Sp_Letras As String
    Dim Sp_Neto As String * 12, Sp_IVA As String * 12
    On Error GoTo ePrev
    Dim Sp_Nro_Comprobante As String
    Dim Sp_Empresa As String
    Dim Sp_Giro As String
    Dim Sp_Direccion As String
    Dim Sp_Nombre As String * 45
    Dim Sp_Rut As String * 15
    Dim Sp_Concepto As String
    Dim Sp_Nro_Doc As String * 12
    Dim Sp_Documento As String * 35
    Dim Sp_Valor As String * 15
    Dim Sp_Saldo As String * 15
    Dim Sp_Fpago As String * 24
    Dim Sp_Total As String * 12
    Dim Sp_Banco As String * 15
    Dim Sp_NroCheque As String * 10
    Dim Sp_FechaCheque As String * 10
    Dim Sp_ValorCheque As String * 15
    Dim Sp_SumaCheque As String * 15
    Dim Sp_Observacion As String * 80
    Sql = "SELECT giro,direccion,ciudad " & _
         "FROM sis_empresas " & _
         "WHERE rut='" & SP_Rut_Activo & "'"
    Consulta RsTmp2, Sql
    If RsTmp2.RecordCount > 0 Then
        Sp_Giro = RsTmp2!giro
        Sp_Direccion = RsTmp2!direccion & " - " & RsTmp2!ciudad
    End If
    Sis_Previsualizar.Pic.ScaleMode = vbCentimeters
    Sis_Previsualizar.Pic.BackColor = vbWhite
    Sis_Previsualizar.Pic.AutoRedraw = True
    Sis_Previsualizar.Pic.DrawWidth = 1
    Sis_Previsualizar.Pic.DrawMode = 1
    Sis_Previsualizar.Pic.FontName = "Courier New"
    Sis_Previsualizar.Pic.FontSize = 10
   
    Cx = 2
    Cy = 1.9
    Dp_S = 0.17
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.CurrentY = Cy
    Sis_Previsualizar.Pic.FontSize = 16  'tama�o de letra
    Sis_Previsualizar.Pic.FontBold = True
    
    '**
    'Sis_Previsualizar.Pic.PaintPicture LoadPicture(App.Path & "\IMG\LAFLOR\LAFLOR.jpg"), Cx + 0.62, 1
    Sis_Previsualizar.Pic.PaintPicture LoadPicture(App.Path & "\REDMAROK.jpg"), Cx + 0.62, 1
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S + Dp_S
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S + Dp_S
    '**
    
    Sis_Previsualizar.Pic.Print SP_Empresa_Activa
        
    
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY
  
    Sis_Previsualizar.Pic.FontSize = 10
  
    pos = Sis_Previsualizar.Pic.CurrentY
    Sis_Previsualizar.Pic.Print "R.U.T.   :" & SP_Rut_Activo
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY
    

    Sis_Previsualizar.Pic.CurrentY = pos
    Sis_Previsualizar.Pic.CurrentX = Cx + 11
    'If Sm_Cli_Pro = "CLI" Then
    '     Sis_Previsualizar.Pic.Print "COMPROBANTE DE INGRESO N�: " & lp_IDAbo
    'Else
         Sis_Previsualizar.Pic.Print "COMPROBANTE DE EGRESO N�: " & Lp_Nro_Comprobante
    'End If
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY
 
 
    Sis_Previsualizar.Pic.FontBold = False
    Sis_Previsualizar.Pic.FontSize = 10
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.Print "GIRO     :" & Sp_Giro
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY
    
  
    Sis_Previsualizar.Pic.FontSize = 10
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.Print "DIRECCION:"; Sp_Direccion
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
    
    Sis_Previsualizar.Pic.CurrentX = Cx
    'If Sm_Cli_Pro = "CLI" Then
    '     Sis_Previsualizar.Pic.Print "Recibi de:"
    'Else
         Sis_Previsualizar.Pic.Print "Pagado a :"
    'End If
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
    Sis_Previsualizar.Pic.FontSize = 10
    pos = Sis_Previsualizar.Pic.CurrentY
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.Print "Nombre:" & TxtRsocial
    'Sis_Previsualizar.Pic.Print "Nombre:" & Sm_NombreAbono
    
    Sis_Previsualizar.Pic.CurrentY = pos
    Sis_Previsualizar.Pic.CurrentX = Cx + 14
    Sis_Previsualizar.Pic.Print "Fecha:" & Me.DtFecha.Value
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
    
    
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.Print "RUT   :" & Me.TxtRutProveedor
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
    
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.FontBold = True
    Sis_Previsualizar.Pic.Print "CONCEPTO :"
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
    
       
    'Aqui agregamos los documentos a los que se abonara
    Sis_Previsualizar.Pic.FontBold = False
    Sp_Nro_Doc = "Nro"
    Sp_Documento = "Documento"
    RSet Sp_Valor = "Valor"
    RSet Sp_Saldo = "Saldo"
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.Print Sp_Nro_Doc & " " & Sp_Documento & " " & Sp_Valor & " " & Sp_Saldo
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S - 0.1
   ' For i = 1 To LVDetalle.ListItems.Count
        Sp_Nro_Doc = Me.TxtNDoc
        Sp_Documento = Me.CboTipoDoc.Text
        RSet Sp_Valor = Me.txtTotal
        RSet Sp_Saldo = 0
        Sis_Previsualizar.Pic.CurrentX = Cx
        Sis_Previsualizar.Pic.Print Sp_Nro_Doc & " " & Sp_Documento & " " & Sp_Valor & " " & Sp_Saldo
        Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S - 0.2
   ' Next
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + 0.2
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.FontBold = True
    Sp_Fpago = CboFpago.Text
    pos = Sis_Previsualizar.Pic.CurrentY
    Sis_Previsualizar.Pic.Print "FORMA DE PAGO:" & CboFpago.Text
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.FontBold = True
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
    Sis_Previsualizar.Pic.Print "TOTAL PAGO   :$" & txtTotal
    
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S + Dp_S
    
    
    Sis_Previsualizar.Pic.FontBold = False
    
    'If CboMPago.ItemData(CboMPago.ListIndex) = 3 Or CboMPago.ItemData(CboMPago.ListIndex) = 4 Then
    '    Sis_Previsualizar.Pic.CurrentX = cx
    '    Sis_Previsualizar.Pic.Print "NRO OPERACION:" & txtNroOperacion
    '    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
    'End If
    finpago = Sis_Previsualizar.Pic.CurrentY
    
    'Aqui detallaremos los cheques
    
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.FontBold = True
    posobs = Sis_Previsualizar.Pic.CurrentY
    Sis_Previsualizar.Pic.Print "OBSERVACIONES:"
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
    Sis_Previsualizar.Pic.FontBold = False
     
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.Print "COMPRA CONTADO"
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
    pos = Sis_Previsualizar.Pic.CurrentY
    Sis_Previsualizar.Pic.CurrentY = pos + 1
    Sis_Previsualizar.Pic.CurrentX = Cx
    
    Sis_Previsualizar.Pic.Print "CONTABILIDAD"
        
    Sis_Previsualizar.Pic.CurrentY = pos + 1
    Sis_Previsualizar.Pic.CurrentX = Cx + 6
    Sis_Previsualizar.Pic.Print "V� B� CAJA"
   
   
   
    Sis_Previsualizar.Pic.CurrentY = pos + 1
    Sis_Previsualizar.Pic.CurrentX = Cx + 12
    Sis_Previsualizar.Pic.Print "NOMBRE, RUT Y FIRMA"
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
    
    
    
     Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY - (Dp_S * 6)
     Sis_Previsualizar.Pic.CurrentX = Cx + 12
     Sis_Previsualizar.Pic.Print "RECIBI CONFORME"
     Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
    
    Sis_Previsualizar.Pic.Line (1.5, 1)-(20, 4.5), , B 'Rectangulo Encabezado y N� Comprobante
    
    Sis_Previsualizar.Pic.Line (1.5, 4.5)-(20, 6.2), , B 'Pagado o Recibido del cliente o proveedor
    
   ' If LVCheques.ListItems.Count > 0 Then
   '    Sis_Previsualizar.Pic.Line (1.5, 6.2)-(20, finpago), , B  'Concepto - documentos pagados o recibidos y forma de pago
   ' Else
    Sis_Previsualizar.Pic.Line (1.5, 6.2)-(20, finpago + 0.15), , B 'Concepto - documentos pagados o recibidos y forma de pago
    'End If
    
    Sis_Previsualizar.Pic.Line (13.5, 6.2)-(16.8, posobs), , B  'columnas de los pagos en efectivo
    Sis_Previsualizar.Pic.Line (13.5, 6.2)-(16.8, 9.37), , B  'columnas de los pagos en efectivo
    
  
    
    Sis_Previsualizar.Pic.Line (1.5, posobs)-(20, posobs + 1.5), , B 'Observaciones
    Sis_Previsualizar.Pic.Line (1.5, posobs + 1.5)-(20, posobs + 1.5 + 2), , B 'Pie del comprobante
        
    pos = Sis_Previsualizar.Pic.CurrentY
    
 '   Sis_Previsualizar.Pic.Height = pos * 0.04

    Sis_Previsualizar.Show 1
    
            If SG_codigo = "print" Then
                On Error GoTo noImprime
                Dialogo.CancelError = True
                Dialogo.ShowPrinter
                ImprimeComprobante
            End If

Exit Sub
ePrev:
MsgBox "Error " & Err.Description & vbNewLine & "Nro " & Err.Number
Err.Clear

noImprime:



End Sub

Private Sub ImprimeComprobante()
    Dim Cx As Double, Cy As Double, Sp_Fecha As String, Ip_Mes As Integer, Dp_S As Double, Sp_Letras As String
    Dim Sp_Neto As String * 12, Sp_IVA As String * 12
    
    Dim Sp_Nro_Comprobante As String
    
    Dim Sp_Empresa As String
    Dim Sp_Giro As String
    Dim Sp_Direccion As String
    
    
    Dim Sp_Nombre As String * 45
    Dim Sp_Rut As String * 15
    Dim Sp_Concepto As String
    
    Dim Sp_Nro_Doc As String * 12
    Dim Sp_Documento As String * 35
    Dim Sp_Valor As String * 15
    Dim Sp_Saldo As String * 15
    Dim Sp_Fpago As String * 24
    Dim Sp_Total As String * 12
    
    Dim Sp_Banco As String * 20
    Dim Sp_NroCheque As String * 10
    Dim Sp_FechaCheque As String * 10
    Dim Sp_ValorCheque As String * 15
    Dim Sp_SumaCheque As String * 15
    
    Dim Sp_Observacion As String * 80
    
    For i = 1 To Dialogo.Copies
        Sql = "SELECT giro,direccion,ciudad " & _
             "FROM sis_empresas " & _
             "WHERE rut='" & SP_Rut_Activo & "'"
        Consulta RsTmp2, Sql
        If RsTmp2.RecordCount > 0 Then
            Sp_Giro = RsTmp2!giro
            Sp_Direccion = RsTmp2!direccion & " - " & RsTmp2!ciudad
        End If
        
        
        
        Printer.FontName = "Courier New"
        Printer.FontSize = 10
        Printer.ScaleMode = 7
        Cx = 2
        Cy = 1.9
        Dp_S = 0.17
        Printer.CurrentX = Cx
        Printer.CurrentY = Cy
        Printer.FontSize = 16  'tama�o de letra
        Printer.FontBold = True
        
        '**
        'Printer.PaintPicture LoadPicture(App.Path & "\IMG\LAFLOR\LAFLOR.jpg"), Cx + 0.62, 1
        Printer.PaintPicture LoadPicture(App.Path & "\REDMAROK.jpg"), Cx + 0.62, 1
        Printer.CurrentY = Printer.CurrentY + Dp_S + Dp_S
        Printer.CurrentY = Printer.CurrentY + Dp_S + Dp_S
        '**
        
        Printer.Print SP_Empresa_Activa
            
        
        Printer.CurrentX = Cx
        Printer.CurrentY = Printer.CurrentY
      
        Printer.FontSize = 10
      
        pos = Printer.CurrentY
        Printer.Print "R.U.T.   :" & SP_Rut_Activo
        Printer.CurrentY = Printer.CurrentY
        
    
        Printer.CurrentY = pos
        Printer.CurrentX = Cx + 11
        'If Sm_Cli_Pro = "CLI" Then
        '     Printer.Print "COMPROBANTE DE INGRESO N�: " & lp_IDAbo
        'Else
             Printer.Print "COMPROBANTE DE EGRESO N�: " & Lp_Nro_Comprobante
        'End If
        Printer.CurrentY = Printer.CurrentY
     
     
        Printer.FontBold = False
        Printer.FontSize = 10
        Printer.CurrentX = Cx
        Printer.Print "GIRO     :" & Sp_Giro
        Printer.CurrentY = Printer.CurrentY
        
        Printer.FontSize = 10
        Printer.CurrentX = Cx
        Printer.Print "DIRECCION:"; Sp_Direccion
        Printer.CurrentY = Printer.CurrentY + Dp_S
        
        Printer.CurrentX = Cx
        Printer.FontBold = True
        If Sm_Cli_Pro = "CLI" Then
             Printer.Print "Recibi de"
        Else
             Printer.Print "Pagado  a "
        End If
        Printer.FontBold = False
        Printer.CurrentY = Printer.CurrentY + Dp_S
        Printer.FontSize = 10
        pos = Printer.CurrentY
        Printer.CurrentX = Cx
        Printer.Print "Nombre:" & Me.TxtRsocial
        
        Printer.CurrentY = pos
        Printer.CurrentX = Cx + 14
        Printer.Print "Fecha:" & Me.DtFecha
        Printer.CurrentY = Printer.CurrentY + Dp_S
        
        
        Printer.CurrentX = Cx
        Printer.Print "RUT   :" & Me.TxtRutProveedor
        Printer.CurrentY = Printer.CurrentY + Dp_S
        
        Printer.CurrentX = Cx
        Printer.FontBold = True
        Printer.Print "CONCEPTO :"
        Printer.CurrentY = Printer.CurrentY + Dp_S
        
           
        'Aqui agregamos los documentos a los que se abonara
        Printer.FontBold = False
        Sp_Nro_Doc = "Nro"
        Sp_Documento = "Documento"
        RSet Sp_Valor = "Valor"
        RSet Sp_Saldo = "Saldo"
        Printer.CurrentX = Cx
        Printer.Print Sp_Nro_Doc & " " & Sp_Documento & " " & Sp_Valor & " " & Sp_Saldo
        Printer.CurrentY = Printer.CurrentY + Dp_S - 0.1
        'For i = 1 To LVDetalle.ListItems.Count
            Sp_Nro_Doc = TxtNDoc
            Sp_Documento = CboTipoDoc.Text
            RSet Sp_Valor = txtTotal
            RSet Sp_Saldo = 0
            Printer.CurrentX = Cx
            Printer.Print Sp_Nro_Doc & " " & Sp_Documento & " " & Sp_Valor & " " & Sp_Saldo
            Printer.CurrentY = Printer.CurrentY + Dp_S - 0.2
        'Next
        Printer.CurrentY = Printer.CurrentY + 0.2
        Printer.CurrentX = Cx
        Printer.FontBold = True
        Sp_Fpago = CboFpago.Text
        pos = Printer.CurrentY
        Printer.Print "FORMA DE PAGO:" & Sp_Fpago
        Printer.CurrentX = Cx
        Printer.FontBold = True
        Printer.CurrentY = Printer.CurrentY + Dp_S
        Printer.Print "TOTAL PAGO   :$" & txtTotal
        
        Printer.CurrentY = Printer.CurrentY + Dp_S + Dp_S
        
        
        Printer.FontBold = False
        
        
        finpago = Printer.CurrentY
        
        
        Printer.CurrentY = Printer.CurrentY + Dp_S
        Printer.CurrentX = Cx
        Printer.FontBold = True
        posobs = Printer.CurrentY
        Printer.Print "OBSERVACIONES:"
        Printer.CurrentY = Printer.CurrentY + Dp_S
        Printer.FontBold = False
         
        Printer.CurrentX = Cx
        Printer.Print "COMPRA CONTADO"
        Printer.CurrentY = Printer.CurrentY + Dp_S
        pos = Printer.CurrentY
        Printer.CurrentY = pos + 1
        Printer.CurrentX = Cx
        
        Printer.Print "CONTABILIDAD"
            
        Printer.CurrentY = pos + 1
        Printer.CurrentX = Cx + 6
        Printer.Print "V� B� CAJA"
       
        Printer.CurrentY = pos + 1
        Printer.CurrentX = Cx + 12
        Printer.Print "NOMBRE, RUT Y FIRMA"
        Printer.CurrentY = Printer.CurrentY + Dp_S
        
        
         Printer.CurrentY = Printer.CurrentY - (Dp_S * 6)
        Printer.CurrentX = Cx + 12
        Printer.Print "RECIBI CONFORME"
        Printer.CurrentY = Printer.CurrentY + Dp_S
        
       ' Printer.DrawMode = 1
       ' Printer.DrawWidth = 3
        
        Printer.Line (1.5, 1)-(20, 4.5), , B 'Rectangulo Encabezado y N� Comprobante
        
        Printer.Line (1.5, 4.5)-(20, 6.2), , B 'Pagado o Recibido del cliente o proveedor
        
        'If LVCheques.ListItems.Count > 0 Then
        '    Printer.Line (1.5, 6.2)-(20, finpago), , B  'Concepto - documentos pagados o recibidos y forma de pago
        'Else
        Printer.Line (1.5, 6.2)-(20, finpago + 0.15), , B 'Concepto - documentos pagados o recibidos y forma de pago
        'End If
        
        Printer.Line (13.5, 6.2)-(16.8, posobs), , B  'columnas de los pagos en efectivo
        'Printer.Line (13.5, 6.2)-(16.8, 9.37), , B  'columnas de los pagos en efectivo
        
        
        
        Printer.Line (1.5, posobs)-(20, posobs + 1.5), , B 'Observaciones
        Printer.Line (1.5, posobs + 1.5)-(20, posobs + 1.5 + 2), , B 'Pie del comprobante
        
        Printer.NewPage
        Printer.EndDoc
    Next
End Sub






Private Function ValorOriginal(Tipo As String, Cod As String) As Double
    Dim Rp_Vo As Recordset
    Select Case Tipo
        Case "Neto"
            Sql = "SELECT cmd_total_neto xprecio "
        Case "Bruto"
            Sql = "SELECT cmd_total_bruto xprecio "
    End Select
    Sql = Sql & "FROM com_doc_compra c " & _
                "INNER JOIN com_doc_compra_detalle d USING(id) " & _
                "WHERE c.id=" & TxtDocumentoRef.Tag & " AND pro_codigo='" & Cod & "'"
    Consulta Rp_Vo, Sql
    If Rp_Vo.RecordCount > 0 Then
        ValorOriginal = CxP(Str(Rp_Vo!xprecio))
    Else
        ValorOriginal = 0
    End If
End Function
Private Sub SalidaNotaDebito(En_Sa As String)
    Dim Sp_Codigos As String, Rp_ND As Recordset
    'Items detalle
    
    Sp_Codigos = Empty
    With compra_detalle.LvDetalle
        For i = 1 To .ListItems.Count
            Sp_Codigos = Sp_Codigos & .ListItems(i).SubItems(1) & ","
        Next
        If Len(Sp_Codigos) > 0 Then Sp_Codigos = Mid(Sp_Codigos, 1, Len(Sp_Codigos) - 1)
        Sql = "SELECT kar_id,pro_codigo,kar_cantidad,pro_precio_neto " & _
                "FROM inv_kardex " & _
                "WHERE pro_codigo IN(" & Sp_Codigos & ") AND doc_id=" & FrameRef.Tag & " AND kar_numero=" & TxtNroRef
        Consulta Rp_ND, Sql
        If Rp_ND.RecordCount > 0 Then
            Rp_ND.MoveFirst
            Do While Not Rp_ND.EOF
                Kardex Fql(DtFecha.Value), En_Sa, CboTipoDoc.ItemData(CboTipoDoc.ListIndex), TxtNDoc, CboBodega.ItemData(CboBodega.ListIndex), _
                Rp_ND!pro_codigo, CxP(Rp_ND!kar_cantidad), "ACTUALIZA POR " & CboTipoDoc.Text & " Nro:" & TxtNDoc, Rp_ND!pro_precio_neto, _
                CxP(Rp_ND!pro_precio_neto * Rp_ND!kar_cantidad), TxtRutProveedor, TxtRsocial, "SI", Rp_ND!pro_precio_neto, CboTipoDoc.ItemData(CboTipoDoc.ListIndex), "SI", "NO", "0", "0"
    
            
            
                Rp_ND.MoveNext
            Loop
        
        
        End If
    End With
End Sub


Private Sub LimpiaDatos()
    TxtRutProveedor = ""
    TxtRsocial = ""
    Me.TxtNDoc = ""
    TxtExento = "0"
    TxtNeto = "0"
    txtIVA = "0"
    txtIvaRetenido = 0
    txtSubNeto = 0
    txtTotalNeto = 0
    txtTotal = 0
    TxtSubTotal = 0
    For i = 1 To LvImpuestos.ListItems.Count
        LvImpuestos.ListItems(i).SubItems(2) = 0
    Next
    
    
End Sub

Private Sub CmdNuevoProveedor_Click()
    SG_codigo = Empty
    AgregoProveedor.Show 1
    If SG_codigo <> Empty Then TxtRutProveedor = SG_codigo
End Sub

Private Sub CmdOkImp_Click()
    CalculaAdicionales
End Sub
Private Sub CalculaAdicionales()
    If Val(TxtImpuesto.Tag) = 0 Then Exit Sub
    LvImpuestos.Tag = 0
    SkImpCosto = 0
    SkImpCredito = 0
    SkTotalAdicionales = 0
    
    For i = 1 To LvImpuestos.ListItems.Count
        If LvImpuestos.ListItems(i) = Val(TxtImpuesto.Tag) Then
            LvImpuestos.ListItems(i).SubItems(2) = Format(CDbl(TxtValorImp), "#,0")
            'Exit For
        End If
        LvImpuestos.Tag = LvImpuestos.Tag + CDbl(LvImpuestos.ListItems(i).SubItems(2))
        If LvImpuestos.ListItems(i).SubItems(4) = "COSTO" Then
            SkImpCosto = SkImpCosto + CDbl(LvImpuestos.ListItems(i).SubItems(2))
        End If
        SkTotalAdicionales = SkTotalAdicionales + CDbl(LvImpuestos.ListItems(i).SubItems(2))
        
    Next
    TxtImpuesto.Tag = 0
    TxtImpuesto = ""
    TxtValorImp = "0"
    CTotal
    Factores
    LvImpuestos.SetFocus

End Sub


Private Sub Factores()
    'Calcular Factor del impuesto
    If CDbl(TxtNeto) > 0 Then
        For i = 1 To LvImpuestos.ListItems.Count
            LvImpuestos.ListItems(i).SubItems(3) = Format(CDbl(LvImpuestos.ListItems(i).SubItems(2)) / CDbl(TxtNeto) * 100, "#,0.#0")
        Next
    End If

End Sub

Private Sub CmdPeriodoContable_Click()
        sis_cambia_periodo_contable.Show 1
        TxtMesContable = Principal.CboMes
        TxtAnoContable = Principal.CboAno
End Sub

Private Sub CmdSalir_Click()
    On Error Resume Next
    Unload Me
End Sub







Private Sub Command1_Click()
MsgBox Right(CboPlazos.Text, 5)
End Sub

Private Sub Command2_Click()
       Lp_SaldoRef = SaldoDocumento(Me.CmdLlamaOC.Tag, "PRO")
        MsgBox Lp_SaldoRef
End Sub

Private Sub DtFecha_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then CboPlazos.SetFocus
End Sub


Private Sub DtFecha_LostFocus()
    If Val(Year(DtFecha)) < IG_Ano_Contable Then
       
    Else
        If (Month(DtFecha) + Year(DtFecha)) > (IG_Ano_Contable + IG_Mes_Contable) Then
            MsgBox "No puede ingresar documentos con FECHA posterior al periodo contable...", vbInformation
            sis_cambia_periodo_contable.Show 1
            TxtMesContable = Principal.CboMes
            TxtAnoContable = Principal.CboAno
            DtFecha.SetFocus
            Exit Sub
        End If
    End If
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
            On Error Resume Next
            SendKeys "{tab}"
            KeyAscii = 0
    End If
End Sub

Private Sub Form_Load()
    Centrar Me, True
    If SP_Rut_Activo = "76.063.757-2" Then
            'diconor recargo por flete
            Me.FrmDiconor.Visible = True
    End If
    'TITULO EMPRESA ACTIVA
    Lp_TopFrmProveedor = Me.FrmProveedor.Top
    SkRut = SP_Rut_Activo
    SkEmpresa = SP_Empresa_Activa
    'FIN TITULO
    CboCaja.ListIndex = 1
    If SG_Modulo_Caja = "SI" Then
        Me.FrmCaja.Visible = True
    Else
        Me.FrmCaja.Visible = False
    End If
    If SP_Rut_Activo = "76.169.962-8" Then
        'Alcade si afecta caja
        CboCaja.ListIndex = 0
    End If
    
    
    Lm_Top1 = Frame3.Top
    Lm_Top2 = Me.FrmProveedor.Top
    Bm_Permite_Documento = False
    Skin2 Me, , 5
    Me.Height = 10260
    
    DtFecha = Date
    Sm_Nota = Empty
    LLenarCombo CboBodega, "bod_nombre", "bod_id", "par_bodegas", "bod_activo='SI' AND rut_emp='" & SP_Rut_Activo & "'", "bod_id"
    CboBodega.ListIndex = 0
    If SP_Control_Inventario = "SI" Then LLenarCombo CboTipoDoc, "doc_nombre", "doc_id", "sis_documentos", "doc_libro_gastos='NO' AND doc_documento='COMPRA' AND doc_activo='SI' AND doc_honorarios='NO'", "doc_orden"
    If SP_Control_Inventario = "NO" Then LLenarCombo CboTipoDoc, "doc_nombre", "doc_id", "sis_documentos", "doc_libro_gastos='NO' AND doc_documento='COMPRA' AND doc_activo='SI' AND doc_honorarios='NO'", "doc_orden"
    
    LLenarCombo CboPlazos, "CONCAT(pla_nombre,'                                                      ',CAST(pla_id AS CHAR ))", "pla_dias", "par_plazos_vencimiento", "pla_activo='SI'", "pla_id"
    CboPlazos.AddItem "FONDOS POR RENDIR"
    CboPlazos.ItemData(CboPlazos.ListCount - 1) = 9999
    CboPlazos.AddItem "ANTICIPO DE PROVEEDOR"
    CboPlazos.ItemData(CboPlazos.ListCount - 1) = 4444
    
    
    
    CboPlazos.ListIndex = 0
    TxtMesContable = UCase(MonthName(IG_Mes_Contable))
    TxtAnoContable = IG_Ano_Contable
    
    Sql = "SELECT e.imp_id,CONCAT(imp_nombre,'    (',LCASE(ime_costo_credito),')') ,0 valor,0 factor,ime_costo_credito  " & _
          "FROM par_impuestos_empresas e " & _
          "INNER JOIN par_impuestos i USING(imp_id) " & _
          "WHERE e.rut='" & SP_Rut_Activo & "'"
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvImpuestos, False, True, True, False
    CboFpago.ListIndex = -1
    Sm_Nota = Empty
End Sub
Private Sub CTotal()
    Dim lp_Impuestos As Long
    lp_Impuestos = 0
    For i = 1 To LvImpuestos.ListItems.Count
        lp_Impuestos = lp_Impuestos + CDbl(LvImpuestos.ListItems(i).SubItems(2))
    Next
    TxtSubTotal = NumFormat(CDbl(TxtExento) + CDbl(TxtNeto) + CDbl(txtIVA) + CDbl(txtIvaSinCredito) - CDbl(txtIvaRetenido))
    txtTotal = Format(CDbl(TxtExento) + CDbl(TxtNeto) + CDbl(txtIVA) + CDbl(txtIvaSinCredito) - CDbl(Me.txtIvaRetenido) + lp_Impuestos, "#,0")
    If CDbl(txtIVA) > 0 Then
        SkTasaIVA = DG_IVA
    Else
        SkTasaIVA = 0
    End If
End Sub





Private Sub Form_Unload(Cancel As Integer)
    SG_codigo = ""
    SG_codigo2 = ""
'    Unload compra_detalle
End Sub



Private Sub LvImpuestos_DblClick()
    If LvImpuestos.SelectedItem Is Nothing Then Exit Sub
    TxtImpuesto.Tag = LvImpuestos.SelectedItem
    TxtImpuesto = LvImpuestos.SelectedItem.SubItems(1)
    TxtValorImp = LvImpuestos.SelectedItem.SubItems(2)
    TxtValorImp.SetFocus
End Sub

Private Sub LvImpuestos_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 13 Then
        If LvImpuestos.SelectedItem Is Nothing Then Exit Sub
        TxtImpuesto.Tag = LvImpuestos.SelectedItem
        TxtImpuesto = LvImpuestos.SelectedItem.SubItems(1)
        TxtValorImp = LvImpuestos.SelectedItem.SubItems(2)
        TxtValorImp.SetFocus
    End If
End Sub




Private Sub Timer1_Timer()
    On Error Resume Next
    DtFecha.SetFocus
    Timer1.Enabled = False
End Sub

Private Sub TxtAutorizadoPor_GotFocus()
    En_Foco Me.ActiveControl
End Sub

Private Sub TxtAutorizadoPor_KeyPress(KeyAscii As Integer)
KeyAscii = Asc(UCase(Chr(KeyAscii)))
 If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtDescuentoNeto_GotFocus()
    En_Foco Me.ActiveControl
End Sub

Private Sub TxtDescuentoNeto_KeyPress(KeyAscii As Integer)
    KeyAscii = AceptaSoloNumeros(KeyAscii)

End Sub

Private Sub TxtDescuentoNeto_Validate(Cancel As Boolean)
    If SinDesborde(Me.ActiveControl) Then
        Cancel = False
    Else
        Cancel = True
        TxtDescuentoNeto = 0 'ESTE SE REEMPLAZA POR EL CONTROL ACTIVO
    End If
    
    ValidarControl
    
    TxtNeto = Format(CDbl(txtSubNeto) - CDbl(TxtDescuentoNeto), "#,0")
    CalculaIva
End Sub

Private Sub TxtExento_GotFocus()
    En_Foco Me.ActiveControl
End Sub

Private Sub TxtExento_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
End Sub

Private Sub TxtExento_Validate(Cancel As Boolean)
    'ESTO ES PARA EVITAR ERROR POR DESBORDAMIENTO
    'EVITANDO LA CAIDA DEL SISTEMA
    If SinDesborde(Me.ActiveControl) Then
        Cancel = False
    Else
        Cancel = True
        TxtExento = 0 'ESTE SE REEMPLAZA POR EL CONTROL ACTIVO
    End If
    'REEMPLAZAR EL NOMBRE DEL CONTROL EJ: TXTSDIVA  , TXTNETO,
    '----------------------------------------


    ValidarControl
End Sub

Private Sub TxtIva_GotFocus()
    En_Foco txtIVA
End Sub

Private Sub txtIVA_KeyPress(KeyAscii As Integer)
    KeyAscii = AceptaSoloNumeros(KeyAscii)
End Sub

Private Sub txtIVA_Validate(Cancel As Boolean)
    ValidarControl
End Sub

Private Sub txtIvaRetenido_GotFocus()
    En_Foco Me.ActiveControl
End Sub

Private Sub TxtIVARetenido_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
End Sub

Private Sub txtIvaRetenido_Validate(Cancel As Boolean)
    ValidarControl
    
End Sub







Private Sub txtIvaSinCredito_GotFocus()
    En_Foco txtIvaSinCredito
End Sub

Private Sub txtIvaSinCredito_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
End Sub

Private Sub txtIvaSinCredito_Validate(Cancel As Boolean)
    ValidarControl
End Sub

Private Sub TxtNDoc_GotFocus()
    On Error Resume Next
    En_Foco Me.ActiveControl
End Sub

Private Sub TxtNDoc_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
     If KeyAscii = 39 Then KeyAscii = 0
End Sub
Private Sub TxtNDoc_Validate(Cancel As Boolean)
   ' ComprobarSiDocumento DG_ID_Unico
   VerificarUsado
   
   'If Not Bm_Permite_Documento Then TxtNDoc = ""
End Sub
Private Sub VerificarUsado()
    Bm_Permite_Documento = False
    If Val(TxtNDoc) > 0 And Sm_EsOC = "SI" Then
        Sql = "SELECT id " & _
                "FROM com_doc_compra " & _
                "WHERE doc_id=" & ObtID(CboTipoDoc) & " AND  no_documento=" & TxtNDoc & " AND rut_emp='" & SP_Rut_Activo & "'"
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
            MsgBox "Orden  de Compra Nro " & TxtNDoc & " ya ha sido utilizado...", vbInformation
            TxtNDoc = ""
            Exit Sub
        End If
    End If
    If CboTipoDoc.ListIndex > -1 And Val(TxtNDoc) > 0 And Len(TxtRutProveedor) > 0 Then
        Respuesta = VerificaRut(TxtRutProveedor, NuevoRut)
        TxtRutProveedor = NuevoRut
    
        Sql = "SELECT id,doc_orden_de_compra " & _
              "FROM com_doc_compra " & _
              "INNER JOIN sis_documentos d USING(doc_id) " & _
              "WHERE doc_id=" & ObtID(CboTipoDoc) & " AND no_documento=" & TxtNDoc & " AND rut='" & TxtRutProveedor & "' "
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
            If RsTmp!doc_orden_de_compra = "NO" Then
                TxtNDoc = ""
                MsgBox " Este documento ya ha sido utilizado...", vbInformation
                TxtNDoc.SetFocus
            Else
                Sql = "SELECT id " & _
                        "FROM com_doc_compra " & _
                        "INNER JOIN sis_documentos d USING(doc_id) " & _
                        "WHERE doc_id=" & ObtID(CboTipoDoc) & " AND no_documento=" & TxtNDoc & " AND rut_emp='" & SP_Rut_Activo & "' "
                Consulta RsTmp, Sql
                If RsTmp.RecordCount > 0 Then
                    MsgBox " Nro de OC ya ha sido utilizado...", vbInformation
                    TxtNDoc = ""
                    TxtNDoc.SetFocus
                Else
                     Bm_Permite_Documento = True
                End If
            End If
        Else
            Bm_Permite_Documento = True
        End If
    End If
End Sub

Private Sub TxtNeto_GotFocus()
    En_Foco Me.ActiveControl
End Sub

Private Sub TxtNeto_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
End Sub
Private Sub CalculaIva()
    txtIVA = Format(CDbl(TxtNeto) / 100 * DG_IVA, "#,0")
    CTotal
    Factores
End Sub




Private Sub TxtRetiradoPor_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
     If KeyAscii = 39 Then KeyAscii = 0
End Sub


Private Sub TxtRutProveedor_GotFocus()
    On Error Resume Next
    En_Foco Me.ActiveControl
End Sub

Private Sub TxtRutProveedor_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
    If KeyAscii = 13 Then TxtRutProveedor_Validate True
     If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtRutProveedor_Validate(Cancel As Boolean)
If Len(TxtRutProveedor) > 0 Then
        VerificarUsado
        If Not Bm_Permite_Documento Then Cancel = False
        If Bm_Permite_Documento = False Then
            TxtNDoc.SetFocus
            Exit Sub
        End If
        

        
        Sql = "SELECT nombre_empresa,rut_emp " & _
              "FROM maestro_proveedores " & _
              "WHERE rut_proveedor='" & TxtRutProveedor & "' AND habilitado='SI' "
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
            Sql = "SELECT rut_emp " & _
                  "FROM par_asociacion_ruts " & _
                  "WHERE rut_emp='" & SP_Rut_Activo & "' AND rut_pro='" & TxtRutProveedor & "'"
            Consulta RsTmp2, Sql
            If RsTmp2.RecordCount = 0 Then
                Sql = "INSERT INTO par_asociacion_ruts (rut_emp,rut_pro) " & _
                      "VALUES('" & SP_Rut_Activo & "','" & TxtRutProveedor & "')"
                cn.Execute Sql
            End If
        
            TxtRsocial = RsTmp!nombre_empresa
            
            If TxtRutProveedor = Empty Then Exit Sub
            
            If CboTipoDoc.ListIndex > -1 And Mid(CboTipoDoc.Text, 1, 7) = "FACTURA" Then
             
                If CboTipoDoc.ItemData(CboTipoDoc.ListIndex) <> IG_id_GuiaProveedor Then
                    'Aqui buscamos si el Proveedor a emitido guias y no estan facturadas
                    Sql = "SELECT id,fecha,no_documento,rut,nombre_proveedor,neto,iva,total " & _
                          "FROM com_doc_compra " & _
                          "WHERE  rut_emp='" & SP_Rut_Activo & "' AND rut='" & TxtRutProveedor & "' AND doc_id=" & IG_id_GuiaProveedor & " AND nro_factura=0 "
                    Consulta RsTmp2, Sql
                    If RsTmp2.RecordCount > 0 Then
                        'Se econtraron Guias no facturadas de este proveedor
                        'Abrimos ventana de guias no facturadas
                         'LLenar_Grilla RsTmp2, Me, LvDetalle, True, True, True, False
                        'Stab.TabVisible(1) = True
                        '�Stab.Tab = 1
                        RutBuscado = TxtRutProveedor
                        
                        compra_FacturaGuias.Im_Id_Factura = CboTipoDoc.ItemData(CboTipoDoc.ListIndex)
                        compra_FacturaGuias.Sm_Plazo = Format(DtFecha.Value + CboPlazos.ItemData(CboPlazos.ListIndex), "YYYY-MM-DD")
                        compra_FacturaGuias.Sm_Exento = Me.TxtExento
                        compra_FacturaGuias.Sm_Fecha = Fql(DtFecha)
                        compra_FacturaGuias.Sm_NroDoc = TxtNDoc
                        compra_FacturaGuias.Sm_TipoDoc = CboTipoDoc.Text
                        compra_FacturaGuias.Sm_Rsocial = TxtRsocial
                        compra_FacturaGuias.Show 1
                        If SG_codigo2 = "grabado" Then
                            TxtRutProveedor = ""
                            TxtNDoc = ""
                            TxtRsocial = ""
                            CmdSalir_Click
                          '  Unload Me
                        End If
                    End If
               End If
            End If
            
        Else
            MsgBox "Proveedor no encontrado... ", vbOKOnly + vbInformation
            TxtRsocial = ""
            
            Exit Sub
        End If
    End If
End Sub
Private Sub ValidarControl()
    If Val(Me.ActiveControl.Text) = 0 Then
        Me.ActiveControl.Text = 0
    Else
        Me.ActiveControl.Text = Format(Me.ActiveControl.Text, "#,0")
    End If
    'Aqui sumaremos todo
    CTotal
End Sub

Private Sub TxtSolicitadoPor_GotFocus()
    En_Foco Me.ActiveControl
End Sub

Private Sub TxtSolicitadoPor_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
     If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtSolicitadoPor_Validate(Cancel As Boolean)
TxtSolicitadoPor = Replace(TxtSolicitadoPor, "'", "")
End Sub

Private Sub txtSubNeto_GotFocus()
    En_Foco Me.ActiveControl
End Sub

Private Sub txtSubNeto_KeyPress(KeyAscii As Integer)
        KeyAscii = AceptaSoloNumeros(KeyAscii)
End Sub
Private Sub txtSubNeto_Validate(Cancel As Boolean)

    If SinDesborde(Me.ActiveControl) Then
        Cancel = False
    Else
        Cancel = True
        txtSubNeto = 0 'ESTE SE REEMPLAZA POR EL CONTROL ACTIVO
    End If
    
    ValidarControl
    TxtNeto = Format(CDbl(txtSubNeto) - CDbl(TxtDescuentoNeto), "#,0")
    CalculaIva
End Sub





Private Sub TxtValorImp_GotFocus()
    En_Foco Me.ActiveControl
End Sub
Private Sub TxtValorImp_KeyPress(KeyAscii As Integer)
    KeyAscii = AceptaSoloNumeros(KeyAscii)
End Sub
Private Sub TxtValorImp_Validate(Cancel As Boolean)
    ValidarControl
End Sub
Private Sub ImprimeOC()
    
    Dim Cx As Double, Cy As Double, Sp_Fecha As String, Ip_Mes As Integer, Dp_S As Double, Sp_Letras As String
    Dim Sp_Neto As String * 12, Sp_IVA As String * 12
    
    
    
    
    Dim Sp_Nro_Comprobante As String
    
    Dim Sp_Empresa As String
    Dim Sp_Giro As String
    Dim Sp_Direccion As String
    Dim Sp_Fono As String * 12
    Dim Sp_Mail As String
    Dim Sp_Ciudad As String * 15
    Dim Sp_Comuna As String * 13
    
    
    Dim PSp_Empresa As String
    Dim PSp_Giro As String
    Dim PSp_Direccion As String
    Dim PSp_Fono As String * 12
    Dim PSp_Mail As String
    Dim PSp_Ciudad As String * 15
    Dim PSp_Comuna As String * 13
    
    
    'Variables para detalle de articulos
    Dim Sp_CodigoInt As String * 14
    Dim Sp_Descripcin As String * 29
    Dim Sp_PU As String * 11
    Dim Sp_Cant As String * 7
    Dim Sp_UM As String * 6
    Dim Sp_TotalL As String * 12
    
    Dim sp_AExento As String * 15
    Dim Sp_ANeto As String * 15
    Dim Sp_AIva As String * 15
    Dim Sp_AEspecifico As String * 15
    Dim Sp_ATotal As String * 15
    
    
    
    
    
    Dim Sp_Nombre As String * 45
    Dim Sp_Rut As String * 15
    Dim Sp_Concepto As String
    
    Dim Sp_Nro_Doc As String * 12
    Dim Sp_Documento As String * 35
    Dim Sp_Valor As String * 15
    Dim Sp_Saldo As String * 15
    Dim Sp_Fpago As String * 24
    Dim Sp_Total As String * 12
    
    Dim Sp_Banco As String * 20
    Dim Sp_NroCheque As String * 10
    Dim Sp_FechaCheque As String * 10
    Dim Sp_ValorCheque As String * 15
    Dim Sp_SumaCheque As String * 15
    
    
    
    
    
    
    Dim Sp_Observacion As String * 80
    
    
    For u = 1 To Dialogo.Copies
    
    
    
    
    On Error GoTo ERRORIMPRESION
    Sql = "SELECT giro,direccion,ciudad,fono,email,comuna " & _
         "FROM sis_empresas " & _
         "WHERE rut='" & SP_Rut_Activo & "'"
    Consulta RsTmp2, Sql
    If RsTmp2.RecordCount > 0 Then
        Sp_Giro = RsTmp2!giro
        Sp_Direccion = RsTmp2!direccion
        Sp_Fono = RsTmp2!fono
        Sp_Mail = RsTmp2!Email
        Sp_Ciudad = RsTmp2!ciudad
        Sp_Comuna = RsTmp2!comuna
    End If
            
    Sql = "SELECT direccion,ciudad,fono,email,comuna " & _
         "FROM maestro_proveedores " & _
         "WHERE rut_proveedor='" & TxtRutProveedor & "'"
    Consulta RsTmp2, Sql
    If RsTmp2.RecordCount > 0 Then
        PSp_Direccion = RsTmp2!direccion
        PSp_Fono = RsTmp2!fono
        PSp_Mail = RsTmp2!Email
        PSp_Ciudad = RsTmp2!ciudad
        PSp_Comuna = RsTmp2!comuna
    End If
            
            
            
    Printer.FontName = "Courier New"
    Printer.FontSize = 10
    Printer.ScaleMode = 7
    Cx = 2
    Cy = 3
    Dp_S = 0.13
    
    
    Printer.CurrentX = Cx
    Printer.CurrentY = Cy
    'Printer.PaintPicture LoadPicture(App.Path & "\IMG\LAFLOR\LAFLOR.jpg"), Cx + 0.62, 1
    Printer.PaintPicture LoadPicture(App.Path & "\REDMAROK.jpg"), Cx + 0.62, 1
    
    Printer.CurrentY = Printer.CurrentY + Dp_S
    
    Printer.FontSize = 18
    Printer.FontBold = True
    
    Printer.Print "            ORDEN DE COMPRA NRO " & TxtNDoc
    Printer.CurrentY = Printer.CurrentY + Dp_S + Dp_S
            
    Printer.FontSize = 12
    Printer.CurrentX = Cx
    Printer.Print SP_Empresa_Activa
    
    
    Printer.CurrentX = Cx
    Printer.CurrentY = Printer.CurrentY + Dp_S
  
    Printer.FontSize = 12
  
    pos = Printer.CurrentY
    Printer.Print "R.U.T.   :" & SP_Rut_Activo
    Printer.CurrentY = pos
    Printer.CurrentX = Cx + 10
    Printer.Print "FECHA:" & DtFecha.Value
    
    Printer.CurrentY = Printer.CurrentY + Dp_S
    
    Printer.FontBold = False
    Printer.FontSize = 12
    Printer.CurrentX = Cx
    Printer.Print "GIRO     :" & Sp_Giro
    Printer.CurrentY = Printer.CurrentY + Dp_S
    
    Printer.FontSize = 12
    Printer.CurrentX = Cx
    Printer.Print "DIRECCION:" & Sp_Direccion
    Printer.CurrentY = Printer.CurrentY + Dp_S
    
    'email
    Printer.FontSize = 12
    Printer.CurrentX = Cx
    Printer.Print "EMAIL    :" & Sp_Mail
    Printer.CurrentY = Printer.CurrentY + Dp_S
    
    
    Printer.FontSize = 12
    Printer.CurrentX = Cx
    Printer.Print "CIUDAD   :" & Sp_Ciudad & "   COMUNA:" & Sp_Comuna & "   FONO:" & Sp_Fono
    Printer.CurrentY = Printer.CurrentY + Dp_S + (Dp_S * 3)
    
    'FIN SECCION EMPRESA
    
    
    
    'AHORA SECCION PROVEEDOR
    'Printer.FontSize = 10
    pos = Printer.CurrentY
    Printer.CurrentX = Cx
    Printer.Print "SE�ORES  :" & TxtRsocial
    Printer.CurrentY = Printer.CurrentY + Dp_S
    
    
    
    Printer.CurrentX = Cx
    pos = Printer.CurrentY
    Printer.Print "RUT      :" & TxtRutProveedor
    
    Printer.CurrentY = pos
    Printer.CurrentX = Cx + 8
    
    If CboFpago.Text = "CREDITO" Then
        Printer.Print "CONDICION DE PAGO:" & CboPlazos.Text
    Else
        Printer.Print "CONDICION DE PAGO:" & Me.CboPlazos.Text
    End If
    
    Printer.CurrentY = Printer.CurrentY + Dp_S
    
    
    Printer.CurrentX = Cx
    Printer.Print "DIRECCION:" & PSp_Direccion
    Printer.CurrentY = Printer.CurrentY + Dp_S
    
    Printer.CurrentX = Cx
    Printer.Print "EMAIL    :" & PSp_Mail
    Printer.CurrentY = Printer.CurrentY + Dp_S
        
    Printer.CurrentX = Cx
    Printer.Print "CIUDAD   :" & PSp_Ciudad & "    COMUNA:" & PSp_Comuna & "    FONO:" & PSp_Fono
    Printer.CurrentY = Printer.CurrentY + Dp_S + (Dp_S * 3)
    
    'Fin seccion Proveedor
    
    
    
    'Inicio SECCION DETALLE DE ARTICULOS
            With compra_detalle.LvDetalle
                If .ListItems.Count > 0 Then
                    Printer.CurrentX = Cx
                    Printer.Print "Detalle de Art�culos"
                    Printer.CurrentY = Printer.CurrentY + Dp_S
                    Sp_CodigoInt = "Cod. Interno"
                    Sp_Descripcin = "Descripcion"
                    RSet Sp_PU = "Pre. Unitario"
                    RSet Sp_Cant = "Cant."
                    Sp_UM = "U.Medida"
                    RSet Sp_TotalL = "Total"
                    
                    Printer.FontSize = 10
                    Printer.FontBold = False
                    
                    Printer.CurrentX = Cx
                    
                    Printer.Print Sp_CodigoInt & " " & Sp_Descripcin & " " & Sp_PU & " " & Sp_Cant & " " & Sp_UM & " " & Sp_TotalL
                    Printer.CurrentY = Printer.CurrentY + Dp_S
                    
                    For i = 1 To .ListItems.Count
                        
                        Sp_CodigoInt = ""
                       If Len(.ListItems(i).SubItems(22)) > 0 Then
                            If .ListItems(i).SubItems(22) <> "Falso" Then
                                Coloca Cx, Printer.CurrentY, Sp_CodigoInt & "     (CC: " & .ListItems(i).SubItems(22) & ")", True, 8
                            End If
                                
                            'End If
                            Printer.CurrentY = Printer.CurrentY + Dp_S - 0.2
                            Printer.FontSize = 10
                            Printer.FontBold = False
                            '�Busca_Id_Combo compra_detalle.CboCentroCosto, Val(.ListItems(i).SubItems(9))
                            '�Sp_Descripcin = .ListItems(i).SubItems(2) & " (" & .ListItems(i).SubItems(22) & ")"
                            Sp_Descripcin = .ListItems(i).SubItems(2)
                       Else
                            Sp_Descripcin = .ListItems(i).SubItems(2)
                        End If
                        Sp_CodigoInt = .ListItems(i).SubItems(1)
                        RSet Sp_PU = .ListItems(i).SubItems(4)
                        RSet Sp_Cant = .ListItems(i).SubItems(5)
                        Sp_UM = .ListItems(i).SubItems(6)
                        RSet Sp_TotalL = .ListItems(i).SubItems(7)
                        Printer.CurrentX = Cx
                        Printer.Print Sp_CodigoInt & " " & Sp_Descripcin & " " & Sp_PU & " " & Sp_Cant & " " & Sp_UM & " " & Sp_TotalL
                        Printer.CurrentY = Printer.CurrentY + Dp_S - 0.1
                    Next
                    Printer.CurrentY = Printer.CurrentY + Dp_S
                    posdetalle = Printer.CurrentY
                End If
            End With
    'FIN SECCION DETALLE DE ARTICULOS
    Printer.CurrentX = Cx
    Printer.FontBold = True
    
    RSet sp_AExento = TxtExento
    RSet Sp_ANeto = TxtNeto
    RSet Sp_AIva = txtIVA
    RSet Sp_Especifico = Me.TxtImpuesto
    RSet Sp_ATotal = txtTotal
    
    Printer.CurrentX = Cx + 12
    Printer.CurrentY = Printer.CurrentY + Dp_S + Dp_S + Dp_S
    pos = Printer.CurrentY
    Printer.Print "E X E N T O:"
    Printer.CurrentY = pos
    Printer.CurrentX = Cx + 14.5
    Printer.Print sp_AExento
    
    Printer.CurrentX = Cx + 12
    Printer.CurrentY = Printer.CurrentY + Dp_S - 0.2
    pos = Printer.CurrentY
    Printer.Print "N E T O    :"
    Printer.CurrentY = pos
    posn = Printer.CurrentY
    Printer.CurrentX = Cx + 14.5
    Printer.Print Sp_ANeto
    
    Printer.CurrentX = Cx + 12
    Printer.CurrentY = Printer.CurrentY + Dp_S - 0.2
    pos = Printer.CurrentY
    Printer.Print "I.V.A.     :"
    Printer.CurrentY = pos
    Printer.CurrentX = Cx + 14.5
    Printer.Print Sp_AIva
    
    Printer.CurrentX = Cx + 12
    Printer.CurrentY = Printer.CurrentY + Dp_S - 0.2
    pos = Printer.CurrentY
    Printer.Print "ESPECIFICO :"
    Printer.CurrentY = pos
    Printer.CurrentX = Cx + 14.5
    Printer.Print Sp_Especifico
    
    Printer.CurrentX = Cx + 12
    Printer.CurrentY = Printer.CurrentY + Dp_S - 0.2
    pos = Printer.CurrentY
    Printer.Print "T O T A L  :"
    Printer.CurrentY = pos
    Printer.CurrentX = Cx + 14.5
    Printer.Print Sp_ATotal
    
    Printer.CurrentY = posn
    Printer.CurrentX = Cx
    Printer.Print "SOLICITADO POR:"; TxtSolicitadoPor
    Printer.CurrentX = Cx
    Printer.CurrentY = Printer.CurrentY + Dp_S
    Printer.Print "AUTORIZADO POR:"; TxtAutorizadoPor
    Printer.CurrentX = Cx
    Printer.CurrentY = Printer.CurrentY + Dp_S
    Printer.Print "RETIRADO   POR:"; TxtRetiradoPor
    
    Printer.FontBold = False
        
   ' Printer.DrawMode = 1
    Printer.DrawWidth = 3
            
    Printer.Line (1.2, 4)-(20, 1), , B  'Rectangulo Encabezado y N� Orden de Compra
    Printer.Line (1.2, 4)-(20, 7.7), , B  'Datos de la Empresa
    Printer.Line (1.2, 4)-(20, 11.2), , B 'Datos del Proveedor
    Printer.Line (1.2, 4)-(20, posdetalle), , B 'Detalle de Orden de Compra
    Printer.Line (1.2, posdetalle)-(20, posdetalle + 2.3), , B 'Detalle del Exento,Neto,Iva,Total
       
    Printer.NewPage
    Printer.EndDoc
    
    Next
    
    Exit Sub
ERRORIMPRESION:
    MsgBox Err.Number & vbNewLine & Err.Description
End Sub


Private Sub FacturaElectronicaC(Tipo As String)
    
    Dim Sp_Xml As String, Sp_XmlAdicional As String
    Dim obj As DTECloud.Integracion
    Dim Lp_Sangria As Long
    Dim Sp_Sql As String
    Dim Bp_Adicional As Boolean
    '13-9-2014
    'Modulo genera factura electronica
    'Autor: alvamar
    Bp_Adicional = False
    '1ro Crear xml
    Lp_Sangria = 4
    X = FreeFile
    
    Sp_Xml = App.Path & "\dte\" & CboTipoDoc.ItemData(CboTipoDoc.ListIndex) & "_" & TxtNDoc & ".xml"
    Sp_Xml = "C:\FACTURAELECTRONICA\nueva.xml"
      'Open Sp_Archivo For Output As X
    Open Sp_Xml For Output As X
        '<?xml version="1.0" encoding="ISO-8859-1"?>
        Print #X, "<DTE version=" & Chr(34) & "1.0" & Chr(34) & ">"
        Print #X, "" & Space(Lp_Sangria) & "<Documento ID=" & Chr(34) & "ID" & Lp_Id & Chr(34) & ">"
        'Encabezado
        Print #X, "" & Space(Lp_Sangria) & "<Encabezado>"
            'id documento
            cn.Execute "UPDATE dte_folios SET dte_disponible='NO' WHERE doc_id=" & Tipo & " AND dte_folio=" & TxtNDoc & " AND rut_emp='" & SP_Rut_Activo & "'"
            Print #X, "" & Space(Lp_Sangria * 2) & "<IdDoc>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<TipoDTE>" & Trim(Tipo) & "</TipoDTE>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<Folio>" & Trim(TxtNDoc) & "</Folio>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<FchEmis>" & Fql(DtFecha) & "</FchEmis>"
           '     Print #X, "" & Space(Lp_Sangria * 3) & "<FchVenc>" & Format(DtFecha.Value + CboPlazos.ItemData(CboPlazos.ListIndex), "; YYYY - MM - DD; ") & "</FchVenc>"
                If Sm_Con_Precios_Brutos = "SI" Then
                    '30 Mayo 2015, cuando las lineas de detalle son precios brutos se debe identificar aqui.
                    Print #X, "" & Space(Lp_Sangria * 2) & "<MntBruto>1</MntBruto>"
                End If
            Print #X, "" & Space(Lp_Sangria * 2) & "</IdDoc>"
            'Emisor
            'Select empresa para obtener los datos del emisor
            Sp_Sql = "SELECT nombre_empresa,direccion,comuna,ciudad,giro,emp_codigo_actividad_economica actividad " & _
                    "FROM sis_empresas " & _
                    "WHERE rut='" & SP_Rut_Activo & "'"
            Consulta RsTmp, Sp_Sql
            
            Print #X, "" & Space(Lp_Sangria * 2) & " <Emisor>"
                'Print #X, "" & Space(Lp_Sangria * 3) & "<RUTEmisor>" & Replace(SP_Rut_Activo, ".", "") & "</RUTEmisor>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<RUTEmisor>" & Replace(SP_Rut_Activo, ".", "") & "</RUTEmisor>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<RznSoc>" & Replace(RsTmp!nombre_empresa, "�", "N") & "</RznSoc>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<GiroEmis>" & Replace(RsTmp!giro, "�", "N") & "</GiroEmis>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<Acteco>" & Replace(RsTmp!actividad, "�", "N") & "</Acteco>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<DirOrigen>" & Replace(RsTmp!direccion, "�", "N") & "</DirOrigen>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<CmnaOrigen>" & Replace(RsTmp!comuna, "�", "N") & "</CmnaOrigen>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<CiudadOrigen>" & Replace(RsTmp!ciudad, "�", "N") & "</CiudadOrigen>"
            Print #X, "" & Space(Lp_Sangria * 2) & "</Emisor>"
            
            'Receptor
            
            'buscar datos del proveedor
           
            
            Sql = "SELECT nombre_empresa,direccion,comuna,ciudad,fono " & _
                    "FROM maestro_proveedores " & _
                    "WHERE rut_proveedor='" & TxtRutProveedor & "'"
            Consulta RsTmp, Sql
            If RsTmp.RecordCount > 1 Then
                
            
            
            End If
            
            
            Print #X, "" & Space(Lp_Sangria * 2) & " <Receptor>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<RUTRecep>" & Replace(TxtRutProveedor, ".", "") & "</RUTRecep>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<RznSocRecep>" & Mid(Replace(RsTmp!nombre_empresa, "�", "N"), 1, 100) & "</RznSocRecep>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<GiroRecep>COMERCIAL</GiroRecep>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<Contacto></Contacto>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<DirRecep>" & Mid(Replace(RsTmp!direccion, "�", "N"), 1, 60) & "</DirRecep>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<CmnaRecep>" & Mid(Replace(RsTmp!comuna, "�", "N"), 1, 20) & "</CmnaRecep>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<CiudadRecep>" & Mid(Replace(RsTmp!ciudad, "�", "N"), 1, 20) & "</CiudadRecep>"
            Print #X, "" & Space(Lp_Sangria * 2) & "</Receptor>"
            
             'Totales
                Print #X, "" & Space(Lp_Sangria * 2) & " <Totales>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<MntNeto>" & CDbl(TxtNeto) & "</MntNeto>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<MntExe>0</MntExe>"
                If SP_Rut_Activo <> "96.803.210-0" Then
                    Print #X, "" & Space(Lp_Sangria * 3) & " <TasaIVA>" & DG_IVA & "</TasaIVA>"
                Else
                    Print #X, "" & Space(Lp_Sangria * 3) & " <TasaIVA>0</TasaIVA>"
                End If
                Print #X, "" & Space(Lp_Sangria * 3) & " <IVA>" & CDbl(txtIVA) & "</IVA>"
                
                If Val(txtIvaRetenido) > 0 Then
                    'Antes del monto total debemos verificar si la factura incluye impuestos retenidos
    '                                         <ImptoReten>
    '                                            <TipoImp>18</TipoImp>
    '                                            <TasaImp>5</TasaImp>
    '                                            <MontoImp>8887</MontoImp>
    '                                        </ImptoReten>
                     Print #X, "" & Space(Lp_Sangria * 3) & " <ImptoReten>"
                     Print #X, "" & Space(Lp_Sangria * 6) & "<TipoImp>15</TipoImp>"
                     Print #X, "" & Space(Lp_Sangria * 6) & "<TasaImp>19</TasaImp>"
                    Print #X, "" & Space(Lp_Sangria * 6) & "<MontoImp>" & CDbl(Me.txtIvaRetenido) & "</MontoImp>"
                           
                    Print #X, "" & Space(Lp_Sangria * 3) & " </ImptoReten>"
                End If


                Print #X, "" & Space(Lp_Sangria * 3) & "<MntTotal>" & CDbl(txtTotal) & "</MntTotal>"
                Print #X, "" & Space(Lp_Sangria * 2) & "</Totales>"
                
            'Cerramos encabezado
            Print #X, "" & Space(Lp_Sangria) & "</Encabezado>"
                
                
           'Comenzamos el detalle
           
           If SP_Control_Inventario = "SI" Then
           'Recorremos la grilla con productos
                        Sql = ""
           
           
           
                       For i = 1 To compra_detalle.LvDetalle.ListItems.Count
                          Print #X, "" & Space(Lp_Sangria) & "<Detalle>"
                              Print #X, "" & Space(Lp_Sangria * 2) & "<NroLinDet>" & i & "</NroLinDet>"
                              Print #X, "" & Space(Lp_Sangria * 2) & " <CdgItem>"
                              Print #X, "" & Space(Lp_Sangria * 3) & "<TpoCodigo>PLU</TpoCodigo>"
                              Print #X, "" & Space(Lp_Sangria * 3) & "<VlrCodigo>" & compra_detalle.LvDetalle.ListItems(i).SubItems(1) & "</VlrCodigo>"
                              Print #X, "" & Space(Lp_Sangria * 2) & "</CdgItem>"
                              Print #X, "" & Space(Lp_Sangria * 2) & "<NmbItem>" & Replace(compra_detalle.LvDetalle.ListItems(i).SubItems(2), "�", "N") & "</NmbItem>"
                              Print #X, "" & Space(Lp_Sangria * 2) & "<QtyItem>" & Replace(compra_detalle.LvDetalle.ListItems(i).SubItems(5), ",", ".") & "</QtyItem>"
                              Print #X, "" & Space(Lp_Sangria * 2) & "<PrcItem>" & CDbl(compra_detalle.LvDetalle.ListItems(i).SubItems(4)) & "</PrcItem>"
              '                    <DescuentoPct>15</DescuentoPct>
              '                    <DescuentoMonto>4212</DescuentoMonto>
              '                             <!- Solo se indica el c�digo del impuesto adicional '
                              'If Val(LvMateriales.ListItems(i).SubItems(23)) > 0 Then
                              '      Print #X, "" & Space(Lp_Sangria * 2) & "<CodImpAdic>" & LvMateriales.ListItems(i).SubItems(23) & "</CodImpAdic>"
              ''
               '               End If
                              Print #X, "" & Space(Lp_Sangria * 2) & "<MontoItem>" & CDbl(compra_detalle.LvDetalle.ListItems(i).SubItems(7)) & "</MontoItem>"
                      
                          Print #X, "" & Space(Lp_Sangria) & "</Detalle>"
                      Next
            Else
                'CUANDO ES SIN INVENTARIO
                
            
            
            
            End If
            'Cerramos detalle
            

        Print #X, "" & Space(Lp_Sangria) & "</Documento>"
        Print #X, "" & Space(Lp_Sangria) & "</DTE>"
    Close #X
        
        
  '  If Len(TxtComentario) > 0 Or Len(TxtOrdenesCompra) > 0 Then
        Bp_Adicional = True
        X = FreeFile
        Sp_XmlAdicional = "C:\FACTURAELECTRONICA\nueva_adicional.xml"
        Open Sp_XmlAdicional For Output As X
            Print #X, "<Adicional>"
           
            Print #X, "" & Space(Lp_Sangria) & "<Tres>" & Mid(CboPlazos.Text, 1, Len(CboPlazos.Text) - 10) & "</Tres>"
            Print #X, "" & Space(Lp_Sangria) & "<Cuatro></Cuatro>"
            Print #X, "" & Space(Lp_Sangria) & "<Cinco></Cinco>"
            Print #X, "" & Space(Lp_Sangria) & "<Seis></Seis>"
            Print #X, "" & Space(Lp_Sangria) & "<Siete></Siete>"
            Print #X, "" & Space(Lp_Sangria) & "<Ocho></Ocho>"
            Print #X, "" & Space(Lp_Sangria) & "<Nueve></Nueve>"
            Print #X, "" & Space(Lp_Sangria) & "<Diez></Diez>"
            Print #X, "</Adicional>"
        Close #X
    
    
  '  End If
        
        Set obj = New DTECloud.Integracion
   
        obj.UrlServicioFacturacion = SG_Url_Factura_Electronica '"http://wsdtedesa.dyndns.biz/WSDTE/Service.asmx"
        obj.HabilitarDescarga = True
        obj.FormatoNombrePDF = "0"
        
        If Dir("C:\FACTURAELECTRONICA\PDF\" & Replace(SP_Rut_Activo, ".", ""), vbDirectory) = "" Then
            MsgBox "La carpeta no existe, se creara automaticamente"
            MkDir ("C:\FACTURAELECTRONICA\PDF\" & Replace(SP_Rut_Activo, ".", ""))
            
        End If
        
        obj.CarpetaDestino = "C:\FACTURAELECTRONICA\PDF\" & Replace(SP_Rut_Activo, ".", "")
        obj.Password = "1234"

     
        
        
        'DTECLOUD obj.TipoImpresionRDL="6-7";
        obj.AsignarFolio = False
        obj.IncluirCopiaCesion = True
        
        'MSXML.DOMDocument
        Dim Documento As MSXML2.DOMDocument30
        Set Documento = New DOMDocument30
        
        Documento.preserveWhiteSpace = True
        Documento.Load (Sp_Xml)
        '  Dim oXMLAdicional As New xml.XMLDocument
        Dim documento2 As MSXML2.DOMDocument30
        Set documento2 = New DOMDocument30
        
        documento2.preserveWhiteSpace = True
        If Bp_Adicional Then
            documento2.Load (Sp_XmlAdicional)
        Else
            documento2.Load ("")
        End If
        obj.Productivo = True
        If obj.ProcesarXMLBasico(Documento.XML, documento2.XML) Then
             'MsgBox obj.URLPDF
           
             Archivo = "C:\FACTURAELECTRONICA\PDF\" & Replace(SP_Rut_Activo, ".", "") & "\T" & Tipo & "F" & TxtNDoc & ".PDF"
             ShellExecute Me.hWnd, "open", Archivo, "", "", 4
            ' MsgBox obj.URLPDF
        Else
            MsgBox (CStr(obj.IDResultado) + " " + obj.DescripcionResultado)
        End If
    
        
    FraProcesandoDTE.Visible = False
        
End Sub


Private Sub AcuseRecibo()

        
        FraProcesandoDTE.Visible = True
        'Dim resp As Boolean
        DoEvents
        'Consulta electronica
        Dim obj As DTECloud.Integracion
        Set obj = New DTECloud.Integracion
        Dim sRut As String
        Dim sRutE As String
        Dim sTipo As String
        Dim sFolio As Long
        Dim iCodigoAcuse As String
        Dim sGlosa As String
        Dim bProductivo As Boolean
        Dim I_Cn As Integer
       
        iCodigoAcuse = "0"
        sGlosa = "Aceptado Ok en Recepcion de Compra"
        bProductivo = True
       
        FrmLoad.Visible = True
        On Error GoTo ErrorAcuse
        DoEvents
        cnt = 0
        
                sRut = Replace(SP_Rut_Activo, ".", "")
                sRutE = Replace(Me.TxtRutProveedor, ".", "")
                Sql = "SELECT doc_cod_sii " & _
                        "FROM sis_documentos " & _
                        "WHERE doc_id=" & Me.CboTipoDoc.ItemData(CboTipoDoc.ListIndex)
                Consulta RsTmp, Sql
                If RsTmp.RecordCount > 0 Then
                    sTipo = "" & RsTmp!doc_cod_sii
                End If
                
                
                sFolio = Me.TxtNDoc
                'Los iCodigoAcuse son '
                '0: Aceptado OK
                '1: Aceptado con Reparo
                '2: Rechazo Comercial
                'iCodigoAcuse = Mid(Me.CboGlosaAcuse.Text, 1, 1)
                'sGlosa = "Aceptado OK"
                
                
                obj.UrlServicioFacturacion = SG_Url_Factura_Electronica
                obj.Productivo = bProductivo
                obj.Password = "1234"
                
                'obj.AcuseComercialDTE(
                If obj.AcuseComercialDTE(sRut, sRutE, sTipo, sFolio, bProductivo, iCodigoAcuse, sGlosa, "acuse@alvamar.cl") Then
                    cnt = cnt + 1
                    Sql = "INSERT INTO sii_acuse (rut_emp,acu_folio,acu_cod_sii,acu_rut_proveedor,acu_codigo_acuse,acu_glosa) " & _
                            "VALUES('" & SP_Rut_Activo & "'," & sFolio & "," & sTipo & ",'" & TxtRutProveedor & "'," & iCodigoAcuse & ",'" & sGlosa & "')"
                    cn.Execute Sql
                    'LvLibro.ListItems(i).SubItems(12) = sGlosa
                    If obj.AcuseDeReciboDeMercaderias(sRut, sRutE, sFolio, sTipo, "DOMICILIO COMERCIAL", True, "acuse@alvamar.cl") Then
                         Sql = "INSERT INTO sii_acuse (rut_emp,acu_folio,acu_cod_sii,acu_rut_proveedor,acu_codigo_acuse,acu_glosa,acu_mercaderia) " & _
                            "VALUES('" & SP_Rut_Activo & "'," & sFolio & "," & sTipo & ",'" & TxtRutProveedor & "'," & iCodigoAcuse & ",'" & sGlosa & "','SI')"
                        cn.Execute Sql
                    End If
                   
                Else
                    MsgBox "Folio: " & sFolio & vbNewLine & "RUT :" & sRutE & vbNewLine & "No se pudo dar acuse a este DTE" & vbNewLine & Err.Description
                End If
            
        Me.FrmLoad.Visible = False
        FraProcesandoDTE.Visible = False
        MsgBox "Se dio acuse de recibo a " & cnt & " DTEs" & vbNewLine & vwnewline & "IMPORTANTE: Verifique en el sitio del SII el acuse correcto de sus documentos", vbInformation
        
        Exit Sub
ErrorConexion:
    FrmLoad.Visible = False
    Exit Sub
Me.FraProcesandoDTE.Visible = False
    MsgBox "Algo fallo :(  ..." & vbNewLine & vbNewLine & Err.Description, vbExclamation
    Exit Sub
ErrorAcuse:

    FrmLoad.Visible = False
 FraProcesandoDTE.Visible = False
    MsgBox "Algo fallo durante el envio:(  ..." & vbNewLine & vbNewLine & Err.Description, vbExclamation
End Sub


