VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form com_boletashonorarios 
   Caption         =   "Ingreso Boletas Honorarios"
   ClientHeight    =   5820
   ClientLeft      =   3270
   ClientTop       =   2700
   ClientWidth     =   8325
   LinkTopic       =   "Form1"
   ScaleHeight     =   5820
   ScaleWidth      =   8325
   Begin VB.Frame Frame3 
      Caption         =   "Ingrese datos de boleta"
      Height          =   4935
      Left            =   1020
      TabIndex        =   23
      Top             =   270
      Width           =   6165
      Begin VB.Frame FrmPeriodo 
         Caption         =   "Periodo Contable"
         Height          =   780
         Left            =   3690
         TabIndex        =   32
         Top             =   285
         Width           =   2250
         Begin VB.TextBox TxtAnoContable 
            Alignment       =   1  'Right Justify
            BackColor       =   &H8000000F&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   1365
            Locked          =   -1  'True
            TabIndex        =   34
            TabStop         =   0   'False
            Text            =   "0"
            Top             =   390
            Width           =   780
         End
         Begin VB.TextBox TxtMesContable 
            Alignment       =   1  'Right Justify
            BackColor       =   &H8000000F&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   60
            Locked          =   -1  'True
            TabIndex        =   33
            TabStop         =   0   'False
            Text            =   "0"
            Top             =   390
            Width           =   1275
         End
      End
      Begin VB.TextBox TxtRutProveedor 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1785
         TabIndex        =   5
         Tag             =   "T"
         Top             =   1755
         Width           =   1290
      End
      Begin VB.CommandButton CmdBuscaProv 
         Caption         =   "Buscar"
         Height          =   255
         Left            =   3105
         TabIndex        =   31
         Top             =   1800
         Width           =   660
      End
      Begin VB.TextBox TxtRsocial 
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1800
         Locked          =   -1  'True
         TabIndex        =   30
         TabStop         =   0   'False
         Top             =   2115
         Width           =   4155
      End
      Begin VB.CommandButton CmdNuevoProveedor 
         Caption         =   "Crear"
         Height          =   255
         Left            =   3765
         TabIndex        =   29
         Top             =   1800
         Width           =   675
      End
      Begin VB.TextBox TxtNDoc 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1785
         TabIndex        =   4
         Tag             =   "N"
         Text            =   "0"
         Top             =   1425
         Width           =   1290
      End
      Begin VB.ComboBox CboTipoDoc 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         ItemData        =   "com_boletashonorarios.frx":0000
         Left            =   1785
         List            =   "com_boletashonorarios.frx":000D
         Style           =   2  'Dropdown List
         TabIndex        =   3
         Top             =   1065
         Width           =   4155
      End
      Begin VB.ComboBox CboPlazos 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         ItemData        =   "com_boletashonorarios.frx":0032
         Left            =   1785
         List            =   "com_boletashonorarios.frx":0034
         Style           =   2  'Dropdown List
         TabIndex        =   2
         Top             =   690
         Width           =   1665
      End
      Begin VB.Frame Frame1 
         Caption         =   "Valores"
         Height          =   1890
         Left            =   585
         TabIndex        =   25
         Top             =   2475
         Width           =   5400
         Begin VB.TextBox txtTotal 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   2760
            TabIndex        =   6
            Tag             =   "N"
            Text            =   "0"
            Top             =   360
            Width           =   1290
         End
         Begin VB.TextBox txtRetencion 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   2760
            TabIndex        =   7
            Tag             =   "N"
            Text            =   "0"
            Top             =   705
            Width           =   1290
         End
         Begin VB.TextBox txtPagar 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   2760
            TabIndex        =   8
            Tag             =   "N"
            Text            =   "0"
            Top             =   1020
            Width           =   1290
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
            Height          =   225
            Index           =   1
            Left            =   1200
            OleObjectBlob   =   "com_boletashonorarios.frx":0036
            TabIndex        =   26
            Top             =   435
            Width           =   1485
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
            Height          =   225
            Index           =   2
            Left            =   1200
            OleObjectBlob   =   "com_boletashonorarios.frx":0097
            TabIndex        =   27
            Top             =   780
            Width           =   1485
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
            Height          =   225
            Index           =   3
            Left            =   1200
            OleObjectBlob   =   "com_boletashonorarios.frx":0100
            TabIndex        =   28
            Top             =   1095
            Width           =   1485
         End
      End
      Begin VB.CommandButton CmdGuardar 
         Caption         =   "Guardar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   675
         TabIndex        =   24
         Top             =   4410
         Width           =   1560
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel10 
         Height          =   225
         Left            =   675
         OleObjectBlob   =   "com_boletashonorarios.frx":0165
         TabIndex        =   35
         Top             =   2130
         Width           =   1020
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   225
         Index           =   0
         Left            =   225
         OleObjectBlob   =   "com_boletashonorarios.frx":01E0
         TabIndex        =   36
         Top             =   1485
         Width           =   1485
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   225
         Index           =   0
         Left            =   135
         OleObjectBlob   =   "com_boletashonorarios.frx":0251
         TabIndex        =   37
         Top             =   1125
         Width           =   1575
      End
      Begin MSComCtl2.DTPicker DtFecha 
         Height          =   345
         Left            =   1785
         TabIndex        =   1
         Top             =   360
         Width           =   1665
         _ExtentX        =   2937
         _ExtentY        =   609
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         CustomFormat    =   "dd-mm-yyyy hh:mm:ss"
         Format          =   273416193
         CurrentDate     =   39855
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   225
         Left            =   660
         OleObjectBlob   =   "com_boletashonorarios.frx":02CA
         TabIndex        =   38
         Top             =   420
         Width           =   1065
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
         Height          =   225
         Left            =   210
         OleObjectBlob   =   "com_boletashonorarios.frx":033B
         TabIndex        =   39
         Top             =   1800
         Width           =   1500
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel9 
         Height          =   195
         Left            =   1110
         OleObjectBlob   =   "com_boletashonorarios.frx":03B0
         TabIndex        =   40
         Top             =   735
         Width           =   615
      End
   End
   Begin VB.CommandButton CmdRetorna 
      Caption         =   "&Retornar"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   6120
      TabIndex        =   0
      Top             =   5340
      Width           =   1065
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   15
      OleObjectBlob   =   "com_boletashonorarios.frx":0411
      Top             =   1935
   End
   Begin VB.Timer Timer1 
      Interval        =   100
      Left            =   225
      Top             =   300
   End
   Begin VB.Frame Frame2 
      Caption         =   "Cuentas Boletas"
      Height          =   2820
      Left            =   315
      TabIndex        =   9
      Top             =   6015
      Width           =   8985
      Begin VB.TextBox TxtDebe 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   5925
         Locked          =   -1  'True
         TabIndex        =   21
         TabStop         =   0   'False
         Text            =   "0"
         Top             =   2310
         Width           =   1410
      End
      Begin VB.TextBox TxtHaber 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   7350
         Locked          =   -1  'True
         TabIndex        =   20
         TabStop         =   0   'False
         Text            =   "0"
         Top             =   2310
         Width           =   1395
      End
      Begin VB.CommandButton CmdOk 
         Caption         =   "Ok"
         Height          =   375
         Left            =   8445
         TabIndex        =   16
         Top             =   495
         Width           =   420
      End
      Begin VB.TextBox TxtCredito 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   7080
         TabIndex        =   15
         Text            =   "0"
         Top             =   510
         Width           =   1380
      End
      Begin VB.TextBox TxtDebito 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   5700
         TabIndex        =   14
         Text            =   "0"
         Top             =   510
         Width           =   1380
      End
      Begin VB.TextBox TxtNombre 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   1320
         Locked          =   -1  'True
         TabIndex        =   13
         TabStop         =   0   'False
         Top             =   510
         Width           =   4365
      End
      Begin VB.TextBox TxtCodigoCuenta 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   165
         TabIndex        =   12
         Top             =   510
         Width           =   1140
      End
      Begin VB.CommandButton CmdCuenta 
         Caption         =   "Cuenta"
         Height          =   195
         Left            =   180
         TabIndex        =   11
         Top             =   300
         Width           =   930
      End
      Begin MSComctlLib.ListView LVDetalle 
         Height          =   1350
         Left            =   180
         TabIndex        =   10
         Top             =   870
         Width           =   8685
         _ExtentX        =   15319
         _ExtentY        =   2381
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   6
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "T500"
            Text            =   "Cod"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "N109"
            Text            =   "Cuenta"
            Object.Width           =   1993
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "T3000"
            Text            =   "CUENTAS"
            Object.Width           =   7726
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   3
            Key             =   "debe"
            Object.Tag             =   "N100"
            Text            =   "DEBE"
            Object.Width           =   2434
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   4
            Key             =   "haber"
            Object.Tag             =   "N100"
            Text            =   "HABER"
            Object.Width           =   2434
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Key             =   "retenido"
            Object.Tag             =   "N100"
            Text            =   "Retenido"
            Object.Width           =   0
         EndProperty
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel7 
         Height          =   240
         Left            =   1305
         OleObjectBlob   =   "com_boletashonorarios.frx":0645
         TabIndex        =   17
         Top             =   315
         Width           =   2475
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel8 
         Height          =   240
         Left            =   6225
         OleObjectBlob   =   "com_boletashonorarios.frx":06C9
         TabIndex        =   18
         Top             =   315
         Width           =   795
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel5 
         Height          =   240
         Left            =   7635
         OleObjectBlob   =   "com_boletashonorarios.frx":0735
         TabIndex        =   19
         Top             =   315
         Width           =   780
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel6 
         Height          =   240
         Left            =   4065
         OleObjectBlob   =   "com_boletashonorarios.frx":07A3
         TabIndex        =   22
         Top             =   2370
         Width           =   1665
      End
   End
End
Attribute VB_Name = "com_boletashonorarios"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False



Private Sub CmdBuscaProv_Click()
    AccionProveedor = 0
    BuscaProveedor.Show 1
    txtRutProveedor = RutBuscado

    txtRutProveedor_Validate True
End Sub

Private Sub cmdCuenta_Click()
     With BuscarSimple
        SG_codigo = Empty
        .Sm_Consulta = "SELECT pla_id, pla_nombre,tpo_nombre,det_nombre " & _
                       "FROM    con_plan_de_cuentas p,  con_tipo_de_cuenta t,   con_detalle_tipo_cuenta d " & _
                       "WHERE   p.tpo_id = t.tpo_id AND p.det_id = d.det_id "
        .Sm_CampoLike = "pla_nombre"
        .Im_Columnas = 2
        .CmdCuentasContables.Visible = True
        .Show 1
        If SG_codigo = Empty Then Exit Sub
        TxtCodigoCuenta = Val(SG_codigo)
        TxtCodigoCuenta.SetFocus
    End With
End Sub

Private Sub CmdGuardar_Click()
    Dim Lp_IdCompra As Long, Lp_ValorDebe As Long, Lp_ValorHaber As Long, Lp_CuentaID As Long
    If Principal.CmdMenu(8).Visible = True Then
        If ConsultaCentralizado("18", IG_Mes_Contable, IG_Ano_Contable) Then
            MsgBox "periodo contable ya ha sido centralizado, " & vbNewLine & "No puede modificar"
            Exit Sub
        End If
    End If
    
    Sql = "SELECT id " & _
            "FROM com_doc_compra " & _
            "WHERE rut='" & txtRutProveedor & "' AND rut_emp='" & SP_Rut_Activo & "' AND doc_id=" & CboTipoDoc.ItemData(CboTipoDoc.ListIndex) & " AND no_documento=" & TxtNDoc
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        MsgBox "Este documento ya ha sido ingresado..."
        TxtNDoc.SetFocus
        Exit Sub
    End If
    
    LvDetalle.ListItems.Clear
    
    Sql = "SELECT doc_cuenta_honorarios cta " & _
            "FROM sis_documentos " & _
            "WHERE doc_id=" & CboTipoDoc.ItemData(CboTipoDoc.ListIndex)
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        LvDetalle.ListItems.Add , , ""
        LvDetalle.ListItems(LvDetalle.ListItems.Count).SubItems(1) = RsTmp!cta
        LvDetalle.ListItems(LvDetalle.ListItems.Count).SubItems(3) = txtTotal
        LvDetalle.ListItems(LvDetalle.ListItems.Count).SubItems(4) = 0
    End If
    LvDetalle.ListItems.Add , , ""
    If Mid(CboPlazos.Text, 1, 7) = "CONTADO" Then
       
        LvDetalle.ListItems(LvDetalle.ListItems.Count).SubItems(1) = IG_Id_CuentaCaja
        LvDetalle.ListItems(LvDetalle.ListItems.Count).SubItems(3) = 0
        LvDetalle.ListItems(LvDetalle.ListItems.Count).SubItems(4) = CDbl(txtPagar)
    
    Else
        LvDetalle.ListItems(LvDetalle.ListItems.Count).SubItems(1) = IG_IdCuentaHonorariosPagar
        LvDetalle.ListItems(LvDetalle.ListItems.Count).SubItems(3) = 0
        LvDetalle.ListItems(LvDetalle.ListItems.Count).SubItems(4) = CDbl(txtPagar)
    End If
    
    If CDbl(txtRetencion) > 0 Then
         LvDetalle.ListItems.Add , , ""
        LvDetalle.ListItems(LvDetalle.ListItems.Count).SubItems(1) = IG_IdRetencionImpHonorarios
        LvDetalle.ListItems(LvDetalle.ListItems.Count).SubItems(3) = 0
        LvDetalle.ListItems(LvDetalle.ListItems.Count).SubItems(4) = CDbl(txtRetencion)
    
    End If
    Tvoucher
    
    If CDbl(TxtDebe) <> CDbl(TxtHaber) Then
        MsgBox "Debe y Haber deben ser iguales...", vbInformation
        Exit Sub
    End If
    
    If CDbl(TxtDebe) = 0 Or CDbl(TxtHaber) = 0 Then
        MsgBox "Totales deben mayor a 0...", vbInformation
        TxtCodigoCuenta.SetFocus
        Exit Sub
    End If
    
    
    'Validaciones listas, ahora grabar
    lacajita = 0
    If SG_Modulo_Caja = "SI" Then
        If Mid(CboPlazos.Text, 1, 7) = "CONTADO" Then
            lacajita = LG_id_Caja
        End If
    End If
    On Error GoTo ErrorBH
    cn.BeginTrans
        Lp_IdCompra = UltimoNro("com_doc_compra", "id")
        Sql = "INSERT INTO com_doc_compra (id,doc_id,no_documento,rut,nombre_proveedor,total,fecha,tipo_movimiento,mes_contable,ano_contable,doc_fecha_vencimiento,rut_emp," & _
                          "pago,com_plazo,caj_id,com_pla_id,iva,neto) " & _
              "VALUES(" & Lp_IdCompra & "," & CboTipoDoc.ItemData(CboTipoDoc.ListIndex) & "," & TxtNDoc & ",'" & txtRutProveedor & "','" & TxtRsocial & "'," & CDbl(txtPagar) & "," & _
               "'" & Fql(DtFecha.Value) & "','INGRESO BH'," & IG_Mes_Contable & "," & IG_Ano_Contable & ",'" & Fql(DtFecha + CboPlazos.ItemData(CboPlazos.ListIndex)) & _
               "','" & SP_Rut_Activo & "','" & CboTipoDoc.Text & "'," & CboPlazos.ItemData(CboPlazos.ListIndex) & "," & _
               lacajita & "," & Val(Right(CboPlazos.Text, 5)) & "," & CDbl(txtRetencion) & "," & CDbl(txtTotal) & ")"
        cn.Execute Sql
        'Ahora grabar cuentas
        Sql = "INSERT INTO com_detalle_honorarios (com_id,pla_id,deh_debe,deh_haber) VALUES "
        With LvDetalle
            For i = 1 To .ListItems.Count
                Lp_ValorDebe = 0
                Lp_ValorHaber = 0
                
                Lp_ValorDebe = CDbl(.ListItems(i).SubItems(3))
                Lp_ValorHaber = CDbl(.ListItems(i).SubItems(4))
                
                Sql = Sql & "(" & Lp_IdCompra & "," & .ListItems(i).SubItems(1) & "," & Lp_ValorDebe & "," & Lp_ValorHaber & "),"
            Next
            Sql = Mid(Sql, 1, Len(Sql) - 1)
            cn.Execute Sql
        End With
        
            
        'Aqui realizar abono a la cta cte
        '8 Octubre 2011
        If Mid(CboPlazos.Text, 1, 7) = "CONTADO" Then
                Dim Lp_Nro_Comprobante As Long
                Dim Lp_IdAbo As Long
                Lp_Nro_Comprobante = AutoIncremento("lee", 200, , IG_id_Sucursal_Empresa)
            
                
                Bp_Comprobante_Pago = True
                Lp_IdAbo = UltimoNro("cta_abonos", "abo_id")
                PagoDocumento Lp_IdAbo, "PRO", Me.txtRutProveedor, DtFecha, CDbl(txtPagar), IIf(Mid(CboPlazos.Text, 1, 7) = "CONTADO", 1, Right(CboPlazos.ItemData(CboPlazos.ListIndex), 5)), "CONTADO", "PAGADO", LogUsuario, Lp_Nro_Comprobante, 0, "BH"
                Sql = "INSERT INTO cta_abono_documentos (abo_id,id,ctd_monto,rut_emp) VALUES "
                Sql = Sql & "(" & Lp_IdAbo & "," & Lp_IdCompra & "," & CDbl(txtPagar) & ",'" & SP_Rut_Activo & "')"
                cn.Execute Sql
                
                AutoIncremento "GUARDA", 200, Lp_Nro_Comprobante, IG_id_Sucursal_Empresa
        End If
        
    cn.CommitTrans
    If MsgBox("Documento guardado correctamente..." & vbNewLine & " �Ingresar otro ? ...", vbQuestion + vbYesNo) = vbNo Then
        Unload Me
    Else
        Limpia
    End If
    
    Exit Sub
ErrorBH:
    MsgBox "Problema al grabar Documento..." & vbNewLine & Err.Number & vbNewLine & Err.Description, vbExclamation + vbOKOnly
    cn.RollbackTrans
    
End Sub
Private Sub Limpia()
    TxtNDoc = 0
    txtRutProveedor = ""
    LvDetalle.ListItems.Clear
    Me.TxtRsocial = ""
    TxtCodigoCuenta = ""
    Me.TxtNombre = ""
    TxtDebe = "0"
    TxtHaber = "0"
    DtFecha.SetFocus
End Sub

Private Sub CmdNuevoProveedor_Click()
    SG_codigo = Empty
    AgregoProveedor.Show 1
    If SG_codigo <> Empty Then txtRutProveedor = SG_codigo
End Sub

Private Sub CmdOk_Click()
    Dim Ip_C As Integer, Bp_corregir As Boolean
    If Val(TxtCodigoCuenta) = 0 Then Exit Sub
    
    If Val(TxtCredito) > 0 And Val(TxtDebito) > 0 Then
        MsgBox "No puede tener Debitos y Creditos en una misma cuenta...", vbInformation
        TxtCredito.SetFocus
        Exit Sub
    End If
    
    
    If Val(TxtCredito) = 0 And Val(TxtDebito) = 0 Then
        MsgBox "Debe ingresar valores...", vbInformation
        TxtCredito.SetFocus
        Exit Sub
    End If
       
    
    Bp_corregir = False
    For i = 1 To LvDetalle.ListItems.Count
        If Val(LvDetalle.ListItems(i).SubItems(1)) = TxtCodigoCuenta Then
                If MsgBox("Cuenta ya ingresada..." & vbNewLine & " �Corregir?..", vbQuestion + vbYesNo) = vbNo Then Exit Sub
                Bp_corregir = True
                Ip_C = i
        End If
    Next
    
    If Not Bp_corregir Then
        LvDetalle.ListItems.Add , , tXTNuevo
        Ip_C = LvDetalle.ListItems.Count
    End If
    LvDetalle.ListItems(Ip_C).SubItems(1) = TxtCodigoCuenta
    LvDetalle.ListItems(Ip_C).SubItems(2) = TxtNombre

    LvDetalle.ListItems(Ip_C).SubItems(3) = NumFormat(TxtDebito)
    LvDetalle.ListItems(Ip_C).SubItems(4) = NumFormat(TxtCredito)
    
    TxtCodigoCuenta = ""
    TxtNombre = ""
    TxtCredito = 0
    TxtDebito = 0
 
    Tvoucher
 
    TxtCodigoCuenta.SetFocus
End Sub
Private Sub Tvoucher()
    TxtDebe = NumFormat(TotalizaColumna(LvDetalle, "debe"))
    TxtHaber = NumFormat(TotalizaColumna(LvDetalle, "haber"))
End Sub


Private Sub CmdRetorna_Click()
    Unload Me
End Sub

Private Sub DtFecha_LostFocus()
    Dim Dp_Fecha As Date
    Dp_Fecha = UltimoDiaMes("01-" & Right("00" & Trim(Str(IG_Mes_Contable)), 2) & "-" & IG_Ano_Contable)
         
    If DtFecha > Dp_Fecha Then
            MsgBox "No puede ingresar documentos con FECHA posterior al periodo contable...", vbInformation
            DtFecha.SetFocus
            Exit Sub
    End If
End Sub

Private Sub Form_Load()
    Aplicar_skin Me
    Centrar Me
    'LLenarCombo CboPlazos, "CONCAT(pla_nombre,'                            ',CAST(pla_id AS CHAR ))", "pla_dias", "par_plazos_vencimiento", "pla_activo='SI'", "pla_id"
    'CboPlazos.ListIndex = 0
    TxtMesContable = UCase(MonthName(IG_Mes_Contable))
    TxtAnoContable = IG_Ano_Contable
    LLenarCombo CboTipoDoc, "doc_nombre", "doc_id", "sis_documentos", "doc_documento='COMPRA' AND doc_activo='SI' AND doc_honorarios='SI'", "doc_orden"
    DtFecha = Date
    
    LLenarCombo CboPlazos, "CONCAT(pla_nombre,'                            ',CAST(pla_id AS CHAR ))", "pla_dias", "par_plazos_vencimiento", "pla_activo='SI'", "pla_id"
    CboPlazos.AddItem "FONDOS POR RENDIR"
    CboPlazos.ItemData(CboPlazos.ListCount - 1) = 9999
    CboPlazos.AddItem "ANTICIPO DE PROVEEDOR"
    CboPlazos.ItemData(CboPlazos.ListCount - 1) = 4444
    CboPlazos.ListIndex = 0
End Sub


Private Sub LvDetalle_DblClick()
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    TxtCodigoCuenta = LvDetalle.SelectedItem.SubItems(1)
    TxtNombre = LvDetalle.SelectedItem.SubItems(2)
    TxtDebito = CDbl(LvDetalle.SelectedItem.SubItems(3))
    TxtCredito = CDbl(LvDetalle.SelectedItem.SubItems(4))
    LvDetalle.ListItems.Remove LvDetalle.SelectedItem.Index
End Sub

Private Sub Timer1_Timer()
    On Error Resume Next
    DtFecha.SetFocus
    Timer1.Enabled = False
End Sub

Private Sub TxtCodigoCuenta_GotFocus()
    En_Foco TxtCodigoCuenta
End Sub

Private Sub TxtCodigoCuenta_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
End Sub

Private Sub TxtCodigoCuenta_Validate(Cancel As Boolean)
    If Val(TxtCodigoCuenta) = 0 Then Exit Sub
    Sql = "SELECT pla_nombre,tpo_id,det_id,pla_analisis " & _
            "FROM con_plan_de_cuentas " & _
            "WHERE pla_id=" & TxtCodigoCuenta
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        TxtNombre = RsTmp!pla_nombre
    Else
        MsgBox "Cuenta no encontrada...", vbInformation
        Cancel = True
    End If
End Sub
Private Sub TxtCredito_GotFocus()
    En_Foco Me.ActiveControl
End Sub
Private Sub TxtCredito_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
End Sub

Private Sub TxtDebito_GotFocus()
    En_Foco Me.ActiveControl
End Sub

Private Sub TxtDebito_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
End Sub

Private Sub TxtNDocGotFocus()
    En_Foco TxtNDoc
End Sub

Private Sub TxtNDoc_GotFocus()
    En_Foco TxtNDoc
End Sub

Private Sub TxtNDoc_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
End Sub

Private Sub txtPagar_GotFocus()
    En_Foco txtPagar
End Sub

Private Sub txtPagar_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
End Sub

Private Sub txtPagar_Validate(Cancel As Boolean)
    If Val(txtPagar) = 0 Then txtPagar = "0" Else txtPagar = NumFormat(txtPagar)
End Sub

Private Sub txtRetencion_GotFocus()
    En_Foco txtRetencion
End Sub

Private Sub txtRetencion_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
End Sub

Private Sub txtRetencion_Validate(Cancel As Boolean)
        If Val(txtRetencion) = 0 Then txtRetencion = "0" Else txtRetencion = NumFormat(txtRetencion)
End Sub

Private Sub TxtRutProveedor_GotFocus()
    On Error Resume Next
    En_Foco Me.ActiveControl
End Sub

Private Sub TxtRutProveedor_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then txtRutProveedor_Validate True
End Sub

Private Sub txtRutProveedor_Validate(Cancel As Boolean)
    If Len(txtRutProveedor) > 0 Then
        Sql = "SELECT nombre_empresa,rut_emp " & _
              "FROM maestro_proveedores " & _
              "WHERE rut_proveedor='" & txtRutProveedor & "' AND habilitado='SI' "
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
            Sql = "SELECT rut_emp " & _
                  "FROM par_asociacion_ruts " & _
                  "WHERE rut_emp='" & SP_Rut_Activo & "' AND rut_pro='" & txtRutProveedor & "'"
            Consulta RsTmp2, Sql
            If RsTmp2.RecordCount = 0 Then
                Sql = "INSERT INTO par_asociacion_ruts (rut_emp,rut_pro) " & _
                      "VALUES('" & SP_Rut_Activo & "','" & txtRutProveedor & "')"
                cn.Execute Sql
            End If
        
            TxtRsocial = RsTmp!nombre_empresa
            
            If txtRutProveedor = Empty Then Exit Sub
        Else
            MsgBox "Proveedor no encontrado... ", vbOKOnly + vbInformation
            TxtRsocial = ""
            
            Exit Sub
        End If
    End If
End Sub
Private Sub txtTotal_GotFocus()
    En_Foco txtTotal
End Sub

Private Sub txtTotal_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
End Sub

Private Sub txtTotal_Validate(Cancel As Boolean)
    If Val(txtTotal) > 0 Then
        txtTotal = NumFormat(txtTotal)
        txtRetencion = NumFormat(CDbl(txtTotal) / 100 * 10)
        txtPagar = CDbl(txtTotal) - CDbl(txtRetencion)
    Else: txtTotal = "0"
    End If
End Sub
