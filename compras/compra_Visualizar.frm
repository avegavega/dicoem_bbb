VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{28D47522-CF84-11D1-834C-00A0249F0C28}#1.0#0"; "gif89.dll"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{DA729E34-689F-49EA-A856-B57046630B73}#1.0#0"; "progressbar-xp.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form compra_Visualizar 
   Caption         =   "Visualizar Compra"
   ClientHeight    =   10140
   ClientLeft      =   2640
   ClientTop       =   360
   ClientWidth     =   15765
   LinkTopic       =   "Form1"
   ScaleHeight     =   10140
   ScaleWidth      =   15765
   Begin VB.CommandButton CmdRecargoFlete 
      Caption         =   "Recargo por flete"
      Height          =   345
      Left            =   9510
      TabIndex        =   96
      Top             =   9825
      Width           =   2910
   End
   Begin VB.TextBox TxtRecargoFlete 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   8205
      Locked          =   -1  'True
      TabIndex        =   95
      Tag             =   "T"
      ToolTipText     =   "% de recargo para esta factura por flete"
      Top             =   9825
      Width           =   1275
   End
   Begin VB.CommandButton CmdGeneraEtiquetas 
      Caption         =   "Generar Archivo para Etiquetas"
      Height          =   285
      Left            =   4170
      TabIndex        =   89
      Top             =   9795
      Width           =   2550
   End
   Begin VB.Frame FrmLoad 
      Height          =   1740
      Left            =   6030
      TabIndex        =   84
      Top             =   3585
      Width           =   2835
      Begin GIF89LibCtl.Gif89a Gif89a1 
         Height          =   1275
         Left            =   210
         OleObjectBlob   =   "compra_Visualizar.frx":0000
         TabIndex        =   85
         Top             =   285
         Width           =   2445
      End
   End
   Begin VB.Frame FraProgreso 
      Caption         =   "Progreso exportaci�n"
      Height          =   795
      Left            =   1890
      TabIndex        =   80
      Top             =   6435
      Visible         =   0   'False
      Width           =   11430
      Begin Proyecto2.XP_ProgressBar BarraProgreso 
         Height          =   330
         Left            =   150
         TabIndex        =   81
         Top             =   285
         Width           =   11130
         _ExtentX        =   19632
         _ExtentY        =   582
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BrushStyle      =   0
         Color           =   16777088
         Scrolling       =   1
         ShowText        =   -1  'True
      End
   End
   Begin VB.CommandButton CmdExcel 
      Caption         =   "Excel"
      Height          =   300
      Left            =   2190
      TabIndex        =   79
      Top             =   9780
      Width           =   1860
   End
   Begin VB.CommandButton CmdImprimeDetalle 
      Caption         =   "Imprimir Detalle"
      Height          =   285
      Left            =   135
      TabIndex        =   78
      Top             =   9780
      Width           =   1965
   End
   Begin VB.TextBox txtRetiradoPor 
      BackColor       =   &H8000000F&
      Height          =   285
      Left            =   9870
      Locked          =   -1  'True
      TabIndex        =   70
      TabStop         =   0   'False
      Top             =   9345
      Width           =   1875
   End
   Begin VB.TextBox txtAutorizadoPor 
      BackColor       =   &H8000000F&
      Height          =   285
      Left            =   6960
      Locked          =   -1  'True
      TabIndex        =   68
      TabStop         =   0   'False
      Top             =   9345
      Width           =   1920
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel20 
      Height          =   195
      Index           =   0
      Left            =   2415
      OleObjectBlob   =   "compra_Visualizar.frx":0086
      TabIndex        =   67
      Top             =   9345
      Width           =   1140
   End
   Begin VB.TextBox TxtSolicitadoPor 
      BackColor       =   &H8000000F&
      Height          =   285
      Left            =   3555
      Locked          =   -1  'True
      TabIndex        =   66
      TabStop         =   0   'False
      Top             =   9345
      Width           =   2175
   End
   Begin MSComDlg.CommonDialog Dialogo 
      Left            =   15165
      Top             =   8460
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.CommandButton CmdImprimeOC 
      Caption         =   "Imprime Orden Compra"
      Height          =   285
      Left            =   150
      TabIndex        =   65
      Top             =   9345
      Visible         =   0   'False
      Width           =   1965
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   705
      OleObjectBlob   =   "compra_Visualizar.frx":0102
      Top             =   7590
   End
   Begin VB.Timer Timer1 
      Interval        =   20
      Left            =   135
      Top             =   7530
   End
   Begin VB.CommandButton CmdSalir 
      Cancel          =   -1  'True
      Caption         =   "&Salir"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   465
      Left            =   13695
      TabIndex        =   38
      ToolTipText     =   "Salir"
      Top             =   9375
      Width           =   1470
   End
   Begin VB.Frame Frame1 
      Caption         =   " Documento refencia"
      Height          =   5505
      Left            =   120
      TabIndex        =   36
      ToolTipText     =   "Solo si es nota de credito/debito"
      Top             =   3780
      Width           =   15360
      Begin VB.ComboBox CboGuias 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         ItemData        =   "compra_Visualizar.frx":0336
         Left            =   9555
         List            =   "compra_Visualizar.frx":0338
         Locked          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   61
         ToolTipText     =   $"compra_Visualizar.frx":033A
         Top             =   180
         Width           =   5025
      End
      Begin VB.Frame FrameDatos 
         Caption         =   "Datos Contables"
         Height          =   870
         Left            =   210
         TabIndex        =   43
         Top             =   450
         Width           =   14400
         Begin VB.CommandButton CmdModifica 
            Caption         =   "Modificar"
            Height          =   285
            Left            =   13110
            TabIndex        =   63
            ToolTipText     =   "Modifica datos contables de la linea"
            Top             =   480
            Width           =   1170
         End
         Begin VB.ComboBox CboCentroCosto 
            Height          =   315
            Left            =   6825
            Style           =   2  'Dropdown List
            TabIndex        =   47
            Top             =   480
            Width           =   3150
         End
         Begin VB.ComboBox CboItemGasto 
            Height          =   315
            Left            =   9960
            Style           =   2  'Dropdown List
            TabIndex        =   46
            Top             =   480
            Width           =   3150
         End
         Begin VB.ComboBox CboCuenta 
            Height          =   315
            Left            =   525
            Style           =   2  'Dropdown List
            TabIndex        =   45
            Top             =   480
            Width           =   3150
         End
         Begin VB.ComboBox CboArea 
            Height          =   315
            Left            =   3690
            Style           =   2  'Dropdown List
            TabIndex        =   44
            Top             =   480
            Width           =   3150
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkItem 
            Height          =   180
            Left            =   10005
            OleObjectBlob   =   "compra_Visualizar.frx":03CF
            TabIndex        =   48
            Top             =   285
            Width           =   2055
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkCC 
            Height          =   180
            Left            =   6810
            OleObjectBlob   =   "compra_Visualizar.frx":0447
            TabIndex        =   49
            Top             =   285
            Width           =   2055
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkArea 
            Height          =   180
            Left            =   3735
            OleObjectBlob   =   "compra_Visualizar.frx":04C3
            TabIndex        =   50
            Top             =   285
            Width           =   2055
         End
         Begin MSComctlLib.ListView LvSinDetalle 
            Height          =   1740
            Left            =   90
            TabIndex        =   51
            Top             =   885
            Width           =   14475
            _ExtentX        =   25532
            _ExtentY        =   3069
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   0   'False
            HideSelection   =   0   'False
            FullRowSelect   =   -1  'True
            GridLines       =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   0
            NumItems        =   13
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Object.Tag             =   "N100"
               Text            =   "id"
               Object.Width           =   0
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Object.Tag             =   "T1000"
               Text            =   "Cuenta"
               Object.Width           =   4410
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   2
               Object.Tag             =   "T1500"
               Text            =   "Area"
               Object.Width           =   4410
            EndProperty
            BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   3
               Object.Tag             =   "T3000"
               Text            =   "Centro de Costo"
               Object.Width           =   4410
            EndProperty
            BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   4
               Object.Tag             =   "T1000"
               Text            =   "Items de Gasto"
               Object.Width           =   4410
            EndProperty
            BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   5
               Key             =   "otros"
               Object.Tag             =   "N100"
               Text            =   "Otros Impuestos"
               Object.Width           =   1852
            EndProperty
            BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   6
               Key             =   "neto"
               Object.Tag             =   "N100"
               Text            =   "Neto"
               Object.Width           =   1852
            EndProperty
            BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   7
               Key             =   "iva"
               Object.Tag             =   "N100"
               Text            =   "IVA"
               Object.Width           =   1852
            EndProperty
            BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   8
               Key             =   "retenido"
               Object.Tag             =   "N100"
               Text            =   "Retenido"
               Object.Width           =   1852
            EndProperty
            BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   9
               Object.Tag             =   "N109"
               Text            =   "PLA_ID"
               Object.Width           =   0
            EndProperty
            BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   10
               Object.Tag             =   "N109"
               Text            =   "ARE_ID"
               Object.Width           =   0
            EndProperty
            BeginProperty ColumnHeader(12) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   11
               Object.Tag             =   "N109"
               Text            =   "CEN_ID"
               Object.Width           =   0
            EndProperty
            BeginProperty ColumnHeader(13) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   12
               Object.Tag             =   "N109"
               Text            =   "ITEM GASTO ID"
               Object.Width           =   0
            EndProperty
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel5 
            Height          =   180
            Left            =   525
            OleObjectBlob   =   "compra_Visualizar.frx":0529
            TabIndex        =   52
            Top             =   300
            Width           =   2055
         End
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkNetoBrutos 
         Height          =   270
         Left            =   195
         OleObjectBlob   =   "compra_Visualizar.frx":0593
         TabIndex        =   37
         Top             =   5205
         Width           =   14670
      End
      Begin MSComctlLib.ListView LvDetalle 
         Height          =   3765
         Left            =   210
         TabIndex        =   42
         Top             =   1365
         Width           =   14385
         _ExtentX        =   25374
         _ExtentY        =   6641
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   0   'False
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   25
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "N109"
            Text            =   "Nro Orden"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "T1000"
            Text            =   "Codigo"
            Object.Width           =   2328
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "T3000"
            Text            =   "Descripcion"
            Object.Width           =   6703
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Object.Tag             =   "N102"
            Text            =   "Tipo"
            Object.Width           =   3881
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   4
            Object.Tag             =   "N102"
            Text            =   "Precio"
            Object.Width           =   2646
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   5
            Object.Tag             =   "N102"
            Text            =   "Cant."
            Object.Width           =   2293
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   6
            Object.Tag             =   "T1000"
            Text            =   "U.Medida"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   7
            Key             =   "total"
            Object.Tag             =   "N100"
            Text            =   "Total Neto"
            Object.Width           =   2646
         EndProperty
         BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   8
            Object.Tag             =   "N109"
            Text            =   "Cuenta"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   9
            Object.Tag             =   "N109"
            Text            =   "CentroCosto"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   10
            Object.Tag             =   "N109"
            Text            =   "gasid"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(12) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   11
            Object.Tag             =   "N109"
            Text            =   "Area"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(13) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   12
            Object.Tag             =   "N102"
            Text            =   "Factor Dscto"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(14) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   13
            Object.Tag             =   "N102"
            Text            =   "Valor Dscto"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(15) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   14
            Object.Tag             =   "N102"
            Text            =   "Unitario Con Dscto"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(16) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   15
            Object.Tag             =   "N100"
            Text            =   "Total Linea C/dscto"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(17) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   16
            Object.Tag             =   "N102"
            Text            =   "CostoLinea"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(18) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   17
            Object.Tag             =   "N102"
            Text            =   "Unitario/Final"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(19) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   2
            SubItemIndex    =   18
            Object.Tag             =   "T500"
            Text            =   "Inventario"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(20) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   19
            Object.Tag             =   "T10000"
            Text            =   "Codigo Empresa"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(21) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   20
            Object.Tag             =   "N109"
            Text            =   "AIM ID"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(22) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   21
            Object.Tag             =   "T1000"
            Text            =   "Codigo Proveedor"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(23) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   22
            Object.Tag             =   "T1000"
            Text            =   "Facturada"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(24) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   23
            Object.Tag             =   "T1000"
            Text            =   "Ubicacion"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(25) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   24
            Text            =   "Recargo"
            Object.Width           =   2540
         EndProperty
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel17 
         Height          =   180
         Left            =   7440
         OleObjectBlob   =   "compra_Visualizar.frx":063D
         TabIndex        =   62
         Top             =   240
         Width           =   2055
      End
   End
   Begin VB.Frame FrmProveedor 
      Caption         =   "Datos del documento"
      Height          =   3750
      Left            =   105
      TabIndex        =   0
      Top             =   75
      Width           =   15360
      Begin VB.TextBox TxtTotalImpuestos 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFC0&
         BeginProperty Font 
            Name            =   "Arial Narrow"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00808080&
         Height          =   330
         Left            =   11865
         Locked          =   -1  'True
         TabIndex        =   94
         TabStop         =   0   'False
         Text            =   "0"
         Top             =   1575
         Width           =   915
      End
      Begin VB.TextBox TxtValorImp 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         Height          =   345
         Left            =   11835
         TabIndex        =   92
         Tag             =   "N"
         Text            =   "0"
         Top             =   240
         Visible         =   0   'False
         Width           =   990
      End
      Begin VB.CommandButton CmdOkImp 
         Caption         =   "Ok"
         Height          =   345
         Left            =   12840
         TabIndex        =   91
         Top             =   225
         Visible         =   0   'False
         Width           =   345
      End
      Begin VB.ComboBox CboImpuestos 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         ItemData        =   "compra_Visualizar.frx":06BB
         Left            =   8955
         List            =   "compra_Visualizar.frx":06BD
         Style           =   2  'Dropdown List
         TabIndex        =   90
         Top             =   240
         Visible         =   0   'False
         Width           =   2895
      End
      Begin VB.Frame Frame2 
         Caption         =   "Modificar Datos"
         Height          =   630
         Left            =   8985
         TabIndex        =   86
         Top             =   3075
         Width           =   6105
         Begin VB.CommandButton CmdGuarda 
            Caption         =   "Guardar Cambios"
            Enabled         =   0   'False
            Height          =   300
            Left            =   3510
            TabIndex        =   88
            Top             =   210
            Width           =   1530
         End
         Begin VB.CommandButton CmdEdita 
            Caption         =   "Editar Datos"
            Height          =   300
            Left            =   1860
            TabIndex        =   87
            Top             =   210
            Width           =   1530
         End
      End
      Begin VB.TextBox TxtGlosa 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2175
         Locked          =   -1  'True
         TabIndex        =   83
         Tag             =   "T"
         Top             =   2985
         Width           =   4215
      End
      Begin VB.TextBox txtMargenDist 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   7560
         Locked          =   -1  'True
         TabIndex        =   76
         TabStop         =   0   'False
         Text            =   "0"
         Top             =   2955
         Width           =   1290
      End
      Begin VB.TextBox TxtIVAsinCredito 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   7560
         Locked          =   -1  'True
         TabIndex        =   73
         TabStop         =   0   'False
         Text            =   "0"
         Top             =   1995
         Width           =   1290
      End
      Begin VB.TextBox txtIvaMargen 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   7560
         Locked          =   -1  'True
         TabIndex        =   72
         TabStop         =   0   'False
         Text            =   "0"
         Top             =   2310
         Width           =   1290
      End
      Begin VB.CommandButton CmdFecha 
         Caption         =   "Fecha"
         Height          =   240
         Left            =   3450
         TabIndex        =   64
         ToolTipText     =   "Cambia fecha del documento"
         Top             =   315
         Width           =   615
      End
      Begin VB.TextBox TxtPlazo 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   4545
         Locked          =   -1  'True
         TabIndex        =   59
         Tag             =   "T"
         Top             =   600
         Width           =   1275
      End
      Begin VB.TextBox TxtFolio 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   4545
         Locked          =   -1  'True
         TabIndex        =   57
         Tag             =   "T"
         Top             =   270
         Width           =   1275
      End
      Begin VB.TextBox TxtFpago 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2175
         Locked          =   -1  'True
         TabIndex        =   55
         Tag             =   "T"
         Top             =   2670
         Width           =   1275
      End
      Begin VB.TextBox DtVence 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2175
         Locked          =   -1  'True
         TabIndex        =   54
         Tag             =   "T"
         Top             =   630
         Width           =   1275
      End
      Begin VB.TextBox DtFecha 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2175
         Locked          =   -1  'True
         TabIndex        =   53
         Tag             =   "T"
         Top             =   300
         Width           =   1275
      End
      Begin VB.ComboBox CboBodega 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         ItemData        =   "compra_Visualizar.frx":06BF
         Left            =   2175
         List            =   "compra_Visualizar.frx":06C1
         Locked          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   40
         ToolTipText     =   "Seleccione Bodega"
         Top             =   2325
         Width           =   2805
      End
      Begin VB.ComboBox CboTipoDoc 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         ItemData        =   "compra_Visualizar.frx":06C3
         Left            =   2175
         List            =   "compra_Visualizar.frx":06D0
         Locked          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   21
         ToolTipText     =   $"compra_Visualizar.frx":06F5
         Top             =   1635
         Width           =   4155
      End
      Begin VB.TextBox TxtNDoc 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2175
         Locked          =   -1  'True
         TabIndex        =   20
         Tag             =   "N"
         Text            =   "0"
         Top             =   1995
         Width           =   1290
      End
      Begin VB.TextBox TxtRsocial 
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2175
         Locked          =   -1  'True
         TabIndex        =   19
         TabStop         =   0   'False
         Top             =   1305
         Width           =   4155
      End
      Begin VB.TextBox TxtRutProveedor 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2175
         Locked          =   -1  'True
         TabIndex        =   18
         Tag             =   "T"
         Top             =   975
         Width           =   1275
      End
      Begin VB.Frame FrmPeriodo 
         Caption         =   "Periodo Contable"
         Height          =   1425
         Left            =   13230
         TabIndex        =   15
         Top             =   105
         Width           =   1845
         Begin VB.TextBox TxtMesContable 
            Alignment       =   2  'Center
            BackColor       =   &H8000000F&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   285
            Locked          =   -1  'True
            TabIndex        =   17
            TabStop         =   0   'False
            Text            =   "0"
            Top             =   435
            Width           =   1275
         End
         Begin VB.TextBox TxtAnoContable 
            Alignment       =   2  'Center
            BackColor       =   &H8000000F&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   750
            Locked          =   -1  'True
            TabIndex        =   16
            TabStop         =   0   'False
            Text            =   "0"
            Top             =   840
            Width           =   780
         End
      End
      Begin VB.TextBox txtTotal 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   11625
         Locked          =   -1  'True
         TabIndex        =   14
         TabStop         =   0   'False
         Text            =   "0"
         Top             =   1950
         Width           =   1485
      End
      Begin VB.TextBox txtSubNeto 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   7560
         Locked          =   -1  'True
         TabIndex        =   13
         Text            =   "0"
         Top             =   465
         Width           =   1290
      End
      Begin VB.TextBox txtIVA 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   7560
         Locked          =   -1  'True
         TabIndex        =   12
         TabStop         =   0   'False
         Text            =   "0"
         Top             =   1395
         Width           =   1290
      End
      Begin VB.TextBox txtIvaRetenido 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         Enabled         =   0   'False
         Height          =   285
         Left            =   7560
         Locked          =   -1  'True
         TabIndex        =   11
         Tag             =   "N"
         Text            =   "0"
         Top             =   1695
         Width           =   1290
      End
      Begin VB.TextBox TxtExento 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         Height          =   300
         Left            =   7560
         Locked          =   -1  'True
         TabIndex        =   10
         Tag             =   "N"
         Text            =   "0"
         Top             =   165
         Width           =   1290
      End
      Begin VB.TextBox TxtDescuentoNeto 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   7560
         Locked          =   -1  'True
         TabIndex        =   9
         Text            =   "0"
         Top             =   780
         Width           =   1290
      End
      Begin VB.TextBox TxtNeto 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   7560
         Locked          =   -1  'True
         TabIndex        =   8
         TabStop         =   0   'False
         Text            =   "0"
         Top             =   1080
         Width           =   1290
      End
      Begin VB.TextBox TxtSubTotal 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   7560
         Locked          =   -1  'True
         TabIndex        =   7
         TabStop         =   0   'False
         Text            =   "0"
         Top             =   2625
         Width           =   1290
      End
      Begin VB.Frame FrameRef 
         Caption         =   " Documento refencia"
         Height          =   735
         Left            =   9000
         TabIndex        =   1
         ToolTipText     =   "Solo si es nota de credito/debito"
         Top             =   2280
         Visible         =   0   'False
         Width           =   6135
         Begin VB.TextBox TxtDocumentoRef 
            BackColor       =   &H00E0E0E0&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   210
            Locked          =   -1  'True
            TabIndex        =   4
            TabStop         =   0   'False
            Top             =   330
            Width           =   2505
         End
         Begin VB.TextBox TxtNroRef 
            BackColor       =   &H00E0E0E0&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   2730
            Locked          =   -1  'True
            TabIndex        =   3
            TabStop         =   0   'False
            Top             =   330
            Width           =   1620
         End
         Begin VB.TextBox TxtTotalRef 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00E0E0E0&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   4350
            Locked          =   -1  'True
            TabIndex        =   2
            TabStop         =   0   'False
            Top             =   330
            Width           =   1590
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel18 
            Height          =   240
            Left            =   2745
            OleObjectBlob   =   "compra_Visualizar.frx":078A
            TabIndex        =   5
            Top             =   120
            Width           =   1500
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel19 
            Height          =   240
            Left            =   4365
            OleObjectBlob   =   "compra_Visualizar.frx":07F4
            TabIndex        =   6
            Top             =   120
            Width           =   1500
         End
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel10 
         Height          =   225
         Left            =   1065
         OleObjectBlob   =   "compra_Visualizar.frx":085C
         TabIndex        =   22
         Top             =   1350
         Width           =   1020
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   225
         Index           =   0
         Left            =   600
         OleObjectBlob   =   "compra_Visualizar.frx":08BF
         TabIndex        =   23
         Top             =   2055
         Width           =   1485
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   225
         Index           =   0
         Left            =   510
         OleObjectBlob   =   "compra_Visualizar.frx":0922
         TabIndex        =   24
         Top             =   1695
         Width           =   1575
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   225
         Index           =   0
         Left            =   1020
         OleObjectBlob   =   "compra_Visualizar.frx":0995
         TabIndex        =   25
         Top             =   345
         Width           =   1065
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
         Height          =   225
         Left            =   585
         OleObjectBlob   =   "compra_Visualizar.frx":09F6
         TabIndex        =   26
         Top             =   1020
         Width           =   1500
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel8 
         Height          =   195
         Left            =   9435
         OleObjectBlob   =   "compra_Visualizar.frx":0A53
         TabIndex        =   27
         Top             =   1980
         Width           =   2145
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel6 
         Height          =   195
         Left            =   6015
         OleObjectBlob   =   "compra_Visualizar.frx":0AD7
         TabIndex        =   28
         Top             =   510
         Width           =   1455
      End
      Begin MSComctlLib.ListView LvImpuestos 
         Height          =   975
         Left            =   8955
         TabIndex        =   29
         Top             =   585
         Width           =   4230
         _ExtentX        =   7461
         _ExtentY        =   1720
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   5
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "N109"
            Text            =   "Id"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "T5000"
            Text            =   "Impuestos"
            Object.Width           =   5292
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   2
            Object.Tag             =   "N100"
            Text            =   "Valor"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Object.Tag             =   "N102"
            Text            =   "FACTOR"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Object.Tag             =   "T1000"
            Text            =   "costo/credito"
            Object.Width           =   0
         EndProperty
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel7 
         Height          =   195
         Index           =   0
         Left            =   6465
         OleObjectBlob   =   "compra_Visualizar.frx":0B51
         TabIndex        =   30
         Top             =   1440
         Width           =   1005
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel13 
         Height          =   195
         Left            =   6150
         OleObjectBlob   =   "compra_Visualizar.frx":0BBB
         TabIndex        =   31
         Top             =   1740
         Width           =   1305
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel11 
         Height          =   195
         Left            =   5985
         OleObjectBlob   =   "compra_Visualizar.frx":0C31
         TabIndex        =   32
         Top             =   210
         Width           =   1455
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel14 
         Height          =   195
         Left            =   6000
         OleObjectBlob   =   "compra_Visualizar.frx":0C9B
         TabIndex        =   33
         Top             =   825
         Width           =   1455
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel15 
         Height          =   195
         Index           =   0
         Left            =   6480
         OleObjectBlob   =   "compra_Visualizar.frx":0D0B
         TabIndex        =   34
         Top             =   1125
         Width           =   990
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel16 
         Height          =   195
         Left            =   6015
         OleObjectBlob   =   "compra_Visualizar.frx":0D71
         TabIndex        =   35
         Top             =   2670
         Width           =   1455
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   225
         Index           =   1
         Left            =   780
         OleObjectBlob   =   "compra_Visualizar.frx":0DE1
         TabIndex        =   39
         Top             =   675
         Width           =   1230
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   225
         Index           =   2
         Left            =   510
         OleObjectBlob   =   "compra_Visualizar.frx":0E4E
         TabIndex        =   41
         Top             =   2355
         Width           =   1575
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   225
         Index           =   3
         Left            =   660
         OleObjectBlob   =   "compra_Visualizar.frx":0EB1
         TabIndex        =   56
         Top             =   2685
         Width           =   1350
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel9 
         Height          =   225
         Left            =   3960
         OleObjectBlob   =   "compra_Visualizar.frx":0F22
         TabIndex        =   58
         Top             =   315
         Width           =   525
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel12 
         Height          =   225
         Left            =   3960
         OleObjectBlob   =   "compra_Visualizar.frx":0F83
         TabIndex        =   60
         Top             =   645
         Width           =   525
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel7 
         Height          =   195
         Index           =   1
         Left            =   6465
         OleObjectBlob   =   "compra_Visualizar.frx":0FE4
         TabIndex        =   74
         Top             =   2355
         Width           =   1005
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel15 
         Height          =   195
         Index           =   1
         Left            =   5835
         OleObjectBlob   =   "compra_Visualizar.frx":1056
         TabIndex        =   75
         Top             =   2040
         Width           =   1635
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel7 
         Height          =   195
         Index           =   2
         Left            =   6465
         OleObjectBlob   =   "compra_Visualizar.frx":10D2
         TabIndex        =   77
         Top             =   3000
         Width           =   1005
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   225
         Index           =   4
         Left            =   690
         OleObjectBlob   =   "compra_Visualizar.frx":1148
         TabIndex        =   82
         Top             =   3000
         Width           =   1350
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   240
         Index           =   1
         Left            =   9975
         OleObjectBlob   =   "compra_Visualizar.frx":11A9
         TabIndex        =   93
         Top             =   1605
         Width           =   1830
      End
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel20 
      Height          =   195
      Index           =   1
      Left            =   5820
      OleObjectBlob   =   "compra_Visualizar.frx":1225
      TabIndex        =   69
      Top             =   9345
      Width           =   1140
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel20 
      Height          =   195
      Index           =   2
      Left            =   8925
      OleObjectBlob   =   "compra_Visualizar.frx":12A1
      TabIndex        =   71
      Top             =   9345
      Width           =   1140
   End
End
Attribute VB_Name = "compra_Visualizar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public Lp_ID_Compra As Long
Dim Bm_Neto As Boolean, Sm_Factura_X_guias As String * 2
Dim Sm_CC As String

Dim Ip_Paginas As Integer

Dim P_Fecha As String * 22
Dim P_Documento As String * 40
Dim P_Numero As String * 11
Dim P_Vence As String * 10
Dim P_Venta As String * 12
Dim P_Pago As String * 12
Dim P_Saldo As String * 12
Dim Cy As Double, Cx As Double, Dp As Double, S_Item As String * 4
Dim Im_Paginas As Integer
 Dim Lp_NroOC As Long

Private Sub DTPicker1_CallbackKeyDown(ByVal KeyCode As Integer, ByVal Shift As Integer, ByVal CallbackField As String, CallbackDate As Date)

End Sub



Private Sub CmdEdita_Click()
    txtRetiradoPor.Locked = False
    TxtExento.Locked = False
    txtSubNeto.Locked = False
    TxtDescuentoNeto.Locked = False
    TxtNeto.Locked = False
    txtIVA.Locked = False
    txtIvaRetenido.Locked = False
    TxtIVAsinCredito.Locked = False
    txtIvaMargen.Locked = False
    TxtSubTotal.Locked = False
    txtMargenDist.Locked = False
    TxtGlosa.Locked = False
    
    CboImpuestos.Visible = True
    TxtValorImp.Visible = True
    CmdOkImp.Visible = True
    CmdGuarda.Enabled = True
    
End Sub

Private Sub CmdExcel_Click()
    Dim tit(2) As String
    If LvDetalle.ListItems.Count = 0 Then Exit Sub
    FraProgreso.Visible = True
    tit(0) = CboTipoDoc.Text
    tit(1) = TxtNDoc
    tit(2) = DtFecha
    ExportarNuevo LvDetalle, tit, Me, BarraProgreso
    FraProgreso.Visible = False
End Sub

Private Sub CmdFecha_Click()
    Compra_CambiaFecha.DtFecha = Me.DtFecha
    Compra_CambiaFecha.Show 1
    If SG_codigo2 = Empty Then Exit Sub
    
    Sql = "UPDATE com_doc_compra SET fecha=" & SG_codigo2 & " " & _
          "WHERE id=" & Lp_ID_Compra
    
    cn.Execute Sql
    
End Sub

Private Sub CmdGeneraEtiquetas_Click()
    Dim Sp_NombreArchivo As String
    Dim Ip_Cant As Integer
    If LvDetalle.ListItems.Count = 0 Then Exit Sub
    
   ' On Error GoTo ErrorCancela
' Dialogo.ShowSave
 '    Sp_NombreArchivo = Dialogo.FileName
    
    FrmLoad.Visible = True
    DoEvents
    
    X = FreeFile
    On Error GoTo ErrorCreaLibro
    
    
    
    Sp_NombreArchivo = "c:\facturaelectronica\etiquetas.csv"  ' Dialogo.FileName      ' "c:\facturaelectronica\libros\" & Sm_CompraVenta & "_" & Sm_Mes_Contable & "-" & Sm_Ano_Contable & "_" & Replace(SP_Rut_Activo, ".", "") & ".CSV"
    Open Sp_NombreArchivo For Output As X
        If SP_Rut_Activo = "76.553.302-3" Then
                With LvDetalle 'kyr
                    Print #X, "detalle;codigo;ubicaci�n"
                    For i = 1 To .ListItems.Count
                    
                        Ip_Cant = .ListItems(i).SubItems(5)
                        
                     
                        For p = 1 To Ip_Cant
                        'Descripcion;cantidad,,ubicacion
                            Print #X, Mid(.ListItems(i).SubItems(2), 1, 25) & ";" & .ListItems(i).SubItems(1) & ";" & .ListItems(i).SubItems(23)
                        Next
                
                    Next
                End With
        ElseIf SP_Rut_Activo = "76.063.757-2" Or SP_Rut_Activo = "10.966.790-0" Then
                With LvDetalle
                    Print #X, "detalle;codigo;proveedor;ubicaci�n;revisado"
                    For i = 1 To .ListItems.Count
                        Ip_Cant = .ListItems(i).SubItems(5)
                     '   If Ip_Cant Mod 2 <> 0 Then Ip_Cant = Ip_Cant + 1
                      '  Ip_Cant = Ip_Cant / 2
                        For p = 1 To Ip_Cant
                        'Descripcion;cantidad,codempesa,codproveedor,ubicacion
                            Print #X, Mid(.ListItems(i).SubItems(2), 1, 25) & ";" & .ListItems(i).SubItems(1) & ";" & .ListItems(i).SubItems(19) & ";" & .ListItems(i).SubItems(23) & ";" & LogUsuario
                        Next
                    Next
                End With
                
         Else
                With LvDetalle
                    Print #X, "detalle;codigo;proveedor;ubicaci�n;revisado"
                    For i = 1 To .ListItems.Count
                    
                        Ip_Cant = .ListItems(i).SubItems(5)
                        
                        If Ip_Cant Mod 2 <> 0 Then Ip_Cant = Ip_Cant + 1
                        
                        Ip_Cant = Ip_Cant / 2
                        For p = 1 To Ip_Cant
                        'Descripcion;cantidad,codempesa,codproveedor,ubicacion
                            Print #X, Mid(.ListItems(i).SubItems(2), 1, 25) & ";" & .ListItems(i).SubItems(19) & ";" & .ListItems(i).SubItems(21) & ";" & .ListItems(i).SubItems(23) & ";" & LogUsuario
                        Next
                
                    Next
                End With
         End If
    Close #X
    On Error Resume Next
    MsgBox "Se genero: " & Sp_NombreArchivo, vbInformation
    FrmLoad.Visible = False
    
ejecutarShell = Shell("rundll32.exe url.dll,FileProtocolHandler(" & Sp_NombreArchivo & ")", vbMaximizedFocus)

    
    
    
    Exit Sub
    
    
ErrorCreaLibro:
    '
    MsgBox "No se genero el archivo...", vbInformation
End Sub

Private Sub CmdGuarda_Click()
    If MsgBox("Guardar los cambios...", vbOKCancel + vbQuestion) = vbCancel Then Exit Sub
    
    Sql = "UPDATE com_doc_compra SET neto=" & CDbl(TxtNeto) & ",iva=" & CDbl(txtIVA) & ",total=" & CDbl(Me.txtTotal) & ",com_glosa='" & TxtGlosa & "',com_retirado_por='" & txtRetiradoPor & "' " & _
            "WHERE id= " & Lp_ID_Compra
    cn.Execute Sql
    Sql = "DELETE FROM com_impuestos " & _
            "WHERE id=" & Lp_ID_Compra
   cn.Execute Sql
    If LvImpuestos.ListItems.Count > 0 Then
        For i = 1 To LvImpuestos.ListItems.Count
            Sql = "INSERT INTO com_impuestos (id,imp_id,coi_valor) " & _
                        "VALUES(" & Lp_ID_Compra & "," & LvImpuestos.ListItems(i) & "," & CDbl(LvImpuestos.ListItems(i).SubItems(2)) & ")"
            cn.Execute Sql
        Next
    End If
   
    Exit Sub

ErrorUpdate:

    MsgBox "No se pudo actualizar...", vbInformation
    


End Sub

Private Sub CmdImprimeDetalle_Click()
        On Error GoTo ImpresoraError1
            Dialogo.CancelError = True
            Dialogo.ShowPrinter
            ' O R D E N    D E   C O M P R A
    
    
    EstableImpresora Printer.DeviceName
    
    GoTo ImprimirAhora
    
    Exit Sub
ImpresoraError1:
    'Cancel� impresion
    Exit Sub
ImprimirAhora:




    Dim Sp_Texto As String
    Dim Sp_Lh As String
    Dim Lp_Contador As Long
    Dim Ip_LineasPorPaginas As Integer
    
    Dim Sp_TotalOC As String * 15
    Dim Sp_NetoOC As String * 15
    Dim SP_ivaOC As String * 15
    
     Dim Sp_TotalOCx As String * 15
    Dim Sp_NetoOCx As String * 15
    Dim SP_ivaOCx As String * 15
    
    
    
   
    
    
    
    
    On Error GoTo ErrorP
    Ip_LineasPorPaginas = 55
    Cx = 2
    Cy = 1
    Printer.ScaleMode = 7
    'MsgBox Printer.ScaleMode
    
    For i = 1 To 147
        Sp_Lh = Sp_Lh & "="
    Next
            
        Lp_Contador = 1
        Im_Paginas = 1
        Ip_Paginas = Int(LvDetalle.ListItems.Count / Ip_LineasPorPaginas)
        If (LvDetalle.ListItems.Count Mod Ip_LineasPorPaginas) > 0 Then
            Ip_Paginas = Ip_Paginas + 1
        End If
        ResumenEncabezadoLaser
      '  GoTo fin
        Cy = Cy + Dp
        
        For i = 1 To Me.LvDetalle.ListItems.Count
            S_Item = i
            P_Fecha = LvDetalle.ListItems(i).SubItems(19) & " / " & LvDetalle.ListItems(i).SubItems(21)
            P_Documento = LvDetalle.ListItems(i).SubItems(2)
            P_Numero = LvDetalle.ListItems(i).SubItems(5)
            RSet P_Vence = LvDetalle.ListItems(i).SubItems(4)
            RSet P_Venta = LvDetalle.ListItems(i).SubItems(7)
            EnviaLineasC False
            Cy = Cy + Dp
            Lp_Contador = Lp_Contador + 1
            If Lp_Contador = Ip_LineasPorPaginas Then
                Coloca Cx, Cy, "                                 continua en la siguiente pagina  --->", False, 8
                Cy = Cy + Dp
                Coloca Cx, Cy, Mid(Sp_Lh, 1, 80), False, 8
                Printer.NewPage
                Im_Paginas = Im_Paginas + 1
                ResumenEncabezadoLaser
                Cy = Cy + Dp
                Lp_Contador = 1
            End If
        Next
       ' PieDeCartola
fin:
       ' Coloca Cx, Cy, "                      FIN " & Sp_Lh, False, 8
       
        RSet Sp_TotalOC = txtTotal
        RSet Sp_NetoOC = TxtNeto
        RSet SP_ivaOC = txtIVA
       
        Sp_TotalOCx = " TOTAL OC$: "
        Sp_NetoOCx = " NETO  OC$: "
        SP_ivaOCx = " I.V.A.OC$: "
       

        Cy = Cy + Dp
        Coloca Cx, Cy, Mid(Sp_Lh, 80), False, 8

        pos = Cy
        Cy = Cy + Dp

        Printer.FontName = "Courier New"

        Coloca Cx + 10, Cy, Sp_NetoOCx, True, 10
         Cy = Cy + Dp
        Coloca Cx + 10, Cy, SP_ivaOCx, True, 10
         Cy = Cy + Dp
        Coloca Cx + 10, Cy, Sp_TotalOCx, True, 10

        Cy = pos
         Cy = Cy + Dp
        Coloca Cx + 13, Cy, Sp_NetoOC, True, 10
         Cy = Cy + Dp
        Coloca Cx + 13, Cy, SP_ivaOC, True, 10
         Cy = Cy + Dp
        Coloca Cx + 13, Cy, Sp_TotalOC, True, 10

         Cy = Cy + Dp
        Coloca Cx, Cy, Mid(Sp_Lh, 80), False, 8
        Printer.NewPage
        Printer.EndDoc
        'FIN DEL DOCUMETNO"
    Exit Sub
ErrorP:
    MsgBox "Problema al imprimir..." & vbNewLine & Err.Number & vbNewLine & Err.Description & vbNewLine & Err.Source




End Sub

Private Sub CmdImprimeOC_Click()
   On Error GoTo ImpresoraError1
    'Dialogo.ShowPrinter
    
    
 '   On Error GoTo CancelaImpesion
    SG_codigo = ""
    Sis_SeleccionaImpresionV2.Show 1
    
    If SG_codigo = "" Then GoTo ImpresoraError1
    Do While UCase(Printer.DeviceName) <> UCase(Sm_ImpresoraSeleccionada)
    Loop
    EstableImpresora Printer.DeviceName
    
    ImprimeOC
    Exit Sub
ImpresoraError1:
    'Cancel� impresion
End Sub

Private Sub CmdModifica_Click()
    Dim Bp_Combos As Boolean
    If LvDetalle.SelectedItem Is Nothing Or LvDetalle.ListItems.Count = 0 Then Exit Sub
    Bp_Combos = True
    If CboCuenta.ListIndex = -1 Then Bp_Combos = False
    If CboCentroCosto.ListIndex = -1 Then Bp_Combos = False
    If CboItemGasto.ListIndex = -1 Then Bp_Combos = False
    If CboArea.ListIndex = -1 Then Bp_Combos = False
    
    If Bp_Combos = False Then
        MsgBox "Seleccione datos contables..."
        Exit Sub
    End If
   
    Sql = "UPDATE com_doc_compra_detalle " & _
          "SET pla_id=" & CboCuenta.ItemData(CboCuenta.ListIndex) & "," & _
              "are_id=" & CboArea.ItemData(CboArea.ListIndex) & "," & _
              "cen_id=" & Me.CboCentroCosto.ItemData(CboCentroCosto.ListIndex) & "," & _
              "gas_id=" & Me.CboItemGasto.ItemData(CboItemGasto.ListIndex) & " " & _
          "WHERE cmd_id=" & LvDetalle.SelectedItem
    cn.Execute Sql
    MsgBox "Datos contables modificados...", vbInformation
    CargaDetalle
        
   
End Sub

Private Sub CmdOkImp_Click()
    If Val(TxtValorImp) = 0 Then
        MsgBox "Debe ingresar monto del impuesto...", vbInformation
        TxtValorImp.SetFocus
        Exit Sub
    End If
    If CboImpuestos.ListIndex = -1 Then
        MsgBox "Debe seleccionar el impuesto...", vbInformation�
        CboImpuestos.SetFocus
        Exit Sub
    End If
    
    For i = 1 To LvImpuestos.ListItems.Count
        If CboImpuestos.ItemData(CboImpuestos.ListIndex) = LvImpuestos.ListItems(i) Then
            MsgBox "Este impuesto ya est� ingresado...", vbInformation
            GoTo yaexiste
        End If
    Next
    
    LvImpuestos.ListItems.Add , , CboImpuestos.ItemData(CboImpuestos.ListIndex)
    LvImpuestos.ListItems(LvImpuestos.ListItems.Count).SubItems(1) = CboImpuestos.Text
    LvImpuestos.ListItems(LvImpuestos.ListItems.Count).SubItems(2) = NumFormat(TxtValorImp)
    
    TxtValorImp = ""
    SumeImpuestos
    Exit Sub
yaexiste:
End Sub
Private Sub SumeImpuestos()


    TxtTotalImpuestos = "0"
    For i = 1 To LvImpuestos.ListItems.Count
        TxtTotalImpuestos = CDbl(TxtTotalImpuestos) + CDbl(LvImpuestos.ListItems(i).SubItems(2))
    Next
    TxtTotalImpuestos = NumFormat(TxtTotalImpuestos)
    
    TxtSubTotal = CDbl(TxtExento) + CDbl(txtSubNeto) - CDbl(txtDescuento) + CDbl(TxtNeto) + CDbl(txtIVA)
    TxtSubTotal = NumFormat(TxtSubTotal)
    txtTotal = CDbl(TxtSubTotal) + CDbl(TxtTotalImpuestos)
    txtTotal = NumFormat(txtTotal)
    
End Sub

Private Sub CmdRecargoFlete_Click()
    If Val(TxtRecargoFlete) = 0 Then
        MsgBox "% Recargo debe ser > a 0 ...2", vbInformation
        Exit Sub
    End If
    
    For i = 1 To LvDetalle.ListItems.Count
        LvDetalle.ListItems(i).SubItems(24) = Round(CDbl(LvDetalle.ListItems(i).SubItems(4)) / 100 * Val(TxtRecargoFlete))
    
    Next
    
End Sub

Private Sub CmdSAlir_Click()
       Unload Me
End Sub




Private Sub CargaForm()
    Dim Sp_IDs_Ref As String, Sp_NroGuia As String * 14
    DoEvents
    FrmLoad.Visible = True
    
    Sql = "SELECT "
    LLenarCombo CboCuenta, "pla_nombre", "pla_id", "con_plan_de_cuentas", "pla_activo='SI'"
    LLenarCombo CboCentroCosto, "cen_nombre", "cen_id", "par_centros_de_costo", "cen_activo='SI' AND rut_emp='" & SP_Rut_Activo & "'"
    LLenarCombo Me.CboItemGasto, "gas_nombre", "gas_id", "par_item_gastos", "gas_activo='SI' AND rut_emp='" & SP_Rut_Activo & "'"
    LLenarCombo CboArea, "are_nombre", "are_id", "par_areas", "are_activo='SI' AND rut_emp='" & SP_Rut_Activo & "'"
    Sp_IDs_Ref = Empty
    Bm_Neto = True
    Sql = "SELECT fecha,rut,p.nombre_empresa,doc_id,no_documento,neto,c.iva,iva_retenido,com_exe_otros_imp exento," & _
                 "com_bruto_neto,mes_contable,ano_contable,doc_fecha_vencimiento vence,c.id_ref,bod_id,pago," & _
                 "doc_factura_guias,com_folio_texto,com_plazo,doc_orden_de_compra,com_pla_id " & _
                 ",com_retirado_por,com_solicitado_por,com_autorizado_por,com_iva_sin_credito,com_iva_margen,com_margen_distribuidor,com_glosa " & _
            "FROM com_doc_compra c " & _
            "INNER JOIN sis_documentos d USING(doc_id) " & _
            "INNER JOIN maestro_proveedores p ON c.rut=p.rut_proveedor " & _
            "WHERE c.id=" & Lp_ID_Compra
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        With RsTmp
            DtFecha = !Fecha
            DtVence = !vence
            Sm_Factura_X_guias = !doc_factura_guias
            TxtRutProveedor = !Rut
            TxtRsocial = !nombre_empresa
            TxtFolio = !com_folio_texto
            LLenarCombo CboTipoDoc, "doc_nombre", "doc_id", "sis_documentos", "doc_id=" & !doc_id, "doc_orden"
            CboTipoDoc.ListIndex = 0
            Me.TxtNDoc = !no_documento
            TxtExento = NumFormat(!Exento)
            TxtNeto = NumFormat(!Neto)
            txtIVA = NumFormat(!Iva)
            txtIvaRetenido = NumFormat(!iva_retenido)
            TxtMesContable = UCase(MonthName(!mes_contable))
            TxtAnoContable = !ano_contable
            TxtFpago = !pago
            TxtPlazo = !com_plazo & " D�as"
            TxtSolicitadoPor = "" & !com_solicitado_por
            txtAutorizadoPor = "" & !com_autorizado_por
            txtRetiradoPor = "" & !com_retirado_por
            TxtIVAsinCredito = NumFormat("" & !com_iva_sin_credito)
            txtIvaMargen = NumFormat("" & !com_iva_margen)
            txtMargenDist = NumFormat("" & !com_margen_distribuidor)
            TxtGlosa = "" & !com_glosa
            If id_ref > 0 Then
                FrameRef.Visible = True
            Else
                Me.FrameRef.Visible = False
            End If
            
            LLenarCombo CboBodega, "bod_nombre", "bod_id", "par_bodegas", "bod_id=" & !bod_id, "bod_id"
            If CboBodega.ListCount > 0 Then CboBodega.ListIndex = 0
            If !com_bruto_neto = "BRUTO" Then
                Bm_Neto = False
                Me.SkNetoBrutos = "EN DETALLE INGRESO VALORES BRUTOS"
            End If
            
            If !doc_orden_de_compra = "SI" Then Me.CmdImprimeOC.Visible = True
            
             Sql = "SELECT pla_nombre " & _
                "FROM   par_plazos_vencimiento " & _
                "WHERE pla_id=" & Val(0 & !com_pla_id)
            Consulta RsTmp, Sql
            If RsTmp.RecordCount > 0 Then TxtFpago = RsTmp!pla_nombre Else 'nada
            
        End With
        
       
        
        Sql = "SELECT c.coi_id,imp_nombre,coi_valor " & _
                "FROM com_impuestos c " & _
                "INNER JOIN par_impuestos i USING(imp_id) " & _
                "WHERE  c.id=" & Lp_ID_Compra
        Consulta RsTmp, Sql
        LLenar_Grilla RsTmp, Me, Me.LvImpuestos, False, True, True, False
        
        CTotal
        
        CargaDetalle
            
        
        
    End If
    
    Sql = "SELECT emp_centro_de_costos,emp_areas,emp_item_de_gastos,emp_cuenta " & _
          "FROM sis_empresas " & _
          "WHERE rut='" & SP_Rut_Activo & "'"
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        Sm_CC = "SI"
        If RsTmp!emp_centro_de_costos = "NO" Then
            LLenarCombo CboCentroCosto, "cen_nombre", "cen_id", "par_centros_de_costo", "cen_id=99999"
            CboCentroCosto.ListIndex = 0
            CboCentroCosto.Visible = False
            SkCC.Visible = False
            Sm_CC = "NO"
        End If
        If RsTmp!emp_areas = "NO" Then
            SkArea.Visible = False
            LLenarCombo CboArea, "are_nombre", "are_id", "par_areas", "are_id=99999"
            CboArea.ListIndex = 0
            CboArea.Visible = False
        End If
        If RsTmp!emp_item_de_gastos = "NO" Then
            SkItem.Visible = False
            LLenarCombo CboItemGasto, "gas_nombre", "gas_id", "par_item_gastos", "gas_id=99999"
            CboItemGasto.ListIndex = 0
            CboItemGasto.Visible = False
        End If
        If RsTmp!emp_cuenta = "NO" Then
            
            LLenarCombo CboCuenta, "pla_nombre", "pla_id", "con_plan_de_cuentas", "pla_id=99999"
            CboCuenta.ListIndex = 0
            CboCuenta.Visible = False
        End If
    End If
    CboItemGasto.Visible = True
    CboCentroCosto.Visible = True
    CboArea.Visible = True
    LvDetalle_Click
     FrmLoad.Visible = False
End Sub





Private Sub Form_Load()

    Centrar Me
    Skin2 Me, , 5
    Me.Height = 10650
End Sub
Private Sub CargaDetalle()
    Dim Sp_codProv As String
    LLenarCombo CboImpuestos, "imp_nombre", "imp_id", "par_impuestos", "imp_activo='SI'"
    Sp_codProv = "(SELECT cpv_codigo_proveedor FROM par_codigos_proveedor WHERE rut_proveedor='" & TxtRutProveedor.Text & "' AND pro_codigo=m.codigo LIMIT 1) codprov "
    If Sm_Factura_X_guias = "NO" Then
            If Bm_Neto Then
                Sql = "SELECT cmd_id,pro_codigo,IFNULL(descripcion,cmd_detalle),tip_nombre,cmd_unitario_neto,cmd_cantidad,ume_nombre,cmd_total_neto+cmd_exento, " & _
                        "d.pla_id,d.cen_id,d.gas_id,d.are_id,0,0,0,0,0,0,pro_inventariable,m.pro_codigo_interno,aim_id, " & Sp_codProv & ",cmd_oc_facturada,ubicacion_bodega " & _
                      "FROM com_doc_compra_detalle d " & _
                      "INNER JOIN com_doc_compra c USING(id) " & _
                      "LEFT JOIN maestro_productos m ON m.codigo = d.pro_codigo AND m.rut_emp=c.rut_emp " & _
                      "LEFT JOIN par_tipos_productos t USING(tip_id) " & _
                      "LEFT JOIN sis_unidad_medida u USING(ume_id) " & _
                      "WHERE c.rut_emp='" & SP_Rut_Activo & "' AND d.id=" & Lp_ID_Compra
            Else
                Sql = "SELECT cmd_id,pro_codigo,IFNULL(descripcion,cmd_detalle),tip_nombre,cmd_unitario_bruto,cmd_cantidad,ume_nombre,cmd_total_bruto, " & _
                             "d.pla_id,d.cen_id,d.gas_id,d.are_id,0,0,0,0,0,0,pro_inventariable,m.pro_codigo_interno,aim_id, " & Sp_codProv & ",cmd_oc_facturada,ubicacion_bodega " & _
                      "FROM com_doc_compra_detalle d " & _
                      "INNER JOIN com_doc_compra c USING(id) " & _
                      "LEFT JOIN maestro_productos m ON m.codigo = d.pro_codigo AND m.rut_emp=c.rut_emp " & _
                      "LEFT JOIN par_tipos_productos t USING(tip_id) " & _
                      "LEFT JOIN sis_unidad_medida u USING(ume_id) " & _
                      "WHERE  c.rut_emp='" & SP_Rut_Activo & "' AND  d.id=" & Lp_ID_Compra
            
            End If
    
    
    
    Else
        'FACTURA FUE POR GUIAS
        
        
        
            Sql = "SELECT id,fecha,no_documento,total " & _
                  "FROM com_doc_compra " & _
                  "WHERE id_ref=" & Lp_ID_Compra
            Consulta RsTmp, Sql
            If RsTmp.RecordCount > 0 Then
                RsTmp.MoveFirst
                Sp_IDs_Ref = "0"
                CboGuias.Clear
                Do While Not RsTmp.EOF
                    Sp_IDs_Ref = Sp_IDs_Ref & "," & RsTmp!Id
                    Sp_NroGuia = Right("          " & Trim(Str(RsTmp!no_documento)), 14)
                    CboGuias.AddItem RsTmp!Fecha & "     Nro " & Sp_NroGuia & "    por $" & NumFormat(RsTmp!Total)
                    RsTmp.MoveNext
                Loop
            End If
            'llenarcombo  cboguias,"
             Sql = "SELECT cmd_id,pro_codigo,IFNULL(descripcion,cmd_detalle),tip_nombre,cmd_unitario_neto,cmd_cantidad,ume_nombre,cmd_total_neto, " & _
                        "d.pla_id,d.cen_id,d.gas_id,d.are_id,0,0,0,0,0,0,pro_inventariable,m.pro_codigo_interno,aim_id,  " & Sp_codProv & ",cmd_oc_facturada,ubicacion_bodega " & _
                      "FROM com_doc_compra_detalle d " & _
                      "INNER JOIN com_doc_compra c USING(id) " & _
                      "LEFT JOIN maestro_productos m ON m.codigo = d.pro_codigo AND m.rut_emp=c.rut_emp " & _
                      "LEFT JOIN par_tipos_productos t USING(tip_id) " & _
                      "LEFT JOIN sis_unidad_medida u USING(ume_id) " & _
                      "WHERE d.id IN(" & Sp_IDs_Ref & ")"
    End If
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvDetalle, False, True, True, False

    
    For i = 1 To LvDetalle.ListItems.Count
        If Val(LvDetalle.ListItems(i).SubItems(20)) > 0 Then
        
            Sql = "SELECT aim_nombre " & _
                    "FROM con_activo_inmovilizado " & _
                    "WHERE aim_id=" & Val(LvDetalle.ListItems(i).SubItems(20))
            Consulta RsTmp, Sql
            If RsTmp.RecordCount > 0 Then
                LvDetalle.ListItems(i).SubItems(2) = RsTmp!aim_nombre
                LvDetalle.ListItems(i).SubItems(3) = "ACTIVOS"
                LvDetalle.ListItems(i).SubItems(6) = "UN"
            End If
                            
        
        
        End If
    
   Next
    SumeImpuestos
    
    
    
    
    
   
    
End Sub

Private Sub CTotal()
    Dim lp_Impuestos As Long
    lp_Impuestos = 0
    For i = 1 To LvImpuestos.ListItems.Count
        lp_Impuestos = lp_Impuestos + CDbl(LvImpuestos.ListItems(i).SubItems(2))
    Next
    TxtSubTotal = NumFormat(CDbl(TxtExento) + CDbl(TxtNeto) + CDbl(txtIVA) - CDbl(txtIvaRetenido) + CDbl(TxtIVAsinCredito) + CDbl(txtIvaMargen))
    txtTotal = Format(CDbl(TxtExento) + CDbl(TxtNeto) + CDbl(txtIVA) - CDbl(Me.txtIvaRetenido) + CDbl(TxtIVAsinCredito) + CDbl(txtIvaMargen) + lp_Impuestos, "#,0")
End Sub

Private Sub LvDetalle_Click()
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    With LvDetalle.SelectedItem
        Busca_Id_Combo CboCuenta, .SubItems(8)
        Busca_Id_Combo Me.CboCentroCosto, .SubItems(9)
        Busca_Id_Combo Me.CboItemGasto, .SubItems(10)
        Busca_Id_Combo CboArea, .SubItems(11)
    End With
End Sub



Private Sub LvDetalle_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    ordListView ColumnHeader, Me, LvDetalle
End Sub

Private Sub LvDetalle_KeyDown(KeyCode As Integer, Shift As Integer)
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    With LvDetalle.SelectedItem
        Busca_Id_Combo CboCuenta, .SubItems(8)
        Busca_Id_Combo Me.CboCentroCosto, .SubItems(9)
        Busca_Id_Combo Me.CboItemGasto, .SubItems(10)
        Busca_Id_Combo CboArea, .SubItems(11)
    End With
End Sub



Private Sub Timer1_Timer()
    LvDetalle.SetFocus
    Me.Height = 10650
    Timer1.Enabled = False
    CargaForm
End Sub
Private Sub ImprimeOC()
    Dim Cx As Double, Cy As Double, Sp_Fecha As String, Ip_Mes As Integer, Dp_S As Double, Sp_Letras As String
    Dim Sp_Neto As String * 12, Sp_IVA As String * 12
    
    Dim Sp_Nro_Comprobante As String
    
    Dim Sp_Empresa As String
    Dim Sp_Giro As String
    Dim Sp_Direccion As String
    Dim Sp_Fono As String * 12
    Dim Sp_Mail As String
    Dim Sp_Ciudad As String * 15
    Dim Sp_Comuna As String * 13
    
    
    Dim PSp_Empresa As String
    Dim PSp_Giro As String
    Dim PSp_Direccion As String
    Dim PSp_Fono As String * 12
    Dim PSp_Mail As String
    Dim PSp_Ciudad As String * 15
    Dim PSp_Comuna As String * 13
    
    
    'Variables para detalle de articulos
    Dim Sp_CodigoInt As String * 14
    Dim Sp_Descripcin As String * 29
    Dim Sp_PU As String * 11
    Dim Sp_Cant As String * 7
    Dim Sp_UM As String * 6
    Dim Sp_TotalL As String * 12
    
    Dim sp_AExento As String * 15
    Dim Sp_ANeto As String * 15
    Dim Sp_AIva As String * 15
    Dim Sp_AEspecifico As String * 15
    Dim Sp_ATotal As String * 15
    
    
    
    
    
    Dim Sp_Nombre As String * 45
    Dim Sp_Rut As String * 15
    Dim Sp_Concepto As String
    
    Dim Sp_Nro_Doc As String * 12
    Dim Sp_Documento As String * 35
    Dim Sp_Valor As String * 15
    Dim Sp_Saldo As String * 15
    Dim Sp_Fpago As String * 24
    Dim Sp_Total As String * 12
    
    Dim Sp_Banco As String * 20
    Dim Sp_NroCheque As String * 10
    Dim Sp_FechaCheque As String * 10
    Dim Sp_ValorCheque As String * 15
    Dim Sp_SumaCheque As String * 15
    
    
    
    
    
    
    Dim Sp_Observacion As String * 80
    
    
    For u = 1 To Dialogo.Copies
    
    
    
    
    On Error GoTo ERRORIMPRESION
    Sql = "SELECT giro,direccion,ciudad,fono,email,comuna " & _
         "FROM sis_empresas " & _
         "WHERE rut='" & SP_Rut_Activo & "'"
    Consulta RsTmp2, Sql
    If RsTmp2.RecordCount > 0 Then
        Sp_Giro = "" & RsTmp2!giro
        Sp_Direccion = "" & RsTmp2!direccion
        Sp_Fono = "" & RsTmp2!fono
        Sp_Mail = "" & RsTmp2!email
        Sp_Ciudad = "" & RsTmp2!ciudad
        Sp_Comuna = "" & RsTmp2!comuna
    End If
            
    Sql = "SELECT direccion,ciudad,fono,email,comuna " & _
         "FROM maestro_proveedores " & _
         "WHERE rut_proveedor='" & TxtRutProveedor & "'"
    Consulta RsTmp2, Sql
    If RsTmp2.RecordCount > 0 Then
        PSp_Direccion = "" & RsTmp2!direccion
        PSp_Fono = "" & RsTmp2!fono
        PSp_Mail = "" & RsTmp2!email
        PSp_Ciudad = "" & RsTmp2!ciudad
        PSp_Comuna = "" & RsTmp2!comuna
    End If
            
            
            
    Printer.FontName = "Courier New"
    Printer.FontSize = 10
    Printer.ScaleMode = 7
    Cx = 2
    Cy = 3
    Dp_S = 0.13
    
    
    Printer.CurrentX = Cx
    Printer.CurrentY = Cy
    'Printer.PaintPicture LoadPicture(App.Path & "\IMG\LAFLOR\LAFLOR.jpg"), Cx + 0.62, 1
    Printer.PaintPicture LoadPicture(App.Path & "\REDMAROK.jpg"), Cx + 0.62, 1
    
    Printer.CurrentY = Printer.CurrentY + Dp_S
    
    Printer.FontSize = 18
    Printer.FontBold = True
    
    Printer.Print "            ORDEN DE COMPRA NRO " & TxtNDoc
    Printer.CurrentY = Printer.CurrentY + Dp_S + Dp_S
            
    Printer.FontSize = 12
    Printer.CurrentX = Cx
    Printer.Print SP_Empresa_Activa
    
    
    Printer.CurrentX = Cx
    Printer.CurrentY = Printer.CurrentY + Dp_S
  
    Printer.FontSize = 12
  
    pos = Printer.CurrentY
    Printer.Print "R.U.T.   :" & SP_Rut_Activo
    Printer.CurrentY = pos
    Printer.CurrentX = Cx + 10
    Printer.Print "FECHA:" & Me.DtFecha
    
    Printer.CurrentY = Printer.CurrentY + Dp_S
    
    Printer.FontBold = False
    Printer.FontSize = 12
    Printer.CurrentX = Cx
    Printer.Print "GIRO     :" & Sp_Giro
    Printer.CurrentY = Printer.CurrentY + Dp_S
    
    Printer.FontSize = 12
    Printer.CurrentX = Cx
    Printer.Print "DIRECCION:" & Sp_Direccion
    Printer.CurrentY = Printer.CurrentY + Dp_S
    
    'email
    Printer.FontSize = 12
    Printer.CurrentX = Cx
    Printer.Print "EMAIL    :" & Sp_Mail
    Printer.CurrentY = Printer.CurrentY + Dp_S
    
    
    Printer.FontSize = 12
    Printer.CurrentX = Cx
    Printer.Print "CIUDAD   :" & Sp_Ciudad & "   COMUNA:" & Sp_Comuna & "   FONO:" & Sp_Fono
    Printer.CurrentY = Printer.CurrentY + Dp_S + (Dp_S * 3)
    
    'FIN SECCION EMPRESA
    
    
    
    'AHORA SECCION PROVEEDOR
    'Printer.FontSize = 10
    pos = Printer.CurrentY
    Printer.CurrentX = Cx
    Printer.Print "SE�ORES  :" & TxtRsocial
    Printer.CurrentY = Printer.CurrentY + Dp_S
    
    
    
    Printer.CurrentX = Cx
    pos = Printer.CurrentY
    Printer.Print "RUT      :" & TxtRutProveedor
    
    Printer.CurrentY = pos
    Printer.CurrentX = Cx + 8
    
    If TxtFpago = "CREDITO" Then
        Printer.Print "CONDICION DE PAGO:" & TxtFpago
    Else
        Printer.Print "CONDICION DE PAGO:" & TxtFpago
    End If
    
    Printer.CurrentY = Printer.CurrentY + Dp_S
    
    
    Printer.CurrentX = Cx
    Printer.Print "DIRECCION:" & PSp_Direccion
    Printer.CurrentY = Printer.CurrentY + Dp_S
    
    Printer.CurrentX = Cx
    Printer.Print "EMAIL    :" & PSp_Mail
    Printer.CurrentY = Printer.CurrentY + Dp_S
        
    Printer.CurrentX = Cx
    Printer.Print "CIUDAD   :" & PSp_Ciudad & "    COMUNA:" & PSp_Comuna & "    FONO:" & PSp_Fono
    Printer.CurrentY = Printer.CurrentY + Dp_S + (Dp_S * 3)
    
    'Fin seccion Proveedor
    
    
    
    'Inicio SECCION DETALLE DE ARTICULOS
            With LvDetalle
                If .ListItems.Count > 0 Then
                    Printer.CurrentX = Cx
                    Printer.Print "Detalle de Art�culos"
                    Printer.CurrentY = Printer.CurrentY + Dp_S
                    Sp_CodigoInt = "Cod. Interno"
                    Sp_Descripcin = "Descripcion"
                    RSet Sp_PU = "Pre. Unitario"
                    RSet Sp_Cant = "Cant."
                    Sp_UM = "U.Medida"
                    RSet Sp_TotalL = "Total"
                    
                    Printer.FontSize = 10
                    Printer.FontBold = False
                    
                    Printer.CurrentX = Cx
                    
                    Printer.Print Sp_CodigoInt & " " & Sp_Descripcin & " " & Sp_PU & " " & Sp_Cant & " " & Sp_UM & " " & Sp_TotalL
                    Printer.CurrentY = Printer.CurrentY + Dp_S
                    
                    For i = 1 To .ListItems.Count
                        
                        Sp_CodigoInt = ""
                       If Len(.ListItems(i).SubItems(9)) > 0 Then
                            If .ListItems(i).SubItems(9) <> "Falso" Then
                                If Sm_CC = "NO" Then
                                
                                Else
                                    Busca_Id_Combo CboCentroCosto, Val(.ListItems(i).SubItems(9))
                                    Coloca Cx, Printer.CurrentY, Sp_CodigoInt & "     (CC: " & CboCentroCosto.Text & ")", True, 8
                                End If
                            End If
                                
                            'End If
                            Printer.CurrentY = Printer.CurrentY + Dp_S - 0.2
                            Printer.FontSize = 10
                            Printer.FontBold = False
                            '�Busca_Id_Combo compra_detalle.CboCentroCosto, Val(.ListItems(i).SubItems(9))
                            '�Sp_Descripcin = .ListItems(i).SubItems(2) & " (" & .ListItems(i).SubItems(22) & ")"
                            Sp_Descripcin = .ListItems(i).SubItems(2)
                       Else
                            Sp_Descripcin = .ListItems(i).SubItems(2)
                        End If
                        
                        
                        Sp_CodigoInt = .ListItems(i).SubItems(1)
                        RSet Sp_PU = .ListItems(i).SubItems(4)
                        RSet Sp_Cant = .ListItems(i).SubItems(5)
                        Sp_UM = .ListItems(i).SubItems(6)
                        RSet Sp_TotalL = .ListItems(i).SubItems(7)
                        Printer.CurrentX = Cx
                        Printer.Print Sp_CodigoInt & " " & Sp_Descripcin & " " & Sp_PU & " " & Sp_Cant & " " & Sp_UM & " " & Sp_TotalL
                        Printer.CurrentY = Printer.CurrentY + Dp_S - 0.1
                    Next
                    Printer.CurrentY = Printer.CurrentY + Dp_S
                    posdetalle = Printer.CurrentY
                End If
            End With
    'FIN SECCION DETALLE DE ARTICULOS
    Printer.CurrentX = Cx
    Printer.FontBold = True
    
    RSet sp_AExento = TxtExento
    RSet Sp_ANeto = TxtNeto
    RSet Sp_AIva = txtIVA
    RSet Sp_Especifico = 0 ' Me.TxtImpuesto
    RSet Sp_ATotal = txtTotal
    
    Printer.CurrentX = Cx + 12
    Printer.CurrentY = Printer.CurrentY + Dp_S + Dp_S + Dp_S
    pos = Printer.CurrentY
    Printer.Print "E X E N T O:"
    Printer.CurrentY = pos
    Printer.CurrentX = Cx + 14.5
    Printer.Print sp_AExento
    
    Printer.CurrentX = Cx + 12
    Printer.CurrentY = Printer.CurrentY + Dp_S - 0.2
    pos = Printer.CurrentY
    Printer.Print "N E T O    :"
    Printer.CurrentY = pos
    posn = Printer.CurrentY
    Printer.CurrentX = Cx + 14.5
    Printer.Print Sp_ANeto
    
    Printer.CurrentX = Cx + 12
    Printer.CurrentY = Printer.CurrentY + Dp_S - 0.2
    pos = Printer.CurrentY
    Printer.Print "I.V.A.     :"
    Printer.CurrentY = pos
    Printer.CurrentX = Cx + 14.5
    Printer.Print Sp_AIva
    
    Printer.CurrentX = Cx + 12
    Printer.CurrentY = Printer.CurrentY + Dp_S - 0.2
    pos = Printer.CurrentY
    Printer.Print "ESPECIFICO :"
    Printer.CurrentY = pos
    Printer.CurrentX = Cx + 14.5
    Printer.Print Sp_Especifico
    
    Printer.CurrentX = Cx + 12
    Printer.CurrentY = Printer.CurrentY + Dp_S - 0.2
    pos = Printer.CurrentY
    Printer.Print "T O T A L  :"
    Printer.CurrentY = pos
    Printer.CurrentX = Cx + 14.5
    Printer.Print Sp_ATotal
    
    Printer.CurrentY = posn
    Printer.CurrentX = Cx
    Printer.Print "SOLICITADO POR:"; TxtSolicitadoPor
    Printer.CurrentX = Cx
    Printer.CurrentY = Printer.CurrentY + Dp_S
    Printer.Print "AUTORIZADO POR:"; txtAutorizadoPor
    Printer.CurrentX = Cx
    Printer.CurrentY = Printer.CurrentY + Dp_S
    Printer.Print "RETIRADO   POR:"; txtRetiradoPor
    
    Printer.FontBold = False
        
   ' Printer.DrawMode = 1
    Printer.DrawWidth = 3
            
    Printer.Line (1.2, 4)-(20, 1), , B  'Rectangulo Encabezado y N� Orden de Compra
    Printer.Line (1.2, 4)-(20, 7.7), , B  'Datos de la Empresa
    Printer.Line (1.2, 4)-(20, 11.2), , B 'Datos del Proveedor
    Printer.Line (1.2, 4)-(20, posdetalle), , B 'Detalle de Orden de Compra
    Printer.Line (1.2, posdetalle)-(20, posdetalle + 2.3), , B 'Detalle del Exento,Neto,Iva,Total
       
    Printer.NewPage
    Printer.EndDoc
    
    Next
    
    Exit Sub
ERRORIMPRESION:
    MsgBox Err.Number & vbNewLine & Err.Description
End Sub
Private Sub ResumenEncabezadoLaser()
    Dim Sp_Lh As String, Sp_Tit As String
    Dim Sp_Cetras As String
    
    
    For i = 1 To 100
        Sp_Lh = Sp_Lh & "_"
    Next
    Printer.FontName = "Courier New"
    

    Dp = 0.35
    Cx = 2
    Cy = 1.5
    Coloca Cx + 15, Cy, "P�gina:" & Im_Paginas & " de " & Ip_Paginas, False, 8
    Cy = Cy + Dp
    Coloca Cx + 15, Cy, "Fecha :" & Date, False, 8
    
    Cx = 2
    Cy = 2
   
    
   ' Cy = Cy + Dp
    'Datos de la empresa
    Sql = "SELECT direccion,giro,comuna,ciudad " & _
            "FROM sis_empresas " & _
            "WHERE rut='" & SP_Rut_Activo & "'"
    Consulta RsTmp, Sql
    
    
    '2-Abril-2015
    Coloca Cx, Cy, Principal.SkEmpresa, True, 10 'Nombre empresa
    Cy = Cy + Dp
    Coloca Cx, Cy, Principal.SkRut, False, 10 'rut
    Cy = Cy + Dp
    Coloca Cx, Cy, RsTmp!direccion & " - " & RsTmp!comuna & " - " & RsTmp!ciudad, False, 10 'direccion
    Cy = Cy + Dp
    Coloca Cx, Cy, RsTmp!giro, False, 10 'giro
    Cy = Cy + Dp
    Coloca Cx, Cy, Sp_Lh, False, 8
    Cy = Cy + Dp
    Coloca Cx, Cy, "       DETALLE " & Me.CboTipoDoc.Text & " " & Me.TxtNDoc, True, 14  ' Titulo
    Cy = Cy + Dp
    Coloca Cx, Cy, Sp_Lh, False, 8
    
 
    'Ahora datos del PROVEEDOR
    Sql = "SELECT nombre_empresa,direccion,comuna,ciudad,fono,email " & _
            "FROM maestro_proveedores " & _
            "WHERE rut_proveedor='" & Me.TxtRutProveedor & "'"
    Consulta RsTmp, Sql
     
     
    Cy = Cy + (Dp * 2)
    Coloca Cx, Cy, "R.U.T.   :" & TxtRutProveedor, True, 10
    Cy = Cy + Dp
    Coloca Cx, Cy, "NOMBRE   :" & Me.TxtRsocial, False, 10 'Nombre empresa
    Cy = Cy + Dp
    Coloca Cx, Cy, "DIRECCION:" & RsTmp!direccion & "    ", False, 10
    Cy = Cy + Dp
    Coloca Cx, Cy, " CIUDAD  :" & RsTmp!ciudad & "   FONO:" & RsTmp!fono & "       email:" & RsTmp!email, False, 10 'rut
    Cy = Cy + Dp + Dp
    Coloca Cx, Cy, Sp_Lh, False, 8
  
    S_Item = "Itm"
    P_Fecha = "Codigos"
    P_Documento = "Descripcion"
    P_Numero = "Cant"
    RSet P_Vence = "Precio U."
    RSet P_Venta = "SubTotal"
    'RSet P_Pago = "Abono"
    'RSet P_Saldo = "Saldo"
    Cy = Cy + Dp
    EnviaLineasC True
    Cy = Cy + Dp
     Printer.FontName = "Courier New"
    Coloca Cx, Cy, Sp_Lh, False, 8
    
End Sub

Private Function EnviaLineasC(Negrita As Boolean)
        Dim avance As Double
        avance = 1.5
        Printer.FontSize = 10
        Printer.FontName = "Arial Narrow"
        Coloca Cx, Cy, S_Item, Negrita, 10
        Coloca Cx + 0.6, Cy, P_Fecha, Negrita, 10
        Coloca Cx + 3.9, Cy, P_Documento, Negrita, 10
        Coloca Cx + 13, Cy, P_Numero, Negrita, 10
        Coloca (Cx + 15.5) - Printer.TextWidth(P_Vence), Cy, P_Vence, Negrita, 10
        Coloca (Cx + 17) - Printer.TextWidth(P_Venta), Cy, P_Venta, Negrita, 10
        'Coloca (Cx + 14.2) - Printer.TextWidth(P_Pago), Cy, P_Pago, Negrita, 10
        'Coloca (Cx + 17) - Printer.TextWidth(P_Saldo), Cy, P_Saldo, Negrita, 10
       
       
End Function
Private Sub EstableImpresora(NombreImpresora As String)
   'Seteamos al impresora.
    Dim impresora As Printer
    ' Establece la impresora que se utilizar� para imprimir
    'Recorremos el objeto Printers
    For Each impresora In Printers
        'Si es igual al contenido se�alado en el combobox
        If UCase(impresora.DeviceName) = UCase(NombreImpresora) Then
            'Lo seteamos as�.
            Set Printer = impresora
            Exit For
        End If
    Next

End Sub



Private Sub TxtExento_KeyPress(KeyAscii As Integer)
    KeyAscii = AceptaSoloNumeros(KeyAscii)
End Sub

Private Sub TxtExento_Validate(Cancel As Boolean)
    If Val(TxtExento) Then TxtExento = "0"
    SumeImpuestos
End Sub

Private Sub TxtGlosa_GotFocus()
    En_Foco TxtGlosa
End Sub

Private Sub TxtGlosa_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub TxtIva_GotFocus()
    En_Foco txtIVA
End Sub

Private Sub txtIVA_KeyPress(KeyAscii As Integer)
    KeyAscii = AceptaSoloNumeros(KeyAscii)
End Sub

Private Sub txtIVA_Validate(Cancel As Boolean)
    If Val(txtIVA) = 0 Then txtIVA = 0
    SumeImpuestos
End Sub

Private Sub TxtNeto_GotFocus()
    En_Foco TxtNeto
End Sub

Private Sub TxtNeto_KeyPress(KeyAscii As Integer)
    KeyAscii = AceptaSoloNumeros(KeyAscii)
End Sub

Private Sub TxtNeto_Validate(Cancel As Boolean)
    If Val(TxtNeto) = 0 Then TxtNeto = 0
    SumeImpuestos
End Sub

Private Sub TxtRecargoFlete_GotFocus()
    En_Foco TxtRecargoFlete
End Sub

Private Sub TxtRecargoFlete_KeyPress(KeyAscii As Integer)
    KeyAscii = KeyAscii
End Sub

Private Sub txtSubNeto_KeyPress(KeyAscii As Integer)
    KeyAscii = AceptaSoloNumeros(KeyAscii)
End Sub

Private Sub txtSubNeto_Validate(Cancel As Boolean)
    If Val(txtSubNeto) = 0 Then txtSubNeto = 0
    SumeImpuestos
End Sub

Private Sub TxtSubTotal_GotFocus()
    En_Foco TxtSubTotal
End Sub

Private Sub TxtSubTotal_KeyPress(KeyAscii As Integer)
    KeyAscii = AceptaSoloNumeros(KeyAscii)
End Sub

Private Sub TxtSubTotal_Validate(Cancel As Boolean)
    If TxtSubTotal = 0 Then TxtSubTotal = 0
    SumeImpuestos
End Sub

Private Sub TxtValorImp_GotFocus()
    En_Foco TxtValorImp
End Sub

Private Sub TxtValorImp_KeyPress(KeyAscii As Integer)
    KeyAscii = AceptaSoloNumeros(KeyAscii)
End Sub
