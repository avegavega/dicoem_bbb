VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{DE4917CD-D9B7-45E2-B0C7-DA1DB0A76109}#1.0#0"; "jhTextBoxM.ocx"
Begin VB.Form compra_detalle 
   Caption         =   "Detalle de Compra"
   ClientHeight    =   9195
   ClientLeft      =   4965
   ClientTop       =   810
   ClientWidth     =   14970
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   ScaleHeight     =   9195
   ScaleWidth      =   14970
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   285
      OleObjectBlob   =   "compra_detalle.frx":0000
      Top             =   2340
   End
   Begin VB.Frame Frame4 
      Caption         =   "Actualizacion de precios de Venta"
      Height          =   7905
      Left            =   16950
      TabIndex        =   108
      Top             =   2565
      Visible         =   0   'False
      Width           =   10785
      Begin MSComctlLib.ListView LvPreciosVenta 
         Height          =   6795
         Left            =   480
         TabIndex        =   109
         Top             =   615
         Visible         =   0   'False
         Width           =   9780
         _ExtentX        =   17251
         _ExtentY        =   11986
         View            =   3
         LabelWrap       =   0   'False
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   7
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "F1200"
            Text            =   "Fecha"
            Object.Width           =   1940
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "T1100"
            Text            =   "Nombre "
            Object.Width           =   3351
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "T100"
            Text            =   "Documento"
            Object.Width           =   3175
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Object.Tag             =   "T100"
            Text            =   "Nro Documento"
            Object.Width           =   1940
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   4
            Object.Tag             =   "N102"
            Text            =   "Precio.U."
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   5
            Key             =   "cant"
            Object.Tag             =   "N102"
            Text            =   "Cantidad"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   6
            Key             =   "total"
            Object.Tag             =   "N100"
            Text            =   "Total"
            Object.Width           =   2028
         EndProperty
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H80000001&
         BackStyle       =   1  'Opaque
         BorderColor     =   &H80000001&
         Height          =   7335
         Left            =   240
         Top             =   360
         Width           =   10230
      End
   End
   Begin VB.Frame FrameKyR 
      Caption         =   "Precio de Venta"
      Height          =   2460
      Left            =   4935
      TabIndex        =   101
      Top             =   120
      Visible         =   0   'False
      Width           =   9825
      Begin VB.TextBox TxtKyRPrecioActual 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000004&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   420
         Left            =   5835
         Locked          =   -1  'True
         TabIndex        =   106
         Text            =   "0"
         Top             =   1335
         Width           =   1470
      End
      Begin VB.TextBox TxtKyRSugerido 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   420
         Left            =   5835
         TabIndex        =   104
         Text            =   "0"
         Top             =   1755
         Width           =   1470
      End
      Begin VB.TextBox TxtKyRMargen 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000004&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   420
         Left            =   5835
         Locked          =   -1  'True
         TabIndex        =   102
         Top             =   915
         Width           =   1470
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel20 
         Height          =   285
         Left            =   4320
         OleObjectBlob   =   "compra_detalle.frx":0234
         TabIndex        =   103
         Top             =   945
         Width           =   1260
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel22 
         Height          =   375
         Left            =   2445
         OleObjectBlob   =   "compra_detalle.frx":02A2
         TabIndex        =   105
         Top             =   1830
         Width           =   3150
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel23 
         Height          =   300
         Left            =   2475
         OleObjectBlob   =   "compra_detalle.frx":032A
         TabIndex        =   107
         Top             =   1410
         Width           =   3090
      End
   End
   Begin VB.Frame FraPVenta 
      Caption         =   "Precio de Venta"
      Height          =   1410
      Left            =   7665
      TabIndex        =   60
      Top             =   960
      Visible         =   0   'False
      Width           =   5925
      Begin VB.TextBox TxtPventaBruto 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   4350
         TabIndex        =   72
         Text            =   "0"
         Top             =   1050
         Width           =   1470
      End
      Begin VB.TextBox TxtDiferenciaBruto 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         Height          =   285
         Left            =   2985
         Locked          =   -1  'True
         TabIndex        =   71
         TabStop         =   0   'False
         Top             =   1050
         Width           =   1335
      End
      Begin VB.TextBox TxtPVBrutoActual 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         Height          =   285
         Left            =   1185
         Locked          =   -1  'True
         TabIndex        =   70
         TabStop         =   0   'False
         Text            =   "0"
         Top             =   1050
         Width           =   1785
      End
      Begin VB.TextBox TxtPVActual 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         Height          =   285
         Left            =   1185
         Locked          =   -1  'True
         TabIndex        =   67
         TabStop         =   0   'False
         Text            =   "0"
         Top             =   750
         Width           =   1785
      End
      Begin VB.TextBox txtDiferencia 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         Height          =   285
         Left            =   2985
         Locked          =   -1  'True
         TabIndex        =   65
         TabStop         =   0   'False
         ToolTipText     =   "Diferencia Costo / Venta"
         Top             =   765
         Width           =   1335
      End
      Begin VB.TextBox txtPventa 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   4350
         TabIndex        =   64
         Text            =   "0"
         Top             =   750
         Width           =   1470
      End
      Begin VB.TextBox txtMargen 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   2280
         TabIndex        =   63
         Top             =   210
         Width           =   1035
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel10 
         Height          =   225
         Left            =   4575
         OleObjectBlob   =   "compra_detalle.frx":03AE
         TabIndex        =   62
         Top             =   555
         Width           =   1200
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel9 
         Height          =   195
         Left            =   1560
         OleObjectBlob   =   "compra_detalle.frx":0424
         TabIndex        =   61
         Top             =   255
         Width           =   915
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel12 
         Height          =   225
         Left            =   3060
         OleObjectBlob   =   "compra_detalle.frx":0492
         TabIndex        =   66
         Top             =   540
         Width           =   1230
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel6 
         Height          =   210
         Left            =   225
         OleObjectBlob   =   "compra_detalle.frx":050C
         TabIndex        =   68
         Top             =   750
         Width           =   900
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel14 
         Height          =   210
         Left            =   225
         OleObjectBlob   =   "compra_detalle.frx":057E
         TabIndex        =   73
         Top             =   1080
         Width           =   900
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel17 
         Height          =   225
         Left            =   1470
         OleObjectBlob   =   "compra_detalle.frx":05F2
         TabIndex        =   74
         Top             =   555
         Width           =   1455
      End
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   1620
      Left            =   135
      TabIndex        =   82
      Top             =   675
      Visible         =   0   'False
      Width           =   14580
      _ExtentX        =   25718
      _ExtentY        =   2858
      _Version        =   393216
      TabHeight       =   520
      WordWrap        =   0   'False
      TabCaption(0)   =   "Compras"
      TabPicture(0)   =   "compra_detalle.frx":065C
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "SkinLabel3(4)"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "SkinLabel3(3)"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "SkinLabel3(2)"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "SkinLabel4(0)"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "SkinLabel3(0)"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "LvDocumentos"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).Control(6)=   "SkXcien"
      Tab(0).Control(6).Enabled=   0   'False
      Tab(0).Control(7)=   "TxtDifNeto"
      Tab(0).Control(7).Enabled=   0   'False
      Tab(0).Control(8)=   "txtDifBruto"
      Tab(0).Control(8).Enabled=   0   'False
      Tab(0).Control(9)=   "TxtPcompraBruto"
      Tab(0).Control(9).Enabled=   0   'False
      Tab(0).Control(10)=   "TxtPCompraNeto"
      Tab(0).Control(10).Enabled=   0   'False
      Tab(0).Control(11)=   "txtPVentaNeto"
      Tab(0).Control(11).Enabled=   0   'False
      Tab(0).Control(12)=   "TxtPrecioVentaBruto"
      Tab(0).Control(12).Enabled=   0   'False
      Tab(0).Control(13)=   "SkinLabel18"
      Tab(0).Control(13).Enabled=   0   'False
      Tab(0).Control(14)=   "SkinLabel19"
      Tab(0).Control(14).Enabled=   0   'False
      Tab(0).Control(15)=   "TxtNuevoPrecioDeVenta"
      Tab(0).Control(15).Enabled=   0   'False
      Tab(0).ControlCount=   16
      TabCaption(1)   =   "Ventas"
      TabPicture(1)   =   "compra_detalle.frx":0678
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "LvVentas"
      Tab(1).ControlCount=   1
      TabCaption(2)   =   "Ventas por Mes"
      TabPicture(2)   =   "compra_detalle.frx":0694
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "LvVtaMensual"
      Tab(2).ControlCount=   1
      Begin VB.TextBox TxtNuevoPrecioDeVenta 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   12480
         TabIndex        =   100
         Top             =   1275
         Width           =   1050
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel19 
         Height          =   255
         Left            =   9900
         OleObjectBlob   =   "compra_detalle.frx":06B0
         TabIndex        =   99
         Top             =   1320
         Width           =   2475
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel18 
         Height          =   255
         Left            =   10080
         OleObjectBlob   =   "compra_detalle.frx":073E
         TabIndex        =   98
         Top             =   360
         Width           =   1455
      End
      Begin VB.TextBox TxtPrecioVentaBruto 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   12480
         Locked          =   -1  'True
         TabIndex        =   89
         Top             =   945
         Width           =   1050
      End
      Begin VB.TextBox txtPVentaNeto 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   12480
         Locked          =   -1  'True
         TabIndex        =   88
         Top             =   675
         Width           =   1050
      End
      Begin VB.TextBox TxtPCompraNeto 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   11445
         Locked          =   -1  'True
         TabIndex        =   87
         Top             =   675
         Width           =   1020
      End
      Begin VB.TextBox TxtPcompraBruto 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   11445
         Locked          =   -1  'True
         TabIndex        =   86
         Top             =   945
         Width           =   1020
      End
      Begin VB.TextBox txtDifBruto 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   13530
         Locked          =   -1  'True
         TabIndex        =   85
         Top             =   945
         Width           =   990
      End
      Begin VB.TextBox TxtDifNeto 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   13530
         Locked          =   -1  'True
         TabIndex        =   84
         Top             =   675
         Width           =   990
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkXcien 
         Height          =   240
         Left            =   14595
         OleObjectBlob   =   "compra_detalle.frx":07B2
         TabIndex        =   83
         Top             =   645
         Width           =   780
      End
      Begin MSComctlLib.ListView LvDocumentos 
         Height          =   1170
         Left            =   165
         TabIndex        =   90
         Top             =   390
         Width           =   9705
         _ExtentX        =   17119
         _ExtentY        =   2064
         View            =   3
         LabelWrap       =   0   'False
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   7
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "F1200"
            Text            =   "Fecha"
            Object.Width           =   1940
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "T1100"
            Text            =   "Nombre "
            Object.Width           =   3351
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "T100"
            Text            =   "Documento"
            Object.Width           =   3175
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Object.Tag             =   "T100"
            Text            =   "Nro Documento"
            Object.Width           =   1940
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   4
            Object.Tag             =   "N102"
            Text            =   "Precio.U."
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   5
            Key             =   "cant"
            Object.Tag             =   "N102"
            Text            =   "Cantidad"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   6
            Key             =   "total"
            Object.Tag             =   "N100"
            Text            =   "Total"
            Object.Width           =   2028
         EndProperty
      End
      Begin MSComctlLib.ListView LvVtaMensual 
         Height          =   990
         Left            =   -74835
         TabIndex        =   91
         ToolTipText     =   "Solo incluye las ventas, no resta las NC"
         Top             =   465
         Width           =   10350
         _ExtentX        =   18256
         _ExtentY        =   1746
         View            =   3
         LabelWrap       =   0   'False
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   2
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "T1000"
            Text            =   "Periodo"
            Object.Width           =   2646
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "N109"
            Text            =   "Unidades"
            Object.Width           =   3528
         EndProperty
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   240
         Index           =   0
         Left            =   10770
         OleObjectBlob   =   "compra_detalle.frx":0810
         TabIndex        =   92
         Top             =   645
         Width           =   630
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
         Height          =   240
         Index           =   0
         Left            =   10770
         OleObjectBlob   =   "compra_detalle.frx":087C
         TabIndex        =   93
         Top             =   960
         Width           =   660
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   240
         Index           =   2
         Left            =   12450
         OleObjectBlob   =   "compra_detalle.frx":08E8
         TabIndex        =   94
         Top             =   450
         Width           =   945
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   240
         Index           =   3
         Left            =   11550
         OleObjectBlob   =   "compra_detalle.frx":0954
         TabIndex        =   95
         Top             =   450
         Width           =   915
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   240
         Index           =   4
         Left            =   13530
         OleObjectBlob   =   "compra_detalle.frx":09C4
         TabIndex        =   96
         Top             =   420
         Width           =   915
      End
      Begin MSComctlLib.ListView LvVentas 
         Height          =   1155
         Left            =   -74880
         TabIndex        =   97
         Top             =   360
         Width           =   13875
         _ExtentX        =   24474
         _ExtentY        =   2037
         View            =   3
         LabelWrap       =   0   'False
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   7
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "F1200"
            Text            =   "Fecha"
            Object.Width           =   1940
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "T1100"
            Text            =   "Nombre "
            Object.Width           =   3528
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "T100"
            Text            =   "Documento"
            Object.Width           =   3528
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Object.Tag             =   "T100"
            Text            =   "Nro Documento"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   4
            Object.Tag             =   "N102"
            Text            =   "Precio.U."
            Object.Width           =   2646
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   5
            Key             =   "cant"
            Object.Tag             =   "N102"
            Text            =   "Cantidad"
            Object.Width           =   2646
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   6
            Key             =   "total"
            Object.Tag             =   "N100"
            Text            =   "Total"
            Object.Width           =   2646
         EndProperty
      End
   End
   Begin VB.Frame Frame3 
      Caption         =   "Detalle"
      Height          =   4320
      Left            =   225
      TabIndex        =   46
      Top             =   2670
      Width           =   14190
      Begin jhTextBoxM.TextBoxM TxtMultiplica 
         Height          =   285
         Left            =   11940
         TabIndex        =   110
         TabStop         =   0   'False
         Top             =   150
         Visible         =   0   'False
         Width           =   1425
         _ExtentX        =   2514
         _ExtentY        =   503
         Text            =   ""
         Estilo          =   1
         BackColorEnabled=   65535
         Alignment       =   1
         BackColor       =   65535
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   7.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Locked          =   -1  'True
      End
      Begin VB.ComboBox CboTipoCodigo 
         Height          =   315
         ItemData        =   "compra_detalle.frx":0A36
         Left            =   45
         List            =   "compra_detalle.frx":0A43
         Style           =   2  'Dropdown List
         TabIndex        =   81
         Top             =   450
         Width           =   1470
      End
      Begin VB.CommandButton CmdBuscaActivo 
         Caption         =   "A"
         Height          =   330
         Left            =   2580
         TabIndex        =   47
         ToolTipText     =   "Buscar Activo Inmovilizado"
         Top             =   450
         Width           =   300
      End
      Begin VB.TextBox TxtDescrp 
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2850
         Locked          =   -1  'True
         TabIndex        =   57
         TabStop         =   0   'False
         Tag             =   "T"
         Top             =   435
         Width           =   3800
      End
      Begin VB.TextBox TxtMarca 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   6660
         Locked          =   -1  'True
         TabIndex        =   54
         TabStop         =   0   'False
         Top             =   435
         Width           =   1500
      End
      Begin VB.CommandButton CmdBuscaProducto 
         Caption         =   "F1"
         Height          =   330
         Left            =   2505
         TabIndex        =   50
         ToolTipText     =   "Ayuda Buscador de Articulo"
         Top             =   450
         Width           =   300
      End
      Begin VB.TextBox TxtCodigo 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1515
         TabIndex        =   52
         ToolTipText     =   "Para ingresar un Gasto digite 0 (cero)"
         Top             =   435
         Width           =   1020
      End
      Begin VB.TextBox TxtPrecio 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   8130
         Locked          =   -1  'True
         TabIndex        =   51
         TabStop         =   0   'False
         Tag             =   "N"
         Text            =   "0"
         Top             =   435
         Width           =   1500
      End
      Begin VB.TextBox TxtCantidad 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   9600
         TabIndex        =   53
         Tag             =   "N2"
         ToolTipText     =   "Unidad medida"
         Top             =   435
         Width           =   1300
      End
      Begin VB.TextBox TxtSubTotal 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   11925
         TabIndex        =   55
         Text            =   "0"
         Top             =   435
         Width           =   1500
      End
      Begin VB.TextBox TxtStockActual 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   8685
         Locked          =   -1  'True
         TabIndex        =   49
         TabStop         =   0   'False
         Top             =   120
         Visible         =   0   'False
         Width           =   1905
      End
      Begin VB.TextBox TxtUme 
         Alignment       =   2  'Center
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   10905
         Locked          =   -1  'True
         TabIndex        =   48
         TabStop         =   0   'False
         Text            =   "UM"
         Top             =   435
         Width           =   1020
      End
      Begin VB.CommandButton CmdOkImp 
         Caption         =   "Ok"
         Height          =   330
         Left            =   13425
         TabIndex        =   56
         ToolTipText     =   "Agrega"
         Top             =   405
         Width           =   525
      End
      Begin MSComctlLib.ListView LvDetalle 
         Height          =   3150
         Left            =   1530
         TabIndex        =   58
         Top             =   780
         Width           =   12420
         _ExtentX        =   21908
         _ExtentY        =   5556
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   0   'False
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   29
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "N100"
            Text            =   "Nro Orden"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "T1000"
            Text            =   "Codigo"
            Object.Width           =   2328
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "T3000"
            Text            =   "Descripcion"
            Object.Width           =   6703
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Object.Tag             =   "N102"
            Text            =   "Tipo"
            Object.Width           =   2646
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   4
            Object.Tag             =   "N102"
            Text            =   "Precio Cpra."
            Object.Width           =   2646
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   5
            Object.Tag             =   "N102"
            Text            =   "Cant."
            Object.Width           =   2293
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   6
            Object.Tag             =   "T1000"
            Text            =   "U.Medida"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   7
            Key             =   "total"
            Object.Tag             =   "N100"
            Text            =   "Total"
            Object.Width           =   2646
         EndProperty
         BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   8
            Object.Tag             =   "N100"
            Text            =   "Cuenta"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   9
            Object.Tag             =   "N100"
            Text            =   "CentroCosto"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   10
            Object.Tag             =   "N109"
            Text            =   "gasid"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(12) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   11
            Object.Tag             =   "N109"
            Text            =   "Area"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(13) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   12
            Object.Tag             =   "N102"
            Text            =   "Factor Dscto"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(14) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   13
            Object.Tag             =   "N102"
            Text            =   "Valor Dscto"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(15) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   14
            Object.Tag             =   "N102"
            Text            =   "Unitario Con Dscto"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(16) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   15
            Object.Tag             =   "N100"
            Text            =   "Total Linea C/dscto"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(17) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   16
            Object.Tag             =   "N102"
            Text            =   "CostoLinea"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(18) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   17
            Object.Tag             =   "N102"
            Text            =   "Unitario/Final"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(19) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   18
            Object.Tag             =   "T500"
            Text            =   "Inventariable"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(20) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   19
            Text            =   "Neto final"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(21) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   20
            Object.Tag             =   "N109"
            Text            =   "Costo real neto"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(22) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   21
            Object.Tag             =   "N109"
            Text            =   "exento"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(23) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   22
            Object.Tag             =   "T1000"
            Text            =   "TEXTO CC"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(24) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   23
            Object.Tag             =   "N109"
            Text            =   "Margen"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(25) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   24
            Object.Tag             =   "N100"
            Text            =   "Diferencia"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(26) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   25
            Object.Tag             =   "N100"
            Text            =   "Pventa"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(27) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   26
            Object.Tag             =   "T1000"
            Text            =   "Es Activo Inmobilizado?"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(28) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   27
            Object.Tag             =   "N109"
            Text            =   "Propuesto?"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(29) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   28
            Object.Tag             =   "T1000"
            Text            =   "Cod Ingresado"
            Object.Width           =   2540
         EndProperty
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkNetoBrutos 
         Height          =   270
         Left            =   180
         OleObjectBlob   =   "compra_detalle.frx":0A79
         TabIndex        =   59
         Top             =   3990
         Width           =   7350
      End
   End
   Begin MSComctlLib.ListView LvImpuestosInd 
      Height          =   4335
      Left            =   19065
      TabIndex        =   69
      Top             =   -60
      Width           =   3735
      _ExtentX        =   6588
      _ExtentY        =   7646
      View            =   3
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   3
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Linea"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Id Impuesto"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "Valor"
         Object.Width           =   2540
      EndProperty
   End
   Begin VB.Frame FrameNC 
      Caption         =   "Tipo de nota de credito"
      Height          =   675
      Left            =   195
      TabIndex        =   39
      Top             =   960
      Visible         =   0   'False
      Width           =   4500
      Begin VB.ComboBox CboMotivoNC 
         Height          =   315
         Left            =   135
         Style           =   2  'Dropdown List
         TabIndex        =   40
         Top             =   270
         Width           =   4275
      End
   End
   Begin VB.CommandButton CmdAnterior 
      Caption         =   "Anterior"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   465
      Left            =   1770
      TabIndex        =   38
      ToolTipText     =   "Vuelve a pantalla anterior"
      Top             =   8580
      Width           =   1470
   End
   Begin VB.CommandButton CmdSalir 
      Cancel          =   -1  'True
      Caption         =   "&Salir"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   465
      Left            =   3240
      TabIndex        =   37
      ToolTipText     =   "Salir sin grabar"
      Top             =   8580
      Width           =   1470
   End
   Begin VB.Frame Frame2 
      Height          =   2115
      Left            =   11370
      TabIndex        =   30
      Top             =   6930
      Width           =   3090
      Begin VB.TextBox Text1 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         Height          =   300
         Left            =   1335
         Locked          =   -1  'True
         TabIndex        =   41
         TabStop         =   0   'False
         Tag             =   "N"
         Text            =   "0"
         Top             =   225
         Width           =   1500
      End
      Begin VB.TextBox txtTotal 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1335
         Locked          =   -1  'True
         TabIndex        =   33
         TabStop         =   0   'False
         Text            =   "0"
         Top             =   525
         Width           =   1500
      End
      Begin VB.TextBox txtDescuento 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000E&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1335
         TabIndex        =   32
         Text            =   "0"
         Top             =   810
         Width           =   1500
      End
      Begin VB.TextBox TxtGtotal 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1335
         Locked          =   -1  'True
         TabIndex        =   31
         TabStop         =   0   'False
         Text            =   "0"
         Top             =   1635
         Width           =   1500
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel8 
         Height          =   195
         Left            =   120
         OleObjectBlob   =   "compra_detalle.frx":0AF3
         TabIndex        =   34
         Top             =   555
         Width           =   1140
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   195
         Left            =   120
         OleObjectBlob   =   "compra_detalle.frx":0B61
         TabIndex        =   35
         Top             =   855
         Width           =   1140
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   195
         Left            =   150
         OleObjectBlob   =   "compra_detalle.frx":0BD1
         TabIndex        =   36
         Top             =   1665
         Width           =   1140
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel5 
         Height          =   195
         Left            =   540
         OleObjectBlob   =   "compra_detalle.frx":0C39
         TabIndex        =   42
         Top             =   270
         Width           =   720
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Datos del Documento"
      Height          =   1485
      Left            =   210
      TabIndex        =   17
      Top             =   7065
      Width           =   9435
      Begin VB.TextBox txtIvaSinCredito 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   4950
         TabIndex        =   77
         TabStop         =   0   'False
         Text            =   "0"
         Top             =   240
         Width           =   1290
      End
      Begin VB.TextBox txtIVAMargen 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   4950
         Locked          =   -1  'True
         TabIndex        =   76
         TabStop         =   0   'False
         Text            =   "0"
         Top             =   570
         Width           =   1290
      End
      Begin VB.TextBox txtMargenDist 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   4950
         Locked          =   -1  'True
         TabIndex        =   75
         TabStop         =   0   'False
         Text            =   "0"
         ToolTipText     =   "Dato referencial, no suma"
         Top             =   885
         Width           =   1290
      End
      Begin VB.TextBox TxtExento 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         Height          =   300
         Left            =   2415
         Locked          =   -1  'True
         TabIndex        =   43
         TabStop         =   0   'False
         Tag             =   "N"
         Text            =   "0"
         Top             =   240
         Width           =   1290
      End
      Begin VB.TextBox TxtDSubTotal 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   8040
         Locked          =   -1  'True
         TabIndex        =   23
         TabStop         =   0   'False
         Text            =   "0"
         Top             =   225
         Width           =   1290
      End
      Begin VB.TextBox TxtNeto 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2415
         Locked          =   -1  'True
         TabIndex        =   22
         TabStop         =   0   'False
         Text            =   "0"
         Top             =   540
         Width           =   1290
      End
      Begin VB.TextBox txtIvaRetenido 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         Enabled         =   0   'False
         Height          =   285
         Left            =   2415
         Locked          =   -1  'True
         TabIndex        =   21
         TabStop         =   0   'False
         Tag             =   "N"
         Text            =   "0"
         Top             =   1155
         Width           =   1290
      End
      Begin VB.TextBox txtIVA 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2415
         Locked          =   -1  'True
         TabIndex        =   20
         TabStop         =   0   'False
         Text            =   "0"
         Top             =   855
         Width           =   1290
      End
      Begin VB.TextBox TxtDTotal 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   8040
         Locked          =   -1  'True
         TabIndex        =   19
         TabStop         =   0   'False
         Text            =   "0"
         Top             =   855
         Width           =   1290
      End
      Begin VB.TextBox TxtImp 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   8040
         Locked          =   -1  'True
         TabIndex        =   18
         TabStop         =   0   'False
         Text            =   "0"
         Top             =   540
         Width           =   1290
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   195
         Index           =   1
         Left            =   6825
         OleObjectBlob   =   "compra_detalle.frx":0CA3
         TabIndex        =   24
         Top             =   900
         Width           =   1140
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel7 
         Height          =   195
         Left            =   690
         OleObjectBlob   =   "compra_detalle.frx":0D0B
         TabIndex        =   25
         Top             =   900
         Width           =   1695
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel13 
         Height          =   195
         Left            =   870
         OleObjectBlob   =   "compra_detalle.frx":0D75
         TabIndex        =   26
         Top             =   1170
         Width           =   1515
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel15 
         Height          =   195
         Index           =   0
         Left            =   60
         OleObjectBlob   =   "compra_detalle.frx":0DEB
         TabIndex        =   27
         Top             =   630
         Width           =   2325
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel16 
         Height          =   195
         Left            =   6510
         OleObjectBlob   =   "compra_detalle.frx":0E79
         TabIndex        =   28
         Top             =   270
         Width           =   1455
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
         Height          =   195
         Index           =   1
         Left            =   6510
         OleObjectBlob   =   "compra_detalle.frx":0EE9
         TabIndex        =   29
         Top             =   585
         Width           =   1455
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel11 
         Height          =   195
         Left            =   675
         OleObjectBlob   =   "compra_detalle.frx":0F67
         TabIndex        =   44
         Top             =   255
         Width           =   1695
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel15 
         Height          =   195
         Index           =   1
         Left            =   3585
         OleObjectBlob   =   "compra_detalle.frx":0FD1
         TabIndex        =   78
         Top             =   945
         Width           =   1290
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel15 
         Height          =   195
         Index           =   2
         Left            =   3705
         OleObjectBlob   =   "compra_detalle.frx":1047
         TabIndex        =   79
         Top             =   315
         Width           =   1155
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel15 
         Height          =   195
         Index           =   3
         Left            =   3660
         OleObjectBlob   =   "compra_detalle.frx":10C3
         TabIndex        =   80
         Top             =   615
         Width           =   1185
      End
   End
   Begin VB.CommandButton CmdNueva 
      Caption         =   "Grabar"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   465
      Left            =   285
      TabIndex        =   16
      ToolTipText     =   "Graba compra"
      Top             =   8580
      Width           =   1470
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkImpCosto 
      Height          =   195
      Left            =   16695
      OleObjectBlob   =   "compra_detalle.frx":1135
      TabIndex        =   15
      Top             =   8985
      Width           =   1515
   End
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Interval        =   100
      Left            =   3240
      Top             =   8670
   End
   Begin VB.Timer Timer3 
      Interval        =   30
      Left            =   645
      Top             =   8790
   End
   Begin VB.Frame FrameDatos 
      Caption         =   "Datos Contables"
      Height          =   855
      Left            =   195
      TabIndex        =   0
      Top             =   60
      Width           =   13425
      Begin VB.TextBox TxtNroCuenta 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   270
         Left            =   1905
         TabIndex        =   45
         Tag             =   "N2"
         ToolTipText     =   "Unidad medida"
         Top             =   225
         Width           =   1300
      End
      Begin VB.CommandButton cmdCuenta 
         Caption         =   "Codigo Cuenta"
         Height          =   225
         Left            =   285
         TabIndex        =   8
         Top             =   255
         Width           =   1425
      End
      Begin VB.ComboBox CboArea 
         Height          =   315
         Left            =   3210
         Style           =   2  'Dropdown List
         TabIndex        =   7
         Top             =   495
         Width           =   3150
      End
      Begin VB.ComboBox CboCuenta 
         Height          =   315
         Left            =   105
         Style           =   2  'Dropdown List
         TabIndex        =   6
         Top             =   495
         Width           =   3150
      End
      Begin VB.ComboBox CboItemGasto 
         Height          =   315
         Left            =   9495
         Style           =   2  'Dropdown List
         TabIndex        =   11
         Top             =   495
         Width           =   3150
      End
      Begin VB.ComboBox CboCentroCosto 
         Height          =   315
         Left            =   6375
         Style           =   2  'Dropdown List
         TabIndex        =   9
         Top             =   495
         Width           =   3150
      End
      Begin VB.CommandButton CmdOkSd 
         Caption         =   "Ok"
         Height          =   315
         Left            =   14250
         TabIndex        =   5
         Top             =   480
         Visible         =   0   'False
         Width           =   330
      End
      Begin VB.TextBox TxtSdExento 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   10110
         TabIndex        =   4
         Tag             =   "N"
         Text            =   "0"
         Top             =   480
         Visible         =   0   'False
         Width           =   1050
      End
      Begin VB.TextBox TxtSDNeto 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   11145
         TabIndex        =   3
         Tag             =   "N"
         Text            =   "0"
         Top             =   480
         Visible         =   0   'False
         Width           =   1050
      End
      Begin VB.TextBox TxtSdIvaRetenido 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   13230
         TabIndex        =   2
         Tag             =   "N"
         Text            =   "0"
         Top             =   480
         Visible         =   0   'False
         Width           =   1050
      End
      Begin VB.TextBox TxtSdIva 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   12675
         TabIndex        =   1
         Tag             =   "N"
         Text            =   "0"
         Top             =   510
         Visible         =   0   'False
         Width           =   1050
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkItem 
         Height          =   180
         Left            =   9555
         OleObjectBlob   =   "compra_detalle.frx":11AF
         TabIndex        =   10
         Top             =   300
         Width           =   2055
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkCC 
         Height          =   180
         Left            =   6360
         OleObjectBlob   =   "compra_detalle.frx":1227
         TabIndex        =   12
         Top             =   300
         Width           =   2055
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkArea 
         Height          =   180
         Left            =   3285
         OleObjectBlob   =   "compra_detalle.frx":12A3
         TabIndex        =   13
         Top             =   300
         Width           =   2055
      End
      Begin MSComctlLib.ListView LvSinDetalle 
         Height          =   1740
         Left            =   90
         TabIndex        =   14
         Top             =   885
         Width           =   14475
         _ExtentX        =   25532
         _ExtentY        =   3069
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   0   'False
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   13
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "N100"
            Text            =   "id"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "T1000"
            Text            =   "Cuenta"
            Object.Width           =   4410
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "T1500"
            Text            =   "Area"
            Object.Width           =   4410
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Object.Tag             =   "T3000"
            Text            =   "Centro de Costo"
            Object.Width           =   4410
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Object.Tag             =   "T1000"
            Text            =   "Items de Gasto"
            Object.Width           =   4410
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   5
            Key             =   "otros"
            Object.Tag             =   "N100"
            Text            =   "Otros Impuestos"
            Object.Width           =   1852
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   6
            Key             =   "neto"
            Object.Tag             =   "N100"
            Text            =   "Neto"
            Object.Width           =   1852
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   7
            Key             =   "iva"
            Object.Tag             =   "N100"
            Text            =   "IVA"
            Object.Width           =   1852
         EndProperty
         BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   8
            Key             =   "retenido"
            Object.Tag             =   "N100"
            Text            =   "Retenido"
            Object.Width           =   1852
         EndProperty
         BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   9
            Object.Tag             =   "N109"
            Text            =   "PLA_ID"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   10
            Object.Tag             =   "N109"
            Text            =   "ARE_ID"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(12) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   11
            Object.Tag             =   "N109"
            Text            =   "CEN_ID"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(13) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   12
            Object.Tag             =   "N109"
            Text            =   "ITEM GASTO ID"
            Object.Width           =   0
         EndProperty
      End
   End
   Begin ACTIVESKINLibCtl.Skin Skin2 
      Left            =   -30
      OleObjectBlob   =   "compra_detalle.frx":1309
      Top             =   9255
   End
End
Attribute VB_Name = "compra_detalle"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public Neto As Boolean
Public Bp_Graba As Boolean
Public Sp_Accion As String
Public Sm_Referencia As String
Public Sm_Nota As String
Public Sm_Tipo_Nota_Credito As String
Public Sm_Modifica_Unidades As String
Public Sm_Modifica_Valores As String
Dim Bp_VuelveFoco As Boolean
Dim Sm_Codigos As String
Dim Item As ListItem
Dim Sm_ActualizaPventa As String * 2
Dim Sm_EmpVendeBrutoNeto As String * 5
Dim Sm_Tipo_Codigo As String
Dim Sm_MargenMarca As String * 2






Private Sub CboCuenta_Click()
        CmdBuscaActivo.Visible = False
        If CboCuenta.Visible And CboCuenta.ListIndex > -1 Then
        TxtNroCuenta = CboCuenta.ItemData(CboCuenta.ListIndex)
        Sql = "SELECT pla_id " & _
                    "FROM con_plan_de_cuentas " & _
                    "WHERE tpo_id=1 AND det_id=4 AND pla_analisis='SI' AND pla_id=" & CboCuenta.ItemData(CboCuenta.ListIndex)
            Consulta RsTmp, Sql
            If RsTmp.RecordCount > 0 Then
                Me.CmdBuscaActivo.Visible = True
            End If
    
        End If
End Sub

Private Sub CboMotivoNC_Click()
    If FrameNC.Visible = True Then
        CargaReferencia
        If CboMotivoNC.ListIndex = 0 Then
            CreditoAnula
        Else
            Sql = "SELECT tnc_modifica_inventario inventario,tnc_modifica_valores valores " & _
                  "FROM ntc_tipos " & _
                  "WHERE tnc_id=" & CboMotivoNC.ItemData(CboMotivoNC.ListIndex)
            Consulta RsTmp, Sql
            If RsTmp.RecordCount > 0 Then
                TxtCantidad.Enabled = IIf(RsTmp!Inventario = "SI", True, False)
                TxtSubTotal.Enabled = IIf(RsTmp!valores = "SI", True, False)
                Sm_Modifica_Unidades = RsTmp!Inventario
                Sm_Modifica_Valores = RsTmp!valores
                TxtCodigo.Enabled = True
                CmdOkImp.Enabled = True
                CmdBuscaProducto.Enabled = True
            End If
        End If
    End If
End Sub

Private Sub CmdAnterior_Click()
    Sp_Accion = "MODIFICAR"
    Me.Hide
End Sub

Private Sub CmdBuscaActivo_Click()
    SG_codigo = Empty
    If CboCuenta.ListIndex = -1 Then
        MsgBox "Seleccione cuenta..."
        CboCuenta.SetFocus
        
        Exit Sub
    End If
    SG_codigo2 = CboCuenta.ItemData(CboCuenta.ListIndex)
    Con_ActivoInmovilizado.Sm_FiltroCta = " AND a.pla_id=" & SG_codigo2
    Con_ActivoInmovilizado.CmdSelecciona.Visible = True
    Con_ActivoInmovilizado.Show 1
    If Val(SG_codigo) > 0 Then
        Sql = "SELECT aim_nombre,aim_valor " & _
                "FROM con_activo_inmovilizado " & _
                "WHERE aim_id=" & Val(SG_codigo)
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
            TxtCodigo = SG_codigo
            TxtDescrp = RsTmp!aim_nombre
            TxtPrecio = NumFormat(RsTmp!aim_valor)
            TxtSubTotal = NumFormat(RsTmp!aim_valor)
            TxtCantidad = 1
        End If
    End If
End Sub

Private Sub CmdBuscaProducto_Click()



   
     If SP_Rut_Activo = "76.178.895-7" Then
                    SG_codigo = ""
                    SG_codigo2 = ""
                    Busca_Producto_Repuestos.Show 1
                    If Sm_UtilizaCodigoInterno = "SI" Then
                        TxtCodigo = SG_codigo2
                    Else
                        TxtCodigo = SG_codigo
                    End If
    Else
                     BuscaProducto.Show 1
    End If
    
    
    
    If Len(SG_codigo) = 0 Then Exit Sub
    TxtCodigo = SG_codigo
    TxtCodigo_Validate True
End Sub

Private Sub cmdCuenta_Click()
    With BuscarSimple
        SG_codigo = Empty
        .Sm_Consulta = "SELECT pla_id, pla_nombre,tpo_nombre,det_nombre " & _
                       "FROM    con_plan_de_cuentas p,  con_tipo_de_cuenta t,   con_detalle_tipo_cuenta d " & _
                       "WHERE   p.tpo_id = t.tpo_id AND p.det_id = d.det_id "
        .Sm_CampoLike = "pla_nombre"
        .Im_Columnas = 2
        .Show 1
        If SG_codigo = Empty Then Exit Sub
        TxtNroCuenta = SG_codigo
        Busca_Id_Combo CboCuenta, Val(SG_codigo)
    End With
End Sub

Private Sub CmdNueva_Click()
    Dim Lp_Dif As Long
    Bp_Graba = False
    CostoFinal
    If LvDetalle.ListItems.Count = 0 Then
        MsgBox "No ha ingresado Items ...", vbInformation
        Exit Sub
    End If
    If Neto Then
        Lp_Dif = CDbl(TxtGtotal) - (CDbl(TxtNeto) + CDbl(txtIvaSinCredito))
        If Lp_Dif <> 0 Then
            If Abs(Lp_Dif) < 4 Then
                If MsgBox("Existe una diferencia inferior a $4 pesos," & vbNewLine & "� Continuar ?", vbQuestion + vbYesNo) = vbNo Then Exit Sub
            Else
                If SP_Rut_Activo = "76.063.757-2" Or SP_Rut_Activo = "10.966.790-0" Then
                    If MsgBox("Los suma de los valores no cuadran con el valore del documento" & vbNewLine & "�Continuar?", vbQuestion + vbOKCancel) = vbCancel Then Exit Sub
                Else
                    MsgBox "Los totales no cuadran con los valores ingresados en el documento...", vbInformation
                
                    Exit Sub
                End If
            End If
        End If
    Else
        Lp_Dif = CDbl(TxtGtotal) - CDbl(TxtDSubTotal)
        If Lp_Dif <> 0 Then
            If Abs(Lp_Dif) < 4 Then
                If MsgBox("Existe una diferencia inferior a $4 pesos," & vbNewLine & "� Continuar ?", vbQuestion + vbYesNo) = vbNo Then Exit Sub
            Else
                MsgBox "Los totales no cuadran con los valores ingresados en el documento...", vbInformation
                Exit Sub
            End If
        End If
    End If
    
    'totalgomas
    'no pasar si no ha actualizado todos los precios de venta
    '25 Julio 2019
    'If SP_Rut_Activo = "76.178.895-7" Then
    '    For i = 1 To LvDetalle.ListItems.Count
    '        If LvDetalle.ListItems(i).SubItems(27) <> "SI" Then
    '            MsgBox "Aun no ha confirmado el precio de venta para uno o mas items...", vbInformation
    '            LvDetalle.SetFocus
    '            Exit Sub
    '        End If
    '    Next
    'End If
    
    If Sm_Nota = "CREDITO" Then
        If CboMotivoNC.ListIndex = 0 Then
            'If CDbl(compra_Ingreso.TxtTotalRef) <> CDbl(TxtDTotal) Then
            '    MsgBox "El valor de la NC por anulacion debe ser igual" & vbNewLine & "al documento de referencia...", vbInformation
            '    Exit Sub
            'End If
            Sm_Tipo_Nota_Credito = "ANULA"
            
        Else
            Sm_Tipo_Nota_Credito = "MODIFICA"
        End If
    End If
    
    Sp_Accion = "GRABAR"
    Bp_Graba = True
    compra_Ingreso.Smp_ActualizaPventa = Sm_ActualizaPventa
    'Ya estamos en condiciones de realizar la grabacion del detalle
    'Pasaremos el contenido de la grilla a una grilla igual en la pantalla del documento
    'compra_Ingreso.TomarLv
    'compra_Ingreso.TxtImpuesto = "aaldfmlakdfjaslkfjasdlf"
    Me.Hide
End Sub
Private Sub CostoFinal()
    Dim dp_Factor As Double, dg_TotalLinea As Long, Dp_Factor2 As Double, Lp_Mexento As Long
    If LvDetalle.ListItems.Count = 0 Then Exit Sub
    'If Neto Then
    'Debemos saber si hay exento, para no incluirlo en el valor neto de la linea, sino sumarlo aparte para el costo final del producto
        LvImpuestosInd.ListItems.Clear
        For i = 1 To LvDetalle.ListItems.Count
           ' If Sm_Nota = "CREDITO" And Sm_Modifica_Unidades = "SI" And Sm_Modifica_Valores = "NO" Then
           '     LvDetalle.ListItems(i).SubItems(17) = 0
           '     LvDetalle.ListItems(i).SubItems(16) = 0
           '     LvDetalle.ListItems(i).SubItems(19) = 0
           '     LvDetalle.ListItems(i).SubItems(4) = 0
            '    LvDetalle.ListItems(i).SubItems(7) = 0
           ' Else
    
                If Not Neto Then 'brutos
                    dg_TotalLinea = (CDbl(LvDetalle.ListItems(i).SubItems(15)) - CDbl(TxtExento)) / Val("1." & DG_IVA) + CDbl(TxtExento)
                    LvDetalle.ListItems(i).SubItems(19) = dg_TotalLinea / CDbl(LvDetalle.ListItems(i).SubItems(5)) 'Total Neto igual
                    dp_Factor = dg_TotalLinea / (CDbl(TxtGtotal) - CDbl(txtIVA)) * 100
                    LvDetalle.ListItems(i).SubItems(16) = ((SkImpCosto / 100) * dp_Factor) + (dg_TotalLinea)
                    LvDetalle.ListItems(i).SubItems(17) = CDbl(LvDetalle.ListItems(i).SubItems(16)) / CDbl(LvDetalle.ListItems(i).SubItems(5))
                    
                    LvDetalle.ListItems(i).SubItems(20) = LvDetalle.ListItems(i).SubItems(16)
                    LvDetalle.ListItems(i).SubItems(21) = CDbl(LvDetalle.ListItems(i).SubItems(20)) - CDbl(LvDetalle.ListItems(i).SubItems(17))
                    If CDbl(TxtExento) > 0 Then
                        Dp_Factor2 = CDbl(TxtExento) / (CDbl(TxtGtotal) - CDbl(txtIVA)) * 100
                        LvDetalle.ListItems(i).SubItems(20) = LvDetalle.ListItems(i).SubItems(16)
                        LvDetalle.ListItems(i).SubItems(17) = CDbl(LvDetalle.ListItems(i).SubItems(16)) - (CDbl(LvDetalle.ListItems(i).SubItems(16)) / 100 * Dp_Factor2)
                        LvDetalle.ListItems(i).SubItems(21) = CDbl(LvDetalle.ListItems(i).SubItems(20)) - CDbl(LvDetalle.ListItems(i).SubItems(17))
                    End If
                    
                    
                    
                Else 'netos
                    'aqui error
                    dp_Factor = CDbl(LvDetalle.ListItems(i).SubItems(15)) / CDbl(TxtGtotal) * 100
                    LvDetalle.ListItems(i).SubItems(16) = ((SkImpCosto / 100) * dp_Factor) + (LvDetalle.ListItems(i).SubItems(15))
                    LvDetalle.ListItems(i).SubItems(20) = LvDetalle.ListItems(i).SubItems(16)
                    'LvDetalle.ListItems(i).SubItems(17) = CDbl(LvDetalle.ListItems(i).SubItems(16)) / CDbl(LvDetalle.ListItems(i).SubItems(5))
                    LvDetalle.ListItems(i).SubItems(17) = CDbl(LvDetalle.ListItems(i).SubItems(15)) / CDbl(LvDetalle.ListItems(i).SubItems(5))
                    
                    
                    
                    '25-10-2014
                    'Recorremos la lista de impuestos ingresados
                    For d = 1 To compra_Ingreso.LvImpuestos.ListItems.Count
                        'Aqui debemos ponderar esos impuestos linea a linea
                        If CDbl(compra_Ingreso.LvImpuestos.ListItems(d).SubItems(2)) > 0 Then
                            Me.LvImpuestosInd.ListItems.Add , , i
                            n = LvImpuestosInd.ListItems.Count
                            LvImpuestosInd.ListItems(n).SubItems(1) = compra_Ingreso.LvImpuestos.ListItems(d)
                            LvImpuestosInd.ListItems(n).SubItems(2) = Round((CDbl(compra_Ingreso.LvImpuestos.ListItems(d).SubItems(2)) / 100) * dp_Factor, 0) '+ LvDetalle.ListItems(i).SubItems(15),0)
                            
                        End If
                        
                            
                    Next
                    
                    
                    LvDetalle.ListItems(i).SubItems(21) = 0
                    
                    
                    
                    
                    If CDbl(TxtExento) > 0 Then
                        Dp_Factor2 = CDbl(TxtExento) / CDbl(TxtGtotal) * 100
                        
                        LvDetalle.ListItems(i).SubItems(17) = (CDbl(LvDetalle.ListItems(i).SubItems(17)) - (CDbl(LvDetalle.ListItems(i).SubItems(17)) / 100 * Dp_Factor2)) * CDbl(LvDetalle.ListItems(i).SubItems(5))
                        LvDetalle.ListItems(i).SubItems(21) = CDbl(LvDetalle.ListItems(i).SubItems(20)) - CDbl(LvDetalle.ListItems(i).SubItems(17))
                    Else
                        LvDetalle.ListItems(i).SubItems(17) = CDbl(LvDetalle.ListItems(i).SubItems(17)) * CDbl(LvDetalle.ListItems(i).SubItems(5))
                    End If
                    
                    
                End If
          '  End If
        Next
    'Else
        'Valores brutos
        
        
            
   ' End If
End Sub

Private Sub CmdOkImp_Click()
    If CDbl(TxtSubTotal) = 0 Then
        MsgBox "Faltan valores", vbInformation
        TxtSubTotal.SetFocus
        Exit Sub
    End If
    If Len(TxtCodigo) = 0 Then
        MsgBox "Falta C�digo...", vbInformation
        TxtCodigo.SetFocus
        Exit Sub
    End If
    If TxtCodigo <> "0" And TxtCantidad <= 0 Then
        MsgBox "Falta cantidad...", vbInformation
        TxtCodigo.SetFocus
        Exit Sub
    End If
    If CboCentroCosto.ListIndex = -1 Or CboCuenta.ListIndex = -1 Or Me.CboItemGasto.ListIndex = -1 Or CboArea.ListIndex = -1 Then
            MsgBox "Seleccione Datos de Cuenta, Area, Centro Costo e Item de gasto ... ", vbInformation
           ' CboCuenta.SetFocus
            Exit Sub
    End If
    
    '28-12-2018
    'aqui comprobaremos que el precio de venta, esta ok con el margen
    
    If FraPVenta.Visible Then
        If Val(txtMargen) = 0 Then
            MsgBox "Debe establecer el margen para venta...", vbInformation
            txtMargen.SetFocus
            Exit Sub
        End If
        'TxtPventaBruto
    
    End If
    
    If (SP_Rut_Activo = "76.553.302-3" Or SP_Rut_Activo = "76.881.179-2") Or Sm_MargenMarca = "SI" And Val(TxtCodigo) > 0 Then 'kyr
        'si es kyr comprobar el precio de venta sugerido
        If CDbl(TxtKyRPrecioActual) < CDbl(Me.TxtKyRSugerido) Then
        
            'si es total gomas haremos al final esta comprobacion
            '1 Agosto 2019
            If SP_Rut_Activo = "76.178.895-7" Then
                txtPventa = TxtKyRSugerido
                Me.TxtKyRPrecioActual = txtPventa
            Else
                
                If MsgBox("El precio de venta actual es menor al sugerido..." & vbNewLine & " �Actualizar al precio de venta sugerido? ", vbOKCancel + vbQuestion) = vbOK Then
                
                    txtPventa = TxtKyRSugerido
                    Me.TxtKyRPrecioActual = txtPventa
                Else
                    Exit Sub
                End If
            End If
        ElseIf CDbl(TxtKyRPrecioActual) > CDbl(Me.TxtKyRSugerido) Then
               If MsgBox("Seguro en rebajar el precio desde $" & TxtKyRPrecioActual & vbNewLine & "$" & TxtKyRSugerido, vbOKCancel) = vbCancel Then Exit Sub
                txtPventa = TxtKyRSugerido
                Me.TxtKyRPrecioActual = txtPventa
        
        
        End If
        
    
    
    End If
    
    
    
    'Si es con plan de cuenta    'y es Activo fijo con analisis.    'Debe llevar un codigo real.
    If CboCuenta.Visible Then
        If TxtCodigo = "0" Then
            Sql = "SELECT pla_id " & _
                    "FROM con_plan_de_cuentas " & _
                    "WHERE tpo_id=1 AND det_id=4 AND pla_analisis='SI' AND pla_id=" & CboCuenta.ItemData(CboCuenta.ListIndex)
            Consulta RsTmp, Sql
            If RsTmp.RecordCount > 0 Then
                MsgBox "La cuenta es de Activo Fijo con Analisis de cuenta.." & vbNewLine & "Debe ingresar producto..."
                TxtCodigo = ""
                TxtCodigo.SetFocus
                Exit Sub
            
            End If
            
        End If
    End If
    
    
    
    If Not BuscaItem(TxtCodigo) Then Exit Sub
    
    Set Item = LvDetalle.ListItems.Add
    Item = ""
    With Item
        .SubItems(1) = TxtCodigo
        .SubItems(2) = Me.TxtDescrp
        .SubItems(3) = TxtMarca
        .SubItems(4) = TxtPrecio
        .SubItems(5) = TxtCantidad
        .SubItems(6) = TxtUme
        .SubItems(7) = TxtSubTotal
        .SubItems(12) = 0
        .SubItems(13) = 0
        .SubItems(14) = 0
        .SubItems(15) = TxtSubTotal
        .SubItems(8) = CboCuenta.ItemData(CboCuenta.ListIndex)
        .SubItems(9) = CboCentroCosto.ItemData(CboCentroCosto.ListIndex)
        .SubItems(10) = Me.CboItemGasto.ItemData(CboItemGasto.ListIndex)
        .SubItems(11) = CboArea.ItemData(CboArea.ListIndex)
        .SubItems(18) = TxtDescrp.Tag
        If CboCentroCosto.Visible Then
            .SubItems(22) = CboCentroCosto.Text
        Else
            .SubItems(22) = CboCentroCosto.Text = ""
        End If
        
        
        .SubItems(23) = txtMargen
        .SubItems(24) = 0 'txtDiferencia
        
        If Sm_EmpVendeBrutoNeto = "NETO" Then
            .SubItems(25) = txtPventa
        Else
            .SubItems(25) = TxtPventaBruto
        End If
        
        If SP_Rut_Activo = "76.553.302-3" Or SP_Rut_Activo = "76.881.179-2" Or Sm_MargenMarca = "SI" Then
            'si es kyr dejaremos el precio de venta actual, que pudo ser el sugerido
            .SubItems(25) = Me.TxtKyRPrecioActual
            .SubItems(27) = "SI"
            .ListSubItems(2).ForeColor = vbBlue
        End If
        
        
        If SP_Rut_Activo = "12.072.366-9" Then
            'mario acu�a, acutalizar nuevo precio de venta
            .SubItems(25) = CDbl(Me.TxtNuevoPrecioDeVenta)
                
        End If
        If CmdBuscaActivo.Visible Then
            .SubItems(26) = "SI"
        Else
            .SubItems(26) = "NO"
        End If
        .SubItems(28) = TxtCodigo.Tag
        
    End With
    FraPVenta.Visible = False
    Limpia
    ProrogateaDscto
    If SP_Control_Inventario = "NO" Then TxtCodigo = "0"
    
    TxtCodigo.SetFocus
End Sub
Private Sub Limpia()
    TxtCodigo = ""
    TxtDescrp = ""
    TxtDescrp.Tag = ""
    TxtMarca = ""
    TxtPrecio = 0
    TxtCantidad = 0
    TxtSubTotal = 0
    TxtUme = "UM"
    If Sm_Nota = "CREDITO" And Sm_Modifica_Valores = "NO" And Sm_Modifica_Unidades = "SI" Then
        txtTotal = 0
    Else
        txtTotal = NumFormat(TotalizaColumna(LvDetalle, "total"))
    End If
    TxtGtotal = NumFormat(CDbl(txtTotal) - CDbl(txtDescuento))
    FrameKyR.Visible = False
End Sub

Private Sub CmdSalir_Click()
    If MsgBox("�Salir sin grabar...?", vbOKCancel + vbQuestion) = vbCancel Then Exit Sub
    Sp_Accion = "SALIR"
    Bp_Graba = False
    Unload Me
End Sub
Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        On Error Resume Next
        SendKeys "{tab}"
        KeyAscii = 0
    Else
        If Me.ActiveControl.Name = "TxtDescrp" Or Me.ActiveControl.Name = "TxtCodigo" Then
            KeyAscii = Asc(UCase(Chr(KeyAscii)))
        Else
            KeyAscii = AceptaSoloNumeros(KeyAscii)
        End If
    End If
End Sub

Private Sub Form_Load()
  '  ConexionBD
    Sm_MargenMarca = "NO"
    Centrar Me, False
    
    
    If SP_Control_Inventario = "SI" Then
        LLenarCombo CboMotivoNC, "tnc_nombre", "tnc_id", "ntc_tipos", "tnc_activo='SI'"
    Else
        LLenarCombo CboMotivoNC, "tnc_nombre", "tnc_id", "ntc_tipos", "tnc_activo='SI' AND tnc_id IN(1,4)"
    End If
    CboTipoCodigo.ListIndex = 0
    CboMotivoNC.ListIndex = 0
    LLenarCombo CboCuenta, "CONCAT(pla_nombre,'-',MID(tpo_nombre,1,2))", "pla_id", "con_plan_de_cuentas join con_tipo_de_cuenta USING(tpo_id) ", "pla_activo='SI'"
    LLenarCombo CboCentroCosto, "cen_nombre", "cen_id", "par_centros_de_costo", "cen_activo='SI' AND rut_emp='" & SP_Rut_Activo & "'"
    LLenarCombo Me.CboItemGasto, "gas_nombre", "gas_id", "par_item_gastos", "gas_activo='SI' AND rut_emp='" & SP_Rut_Activo & "'"
    LLenarCombo CboArea, "are_nombre", "are_id", "par_areas", "are_activo='SI' AND rut_emp='" & SP_Rut_Activo & "'"
    If CboCuenta.ListCount = 1 Then CboCuenta.ListIndex = 0
    If CboCentroCosto.ListCount = 1 Then CboCentroCosto.ListIndex = 0
    If CboItemGasto.ListCount = 1 Then CboItemGasto.ListIndex = 0
    If CboArea.ListCount = 1 Then CboArea.ListIndex = 0
    Busca_Id_Combo CboCuenta, 99999
    'CONSULTAMOS SI LA EMPRESA ACTIVA
    FrameDatos.Visible = False
    'LLEVA CENTRO DE COSTO, AREAS
    '8 OCTUBRE 2011
    Sql = "SELECT emp_centro_de_costos,emp_areas,emp_item_de_gastos,emp_cuenta,emp_actualiza_pv_compra actpv,emp_venta_precios_brutos  " & _
          "FROM sis_empresas " & _
          "WHERE rut='" & SP_Rut_Activo & "'"
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        Sm_ActualizaPventa = RsTmp!actpv
        Sm_EmpVendeBrutoNeto = RsTmp!emp_venta_precios_brutos
        If RsTmp!emp_centro_de_costos = "NO" Then
            LLenarCombo CboCentroCosto, "cen_nombre", "cen_id", "par_centros_de_costo", "cen_id=99999"
            CboCentroCosto.ListIndex = 0
            CboCentroCosto.Visible = False
            SkCC.Visible = False
        Else
            FrameDatos.Visible = True
        End If
        If RsTmp!emp_areas = "NO" Then
            SkArea.Visible = False
            LLenarCombo CboArea, "are_nombre", "are_id", "par_areas", "are_id=99999"
            CboArea.ListIndex = 0
            CboArea.Visible = False
        Else
            FrameDatos.Visible = True
        End If
        If RsTmp!emp_item_de_gastos = "NO" Then
            SkItem.Visible = False
            LLenarCombo CboItemGasto, "gas_nombre", "gas_id", "par_item_gastos", "gas_id=99999"
            CboItemGasto.ListIndex = 0
            CboItemGasto.Visible = False
        Else
            FrameDatos.Visible = True
        End If
        If RsTmp!emp_cuenta = "NO" Then
            CmdCuenta.Visible = False
            LLenarCombo CboCuenta, "pla_nombre", "pla_id", "con_plan_de_cuentas", "pla_id=99999"
            CboCuenta.ListIndex = 0
            CboCuenta.Visible = False
        Else
            FrameDatos.Visible = True
        End If
    End If
    If SP_Control_Inventario = "NO" Then
        TxtCodigo = "0"
        TxtDescrp.Locked = True
        CmdBuscaProducto.Enabled = False
    End If
    
    '8 Julio 2019
    'esto es si tenemos margen en las marcas, y lo sugerimos como precio de venta
    Sql = "SELECT par_valor " & _
            "FROM tabla_parametros " & _
            "WHERE par_id=30000"
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        If RsTmp!par_valor = "SI" Then
            Sm_MargenMarca = "SI"
        End If
    End If
    CboCuenta.ListIndex = 0
    
End Sub
Public Sub CargaReferencia()
        Dim Sp_Where As String
        If Sm_Referencia <> Empty Then
            Sql = "SELECT doc_factura_guias,no_documento,doc_id " & _
                        "FROM com_doc_compra " & _
                        "WHERE id=" & Sm_Referencia & " AND  rut_emp='" & SP_Rut_Activo & "'"
            Consulta RsTmp, Sql
            If RsTmp.RecordCount > 0 Then
                If RsTmp!doc_factura_guias = "SI" Then
                    Sp_Where = "WHERE d.id IN( (SELECT id FROM com_doc_compra WHERE id_ref=" & Sm_Referencia & " AND nro_factura=" & RsTmp!no_documento & " AND doc_id_factura=" & RsTmp!doc_id & "))"
                Else
                    Sp_Where = "WHERE d.id =" & Sm_Referencia
                End If
            End If
        
            Sql = "SELECT  cmd_id,pro_codigo,IFNULL(descripcion,cmd_detalle),tip_nombre," & _
                                        "IF(c.com_bruto_neto='NETO',cmd_unitario_neto+(cmd_exento/cmd_cantidad),cmd_unitario_bruto) unitario," & _
                                        "cmd_cantidad,ume_nombre," & _
                                        "IF(c.com_bruto_neto='NETO',cmd_total_neto+cmd_exento,cmd_total_bruto) totallinea," & _
                                        "d.pla_id, d.cen_id, d.gas_id, d.are_id, 0, 0, 0,IF(c.com_bruto_neto='NETO',cmd_total_neto+cmd_exento,cmd_total_bruto) , 0,0,  pro_inventariable,0,0,0,0,0,0,0,'NO' " & _
                                        "FROM    com_doc_compra_detalle d " & _
                                        "INNER JOIN com_doc_compra c ON d.id=c.id " & _
                                        "LEFT JOIN maestro_productos m ON m.codigo = d.pro_codigo AND m.rut_emp='" & SP_Rut_Activo & "' " & _
                                        "LEFT JOIN par_tipos_productos USING(tip_id) " & _
                                        "LEFT JOIN sis_unidad_medida USING(ume_id) " & _
                                        Sp_Where
            Consulta RsTmp, Sql
            LLenar_Grilla RsTmp, Me, Me.LvDetalle, False, True, True, False
            If LvDetalle.ListItems.Count > 0 Then
                Sm_Codigos = 0
                For i = 1 To LvDetalle.ListItems.Count
                    Sm_Codigos = Sm_Codigos & "," & LvDetalle.ListItems(i).SubItems(1)
                Next
            End If
        End If
        txtTotal = NumFormat(TotalizaColumna(LvDetalle, "total"))
        TxtGtotal = txtTotal
        If Sm_Nota = "CREDITO" Then
            CreditoAnula
        End If
End Sub
Private Sub CreditoAnula()
        CmdOkImp.Enabled = False
        CmdBuscaProducto.Enabled = False
        TxtCodigo.Enabled = False
        TxtCantidad.Enabled = False
        TxtSubTotal.Enabled = False
End Sub
Private Sub LvDetalle_Click()
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    With LvDetalle.SelectedItem
        Busca_Id_Combo CboCuenta, .SubItems(8)
        Busca_Id_Combo CboCentroCosto, .SubItems(9)
        Busca_Id_Combo CboItemGasto, .SubItems(10)
        Busca_Id_Combo CboArea, .SubItems(11)
    End With
    
End Sub

Private Sub LvDetalle_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    If SP_Rut_Activo <> "76.178.895-7" Then
        ordListView ColumnHeader, Me, LvDetalle
    End If
End Sub

Private Sub LvDetalle_DblClick()
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    If CmdOkImp.Enabled = False Then Exit Sub
    With LvDetalle.SelectedItem
        If SP_Rut_Activo = "76.063.757-2" Or SP_Rut_Activo = "10.966.790-0" Then
            TxtCodigo = .SubItems(28)
        Else
            TxtCodigo = .SubItems(1)
        
        End If
        
        TxtDescrp = .SubItems(2)
        TxtDescrp.Tag = .SubItems(18)
        TxtMarca = .SubItems(3)
        TxtPrecio = .SubItems(4)
        TxtCantidad = .SubItems(5)
        TxtUme = .SubItems(6)
        TxtSubTotal = .SubItems(7)
        Busca_Id_Combo CboCuenta, .SubItems(8)
        Busca_Id_Combo CboCentroCosto, .SubItems(9)
        Busca_Id_Combo CboItemGasto, .SubItems(10)
        Busca_Id_Combo CboArea, .SubItems(11)
      
        If Sm_ActualizaPventa = "SI" And Sm_Nota <> "CREDITO" Then
                        
                If SP_Rut_Activo <> "12.072.366-9" Then 'mario acu�a
                
                
                        
                      '  FraPVenta.Visible = True
                      '  txtMargen = .SubItems(23)
                      '  txtDiferencia = .SubItems(24)
                     '   txtPventa = .SubItems(25)
                End If
        End If
        
        
        LvDetalle.ListItems.Remove .Index
    End With
    txtTotal = NumFormat(TotalizaColumna(LvDetalle, "total"))
    
    If TxtCantidad.Enabled = True Then
        'TxtCodigo.SetFocus
        TxtCantidad.SetFocus
    Else
        If TxtSubTotal.Enabled = True Then TxtSubTotal.SetFocus
    End If
    If SP_Rut_Activo = "" Then
    
    End If
    
    
    
    ProrogateaDscto
    
    If SP_Rut_Activo = "76.063.757-2" Or SP_Rut_Activo = "10.966.790-0" Then
        TxtCodigo.SetFocus
    End If
End Sub

Private Sub Timer1_Timer()
    Timer1.Enabled = False
    On Error Resume Next
    For Each ltxts In Controls
        If (TypeOf ltxts Is TextBox) Then
            'If ltxts.Visible = False Then Exit Sub
            If ltxts.Name = Me.ActiveControl.Name Then  'Foco activo
                ltxts.BackColor = IIf(ltxts.Locked, ClrDesha, ClrCfoco)
            Else
                ltxts.BackColor = IIf(ltxts.Locked, ClrDesha, ClrSfoco)
            End If
        End If
    Next
End Sub
Private Sub Timer3_Timer()
    On Error Resume Next
    If TxtCodigo.Enabled Then TxtCodigo.SetFocus Else LvDetalle.SetFocus
    Timer3.Enabled = False
'    Timer1.Enabled = True
End Sub

Private Sub TxtCantidad_Change()
    If Val(TxtCantidad) > 0 Then
     '   If SP_Rut_Activo = "" Or SP_Rut_Activo = "" Then
            TxtSubTotal = NumFormat(Int(Val(TxtCantidad) * CDbl(TxtPrecio)))
     '   End If
        'TxtMultiplica = NumFormat(Int(Val(TxtCantidad) * CDbl(TxtPrecio)))
    End If
End Sub

Private Sub TxtCantidad_GotFocus()
    On Error Resume Next
    En_Foco Me.ActiveControl
    TxtStockActual.Visible = True
  '  TxtMultiplica.Visible = True
End Sub
Private Sub TxtCantidad_KeyPress(KeyAscii As Integer)
    KeyAscii = AceptaSoloNumeros(KeyAscii)
End Sub

Private Sub TxtCantidad_LostFocus()
    TxtStockActual.Visible = False
  '  TxtMultiplica.Visible = False
End Sub

Private Sub TxtCantidad_Validate(Cancel As Boolean)
    ValidarControl "#0"
End Sub

Private Sub TxtCodigo_GotFocus()
    En_Foco ActiveControl
    CmdBuscaActivo.Visible = False
    'Si es con plan de cuenta    'y es Activo fijo con analisis.    'Debe llevar un codigo real.
    If CboCuenta.Visible And CboCuenta.ListIndex > -1 Then
         Sql = "SELECT pla_id " & _
                    "FROM con_plan_de_cuentas " & _
                    "WHERE tpo_id=1 AND det_id=4 AND pla_analisis='SI' AND pla_id=" & CboCuenta.ItemData(CboCuenta.ListIndex)
            Consulta RsTmp, Sql
            If RsTmp.RecordCount > 0 Then
                Me.CmdBuscaActivo.Visible = True
            End If
    
    End If
End Sub
Private Sub VerHistorialProducto(HCodigo As String)
    'Primero, historico Ventas
    '14 Marzo 2016
   ' Me.TxtPrecioVentaBruto = NumFormat(CDbl(LvDetalle.SelectedItem.SubItems(6)))
    Me.txtPVentaNeto = NumFormat(CDbl(TxtPrecioVentaBruto) / 1.19)
    
    
    
    'Me.TxtPCompraNeto = NumFormat(CDbl(LvDetalle.SelectedItem.SubItems(4)))
    Me.TxtPcompraBruto = NumFormat(CDbl(Me.TxtPCompraNeto) * 1.19)
    
    Me.txtDifBruto = NumFormat(CDbl(TxtPrecioVentaBruto) - CDbl(TxtPcompraBruto))
    Me.TxtDifNeto = NumFormat(CDbl(txtPVentaNeto) - CDbl(TxtPCompraNeto))
    SkXcien = ""
    If CDbl(TxtPCompraNeto) > 0 And CDbl(TxtDifNeto) > 0 Then
        SkXcien = (CDbl(TxtDifNeto) / CDbl(TxtPCompraNeto)) * 100 & "%"
    End If
    Sql = "SELECT m.fecha,nombre_cliente,doc_nombre,m.no_documento,c.precio_final,c.unidades,c.subtotal " & _
              "FROM ven_doc_venta m " & _
              "INNER JOIN sis_documentos d USING(doc_id),ven_detalle c " & _
              "WHERE ven_informa_venta = 'SI' AND c.rut_emp='" & SP_Rut_Activo & "' AND doc_nota_de_venta='NO' AND m.rut_emp='" & SP_Rut_Activo & "' AND c.doc_id=m.doc_id AND c.no_documento=m.no_documento AND codigo ='" & HCodigo & "' " & _
              "ORDER BY m.fecha"
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, Me.LvVentas, False, True, True, False
    
    Sql = "SELECT concat(year(fecha),'-',month(fecha)),SUM(unidades) cantidad " & _
                "FROM    ven_detalle c " & _
                "WHERE c.rut_emp = '" & SP_Rut_Activo & "' AND codigo = " & HCodigo & " " & _
                "GROUP BY    year (c.fecha),month(fecha) " & _
                "ORDER BY    year (c.fecha),month(fecha)"
    Consulta RsTmp, Sql
        LLenar_Grilla RsTmp, Me, Me.LvVtaMensual, False, True, True, False
    
    
    'Ahora las compras ' P R O V E D O R E S
    Sql = "SELECT m.fecha,nombre_empresa,doc_nombre,no_documento,c.cmd_unitario_neto,c.cmd_cantidad,c.cmd_total_neto " & _
              "FROM com_doc_compra m " & _
              "INNER JOIN com_doc_compra_detalle c ON m.id = c.id " & _
              "INNER JOIN sis_documentos d USING(doc_id) " & _
              "JOIN maestro_proveedores p ON m.rut=p.rut_proveedor " & _
              "WHERE d.doc_orden_de_compra='NO' AND m.rut_emp='" & SP_Rut_Activo & "' AND pro_codigo ='" & HCodigo & "' " & _
              "ORDER BY m.fecha"
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, Me.LvDocumentos, False, True, True, False

End Sub

Private Sub BuscaActivo()
        
            Sql = "SELECT pla_id " & _
                    "FROM con_plan_de_cuentas " & _
                    "WHERE tpo_id=1 AND det_id=4 AND pla_analisis='SI' AND pla_id=" & CboCuenta.ItemData(CboCuenta.ListIndex)
            Consulta RsTmp, Sql
            If RsTmp.RecordCount > 0 Then
                SG_codigo = Empty
                Con_ActivoInmovilizado.CmdSelecciona.Visible = True
                Con_ActivoInmovilizado.Show 1
                If Val(SG_codigo) > 0 Then
                    Sql = "SELECT aim_nombre,aim_valor " & _
                            "WHERE aim_id=" & Val(SG_codigo)
                    Consulta RsTmp, Sql
                    If RsTmp.RecordCount > 0 Then
                        TxtCodigo = SG_codigo
                        TxtDescrp = RsTmp!aim_nombre
                        TxtPrecio = RsTmp!aim_valor
                        TxtSubTotal = RsTmp!aim_valor
                        TxtCantidad = 1
                    End If
                End If
            End If
    




End Sub



Private Sub TxtCodigo_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 112 Then
        CmdBuscaProducto_Click
    End If
End Sub

Private Sub TxtCodigo_Validate(Cancel As Boolean)
    Dim pv_Respuesta As Variant, RsTmp2 As Recordset
    Dim Sp_FiltroCodigo As String
    Dim Sp_Cod_Ingresado As String
    bp_PermiteOk = False
    If Len(TxtCodigo) = 0 Then Exit Sub
    Bp_VuelveFoco = False
    Sp_Cod_Ingresado = TxtCodigo
    SSTab1.Visible = False
    If Trim(TxtCodigo.Text) = "0" Then
        
        If SP_Control_Inventario = "SI" Then
            pv_Respuesta = MsgBox("�Ingresar Descripcion manual...?" & vbNewLine & "  (no afecta inventarios) ... ", vbQuestion + vbYesNoCancel)
            If pv_Respuesta = vbNo Then
                TxtDescrp.Locked = True
                Cancel = True
                bp_Ingresa_Gasto = False
                Exit Sub
            ElseIf pv_Respuesta = vbCancel Then
                TxtDescrp.Locked = True
                bp_Ingresa_Gasto = False
                Cancel = False
                Exit Sub
            Else
                Bp_VuelveFoco = True
                TxtCantidad = 1
                TxtDescrp = ""
                TxtMarca = ""
                TxtCantidad.Locked = True
                TxtDescrp.BackColor = vbWhite
                TxtDescrp.TabStop = True
                TxtDescrp.Locked = False
                TxtDescrp.SetFocus
                bp_Ingresa_Gasto = True
                Cancel = False
                Exit Sub
            End If
        Else
                TxtCantidad = 1
                TxtDescrp = ""
                TxtMarca = ""
                TxtCantidad.Locked = True
                TxtDescrp.BackColor = vbWhite
                TxtDescrp.TabStop = True
                TxtDescrp.Locked = False
                TxtDescrp.SetFocus
                bp_Ingresa_Gasto = True
                Cancel = False
                Exit Sub
        End If
    Else
        'Nada
    End If
    Sp_FiltroCodigo = Empty
    If Sm_Nota = "DEBITO" Then
        If Len(Sm_Codigos) > 0 Then
            Sp_FiltroCodigo = " AND m.codigo IN(" & Sm_Codigos & ") "
        Else
            Sp_FiltroCodigo = " AND m.codigo='" & TxtCodigo & "' "
        End If
    End If
    
    
    
    '24 Sep 16
    'Buscaremos por tipo de codigo seleccionado.
    If CboTipoCodigo.Text = "Codigo Sistema" Then
        Sp_FiltroCodigo = " AND codigo='" & TxtCodigo & "' "
    ElseIf CboTipoCodigo.Text = "Codigo Empresa" Then
        Sp_FiltroCodigo = " AND pro_codigo_interno='" & TxtCodigo & "' "
    ElseIf CboTipoCodigo.Text = "Codigo Proveedor" Then
        Sql = "SELECT pro_codigo " & _
                "FROM par_codigos_proveedor " & _
                "WHERE rut_proveedor='" & compra_Ingreso.TxtRutProveedor & "' AND cpv_codigo_proveedor='" & TxtCodigo & "'"
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
            Sp_FiltroCodigo = " AND codigo='" & RsTmp!pro_codigo & "' "
                
        Else
            MsgBox "Codigo proveedor no encontrado...", vbExclamation
            Exit Sub
        
        End If
        
        
    End If
    
    Sql = "SELECT codigo, descripcion,precio_compra,0 stock_actual,tip_nombre tipo,ume_nombre,m.pro_inventariable inventariable,precio_venta,porciento_utilidad,margen, " & _
                "(SELECT mar_margen FROM par_marcas mm WHERE mm.mar_id=m.mar_id) margen_m " & _
            "/*IFNULL((SELECT cmd_unitario_neto " & _
                    "FROM com_doc_compra_detalle d " & _
                    "JOIN com_doc_compra c ON d.id=c.id " & _
                    "WHERE d.pro_codigo = m.codigo And c.rut_emp = m.rut_emp " & _
                    "ORDER BY c.id DESC " & _
                    "LIMIT 1),precio_compra) uprecio */" & _
          "FROM maestro_productos m,par_tipos_productos t,sis_unidad_medida u " & _
          "WHERE u.ume_id=m.ume_id AND m.rut_emp='" & SP_Rut_Activo & "' AND  m.tip_id=t.tip_id " & Sp_FiltroCodigo & Sp_FiltroCodigo
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
                'Codigo ingresado ha sido encontrado
            TxtCodigo.Tag = TxtCodigo
            TxtCodigo = RsTmp!Codigo
            TxtCantidad = 0
            TxtCantidad.Locked = False
            TxtDescrp.TabStop = False
            
            If Sm_ActualizaPventa = "SI" And Sm_Nota <> "CREDITO" Then
                If SP_Rut_Activo <> "12.072.366-9" Then 'mario acu�a
            
                    FraPVenta.Visible = True
                End If
            End If
            
            'TxtDescrp.Locked = False
            With RsTmp
            
                txtMargen = "" & !porciento_utilidad
                If Sm_MargenMarca = "SI" Then
                    txtMargen = "" & !margen_m
                    
                
                End If
                txtDiferencia = "" & !Margen
                
                If Sm_EmpVendeBrutoNeto = "BRUTO" Then
                    TxtPVBrutoActual = Round(!precio_venta, 0)
                    Me.TxtPventaBruto = Round(!precio_venta, 0)
                    
                    
                    txtPventa = NumFormat(Round(!precio_venta, 0) / Val("1." & DG_IVA))
                    TxtPVActual = txtPventa
                Else
                    
                    txtPventa = Round(!precio_venta, 0)
                    Me.TxtPVActual = Round(!precio_venta, 0)
                    
                    TxtPVBrutoActual = NumFormat(Round(!precio_venta, 0) * (Val("1." & DG_IVA)))
                    Me.TxtPventaBruto = TxtPVBrutoActual
                    
                    
                End If
                'veterinaria
                TxtPrecioVentaBruto = NumFormat(Round(!precio_venta, 0))
                TxtPCompraNeto = NumFormat(!precio_compra)
                
                If SP_Rut_Activo = "76.063.757-2" Or SP_Rut_Activo = "10.966.790-0" Then
                    Me.TxtDescrp = Sp_Cod_Ingresado & " -> " & !Descripcion
                Else
                    Me.TxtDescrp = !Descripcion
                End If
                TxtDescrp.Tag = !inventariable
                Me.TxtMarca.Text = !Tipo
                Me.TxtPrecio.Text = !precio_compra
                TxtUme = !ume_nombre
                
                'Me.LbStockActual.Caption = !stock_actual
                'Me.TxtStockActual.Text = !stock_actual
                Sql = "SELECT SUM(sto_stock) stock,pro_ultimo_precio_compra pucom " & _
                     "FROM pro_stock " & _
                     "WHERE rut_emp='" & SP_Rut_Activo & "' AND  pro_codigo='" & TxtCodigo & "' " & _
                     "GROUP By pro_codigo"
                Consulta RsTmp2, Sql
                If RsTmp2.RecordCount > 0 Then
                    Me.TxtStockActual.Text = "STOCK ACTUAL : " & RsTmp2!stock
                    TxtPrecio = Format(RsTmp2!pucom, "#,##0")
                End If
                
                If TxtCantidad.Enabled = True Then Me.TxtCantidad.SetFocus
                
                bp_PermiteOk = True
                
                
            
            
                'Si es veterinaria curacautin
                If SP_Rut_Activo = "12.072.366-9" Then
                        SSTab1.Visible = True
                        TxtNuevoPrecioDeVenta = Me.TxtPrecioVentaBruto
                        VerHistorialProducto TxtCodigo
                End If
                'bast y kyr y tg
                If SP_Rut_Activo = "76.553.302-3" Or SP_Rut_Activo = "76.881.179-2" Or Sm_MargenMarca = "SI" Then ' KyR
                    If SP_Rut_Activo = "76.178.895-7" Then
                        
                    Else
                        FrameKyR.Visible = True
                        Me.TxtKyRMargen = txtMargen
                        Me.TxtKyRPrecioActual = Round(!precio_venta, 0)
                        Me.TxtKyRSugerido = Round(!precio_venta, 0)
                    End If
                End If
            End With
            
    Else
            If Sm_Nota <> Empty Then
                MsgBox "El codigo ingresado no est� en el documento Original...", vbInformation
                TxtCodigo = ""
            
                
                
                
            Else
                Respuesta = MsgBox("Codigo no encontrado..." & Chr(13) & "�Crear nuevo producto?", vbYesNo + vbQuestion)
                If Respuesta = 6 Then
                    'Crear nuevo
                    
                    
                    SG_codigo = Empty
                    If Len(SG_Formulario_Ficha_Producto) > 0 Then
                        'traemos el formulario de la bd 'parametro 27000
                                Set Form_Ficha = Forms.Add(SG_Formulario_Ficha_Producto)
                                SG_codigo = ""
                                Form_Ficha.Bm_Nuevo = True
                                Form_Ficha.Show 1
                                If SG_codigo3 = "GRABO" Then
                                     GoTo Saliendo
                                Else
                                    Exit Sub
                                End If
                                    
                    End If
                    
                    
                    SG_codigo = Empty
                    AgregarProducto.Bm_Nuevo = True
                    AgregarProducto.TxtCodigo.Text = Me.TxtCodigo.Text
                    If SG_Es_la_Flor = "SI" Then
                        AgregarProductoFlor.Show 1
                     Else
                        AgregarProducto.Show 1
                    End If
                    Cancel = True
                Else
                    TxtCodigo.Text = ""
                    Me.TxtDescrp.Text = ""
                    TxtDescrp.Tag = ""
                    Me.TxtMarca.Text = ""
                    Me.TxtPrecio.Text = ""
                    TxtCantidad = ""
                    Me.TxtCodigo.SetFocus
                End If
            End If
    End If
Saliendo:
    

End Sub



Private Sub TxtDescrp_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub TxtDescrp_LostFocus()
    TxtSubTotal.SetFocus
End Sub

Private Sub txtDescuento_GotFocus()
    En_Foco ActiveControl
End Sub

Private Sub txtDescuento_KeyPress(KeyAscii As Integer)
    KeyAscii = AceptaSoloNumeros(KeyAscii)
End Sub

Private Sub txtDescuento_Validate(Cancel As Boolean)
    ValidarControl
    
    If CDbl(txtDescuento) > CDbl(txtTotal) Then
        MsgBox "Descuento no puede ser mayor al total...", vbInformation
        txtDescuento = 0
        TxtGtotal = txtTotal
        QuitaDscto
        Exit Sub
    End If
    
    ProrogateaDscto
End Sub
Private Sub QuitaDscto()
        For i = 1 To LvDetalle.ListItems.Count
            If CDbl(txtotal) > 0 Then LvDetalle.ListItems(i).SubItems(12) = Format(CDbl(LvDetalle.ListItems(i).SubItems(7) / CDbl(txtTotal) * 100), "#.#0")
            LvDetalle.ListItems(i).SubItems(13) = CDbl(txtDescuento) / 100 * CDbl(LvDetalle.ListItems(i).SubItems(12))
           ' If CDbl(LVDetalle.ListItems(i).SubItems(13)) = 0 Then
           '     LVDetalle.ListItems(i).SubItems(14) = 0
           '     LVDetalle.ListItems(i).SubItems(15) = 0
           ' Else
           On Error Resume Next
                LvDetalle.ListItems(i).SubItems(14) = (CDbl(LvDetalle.ListItems(i).SubItems(7)) - CDbl(LvDetalle.ListItems(i).SubItems(13))) / CDbl(LvDetalle.ListItems(i).SubItems(5))
                LvDetalle.ListItems(i).SubItems(15) = CDbl(LvDetalle.ListItems(i).SubItems(7)) - CDbl(LvDetalle.ListItems(i).SubItems(13))
            'End If
        Next

End Sub


Private Sub txtDiferencia_GotFocus()
    En_Foco txtDiferencia
End Sub

Private Sub txtDiferencia_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
End Sub

Private Sub TxtKyRSugerido_GotFocus()
    En_Foco TxtKyRSugerido
End Sub

Private Sub TxtKyRSugerido_KeyPress(KeyAscii As Integer)
    KeyAscii = AceptaSoloNumeros(KeyAscii)
End Sub


Private Sub TxtKyRSugerido_Validate(Cancel As Boolean)
    If Val(TxtKyRSugerido) = 0 Then TxtKyRSugerido = "0"
End Sub

Private Sub txtMargen_GotFocus()
    En_Foco txtMargen
End Sub

Private Sub txtMargen_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
End Sub

Private Sub txtMargen_Validate(Cancel As Boolean)
    If Val(txtMargen) = 0 Then Exit Sub
    CalculaPV
End Sub

Private Sub TxtNroCuenta_GotFocus()
    En_Foco TxtNroCuenta
End Sub
Private Sub TxtNroCuenta_KeyPress(KeyAscii As Integer)
    KeyAscii = AceptaSoloNumeros(KeyAscii)
End Sub
Private Sub TxtNroCuenta_Validate(Cancel As Boolean)
    If Val(TxtNroCuenta) = 0 Then Exit Sub
    Busca_Id_Combo CboCuenta, Val(TxtNroCuenta)
    If CboCuenta.ListIndex = -1 Then
        MsgBox "Cuenta no existe..."
    End If
End Sub

Private Sub TxtNuevoPrecioDeVenta_GotFocus()
    En_Foco TxtNuevoPrecioDeVenta
End Sub

Private Sub TxtNuevoPrecioDeVenta_KeyPress(KeyAscii As Integer)
    KeyAscii = AceptaSoloNumeros(KeyAscii)
End Sub

Private Sub TxtNuevoPrecioDeVenta_Validate(Cancel As Boolean)
    If Val(TxtNuevoPrecioDeVenta) = 0 Then TxtNuevoPrecioDeVenta = "0"
End Sub

Private Sub TxtPrecio_Change()
    'Calcular nuevo Precio Venta de acuerdo al % que tenga el producto
    CalculaPV
End Sub
Private Sub CalculaPV()
    Dim Lp_PCompra As Long
    If Val(TxtPrecio) = 0 Then Exit Sub
    Lp_PCompra = CDbl(TxtPrecio)
    If Neto And Sm_EmpVendeBrutoNeto = "BRUTO" Then
        'Calculcar precio venta sobre precio compra neto mas iva
        Lp_PCompra = NumFormat(Lp_PCompra + Lp_PCompra * (DG_IVA / 100))
        txtPventa = NumFormat(Lp_PCompra + (Lp_PCompra / 100 * Val(txtMargen)))
        txtDiferencia = CDbl(txtPventa) - Lp_PCompra
        
    Else
        txtPventa = NumFormat(Lp_PCompra + (Lp_PCompra / 100 * Val(txtMargen)))
        txtDiferencia = CDbl(txtPventa) - Lp_PCompra
    End If
End Sub


Private Sub PVentas(DBN As String)
    '20 Jun 2015
    'Calculamos los precios de ventas brutos y  netos
    If DBN = "BRUTO" Then
        txtPventa = NumFormat(CDbl(TxtPventaBruto) / Val("1." & DG_IVA))
        
    Else
        TxtPventaBruto = NumFormat(CDbl(txtPventa) * Val("1." & DG_IVA))
    End If
End Sub
Private Sub txtPventa_GotFocus()
    En_Foco txtPventa
End Sub

Private Sub txtPventa_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
End Sub

Private Sub txtPventa_Validate(Cancel As Boolean)
    If Val(txtPventa) = 0 Then Exit Sub
    txtDiferencia = CDbl(txtPventa) - CDbl(TxtPrecio)
    If CDbl(TxtPrecio) = 0 Then Exit Sub
    txtMargen = Format(Val(txtDiferencia) * 100 / CDbl(TxtPrecio), "##")
    PVentas "NETO"
End Sub

Private Sub TxtPventaBruto_GotFocus()
    En_Foco TxtPventaBruto
End Sub

Private Sub TxtPventaBruto_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
End Sub

Private Sub TxtPventaBruto_Validate(Cancel As Boolean)
    PVentas "BRUTO"
End Sub

Private Sub TxtSubTotal_GotFocus()
    If Bp_VuelveFoco = True Then
        TxtDescrp.SetFocus
        Bp_VuelveFoco = False
        Exit Sub
    End If
    
 '   Me.TxtMultiplica.Visible = True
    En_Foco ActiveControl
End Sub
Private Sub ValidarControl(Optional ByVal Decimales As String)
    If Val(Me.ActiveControl.Text) = 0 Then
        Me.ActiveControl.Text = 0
    Else
        If Decimales = Empty Then
            Me.ActiveControl.Text = Format(Me.ActiveControl.Text, "#,0")
        Else
            Me.ActiveControl.Text = Format(Me.ActiveControl.Text, "#,0." & Decimales)
        End If
    End If
    TxtGtotal = NumFormat(CDbl(txtTotal) - CDbl(txtDescuento))
    'Aqui sumaremos todo
End Sub

Private Sub TxtSubTotal_KeyPress(KeyAscii As Integer)
    KeyAscii = AceptaSoloNumeros(KeyAscii)
End Sub

Private Sub TxtSubTotal_Validate(Cancel As Boolean)
    If TxtCodigo.Enabled = False Then Exit Sub
    ValidarControl
    If Val(TxtCantidad) = 0 Then TxtCantidad = 0
    If CDbl(TxtCantidad) = 0 Then Exit Sub
    On Error GoTo errorDivision
    TxtPrecio = Format(CDbl(TxtSubTotal) / CDbl(TxtCantidad), "#,0.#0")
    
    If SP_Rut_Activo = "76.553.302-3" Then
        TxtKyRSugerido = NumFormat(Int(CDbl(TxtPrecio) * 2))
        
    End If
    '8 marzo 2019 Bast
    If SP_Rut_Activo = "76.881.179-2" Or SP_Rut_Activo = "77.238.472-6" Then
        TxtKyRSugerido = NumFormat(Int(CDbl(TxtPrecio) * Val("1." & DG_IVA)) * Val("1." & TxtKyRMargen & ")"))
    End If
    
    '8Julio2019
    
    If Sm_MargenMarca = "SI" Then
        TxtKyRSugerido = CDbl(TxtPrecio) + Int(TxtPrecio / 100 * TxtKyRMargen)
        TxtKyRSugerido = NumFormat(Int(CDbl(TxtKyRSugerido) * Val("1." & DG_IVA))) '  * Val("1." & TxtKyRMargen & ")"))
        
        If SP_Rut_Activo = "76.178.895-7" Then
            'totalgomas x 1.8
            TxtKyRSugerido = NumFormat(Round(CDbl(TxtPrecio) * Val(CxP(TxtKyRMargen))))
        End If
        If SP_Rut_Activo = "77.238.472-6" Then
            TxtKyRSugerido = NumFormat(Int(CDbl(TxtPrecio) * Val("1." & TxtKyRMargen)))
            If Val(TxtKyRSugerido) > 0 Then 'para ruta3 cuadrar al 100 3 Nov 2020
                    Lp_Mod = CDbl(TxtKyRSugerido) Mod 100
                If Lp_Mod > 0 Then
                    'redondeo
                    If Lp_Mod >= 50 Then
                            Ip_Dif = 100 - Lp_Mod
                            TxtKyRSugerido = NumFormat(TxtKyRSugerido + Ip_Dif)
                    Else
                            Ip_Dif = Lp_Mod
                            TxtKyRSugerido = NumFormat(TxtKyRSugerido - Ip_Dif)
                    End If
                End If
            End If
        Else
                If Val(TxtKyRSugerido) > 0 Then
                        Lp_Mod = CDbl(TxtKyRSugerido) Mod 10
                        If Lp_Mod > 0 Then
                            'redondeo
                            If Lp_Mod >= 5 Then
                                    Ip_Dif = 10 - Lp_Mod
                                    TxtKyRSugerido = NumFormat(TxtKyRSugerido + Ip_Dif)
                            Else
                                    Ip_Dif = Lp_Mod
                                    TxtKyRSugerido = NumFormat(TxtKyRSugerido - Ip_Dif)
                            End If
                        End If
                End If
        End If
        
        
        
        
        
        
        
    End If
    
    
    
    Exit Sub
errorDivision:
    MsgBox "Verifique que los valores esten correctos...", vbInformation
End Sub
Public Sub ProrogateaDscto()
    TxtGtotal = NumFormat(CDbl(txtTotal) - CDbl(txtDescuento))
    If CDbl(txtDescuento) = 0 Then
        QuitaDscto
        Exit Sub
    End If
    If LvDetalle.ListItems.Count = 0 Then Exit Sub
    On Error GoTo errorDivision
    For i = 1 To LvDetalle.ListItems.Count
        LvDetalle.ListItems(i).SubItems(12) = Format(CDbl(LvDetalle.ListItems(i).SubItems(7) / CDbl(txtTotal) * 100), "#.#0")
        LvDetalle.ListItems(i).SubItems(13) = CDbl(txtDescuento) / 100 * CDbl(LvDetalle.ListItems(i).SubItems(12))
        LvDetalle.ListItems(i).SubItems(14) = (CDbl(LvDetalle.ListItems(i).SubItems(7)) - CDbl(LvDetalle.ListItems(i).SubItems(13))) / CDbl(LvDetalle.ListItems(i).SubItems(5))
        LvDetalle.ListItems(i).SubItems(15) = CDbl(LvDetalle.ListItems(i).SubItems(7)) - CDbl(LvDetalle.ListItems(i).SubItems(13))
    Next
    
    Exit Sub
errorDivision:
MsgBox "Verifique que los valores esten correctos...", vbInformation
End Sub
Private Function BuscaItem(Cadena As String) As Boolean
    BuscaItem = True
    For i = 1 To LvDetalle.ListItems.Count
        If LvDetalle.ListItems(i).SubItems(1) = Cadena And Cadena <> "0" Then
            If CboCuenta.ItemData(CboCuenta.ListIndex) = LvDetalle.ListItems(i).SubItems(8) And _
               Me.CboCentroCosto.ItemData(Me.CboCentroCosto.ListIndex) = LvDetalle.ListItems(i).SubItems(9) And _
               Me.CboItemGasto.ItemData(Me.CboItemGasto.ListIndex) = LvDetalle.ListItems(i).SubItems(10) And _
               Me.CboArea.ItemData(CboArea.ListIndex) = LvDetalle.ListItems(i).SubItems(11) Then
            
                BuscaItem = False
                MsgBox " Ya ingreso este art�culo "
                Limpia
                LvDetalle.SetFocus
                LvDetalle.ListItems(i).Selected = True
                Exit For
            End If
        End If
    Next
End Function
