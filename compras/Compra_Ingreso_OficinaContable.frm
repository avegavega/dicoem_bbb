VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form Compra_Ingreso_OficinaContable 
   Caption         =   "Ingreso Compra - Oficina Contable"
   ClientHeight    =   7410
   ClientLeft      =   2190
   ClientTop       =   2970
   ClientWidth     =   15120
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   7410
   ScaleWidth      =   15120
   Begin VB.CommandButton CmdSalir 
      Cancel          =   -1  'True
      Caption         =   "&Retornar"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   435
      Left            =   13875
      TabIndex        =   48
      ToolTipText     =   "Salir"
      Top             =   6810
      Width           =   990
   End
   Begin VB.Frame Frame6 
      Caption         =   "Documento"
      Height          =   5475
      Left            =   75
      TabIndex        =   49
      Top             =   1260
      Width           =   14970
      Begin VB.Frame FrmCaja 
         Caption         =   "Afecta Caja Actual"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   795
         Left            =   5400
         TabIndex        =   100
         Top             =   4440
         Width           =   1920
         Begin VB.ComboBox CboCaja 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            ItemData        =   "Compra_Ingreso_OficinaContable.frx":0000
            Left            =   135
            List            =   "Compra_Ingreso_OficinaContable.frx":000A
            Style           =   2  'Dropdown List
            TabIndex        =   101
            ToolTipText     =   "Seleccione Bodega"
            Top             =   300
            Width           =   1695
         End
      End
      Begin VB.CommandButton CmdNueva 
         Caption         =   "Guardar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   510
         Left            =   75
         TabIndex        =   17
         ToolTipText     =   "Nueva compra"
         Top             =   4890
         Width           =   2295
      End
      Begin VB.Frame Frame4 
         Caption         =   "Contabilizacion"
         Height          =   2400
         Left            =   8325
         TabIndex        =   90
         Top             =   2325
         Width           =   6525
         Begin VB.TextBox TxtTotalCuentas 
            Alignment       =   1  'Right Justify
            BackColor       =   &H8000000F&
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Left            =   4770
            Locked          =   -1  'True
            TabIndex        =   94
            TabStop         =   0   'False
            Text            =   "0"
            Top             =   1935
            Width           =   1470
         End
         Begin VB.CommandButton CmdOk 
            Caption         =   "Ok"
            Height          =   330
            Left            =   6120
            TabIndex        =   16
            Top             =   495
            Width           =   330
         End
         Begin VB.TextBox TxtValorCuenta 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Left            =   4740
            TabIndex        =   11
            Text            =   "0"
            Top             =   495
            Width           =   1380
         End
         Begin VB.TextBox TxtNombre 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Left            =   1095
            Locked          =   -1  'True
            TabIndex        =   93
            TabStop         =   0   'False
            Top             =   495
            Width           =   3645
         End
         Begin VB.TextBox TxtCodigoCuenta 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Left            =   75
            TabIndex        =   10
            Top             =   510
            Width           =   1000
         End
         Begin VB.CommandButton CmdCuenta 
            Caption         =   "Cuenta"
            Height          =   195
            Left            =   75
            TabIndex        =   92
            Top             =   285
            Width           =   930
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkRequerido 
            Height          =   240
            Left            =   150
            OleObjectBlob   =   "Compra_Ingreso_OficinaContable.frx":0016
            TabIndex        =   91
            ToolTipText     =   "Valor Requerido (neto + exentos)"
            Top             =   2010
            Width           =   2580
         End
         Begin MSComctlLib.ListView LvCuentaContable 
            Height          =   990
            Left            =   90
            TabIndex        =   95
            Top             =   855
            Width           =   6465
            _ExtentX        =   11404
            _ExtentY        =   1746
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   4
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Object.Tag             =   "N109"
               Text            =   "compra ID"
               Object.Width           =   0
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Object.Tag             =   "N109"
               Text            =   "Cuenta"
               Object.Width           =   1782
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   2
               Object.Tag             =   "T3000"
               Text            =   "Nombre"
               Object.Width           =   6438
            EndProperty
            BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   3
               Key             =   "total"
               Object.Tag             =   "N100"
               Text            =   "Valor"
               Object.Width           =   2443
            EndProperty
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel12 
            Height          =   240
            Left            =   1110
            OleObjectBlob   =   "Compra_Ingreso_OficinaContable.frx":009A
            TabIndex        =   96
            Top             =   300
            Width           =   2475
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel24 
            Height          =   240
            Left            =   4935
            OleObjectBlob   =   "Compra_Ingreso_OficinaContable.frx":011E
            TabIndex        =   97
            Top             =   285
            Width           =   1110
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel25 
            Height          =   195
            Left            =   3240
            OleObjectBlob   =   "Compra_Ingreso_OficinaContable.frx":0186
            TabIndex        =   98
            Top             =   2025
            Width           =   1455
         End
      End
      Begin VB.Frame FrameRef 
         Caption         =   "Seleccionar Documento Referencia"
         Height          =   1695
         Left            =   4860
         TabIndex        =   82
         Top             =   2325
         Visible         =   0   'False
         Width           =   3435
         Begin VB.CommandButton CmdBuscar 
            Caption         =   "Buscar"
            Height          =   270
            Left            =   2430
            TabIndex        =   86
            Top             =   300
            Width           =   900
         End
         Begin VB.TextBox TxtDocumentoRef 
            BackColor       =   &H00E0E0E0&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   90
            Locked          =   -1  'True
            TabIndex        =   85
            TabStop         =   0   'False
            Top             =   615
            Width           =   3225
         End
         Begin VB.TextBox TxtNroRef 
            BackColor       =   &H00E0E0E0&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   90
            Locked          =   -1  'True
            TabIndex        =   84
            TabStop         =   0   'False
            Top             =   1200
            Width           =   1620
         End
         Begin VB.TextBox TxtTotalRef 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00E0E0E0&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   1725
            Locked          =   -1  'True
            TabIndex        =   83
            TabStop         =   0   'False
            Top             =   1200
            Width           =   1590
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel18 
            Height          =   240
            Left            =   105
            OleObjectBlob   =   "Compra_Ingreso_OficinaContable.frx":01F6
            TabIndex        =   87
            Top             =   1020
            Width           =   1500
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel17 
            Height          =   195
            Left            =   105
            OleObjectBlob   =   "Compra_Ingreso_OficinaContable.frx":0260
            TabIndex        =   88
            Top             =   420
            Width           =   1365
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel19 
            Height          =   240
            Left            =   1740
            OleObjectBlob   =   "Compra_Ingreso_OficinaContable.frx":02D0
            TabIndex        =   89
            Top             =   1020
            Width           =   1500
         End
      End
      Begin VB.Frame Frame5 
         Caption         =   "Impuestos adicionales"
         Height          =   2160
         Left            =   75
         TabIndex        =   73
         Top             =   2325
         Width           =   4770
         Begin VB.TextBox txtTotal 
            Alignment       =   1  'Right Justify
            BackColor       =   &H8000000F&
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   75
            Locked          =   -1  'True
            TabIndex        =   77
            TabStop         =   0   'False
            Text            =   "0"
            Top             =   1770
            Width           =   1485
         End
         Begin VB.TextBox TxtValorImp 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   3000
            TabIndex        =   76
            Tag             =   "N"
            Text            =   "0"
            Top             =   300
            Width           =   1260
         End
         Begin VB.CommandButton CmdOkImp 
            Caption         =   "Ok"
            Height          =   270
            Left            =   4260
            TabIndex        =   75
            Top             =   300
            Width           =   345
         End
         Begin VB.TextBox TxtImpuesto 
            BackColor       =   &H00E0E0E0&
            Height          =   285
            Left            =   75
            Locked          =   -1  'True
            TabIndex        =   74
            TabStop         =   0   'False
            Top             =   300
            Width           =   2940
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkTotalAdicionales 
            Height          =   300
            Left            =   3525
            OleObjectBlob   =   "Compra_Ingreso_OficinaContable.frx":0338
            TabIndex        =   78
            Top             =   1695
            Width           =   1035
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkImpCosto 
            Height          =   180
            Left            =   2325
            OleObjectBlob   =   "Compra_Ingreso_OficinaContable.frx":0398
            TabIndex        =   79
            Top             =   1695
            Width           =   1185
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel8 
            Height          =   195
            Left            =   90
            OleObjectBlob   =   "Compra_Ingreso_OficinaContable.frx":03F8
            TabIndex        =   80
            Top             =   1575
            Width           =   1425
         End
         Begin MSComctlLib.ListView LvImpuestos 
            Height          =   975
            Left            =   75
            TabIndex        =   81
            Top             =   585
            Width           =   4545
            _ExtentX        =   8017
            _ExtentY        =   1720
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            FullRowSelect   =   -1  'True
            GridLines       =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   0
            NumItems        =   5
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Object.Tag             =   "N109"
               Text            =   "Id"
               Object.Width           =   0
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Object.Tag             =   "T5000"
               Text            =   "Impuesto"
               Object.Width           =   5186
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   2
               Object.Tag             =   "N100"
               Text            =   "Valor"
               Object.Width           =   2117
            EndProperty
            BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   3
               Object.Tag             =   "N102"
               Text            =   "FACTOR"
               Object.Width           =   0
            EndProperty
            BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   4
               Object.Tag             =   "T1000"
               Text            =   "costo/credito"
               Object.Width           =   0
            EndProperty
         End
      End
      Begin VB.Frame Frame7 
         Caption         =   "Datos del documento"
         Height          =   1935
         Left            =   60
         TabIndex        =   12
         Top             =   345
         Width           =   14640
         Begin VB.TextBox TxtRsocial 
            BackColor       =   &H00E0E0E0&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   9825
            Locked          =   -1  'True
            TabIndex        =   54
            TabStop         =   0   'False
            Top             =   540
            Width           =   4575
         End
         Begin VB.ComboBox CboTipoDoc 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            ItemData        =   "Compra_Ingreso_OficinaContable.frx":0486
            Left            =   4260
            List            =   "Compra_Ingreso_OficinaContable.frx":0493
            Style           =   2  'Dropdown List
            TabIndex        =   2
            ToolTipText     =   $"Compra_Ingreso_OficinaContable.frx":04B8
            Top             =   540
            Width           =   2955
         End
         Begin VB.TextBox TxtNDoc 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Left            =   7215
            TabIndex        =   3
            Tag             =   "N"
            Text            =   "0"
            Top             =   540
            Width           =   1290
         End
         Begin VB.CommandButton CmdNuevoProveedor 
            Caption         =   "Crear"
            Height          =   255
            Left            =   9195
            TabIndex        =   53
            Top             =   900
            Width           =   600
         End
         Begin VB.CommandButton CmdBuscaProv 
            Caption         =   "Buscar"
            Height          =   255
            Left            =   8520
            TabIndex        =   52
            Top             =   900
            Width           =   660
         End
         Begin VB.TextBox TxtRutProveedor 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   8505
            TabIndex        =   4
            Tag             =   "T"
            Top             =   540
            Width           =   1305
         End
         Begin VB.ComboBox CboPlazos 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            ItemData        =   "Compra_Ingreso_OficinaContable.frx":0562
            Left            =   1740
            List            =   "Compra_Ingreso_OficinaContable.frx":0564
            Style           =   2  'Dropdown List
            TabIndex        =   1
            Top             =   540
            Width           =   2520
         End
         Begin VB.TextBox txtIvaRetenido 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            Enabled         =   0   'False
            Height          =   285
            Left            =   7665
            TabIndex        =   8
            Tag             =   "N"
            Text            =   "0"
            Top             =   1410
            Width           =   1320
         End
         Begin VB.TextBox TxtSubTotal 
            Alignment       =   1  'Right Justify
            BackColor       =   &H8000000F&
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   11655
            Locked          =   -1  'True
            TabIndex        =   51
            TabStop         =   0   'False
            Text            =   "0"
            Top             =   1425
            Width           =   1320
         End
         Begin VB.TextBox txtIvaSinCredito 
            Alignment       =   1  'Right Justify
            BackColor       =   &H80000009&
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   9000
            TabIndex        =   9
            Tag             =   "N"
            Text            =   "0"
            Top             =   1425
            Width           =   1320
         End
         Begin VB.TextBox txtIVAMargen 
            Alignment       =   1  'Right Justify
            BackColor       =   &H8000000F&
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   10335
            Locked          =   -1  'True
            TabIndex        =   14
            Tag             =   "N"
            Text            =   "0"
            Top             =   1425
            Width           =   1320
         End
         Begin VB.TextBox txtMargenDist 
            Alignment       =   1  'Right Justify
            BackColor       =   &H8000000F&
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   12990
            Locked          =   -1  'True
            TabIndex        =   15
            Tag             =   "N"
            Text            =   "0"
            ToolTipText     =   "Dato referencial, no suma"
            Top             =   1425
            Width           =   1320
         End
         Begin VB.TextBox txtSubNeto 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   1485
            TabIndex        =   6
            Text            =   "0"
            Top             =   1425
            Width           =   1320
         End
         Begin VB.TextBox txtIVA 
            Alignment       =   1  'Right Justify
            BackColor       =   &H8000000F&
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   5475
            TabIndex        =   13
            Text            =   "0"
            Top             =   1425
            Width           =   1320
         End
         Begin VB.TextBox TxtExento 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   150
            TabIndex        =   5
            Tag             =   "N"
            Text            =   "0"
            Top             =   1425
            Width           =   1320
         End
         Begin VB.TextBox TxtDescuentoNeto 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   2805
            TabIndex        =   7
            Text            =   "0"
            Top             =   1425
            Width           =   1320
         End
         Begin VB.TextBox TxtNeto 
            Alignment       =   1  'Right Justify
            BackColor       =   &H8000000F&
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   4140
            Locked          =   -1  'True
            TabIndex        =   50
            TabStop         =   0   'False
            Text            =   "0"
            Top             =   1425
            Width           =   1320
         End
         Begin MSComCtl2.DTPicker DtFecha 
            Height          =   345
            Left            =   135
            TabIndex        =   0
            Top             =   540
            Width           =   1605
            _ExtentX        =   2831
            _ExtentY        =   609
            _Version        =   393216
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            CustomFormat    =   "dd-mm-yyyy hh:mm:ss"
            Format          =   95617025
            CurrentDate     =   39855
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
            Height          =   225
            Left            =   195
            OleObjectBlob   =   "Compra_Ingreso_OficinaContable.frx":0566
            TabIndex        =   55
            Top             =   330
            Width           =   1065
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel9 
            Height          =   195
            Left            =   1770
            OleObjectBlob   =   "Compra_Ingreso_OficinaContable.frx":05D7
            TabIndex        =   56
            Top             =   360
            Width           =   615
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
            Height          =   225
            Index           =   0
            Left            =   7170
            OleObjectBlob   =   "Compra_Ingreso_OficinaContable.frx":0638
            TabIndex        =   57
            Top             =   330
            Width           =   1200
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
            Height          =   225
            Index           =   0
            Left            =   4275
            OleObjectBlob   =   "Compra_Ingreso_OficinaContable.frx":06A9
            TabIndex        =   58
            Top             =   330
            Width           =   1575
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
            Height          =   225
            Left            =   8520
            OleObjectBlob   =   "Compra_Ingreso_OficinaContable.frx":0722
            TabIndex        =   59
            Top             =   330
            Width           =   1095
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel10 
            Height          =   225
            Left            =   9840
            OleObjectBlob   =   "Compra_Ingreso_OficinaContable.frx":0793
            TabIndex        =   60
            Top             =   330
            Width           =   1020
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel6 
            Height          =   195
            Left            =   1515
            OleObjectBlob   =   "Compra_Ingreso_OficinaContable.frx":0802
            TabIndex        =   61
            Top             =   1230
            Width           =   1215
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel7 
            Height          =   195
            Left            =   6120
            OleObjectBlob   =   "Compra_Ingreso_OficinaContable.frx":087C
            TabIndex        =   62
            Top             =   1230
            Width           =   690
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel11 
            Height          =   195
            Left            =   240
            OleObjectBlob   =   "Compra_Ingreso_OficinaContable.frx":08E6
            TabIndex        =   63
            Top             =   1230
            Width           =   1200
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel14 
            Height          =   195
            Left            =   2610
            OleObjectBlob   =   "Compra_Ingreso_OficinaContable.frx":0950
            TabIndex        =   64
            Top             =   1230
            Width           =   1455
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel15 
            Height          =   195
            Index           =   0
            Left            =   3990
            OleObjectBlob   =   "Compra_Ingreso_OficinaContable.frx":09C0
            TabIndex        =   65
            Top             =   1230
            Width           =   1455
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel23 
            Height          =   255
            Left            =   7245
            OleObjectBlob   =   "Compra_Ingreso_OficinaContable.frx":0A26
            TabIndex        =   66
            Top             =   1485
            Width           =   420
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkTasaIVA 
            Height          =   255
            Left            =   6960
            OleObjectBlob   =   "Compra_Ingreso_OficinaContable.frx":0A86
            TabIndex        =   67
            Top             =   1485
            Width           =   240
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel13 
            Height          =   195
            Left            =   7680
            OleObjectBlob   =   "Compra_Ingreso_OficinaContable.frx":0AE6
            TabIndex        =   68
            Top             =   1230
            Width           =   1275
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel16 
            Height          =   195
            Left            =   11760
            OleObjectBlob   =   "Compra_Ingreso_OficinaContable.frx":0B5C
            TabIndex        =   69
            Top             =   1230
            Width           =   1170
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel15 
            Height          =   195
            Index           =   1
            Left            =   13110
            OleObjectBlob   =   "Compra_Ingreso_OficinaContable.frx":0BCC
            TabIndex        =   70
            Top             =   1215
            Width           =   1140
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel15 
            Height          =   195
            Index           =   2
            Left            =   9195
            OleObjectBlob   =   "Compra_Ingreso_OficinaContable.frx":0C42
            TabIndex        =   71
            Top             =   1230
            Width           =   1080
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel15 
            Height          =   195
            Index           =   3
            Left            =   10500
            OleObjectBlob   =   "Compra_Ingreso_OficinaContable.frx":0CBE
            TabIndex        =   72
            Top             =   1245
            Width           =   1140
         End
      End
      Begin VB.Label Label1 
         BackColor       =   &H00FFFF80&
         Caption         =   "CUENTA I.V.A. NO SE INGRESA, SE CREA AUTOMATICAMENTE EN CONTABILIDAD"
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   8355
         TabIndex        =   99
         Top             =   4785
         Width           =   6495
      End
   End
   Begin VB.Frame Frame1 
      Height          =   540
      Left            =   4800
      TabIndex        =   42
      Top             =   9960
      Width           =   3690
      Begin VB.OptionButton Option2 
         Caption         =   "Valores Brutos"
         Height          =   225
         Left            =   2025
         TabIndex        =   44
         Top             =   255
         Width           =   1440
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Valores Netos"
         Height          =   225
         Left            =   135
         TabIndex        =   43
         Top             =   225
         Value           =   -1  'True
         Width           =   1680
      End
   End
   Begin VB.CheckBox Check1 
      Alignment       =   1  'Right Justify
      Caption         =   "Ingresar Detalle"
      Height          =   195
      Left            =   1650
      TabIndex        =   37
      Top             =   10335
      Value           =   1  'Checked
      Width           =   1455
   End
   Begin VB.ComboBox CboBodega 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      ItemData        =   "Compra_Ingreso_OficinaContable.frx":0D30
      Left            =   1650
      List            =   "Compra_Ingreso_OficinaContable.frx":0D3D
      Style           =   2  'Dropdown List
      TabIndex        =   36
      ToolTipText     =   "Seleccione Bodega"
      Top             =   9960
      Width           =   3705
   End
   Begin VB.TextBox TxtSolicitadoPor 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   1635
      TabIndex        =   35
      Tag             =   "T"
      Top             =   11040
      Width           =   3675
   End
   Begin VB.TextBox TxtAutorizadoPor 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   1635
      TabIndex        =   34
      Tag             =   "T"
      Top             =   11340
      Width           =   3675
   End
   Begin VB.TextBox TxtRetiradoPor 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   1635
      TabIndex        =   33
      Tag             =   "T"
      Top             =   11625
      Width           =   3675
   End
   Begin VB.Frame FrmProveedor 
      Caption         =   "Datos del documento"
      Height          =   7800
      Left            =   -225
      TabIndex        =   24
      Top             =   7845
      Width           =   13035
      Begin VB.Frame Frame2 
         Caption         =   "Forma de pago"
         Height          =   570
         Left            =   5970
         TabIndex        =   30
         Top             =   6570
         Width           =   1785
         Begin VB.ComboBox CboFpago 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            ItemData        =   "Compra_Ingreso_OficinaContable.frx":0D62
            Left            =   60
            List            =   "Compra_Ingreso_OficinaContable.frx":0D6C
            Style           =   2  'Dropdown List
            TabIndex        =   31
            ToolTipText     =   "Seleccione Bodega"
            Top             =   195
            Width           =   1695
         End
      End
      Begin VB.CommandButton CmdLlamaOC 
         Caption         =   "Cargar Orden Compra"
         Height          =   285
         Left            =   3450
         TabIndex        =   29
         Top             =   1365
         Width           =   2835
      End
      Begin VB.CommandButton Command2 
         Caption         =   "Command2"
         Height          =   225
         Left            =   7005
         TabIndex        =   28
         Top             =   1005
         Visible         =   0   'False
         Width           =   285
      End
      Begin VB.Frame fraSaldoOC 
         Caption         =   "Abonos OC"
         Height          =   1065
         Left            =   6345
         TabIndex        =   25
         Top             =   1275
         Visible         =   0   'False
         Width           =   1500
         Begin VB.TextBox txtSaldoOC 
            Alignment       =   1  'Right Justify
            BackColor       =   &H8000000F&
            Height          =   285
            Left            =   120
            Locked          =   -1  'True
            TabIndex        =   27
            Text            =   "0"
            ToolTipText     =   "Abonos realizados a la(s) OC's"
            Top             =   360
            Width           =   1260
         End
         Begin VB.CheckBox ChkUtilizaOC 
            Caption         =   "Utilizar"
            Height          =   195
            Left            =   120
            TabIndex        =   26
            Top             =   750
            Value           =   1  'Checked
            Width           =   1155
         End
      End
   End
   Begin VB.Timer Timer1 
      Interval        =   30
      Left            =   435
      Top             =   7560
   End
   Begin VB.TextBox Text1 
      Height          =   480
      Left            =   8565
      TabIndex        =   23
      Text            =   "Text1"
      Top             =   9795
      Visible         =   0   'False
      Width           =   750
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Command1"
      Height          =   300
      Left            =   11805
      TabIndex        =   22
      Top             =   510
      Visible         =   0   'False
      Width           =   195
   End
   Begin VB.Frame Frame3 
      Caption         =   "Empresa Activa"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1080
      Left            =   135
      TabIndex        =   19
      Top             =   120
      Width           =   14850
      Begin VB.Frame FrmPeriodo 
         Caption         =   "Periodo Contable"
         Height          =   780
         Left            =   11580
         TabIndex        =   45
         Top             =   240
         Width           =   3135
         Begin VB.TextBox TxtAnoContable 
            Alignment       =   1  'Right Justify
            BackColor       =   &H8000000F&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   2190
            Locked          =   -1  'True
            TabIndex        =   47
            TabStop         =   0   'False
            Text            =   "0"
            Top             =   390
            Width           =   780
         End
         Begin VB.TextBox TxtMesContable 
            Alignment       =   1  'Right Justify
            BackColor       =   &H8000000F&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   435
            Locked          =   -1  'True
            TabIndex        =   46
            TabStop         =   0   'False
            Text            =   "0"
            Top             =   390
            Width           =   1695
         End
      End
      Begin VB.TextBox SkEmpresa 
         BackColor       =   &H00FFFFC0&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00C00000&
         Height          =   420
         Left            =   2415
         TabIndex        =   21
         Text            =   "Text1"
         Top             =   555
         Width           =   8985
      End
      Begin VB.TextBox SkRut 
         BackColor       =   &H00FFFFC0&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00C00000&
         Height          =   420
         Left            =   195
         TabIndex        =   20
         Text            =   "Text1"
         Top             =   555
         Width           =   2100
      End
   End
   Begin VB.TextBox Text2 
      Height          =   420
      Left            =   3855
      TabIndex        =   18
      Text            =   "Text2"
      Top             =   10125
      Width           =   1155
   End
   Begin MSComDlg.CommonDialog Dialogo 
      Left            =   -120
      Top             =   6630
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   -60
      OleObjectBlob   =   "Compra_Ingreso_OficinaContable.frx":0D82
      Top             =   7530
   End
   Begin MSComctlLib.ListView LvDetalle 
      Height          =   3705
      Left            =   9930
      TabIndex        =   32
      Top             =   9810
      Visible         =   0   'False
      Width           =   7500
      _ExtentX        =   13229
      _ExtentY        =   6535
      View            =   3
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   0
      NumItems        =   5
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Object.Tag             =   "N109"
         Object.Width           =   617
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Object.Tag             =   "N109"
         Text            =   "NRO CHEQUE"
         Object.Width           =   2293
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Key             =   "debe"
         Object.Tag             =   "T3000"
         Text            =   "BANCO"
         Object.Width           =   5292
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   3
         Key             =   "haber"
         Object.Tag             =   "N100"
         Text            =   "DISPONIBLE"
         Object.Width           =   2469
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Object.Tag             =   "N100"
         Text            =   "Utilizar"
         Object.Width           =   2293
      EndProperty
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
      Height          =   225
      Index           =   1
      Left            =   0
      OleObjectBlob   =   "Compra_Ingreso_OficinaContable.frx":0FB6
      TabIndex        =   38
      Top             =   10005
      Width           =   1575
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel20 
      Height          =   225
      Left            =   60
      OleObjectBlob   =   "Compra_Ingreso_OficinaContable.frx":102D
      TabIndex        =   39
      Top             =   11085
      Width           =   1500
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel21 
      Height          =   225
      Left            =   -255
      OleObjectBlob   =   "Compra_Ingreso_OficinaContable.frx":10A0
      TabIndex        =   40
      Top             =   11370
      Width           =   1815
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel22 
      Height          =   225
      Left            =   -360
      OleObjectBlob   =   "Compra_Ingreso_OficinaContable.frx":1113
      TabIndex        =   41
      Top             =   11670
      Width           =   1905
   End
End
Attribute VB_Name = "Compra_Ingreso_OficinaContable"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public Smp_ActualizaPventa As String
Dim Ip_CenId As Long, Ip_PlaId As Long, Ip_GasId As Long, Ip_AreId As Long
Dim Lp_Neto As String, Lp_NetoTotal As String, Lp_Bruto As String, Lp_BrutoTotal As String
Dim Sm_Nota As String, Bm_Permite_Documento As Boolean, bm_Documento_Permite_Pago As Boolean
Dim Sm_MueveInventario As String * 2, Bm_Neto As Boolean, Lp_Nro_Comprobante As Long
Dim Lm_Top1 As Long, Lm_Top2 As Long
Dim Sm_EsOC As String
Dim Lp_TopFrmProveedor As Long
Dim Sm_DistribuidorGas As String * 2



Private Sub CboFpago_GotFocus()
        On Error Resume Next
    If Me.CboPlazos.ItemData(CboPlazos.ListIndex) = 0 Then
        CboFpago.Clear
        CboFpago.AddItem "CONTADO"
    Else
        CboFpago.Clear
        CboFpago.AddItem "CREDITO"
    End If
    CboFpago.ListIndex = 0
End Sub

Private Sub CboPlazos_Validate(Cancel As Boolean)
        On Error Resume Next
        
        If Me.CboPlazos.ItemData(CboPlazos.ListIndex) = 0 Then
            CboFpago.Clear
            CboFpago.AddItem "CONTADO"
        ElseIf CboPlazos.ItemData(CboPlazos.ListIndex) = 9999 Then
            CboFpago.Clear
            CboFpago.AddItem "FONDOS POR RENDIR"
        ElseIf CboPlazos.ItemData(CboPlazos.ListIndex) = 4444 Then
            CboFpago.Clear
            CboFpago.AddItem "CON ANTICIPO PROVEEDOR"
            
        Else
            CboFpago.Clear
            CboFpago.AddItem "CREDITO"
        End If
        CboFpago.ListIndex = 0
End Sub

Private Sub CboTipoDoc_Click()
    CboPlazos.Enabled = True
    If CboTipoDoc.ListIndex > -1 Then
        'TipoDoc
        TipoDoc
        If CboTipoDoc.Text = "INVENTARIO INICIAL" Then
            CboPlazos.ListIndex = 0
            CboPlazos.Enabled = False
        End If
    End If
    VerificarUsado
    If Me.CboPlazos.ItemData(CboPlazos.ListIndex) = 0 Then
        CboFpago.Clear
        CboFpago.AddItem "CONTADO"
    ElseIf CboPlazos.ItemData(CboPlazos.ListIndex) = 9999 Then
            CboFpago.Clear
            CboFpago.AddItem "FONDOS POR RENDIR"
    ElseIf CboPlazos.ItemData(CboPlazos.ListIndex) = 4444 Then
            CboFpago.Clear
            CboFpago.AddItem "ANTICIPO PROVEEDOR"
    Else
        CboFpago.Clear
        CboFpago.AddItem "CREDITO"
    End If
    CboFpago.ListIndex = 0
End Sub
Private Sub TipoDoc()
'    If Not Bm_Permite_Documento Then Exit Sub
        CboBodega.Enabled = True
        Sm_Nota = Empty
        CboPlazos.Enabled = True
        CboFpago.Enabled = True
        FrameRef.Visible = False
        Sql = "SELECT doc_retiene_iva,doc_permite_pago,doc_movimiento,doc_nota_de_debito debito," & _
                     "doc_nota_de_credito credito,doc_mueve_inventario,doc_orden_de_compra,doc_solo_exento " & _
              "FROM sis_documentos " & _
              "WHERE doc_id=" & CboTipoDoc.ItemData(CboTipoDoc.ListIndex)
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then 'Consultamos si el documento corresponde
            Sm_EsOC = "NO"
            If RsTmp!doc_orden_de_compra = "SI" Then
                Sm_EsOC = "SI"
                'CORRELATIVO FERTIQUIMICA
                'DEBE SER UNICO PARA TODA LA COMPA�IA, NO POR SUCURSALES
                'EN CUANTO A LAS OC SE REFIERE
                If SP_Rut_Activo = "76.369.600-6" Then
                    TxtNDoc = AutoIncremento("LEE", CboTipoDoc.ItemData(CboTipoDoc.ListIndex), , 1)
                Else
                    TxtNDoc = AutoIncremento("LEE", CboTipoDoc.ItemData(CboTipoDoc.ListIndex), , IG_id_Sucursal_Empresa)
                End If
            End If
            bm_Documento_Permite_Pago = IIf(RsTmp!doc_permite_pago = "SI", True, False)
            Sm_MueveInventario = RsTmp!doc_mueve_inventario
            
            txtSubNeto.Locked = False
            
            If RsTmp!doc_solo_exento = "SI" Then
                txtSubNeto.Locked = True
            End If
            
            CboTipoDoc.Tag = RsTmp!doc_movimiento
            
            
            If RsTmp!doc_retiene_iva = "SI" Then ' IVA retenido
                 txtIvaRetenido.Locked = False
                 txtIvaRetenido.Enabled = True
                 txtIvaRetenido = txtIVA
            Else
                 txtIvaRetenido.Locked = True
                 txtIvaRetenido.Enabled = False
                 txtIvaRetenido = 0
            End If
            If RsTmp!debito = "SI" Or RsTmp!Credito = "SI" Then
                If RsTmp!debito = "SI" Then Sm_Nota = "DEBITO"
                If RsTmp!Credito = "SI" Then Sm_Nota = "CREDITO"
                CboPlazos.Enabled = False
                CboFpago.Enabled = False
                FrameRef.Visible = True
                CboBodega.Enabled = False
             '   CmdBuscar_Click
                TxtNDoc.SetFocus
            End If
        End If
End Sub

Private Sub CboTipoDoc_GotFocus()
    CmdLlamaOC.Tag = ""
End Sub

Private Sub CboTipoDoc_KeyDown(KeyCode As Integer, Shift As Integer)
    'TipoDoc
End Sub

Private Sub Check1_Click()
    If Check1.Value = 1 Then
        Option1.Enabled = True
        Option2.Enabled = True
    Else
        Option1.Enabled = False
        Option2.Enabled = False
    End If
    CmdNueva.SetFocus
End Sub

Private Sub CmdBuscaProv_Click()
    AccionProveedor = 0
    BuscaProveedor.Show 1
    TxtRutProveedor = RutBuscado

    TxtRutProveedor_Validate True
End Sub

Private Sub CmdBuscar_Click()
    SG_codigo = Empty
    CompraListaReferencia.Rut = Me.TxtRutProveedor
    TxtDocumentoRef = ""
    TxtNroRef = ""
    TxtTotalRef = 0
    TxtNroRef.Tag = ""
    If Sm_Nota = "DEBITO" Then CompraListaReferencia.Sm_Nota = " doc_nota_de_debito='NO' "
    If Sm_Nota = "CREDITO" Then CompraListaReferencia.Sm_Nota = " doc_nota_de_credito='NO' "
    CompraListaReferencia.Show 1
    If SG_codigo <> Empty Then
        Sql = "SELECT c.id,c.doc_id,doc_nombre,no_documento,total,iva_retenido,bod_id,pago,com_plazo " & _
              "FROM com_doc_compra c " & _
              "INNER JOIN sis_documentos d USING(doc_id) " & _
              "WHERE  id=" & SG_codigo
        Consulta RsTmp2, Sql
        If RsTmp2.RecordCount > 0 Then
            TxtDocumentoRef = RsTmp2!doc_nombre
            TxtDocumentoRef.Tag = RsTmp2!ID
            FrameRef.Tag = RsTmp2!doc_id
            TxtNroRef = RsTmp2!no_documento
            TxtTotalRef = NumFormat(RsTmp2!Total)
            TxtNroRef.Tag = SG_codigo
            If RsTmp2!iva_retenido > 0 Then
                txtIvaRetenido.Enabled = True
                txtIvaRetenido.Locked = False
            End If
            Busca_Id_Combo CboBodega, Val(RsTmp2!bod_id)
            Busca_Id_Combo CboPlazos, Val(RsTmp2!com_plazo)
            CboPlazos_Validate True
            TxtExento.SetFocus
            'Check1.Value = 0
        End If
    End If
End Sub

Private Sub cmdCuenta_Click()
    With BuscarSimple
        SG_codigo = Empty
        .Sm_Consulta = "SELECT pla_id, pla_nombre,tpo_nombre,det_nombre " & _
                       "FROM    con_plan_de_cuentas p,  con_tipo_de_cuenta t,   con_detalle_tipo_cuenta d " & _
                       "WHERE   p.tpo_id = t.tpo_id AND p.det_id = d.det_id "
        .Sm_CampoLike = "pla_nombre"
        .Im_Columnas = 2
        .CmdCuentasContables.Visible = True
        .Show 1
        If SG_codigo = Empty Then Exit Sub
        TxtCodigoCuenta = Val(SG_codigo)
        TxtCodigoCuenta.SetFocus
        
    End With
End Sub

Private Sub CmdLlamaOC_Click()
    Dim Sm_Bruto_Neto As String
    fraSaldoOC.Visible = False
    If Len(TxtRutProveedor) = 0 Then
        MsgBox "Debe Seleccionar Proveedor...", vbInformation
        TxtRutProveedor.SetFocus
        Exit Sub
    End If
    'Buscamos OC del proveedor
    Sql = "SELECT id,no_documento nro_oc,fecha,rut,nombre_proveedor,neto,iva,total " & _
          "FROM com_doc_compra c " & _
          "WHERE com_estado_oc='PROCESO' AND  rut_emp='" & SP_Rut_Activo & "' AND doc_id=" & IG_id_OrdenCompra & " AND rut='" & TxtRutProveedor & "'"
    SG_codigo = Empty
    Com_OC_Pendientes.Sm_Sql = Sql
    Com_OC_Pendientes.TxtRutProveedor = TxtRutProveedor
    Com_OC_Pendientes.TxtRsocial = TxtRsocial
    Com_OC_Pendientes.TxtRutProveedor.Locked = True
    Com_OC_Pendientes.CmdBuscaProv.Enabled = False
    
    Com_OC_Pendientes.Show 1
    If SG_codigo = Empty Then
        CmdLlamaOC.Tag = Empty
        CmdLlamaOC.Caption = "Carga Orden de Compra"
        sg_codig2 = Empty
        Exit Sub
    End If
    CmdLlamaOC.Tag = SG_codigo
    fraSaldoOC.Visible = True
    txtSaldoOC = NumFormat(SaldoDocumentoOC(Me.CmdLlamaOC.Tag, "PRO"))
    If CDbl(txtSaldoOC) > 0 Then MsgBox "Existen abonos en OC(s), para utilizar este abono, el tipo de pago debe ser CREDITO...", vbInformation
    
    'CmdLlamaOC.Caption = "ORDEN DE COMPRA NRO " & SG_codigo2
    
    
    'Cargamos la OC seleccionada
    Sql = "SELECT pago,SUM(neto) Neto,SUM(iva) IVA,SUM(total),SUM(com_exe_otros_imp) Exento,SUM(especifico),SUM(iva_retenido) iva_retenido,SUM(com_bruto_neto) com_bruto_neto," & _
            "com_solicitado_por,com_autorizado_por " & _
          "FROM  com_doc_compra " & _
          "WHERE id IN(" & SG_codigo & ")"
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        TxtExento = NumFormat(RsTmp!Exento)
        TxtNeto = NumFormat(RsTmp!Neto)
        txtIVA = NumFormat(RsTmp!Iva)
        txtIvaRetenido = NumFormat(RsTmp!iva_retenido)
        TxtSubTotal = NumFormat(CDbl(TxtExento) + CDbl(TxtNeto) + CDbl(txtIVA) - CDbl(txtIvaRetenido))
        If RsTmp!pago = "CONTADO" Then CboFpago.ListIndex = 0 Else CboFpago.ListIndex = 0
        Sm_Bruto_Neto = RsTmp!com_bruto_neto
        TxtSolicitadoPor = RsTmp!com_solicitado_por
        TxtAutorizadoPor = RsTmp!com_autorizado_por
        If Sm_Bruto_Neto = "NETO" Then
            Bm_Neto = True
        Else
            Bm_Neto = False
        End If
    End If
    'Cargamos Impuestos del documento seleccionado
    Sql = "SELECT e.imp_id,CONCAT(imp_nombre,'    (',LCASE(ime_costo_credito),')') ," & _
           "(SELECT SUM(coi_valor) coi_valor FROM com_impuestos c WHERE c.imp_id=i.imp_id AND c.id IN(" & SG_codigo & ")) valor," & _
           "0 factor,ime_costo_credito  " & _
          "FROM par_impuestos_empresas e " & _
          "INNER JOIN par_impuestos i USING(imp_id) " & _
          "WHERE e.rut='" & SP_Rut_Activo & "'"
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvImpuestos, False, True, True, False
    For i = 1 To LvImpuestos.ListItems.Count
        If LvImpuestos.ListItems(i).SubItems(4) = "COSTO" Then
            SkImpCosto = SkImpCosto + CDbl(LvImpuestos.ListItems(i).SubItems(2))
        End If
        SkTotalAdicionales = SkTotalAdicionales + CDbl(LvImpuestos.ListItems(i).SubItems(2))
    Next
    LvImpuestos.Tag = SkTotalAdicionales
   'TxtImpuesto.Tag = 1
  '  CalculaAdicionales
    
    
    Factores
    CTotal
  '  With compra_detalle
        
  
  '      compra_detalle.Show 1
        'compra_detalle.Hide
  '  End With
    
End Sub

Private Sub CmdNueva_Click()
    If Len(TxtRutProveedor) = 0 Then
        MsgBox "Falta ingresar proveedor..", vbInformation
        TxtRutProveedor.SetFocus
        Exit Sub
    End If
    If CboTipoDoc.ListIndex = -1 Then
        MsgBox "Seleccione documento..", vbInformation
        CboTipoDoc.SetFocus
        Exit Sub
    End If
    If Val(TxtNDoc) = 0 Then
        MsgBox "Falta Nro Documento..", vbInformation
        TxtNDoc.SetFocus
        Exit Sub
    End If
    If Val(txtTotal) < 1 Then
        If Sm_Nota <> "CREDITO" Then
            MsgBox "Faltan Valores...", vbInformation
            Exit Sub
        End If
    End If
    
    If CDbl(Me.TxtTotalCuentas) <> CDbl(SkRequerido) Then
        MsgBox "Valor requerido no coincide con valor de las cuentas...", vbInformation
        TxtCodigoCuenta.SetFocus
        Exit Sub
    End If

    
    
    
        'validacion de la fecha
    If Val(Year(DtFecha)) < IG_Ano_Contable Then
       
    Else
        If (Month(DtFecha) + Year(DtFecha)) > (IG_Ano_Contable + IG_Mes_Contable) Then
            MsgBox "No puede ingresar documentos con FECHA posterior al periodo contable...", vbInformation
            DtFecha.SetFocus
            Exit Sub
        End If
    End If
    If CboFpago.ListIndex = -1 Then
        MsgBox "Seleccione forma de pago...", vbInformation
        CboFpago.SetFocus
        Exit Sub
    End If
    
    If Not Bm_Permite_Documento Then Exit Sub
    
   ' If SG_Modulo_Caja = "SI" Then If CboPlazos.Text = "CONTADO" Then MsgBox " Este valor se ver� reflejado en el resumen de su caja...  ", vbInformation

    GrabarCompra
    
    For i = 1 To LvImpuestos.ListItems.Count
        LvImpuestos.ListItems(i).SubItems(2) = 0
    Next
    Me.SkTotalAdicionales = 0
    Me.SkImpCosto = 0
    CalculaAdicionales
End Sub
Private Sub GrabarCompra()
    Dim Lp_Id As Long, Sp_SoloUnidades As String, Sp_SoloValores As String, mFinal As String, Sp_Folio As String, Sp_FolioNro As Long
    Dim Bp_Comprobante_Pago As Boolean, Sp_OC As String, Sp_ImpCompPago As String * 2, Lp_SaldoRef As Long, Sp_NC_Utilizada As String * 2
    Dim Sp_NroIdRef As String, Lp_SaldoOC As Long
    Bp_Comprobante_Pago = False
    Sp_SoloUnidades = "NO"
    Sp_SoloValores = "NO"
    mFinal = Empty
    Lp_Id = 0
    Lp_Id = UltimoNro("com_doc_compra", "id")
    Sp_Folio = "0"
    Sp_FolioNro = 0
    On Error GoTo ErrorGrabacion
    cn.BeginTrans
    'Documento de compra
    Sp_NC_Utilizada = "NO"
    SG_codigo2 = Empty
    Sp_NroIdRef = "0"
    'Si selecciona Fondos por rendir como medio de pago
    'Mostrar ventana para buscar documentos emitidos con cta fondos por rendir
    
    If Val(Me.CmdLlamaOC.Tag) > 0 Then
    '    bm_Documento_Permite_Pago = False
        Sql = "UPDATE com_doc_compra SET com_estado_oc='TERMINADA',id_ref=" & Lp_Id & " " & _
             "WHERE id IN(" & CmdLlamaOC.Tag & ")"
        cn.Execute Sql
        
        If CDbl(txtSaldoOC) > 0 And ChkUtilizaOC.Value = 1 Then
            'Debemos abonar este valor al documento
            'si sobra, se debe incluir en el pozo,
            'si el pago es contado, se debe restar este monto tambien
            '3 Marz 2014
          
                Lp_Nro_Comprobante = AutoIncremento("lee", 200, , IG_id_Sucursal_Empresa)
            
                Dim Lp_IdAbo As Long
                Bp_Comprobante_Pago = True
                Lp_IdAbo = UltimoNro("cta_abonos", "abo_id")
                PagoDocumento Lp_IdAbo, "PRO", Me.TxtRutProveedor, DtFecha, CDbl(txtSaldoOC), 999, "ABONOS EN OC", "PAGADO", LogUsuario, Lp_Nro_Comprobante, 0, "COMPRA"
                Sql = "INSERT INTO cta_abono_documentos (abo_id,id,ctd_monto,rut_emp) VALUES "
                Sql = Sql & "(" & Lp_IdAbo & "," & Lp_Id & "," & CDbl(txtSaldoOC) & ",'" & SP_Rut_Activo & "')"
                cn.Execute Sql
                
                'If SP_Rut_Activo = "76.369.600-6" Then
                    'SOLO PARA FERTIQUIMICA, SE GUARDARA EL CORRELATIVO UNICO DE ORDENES DE COMPRA
                '    If CboTipoDoc.Text = "ORDEN DE COMPRA" Then
                '        AutoIncremento "GUARDA", 200, Lp_Nro_Comprobante, 1
                '    End If
                'Else
                    AutoIncremento "GUARDA", 200, Lp_Nro_Comprobante, IG_id_Sucursal_Empresa
                'End If
        End If
        
    End If
   'SI es nota de debito, indicar cual sera el nro de ref
    If Sm_Nota = "DEBITO" Or Sm_Nota = "CREDITO" And Val(TxtDocumentoRef.Tag) > 0 Then Sp_NroIdRef = TxtDocumentoRef.Tag
    
    If bm_Documento_Permite_Pago And CboFpago.Text <> "CREDITO" Then
        'Aqui realizar abono a la cta cte
        '8 Octubre 2011
        'Comprobar si este documento ya tiene un pago
        'Sql = "DELETE FROM cta_abonos WHERE "
        Dim Lp_RestaOC As Long
        Dim Lp_MontoPagar As Long
        Dim Lp_EnviarAPozo As Long
        Lp_MontoPagar = CDbl(txtTotal)
        Lp_EnviarAPozo = 0
        If Val(txtSaldoOC) > 0 And ChkUtilizaOC.Value = 1 And fraSaldoOC.Visible Then
            Lp_RestaOC = CDbl(txtSaldoOC)
            If CDbl(txtTotal) - Lp_RestaOC >= 0 Then
                Lp_MontoPagar = Lp_MontoPagar - Lp_RestaOC
            Else
                Lp_MontoPagar = CDbl(txtTotal)
                Lp_EnviarAPozo = Lp_RestaOC - Lp_MontoPagar
            End If
        End If
         
        Lp_Nro_Comprobante = AutoIncremento("lee", 200, , IG_id_Sucursal_Empresa)
    
     '   Dim lp_IDAbo As Long
        Bp_Comprobante_Pago = True
        Lp_IdAbo = UltimoNro("cta_abonos", "abo_id")
        PagoDocumento Lp_IdAbo, "PRO", Me.TxtRutProveedor, DtFecha, Lp_MontoPagar, IIf(Mid(CboPlazos.Text, 1, 7) = "CONTADO", 1, Right(CboPlazos.ItemData(CboPlazos.ListIndex), 5)), CboFpago.Text, "PAGADO", LogUsuario, Lp_Nro_Comprobante, 0, "COMPRA"
        Sql = "INSERT INTO cta_abono_documentos (abo_id,id,ctd_monto,rut_emp) VALUES "
        Sql = Sql & "(" & Lp_IdAbo & "," & Lp_Id & "," & Lp_MontoPagar & ",'" & SP_Rut_Activo & "')"
        cn.Execute Sql
        
        'Si por alguna razon nos sobra para el pozo, lo insertamos oqui
        If Lp_EnviarAPozo > 0 Then
            
            Sql = "INSERT INTO cta_pozo (id_ref,pzo_fecha,rut_emp,pzo_cli_pro,pzo_rut,pzo_abono) " & _
                "VALUES(" & Lp_Id & ",'" & Fql(DtFecha) & "','" & SP_Rut_Activo & "','PRO','" & TxtRutProveedor & "'," & Lp_EnviarAPozo & ")"
            cn.Execute Sql
        
        End If
        
        
        AutoIncremento "GUARDA", 200, Lp_Nro_Comprobante, IG_id_Sucursal_Empresa
        
        If CboPlazos.ItemData(CboPlazos.ListIndex) = 9999 Then
            Ban_SeleccionaChequeRendir.Sm_Rut_Anticipo = ""
            Ban_SeleccionaChequeRendir.Sm_EnviarA = "COMPRAS"
            Ban_SeleccionaChequeRendir.TxtRequerido = txtTotal
            Ban_SeleccionaChequeRendir.TxtSaldo = txtTotal
            
            Ban_SeleccionaChequeRendir.Show 1
            If SG_codigo = Empty Then
                cn.RollbackTrans
                Exit Sub
            End If
            Sql = "INSERT INTO ban_cheques_fondos_rendir (che_id,chf_debe,abo_id) VALUES"
            For i = 1 To LvDetalle.ListItems.Count
                Sql = Sql & "(" & LvDetalle.ListItems(i) & "," & CDbl(LvDetalle.ListItems(i).SubItems(4)) & "," & Lp_IdAbo & "),"
            Next
            Sql = Mid(Sql, 1, Len(Sql) - 1)
            cn.Execute Sql
        End If
        
        'Anticipo de proveedor, siempre que tenga disponible
        '5-4-2014
        If CboPlazos.ItemData(CboPlazos.ListIndex) = 4444 Then
            Ban_SeleccionaChequeRendir.Sm_EnviarA = "COMPRAS"
            Ban_SeleccionaChequeRendir.Sm_Rut_Anticipo = TxtRutProveedor
            Ban_SeleccionaChequeRendir.TxtRequerido = txtTotal
            Ban_SeleccionaChequeRendir.TxtSaldo = txtTotal
            Ban_SeleccionaChequeRendir.Show 1
            If SG_codigo = Empty Then
                cn.RollbackTrans
                Exit Sub
            End If
            Sql = "INSERT INTO ban_cheques_fondos_rendir (che_id,chf_debe,abo_id) VALUES"
            For i = 1 To LvDetalle.ListItems.Count
                Sql = Sql & "(" & LvDetalle.ListItems(i) & "," & CDbl(LvDetalle.ListItems(i).SubItems(4)) & "," & Lp_IdAbo & "),"
            Next
            Sql = Mid(Sql, 1, Len(Sql) - 1)
            cn.Execute Sql
        End If
        
        
    End If
    
    
    
    
      
    Sql = "SELECT doc_contable,doc_orden_de_compra,doc_nota_de_credito  " & _
          "FROM sis_documentos " & _
          "WHERE doc_id=" & CboTipoDoc.ItemData(CboTipoDoc.ListIndex)
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        Sp_OC = RsTmp!doc_orden_de_compra
        If RsTmp!doc_contable = "SI" Then
              
            Sql = "SELECT IFNULL(MAX(com_folio)+1,1) folio " & _
                  "FROM com_doc_compra " & _
                  "WHERE rut_emp='" & SP_Rut_Activo & "' AND ano_contable=" & TxtAnoContable & " AND  mes_contable=" & IG_Mes_Contable
            Consulta RsTmp2, Sql
            If RsTmp2.RecordCount > 0 Then
                Sp_FolioNro = RsTmp2!Folio
                Sp_Folio = RsTmp2!Folio
                SG_codigo2 = Empty
        '        sis_InputBox.texto.PasswordChar = ""
        '        sis_InputBox.texto.Locked = True
        '        sis_InputBox.Caption = "Tome nota"
        '        sis_InputBox.FramBox = "Folio para libro contable"
        '        sis_InputBox.CmdCancelar.Visible = False
                Sp_Folio = Right("00" & IG_Mes_Contable, 2) & Right("000" & Sp_Folio, 3)
        '        sis_InputBox.texto = Sp_Folio
        '        sis_InputBox.Show 1
            End If
        End If
        
        If RsTmp!doc_nota_de_credito = "SI" Then
            'Buscar el documento referenciado    /// 20-10-2012
            'y ver si su saldo.
            'si el saldo es mayor o igual a la NC abonar el monto de la nota de credito
            
            Lp_SaldoRef = SaldoDocumento(Str(Val(Me.TxtNroRef.Tag)), "PRO")
            
            Dim Lp_Abono_a_Realizar As Long
            Dim Lp_Abono_a_Pozo As Long
            
            'If Lp_SaldoRef >= CDbl(TxtTotal) Then
            If Lp_SaldoRef > 0 Then
                    'hacer abono
                    lp_abono_a_plazo = 0
                    
                    If Lp_SaldoRef >= CDbl(txtTotal) Then
                        Lp_Abono_a_Realizar = CDbl(txtTotal)
                    Else
                        Lp_Abono_a_Realizar = Lp_SaldoRef
                        Lp_Abono_a_Pozo = CDbl(txtTotal) - Lp_SaldoRef
                    End If

                    
                    
                    MsgBox "El monto " & NumFormat(Lp_Abono_a_Realizar) & " de esta NC se abono al documento original...", vbInformation
                    Lp_Nro_Comprobante = AutoIncremento("lee", 200, , IG_id_Sucursal_Empresa)
                
                    'Dim lp_IDAbo As Long
                    Bp_Comprobante_Pago = True
                    Lp_IdAbo = UltimoNro("cta_abonos", "abo_id")
                    PagoDocumento Lp_IdAbo, "PRO", Me.TxtRutProveedor, DtFecha, CDbl(Lp_Abono_a_Realizar), 8888, "NC", "ABONO CON NC", LogUsuario, Lp_Nro_Comprobante, Lp_Id, "ncompra"
                    Sql = "INSERT INTO cta_abono_documentos (abo_id,id,ctd_monto,rut_emp) VALUES "
                    Sql = Sql & "(" & Lp_IdAbo & "," & Val(Me.TxtNroRef.Tag) & "," & CDbl(Lp_Abono_a_Realizar) & ",'" & SP_Rut_Activo & "')"
                    cn.Execute Sql
                    
                    AutoIncremento "GUARDA", 200, Lp_Nro_Comprobante, IG_id_Sucursal_Empresa
                    
                    Sp_NC_Utilizada = "SI"
                    If Lp_Abono_a_Pozo > 0 Then
                        
                        Sql = "INSERT INTO cta_pozo (id_ref,pzo_fecha,rut_emp,pzo_cli_pro,pzo_rut,pzo_abono) " & _
                            "VALUES(" & Lp_Id & ",'" & Fql(DtFecha) & "','" & SP_Rut_Activo & "','PRO','" & TxtRutProveedor & "'," & Lp_Abono_a_Pozo & ")"
                        cn.Execute Sql
                    
                    End If
                    
                    
            Else
                    'PagoDocumento Lp_Id, "PRO", Me.TxtRutProveedor, Me.DtFecha, Lp_SaldoRef, 8888, "DESCUENTA NC", "", LogUsuario,
                    Sql = "INSERT INTO cta_pozo (id_ref,pzo_fecha,rut_emp,pzo_cli_pro,pzo_rut,pzo_abono) " & _
                        "VALUES(" & Lp_Id & ",'" & Fql(DtFecha) & "','" & SP_Rut_Activo & "','PRO','" & TxtRutProveedor & "'," & Lp_Abono_a_Pozo & ")"
                    cn.Execute Sql
                    Sp_NC_Utilizada = "SI"
            End If
            
        
        
        End If
        
        
    End If

    
    If Sp_OC = "SI" Then
        If SP_Rut_Activo = "76.369.600-6" Then
            'SOLO PARA FERTIQUIMICA
            AutoIncremento "GUARDA", Me.CboTipoDoc.ItemData(CboTipoDoc.ListIndex), TxtNDoc, 1
        Else
        
            AutoIncremento "GUARDA", Me.CboTipoDoc.ItemData(CboTipoDoc.ListIndex), TxtNDoc, IG_id_Sucursal_Empresa
        End If
    
    End If
    
'    lacajita = 0
'    If SG_Modulo_Caja = "SI" Then
'        If Mid(CboPlazos.Text, 1, 7) = "CONTADO" Then
'            lacajita = LG_id_Caja
'        End If
'    End If
    
    lacajita = 0
    If SG_Modulo_Caja = "SI" Then
        If Mid(CboPlazos.Text, 1, 7) = "CONTADO" Then
            If CboCaja.Text = "SI" Then
                lacajita = LG_id_Caja
            End If
        End If
    End If
    
    
    Sql = "INSERT INTO com_doc_compra (id,doc_id,no_documento,rut,nombre_proveedor,neto,iva,total,iva_retenido,total_impuestos,fecha,tipo_movimiento,mes_contable,ano_contable,doc_fecha_vencimiento,rut_emp,com_bruto_neto,bod_id,com_exe_otros_imp," & _
                      "pago,com_folio,com_folio_texto,com_plazo,com_solicitado_por,com_autorizado_por,com_nc_utilizada,com_id_ref_de_nota_debito,caj_id,com_pla_id,com_retirado_por,com_tasa_iva,com_iva_sin_credito,com_iva_margen,com_margen_distribuidor) " & _
          "VALUES(" & Lp_Id & "," & CboTipoDoc.ItemData(CboTipoDoc.ListIndex) & "," & TxtNDoc & ",'" & TxtRutProveedor & "','" & TxtRsocial & "'," & CDbl(TxtNeto) & "," & CDbl(txtIVA) & "," & CDbl(txtTotal) & "," & _
           CDbl(txtIvaRetenido) & "," & CDbl(SkTotalAdicionales) & ",'" & Fql(DtFecha.Value) & "','INGRESO COMPRA'," & IG_Mes_Contable & "," & IG_Ano_Contable & ",'" & Fql(DtFecha + CboPlazos.ItemData(CboPlazos.ListIndex)) & _
           "','" & SP_Rut_Activo & "','" & IIf(Option1.Value, "NETO", "BRUTO") & "'," & CboBodega.ItemData(CboBodega.ListIndex) & "," & CDbl(TxtExento) & ",'" & CboFpago.Text & "'," & Sp_FolioNro & ",'" & Sp_Folio & "'," & CboPlazos.ItemData(CboPlazos.ListIndex) & ",'" & _
           TxtSolicitadoPor & "','" & TxtAutorizadoPor & "','" & Sp_NC_Utilizada & "'," & Sp_NroIdRef & "," & lacajita & "," & Val(Right(CboPlazos.Text, 5)) & ",'" & Me.TxtRetiradoPor & "','" & SkTasaIVA & "'," & CDbl(txtIvaSinCredito) & "," & CDbl(txtIVAMargen) & "," & CDbl(txtMargenDist) & ")"
    cn.Execute Sql
    'Impuestos adicionales
    If SkTotalAdicionales > 0 Then
        Sql = "INSERT INTO com_impuestos(id,imp_id,coi_valor) VALUES  "
        For i = 1 To Me.LvImpuestos.ListItems.Count
            If CDbl(LvImpuestos.ListItems(i).SubItems(2)) > 0 Then Sql = Sql & "(" & Lp_Id & "," & LvImpuestos.ListItems(i) & "," & CDbl(LvImpuestos.ListItems(i).SubItems(2)) & "),"
        Next
        Sql = Mid(Sql, 1, Len(Sql) - 1)
        cn.Execute Sql
    End If
    'Items detalle
    '********************************************************************************
    ' D E T A L L E   D E    C U E N T A S
    ' 15-11-2014
    'Cada cuenta con su valor, ira en la tabla com_doc_compra_detalle
    'para con esto generar el libro de compras
    '********************************************************************************
    With Me.LvCuentaContable
        Dim Lp_Id_Detalle As Long
        
       
        For i = 1 To LvCuentaContable.ListItems.Count
             Sql = "INSERT INTO com_doc_compra_detalle (cmd_id,id,pro_codigo,cmd_cantidad,cmd_unitario_neto,cmd_unitario_bruto, " & _
                    "cmd_total_neto,cmd_total_bruto,pla_id,cen_id,gas_id,are_id,cmd_detalle,cmd_costo_real_neto,cmd_exento,cmd_nro_linea) VALUES "
                    
            Lp_Id_Detalle = UltimoNro("com_doc_compra_detalle", "cmd_id")
            
            'Mientras tanto no utilizaremos estas opciones, quedaran en 0
            'Ip_CenId = .ListItems(i).SubItems(9)
            'Ip_PlaId = .ListItems(i).SubItems(8)
            'Ip_GasId = .ListItems(i).SubItems(10)
            'Ip_AreId = .ListItems(i).SubItems(11)
            
            Ip_CenId = 0
            Ip_PlaId = .ListItems(i).SubItems(1)
            Ip_GasId = 0
            Ip_AreId = 0
            
            Lp_Neto = CDbl(.ListItems(i).SubItems(3))
            
            Sql = Sql & "(" & Lp_Id_Detalle & "," & Lp_Id & ",0,1," & Lp_Neto & "," & _
           0 & "," & Lp_Neto & "," & 0 & "," & Ip_PlaId & "," & Ip_CenId & "," & Ip_GasId & "," & Ip_AreId & ",'" & _
            .ListItems(i).SubItems(2) & "'," & Lp_Neto & "," & 0 & "," & i & "),"
            
            '25 Octubre 2014
            'Grabaremos la penderacion de los impuestos linea a linea, (harina, carnes, etc)
   
            Sql = Mid(Sql, 1, Len(Sql) - 1)
            cn.Execute Sql
        Next
    End With
    

    
    'Se completaron todos los procesos, Confirmamos la grabacion en todas
    'las tablas
    cn.CommitTrans
    
    CmdLlamaOC.Tag = Empty
    CmdLlamaOC.Caption = "Carga Orden de Compra"
    sg_codig2 = Empty
    SG_codigo = Empty
    Sql = "SELECT emp_comprobantes_pagos_contado comp " & _
         "FROM sis_empresas " & _
         "WHERE rut='" & SP_Rut_Activo & "'"
    Consulta RsTmp, Sql
    Sp_ImpCompPago = "SI"
    If RsTmp.RecordCount > 0 Then
        Sp_ImpCompPago = RsTmp!comp
    End If
        
    'If MsgBox("Grabacion correcta..." & vbNewLine & " � Ingresa nuevo documento ?", vbYesNo + vbQuestion) = vbNo Then
    MsgBox "  Grabacion correcta...    " & vbNewLine & "     FOLIO: " & Sp_Folio, vbInformation + vbOKOnly
      
        'Limpiar datos
        
        
    If Mid(CboPlazos.Text, 1, 7) <> "CONTADO" Then
            If MsgBox("Desea registrar el pago($) del documento ahora?...", vbOKCancel + vbQuestion) = vbOK Then
              
                        Sql = "SELECT v.id,no_documento,doc_nombre " & _
                                ",v.rut,nombre_proveedor,'',fecha, v.doc_fecha_vencimiento, total,0 abonos," & _
                                               "total saldo " & _
                                      "FROM    com_doc_compra v,   maestro_proveedores c,  sis_documentos d " & _
                                      "WHERE v.id IN (" & Lp_Id & ") AND v.com_nc_utilizada='NO' AND v.rut_emp='" & SP_Rut_Activo & "' AND id_ref=0 AND  v.doc_id_factura = 0 AND v.Rut = c.rut_proveedor AND v.doc_id = d.doc_id  " & _
                                       "ORDER BY id"
                        
                        Consulta RsTmp, Sql
                        If RsTmp.RecordCount > 0 Then
                                   
                               '
                                   
                                   
                               
                             '  If Mid(CboPlazos.Text, 1, 7) = "CONTADO" Then ctacte_GestionDePagos.Im_Fpago_Defecto = 1
                             '  If Mid(CboPlazos.Text, 1, 6) = "CHEQUE" Then ctacte_GestionDePagos.Im_Fpago_Defecto = 2
                             '  If Mid(CboPlazos.Text, 1, 14) = "DEBITO/CREDITO" Then ctacte_GestionDePagos.Im_Fpago_Defecto = 8
                               
                        
                               ctacte_GestionDePagos.Sm_Cli_Pro = "PRO"
                               ctacte_GestionDePagos.Sm_NombreAbono = TxtRsocial 'NOMBRE PROVEEDOR
                               ctacte_GestionDePagos.Sm_RutAbono = TxtRutProveedor
                              ' ctacte_GestionDePagos.Sm_RelacionMP = Right(CboPlazos.Text, 3)
                               ctacte_GestionDePagos.CmdSalir.Visible = False
                               ctacte_GestionDePagos.SkORIGEN = "COMPRA"
                               ctacte_GestionDePagos.txtSuperMensaje.Top = 6870
                               ctacte_GestionDePagos.CmdSalir.Visible = True
                               ctacte_GestionDePagos.Show 1
                        End If
                
                
            End If
    End If
    LimpiaDatos
    
    DtFecha.SetFocus
    Exit Sub
ErrorGrabacion:
    cn.RollbackTrans
    MsgBox "Hubo un error al intentar grabar..." & vbNewLine & "Nro Error: " & Err.Number & vbNewLine & Err.Description
    Exit Sub

End Sub

Private Sub Previsualiza()
   Dim Cx As Double, Cy As Double, Sp_Fecha As String, Ip_Mes As Integer, Dp_S As Double, Sp_Letras As String
    Dim Sp_Neto As String * 12, Sp_IVA As String * 12
    On Error GoTo ePrev
    
    
    
    
    Dim Sp_Nro_Comprobante As String
    
    Dim Sp_Empresa As String
    Dim Sp_Giro As String
    Dim Sp_Direccion As String
    
    
    Dim Sp_Nombre As String * 45
    Dim Sp_Rut As String * 15
    Dim Sp_Concepto As String
    
    Dim Sp_Nro_Doc As String * 12
    Dim Sp_Documento As String * 35
    Dim Sp_Valor As String * 15
    Dim Sp_Saldo As String * 15
    Dim Sp_Fpago As String * 24
    Dim Sp_Total As String * 12
    
    Dim Sp_Banco As String * 15
    Dim Sp_NroCheque As String * 10
    Dim Sp_FechaCheque As String * 10
    Dim Sp_ValorCheque As String * 15
    Dim Sp_SumaCheque As String * 15
    
    Dim Sp_Observacion As String * 80
    
   
    
    Sql = "SELECT giro,direccion,ciudad " & _
         "FROM sis_empresas " & _
         "WHERE rut='" & SP_Rut_Activo & "'"
    Consulta RsTmp2, Sql
    If RsTmp2.RecordCount > 0 Then
        Sp_Giro = RsTmp2!giro
        Sp_Direccion = RsTmp2!direccion & " - " & RsTmp2!ciudad
    End If
    
    
    Sis_Previsualizar.Pic.ScaleMode = vbCentimeters
    Sis_Previsualizar.Pic.BackColor = vbWhite
    Sis_Previsualizar.Pic.AutoRedraw = True
    Sis_Previsualizar.Pic.DrawWidth = 1
    Sis_Previsualizar.Pic.DrawMode = 1
    
    Sis_Previsualizar.Pic.FontName = "Courier New"
    Sis_Previsualizar.Pic.FontSize = 10
   
    Cx = 2
    Cy = 1.9
    Dp_S = 0.17
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.CurrentY = Cy
    Sis_Previsualizar.Pic.FontSize = 16  'tama�o de letra
    Sis_Previsualizar.Pic.FontBold = True
    
    '**
    'Sis_Previsualizar.Pic.PaintPicture LoadPicture(App.Path & "\IMG\LAFLOR\LAFLOR.jpg"), Cx + 0.62, 1
    Sis_Previsualizar.Pic.PaintPicture LoadPicture(App.Path & "\REDMAROK.jpg"), Cx + 0.62, 1
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S + Dp_S
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S + Dp_S
    '**
    
    Sis_Previsualizar.Pic.Print SP_Empresa_Activa
        
    
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY
  
    Sis_Previsualizar.Pic.FontSize = 10
  
    pos = Sis_Previsualizar.Pic.CurrentY
    Sis_Previsualizar.Pic.Print "R.U.T.   :" & SP_Rut_Activo
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY
    

    Sis_Previsualizar.Pic.CurrentY = pos
    Sis_Previsualizar.Pic.CurrentX = Cx + 11
    'If Sm_Cli_Pro = "CLI" Then
    '     Sis_Previsualizar.Pic.Print "COMPROBANTE DE INGRESO N�: " & lp_IDAbo
    'Else
         Sis_Previsualizar.Pic.Print "COMPROBANTE DE EGRESO N�: " & Lp_Nro_Comprobante
    'End If
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY
 
 
    Sis_Previsualizar.Pic.FontBold = False
    Sis_Previsualizar.Pic.FontSize = 10
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.Print "GIRO     :" & Sp_Giro
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY
    
  
    Sis_Previsualizar.Pic.FontSize = 10
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.Print "DIRECCION:"; Sp_Direccion
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
    
    Sis_Previsualizar.Pic.CurrentX = Cx
    'If Sm_Cli_Pro = "CLI" Then
    '     Sis_Previsualizar.Pic.Print "Recibi de:"
    'Else
         Sis_Previsualizar.Pic.Print "Pagado a :"
    'End If
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
    Sis_Previsualizar.Pic.FontSize = 10
    pos = Sis_Previsualizar.Pic.CurrentY
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.Print "Nombre:" & TxtRsocial
    'Sis_Previsualizar.Pic.Print "Nombre:" & Sm_NombreAbono
    
    Sis_Previsualizar.Pic.CurrentY = pos
    Sis_Previsualizar.Pic.CurrentX = Cx + 14
    Sis_Previsualizar.Pic.Print "Fecha:" & Me.DtFecha.Value
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
    
    
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.Print "RUT   :" & Me.TxtRutProveedor
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
    
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.FontBold = True
    Sis_Previsualizar.Pic.Print "CONCEPTO :"
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
    
       
    'Aqui agregamos los documentos a los que se abonara
    Sis_Previsualizar.Pic.FontBold = False
    Sp_Nro_Doc = "Nro"
    Sp_Documento = "Documento"
    RSet Sp_Valor = "Valor"
    RSet Sp_Saldo = "Saldo"
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.Print Sp_Nro_Doc & " " & Sp_Documento & " " & Sp_Valor & " " & Sp_Saldo
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S - 0.1
   ' For i = 1 To LVDetalle.ListItems.Count
        Sp_Nro_Doc = Me.TxtNDoc
        Sp_Documento = Me.CboTipoDoc.Text
        RSet Sp_Valor = Me.txtTotal
        RSet Sp_Saldo = 0
        Sis_Previsualizar.Pic.CurrentX = Cx
        Sis_Previsualizar.Pic.Print Sp_Nro_Doc & " " & Sp_Documento & " " & Sp_Valor & " " & Sp_Saldo
        Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S - 0.2
   ' Next
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + 0.2
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.FontBold = True
    Sp_Fpago = CboFpago.Text
    pos = Sis_Previsualizar.Pic.CurrentY
    Sis_Previsualizar.Pic.Print "FORMA DE PAGO:" & CboFpago.Text
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.FontBold = True
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
    Sis_Previsualizar.Pic.Print "TOTAL PAGO   :$" & txtTotal
    
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S + Dp_S
    
    
    Sis_Previsualizar.Pic.FontBold = False
    
    'If CboMPago.ItemData(CboMPago.ListIndex) = 3 Or CboMPago.ItemData(CboMPago.ListIndex) = 4 Then
    '    Sis_Previsualizar.Pic.CurrentX = cx
    '    Sis_Previsualizar.Pic.Print "NRO OPERACION:" & txtNroOperacion
    '    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
    'End If
    finpago = Sis_Previsualizar.Pic.CurrentY
    
    'Aqui detallaremos los cheques
    
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.FontBold = True
    posobs = Sis_Previsualizar.Pic.CurrentY
    Sis_Previsualizar.Pic.Print "OBSERVACIONES:"
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
    Sis_Previsualizar.Pic.FontBold = False
     
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.Print "COMPRA CONTADO"
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
    pos = Sis_Previsualizar.Pic.CurrentY
    Sis_Previsualizar.Pic.CurrentY = pos + 1
    Sis_Previsualizar.Pic.CurrentX = Cx
    
    Sis_Previsualizar.Pic.Print "CONTABILIDAD"
        
    Sis_Previsualizar.Pic.CurrentY = pos + 1
    Sis_Previsualizar.Pic.CurrentX = Cx + 6
    Sis_Previsualizar.Pic.Print "V� B� CAJA"
   
   
   
    Sis_Previsualizar.Pic.CurrentY = pos + 1
    Sis_Previsualizar.Pic.CurrentX = Cx + 12
    Sis_Previsualizar.Pic.Print "NOMBRE, RUT Y FIRMA"
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
    
    
    
     Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY - (Dp_S * 6)
     Sis_Previsualizar.Pic.CurrentX = Cx + 12
     Sis_Previsualizar.Pic.Print "RECIBI CONFORME"
     Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
    
    Sis_Previsualizar.Pic.Line (1.5, 1)-(20, 4.5), , B 'Rectangulo Encabezado y N� Comprobante
    
    Sis_Previsualizar.Pic.Line (1.5, 4.5)-(20, 6.2), , B 'Pagado o Recibido del cliente o proveedor
    
   ' If LVCheques.ListItems.Count > 0 Then
   '    Sis_Previsualizar.Pic.Line (1.5, 6.2)-(20, finpago), , B  'Concepto - documentos pagados o recibidos y forma de pago
   ' Else
    Sis_Previsualizar.Pic.Line (1.5, 6.2)-(20, finpago + 0.15), , B 'Concepto - documentos pagados o recibidos y forma de pago
    'End If
    
    Sis_Previsualizar.Pic.Line (13.5, 6.2)-(16.8, posobs), , B  'columnas de los pagos en efectivo
    Sis_Previsualizar.Pic.Line (13.5, 6.2)-(16.8, 9.37), , B  'columnas de los pagos en efectivo
    
  
    
    Sis_Previsualizar.Pic.Line (1.5, posobs)-(20, posobs + 1.5), , B 'Observaciones
    Sis_Previsualizar.Pic.Line (1.5, posobs + 1.5)-(20, posobs + 1.5 + 2), , B 'Pie del comprobante
        
    pos = Sis_Previsualizar.Pic.CurrentY
    
 '   Sis_Previsualizar.Pic.Height = pos * 0.04

    Sis_Previsualizar.Show 1
    
            If SG_codigo = "print" Then
                On Error GoTo noImprime
                Dialogo.CancelError = True
                Dialogo.ShowPrinter
                ImprimeComprobante
            End If

Exit Sub
ePrev:
MsgBox "Error " & Err.Description & vbNewLine & "Nro " & Err.Number
Err.Clear

noImprime:



End Sub

Private Sub ImprimeComprobante()
    Dim Cx As Double, Cy As Double, Sp_Fecha As String, Ip_Mes As Integer, Dp_S As Double, Sp_Letras As String
    Dim Sp_Neto As String * 12, Sp_IVA As String * 12
    
    Dim Sp_Nro_Comprobante As String
    
    Dim Sp_Empresa As String
    Dim Sp_Giro As String
    Dim Sp_Direccion As String
    
    
    Dim Sp_Nombre As String * 45
    Dim Sp_Rut As String * 15
    Dim Sp_Concepto As String
    
    Dim Sp_Nro_Doc As String * 12
    Dim Sp_Documento As String * 35
    Dim Sp_Valor As String * 15
    Dim Sp_Saldo As String * 15
    Dim Sp_Fpago As String * 24
    Dim Sp_Total As String * 12
    
    Dim Sp_Banco As String * 20
    Dim Sp_NroCheque As String * 10
    Dim Sp_FechaCheque As String * 10
    Dim Sp_ValorCheque As String * 15
    Dim Sp_SumaCheque As String * 15
    
    Dim Sp_Observacion As String * 80
    
    For i = 1 To Dialogo.Copies
        Sql = "SELECT giro,direccion,ciudad " & _
             "FROM sis_empresas " & _
             "WHERE rut='" & SP_Rut_Activo & "'"
        Consulta RsTmp2, Sql
        If RsTmp2.RecordCount > 0 Then
            Sp_Giro = RsTmp2!giro
            Sp_Direccion = RsTmp2!direccion & " - " & RsTmp2!ciudad
        End If
        
        
        
        Printer.FontName = "Courier New"
        Printer.FontSize = 10
        Printer.ScaleMode = 7
        Cx = 2
        Cy = 1.9
        Dp_S = 0.17
        Printer.CurrentX = Cx
        Printer.CurrentY = Cy
        Printer.FontSize = 16  'tama�o de letra
        Printer.FontBold = True
        
        '**
        'Printer.PaintPicture LoadPicture(App.Path & "\IMG\LAFLOR\LAFLOR.jpg"), Cx + 0.62, 1
        Printer.PaintPicture LoadPicture(App.Path & "\REDMAROK.jpg"), Cx + 0.62, 1
        Printer.CurrentY = Printer.CurrentY + Dp_S + Dp_S
        Printer.CurrentY = Printer.CurrentY + Dp_S + Dp_S
        '**
        
        Printer.Print SP_Empresa_Activa
            
        
        Printer.CurrentX = Cx
        Printer.CurrentY = Printer.CurrentY
      
        Printer.FontSize = 10
      
        pos = Printer.CurrentY
        Printer.Print "R.U.T.   :" & SP_Rut_Activo
        Printer.CurrentY = Printer.CurrentY
        
    
        Printer.CurrentY = pos
        Printer.CurrentX = Cx + 11
        'If Sm_Cli_Pro = "CLI" Then
        '     Printer.Print "COMPROBANTE DE INGRESO N�: " & lp_IDAbo
        'Else
             Printer.Print "COMPROBANTE DE EGRESO N�: " & Lp_Nro_Comprobante
        'End If
        Printer.CurrentY = Printer.CurrentY
     
     
        Printer.FontBold = False
        Printer.FontSize = 10
        Printer.CurrentX = Cx
        Printer.Print "GIRO     :" & Sp_Giro
        Printer.CurrentY = Printer.CurrentY
        
        Printer.FontSize = 10
        Printer.CurrentX = Cx
        Printer.Print "DIRECCION:"; Sp_Direccion
        Printer.CurrentY = Printer.CurrentY + Dp_S
        
        Printer.CurrentX = Cx
        Printer.FontBold = True
        If Sm_Cli_Pro = "CLI" Then
             Printer.Print "Recibi de"
        Else
             Printer.Print "Pagado  a "
        End If
        Printer.FontBold = False
        Printer.CurrentY = Printer.CurrentY + Dp_S
        Printer.FontSize = 10
        pos = Printer.CurrentY
        Printer.CurrentX = Cx
        Printer.Print "Nombre:" & Me.TxtRsocial
        
        Printer.CurrentY = pos
        Printer.CurrentX = Cx + 14
        Printer.Print "Fecha:" & Me.DtFecha
        Printer.CurrentY = Printer.CurrentY + Dp_S
        
        
        Printer.CurrentX = Cx
        Printer.Print "RUT   :" & Me.TxtRutProveedor
        Printer.CurrentY = Printer.CurrentY + Dp_S
        
        Printer.CurrentX = Cx
        Printer.FontBold = True
        Printer.Print "CONCEPTO :"
        Printer.CurrentY = Printer.CurrentY + Dp_S
        
           
        'Aqui agregamos los documentos a los que se abonara
        Printer.FontBold = False
        Sp_Nro_Doc = "Nro"
        Sp_Documento = "Documento"
        RSet Sp_Valor = "Valor"
        RSet Sp_Saldo = "Saldo"
        Printer.CurrentX = Cx
        Printer.Print Sp_Nro_Doc & " " & Sp_Documento & " " & Sp_Valor & " " & Sp_Saldo
        Printer.CurrentY = Printer.CurrentY + Dp_S - 0.1
        'For i = 1 To LVDetalle.ListItems.Count
            Sp_Nro_Doc = TxtNDoc
            Sp_Documento = CboTipoDoc.Text
            RSet Sp_Valor = txtTotal
            RSet Sp_Saldo = 0
            Printer.CurrentX = Cx
            Printer.Print Sp_Nro_Doc & " " & Sp_Documento & " " & Sp_Valor & " " & Sp_Saldo
            Printer.CurrentY = Printer.CurrentY + Dp_S - 0.2
        'Next
        Printer.CurrentY = Printer.CurrentY + 0.2
        Printer.CurrentX = Cx
        Printer.FontBold = True
        Sp_Fpago = CboFpago.Text
        pos = Printer.CurrentY
        Printer.Print "FORMA DE PAGO:" & Sp_Fpago
        Printer.CurrentX = Cx
        Printer.FontBold = True
        Printer.CurrentY = Printer.CurrentY + Dp_S
        Printer.Print "TOTAL PAGO   :$" & txtTotal
        
        Printer.CurrentY = Printer.CurrentY + Dp_S + Dp_S
        
        
        Printer.FontBold = False
        
        
        finpago = Printer.CurrentY
        
        
        Printer.CurrentY = Printer.CurrentY + Dp_S
        Printer.CurrentX = Cx
        Printer.FontBold = True
        posobs = Printer.CurrentY
        Printer.Print "OBSERVACIONES:"
        Printer.CurrentY = Printer.CurrentY + Dp_S
        Printer.FontBold = False
         
        Printer.CurrentX = Cx
        Printer.Print "COMPRA CONTADO"
        Printer.CurrentY = Printer.CurrentY + Dp_S
        pos = Printer.CurrentY
        Printer.CurrentY = pos + 1
        Printer.CurrentX = Cx
        
        Printer.Print "CONTABILIDAD"
            
        Printer.CurrentY = pos + 1
        Printer.CurrentX = Cx + 6
        Printer.Print "V� B� CAJA"
       
        Printer.CurrentY = pos + 1
        Printer.CurrentX = Cx + 12
        Printer.Print "NOMBRE, RUT Y FIRMA"
        Printer.CurrentY = Printer.CurrentY + Dp_S
        
        
         Printer.CurrentY = Printer.CurrentY - (Dp_S * 6)
        Printer.CurrentX = Cx + 12
        Printer.Print "RECIBI CONFORME"
        Printer.CurrentY = Printer.CurrentY + Dp_S
        
       ' Printer.DrawMode = 1
       ' Printer.DrawWidth = 3
        
        Printer.Line (1.5, 1)-(20, 4.5), , B 'Rectangulo Encabezado y N� Comprobante
        
        Printer.Line (1.5, 4.5)-(20, 6.2), , B 'Pagado o Recibido del cliente o proveedor
        
        'If LVCheques.ListItems.Count > 0 Then
        '    Printer.Line (1.5, 6.2)-(20, finpago), , B  'Concepto - documentos pagados o recibidos y forma de pago
        'Else
        Printer.Line (1.5, 6.2)-(20, finpago + 0.15), , B 'Concepto - documentos pagados o recibidos y forma de pago
        'End If
        
        Printer.Line (13.5, 6.2)-(16.8, posobs), , B  'columnas de los pagos en efectivo
        'Printer.Line (13.5, 6.2)-(16.8, 9.37), , B  'columnas de los pagos en efectivo
        
        
        
        Printer.Line (1.5, posobs)-(20, posobs + 1.5), , B 'Observaciones
        Printer.Line (1.5, posobs + 1.5)-(20, posobs + 1.5 + 2), , B 'Pie del comprobante
        
        Printer.NewPage
        Printer.EndDoc
    Next
End Sub






Private Function ValorOriginal(Tipo As String, Cod As String) As Double
    Dim Rp_Vo As Recordset
    Select Case Tipo
        Case "Neto"
            Sql = "SELECT cmd_total_neto xprecio "
        Case "Bruto"
            Sql = "SELECT cmd_total_bruto xprecio "
    End Select
    Sql = Sql & "FROM com_doc_compra c " & _
                "INNER JOIN com_doc_compra_detalle d USING(id) " & _
                "WHERE c.id=" & TxtDocumentoRef.Tag & " AND pro_codigo='" & Cod & "'"
    Consulta Rp_Vo, Sql
    If Rp_Vo.RecordCount > 0 Then
        ValorOriginal = CxP(Str(Rp_Vo!xprecio))
    Else
        ValorOriginal = 0
    End If
End Function
Private Sub SalidaNotaDebito(En_Sa As String)
    Dim Sp_Codigos As String, Rp_ND As Recordset
    'Items detalle
    
    Sp_Codigos = Empty
    With compra_detalle.LvDetalle
        For i = 1 To .ListItems.Count
            Sp_Codigos = Sp_Codigos & .ListItems(i).SubItems(1) & ","
        Next
        If Len(Sp_Codigos) > 0 Then Sp_Codigos = Mid(Sp_Codigos, 1, Len(Sp_Codigos) - 1)
        Sql = "SELECT kar_id,pro_codigo,kar_cantidad,pro_precio_neto " & _
                "FROM inv_kardex " & _
                "WHERE pro_codigo IN(" & Sp_Codigos & ") AND doc_id=" & FrameRef.Tag & " AND kar_numero=" & TxtNroRef
        Consulta Rp_ND, Sql
        If Rp_ND.RecordCount > 0 Then
            Rp_ND.MoveFirst
            Do While Not Rp_ND.EOF
                Kardex Fql(DtFecha.Value), En_Sa, CboTipoDoc.ItemData(CboTipoDoc.ListIndex), TxtNDoc, CboBodega.ItemData(CboBodega.ListIndex), _
                Rp_ND!pro_codigo, Rp_ND!kar_cantidad, "ACTUALIZA POR " & CboTipoDoc.Text & " Nro:" & TxtNDoc, Rp_ND!pro_precio_neto, _
                Rp_ND!pro_precio_neto * Rp_ND!kar_cantidad, TxtRutProveedor, TxtRsocial, "SI", Rp_ND!pro_precio_neto, CboTipoDoc.ItemData(CboTipoDoc.ListIndex)
    
            
            
                Rp_ND.MoveNext
            Loop
        
        
        End If
    End With
End Sub


Private Sub LimpiaDatos()
    TxtRutProveedor = ""
    TxtRsocial = ""
    Me.TxtNDoc = ""
    TxtExento = "0"
    TxtNeto = "0"
    txtIVA = "0"
    txtIvaRetenido = 0
    txtSubNeto = 0
    txtTotalNeto = 0
    txtTotal = 0
    txtIVAMargen = 0
    txtMargenDist = 0
    txtIvaSinCredito = 0
    TxtSubTotal = 0
    For i = 1 To LvImpuestos.ListItems.Count
        LvImpuestos.ListItems(i).SubItems(2) = 0
    Next
   LvCuentaContable.ListItems.Clear
   SkRequerido = 0
   TxtTotalCuentas = 0
    TxtDocumentoRef = ""
    TxtNroRef = ""
    TxtTotalRef = ""
    FrameRef.Tag = ""
    FrameRef.Visible = False
End Sub

Private Sub CmdNuevoProveedor_Click()
    SG_codigo = Empty
    AgregoProveedor.Show 1
    If SG_codigo <> Empty Then TxtRutProveedor = SG_codigo
End Sub

Private Sub CmdOk_Click()
    If Len(TxtCodigoCuenta) = 0 Or Val(TxtCodigoCuenta) = 0 Then
        MsgBox "Debe ingresar cuenta contable...", vbInformation, "AlvaMar"
        TxtCodigoCuenta.SetFocus
        Exit Sub
    End If
    If Val(TxtValorCuenta) = 0 Then
        MsgBox "Debe ingresar valor para la cuenta ...", vbInformation
        TxtValorCuenta.SetFocus
        Exit Sub
    End If
    'Agregar La cuenta
    
    LvCuentaContable.ListItems.Add , , ""
    LvCuentaContable.ListItems(LvCuentaContable.ListItems.Count).SubItems(1) = TxtCodigoCuenta
    LvCuentaContable.ListItems(LvCuentaContable.ListItems.Count).SubItems(2) = TxtNombre
    LvCuentaContable.ListItems(LvCuentaContable.ListItems.Count).SubItems(3) = NumFormat(TxtValorCuenta)
    SumaCuentas
    TxtCodigoCuenta = ""
    TxtNombre = ""
    TxtValorCuenta = ""
    TxtCodigoCuenta.SetFocus
End Sub

Private Sub CmdOkImp_Click()
    CalculaAdicionales
End Sub
Private Sub CalculaAdicionales()
    If Val(TxtImpuesto.Tag) = 0 Then Exit Sub
    LvImpuestos.Tag = 0
    SkImpCosto = 0
    SkImpCredito = 0
    SkTotalAdicionales = 0
    
    For i = 1 To LvImpuestos.ListItems.Count
        If LvImpuestos.ListItems(i) = Val(TxtImpuesto.Tag) Then
            LvImpuestos.ListItems(i).SubItems(2) = Format(CDbl(TxtValorImp), "#,0")
            'Exit For
        End If
        LvImpuestos.Tag = LvImpuestos.Tag + CDbl(LvImpuestos.ListItems(i).SubItems(2))
        If LvImpuestos.ListItems(i).SubItems(4) = "COSTO" Then
            SkImpCosto = SkImpCosto + CDbl(LvImpuestos.ListItems(i).SubItems(2))
        End If
        SkTotalAdicionales = SkTotalAdicionales + CDbl(LvImpuestos.ListItems(i).SubItems(2))
        
    Next
    TxtImpuesto.Tag = 0
    TxtImpuesto = ""
    TxtValorImp = "0"
    SkRequerido = NumFormat(CDbl(TxtExento) + CDbl(TxtNeto) + CDbl(txtIvaSinCredito) + CDbl(SkImpCosto))


    CTotal
    Factores
    LvImpuestos.SetFocus

End Sub


Private Sub Factores()
    'Calcular Factor del impuesto
    If CDbl(TxtNeto) > 0 Then
        For i = 1 To LvImpuestos.ListItems.Count
            LvImpuestos.ListItems(i).SubItems(3) = Format(CDbl(LvImpuestos.ListItems(i).SubItems(2)) / CDbl(TxtNeto) * 100, "#,0.#0")
        Next
    End If

End Sub
Private Sub CmdSalir_Click()
    On Error Resume Next
    Unload Me
End Sub
Private Sub Command1_Click()
    MsgBox Right(CboPlazos.Text, 5)
End Sub

Private Sub Command2_Click()
       Lp_SaldoRef = SaldoDocumento(Me.CmdLlamaOC.Tag, "PRO")
        MsgBox Lp_SaldoRef
End Sub

Private Sub Command3_Click()
 On Error Resume Next
    Unload Me
End Sub

Private Sub DtFecha_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then CboPlazos.SetFocus
End Sub


Private Sub DtFecha_LostFocus()
    If Val(Year(DtFecha)) < IG_Ano_Contable Then
       
    Else
        If (Month(DtFecha) + Year(DtFecha)) > (IG_Ano_Contable + IG_Mes_Contable) Then
            MsgBox "No puede ingresar documentos con FECHA posterior al periodo contable...", vbInformation
            DtFecha.SetFocus
            Exit Sub
        End If
    End If
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
            On Error Resume Next
            SendKeys "{tab}"
            KeyAscii = 0
    End If
End Sub

Private Sub Form_Load()
    Centrar Me, True
    'TITULO EMPRESA ACTIVA
    Lp_TopFrmProveedor = Me.FrmProveedor.Top
    SkRut = SP_Rut_Activo
    SkEmpresa = SP_Empresa_Activa
    'FIN TITULO
    CboCaja.ListIndex = 1
    If SG_Modulo_Caja = "SI" Then
        Me.FrmCaja.Visible = True
    Else
        Me.FrmCaja.Visible = False
    End If
    
    
    Sm_DistribuidorGas = "NO"
    Lm_Top1 = Frame3.Top
    Lm_Top2 = Me.FrmProveedor.Top
    Bm_Permite_Documento = False
    Aplicar_skin Me
    DtFecha = Date
    Sm_Nota = Empty
    LLenarCombo CboBodega, "bod_nombre", "bod_id", "par_bodegas", "bod_activo='SI' AND rut_emp='" & SP_Rut_Activo & "'", "bod_id"
    CboBodega.ListIndex = 0
    If SP_Control_Inventario = "SI" Then LLenarCombo CboTipoDoc, "doc_nombre", "doc_id", "sis_documentos", "doc_libro_gastos='NO' AND doc_documento='COMPRA' AND doc_activo='SI' AND doc_honorarios='NO' AND doc_contable='SI'", "doc_orden"
    If SP_Control_Inventario = "NO" Then LLenarCombo CboTipoDoc, "doc_nombre", "doc_id", "sis_documentos", "doc_libro_gastos='NO' AND doc_documento='COMPRA' AND doc_activo='SI' AND doc_honorarios='NO'  AND doc_contable='SI'", "doc_orden"
    
    LLenarCombo CboPlazos, "CONCAT(pla_nombre,'                                                      ',CAST(pla_id AS CHAR ))", "pla_dias", "par_plazos_vencimiento", "pla_activo='SI'", "pla_id"
    CboPlazos.AddItem "FONDOS POR RENDIR"
    CboPlazos.ItemData(CboPlazos.ListCount - 1) = 9999
    CboPlazos.AddItem "ANTICIPO DE PROVEEDOR"
    CboPlazos.ItemData(CboPlazos.ListCount - 1) = 4444
    
    
    
    CboPlazos.ListIndex = 0
    TxtMesContable = UCase(MonthName(IG_Mes_Contable))
    TxtAnoContable = IG_Ano_Contable
    
    Sql = "SELECT e.imp_id,CONCAT(imp_nombre,'    (',LCASE(ime_costo_credito),')') ,0 valor,0 factor,ime_costo_credito  " & _
          "FROM par_impuestos_empresas e " & _
          "INNER JOIN par_impuestos i USING(imp_id) " & _
          "WHERE e.rut='" & SP_Rut_Activo & "'"
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvImpuestos, False, True, True, False
    CboFpago.ListIndex = -1
    Sm_Nota = Empty
End Sub
Private Sub CTotal()
    Dim lp_Impuestos As Long
    lp_Impuestos = 0
    For i = 1 To LvImpuestos.ListItems.Count
        lp_Impuestos = lp_Impuestos + CDbl(LvImpuestos.ListItems(i).SubItems(2))
    Next
    TxtSubTotal = NumFormat(CDbl(TxtExento) + CDbl(TxtNeto) + CDbl(txtIVA) - CDbl(txtIvaRetenido)) + CDbl(txtIvaSinCredito) + CDbl(txtIVAMargen)
    txtTotal = Format(CDbl(TxtExento) + CDbl(TxtNeto) + CDbl(txtIVA) - CDbl(Me.txtIvaRetenido) + lp_Impuestos + CDbl(txtIvaSinCredito) + CDbl(txtIVAMargen), "#,0")
    If CDbl(txtIVA) > 0 Then
        SkTasaIVA = DG_IVA
    Else
        SkTasaIVA = 0
    End If
End Sub
Private Sub Form_Unload(Cancel As Integer)
    SG_codigo = ""
    SG_codigo2 = ""
'    Unload compra_detalle
End Sub

Private Sub LvCuentaContable_DblClick()
    If LvCuentaContable.SelectedItem Is Nothing Then Exit Sub
    TxtCodigoCuenta = LvCuentaContable.SelectedItem.SubItems(1)
    TxtNombre = LvCuentaContable.SelectedItem.SubItems(2)
    TxtValorCuenta = CDbl(LvCuentaContable.SelectedItem.SubItems(3))
    LvCuentaContable.ListItems.Remove LvCuentaContable.SelectedItem.Index
    SumaCuentas
    TxtCodigoCuenta.SetFocus
End Sub
Private Sub SumaCuentas()
    Me.TxtTotalCuentas = NumFormat(TotalizaColumna(LvCuentaContable, "total"))
End Sub


Private Sub LvImpuestos_DblClick()
    If LvImpuestos.SelectedItem Is Nothing Then Exit Sub
    TxtImpuesto.Tag = LvImpuestos.SelectedItem
    TxtImpuesto = LvImpuestos.SelectedItem.SubItems(1)
    TxtValorImp = LvImpuestos.SelectedItem.SubItems(2)
    TxtValorImp.SetFocus
End Sub

Private Sub LvImpuestos_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 13 Then
        If LvImpuestos.SelectedItem Is Nothing Then Exit Sub
        TxtImpuesto.Tag = LvImpuestos.SelectedItem
        TxtImpuesto = LvImpuestos.SelectedItem.SubItems(1)
        TxtValorImp = LvImpuestos.SelectedItem.SubItems(2)
        TxtValorImp.SetFocus
    End If
End Sub


Private Sub Timer1_Timer()
    On Error Resume Next
    DtFecha.SetFocus
    Timer1.Enabled = False
End Sub

Private Sub TxtAutorizadoPor_GotFocus()
    En_Foco Me.ActiveControl
End Sub

Private Sub TxtAutorizadoPor_KeyPress(KeyAscii As Integer)
KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub TxtCodigoCuenta_DblClick()
            'Cargaremos bitacora de productos
        Sql = "SELECT  c.pla_id codigo,p.pla_nombre descripcion " & _
                "FROM    com_doc_compra_detalle c " & _
                "JOIN com_doc_compra r ON c.id=r.id " & _
                "INNER JOIN con_plan_de_cuentas p ON c.pla_id= p.pla_id " & _
                "WHERE fecha>'" & Date - 30 & "'  AND r.rut_emp='" & SP_Rut_Activo & "' " & _
                "GROUP BY c.pla_id " & _
                "LIMIT 5"

        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
            'Mostraremos Bitocara
            Inv_BitacoraProductos.Show 1
            If Len(SG_codigo) > 0 Then
                TxtCodigoCuenta = SG_codigo
            End If
        End If
End Sub

Private Sub TxtCodigoCuenta_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
     If KeyAscii = 39 Then KeyAscii = 0
    If Len(TxtCodigoCuenta) = 2 And KeyAscii <> 8 Then
        If Val(TxtCodigoCuenta) = 0 Then
            With BuscarSimple
                .Sm_Busqueda_por_Defecto = TxtCodigoCuenta & Chr(KeyAscii)
                SG_codigo = Empty
                .Sm_Consulta = "SELECT pla_id, pla_nombre,tpo_nombre,det_nombre " & _
                               "FROM    con_plan_de_cuentas p,  con_tipo_de_cuenta t,   con_detalle_tipo_cuenta d " & _
                               "WHERE   p.tpo_id = t.tpo_id AND p.det_id = d.det_id "
                .Sm_CampoLike = "pla_nombre"
                .Im_Columnas = 2
                .CmdCuentasContables.Visible = True
                .Show 1
                If SG_codigo = Empty Then Exit Sub
                
                TxtCodigoCuenta = Val(SG_codigo)
               ' TxtCodigoCuenta.SetFocus
                KeyAscii = 0
              '  TxtCodigoCuenta_Validate True
                On Error Resume Next
                SendKeys "{tab}"
               ' TxtValorCuenta.SetFocus
            End With
        End If
    End If
End Sub

Private Sub TxtCodigoCuenta_Validate(Cancel As Boolean)
    If Len(TxtCodigoCuenta) = 0 Then
        TxtNombre = 0
        Exit Sub
    End If
    Sql = "SELECT pla_nombre,tpo_id,det_id,pla_analisis " & _
            "FROM con_plan_de_cuentas " & _
            "WHERE pla_id='" & TxtCodigoCuenta & "'"
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        TxtNombre = RsTmp!pla_nombre
        
        'Aqui comprobamos si corresponde a Activo inmovilizado
        If RsTmp!tpo_id = 1 And RsTmp!det_id = 4 And RsTmp!pla_analisis = "SI" Then
            'Activo inmobilizado de ser ingresado en formulario de compra normal
            MsgBox "Para ingresar Activos Fijos, debe ir a bot�n COMPRAS--F1 NUEVA..."
            TxtCodigoCuenta = ""
            TxtNombre = ""
            Cancel = True
            Exit Sub
        End If
      
        Cancel = False
    Else
        MsgBox "Cuenta no encontrada...", vbInformation
        TxtCodigoCuenta = ""
        TxtNombre = ""
        Cancel = True
    End If
End Sub

Private Sub TxtDescuentoNeto_GotFocus()
    En_Foco Me.ActiveControl
End Sub

Private Sub TxtDescuentoNeto_KeyPress(KeyAscii As Integer)
    KeyAscii = AceptaSoloNumeros(KeyAscii)
    If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtDescuentoNeto_Validate(Cancel As Boolean)
    If SinDesborde(Me.ActiveControl) Then
        Cancel = False
    Else
        Cancel = True
        TxtDescuentoNeto = 0 'ESTE SE REEMPLAZA POR EL CONTROL ACTIVO
    End If
    
    ValidarControl
    
    TxtNeto = Format(CDbl(txtSubNeto) - CDbl(TxtDescuentoNeto), "#,0")
    CalculaIva
    SkRequerido = NumFormat(CDbl(TxtExento) + CDbl(TxtNeto) + CDbl(txtIvaSinCredito) + CDbl(SkImpCosto))
End Sub

Private Sub TxtExento_GotFocus()
    En_Foco Me.ActiveControl
End Sub

Private Sub TxtExento_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
     If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtExento_Validate(Cancel As Boolean)
    'ESTO ES PARA EVITAR ERROR POR DESBORDAMIENTO
    'EVITANDO LA CAIDA DEL SISTEMA
    If SinDesborde(Me.ActiveControl) Then
        Cancel = False
    Else
        Cancel = True
        TxtExento = 0 'ESTE SE REEMPLAZA POR EL CONTROL ACTIVO
    End If
    'REEMPLAZAR EL NOMBRE DEL CONTROL EJ: TXTSDIVA  , TXTNETO,
    '----------------------------------------

    
    ValidarControl
    
    SkRequerido = NumFormat(CDbl(TxtExento) + CDbl(TxtNeto) + CDbl(txtIvaSinCredito) + CDbl(SkImpCosto))
End Sub

Private Sub TxtIva_GotFocus()
    En_Foco txtIVA
End Sub

Private Sub txtIVA_KeyPress(KeyAscii As Integer)
    KeyAscii = AceptaSoloNumeros(KeyAscii)
End Sub

Private Sub txtIVA_Validate(Cancel As Boolean)
    ValidarControl
End Sub

Private Sub txtIVAMargen_GotFocus()
    En_Foco txtIVAMargen
End Sub

Private Sub txtIVAMargen_KeyPress(KeyAscii As Integer)
     KeyAscii = AceptaSoloNumeros(KeyAscii)
End Sub

Private Sub txtIVAMargen_Validate(Cancel As Boolean)
    If Val(txtIVAMargen) = 0 Then
        txtIVAMargen = 0
    Else
        txtIVAMargen = NumFormat(txtIVAMargen)
    End If
    CTotal
End Sub

Private Sub txtIvaRetenido_GotFocus()
    En_Foco Me.ActiveControl
End Sub

Private Sub TxtIVARetenido_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
     If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub txtIvaRetenido_Validate(Cancel As Boolean)
    ValidarControl
    
End Sub









Private Sub txtIvaSinCredito_GotFocus()
    En_Foco txtIvaSinCredito
End Sub

Private Sub txtIvaSinCredito_KeyPress(KeyAscii As Integer)
    KeyAscii = AceptaSoloNumeros(KeyAscii)
     If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub txtIvaSinCredito_Validate(Cancel As Boolean)
    If Val(txtIvaSinCredito) = 0 Then
        txtIvaSinCredito = 0
    Else
        txtIvaSinCredito = NumFormat(txtIvaSinCredito)
    End If
    If SinDesborde(Me.ActiveControl) Then
        Cancel = False
    Else
        Cancel = True
        txtIvaSinCredito = 0 'ESTE SE REEMPLAZA POR EL CONTROL ACTIVO
    End If
    
    ValidarControl
    SkRequerido = NumFormat(CDbl(TxtExento) + CDbl(TxtNeto) + CDbl(txtIvaSinCredito) + CDbl(SkImpCosto))
    CTotal
End Sub



Private Sub txtMargenDist_GotFocus()
    En_Foco txtMargenDist
End Sub

Private Sub txtMargenDist_KeyPress(KeyAscii As Integer)
     KeyAscii = AceptaSoloNumeros(KeyAscii)
End Sub

Private Sub txtMargenDist_Validate(Cancel As Boolean)
    If Val(txtMargenDist) = 0 Then
        txtMargenDist = "0"
    Else
        txtMargenDist = NumFormat(txtMargenDist)
    End If
End Sub

Private Sub TxtNDoc_GotFocus()
    On Error Resume Next
    En_Foco Me.ActiveControl
End Sub

Private Sub TxtNDoc_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
     If KeyAscii = 39 Then KeyAscii = 0
End Sub
Private Sub TxtNDoc_Validate(Cancel As Boolean)
   ' ComprobarSiDocumento DG_ID_Unico
   VerificarUsado
   
   'If Not Bm_Permite_Documento Then TxtNDoc = ""
End Sub
Private Sub VerificarUsado()
    Bm_Permite_Documento = False
    If Val(TxtNDoc) > 0 And Sm_EsOC = "SI" Then
        Sql = "SELECT id " & _
                "FROM com_doc_compra " & _
                "WHERE doc_id=" & ObtID(CboTipoDoc) & " AND  no_documento=" & TxtNDoc & " AND rut_emp='" & SP_Rut_Activo & "'"
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
            MsgBox "Orden  de Compra Nro " & TxtNDoc & " ya ha sido utilizado...", vbInformation
            TxtNDoc = ""
            Exit Sub
        End If
    End If
    If CboTipoDoc.ListIndex > -1 And Val(TxtNDoc) > 0 And Len(TxtRutProveedor) > 0 Then
        Respuesta = VerificaRut(TxtRutProveedor, NuevoRut)
        TxtRutProveedor = NuevoRut
    
        Sql = "SELECT id,doc_orden_de_compra " & _
              "FROM com_doc_compra " & _
              "INNER JOIN sis_documentos d USING(doc_id) " & _
              "WHERE doc_id=" & ObtID(CboTipoDoc) & " AND no_documento=" & TxtNDoc & " AND rut='" & TxtRutProveedor & "' "
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
            If RsTmp!doc_orden_de_compra = "NO" Then
                TxtNDoc = ""
                MsgBox " Este documento ya ha sido utilizado...", vbInformation
                TxtNDoc.SetFocus
            Else
                Sql = "SELECT id " & _
                        "FROM com_doc_compra " & _
                        "INNER JOIN sis_documentos d USING(doc_id) " & _
                        "WHERE doc_id=" & ObtID(CboTipoDoc) & " AND no_documento=" & TxtNDoc & " AND rut_emp='" & SP_Rut_Activo & "' "
                Consulta RsTmp, Sql
                If RsTmp.RecordCount > 0 Then
                    MsgBox " Nro de OC ya ha sido utilizado...", vbInformation
                    TxtNDoc = ""
                    TxtNDoc.SetFocus
                Else
                     Bm_Permite_Documento = True
                End If
            End If
        Else
            Bm_Permite_Documento = True
        End If
    End If
End Sub

Private Sub TxtNeto_GotFocus()
    En_Foco Me.ActiveControl
End Sub

Private Sub TxtNeto_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
End Sub
Private Sub CalculaIva()
    txtIVA = Format(CDbl(TxtNeto) / 100 * DG_IVA, "#,0")
    CTotal
    Factores
End Sub




Private Sub TxtRetiradoPor_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub


Private Sub TxtRutProveedor_GotFocus()
    On Error Resume Next
    En_Foco Me.ActiveControl
End Sub

Private Sub TxtRutProveedor_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
    If KeyAscii = 13 Then TxtRutProveedor_Validate True
     If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtRutProveedor_Validate(Cancel As Boolean)
If Len(TxtRutProveedor) > 0 Then
        VerificarUsado
        If Not Bm_Permite_Documento Then Cancel = False
        If Bm_Permite_Documento = False Then
            TxtNDoc.SetFocus
            Exit Sub
        End If
        
    '    txtIvaSinCredito.Locked = True
        txtIVAMargen.Locked = True
        txtMargenDist.Locked = True
        txtIvaSinCredito = 0
        txtIVAMargen = 0
        txtMargenDist = 0
        
        txtIvaSinCredito.BackColor = &H8000000F
        txtIVAMargen.BackColor = &H8000000F
        txtMargenDist.BackColor = &H8000000F

        
        Sql = "SELECT nombre_empresa,rut_emp,prv_distribuidor_de_gas gas " & _
              "FROM maestro_proveedores " & _
              "WHERE rut_proveedor='" & TxtRutProveedor & "' AND habilitado='SI' "
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
            Sql = "SELECT rut_emp " & _
                  "FROM par_asociacion_ruts " & _
                  "WHERE rut_emp='" & SP_Rut_Activo & "' AND rut_pro='" & TxtRutProveedor & "'"
            Consulta RsTmp2, Sql
            If RsTmp2.RecordCount = 0 Then
                Sql = "INSERT INTO par_asociacion_ruts (rut_emp,rut_pro) " & _
                      "VALUES('" & SP_Rut_Activo & "','" & TxtRutProveedor & "')"
                cn.Execute Sql
            End If
            
        
            TxtRsocial = RsTmp!nombre_empresa
            Sm_DistribuidorGas = RsTmp!gas
            
            If Sm_DistribuidorGas = "SI" Then
                txtIvaSinCredito.Locked = False
                txtIVAMargen.Locked = False
                txtMargenDist.Locked = False
                
                txtIvaSinCredito.BackColor = vbWhite
                txtIVAMargen.BackColor = vbWhite
                txtMargenDist.BackColor = vbWhite
                
            End If
            
            If TxtRutProveedor = Empty Then Exit Sub
            
            If CboTipoDoc.ListIndex > -1 And Mid(CboTipoDoc.Text, 1, 7) = "FACTURA" Then
             
                If CboTipoDoc.ItemData(CboTipoDoc.ListIndex) <> IG_id_GuiaProveedor Then
                    'Aqui buscamos si el Proveedor a emitido guias y no estan facturadas
                    Sql = "SELECT id,fecha,no_documento,rut,nombre_proveedor,neto,iva,total " & _
                          "FROM com_doc_compra " & _
                          "WHERE  rut_emp='" & SP_Rut_Activo & "' AND rut='" & TxtRutProveedor & "' AND doc_id=" & IG_id_GuiaProveedor & " AND nro_factura=0 "
                    Consulta RsTmp2, Sql
                    If RsTmp2.RecordCount > 0 Then
                        'Se econtraron Guias no facturadas de este proveedor
                        'Abrimos ventana de guias no facturadas
                         'LLenar_Grilla RsTmp2, Me, LvDetalle, True, True, True, False
                        'Stab.TabVisible(1) = True
                        '�Stab.Tab = 1
                        RutBuscado = TxtRutProveedor
                        
                        compra_FacturaGuias.Im_Id_Factura = CboTipoDoc.ItemData(CboTipoDoc.ListIndex)
                        compra_FacturaGuias.Sm_Plazo = Format(DtFecha.Value + CboPlazos.ItemData(CboPlazos.ListIndex), "YYYY-MM-DD")
                        compra_FacturaGuias.Sm_Exento = Me.TxtExento
                        compra_FacturaGuias.Sm_Fecha = Fql(DtFecha)
                        compra_FacturaGuias.Sm_NroDoc = TxtNDoc
                        compra_FacturaGuias.Sm_TipoDoc = CboTipoDoc.Text
                        compra_FacturaGuias.Sm_Rsocial = TxtRsocial
                        compra_FacturaGuias.Show 1
                        If SG_codigo2 = "grabado" Then
                            TxtRutProveedor = ""
                            TxtNDoc = ""
                            TxtRsocial = ""
                            CmdSalir_Click
                          '  Unload Me
                        End If
                    End If
               End If
            End If
            
        Else
            MsgBox "Proveedor no encontrado... ", vbOKOnly + vbInformation
            TxtRsocial = ""
            
            Exit Sub
        End If
    End If
End Sub
Private Sub ValidarControl()
    If Val(Me.ActiveControl.Text) = 0 Then
        Me.ActiveControl.Text = 0
    Else
        Me.ActiveControl.Text = Format(Me.ActiveControl.Text, "#,0")
    End If
    'Aqui sumaremos todo
    CTotal
End Sub

Private Sub TxtSolicitadoPor_GotFocus()
    En_Foco Me.ActiveControl
End Sub

Private Sub TxtSolicitadoPor_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub txtSubNeto_GotFocus()
    En_Foco Me.ActiveControl
End Sub

Private Sub txtSubNeto_KeyPress(KeyAscii As Integer)
        KeyAscii = AceptaSoloNumeros(KeyAscii)
         If KeyAscii = 39 Then KeyAscii = 0
End Sub
Private Sub txtSubNeto_Validate(Cancel As Boolean)

    If SinDesborde(Me.ActiveControl) Then
        Cancel = False
    Else
        Cancel = True
        txtSubNeto = 0 'ESTE SE REEMPLAZA POR EL CONTROL ACTIVO
    End If
    
    ValidarControl
    TxtNeto = Format(CDbl(txtSubNeto) - CDbl(TxtDescuentoNeto), "#,0")
    TxtValorCuenta = TxtNeto
    CalculaIva
    SkRequerido = NumFormat(CDbl(TxtExento) + CDbl(TxtNeto) + CDbl(txtIvaSinCredito) + CDbl(SkImpCosto))
End Sub
Private Sub TxtValorCuenta_GotFocus()
    En_Foco Me.ActiveControl
End Sub

Private Sub TxtValorCuenta_KeyPress(KeyAscii As Integer)
    KeyAscii = AceptaSoloNumeros(KeyAscii)
     If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtValorCuenta_Validate(Cancel As Boolean)
    If Val(TxtValorCuenta) = 0 Then TxtValorCuenta = "0"
End Sub

Private Sub TxtValorImp_GotFocus()
    En_Foco Me.ActiveControl
End Sub
Private Sub TxtValorImp_KeyPress(KeyAscii As Integer)
    KeyAscii = AceptaSoloNumeros(KeyAscii)
End Sub
Private Sub TxtValorImp_Validate(Cancel As Boolean)
    ValidarControl
End Sub
