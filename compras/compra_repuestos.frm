VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form compra_repuestos 
   Caption         =   "Ingreso de Compras"
   ClientHeight    =   8850
   ClientLeft      =   -2655
   ClientTop       =   1710
   ClientWidth     =   14925
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   ScaleHeight     =   8850
   ScaleWidth      =   14925
   Begin VB.Frame FrameGuias 
      Caption         =   "Guias Facturadas"
      Height          =   4830
      Left            =   435
      TabIndex        =   61
      Top             =   9000
      Visible         =   0   'False
      Width           =   14190
      Begin MSComctlLib.ListView LvDetalle 
         Height          =   3600
         Left            =   720
         TabIndex        =   62
         Top             =   660
         Width           =   12855
         _ExtentX        =   22675
         _ExtentY        =   6350
         View            =   3
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   8
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "T1000"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "T1300"
            Text            =   "Fecha"
            Object.Width           =   2293
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "N109"
            Text            =   "Nro Guia"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Object.Tag             =   "T800"
            Text            =   "Rut"
            Object.Width           =   2469
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Object.Tag             =   "T3000"
            Text            =   "Nombre Proveedor"
            Object.Width           =   7408
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   5
            Object.Tag             =   "N100"
            Text            =   "Neto"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   6
            Object.Tag             =   "N100"
            Text            =   "IVA"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   7
            Object.Tag             =   "N100"
            Text            =   "Total"
            Object.Width           =   2117
         EndProperty
      End
   End
   Begin VB.Frame FrameDatos 
      Caption         =   "Datos Contables"
      Height          =   885
      Left            =   300
      TabIndex        =   63
      Top             =   2160
      Width           =   14460
      Begin VB.TextBox TxtSdIva 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   12180
         TabIndex        =   79
         Tag             =   "N"
         Text            =   "0"
         Top             =   480
         Visible         =   0   'False
         Width           =   1050
      End
      Begin VB.TextBox TxtSdIvaRetenido 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   13230
         TabIndex        =   80
         Tag             =   "N"
         Text            =   "0"
         Top             =   480
         Visible         =   0   'False
         Width           =   1050
      End
      Begin VB.TextBox TxtSDNeto 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   11145
         TabIndex        =   78
         Tag             =   "N"
         Text            =   "0"
         Top             =   480
         Visible         =   0   'False
         Width           =   1050
      End
      Begin VB.TextBox TxtSdExento 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   10110
         TabIndex        =   77
         Tag             =   "N"
         Text            =   "0"
         Top             =   480
         Visible         =   0   'False
         Width           =   1050
      End
      Begin VB.CommandButton CmdOkSd 
         Caption         =   "Ok"
         Height          =   315
         Left            =   14250
         TabIndex        =   81
         Top             =   480
         Visible         =   0   'False
         Width           =   330
      End
      Begin VB.ComboBox CboCentroCosto 
         Height          =   315
         Left            =   6375
         Style           =   2  'Dropdown List
         TabIndex        =   6
         Top             =   495
         Width           =   3150
      End
      Begin VB.ComboBox CboItemGasto 
         Height          =   315
         Left            =   9495
         Style           =   2  'Dropdown List
         TabIndex        =   7
         Top             =   180
         Width           =   3150
      End
      Begin VB.ComboBox CboCuenta 
         Height          =   315
         Left            =   105
         Style           =   2  'Dropdown List
         TabIndex        =   4
         Top             =   495
         Width           =   3150
      End
      Begin VB.ComboBox CboArea 
         Height          =   315
         Left            =   3240
         Style           =   2  'Dropdown List
         TabIndex        =   5
         Top             =   495
         Width           =   3150
      End
      Begin VB.CommandButton cmdCuenta 
         Caption         =   "Cuenta"
         Height          =   225
         Left            =   105
         TabIndex        =   64
         Top             =   240
         Width           =   870
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkItem 
         Height          =   180
         Left            =   9555
         OleObjectBlob   =   "compra_repuestos.frx":0000
         TabIndex        =   65
         Top             =   300
         Width           =   2055
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkCC 
         Height          =   180
         Left            =   6360
         OleObjectBlob   =   "compra_repuestos.frx":0078
         TabIndex        =   66
         Top             =   300
         Width           =   2055
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkArea 
         Height          =   180
         Left            =   3285
         OleObjectBlob   =   "compra_repuestos.frx":00F4
         TabIndex        =   71
         Top             =   300
         Width           =   2055
      End
      Begin MSComctlLib.ListView LvSinDetalle 
         Height          =   1740
         Left            =   90
         TabIndex        =   76
         Top             =   885
         Width           =   14475
         _ExtentX        =   25532
         _ExtentY        =   3069
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   0   'False
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   13
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "N100"
            Text            =   "id"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "T1000"
            Text            =   "Cuenta"
            Object.Width           =   4410
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "T1500"
            Text            =   "Area"
            Object.Width           =   4410
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Object.Tag             =   "T3000"
            Text            =   "Centro de Costo"
            Object.Width           =   4410
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Object.Tag             =   "T1000"
            Text            =   "Items de Gasto"
            Object.Width           =   4410
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   5
            Key             =   "otros"
            Object.Tag             =   "N100"
            Text            =   "Otros Impuestos"
            Object.Width           =   1852
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   6
            Key             =   "neto"
            Object.Tag             =   "N100"
            Text            =   "Neto"
            Object.Width           =   1852
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   7
            Key             =   "iva"
            Object.Tag             =   "N100"
            Text            =   "IVA"
            Object.Width           =   1852
         EndProperty
         BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   8
            Key             =   "retenido"
            Object.Tag             =   "N100"
            Text            =   "Retenido"
            Object.Width           =   1852
         EndProperty
         BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   9
            Object.Tag             =   "N109"
            Text            =   "PLA_ID"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   10
            Object.Tag             =   "N109"
            Text            =   "ARE_ID"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(12) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   11
            Object.Tag             =   "N109"
            Text            =   "CEN_ID"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(13) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   12
            Object.Tag             =   "N109"
            Text            =   "ITEM GASTO ID"
            Object.Width           =   0
         EndProperty
      End
   End
   Begin VB.Frame FrameMA 
      Caption         =   "Articulos"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3765
      Left            =   465
      TabIndex        =   39
      Top             =   3105
      Width           =   14115
      Begin ACTIVESKINLibCtl.SkinLabel SKUme 
         Height          =   165
         Left            =   9285
         OleObjectBlob   =   "compra_repuestos.frx":015A
         TabIndex        =   75
         Top             =   255
         Width           =   1260
      End
      Begin VB.TextBox TxtStockActual 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   7350
         Locked          =   -1  'True
         TabIndex        =   73
         TabStop         =   0   'False
         Top             =   120
         Visible         =   0   'False
         Width           =   1905
      End
      Begin VB.TextBox TxtPexento 
         Alignment       =   1  'Right Justify
         BackColor       =   &H80000009&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   8025
         TabIndex        =   49
         Tag             =   "N"
         Text            =   "0"
         Top             =   450
         Width           =   1095
      End
      Begin VB.CommandButton CMDeliminaMaterial 
         Caption         =   "X"
         Height          =   240
         Left            =   165
         TabIndex        =   54
         ToolTipText     =   "Eliminar Linea seleccionada"
         Top             =   3405
         Width           =   420
      End
      Begin VB.TextBox TxtPrecio 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   5970
         TabIndex        =   47
         Tag             =   "N"
         Text            =   "0"
         Top             =   450
         Width           =   1140
      End
      Begin VB.TextBox TxtDescrp 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2820
         Locked          =   -1  'True
         TabIndex        =   46
         Top             =   450
         Width           =   3180
      End
      Begin VB.TextBox TxtMarca 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1410
         Locked          =   -1  'True
         TabIndex        =   45
         TabStop         =   0   'False
         Top             =   450
         Width           =   1440
      End
      Begin VB.CommandButton CmdAgregarProducto 
         Caption         =   "Ok"
         Height          =   300
         Left            =   13620
         TabIndex        =   52
         Top             =   435
         Width           =   360
      End
      Begin VB.CommandButton CmdBuscaProducto 
         Caption         =   "B"
         Height          =   300
         Left            =   1140
         TabIndex        =   53
         ToolTipText     =   "Buscador de Articulo"
         Top             =   465
         Width           =   255
      End
      Begin VB.TextBox TxtCodigo 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   120
         TabIndex        =   44
         ToolTipText     =   "Para ingresar un Gasto digite 0 (cero)"
         Top             =   450
         Width           =   1020
      End
      Begin VB.TextBox TxtDescuento 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   7065
         TabIndex        =   48
         Tag             =   "N"
         Text            =   "0"
         Top             =   450
         Width           =   990
      End
      Begin VB.TextBox TxtCantidad 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   9105
         TabIndex        =   50
         Tag             =   "N2"
         ToolTipText     =   "Unidad medida"
         Top             =   450
         Width           =   975
      End
      Begin VB.TextBox TxtSubTotal 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   11145
         Locked          =   -1  'True
         TabIndex        =   43
         TabStop         =   0   'False
         Text            =   "0"
         Top             =   450
         Width           =   1125
      End
      Begin VB.TextBox txtPrecioFinal 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   10065
         Locked          =   -1  'True
         TabIndex        =   42
         TabStop         =   0   'False
         Text            =   "0"
         Top             =   450
         Width           =   1110
      End
      Begin VB.TextBox TxtTotalProductos 
         Alignment       =   1  'Right Justify
         Height          =   315
         Left            =   10470
         Locked          =   -1  'True
         TabIndex        =   41
         Tag             =   "N"
         Text            =   "0"
         Top             =   3390
         Width           =   1185
      End
      Begin VB.TextBox TxtPrecioDocumento 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000009&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   315
         Left            =   4950
         Locked          =   -1  'True
         TabIndex        =   40
         TabStop         =   0   'False
         Text            =   "Ingrese valor diferencia de precio"
         Top             =   165
         Visible         =   0   'False
         Width           =   3240
      End
      Begin MSComctlLib.ListView LvMateriales 
         Height          =   2550
         Left            =   165
         TabIndex        =   55
         Top             =   780
         Width           =   13830
         _ExtentX        =   24395
         _ExtentY        =   4498
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   0   'False
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   16
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "N100"
            Text            =   "Nro Orden"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "T1000"
            Text            =   "Codigo"
            Object.Width           =   2249
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "T1500"
            Text            =   "Marca"
            Object.Width           =   2487
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Object.Tag             =   "T3000"
            Text            =   "Descripcion"
            Object.Width           =   5556
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   4
            Object.Tag             =   "N102"
            Text            =   "Precio"
            Object.Width           =   1984
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   5
            Object.Tag             =   "N102"
            Text            =   "Dscto"
            Object.Width           =   1693
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   6
            Object.Tag             =   "N102"
            Text            =   "Exe. Otos Imp."
            Object.Width           =   1905
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   7
            Object.Tag             =   "N102"
            Text            =   "Un."
            Object.Width           =   1670
         EndProperty
         BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   8
            Object.Tag             =   "N102"
            Text            =   "Precio U."
            Object.Width           =   1931
         EndProperty
         BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   9
            Object.Tag             =   "N100"
            Text            =   "Total"
            Object.Width           =   1958
         EndProperty
         BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   10
            Object.Tag             =   "T100"
            Text            =   "Id Familia"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(12) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   11
            Object.Tag             =   "F1300"
            Text            =   "Fec. Entrada"
            Object.Width           =   2434
         EndProperty
         BeginProperty ColumnHeader(13) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   12
            Object.Tag             =   "N100"
            Text            =   "Cuenta"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(14) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   13
            Object.Tag             =   "N100"
            Text            =   "CentroCosto"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(15) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   14
            Object.Tag             =   "N109"
            Text            =   "gasid"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(16) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   15
            Object.Tag             =   "N109"
            Text            =   "Area"
            Object.Width           =   0
         EndProperty
      End
      Begin MSComCtl2.DTPicker DtEntrada 
         Height          =   330
         Left            =   12255
         TabIndex        =   51
         Top             =   450
         Width           =   1365
         _ExtentX        =   2408
         _ExtentY        =   582
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   58523649
         CurrentDate     =   40543
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel14 
         Height          =   180
         Index           =   4
         Left            =   8550
         OleObjectBlob   =   "compra_repuestos.frx":01B8
         TabIndex        =   56
         Top             =   3435
         Width           =   1860
      End
   End
   Begin VB.Timer Timer2 
      Interval        =   50
      Left            =   6390
      Top             =   6495
   End
   Begin VB.Frame FrmRef 
      Caption         =   "Documento Referencia"
      Height          =   660
      Left            =   465
      TabIndex        =   33
      Top             =   1410
      Visible         =   0   'False
      Width           =   14100
      Begin VB.ComboBox CboMotivoNC 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         ItemData        =   "compra_repuestos.frx":0234
         Left            =   7680
         List            =   "compra_repuestos.frx":0236
         Style           =   2  'Dropdown List
         TabIndex        =   12
         Top             =   210
         Width           =   5970
      End
      Begin VB.PictureBox PicRefMalo 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   6285
         Picture         =   "compra_repuestos.frx":0238
         ScaleHeight     =   240
         ScaleWidth      =   285
         TabIndex        =   37
         Top             =   270
         Visible         =   0   'False
         Width           =   285
      End
      Begin VB.PictureBox PicRefOk 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   270
         Left            =   6285
         Picture         =   "compra_repuestos.frx":056E
         ScaleHeight     =   270
         ScaleWidth      =   300
         TabIndex        =   36
         Top             =   240
         Visible         =   0   'False
         Width           =   300
      End
      Begin VB.CommandButton cmdBuscaReferencia 
         Caption         =   "B"
         Height          =   315
         Left            =   5955
         TabIndex        =   11
         ToolTipText     =   "Buscar documento referencia"
         Top             =   240
         Width           =   300
      End
      Begin VB.ComboBox CboDocRef 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         ItemData        =   "compra_repuestos.frx":079A
         Left            =   3225
         List            =   "compra_repuestos.frx":079C
         Style           =   2  'Dropdown List
         TabIndex        =   10
         Top             =   240
         Width           =   2730
      End
      Begin VB.TextBox TxtNroRef 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   450
         TabIndex        =   9
         Tag             =   "N"
         Text            =   "0"
         Top             =   270
         Width           =   1335
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   255
         Index           =   1
         Left            =   90
         OleObjectBlob   =   "compra_repuestos.frx":079E
         TabIndex        =   34
         Top             =   270
         Width           =   315
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   255
         Index           =   1
         Left            =   1905
         OleObjectBlob   =   "compra_repuestos.frx":0801
         TabIndex        =   35
         Top             =   270
         Width           =   1200
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkMotivo 
         Height          =   255
         Left            =   7050
         OleObjectBlob   =   "compra_repuestos.frx":087A
         TabIndex        =   38
         Top             =   240
         Width           =   825
      End
   End
   Begin VB.Frame FrameTotal 
      Caption         =   "Totales"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1005
      Left            =   5265
      TabIndex        =   22
      Top             =   6960
      Width           =   9330
      Begin VB.TextBox TextEspecifico 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         Height          =   285
         Left            =   6090
         TabIndex        =   83
         Text            =   "0"
         Top             =   510
         Width           =   1485
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel11 
         Height          =   240
         Left            =   6180
         OleObjectBlob   =   "compra_repuestos.frx":08E3
         TabIndex        =   82
         Top             =   315
         Width           =   1305
      End
      Begin VB.TextBox txtIvaRetenido 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         Enabled         =   0   'False
         Height          =   285
         Left            =   4620
         TabIndex        =   70
         Tag             =   "N"
         Text            =   "0"
         Top             =   510
         Width           =   1485
      End
      Begin VB.TextBox txtTotal 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   7560
         Locked          =   -1  'True
         TabIndex        =   27
         TabStop         =   0   'False
         Text            =   "0"
         Top             =   510
         Width           =   1485
      End
      Begin VB.TextBox TxtIva 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         Height          =   285
         Left            =   3150
         Locked          =   -1  'True
         TabIndex        =   69
         Text            =   "0"
         Top             =   510
         Width           =   1485
      End
      Begin VB.TextBox txtTotalNeto 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         Height          =   285
         Left            =   1635
         Locked          =   -1  'True
         TabIndex        =   68
         Text            =   "0"
         Top             =   510
         Width           =   1530
      End
      Begin VB.TextBox TxtExento 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         Height          =   285
         Left            =   165
         Locked          =   -1  'True
         TabIndex        =   67
         Text            =   "0"
         Top             =   510
         Width           =   1485
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel5 
         Height          =   195
         Left            =   -210
         OleObjectBlob   =   "compra_repuestos.frx":0961
         TabIndex        =   23
         Top             =   315
         Width           =   1845
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel6 
         Height          =   195
         Left            =   1650
         OleObjectBlob   =   "compra_repuestos.frx":09E7
         TabIndex        =   24
         Top             =   315
         Width           =   1455
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel7 
         Height          =   195
         Left            =   3300
         OleObjectBlob   =   "compra_repuestos.frx":0A4D
         TabIndex        =   25
         Top             =   315
         Width           =   1275
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel8 
         Height          =   195
         Left            =   7890
         OleObjectBlob   =   "compra_repuestos.frx":0AB1
         TabIndex        =   26
         Top             =   315
         Width           =   1140
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel13 
         Height          =   195
         Left            =   4800
         OleObjectBlob   =   "compra_repuestos.frx":0B19
         TabIndex        =   29
         Top             =   315
         Width           =   1275
      End
   End
   Begin VB.Frame FrmPeriodo 
      Caption         =   "Periodo Contable"
      Height          =   1155
      Left            =   12285
      TabIndex        =   31
      Top             =   255
      Width           =   2250
      Begin VB.TextBox TxtAnoContable 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1365
         Locked          =   -1  'True
         TabIndex        =   60
         TabStop         =   0   'False
         Text            =   "0"
         Top             =   480
         Width           =   780
      End
      Begin VB.TextBox TxtMesContable 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   75
         Locked          =   -1  'True
         TabIndex        =   59
         TabStop         =   0   'False
         Text            =   "0"
         Top             =   480
         Width           =   1275
      End
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Timer On"
      Height          =   300
      Left            =   15
      TabIndex        =   30
      Top             =   9300
      Width           =   1365
   End
   Begin VB.Timer Timer1 
      Interval        =   2
      Left            =   3225
      Top             =   6405
   End
   Begin VB.Frame FramAc 
      Height          =   900
      Left            =   8775
      TabIndex        =   21
      Top             =   7905
      Width           =   5805
      Begin VB.ComboBox CboFpago 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         ItemData        =   "compra_repuestos.frx":0B8F
         Left            =   120
         List            =   "compra_repuestos.frx":0B99
         Style           =   2  'Dropdown List
         TabIndex        =   84
         Top             =   405
         Width           =   2865
      End
      Begin VB.CommandButton CmdGuardar 
         Caption         =   "&Guardar"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   510
         Left            =   3000
         TabIndex        =   72
         Top             =   270
         Width           =   1215
      End
      Begin VB.CommandButton CmdSalir 
         Caption         =   "&Retornar"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   510
         Left            =   4320
         TabIndex        =   13
         Top             =   255
         Width           =   1215
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkFpago 
         Height          =   225
         Left            =   120
         OleObjectBlob   =   "compra_repuestos.frx":0BAF
         TabIndex        =   85
         Top             =   210
         Width           =   1575
      End
   End
   Begin VB.Frame FrmProveedor 
      Caption         =   "Datos del documento"
      Height          =   1140
      Left            =   480
      TabIndex        =   14
      Top             =   255
      Width           =   11805
      Begin VB.ComboBox CboPlazos 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         ItemData        =   "compra_repuestos.frx":0C20
         Left            =   1410
         List            =   "compra_repuestos.frx":0C22
         Style           =   2  'Dropdown List
         TabIndex        =   58
         Top             =   480
         Width           =   1260
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel10 
         Height          =   225
         Left            =   8235
         OleObjectBlob   =   "compra_repuestos.frx":0C24
         TabIndex        =   28
         Top             =   255
         Width           =   3180
      End
      Begin VB.ComboBox CboTipoDoc 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         ItemData        =   "compra_repuestos.frx":0C93
         Left            =   2685
         List            =   "compra_repuestos.frx":0CA0
         Style           =   2  'Dropdown List
         TabIndex        =   1
         ToolTipText     =   $"compra_repuestos.frx":0CC5
         Top             =   480
         Width           =   2865
      End
      Begin VB.TextBox TxtNDoc 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   5535
         TabIndex        =   2
         Tag             =   "N"
         Text            =   "0"
         Top             =   480
         Width           =   1200
      End
      Begin VB.CommandButton CmdNuevoProveedor 
         Caption         =   "Crear"
         Height          =   255
         Left            =   7395
         TabIndex        =   16
         Top             =   795
         Width           =   675
      End
      Begin VB.TextBox TxtRsocial 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   8220
         Locked          =   -1  'True
         TabIndex        =   8
         TabStop         =   0   'False
         Top             =   480
         Width           =   3405
      End
      Begin VB.CommandButton CmdBuscaProv 
         Caption         =   "Buscar"
         Height          =   255
         Left            =   6735
         TabIndex        =   15
         Top             =   795
         Width           =   660
      End
      Begin VB.TextBox TxtRutProveedor 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   6735
         TabIndex        =   3
         Tag             =   "T"
         Top             =   480
         Width           =   1485
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   225
         Index           =   0
         Left            =   5385
         OleObjectBlob   =   "compra_repuestos.frx":0D5A
         TabIndex        =   17
         Top             =   255
         Width           =   1290
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   225
         Index           =   0
         Left            =   2670
         OleObjectBlob   =   "compra_repuestos.frx":0DB7
         TabIndex        =   18
         Top             =   285
         Width           =   1575
      End
      Begin MSComCtl2.DTPicker DtFecha 
         Height          =   345
         Left            =   135
         TabIndex        =   0
         Top             =   480
         Width           =   1290
         _ExtentX        =   2275
         _ExtentY        =   609
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         CustomFormat    =   "dd-mm-yyyy hh:mm:ss"
         Format          =   58523649
         CurrentDate     =   39855
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   225
         Left            =   120
         OleObjectBlob   =   "compra_repuestos.frx":0E30
         TabIndex        =   19
         Top             =   255
         Width           =   1065
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
         Height          =   225
         Left            =   6735
         OleObjectBlob   =   "compra_repuestos.frx":0EA1
         TabIndex        =   20
         Top             =   255
         Width           =   1470
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel9 
         Height          =   195
         Left            =   1425
         OleObjectBlob   =   "compra_repuestos.frx":0F16
         TabIndex        =   57
         Top             =   285
         Width           =   615
      End
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   4200
      OleObjectBlob   =   "compra_repuestos.frx":0F77
      Top             =   6285
   End
   Begin MSComctlLib.ListView LvMaterialesGrabado 
      Height          =   2790
      Left            =   1080
      TabIndex        =   32
      Top             =   9660
      Width           =   12975
      _ExtentX        =   22886
      _ExtentY        =   4921
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   0   'False
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   0
      NumItems        =   17
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Object.Tag             =   "N100"
         Text            =   "Nro Orden"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Object.Tag             =   "T1000"
         Text            =   "Codigo"
         Object.Width           =   2399
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Object.Tag             =   "T1500"
         Text            =   "Marca"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Object.Tag             =   "T3000"
         Text            =   "Descripcion"
         Object.Width           =   5292
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   4
         Object.Tag             =   "N102"
         Text            =   "Precio"
         Object.Width           =   1676
      EndProperty
      BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   5
         Object.Tag             =   "N102"
         Text            =   "Dscto"
         Object.Width           =   1676
      EndProperty
      BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   6
         Object.Tag             =   "N102"
         Text            =   "Exe. Otos Imp."
         Object.Width           =   1676
      EndProperty
      BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   7
         Object.Tag             =   "N102"
         Text            =   "Un."
         Object.Width           =   1235
      EndProperty
      BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   8
         Object.Tag             =   "N102"
         Text            =   "Precio U."
         Object.Width           =   1676
      EndProperty
      BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   9
         Object.Tag             =   "N100"
         Text            =   "Total"
         Object.Width           =   1940
      EndProperty
      BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   10
         Object.Tag             =   "T100"
         Text            =   "Id Familia"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(12) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   11
         Object.Tag             =   "F1300"
         Text            =   "Fec. Entrada"
         Object.Width           =   2187
      EndProperty
      BeginProperty ColumnHeader(13) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   12
         Object.Tag             =   "N100"
         Text            =   "IdCentroCosto"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(14) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   13
         Object.Tag             =   "N100"
         Text            =   "IdCuenta"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(15) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   14
         Object.Tag             =   "N100"
         Text            =   "IdSubCuenta"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(16) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   15
         Object.Tag             =   "N109"
         Text            =   "area"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(17) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   16
         Object.Tag             =   "N109"
         Text            =   "Dif Nota Credito"
         Object.Width           =   2540
      EndProperty
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkEmpresaActiva 
      Height          =   285
      Left            =   6780
      OleObjectBlob   =   "compra_repuestos.frx":11AB
      TabIndex        =   74
      Top             =   -15
      Width           =   6690
   End
End
Attribute VB_Name = "compra_repuestos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim bp_PermiteOk As Boolean
Dim bp_Documento_disponible As Boolean
Dim lm_TotalExento As Long
Dim lm_IVA As Long
Dim lm_TotalNeto As Long
Dim bp_Ingresa_Gasto As Boolean
Dim sm_Movimiento_Doc As String
Dim bp_Nueva_Compra As Boolean
Dim s_ActualizaPcompra As String * 2
Dim ip_ID_Doc_Grabado As Integer
Dim sp_Nombre_Doc_Grabado As String
Dim sp_Movimiento_Doc_Grabado As String
Dim dp_NroDoc_Grabado As Double
Dim s_Requiere_Refencia As String * 2
Dim s_Anula_Documento As String * 2
Dim s_Modifica_Inventario As String * 2
Dim bm_Solo_Corrige_Montos As Boolean
Dim b_NotaDebito As Boolean
Dim b_NotaCredito As Boolean
Dim s_Inventario As String
Dim b_Agrega As Boolean
Dim bm_Documento_Permite_Pago As Boolean
Public lp_IdCompra As Double
Dim Bm_Editable As Boolean

Private Sub CalculaTotal()
    TxtTotal = NumFormat(CDbl(TxtExento) + CDbl(txtTotalNeto) + CDbl(TxtIVA) + CDbl(Me.TextEspecifico) - CDbl(txtIvaRetenido))
End Sub



Private Sub CboItemGasto_LostFocus()
    On Error GoTo ElError
    If CboItemGasto.ListIndex > -1 Then
        If SP_Control_Inventario = "NO" Then
            TxtSdExento.SetFocus
        Else
            TxtCodigo.SetFocus
        End If
    End If
ElError:
End Sub

'NOTAS DE DEBITO Los vendedores afectos al tributo al valor agregado
'deber�n emitir notas de d�bito por aumentos del impuesto facturado.





Private Sub CboMotivoNC_Click()
    If CboMotivoNC.ListIndex = -1 Then Exit Sub
    
    Me.CmdAgregarProducto.Enabled = True
    Me.CMDeliminaMaterial.Enabled = True
    If bp_Nueva_Compra Then CMDeliminaMaterial.Enabled = False
    Me.CmdBuscaProducto.Enabled = True
    Sql = "SELECT tnc_modifica_inventario modifica,tnc_anula_documento anula " & _
          "FROM ntc_tipos " & _
          "WHERE tnc_id=" & CboMotivoNC.ItemData(CboMotivoNC.ListIndex)
    Consulta RsTmp, Sql
    b_Agrega = False
    If RsTmp.RecordCount > 0 Then
        s_Anula_Documento = RsTmp!anula
        s_Modifica_Inventario = RsTmp!modifica
        CmdAgregarProducto.Enabled = True
        CmdBuscaProducto.Enabled = True
        TxtCodigo.Enabled = True
        Me.CMDeliminaMaterial.Enabled = True
        If s_Modifica_Inventario = "NO" Then
            bm_Solo_Corrige_Montos = True
            CmdBuscaProducto.Enabled = False
            TxtCodigo.Enabled = False
            Me.CMDeliminaMaterial.Enabled = False
        Else
            bm_Solo_Corrige_Montos = False
            Me.TxtCantidad.Enabled = True
            TxtPrecio.Enabled = False
            TxtCodigo.Enabled = False
            Me.CMDeliminaMaterial.Enabled = True
        End If
        If RsTmp!anula = "SI" Then
            FrameMA.Enabled = False
            Me.CmdAgregarProducto.Enabled = False
            Me.CMDeliminaMaterial.Enabled = False
            Me.CmdBuscaProducto.Enabled = False
            CargaDatos
        Else
            CargaDatos
            FrameMA.Enabled = True
        End If
    
    End If
End Sub

Private Sub CboTipoDoc_Click()
    b_NotaCredito = False
    b_NotaDebito = False
    b_Agrega = True
    TxtCantidad.Enabled = True
    ComprobarSiDocumento DG_ID_Unico
    CMDeliminaMaterial.Enabled = True
    TxtCodigo.Enabled = True
    CmdBuscaProducto.Enabled = True
    bm_Solo_Corrige_Montos = False
    If CboTipoDoc.ListIndex > -1 Then
        Sql = "SELECT doc_retiene_iva,doc_permite_pago " & _
              "FROM sis_documentos " & _
              "WHERE doc_id=" & CboTipoDoc.ItemData(CboTipoDoc.ListIndex)
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then 'Consultamos si el documento corresponde
            bm_Documento_Permite_Pago = IIf(RsTmp!doc_permite_pago = "SI", True, False)
            
            If RsTmp!doc_retiene_iva = "SI" Then ' IVA retenido
                If SP_Control_Inventario = "SI" Then
                     txtIvaRetenido.Locked = False
                     txtIvaRetenido.Enabled = True
                     txtIvaRetenido = TxtIVA
                    TxtTotal = Format(CDbl(TxtExento) + CDbl(txtTotalNeto) + CDbl(TxtIVA) - CDbl(txtIvaRetenido), "#,##0")
                Else
                     txtIvaRetenido.Locked = True
                     txtIvaRetenido.Enabled = False
                     txtIvaRetenido = 0
                End If
            Else
                 txtIvaRetenido.Locked = True
                 txtIvaRetenido.Enabled = False
                 txtIvaRetenido = 0
            End If
        End If
        If CboTipoDoc.ItemData(CboTipoDoc.ListIndex) = IG_id_Nota_Debito Or _
           CboTipoDoc.ItemData(CboTipoDoc.ListIndex) = IG_id_Nota_Debito_Electronica Then
            CMDeliminaMaterial.Enabled = False
            TxtCodigo.Enabled = False
            bm_Solo_Corrige_Montos = True
            CmdBuscaProducto.Enabled = False
            b_NotaDebito = True
        End If
        If CboTipoDoc.ItemData(CboTipoDoc.ListIndex) = IG_id_Nota_Credito Or _
            CboTipoDoc.ItemData(CboTipoDoc.ListIndex) = IG_id_Nota_Credito_Electronica Then
            'CMDeliminaMaterial.Enabled = False
            'TxtCodigo.Enabled = False
            'bm_Solo_Corrige_Montos = True
            'CmdBuscaProducto.Enabled = False
            b_NotaCredito = True
        End If
        
       
         
    End If
    If s_Requiere_Refencia = "SI" Then
        
        Me.FrmProveedor.Height = 1680
        FrmPeriodo.Height = 1680
        'FrameMA.Top = 1965
        FrmRef.Visible = True
        If SP_Control_Inventario = "NO" Then
            'SkMotivo.Visible = False
            'CboMotivoNC.Visible = False
            
            
            
        Else
            SkMotivo.Visible = True
            CboMotivoNC.Visible = True
        End If
        
    Else
        'FrameMA.Top = 1500
        FrmPeriodo.Height = 1110
        FrmProveedor.Height = 1110
        FrmRef.Visible = False
    End If
    
    If b_NotaCredito Then
        SkFpago.Visible = False
        CboFpago.Visible = False
    End If
End Sub
Private Sub LimpiaCajas()
    TxtCodigo = ""
    TxtMarca = ""
    TxtDescrp = ""
    TxtPrecio = 0
    txtDescuento = 0
    TxtPexento = 0
    TxtCantidad = 0
    txtPrecioFinal = 0
    TxtSubTotal = 0
    TxtCodigo.Tag = 0
    SKUme = Empty
    If TxtCodigo.Enabled Then TxtCodigo.SetFocus
End Sub
Private Sub CmdAgregarProducto_Click()
    Dim i_indice As Integer
    If Not bm_Solo_Corrige_Montos Then
        If CboCentroCosto.ListIndex = -1 Or CboCuenta.ListIndex = -1 Or Me.CboItemGasto.ListIndex = -1 Or CboArea.ListIndex = -1 Then
            MsgBox "Seleccione Datos de Cuenta,Area, Centro Costo e Item de gasto ... ", vbInformation
            CboCentroCosto.SetFocus
            Exit Sub
        End If
    End If
    If Len(TxtCodigo) = 0 Then Exit Sub
    
    If Val(TxtCantidad) = 0 Then
        MsgBox "Ingrese cantidad...", vbInformation
        If TxtCantidad.Enabled Then TxtCantidad.SetFocus
        Exit Sub
    End If
    If Val(TxtSubTotal) = 0 Then Exit Sub
    If Not bm_Solo_Corrige_Montos And Val(TxtSubTotal) > 0 Then
        If Not bp_PermiteOk Or CDbl(TxtSubTotal) = 0 Then
            MsgBox "Verifique los datos por favor...", vbOKOnly + vbInformation
            If TxtCodigo.Enabled Then TxtCodigo.SetFocus
            Exit Sub
        End If
    End If
    If Not bm_Solo_Corrige_Montos Then
        If bp_Ingresa_Gasto And Len(TxtDescrp) = 0 Then
            MsgBox "Ingrese descripcion...", vbInformation
            TxtDescrp.SetFocus
            Exit Sub
        End If
    End If
    
    If CboCuenta.ListIndex = -1 Or CboArea.ListIndex = -1 Or CboCentroCosto.ListIndex = -1 Or CboItemGasto.ListIndex = -1 Then
        MsgBox "Seleccione datos contables...", vbInformation
        CboCuenta.SetFocus
        Exit Sub
    End If
    
    
    
    If LvMateriales.ListItems.Count > 0 Then
        For i = 1 To LvMateriales.ListItems.Count
            If TxtCodigo = LvMateriales.ListItems(i).SubItems(1) And TxtCodigo <> "0" Then
                i_indice = LvMateriales.ListItems(i).Index
                If Not bm_Solo_Corrige_Montos Then
                    If Not b_NotaCredito And b_NotaDebito Then
                        MsgBox "Codigo ya se encuentra en la lista...", vbOKOnly + vbInformation
                        TxtCodigo.SetFocus
                        Exit Sub
                    End If
                    
                Else
                    For X = 1 To Me.LvMaterialesGrabado.ListItems.Count
                        If LvMaterialesGrabado.ListItems(X).SubItems(1) = TxtCodigo Then
                            If b_NotaDebito Or b_NotaCredito Then
                                If CDbl(LvMaterialesGrabado.ListItems(X).SubItems(9)) <= 0 Then
                                'If CDbl(LvMaterialesGrabado.ListItems(x).SubItems(9)) >= CDbl(TxtSubTotal) Then
                                    TxtPrecio.SetFocus
                                    MsgBox "VALOR DEBE SER MAYOR A 0 (cero)..."
                                    TxtPrecioDocumento.Visible = True
                                    Exit Sub
                                Else
                                    'LvMaterialesGrabado.ListItems(x).SubItems(15) = CDbl(TxtSubTotal) - CDbl(LvMaterialesGrabado.ListItems(x).SubItems(9))
                                End If
                            
                                
                            End If
                            If b_NotaCredito Then
                                If CDbl(LvMaterialesGrabado.ListItems(X).SubItems(9)) < CDbl(TxtSubTotal) Then
                                    
                                    MsgBox "PRECIO DEBE SER MENOR O IGUAL AL ACTUAL...", vbInformation
                                    TxtPrecio.SetFocus
                                    Exit Sub
                                Else
                                   LvMaterialesGrabado.ListItems(X).SubItems(16) = CDbl(LvMaterialesGrabado.ListItems(X).SubItems(9)) - CDbl(TxtSubTotal)
                                End If
                            End If
                            
                            
                            
                            
                        End If
                    Next X
                End If
            End If
        Next
    End If
    
   ' If s_Modifica_Inventario = "SI" Or (b_NotaCredito And Not bm_Solo_Corrige_Montos) Then
   If b_Agrega Then
        LvMateriales.ListItems.Add
        LvMateriales.ListItems(LvMateriales.ListItems.Count).SubItems(1) = TxtCodigo
        LvMateriales.ListItems(LvMateriales.ListItems.Count).SubItems(2) = TxtMarca
        LvMateriales.ListItems(LvMateriales.ListItems.Count).SubItems(3) = TxtDescrp
        LvMateriales.ListItems(LvMateriales.ListItems.Count).SubItems(4) = Format(TxtPrecio, "#,##0.#0")
        LvMateriales.ListItems(LvMateriales.ListItems.Count).SubItems(5) = Format(txtDescuento, "#,##0.#0")
        LvMateriales.ListItems(LvMateriales.ListItems.Count).SubItems(6) = Format(TxtPexento, "#,##0.#0")
        LvMateriales.ListItems(LvMateriales.ListItems.Count).SubItems(7) = NumFormat(CDbl(TxtCantidad))
        LvMateriales.ListItems(LvMateriales.ListItems.Count).SubItems(8) = Format(txtPrecioFinal, "#,##0.#0")
        LvMateriales.ListItems(LvMateriales.ListItems.Count).SubItems(9) = TxtSubTotal
        LvMateriales.ListItems(LvMateriales.ListItems.Count).SubItems(10) = TxtCodigo.Tag
        LvMateriales.ListItems(LvMateriales.ListItems.Count).SubItems(11) = Me.DtEntrada.Value
        LvMateriales.ListItems(LvMateriales.ListItems.Count).SubItems(12) = CboCuenta.ItemData(CboCuenta.ListIndex)
        LvMateriales.ListItems(LvMateriales.ListItems.Count).SubItems(13) = CboCentroCosto.ItemData(CboCentroCosto.ListIndex)
        LvMateriales.ListItems(LvMateriales.ListItems.Count).SubItems(14) = Me.CboItemGasto.ItemData(CboItemGasto.ListIndex)
        LvMateriales.ListItems(LvMateriales.ListItems.Count).SubItems(15) = CboArea.ItemData(CboArea.ListIndex)
    Else
        With LvMateriales.ListItems(i_indice)
            .SubItems(1) = TxtCodigo
            .SubItems(2) = TxtMarca
            .SubItems(3) = TxtDescrp
            .SubItems(4) = Format(TxtPrecio, "#,##0.#0")
            .SubItems(5) = Format(txtDescuento, "#,##0.#0")
            .SubItems(6) = Format(TxtPexento, "#,##0.#0")
            .SubItems(7) = Format(TxtCantidad, "#,##0.#0")
            .SubItems(8) = Format(txtPrecioFinal, "#,##0.#0")
            .SubItems(9) = TxtSubTotal
            .SubItems(10) = TxtCodigo.Tag
            .SubItems(11) = Me.DtEntrada.Value
            .SubItems(12) = CboCuenta.ItemData(CboCuenta.ListIndex)
            .SubItems(13) = CboCentroCosto.ItemData(CboCentroCosto.ListIndex)
            .SubItems(14) = CboItemGasto.ItemData(CboItemGasto.ListIndex)
            .SubItems(15) = CboArea.ItemData(CboArea.ListIndex)
        End With
    End If
    CalculaTotales
    LimpiaCajas
End Sub
Private Sub CalculaTotales()
    Dim Exento As Long
    Dim Neto As Long
    lm_TotalExento = 0
    TxtExento = 0
    txtTotalNeto = 0
    TxtTotal = 0
    txtIvaRetenido = 0
    TxtIVA = 0
    txtTotalGrillaGastos = 0
    txtExentoGastos = 0
  '  If LvMateriales.ListItems.Count = 0 Then Exit Sub
    
    lm_TotalNeto = 0
    
    TxtTotalProductos = 0
    For i = 1 To LvMateriales.ListItems.Count
        TxtTotalProductos = TxtTotalProductos + CDbl(LvMateriales.ListItems(i).SubItems(9))
        Exento = CDbl(LvMateriales.ListItems(i).SubItems(6)) * CDbl(LvMateriales.ListItems(i).SubItems(7))
        Neto = CDbl(LvMateriales.ListItems(i).SubItems(9))
        lm_TotalExento = lm_TotalExento + Exento
        
        lm_TotalNeto = lm_TotalNeto + Neto '- lm_TotalExento
    Next
    TxtTotalProductos = Format(TxtTotalProductos, "#,##0")
    
    
    lm_TotalNeto = lm_TotalNeto - lm_TotalExento
    TxtExento = Format(lm_TotalExento, "#,##0")
    
    txtTotalNeto = Format(lm_TotalNeto, "#,##0")
    TxtIVA = Format(lm_TotalNeto / 100 * DG_IVA, "#,##0")
    If CboTipoDoc.ListIndex > -1 Then
        If CboTipoDoc.ItemData(CboTipoDoc.ListIndex) = IG_documentoIvaRetenido Then
            txtIvaRetenido = TxtIVA
        Else
            txtIvaRetenido = 0
        End If
    End If
    TxtTotal = Format(CDbl(TxtExento) + CDbl(txtTotalNeto) + CDbl(TxtIVA), "#,##0")
    TxtTotal = Format(CDbl(TxtTotal) - CDbl(txtIvaRetenido), "#,##0")
    
    If SP_Control_Inventario = "NO" Then
        Sql = "SELECT c.com_exe_otros_imp exento,c.neto,c.iva,c.iva_retenido,c.total " & _
              "FROM com_doc_compra AS c " & _
              "WHERE c.id =" & DG_ID_Unico
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
            TxtExento = NumFormat(RsTmp!Exento)
            txtTotalNeto = NumFormat(RsTmp!Neto)
            TxtIVA = NumFormat(RsTmp!IVA)
            txtIvaRetenido = NumFormat(RsTmp!iva_retenido)
            CalculaTotal
            'FrameTotal.Enabled = False
        End If
    End If
    CalculaTotal
    'If b_NotaDebito Or b_NotaCredito Then CalculaTotalNCND
        
End Sub
Private Sub CalculaTotalNCND()
    Dim Exento As Long
    Dim Neto As Long
    lm_TotalExento = 0
    TxtExento = 0
    txtTotalNeto = 0
    TxtTotal = 0
    txtIvaRetenido = 0
    TxtIVA = 0
    txtTotalGrillaGastos = 0
    txtExentoGastos = 0
  '  If LvMateriales.ListItems.Count = 0 Then Exit Sub
    
    lm_TotalNeto = 0
    With Me.LvMaterialesGrabado
        TxtTotalProductos = 0
        For i = 1 To .ListItems.Count
            TxtTotalProductos = TxtTotalProductos + CDbl(Val(.ListItems(i).SubItems(16)))
             'Exento = CDbl(.ListItems(I).SubItems(6)) * CDbl(.ListItems(I).SubItems(7))
            'Neto = CDbl(.ListItems(I).SubItems(9))
            'lm_TotalExento = lm_TotalExento + Exento
           ' lm_TotalNeto = lm_TotalNeto + Neto '- lm_TotalExento
        Next
        TxtTotalProductos = Format(TxtTotalProductos, "#,##0")
        lm_TotalNeto = lm_TotalNeto - lm_TotalExento
        TxtExento = Format(lm_TotalExento, "#,##0")
        txtTotalNeto = Format(lm_TotalNeto, "#,##0")
        TxtIVA = Format(lm_TotalNeto / 100 * DG_IVA, "#,##0")
        'If CboTipoDoc.ListIndex > -1 Then
        ''    If CboTipoDoc.ItemData(CboTipoDoc.ListIndex) = IG_documentoIvaRetenido Then
         '       txtIvaRetenido = TxtIva
         '   Else
         '       txtIvaRetenido = 0
         '   End If
        'End If
        TxtTotal = Format(CDbl(TxtExento) + CDbl(txtTotalNeto) + CDbl(TxtIVA), "#,##0")
        TxtTotal = Format(CDbl(TxtTotal) - CDbl(txtIvaRetenido), "#,##0")
    End With
    
End Sub



Private Sub CmdBuscaProducto_Click()
    BuscaProducto.Show 1
    If Len(SG_codigo) = 0 Then Exit Sub
    TxtCodigo = SG_codigo
    TxtCodigo_Validate True
    
End Sub

Private Sub CmdBuscaProv_Click()
    AccionProveedor = 0
    BuscaProveedor.Show 1
    TxtRutProveedor = RutBuscado
    If TxtCodigo.Enabled Then If Len(TxtRutProveedor) > 0 Then TxtCodigo.SetFocus
    TxtRutProveedor_Validate True
End Sub

Private Sub cmdBuscaReferencia_Click()
    PicRefOk.Visible = False
    DG_ID_Unico = 0
    If Len(TxtRutProveedor) > 0 And Val(TxtNroRef) > 0 And CboDocRef.ListIndex > -1 Then
        Sql = "SELECT id,doc_factura_guias,pago " & _
              "FROM com_doc_compra " & _
              "WHERE  rut_emp='" & SP_Rut_Activo & "' AND  doc_id=" & CboDocRef.ItemData(CboDocRef.ListIndex) & " AND rut='" & TxtRutProveedor & "' AND no_documento=" & TxtNroRef
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
            DG_ID_Unico = RsTmp!ID
            PicRefOk.Visible = True
            Me.PicRefMalo.Visible = False
            If RsTmp!doc_factura_guias = "SI" Then
                MsgBox "Este es un documento por guias, no se puede refenciar para Nota de Credito o Debito" & _
                vbNewLine & "Ingresar como mayor Cargo o mayor Abono, ingresando 0 en c�digo de art�culo"
                DG_ID_Unico = 0
                CboDocRef.ListIndex = -1
                Me.FrmRef.Visible = False
                TxtNroRef = 0
                Me.FrameMA.Enabled = True
                TxtCodigo.Locked = False
                TxtCodigo.Enabled = True
                CmdBuscaProducto.Enabled = True
            End If
        Else
            PicRefOk.Visible = False
            Me.PicRefMalo.Visible = True
        End If
    End If
End Sub

Private Sub cmdCuenta_Click()
    With BuscarSimple
        SG_codigo = Empty
        .Sm_Consulta = "SELECT pla_id, pla_nombre,tpo_nombre,det_nombre " & _
                       "FROM    con_plan_de_cuentas p,  con_tipo_de_cuenta t,   con_detalle_tipo_cuenta d " & _
                       "WHERE   p.tpo_id = t.tpo_id AND p.det_id = d.det_id "
        .Sm_CampoLike = "pla_nombre"
        .Im_Columnas = 2
        .Show 1
        If SG_codigo = Empty Then Exit Sub
        Busca_Id_Combo CboCuenta, Val(SG_codigo)
    End With
End Sub

Private Sub CMDeliminaMaterial_Click()
    If LvMateriales.SelectedItem Is Nothing Then Exit Sub
    
    If MsgBox("� Eliminar producto seleccionado ...?", vbQuestion + vbYesNo) = vbNo Then Exit Sub
    
    LvMateriales.ListItems.Remove LvMateriales.SelectedItem.Index
    CalculaTotales
    If Not b_NotaCredito Then TxtCodigo.SetFocus
End Sub

Private Sub CmdGuardar_Click()
    Dim lp_IdCompra As Double
    Dim Sql_Values As String
    Dim lp_Promedio As Double
    Dim Ip_CenId As Long, Ip_PlaId As Long, Ip_GasId As Long, Ip_AreId As Long
    Dim lp_IDAbo As Long
    If bp_Nueva_Compra Then ComprobarSiDocumento 0 Else ComprobarSiDocumento DG_ID_Unico
    If Not bp_Documento_disponible Then Exit Sub
    
    If Month(DtFecha.Value) > IG_Mes_Contable Or Year(Date) > IG_Ano_Contable Then
        MsgBox "No puede ingresar fecha mayor al periodo contable...", vbInformation
        Exit Sub
    End If
    If CboPlazos.ListIndex = -1 Then
        MsgBox "SELECCIONE EL PLAZO DEL DOCUMENTO...", vbOKOnly + vbInformation
        CboPlazos.SetFocus
        Exit Sub
    End If
    
    If Month(DtFecha.Value) < IG_Mes_Contable Then
        If MsgBox("Factura mes anterior, " & vbNewLine & "�Continuar?", vbYesNo) = vbNo Then
            DtFecha.SetFocus
            Exit Sub
        End If
    End If
    If CboFpago.ListIndex < 0 And Not b_NotaCredito Then
        MsgBox "SELECCIONE FORMA DE PAGO...", vbOKOnly + vbInformation
        CboFpago.SetFocus
        Exit Sub
    End If
    
    
    If SP_Control_Inventario = "SI" Then
        If LvMateriales.ListItems.Count = 0 Then
            MsgBox "A�n no ha ingresado Item's...", vbOKOnly + vbInformation
            Exit Sub
        End If
    End If
    If LvMateriales.ListItems.Count > 0 Then
        If Me.CboCuenta.ListIndex = -1 Or Me.CboCentroCosto.ListIndex = -1 Then
            MsgBox "Complete los datos del plan de cuentas...", vbOKOnly + vbInformation
            Me.CboItemGasto.SetFocus
            Exit Sub
        End If
    End If
    
    'ESTAMOS LISTOS PARA PROCEDER A GRABAR EL DOCUMENTO DE COMPRA
    'Consulta RsTmp, "BEGIN"
    lp_IdCompra = 1
    Sql = "SELECT max(id) elid " & _
          "FROM com_doc_compra "
    Consulta RsTmp, Sql
    If RsTmp.RecordCount Then lp_IdCompra = (0 & RsTmp!elid) + 1
    If Not bp_Nueva_Compra Then
        
        'Guardando modificacion
        'Nuevo
        If 1 = 2 Then
                If SP_Control_Inventario = "NO" Then
                   
                Else
                    'Regenerar Kardex
                    
                        Sql = "SELECT c.fecha,c.doc_id,doc_nombre,c.no_documento,d.pro_codigo,m.descripcion,d.cantidad,d.total_linea/cantidad unitario " & _
                              "FROM com_detalle d " & _
                              "INNER JOIN com_doc_compra c ON d.id_compra=c.id " & _
                              "INNER JOIN maestro_productos m ON m.codigo=d.pro_codigo " & _
                              "INNER JOIN sis_documentos s ON c.doc_id=s.doc_id " & _
                              "WHERE c.id =" & DG_ID_Unico
                        Consulta RsTmp, Sql
                        If RsTmp.RecordCount > 0 Then
                            RsTmp.MoveFirst
                            Do While Not RsTmp.EOF
                                 Kardex Format(RsTmp!Fecha, "YYYY-MM-DD"), "SALIDA", RsTmp!doc_id, RsTmp!no_documento, 1, _
                                 RsTmp!pro_codigo, RsTmp!cantidad, "MODIFICACION " & RsTmp!doc_nombre & " Nro:" & RsTmp!no_documento, RsTmp!unitario, RsTmp!unitario, , , , , CboTipoDoc.ItemData(CboTipoDoc.ListIndex)
                                RsTmp.MoveNext
                            Loop
                        End If
                
                End If
                Sql = "DELETE FROM com_doc_compra " & _
                         "WHERE id=" & DG_ID_Unico
                Consulta RsTmp, Sql
                        Sql = "DELETE FROM com_detalle " & _
                      "WHERE id_compra=" & DG_ID_Unico
                Consulta RsTmp, Sql
                
                
            End If
        End If
        
    If Not bp_Nueva_Compra And s_Requiere_Refencia = "NO" Then
        'Eliminar repuestos y actualizar documento de compra
        'Incluir salidas de stock y kardex
        'Ahora continuamos con kardex
        
        'Aqui hacemos la devolucion de los productos
        'los sacaremos de grilla oculta para no estar
        'llamando a la bd
        For i = 1 To LvMaterialesGrabado.ListItems.Count
            If LvMaterialesGrabado.ListItems(i).SubItems(1) <> "0" Then
                Kardex Format(LvMaterialesGrabado.ListItems(i).SubItems(11), "YYYY-MM-DD"), "SALIDA", ip_ID_Doc_Grabado, dp_NroDoc_Grabado, 1, _
                LvMaterialesGrabado.ListItems(i).SubItems(1), LvMaterialesGrabado.ListItems(i).SubItems(7), "MODIFICACION " & sp_Nombre_Doc_Grabado & " Nro:" & dp_NroDoc_Grabado, LvMaterialesGrabado.ListItems(i).SubItems(8), 0, , , , , CboTipoDoc.ItemData(CboTipoDoc.ListIndex)
            End If
        Next
        
        'Ahora eliminamos el Documento anterior
        Sql = "DELETE FROM com_doc_compra " & _
              "WHERE id=" & DG_ID_Unico
        Consulta RsTmp, Sql
        'Ahora eliminamos los productos
        Sql = "DELETE FROM com_detalle " & _
              "WHERE id_compra=" & DG_ID_Unico
        Consulta RsTmp, Sql
        lp_IdCompra = DG_ID_Unico
    End If
    
    '�************************************************
    'Nota de Credito que Corrige montos o Nota Debito
    '2 Abril 20
'        If s_Requiere_Refencia = "SI" Then
'            If s_Anula_Documento = "NO" And s_Modifica_Inventario = "SI" Then
'             'Aqui hacemos la devolucion de los productos
'            'los sacaremos de grilla oculta para no estar
'             'llamando a la bd
'             Movimiento = sm_Movimiento_Doc
'             If CboTipoDoc.ItemData(CboTipoDoc.ListIndex) = IG_id_Nota_Credito Then sm_Movimiento_Doc = "ENTRADA"
'                For I = 1 To LvMaterialesGrabado.ListItems.Count
'                    If LvMaterialesGrabado.ListItems(I).SubItems(1) <> "0" Then
'                        Kardex Format(LvMaterialesGrabado.ListItems(I).SubItems(11), "YYYY-MM-DD"), sm_Movimiento_Doc, ip_ID_Doc_Grabado, dp_NroDoc_Grabado, 1, _
'                        LvMaterialesGrabado.ListItems(I).SubItems(1), LvMaterialesGrabado.ListItems(I).SubItems(7), _
'                        "NC CORRIGE MONTO " & sp_Nombre_Doc_Grabado & " Nro:" & dp_NroDoc_Grabado, _
'                        LvMaterialesGrabado.ListItems(I).SubItems(8), 0, , , , 0 ' LvMaterialesGrabado.ListItems(I).SubItems(8)
'                    End If
'                Next
'            sm_Movimiento_Doc = Movimiento
'            End If
 '       End If
    '�************************************************
            
    
    
    If b_NotaDebito Then s_Inventario = "NN"
    If b_NotaCredito And s_Modifica_Inventario = "NO" Then s_Inventario = "NN"
    If b_NotaCredito And s_Modifica_Inventario = "SI" Then s_Inventario = "RESTA"
    
    If SP_Control_Inventario = "NO" Then
        Ip_CenId = Me.CboCentroCosto.ItemData(Me.CboCentroCosto.ListIndex)
        Ip_PlaId = Me.CboCuenta.ItemData(CboCuenta.ListIndex)
        Ip_GasId = CboItemGasto.ItemData(CboItemGasto.ListIndex)
        Ip_AreId = CboArea.ItemData(CboArea.ListIndex)
    Else
        Ip_CenId = 0
        Ip_PlaId = 0
        Ip_GasId = 0
        Ip_AreId = 0
    End If
    Dim Sp_Fpago As String
    Sp_Fpago = Me.CboFpago.Text
    If b_NotaCredito Then
        Sql = "SELECT pago FROM com_doc_compra WHERE id =" & DG_ID_Unico
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then Sp_Fpago = RsTmp!pago
    End If
    'Insertamos doc de compra con id's 24 Agosto 2011
     Sql = "INSERT INTO com_doc_compra (id,rut,nombre_proveedor,documento,no_documento,pago,com_exe_otros_imp,neto,iva,especifico,total,fecha,doc_id,mes_contable,ano_contable,tipo_movimiento,id_ref,inventario,iva_retenido,doc_fecha_vencimiento,rut_emp,pla_id,cen_id,gas_id,are_id) " & _
          "VALUES (" & lp_IdCompra & ",'" & TxtRutProveedor & "','" & TxtRsocial & "','" & CboTipoDoc.Text & "'," & TxtNDoc & ",'" & Sp_Fpago & "'," & _
          CDbl(TxtExento) & "," & CDbl(txtTotalNeto) & "," & CDbl(TxtIVA) & "," & CDbl(TextEspecifico) & "," & CDbl(TxtTotal) & ",'" & Format(DtFecha.Value, "YYYY-MM-DD") & "'," & _
          CboTipoDoc.ItemData(CboTipoDoc.ListIndex) & "," & IG_Mes_Contable & "," & IG_Ano_Contable & ",'COMPRA PRODUCTO'," & DG_ID_Unico & ",'" & s_Inventario & "'," & CDbl(txtIvaRetenido) & ",'" & Format(DtFecha.Value + CboPlazos.ItemData(CboPlazos.ListIndex), "YYYY-MM-DD") & _
          "','" & SP_Rut_Activo & "'," & Ip_PlaId & "," & Ip_CenId & "," & Ip_GasId & "," & Ip_AreId & ")"
    Consulta RsTmp, Sql
  
    'Aqui grabamos el detalle de los productos 4-marzo-2011
    If SP_Control_Inventario = "SI" Then
        
    
        Sql = "INSERT INTO com_detalle(id_compra,mar_id,pro_codigo,descripcion,exento,descuento,neto,total,cantidad,total_linea,pla_id,cen_id,gas_id,fecha_entrada,are_id) VALUES "
        For i = 1 To LvMateriales.ListItems.Count
            Sql_Values = Sql_Values & "(" & lp_IdCompra & ",1,'" & LvMateriales.ListItems(i).SubItems(1) & "','" & LvMateriales.ListItems(i).SubItems(3) & "'," & _
            Replace(CDbl(LvMateriales.ListItems(i).SubItems(6)), ",", ".") & "," & Replace(CDbl(LvMateriales.ListItems(i).SubItems(5)), ",", ".") & "," & _
            Replace(CDbl(LvMateriales.ListItems(i).SubItems(4)), ",", ".") & "," & Replace(CDbl(LvMateriales.ListItems(i).SubItems(8)), ",", ".") & "," & _
            Replace(CDbl(LvMateriales.ListItems(i).SubItems(7)), ",", ".") & "," & Replace(CDbl(LvMateriales.ListItems(i).SubItems(9)), ",", ".") & "," & _
            LvMateriales.ListItems(i).SubItems(12) & "," & LvMateriales.ListItems(i).SubItems(13) & "," & _
            LvMateriales.ListItems(i).SubItems(14) & ",'" & Format(LvMateriales.ListItems(i).SubItems(11), "YYYY-MM-DD") & "'," & LvMateriales.ListItems(i).SubItems(15) & "),"
        Next
        Sql_Values = Mid(Sql_Values, 1, Len(Sql_Values) - 1)
    
        Consulta RsTmp, Sql & Sql_Values
    Else ' '  ACA GRABAMOS EL DETALLE SIN DETALLE 6 SEPTIEMBRE 2011
        If Not bp_Nueva_Compra Then
            Sql = "DELETE FROM com_sin_detalle WHERE id=" & lp_IdCompra
            Consulta RsTmp, Sql
        End If
    
        Sql = "INSERT INTO com_sin_detalle(id,csd_exento,csd_neto,csd_iva,csd_retencion,pla_id,are_id,cen_id,gas_id) VALUES "
        With LvSinDetalle
            For i = 1 To .ListItems.Count
                Sql_Values = Sql_Values & "(" & lp_IdCompra & "," & CDbl(.ListItems(i).SubItems(5)) & "," & _
                CDbl(.ListItems(i).SubItems(6)) & "," & _
                CDbl(.ListItems(i).SubItems(7)) & "," & _
                CDbl(.ListItems(i).SubItems(8)) & "," & _
                CDbl(.ListItems(i).SubItems(9)) & "," & _
                CDbl(.ListItems(i).SubItems(10)) & "," & _
                CDbl(.ListItems(i).SubItems(11)) & "," & _
                CDbl(.ListItems(i).SubItems(12)) & "),"
            Next
        End With
        Sql_Values = Mid(Sql_Values, 1, Len(Sql_Values) - 1)
        Consulta RsTmp, Sql & Sql_Values
    End If
    'Ahora continuamos con kardex
    For i = 1 To LvMateriales.ListItems.Count
        If LvMateriales.ListItems(i).SubItems(1) <> "0" Then
            If b_NotaDebito Then
                With LvMaterialesGrabado
                    KardexNN_NotaDebito Format(.ListItems(i).SubItems(11), "YYYY-MM-DD"), sm_Movimiento_Doc & "D", CboTipoDoc.ItemData(CboTipoDoc.ListIndex), TxtNDoc, 1, _
                    .ListItems(i).SubItems(1), "COM- " & CboTipoDoc.Text & " Nro:" & TxtNDoc, LvMateriales.ListItems(i).SubItems(9), LvMateriales.ListItems(i).SubItems(9) + .ListItems(i).SubItems(9), _
                    TxtRutProveedor, TxtRsocial, "+", CboTipoDoc.ItemData(CboTipoDoc.ListIndex)
                End With
            ElseIf b_NotaCredito And bm_Solo_Corrige_Montos Then
                With LvMaterialesGrabado
                    KardexNN_NotaDebito Format(.ListItems(i).SubItems(11), "YYYY-MM-DD"), "NNC", CboTipoDoc.ItemData(CboTipoDoc.ListIndex), TxtNDoc, 1, _
                    .ListItems(i).SubItems(1), "COM- " & CboTipoDoc.Text & " Nro:" & TxtNDoc, LvMateriales.ListItems(i).SubItems(9), CDbl(LvMateriales.ListItems(i).SubItems(9)) - CDbl(.ListItems(i).SubItems(9)), _
                    TxtRutProveedor, TxtRsocial, "-", CboTipoDoc.ItemData(CboTipoDoc.ListIndex)
                End With
            Else
                Sql = "SELECT pro_precio_neto promedio " & _
                              "FROM inv_kardex " & _
                              "WHERE rut_emp='" & SP_Rut_Activo & "' AND  pro_codigo='" & LvMateriales.ListItems(i).SubItems(1) & "' " & _
                              "ORDER BY kar_id DESC " & _
                              "LIMIT 1"
                        Consulta RsTmp, Sql
                        lp_Promedio = 0
                        If RsTmp.RecordCount > 0 Then
                            lp_Promedio = RsTmp!promedio
                        End If
                
            
                Kardex Format(LvMateriales.ListItems(i).SubItems(11), "YYYY-MM-DD"), sm_Movimiento_Doc, CboTipoDoc.ItemData(CboTipoDoc.ListIndex), TxtNDoc, 1, _
                LvMateriales.ListItems(i).SubItems(1), LvMateriales.ListItems(i).SubItems(7), "COMPRA " & CboTipoDoc.Text & " Nro:" & TxtNDoc, CDbl(LvMateriales.ListItems(i).SubItems(8)), _
                Replace(CDbl(LvMateriales.ListItems(i).SubItems(9)), ",", "."), TxtRutProveedor, TxtRsocial, s_ActualizaPcompra, CDbl(LvMateriales.ListItems(i).SubItems(8)), CboTipoDoc.ItemData(CboTipoDoc.ListIndex)
            End If
        End If
    Next
    
    If bm_Documento_Permite_Pago And CboFpago.Text = "CONTADO" Then
        'Aqui realizar abono a la cta cte
        '8 Octubre 2011
        'Comprobar si este documento ya tiene un pago
        'Sql = "DELETE FROM cta_abonos WHERE "
        
        
        lp_IDAbo = UltimoNro("cta_abonos", "abo_id")
        PagoDocumento lp_IDAbo, "PRO", Me.TxtRutProveedor, DtFecha, CDbl(TxtTotal), 1, "EFECTIVO", "PAGO CONTADO", LogUsuario
        Sql = "INSERT INTO cta_abono_documentos (abo_id,id,ctd_monto) VALUES "
        Sql = Sql & "(" & lp_IDAbo & "," & lp_IdCompra & "," & CDbl(CDbl(TxtTotal)) & ")"
        Consulta RsTmp, Sql
        
    End If
    
    
    MsgBox "DOCUMENTO GRABADO CON EXITO " & vbNewLine & CboTipoDoc.Text & " Nro " & TxtNDoc
    'Set Me = Nothing
    LimpiaTodo
End Sub
Public Sub LimpiaTodo()
    Me.PicRefMalo.Visible = False
    Me.PicRefOk.Visible = False
    DG_ID_Unico = 0
    TxtNroRef = 0
    TxtNDoc = 0
    TxtRutProveedor = ""
    TxtRsocial = ""
    LvMateriales.ListItems.Clear
    LvMaterialesGrabado.ListItems.Clear
    LvSinDetalle.ListItems.Clear
    Me.TxtCodigo = ""
    Me.TxtDescrp = ""
    Me.TxtCantidad = 0
    Me.TxtPexento = 0
    Me.txtPrecioFinal = 0
    Me.TxtSubTotal = 0
    Me.txtDescuento = 0
    Me.TextEspecifico = 0
    CalculaTotales
    
    ip_ID_Doc_Grabado = 0
    sp_Nombre_Doc_Grabado = ""
    dp_NroDoc_Grabado = 0
    sp_Movimiento_Doc_Grabado = ""
    
    bp_Nueva_Compra = True
    
    
'    DtFecha.SetFocus
    
End Sub

Private Sub CmdNuevoProveedor_Click()
    SG_codigo = Empty
    AgregoProveedor.Show 1
    If SG_codigo <> Empty Then TxtRutProveedor = SG_codigo
    
End Sub

Private Sub CmdOkSd_Click()
    If CboCuenta.ListIndex = -1 Or CboArea.ListIndex = -1 Or CboCentroCosto.ListIndex = -1 Or CboItemGasto.ListIndex = -1 Then
        MsgBox "Faltan datos contables...", vbInformation
        CboCuenta.SetFocus
        Exit Sub
    End If
    If CDbl(TxtSdExento) + CDbl(TxtSDNeto) + CDbl(TxtSdIvaRetenido) = 0 Then
        MsgBox "Faltan valores...", vbInformation
        TxtSDNeto.SetFocus
        Exit Sub
    End If
    
    If s_Anula_Documento = "SI" Then Exit Sub
    
    LvSinDetalle.ListItems.Add
    LvSinDetalle.ListItems(LvSinDetalle.ListItems.Count).SubItems(1) = CboCuenta.Text
    LvSinDetalle.ListItems(LvSinDetalle.ListItems.Count).SubItems(2) = CboArea.Text
    LvSinDetalle.ListItems(LvSinDetalle.ListItems.Count).SubItems(3) = CboCentroCosto.Text
    LvSinDetalle.ListItems(LvSinDetalle.ListItems.Count).SubItems(4) = CboItemGasto.Text
    LvSinDetalle.ListItems(LvSinDetalle.ListItems.Count).SubItems(5) = TxtSdExento
    LvSinDetalle.ListItems(LvSinDetalle.ListItems.Count).SubItems(6) = TxtSDNeto
    LvSinDetalle.ListItems(LvSinDetalle.ListItems.Count).SubItems(7) = TxtSdIva
    LvSinDetalle.ListItems(LvSinDetalle.ListItems.Count).SubItems(8) = TxtSdIvaRetenido
    
    
    LvSinDetalle.ListItems(LvSinDetalle.ListItems.Count).SubItems(9) = CboCuenta.ItemData(CboCuenta.ListIndex)
    LvSinDetalle.ListItems(LvSinDetalle.ListItems.Count).SubItems(10) = CboArea.ItemData(CboArea.ListIndex)
    LvSinDetalle.ListItems(LvSinDetalle.ListItems.Count).SubItems(11) = CboCentroCosto.ItemData(CboCentroCosto.ListIndex)
    LvSinDetalle.ListItems(LvSinDetalle.ListItems.Count).SubItems(12) = CboItemGasto.ItemData(CboItemGasto.ListIndex)
    
    TxtSdExento = 0
    TxtSDNeto = 0
    TxtSdIvaRetenido = 0
    TxtSdIva = 0
    CboCuenta.SetFocus
    
    txtTotalNeto = NumFormat(TotalizaColumna(LvSinDetalle, "neto"))
    TxtExento = NumFormat(TotalizaColumna(LvSinDetalle, "otros"))
    TxtIVA = NumFormat(TotalizaColumna(LvSinDetalle, "iva"))
    txtIvaRetenido = NumFormat(TotalizaColumna(LvSinDetalle, "retenido"))
    CalculaTotal
End Sub
Private Sub cmdSalir_Click()
    Unload Me
End Sub

Private Sub Command1_Click()
    If Command1.Caption = "Timer On" Then
        Command1.Caption = "Timer Off"
        Timer1.Enabled = False
    Else
        Command1.Caption = "Timer On"
        Timer1.Enabled = True
    End If
    
End Sub

Private Sub Command3_Click()
    For i = 1 To LVDetalle.ListItems.Count
        LVDetalle.ListItems(i).Checked = False
    Next
   ' SumaGuias
    Stab.Tab = 0
End Sub

Private Sub DtEntrada_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then CmdAgregarProducto.SetFocus
End Sub

'
Private Sub Form_KeyPress(KeyAscii As Integer)
  
    If KeyAscii = 13 Then
            SendKeys "{tab}"
            KeyAscii = 0
    Else
   
        If KeyAscii = 8 Or KeyAscii = 45 Then Exit Sub
        'If KeyAscii = 46 Then KeyAscii = 44
        KeyAscii = Asc(UCase$(Chr(KeyAscii))) 'Convertimos a mayuscula cualquier caractar
        Dim txtS      As Control '                  ingresado
        Dim Decimales As Integer
        For Each txtS In Controls
            If (TypeOf txtS Is TextBox) Then
                If Me.ActiveControl.Name = txtS.Name Then
                    If Mid(txtS.Tag, 1, 1) = "N" Then 'Tag de textbox si es N = numerico
                        '                        If Not IsNumeric(KeyAscii) Then
                        If KeyAscii = 44 Then
                            If Len(txtS.Tag) > 1 Then
                                Decimales = Mid(txtS.Tag, 2, 1)
                                Replace txtS, ",", "."
                                If Decimales > 0 Then If InStrRev(txtS, ".") > 0 Or InStrRev(txtS, ",") > 0 Then KeyAscii = 0
                            End If
                        Else
                            KeyAscii = AceptaSoloNumeros(KeyAscii)
                        End If
                        'If KeyAscii = 46 Then If InStrRev(txtS, ".") > 0 Or InStrRev(txtS, ",") > 0 Then KeyAscii = 0
                    End If
                End If
            End If
        Next
    End If
End Sub


'
Private Sub Form_Load()
    Bm_Editable = True
    FrameGuias.Top = 1800
    Aplicar_skin Me, App.Path
     SkEmpresaActiva = "EMPRESA ACTIVA: " & SP_Empresa_Activa
    s_Requiere_Refencia = "NO"
    sm_Movimiento_Doc = "NN"
    s_Modifica_Inventario = "SI"
    s_Inventario = "SUMA"
    b_NotaDebito = False
    b_NotaCredito = False
    b_Agrega = True
    If SP_Control_Inventario = "SI" Then LLenarCombo CboTipoDoc, "doc_nombre", "doc_id", "sis_documentos", "doc_documento='COMPRA' and doc_activo='SI'", "doc_orden"
    If SP_Control_Inventario = "NO" Then LLenarCombo CboTipoDoc, "doc_nombre", "doc_id", "sis_documentos", "doc_documento='COMPRA' and doc_activo='SI' AND doc_contable='SI'", "doc_orden"
    LLenarCombo CboPlazos, "pla_nombre", "pla_dias", "par_plazos_vencimiento", "pla_activo='SI'", "pla_id"
    CboPlazos.ListIndex = 0
    TxtMesContable = UCase(MonthName(IG_Mes_Contable))
    TxtAnoContable = IG_Ano_Contable
   
   
    LLenarCombo CboCuenta, "pla_nombre", "pla_id", "con_plan_de_cuentas", "pla_activo='SI'"
    LLenarCombo CboCentroCosto, "cen_nombre", "cen_id", "par_centros_de_costo", "cen_activo='SI' AND rut_emp='" & SP_Rut_Activo & "'"
    LLenarCombo Me.CboItemGasto, "gas_nombre", "gas_id", "par_item_gastos", "gas_activo='SI' AND rut_emp='" & SP_Rut_Activo & "'"
    LLenarCombo CboArea, "are_nombre", "are_id", "par_areas", "are_activo='SI' AND rut_emp='" & SP_Rut_Activo & "'"
    Timer1.Enabled = True
    bp_PermiteOk = False
    DtEntrada.Value = Date
    DtFecha.Value = Date
    bp_Ingresa_Gasto = False
    CboItemGasto.Top = CboCuenta.Top
    
    If DG_ID_Unico = 0 Then
        'Nuevo
        
        bp_Nueva_Compra = True
        FrameTotal.Enabled = True
    Else
        If SP_Control_Inventario = "SI" Then Bm_Editable = False
         bp_Nueva_Compra = False
      '  CMDeliminaMaterial.Enabled = False
      '  CmdGuardar.Enabled = False
      '  Me.CmdAgregarProducto.Enabled = False
      '  FrmProveedor.Enabled = False
        CargaDatos
       ' FrameTotal.Enabled = False
    End If
    Centrar Me
    If SP_Control_Inventario = "SI" Then
        FrameMA.Enabled = True
        FrameDatos.Height = 900
        FrameDatos.Width = 14085
        
    Else
        FrameDatos.Left = 200
        FrameDatos.Width = 14625
        
        CboCuenta.Width = 2500
        CboArea.Left = CboCuenta.Left + CboCuenta.Width + 10
        CboArea.Width = 2500
        SkArea.Left = CboArea.Left
        CboCentroCosto.Left = CboArea.Left + CboArea.Width + 10
        CboCentroCosto.Width = 2500
        SkCC.Left = CboCentroCosto.Left
        CboItemGasto.Left = CboCentroCosto.Left + CboCentroCosto.Width + 10
        CboItemGasto.Width = 2500
        SkItem.Left = CboItemGasto.Left
        
        TxtSdExento.Visible = True
        TxtSdIvaRetenido.Visible = True
        TxtSDNeto.Visible = True
        TxtSdIva.Visible = True
        
        FrameDatos.Height = 2900
        FrameMA.Enabled = False
      '  TxtExento.Locked = False
      '  txtTotalNeto.Locked = False
       ' txtIVA.Locked = False
        txtIvaRetenido.Locked = False
        txtIvaRetenido.Enabled = False
        FrameMA.Visible = False
        
        FrameTotal.Top = FrameMA.Top + 2000
        FramAc.Top = FrameTotal.Top + FrameTotal.Height + 100
        Me.Height = Me.Height - FrameMA.Height + 2000
        Me.CmdOkSd.Visible = True
    End If
     'CONSULTAMOS SI LA EMPRESA ACTIVA
    'LLEVA CENTRO DE COSTO, AREAS
    '8 OCTUBRE 2011
    Sql = "SELECT emp_centro_de_costos,emp_areas,emp_item_de_gastos " & _
          "FROM sis_empresas " & _
          "WHERE rut='" & SP_Rut_Activo & "'"
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        If RsTmp!emp_centro_de_costos = "NO" Then
            LLenarCombo CboCentroCosto, "cen_nombre", "cen_id", "par_centros_de_costo", "cen_id=99999"
            CboCentroCosto.ListIndex = 0
            CboCentroCosto.Visible = False
            SkCC.Visible = False
        End If
        If RsTmp!emp_areas = "NO" Then
            SkArea.Visible = False
            LLenarCombo CboArea, "are_nombre", "are_id", "par_areas", "are_id=99999"
            CboArea.ListIndex = 0
            CboArea.Visible = False
        End If
        If RsTmp!emp_item_de_gastos = "NO" Then
            SkItem.Visible = False
            LLenarCombo CboItemGasto, "gas_nombre", "gas_id", "par_item_gastos", "gas_id=99999"
            CboItemGasto.ListIndex = 0
            CboItemGasto.Visible = False
        End If
    End If
    If Not Bm_Editable Then Me.CmdGuardar.Enabled = False
End Sub
Private Sub CargaDatos()
        Dim sp_Por_Guias As String
        Sql = "SELECT fecha,doc_id,no_documento,rut,nombre_proveedor,mes_contable,ano_contable,doc_factura_guias, " & _
                     "neto,iva,especifico,total,pla_id,cen_id,are_id,gas_id " & _
              "FROM com_doc_compra " & _
              "WHERE id=" & DG_ID_Unico 'Aqui no es necesario el rut, ya que cada doc de compra tiene un ID unico
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
            With RsTmp
                'Si es por ejemplo Nota de credito no vuelve a cargar los datos
                'del documento de compra
                '2 Abril 2011
                sp_Por_Guias = !doc_factura_guias
                If s_Requiere_Refencia = "NO" Then
                    'FrameMA.Enabled = False
                    DtFecha.Value = !Fecha
                    Busca_Id_Combo CboTipoDoc, !doc_id
                    TxtNDoc = !no_documento
                    TxtRutProveedor = !Rut
                    TxtRsocial = !nombre_proveedor
                    TxtMesContable = UCase(MonthName(!mes_contable))
                    TxtAnoContable = !ano_contable
                    Busca_Id_Combo CboCuenta, 0 & !pla_id
                    Busca_Id_Combo CboArea, 0 & !are_id
                    Busca_Id_Combo CboItemGasto, 0 & !gas_id
                    Busca_Id_Combo CboCentroCosto, 0 & !cen_id
                    
                Else
                    FrameMA.Enabled = True
                End If
                ip_ID_Doc_Grabado = !doc_id
                sp_Nombre_Doc_Grabado = CboTipoDoc.Text
                dp_NroDoc_Grabado = !no_documento
                sp_Movimiento_Doc_Grabado = sm_Movimiento_Doc
                 Me.TextEspecifico = NumFormat(!especifico)
                 Me.TextEspecifico.Locked = True
                If sp_Por_Guias = "SI" Then
                    txtTotalNeto = NumFormat(!Neto)
                    TxtIVA = NumFormat(!IVA)
                    TxtTotal = NumFormat(!Total)
                   
                End If
            End With
        End If
        If sp_Por_Guias = "NO" Then
            If SP_Control_Inventario = "SI" Then
                    Sql = "SELECT id_compra_detalle,pro_codigo,mar_nombre,descripcion,neto,descuento,exento," & _
                                 "cantidad- IFNULL((SELECT SUM(e.cantidad) FROM com_detalle e,com_doc_compra o WHERE e.id_compra=o.id and o.inventario='RESTA' and o.id_ref=d.id_compra AND d.pro_codigo=e.pro_codigo GROUP BY e.pro_codigo),0) cant, " & _
                                 "total,total_linea,'',fecha_entrada,pla_id,cen_id,gas_id,are_id " & _
                          "FROM com_detalle d, par_marcas m " & _
                          "WHERE d.mar_id=m.mar_id AND id_compra=" & DG_ID_Unico
                    Consulta RsTmp, Sql
                    LLenar_Grilla RsTmp, Me, LvMaterialesGrabado, False, True, True, False
                    LLenar_Grilla RsTmp, Me, LvMateriales, False, True, True, False
                    If LvMateriales.ListItems.Count > 0 Then LvMateriales_Click
                    CalculaTotales
'                    bp_Nueva_Compra = False
            Else
                    Sql = "SELECT d.csd_id,pla_nombre,are_nombre,cen_nombre,gas_nombre,d.csd_exento,d.csd_neto,d.csd_iva,d.csd_retencion,d.pla_id,d.are_id,d.cen_id,d.gas_id " & _
                            "FROM com_sin_detalle d " & _
                            "INNER JOIN con_plan_de_cuentas c USING(pla_id) " & _
                            "INNER JOIN par_areas a USING(are_id) " & _
                            "INNER JOIN par_centros_de_costo o USING(cen_id) " & _
                            "INNER JOIN par_item_gastos g USING(gas_id) " & _
                          "WHERE d.id=" & DG_ID_Unico
                    Consulta RsTmp, Sql
                    LLenar_Grilla RsTmp, Me, LvSinDetalle, False, True, True, False
                    CalculaTotales
'                    bp_Nueva_Compra = False
            End If
                    
            'Me.CMDeliminaMaterial.Enabled = False
        Else
            If Mid(CboTipoDoc.Text, 1, 7) = "FACTURA" Then
                Sql = "SELECT id,fecha,no_documento,rut,nombre_proveedor,neto,iva,total " & _
                      "FROM com_doc_compra " & _
                      "WHERE  rut_emp='" & SP_Rut_Activo & "' AND  rut='" & TxtRutProveedor & "' AND doc_id_factura=" & CboTipoDoc.ItemData(CboTipoDoc.ListIndex) & " AND nro_factura=" & TxtNDoc
                Consulta RsTmp2, Sql
                LLenar_Grilla RsTmp2, Me, LVDetalle, True, True, True, False
                FrameGuias.Visible = True
            End If
        End If
        
End Sub




Private Sub LvDetalle_ItemCheck(ByVal item As MSComctlLib.ListItem)
'    SumaGuias
End Sub

Private Sub LvMateriales_Click()
    If LvMateriales.SelectedItem Is Nothing Then Exit Sub
    
    Busca_Id_Combo Me.CboCuenta, LvMateriales.SelectedItem.SubItems(12)
    Busca_Id_Combo Me.CboCentroCosto, LvMateriales.SelectedItem.SubItems(13)
    Busca_Id_Combo Me.CboItemGasto, LvMateriales.SelectedItem.SubItems(14)
    Busca_Id_Combo Me.CboArea, LvMateriales.SelectedItem.SubItems(15)
    
    
End Sub

Private Sub LvMateriales_DblClick()
    If LvMateriales.SelectedItem Is Nothing Then Exit Sub
    TxtCantidad.Enabled = True
    With LvMateriales.SelectedItem
        Busca_Id_Combo Me.CboCuenta, LvMateriales.SelectedItem.SubItems(12)
        Busca_Id_Combo Me.CboCentroCosto, LvMateriales.SelectedItem.SubItems(13)
        Busca_Id_Combo Me.CboItemGasto, LvMateriales.SelectedItem.SubItems(14)
        
        TxtCodigo = .SubItems(1)
        TxtMarca = .SubItems(2)
        TxtDescrp = .SubItems(3)
        TxtPrecio = .SubItems(4)
        txtDescuento = .SubItems(5)
        TxtPexento = .SubItems(6)
        TxtCantidad = .SubItems(7)
        txtPrecioFinal = .SubItems(8)
        TxtSubTotal = .SubItems(9)
        DtEntrada.Value = .SubItems(11)
        'TxtPrecioDocumento = .SubItems(4)
        bp_PermiteOk = True
        If bm_Solo_Corrige_Montos Then
            
            TxtCantidad.Enabled = False
            TxtPrecio.Enabled = True
            TxtPrecio.SetFocus
            
        End If
        'TxtPrecioDocumento = NumFormat(Round(LvMaterialesGrabado.ListItems(LvMateriales.SelectedItem.Index).SubItems(4)))
        
    End With
    
    
    
End Sub


Private Sub LvSinDetalle_DblClick()
    If LvSinDetalle.SelectedItem Is Nothing Then Exit Sub
    
    If s_Anula_Documento = "SI" Then Exit Sub
    
    TxtSdExento = LvSinDetalle.SelectedItem.SubItems(5)
    TxtSDNeto = LvSinDetalle.SelectedItem.SubItems(6)
    TxtSdIva = LvSinDetalle.SelectedItem.SubItems(7)
    TxtSdIvaRetenido = LvSinDetalle.SelectedItem.SubItems(8)
    
    
    Busca_Id_Combo CboCuenta, LvSinDetalle.SelectedItem.SubItems(9)
    Busca_Id_Combo CboArea, LvSinDetalle.SelectedItem.SubItems(10)
    Busca_Id_Combo CboCentroCosto, LvSinDetalle.SelectedItem.SubItems(11)
    Busca_Id_Combo CboItemGasto, LvSinDetalle.SelectedItem.SubItems(12)
    
    
    LvSinDetalle.ListItems.Remove LvSinDetalle.SelectedItem.Index

End Sub



Private Sub TextEspecifico_GotFocus()
    En_Foco TextEspecifico
End Sub

Private Sub TextEspecifico_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
End Sub

Private Sub TextEspecifico_Validate(Cancel As Boolean)
    If Val(TextEspecifico) = 0 Then TextEspecifico = 0
    TextEspecifico = Format(Abs(CDbl(TextEspecifico)), "#,0")
    CalculaTotal
End Sub

Private Sub Timer1_Timer()
    Timer1.Enabled = False
    On Error Resume Next
    For Each ltxts In Controls
        If (TypeOf ltxts Is TextBox) Then
            'If ltxts.Visible = False Then Exit Sub
            If ltxts.Name = Me.ActiveControl.Name Then  'Foco activo
                ltxts.BackColor = IIf(ltxts.Locked, ClrDesha, ClrCfoco)
            Else
                ltxts.BackColor = IIf(ltxts.Locked, ClrDesha, ClrSfoco)
            End If
        End If
    Next
End Sub

Private Sub Timer2_Timer()
    On Error Resume Next
    If Me.FrmProveedor.Enabled Then DtFecha.SetFocus Else CmdSalir.SetFocus
    Timer2.Enabled = False
End Sub

Private Sub Timer3_Timer()
    
End Sub

Private Sub TxtCantidad_Change()
    PrecioFinalProducto
    If Val(TxtCantidad) > 0 Then
        TxtSubTotal = Format(txtPrecioFinal * Val(TxtCantidad), "#,#")
    Else
        TxtSubTotal = 0
    End If
    
End Sub


Private Sub TxtCantidad_GotFocus()
    En_Foco TxtCantidad
    TxtStockActual.Visible = True
End Sub

Private Sub TxtCantidad_LostFocus()
    TxtStockActual.Visible = False
End Sub

Private Sub TxtCantidad_Validate(Cancel As Boolean)
    If Val(TxtCantidad) = 0 Then TxtCantidad = 0
    TxtCantidad = Replace(TxtCantidad, ",", ".")
    TxtCantidad = Abs(CDbl(TxtCantidad))
End Sub

Private Sub TxtCodigo_GotFocus()
    Me.TxtCodigo.BackColor = ClrCfoco
    Me.Timer1.Enabled = True
End Sub

Private Sub TxtCodigo_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 And Len(TxtCodigo.Text) > 0 Then SendKeys "{Tab}"
        
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub TxtCodigo_LostFocus()
    Me.TxtCodigo.BackColor = ClrSfoco
    If Len(TxtCodigo) = 0 Then
        TxtMarca = ""
        TxtDescrp = ""
        TxtPrecio = 0
        txtDescuento = 0
        TxtPexento = 0
        TxtCantidad = 0
        txtPrecioFinal = 0
        TxtSubTotal = 0
    End If
        
End Sub

Private Sub TxtCodigo_Validate(Cancel As Boolean)
    Dim pv_Respuesta As Variant, RsTmp2 As Recordset
    bp_PermiteOk = False
    If Len(TxtCodigo) = 0 Then Exit Sub
    If Trim(TxtCodigo.Text) = "0" Then
        pv_Respuesta = MsgBox("�Ingresar Descripcion manual...?" & vbNewLine & "  (no afecta inventarios) ... ", vbQuestion + vbYesNoCancel)
        If pv_Respuesta = vbNo Then
            TxtDescrp.Locked = True
            Cancel = True
            bp_Ingresa_Gasto = False
            Exit Sub
           
        ElseIf pv_Respuesta = vbCancel Then
            TxtDescrp.Locked = True
            bp_Ingresa_Gasto = False
            Cancel = False
            Exit Sub
        Else
            TxtCantidad = 1
            TxtCantidad.Locked = True
            TxtDescrp.TabStop = True
            TxtDescrp.Locked = False
            TxtDescrp.SetFocus
            bp_Ingresa_Gasto = True
            Cancel = False
            Exit Sub
        End If
    Else
    
    End If
    Sql = "SELECT descripcion,marca,precio_compra,stock_actual,tip_nombre familia,ume_nombre " & _
          "FROM maestro_productos m,par_tipos_productos t,sis_unidad_medida u " & _
          "WHERE u.ume_id=m.ume_id AND m.rut_emp='" & SP_Rut_Activo & "' AND  m.tip_id=t.tip_id AND codigo='" & TxtCodigo & "'"
    Call Consulta(RsTmp, Sql)
    If RsTmp.RecordCount > 0 Then
                'Codigo ingresado ha sido encontrado
            TxtCantidad = 0
            TxtCantidad.Locked = False
            TxtDescrp.TabStop = False
            'TxtDescrp.Locked = False
            With RsTmp
                TxtCodigo.Tag = !familia
                Me.TxtDescrp = !Descripcion
                Me.TxtMarca.Text = !MARCA
                Me.TxtPrecio.Text = !precio_compra
                TxtCantidad.ToolTipText = "Unidad Medida: " & !ume_nombre
                SKUme = !ume_nombre
                'Me.LbStockActual.Caption = !stock_actual
                'Me.TxtStockActual.Text = !stock_actual
                Sql = "SELECT SUM(sto_stock) stock,pro_ultimo_precio_compra pucom " & _
                     "FROM pro_stock " & _
                     "WHERE rut_emp='" & SP_Rut_Activo & "' AND  pro_codigo='" & TxtCodigo & "' " & _
                     "GROUP By pro_codigo"
                Consulta RsTmp2, Sql
                If RsTmp2.RecordCount > 0 Then
                    Me.TxtStockActual.Text = "STOCK ACTUAL : " & RsTmp2!stock
                    TxtPrecio = Format(RsTmp2!pucom, "#,##0")
                End If
                Me.TxtPrecio.SetFocus
                bp_PermiteOk = True
                
                
            End With
    Else
            Respuesta = MsgBox("Codigo no encontrado..." & Chr(13) & "�Crear nuevo producto?", vbYesNo + vbQuestion)
            If Respuesta = 6 Then
                'Crear nuevo
                SG_codigo = Empty
                AgregarProducto.TxtCodigo.Text = Me.TxtCodigo.Text
                AgregarProducto.Show 1
                Cancel = True
            Else
                TxtCodigo.Text = ""
                Me.TxtDescrp.Text = ""
                Me.TxtMarca.Text = ""
                Me.TxtPrecio.Text = ""
                TxtCantidad = ""
                Me.TxtCodigo.SetFocus
            End If
    End If
    
End Sub

Private Sub TxtDescrp_Validate(Cancel As Boolean)
    bp_PermiteOk = IIf(Len(TxtDescrp) > 0, True, False)
End Sub

Private Sub txtDescuento_GotFocus()
    En_Foco txtDescuento
End Sub

Private Sub TxtDescuento_LostFocus()
    txtDescuento = Format(txtDescuento, "#,##0")
End Sub

Private Sub txtDescuento_Validate(Cancel As Boolean)
    txtDescuento = Format(txtDescuento, "#,0")
    If Val(txtDescuento) = 0 Then txtDescuento = 0
    If CDbl(txtDescuento) >= CDbl(TxtPrecio) Then
        MsgBox "Descuento no puede ser mayor que el precio de compra...", vbInformation
        txtDescuento = 0
    End If
    txtDescuento = Format(Abs(CDbl(txtDescuento)), "#,0")
    PrecioFinalProducto
End Sub
Private Sub PrecioFinalProducto()
    txtPrecioFinal = CDbl(IIf(Len(TxtPrecio) = 0, 0, TxtPrecio)) - CDbl(IIf(Len(txtDescuento) = 0, 0, txtDescuento)) + CDbl(IIf(Len(TxtPexento) = 0, 0, TxtPexento))
    If Val(TxtCantidad) > 0 Then
        TxtSubTotal = Format(txtPrecioFinal * Val(TxtCantidad), "#,#")
    Else
        TxtSubTotal = 0
    End If

End Sub



Private Sub TxtExentoGasto_Validate(Cancel As Boolean)
    If Val(TxtExentoGasto) = 0 Then TxtExentoGasto = 0 Else TxtExentoGasto = Format(TxtExentoGasto, "#,##0")
End Sub

Private Sub TxtExento_GotFocus()
    En_Foco TxtExento
End Sub


Private Sub TxtExento_Validate(Cancel As Boolean)
    CalculaTotal
End Sub


Private Sub txtIVA_GotFocus()
    En_Foco TxtIVA
End Sub


Private Sub TxtIva_Validate(Cancel As Boolean)
    If SP_Control_Inventario = "NO" Then
        If Val(TxtIVA) = 0 Then TxtIVA = 0 Else TxtIVA = NumFormat(TxtIVA)
    End If
    TxtIVA = NumFormat(TxtIVA)
    CalculaTotal
End Sub

Private Sub txtIvaRetenido_GotFocus()
    En_Foco txtIvaRetenido
End Sub
Private Sub txtIvaRetenido_Validate(Cancel As Boolean)
    If Val(txtIvaRetenido) = 0 Then
        txtIvaRetenido = 0
    Else
        txtIvaRetenido = Format(txtIvaRetenido, "#,##0")
    End If
    TxtTotal = Format(CDbl(TxtExento) + CDbl(txtTotalNeto) + CDbl(TxtIVA) - CDbl(txtIvaRetenido), "#,##0")
End Sub

Private Sub TxtNDoc_GotFocus()
    En_Foco TxtNDoc
End Sub

Private Sub TxtNDoc_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
End Sub

Private Sub TxtNDoc_Validate(Cancel As Boolean)
    ComprobarSiDocumento DG_ID_Unico
End Sub
Sub ComprobarSiDocumento(Optional ByVal id_Excepcion As Long)
    Dim sp_Condicion As String, s_NotaDebito As String, sp_Abonos As String
    bp_Documento_disponible = False
    If Val(TxtNDoc) > 0 And Len(TxtRutProveedor) > 0 And Me.CboTipoDoc.ListIndex > -1 Then
        If id_Excepcion <> Empty Then sp_Condicion = " AND id<>" & id_Excepcion Else sp_Condicion = ""
        Sql = "SELECT no_documento " & _
              "FROM com_doc_compra " & _
              "WHERE rut_emp='" & SP_Rut_Activo & "' AND  rut='" & TxtRutProveedor & "' AND no_documento=" & TxtNDoc & " AND doc_id=" & CboTipoDoc.ItemData(CboTipoDoc.ListIndex) & sp_Condicion
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
            TxtRutProveedor = Empty
            MsgBox "Documento de compra ya ha sido utilizado...", vbInformation + vbOKOnly
            DtFecha.SetFocus
        Else
            Sql = "SELECT m.mpa_nombre,abo_fecha,d.ctd_monto " & _
                  "FROM cta_abono_documentos d " & _
                  "INNER JOIN cta_abonos a USING(abo_id) " & _
                  "INNER JOIN par_medios_de_pago m USING(mpa_id) " & _
                  "WHERE abo_cli_pro ='PRO' AND id=" & id_Excepcion
            Consulta RsTmp2, Sql
            If RsTmp2.RecordCount > 0 Then
                sp_Abonos = Empty
                With RsTmp2
                    .MoveFirst
                    Do While Not .EOF
                        sp_Abonos = sp_Abonos & !abo_fecha & " " & !mpa_nombre & " " & !ctd_monto & vbNewLine
                        .MoveNext
                    Loop
                End With
                MsgBox "No es posible modificar este documento" & vbNewLine & "Ya tiene pago(s) registrado(s)" & vbNewLine & sp_Abonos
                bp_Documento_disponible = False
            Else
                bp_Documento_disponible = True
                Sql = "SELECT doc_movimiento,doc_actualiza_precio_compra actp,doc_requiere_referencia reqi,doc_abreviado  " & _
                      "FROM sis_documentos " & _
                      "WHERE doc_id=" & CboTipoDoc.ItemData(CboTipoDoc.ListIndex)
                
                Consulta RsTmp, Sql
                If RsTmp.RecordCount > 0 Then
                    sm_Movimiento_Doc = RsTmp!doc_movimiento
                    s_ActualizaPcompra = RsTmp!actp
                    s_Requiere_Refencia = RsTmp!reqi
                    s_NotaDebito = RsTmp!doc_abreviado
                End If
            End If
        End If
    Else
            
        If CboTipoDoc.ListIndex = -1 Then Exit Sub
        Sql = "SELECT doc_movimiento,doc_actualiza_precio_compra actp,doc_requiere_referencia reqi,doc_abreviado " & _
              "FROM sis_documentos " & _
              "WHERE doc_id=" & CboTipoDoc.ItemData(CboTipoDoc.ListIndex)
        
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
            sm_Movimiento_Doc = RsTmp!doc_movimiento
            s_ActualizaPcompra = RsTmp!actp
            s_Requiere_Refencia = RsTmp!reqi
            s_NotaDebito = RsTmp!doc_abreviado

        End If
    End If
    If s_Requiere_Refencia = "SI" Then
        If CboTipoDoc.ListIndex > -1 And Val(TxtNDoc) > 0 And Len(Me.TxtRutProveedor) > 0 Then
                LLenarCombo Me.CboDocRef, "doc_nombre", "doc_id", "sis_documentos", "doc_activo='SI' AND doc_referenciable='SI' AND doc_documento='COMPRA'", "doc_id"
                If s_NotaDebito = "NDM" Or s_NotaDebito = "NDE" Then
                    LLenarCombo CboMotivoNC, "tnc_nombre", "tnc_id", "ntc_tipos", "tnc_activo='SI' AND tnc_nota_debito='SI'"
                Else
                    LLenarCombo CboMotivoNC, "tnc_nombre", "tnc_id", "ntc_tipos", "tnc_activo='SI'"
                End If
        End If
    End If
End Sub



Private Sub TxtNroRef_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
End Sub

Private Sub TxtPexento_Validate(Cancel As Boolean)
    TxtPexento = Format(TxtPexento, "#,0")
    If Val(TxtPexento) = 0 Then TxtPexento = 0
    TxtPexento = Format(CDbl(Abs(TxtPexento)), "#,0")
    PrecioFinalProducto
End Sub
Private Sub TxtPexento_GotFocus()
    En_Foco TxtPexento
End Sub
Private Sub TxtPRecio_GotFocus()
    En_Foco TxtPrecio
    If b_NotaCredito Or b_NotaDebito Then
        TxtPrecioDocumento.Visible = True
    End If
End Sub

Private Sub TxtPrecio_LostFocus()
    TxtPrecio = Format(TxtPrecio, "#,0")
    
    TxtPrecioDocumento.Visible = False
End Sub

Private Sub TxtPrecio_Validate(Cancel As Boolean)
    If Val(TxtPrecio) = 0 Then TxtPrecio = 0
    TxtPrecio = Format(TxtPrecio, "#,0")
    TxtPrecio = Format(Abs(CDbl(TxtPrecio)), "#,0")
    PrecioFinalProducto
End Sub
Private Sub txtPrecioGasto_Validate(Cancel As Boolean)
    If Val(txtPrecioGasto) = 0 Then txtPrecioGasto = 0 Else txtPrecioGasto = Format(txtPrecioGasto, "#,##0")
End Sub

Private Sub TxtRutProveedor_Validate(Cancel As Boolean)
    If Len(TxtRutProveedor) > 0 Then
        Respuesta = VerificaRut(TxtRutProveedor, NuevoRut)
        TxtRutProveedor = NuevoRut
        Sql = "SELECT nombre_empresa " & _
              "FROM maestro_proveedores " & _
              "WHERE rut_proveedor='" & TxtRutProveedor & "' "
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
            TxtRsocial = RsTmp!nombre_empresa
            ComprobarSiDocumento
            If TxtRutProveedor = Empty Then Exit Sub
            If CboTipoDoc.ListIndex > -1 And Mid(CboTipoDoc.Text, 1, 7) = "FACTURA" Then
                If CboTipoDoc.ItemData(CboTipoDoc.ListIndex) <> IG_id_GuiaProveedor Then
                    'Aqui buscamos si el Proveedor a emitido guias y no estan facturadas
                        
                        

                    Sql = "SELECT id,fecha,no_documento,rut,nombre_proveedor,neto,iva,total " & _
                          "FROM com_doc_compra " & _
                          "WHERE  rut_emp='" & SP_Rut_Activo & "' AND rut='" & TxtRutProveedor & "' AND doc_id=" & IG_id_GuiaProveedor & " AND nro_factura=0 "
                    Consulta RsTmp2, Sql
                    If RsTmp2.RecordCount > 0 Then
                        'Se econtraron Guias no facturadas de este proveedor
                        'Abrimos ventana de guias no facturadas
                         'LLenar_Grilla RsTmp2, Me, LvDetalle, True, True, True, False
                        'Stab.TabVisible(1) = True
                        '�Stab.Tab = 1
                        RutBuscado = TxtRutProveedor
                        compra_FacturaGuias.Show 1
                    
                    End If
                    
                    
                    
                    
                    
                End If
            End If
            
        Else
            MsgBox "Proveedor no encontrado... ", vbOKOnly + vbInformation
            TxtRsocial = ""
            
            Exit Sub
        End If
    End If
    
End Sub

Private Sub TxtSdExento_GotFocus()
    En_Foco TxtSdExento
End Sub


Private Sub TxtSdExento_KeyPress(KeyAscii As Integer)
    KeyAscii = AceptaSoloNumeros(KeyAscii)
End Sub


Private Sub TxtSdExento_Validate(Cancel As Boolean)
    If Val(TxtSdExento) = 0 Then TxtSdExento = "0" Else TxtSdExento = NumFormat(TxtSdExento)
End Sub

Private Sub TxtSdIva_GotFocus()
    En_Foco TxtSdIva
End Sub


Private Sub TxtSdIva_KeyPress(KeyAscii As Integer)
    KeyAscii = AceptaSoloNumeros(KeyAscii)
End Sub


Private Sub TxtSdIva_Validate(Cancel As Boolean)
    If Val(TxtSdIva) = 0 Then TxtSdIva = NumFormat(TxtSdIva)
End Sub

Private Sub TxtSdIvaRetenido_GotFocus()
    En_Foco TxtSdIvaRetenido
End Sub


Private Sub TxtSdIvaRetenido_KeyPress(KeyAscii As Integer)
       KeyAscii = AceptaSoloNumeros(KeyAscii)
End Sub


Private Sub TxtSdIvaRetenido_Validate(Cancel As Boolean)
    If Val(TxtSdIvaRetenido) = 0 Then TxtSdIvaRetenido = NumFormat(TxtSdIvaRetenido)
End Sub


Private Sub TxtSDNeto_GotFocus()
    En_Foco TxtSDNeto
End Sub
Private Sub TxtSDNeto_KeyPress(KeyAscii As Integer)
           KeyAscii = AceptaSoloNumeros(KeyAscii)
End Sub
Private Sub TxtSDNeto_Validate(Cancel As Boolean)
    If Val(TxtSDNeto) = 0 Then
        TxtSDNeto = "0"
    Else
        TxtSDNeto = NumFormat(TxtSDNeto)
        TxtSdIva = NumFormat(CDbl(TxtSDNeto) * Val(("1." & DG_IVA)) - CDbl(TxtSDNeto))
    End If
End Sub





Private Sub txtTotalNeto_Change()
    If SP_Control_Inventario = "NO" Then
        TxtIVA = NumFormat(Round(CDbl(txtTotalNeto) / 100 * DG_IVA))
    End If
End Sub


Private Sub txtTotalNeto_GotFocus()
    En_Foco txtTotalNeto
End Sub





