VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form Compra_CambiaFecha 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Cambia Fecha Documento"
   ClientHeight    =   1980
   ClientLeft      =   6240
   ClientTop       =   540
   ClientWidth     =   3675
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1980
   ScaleWidth      =   3675
   ShowInTaskbar   =   0   'False
   Begin VB.Timer Timer1 
      Interval        =   10
      Left            =   90
      Top             =   1080
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   0
      OleObjectBlob   =   "Compra_CambiaFecha.frx":0000
      Top             =   1620
   End
   Begin VB.CommandButton CmdOk 
      Caption         =   "&Aceptar"
      Height          =   300
      Left            =   1155
      TabIndex        =   2
      Top             =   1560
      Width           =   1080
   End
   Begin VB.CommandButton CmdCancelar 
      Cancel          =   -1  'True
      Caption         =   "&Cancelar"
      Height          =   300
      Left            =   2265
      TabIndex        =   1
      Top             =   1560
      Width           =   1080
   End
   Begin VB.Frame Frame1 
      Caption         =   "Nueva fecha"
      Height          =   1185
      Left            =   840
      TabIndex        =   0
      Top             =   345
      Width           =   2505
      Begin MSComCtl2.DTPicker DtFecha 
         Height          =   285
         Left            =   600
         TabIndex        =   3
         Top             =   510
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   503
         _Version        =   393216
         Format          =   30212097
         CurrentDate     =   41053
      End
   End
End
Attribute VB_Name = "Compra_CambiaFecha"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub CmdCancelar_Click()
    SG_codigo2 = Empty
    Unload Me
End Sub

Private Sub CmdOk_Click()
'    compra_Visualizar.DtFecha = Me.DtFecha
    SG_codigo2 = "'" & Fql(DtFecha.Value) & "'"
    Unload Me
End Sub

Private Sub Form_Load()
    Centrar Me, False
    Aplicar_skin Me
End Sub

Private Sub Timer1_Timer()
    On Error Resume Next
    DtFecha.SetFocus
    Timer1.Enabled = False
End Sub
