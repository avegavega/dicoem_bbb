VERSION 5.00
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "MSDATGRD.OCX"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Begin VB.Form MaestroDeProductos 
   Caption         =   "Form1"
   ClientHeight    =   8595
   ClientLeft      =   165
   ClientTop       =   450
   ClientWidth     =   10875
   LinkTopic       =   "Form1"
   ScaleHeight     =   8595
   ScaleWidth      =   11880
   StartUpPosition =   3  'Windows Default
   WindowState     =   2  'Maximized
   Begin VB.ComboBox CboMarca 
      Height          =   315
      ItemData        =   "MaestroDeProductos.frx":0000
      Left            =   480
      List            =   "MaestroDeProductos.frx":000A
      Style           =   2  'Dropdown List
      TabIndex        =   5
      Top             =   600
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.CommandButton CmdMostrardtodos 
      Caption         =   "Todos"
      Height          =   255
      Left            =   8520
      TabIndex        =   4
      Top             =   480
      Width           =   855
   End
   Begin VB.CommandButton CmdFiltro 
      Caption         =   "Mostrar"
      Height          =   255
      Left            =   7560
      TabIndex        =   3
      Top             =   480
      Width           =   855
   End
   Begin VB.TextBox Txtfiltro 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   5040
      TabIndex        =   1
      Top             =   480
      Width           =   2415
   End
   Begin MSDataGridLib.DataGrid GridProductos 
      Bindings        =   "MaestroDeProductos.frx":0024
      Height          =   5295
      Left            =   120
      TabIndex        =   0
      Top             =   960
      Width           =   11565
      _ExtentX        =   20399
      _ExtentY        =   9340
      _Version        =   393216
      AllowUpdate     =   -1  'True
      HeadLines       =   1
      RowHeight       =   15
      FormatLocked    =   -1  'True
      AllowAddNew     =   -1  'True
      AllowDelete     =   -1  'True
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ColumnCount     =   10
      BeginProperty Column00 
         DataField       =   "RUT_EMPRESA"
         Caption         =   "RUT_EMPRESA"
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1034
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   "MARCA"
         Caption         =   "MARCA"
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1034
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column02 
         DataField       =   "Codigo"
         Caption         =   "Codigo"
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1034
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column03 
         DataField       =   "Descripcion"
         Caption         =   "Descripcion"
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1034
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column04 
         DataField       =   "Precio_Compra"
         Caption         =   "Precio_Compra"
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1034
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column05 
         DataField       =   "Porciento_Utilidad"
         Caption         =   "Porciento_Utilidad"
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1034
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column06 
         DataField       =   "Precio_Venta"
         Caption         =   "Precio_Venta"
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1034
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column07 
         DataField       =   "Margen"
         Caption         =   "Margen"
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1034
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column08 
         DataField       =   "Stock_Actual"
         Caption         =   "Stock_Actual"
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1034
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column09 
         DataField       =   "Stock_Critico"
         Caption         =   "Stock_Critico"
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1034
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         BeginProperty Column00 
            ColumnWidth     =   60,094
         EndProperty
         BeginProperty Column01 
            Button          =   -1  'True
            ColumnWidth     =   1620,284
         EndProperty
         BeginProperty Column02 
            ColumnWidth     =   1379,906
         EndProperty
         BeginProperty Column03 
            ColumnWidth     =   1739,906
         EndProperty
         BeginProperty Column04 
            ColumnWidth     =   1094,74
         EndProperty
         BeginProperty Column05 
            ColumnWidth     =   1305,071
         EndProperty
         BeginProperty Column06 
            ColumnWidth     =   975,118
         EndProperty
         BeginProperty Column07 
            ColumnWidth     =   915,024
         EndProperty
         BeginProperty Column08 
            ColumnWidth     =   975,118
         EndProperty
         BeginProperty Column09 
            ColumnWidth     =   959,811
         EndProperty
      EndProperty
   End
   Begin MSAdodcLib.Adodc AdoProductos 
      Height          =   330
      Left            =   360
      Top             =   240
      Width           =   2655
      _ExtentX        =   4683
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   2
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   3
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   "DSN=Citroen"
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   "Citroen"
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   "Maestro_PRODUCTOS"
      Caption         =   "Adodc1"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.Label Label1 
      Caption         =   "Filtro"
      Height          =   375
      Left            =   4560
      TabIndex        =   2
      Top             =   480
      Width           =   735
   End
End
Attribute VB_Name = "MaestroDeProductos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub CmdFiltro_Click()
    Dim Filtro As String
    If Me.Txtfiltro.Text <> "" Then
        Filtro = "Descripcion like '*" & Me.Txtfiltro.Text & "*'" 'asigno filtro
        Me.AdoProductos.Recordset.Filter = Filtro 'ejecuto el filtro
    
    End If
        
End Sub

Private Sub CmdMostrardtodos_Click()
    Me.AdoProductos.Recordset.Filter = 0 'libro el filtro
    
    Set Me.GridProductos.DataSource = Me.AdoProductos.Recordset 'forzar a llenar la grilla
    
End Sub



Private Sub Form_Load()
    CboMarca.ListIndex = 0
End Sub

Private Sub GridProductos_AfterColEdit(ByVal ColIndex As Integer)
    Dim VCPorcentaje As Double
    Dim VCPrecioCpra As Double
    Dim VCPrecioVta As Double
    Dim VCMargen As Double
    
    With Me.GridProductos
        If ColIndex = 4 Then  'Si el precio compra = 0 asigna 1
            If Val(.Text) = 0 Then .Text = 1
        ElseIf ColIndex = 5 Then  'Calcula precio venta de acuerdo
             VCPorcentaje = Val(.Text) 'al porcentaje ingresado
            .Col = 4 'me cambio a la columna 4
            VCPrecioCpra = Val(.Text) ' para rescatar el precio de compra
            .Col = 5 'vuelvo a la columna 5
            VCPrecioVta = VCPrecioCpra + (VCPrecioCpra / 100 * VCPorcentaje)
            VCMargen = VCPrecioVta - VCPrecioCpra 'Calcula el margen
            .Col = 6 'Me cambio a la columna 6
            .Text = VCPrecioVta ' y asigno nuevo precio de venta calculado
            .Col = 7
            .Text = VCMargen ' Asigno nuevo margen calculado
            .Col = 5
             
        ElseIf ColIndex = 6 Then
            .Col = 4
            VCPrecioCpra = Val(.Text)
            .Col = 6
            VCPrecioVta = Val(.Text)
            VCMargen = VCPrecioVta - VCPrecioCpra
            .Col = 7
            .Text = VCMargen
            VCPorcentaje = VCMargen * 100 / VCPrecioCpra
            .Col = 5
            .Text = VCPorcentaje
    
        End If
        
    End With
    
End Sub





Private Sub GridProductos_BeforeUpdate(Cancel As Integer)
    
    With Me.GridProductos
        
        .Col = 1
        If .Text = "" Then
            MsgBox "Debe especificar marca del repuesto"
            Cancel = 1
        End If
        
        .Col = 3
        If .Text = "" Then
            MsgBox "Debe especificar descripcion"
            Cancel = 1
        End If
        
        .Col = 1
        If .Text = "" Then
            MsgBox "Debe especificar marca del repuesto"
            Cancel = 1
        End If
        .Col = 4
        VCPrecioCpra = Val(.Text)
        If VCPrecioCpra = 0 Then .Text = 1
        .Col = 6
        VCPrecioVta = Val(.Text)
        If VCPrecioVta = 0 Then .Text = 1
        
        
        If VCPrecioVta = VCPrecioCpra Then
            .Col = 5 'Porcentaje
            .Text = 0 'asigna 0
            .Col = 7 'Magen
            .Text = 0 ' asigna 0
        End If
            
        .Col = 8
        If Val(.Text) = 0 Then .Text = 0
        .Col = 9
        If Val(.Text) = 0 Then .Text = 0
    End With
End Sub

Private Sub GridProductos_ButtonClick(ByVal ColIndex As Integer)

If ColIndex = 1 Then
   
    
    With GridProductos    'evita escribir gridproductos.etc ej. .top
        CboMarca.Top = .Top + .RowTop(.Row) '+ .RowHeight
          '.Left = .Left + .Columns(ColIndex).Left
          ' Width and Height properties can be set a design time
          ' The width of the list does not have to be the same as the width of the grid column
          'CboMarca.Width = .Columns(1).Width
         ' CboMarca.Height = 1440
          CboMarca.Visible = Not CboMarca.Visible
          If CboMarca.Visible Then
             'CboMarca.Text = .Text
             CboMarca.ZOrder      ' make sure the list is on top of the grid
          End If
    
    End With
End If
End Sub

Private Sub GridProductos_Click()
    CboMarca.Visible = False
End Sub


Private Sub CboMarca_Click()
  Me.GridProductos.Text = CboMarca.Text
  CboMarca.Visible = False
End Sub

Private Sub CboMarca_LostFocus()
  CboMarca.Visible = False
End Sub



Private Sub GridProductos_KeyPress(KeyAscii As Integer)
    Dim letra As String    'Aqui controlamos las teclas
    With Me.GridProductos  'Presionadas en el GridProductos
        If .Col = 2 Or .Col = 3 Then
            letra = UCase(Chr(KeyAscii)) 'a mayuscula el caractere ingresado
            KeyAscii = Asc(letra) 'recupero el codigo ascci del caractar ya transofrmado
        ElseIf .Col = 4 Or .Col = 5 Or .Col = 6 Or .Col = 8 Or .Col = 9 Then
            
            If IsNumeric(Chr(KeyAscii)) Then
                'pasa
            Else
                If KeyAscii = 8 Then Exit Sub ' acepta retorceso
                
                'If KeyAscii = 13 Then
                '    SendKeys "{TAB}" 'envia un tab
                '    Exit Sub '8=retroceso y 13 =enter
                'End If
                
                KeyAscii = 0 'No acepta el caracter ingresado
                
            End If
            
        ElseIf .Col = 7 Then KeyAscii = 0 'Columna 7 inhabilitada
        
        
        End If
    End With
    
    
End Sub

Private Sub GridProductos_Scroll(Cancel As Integer)
  CboMarca.Visible = False
End Sub


