VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{28D47522-CF84-11D1-834C-00A0249F0C28}#1.0#0"; "gif89.dll"
Object = "{248DD890-BB45-11CF-9ABC-0080C7E7B78D}#1.0#0"; "MSWINSCK.OCX"
Begin VB.Form Principal 
   ClientHeight    =   10440
   ClientLeft      =   2865
   ClientTop       =   345
   ClientWidth     =   12825
   LinkTopic       =   "Form1"
   ScaleHeight     =   10440
   ScaleWidth      =   12825
   Visible         =   0   'False
   WhatsThisButton =   -1  'True
   WhatsThisHelp   =   -1  'True
   Begin VB.PictureBox PicDicoem 
      BorderStyle     =   0  'None
      Height          =   795
      Left            =   255
      Picture         =   "Principal.frx":0000
      ScaleHeight     =   795
      ScaleWidth      =   5970
      TabIndex        =   37
      Top             =   195
      Width           =   5970
   End
   Begin VB.PictureBox PicRedic 
      AutoSize        =   -1  'True
      BorderStyle     =   0  'None
      Height          =   1245
      Left            =   855
      Picture         =   "Principal.frx":2E92
      ScaleHeight     =   1245
      ScaleWidth      =   4350
      TabIndex        =   43
      Top             =   -15
      Visible         =   0   'False
      Width           =   4350
   End
   Begin VB.Frame FrmLoad 
      Height          =   1950
      Left            =   4545
      TabIndex        =   44
      Top             =   3660
      Visible         =   0   'False
      Width           =   2805
      Begin GIF89LibCtl.Gif89a Gif89a1 
         Height          =   1425
         Left            =   210
         OleObjectBlob   =   "Principal.frx":7A1A
         TabIndex        =   45
         Top             =   285
         Width           =   2445
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Sucursal Empresa"
      Height          =   1095
      Left            =   6705
      TabIndex        =   40
      Top             =   75
      Width           =   3870
      Begin VB.CommandButton CmdSeleccionaEmpresa 
         Caption         =   "Cambiar a Empresa Seleccinada"
         Enabled         =   0   'False
         Height          =   285
         Left            =   150
         TabIndex        =   42
         Top             =   750
         Width           =   3585
      End
      Begin VB.ComboBox CboSeleccionaSucursal 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         ItemData        =   "Principal.frx":7AA0
         Left            =   105
         List            =   "Principal.frx":7AA2
         Style           =   2  'Dropdown List
         TabIndex        =   41
         Top             =   390
         Width           =   3645
      End
   End
   Begin VB.TextBox skEmpresa 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Gabriola"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   405
      Left            =   1275
      Locked          =   -1  'True
      TabIndex        =   39
      TabStop         =   0   'False
      Text            =   "Text1"
      Top             =   495
      Width           =   2880
   End
   Begin VB.TextBox SkRut 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Arial Narrow"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   330
      Left            =   960
      Locked          =   -1  'True
      TabIndex        =   38
      TabStop         =   0   'False
      Text            =   "Text1"
      Top             =   495
      Visible         =   0   'False
      Width           =   1440
   End
   Begin VB.CommandButton Command3 
      Caption         =   "Command3"
      Height          =   300
      Left            =   30
      TabIndex        =   36
      Top             =   2805
      Visible         =   0   'False
      Width           =   390
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Command2"
      Height          =   780
      Left            =   135
      TabIndex        =   35
      Top             =   1335
      Visible         =   0   'False
      Width           =   675
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Command1"
      Height          =   435
      Left            =   30
      TabIndex        =   34
      Top             =   9705
      Visible         =   0   'False
      Width           =   300
   End
   Begin VB.TextBox TxtImpresionDirecta 
      Height          =   285
      Left            =   150
      TabIndex        =   33
      Text            =   "NO"
      Top             =   6840
      Visible         =   0   'False
      Width           =   2400
   End
   Begin VB.CommandButton CmdMenu 
      Caption         =   "Contabilidad"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   600
      Index           =   8
      Left            =   1065
      TabIndex        =   25
      Tag             =   "200"
      Top             =   7500
      Width           =   4000
   End
   Begin VB.PictureBox foto 
      Height          =   330
      Left            =   495
      ScaleHeight     =   270
      ScaleWidth      =   11415
      TabIndex        =   23
      Top             =   10785
      Visible         =   0   'False
      Width           =   11475
   End
   Begin VB.Frame Frame2 
      Caption         =   "Periodo Contable"
      Height          =   1095
      Left            =   10665
      TabIndex        =   17
      Top             =   225
      Width           =   1845
      Begin VB.TextBox CboAno 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFC0&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   360
         Left            =   60
         Locked          =   -1  'True
         TabIndex        =   19
         TabStop         =   0   'False
         Text            =   "Text1"
         Top             =   255
         Width           =   1695
      End
      Begin VB.TextBox CboMes 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFC0&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   360
         Left            =   60
         Locked          =   -1  'True
         TabIndex        =   18
         TabStop         =   0   'False
         Top             =   690
         Width           =   1695
      End
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
      Height          =   435
      Left            =   90
      OleObjectBlob   =   "Principal.frx":7AA4
      TabIndex        =   16
      Top             =   7065
      Visible         =   0   'False
      Width           =   1185
   End
   Begin VB.PictureBox Picture4 
      Height          =   30
      Left            =   60
      ScaleHeight     =   30
      ScaleWidth      =   105
      TabIndex        =   7
      Top             =   1590
      Width           =   105
   End
   Begin MSWinsockLib.Winsock WinsocK 
      Left            =   225
      Top             =   8340
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   393216
   End
   Begin VB.Frame Frame4 
      Caption         =   "Empresa Activa"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   1185
      Left            =   9465
      TabIndex        =   6
      Top             =   120
      Visible         =   0   'False
      Width           =   10305
      Begin ACTIVESKINLibCtl.SkinLabel Skconsininventario 
         Height          =   180
         Left            =   2340
         OleObjectBlob   =   "Principal.frx":7B1E
         TabIndex        =   20
         Top             =   900
         Visible         =   0   'False
         Width           =   4530
      End
   End
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Interval        =   400
      Left            =   -30
      Top             =   2115
   End
   Begin VB.Frame Frame3 
      Caption         =   "Informacion de usuario"
      Height          =   795
      Left            =   375
      TabIndex        =   1
      Top             =   9480
      Width           =   10485
      Begin VB.TextBox TxtSucursal 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         Height          =   345
         Left            =   7230
         Locked          =   -1  'True
         TabIndex        =   29
         TabStop         =   0   'False
         Text            =   "Text1"
         Top             =   405
         Width           =   3180
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   180
         Left            =   240
         OleObjectBlob   =   "Principal.frx":7B98
         TabIndex        =   26
         Top             =   225
         Width           =   1650
      End
      Begin VB.TextBox TxtServidor 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         Height          =   345
         Left            =   4785
         Locked          =   -1  'True
         TabIndex        =   24
         TabStop         =   0   'False
         Text            =   "Text1"
         Top             =   405
         Width           =   2400
      End
      Begin VB.TextBox TxtPerfil 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         Height          =   345
         Left            =   2610
         Locked          =   -1  'True
         TabIndex        =   15
         TabStop         =   0   'False
         Text            =   "Text1"
         Top             =   405
         Width           =   2115
      End
      Begin VB.TextBox txtNombre 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         Height          =   345
         Left            =   225
         Locked          =   -1  'True
         TabIndex        =   14
         TabStop         =   0   'False
         Text            =   "Text1"
         Top             =   405
         Width           =   2355
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   180
         Left            =   2610
         OleObjectBlob   =   "Principal.frx":7C02
         TabIndex        =   27
         Top             =   225
         Width           =   1650
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkSrv 
         Height          =   180
         Left            =   4800
         OleObjectBlob   =   "Principal.frx":7C6C
         TabIndex        =   28
         Top             =   225
         Width           =   1650
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkSucursal 
         Height          =   180
         Left            =   7245
         OleObjectBlob   =   "Principal.frx":7CD0
         TabIndex        =   30
         Top             =   225
         Width           =   1650
      End
   End
   Begin VB.CommandButton cmdSalir 
      Cancel          =   -1  'True
      Caption         =   "&Salir"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   10905
      TabIndex        =   0
      Top             =   9735
      Width           =   1380
   End
   Begin VB.Frame Principal 
      Caption         =   "              Menu Principal        "
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   8235
      Left            =   345
      TabIndex        =   8
      Top             =   1305
      Width           =   11925
      Begin VB.CommandButton CmdMenu 
         Caption         =   "&Facturación Electrónica"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   600
         Index           =   10
         Left            =   690
         TabIndex        =   32
         Tag             =   "11"
         ToolTipText     =   "Gestion de facturacion electronica"
         Top             =   6645
         Width           =   4000
      End
      Begin VB.CommandButton CmdMenu 
         Caption         =   "Caja"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   600
         Index           =   9
         Left            =   690
         TabIndex        =   31
         Tag             =   "500"
         Top             =   4650
         Width           =   4000
      End
      Begin VB.PictureBox Picture2 
         Height          =   45
         Left            =   15
         ScaleHeight     =   45
         ScaleWidth      =   15
         TabIndex        =   22
         Top             =   105
         Width           =   15
      End
      Begin VB.CommandButton CmdMenu 
         Caption         =   " Bancos"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   600
         Index           =   7
         Left            =   690
         TabIndex        =   21
         Tag             =   "100"
         Top             =   5325
         Width           =   4000
      End
      Begin VB.Frame FrmSub 
         Caption         =   "Sub-Menu"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   7680
         Left            =   5385
         TabIndex        =   11
         Top             =   240
         Width           =   5715
         Begin VB.CommandButton cmdSub 
            Caption         =   "Command6"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   525
            Index           =   0
            Left            =   300
            TabIndex        =   12
            Top             =   -165
            Visible         =   0   'False
            Width           =   5100
         End
      End
      Begin VB.CommandButton CmdMenu 
         Caption         =   "&Compras"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   600
         Index           =   0
         Left            =   675
         TabIndex        =   2
         Tag             =   "1"
         Top             =   495
         Width           =   4000
      End
      Begin VB.CommandButton CmdMenu 
         Caption         =   "&Inventarios"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   600
         Index           =   2
         Left            =   675
         TabIndex        =   4
         Tag             =   "3"
         Top             =   1890
         Width           =   4000
      End
      Begin VB.CommandButton CmdMenu 
         Caption         =   "Tesoreria"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   600
         Index           =   3
         Left            =   690
         TabIndex        =   5
         Tag             =   "4"
         Top             =   2580
         Width           =   4000
      End
      Begin VB.CommandButton CmdMenu 
         Caption         =   "I&nformes"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   600
         Index           =   4
         Left            =   705
         TabIndex        =   13
         Tag             =   "5"
         Top             =   3285
         Width           =   4000
      End
      Begin VB.CommandButton CmdMenu 
         Caption         =   "C&onfiguración"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   600
         Index           =   6
         Left            =   690
         TabIndex        =   10
         Tag             =   "15"
         Top             =   7320
         Width           =   4000
      End
      Begin VB.CommandButton CmdMenu 
         Caption         =   "&Mantenedores"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   600
         Index           =   5
         Left            =   705
         TabIndex        =   9
         Tag             =   "41"
         Top             =   3990
         Width           =   4000
      End
      Begin VB.CommandButton CmdMenu 
         Caption         =   "&Ventas"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   600
         Index           =   1
         Left            =   690
         TabIndex        =   3
         Tag             =   "2"
         Top             =   1185
         Width           =   4000
      End
   End
End
Attribute VB_Name = "Principal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim Formulario As Form
'Declaración de la API
Private Declare Function GetUserName Lib "advapi32.dll" Alias "GetUserNameA" _
(ByVal lpBuffer As String, nSize As Long) As Long
Function Get_User_Name() As String
'creamos variables
Dim lpBuff As String * 25
Dim ret As Long
Dim UserName As String
'Obtenemos el nombre de la api.
ret = GetUserName(lpBuff, 25)
UserName = Left(lpBuff, InStr(lpBuff, Chr(0)) - 1)
' Devolvemos el nombre de usuario
Get_User_Name = UserName
End Function

'Private Type ElMenu
'    men_id As Integer
'    men_padre As String
'    men_nombre As String
'    men_toolstips As String
'    per_nombre As String
'End Type
'Dim Sm_Botones() As ElMenu
Private Sub CmdMenu_Click(Index As Integer)
    
    If cmdSub.Count > 2 Then
        For i = cmdSub.Count - 1 To 1 Step -1
            Unload cmdSub(i)
        Next
    ElseIf cmdSub.Count = 2 Then
        Unload cmdSub(1)
    End If
              
    Sql = "SELECT  m.men_id, men_padre,  men_nombre, men_tooltips,   p.per_nombre " & _
          "FROM    sis_menu m INNER JOIN (sis_accesos a INNER JOIN sis_perfiles p ON a.per_id=p.per_id) ON a.men_id=m.men_id " & _
          "WHERE   men_padre=" & CmdMenu(Index).Tag & " AND a.per_id=" & LogPerfil & " AND men_activo = 'SI' " & _
          "ORDER BY men_orden"
          
    Consulta RsTmp, Sql
    
    
    FrmSub.Caption = "Sub-Menu de " & CmdMenu(Index).Caption
    If RsTmp.RecordCount > 0 Then
        
        With RsTmp
            
            i = 1
            .MoveFirst
            Do While Not .EOF
                
                        
                 Load cmdSub(i)
                 cmdSub(i).Caption = !men_nombre
                 cmdSub(i).Height = cmdSub(0).Height
                 cmdSub(i).Top = cmdSub(i - 1).Top + cmdSub(0).Height + 70
                 cmdSub(i).ToolTipText = !men_tooltips
                 cmdSub(i).Visible = True
                 cmdSub(i).Tag = !men_id
                 
                i = i + 1
                .MoveNext
            Loop
        End With
    End If
    
    Aplicar_skin Me
End Sub

Private Sub CmdMenu_MouseMove(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
'    If SkinLabel1.Tag = Index Then Exit Sub
'    CmdMenu_Click (Index)
'    CmdMenu(Index).SetFocus
'    SkinLabel1.Tag = Index
End Sub

Private Sub CmdSalir_Click()
    End
End Sub

Private Sub CmdSeleccionaEmpresa_Click()
    If CboSeleccionaSucursal.ListIndex = -1 Then Exit Sub
    FrmLoad.Visible = True
    DoEvents
    PicDicoem.Visible = False
    PicRedic.Visible = False
    If CboSeleccionaSucursal.ListIndex = 0 Then
        LogSueID = 1
        PicDicoem.Visible = True
        SP_Rut_Activo = "76.046.995-5"
        Me.SkRut = SP_Rut_Activo
        SG_Sucursal_Activa = "TEMUCO"
        ' MsgBox "UD Ahora tienen activa la empresa DICOEM - CASA MATRIZ ...", vbInformation
         
    End If
    
    If CboSeleccionaSucursal.ListIndex = 1 Then
        LogSueID = 2
        SP_Rut_Activo = "76.046.995-5"
        PicDicoem.Visible = True
        Me.SkRut = SP_Rut_Activo
        SG_Sucursal_Activa = "TEMUCO"
       '  MsgBox "UD Ahora tienen activa la empresa DICOEM - VALDIVIA ...", vbInformation
        
    End If
    
    If CboSeleccionaSucursal.ListIndex = 2 Then
            'Redic
            SG_BaseDato = "alvaro_redic"
            SG_Sucursal_Activa = "TALCA"
            FrmLoad.Visible = True
            DoEvents
            Sql = "SELECT rut_emp " & _
                    "FROM sis_empresas_sucursales " & _
                    "WHERE sue_id=3"
            SP_Rut_Activo = "76.115.474-5"
            Me.SkRut = SP_Rut_Activo
            LogSueID = 1
            PicRedic.Visible = True
            Sql = "SELECT par_valor " & _
                    ""
            
            

    End If
    Main
    'ConexionBD
    FrmLoad.Visible = False
    MsgBox "UD Ahora tienen activa la empresa  ..." & vbNewLine & Me.CboSeleccionaSucursal.Text, vbInformation
    If CboSeleccionaSucursal.ListIndex = 3 Then LogSueID = 3
    
    If CboSeleccionaSucursal.ListIndex = 4 Then LogSueID = 0
    
    TxtSucursal = CboSeleccionaSucursal
End Sub

Private Sub cmdSub_Click(Index As Integer)
    Dim Sp_extra As String
    DG_ID_Unico = 0
    Sql = "SELECT men_id,men_nombre,men_formulario,men_infoextra " & _
          "FROM sis_menu " & _
          "WHERE men_id=" & cmdSub(Index).Tag & " AND men_activo='SI'"
    Call Consulta(RsTmp, Sql)
    If RsTmp.RecordCount > 0 Then
        If IsNull(RsTmp!men_formulario) Or Len(RsTmp!men_formulario) = 0 Then Exit Sub
       ' Me.Hide
        
        IP_IDMenu = RsTmp!men_id
        
        If Mid(RsTmp!men_formulario, 1, 4) = "man_" Then
            'Aqui llamamos a un mantenedor
            Sql = "SELECT mav_id,mav_nombre,mav_activo,mav_tabla,mav_consulta,mav_caption,mav_caption_frm,man_editable,man_rut_empresa " & _
                  "FROM sis_mantenedor_simple " & _
                  "WHERE man_activo='SI' AND man_id=" & Mid(RsTmp!men_formulario, 5)
            Consulta RsTmp2, Sql
            If RsTmp2.RecordCount > 0 Then
                With Mantenedor_Simple
                    .S_Id = RsTmp2!mav_id
                    .S_Nombre = RsTmp2!mav_nombre
                    .S_Activo = RsTmp2!mav_activo
                    .S_tabla = RsTmp2!mav_tabla
                    .S_Consulta = RsTmp2!mav_consulta
                    .S_RutEmpresa = RsTmp2!man_rut_empresa
                    .Caption = RsTmp2!mav_caption
                    .FrmMantenedor.Caption = RsTmp2!mav_caption_frm
                    .B_Editable = IIf(RsTmp2!man_editable = "SI", True, False)
                    foto.Visible = True
                   ' Picture1.Visible = True
                    .Show
                End With
            End If
        ElseIf Mid(RsTmp!men_formulario, 1, 4) = "mod_" Then
            Shell App.Path & "\" & RsTmp!men_infoextra & " " & SG_BaseDato & "|" & Sp_Servidor & "|" & Mid(RsTmp!men_formulario, 5) & "|" & SP_Rut_Activo & "|SI|" & SP_Nombre_Equipo & "|" & LG_id_Caja & "|" & IG_id_Sucursal_Empresa & "|&H80FF80|&H80000014|&HE0E0E0|" & LogUsuario & "|" & LogPerfil & "|" & PwdDb & "|" & LogIdUsuario & "|" & UserDb & "|" & IG_Nro_Caja, vbNormalFocus
            'db_mafil|localhost|VenPos|76.354.346-3|SI|SoftDev-pc|111|1|&H80FF80| &H80000014|&HE0E0E0|demoledor|1
           ' db_talentos_fusion|127.0.0.1|VtaListado|76.039.757-1|SI|SoftDev-pc
        
        ElseIf Mid(RsTmp!men_formulario, 1, 7) = "mandep_" Then
                'Aqui llamamos a un mantenedor con dependencia
            Sql = "SELECT mav_id,mav_nombre,mav_activo,mav_tabla,mav_consulta,mav_caption,mav_caption_frm,man_editable, " & _
                        "man_dep_id,man_dep_nombre,man_dep_tabla,man_dep_activo,man_dep_titulo " & _
                  "FROM sis_mantenedor_simple " & _
                  "WHERE man_activo='SI' AND man_id=" & Mid(RsTmp!men_formulario, 8)
            Consulta RsTmp2, Sql
            If RsTmp2.RecordCount > 0 Then
                With Mantenedor_Dependencia
                    .S_Id = RsTmp2!mav_id
                    .S_Nombre = RsTmp2!mav_nombre
                    .S_Activo = RsTmp2!mav_activo
                    .S_tabla = RsTmp2!mav_tabla
                    .S_Consulta = RsTmp2!mav_consulta
                    .B_Editable = IIf(RsTmp2!man_editable = "SI", True, False)
                    .S_id_dep = RsTmp2!man_dep_id
                    .S_nombre_dep = RsTmp2!man_dep_nombre
                    .S_Activo_dep = RsTmp2!man_dep_activo
                    .S_tabla_dep = RsTmp2!man_dep_tabla
                    .S_titulo_dep = RsTmp2!man_dep_titulo
'                    .Caption = RsTmp2!mav_caption
'                    .FrmMantenedor.Caption = RsTmp2!mav_caption_frm
                   foto.Visible = True
    '              Picture1.Visible = True
                    .Show 1
                End With
            End If
        
        Else
            'Cargamos el formulario
          
          If RsTmp!men_id = 2200 Then
                    Me.Enabled = False ' blue market por espera con conexion con impresora
           End If
          '  Me.Enabled = False
            Sp_extra = RsTmp!men_infoextra
            
            Me.Hide
            Set Formulario = Forms.Add(RsTmp!men_formulario)
          '  foto.Visible = True
          '  Picture1.Visible = True
          
            Formulario.Visible = True
            Formulario.Show
           
               
                
                
            
             Me.Enabled = True
            bm_NoTimer = False
             Formulario.SetFocus
             Me.Show
        End If
    Else
        MsgBox "Acceso no autorizado", vbInformation
    
    End If
    Me.Show
    foto.Visible = False
   ' Picture1.Visible = False
End Sub




Private Sub Command1_Click()
    xml_cesiones.Show
End Sub

Private Sub Command2_Click()
    
    Form1.Show 1

    ''Informe_historial_arr_proveedor.Show 1
'        dte_contratos.Show 1
End Sub

Private Sub Command3_Click()
    Sql = "CALL Cmb_Rp_DocVenta(2)"
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        MsgBox RsTmp!NC
    End If
    
End Sub

Private Sub Form_Load()
   ' Centrar Me
    SkinLabel1.Tag = 0
    Aplicar_skin Me
    ClrCfoco = &H80FF80
    ClrSfoco = &H80000014
    ClrDesha = &HE0E0E0
   'MENSAJE DE ALERTA POR ATRASOS EN LOS PAGOS DE ARRIENDO DE SISTEMA
' If Format(Date, "dd/mm/yy") = "20/05/13" Then MsgBox "Existen facturas pendientes de pago. ..." & vbNewLine & " Favor solucionar con VALVER-SOFT....", vbExclamation
    TxtServidor = Sp_Servidor & " " & SG_BaseDato
    SkRut = SP_Rut_Activo
    SkEmpresa = SP_Empresa_Activa
    Skconsininventario = SP_Control_Inventario
    Me.Enabled = False
    
   
    SP_Nombre_Equipo = WinsocK.LocalHostName
    
   
  
   
   
    SG_Oficina_Contable = "NO"
    IG_id_Sucursal_Empresa = 1
    Sql = "SELECT rut,ano_contable,mes_contable,ema_id_empresa emp_id,sue_id,ema_oficina_contable,ema_solo_nota_de_venta " & _
          "FROM sis_empresa_activa " & _
          "WHERE UPPER(ema_nombre_equipo)='" & UCase(SP_Nombre_Equipo) & "'"
    Consulta RsTmp2, Sql
    If RsTmp2.RecordCount > 0 Then
    
        SG_Equipo_Solo_Nota_de_Venta = "SI" ' RsTmp2!ema_solo_nota_de_venta
        SG_Oficina_Contable = RsTmp2!ema_oficina_contable
        CboMes = UCase(MonthName(RsTmp2!mes_contable))
        CboAno = RsTmp2!ano_contable
        If RsTmp2!sue_id = 0 Then
            IG_id_Sucursal_Empresa = 1
        Else
            IG_id_Sucursal_Empresa = RsTmp2!sue_id
        End If
         
        IG_Ano_Contable = RsTmp2!ano_contable
        IG_Mes_Contable = RsTmp2!mes_contable
        
        
        Sql = "SELECT rut,nombre_empresa,control_inventario,emp_id,emp_modulo_caja,emp_impresion_directa_documentos impresion /*,emp_busquedas_por_codigo_interno codinterno */,emp_utiliza_impresora_termica_dte termicadte " & _
              "FROM sis_empresas " & _
              "WHERE emp_id='" & RsTmp2!emp_id & "'"
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
            '/* SG_Busqueda_codigo_Interno = RsTmp!codinterno */
            IG_id_Empresa = RsTmp!emp_id
            SP_Rut_Activo = RsTmp!Rut
            SP_Empresa_Activa = RsTmp!nombre_empresa
            SP_Control_Inventario = RsTmp!control_inventario
            SkRut = SP_Rut_Activo
            SkEmpresa = SP_Empresa_Activa
            Skconsininventario = "CONTROL DE EXISTENCIAS: " & SP_Control_Inventario
            SG_Modulo_Caja = RsTmp!emp_modulo_caja
            Me.TxtImpresionDirecta = RsTmp!impresion
            SG_UtilizaImpresoraTermicaDTE = RsTmp!termicadte
            
        End If
    Else
        
        Sql = "SELECT emp_id,emp_modulo_caja " & _
                "FROM sis_empresas " & _
                "WHERE rut='" & SP_Rut_Activo & "'"
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
            IG_id_Empresa = RsTmp!emp_id
            SG_Modulo_Caja = RsTmp!emp_modulo_caja
        End If
        Sql = "INSERT INTO sis_empresa_activa (ema_nombre_equipo,rut,ano_contable,mes_contable,ema_id_empresa) " & _
              "VALUES('" & SP_Nombre_Equipo & "','" & SP_Rut_Activo & "'," & Year(Date) & "," & Month(Date) & "," & IG_id_Empresa & ")"
        cn.Execute Sql
        IG_Ano_Contable = Year(Date)
        IG_Mes_Contable = Month(Date)
        CboMes = UCase(MonthName(IG_Mes_Contable))
        CboAno = IG_Ano_Contable
        
    End If
    
    
    If IG_id_Sucursal_Empresa = 1 Then
        TxtSucursal = "CASA MATRIZ"
    Else
        Sql = "SELECT CONCAT(sue_direccion,'-',sue_ciudad) sucursal,sue_fiscal,lst_id " & _
                "FROM sis_empresas_sucursales " & _
                "WHERE sue_id=" & IG_id_Sucursal_Empresa
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
            TxtSucursal = RsTmp!Sucursal
            SG_Marca_Impresora_Fiscal = RsTmp!sue_fiscal
             SkSucursal.Tag = RsTmp!lst_id
        End If
    
    End If
    

    
    Sql = "SELECT men_id,men_activo " & _
          "FROM sis_menu " & _
          "WHERE men_padre=0"
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        RsTmp.MoveFirst
        Do While Not RsTmp.EOF
            For i = 1 To CmdMenu.Count - 1
                If CmdMenu(i).Tag = RsTmp!men_id Then
                    If RsTmp!men_activo = "SI" Then CmdMenu(i).Visible = True Else CmdMenu(i).Visible = False
                    Exit For
                End If
            Next
            RsTmp.MoveNext
        Loop
    End If
 

    Centrar Me

    CmdMenu(9).Visible = False
    '26 Octubre 2013
    LG_id_Caja = 0
    SG_Modulo_Caja = "NO"
    If SG_Modulo_Caja = "SI" Then
        'PARA QUE NO AFECTE LA CAJA
        If LogUsuario = "VENTASANTERIORES" Then GoTo final
        
    
        CmdMenu(9).Visible = True
        Sql = "SELECT caj_id,c.sue_id,caj_fecha_apertura,caj_estado,caj_monto_inicial,usu_nombre " & _
                "FROM ven_caja c " & _
                "JOIN sis_usuarios u ON c.usu_id=u.usu_id " & _
                "WHERE emp_id=" & IG_id_Empresa & " AND c.sue_id=" & IG_id_Sucursal_Empresa & " AND caj_estado='ABIERTA'"
        
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
            'Identificamos la caja abierta acualmente
            LG_id_Caja = RsTmp!caj_id
            caj_apertura.txtUsuario = RsTmp!usu_nombre
            caj_apertura.TxtSucursal = TxtSucursal
            caj_apertura.txtEfectivoInicial = NumFormat(RsTmp!caj_monto_inicial)
            caj_apertura.DtFecha = RsTmp!caj_fecha_apertura
            caj_apertura.DtFecha.Enabled = False
            
        Else
            'Debemos abrir una caja
            caj_apertura.txtUsuario = LogUsuario
            caj_apertura.TxtSucursal = TxtSucursal
            caj_apertura.txtEfectivoInicial = 0
            caj_apertura.DtFecha = Date
            
            '2-12-2015 _
            Debemos obtener el saldo de la caja anterior para que sea el saldo de la nueva caja.
            Sql = "SELECT caj_saldo " & _
                "FROM ven_caja c " & _
                "JOIN sis_usuarios u ON c.usu_id=u.usu_id " & _
                "WHERE emp_id=" & IG_id_Empresa & " AND sue_id=" & IG_id_Sucursal_Empresa & " AND caj_estado='CERRADA' " & _
                "ORDER BY caj_id DESC"
            Consulta RsTmp, Sql
            If RsTmp.RecordCount > 0 Then
                caj_apertura.txtEfectivoInicial = RsTmp!caj_saldo
            End If
        
        End If
        caj_apertura.Show 1
    End If
    
final:
If BG_ModoProductivoDTE Then
    Me.SkSrv = "Productivo"
Else
    Me.SkSrv = "Certificacion"
End If
   
 
  CboSeleccionaSucursal.AddItem "DICOEM - TEMUCO"
  CboSeleccionaSucursal.AddItem "DICOEM - VALDIVIA"
  CboSeleccionaSucursal.AddItem "REDIC  - TALCA"
 ' CboSeleccionaSucursal.AddItem "DICOEM - TEMUCO-VALDIVIA"
  
 ' CboSeleccionaSucursal.AddItem "TODOS"
  CboSeleccionaSucursal.Locked = True

  If LogSueID = 1 Then CboSeleccionaSucursal.ListIndex = 0
  If LogSueID = 2 Then CboSeleccionaSucursal.ListIndex = 1
  If LogSueID = 3 Then CboSeleccionaSucursal.ListIndex = 2
  
  
  If SG_Sucursal_Activa = "TALCA" Then
     'Redic
            SG_BaseDato = "alvaro_redic"
            SG_Sucursal_Activa = "TALCA"
            FrmLoad.Visible = True
            DoEvents
            Sql = "SELECT rut_emp " & _
                    "FROM sis_empresas_sucursales " & _
                    "WHERE sue_id=3"
            SP_Rut_Activo = "76.115.474-5"
            Me.SkRut = SP_Rut_Activo
            LogSueID = 1
            PicDicoem.Visible = False
            PicRedic.Visible = True
            CboSeleccionaSucursal.ListIndex = 2
            Sql = "SELECT par_valor " & _
                    ""
    End If
  
  If UsuarioGerencia Then
        Me.CmdSeleccionaEmpresa.Enabled = True
        CboSeleccionaSucursal.Locked = False
   End If
   Me.Enabled = True
   FrmLoad.Visible = False
End Sub
Private Sub Form_Unload(Cancel As Integer)
    End
End Sub


Private Sub Timer1_Timer()
  '  CmdMenu_Click (0)
  '  SkinLabel1.Tag = 0
  '  Timer1.Enabled = False
    
End Sub
Function IsFormLoaded(FormToCheck As Form) As Integer
    Dim Y As Integer
    
    For Y = 0 To Forms.Count - 1
    If Forms(Y) Is FormToCheck Then
        IsFormLoaded = True
        Exit Function
        End If
    Next
    IsFormLoaded = False
End Function
Public Function FormularioActivo(NmbFormulario As String) As Boolean
Dim Formulario As Form
For Each Formulario In Forms
If (UCase(Formulario.Name) = UCase(NmbFormulario)) Then
FormularioActivo = True
Exit For
End If
Next
End Function
