VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Begin VB.Form ctacte_detalle_pago 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Form1"
   ClientHeight    =   3015
   ClientLeft      =   3720
   ClientTop       =   3990
   ClientWidth     =   5565
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3015
   ScaleWidth      =   5565
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton cmdSalir 
      Cancel          =   -1  'True
      Caption         =   "&Retornar"
      Height          =   360
      Left            =   3675
      TabIndex        =   3
      Top             =   2430
      Width           =   1560
   End
   Begin VB.Frame Frame1 
      Caption         =   "Detalle"
      Height          =   1665
      Left            =   765
      TabIndex        =   4
      Top             =   540
      Width           =   4290
      Begin VB.ComboBox CboCuenta 
         Appearance      =   0  'Flat
         Height          =   315
         ItemData        =   "ctacte_detalle_pago.frx":0000
         Left            =   240
         List            =   "ctacte_detalle_pago.frx":0002
         Style           =   2  'Dropdown List
         TabIndex        =   8
         Top             =   345
         Visible         =   0   'False
         Width           =   3240
      End
      Begin VB.CommandButton cmdSigue 
         Caption         =   "&Continuar"
         Height          =   360
         Left            =   225
         TabIndex        =   2
         Top             =   1245
         Width           =   1560
      End
      Begin VB.ComboBox CboTipoDeposito 
         Height          =   315
         ItemData        =   "ctacte_detalle_pago.frx":0004
         Left            =   2280
         List            =   "ctacte_detalle_pago.frx":000E
         Style           =   2  'Dropdown List
         TabIndex        =   1
         Top             =   885
         Width           =   1185
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   180
         Left            =   225
         OleObjectBlob   =   "ctacte_detalle_pago.frx":0024
         TabIndex        =   5
         Top             =   675
         Width           =   1650
      End
      Begin VB.TextBox TxtNroTransaccion 
         Height          =   285
         Left            =   225
         TabIndex        =   0
         Top             =   885
         Width           =   2070
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkDeposito 
         Height          =   210
         Left            =   2265
         OleObjectBlob   =   "ctacte_detalle_pago.frx":00A0
         TabIndex        =   6
         Top             =   675
         Width           =   1155
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkCuenta 
         Height          =   180
         Left            =   1320
         OleObjectBlob   =   "ctacte_detalle_pago.frx":0118
         TabIndex        =   7
         Top             =   165
         Visible         =   0   'False
         Width           =   1650
      End
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   660
      OleObjectBlob   =   "ctacte_detalle_pago.frx":0186
      Top             =   1230
   End
   Begin VB.Timer Timer1 
      Interval        =   10
      Left            =   105
      Top             =   1080
   End
End
Attribute VB_Name = "ctacte_detalle_pago"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public Sm_Tipo As String
Dim Sm_SistemaBanco As String
Private Sub cmdSalir_Click()
    
    Unload Me
End Sub

Private Sub cmdSigue_Click()
    If Val(TxtNroTransaccion) = 0 Then
        MsgBox "Falta nro de transacción..."
        TxtNroTransaccion.SetFocus
        Exit Sub
    End If
    If Sm_SistemaBanco = "SI" Then
        If CboCuenta.ListIndex < 0 Then
            MsgBox "No ha seleccionado Cta Cte para el movimiento...", vbInformation
            Exit Sub
        End If
        SG_codigo3 = CboCuenta.ItemData(CboCuenta.ListIndex)
        
    End If
    
    
    SG_codigo = TxtNroTransaccion
    
    
    
    If Sm_Tipo = "TRANSFERENCIA" Then
        SG_codigo2 = Empty
    Else
        SG_codigo2 = CboTipoDeposito.Text
    End If
    
    
    
    
    Unload Me
    
End Sub

Private Sub Form_Load()
    Centrar Me
    Aplicar_skin Me
    SG_codigo = Empty
    SG_codigo2 = Empty
    Me.CboTipoDeposito.ListIndex = 0
    If Sm_Tipo = "TRANSFERENCIA" Then
        Me.Caption = "DATOS TRANSFERENCIA"
        Me.SkDeposito.Visible = False
        Me.CboTipoDeposito.Visible = False
    Else
        Me.Caption = "DATOS DEPOSITOS"
    End If
    
    Sm_SistemaBanco = "NO"
    
    Sql = "SELECT emp_modulo_banco " & _
          "FROM sis_empresas " & _
          "WHERE rut='" & SP_Rut_Activo & "'"
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        If RsTmp!emp_modulo_banco = "SI" Then
            Sm_SistemaBanco = "SI"
            CboCuenta.Visible = True
            LLenarCombo CboCuenta, "CONCAT(cte_numero,' ',pla_nombre)", "cte_id", "ban_cta_cte c INNER JOIN con_plan_de_cuentas z ON z.pla_id=c.ban_id", "cte_activo='SI' AND rut_emp='" & SP_Rut_Activo & "'"
            If CboCuenta.ListCount > 0 Then
                CboCuenta.ListIndex = 0
            Else
            
            
            End If
        End If
        
    End If
          
    
End Sub
Private Sub Timer1_Timer()
    On Error Resume Next
    TxtNroTransaccion.SetFocus
    Timer1.Enabled = False
End Sub
Private Sub TxtNroTransaccion_GotFocus()
    En_Foco Me.ActiveControl
End Sub

Private Sub TxtNroTransaccion_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
    If KeyAscii = 39 Then KeyAscii = 0
End Sub
