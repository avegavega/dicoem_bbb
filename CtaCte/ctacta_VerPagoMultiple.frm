VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form ctacta_VerPagoMultiple 
   Caption         =   "Pago multiple"
   ClientHeight    =   4650
   ClientLeft      =   1815
   ClientTop       =   5610
   ClientWidth     =   8400
   LinkTopic       =   "Form1"
   ScaleHeight     =   4650
   ScaleWidth      =   8400
   Begin VB.Frame Frame1 
      Height          =   3855
      Left            =   240
      TabIndex        =   1
      Top             =   120
      Width           =   7935
      Begin VB.PictureBox FrmDetalles 
         BackColor       =   &H00C0C000&
         Height          =   1590
         Left            =   120
         ScaleHeight     =   1530
         ScaleWidth      =   7635
         TabIndex        =   5
         ToolTipText     =   "Detalle de cheques"
         Top             =   2040
         Width           =   7695
         Begin MSComctlLib.ListView LVCheques 
            Height          =   1155
            Left            =   90
            TabIndex        =   6
            ToolTipText     =   "Este es el detalle de cheque(s)"
            Top             =   255
            Width           =   7470
            _ExtentX        =   13176
            _ExtentY        =   2037
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   0   'False
            HideSelection   =   0   'False
            FullRowSelect   =   -1  'True
            GridLines       =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            NumItems        =   6
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Object.Tag             =   "T1000"
               Text            =   "Banco"
               Object.Width           =   2646
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Object.Tag             =   "T1000"
               Text            =   "Plaza"
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   2
               Object.Tag             =   "N109"
               Text            =   "Nro Cheque"
               Object.Width           =   1764
            EndProperty
            BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   3
               Object.Tag             =   "T1000"
               Text            =   "Fecha"
               Object.Width           =   2117
            EndProperty
            BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   4
               Object.Tag             =   "N100"
               Text            =   "Monto"
               Object.Width           =   1940
            EndProperty
            BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   5
               Object.Tag             =   "T1200"
               Text            =   "Estado"
               Object.Width           =   2117
            EndProperty
         End
         Begin VB.Label Label1 
            BackStyle       =   0  'Transparent
            Caption         =   "Detalle de cheques"
            ForeColor       =   &H00FFFFFF&
            Height          =   195
            Left            =   120
            TabIndex        =   7
            Top             =   45
            Width           =   1515
         End
      End
      Begin VB.PictureBox Picture3 
         BackColor       =   &H00C0C000&
         Height          =   1725
         Left            =   120
         ScaleHeight     =   1665
         ScaleWidth      =   7635
         TabIndex        =   2
         Top             =   240
         Width           =   7695
         Begin MSComctlLib.ListView LvMediosDePago 
            Height          =   1365
            Left            =   60
            TabIndex        =   3
            ToolTipText     =   "Estas son las formas de pago"
            Top             =   240
            Width           =   7470
            _ExtentX        =   13176
            _ExtentY        =   2408
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   0   'False
            HideSelection   =   0   'False
            FullRowSelect   =   -1  'True
            GridLines       =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            NumItems        =   5
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Object.Tag             =   "T1000"
               Text            =   "Fecha"
               Object.Width           =   2117
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Object.Tag             =   "T1000"
               Text            =   "Tipo de pago"
               Object.Width           =   4762
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   2
               Object.Tag             =   "N100"
               Text            =   "Valor"
               Object.Width           =   1764
            EndProperty
            BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   3
               Object.Tag             =   "T1000"
               Text            =   "Nro Transaccion"
               Object.Width           =   2117
            EndProperty
            BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   4
               Object.Tag             =   "T1000"
               Text            =   "Mas info."
               Object.Width           =   2117
            EndProperty
         End
         Begin VB.Label Label3 
            BackStyle       =   0  'Transparent
            Caption         =   "Forma de pago"
            ForeColor       =   &H00FFFFFF&
            Height          =   195
            Left            =   105
            TabIndex        =   4
            ToolTipText     =   "Formas de pago"
            Top             =   15
            Width           =   1515
         End
      End
   End
   Begin VB.Timer Timer1 
      Interval        =   100
      Left            =   120
      Top             =   4080
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   0
      OleObjectBlob   =   "ctacta_VerPagoMultiple.frx":0000
      Top             =   5040
   End
   Begin VB.CommandButton CmdSalir 
      Cancel          =   -1  'True
      Caption         =   "&Retornar"
      Height          =   375
      Left            =   6960
      TabIndex        =   0
      Top             =   4080
      Width           =   1215
   End
End
Attribute VB_Name = "ctacta_VerPagoMultiple"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public Lm_IdAbono As Long

Private Sub CmdSalir_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    Centrar Me
    Aplicar_skin Me
    'CONSULTAMOS SI HAY PAGO CON CHEQUES
    LVCheques.ListItems.Clear
    Sql = "SELECT ban_nombre,che_plaza,che_numero,che_fecha,che_monto,che_estado " & _
            "FROM vi_abono_con_cheques " & _
            "WHERE abo_id=" & Lm_IdAbono
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LVCheques, False, True, True, False

    'Aqui mostraremos como se pago el o los docuemtnso,
     Sql = "SELECT abo_fecha_pago,mpa_nombre,pad_valor,pad_nro_transaccion,pad_tipo_deposito " & _
            "FROM vi_abono_tipos_de_pago " & _
            "WHERE abo_id=" & Lm_IdAbono
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, Me.LvMediosDePago, False, True, True, False
End Sub

Private Sub Timer1_Timer()
    On Error Resume Next
    CmdSalir.SetFocus
    Timer1.Enabled = False
End Sub
