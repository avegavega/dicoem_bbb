VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form ctacte_GestionDePagos 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Gestion de Pago"
   ClientHeight    =   7005
   ClientLeft      =   1725
   ClientTop       =   2040
   ClientWidth     =   11280
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7005
   ScaleWidth      =   11280
   Begin VB.TextBox txtSuperMensaje 
      BackColor       =   &H00FFFFC0&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   315
      Left            =   2955
      TabIndex        =   38
      Text            =   "Si se equivoca debe completar el pago.Luego Eliminar Doc. y reingresar"
      Top             =   9435
      Width           =   6240
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkORIGEN 
      Height          =   360
      Left            =   405
      OleObjectBlob   =   "ctacte_GestionDePagos.frx":0000
      TabIndex        =   37
      Top             =   7530
      Width           =   1125
   End
   Begin VB.TextBox Text1 
      BackColor       =   &H00FFFFC0&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   315
      Left            =   2925
      TabIndex        =   34
      Text            =   "Con Enter el cursor se posiciona en columna VALOR"
      Top             =   6480
      Width           =   4620
   End
   Begin MSComDlg.CommonDialog dialogo 
      Left            =   8340
      Top             =   6465
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      DialogTitle     =   "Imprimir Comprobante"
   End
   Begin VB.CommandButton cmdPago 
      Caption         =   "Proceder al Pago"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   540
      Left            =   270
      TabIndex        =   28
      Tag             =   "Confirma el pago con los valores ingresados"
      Top             =   6360
      Width           =   2565
   End
   Begin VB.CommandButton CmdSalir 
      Caption         =   "&Salir"
      Height          =   390
      Left            =   9375
      TabIndex        =   1
      Top             =   6510
      Width           =   1650
   End
   Begin VB.Frame Frame1 
      Caption         =   "Documentos para abonar"
      Height          =   6180
      Left            =   270
      TabIndex        =   0
      Top             =   165
      Width           =   10755
      Begin VB.TextBox TxtPozo 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   8790
         TabIndex        =   35
         Tag             =   "N"
         Text            =   "0"
         Top             =   2100
         Width           =   1350
      End
      Begin VB.CommandButton CmdPagaTodo 
         Caption         =   "&Paga Todo"
         Height          =   225
         Left            =   210
         TabIndex        =   33
         Top             =   2100
         Width           =   1290
      End
      Begin VB.TextBox TxtSumaPagos 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   405
         Left            =   7680
         Locked          =   -1  'True
         TabIndex        =   31
         TabStop         =   0   'False
         Text            =   "0"
         Top             =   5085
         Width           =   2445
      End
      Begin VB.TextBox TxtTemp 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   7740
         TabIndex        =   29
         Tag             =   "N"
         Text            =   "0"
         Top             =   4320
         Visible         =   0   'False
         Width           =   2310
      End
      Begin VB.Frame Frame3 
         Caption         =   "Observacion"
         Height          =   570
         Left            =   420
         TabIndex        =   26
         Top             =   5505
         Width           =   10125
         Begin VB.TextBox TxtComentario 
            Height          =   285
            Left            =   330
            TabIndex        =   27
            Top             =   210
            Width           =   9585
         End
      End
      Begin MSComCtl2.DTPicker DTaboFechaPago 
         Height          =   345
         Left            =   1305
         TabIndex        =   25
         Top             =   375
         Width           =   1305
         _ExtentX        =   2302
         _ExtentY        =   609
         _Version        =   393216
         Format          =   91947009
         CurrentDate     =   40840
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   270
         Left            =   195
         OleObjectBlob   =   "ctacte_GestionDePagos.frx":006A
         TabIndex        =   24
         Top             =   405
         Width           =   1305
      End
      Begin VB.Frame FramePago 
         Caption         =   "Pago"
         Height          =   630
         Left            =   -5160
         TabIndex        =   15
         Top             =   3225
         Visible         =   0   'False
         Width           =   5265
         Begin VB.ComboBox CboTipoDeposito 
            Height          =   315
            ItemData        =   "ctacte_GestionDePagos.frx":00E2
            Left            =   2145
            List            =   "ctacte_GestionDePagos.frx":00EC
            Style           =   2  'Dropdown List
            TabIndex        =   23
            Top             =   960
            Width           =   1185
         End
         Begin VB.TextBox TxtObs 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   3330
            TabIndex        =   22
            Tag             =   "L"
            Top             =   960
            Visible         =   0   'False
            Width           =   1875
         End
         Begin VB.TextBox TxtNroOperacion 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   45
            TabIndex        =   21
            Tag             =   "L"
            Top             =   960
            Width           =   2085
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkTexto 
            Height          =   210
            Left            =   60
            OleObjectBlob   =   "ctacte_GestionDePagos.frx":0102
            TabIndex        =   18
            Top             =   750
            Width           =   2010
         End
         Begin VB.ComboBox CboMPago 
            Height          =   315
            Left            =   165
            Style           =   2  'Dropdown List
            TabIndex        =   16
            Top             =   270
            Width           =   4800
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkDeposito 
            Height          =   210
            Left            =   2115
            OleObjectBlob   =   "ctacte_GestionDePagos.frx":0196
            TabIndex        =   19
            Top             =   750
            Width           =   1155
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkObs 
            Height          =   210
            Left            =   3315
            OleObjectBlob   =   "ctacte_GestionDePagos.frx":020E
            TabIndex        =   20
            Top             =   750
            Visible         =   0   'False
            Width           =   1875
         End
      End
      Begin VB.CommandButton CmdOk 
         Caption         =   "Ok"
         Height          =   285
         Left            =   10155
         TabIndex        =   13
         Top             =   735
         Width           =   390
      End
      Begin VB.TextBox TxtSaldo 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00E0E0E0&
         Height          =   285
         Left            =   7440
         Locked          =   -1  'True
         TabIndex        =   14
         Tag             =   "L"
         Top             =   735
         Width           =   1350
      End
      Begin VB.TextBox TxtAbono 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   8805
         TabIndex        =   12
         Tag             =   "N"
         Top             =   735
         Width           =   1350
      End
      Begin VB.TextBox TxtTotal 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00E0E0E0&
         Height          =   285
         Left            =   6090
         Locked          =   -1  'True
         TabIndex        =   11
         Tag             =   "L"
         Top             =   735
         Width           =   1350
      End
      Begin VB.TextBox TxtSucursal 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00E0E0E0&
         Height          =   285
         Left            =   4590
         Locked          =   -1  'True
         TabIndex        =   10
         Tag             =   "L"
         Top             =   735
         Width           =   1500
      End
      Begin VB.TextBox txtCliente 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00E0E0E0&
         Height          =   285
         Left            =   2595
         Locked          =   -1  'True
         TabIndex        =   9
         Tag             =   "L"
         Top             =   735
         Width           =   2000
      End
      Begin VB.TextBox txtDoc 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00E0E0E0&
         Height          =   285
         Left            =   1095
         Locked          =   -1  'True
         TabIndex        =   8
         Tag             =   "L"
         Top             =   735
         Width           =   1500
      End
      Begin VB.TextBox txtNro 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00E0E0E0&
         Height          =   285
         Left            =   195
         Locked          =   -1  'True
         TabIndex        =   7
         Tag             =   "L"
         Top             =   735
         Width           =   900
      End
      Begin VB.Frame Frame2 
         Caption         =   "Totales"
         Height          =   690
         Left            =   5865
         TabIndex        =   3
         Top             =   2325
         Width           =   4365
         Begin VB.TextBox txtSaldos 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00E0E0E0&
            Height          =   285
            Left            =   1575
            Locked          =   -1  'True
            TabIndex        =   6
            Text            =   "0"
            Top             =   300
            Width           =   1350
         End
         Begin VB.TextBox TxtAbonos 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFC0&
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   2925
            Locked          =   -1  'True
            TabIndex        =   5
            Text            =   "0"
            Top             =   300
            Width           =   1335
         End
         Begin VB.TextBox TxtTotales 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00E0E0E0&
            Height          =   285
            Left            =   225
            Locked          =   -1  'True
            TabIndex        =   4
            Text            =   "0"
            Top             =   300
            Width           =   1350
         End
      End
      Begin MSComctlLib.ListView LvDetalle 
         Height          =   1035
         Left            =   210
         TabIndex        =   2
         Top             =   1050
         Width           =   10350
         _ExtentX        =   18256
         _ExtentY        =   1826
         View            =   3
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   14
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "T1000"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "N109"
            Text            =   "Nro Doc."
            Object.Width           =   1587
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "T1500"
            Text            =   "Documento"
            Object.Width           =   2646
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Object.Tag             =   "T1300"
            Text            =   "Rut"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Object.Tag             =   "T2000"
            Text            =   "Cliente"
            Object.Width           =   3528
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Object.Tag             =   "T1500"
            Text            =   "Sucursal"
            Object.Width           =   2646
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   6
            Object.Tag             =   "T1000"
            Text            =   "Emision"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   7
            Object.Tag             =   "T1000"
            Text            =   "Vencimiento"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   8
            Key             =   "total"
            Object.Tag             =   "N100"
            Text            =   "Total"
            Object.Width           =   2381
         EndProperty
         BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   9
            Object.Tag             =   "N100"
            Text            =   "Abonos"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   10
            Key             =   "saldo"
            Object.Tag             =   "N100"
            Text            =   "Saldo"
            Object.Width           =   2381
         EndProperty
         BeginProperty ColumnHeader(12) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   11
            Key             =   "nuevoabono"
            Object.Tag             =   "N100"
            Text            =   "Nuevo Abono"
            Object.Width           =   2381
         EndProperty
         BeginProperty ColumnHeader(13) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   12
            Object.Tag             =   "N109"
            Text            =   "ID Sucursal"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(14) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   13
            Text            =   "Abo_id"
            Object.Width           =   2540
         EndProperty
      End
      Begin MSComctlLib.ListView LvPagos 
         Height          =   1950
         Left            =   345
         TabIndex        =   30
         Top             =   3105
         Width           =   10230
         _ExtentX        =   18045
         _ExtentY        =   3440
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   13
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "N109"
            Text            =   "Id"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "T3000"
            Text            =   "Forma de pago"
            Object.Width           =   13053
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   2
            Key             =   "pago"
            Object.Tag             =   "T4000"
            Text            =   "Valor"
            Object.Width           =   4233
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "Detalle ?"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Text            =   "Cheque?"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Text            =   "Nro Operaciopn"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   6
            Text            =   "Tipo depoisot"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   7
            Text            =   "Trans"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   8
            Text            =   "Depoisot"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   9
            Object.Tag             =   "N109"
            Text            =   "a que Cta Cte???"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   10
            Object.Tag             =   "N109"
            Text            =   "saldo"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(12) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   11
            Object.Tag             =   "N109"
            Text            =   "Cta Contable para saldar"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(13) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   12
            Object.Tag             =   "T1000"
            Text            =   "Asientos Involucrados"
            Object.Width           =   0
         EndProperty
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel16 
         Height          =   345
         Left            =   5880
         OleObjectBlob   =   "ctacte_GestionDePagos.frx":0282
         TabIndex        =   32
         Top             =   5130
         Width           =   1725
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   270
         Left            =   6750
         OleObjectBlob   =   "ctacte_GestionDePagos.frx":02ED
         TabIndex        =   36
         Top             =   2100
         Width           =   1980
      End
   End
   Begin VB.Timer Timer1 
      Interval        =   20
      Left            =   8790
      Top             =   -45
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   8190
      OleObjectBlob   =   "ctacte_GestionDePagos.frx":0365
      Top             =   -45
   End
   Begin MSComctlLib.ListView LvCheques 
      Height          =   2700
      Left            =   1140
      TabIndex        =   17
      Top             =   7695
      Width           =   7215
      _ExtentX        =   12726
      _ExtentY        =   4763
      View            =   3
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   0
      NumItems        =   9
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Object.Tag             =   "N100"
         Text            =   "Fecha"
         Object.Width           =   2117
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Object.Tag             =   "N109"
         Text            =   "Numero"
         Object.Width           =   1940
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   2
         Object.Tag             =   "N100"
         Text            =   "Monto"
         Object.Width           =   3413
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Text            =   "Banco"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Text            =   "Plaza"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   5
         Text            =   "Autorizacion"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   6
         Object.Tag             =   "T1000"
         Text            =   "nombre banco"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   7
         Text            =   "ID CHEQUQE"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   8
         Object.Tag             =   "T1000"
         Text            =   "Plaza de cuenta"
         Object.Width           =   2540
      EndProperty
   End
End
Attribute VB_Name = "ctacte_GestionDePagos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public Sm_Cli_Pro As String, Sm_NombreAbono As String, Sm_RutAbono As String
Dim Im_ID_Sucursal As Integer
Dim ip_LineaSeleccionada As Integer
Dim Lp_IdAbo As Long
Dim Lp_Nro_Comprobante As Long
Dim Sp_Fagos As String, Sp_VPagos As String * 14, Sm_Mpagos As String * 59
Public Im_Fpago_Defecto As Integer
Public Sm_RelacionMP As String
Private Sub CboMPago_Click()
    'If CboMPago.ListIndex > -1 Then Exit Sub
    If Me.CboMPago.ListIndex = 2 Or CboMPago.ListIndex = 3 Then
        FramePago.Height = 1350
        If CboMPago.ListIndex = 2 Then
            'Si es transferencia bancaria quitamos tipo deposito
            Me.SkDeposito.Visible = False
            Me.CboTipoDeposito.Visible = False
            SkTexto = "N�mero de Transacci�n"
        Else
            SkTexto = "Numero de Deposito"
            Me.SkDeposito.Visible = True
            Me.CboTipoDeposito.Visible = True
            CboTipoDeposito.ListIndex = 0
        End If
    Else
        FramePago.Height = 700
    End If
    
End Sub


Private Sub CmdOk_Click()
    If ip_LineaSeleccionada = 0 Then Exit Sub
    If Val(TxtAbono) < 0 Then Exit Sub
        
    If Val(TxtAbono) > LvDetalle.ListItems(ip_LineaSeleccionada).SubItems(10) Then
    
        MsgBox "El abono es superior al total del documento ...", vbInformation
        
       ' MsgBox "El abono no puede superar el total del documento ...", vbInformation
        TxtAbono.SetFocus
        Exit Sub
    End If
    LvDetalle.ListItems(ip_LineaSeleccionada).SubItems(11) = NumFormat(TxtAbono)
    
    ip_LineaSeleccionada = 0
    TxtAbonos = NumFormat(TotalizaColumna(LvDetalle, "nuevoabono") + Val(TxtPozo))
    Limpia
End Sub

Private Sub CmdPagaTodo_Click()
    If LvDetalle.ListItems.Count = 0 Then Exit Sub
    'Marcamos todos los documentos como pagados
    For i = 1 To LvDetalle.ListItems.Count
        LvDetalle.ListItems(i).SubItems(11) = LvDetalle.ListItems(i).SubItems(10)
    Next
    TxtAbonos = NumFormat(TotalizaColumna(LvDetalle, "nuevoabono") + TxtPozo)
    Limpia

End Sub

Private Sub cmdPago_Click()
    Dim Sp_EC As String
    If Val(TxtAbonos) = 0 Then
        MsgBox "Debe ingresar abonos...", vbInformation
        Exit Sub
    End If
    '�If CboMPago.ListIndex = -1 Then
    '    MsgBox "Seleccione medio de pago...", vbInformation
    '    CboMPago.SetFocus
    '    Exit Sub
    'End If
    For i = 1 To LvDetalle.ListItems.Count
        If Val(LvDetalle.ListItems(i).SubItems(11)) = 0 Then
            MsgBox "Hay documentos con abono 0... " & vbNewLine & "Si esta en la lista, debe tener abono...", vbInformation
            Exit Sub
        End If
    Next
        
    ''Control de date distinta de per�odo contable
'    If Month(DTaboFechaPago.Value) <> IG_Mes_Contable Or Year(DTaboFechaPago.Value) <> IG_Ano_Contable Then
'         MsgBox "Fecha no corresponde al periodo contable..."
'         DTaboFechaPago.SetFocus
'         Exit Sub
'    End If
    ''
    If CDbl(TxtSumaPagos) <> CDbl(TxtAbonos) Then
        MsgBox "El total de abonos no coincide con el total de pagos..."
        Exit Sub
    End If
  

    Sp_EC = Empty
    
    
    '19-10-2013
    'Debemos comprobar que el periodo en el  q se esta realizando el abono
    'no est� centralizado.
    For i = 1 To LvPagos.ListItems.Count
        If CDbl(LvPagos.ListItems(i).SubItems(2)) > 0 Then
            If Principal.CmdMenu(8).Visible = True Then
                If Len(LvPagos.ListItems(i).SubItems(12)) > 0 Then
                    If ConsultaCentralizado(LvPagos.ListItems(i).SubItems(12), Month(DTaboFechaPago), Year(DTaboFechaPago)) Then
                        Sql = "SELECT asi_nombre " & _
                                "FROM con_asientos_centralizar " & _
                                "WHERE asi_id IN(" & LvPagos.ListItems(i).SubItems(12) & ")"
                        Consulta RsTmp, Sql
                        If RsTmp.RecordCount > 0 Then
                            RsTmp.MoveFirst
                            Do While Not RsTmp.EOF
                                asientos = asientos & vbNewLine & RsTmp!asi_nombre
                                RsTmp.MoveNext
                            Loop
                        End If
                        MsgBox "periodo contable ya ha sido centralizado, " & vbNewLine & "No puede modificar " & vbNewLine & _
                        "Asientos que no deben estar centralizados para proceder:" & vbNewLine & asientos, vbInformation

                        Exit Sub
                    End If
                End If
            End If
            
            
        End If
    Next
    
    
    
    If SkORIGEN = "CAMBIOFP" Then
        '4 -5 SP
        '5:AM
        'DEBEMOS ELIMINAR EL ABONO ANTERIOR PARA QUE QUEDE REGISTRATDO EL NUEVO
        EliminaPrevio Val(LvDetalle.SelectedItem.SubItems(13))
        
        
        SkORIGEN = "VENTA"
    End If
    
    
    
    Lp_IdAbo = UltimoNro("cta_abonos", "abo_id")
    
    On Error GoTo ErrorPago
    
    
    cn.BeginTrans
            'Codigo 100 es para autoincremento de Comprobantes de ingresos de Clientes y 200 Proveedores
            If Sm_Cli_Pro = "CLI" Then Lp_Nro_Comprobante = AutoIncremento("lee", 100, , IG_id_Sucursal_Empresa)
            If Sm_Cli_Pro = "PRO" Then Lp_Nro_Comprobante = AutoIncremento("lee", 200, , IG_id_Sucursal_Empresa)
            
            
            If CDbl(TxtPozo) > 0 Then
                'Detectamos que estamos realizando un abono al pozo.
                '6 Nov 2012
                
                'Guardaremos estos valores en la tabla cta_pozo
                    
                Sql = "INSERT INTO cta_pozo (abo_id,pzo_fecha,rut_emp,pzo_cli_pro,pzo_rut,pzo_abono) " & _
                        "VALUES(" & Lp_IdAbo & ",'" & Fql(Date) & "','" & SP_Rut_Activo & "','" & Sm_Cli_Pro & "','" & LvDetalle.ListItems(1).SubItems(3) & "'," & CDbl(TxtPozo) & ")"
                cn.Execute Sql
                
            End If
            
            
            
            'Aqui registra el pago con su formas
            Sql = "INSERT INTO cta_abonos (abo_id,abo_cli_pro,abo_rut,abo_fecha,abo_fecha_pago,abo_monto,abo_observacion,usu_nombre,suc_id,abo_obs_extra,rut_emp,abo_nro_comprobante,caj_id,abo_origen) " & _
                  "VALUES(" & Lp_IdAbo & ",'" & Sm_Cli_Pro & "','" & LvDetalle.ListItems(1).SubItems(3) & "','" & _
                  Format(Date, "YYYY-MM-DD") & "','" & Format(DTaboFechaPago, "YYYY-MM-DD") & "'," & CDbl(TxtAbonos) & _
                  ",'" & CboMPago.Text & " " & Sp_EC & "','" & LogUsuario & "'," & Im_ID_Sucursal & ",'" & Me.TxtComentario & "','" & SP_Rut_Activo & "'," & Lp_Nro_Comprobante & "," & LG_id_Caja & ",'" & SkORIGEN & "')"
                  
            cn.Execute Sql
                
            Sql = "INSERT INTO cta_abono_documentos (abo_id,id,ctd_monto,rut_emp) VALUES "
            For i = 1 To LvDetalle.ListItems.Count
                Sql = Sql & "(" & Lp_IdAbo & "," & LvDetalle.ListItems(i) & "," & Abs(CDbl(LvDetalle.ListItems(i).SubItems(11))) & ",'" & SP_Rut_Activo & "'),"
            Next
            Sql = Mid(Sql, 1, Len(Sql) - 1)
            cn.Execute Sql
            
            If Sm_Cli_Pro = "CLI" Then AutoIncremento "GUARDA", 100, Lp_Nro_Comprobante, IG_id_Sucursal_Empresa
            If Sm_Cli_Pro = "PRO" Then AutoIncremento "GUARDA", 200, Lp_Nro_Comprobante, IG_id_Sucursal_Empresa
            
            'If CboMPago.ItemData(CboMPago.ListIndex) = 1 Then
            '    'EFECTIVO, NO REQUIERE NINGUN DETALLE
            'End If
            
            For i = 1 To LvPagos.ListItems.Count
                If Val(LvPagos.ListItems(i)) = 7777 Then
                    'Aqui detectamos que esta utilizando saldo a favor para abonar al documento
                    'por lo que debemos agregar un registro a los cargos del pozo.
                    '10 Nov 2012
                     'Guardaremos estos valores en la tabla cta_pozo
                    Sql = "INSERT INTO cta_pozo (abo_id,pzo_fecha,rut_emp,pzo_cli_pro,pzo_rut,pzo_cargo) " & _
                            "VALUES(" & Lp_IdAbo & ",'" & Fql(Date) & "','" & SP_Rut_Activo & "','" & Sm_Cli_Pro & "','" & LvDetalle.ListItems(1).SubItems(3) & "'," & CDbl(LvPagos.ListItems(i).SubItems(2)) & ")"
                    cn.Execute Sql
                End If
            
                If Val(LvPagos.ListItems(i).SubItems(9)) > 0 Then
                    'Aca detectamos que es un movimiento que debe ir
                    'al sistema de bancos - transfr-deposito
                    If Sm_Cli_Pro = "PRO" Then
                        Movimiento = "CARGO"
                        
                        idmov = 4
                        dpago = "PAGO A PROVEEDORES"
                        tacuen = IG_IdCtaProveedores
                        
                    Else
                        Movimiento = "INGRESO"
                        idmov = 2
                        dpago = "PAGO DE CLIENTES"
                        tacuen = IG_IdCtaClientes
                    End If
                    
                    Lp_IdMov = UltimoNro("ban_movimientos", "mov_id")
                    Sql = "INSERT INTO ban_movimientos (mov_id,cte_id,mov_detalle,mov_fecha,mov_valor,mov_identificacion,mov_identificacion_id,con_id,des_id,pla_id,mov_de_tesoreria,mov_tipo,abo_id) " & _
                              "VALUES(" & Lp_IdMov & "," & Val(LvPagos.ListItems(i).SubItems(9)) & ",'" & dpago & "','" & Fql(DTaboFechaPago) & "'," & CDbl(LvPagos.ListItems(i).SubItems(2)) & ",'" & LvPagos.ListItems(i).SubItems(1) & "'," & idmov & "," & 0 & "," & 0 & "," & tacuen & ",'SI','" & Movimiento & "'," & Lp_IdAbo & ")"
                    cn.Execute Sql
                    cn.Execute "INSERT INTO com_con_multicuenta (dcu_tipo,id_unico,pla_id,dcu_valor) " & _
                            "VALUES('TRANSFERENCIA'," & Lp_IdMov & "," & tacuen & "," & CDbl(LvPagos.ListItems(i).SubItems(2)) & ")"
                 
                End If
            Next
            
            If LvCheques.ListItems.Count > 0 Then
                'CHEQUE, AQUI GRABAMOS EL DETALLE DE LOS CHEQUES
                If LvCheques.ListItems.Count > 0 Then
                    Sql = "INSERT INTO abo_cheques (abo_id,ban_id,che_plaza,che_numero,che_monto,che_fecha,che_estado,che_autorizacion,rut_emp,che_ban_nombre) " & _
                             "VALUES "
                    With LvCheques
                        For i = 1 To LvCheques.ListItems.Count
                            Sql = Sql & "(" & Lp_IdAbo & "," & .ListItems(i).SubItems(3) & ",'" & .ListItems(i).SubItems(4) & _
                            "'," & .ListItems(i).SubItems(1) & "," & CDbl(.ListItems(i).SubItems(2)) & ",'" & _
                            Format(.ListItems(i), "YYYY-MM-DD") & "','CARTERA','" & .ListItems(i).SubItems(5) & "','" & SP_Rut_Activo & "','" & .ListItems(i).SubItems(6) & "'),"
                        Next
                    End With
                    Sql = Mid(Sql, 1, Len(Sql) - 1)
                    cn.Execute Sql
                End If
                
                'Aqui registramos los cheques, si existiesen, en movimientos de cuentas corrientes.
                For i = 1 To LvCheques.ListItems.Count
                
                    If Val(LvCheques.ListItems(i).SubItems(7)) > 0 Then
                        Lp_IdMov = UltimoNro("ban_movimientos", "mov_id")
                        Lp_NroCheque = Me.LvCheques.ListItems(i).SubItems(7)
                        
                         
                        Sql = "UPDATE ban_cheques SET che_fecha_emision='" & Fql(DTaboFechaPago) & "'," & _
                                                      "che_fecha_cobro='" & Fql(LvCheques.ListItems(i)) & "'," & _
                                                      "che_valor=" & CDbl(LvCheques.ListItems(i).SubItems(2)) & "," & _
                                                      "che_destinatario='" & "PAGO" & "'," & _
                                                      "che_concepto='" & "A PROVEEDORES" & "'," & _
                                                      "che_estado='POR COBRAR'," & _
                                                      "pla_id=" & IG_IdCtaProveedores & "," & _
                                                      "mes_contable=" & IG_Mes_Contable & "," & _
                                                      "ano_contable=" & IG_Ano_Contable & "," & _
                                                      "mov_id=" & Lp_IdMov & " " & _
                                "WHERE che_id=" & Lp_NroCheque
                        cn.Execute Sql
                        
                        Sql = "INSERT INTO com_con_multicuenta (dcu_tipo,id_unico,pla_id,dcu_valor) VALUES (" & _
                                "'CHEQUE'," & Lp_NroCheque & "," & IG_IdCtaProveedores & "," & CDbl(LvCheques.ListItems(i).SubItems(2)) & ")"
                        cn.Execute Sql
                        
                        
                        cn.Execute "DELETE FROM ban_movimientos " & _
                                   "WHERE mov_id=" & Lp_IdMov
                                   
                        Sql = "SELECT cte_id " & _
                              "FROM ban_cheques b " & _
                              "INNER JOIN ban_chequera c USING(chc_id) " & _
                              "WHERE che_id=" & Lp_NroCheque
                        Consulta RsTmp, Sql
                        If RsTmp.RecordCount > 0 Then
                            ctecteid = RsTmp!cte_id
                        Else
                            ctecteid = 0
                        End If
                        Sql = "INSERT INTO ban_movimientos (mov_id,cte_id,mov_detalle,mov_fecha,mov_valor,mov_identificacion,mov_identificacion_id,con_id,des_id,pla_id,mov_de_tesoreria,abo_id) " & _
                              "VALUES(" & Lp_IdMov & "," & ctecteid & ",'" & "PAGO A PROVEEDORES','" & Fql(DTaboFechaPago) & "'," & CDbl(LvCheques.ListItems(i).SubItems(2)) & ",'CHEQUE',1," & 0 & "," & 0 & "," & IG_IdCtaProveedores & ",'SI'," & Lp_IdAbo & ")"
                        cn.Execute Sql
                    End If
                    
                Next
            End If
            'Grabaremos la(s) formas de pago en que se pago o pagaron los documentos
            '26 Marzo 2012
            Sql = "INSERT INTO abo_tipos_de_pagos (abo_id,mpa_id,pad_valor,pad_nro_transaccion,pad_tipo_deposito,pla_id,caj_id) VALUES"
            
            For i = 1 To LvPagos.ListItems.Count
                If CDbl(LvPagos.ListItems(i).SubItems(2)) > 0 Then
                    Sql = Sql & "(" & Lp_IdAbo & "," & LvPagos.ListItems(i) & "," & CDbl(LvPagos.ListItems(i).SubItems(2)) & ",'" & _
                            LvPagos.ListItems(i).SubItems(5) & "','" & LvPagos.ListItems(i).SubItems(6) & "'," & Val(LvPagos.ListItems(i).SubItems(11)) & "," & LG_id_Caja & "),"
                End If
            Next
            Sql = Mid(Sql, 1, Len(Sql) - 1)
            cn.Execute Sql
    cn.CommitTrans
    On Error GoTo ImpresoraError
  '  SG_codigo = "previsualizar"
      
    '14 Diciem 2013
    'Si es pago directo desde vta_directa
    'no debemos imprimir comprobante
    If CmdSalir.Visible Then
        Previsualiza
        If SG_codigo = "print" Then
            dialogo.CancelError = True
            dialogo.ShowPrinter
            
            
            
            EstableImpresora Printer.DeviceName
            
            
            
            
            ImprimeComprobante
            

            
        End If
    End If
    '**************
    
    'If MsgBox("Pago realizado correctamente..." & vbNewLine & "�Desea imprimir comprobante...?", vbQuestion + vbYesNo) = vbYes Then ImprimeComprobante
    
    
    Unload Me
    Exit Sub
    
ErrorPago:
    cn.RollbackTrans
    MsgBox "Ocurrio un error al realizar el pago..." & vbNewLine & Err.Number & vbNewLine & Err.Description
    Exit Sub
ImpresoraError:
    Unload Me
    'Presiono el boton cancelar de la impresion
End Sub
Private Sub EstableImpresora(NombreImpresora As String)
   'Seteamos al impresora.
    Dim impresora As Printer
    ' Establece la impresora que se utilizar� para imprimir
    'Recorremos el objeto Printers
    For Each impresora In Printers
        'Si es igual al contenido se�alado en el combobox
        If UCase(impresora.DeviceName) = UCase(NombreImpresora) Then
            'Lo seteamos as�.
            Set Printer = impresora
            Exit For
        End If
    Next

End Sub
Private Sub ImprimeComprobante()
    'Previsualiza

    'Exit Sub
    Dim Cx As Double, Cy As Double, Sp_Fecha As String, Ip_Mes As Integer, Dp_S As Double, Sp_Letras As String
    Dim Sp_Neto As String * 12, Sp_IVA As String * 12
    
    Dim Sp_Nro_Comprobante As String, Sp_Imprime_Cheque As String * 2
    
    Dim Sp_Empresa As String
    Dim Sp_Giro As String
    Dim Sp_Direccion As String
    
    
    Dim Sp_Nombre As String * 45
    Dim Sp_Rut As String * 15
    Dim Sp_Concepto As String
    
    Dim Sp_Nro_Doc As String * 12
    Dim Sp_Documento As String * 35
    Dim Sp_Valor As String * 15
    Dim Sp_Saldo As String * 15
    Dim Sp_Fpago As String * 24
    Dim Sp_Total As String * 12
    
    Dim Sp_Banco As String * 15
    Dim Sp_NroCheque As String * 10
    Dim Sp_FechaCheque As String * 10
    Dim Sp_ValorCheque As String * 15
    Dim Sp_SumaCheque As String * 15
    
    Dim Sp_Observacion As String * 80
    Dim Sp_PlazaFechas As String * 50
    
    Dim Sp_PosicionValor As String
    Dim Sp_PosicionFechaPlaza As String
    Dim Sp_PosicionBenefeciario As String
    Dim Sp_PosicionMontoLetras As String
    
'    Sql = "SELECT poc_valor,poc_fecha_plaza,poc_beneficiario,poc_monto_letras " & _
            "FROM sis_posicion_cheque "
'    Consulta RsTmp, Sql
'    If RsTmp.RecordCount > 0 Then
'
'        Sp_PosicionValor = RsTmp!poc_valor
'        Sp_PosicionFechaPlaza = RsTmp!poc_fecha_plaza
'        Sp_PosicionBenefeciario = RsTmp!poc_beneficiario
'        Sp_PosicionMontoLetras = RsTmp!poc_monto_letras
'
'
'    End If
    
    Sp_Imprime_Cheque = "NO"
    
    
    For f = 1 To dialogo.Copies
        On Error GoTo ErroComp
        
        Sql = "SELECT giro,direccion,ciudad,emp_emite_cheque_en_comprobante_pago cheque " & _
             "FROM sis_empresas " & _
             "WHERE rut='" & SP_Rut_Activo & "'"
        Consulta RsTmp2, Sql
        If RsTmp2.RecordCount > 0 Then
            Sp_Giro = RsTmp2!giro
            Sp_Direccion = RsTmp2!direccion & " - " & RsTmp2!ciudad
            Sp_Imprime_Cheque = RsTmp2!cheque
        End If
        
    V = 1
    If Sp_Imprime_Cheque = "SI" Then
        V = LvCheques.ListItems.Count
        If V = 0 Then V = 1
    End If
        
    For p = 1 To V
        
        Printer.FontName = "Courier New"
        Printer.FontSize = 10
        Printer.ScaleMode = 7
        Cx = 2
        Cy = 1.9
        Dp_S = 0.17
        Printer.CurrentX = Cx
        Printer.CurrentY = Cy
        Printer.FontSize = 16  'tama�o de letra
        Printer.FontBold = True
        
        '**
        'Printer.PaintPicture LoadPicture(App.Path & "\IMG\LAFLOR\LAFLOR.jpg"), Cx + 0.62, 1
        Printer.PaintPicture LoadPicture(App.Path & "\REDMAROK.jpg"), Cx + 0.62, 1
        Printer.CurrentY = Printer.CurrentY + Dp_S + Dp_S
        Printer.CurrentY = Printer.CurrentY + Dp_S + Dp_S
        '**
        
        Printer.Print SP_Empresa_Activa
            
        
        Printer.CurrentX = Cx
        Printer.CurrentY = Printer.CurrentY
      
        Printer.FontSize = 10
      
        pos = Printer.CurrentY
        Printer.Print "R.U.T.   :" & SP_Rut_Activo
        Printer.CurrentY = Printer.CurrentY
        
    
        Printer.CurrentY = pos
        Printer.CurrentX = Cx + 11
        If Sm_Cli_Pro = "CLI" Then
             Printer.Print "COMPROBANTE DE INGRESO N�: " & Lp_Nro_Comprobante
        Else
             Printer.Print "COMPROBANTE DE EGRESO N�: " & Lp_Nro_Comprobante
        End If
        Printer.CurrentY = Printer.CurrentY
     
     
        Printer.FontBold = False
        Printer.FontSize = 10
        Printer.CurrentX = Cx
        Printer.Print "GIRO     :" & Sp_Giro
        Printer.CurrentY = Printer.CurrentY
        
        Printer.FontSize = 10
        Printer.CurrentX = Cx
        Printer.Print "DIRECCION:"; Sp_Direccion
        Printer.CurrentY = Printer.CurrentY + Dp_S
        
        Printer.CurrentX = Cx
        If Sm_Cli_Pro = "CLI" Then
             Printer.Print "Recibi de:"
        Else
             Printer.Print "Pagado a :"
        End If
        Printer.CurrentY = Printer.CurrentY + Dp_S
        Printer.FontSize = 10
        pos = Printer.CurrentY
        Printer.CurrentX = Cx
        Printer.Print "Nombre:" & Sm_NombreAbono
        
        Printer.CurrentY = pos
        Printer.CurrentX = Cx + 14
        Printer.Print "Fecha:" & DTaboFechaPago.Value
        Printer.CurrentY = Printer.CurrentY + Dp_S
        
        
        Printer.CurrentX = Cx
        Printer.Print "RUT   :" & Sm_RutAbono
        Printer.CurrentY = Printer.CurrentY + Dp_S
        
        Printer.CurrentX = Cx
        Printer.FontBold = True
        Printer.Print "CONCEPTO :"
        Printer.CurrentY = Printer.CurrentY + Dp_S
        
           
        'Aqui agregamos los documentos a los que se abonara
        Printer.FontBold = False
        Sp_Nro_Doc = "Nro"
        Sp_Documento = "Documento"
        RSet Sp_Valor = "Valor"
        RSet Sp_Saldo = "Saldo"
        Printer.CurrentX = Cx
        Printer.Print Sp_Nro_Doc & " " & Sp_Documento & " " & Sp_Valor & " " & Sp_Saldo
        Printer.CurrentY = Printer.CurrentY + Dp_S - 0.1
        For i = 1 To LvDetalle.ListItems.Count
            
            Sp_Nro_Doc = LvDetalle.ListItems(i).SubItems(1)
            Sp_Documento = LvDetalle.ListItems(i).SubItems(2)
            RSet Sp_Valor = LvDetalle.ListItems(i).SubItems(11)
            RSet Sp_Saldo = NumFormat(CDbl(LvDetalle.ListItems(i).SubItems(8)) - (CDbl(LvDetalle.ListItems(i).SubItems(9)) + CDbl(LvDetalle.ListItems(i).SubItems(11))))
            Printer.CurrentX = Cx
            Printer.Print Sp_Nro_Doc & " " & Sp_Documento & " " & Sp_Valor & " " & Sp_Saldo
            Printer.CurrentY = Printer.CurrentY + Dp_S - 0.2
            
        Next
        Printer.CurrentY = Printer.CurrentY + 0.2
        Printer.CurrentX = Cx
        Printer.FontBold = True
       ' Sp_Fpago = CboMPago.Text
        pos = Printer.CurrentY
        Printer.Print "FORMA DE PAGO:" ' & Sp_Fpago & " " & IIf(CboTipoDeposito.ListIndex > -1, Me.CboTipoDeposito.Text, "")
        Printer.CurrentX = Cx
        Printer.FontBold = True
        Printer.CurrentY = Printer.CurrentY + Dp_S
        Cy = Printer.CurrentY
        For i = 1 To LvPagos.ListItems.Count
            Printer.CurrentY = Cy
            Printer.CurrentX = Cx
            If CDbl(LvPagos.ListItems(i).SubItems(2)) > 0 Then
                RSet Sp_VPagos = LvPagos.ListItems(i).SubItems(2)
                RSet Sm_Mpagos = LvPagos.ListItems(i).SubItems(1) & _
                IIf(Val(LvPagos.ListItems(i).SubItems(5)) > 0, " NRO " & LvPagos.ListItems(i).SubItems(5) & IIf(Len(LvPagos.ListItems(i).SubItems(6)) > 0, " " & LvPagos.ListItems(i).SubItems(6), ""), "") & _
                " $" & Sp_VPagos
                Coloca Cx, Cy, Sm_Mpagos, False, 9
                Cy = Cy + (Dp_S * 2)
            End If
        Next
        Printer.CurrentX = Cx
        RSet Sm_Mpagos = "----------------------------------------------"
        Coloca Cx, Cy, Sm_Mpagos, False, 9
        Printer.CurrentY = Printer.CurrentY + Dp_S
        Printer.CurrentX = Cx
        Printer.FontBold = True
        RSet Sm_Mpagos = "TOTAL PAGO   :$" & TxtAbonos
        Printer.Print Sm_Mpagos
        Printer.CurrentY = Printer.CurrentY + Dp_S + Dp_S
        Printer.FontBold = False
        
       ' If CboMPago.ItemData(CboMPago.ListIndex) = 3 Or CboMPago.ItemData(CboMPago.ListIndex) = 4 Then
       '     Printer.CurrentX = Cx
       '     Printer.Print "NRO OPERACION:" & txtNroOperacion
       '     Printer.CurrentY = Printer.CurrentY + Dp_S
       ' End If
        finpago = Printer.CurrentY
        
        'Aqui detallaremos los cheques
        If LvCheques.ListItems.Count > 0 Then
            iniciocheque = Printer.CurrentY
            Printer.CurrentX = Cx
            Printer.FontBold = True
            Printer.Print "DETALLE DE CHEQUES"
            Printer.FontBold = False
            Printer.CurrentY = Printer.CurrentY + Dp_S
            Sp_FechaCheque = "Fecha"
            Sp_NroCheque = "Nro"
            RSet Sp_ValorCheque = "Valor"
            Sp_Banco = "Banco"
            Printer.CurrentX = Cx
            Printer.Print Sp_Banco & " " & Sp_NroCheque & " " & Sp_FechaCheque & "       " & Sp_ValorCheque
            Printer.CurrentY = Printer.CurrentY + Dp_S - 0.2
            tcheques = 0
            For i = 1 To LvCheques.ListItems.Count
                If p = i Then Printer.FontBold = True
                Printer.CurrentX = Cx
                Sp_FechaCheque = LvCheques.ListItems(i)
                Sp_NroCheque = LvCheques.ListItems(i).SubItems(1)
                RSet Sp_ValorCheque = LvCheques.ListItems(i).SubItems(2)
                Sp_Banco = LvCheques.ListItems(i).SubItems(6)
                Printer.Print Sp_Banco & " " & Sp_NroCheque & " " & Sp_FechaCheque & "       " & Sp_ValorCheque
                Printer.CurrentY = Printer.CurrentY + Dp_S - 0.2
                tcheques = tcheques + CDbl(LvCheques.ListItems(i).SubItems(2))
                Printer.FontBold = False
            Next
            Sp_FechaCheque = ""
            Sp_NroCheque = ""
            RSet Sp_ValorCheque = NumFormat(tcheques)
            Sp_Banco = ""
            Printer.CurrentX = Cx
            Printer.FontBold = True
            Printer.Print Sp_Banco & " " & Sp_NroCheque & " " & Sp_FechaCheque & "       " & Sp_ValorCheque
            fincheque = Printer.CurrentY
            Printer.CurrentY = Printer.CurrentY + Dp_S
        End If
        Printer.CurrentY = Printer.CurrentY + Dp_S
        Printer.CurrentX = Cx
        Printer.FontBold = True
        posobs = Printer.CurrentY
        Printer.Print "OBSERVACIONES:"
        Printer.CurrentY = Printer.CurrentY + Dp_S
        Printer.FontBold = False
         
        Printer.CurrentX = Cx
        Printer.Print TxtComentario
        Printer.CurrentY = Printer.CurrentY + Dp_S
        pos = Printer.CurrentY
        Printer.CurrentY = pos + 1
        Printer.CurrentX = Cx
        
        Printer.Print "CONTABILIDAD"
            
        Printer.CurrentY = pos + 1
        Printer.CurrentX = Cx + 6
        Printer.Print "V� B� CAJA"
       
        Printer.CurrentY = pos + 1
        Printer.CurrentX = Cx + 12
        Printer.Print "NOMBRE, RUT Y FIRMA"
        Printer.CurrentY = Printer.CurrentY + Dp_S
        
        Printer.CurrentY = Printer.CurrentY - (Dp_S * 6)
        Printer.CurrentX = Cx + 12
        Printer.Print "RECIBI CONFORME"
        Printer.CurrentY = Printer.CurrentY + Dp_S
        
       ' Printer.DrawMode = 1
        Printer.DrawWidth = 3
        
        Printer.Line (1.5, 1)-(20, 4.5), , B 'Rectangulo Encabezado y N� Comprobante
        
        Printer.Line (1.5, 4.5)-(20, 6.2), , B 'Pagado o Recibido del cliente o proveedor
        
        If LvCheques.ListItems.Count > 0 Then
            Printer.Line (1.5, 6.2)-(20, finpago), , B  'Concepto - documentos pagados o recibidos y forma de pago
        Else
            Printer.Line (1.5, 6.2)-(20, finpago + 0.15), , B 'Concepto - documentos pagados o recibidos y forma de pago
        End If
        
        Printer.Line (13.5, 6.2)-(16.8, posobs), , B  'columnas de los pagos en efectivo
        'Printer.Line (13.5, 6.2)-(16.8, 9.37), , B  'columnas de los pagos en efectivo
        
        If LvCheques.ListItems.Count > 0 Then
            Printer.Line (1.5, iniciocheque)-(20, posobs), , B  'documentos pagados y forma de pago
            Printer.Line (13.5, iniciocheque)-(16.8, posobs), , B  'columnas de los pagos con cheques
        End If
        
        Printer.Line (1.5, posobs)-(20, posobs + 1.5), , B 'Observaciones
        Printer.Line (1.5, posobs + 1.5)-(20, posobs + 1.5 + 2), , B 'Pie del comprobante
        
        
        
                    '8 nov 2014
            'Imprime cheque
            If Sp_Imprime_Cheque = "SI" Then
                If LvCheques.ListItems.Count > 0 Then
                        If MsgBox("Imprime Cheque " & LvCheques.ListItems(p).SubItems(6) & " " & LvCheques.ListItems(p).SubItems(1), vbOKCancel + vbQuestion) = vbOK Then
                                Printer.CurrentY = Val(IG_Posicion_Inicial_Cheques)
                                Printer.CurrentX = 16.5
                                Printer.FontBold = True
                                Printer.FontSize = 10
                                
                              '  Sp_PosicionFechaPlaza = RsTmp!poc_fecha_plaza
                              '  Sp_PosicionBenefeciario = RsTmp!poc_beneficiario
                              '  Sp_PosicionMontoLetras = RsTmp!poc_monto_letras
                                'monto cheque
                                Printer.Print LvCheques.ListItems(p).SubItems(2) & "*****"
                                Printer.FontBold = False
                                
                                Printer.CurrentY = Val(IG_Posicion_Inicial_Cheques) + 1.1
                                Printer.CurrentX = 7.2
                                pos = Printer.CurrentY
                                'Fecha del cheque
                                RSet Sp_PlazaFechas = LvCheques.ListItems(p).SubItems(8) & ", " & Day(LvCheques.ListItems(p)) & " de " & MonthName(Month(LvCheques.ListItems(p))) & " de " & Year(LvCheques.ListItems(p))
                                'Printer.CurrentY = IG_Posicion_Inicial_Cheques + 2.5
                                  'Coloca (Cx + 13.3 + avance) - Printer.TextWidth(PTotal)
                                Printer.CurrentX = 19 - Printer.TextWidth(Sp_PlazaFechas)
                                pos = Printer.CurrentY
                                Printer.Print Sp_PlazaFechas 'se imprimie la plza
                                
                                
                                'nombre del beneficiario del cheque
                                Printer.CurrentY = Val(IG_Posicion_Inicial_Cheques) + 2.1
                                pos = Printer.CurrentY
                                'proveedor
                                Printer.CurrentX = 7.7
                                Printer.Print LvDetalle.ListItems(1).SubItems(4)
                                '/ fin nombre benefeciairios
                                
                                
                                Printer.FontSize = 10
                                Printer.CurrentX = 7.8
                                pos = Printer.CurrentY
                                MONTOENLETRAS = BrutoAletras(CDbl(LvCheques.ListItems(p).SubItems(2)), True)
                                If Len(MONTOENLETRAS) > 50 Then
                                    hasta = InStr(50, MONTOENLETRAS, " ")
                                    If hasta > 0 Then
                                        LINEA1 = Mid(MONTOENLETRAS, 1, hasta)
                                        LINEA2 = Mid(MONTOENLETRAS, hasta + 1)
                                        Printer.CurrentY = Val(IG_Posicion_Inicial_Cheques) + 3
                                        Printer.Print LINEA1
                                        Printer.CurrentX = 7.8
                                        Printer.Print LINEA2 & " ***********"
                                    Else
                                        Printer.CurrentY = 24.1
                                        Printer.Print MONTOENLETRAS & "  ********"
                                    End If
                                Else
                                    Printer.CurrentY = Val(IG_Posicion_Inicial_Cheques) + 3.1
                                    Printer.Print MONTOENLETRAS & "  ********"
                                End If
                            
                        End If
                End If
            End If
         '   printer.Height = pos * 0.04
        
            Printer.NewPage
            Printer.EndDoc
    Next p
    Next
    Exit Sub
ErroComp:
    MsgBox "Error :" & Err.Description & vbNewLine & "Nro " & Err.Number & vbNewLine & "Lin:" & Err.Source
End Sub


Private Sub CmdSalir_Click()
    Unload Me
End Sub









Private Sub Form_Load()
    Dim Sp_Filtro5555 As String, Sql_select As String
    Centrar Me
    On Error Resume Next
    DTaboFechaPago = Date
    

            
   
    Aplicar_skin Me
    Consulta RsTmp3, Sql
    If RsTmp3.RecordCount > 0 Then
        LLenar_Grilla RsTmp3, Me, LvDetalle, False, True, True, False
        If LvDetalle.ListItems.Count > 0 Then
            'Buscar documento vencidos y marcarlos con rojo
            For i = 1 To LvDetalle.ListItems.Count
                Im_ID_Sucursal = LvDetalle.ListItems(i).SubItems(13)
                If Len(LvDetalle.ListItems(i).SubItems(7)) > 0 Then
                    If DateDiff("d", Date, LvDetalle.ListItems(i).SubItems(7)) < 0 Then
                        For X = 1 To LvDetalle.ColumnHeaders.Count - 2
                            LvDetalle.ListItems(i).ListSubItems(X).ForeColor = vbRed
                            LvDetalle.ListItems(i).ListSubItems(X).Bold = True
                        Next
                    End If
                End If
            Next
            TxtTotales = NumFormat(TotalizaColumna(LvDetalle, "total"))
            txtSaldos = NumFormat(TotalizaColumna(LvDetalle, "saldo"))
            Im_ID_Sucursal = Val(LvDetalle.ListItems(1).SubItems(12))
        End If
        
    End If
    LLenarCombo CboMPago, "mpa_nombre", "mpa_id", "par_medios_de_pago", "mpa_activo='SI' AND mpa_factoring='NO'"
        
    'Si tiene contabilidad, tambien cargar la opcion 5555
    ' que dice saldar con cuenta contable.
    ' 29 - 12 - 2012 - Se agrego UNION SELECT
    If Val(Sm_RelacionMP) > 0 Then
                SM_FILTROMP = " AND mpa_id IN(SELECT mpa_id FROM par_relaciona_plazo_medio_pago WHERE pla_id=" & Sm_RelacionMP & ")"
    Else
        SM_FILTROMP = ""
    End If
    
    'Solo para Alcalde _
    /16-10-2015 _
    Cuando es Boleta, solo efectivo, _
    Cuando es Factura efectivo, tranbank, tc
    If SP_Rut_Activo = "76.169.962-8" Then
        If Sm_RelacionMP = 1 Then
            If LvDetalle.ListItems(1).SubItems(2) = "FACTURA MANUAL" Then
                SM_FILTROMP = " AND mpa_id IN(1,8,9) "
            End If
            
        
        End If
    
    End If
    
    
    Sql = "SELECT mpa_id,mpa_nombre,0,mpa_detalle,mpa_cheque,'','',mpa_transferencia,mpa_deposito,0,0 saldo,0,asi_id_involucrados " & _
          "FROM par_medios_de_pago " & _
          "WHERE mpa_activo='SI' AND mpa_factoring='NO'  " & Sp_Filtro5555 & SM_FILTROMP & _
          " UNION " & _
          "SELECT 7777,CONCAT('SALDO A FAVOR $',CAST(FORMAT(SUM(pzo_abono)- SUM(pzo_cargo),0) AS CHAR)) mpa_nombre," & _
          "0,'NO','NO','','','NO','NO',0,SUM(pzo_abono)-SUM(pzo_cargo) saldo,0,0 asi_id_involucrados  " & _
          "FROM cta_pozo " & _
          "WHERE rut_emp='" & SP_Rut_Activo & "' AND pzo_rut='" & LvDetalle.ListItems(1).SubItems(3) & "' " & _
          "AND pzo_cli_pro='" & Sm_Cli_Pro & "' " & _
          "HAVING saldo > 0 " & _
          IIf(Val(Sm_RelacionMP) = 0, "UNION " & _
          "SELECT 5555,'SALDAR CON CTA CONTABLE',0,'NO','NO','','','NO','N0',0,0,0, 0 asi_id_involucrados  " & _
          "FROM par_medios_de_pago " & _
          "WHERE (SELECT men_activo FROM sis_menu WHERE men_id=200)='SI' ", "")
          If Sm_Cli_Pro = "CLI" Then
             '   Sql_select = " UNION SELECT 3333,CONCAT('NOTAS DE CREDITO',' $',CAST(SUM(bruto) AS CHAR)) ,0,'NO','NO','','','NO','NO',0, SUM(bruto) bruto FROM ven_doc_venta v " & _
                        "JOIN sis_documentos d ON v.doc_id=d.doc_id " & _
                        "WHERE rut_emp = '" & SP_Rut_Activo & "' AND rut_cliente='" & LVDetalle.ListItems(1).SubItems(3) & "' " & _
                          "AND doc_nota_de_credito='SI' AND ven_nc_utilizada='NO' HAVING bruto>0 "
            Else
            
              '  Sql_select = " UNION SELECT 3333,CONCAT('NOTAS DE CREDITO',' $',CAST(SUM(total) AS CHAR)),0,'NO','NO','','','NO','NO',0, SUM(total) bruto FROM com_doc_compra v " & _
                        "JOIN sis_documentos d ON v.doc_id=d.doc_id " & _
                        "WHERE rut_emp = '" & SP_Rut_Activo & "' AND rut='" & LVDetalle.ListItems(1).SubItems(3) & "' " & _
                          "AND doc_nota_de_credito='SI' AND com_nc_utilizada='NO' HAVING bruto>0 "
            
          End If
          
          
    Consulta RsTmp, Sql & Sql_select
    LLenar_Grilla RsTmp, Me, LvPagos, False, True, True, False
    If Im_Fpago_Defecto > 0 Then
        TxtAbonos = NumFormat(TotalizaColumna(LvDetalle, "nuevoabono") + TxtPozo)
        For i = 1 To LvPagos.ListItems.Count
            If Val(LvPagos.ListItems(i)) = Im_Fpago_Defecto Then
                If Im_Fpago_Defecto = 1 Then LvPagos.ListItems(i).SubItems(2) = TxtAbonos
                TxtAbono.Locked = True
                CmdOk.Enabled = False
                CmdPagaTodo.Enabled = False
                
                
                TxtSumaPagos = NumFormat(TotalizaColumna(LvPagos, "pago"))
                If CDbl(TxtSumaPagos) = CDbl(TxtAbonos) Then
                    TxtSumaPagos.ForeColor = vbBlue
                Else
                    TxtSumaPagos.ForeColor = vbRed
                End If
                
                LvPagos.SetFocus
                
                
                
                Exit Sub
            End If
        Next
    End If
    
         
End Sub


Private Sub LvDetalle_DblClick()
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    ip_LineaSeleccionada = 0
    With LvDetalle.SelectedItem
        ip_LineaSeleccionada = .Index
        txtNro = .SubItems(1)
        txtDoc = .SubItems(2)
        txtCliente = .SubItems(4)
        TxtSucursal = .SubItems(5)
        TxtTotal = .SubItems(8)
        TxtSaldo = .SubItems(10)
        TxtAbono = TxtSaldo
        TxtAbono.SetFocus
    End With
       
    
End Sub

Private Sub LvPagos_KeyDown(KeyCode As Integer, Shift As Integer)
    If LvPagos.SelectedItem Is Nothing Then Exit Sub
    If KeyCode = 13 Then
            TxtTemp.Visible = True
            TxtTemp = LvPagos.SelectedItem.SubItems(2)
            TxtTemp = CDbl(TxtAbonos) - CDbl(Me.TxtSumaPagos)
            TxtTemp.Tag = LvPagos.SelectedItem
            TxtTemp.Top = LvPagos.Top + LvPagos.SelectedItem.Top
            If Val(LvPagos.SelectedItem.SubItems(10)) > 0 Then
                If CDbl(TxtTemp) > Val(LvPagos.SelectedItem.SubItems(10)) Then
                    TxtTemp = Val(LvPagos.SelectedItem.SubItems(10))
                End If
            End If
            TxtTemp.SetFocus
    End If
    
    
End Sub

Private Sub Timer1_Timer()
    On Error Resume Next
    LvDetalle.SetFocus
    Timer1.Enabled = False
    
End Sub
Private Sub Limpia()
        txtNro = Empty
        txtDoc = Empty
        txtCliente = Empty
        TxtSucursal = Empty
        TxtTotal = Empty
        TxtSaldo = Empty
        TxtAbono = Empty
End Sub


Private Sub TxtAbono_GotFocus()
    En_Foco TxtAbono
End Sub

Private Sub TxtAbono_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
End Sub
Private Sub TxtComentario_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub TxtNroOperacion_GotFocus()
    En_Foco TxtNroOperacion
End Sub

Private Sub TxtNroOperacion_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
    If KeyAscii = 39 Then KeyAscii = 0
End Sub


Private Sub Previsualiza()
   Dim Cx As Double, Cy As Double, Sp_Fecha As String, Ip_Mes As Integer, Dp_S As Double, Sp_Letras As String
    Dim Sp_Neto As String * 12, Sp_IVA As String * 12
    
    Dim Sp_Nro_Comprobante As String
    
    Dim Sp_Empresa As String
    Dim Sp_Giro As String
    Dim Sp_Direccion As String
    
    
    Dim Sp_Nombre As String * 45
    Dim Sp_Rut As String * 15
    Dim Sp_Concepto As String
    
    Dim Sp_Nro_Doc As String * 12
    Dim Sp_Documento As String * 35
    Dim Sp_Valor As String * 15
    Dim Sp_Saldo As String * 15
    Dim Sp_Fpago As String * 24
    Dim Sp_Total As String * 12
    
    Dim Sp_Banco As String * 15
    Dim Sp_NroCheque As String * 10
    Dim Sp_FechaCheque As String * 10
    Dim Sp_ValorCheque As String * 15
    Dim Sp_SumaCheque As String * 15
    
    Dim Sp_Observacion As String * 80
    
    
    Sql = "SELECT giro,direccion,ciudad " & _
         "FROM sis_empresas " & _
         "WHERE rut='" & SP_Rut_Activo & "'"
    Consulta RsTmp2, Sql
    If RsTmp2.RecordCount > 0 Then
        Sp_Giro = RsTmp2!giro
        Sp_Direccion = RsTmp2!direccion & " - " & RsTmp2!ciudad
    End If
    
    
    Sis_Previsualizar.Pic.ScaleMode = vbCentimeters
    Sis_Previsualizar.Pic.BackColor = vbWhite
    Sis_Previsualizar.Pic.AutoRedraw = True
    Sis_Previsualizar.Pic.DrawWidth = 1
    Sis_Previsualizar.Pic.DrawMode = 1
    
    Sis_Previsualizar.Pic.FontName = "Courier New"
    Sis_Previsualizar.Pic.FontSize = 10
   
    Cx = 2
    Cy = 1.9
    Dp_S = 0.17
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.CurrentY = Cy
    Sis_Previsualizar.Pic.FontSize = 16  'tama�o de letra
    Sis_Previsualizar.Pic.FontBold = True
    
    '**
    
    'Sis_Previsualizar.Pic.PaintPicture LoadPicture(App.Path & "\IMG\LAFLOR\LAFLOR.jpg"), Cx + 0.62, 1
    Sis_Previsualizar.Pic.PaintPicture LoadPicture(App.Path & "\REDMAROK.jpg"), Cx + 0.62, 1
    'Printer.PaintPicture LoadPicture(App.Path & "\REDMAROK.jpg"), Cx + 0.62, 1
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S + Dp_S
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S + Dp_S
    '**
    
    Sis_Previsualizar.Pic.Print SP_Empresa_Activa
        
    
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY
  
    Sis_Previsualizar.Pic.FontSize = 10
  
    pos = Sis_Previsualizar.Pic.CurrentY
    Sis_Previsualizar.Pic.Print "R.U.T.   :" & SP_Rut_Activo
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY
    

    Sis_Previsualizar.Pic.CurrentY = pos
    Sis_Previsualizar.Pic.CurrentX = Cx + 11
    If Sm_Cli_Pro = "CLI" Then
         Sis_Previsualizar.Pic.Print "COMPROBANTE DE INGRESO N�: " & Lp_Nro_Comprobante
    Else
         Sis_Previsualizar.Pic.Print "COMPROBANTE DE EGRESO N�: " & Lp_Nro_Comprobante
    End If
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY
 
 
    Sis_Previsualizar.Pic.FontBold = False
    Sis_Previsualizar.Pic.FontSize = 10
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.Print "GIRO     :" & Sp_Giro
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY
    
  
    Sis_Previsualizar.Pic.FontSize = 10
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.Print "DIRECCION:"; Sp_Direccion
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
    
    Sis_Previsualizar.Pic.CurrentX = Cx
    If Sm_Cli_Pro = "CLI" Then
         Sis_Previsualizar.Pic.Print "Recibi de:"
    Else
         Sis_Previsualizar.Pic.Print "Pagado a :"
    End If
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
    Sis_Previsualizar.Pic.FontSize = 10
    pos = Sis_Previsualizar.Pic.CurrentY
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.Print "Nombre:" & Sm_NombreAbono
    
    Sis_Previsualizar.Pic.CurrentY = pos
    Sis_Previsualizar.Pic.CurrentX = Cx + 14
    Sis_Previsualizar.Pic.Print "Fecha:" & DTaboFechaPago.Value
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
    
    
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.Print "RUT   :" & Sm_RutAbono
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
    
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.FontBold = True
    Sis_Previsualizar.Pic.Print "CONCEPTO :"
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
    
       
    'Aqui agregamos los documentos a los que se abonara
    Sis_Previsualizar.Pic.FontBold = False
    Sp_Nro_Doc = "Nro"
    Sp_Documento = "Documento"
    RSet Sp_Valor = "Valor"
    RSet Sp_Saldo = "Saldo"
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.Print Sp_Nro_Doc & " " & Sp_Documento & " " & Sp_Valor & " " & Sp_Saldo
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S - 0.1
    For i = 1 To LvDetalle.ListItems.Count
        Sp_Nro_Doc = LvDetalle.ListItems(i).SubItems(1)
        Sp_Documento = LvDetalle.ListItems(i).SubItems(2)
        RSet Sp_Valor = LvDetalle.ListItems(i).SubItems(11)
        RSet Sp_Saldo = NumFormat(CDbl(LvDetalle.ListItems(i).SubItems(8)) - (CDbl(LvDetalle.ListItems(i).SubItems(9)) + CDbl(LvDetalle.ListItems(i).SubItems(11))))
        Sis_Previsualizar.Pic.CurrentX = Cx
        Sis_Previsualizar.Pic.Print Sp_Nro_Doc & " " & Sp_Documento & " " & Sp_Valor & " " & Sp_Saldo
        Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S - 0.2
    Next
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + 0.2
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.FontBold = True
   ' Sp_Fpago = CboMPago.Text
    pos = Sis_Previsualizar.Pic.CurrentY
    Sis_Previsualizar.Pic.FontSize = 10
    Sis_Previsualizar.Pic.Print "FORMA DE PAGO:" '& Sp_Fpago & " " & IIf(CboTipoDeposito.ListIndex > -1, Me.CboTipoDeposito.Text, "")
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.FontBold = False
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
    Cy = Sis_Previsualizar.Pic.CurrentY
    Sis_Previsualizar.Pic.FontSize = 9
    For i = 1 To LvPagos.ListItems.Count
        Sis_Previsualizar.Pic.CurrentY = Cy
        Sis_Previsualizar.Pic.CurrentX = Cx
        If CDbl(LvPagos.ListItems(i).SubItems(2)) > 0 Then
            RSet Sp_VPagos = LvPagos.ListItems(i).SubItems(2)
            RSet Sm_Mpagos = LvPagos.ListItems(i).SubItems(1) & _
            IIf(Val(LvPagos.ListItems(i).SubItems(5)) > 0, " NRO " & LvPagos.ListItems(i).SubItems(5) & IIf(Len(LvPagos.ListItems(i).SubItems(6)) > 0, " " & LvPagos.ListItems(i).SubItems(6), ""), "") & _
            " $" & Sp_VPagos
            'Coloca Cx, Cy, Sp_Fagos, False, 10
            
            Sis_Previsualizar.Pic.Print Sm_Mpagos
            Cy = Cy + (Dp_S * 2)
        End If
    Next
    RSet Sm_Mpagos = "--------------------------------------------"
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.Print Sm_Mpagos
    Sis_Previsualizar.Pic.FontBold = True
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
    RSet Sm_Mpagos = "TOTAL PAGO   :$" & TxtAbonos
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.Print Sm_Mpagos
    
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S + Dp_S
    
    
    Sis_Previsualizar.Pic.FontBold = False
    
   ' If CboMPago.ItemData(CboMPago.ListIndex) = 3 Or CboMPago.ItemData(CboMPago.ListIndex) = 4 Then
   ''     Sis_Previsualizar.Pic.CurrentX = Cx
   '     Sis_Previsualizar.Pic.Print "NRO OPERACION:" & txtNroOperacion
   '     Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
   ' End If
    finpago = Sis_Previsualizar.Pic.CurrentY
    
    'Aqui detallaremos los cheques
    If LvCheques.ListItems.Count > 0 Then
        iniciocheque = Sis_Previsualizar.Pic.CurrentY
        Sis_Previsualizar.Pic.CurrentX = Cx
        Sis_Previsualizar.Pic.FontBold = True
        Sis_Previsualizar.Pic.Print "DETALLE DE CHEQUES"
        Sis_Previsualizar.Pic.FontBold = False
        Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
        Sp_FechaCheque = "Fecha"
        Sp_NroCheque = "Nro"
        RSet Sp_ValorCheque = "Valor"
        Sp_Banco = "Banco"
        Sis_Previsualizar.Pic.CurrentX = Cx
        Sis_Previsualizar.Pic.Print Sp_Banco & " " & Sp_NroCheque & " " & Sp_FechaCheque & "       " & Sp_ValorCheque
        Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S - 0.2
        tcheques = 0
        For i = 1 To LvCheques.ListItems.Count
            Sis_Previsualizar.Pic.CurrentX = Cx
            Sp_FechaCheque = LvCheques.ListItems(i)
            Sp_NroCheque = LvCheques.ListItems(i).SubItems(1)
            RSet Sp_ValorCheque = LvCheques.ListItems(i).SubItems(2)
            Sp_Banco = LvCheques.ListItems(i).SubItems(6)
            Sis_Previsualizar.Pic.Print Sp_Banco & " " & Sp_NroCheque & " " & Sp_FechaCheque & "       " & Sp_ValorCheque
            Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S - 0.2
            tcheques = tcheques + CDbl(LvCheques.ListItems(i).SubItems(2))
        Next
        Sp_FechaCheque = ""
        Sp_NroCheque = ""
        RSet Sp_ValorCheque = NumFormat(tcheques)
        Sp_Banco = ""
        Sis_Previsualizar.Pic.CurrentX = Cx
        Sis_Previsualizar.Pic.FontBold = True
        Sis_Previsualizar.Pic.Print Sp_Banco & " " & Sp_NroCheque & " " & Sp_FechaCheque & "       " & Sp_ValorCheque
        fincheque = Sis_Previsualizar.Pic.CurrentY
        Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
    End If
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.FontBold = True
    posobs = Sis_Previsualizar.Pic.CurrentY
    Sis_Previsualizar.Pic.Print "OBSERVACIONES:"
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
    Sis_Previsualizar.Pic.FontBold = False
     
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.Print TxtComentario
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
    pos = Sis_Previsualizar.Pic.CurrentY
    Sis_Previsualizar.Pic.CurrentY = pos + 1
    Sis_Previsualizar.Pic.CurrentX = Cx
    
    Sis_Previsualizar.Pic.Print "CONTABILIDAD"
        
    Sis_Previsualizar.Pic.CurrentY = pos + 1
    Sis_Previsualizar.Pic.CurrentX = Cx + 6
    Sis_Previsualizar.Pic.Print "V� B� CAJA"
   
   
   
    Sis_Previsualizar.Pic.CurrentY = pos + 1
    Sis_Previsualizar.Pic.CurrentX = Cx + 12
    Sis_Previsualizar.Pic.Print "NOMBRE, RUT Y FIRMA"
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
    
    
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY - (Dp_S * 6)
    Sis_Previsualizar.Pic.CurrentX = Cx + 12
    Sis_Previsualizar.Pic.Print "RECIBI CONFORME"
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
   ' Sis_Previsualizar.pic.DrawMode = 1
    'Sis_Previsualizar.pic.DrawWidth = 3
    
    Sis_Previsualizar.Pic.Line (1.5, 1)-(20, 4.5), , B 'Rectangulo Encabezado y N� Comprobante
    
    Sis_Previsualizar.Pic.Line (1.5, 4.5)-(20, 6.2), , B 'Pagado o Recibido del cliente o proveedor
    
    If LvCheques.ListItems.Count > 0 Then
        Sis_Previsualizar.Pic.Line (1.5, 6.2)-(20, finpago), , B  'Concepto - documentos pagados o recibidos y forma de pago
    Else
        Sis_Previsualizar.Pic.Line (1.5, 6.2)-(20, finpago + 0.15), , B 'Concepto - documentos pagados o recibidos y forma de pago
    End If
    
    Sis_Previsualizar.Pic.Line (13.5, 6.2)-(16.8, posobs), , B  'columnas de los pagos en efectivo
    'Sis_Previsualizar.pic.Line (13.5, 6.2)-(16.8, 9.37), , B  'columnas de los pagos en efectivo
    
    If LvCheques.ListItems.Count > 0 Then
        Sis_Previsualizar.Pic.Line (1.5, iniciocheque)-(20, posobs), , B  'documentos pagados y forma de pago
        Sis_Previsualizar.Pic.Line (13.5, iniciocheque)-(16.8, posobs), , B  'columnas de los pagos con cheques
    End If
    
    Sis_Previsualizar.Pic.Line (1.5, posobs)-(20, posobs + 1.5), , B 'Observaciones
    Sis_Previsualizar.Pic.Line (1.5, posobs + 1.5)-(20, posobs + 1.5 + 2), , B 'Pie del comprobante
        
    pos = Sis_Previsualizar.Pic.CurrentY
    
 '   Sis_Previsualizar.Pic.Height = pos * 0.04

    Sis_Previsualizar.Show 1
End Sub
Private Sub PrevisualizaXXX()
   Dim Cx As Double, Cy As Double, Sp_Fecha As String, Ip_Mes As Integer, Dp_S As Double, Sp_Letras As String
    Dim Sp_Neto As String * 12, Sp_IVA As String * 12
    
    Dim Sp_Nro_Comprobante As String
    
    Dim Sp_Empresa As String
    Dim Sp_Giro As String
    Dim Sp_Direccion As String
    
    
    Dim Sp_Nombre As String * 45
    Dim Sp_Rut As String * 15
    Dim Sp_Concepto As String
    
    Dim Sp_Nro_Doc As String * 12
    Dim Sp_Documento As String * 35
    Dim Sp_Valor As String * 15
    Dim Sp_Saldo As String * 15
    Dim Sp_Fpago As String * 24
    Dim Sp_Total As String * 12
    
    Dim Sp_Banco As String * 20
    Dim Sp_NroCheque As String * 10
    Dim Sp_FechaCheque As String * 10
    Dim Sp_ValorCheque As String * 15
    Dim Sp_SumaCheque As String * 15
    
    Dim Sp_Observacion As String * 80
    
    
    Sql = "SELECT giro,direccion,ciudad " & _
         "FROM sis_empresas " & _
         "WHERE rut='" & SP_Rut_Activo & "'"
    Consulta RsTmp2, Sql
    If RsTmp2.RecordCount > 0 Then
        Sp_Giro = RsTmp2!giro
        Sp_Direccion = RsTmp2!direccion & " - " & RsTmp2!ciudad
    End If
    
    
    Sis_Previsualizar.Pic.ScaleMode = vbCentimeters
    Sis_Previsualizar.Pic.BackColor = vbWhite
    Sis_Previsualizar.Pic.AutoRedraw = True
    Sis_Previsualizar.Pic.DrawWidth = 1
    Sis_Previsualizar.Pic.DrawMode = 1
    
    Sis_Previsualizar.Pic.FontName = "Courier New"
    Sis_Previsualizar.Pic.FontSize = 10
   
    Cx = 2
    Cy = 1.9
    Dp_S = 0.17
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.CurrentY = Cy
    Sis_Previsualizar.Pic.FontSize = 16  'tama�o de letra
    Sis_Previsualizar.Pic.FontBold = True
    
    '**
    'Sis_Previsualizar.Pic.PaintPicture LoadPicture(App.Path & "\IMG\LAFLOR\LAFLOR.jpg"), Cx + 0.62, 1
    Sis_Previsualizar.Pic.PaintPicture LoadPicture(App.Path & "\REDMAROK.jpg"), Cx + 0.62, 1
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S + Dp_S
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S + Dp_S
    '**
    
    Sis_Previsualizar.Pic.Print SP_Empresa_Activa
        
    
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY
  
    Sis_Previsualizar.Pic.FontSize = 10
  
    pos = Sis_Previsualizar.Pic.CurrentY
    Sis_Previsualizar.Pic.Print "R.U.T.   :" & SP_Rut_Activo
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY
    

    Sis_Previsualizar.Pic.CurrentY = pos
    Sis_Previsualizar.Pic.CurrentX = Cx + 11
    If Sm_Cli_Pro = "CLI" Then
         Sis_Previsualizar.Pic.Print "COMPROBANTE DE INGRESO N�: " & Lp_Nro_Comprobante
    Else
         Sis_Previsualizar.Pic.Print "COMPROBANTE DE EGRESO N�: " & Lp_Nro_Comprobante
    End If
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY
 
 
    Sis_Previsualizar.Pic.FontBold = False
    Sis_Previsualizar.Pic.FontSize = 10
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.Print "GIRO     :" & Sp_Giro
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY
    
  
    Sis_Previsualizar.Pic.FontSize = 10
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.Print "DIRECCION:"; Sp_Direccion
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
    
    Sis_Previsualizar.Pic.CurrentX = Cx
    If Sm_Cli_Pro = "CLI" Then
         Sis_Previsualizar.Pic.Print "Recibi de:"
    Else
         Sis_Previsualizar.Pic.Print "Pagado a :"
    End If
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
    Sis_Previsualizar.Pic.FontSize = 10
    pos = Sis_Previsualizar.Pic.CurrentY
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.Print "Nombre:" & Sm_NombreAbono
    
    Sis_Previsualizar.Pic.CurrentY = pos
    Sis_Previsualizar.Pic.CurrentX = Cx + 14
    Sis_Previsualizar.Pic.Print "Fecha:" & DTaboFechaPago.Value
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
    
    
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.Print "RUT   :" & Sm_RutAbono
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
    
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.FontBold = True
    Sis_Previsualizar.Pic.Print "CONCEPTO :"
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
    
       
    'Aqui agregamos los documentos a los que se abonara
    Sis_Previsualizar.Pic.FontBold = False
    Sp_Nro_Doc = "Nro"
    Sp_Documento = "Documento"
    RSet Sp_Valor = "Valor"
    RSet Sp_Saldo = "Saldo"
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.Print Sp_Nro_Doc & " " & Sp_Documento & " " & Sp_Valor & " " & Sp_Saldo
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S - 0.1
    For i = 1 To LvDetalle.ListItems.Count
        Sp_Nro_Doc = LvDetalle.ListItems(i).SubItems(1)
        Sp_Documento = LvDetalle.ListItems(i).SubItems(2)
        RSet Sp_Valor = LvDetalle.ListItems(i).SubItems(11)
        RSet Sp_Saldo = NumFormat(CDbl(LvDetalle.ListItems(i).SubItems(8)) - (CDbl(LvDetalle.ListItems(i).SubItems(9)) + CDbl(LvDetalle.ListItems(i).SubItems(11))))
        Sis_Previsualizar.Pic.CurrentX = Cx
        Sis_Previsualizar.Pic.Print Sp_Nro_Doc & " " & Sp_Documento & " " & Sp_Valor & " " & Sp_Saldo
        Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S - 0.2
    Next
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + 0.2
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.FontBold = True
   ' Sp_Fpago = CboMPago.Text
    pos = Sis_Previsualizar.Pic.CurrentY
    Sis_Previsualizar.Pic.FontSize = 10
    Sis_Previsualizar.Pic.Print "FORMA DE PAGO:" '& Sp_Fpago & " " & IIf(CboTipoDeposito.ListIndex > -1, Me.CboTipoDeposito.Text, "")
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.FontBold = False
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
    Cy = Sis_Previsualizar.Pic.CurrentY
    Sis_Previsualizar.Pic.FontSize = 9
    For i = 1 To LvPagos.ListItems.Count
        Sis_Previsualizar.Pic.CurrentY = Cy
        Sis_Previsualizar.Pic.CurrentX = Cx
        If CDbl(LvPagos.ListItems(i).SubItems(2)) > 0 Then
            RSet Sp_VPagos = LvPagos.ListItems(i).SubItems(2)
            RSet Sm_Mpagos = LvPagos.ListItems(i).SubItems(1) & _
            IIf(Val(LvPagos.ListItems(i).SubItems(5)) > 0, " NRO " & LvPagos.ListItems(i).SubItems(5) & IIf(Len(LvPagos.ListItems(i).SubItems(6)) > 0, " " & LvPagos.ListItems(i).SubItems(6), ""), "") & _
            " $" & Sp_VPagos
            'Coloca Cx, Cy, Sp_Fagos, False, 10
            
            Sis_Previsualizar.Pic.Print Sm_Mpagos
            Cy = Cy + (Dp_S * 2)
        End If
    Next
    RSet Sm_Mpagos = "--------------------------------------------"
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.Print Sm_Mpagos
    Sis_Previsualizar.Pic.FontBold = True
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
    RSet Sm_Mpagos = "TOTAL PAGO   :$" & TxtAbonos
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.Print Sm_Mpagos
    
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S + Dp_S
    
    
    Sis_Previsualizar.Pic.FontBold = False
    
   ' If CboMPago.ItemData(CboMPago.ListIndex) = 3 Or CboMPago.ItemData(CboMPago.ListIndex) = 4 Then
   ''     Sis_Previsualizar.Pic.CurrentX = Cx
   '     Sis_Previsualizar.Pic.Print "NRO OPERACION:" & txtNroOperacion
   '     Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
   ' End If
    finpago = Sis_Previsualizar.Pic.CurrentY
    
    'Aqui detallaremos los cheques
    If LvCheques.ListItems.Count > 0 Then
        iniciocheque = Sis_Previsualizar.Pic.CurrentY
        Sis_Previsualizar.Pic.CurrentX = Cx
        Sis_Previsualizar.Pic.FontBold = True
        Sis_Previsualizar.Pic.Print "DETALLE DE CHEQUES"
        Sis_Previsualizar.Pic.FontBold = False
        Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
        Sp_FechaCheque = "Fecha"
        Sp_NroCheque = "Nro"
        RSet Sp_ValorCheque = "Valor"
        Sp_Banco = "Banco"
        Sis_Previsualizar.Pic.CurrentX = Cx
        Sis_Previsualizar.Pic.Print Sp_Banco & " " & Sp_NroCheque & " " & Sp_FechaCheque & "       " & Sp_ValorCheque
        Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S - 0.2
        For i = 1 To LvCheques.ListItems.Count
            Sis_Previsualizar.Pic.CurrentX = Cx
            Sp_FechaCheque = LvCheques.ListItems(i)
            Sp_NroCheque = LvCheques.ListItems(i).SubItems(1)
            RSet Sp_ValorCheque = LvCheques.ListItems(i).SubItems(2)
            Sp_Banco = LvCheques.ListItems(i).SubItems(6)
            Sis_Previsualizar.Pic.Print Sp_Banco & " " & Sp_NroCheque & " " & Sp_FechaCheque & "       " & Sp_ValorCheque
            Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S - 0.2
        Next
        Sp_FechaCheque = ""
        Sp_NroCheque = ""
        RSet Sp_ValorCheque = TxtAbonos
        Sp_Banco = ""
        Sis_Previsualizar.Pic.CurrentX = Cx
        Sis_Previsualizar.Pic.FontBold = True
        Sis_Previsualizar.Pic.Print Sp_Banco & " " & Sp_NroCheque & " " & Sp_FechaCheque & "       " & Sp_ValorCheque
        fincheque = Sis_Previsualizar.Pic.CurrentY
        Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
    End If
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.FontBold = True
    posobs = Sis_Previsualizar.Pic.CurrentY
    Sis_Previsualizar.Pic.Print "OBSERVACIONES:"
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
    Sis_Previsualizar.Pic.FontBold = False
     
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.Print TxtComentario
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
    pos = Sis_Previsualizar.Pic.CurrentY
    Sis_Previsualizar.Pic.CurrentY = pos + 1
    Sis_Previsualizar.Pic.CurrentX = Cx
    
    Sis_Previsualizar.Pic.Print "CONTABILIDAD"
        
    Sis_Previsualizar.Pic.CurrentY = pos + 1
    Sis_Previsualizar.Pic.CurrentX = Cx + 6
    Sis_Previsualizar.Pic.Print "V� B� CAJA"
   
   
   
    Sis_Previsualizar.Pic.CurrentY = pos + 1
    Sis_Previsualizar.Pic.CurrentX = Cx + 12
    Sis_Previsualizar.Pic.Print "RUT Y FIRMA"
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
    
    
    
   ' Sis_Previsualizar.pic.DrawMode = 1
    'Sis_Previsualizar.pic.DrawWidth = 3
    
    Sis_Previsualizar.Pic.Line (1.5, 1)-(20, 4.5), , B 'Rectangulo Encabezado y N� Comprobante
    
    Sis_Previsualizar.Pic.Line (1.5, 4.5)-(20, 6.2), , B 'Pagado o Recibido del cliente o proveedor
    
    If LvCheques.ListItems.Count > 0 Then
        Sis_Previsualizar.Pic.Line (1.5, 6.2)-(20, finpago), , B  'Concepto - documentos pagados o recibidos y forma de pago
    Else
        Sis_Previsualizar.Pic.Line (1.5, 6.2)-(20, finpago + 0.15), , B 'Concepto - documentos pagados o recibidos y forma de pago
    End If
    
    Sis_Previsualizar.Pic.Line (13.5, 6.2)-(16.8, posobs), , B  'columnas de los pagos en efectivo
    'Sis_Previsualizar.pic.Line (13.5, 6.2)-(16.8, 9.37), , B  'columnas de los pagos en efectivo
    
    If LvCheques.ListItems.Count > 0 Then
        Sis_Previsualizar.Pic.Line (1.5, iniciocheque)-(20, posobs), , B  'documentos pagados y forma de pago
        Sis_Previsualizar.Pic.Line (13.5, iniciocheque)-(16.8, posobs), , B  'columnas de los pagos con cheques
    End If
    
    Sis_Previsualizar.Pic.Line (1.5, posobs)-(20, posobs + 1.5), , B 'Observaciones
    Sis_Previsualizar.Pic.Line (1.5, posobs + 1.5)-(20, posobs + 1.5 + 2), , B 'Pie del comprobante
        
    pos = Sis_Previsualizar.Pic.CurrentY
    
 '   Sis_Previsualizar.Pic.Height = pos * 0.04

    Sis_Previsualizar.Show 1
End Sub

Private Sub TxtPozo_GotFocus()
    En_Foco TxtPozo
End Sub
Private Sub TxtPozo_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
    If KeyAscii = 39 Then KeyAscii = 0
End Sub
Private Sub TxtPozo_Validate(Cancel As Boolean)
    If Val(TxtPozo) = 0 Then TxtPozo = "0"
    TxtAbonos = NumFormat(TotalizaColumna(LvDetalle, "nuevoabono") + CDbl(TxtPozo))
    TxtPozo = NumFormat(TxtPozo)
        
End Sub

Private Sub TxtTemp_GotFocus()
    On Error Resume Next
    En_Foco Me.ActiveControl
End Sub
Private Sub TxtTemp_KeyPress(KeyAscii As Integer)
    Dim Ip_Letra As Integer
    Ip_Letra = KeyAscii
    KeyAscii = AceptaSoloNumeros(KeyAscii)
    
    If KeyAscii = 13 Then
        For i = 1 To LvPagos.ListItems.Count
            If LvPagos.ListItems(i) = TxtTemp.Tag Then
                If Val(TxtTemp) = 0 Then TxtTemp = 0
                LvPagos.ListItems(i).SubItems(2) = NumFormat(TxtTemp)
                
                If CDbl(TxtTemp) > 0 Then
                    If LvPagos.ListItems(i).SubItems(4) = "SI" Then 'Si es cheque
                        'CHEQUE, SI REQUIERE  DETALLE
                        ctacte_DetalleCheques.Sm_Rut = LvDetalle.ListItems(1).SubItems(3)
                        ctacte_DetalleCheques.TxtRequerido = LvPagos.ListItems(i).SubItems(2)
                        ctacte_DetalleCheques.TxtMonto = TxtTemp
                        
                        ctacte_DetalleCheques.Show 1
                        
                        If Len(SG_codigo) = 0 Then
                            LvPagos.ListItems(i).SubItems(2) = 0
                            TxtTemp = 0
                            TxtTemp.Visible = False
                            LvPagos.SetFocus
                            Exit Sub
                        End If
                        LvPagos.Refresh
                    End If
                    If LvPagos.ListItems(i).SubItems(3) = "SI" And LvPagos.ListItems(i).SubItems(4) = "NO" Then
                        'Requiere detalle pero no es cheque 26 marzo 2012
                        If LvPagos.ListItems(i).SubItems(7) = "SI" Then
                            ctacte_detalle_pago.Sm_Tipo = "TRANSFERENCIA"
                        Else
                            ctacte_detalle_pago.Sm_Tipo = "DEPOSITO"
                        End If
                    
                        
                        
                        ctacte_detalle_pago.Show 1
                        If SG_codigo = Empty Then
                            TxtTemp = 0
                            LvPagos.ListItems(i).SubItems(5) = ""
                            LvPagos.ListItems(i).SubItems(6) = ""
                            LvPagos.ListItems(i).SubItems(2) = "0"
                            LvPagos.ListItems(i).SubItems(9) = 0
                        Else
                            LvPagos.ListItems(i).SubItems(5) = SG_codigo
                            LvPagos.ListItems(i).SubItems(6) = SG_codigo2
                            LvPagos.ListItems(i).SubItems(9) = SG_codigo3
                        End If
                    End If
                
                    If Val(LvPagos.ListItems(i)) = 7777 Then
                        'Sabemos que esta utilizando pozo para abonar
                        'este no debe superar el valor del saldo
                        '10 Nov 2012
                        If CDbl(TxtTemp) > CDbl(LvPagos.ListItems(i).SubItems(10)) Then
                            MsgBox "Saldo insuficiente... " & vbNewLine & "Solo dispone de $" & NumFormat(LvPagos.ListItems(i).SubItems(10)), vbInformation
                            TxtTemp = 0
                            LvPagos.ListItems(i).SubItems(2) = 0
                        End If
                        
                    End If
                    
                    If Val(LvPagos.ListItems(i)) = 5555 Then
                        'Sabemos que esta utilizando CUENTA para saldar
                        '29 Dic 2012
                        'Ahora debemos seleccionar cuenta
                        'Separamos si es cliente, solo ctas de perdida, si es proveedor, solo ctas de ganancia.
                        With BuscarSimple
                            SG_codigo = Empty
                            .Sm_Consulta = "SELECT pla_id, pla_nombre,tpo_nombre,det_nombre " & _
                                           "FROM    con_plan_de_cuentas p,  con_tipo_de_cuenta t,   con_detalle_tipo_cuenta d " & _
                                           "WHERE   p.tpo_id = t.tpo_id AND p.det_id = d.det_id " & _
                                           "/* AND p.tpo_id=" & IIf(Sm_Cli_Pro = "CLI", 3, 4) & "*/ "
                            .Sm_CampoLike = "pla_nombre"
                            .Im_Columnas = 2
                            .Show 1
                            If SG_codigo = Empty Then
                                TxtTemp = 0
                                LvPagos.ListItems(i).SubItems(2) = 0
                            Else
                                LvPagos.ListItems(i).SubItems(11) = Val(SG_codigo)
                            End If
                        End With
                        
                        
                    End If
                    
                Else
                    If LvPagos.ListItems(i).SubItems(4) = "SI" Then 'Si es cheque
                        'Eliminamos el detalle de cheques
                        Me.LvCheques.ListItems.Clear
                    End If
                    If LvPagos.ListItems(i).SubItems(3) = "SI" And LvPagos.ListItems(i).SubItems(4) = "NO" Then
                        LvPagos.ListItems(i).SubItems(5) = ""
                        LvPagos.ListItems(i).SubItems(6) = ""
                    End If
                End If
                
                TxtTemp = 0
                Exit For
            End If
        Next
        TxtTemp.Visible = False
        TxtSumaPagos = NumFormat(TotalizaColumna(LvPagos, "pago"))
        If CDbl(TxtSumaPagos) = CDbl(TxtAbonos) Then
            TxtSumaPagos.ForeColor = vbBlue
        Else
            TxtSumaPagos.ForeColor = vbRed
        End If
        'TxtVuelto = NumFormat(CDbl(TxtSumaPagos) - CDbl(TxtTotal)
        
        On Error Resume Next
        LvPagos.SetFocus
    ElseIf Ip_Letra = 27 Then
        TxtTemp.Visible = False
        
    Else
        KeyAscii = AceptaSoloNumeros(KeyAscii)
    End If
End Sub

Private Sub TxtTemp_LostFocus()
    TxtTemp.Visible = False
End Sub
Private Sub EliminaPrevio(AboId As Long)
    Dim Lp_Nro As Long, Lp_Abo As Long, Sp_Wh As String, Sp_Eli As String, Lp_MovId As Long
    'If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    'If Val(LvDetalle.SelectedItem) = 0 Then Exit Sub
    
    'Comprobamos si es una nota de credito la que se esta abonando
    '19-10-2013
'    For i = 1 To LvMediosDePago.ListItems.Count
'        If Val(LvMediosDePago.ListItems(i).SubItems(5)) = 8888 Then
''            MsgBox "Para eliminar el abono por nota de credito, elimine el documento correspondiente"
'            Exit Sub
'        End If
'
'        If Principal.CmdMenu(8).Visible = True Then
'            If Len(LvMediosDePago.ListItems(i).SubItems(6)) > 0 Then
'                If ConsultaCentralizado(LvMediosDePago.ListItems(i).SubItems(6), Month(LvMediosDePago.ListItems(i)), Year(LvMediosDePago.ListItems(i))) Then
'                    MsgBox "Periodo contable ya ha sido centralizado,  " & vbNewLine & "No puede modificar"
'                    Exit Sub
'                End If
''            End If
 '       End If
        
        
        
        
 '   Next
    'Tambien comprobaremos los asientos en los que esta involucrado el abono
    
    
    
    
    'If Sp_extra = "CLIENTES" Then
        Sp_Eli = "ING"
    'Else
    '    Sp_Eli = "EGR"
    'End If
    'Lp_Nro = LvDetalle.SelectedItem.SubItems(9)
    Lp_Abo = AboId
    'If MsgBox("Esta a punto de eliminar el comprobante Nro " & Lp_Nro & vbNewLine & "�Continuar...?", vbYesNo + vbQuestion) = vbNo Then Exit Sub
    SG_codigo2 = Empty
    sis_InputBox.Caption = "Se requiere autenticaci�n"
    sis_InputBox.FramBox.Caption = "Ingrese clave"
    sis_InputBox.Show 1
    If SG_codigo2 <> Empty Then
        Sql = "SELECT usu_nombre,usu_login " & _
              "FROM sis_usuarios " & _
              "WHERE usu_pwd=MD5('" & UCase(SG_codigo2) & "')"
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
            On Error GoTo errorEliminando
            cn.BeginTrans
            
            
            
            
            Sp_Wh = " WHERE abo_id=" & Lp_Abo
            cn.Execute "DELETE FROM cta_abonos " & Sp_Wh
            cn.Execute "DELETE FROM abo_transferencia " & Sp_Wh
            cn.Execute "DELETE FROM cta_abono_documentos " & Sp_Wh
            
            cn.Execute "DELETE FROM abo_tipos_de_pagos " & Sp_Wh
            
            If Sm_SistemaBanco = "SI" Then
                'Ubicar cheques relacionados, si correponde
                
                
                'O tambien buscar depositos de cheques en cartera 19 Octubre 2013
                
                Sql = "SELECT mov_id " & _
                      "FROM ban_movimientos " & Sp_Wh
                Consulta RsTmp3, Sql
                If RsTmp3.RecordCount > 0 Then
                
                    RsTmp3.MoveFirst
                    Do While Not RsTmp3.EOF
                        Sql = "DELETE FROM com_con_multicuenta " & _
                                "WHERE dcu_tipo='CHEQUE' AND id_unico IN(" & _
                                        "SELECT che_id " & _
                                        "FROM ban_cheques " & _
                                        "WHERE mov_id=" & RsTmp3!mov_id & ")"
                        cn.Execute Sql
                            
                        
                    
                        cn.Execute "UPDATE ban_cheques SET che_estado='SIN EMITIR',che_concepto='',che_destinatario=''," & _
                                          "che_valor=0,pla_id=0," & _
                                          "mes_contable=0,ano_contable=0,mov_id=0 " & _
                                   "WHERE mov_id=" & RsTmp3!mov_id
                    
                        RsTmp3.MoveNext
                    Loop
                End If
                cn.Execute "DELETE FROM ban_movimientos " & Sp_Wh
                
                If Sp_extra = "CLIENTES" Then
                    For i = 1 To LvCheques.ListItems.Count
                        cn.Execute "DELETE FROM ban_movimientos WHERE mov_id=" & LvCheques.ListItems(i).SubItems(6)
                    Next
                End If
                
            End If
            cn.Execute "DELETE FROM abo_cheques " & Sp_Wh
            
            
            Sql = "INSERT INTO sis_eliminacion_documentos (id,eli_fecha,eli_motivo,usu_login,eli_ven_com)" & _
                  "VALUES(" & Lp_Abo & ",'" & Fql(Date) & "','" & "ELIMINACION COMPROBANTE " & Sp_extra & "','" & RsTmp!usu_login & "','" & Sp_Eli & "')"
            cn.Execute Sql
            
            cn.CommitTrans
         '   MsgBox RsTmp!usu_nombre & " ha eliminado el comprobante Nro " & Lp_Nro
    '        Me.LvDocumentos.ListItems.Clear
    '        Me.LvCheques.ListItems.Clear
    '        Me.LvMediosDePago.ListItems.Clear
     '       cmdConsultar_Click
        Else
            MsgBox "Usuario no encontrado...", vbInformation
            
        End If
    End If
    Exit Sub
        
errorEliminando:
    cn.RollbackTrans
    MsgBox "Error Eliminando abono..." & vbNewLine & Err.Number & vbNewLine & Err.Description, vbInformation
End Sub
