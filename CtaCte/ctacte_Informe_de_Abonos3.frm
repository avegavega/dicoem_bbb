VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{DA729E34-689F-49EA-A856-B57046630B73}#1.0#0"; "progressbar-xp.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form ctacte_Informe_de_Abonos 
   ClientHeight    =   8895
   ClientLeft      =   2445
   ClientTop       =   1215
   ClientWidth     =   15135
   LinkTopic       =   "Form1"
   ScaleHeight     =   8895
   ScaleWidth      =   15135
   Begin VB.PictureBox PicLoad 
      Appearance      =   0  'Flat
      BackColor       =   &H00000000&
      ForeColor       =   &H80000008&
      Height          =   2025
      Left            =   585
      ScaleHeight     =   1995
      ScaleWidth      =   4875
      TabIndex        =   34
      Top             =   5565
      Visible         =   0   'False
      Width           =   4905
      Begin VB.Timer TimLoad 
         Interval        =   100
         Left            =   315
         Top             =   1470
      End
   End
   Begin VB.Frame FraProgreso 
      Caption         =   "Progreso exportaci�n"
      Height          =   795
      Left            =   1320
      TabIndex        =   31
      Top             =   2385
      Visible         =   0   'False
      Width           =   11430
      Begin Proyecto2.XP_ProgressBar BarraProgreso 
         Height          =   330
         Left            =   150
         TabIndex        =   32
         Top             =   315
         Width           =   11130
         _ExtentX        =   19632
         _ExtentY        =   582
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BrushStyle      =   0
         Color           =   16777088
         Scrolling       =   1
         ShowText        =   -1  'True
      End
   End
   Begin VB.PictureBox Picture3 
      BackColor       =   &H00C0C000&
      Height          =   1725
      Left            =   7395
      ScaleHeight     =   1665
      ScaleWidth      =   7635
      TabIndex        =   20
      Top             =   4920
      Width           =   7695
      Begin MSComctlLib.ListView LvMediosDePago 
         Height          =   1365
         Left            =   60
         TabIndex        =   21
         ToolTipText     =   "Estas son las formas de pago"
         Top             =   240
         Width           =   7470
         _ExtentX        =   13176
         _ExtentY        =   2408
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   0   'False
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   7
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "T1000"
            Text            =   "Fecha"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "T1000"
            Text            =   "Tipo de pago"
            Object.Width           =   4762
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   2
            Object.Tag             =   "N100"
            Text            =   "Valor"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   3
            Object.Tag             =   "T1000"
            Text            =   "Nro Transaccion"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   4
            Object.Tag             =   "T1000"
            Text            =   "Mas info."
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Object.Tag             =   "N109"
            Text            =   "Medio de Pago ID"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   6
            Object.Tag             =   "T1000"
            Text            =   "Asientos verificar centralizacion"
            Object.Width           =   0
         EndProperty
      End
      Begin VB.Label Label3 
         BackStyle       =   0  'Transparent
         Caption         =   "Forma de pago"
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   105
         TabIndex        =   22
         ToolTipText     =   "Formas de pago"
         Top             =   15
         Width           =   1515
      End
   End
   Begin VB.CommandButton CmdEliminar 
      Caption         =   "Eliminar Comprobante con sus abonos"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   3450
      TabIndex        =   19
      Top             =   8400
      Width           =   3855
   End
   Begin MSComDlg.CommonDialog Dialogo 
      Left            =   6450
      Top             =   105
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.CommandButton CmdComprobante 
      Caption         =   "Imprimir Comprobante"
      Height          =   330
      Left            =   165
      TabIndex        =   18
      Top             =   8400
      Width           =   3210
   End
   Begin VB.Frame Frame3 
      Caption         =   "Filtro"
      Height          =   1170
      Left            =   210
      TabIndex        =   9
      Top             =   270
      Width           =   14775
      Begin VB.TextBox TxtFono 
         BackColor       =   &H00E0E0E0&
         Height          =   315
         Left            =   5715
         Locked          =   -1  'True
         TabIndex        =   27
         Top             =   630
         Width           =   1245
      End
      Begin VB.CommandButton cmdBtnRut 
         Caption         =   "Rut"
         Height          =   225
         Left            =   390
         TabIndex        =   26
         Top             =   420
         Width           =   585
      End
      Begin VB.TextBox txtCliente 
         BackColor       =   &H00E0E0E0&
         Height          =   315
         Left            =   1935
         Locked          =   -1  'True
         TabIndex        =   25
         Top             =   630
         Width           =   3795
      End
      Begin VB.TextBox TxtRut 
         BackColor       =   &H0080FF80&
         Height          =   315
         Left            =   390
         TabIndex        =   24
         ToolTipText     =   "Ingrese el RUT sin puntos ni guion."
         Top             =   630
         Width           =   1560
      End
      Begin VB.CommandButton cmdSinRut 
         Caption         =   "X"
         Height          =   210
         Left            =   990
         TabIndex        =   23
         ToolTipText     =   "Limpiar"
         Top             =   420
         Width           =   180
      End
      Begin VB.Frame Frame4 
         Caption         =   "Informacion"
         Height          =   885
         Left            =   8295
         TabIndex        =   10
         Top             =   240
         Width           =   6075
         Begin VB.OptionButton Option1 
            Caption         =   "Todas las fechas"
            Height          =   240
            Left            =   300
            TabIndex        =   13
            Top             =   255
            Value           =   -1  'True
            Width           =   1740
         End
         Begin VB.OptionButton Option2 
            Caption         =   "Seleccionar fechas"
            Height          =   240
            Left            =   300
            TabIndex        =   12
            Top             =   525
            Width           =   1725
         End
         Begin VB.CommandButton cmdConsultar 
            Caption         =   "&Consultar"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   420
            Left            =   4515
            TabIndex        =   11
            Top             =   270
            Width           =   1365
         End
         Begin ACTIVESKINLibCtl.SkinLabel skIni 
            Height          =   240
            Left            =   1980
            OleObjectBlob   =   "ctacte_Informe_de_Abonos3.frx":0000
            TabIndex        =   14
            Top             =   225
            Width           =   795
         End
         Begin MSComCtl2.DTPicker DTInicio 
            Height          =   300
            Left            =   2820
            TabIndex        =   15
            Top             =   210
            Width           =   1305
            _ExtentX        =   2302
            _ExtentY        =   529
            _Version        =   393216
            Enabled         =   0   'False
            Format          =   83230721
            CurrentDate     =   40628
         End
         Begin MSComCtl2.DTPicker DtHasta 
            Height          =   285
            Left            =   2820
            TabIndex        =   16
            Top             =   510
            Width           =   1305
            _ExtentX        =   2302
            _ExtentY        =   503
            _Version        =   393216
            Enabled         =   0   'False
            Format          =   83230721
            CurrentDate     =   40628
         End
         Begin ACTIVESKINLibCtl.SkinLabel skFin 
            Height          =   240
            Left            =   1980
            OleObjectBlob   =   "ctacte_Informe_de_Abonos3.frx":0068
            TabIndex        =   17
            Top             =   495
            Width           =   795
         End
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel16 
         Height          =   195
         Left            =   1950
         OleObjectBlob   =   "ctacte_Informe_de_Abonos3.frx":00D0
         TabIndex        =   28
         Top             =   450
         Width           =   540
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   195
         Left            =   5730
         OleObjectBlob   =   "ctacte_Informe_de_Abonos3.frx":013A
         TabIndex        =   29
         Top             =   450
         Width           =   540
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Pagos/Abonos"
      Height          =   3360
      Left            =   165
      TabIndex        =   7
      Top             =   1515
      Width           =   14880
      Begin VB.CommandButton CmdExportar 
         Caption         =   "Exportar"
         Height          =   270
         Left            =   195
         TabIndex        =   33
         Top             =   3015
         Width           =   1515
      End
      Begin VB.TextBox TxtTotal 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   11640
         Locked          =   -1  'True
         TabIndex        =   30
         TabStop         =   0   'False
         Top             =   3030
         Width           =   2145
      End
      Begin MSComctlLib.ListView LvDetalle 
         Height          =   2715
         Left            =   210
         TabIndex        =   8
         ToolTipText     =   "Ultimos Rut con movimientos"
         Top             =   285
         Width           =   14595
         _ExtentX        =   25744
         _ExtentY        =   4789
         View            =   3
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   10
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "T1200"
            Text            =   "Abo Id"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Fecha"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Rut"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Object.Tag             =   "T2000"
            Text            =   "Nombre"
            Object.Width           =   6703
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Object.Tag             =   "T1500"
            Text            =   "Sucursal"
            Object.Width           =   4762
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Object.Tag             =   "T1300"
            Text            =   "Forma de Pago"
            Object.Width           =   2646
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   6
            Key             =   "total"
            Object.Tag             =   "N100"
            Text            =   "Monto"
            Object.Width           =   2469
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   7
            Object.Tag             =   "T1500"
            Text            =   "Usuario"
            Object.Width           =   2646
         EndProperty
         BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   8
            Object.Tag             =   "N109"
            Text            =   "Id forma pago"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   9
            Text            =   "Nro Comprobante"
            Object.Width           =   2646
         EndProperty
      End
   End
   Begin VB.PictureBox Picture1 
      BackColor       =   &H00C0C000&
      Height          =   3345
      Left            =   150
      ScaleHeight     =   3285
      ScaleWidth      =   7110
      TabIndex        =   4
      Top             =   4935
      Width           =   7170
      Begin MSComctlLib.ListView LvDocumentos 
         Height          =   2895
         Left            =   15
         TabIndex        =   6
         ToolTipText     =   "Documentos pagados con el abono"
         Top             =   225
         Width           =   7020
         _ExtentX        =   12383
         _ExtentY        =   5106
         View            =   3
         LabelWrap       =   0   'False
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   8
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "N100"
            Text            =   "Id Unico"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "T1100"
            Text            =   "Emision"
            Object.Width           =   1940
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "T2000"
            Text            =   "Documento"
            Object.Width           =   3175
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Object.Tag             =   "N109"
            Text            =   "Nro Doc"
            Object.Width           =   1676
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   4
            Object.Tag             =   "N100"
            Text            =   "Valor Doc"
            Object.Width           =   1852
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   5
            Object.Tag             =   "N100"
            Text            =   "Abono"
            Object.Width           =   1852
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   6
            Object.Tag             =   "N100"
            Text            =   "Saldo"
            Object.Width           =   1852
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   7
            Object.Tag             =   "N109"
            Text            =   "Id Centralizacion"
            Object.Width           =   2540
         EndProperty
      End
      Begin VB.Label Label2 
         BackStyle       =   0  'Transparent
         Caption         =   "Documentos"
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   105
         TabIndex        =   5
         Top             =   45
         Width           =   1515
      End
   End
   Begin VB.PictureBox FrmDetalles 
      BackColor       =   &H00C0C000&
      Height          =   1590
      Left            =   7395
      ScaleHeight     =   1530
      ScaleWidth      =   7635
      TabIndex        =   1
      ToolTipText     =   "Detalle de cheques"
      Top             =   6690
      Width           =   7695
      Begin MSComctlLib.ListView LVCheques 
         Height          =   1155
         Left            =   90
         TabIndex        =   2
         ToolTipText     =   "Este es el detalle de cheque(s)"
         Top             =   255
         Width           =   7470
         _ExtentX        =   13176
         _ExtentY        =   2037
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   0   'False
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   8
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "T1000"
            Text            =   "Banco"
            Object.Width           =   2646
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "T1000"
            Text            =   "Plaza"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "N109"
            Text            =   "Nro Cheque"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   3
            Object.Tag             =   "T1000"
            Text            =   "Fecha"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   4
            Object.Tag             =   "N100"
            Text            =   "Monto"
            Object.Width           =   1940
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Object.Tag             =   "T1200"
            Text            =   "Estado"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   6
            Object.Tag             =   "N109"
            Text            =   "Mov_id"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   7
            Object.Tag             =   "T1000"
            Text            =   "Plaza"
            Object.Width           =   2540
         EndProperty
      End
      Begin VB.Label Label1 
         BackStyle       =   0  'Transparent
         Caption         =   "Detalle de cheques"
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   105
         TabIndex        =   3
         Top             =   45
         Width           =   1515
      End
   End
   Begin VB.CommandButton cmdSalir 
      Cancel          =   -1  'True
      Caption         =   "&Salir"
      Height          =   405
      Left            =   13740
      TabIndex        =   0
      Top             =   8460
      Width           =   1350
   End
   Begin VB.Timer Timer1 
      Interval        =   30
      Left            =   1740
      Top             =   7800
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   900
      OleObjectBlob   =   "ctacte_Informe_de_Abonos3.frx":01A0
      Top             =   7710
   End
   Begin Proyecto2.XP_ProgressBar ProLoad 
      Height          =   630
      Left            =   8370
      TabIndex        =   35
      Top             =   8325
      Width           =   4530
      _ExtentX        =   7990
      _ExtentY        =   1111
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BrushStyle      =   0
      Color           =   16750899
      Scrolling       =   6
      ShowText        =   -1  'True
   End
End
Attribute VB_Name = "ctacte_Informe_de_Abonos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim Sm_FiltroNombre As String
Dim sm_FiltroFecha As String
Dim Sp_Fagos As String, Sp_VPagos As String * 14, Sm_Mpagos As String * 59
Dim Sm_SistemaBanco As String



Private Sub cmdBtnRut_Click()
    ClienteEncontrado = False
    SG_codigo = Empty
    
    If Sp_extra = "CLIENTES" Then
        BuscaCliente.Show 1
    Else
        BuscaProveedor.Show 1
    End If
    
    TxtRut = SG_codigo
    If SG_codigo <> Empty Then
        TxtRut_Validate (True)
    End If
End Sub

Private Sub CmdComprobante_Click()
   On Error GoTo ImpresoraError
            
    Previsualiza
    If SG_codigo = "print" Then
        Dialogo.CancelError = True
        Dialogo.ShowPrinter
        'ImprimeComprobante
        PrevisualizaPrint
    End If

    Exit Sub
    
ImpresoraError:
MsgBox Err.Description & Err.Number
End Sub
Private Sub ImprimeComprobante()
   Dim Cx As Double, Cy As Double, Sp_Fecha As String, Ip_Mes As Integer, Dp_S As Double, Sp_Letras As String
    Dim Sp_Neto As String * 12, Sp_IVA As String * 12
    
    Dim Sp_Nro_Comprobante As String, Sp_Imprime_Cheque As String * 2
    
    Dim Sp_Empresa As String
    Dim Sp_Giro As String
    Dim Sp_Direccion As String
    
    
    Dim Sp_Nombre As String * 45
    Dim Sp_Rut As String * 15
    Dim Sp_Concepto As String
    
    Dim Sp_Nro_Doc As String * 12
    Dim Sp_Documento As String * 25
    Dim Sp_Tdocumento  As String * 12
    Dim Sp_Valor As String * 15
    Dim Sp_Saldo As String * 15
    Dim Sp_Fpago As String * 24
    Dim Sp_Total As String * 12
    
    Dim Sp_Banco As String * 15
    Dim Sp_NroCheque As String * 10
    Dim Sp_FechaCheque As String * 10
    Dim Sp_ValorCheque As String * 15
    Dim Sp_SumaCheque As String * 15
    
    Dim Sp_Observacion As String * 80
    
    Dim Sp_PlazaFechas As String
    Sp_Imprime_Cheque = "NO"
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    
    Sp_Nro_Comprobante = LvDetalle.SelectedItem.SubItems(9)
    
    If Val(Sp_Nro_Comprobante) = 0 Then Exit Sub
    
    Sql = "SELECT giro,direccion,ciudad,emp_emite_cheque_en_comprobante_pago cheque " & _
         "FROM sis_empresas " & _
         "WHERE rut='" & SP_Rut_Activo & "'"
    Consulta RsTmp2, Sql
    If RsTmp2.RecordCount > 0 Then
        Sp_Giro = RsTmp2!giro
        Sp_Direccion = RsTmp2!direccion & " - " & RsTmp2!ciudad
        Sp_Imprime_Cheque = RsTmp2!cheque
    End If
    
    
    Printer.ScaleMode = vbCentimeters
    'Printer.BackColor = vbWhite
    'Printer.AutoRedraw = True
    Printer.DrawWidth = 1
    Printer.DrawMode = 1
    
    Printer.FontName = "Courier New"
    Printer.FontSize = 10
   
    Cx = 2
    Cy = 1.9
    Dp_S = 0.17
    Printer.CurrentX = Cx
    Printer.CurrentY = Cy
    Printer.FontSize = 16  'tama�o de letra
    Printer.FontBold = True
    
    '**
    Sis_Previsualizar.Pic.PaintPicture LoadPicture(App.Path & "\REDMAROK.jpg"), Cx + 0.62, 1
    Printer.CurrentY = Printer.CurrentY + Dp_S + Dp_S
    Printer.CurrentY = Printer.CurrentY + Dp_S + Dp_S
    '**
    
    Printer.Print SP_Empresa_Activa
        
    
    Printer.CurrentX = Cx
    Printer.CurrentY = Printer.CurrentY
  
    Printer.FontSize = 10
  
    pos = Printer.CurrentY
    Printer.Print "R.U.T.   :" & SP_Rut_Activo
    Printer.CurrentY = Printer.CurrentY
    
    Sm_Cli_Pro = Mid(Sp_extra, 1, 3)
    Printer.CurrentY = pos
    Printer.CurrentX = Cx + 11
    If Sm_Cli_Pro = "CLI" Then
         Printer.Print "COMPROBANTE DE INGRESO N�: " & Sp_Nro_Comprobante
    Else
         Printer.Print "COMPROBANTE DE EGRESO N�: " & Sp_Nro_Comprobante
    End If
    Printer.CurrentY = Printer.CurrentY
 
 
    Printer.FontBold = False
    Printer.FontSize = 10
    Printer.CurrentX = Cx
    Printer.Print "GIRO     :" & Sp_Giro
    Printer.CurrentY = Printer.CurrentY
    
  
    Printer.FontSize = 10
    Printer.CurrentX = Cx
    Printer.Print "DIRECCION:"; Sp_Direccion
    Printer.CurrentY = Printer.CurrentY + Dp_S
    
    Printer.CurrentX = Cx
    Printer.FontBold = True
    If Sm_Cli_Pro = "CLI" Then
         Printer.Print "Recibi de"
    Else
         Printer.Print "Pagado a "
    End If
    Printer.FontBold = False
    Printer.CurrentY = Printer.CurrentY + Dp_S
    Printer.FontSize = 10
    pos = Printer.CurrentY
    Printer.CurrentX = Cx
    Printer.Print "Nombre:" & LvDetalle.SelectedItem.SubItems(3)
    
    Printer.CurrentY = pos
    Printer.CurrentX = Cx + 14
    Printer.Print "Fecha:" & LvDetalle.SelectedItem.SubItems(1)
    Printer.CurrentY = Printer.CurrentY + Dp_S
    
    
    Printer.CurrentX = Cx
    Printer.Print "RUT   :" & LvDetalle.SelectedItem.SubItems(2)
    Printer.CurrentY = Printer.CurrentY + Dp_S
    
    Printer.CurrentX = Cx
    Printer.FontBold = True
    Printer.Print "CONCEPTO :"
    Printer.CurrentY = Printer.CurrentY + Dp_S
    
       
    'Aqui agregamos los documentos a los que se abonara
    'Printer.FontSize = 8
    Printer.FontBold = False
    Sp_Nro_Doc = "Nro"
    Sp_Documento = "Documento"
    RSet Sp_Tdocumento = "Valor Docto."
    RSet Sp_Valor = "Abono"
    RSet Sp_Saldo = "Saldo"
    Printer.CurrentX = Cx
    Printer.Print Sp_Nro_Doc & " " & Sp_Documento & " " & Sp_Tdocumento & " " & Sp_Valor & " " & Sp_Saldo
    Printer.CurrentY = Printer.CurrentY + Dp_S - 0.1
    For i = 1 To LvDocumentos.ListItems.Count
        Sp_Nro_Doc = LvDocumentos.ListItems(i).SubItems(3)
        Sp_Documento = LvDocumentos.ListItems(i).SubItems(2)
        RSet Sp_Tdocumento = LvDocumentos.ListItems(i).SubItems(4)
        RSet Sp_Valor = LvDocumentos.ListItems(i).SubItems(5)
        RSet Sp_Saldo = LvDocumentos.ListItems(i).SubItems(6)
        Printer.CurrentX = Cx
        Printer.Print Sp_Nro_Doc & " " & Sp_Documento & " " & Sp_Tdocumento & " " & Sp_Valor & " " & Sp_Saldo
        Printer.CurrentY = Printer.CurrentY + Dp_S - 0.2
    Next
    Printer.FontSize = 10
    Printer.CurrentY = Printer.CurrentY + 0.2
    Printer.CurrentX = Cx
    Printer.FontBold = True
    Sp_Fpago = LvDetalle.SelectedItem.SubItems(5)
    pos = Printer.CurrentY
    Printer.Print "FORMA DE PAGO " ' & Sp_Fpago
    Printer.CurrentX = Cx
    Printer.FontBold = True
    'Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
    'S� 'is_Previsualizar.Pic.Print "FORMA DE  PAGO   "
    
    Printer.CurrentX = Cx
    Printer.FontBold = False
    Printer.CurrentY = printerCurrentY + Dp_S
    Cy = Printer.CurrentY
    Printer.FontSize = 9
    For i = 1 To Me.LvMediosDePago.ListItems.Count
        Printer.CurrentY = Cy
        Printer.CurrentX = Cx
        If CDbl(LvMediosDePago.ListItems(i).SubItems(2)) > 0 Then
            RSet Sp_VPagos = LvMediosDePago.ListItems(i).SubItems(2)
            RSet Sm_Mpagos = LvMediosDePago.ListItems(i).SubItems(1) & _
            IIf(Val(LvMediosDePago.ListItems(i).SubItems(3)) > 0, " NRO " & LvMediosDePago.ListItems(i).SubItems(3) & IIf(Len(LvMediosDePago.ListItems(i).SubItems(4)) > 0, " " & LvMediosDePago.ListItems(i).SubItems(4), ""), "") & _
            " $" & Sp_VPagos
            'Coloca Cx, Cy, Sp_Fagos, False, 10
            
            Printer.Print Sm_Mpagos
            Cy = Cy + (Dp_S * 2)
        End If
    Next
    RSet Sm_Mpagos = "--------------------------------------------"
    Printer.CurrentX = Cx
    Printer.Print Sm_Mpagos
    Printer.FontBold = True
    Printer.CurrentY = Printer.CurrentY + Dp_S
    RSet Sm_Mpagos = "TOTAL PAGO   :$" & LvDetalle.SelectedItem.SubItems(6)
    Printer.CurrentX = Cx
    Printer.Print Sm_Mpagos
    
    Printer.CurrentY = Printer.CurrentY + Dp_S + Dp_S
    
    
    Printer.FontBold = False
    finpago = Printer.CurrentY
    
    'Aqui detallaremos los cheques
    If LvCheques.ListItems.Count > 0 Then
        iniciocheque = Printer.CurrentY
        Printer.CurrentX = Cx
        Printer.FontBold = True
        Printer.Print "DETALLE DE CHEQUES"
        Printer.FontBold = False
        Printer.CurrentY = Printer.CurrentY + Dp_S
        Sp_FechaCheque = "Fecha"
        Sp_NroCheque = "Nro"
        RSet Sp_ValorCheque = "Valor"
        Sp_Banco = "Banco"
        Printer.CurrentX = Cx
        Printer.Print Sp_Banco & " " & Sp_NroCheque & " " & Sp_FechaCheque & "       " & Sp_ValorCheque
        Printer.CurrentY = Printer.CurrentY + Dp_S - 0.2
        For i = 1 To LvCheques.ListItems.Count
            Printer.CurrentX = Cx
            Sp_FechaCheque = LvCheques.ListItems(i).SubItems(3)
            Sp_NroCheque = LvCheques.ListItems(i).SubItems(2)
            RSet Sp_ValorCheque = LvCheques.ListItems(i).SubItems(4)
            Sp_Banco = LvCheques.ListItems(i)
            Printer.Print Sp_Banco & " " & Sp_NroCheque & " " & Sp_FechaCheque & "       " & Sp_ValorCheque
            Printer.CurrentY = Printer.CurrentY + Dp_S - 0.2
        Next
        Sp_FechaCheque = ""
        Sp_NroCheque = ""
        RSet Sp_ValorCheque = LvDetalle.SelectedItem.SubItems(6)
        Sp_Banco = ""
        Printer.CurrentX = Cx
        Printer.FontBold = True
        Printer.Print Sp_Banco & " " & Sp_NroCheque & " " & Sp_FechaCheque & "       " & Sp_ValorCheque
        fincheque = Printer.CurrentY
        Printer.CurrentY = Printer.CurrentY + Dp_S
    End If
    Printer.CurrentY = Printer.CurrentY + Dp_S
    Printer.CurrentX = Cx
    Printer.FontBold = True
    posobs = Printer.CurrentY
    Printer.Print "OBSERVACIONES:"
    Printer.CurrentY = Printer.CurrentY + Dp_S
    Printer.FontBold = False
     
    Printer.CurrentX = Cx
    Printer.Print "" '; TxtComentario
    Printer.CurrentY = Printer.CurrentY + Dp_S
    pos = Printer.CurrentY
    Printer.CurrentY = pos + 1
    Printer.CurrentX = Cx
    
    Printer.Print "CONTABILIDAD"
        
    Printer.CurrentY = pos + 1
    Printer.CurrentX = Cx + 6
    Printer.Print "V� B� CAJA"
   
   
   
    Printer.CurrentY = pos + 1
    Printer.CurrentX = Cx + 12
    Printer.Print "NOMBRE, RUT Y FIRMA"
    Printer.CurrentY = Printer.CurrentY + Dp_S
    
    Printer.CurrentY = Printer.CurrentY - (Dp_S * 6)
    Printer.CurrentX = Cx + 12
    Printer.Print "RECIBI CONFORME"
    Printer.CurrentY = Printer.CurrentY + Dp_S
    
   ' printer.DrawMode = 1
    'printer.DrawWidth = 3
    
    Printer.Line (1.5, 1)-(20, 4.5), , B 'Rectangulo Encabezado y N� Comprobante
    
    Printer.Line (1.5, 4.5)-(20, 6.2), , B 'Pagado o Recibido del cliente o proveedor
    
    If LvCheques.ListItems.Count > 0 Then
        Printer.Line (1.5, 6.2)-(20, finpago), , B  'Concepto - documentos pagados o recibidos y forma de pago
    Else
        Printer.Line (1.5, 6.2)-(20, finpago + 0.15), , B 'Concepto - documentos pagados o recibidos y forma de pago
    End If
    
    Printer.Line (13.5, 6.2)-(16.8, posobs), , B  'columnas de los pagos en efectivo
    Printer.Line (13.5, 6.2)-(16.8, 9.37), , B  'columnas de los pagos en efectivo
    
    If LvCheques.ListItems.Count > 0 Then
        Printer.Line (1.5, iniciocheque)-(20, posobs), , B  'documentos pagados y forma de pago
        Printer.Line (13.5, iniciocheque)-(16.8, posobs), , B  'columnas de los pagos con cheques
    End If
    
    Printer.Line (1.5, posobs)-(20, posobs + 1.5), , B 'Observaciones
    Printer.Line (1.5, posobs + 1.5)-(20, posobs + 1.5 + 2), , B 'Pie del comprobante
        
    pos = Printer.CurrentY
    
    If Sp_Imprime_Cheque = "SI" Then
        Printer.CurrentY = 23 'monto
        Printer.CurrentX = 15
        Printer.Print LvCheques.ListItems(1).SubItems(4)
        
              
        RSet Sp_PlazaFechas = LvCheques.ListItems(1).SubItems(7) & ", " & LvCheques.ListItems(1).SubItems(3)
        Printer.CurrentY = 24 'monto
        Printer.CurrentX = 15
        Printer.Print Sp_PlazaFechas
        
        Printer.CurrentX = 24 'beneficiacrio
        Printer.CurrentX = 5
        pos = Printer.CurrentY
        Printer.Print LvDetalle.SelectedItem.SubItems(3)
        
        Printer.CurrentX = 25 ' numero en letras
        Printer.CurrentX = 5
        pos = Printer.CurrentY
        Printer.Print BrutoAletras(CDbl(LvDetalle.SelectedItem.SubItems(3)), True)
  
        
    
    End If
    
    
    
 

    
    Printer.NewPage
    Printer.EndDoc

End Sub






Private Sub Previsualiza()
   Dim Cx As Double, Cy As Double, Sp_Fecha As String, Ip_Mes As Integer, Dp_S As Double, Sp_Letras As String
    Dim Sp_Neto As String * 12, Sp_IVA As String * 12
    
    Dim Sp_Nro_Comprobante As String
    
    Dim Sp_Empresa As String
    Dim Sp_Giro As String
    Dim Sp_Direccion As String
    
    
    Dim Sp_Nombre As String * 45
    Dim Sp_Rut As String * 15
    Dim Sp_Concepto As String
    
    Dim Sp_Nro_Doc As String * 12
    Dim Sp_Documento As String * 25
    Dim Sp_Tdocumento  As String * 12
    Dim Sp_Valor As String * 15
    Dim Sp_Saldo As String * 15
    Dim Sp_Fpago As String * 24
    Dim Sp_Total As String * 12
    
    Dim Sp_Banco As String * 15
    Dim Sp_NroCheque As String * 10
    Dim Sp_FechaCheque As String * 10
    Dim Sp_ValorCheque As String * 15
    Dim Sp_SumaCheque As String * 15
    
    Dim Sp_Observacion As String * 80
    
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    
    Sp_Nro_Comprobante = LvDetalle.SelectedItem.SubItems(9)
    If Val(Sp_Nro_Comprobante) = 0 Then Exit Sub
    
    Sql = "SELECT giro,direccion,ciudad " & _
         "FROM sis_empresas " & _
         "WHERE rut='" & SP_Rut_Activo & "'"
    Consulta RsTmp2, Sql
    If RsTmp2.RecordCount > 0 Then
        Sp_Giro = RsTmp2!giro
        Sp_Direccion = RsTmp2!direccion & " - " & RsTmp2!ciudad
    End If
    

    
    Sis_Previsualizar.Pic.ScaleMode = vbCentimeters
    Sis_Previsualizar.Pic.BackColor = vbWhite
    Sis_Previsualizar.Pic.AutoRedraw = True
    Sis_Previsualizar.Pic.DrawWidth = 1
    Sis_Previsualizar.Pic.DrawMode = 1
    
    Sis_Previsualizar.Pic.FontName = "Courier New"
    Sis_Previsualizar.Pic.FontSize = 10
   
    Cx = 2
    Cy = 1.9
    Dp_S = 0.17
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.CurrentY = Cy
    Sis_Previsualizar.Pic.FontSize = 16  'tama�o de letra
    Sis_Previsualizar.Pic.FontBold = True
    
    '**
    On Error Resume Next
    Sis_Previsualizar.Pic.PaintPicture LoadPicture(App.Path & "\REDMAROK.jpg"), Cx + 0.62, 1
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S + Dp_S
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S + Dp_S
    '**
    
    Sis_Previsualizar.Pic.Print SP_Empresa_Activa
        
    
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY
  
    Sis_Previsualizar.Pic.FontSize = 10
  
    pos = Sis_Previsualizar.Pic.CurrentY
    Sis_Previsualizar.Pic.Print "R.U.T.   :" & SP_Rut_Activo
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY
    
    Sm_Cli_Pro = Mid(Sp_extra, 1, 3)
    Sis_Previsualizar.Pic.CurrentY = pos
    Sis_Previsualizar.Pic.CurrentX = Cx + 11
    If Sm_Cli_Pro = "CLI" Then
         Sis_Previsualizar.Pic.Print "COMPROBANTE DE INGRESO N�: " & Sp_Nro_Comprobante
    Else
         Sis_Previsualizar.Pic.Print "COMPROBANTE DE EGRESO N�: " & Sp_Nro_Comprobante
    End If
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY
 
 
    Sis_Previsualizar.Pic.FontBold = False
    Sis_Previsualizar.Pic.FontSize = 10
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.Print "GIRO     :" & Sp_Giro
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY
    
  
    Sis_Previsualizar.Pic.FontSize = 10
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.Print "DIRECCION:"; Sp_Direccion
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
    
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.FontBold = True
    If Sm_Cli_Pro = "CLI" Then
         Sis_Previsualizar.Pic.Print "Recibi de"
    Else
         Sis_Previsualizar.Pic.Print "Pagado a "
    End If
    Sis_Previsualizar.Pic.FontBold = False
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
    Sis_Previsualizar.Pic.FontSize = 10
    pos = Sis_Previsualizar.Pic.CurrentY
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.Print "Nombre:" & LvDetalle.SelectedItem.SubItems(3)
    
    Sis_Previsualizar.Pic.CurrentY = pos
    Sis_Previsualizar.Pic.CurrentX = Cx + 14
    Sis_Previsualizar.Pic.Print "Fecha:" & LvDetalle.SelectedItem.SubItems(1)
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
    
    
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.Print "RUT   :" & LvDetalle.SelectedItem.SubItems(2)
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
    
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.FontBold = True
    Sis_Previsualizar.Pic.Print "CONCEPTO :"
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
    
       
    'Aqui agregamos los documentos a los que se abonara
    Printer.FontSize = 8
    Sis_Previsualizar.Pic.FontBold = False
    Sp_Nro_Doc = "Nro"
    Sp_Documento = "Documento"
    RSet Sp_Tdocumento = "Valor Docto."
    RSet Sp_Valor = "Abono"
    RSet Sp_Saldo = "Saldo"
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.Print Sp_Nro_Doc & " " & Sp_Documento & " " & Sp_Tdocumento & " " & Sp_Valor & " " & Sp_Saldo
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S - 0.1
    For i = 1 To LvDocumentos.ListItems.Count
        Sp_Nro_Doc = LvDocumentos.ListItems(i).SubItems(3)
        Sp_Documento = LvDocumentos.ListItems(i).SubItems(2)
        RSet Sp_Tdocumento = LvDocumentos.ListItems(i).SubItems(4)
        RSet Sp_Valor = LvDocumentos.ListItems(i).SubItems(5)
        RSet Sp_Saldo = LvDocumentos.ListItems(i).SubItems(6)
        Sis_Previsualizar.Pic.CurrentX = Cx
        Sis_Previsualizar.Pic.Print Sp_Nro_Doc & " " & Sp_Documento & " " & Sp_Tdocumento & " " & Sp_Valor & " " & Sp_Saldo
        Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S - 0.2
    Next
    Printer.FontSize = 10
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + 0.2
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.FontBold = True
    Sp_Fpago = LvDetalle.SelectedItem.SubItems(5)
    pos = Sis_Previsualizar.Pic.CurrentY
    Sis_Previsualizar.Pic.Print "FORMA DE PAGO:" & Sp_Fpago
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.FontBold = True
    'Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
    'S� 'is_Previsualizar.Pic.Print "FORMA DE  PAGO   "
    
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.FontBold = False
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
    Cy = Sis_Previsualizar.Pic.CurrentY
    Sis_Previsualizar.Pic.FontSize = 9
    For i = 1 To Me.LvMediosDePago.ListItems.Count
        Sis_Previsualizar.Pic.CurrentY = Cy
        Sis_Previsualizar.Pic.CurrentX = Cx
        If CDbl(LvMediosDePago.ListItems(i).SubItems(2)) > 0 Then
            RSet Sp_VPagos = LvMediosDePago.ListItems(i).SubItems(2)
            RSet Sm_Mpagos = LvMediosDePago.ListItems(i).SubItems(1) & _
            IIf(Val(LvMediosDePago.ListItems(i).SubItems(3)) > 0, " NRO " & LvMediosDePago.ListItems(i).SubItems(3) & IIf(Len(LvMediosDePago.ListItems(i).SubItems(4)) > 0, " " & LvMediosDePago.ListItems(i).SubItems(4), ""), "") & _
            " $" & Sp_VPagos
            'Coloca Cx, Cy, Sp_Fagos, False, 10
            
            Sis_Previsualizar.Pic.Print Sm_Mpagos
            Cy = Cy + (Dp_S * 2)
        End If
    Next
    RSet Sm_Mpagos = "--------------------------------------------"
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.Print Sm_Mpagos
    Sis_Previsualizar.Pic.FontBold = True
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
    RSet Sm_Mpagos = "TOTAL PAGO   :$" & LvDetalle.SelectedItem.SubItems(6)
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.Print Sm_Mpagos
    
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S + Dp_S
    
    
    Sis_Previsualizar.Pic.FontBold = False
    
   ' If LvTransferencia.ListItems.Count > 0 Then
   '     Sis_Previsualizar.Pic.CurrentX = Cx
   '     Sis_Previsualizar.Pic.Print "NRO OPERACION:" & LvTransferencia.ListItems(1).SubItems(1) & " " & LvTransferencia.ListItems(1)
   '     Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
   ' End If
    finpago = Sis_Previsualizar.Pic.CurrentY
    
    'Aqui detallaremos los cheques
    If LvCheques.ListItems.Count > 0 Then
        iniciocheque = Sis_Previsualizar.Pic.CurrentY
        Sis_Previsualizar.Pic.CurrentX = Cx
        Sis_Previsualizar.Pic.FontBold = True
        Sis_Previsualizar.Pic.Print "DETALLE DE CHEQUES"
        Sis_Previsualizar.Pic.FontBold = False
        Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
        Sp_FechaCheque = "Fecha"
        Sp_NroCheque = "Nro"
        RSet Sp_ValorCheque = "Valor"
        Sp_Banco = "Banco"
        Sis_Previsualizar.Pic.CurrentX = Cx
        Sis_Previsualizar.Pic.Print Sp_Banco & " " & Sp_NroCheque & " " & Sp_FechaCheque & "       " & Sp_ValorCheque
        Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S - 0.2
        tcheques = 0
        For i = 1 To LvCheques.ListItems.Count
            Sis_Previsualizar.Pic.CurrentX = Cx
            Sp_FechaCheque = LvCheques.ListItems(i).SubItems(3)
            Sp_NroCheque = LvCheques.ListItems(i).SubItems(2)
            RSet Sp_ValorCheque = LvCheques.ListItems(i).SubItems(4)
            Sp_Banco = LvCheques.ListItems(i)
            Sis_Previsualizar.Pic.Print Sp_Banco & " " & Sp_NroCheque & " " & Sp_FechaCheque & "       " & Sp_ValorCheque
            Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S - 0.2
            tcheques = tcheques + CDbl(LvCheques.ListItems(i).SubItems(4))
            
        Next
        Sp_FechaCheque = ""
        Sp_NroCheque = ""
        RSet Sp_ValorCheque = NumFormat(tcheques)
        Sp_Banco = ""
        Sis_Previsualizar.Pic.CurrentX = Cx
        Sis_Previsualizar.Pic.FontBold = True
        Sis_Previsualizar.Pic.Print Sp_Banco & " " & Sp_NroCheque & " " & Sp_FechaCheque & "       " & Sp_ValorCheque
        fincheque = Sis_Previsualizar.Pic.CurrentY
        Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
    End If
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.FontBold = True
    posobs = Sis_Previsualizar.Pic.CurrentY
    Sis_Previsualizar.Pic.Print "OBSERVACIONES:"
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
    Sis_Previsualizar.Pic.FontBold = False
     
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.Print "" '; TxtComentario
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
    pos = Sis_Previsualizar.Pic.CurrentY
    Sis_Previsualizar.Pic.CurrentY = pos + 1
    Sis_Previsualizar.Pic.CurrentX = Cx
    
    Sis_Previsualizar.Pic.Print "CONTABILIDAD"
        
    Sis_Previsualizar.Pic.CurrentY = pos + 1
    Sis_Previsualizar.Pic.CurrentX = Cx + 6
    Sis_Previsualizar.Pic.Print "V� B� CAJA"
   
   
   
    Sis_Previsualizar.Pic.CurrentY = pos + 1
    Sis_Previsualizar.Pic.CurrentX = Cx + 12
    Sis_Previsualizar.Pic.Print "NOMBRE, RUT  Y  FIRMA"
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
    
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY - (Dp_S * 6)
    Sis_Previsualizar.Pic.CurrentX = Cx + 12
    Sis_Previsualizar.Pic.Print "RECIBI CONFORME"
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
    
   ' Sis_Previsualizar.pic.DrawMode = 1
    'Sis_Previsualizar.pic.DrawWidth = 3
    
    Sis_Previsualizar.Pic.Line (1.5, 1)-(20, 4.5), , B 'Rectangulo Encabezado y N� Comprobante
    
    Sis_Previsualizar.Pic.Line (1.5, 4.5)-(20, 6.2), , B 'Pagado o Recibido del cliente o proveedor
    
    If LvCheques.ListItems.Count > 0 Then
        Sis_Previsualizar.Pic.Line (1.5, 6.2)-(20, finpago), , B  'Concepto - documentos pagados o recibidos y forma de pago
    Else
        Sis_Previsualizar.Pic.Line (1.5, 6.2)-(20, finpago + 0.15), , B 'Concepto - documentos pagados o recibidos y forma de pago
    End If
    
    Sis_Previsualizar.Pic.Line (13.5, 6.2)-(16.8, posobs), , B  'columnas de los pagos en efectivo
    Sis_Previsualizar.Pic.Line (13.5, 6.2)-(16.8, 9.37), , B  'columnas de los pagos en efectivo
    
    If LvCheques.ListItems.Count > 0 Then
        Sis_Previsualizar.Pic.Line (1.5, iniciocheque)-(20, posobs), , B  'documentos pagados y forma de pago
        Sis_Previsualizar.Pic.Line (13.5, iniciocheque)-(16.8, posobs), , B  'columnas de los pagos con cheques
    End If
    
    Sis_Previsualizar.Pic.Line (1.5, posobs)-(20, posobs + 1.5), , B 'Observaciones
    Sis_Previsualizar.Pic.Line (1.5, posobs + 1.5)-(20, posobs + 1.5 + 2), , B 'Pie del comprobante
        
    pos = Sis_Previsualizar.Pic.CurrentY
    
 '   Sis_Previsualizar.Pic.Height = pos * 0.04

Sis_Previsualizar.Show 1
End Sub


Private Sub PrevisualizaPrint()
   Dim Cx As Double, Cy As Double, Sp_Fecha As String, Ip_Mes As Integer, Dp_S As Double, Sp_Letras As String
    Dim Sp_Neto As String * 12, Sp_IVA As String * 12
    
    Dim Sp_Nro_Comprobante As String
    
    Dim Sp_Empresa As String
    Dim Sp_Giro As String
    Dim Sp_Direccion As String
    
    
    Dim Sp_Nombre As String * 45
    Dim Sp_Rut As String * 15
    Dim Sp_Concepto As String
    
    Dim Sp_Nro_Doc As String * 12
    Dim Sp_Documento As String * 25
    Dim Sp_Tdocumento  As String * 12
    Dim Sp_Valor As String * 15
    Dim Sp_Saldo As String * 15
    Dim Sp_Fpago As String * 24
    Dim Sp_Total As String * 12
    
    Dim Sp_Banco As String * 15
    Dim Sp_NroCheque As String * 10
    Dim Sp_FechaCheque As String * 10
    Dim Sp_ValorCheque As String * 15
    Dim Sp_SumaCheque As String * 15
    Dim Sp_PlazaFechas As String * 50
    Dim Sp_Observacion As String * 80
    
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    
    Sp_Nro_Comprobante = LvDetalle.SelectedItem.SubItems(9)
    If Val(Sp_Nro_Comprobante) = 0 Then Exit Sub
    Sp_Imprime_Cheque = "NO"
    Sql = "SELECT giro,direccion,ciudad,emp_emite_cheque_en_comprobante_pago cheque " & _
         "FROM sis_empresas " & _
         "WHERE rut='" & SP_Rut_Activo & "'"
    Consulta RsTmp2, Sql
    If RsTmp2.RecordCount > 0 Then
        Sp_Giro = RsTmp2!giro
        Sp_Direccion = RsTmp2!direccion & " - " & RsTmp2!ciudad
        Sp_Imprime_Cheque = RsTmp2!cheque
    End If

    V = 1
    If Sp_Imprime_Cheque = "SI" Then
        If LvCheques.ListItems.Count > 0 Then
            V = LvCheques.ListItems.Count
        End If
    End If
        
    For p = 1 To V
            Printer.ScaleMode = vbCentimeters
            'Printer.BackColor = vbWhite
            'Printer.AutoRedraw = True
            Printer.DrawWidth = 1
            Printer.DrawMode = 1
            
            Printer.FontName = "Courier New"
            Printer.FontSize = 10
           
            Cx = 2
            Cy = 1.9
            Dp_S = 0.17
            Printer.CurrentX = Cx
            Printer.CurrentY = Cy
            Printer.FontSize = 16  'tama�o de letra
            Printer.FontBold = True
            
            '**
            'Sis_Previsualizar.Pic.PaintPicture LoadPicture(App.Path & "\REDMAROK.jpg"), Cx + 0.62, 1
            Printer.PaintPicture LoadPicture(App.Path & "\REDMAROK.jpg"), Cx + 0.62, 1
            Printer.CurrentY = Printer.CurrentY + Dp_S + Dp_S
            Printer.CurrentY = Printer.CurrentY + Dp_S + Dp_S
            '**
            
            Printer.Print SP_Empresa_Activa
                
            
            Printer.CurrentX = Cx
            Printer.CurrentY = Printer.CurrentY
          
            Printer.FontSize = 10
          
            pos = Printer.CurrentY
            Printer.Print "R.U.T.   :" & SP_Rut_Activo
            Printer.CurrentY = Printer.CurrentY
            
            Sm_Cli_Pro = Mid(Sp_extra, 1, 3)
            Printer.CurrentY = pos
            Printer.CurrentX = Cx + 11
            If Sm_Cli_Pro = "CLI" Then
                 Printer.Print "COMPROBANTE DE INGRESO N�: " & Sp_Nro_Comprobante
            Else
                 Printer.Print "COMPROBANTE DE EGRESO N�: " & Sp_Nro_Comprobante
            End If
            Printer.CurrentY = Printer.CurrentY
         
         
            Printer.FontBold = False
            Printer.FontSize = 10
            Printer.CurrentX = Cx
            Printer.Print "GIRO     :" & Sp_Giro
            Printer.CurrentY = Printer.CurrentY
            
          
            Printer.FontSize = 10
            Printer.CurrentX = Cx
            Printer.Print "DIRECCION:"; Sp_Direccion
            Printer.CurrentY = Printer.CurrentY + Dp_S
            
            Printer.CurrentX = Cx
            Printer.FontBold = True
            If Sm_Cli_Pro = "CLI" Then
                 Printer.Print "Recibi de"
            Else
                 Printer.Print "Pagado a "
            End If
            Printer.FontBold = False
            Printer.CurrentY = Printer.CurrentY + Dp_S
            Printer.FontSize = 10
            pos = Printer.CurrentY
            Printer.CurrentX = Cx
            Printer.Print "Nombre:" & LvDetalle.SelectedItem.SubItems(3)
            
            Printer.CurrentY = pos
            Printer.CurrentX = Cx + 14
            Printer.Print "Fecha:" & LvDetalle.SelectedItem.SubItems(1)
            Printer.CurrentY = Printer.CurrentY + Dp_S
            
            
            Printer.CurrentX = Cx
            Printer.Print "RUT   :" & LvDetalle.SelectedItem.SubItems(2)
            Printer.CurrentY = Printer.CurrentY + Dp_S
            
            Printer.CurrentX = Cx
            Printer.FontBold = True
            Printer.Print "CONCEPTO :"
            Printer.CurrentY = Printer.CurrentY + Dp_S
            
               
            'Aqui agregamos los documentos a los que se abonara
            Printer.FontSize = 10
            Printer.FontBold = False
            Sp_Nro_Doc = "Nro"
            Sp_Documento = "Documento"
            RSet Sp_Tdocumento = "Valor Docto."
            RSet Sp_Valor = "Abono"
            RSet Sp_Saldo = "Saldo"
            Printer.CurrentX = Cx
            Printer.Print Sp_Nro_Doc & " " & Sp_Documento & " " & Sp_Tdocumento & " " & Sp_Valor & " " & Sp_Saldo
            Printer.CurrentY = Printer.CurrentY + Dp_S - 0.1
            For i = 1 To LvDocumentos.ListItems.Count
                Sp_Nro_Doc = LvDocumentos.ListItems(i).SubItems(3)
                Sp_Documento = LvDocumentos.ListItems(i).SubItems(2)
                RSet Sp_Tdocumento = LvDocumentos.ListItems(i).SubItems(4)
                RSet Sp_Valor = LvDocumentos.ListItems(i).SubItems(5)
                RSet Sp_Saldo = LvDocumentos.ListItems(i).SubItems(6)
                Printer.CurrentX = Cx
                Printer.Print Sp_Nro_Doc & " " & Sp_Documento & " " & Sp_Tdocumento & " " & Sp_Valor & " " & Sp_Saldo
                Printer.CurrentY = Printer.CurrentY + Dp_S - 0.2
            Next
            Printer.FontSize = 10
            Printer.CurrentY = Printer.CurrentY + 0.2
            Printer.CurrentX = Cx
            Printer.FontBold = True
            Sp_Fpago = LvDetalle.SelectedItem.SubItems(5)
            pos = Printer.CurrentY
            Printer.Print "FORMA DE PAGO:" & Sp_Fpago
            Printer.CurrentX = Cx
            Printer.FontBold = True
            'printer.CurrentY = printer.CurrentY + Dp_S
            'S� 'is_Previsualizar.Pic.Print "FORMA DE  PAGO   "
            
            Printer.CurrentX = Cx
            Printer.FontBold = False
            Printer.CurrentY = Printer.CurrentY + Dp_S
            Cy = Printer.CurrentY
            Printer.FontSize = 9
            For i = 1 To Me.LvMediosDePago.ListItems.Count
                Printer.CurrentY = Cy
                Printer.CurrentX = Cx
                If CDbl(LvMediosDePago.ListItems(i).SubItems(2)) > 0 Then
                    RSet Sp_VPagos = LvMediosDePago.ListItems(i).SubItems(2)
                    RSet Sm_Mpagos = LvMediosDePago.ListItems(i).SubItems(1) & _
                    IIf(Val(LvMediosDePago.ListItems(i).SubItems(3)) > 0, " NRO " & LvMediosDePago.ListItems(i).SubItems(3) & IIf(Len(LvMediosDePago.ListItems(i).SubItems(4)) > 0, " " & LvMediosDePago.ListItems(i).SubItems(4), ""), "") & _
                    " $" & Sp_VPagos
                    'Coloca Cx, Cy, Sp_Fagos, False, 10
                    
                    Printer.Print Sm_Mpagos
                    Cy = Cy + (Dp_S * 2)
                End If
            Next
            RSet Sm_Mpagos = "--------------------------------------------"
            Printer.CurrentX = Cx
            Printer.Print Sm_Mpagos
            Printer.FontBold = True
            Printer.CurrentY = Printer.CurrentY + Dp_S
            RSet Sm_Mpagos = "TOTAL PAGO   :$" & LvDetalle.SelectedItem.SubItems(6)
            Printer.CurrentX = Cx
            Printer.Print Sm_Mpagos
            
            Printer.CurrentY = Printer.CurrentY + Dp_S + Dp_S
            
            
            Printer.FontBold = False
            
           ' If LvTransferencia.ListItems.Count > 0 Then
           '     printer.CurrentX = Cx
           '     printer.Print "NRO OPERACION:" & LvTransferencia.ListItems(1).SubItems(1) & " " & LvTransferencia.ListItems(1)
           '     printer.CurrentY = printer.CurrentY + Dp_S
           ' End If
            finpago = Printer.CurrentY
            
            'Aqui detallaremos los cheques
            If LvCheques.ListItems.Count > 0 Then
                iniciocheque = Printer.CurrentY
                Printer.CurrentX = Cx
                Printer.FontBold = True
                Printer.Print "DETALLE DE CHEQUES"
                Printer.FontBold = False
                Printer.CurrentY = Printer.CurrentY + Dp_S
                Sp_FechaCheque = "Fecha"
                Sp_NroCheque = "Nro"
                RSet Sp_ValorCheque = "Valor"
                Sp_Banco = "Banco"
                Printer.CurrentX = Cx
                Printer.Print Sp_Banco & " " & Sp_NroCheque & " " & Sp_FechaCheque & "       " & Sp_ValorCheque
                Printer.CurrentY = Printer.CurrentY + Dp_S - 0.2
                tcheques = 0
                For i = 1 To LvCheques.ListItems.Count
                    Printer.FontBold = False
                    If i = p Then Printer.FontBold = True
                    Printer.CurrentX = Cx
                    Sp_FechaCheque = LvCheques.ListItems(i).SubItems(3)
                    Sp_NroCheque = LvCheques.ListItems(i).SubItems(2)
                    RSet Sp_ValorCheque = LvCheques.ListItems(i).SubItems(4)
                    Sp_Banco = LvCheques.ListItems(i)
                    Printer.Print Sp_Banco & " " & Sp_NroCheque & " " & Sp_FechaCheque & "       " & Sp_ValorCheque
                    Printer.CurrentY = Printer.CurrentY + Dp_S - 0.2
                    tcheques = tcheques + CDbl(LvCheques.ListItems(i).SubItems(4))
                    Printer.FontBold = False
                Next
                Sp_FechaCheque = ""
                Sp_NroCheque = ""
                RSet Sp_ValorCheque = NumFormat(tcheques)
                Sp_Banco = ""
                Printer.CurrentX = Cx
                Printer.FontBold = True
                Printer.Print Sp_Banco & " " & Sp_NroCheque & " " & Sp_FechaCheque & "       " & Sp_ValorCheque
                fincheque = Printer.CurrentY
                Printer.CurrentY = Printer.CurrentY + Dp_S
            End If
            Printer.CurrentY = Printer.CurrentY + Dp_S
            Printer.CurrentX = Cx
            Printer.FontBold = True
            posobs = Printer.CurrentY
            Printer.Print "OBSERVACIONES:"
            Printer.CurrentY = Printer.CurrentY + Dp_S
            Printer.FontBold = False
             
            Printer.CurrentX = Cx
            Printer.Print "" '; TxtComentario
            Printer.CurrentY = Printer.CurrentY + Dp_S
            pos = Printer.CurrentY
            Printer.CurrentY = pos + 1
            Printer.CurrentX = Cx
            
            Printer.Print "CONTABILIDAD"
                
            Printer.CurrentY = pos + 1
            Printer.CurrentX = Cx + 6
            Printer.Print "V� B� CAJA"
           
           
           
            Printer.CurrentY = pos + 1
            Printer.CurrentX = Cx + 12
            Printer.Print "NOMBRE, RUT  Y  FIRMA"
            Printer.CurrentY = Printer.CurrentY + Dp_S
            
            Printer.CurrentY = Printer.CurrentY - (Dp_S * 6)
            Printer.CurrentX = Cx + 12
            Printer.Print "RECIBI CONFORME"
            Printer.CurrentY = Printer.CurrentY + Dp_S
            
           ' printer.DrawMode = 1
            'printer.DrawWidth = 3
            
            Printer.Line (1.5, 1)-(20, 4.5), , B 'Rectangulo Encabezado y N� Comprobante
            
            Printer.Line (1.5, 4.5)-(20, 6.2), , B 'Pagado o Recibido del cliente o proveedor
            
            If LvCheques.ListItems.Count > 0 Then
                Printer.Line (1.5, 6.2)-(20, finpago), , B  'Concepto - documentos pagados o recibidos y forma de pago
            Else
                Printer.Line (1.5, 6.2)-(20, finpago + 0.15), , B 'Concepto - documentos pagados o recibidos y forma de pago
            End If
            
            Printer.Line (13.5, 6.2)-(16.8, posobs), , B  'columnas de los pagos en efectivo
            Printer.Line (13.5, 6.2)-(16.8, 9.37), , B  'columnas de los pagos en efectivo
            
            If LvCheques.ListItems.Count > 0 Then
                Printer.Line (1.5, iniciocheque)-(20, posobs), , B  'documentos pagados y forma de pago
                Printer.Line (13.5, iniciocheque)-(16.8, posobs), , B  'columnas de los pagos con cheques
            End If
            
            Printer.Line (1.5, posobs)-(20, posobs + 1.5), , B 'Observaciones
            Printer.Line (1.5, posobs + 1.5)-(20, posobs + 1.5 + 2), , B 'Pie del comprobante
                
            pos = Printer.CurrentY
            '8 nov 2014
            'Imprime cheque
            If Sp_Imprime_Cheque = "SI" And LvCheques.ListItems.Count > 0 Then
                If MsgBox("Imprime Cheque " & LvCheques.ListItems(p) & " " & LvCheques.ListItems(p).SubItems(2), vbOKCancel + vbQuestion) = vbOK Then
                        Printer.CurrentY = Val(IG_Posicion_Inicial_Cheques)
                        Printer.CurrentX = 16.5
                        Printer.FontBold = True
                        Printer.FontSize = 10
                        'Monto del cheque
                        Printer.Print LvCheques.ListItems(p).SubItems(4) & "*****"
                        Printer.FontBold = False
                        
                        'Fecha del cheque
                        RSet Sp_PlazaFechas = LvCheques.ListItems(p).SubItems(7) & ", " & Day(LvCheques.ListItems(p).SubItems(3)) & " de " & MonthName(Month(LvCheques.ListItems(p).SubItems(3))) & " de " & Year(LvCheques.ListItems(p).SubItems(3))
                        Printer.CurrentY = Val(IG_Posicion_Inicial_Cheques) + 1.1
                          'Coloca (Cx + 13.3 + avance) - Printer.TextWidth(PTotal)
                        Printer.CurrentX = 19 - Printer.TextWidth(Sp_PlazaFechas)
                        pos = Printer.CurrentY
                        Printer.Print Sp_PlazaFechas
                        
                        'Beneficiarios
                        Printer.CurrentY = Val(IG_Posicion_Inicial_Cheques) + 2.1
                        Printer.CurrentX = 7.7
                        pos = Printer.CurrentY
                        Printer.Print LvDetalle.SelectedItem.SubItems(3)
                        
                        Printer.FontSize = 10
                        Printer.CurrentX = 7.8
                        pos = Printer.CurrentY
                        MONTOENLETRAS = BrutoAletras(CDbl(LvCheques.ListItems(p).SubItems(4)), True)
                        If Len(MONTOENLETRAS) > 50 Then
                            hasta = InStr(50, MONTOENLETRAS, " ")
                            If hasta > 0 Then
                                LINEA1 = Mid(MONTOENLETRAS, 1, hasta)
                                LINEA2 = Mid(MONTOENLETRAS, hasta + 1)
                                Printer.CurrentY = Val(IG_Posicion_Inicial_Cheques) + 3
                                Printer.Print LINEA1
                                Printer.CurrentX = 7.8
                                Printer.Print LINEA2 & " ***********"
                            Else
                                Printer.CurrentY = Val(IG_Posicion_Inicial_Cheques) + 3.1
                                Printer.Print MONTOENLETRAS & "  ********"
                            End If
                        Else
                            Printer.CurrentY = 24.1
                            Printer.Print MONTOENLETRAS & "  ********"
                        End If
                    
                End If
            End If
         '   printer.Height = pos * 0.04
        
            Printer.NewPage
            Printer.EndDoc
    Next p
End Sub



Private Sub cmdConsultar_Click()
    Dim Sp_111 As String
    Sp_111 = ""
    Sm_FiltroNombre = Empty
    

    
    If Option1.Value Then
        sm_FiltroFecha = Empty
    Else
        sm_FiltroFecha = " AND (a.abo_fecha_pago BETWEEN '" & Format(DTInicio.Value, "YYYY-MM-DD") & "' AND '" & Format(DtHasta.Value, "YYYY-MM-DD") & "') "
    End If
    
    LvDetalle.ListItems.Clear
    LvDocumentos.ListItems.Clear
    LvMediosDePago.ListItems.Clear
    LvCheques.ListItems.Clear
    

    'If Len(TxtBusqueda) > 0 Then
    '        If Sp_extra = "CLIENTES" Then Sm_FiltroNombre = " AND m.nombre_rsocial LIKE '%" & TxtBusqueda & "%' "
    '        If Sp_extra = "PROVEEDORE" Then Sm_FiltroNombre = " AND m.nombre_empresa LIKE '%" & TxtBusqueda & "%' "
    '
    'End If
    If Len(TxtRut) > 0 Then
            If Sp_extra = "CLIENTES" Then Sm_FiltroNombre = " AND m.rut_cliente='" & TxtRut & "' "
            If Sp_extra = "PROVEEDORE" Then Sm_FiltroNombre = " AND m.rut_proveedor='" & TxtRut & "' "
    End If
    'En vega modelo, no mostrar los abonos al rut de cliente pasajero...
    If SP_Rut_Activo = "96.803.210-0" Then
        Sp_111 = " AND rut_cli  NOT IN('11.111.111-1')"
    End If
    
    If Sp_extra = "CLIENTES" Then
        Sql = "SELECT a.abo_id, abo_fecha_pago,a.abo_rut,m.nombre_rsocial," & _
                "IF(a.suc_id=0,'CASA MATRIZ',CONCAT(s.suc_ciudad,' ',suc_direccion)) sucursal," & _
                "'' mpa_nombre , abo_monto, usu_nombre, a.abo_id,abo_nro_comprobante " & _
                "FROM cta_abonos a " & _
                "INNER JOIN maestro_clientes m ON a.abo_rut=m.rut_cliente " & _
                "/*iNNER JOIN par_medios_de_pago p USING(mpa_id) */ " & _
                "INNER JOIN par_sucursales s USING(suc_id) " & _
                "WHERE (SELECT sue_id " & _
                        "FROM ven_doc_venta v  " & _
                        "JOIN cta_abono_documentos e " & _
                        "WHERE e.abo_id=a.abo_id AND v.id=e.id LIMIT 1) =" & IG_id_Sucursal_Empresa & " AND " & _
                "a.rut_emp='" & SP_Rut_Activo & "' AND m.rut_cliente IN(SELECT rut_cli FROM par_asociacion_ruts WHERE rut_emp='" & SP_Rut_Activo & "'" & Sp_111 & ") AND " & _
                "abo_cli_pro='CLI' " & sm_FiltroFecha & Sm_FiltroNombre & _
                "GROUP BY a.abo_id " & _
                "ORDER BY abo_nro_comprobante DESC"
    
    
    Else
        Sql = "SELECT a.abo_id,abo_fecha_pago,a.abo_rut,m.nombre_empresa," & _
                "'' sucursal," & _
                "'' mpa_nombre , abo_monto, usu_nombre, a.abo_id,abo_nro_comprobante " & _
                "FROM cta_abonos a " & _
                "INNER JOIN maestro_proveedores m ON a.abo_rut=m.rut_proveedor " & _
                "/*INNER JOIN par_medios_de_pago p USING(mpa_id) */" & _
                "INNER JOIN cta_abono_documentos d USING(abo_id) " & _
                "INNER JOIN com_doc_compra c ON c.id=d.id " & _
                "WHERE a.rut_emp='" & SP_Rut_Activo & "' AND m.rut_proveedor IN(SELECT rut_pro FROM par_asociacion_ruts WHERE rut_emp='" & SP_Rut_Activo & "') AND " & _
                "abo_cli_pro='PRO' " & sm_FiltroFecha & Sm_FiltroNombre & _
                "GROUP BY a.abo_id " & _
                "ORDER BY abo_nro_comprobante DESC"
    
    End If
    
    PicLoad.Visible = True
    TimLoad.Enabled = True
    
    'DoEvents
    'Sis_BarraProgreso.Show 1
  '  rstmp.
    Consulta RsTmp, Sql
    
    LLenar_Grilla RsTmp, Me, LvDetalle, False, True, True, False
    TxtTotal = NumFormat(TotalizaColumna(LvDetalle, "total"))


    PicLoad.Visible = False
    TimLoad.Enabled = False
End Sub


Private Sub CmdEliminar_Click()
    Dim Lp_Nro As Long, Lp_Abo As Long, Sp_Wh As String, Sp_Eli As String, Lp_MovId As Long
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    If Val(LvDetalle.SelectedItem) = 0 Then Exit Sub
    
    'Comprobamos si es una nota de credito la que se esta abonando
    '19-10-2013
    For i = 1 To LvMediosDePago.ListItems.Count
        If Val(LvMediosDePago.ListItems(i).SubItems(5)) = 8888 Then
            MsgBox "Para eliminar el abono por nota de credito, elimine el documento correspondiente"
            Exit Sub
        End If
        
        If Principal.CmdMenu(8).Visible = True Then
            If Len(LvMediosDePago.ListItems(i).SubItems(6)) > 0 Then
                If ConsultaCentralizado(LvMediosDePago.ListItems(i).SubItems(6), Month(LvMediosDePago.ListItems(i)), Year(LvMediosDePago.ListItems(i))) Then
                    MsgBox "Periodo contable ya ha sido centralizado,  " & vbNewLine & "No puede modificar"
                    Exit Sub
                End If
            End If
        End If
        
        
        
        
    Next
    'Tambien comprobaremos los asientos en los que esta involucrado el abono
    
    
    
    
    If Sp_extra = "CLIENTES" Then
        Sp_Eli = "ING"
    Else
        Sp_Eli = "EGR"
    End If
    Lp_Nro = LvDetalle.SelectedItem.SubItems(9)
    Lp_Abo = LvDetalle.SelectedItem
    If MsgBox("Esta a punto de eliminar el comprobante Nro " & Lp_Nro & vbNewLine & "�Continuar...?", vbYesNo + vbQuestion) = vbNo Then Exit Sub
    SG_codigo2 = Empty
    sis_InputBox.Caption = "Se requiere autenticaci�n"
    sis_InputBox.FramBox.Caption = "Ingrese clave"
    sis_InputBox.Show 1
    If SG_codigo2 <> Empty Then
        Sql = "SELECT usu_nombre,usu_login " & _
              "FROM sis_usuarios " & _
              "WHERE usu_pwd=MD5('" & UCase(SG_codigo2) & "')"
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
            On Error GoTo errorEliminando
            cn.BeginTrans
            
            
            
            
            Sp_Wh = " WHERE abo_id=" & Lp_Abo
            cn.Execute "DELETE FROM cta_abonos " & Sp_Wh
            cn.Execute "DELETE FROM abo_transferencia " & Sp_Wh
            cn.Execute "DELETE FROM cta_abono_documentos " & Sp_Wh
            
            cn.Execute "DELETE FROM abo_tipos_de_pagos " & Sp_Wh
            
            
            '5 5 2016 _
            Eliminar tambien del pozo los abonos/cargos del abono
            cn.Execute "DELETE FROM cta_pozo WHERE abo_id=" & Lp_Abo
            
            
            
            If Sm_SistemaBanco = "SI" Then
                'Ubicar cheques relacionados, si correponde
                
                
                'O tambien buscar depositos de cheques en cartera 19 Octubre 2013
                
                Sql = "SELECT mov_id " & _
                      "FROM ban_movimientos " & Sp_Wh
                Consulta RsTmp3, Sql
                If RsTmp3.RecordCount > 0 Then
                
                    RsTmp3.MoveFirst
                    Do While Not RsTmp3.EOF
                        Sql = "DELETE FROM com_con_multicuenta " & _
                                "WHERE dcu_tipo='CHEQUE' AND id_unico IN(" & _
                                        "SELECT che_id " & _
                                        "FROM ban_cheques " & _
                                        "WHERE mov_id=" & RsTmp3!mov_id & ")"
                        cn.Execute Sql
                            
                        
                    
                        cn.Execute "UPDATE ban_cheques SET che_estado='SIN EMITIR',che_concepto='',che_destinatario=''," & _
                                          "che_valor=0,pla_id=0," & _
                                          "mes_contable=0,ano_contable=0,mov_id=0 " & _
                                   "WHERE mov_id=" & RsTmp3!mov_id
                    
                        RsTmp3.MoveNext
                    Loop
                End If
                cn.Execute "DELETE FROM ban_movimientos " & Sp_Wh
                
                If Sp_extra = "CLIENTES" Then
                    For i = 1 To LvCheques.ListItems.Count
                        cn.Execute "DELETE FROM ban_movimientos WHERE mov_id=" & LvCheques.ListItems(i).SubItems(6)
                    Next
                End If
                
            End If
            cn.Execute "DELETE FROM abo_cheques " & Sp_Wh
            
            
            Sql = "INSERT INTO sis_eliminacion_documentos (id,eli_fecha,eli_motivo,usu_login,eli_ven_com)" & _
                  "VALUES(" & Lp_Abo & ",'" & Fql(Date) & "','" & "ELIMINACION COMPROBANTE " & Sp_extra & "','" & RsTmp!usu_login & "','" & Sp_Eli & "')"
            cn.Execute Sql
            
            cn.CommitTrans
            MsgBox RsTmp!usu_nombre & " ha eliminado el comprobante Nro " & Lp_Nro
            Me.LvDocumentos.ListItems.Clear
            Me.LvCheques.ListItems.Clear
            Me.LvMediosDePago.ListItems.Clear
            cmdConsultar_Click
        Else
            MsgBox "Usuario no encontrado...", vbInformation
            
        End If
    End If
    Exit Sub
        
errorEliminando:
    cn.RollbackTrans
    MsgBox "Error Eliminando abono..." & vbNewLine & Err.Number & vbNewLine & Err.Description, vbInformation
End Sub

Private Sub CmdExportar_Click()
    Dim tit(2) As String
    If LvDetalle.ListItems.Count = 0 Then Exit Sub
    FraProgreso.Visible = True
    tit(0) = Me.Caption & " " & DtFecha & " - " & Time
    tit(1) = "PAGOS/ABONOS"
    tit(2) = ""
    ExportarNuevo LvDetalle, tit, Me, BarraProgreso
    FraProgreso.Visible = False
End Sub

Private Sub CmdSalir_Click()
    Unload Me
End Sub

Private Sub CmdX_Click()
    On Error Resume Next
    TxtBusqueda = ""
    TxtBusqueda.SetFocus
End Sub


Private Sub cmdSinRut_Click()
    TxtRut = ""
    cmdConsultar_Click
End Sub

Private Sub Form_Load()
    
    Aplicar_skin Me
    Centrar Me
    Me.DTInicio.Value = Date - 30
    Me.DtHasta.Value = Date
    Sql = "SELECT men_infoextra " & _
          "FROM sis_menu " & _
          "WHERE men_id=" & IP_IDMenu
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        Sp_extra = RsTmp!men_infoextra
    End If
    
    If Sp_extra = "CLIENTES" Then
     '   Frame1.Caption = "CLIENTE"
      ' Sm_FiltroSucursal = " AND v.sue_id=" & IG_id_Sucursal_Empresa
        Me.Caption = "INFORME DE ABONOS DE CLIENTES"
        CmdComprobante.Caption = "Imprimir Comprobante de Ingreso"
    
    Else
        CmdComprobante.Caption = "Imprimir Comprobante de Egreso"
        Me.Caption = "INFORME DE ABONOS DE PROVEEDORES"
        LvDetalle.ColumnHeaders(4).Text = ""
    '    Frame1.Caption = "PROVEEDORES"
        
    End If
    Sm_SistemaBanco = "NO"
    Sql = "SELECT emp_modulo_banco " & _
          "FROM sis_empresas " & _
          "WHERE rut='" & SP_Rut_Activo & "'"
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        If RsTmp!emp_modulo_banco = "SI" Then
            Sm_SistemaBanco = "SI"
        End If
    End If
End Sub

Private Sub LvDetalle_Click()
        DetallePago
    
End Sub
Private Sub DetallePago()
    Dim sp_Abo_id As Long
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    sp_Abo_id = LvDetalle.SelectedItem
    
    LvDocumentos.ListItems.Clear
    LvMediosDePago.ListItems.Clear
    LvCheques.ListItems.Clear
    If Sp_extra = "CLIENTES" Then
        'Documenentos
        Sql = "SELECT c.id,fecha emision,doc_nombre,v.no_documento,bruto,c.ctd_monto, " & _
                "bruto-(SELECT SUM(d.ctd_monto) FROM cta_abono_documentos d INNER JOIN cta_abonos a USING(abo_id) WHERE a.abo_cli_pro='CLI' AND  d.id=v.id) saldo_documento,v.ctl_id " & _
                "FROM cta_abono_documentos c " & _
                "INNER JOIN ven_doc_venta v USING(id) " & _
                "INNER JOIN sis_documentos USING(doc_id) " & _
                "WHERE c.abo_id=" & sp_Abo_id
        Consulta RsTmp2, Sql

    Else
        'Documenentos
        Sql = "SELECT c.id,fecha emision,doc_nombre,v.no_documento,total,c.ctd_monto, " & _
                "total-(SELECT SUM(d.ctd_monto) FROM cta_abono_documentos d INNER JOIN cta_abonos a USING(abo_id) WHERE a.abo_cli_pro='PRO' AND d.id=v.id) saldo_documento,v.ctl_id " & _
                "FROM cta_abono_documentos c " & _
                "INNER JOIN com_doc_compra v USING(id) " & _
                "INNER JOIN sis_documentos USING(doc_id) " & _
                "WHERE c.abo_id=" & sp_Abo_id
        Consulta RsTmp2, Sql
    End If
    LLenar_Grilla RsTmp2, Me, LvDocumentos, False, True, True, False
     
     
     
     Sql = "SELECT emp_modulo_banco " & _
            "FROM sis_empresas " & _
            "WHERE rut='" & SP_Rut_Activo & "'"
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        If RsTmp!emp_modulo_banco = "SI" Then
        
            Sql = "SELECT ban_nombre,che_plaza,v.che_numero,che_fecha,che_monto,v.che_estado,v.mov_id, " & _
                    "(SELECT cte_ejecutivo FROM ban_cta_cte c JOIN ban_chequera q ON c.cte_id=q.cte_id WHERE q.chc_id=b.chc_id) PLAZA " & _
                    "FROM vi_cheques_plan_cuentas v " & _
                    "JOIN ban_cheques b ON v.che_numero=b.che_numero " & _
                    "JOIN ban_chequera c ON c.chc_id=b.chc_id " & _
                    "JOIN ban_cta_cte e  ON e.cte_id=c.cte_id " & _
                    "WHERE abo_id=" & sp_Abo_id & " AND e.rut_emp='" & SP_Rut_Activo & "' AND b.mov_id>0"
        Else
                'CONSULTAMOS SI HAY PAGO CON CHEQUES
            
            Sql = "SELECT ban_nombre,che_plaza,che_numero,che_fecha,che_monto,che_estado,mov_id " & _
                    "FROM vi_abono_con_cheques " & _
                    "WHERE abo_id=" & sp_Abo_id
        End If
    End If
    LvCheques.ListItems.Clear
    
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvCheques, False, True, True, False

    'Aqui mostraremos como se pago el o los docuemtnso,
     Sql = "SELECT abo_fecha_pago,mpa_nombre,pad_valor,pad_nro_transaccion,pad_tipo_deposito,mpa_id,asientos " & _
            "FROM vi_abono_tipos_de_pago " & _
            "WHERE abo_id=" & sp_Abo_id
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, Me.LvMediosDePago, False, True, True, False

    
        
        
    
End Sub

'ORDENA COLUMNAS
Private Sub LvDetalle_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    ordListView ColumnHeader, Me, LvDetalle
End Sub
Private Sub LvDetalle_KeyUp(KeyCode As Integer, Shift As Integer)
    DetallePago
End Sub

Private Sub Option1_Click()
        DTInicio.Enabled = True
        DtHasta.Enabled = True
End Sub

Private Sub Option2_Click()

    DTInicio.Enabled = True
    DtHasta.Enabled = True
    

End Sub

Private Sub Timer1_Timer()
    Timer1.Enabled = False
    LvDetalle.SetFocus
End Sub

Private Sub TxtBusqueda_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub









Private Sub TimLoad_Timer()
    ProLoad.Value = ProLoad.Value + 1
    
    If ProLoad.Value = 100 Then
        ProLoad.Value = 1
    End If
End Sub

Private Sub TxtRut_Validate(Cancel As Boolean)
If Len(TxtRut.Text) = 0 Then Exit Sub
    TxtRut.Text = Replace(TxtRut.Text, ".", "")
    TxtRut.Text = Replace(TxtRut.Text, "-", "")
    Respuesta = VerificaRut(TxtRut.Text, NuevoRut)
    If Respuesta = True Then
        Me.TxtRut.Text = NuevoRut
        'Buscar el cliente
        If Sp_extra = "CLIENTES" Then
            Sql = "SELECT rut_cliente rut,IFNULL(cli_nombre_fantasia,nombre_rsocial) nombre,fono,email " & _
                  "FROM maestro_clientes " & _
                  "WHERE rut_cliente='" & TxtRut & "'"
                '� CboSucursal.Clear
        Else
            Sql = "SELECT rut_proveedor rut,nombre_empresa nombre,fono,email " & _
                  "FROM maestro_proveedores " & _
                  "WHERE rut_proveedor='" & TxtRut & "'"
        
        End If
                    
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
                'Cliente encontrado
            With RsTmp
                TxtRut.Text = !Rut
                txtCliente = !nombre
                txtFono = "" & !fono
                'TxtMail = !email
            End With
           ' If Sp_extra = "CLIENTES" Then
           '     Filtro = " AND v.rut_cliente='" & TxtRut & "' "
           '     LLenarCombo CboSucursal, "CONCAT(suc_ciudad,' ',suc_direccion,' ',suc_contacto)", "suc_id", "par_sucursales", "suc_activo='SI' AND rut_cliente='" & TxtRut & "'", "suc_id"
           '     CboSucursal.AddItem "CASA MATRIZ"
           '     CboSucursal.ItemData(CboSucursal.ListCount - 1) = 0
           '     CboSucursal.AddItem "TODOS"
           '     CboSucursal.ListIndex = CboSucursal.ListCount - 1
           ' Else
                Filtro = " AND v.rut='" & TxtRut & "' "
           ' End If
        End If
        If Sp_extra = "CLIENTES" Then
            'CargaClientes
        Else
            'CargaProveedores
        End If
    End If
End Sub
