VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form ctacte_GestionDeCobranza 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Gestion de Cobranza"
   ClientHeight    =   7500
   ClientLeft      =   90
   ClientTop       =   1770
   ClientWidth     =   12360
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7500
   ScaleWidth      =   12360
   ShowInTaskbar   =   0   'False
   Begin VB.Timer Timer1 
      Interval        =   20
      Left            =   8955
      Top             =   105
   End
   Begin VB.Frame Frame1 
      Caption         =   "Documentos con saldo pendiente"
      Height          =   6675
      Left            =   240
      TabIndex        =   1
      Top             =   255
      Width           =   11790
      Begin VB.CommandButton cmdGuardar 
         Caption         =   "&Guardar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   435
         Left            =   180
         TabIndex        =   36
         Top             =   6150
         Width           =   1380
      End
      Begin VB.TextBox TxtDireccion 
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3810
         Locked          =   -1  'True
         TabIndex        =   34
         Top             =   3090
         Width           =   3240
      End
      Begin VB.TextBox Txtemail 
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1620
         Locked          =   -1  'True
         TabIndex        =   22
         Top             =   3090
         Width           =   2175
      End
      Begin VB.TextBox txtFono 
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   270
         Locked          =   -1  'True
         TabIndex        =   21
         Top             =   3090
         Width           =   1335
      End
      Begin VB.Frame Frame3 
         Caption         =   "Datos Cobranza"
         Height          =   2535
         Left            =   180
         TabIndex        =   7
         Top             =   3540
         Width           =   11505
         Begin VB.TextBox TxtOtro 
            BackColor       =   &H00FFFFFF&
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   6960
            TabIndex        =   35
            Top             =   705
            Width           =   4290
         End
         Begin VB.CheckBox Check1 
            Alignment       =   1  'Right Justify
            Caption         =   "Compromiso de pago"
            Height          =   195
            Left            =   5235
            TabIndex        =   32
            Top             =   1920
            Width           =   1815
         End
         Begin MSComCtl2.DTPicker DtFecha 
            Height          =   285
            Left            =   7110
            TabIndex        =   31
            Top             =   1860
            Width           =   1305
            _ExtentX        =   2302
            _ExtentY        =   503
            _Version        =   393216
            Enabled         =   0   'False
            Format          =   273416193
            CurrentDate     =   40733
         End
         Begin VB.TextBox TXToBS 
            BackColor       =   &H00FFFFFF&
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   600
            Left            =   5220
            MaxLength       =   150
            MultiLine       =   -1  'True
            TabIndex        =   30
            Top             =   1245
            Width           =   6045
         End
         Begin VB.ComboBox CboContacto 
            Height          =   315
            ItemData        =   "ctacte_GestionDeCobranza.frx":0000
            Left            =   5205
            List            =   "ctacte_GestionDeCobranza.frx":0002
            Style           =   2  'Dropdown List
            TabIndex        =   17
            Top             =   705
            Width           =   1740
         End
         Begin VB.Frame Frame4 
            Caption         =   "Contacto"
            Height          =   1845
            Left            =   180
            TabIndex        =   8
            Top             =   420
            Width           =   4935
            Begin VB.OptionButton Option4 
               Alignment       =   1  'Right Justify
               Caption         =   "Otro"
               Height          =   210
               Left            =   75
               TabIndex        =   28
               Top             =   1455
               Width           =   660
            End
            Begin VB.OptionButton Option3 
               Alignment       =   1  'Right Justify
               Caption         =   "Option1"
               Height          =   210
               Left            =   465
               TabIndex        =   27
               Top             =   1155
               Width           =   270
            End
            Begin VB.OptionButton Option2 
               Alignment       =   1  'Right Justify
               Caption         =   "Option1"
               Height          =   210
               Left            =   465
               TabIndex        =   26
               Top             =   870
               Width           =   270
            End
            Begin VB.OptionButton Option1 
               Alignment       =   1  'Right Justify
               Caption         =   "Option1"
               Height          =   210
               Left            =   465
               TabIndex        =   25
               Top             =   570
               Value           =   -1  'True
               Width           =   270
            End
            Begin VB.TextBox txtMailC4 
               BackColor       =   &H00FFFFFF&
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   2655
               TabIndex        =   24
               Top             =   1425
               Width           =   2100
            End
            Begin VB.TextBox TxtNC4 
               BackColor       =   &H00FFFFFF&
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   750
               TabIndex        =   23
               Top             =   1425
               Width           =   1890
            End
            Begin VB.TextBox txtMailC3 
               BackColor       =   &H00FFFFC0&
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   2655
               Locked          =   -1  'True
               TabIndex        =   14
               Top             =   1125
               Width           =   2100
            End
            Begin VB.TextBox TxtNC3 
               BackColor       =   &H00FFFFC0&
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   750
               Locked          =   -1  'True
               TabIndex        =   13
               Top             =   1125
               Width           =   1890
            End
            Begin VB.TextBox txtMailC2 
               BackColor       =   &H00FFFFC0&
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   2655
               Locked          =   -1  'True
               TabIndex        =   12
               Top             =   825
               Width           =   2100
            End
            Begin VB.TextBox TxtNC2 
               BackColor       =   &H00FFFFC0&
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   750
               Locked          =   -1  'True
               TabIndex        =   11
               Top             =   825
               Width           =   1890
            End
            Begin VB.TextBox txtMailC1 
               BackColor       =   &H00FFFFC0&
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   2655
               Locked          =   -1  'True
               TabIndex        =   10
               Top             =   525
               Width           =   2100
            End
            Begin VB.TextBox TxtNC1 
               BackColor       =   &H00FFFFC0&
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   750
               Locked          =   -1  'True
               TabIndex        =   9
               Top             =   525
               Width           =   1890
            End
            Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
               Height          =   180
               Left            =   750
               OleObjectBlob   =   "ctacte_GestionDeCobranza.frx":0004
               TabIndex        =   15
               Top             =   315
               Width           =   930
            End
            Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
               Height          =   210
               Left            =   2640
               OleObjectBlob   =   "ctacte_GestionDeCobranza.frx":006E
               TabIndex        =   16
               Top             =   315
               Width           =   1005
            End
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel5 
            Height          =   210
            Left            =   5190
            OleObjectBlob   =   "ctacte_GestionDeCobranza.frx":00D8
            TabIndex        =   18
            Top             =   495
            Width           =   1695
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel6 
            Height          =   210
            Left            =   5205
            OleObjectBlob   =   "ctacte_GestionDeCobranza.frx":0156
            TabIndex        =   29
            Top             =   1065
            Width           =   1695
         End
      End
      Begin VB.Frame Frame2 
         Caption         =   "Totales"
         Height          =   720
         Left            =   7440
         TabIndex        =   2
         Top             =   2790
         Width           =   4245
         Begin VB.TextBox TxtTotales 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00E0E0E0&
            Height          =   285
            Left            =   105
            Locked          =   -1  'True
            TabIndex        =   5
            Top             =   300
            Width           =   1350
         End
         Begin VB.TextBox TxtAbonos 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFC0&
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   1470
            Locked          =   -1  'True
            TabIndex        =   4
            Top             =   300
            Width           =   1335
         End
         Begin VB.TextBox txtSaldos 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00E0E0E0&
            Height          =   285
            Left            =   2820
            Locked          =   -1  'True
            TabIndex        =   3
            Top             =   300
            Width           =   1350
         End
      End
      Begin MSComctlLib.ListView LvDetalle 
         Height          =   2280
         Left            =   195
         TabIndex        =   6
         Top             =   405
         Width           =   11490
         _ExtentX        =   20267
         _ExtentY        =   4022
         View            =   3
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   12
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "T1000"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "N109"
            Text            =   "Nro Doc."
            Object.Width           =   1587
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "T1500"
            Text            =   "Documento"
            Object.Width           =   3528
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Object.Tag             =   "T1300"
            Text            =   "Rut"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Object.Tag             =   "T2000"
            Text            =   "Cliente"
            Object.Width           =   4410
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Object.Tag             =   "T1500"
            Text            =   "Sucursal"
            Object.Width           =   2646
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   6
            Object.Tag             =   "T1000"
            Text            =   "Emision"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   7
            Object.Tag             =   "T1000"
            Text            =   "Vencimiento"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   8
            Key             =   "total"
            Object.Tag             =   "N100"
            Text            =   "Total"
            Object.Width           =   2381
         EndProperty
         BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   9
            Key             =   "abonos"
            Object.Tag             =   "N100"
            Text            =   "Abonos"
            Object.Width           =   2381
         EndProperty
         BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   10
            Key             =   "saldo"
            Object.Tag             =   "N100"
            Text            =   "Saldo"
            Object.Width           =   2381
         EndProperty
         BeginProperty ColumnHeader(12) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   11
            Key             =   "nuevoabono"
            Object.Tag             =   "N100"
            Text            =   "Nuevo Abono"
            Object.Width           =   0
         EndProperty
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   210
         Left            =   300
         OleObjectBlob   =   "ctacte_GestionDeCobranza.frx":01CA
         TabIndex        =   19
         Top             =   2895
         Width           =   450
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   210
         Left            =   1620
         OleObjectBlob   =   "ctacte_GestionDeCobranza.frx":0230
         TabIndex        =   20
         Top             =   2895
         Width           =   1005
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel7 
         Height          =   210
         Left            =   3795
         OleObjectBlob   =   "ctacte_GestionDeCobranza.frx":029A
         TabIndex        =   33
         Top             =   2895
         Width           =   1005
      End
   End
   Begin VB.CommandButton CmdSalir 
      Cancel          =   -1  'True
      Caption         =   "&Retornar"
      Height          =   405
      Left            =   10890
      TabIndex        =   0
      Top             =   7020
      Width           =   1155
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   8355
      OleObjectBlob   =   "ctacte_GestionDeCobranza.frx":030A
      Top             =   105
   End
End
Attribute VB_Name = "ctacte_GestionDeCobranza"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub CboContacto_Click()
    TxtOtro.Enabled = False
    If CboContacto.ListIndex > -1 Then
        If CboContacto.Text = "OTRO" Then
            TxtOtro.Enabled = True
            TxtOtro.Locked = False
            TxtOtro.SetFocus
        Else
            TxtObs.SetFocus
        End If
        
    End If
End Sub

Private Sub Check1_Click()
    If Check1.Value = 1 Then
        DtFecha.Enabled = True
        DtFecha.SetFocus
    Else
        DtFecha.Enabled = False
    End If
End Sub

Private Sub CmdGuardar_Click()
    Dim Ip_Oc As Integer, Dp_FechasC As Date, Sp_OtroContacto As String, Sp_OtroMail As String, Sp_TipoContacto As String
    Dim Lp_IDCbr As Long
    If CboContacto.ListIndex = -1 Then
        MsgBox "Seleccione Tipo de contacto antes de continuar...", vbInformation
        CboContacto.SetFocus
        Exit Sub
    End If
    If Len(TxtObs) = 0 Then
        MsgBox "Debe ingresar detalle del cobro...", vbInformation
        TxtObs.SetFocus
        Exit Sub
    End If
    If Option4.Value Then
        If Len(TxtNC1) = 0 Then
            MsgBox "Falta Nombre del Contacto...", vbInformation
            TxtNC1.SetFocus
            Exit Sub
        End If
    End If
    If Option1.Value Then Ip_Oc = 1
    If Option2.Value Then Ip_Oc = 2
    If Option3.Value Then Ip_Oc = 3
    If Option4.Value Then Ip_Oc = 4
    If Check1.Value = 0 Then
        Dp_FechasC = Empty
    Else
        Dp_FechasC = Date
    End If
    Sp_TipoContacto = Empty
    Sp_OtroContacto = Empty
    Sp_OtroMail = Empty
    If Ip_Oc = 4 Then
        If Len(TxtNC4) = 0 Then
            MsgBox "Falta nombre de otro contacto"
            TxtNC4.SetFocus
            Exit Sub
        End If
        Sp_OtroContacto = TxtNC4
        Sp_OtroMail = txtMail4
    End If
    
    
    If CboContacto.Text = "OTRO" Then
        If Len(Me.TxtOtro) Then
            MsgBox "Especifique tipo de contacto...", vbInformation
            TxtOtro.SetFocus
            Exit Sub
        End If
        Sp_TipoContacto = TxtOtro
    End If
    Lp_IDCbr = UltimoNro("cta_cobranza", "cbr_id")
    
    Sql = "INSERT INTO cta_cobranza (cbr_id,cbr_rut,cbr_fecha,cbr_contacto,cbr_observacion," & _
                      "cbr_fecha_compromiso,cbr_hora,cbr_tipo_contacto," & _
                      "cbr_contacto_otro,cbr_mail,cbr_tipo_contacto_otro) " & _
          "VALUES(" & Lp_IDCbr & ",'" & LvDetalle.ListItems(1).SubItems(3) & "','" & Format(Date, "YYYY-MM-DD") & "'," & _
                   Ip_Oc & ",'" & TxtObs & "','" & Dp_FechasC & "','" & Time & "'," & _
                    CboContacto.ItemData(CboContacto.ListIndex) & ",'" & Sp_OtroContacto & "','" & _
                    Sp_OtroMail & "','" & Sp_TipoContacto & "')"
    Consulta RsTmp, Sql
    
    Sql = "INSERT INTO cta_cobranza_documentos (cbr_id,id,cbd_monto) " & _
          "VALUES"
    For i = 1 To LvDetalle.ListItems.Count
        Sql = Sql & "(" & Lp_IDCbr & "," & LvDetalle.ListItems(i) & "," & LvDetalle.ListItems(i).SubItems(10) & "),"
    Next
    Sql = Mid(Sql, 1, Len(Sql) - 1)
    Consulta RsTmp, Sql
    Unload Me
    Exit Sub
errorGrabar:
    MsgBox "Ocurrio un problema al momento de grabar " & vbNewLine & _
           "Error Nro: " & Err.Number & vbNewLine & _
           "Descripcion: " & Err.Description
End Sub

Private Sub cmdSalir_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    Centrar Me
    Aplicar_skin Me
    Consulta RsTmp3, Sql
    If RsTmp3.RecordCount > 0 Then
        LLenar_Grilla RsTmp3, Me, LvDetalle, False, True, True, False
        If LvDetalle.ListItems.Count > 0 Then
            'Buscar documento vencidos y marcarlos con rojo
            For i = 1 To LvDetalle.ListItems.Count
                If Len(LvDetalle.ListItems(i).SubItems(7)) > 0 Then
                    If DateDiff("d", Date, LvDetalle.ListItems(i).SubItems(7)) < 0 Then
                        For X = 1 To LvDetalle.ColumnHeaders.Count - 2
                            LvDetalle.ListItems(i).ListSubItems(X).ForeColor = vbRed
                            LvDetalle.ListItems(i).ListSubItems(X).Bold = True
                        Next
                    End If
                End If
            Next
            TxtTotales = NumFormat(TotalizaColumna(LvDetalle, "total"))
            txtSaldos = NumFormat(TotalizaColumna(LvDetalle, "saldo"))
            TxtAbonos = NumFormat(TotalizaColumna(LvDetalle, "abonos"))
        End If
    End If
    LLenarCombo CboContacto, "tct_nombre", "tct_id", "par_tipo_contacto", "tct_activo='SI'"
    DtFecha.Value = Date
    Sql = "SELECT fono,direccion,email,cli_contacto,cli_contacto2,cli_contacto3,cli_mail1,cli_mail2,cli_mail3 " & _
          "FROM maestro_clientes " & _
          "WHERE rut_cliente='" & LvDetalle.ListItems(1).SubItems(3) & "'"
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        With RsTmp
            txtFono = "" & !fono
            TxtEmail = "" & !Email
            TxtDireccion = "" & !direccion
            TxtNC1 = "" & !cli_contacto
            TxtNC2 = "" & !cli_contacto2
            TxtNC3 = "" & !cli_contacto3
            txtMailC1 = "" & !cli_mail1
            txtMailC2 = "" & !cli_mail2
            txtMailC3 = "" & !cli_mail3
        End With
    End If
End Sub

Private Sub Option1_Click()
    BloqueaCajas 1
End Sub
Private Sub BloqueaCajas(Habilita As Integer)
    Me.txtMailC1.Enabled = False
    Me.txtMailC2.Enabled = False
    Me.txtMailC3.Enabled = False
    Me.txtMailC4.Enabled = False
    Me.TxtNC1.Enabled = False
    Me.TxtNC2.Enabled = False
    Me.TxtNC3.Enabled = False
    Me.TxtNC4.Enabled = False
    Select Case Habilita
        Case 1
            txtMailC1.Enabled = True
            TxtNC1.Enabled = True
        Case 2
            txtMailC2.Enabled = True
            TxtNC2.Enabled = True
        Case 3
            txtMailC3.Enabled = True
            TxtNC3.Enabled = True
        Case 4
            txtMailC4.Enabled = True
            TxtNC4.Enabled = True
    End Select
End Sub

Private Sub Option2_Click()
BloqueaCajas 2
End Sub

Private Sub Option3_Click()
    BloqueaCajas 3
End Sub

Private Sub Option4_Click()
    BloqueaCajas 4
    TxtNC4.SetFocus
End Sub


Private Sub Timer1_Timer()
    LvDetalle.SetFocus
    Timer1.Enabled = False
End Sub











Private Sub txtMailC4_GotFocus()
    En_Foco txtMailC4
End Sub



Private Sub TxtNC4_GotFocus()
    En_Foco TxtNC4
End Sub

Private Sub TxtNC4_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
    If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtObs_GotFocus()
    En_Foco TxtObs
End Sub

Private Sub TXToBS_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub TxtOtro_GotFocus()
    En_Foco TxtOtro
End Sub

Private Sub TxtOtro_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
    If KeyAscii = 39 Then KeyAscii = 0
End Sub



