VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{DA729E34-689F-49EA-A856-B57046630B73}#1.0#0"; "progressbar-xp.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form ctacte_Factoring 
   Caption         =   "Documento a Factoring"
   ClientHeight    =   7845
   ClientLeft      =   1245
   ClientTop       =   2130
   ClientWidth     =   14895
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   7845
   ScaleWidth      =   14895
   Begin VB.Frame FraProgreso 
      Height          =   540
      Left            =   1665
      TabIndex        =   20
      Top             =   6945
      Visible         =   0   'False
      Width           =   11205
      Begin Proyecto2.XP_ProgressBar PBar 
         Height          =   255
         Left            =   135
         TabIndex        =   21
         Top             =   195
         Width           =   10830
         _ExtentX        =   19103
         _ExtentY        =   450
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BrushStyle      =   0
         Color           =   16777088
         ShowText        =   -1  'True
      End
   End
   Begin VB.CommandButton CmdExportar 
      Caption         =   "Exportar Excel"
      Height          =   255
      Left            =   270
      TabIndex        =   19
      Top             =   6975
      Width           =   1320
   End
   Begin VB.CommandButton cmdSalir 
      Cancel          =   -1  'True
      Caption         =   "&Retornar"
      Height          =   405
      Left            =   13365
      TabIndex        =   16
      Top             =   7425
      Width           =   1350
   End
   Begin VB.Timer Timer1 
      Interval        =   10
      Left            =   690
      Top             =   7590
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   150
      OleObjectBlob   =   "ctacte_Factoring.frx":0000
      Top             =   7590
   End
   Begin VB.Frame Frame1 
      Caption         =   "Nueva Planilla"
      Height          =   1020
      Left            =   315
      TabIndex        =   7
      Top             =   630
      Width           =   13035
      Begin VB.ComboBox CboBanco 
         Height          =   315
         Left            =   1695
         Style           =   2  'Dropdown List
         TabIndex        =   1
         Top             =   480
         Width           =   2985
      End
      Begin VB.TextBox txtMontoAnticipado 
         Alignment       =   1  'Right Justify
         Height          =   315
         Left            =   11520
         TabIndex        =   6
         Tag             =   "N"
         Top             =   480
         Width           =   1380
      End
      Begin VB.TextBox txtGastos 
         Alignment       =   1  'Right Justify
         Height          =   315
         Left            =   10125
         TabIndex        =   5
         Tag             =   "N"
         Top             =   480
         Width           =   1380
      End
      Begin VB.TextBox txtIntereses 
         Alignment       =   1  'Right Justify
         Height          =   315
         Left            =   8745
         TabIndex        =   4
         Tag             =   "N"
         Top             =   480
         Width           =   1380
      End
      Begin VB.TextBox txtMontoPlanilla 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00E0E0E0&
         Height          =   300
         Left            =   7395
         Locked          =   -1  'True
         TabIndex        =   11
         Top             =   480
         Width           =   1350
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   210
         Left            =   300
         OleObjectBlob   =   "ctacte_Factoring.frx":0234
         TabIndex        =   8
         Top             =   270
         Width           =   1410
      End
      Begin VB.TextBox txtNroOperacion 
         Height          =   315
         Left            =   300
         TabIndex        =   0
         Tag             =   "N"
         Top             =   480
         Width           =   1380
      End
      Begin MSComCtl2.DTPicker DtFechaOperacion 
         Height          =   315
         Left            =   4695
         TabIndex        =   2
         Top             =   480
         Width           =   1365
         _ExtentX        =   2408
         _ExtentY        =   556
         _Version        =   393216
         Format          =   273416193
         CurrentDate     =   40628
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   210
         Left            =   4710
         OleObjectBlob   =   "ctacte_Factoring.frx":02AC
         TabIndex        =   9
         Top             =   270
         Width           =   1320
      End
      Begin MSComCtl2.DTPicker DtFechaAbono 
         Height          =   315
         Left            =   6045
         TabIndex        =   3
         Top             =   480
         Width           =   1365
         _ExtentX        =   2408
         _ExtentY        =   556
         _Version        =   393216
         Format          =   273416193
         CurrentDate     =   40628
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   210
         Left            =   6060
         OleObjectBlob   =   "ctacte_Factoring.frx":0328
         TabIndex        =   10
         Top             =   270
         Width           =   1320
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
         Height          =   210
         Left            =   7395
         OleObjectBlob   =   "ctacte_Factoring.frx":039C
         TabIndex        =   12
         Top             =   270
         Width           =   1320
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel5 
         Height          =   210
         Left            =   8745
         OleObjectBlob   =   "ctacte_Factoring.frx":0416
         TabIndex        =   13
         Top             =   270
         Width           =   1380
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel6 
         Height          =   210
         Left            =   10140
         OleObjectBlob   =   "ctacte_Factoring.frx":0486
         TabIndex        =   14
         Top             =   270
         Width           =   1350
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel7 
         Height          =   210
         Left            =   11535
         OleObjectBlob   =   "ctacte_Factoring.frx":04F0
         TabIndex        =   15
         Top             =   270
         Width           =   1305
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel8 
         Height          =   165
         Left            =   1695
         OleObjectBlob   =   "ctacte_Factoring.frx":056E
         TabIndex        =   18
         Top             =   270
         Width           =   1305
      End
   End
   Begin MSComctlLib.ListView LvDetalle 
      Height          =   4950
      Left            =   255
      TabIndex        =   17
      Top             =   1980
      Width           =   14115
      _ExtentX        =   24897
      _ExtentY        =   8731
      View            =   3
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   0
      NumItems        =   15
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Object.Tag             =   "T1000"
         Object.Width           =   529
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Object.Tag             =   "T1500"
         Text            =   "Documento"
         Object.Width           =   3175
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Object.Tag             =   "N109"
         Text            =   "Nro Doc."
         Object.Width           =   1587
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Object.Tag             =   "T1300"
         Text            =   "Rut"
         Object.Width           =   2346
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Object.Tag             =   "T2000"
         Text            =   "Cliente"
         Object.Width           =   3528
      EndProperty
      BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   5
         Object.Tag             =   "T1500"
         Text            =   "Sucursal"
         Object.Width           =   2646
      EndProperty
      BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   6
         Object.Tag             =   "T1000"
         Text            =   "Emision"
         Object.Width           =   2205
      EndProperty
      BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   7
         Object.Tag             =   "T1000"
         Text            =   "Vencimiento"
         Object.Width           =   2205
      EndProperty
      BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   8
         Key             =   "total"
         Object.Tag             =   "N100"
         Text            =   "Total"
         Object.Width           =   2381
      EndProperty
      BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   9
         Object.Tag             =   "N100"
         Text            =   "Valor Futuro"
         Object.Width           =   2381
      EndProperty
      BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   10
         Key             =   "saldo"
         Object.Tag             =   "N100S"
         Text            =   "Retencion"
         Object.Width           =   2381
      EndProperty
      BeginProperty ColumnHeader(12) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   11
         Object.Tag             =   "N100S"
         Text            =   "Dev_Rem"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(13) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   12
         Object.Tag             =   "T400"
         Text            =   "Pago Probotem - Fecha"
         Object.Width           =   2646
      EndProperty
      BeginProperty ColumnHeader(14) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   13
         Object.Tag             =   "N100S"
         Text            =   "Pago Probotem - Monto"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(15) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   14
         Object.Tag             =   "N100S"
         Text            =   "Dif. Intereses/Mora"
         Object.Width           =   2540
      EndProperty
   End
End
Attribute VB_Name = "ctacte_Factoring"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public Sm_DocumentosMarcados As String

Private Sub CmdExportar_Click()
    Dim tit(2) As String
    FraProgreso.Visible = True
    tit(0) = Me.Caption
    tit(1) = "FECHA INFORME: " & Date & " - " & Time
    tit(2) = "" ' "                                        DESDE:" & DtInicio.Value & "   HASTA:" & Dtfin.Value
    ExportarNuevo LvDetalle, tit, Me, PBar
    PBar.Value = 1
    FraProgreso.Visible = False
End Sub

Private Sub cmdSalir_Click()
    Unload Me
End Sub



Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
            SendKeys "{tab}"
            KeyAscii = 0
    Else
        If KeyAscii = 8 Or KeyAscii = 45 Then Exit Sub   'If KeyAscii = 46 Then KeyAscii = 44
        KeyAscii = Asc(UCase$(Chr(KeyAscii))) 'Convertimos a mayuscula cualquier caractar
        Dim txtS      As Control '                  ingresado
        Dim Decimales As Integer
        For Each txtS In Controls
            If (TypeOf txtS Is TextBox) Then
                If Me.ActiveControl.Name = txtS.Name Then
                    If Mid(txtS.Tag, 1, 1) = "N" Then 'Tag de textbox si es N = numerico
                        If KeyAscii = 44 Then '                        If Not IsNumeric(KeyAscii) Then
                            If Len(txtS.Tag) > 1 Then
                                Decimales = Mid(txtS.Tag, 2, 1)
                                Replace txtS, ",", "."
                                If Decimales > 0 Then If InStrRev(txtS, ".") > 0 Or InStrRev(txtS, ",") > 0 Then KeyAscii = 0
                            End If
                        Else
                            KeyAscii = AceptaSoloNumeros(KeyAscii)
                        End If                        'If KeyAscii = 46 Then If InStrRev(txtS, ".") > 0 Or InStrRev(txtS, ",") > 0 Then KeyAscii = 0
                    End If
                End If
            End If
        Next
    End If
End Sub
Private Sub CargaDocumentos()
    Sql = "SELECT v.id,CONCAT(doc_nombre,IF(doc_factura_guias='SI',' Por guias','')) doc_nombre,no_documento," & _
                "v.rut_cliente,IFNULL(cli_nombre_fantasia,nombre_rsocial) cliente," & _
                "IF(v.suc_id=0,'CM',CONCAT(suc_ciudad,' - ',suc_direccion)) sucursal," & _
                "fecha,v.ven_fecha_vencimiento,bruto,ROUND(bruto/100*" & LP_FactorValorFuturo & ") valorfuturo,bruto-ROUND(bruto/100*" & LP_FactorValorFuturo & ") retencion " & _
            "FROM ven_doc_venta v INNER JOIN maestro_clientes c USING(rut_cliente) " & _
            "INNER JOIN sis_documentos d USING(doc_id) INNER JOIN par_sucursales s USING(suc_id) " & _
            "WHERE v.doc_id_factura=0 AND v.doc_id<>11   AND v.id IN (" & Sm_DocumentosMarcados & ") " & _
            "ORDER BY id "
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvDetalle, True, True, True, False

    
End Sub

Private Sub Form_Load()
    Aplicar_skin Me
    LLenarCombo CboBanco, "ban_nombre", "ban_id", "par_bancos", "ban_activo='SI'"
    DtFechaOperacion.Value = Date
    DtFechaAbono.Value = Date
    CargaDocumentos
End Sub
Private Sub Timer1_Timer()
    txtNroOperacion.SetFocus
    Timer1.Enabled = False
End Sub


