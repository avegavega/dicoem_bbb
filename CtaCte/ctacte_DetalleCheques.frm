VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form ctacte_DetalleCheques 
   Caption         =   "Detalle de Cheques"
   ClientHeight    =   6555
   ClientLeft      =   4035
   ClientTop       =   930
   ClientWidth     =   6750
   LinkTopic       =   "Form1"
   ScaleHeight     =   6555
   ScaleWidth      =   6750
   Begin VB.Frame Frame3 
      Caption         =   "Ch.Girado al:"
      Height          =   4470
      Left            =   450
      TabIndex        =   15
      Top             =   1470
      Width           =   6060
      Begin VB.ComboBox CboCheques 
         Height          =   315
         Left            =   1440
         Style           =   2  'Dropdown List
         TabIndex        =   5
         Top             =   30
         Visible         =   0   'False
         Width           =   2085
      End
      Begin VB.CommandButton cmdSigue 
         Caption         =   "&Continuar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   120
         TabIndex        =   10
         Top             =   4035
         Width           =   1560
      End
      Begin VB.Frame Frame1 
         Caption         =   "Total"
         Height          =   915
         Left            =   1050
         TabIndex        =   17
         Top             =   3030
         Width           =   4440
         Begin VB.TextBox TxtRequerido 
            Alignment       =   1  'Right Justify
            Height          =   300
            Left            =   405
            TabIndex        =   19
            Text            =   "0"
            Top             =   465
            Width           =   1800
         End
         Begin VB.TextBox TxtSuma 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Left            =   2220
            TabIndex        =   18
            Text            =   "0"
            Top             =   465
            Width           =   2070
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
            Height          =   225
            Left            =   2220
            OleObjectBlob   =   "ctacte_DetalleCheques.frx":0000
            TabIndex        =   20
            Top             =   240
            Width           =   2070
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel5 
            Height          =   225
            Left            =   420
            OleObjectBlob   =   "ctacte_DetalleCheques.frx":0084
            TabIndex        =   21
            Top             =   225
            Width           =   1740
         End
      End
      Begin VB.TextBox TxtNro 
         Alignment       =   1  'Right Justify
         Height          =   300
         Left            =   1440
         ScrollBars      =   2  'Vertical
         TabIndex        =   6
         Top             =   360
         Width           =   2055
      End
      Begin VB.TextBox TxtMonto 
         Alignment       =   1  'Right Justify
         Height          =   300
         Left            =   3510
         TabIndex        =   7
         Top             =   360
         Width           =   1935
      End
      Begin VB.CommandButton cmdOk 
         Caption         =   "Ok"
         Height          =   300
         Left            =   5445
         TabIndex        =   8
         Top             =   360
         Width           =   435
      End
      Begin MSComCtl2.DTPicker DtFecha 
         Height          =   315
         Left            =   195
         TabIndex        =   4
         Top             =   360
         Width           =   1245
         _ExtentX        =   2196
         _ExtentY        =   556
         _Version        =   393216
         Format          =   273416193
         CurrentDate     =   40731
      End
      Begin MSComctlLib.ListView LvDetalle 
         Height          =   2280
         Left            =   225
         TabIndex        =   16
         Top             =   705
         Width           =   5685
         _ExtentX        =   10028
         _ExtentY        =   4022
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   8
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "N100"
            Text            =   "Fecha"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "N109"
            Text            =   "Numero"
            Object.Width           =   3625
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   2
            Object.Tag             =   "N100"
            Text            =   "Monto"
            Object.Width           =   3413
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "Banco"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Text            =   "Plaza"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Text            =   "Autorizacion"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   6
            Object.Tag             =   "T1000"
            Text            =   "nombrebanco"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   7
            Object.Tag             =   "N109"
            Text            =   "id del cheque"
            Object.Width           =   2540
         EndProperty
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Banco"
      Height          =   1185
      Left            =   435
      TabIndex        =   11
      Top             =   300
      Width           =   6090
      Begin VB.ComboBox CboChequeras 
         Height          =   315
         Left            =   135
         Style           =   2  'Dropdown List
         TabIndex        =   1
         Top             =   975
         Width           =   5445
      End
      Begin VB.ComboBox CboBanco 
         Height          =   315
         Left            =   165
         Style           =   2  'Dropdown List
         TabIndex        =   0
         Top             =   540
         Width           =   2070
      End
      Begin VB.TextBox TxtPlaza 
         Height          =   300
         Left            =   2205
         TabIndex        =   2
         Top             =   540
         Width           =   1170
      End
      Begin VB.TextBox TxtAutorizacion 
         Height          =   300
         Left            =   3375
         TabIndex        =   3
         Top             =   540
         Width           =   2205
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   165
         Left            =   150
         OleObjectBlob   =   "ctacte_DetalleCheques.frx":0100
         TabIndex        =   12
         Top             =   345
         Width           =   1305
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   165
         Left            =   2205
         OleObjectBlob   =   "ctacte_DetalleCheques.frx":0168
         TabIndex        =   13
         Top             =   345
         Width           =   885
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   165
         Left            =   3390
         OleObjectBlob   =   "ctacte_DetalleCheques.frx":01D0
         TabIndex        =   14
         Top             =   360
         Width           =   2025
      End
   End
   Begin VB.CommandButton cmdSalir 
      Cancel          =   -1  'True
      Caption         =   "&Retornar"
      Height          =   360
      Left            =   4935
      TabIndex        =   9
      Top             =   6015
      Width           =   1560
   End
   Begin VB.Timer Timer1 
      Interval        =   20
      Left            =   -75
      Top             =   -45
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   105
      OleObjectBlob   =   "ctacte_DetalleCheques.frx":0246
      Top             =   7470
   End
End
Attribute VB_Name = "ctacte_DetalleCheques"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public Sm_Rut As String
Public Sm_ModuloBanco As String
Dim Sm_PlazaCta As String

Private Sub CboBanco_Click()
    If CboBanco.ListIndex > -1 Then CboBanco.Tag = CboBanco.Text
        
        
End Sub

Private Sub CboChequeras_Click()
    If CboChequeras.ListIndex = -1 Then Exit Sub
    'llenarcombo cbobanco,"pla_nombre","pla_id","con_plan_de_cuentas","pla_id=" & cbochequeras.
    Sql = "SELECT pla_nombre,cte_ejecutivo " & _
                "FROM    con_plan_de_cuentas p " & _
                "JOIN ban_cta_cte c ON c.ban_id=p.pla_id " & _
                "JOIN ban_chequera b ON c.cte_id=b.cte_id " & _
                "WHERE c.cte_id = " & CboChequeras.ItemData(CboChequeras.ListIndex)
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        CboBanco.Tag = RsTmp!pla_nombre
        Sm_PlazaCta = RsTmp!cte_ejecutivo
    End If
    
    LLenarCombo CboCheques, "che_numero", "che_id", "ban_cheques", "che_estado='SIN EMITIR' AND chc_id=" & CboChequeras.ItemData(CboChequeras.ListIndex)
    If CboCheques.ListCount = 0 Then Exit Sub
    CboCheques.ListIndex = 0
End Sub

Private Sub CboCheques_Validate(Cancel As Boolean)
    If CboCheques.ListIndex > -1 Then
        txtNro = CboCheques.Text
        txtNro.Tag = CboCheques.ItemData(CboCheques.ListIndex)
    Else
        txtNro = 0
    End If
End Sub

Private Sub CmdOk_Click()
    If Val(txtNro) = 0 Then
        MsgBox "Falta Nro de Cheque...", vbInformation
        If txtNro.Visible Then txtNro.SetFocus
        Exit Sub
    End If
    If Val(TxtMonto) = 0 Then
        MsgBox "Falta monto de cheque...", vbInformation
        TxtMonto.SetFocus
        Exit Sub
    End If
    If Sm_ModuloBanco = "NO" Then
        If CboBanco.ListIndex = -1 Then
            MsgBox "Seleccione Banco...", vbInformation
            CboBanco.SetFocus
            Exit Sub
        End If
    End If
    If LvDetalle.ListItems.Count > 0 Then
        For i = 1 To LvDetalle.ListItems.Count
            If Val(LvDetalle.ListItems(i).SubItems(1)) = Val(txtNro) Then
                MsgBox "Cheque ya fue ingresado...", vbInformation
                If txtNro.Visible Then txtNro.SetFocus Else CboCheques.SetFocus
                Exit Sub
            End If
        Next
    End If

    LvDetalle.ListItems.Add , , DtFecha.Value
    LvDetalle.ListItems(LvDetalle.ListItems.Count).SubItems(1) = txtNro
    LvDetalle.ListItems(LvDetalle.ListItems.Count).SubItems(2) = NumFormat(TxtMonto)
    If Sm_ModuloBanco = "NO" Or ctacte_GestionDePagos.Sm_Cli_Pro = "CLI" Then
        LvDetalle.ListItems(LvDetalle.ListItems.Count).SubItems(3) = CboBanco.ItemData(CboBanco.ListIndex)
        LvDetalle.ListItems(LvDetalle.ListItems.Count).SubItems(4) = TxtPlaza
        LvDetalle.ListItems(LvDetalle.ListItems.Count).SubItems(5) = TxtAutorizacion
        LvDetalle.ListItems(LvDetalle.ListItems.Count).SubItems(6) = CboBanco.Text
    Else
        'Tomar datos de la cta cte
        Sql = "SELECT pla_id ban_id,pla_nombre ban_nombre " & _
              "FROM con_plan_de_cuentas b " & _
              "INNER JOIN ban_cta_cte c ON b.pla_id=c.ban_id " & _
              "INNER JOIN ban_chequera q USING(cte_id) " & _
              "WHERE q.chc_id=" & Me.CboChequeras.ItemData(CboChequeras.ListIndex)
        Consulta RsTmp, Sql
        
        If RsTmp.RecordCount > 0 Then
            id_banco = RsTmp!ban_id
            nom_banco = RsTmp!ban_nombre
        
        End If
        LvDetalle.ListItems(LvDetalle.ListItems.Count).SubItems(3) = id_banco
        LvDetalle.ListItems(LvDetalle.ListItems.Count).SubItems(6) = nom_banco
        LvDetalle.ListItems(LvDetalle.ListItems.Count).SubItems(7) = CboCheques.ItemData(CboCheques.ListIndex)
        If CboCheques.ListIndex < CboCheques.ListCount - 1 Then CboCheques.ListIndex = CboCheques.ListIndex + 1
    End If
    SumaCheues
    DtFecha.Value = DtFecha.Value + 30
    txtNro = Val(txtNro) + 1
    TxtMonto = 0
    If txtNro.Visible Then txtNro.SetFocus Else CboCheques.SetFocus
    
End Sub
Private Sub SumaCheues()
    Me.TxtSuma = 0
    If LvDetalle.ListItems.Count > 0 Then
        For i = 1 To LvDetalle.ListItems.Count
            TxtSuma = CDbl(TxtSuma) + CDbl(LvDetalle.ListItems(i).SubItems(2))
        Next
        TxtSuma = NumFormat(TxtSuma)
    End If

End Sub

Private Sub cmdSalir_Click()
    If MsgBox("No grabara los datos del pago..." & vbNewLine & "Continuar...", vbQuestion + vbYesNo) = vbNo Then Exit Sub
    SG_codigo = Empty
    Unload Me
End Sub

Private Sub cmdSigue_Click()
    If CDbl(TxtRequerido) <> CDbl(TxtSuma) Then
        MsgBox "La suma de los cheques no coincide con el valor del abono...", vbInformation
        Exit Sub
    End If
    'Comprobar si se repite el cheque del mismo cliente con el mismo banco
    For i = 1 To LvDetalle.ListItems.Count
        If Sm_ModuloBanco = "NO" Then
            Sql = "SELECT c.ban_id,c.che_numero,ban_nombre " & _
                  "FROM abo_cheques c,cta_abonos a,par_bancos b " & _
                  "WHERE c.abo_id=c.abo_id AND c.ban_id=b.ban_id AND " & _
                  "a.abo_rut='" & Sm_Rut & "' AND " & _
                  "c.ban_id=" & LvDetalle.ListItems(i).SubItems(3) & " AND " & _
                  "che_numero=" & LvDetalle.ListItems(i).SubItems(1)
            Consulta RsTmp, Sql
            If RsTmp.RecordCount > 0 Then
                MsgBox "El Cheque Nro " & LvDetalle.ListItems(i).SubItems(1) & " del " & RsTmp!ban_nombre & vbNewLine & _
                      "ya ha sido utilizado, corrija antes de continuar...", vbInformation
                      LvDetalle.ListItems(i).Selected = True
                      Exit Sub
            End If
        End If
    Next
    
    
    
    
    
    
    With ctacte_GestionDePagos.LvCheques
        .ListItems.Clear
        For i = 1 To LvDetalle.ListItems.Count
            .ListItems.Add , , LvDetalle.ListItems(i)
            .ListItems(i).SubItems(1) = LvDetalle.ListItems(i).SubItems(1)
            .ListItems(i).SubItems(2) = LvDetalle.ListItems(i).SubItems(2)
            .ListItems(i).SubItems(3) = LvDetalle.ListItems(i).SubItems(3)
            .ListItems(i).SubItems(4) = LvDetalle.ListItems(i).SubItems(4)
            .ListItems(i).SubItems(5) = LvDetalle.ListItems(i).SubItems(5)
            .ListItems(i).SubItems(6) = LvDetalle.ListItems(i).SubItems(6)
            .ListItems(i).SubItems(7) = LvDetalle.ListItems(i).SubItems(7)
            .ListItems(i).SubItems(8) = Sm_PlazaCta
            
        Next
    End With
    SG_codigo = "Graba"
    Unload Me
    
End Sub

Private Sub Form_Load()
    Dim Sp_Cuentas As String
    'Dim Sp_LaFechas As String
    'sp_lafecha
    Aplicar_skin Me
    LLenarCombo CboBanco, "ban_nombre", "ban_id", "par_bancos", "ban_activo='SI'"
    
'    DtFecha.Value = Day(Date) & "-" & IG_Mes_Contable & "-" & IG_Ano_Contable
    DtFecha.Value = Date
    Sm_ModuloBanco = "NO"
    Sql = "SELECT emp_modulo_banco " & _
          "FROM  sis_empresas " & _
          "WHERE rut='" & SP_Rut_Activo & "'"
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then Sm_ModuloBanco = RsTmp!emp_modulo_banco
        
    
    If Sm_ModuloBanco = "SI" And ctacte_GestionDePagos.Sm_Cli_Pro = "PRO" Then
        CboBanco.Visible = False
        TxtPlaza.Visible = False
        TxtAutorizacion.Visible = False
        
        CboChequeras.Top = CboBanco.Top
        
        txtNro.Visible = False
        CboCheques.Visible = True
        CboCheques.Top = Me.txtNro.Top
        Sql = "SELECT cte_id,cte_ejecutivo " & _
              "FROM ban_cta_cte " & _
              "WHERE rut_emp='" & SP_Rut_Activo & "'"
        Consulta RsTmp, Sql
        If RsTmp.RecordCount = 0 Then
            MsgBox "No ha creado cuentas ni chequeras en el M�dulo de Bancos...", vbInformation
            Exit Sub
        Else
            RsTmp.MoveFirst
            Do While Not RsTmp.EOF
                Sp_Cuentas = Sp_Cuentas & RsTmp!cte_id & ","
                Sm_PlazaCta = RsTmp!cte_ejecutivo
                RsTmp.MoveNext
            Loop
            If Len(Sp_Cuentas) > 0 Then Sp_Cuentas = Mid(Sp_Cuentas, 1, Len(Sp_Cuentas) - 1)
        End If
        LLenarCombo CboChequeras, "CONCAT(pla_nombre,' ',CAST(chc_serie_inicial AS CHAR),' AL ',CAST(chc_serie_inicial + chc_cantidad - 1 AS CHAR)) chequera", "chc_id", "ban_chequera c INNER JOIN ban_cta_cte USING(cte_id) INNER JOIN con_plan_de_cuentas z ON z.pla_id=ban_cta_cte.ban_id", "cte_id IN (" & Sp_Cuentas & ") AND " & "(SELECT COUNT(che_id) FROM ban_cheques s WHERE (che_estado='SIN EMITIR' /*OR che_estado='NULO'*/) AND  s.chc_id=c.chc_id)>0"
        If CboChequeras.ListCount > 0 Then CboChequeras.ListIndex = 0
    Else
        CboChequeras.Visible = False
        CboCheques.Visible = False
    End If
        
    
End Sub


Private Sub LvDetalle_DblClick()
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    DtFecha.Value = LvDetalle.SelectedItem
    txtNro = LvDetalle.SelectedItem.SubItems(1)
    TxtMonto = LvDetalle.SelectedItem.SubItems(2)
   
    
    
    If Sm_ModuloBanco = "SI" Then Busca_Id_Combo CboCheques, Val(LvDetalle.SelectedItem.SubItems(7))
    
    DtFecha.SetFocus
    LvDetalle.ListItems.Remove LvDetalle.SelectedItem.Index
    
    
    
    
    
    SumaCheues
End Sub

Private Sub Timer1_Timer()
    On Error Resume Next
    If txtNro.Visible Then txtNro.SetFocus Else CboCheques.SetFocus
    Timer1.Enabled = False
End Sub

Private Sub TxtAutorizacion_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
    If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtAutorizacion_Validate(Cancel As Boolean)
 TxtAutorizacion = Replace(TxtAutorizacion, "'", "")
End Sub

Private Sub TxtMonto_GotFocus()
    If CboCheques.ListIndex > -1 Then
        txtNro = CboCheques.Text
        txtNro.Tag = CboCheques.ItemData(CboCheques.ListIndex)
    Else
        If Sm_ModuloBanco = "SI" And ctacte_GestionDePagos.Sm_Cli_Pro = "PRO" Then txtNro = 0
    End If
    En_Foco TxtMonto
End Sub

Private Sub TxtMonto_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
    If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtNro_GotFocus()
    En_Foco txtNro
End Sub

Private Sub TxtNro_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
    If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub txtNro_Validate(Cancel As Boolean)
    txtNro = Trim(txtNro)
End Sub

Private Sub TxtPlaza_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub TxtPlaza_Validate(Cancel As Boolean)
TxtPlaza = Replace(TxtPlaza, "'", "")
End Sub

