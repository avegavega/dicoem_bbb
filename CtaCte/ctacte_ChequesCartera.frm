VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{DA729E34-689F-49EA-A856-B57046630B73}#1.0#0"; "progressbar-xp.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form ctacte_ChequesCartera 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Cheques en Cartera"
   ClientHeight    =   8385
   ClientLeft      =   1725
   ClientTop       =   2865
   ClientWidth     =   14730
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8385
   ScaleWidth      =   14730
   Begin VB.CommandButton cmdSalir 
      Cancel          =   -1  'True
      Caption         =   "&Retornar"
      Height          =   405
      Left            =   13215
      TabIndex        =   2
      Top             =   7920
      Width           =   1350
   End
   Begin VB.Timer Timer1 
      Interval        =   10
      Left            =   585
      Top             =   7230
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   0
      OleObjectBlob   =   "ctacte_ChequesCartera.frx":0000
      Top             =   7245
   End
   Begin VB.Frame Frame1 
      Caption         =   "Listado de cheques"
      Height          =   7695
      Left            =   60
      TabIndex        =   0
      Top             =   120
      Width           =   14580
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   165
         Left            =   3735
         OleObjectBlob   =   "ctacte_ChequesCartera.frx":0234
         TabIndex        =   25
         Top             =   180
         Width           =   1470
      End
      Begin VB.TextBox TxtNroComprobante 
         Height          =   285
         Left            =   3720
         TabIndex        =   24
         Text            =   "0"
         Top             =   390
         Width           =   1320
      End
      Begin VB.Frame Frame6 
         Caption         =   "Total Cheques seleccionados"
         Height          =   600
         Left            =   3930
         TabIndex        =   22
         Top             =   4395
         Width           =   2850
         Begin VB.TextBox TxtTotalSeleccionados 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00E0E0E0&
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   780
            Locked          =   -1  'True
            TabIndex        =   23
            Top             =   270
            Width           =   1350
         End
      End
      Begin MSComCtl2.DTPicker DtDeposito 
         Height          =   285
         Left            =   6135
         TabIndex        =   21
         Top             =   420
         Visible         =   0   'False
         Width           =   1350
         _ExtentX        =   2381
         _ExtentY        =   503
         _Version        =   393216
         Format          =   273416193
         CurrentDate     =   41153
      End
      Begin VB.ComboBox CboCuenta 
         Appearance      =   0  'Flat
         Height          =   315
         ItemData        =   "ctacte_ChequesCartera.frx":02B0
         Left            =   2010
         List            =   "ctacte_ChequesCartera.frx":02B2
         Style           =   2  'Dropdown List
         TabIndex        =   18
         Top             =   1005
         Visible         =   0   'False
         Width           =   3570
      End
      Begin VB.CommandButton cmdExportar 
         Caption         =   "Exportar a Excel"
         Height          =   300
         Left            =   11895
         TabIndex        =   17
         Top             =   4395
         Width           =   1425
      End
      Begin VB.Frame FraProgreso 
         Caption         =   "Progreso Exportaci�n"
         Height          =   540
         Left            =   405
         TabIndex        =   15
         Top             =   5460
         Visible         =   0   'False
         Width           =   8835
         Begin Proyecto2.XP_ProgressBar PBar 
            Height          =   255
            Left            =   135
            TabIndex        =   16
            Top             =   195
            Width           =   8565
            _ExtentX        =   15108
            _ExtentY        =   450
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BrushStyle      =   0
            Color           =   16777088
            ShowText        =   -1  'True
         End
      End
      Begin VB.Frame Frame4 
         Caption         =   "Estado"
         Height          =   960
         Left            =   10650
         TabIndex        =   13
         Top             =   0
         Width           =   2145
         Begin VB.ComboBox CboEstadoFiltro 
            Height          =   315
            ItemData        =   "ctacte_ChequesCartera.frx":02B4
            Left            =   120
            List            =   "ctacte_ChequesCartera.frx":02B6
            Style           =   2  'Dropdown List
            TabIndex        =   14
            Top             =   525
            Width           =   1860
         End
      End
      Begin VB.ComboBox CboEstado 
         Height          =   315
         Left            =   165
         Style           =   2  'Dropdown List
         TabIndex        =   12
         Top             =   900
         Width           =   1845
      End
      Begin VB.Frame Frame3 
         Caption         =   "Informacion del cheque"
         Height          =   2610
         Left            =   450
         TabIndex        =   10
         Top             =   5010
         Width           =   12540
         Begin MSComctlLib.ListView lvInfo 
            Height          =   1965
            Left            =   390
            TabIndex        =   11
            Top             =   420
            Width           =   11880
            _ExtentX        =   20955
            _ExtentY        =   3466
            View            =   3
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            FullRowSelect   =   -1  'True
            GridLines       =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   0
            NumItems        =   6
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Object.Tag             =   "T1000"
               Text            =   "Fecha Emi."
               Object.Width           =   2117
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Object.Tag             =   "T3000"
               Text            =   "Cliente"
               Object.Width           =   5644
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   2
               Object.Tag             =   "T1500"
               Text            =   "Documento"
               Object.Width           =   3528
            EndProperty
            BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   3
               Object.Tag             =   "N109"
               Text            =   "Nro"
               Object.Width           =   2117
            EndProperty
            BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   4
               Object.Tag             =   "N100"
               Text            =   "Bruto Doc"
               Object.Width           =   2381
            EndProperty
            BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   5
               Object.Tag             =   "N100"
               Text            =   "Monto Abono"
               Object.Width           =   2381
            EndProperty
         End
      End
      Begin VB.Frame Frame2 
         Caption         =   "Total Cheques"
         Height          =   600
         Left            =   7890
         TabIndex        =   8
         Top             =   4380
         Width           =   1995
         Begin VB.TextBox TxtTotales 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00E0E0E0&
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   255
            Locked          =   -1  'True
            TabIndex        =   9
            Top             =   270
            Width           =   1350
         End
      End
      Begin VB.Frame Frame5 
         Caption         =   "Cobrar"
         Height          =   960
         Left            =   8025
         TabIndex        =   4
         Top             =   0
         Width           =   1785
         Begin VB.CommandButton CmdVencidas 
            Caption         =   "Ver"
            Height          =   210
            Left            =   1200
            TabIndex        =   5
            ToolTipText     =   "Filtrar hasta la fecha seleccionada"
            Top             =   285
            Width           =   450
         End
         Begin MSComCtl2.DTPicker DtFecha 
            Height          =   285
            Left            =   315
            TabIndex        =   6
            Top             =   525
            Width           =   1350
            _ExtentX        =   2381
            _ExtentY        =   503
            _Version        =   393216
            Format          =   273416193
            CurrentDate     =   40628
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
            Height          =   240
            Left            =   150
            OleObjectBlob   =   "ctacte_ChequesCartera.frx":02B8
            TabIndex        =   7
            Top             =   315
            Width           =   570
         End
      End
      Begin VB.CommandButton cmdCambiaEstado 
         Caption         =   "&Cambiar Estado"
         Height          =   255
         Left            =   2400
         TabIndex        =   3
         ToolTipText     =   "Cambiar el estado del cheque"
         Top             =   855
         Width           =   1350
      End
      Begin MSComctlLib.ListView LvDetalle 
         Height          =   3060
         Left            =   90
         TabIndex        =   1
         Top             =   1320
         Width           =   14385
         _ExtentX        =   25374
         _ExtentY        =   5398
         View            =   3
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   13
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "T1000"
            Object.Width           =   529
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "N109"
            Text            =   "AboID"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "T1500"
            Text            =   "Nro Cheque"
            Object.Width           =   1940
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Object.Tag             =   "T1300"
            Text            =   "Banco"
            Object.Width           =   4233
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Object.Tag             =   "T2000"
            Text            =   "Plaza"
            Object.Width           =   2646
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Object.Tag             =   "T1500"
            Text            =   "Autorizacion"
            Object.Width           =   2646
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   6
            Object.Tag             =   "F1000"
            Text            =   "Fecha Venc."
            Object.Width           =   2205
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   7
            Object.Tag             =   "N100"
            Text            =   "Monto"
            Object.Width           =   1940
         EndProperty
         BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   8
            Object.Tag             =   "F1200"
            Text            =   "Fecha Deposito/Cobrado"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   9
            Object.Tag             =   "T1400"
            Text            =   "Estado"
            Object.Width           =   2469
         EndProperty
         BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   10
            Object.Tag             =   "T1200"
            Text            =   "Usuario"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(12) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   11
            Object.Tag             =   "T2000"
            Text            =   "A Cuenta"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(13) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   12
            Object.Tag             =   "N109"
            Text            =   "Mod_id"
            Object.Width           =   2540
         EndProperty
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkCuenta 
         Height          =   180
         Left            =   2025
         OleObjectBlob   =   "ctacte_ChequesCartera.frx":0320
         TabIndex        =   19
         Top             =   705
         Visible         =   0   'False
         Width           =   1650
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkDeposito 
         Height          =   180
         Left            =   6135
         OleObjectBlob   =   "ctacte_ChequesCartera.frx":038E
         TabIndex        =   20
         Top             =   225
         Visible         =   0   'False
         Width           =   1650
      End
   End
End
Attribute VB_Name = "ctacte_ChequesCartera"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim Sm_FiltroEstado As String, sm_FiltroFecha As String
Dim Sm_SistemaBanco As String, Lp_IdMov As Long

Private Sub CboEstadoFiltro_Click()
    If CboEstadoFiltro.ListIndex = -1 Then Exit Sub
    Sm_FiltroEstado = " AND che_estado='" & CboEstadoFiltro.Text & "'"
    CargaCheques
End Sub

Private Sub cmdCambiaEstado_Click()
    Dim Sp_CuentaBanco As String, Lp_MId As Long
    If CboEstado.ListIndex = -1 Then
        MsgBox "Seleccione estado para el cheque seleccionado...", vbInformation
        CboEstado.SetFocus
        Exit Sub
    End If
    Sp_CuentaBanco = ""
    
    If Sm_SistemaBanco = "SI" Then
        If CboCuenta.ListIndex > -1 Then Sp_CuentaBanco = CboCuenta.Text
        '11-02-2015
        'Verificamos si la fecha esta dentro del periodo contable-
         ''Control de date distinta de per�odo contable
        If Month(DtDeposito.Value) <> IG_Mes_Contable Or Year(DtDeposito.Value) <> IG_Ano_Contable Then
             MsgBox "Fecha no corresponde al periodo contable...", vbInformation
             DtDeposito.SetFocus
             Exit Sub
        End If
        
        If CboEstado.Text = "DEPOSITADO" Then
            If Val(Me.TxtNroComprobante) = 0 Then
                MsgBox "Falta Nro de comprobante...", vbInformation
                TxtNroComprobante.SetFocus
                Exit Sub
            End If
        End If
    End If
    
    
    
    
    
    On Error GoTo errorGraba
    cn.BeginTrans
    For i = 1 To LvDetalle.ListItems.Count
        If LvDetalle.ListItems(i).Checked Then
        
            If CboEstado.Text <> LvDetalle.ListItems(i).SubItems(9) Then
                'No permite un movimiento si el estado a cambiar es igual
                ' al estado actual del cheque
        
                  If MsgBox("Cambiar de " & LvDetalle.ListItems(i).SubItems(9) & " a " & CboEstado.Text & vbNewLine & "del cheque Nro " & LvDetalle.ListItems(i).SubItems(2), vbQuestion + vbYesNo) = vbNo Then Exit Sub

                
                      Lp_IdMov = 0
                      If Sm_SistemaBanco = "SI" Then
                                
                            
                              Lp_IdMov = UltimoNro("ban_movimientos", "mov_id")
                              If CboEstado.Text = "DEPOSITADO" Then
                              
                                    'Vamos a cambiar esto, a solo un movimiento en la cartola
                                    'no cheque por cheque
                                    '18 Feb 2015
                                    CambiarADepostitados
                                    
                                    
                                    
                                    Exit Sub
                                    Sql = "INSERT INTO com_con_multicuenta (dcu_tipo,id_unico,pla_id,dcu_valor) VALUES"
                                
                                    Sql = Sql & "('DEPOSITO'," & Lp_IdMov & "," & IG_IdCtaClientes & "," & CDbl(LvDetalle.ListItems(i).SubItems(7)) & ")"
                                                              
                                    cn.Execute Sql
                              
                              

                                  Sql = "INSERT INTO ban_movimientos (mov_id,cte_id,mov_detalle,mov_fecha,mov_valor,mov_identificacion,mov_identificacion_id,con_id,des_id,pla_id,mov_de_tesoreria,mov_tipo) " & _
                                            "VALUES(" & Lp_IdMov & "," & CboCuenta.ItemData(CboCuenta.ListIndex) & ",'" & "CHEQUE EN CARTERA','" & Fql(DtDeposito) & "'," & CDbl(LvDetalle.ListItems(i).SubItems(7)) & ",'DEPOSITO',2," & 0 & "," & 0 & "," & IG_IdCtaClientes & ",'SI','INGRESO')"
                                  
                              End If
                              If CboEstado.Text = "CARTERA" Then
                                    'paso = MsgBox("1.-Seleccione SI en caso que sea protestado o devuelto por forma..." & vbNewLine & _
                                                  "2.-Seleccione NO si este documento paso a DEPOSITARSE por error. (En este caso se eliminar� el movimiento en la Cta Cte.", vbYesNoCancel + vbQuestion)
                                    paso = MsgBox("1.-Seleccione SI, en caso que sea CHEQUE PROTESTADO O DEVUELTO POR FORMA..." & vbNewLine & _
                                                  "   En este caso se efectu� un CARGO en MODULO BANCO por el valor del Ch/Protestado." & vbNewLine & _
                                                  "2.-Seleccione NO, si este documento paso a DEPOSITARSE por error ..." & vbNewLine & _
                                                  "   En este caso se elimina el DEPOSITO en MODULO BANCO y vuelve a CHS. EN CARTERA.", vbYesNoCancel + vbQuestion)
                                    If paso = 2 Then
                                         GoTo siguiente
                                    ElseIf paso = 6 Then
                                        'Protestadoo
                                    Else
                                        'Debemos saber si hay mas documentos involucrados en el movimiento
                                        'del cual se va a eliminar, QUE SE INGRESO POR EQUIVOCACION
                                        Lp_MId = Val(LvDetalle.ListItems(i).SubItems(12))
                                        
                                        
                                        Sql = "UPDATE abo_cheques SET che_estado='" & CboEstado.Text & "',che_fecha_deposito='" & Format(DtDeposito, "YYYY-MM-DD") & "',che_cuenta='" & Sp_CuentaBanco & "',mov_id=0 " & _
                                                "WHERE mov_id=" & Lp_MId
                                        cn.Execute Sql
                                        
                                        'Error digitacion:En este caso eliminaremos el movimiento en la Cta Cte Asociada
                                        
                                        cn.Execute "DELETE FROM ban_movimientos " & _
                                                   "WHERE mov_id=" & Lp_MId
                                        GoTo siguiente
                                        
                                    End If
                             End If
                             If CboEstado.Text = "PROTESTADO" Then
                                    
                                    'AL SER UN PROTESTO, SE DEBE CARGAR EL VALOR DEL CHEQUE A LA CTA CTE.
                                    'ENTONCES TOMAREMOS EL MISMO ABONO DONDE ENTRO EL CHEQUE,
                                    'COPIANDOLO PERO CON EL VALOR EN NEGATIVO.
                                    Sql = "INSERT INTO "
                                    Dim Lp_VenId As Long
                                    Dim Sp_Rut_Protesto As String
                                    Dim Ip_IdDocProtesto As Integer
                                    
                                    Sql = "SELECT ven_id " & _
                                            "FROM par_vendedores " & _
                                            "WHERE rut_emp='" & SP_Rut_Activo & "' " & _
                                            "LIMIT 1"
                                            
                                    Consulta RsTmp, Sql
                                    vendersito = 0
                                    If RsTmp.RecordCount > 0 Then vendersito = RsTmp!ven_id
                                    
                                    Consulta RsTmp, "SELECT doc_id " & _
                                            "FROM sis_documentos " & _
                                            "WHERE doc_cheque_protestado='SI'"
                                    If RsTmp.RecordCount > 0 Then
                                        Ip_IdDocProtesto = RsTmp!doc_id
                                    End If

                                    
                                    Sql = "SELECT v.rut_cliente,nombre_cliente " & _
                                            "FROM ven_doc_venta v,abo_cheques s,cta_abono_documentos a,sis_documentos d, maestro_clientes c " & _
                                            "WHERE c.rut_cliente = v.rut_cliente AND v.rut_emp = '" & SP_Rut_Activo & "' AND v.id = a.id AND d.doc_id = v.doc_id AND s.abo_id = a.abo_id AND che_id=" & LvDetalle.ListItems(i)
                                    Consulta RsTmp, Sql
                                    If RsTmp.RecordCount > 0 Then
                                        Sp_Rut_Protesto = RsTmp!rut_cliente
                                    End If
                                            
                                            
                                    
                                    Lp_VenId = UltimoNro("ven_doc_venta", "id")
                                    'Grabamos documento de venta para cuenta corriente
                                    cn.Execute "INSERT INTO ven_doc_venta " & _
                                    "(id,no_documento,fecha,rut_cliente,nombre_cliente,bruto,condicionpago," & _
                                    "forma_pago,tipo_doc,doc_id,usu_nombre,rut_emp,ven_informa_venta,ven_id,ven_plazo_id,sue_id,suc_id) VALUES(" & Lp_VenId & "," & _
                                    LvDetalle.ListItems(i).SubItems(2) & ",'" & Fql(DtDeposito) & "',/*rut*/'" & Sp_Rut_Protesto & "'," & _
                                    "/*nombre*/'" & RsTmp!nombre_cliente & "'," & _
                                    "/*VALOR*/" & CDbl(LvDetalle.ListItems(i).SubItems(7)) & ",'CREDIT0','PENDIENTE',/*DOCUMENTO*/'CARGO CHEQUE PROTESTADO'," & _
                                    "/*ID DOCUMENTO*/" & Ip_IdDocProtesto & ",'" & LogUsuario & "','" & SP_Rut_Activo & "','NO'," & vendersito & ",30," & 1 & ",0)"
                                    
                                    'Agregaremos una linea de detalle
                                    'InsertaProductoProtesto
                                    InsertaProductoProtesto CreaProductoPROTESTO, CDbl(LvDetalle.ListItems(i).SubItems(7)), LvDetalle.ListItems(i).SubItems(2), Ip_IdDocProtesto, "CLI", Lp_VenId, "SI"
                                    
                                    
                                    
                                    
                                    
                                    Sql = "INSERT INTO com_con_multicuenta (dcu_tipo,id_unico,pla_id,dcu_valor) VALUES"
                                    Sql = Sql & "('CARGO'," & Lp_IdMov & "," & IG_IdCtaClientes & "," & CDbl(LvDetalle.ListItems(i).SubItems(7)) & ")"
                                                              
                                    cn.Execute Sql
    
                                            
                                            
                                    Sql = "INSERT INTO ban_movimientos (mov_id,cte_id,mov_detalle,mov_fecha,mov_valor,mov_identificacion,mov_identificacion_id,con_id,des_id,pla_id,mov_de_tesoreria,mov_tipo) " & _
                                            "VALUES(" & Lp_IdMov & "," & CboCuenta.ItemData(CboCuenta.ListIndex) & ",'" & "DEVOLUCION CHEQUE DEPOSITADO','" & Fql(DtDeposito) & "'," & CDbl(LvDetalle.ListItems(i).SubItems(7)) & ",'CARGO',4," & 0 & "," & 0 & "," & IG_IdCtaClientes & ",'SI','CARGO')"
                                  
                              End If
                              cn.Execute Sql
                              GoTo siguiente
                      Else
                            GoTo actualiza
                            'Si no lleva banco debe actualizar el estado
                              
                              
                      End If
                      
actualiza:
                      Sql = "UPDATE abo_cheques SET che_estado='" & CboEstado.Text & "',che_fecha_deposito='" & Format(DtDeposito, "YYYY-MM-DD") & "',che_cuenta='" & Sp_CuentaBanco & "',mov_id=" & Lp_IdMov & " " & _
                              "WHERE che_id=" & LvDetalle.ListItems(i)
                      cn.Execute Sql
           
            End If
siguiente:
          
        End If
    
    Next
    cn.CommitTrans
    CargaCheques
    Exit Sub
errorGraba:
    cn.RollbackTrans
    MsgBox "Ocurrio un error mientras intentaba realizar los cambios..." & vbNewLine & Err.Number & " - " & Err.Description
End Sub
Private Sub CambiarADepostitados()

    On Error GoTo errorDeposito
   
    Sql = "INSERT INTO com_con_multicuenta (dcu_tipo,id_unico,pla_id,dcu_valor) VALUES"
                                
    Sql = Sql & "('DEPOSITO'," & Lp_IdMov & "," & IG_Id_CtaDocumentosCartera & "," & CDbl(Me.TxtTotalSeleccionados) & ")"
                                
    cn.Execute Sql



    Sql = "INSERT INTO ban_movimientos (mov_id,cte_id,mov_detalle,mov_fecha,mov_valor,mov_identificacion,mov_identificacion_id,con_id,des_id,pla_id,mov_de_tesoreria,mov_tipo,mov_nro_comprobante) " & _
              "VALUES(" & Lp_IdMov & "," & CboCuenta.ItemData(CboCuenta.ListIndex) & ",'" & "CHEQUE EN CARTERA','" & Fql(DtDeposito) & "'," & CDbl(Me.TxtTotalSeleccionados) & ",'DEPOSITO',2," & 0 & "," & 0 & "," & IG_IdCtaClientes & ",'SI','INGRESO','" & TxtNroComprobante & "')"
    
    cn.Execute Sql
    
    
    For i = 1 To LvDetalle.ListItems.Count
        If LvDetalle.ListItems(i).Checked Then
            Sql = "UPDATE abo_cheques SET che_estado='" & CboEstado.Text & "',che_fecha_deposito='" & Format(DtDeposito, "YYYY-MM-DD") & "',che_cuenta='" & Sp_CuentaBanco & "',mov_id=" & Lp_IdMov & " " & _
                          "WHERE che_id=" & LvDetalle.ListItems(i)
            cn.Execute Sql
        End If
    Next
    cn.CommitTrans
    CargaCheques
    Exit Sub
errorDeposito:
    MsgBox "Ocurrio un error al intentar cambiar a DEPOSITO los cheques seleccionados...", vbInformation
    cn.RollbackTrans
End Sub
Private Sub CmdExportar_Click()
    Dim tit(2) As String
    FraProgreso.Visible = True
    tit(0) = Me.Caption
    tit(1) = "FECHA INFORME: " & Date & " - " & Time
    tit(2) = "" ' "                                        DESDE:" & DtInicio.Value & "   HASTA:" & Dtfin.Value
    ExportarNuevo LvDetalle, tit, Me, PBar
    PBar.Value = 1
    FraProgreso.Visible = False
End Sub

Private Sub cmdSalir_Click()
    Unload Me
End Sub

Private Sub CmdVencidas_Click()
    sm_FiltroFecha = " AND che_fecha<='" & Format(DtFecha.Value, "YYYY-MM-DD") & "'"
    CargaCheques
End Sub


Private Sub Form_Load()
    Centrar Me
    Aplicar_skin Me
    LLenarCombo CboEstado, "esc_nombre", "esc_id", "par_estados_cheques", "esc_activo='SI' " 'AND esc_id<>1"
    LLenarCombo CboEstadoFiltro, "esc_nombre", "esc_id", "par_estados_cheques", "esc_activo='SI'"
    Sm_FiltroEstado = " AND che_estado='CARTERA'"
    DtFecha.Value = Date + 90
    sm_FiltroFecha = " AND che_fecha<='" & Format(DtFecha.Value, "YYYY-MM-DD") & "'"
    CargaCheques
    DtDeposito = Date
    Sm_SistemaBanco = "NO"
    
    Sql = "SELECT emp_modulo_banco " & _
          "FROM sis_empresas " & _
          "WHERE rut='" & SP_Rut_Activo & "'"
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        If RsTmp!emp_modulo_banco = "SI" Then
            SkDeposito.Visible = True
            DtDeposito.Visible = True
            Sm_SistemaBanco = "SI"
            SkCuenta.Visible = True
            CboCuenta.Visible = True
            cmdCambiaEstado.Left = CboCuenta.Left + CboCuenta.Width + 10
            LLenarCombo CboCuenta, "CONCAT(cte_numero,' ',pla_nombre)", "cte_id", "ban_cta_cte c INNER JOIN con_plan_de_cuentas ON pla_id=ban_id", "cte_activo='SI' AND rut_emp='" & SP_Rut_Activo & "'"
            SkDeposito.Left = CboCuenta.Left
            DtDeposito.Left = CboCuenta.Left

            'LLenarCombo CboCuenta, "CONCAT(cte_numero,' ',ban_nombre)", "cte_id", "ban_cta_cte c INNER JOIN par_bancos USING(ban_id)", "cte_activo='SI' AND rut_emp='" & SP_Rut_Activo & "'"
            If CboCuenta.ListCount > 0 Then
                CboCuenta.ListIndex = 0
            Else
            
            
            End If
        End If
        
    End If
End Sub

Private Sub LvDetalle_Click()
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    Sql = "SELECT v.fecha, nombre_cliente cliente," & _
          "doc_nombre , v.no_documento, v.bruto, a.ctd_monto " & _
          "FROM ven_doc_venta v,abo_cheques s, " & _
               "cta_abono_documentos a,maestro_clientes c,sis_documentos d " & _
         "WHERE v.rut_emp='" & SP_Rut_Activo & "' AND  v.id = a.id And c.rut_cliente = v.rut_cliente " & _
                "AND d.doc_id=v.doc_id AND s.abo_id=a.abo_id " & _
                "AND che_id=" & LvDetalle.SelectedItem
                
    Consulta RsTmp, Sql
    
    LLenar_Grilla RsTmp, Me, lvInfo, False, True, True, False
    
End Sub

Private Sub LvDetalle_ItemCheck(ByVal Item As MSComctlLib.ListItem)
    TxtTotalSeleccionados = 0
    For i = 1 To LvDetalle.ListItems.Count
        If LvDetalle.ListItems(i).Checked Then
            TxtTotalSeleccionados = TxtTotalSeleccionados + CDbl(LvDetalle.ListItems(i).SubItems(7))
        End If
    Next
End Sub

Private Sub Timer1_Timer()
    On Error Resume Next
    LvDetalle.SetFocus
    Timer1.Enabled = False
End Sub
Private Sub CargaCheques()
    TxtTotales = 0
    Sql = "SELECT che_id,c.abo_id,che_numero,ban_nombre,che_plaza,che_autorizacion," & _
          "che_fecha,che_monto,che_fecha_deposito,che_estado,a.usu_nombre,che_cuenta,c.mov_id " & _
          "FROM abo_cheques c " & _
          "INNER JOIN par_bancos b USING(ban_id) " & _
          "INNER JOIN cta_abonos a USING(abo_id) " & _
          "INNER JOIN cta_abono_documentos d USING(abo_id) " & _
          "INNER JOIN ven_doc_venta v USING(id) " & _
          "WHERE d.rut_emp='" & SP_Rut_Activo & "' AND c.rut_emp='" & SP_Rut_Activo & "' AND abo_cli_pro='CLI' AND a.abo_id=c.abo_id AND c.ban_id=b.ban_id" & Sm_FiltroEstado & sm_FiltroFecha & _
          " GROUP BY che_id"
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvDetalle, True, True, True, False
    For i = 1 To LvDetalle.ListItems.Count
        LvDetalle.ListItems(i).ListSubItems(7).Bold = True
        TxtTotales = Val(TxtTotales) + CDbl(LvDetalle.ListItems(i).SubItems(7))
    Next
    TxtTotales = NumFormat(TxtTotales)

End Sub
'ORDENA COLUMNAS
Private Sub LvDetalle_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    ordListView ColumnHeader, Me, LvDetalle
End Sub
Private Sub InsertaProductoProtesto(Codigo As String, PrecioFinal As Long, nro_Doc As Long, Id_Doc As Integer, CliPro As String, IdUnico As Long, Inventario As String)
    If CliPro = "CLI" Then
        If Inventario = "SI" Then
            Sql = "INSERT INTO ven_detalle (codigo,marca,descripcion,precio_real," & _
                               "descuento,unidades,precio_final,subtotal,precio_costo,btn_especial," & _
                               "comision,fecha,no_documento,doc_id,rut_emp,pla_id,are_id,cen_id,aim_id," & _
                               "ved_precio_venta_bruto,ved_precio_venta_neto) VALUES"
              sql2 = "('" & _
                               Codigo & "','PROTESTO','CARRGO PROTESTO'," & _
                              1 & "," & 0 & "," & _
                               1 & "," & PrecioFinal & "," & _
                               PrecioFinal & "," & 1 & ",'" & _
                               "NO','NO','" & Fql(Date) & "'," & _
                               nro_Doc & "," & Id_Doc & ",'" & SP_Rut_Activo & "'," & _
                              0 & "," & 0 & "," & 0 & "," & _
                              0 & "," & 0 & "," & 0 & ")"
        
        End If
    Else
            Sql = "INSERT INTO com_doc_compra_detalle (id,pro_codigo,cmd_detalle,cmd_unitario_bruto," & _
                               "cmd_total_bruto,cmd_cantidad) VALUES"
            sql2 = "(" & _
                               IdUnico & ",'" & Codigo & "','PROTESTO'," & _
                               PrecioFinal & "," & _
                               PrecioFinal & ",1)"
    End If
    cn.Execute Sql & sql2
End Sub
Private Function CreaProductoPROTESTO() As String

    Sql = "SELECT (SELECT mar_id FROM par_marcas WHERE rut_emp='" & SP_Rut_Activo & "' LIMIT 1) mar_id," & _
                    "(SELECT ume_id FROM sis_unidad_medida WHERE rut_emp='" & SP_Rut_Activo & "' LIMIT 1) ume_id," & _
                    "(SELECT tip_id FROM par_tipos_productos WHERE rut_emp='" & SP_Rut_Activo & "' LIMIT 1) tip_id"
    Consulta RsTmp, Sql
    Lp_NewId = UltimoNro("maestro_productos", "codigo")
    
    Sql = "INSERT INTO maestro_productos (" & _
          "codigo,marca,descripcion,precio_compra,porciento_utilidad,precio_venta,margen," & _
          "stock_actual,stock_critico,ubicacion_bodega,comentario,bod_id,mar_id,tip_id,pro_inventariable," & _
          "rut_emp,ume_id,pro_precio_sin_flete,pro_precio_flete,pro_activo) " & _
          "VALUES (" & Lp_NewId & ",'ASI. APERTURA','ASI. PARA APERTURA',1,1," & _
          "2,1,0,0,'SIN UBICACION','SOLO APERTURA',0" & _
           "," & RsTmp!mar_id & "," & RsTmp!tip_id & ",'NO'," & _
           "'" & SP_Rut_Activo & "'," & RsTmp!ume_id & ",1,1,'NO')"
    cn.Execute Sql

    CreaProductoPROTESTO = Str(Lp_NewId)

End Function
Private Sub TxtNroComprobante_GotFocus()
    En_Foco TxtNroComprobante
End Sub
Private Sub TxtNroComprobante_KeyPress(KeyAscii As Integer)
    KeyAscii = AceptaSoloNumeros(KeyAscii)
    If KeyAscii = 39 Then KeyAscii = 0
End Sub
