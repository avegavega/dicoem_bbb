VERSION 5.00
Object = "{DA729E34-689F-49EA-A856-B57046630B73}#1.0#0"; "progressbar-xp.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form Man_Informes_Productos 
   Caption         =   "Informe de Maquinarias en Mantencion"
   ClientHeight    =   6600
   ClientLeft      =   2940
   ClientTop       =   2070
   ClientWidth     =   11955
   LinkTopic       =   "Form1"
   ScaleHeight     =   6600
   ScaleWidth      =   11955
   Begin VB.Timer Timer1 
      Interval        =   1000
      Left            =   855
      Top             =   6240
   End
   Begin VB.Frame Frame1 
      Caption         =   "Maquinas en Mantencion"
      Height          =   5940
      Left            =   150
      TabIndex        =   0
      Top             =   165
      Width           =   11550
      Begin VB.CommandButton Command2 
         Caption         =   "Retornar"
         Height          =   450
         Left            =   9720
         TabIndex        =   5
         Top             =   585
         Width           =   1380
      End
      Begin VB.CommandButton CmdExportar 
         Caption         =   "Exportar a Excel"
         Height          =   450
         Left            =   8250
         TabIndex        =   4
         Top             =   585
         Width           =   1395
      End
      Begin Proyecto2.XP_ProgressBar BarraProgreso 
         Height          =   495
         Left            =   1065
         TabIndex        =   3
         Top             =   2655
         Visible         =   0   'False
         Width           =   8460
         _ExtentX        =   14923
         _ExtentY        =   873
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BrushStyle      =   0
         Color           =   16777088
         Scrolling       =   1
         ShowText        =   -1  'True
      End
      Begin VB.CommandButton Command1 
         Caption         =   "Dejar Maquinaria Disponible"
         Height          =   705
         Left            =   8760
         TabIndex        =   2
         Top             =   5055
         Width           =   2340
      End
      Begin MSComctlLib.ListView LvMaquinas 
         Height          =   3840
         Left            =   150
         TabIndex        =   1
         Top             =   1095
         Width           =   10950
         _ExtentX        =   19315
         _ExtentY        =   6773
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   4
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Codigo"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Codigo Empresa"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Descripcion"
            Object.Width           =   8819
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "Estado"
            Object.Width           =   5292
         EndProperty
      End
   End
End
Attribute VB_Name = "Man_Informes_Productos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub CmdExportar_Click()
 Dim tit(2) As String
    If LvMaquinas.ListItems.Count = 0 Then Exit Sub
    BarraProgreso.Visible = True
    tit(0) = Me.Caption & " " & Date & " - " & Time
    tit(1) = ""
    tit(2) = ""
    ExportarNuevo LvMaquinas, tit, Me, BarraProgreso
    BarraProgreso.Visible = False
End Sub

Private Sub Command1_Click()
''Deja Disponible la maquinaria


    For i = 1 To LvMaquinas.ListItems.Count
        If LvMaquinas.ListItems(i).Checked Then
        If MsgBox("Seguro que desea Liberar Maquinaria: " & Me.LvMaquinas.ListItems(i).SubItems(1) & "", vbOKCancel + vbQuestion) = vbCancel Then Exit Sub
    Sql = "UPDATE maestro_productos " & _
    "SET pro_estado = 'DISPONIBLE'" & _
    " WHERE  rut_emp='" & SP_Rut_Activo & "' AND pro_codigo_interno='" & Me.LvMaquinas.ListItems(i).SubItems(1) & "'"
    cn.Execute Sql
        End If
    Next
    Form_Load
End Sub

Private Sub Command2_Click()
Unload Me
End Sub

Private Sub Form_Load()
    Centrar Me
    
        Sql = "SELECT codigo,pro_codigo_interno,descripcion,pro_estado " & _
              "FROM maestro_productos " & _
              "WHERE  pro_estado='En Mantencion'"
     
    
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvMaquinas, True, True, True, False
End Sub

Private Sub Timer1_Timer()
    On Error Resume Next
    Aplicar_skin Me
    Me.Show
    Timer1.Enabled = False
End Sub
