VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form AgregoCliente 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Nuevo Cliente"
   ClientHeight    =   9720
   ClientLeft      =   2625
   ClientTop       =   645
   ClientWidth     =   11190
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   9720
   ScaleWidth      =   11190
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Frame1 
      Caption         =   "Datos"
      Height          =   9150
      Left            =   405
      TabIndex        =   14
      Top             =   150
      Width           =   10560
      Begin VB.Frame Frame5 
         Caption         =   "Cliente Bloqueado"
         Height          =   660
         Left            =   6495
         TabIndex        =   65
         Top             =   1950
         Width           =   3495
         Begin VB.TextBox TxtMotivoBloqueo 
            Height          =   285
            Left            =   1080
            TabIndex        =   67
            Text            =   "MOTIVO"
            Top             =   285
            Width           =   2295
         End
         Begin VB.ComboBox CboBloqueado 
            Appearance      =   0  'Flat
            Height          =   315
            ItemData        =   "AgregoCliente.frx":0000
            Left            =   120
            List            =   "AgregoCliente.frx":000A
            Style           =   2  'Dropdown List
            TabIndex        =   66
            ToolTipText     =   "No lo borra, pero no permite seleccionarlo para ventas"
            Top             =   285
            Width           =   915
         End
      End
      Begin VB.Frame Frame4 
         Caption         =   "Giros"
         Height          =   1785
         Left            =   105
         TabIndex        =   59
         Top             =   4845
         Width           =   9375
         Begin VB.CommandButton CmdGiroOk 
            Caption         =   "Ok"
            Height          =   285
            Left            =   7620
            TabIndex        =   63
            Top             =   255
            Width           =   330
         End
         Begin VB.TextBox TxtGiroNombre 
            Height          =   315
            Left            =   1305
            TabIndex        =   62
            Tag             =   "T"
            Top             =   255
            Width           =   6300
         End
         Begin VB.TextBox txtGiroId 
            BackColor       =   &H8000000F&
            Height          =   315
            Left            =   270
            Locked          =   -1  'True
            TabIndex        =   61
            TabStop         =   0   'False
            Tag             =   "T"
            ToolTipText     =   "Codigo interno, autom�tico"
            Top             =   255
            Width           =   1035
         End
         Begin MSComctlLib.ListView LvGiros 
            Height          =   1080
            Left            =   270
            TabIndex        =   60
            Top             =   585
            Width           =   7710
            _ExtentX        =   13600
            _ExtentY        =   1905
            View            =   3
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            FullRowSelect   =   -1  'True
            GridLines       =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   0
            NumItems        =   2
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Object.Tag             =   "N109"
               Text            =   "Id Giro"
               Object.Width           =   1826
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Object.Tag             =   "T6300"
               Text            =   "Descripcion del Giro"
               Object.Width           =   11112
            EndProperty
         End
      End
      Begin VB.CommandButton CmdVendedores 
         Caption         =   "Vendedores"
         Height          =   300
         Left            =   9045
         TabIndex        =   58
         Top             =   2895
         Width           =   1080
      End
      Begin VB.Frame Frame3 
         Caption         =   "Monto Credito Aprobado"
         Height          =   1065
         Left            =   6480
         TabIndex        =   54
         Top             =   840
         Width           =   3600
         Begin VB.ComboBox CboPlazos 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            ItemData        =   "AgregoCliente.frx":0016
            Left            =   885
            List            =   "AgregoCliente.frx":0018
            Style           =   2  'Dropdown List
            TabIndex        =   68
            Top             =   720
            Width           =   2640
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkAutoriza 
            Height          =   210
            Left            =   135
            OleObjectBlob   =   "AgregoCliente.frx":001A
            TabIndex        =   57
            Top             =   495
            Width           =   2880
         End
         Begin VB.CommandButton CmdCambiaMontoCredito 
            Caption         =   "Cambiar"
            Height          =   285
            Left            =   2610
            TabIndex        =   56
            Top             =   225
            Width           =   885
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkCredito 
            Height          =   315
            Left            =   105
            OleObjectBlob   =   "AgregoCliente.frx":008A
            TabIndex        =   55
            Top             =   225
            Width           =   1680
         End
      End
      Begin VB.ComboBox CboVendedores 
         Appearance      =   0  'Flat
         Height          =   315
         ItemData        =   "AgregoCliente.frx":00EA
         Left            =   5970
         List            =   "AgregoCliente.frx":00EC
         Style           =   2  'Dropdown List
         TabIndex        =   52
         Top             =   2895
         Width           =   3090
      End
      Begin VB.ComboBox CboListaPrecios 
         Appearance      =   0  'Flat
         Height          =   315
         ItemData        =   "AgregoCliente.frx":00EE
         Left            =   5970
         List            =   "AgregoCliente.frx":00F0
         Style           =   2  'Dropdown List
         TabIndex        =   51
         Top             =   3480
         Width           =   3090
      End
      Begin VB.ComboBox CboActivo 
         Appearance      =   0  'Flat
         Height          =   315
         ItemData        =   "AgregoCliente.frx":00F2
         Left            =   9045
         List            =   "AgregoCliente.frx":00FC
         Style           =   2  'Dropdown List
         TabIndex        =   49
         ToolTipText     =   "No lo borra, pero no permite seleccionarlo para ventas"
         Top             =   285
         Width           =   915
      End
      Begin VB.TextBox txtMail1 
         Height          =   315
         Left            =   5715
         TabIndex        =   44
         Top             =   3810
         Width           =   3345
      End
      Begin VB.TextBox txtMail2 
         Height          =   315
         Left            =   5715
         TabIndex        =   43
         Top             =   4155
         Width           =   3345
      End
      Begin VB.TextBox txtMail3 
         Height          =   315
         Left            =   5715
         TabIndex        =   42
         Top             =   4470
         Width           =   3345
      End
      Begin VB.TextBox txtContacto3 
         Height          =   315
         Left            =   2100
         TabIndex        =   40
         Top             =   4440
         Width           =   2850
      End
      Begin VB.TextBox txtContacto2 
         Height          =   315
         Left            =   2115
         TabIndex        =   38
         Top             =   4110
         Width           =   2850
      End
      Begin VB.CommandButton CmdGuarda 
         Appearance      =   0  'Flat
         Caption         =   "&Guardar"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   420
         Left            =   315
         TabIndex        =   31
         ToolTipText     =   "Graba nuevo cliente"
         Top             =   8550
         Width           =   9390
      End
      Begin VB.TextBox txtContacto 
         Height          =   315
         Left            =   2115
         TabIndex        =   11
         Top             =   3795
         Width           =   2850
      End
      Begin VB.Frame Frame2 
         Caption         =   "Sucursales"
         Height          =   1890
         Left            =   120
         TabIndex        =   28
         Top             =   6660
         Width           =   9390
         Begin VB.ComboBox CboGiros 
            Appearance      =   0  'Flat
            Height          =   315
            ItemData        =   "AgregoCliente.frx":0108
            Left            =   6855
            List            =   "AgregoCliente.frx":010A
            Style           =   2  'Dropdown List
            TabIndex        =   64
            Top             =   315
            Width           =   1155
         End
         Begin VB.CommandButton CmdOk 
            Caption         =   "Ok"
            Height          =   285
            Left            =   8730
            TabIndex        =   36
            Top             =   315
            Width           =   330
         End
         Begin VB.ComboBox CboSucActivo 
            Appearance      =   0  'Flat
            Height          =   315
            ItemData        =   "AgregoCliente.frx":010C
            Left            =   7980
            List            =   "AgregoCliente.frx":0116
            Style           =   2  'Dropdown List
            TabIndex        =   35
            Top             =   315
            Width           =   750
         End
         Begin VB.TextBox TxtSucContacto 
            Height          =   315
            Left            =   5040
            TabIndex        =   34
            Tag             =   "T"
            Top             =   315
            Width           =   1800
         End
         Begin VB.TextBox TxtSucDireccion 
            Height          =   315
            Left            =   2145
            TabIndex        =   33
            Tag             =   "T"
            Top             =   315
            Width           =   2900
         End
         Begin VB.TextBox TxtSucCiudad 
            Height          =   315
            Left            =   240
            TabIndex        =   32
            Tag             =   "T"
            Top             =   315
            Width           =   1900
         End
         Begin MSComctlLib.ListView LvDetalle 
            Height          =   1155
            Left            =   240
            TabIndex        =   30
            Top             =   630
            Width           =   8895
            _ExtentX        =   15690
            _ExtentY        =   2037
            View            =   3
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            FullRowSelect   =   -1  'True
            GridLines       =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   0
            NumItems        =   6
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Object.Tag             =   "N109"
               Text            =   "cli_id"
               Object.Width           =   0
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Object.Tag             =   "T1300"
               Text            =   "Ciudad"
               Object.Width           =   3351
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   2
               Object.Tag             =   "T3000"
               Text            =   "Direccion"
               Object.Width           =   5115
            EndProperty
            BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   3
               Object.Tag             =   "T1900"
               Text            =   "Comuna"
               Object.Width           =   3175
            EndProperty
            BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   4
               Object.Tag             =   "N109"
               Text            =   "Cod Giro"
               Object.Width           =   2037
            EndProperty
            BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   5
               Object.Tag             =   "T800"
               Text            =   "Activo"
               Object.Width           =   1323
            EndProperty
         End
         Begin VB.Label LBIdSuc 
            Height          =   225
            Left            =   15
            TabIndex        =   37
            Top             =   360
            Width           =   255
         End
      End
      Begin VB.TextBox TxtFantasia 
         Height          =   315
         Left            =   2115
         MaxLength       =   50
         TabIndex        =   2
         Top             =   900
         Width           =   3375
      End
      Begin VB.TextBox TxtRecargo 
         Alignment       =   1  'Right Justify
         Height          =   315
         Left            =   10620
         TabIndex        =   25
         Text            =   "0"
         Top             =   390
         Visible         =   0   'False
         Width           =   1095
      End
      Begin VB.TextBox TxtMail 
         Height          =   315
         Left            =   2115
         MaxLength       =   50
         TabIndex        =   8
         Top             =   2850
         Width           =   3495
      End
      Begin VB.TextBox TxtFono 
         Height          =   315
         Left            =   2115
         MaxLength       =   15
         TabIndex        =   6
         Text            =   "0"
         Top             =   2190
         Width           =   3375
      End
      Begin VB.TextBox TxtCiudad 
         Height          =   315
         Left            =   2115
         MaxLength       =   25
         TabIndex        =   5
         Top             =   1860
         Width           =   3375
      End
      Begin VB.TextBox TxtComuna 
         Height          =   315
         Left            =   2115
         MaxLength       =   25
         TabIndex        =   4
         Top             =   1530
         Width           =   3375
      End
      Begin VB.TextBox TxtDirec 
         Height          =   315
         Left            =   2115
         MaxLength       =   50
         TabIndex        =   3
         Top             =   1200
         Width           =   3375
      End
      Begin VB.TextBox TxtRz 
         Height          =   315
         Left            =   2115
         MaxLength       =   50
         TabIndex        =   1
         Top             =   570
         Width           =   4350
      End
      Begin VB.TextBox TxtRut 
         Alignment       =   1  'Right Justify
         Height          =   315
         Left            =   2115
         TabIndex        =   0
         Text            =   " "
         Top             =   255
         Width           =   2055
      End
      Begin VB.TextBox TxtGiro 
         Height          =   315
         Left            =   2130
         TabIndex        =   7
         Top             =   2520
         Width           =   3375
      End
      Begin VB.TextBox TxtDesuentoCliente 
         Alignment       =   1  'Right Justify
         Height          =   315
         Left            =   2115
         MaxLength       =   2
         TabIndex        =   9
         Top             =   3165
         Width           =   1095
      End
      Begin VB.TextBox TxtObs 
         Height          =   315
         Left            =   2115
         TabIndex        =   10
         Top             =   3465
         Width           =   3855
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel8 
         Height          =   210
         Left            =   405
         OleObjectBlob   =   "AgregoCliente.frx":0122
         TabIndex        =   15
         Top             =   2910
         Width           =   1605
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel6 
         Height          =   210
         Left            =   405
         OleObjectBlob   =   "AgregoCliente.frx":0185
         TabIndex        =   16
         Top             =   2250
         Width           =   1605
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel5 
         Height          =   255
         Left            =   405
         OleObjectBlob   =   "AgregoCliente.frx":01E4
         TabIndex        =   17
         Top             =   1920
         Width           =   1605
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
         Height          =   375
         Left            =   405
         OleObjectBlob   =   "AgregoCliente.frx":0247
         TabIndex        =   18
         Top             =   1575
         Width           =   1605
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   255
         Left            =   405
         OleObjectBlob   =   "AgregoCliente.frx":02AA
         TabIndex        =   19
         Top             =   1275
         Width           =   1605
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   255
         Left            =   405
         OleObjectBlob   =   "AgregoCliente.frx":0313
         TabIndex        =   20
         Top             =   585
         Width           =   1605
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   375
         Left            =   405
         OleObjectBlob   =   "AgregoCliente.frx":0382
         TabIndex        =   21
         Top             =   300
         Width           =   1605
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel9 
         Height          =   255
         Left            =   405
         OleObjectBlob   =   "AgregoCliente.frx":03E3
         TabIndex        =   22
         Top             =   2565
         Width           =   1605
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel10 
         Height          =   270
         Left            =   60
         OleObjectBlob   =   "AgregoCliente.frx":0442
         TabIndex        =   23
         Top             =   3195
         Width           =   1950
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel11 
         Height          =   255
         Left            =   405
         OleObjectBlob   =   "AgregoCliente.frx":04C5
         TabIndex        =   24
         Top             =   3525
         Width           =   1605
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel7 
         Height          =   210
         Left            =   8895
         OleObjectBlob   =   "AgregoCliente.frx":0536
         TabIndex        =   26
         Top             =   435
         Visible         =   0   'False
         Width           =   1605
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel12 
         Height          =   255
         Left            =   150
         OleObjectBlob   =   "AgregoCliente.frx":059B
         TabIndex        =   27
         Top             =   930
         Width           =   1860
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel13 
         Height          =   255
         Left            =   405
         OleObjectBlob   =   "AgregoCliente.frx":0610
         TabIndex        =   29
         Top             =   3840
         Width           =   1620
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel14 
         Height          =   255
         Left            =   405
         OleObjectBlob   =   "AgregoCliente.frx":067B
         TabIndex        =   39
         Top             =   4155
         Width           =   1620
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel15 
         Height          =   255
         Left            =   405
         OleObjectBlob   =   "AgregoCliente.frx":06E6
         TabIndex        =   41
         Top             =   4470
         Width           =   1620
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel16 
         Height          =   255
         Left            =   5010
         OleObjectBlob   =   "AgregoCliente.frx":0751
         TabIndex        =   45
         Top             =   3855
         Width           =   615
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel17 
         Height          =   255
         Left            =   4965
         OleObjectBlob   =   "AgregoCliente.frx":07B4
         TabIndex        =   46
         Top             =   4170
         Width           =   660
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel18 
         Height          =   255
         Left            =   4995
         OleObjectBlob   =   "AgregoCliente.frx":0817
         TabIndex        =   47
         Top             =   4485
         Width           =   630
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel19 
         Height          =   375
         Left            =   6630
         OleObjectBlob   =   "AgregoCliente.frx":087A
         TabIndex        =   48
         Top             =   345
         Width           =   2355
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel20 
         Height          =   270
         Left            =   6000
         OleObjectBlob   =   "AgregoCliente.frx":08E5
         TabIndex        =   50
         Top             =   3255
         Width           =   1530
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel21 
         Height          =   255
         Left            =   5985
         OleObjectBlob   =   "AgregoCliente.frx":095C
         TabIndex        =   53
         Top             =   2655
         Width           =   1530
      End
   End
   Begin VB.CommandButton CmdSale 
      Cancel          =   -1  'True
      Caption         =   "&Retornar"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   9165
      TabIndex        =   12
      ToolTipText     =   "Salir"
      Top             =   9345
      Width           =   1530
   End
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Interval        =   50
      Left            =   -120
      Top             =   8820
   End
   Begin VB.Timer Timer2 
      Interval        =   10
      Left            =   1050
      Top             =   8955
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   6840
      OleObjectBlob   =   "AgregoCliente.frx":09C3
      Top             =   3120
   End
   Begin VB.Label Label1 
      Caption         =   "Label1"
      Height          =   375
      Left            =   2535
      TabIndex        =   13
      Top             =   345
      Visible         =   0   'False
      Width           =   2415
   End
End
Attribute VB_Name = "AgregoCliente"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim NoGuardar As Boolean
Dim Bp_Nuevo As Boolean




Private Sub CmdCambiaMontoCredito_Click()
    SG_codigo2 = Empty
    sis_InputBox.FramBox = "<Ingrese contrase�a para autorizar>"
    sis_InputBox.Sm_TipoDato = "T"
    sis_InputBox.Show 1
    Sp_Llave = UCase(SG_codigo2)
    If Len(Sp_Llave) = 0 Then Exit Sub
    Sql = "SELECT usu_id,usu_nombre,usu_login " & _
          "FROM sis_usuarios " & _
          "WHERE usu_pwd = MD5('" & Sp_Llave & "')"
    Consulta RsTmp3, Sql
    If RsTmp3.RecordCount > 0 Then
        SG_codigo2 = Empty
        sis_InputBox.FramBox = "Ingrese monto de Credito"
        sis_InputBox.Caption = "Autorizando Cr�dito"
        sis_InputBox.texto.PasswordChar = ""
        sis_InputBox.texto.Tag = "N"
        sis_InputBox.texto = SkCredito
        sis_InputBox.Sm_TipoDato = "N"
        sis_InputBox.Show 1
        If SG_codigo2 = Empty Then
            'SKCREDITO
        Else
            SkAutoriza = "Autoriz�:" & RsTmp3!usu_nombre
            SkCredito = NumFormat(Val(SG_codigo2))
        End If
        
    Else
        MsgBox "Usuario no autorizado !!!", vbExclamation
        
    End If
End Sub

Private Sub CmdGiroOk_Click()
    If Len(Me.TxtGiroNombre) = 0 Then
        MsgBox "Debe ingresar descripci�n del giro...", vbInformation
        TxtGiroNombre.SetFocus
        Exit Sub
    End If
    
    If Len(TxtRut) = 0 Then
        MsgBox "A�n no ha ingresado rut del cliente...", vbInformation
        TxtRut.SetFocus
        Exit Sub
    End If
    
    If Val(txtGiroId) = 0 Then
        'insert
        Sql = "INSERT INTO par_clientes_giros (gir_nombre,cli_rut) VALUES('" & _
            TxtGiroNombre & "','" & TxtRut & "')"
        
    Else
        Sql = "UPDATE par_clientes_giros SET gir_nombre='" & TxtGiroNombre & "' " & _
                "WHERE gir_id=" & txtGiroId
        'update
    End If
    txtGiroId = ""
    TxtGiroNombre = ""
    cn.Execute Sql
    LlenaGiros
End Sub
Private Sub LlenaGiros()
    Sql = "SELECT gir_id,gir_nombre giro " & _
            "FROM par_clientes_giros " & _
            "WHERE cli_rut='" & TxtRut & "'"
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvGiros, False, True, True, False
    
    LLenarCombo CboGiros, "gir_id", "gir_id giro", "par_clientes_giros", "cli_rut='" & TxtRut & "'"
    CboGiros.AddItem "PRINCIPAL", 0
End Sub

Private Sub CmdGuarda_Click()
    Dim Ip_IdVendedor As Integer
    Dim Lp_DiasPlazo As Integer
    
    If CboVendedores.ListCount = 0 Then
        MsgBox "Debe crear vendedor...", vbInformation
        CboVendedores.SetFocus
        Exit Sub
    ElseIf CboVendedores.ListIndex = -1 Then
        MsgBox "Seleccione Vendedor...", vbInformation
        CboVendedores.SetFocus
        Exit Sub
    End If
    
    Sql = "SELECT * " & _
          "FROM maestro_clientes " & _
          "WHERE rut_cliente='" & TxtRut & "'"
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        If Bp_Nuevo Then
            MsgBox "Ya existe un CLIENTE con este RUT...", vbExclamation
            Me.TxtRut.SetFocus
            Exit Sub
        End If
    End If
    
    If Me.CboBloqueado.Text = "SI" Then
        If Len(TxtMotivoBloqueo) = 0 Then
            MsgBox "Debe ingresar motivo por el cual se bloquea al cliente...", vbInformation
            TxtMotivoBloqueo.SetFocus
            Exit Sub
        End If
    End If
    
    If CboListaPrecios.ListIndex = -1 Then CboListaPrecios.ListIndex = 0
    
    If CboPlazos.ListIndex = -1 Then Lp_DiasPlazo = 0 Else Lp_DiasPlazo = CboPlazos.ItemData(CboPlazos.ListIndex)
    
    On Error GoTo Graba
    
        cn.BeginTrans
            If CboVendedores.ListCount = 0 Then Ip_IdVendedor = 0 Else Ip_IdVendedor = CboVendedores.ItemData(CboVendedores.ListIndex)
            If Bp_Nuevo Then
                
                Sql = "INSERT INTO par_asociacion_ruts (rut_emp,rut_cli) " & _
                       "VALUES('" & SP_Rut_Activo & "','" & TxtRut & "')"
                cn.Execute Sql
                
                Sql = "INSERT INTO maestro_clientes (rut_cliente,nombre_rsocial,direccion,comuna,ciudad," & _
                                                     "fono,email,cli_nombre_fantasia,cli_contacto,comentario," & _
                                                     "giro,descuento,cli_contacto2,cli_contacto3," & _
                                                     "cli_mail1,cli_mail2,cli_mail3,habilitado,lst_id,ven_id," & _
                                                     "cli_usu_autoriza_credito,cli_monto_credito,cli_bloqueado_dicoem,cli_motivo_bloqueo,anticipo) " & _
                      "VALUES('" & TxtRut & "','" & TxtRz & "','" & TxtDirec & "','" & txtComuna & "','" & TxtCiudad & "','" & _
                                     txtFono & "','" & TxtMail & "','" & TxtFantasia & "','" & txtContacto & "','" & _
                                     TxtObs & "','" & TxtGiro & "'," & Val(TxtDesuentoCliente) & ",'" & txtContacto2 & "','" & txtContacto3 & "','" & _
                                     txtMail1 & "','" & txtMail2 & "','" & txtMail3 & "','" & CboActivo.Text & "'," & _
                                     CboListaPrecios.ItemData(CboListaPrecios.ListIndex) & "," & Ip_IdVendedor & ",'" & SkAutoriza & "'," & CDbl(SkCredito) & ",'" & CboBloqueado.Text & "','" & Me.TxtMotivoBloqueo & "'," & Lp_DiasPlazo & ")"
            Else
                Sql = "UPDATE maestro_clientes SET nombre_rsocial='" & TxtRz & "'," & _
                                                  "direccion='" & TxtDirec & "'," & _
                                                  "comuna='" & txtComuna & "'," & _
                                                  "ciudad='" & TxtCiudad & "'," & _
                                                  "fono='" & txtFono & "'," & _
                                                  "email='" & TxtMail & "'," & _
                                                  "cli_nombre_fantasia='" & TxtFantasia & "'," & _
                                                  "cli_contacto='" & txtContacto & "'," & _
                                                  "comentario='" & TxtObs & "'," & _
                                                  "giro='" & TxtGiro & "'," & _
                                                  "descuento=" & Val(TxtDesuentoCliente) & "," & _
                                                  "cli_contacto2='" & txtContacto2 & "'," & _
                                                  "cli_contacto3='" & txtContacto3 & "'," & _
                                                  "cli_mail1='" & txtMail1 & "'," & _
                                                  "cli_mail2='" & txtMail2 & "'," & _
                                                  "cli_mail3='" & txtMail3 & "', " & _
                                                  "habilitado='" & CboActivo.Text & "', " & _
                                                  "lst_id=" & CboListaPrecios.ItemData(CboListaPrecios.ListIndex) & ", " & _
                                                  "ven_id=" & Ip_IdVendedor & ", " & _
                                                  "cli_usu_autoriza_credito='" & SkAutoriza & "'," & _
                                                  "cli_monto_credito=" & CDbl(SkCredito) & " " & _
                                                  ",cli_bloqueado_dicoem='" & CboBloqueado.Text & "'," & _
                                                  "cli_motivo_bloqueo='" & TxtMotivoBloqueo & "', " & _
                                                  "anticipo=" & Lp_DiasPlazo & " " & _
                       "WHERE rut_cliente='" & TxtRut & "'"
            End If
            cn.Execute Sql
            
            Sql = "DELETE FROM par_asociacion_lista_precios " & _
                  "WHERE rut_emp='" & SP_Rut_Activo & "' AND cli_rut='" & Me.TxtRut & "'"
            cn.Execute Sql
            
            Sql = "INSERT INTO par_asociacion_lista_precios (rut_emp,cli_rut,lst_id) " & _
                              "VALUES('" & SP_Rut_Activo & "','" & TxtRut & "'," & CboListaPrecios.ItemData(CboListaPrecios.ListIndex) & "    )"
            cn.Execute Sql
        cn.CommitTrans
        
    rutProveedor = True
    Unload Me
    Exit Sub
Graba:
    MsgBox "Problema al intentar Grabar..." & vbNewLine & Err.Description & vbNewLine & Err.Number, vbExclamation
    cn.RollbackTrans
End Sub

Private Sub CmdOk_Click()
    Dim Ip_IdGiro As Integer
    If Len(TxtRut) = 0 Then
        MsgBox "Falta RUT cliente ...", vbInformation
        TxtRut.SetFocus
        Exit Sub
    End If
    If Len(TxtSucCiudad) = 0 Or Len(TxtSucDireccion) = 0 Or Len(Me.TxtSucContacto) = 0 Then
        MsgBox "Faltan datos para Sucursal...", vbInformation
        TxtSucCiudad.SetFocus
        Exit Sub
    End If
    
    If CboGiros.ListIndex = -1 Then
        MsgBox "Seleccione giro de la sucursal..."
        CboGiros.SetFocus
        Exit Sub
    End If
    
    If CboGiros.Text = "PRINCIPAL" Then Ip_IdGiro = 0 Else Ip_IdGiro = CboGiros.Text
    
    If Val(LBIdSuc) = 0 Then
        'Nueva sucursal
        Sql = "INSERT INTO par_sucursales (rut_cliente,suc_ciudad,suc_direccion,suc_contacto,suc_activo,gir_id) " & _
              "VALUES('" & TxtRut & "','" & TxtSucCiudad & "','" & TxtSucDireccion & "','" & TxtSucContacto & "','" & Me.CboSucActivo.Text & "'," & Ip_IdGiro & ")"
    Else
        'Actualiza sucursal
        Sql = "UPDATE par_sucursales SET suc_ciudad='" & TxtSucCiudad & "'," & _
                                        "suc_direccion='" & TxtSucDireccion & "'," & _
                                        "suc_contacto='" & TxtSucContacto & "'," & _
                                        "suc_activo='" & CboSucActivo.Text & "', " & _
                                        "gir_id=" & Ip_IdGiro & " " & _
               "WHERE suc_id=" & LBIdSuc
    End If
    TxtSucCiudad = Empty
    TxtSucDireccion = Empty
    TxtSucContacto = Empty
    Consulta RsTmp, Sql
    LBIdSuc = 0
    CargaSucursales
End Sub

Private Sub CmdSale_Click()
    Unload Me
End Sub

Private Sub CmdVendedores_Click()
    Mantenedor_Vendedores.Show 1
      'VENDEDORES PARA FILTRO
    LLenarCombo CboVendedores, "ven_nombre", "ven_id", "par_vendedores", "ven_activo='SI' AND rut_emp='" & SP_Rut_Activo & "'", "ven_nombre"
    If CboVendedores.ListCount > 0 Then
        CboVendedores.ListIndex = 0
    Else
        MsgBox "Primero debe crear Vendedores...", vbInformation
        CmdSale_Click
        Exit Sub
    End If
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    On Error Resume Next
    If KeyAscii = 13 Then SendKeys "{Tab}"
End Sub

Private Sub Form_Load()
    Dim Ip_ID_Lp As Double
    Centrar Me
    Aplicar_skin Me
    Me.Timer1.Enabled = True
    CboActivo.ListIndex = 0
    NoGuardar = False
    Me.CboBloqueado.ListIndex = 1
    LLenarCombo CboPlazos, "CONCAT(pla_nombre,'                                    ',CAST(pla_id AS CHAR))", "pla_dias", "par_plazos_vencimiento", "pla_activo='SI' AND pla_dias>1", "pla_orden"

    'VENDEDORES PARA FILTRO
    LLenarCombo CboVendedores, "ven_nombre", "ven_id", "par_vendedores", "ven_activo='SI' AND rut_emp='" & SP_Rut_Activo & "'", "ven_nombre"
    If CboVendedores.ListCount > 0 Then CboVendedores.ListIndex = 0
    
    CboSucActivo.ListIndex = 0
    LLenarCombo Me.CboListaPrecios, "lst_nombre", "lst_id", "par_lista_precios", "lst_activo='SI' AND rut_emp='" & SP_Rut_Activo & "'"
    CboListaPrecios.AddItem "SIN LISTA DE PRECIOS"
    CboListaPrecios.ItemData(CboListaPrecios.ListCount - 1) = 0
    CboListaPrecios.ListIndex = CboListaPrecios.ListCount - 1
    
    If Len(SG_codigo) = 0 Then
        Bp_Nuevo = True
    Else
        Bp_Nuevo = False
        Sql = "SELECT rut_cliente,nombre_rsocial,cli_nombre_fantasia,direccion,ciudad,comuna,fono,email,giro," & _
                     "descuento,cli_contacto, comentario,cli_contacto2,cli_contacto3," & _
                     "cli_mail1,cli_mail2,cli_mail3,habilitado,lst_id,ven_id,cli_monto_credito,cli_usu_autoriza_credito,cli_bloqueado,cli_motivo_bloqueo,anticipo " & _
               "FROM maestro_clientes " & _
               "WHERE rut_cliente='" & SG_codigo & "'"
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
            With RsTmp
                TxtRut = !rut_cliente
                Me.TxtRz = "" & !nombre_rsocial
                Me.TxtFantasia = "" & !cli_nombre_fantasia
                TxtDirec = "" & !direccion
                txtComuna = "" & !comuna
                TxtCiudad = "" & !ciudad
                txtFono = "" & !fono
                TxtGiro = "" & !giro
                TxtMail = "" & !Email
                TxtDesuentoCliente = !Descuento
                TxtObs = "" & !comentario
                Me.txtContacto = "" & !cli_contacto
                Me.txtContacto2 = "" & !cli_contacto2
                Me.txtContacto3 = "" & !cli_contacto3
                txtMail1 = "" & !cli_mail1
                txtMail2 = "" & !cli_mail2
                txtMail3 = "" & !cli_mail3
                SkCredito = NumFormat(!cli_monto_credito)
                Me.SkAutoriza = !cli_usu_autoriza_credito
                CboActivo.ListIndex = IIf(!habilitado = "SI", 0, 1)
                Me.CboBloqueado.ListIndex = IIf(!cli_bloqueado = "SI", 0, 1)
                Me.TxtMotivoBloqueo = "" & !cli_motivo_bloqueo
                 Busca_Id_Combo CboPlazos, Val("" & RsTmp!anticipo)
                Sql = "SELECT lst_id " & _
                      "FROM par_asociacion_lista_precios " & _
                      "WHERE rut_emp='" & SP_Rut_Activo & "' AND cli_rut='" & TxtRut & "'"
                Consulta RsTmp, Sql
                Ip_ID_Lp = 0
                If RsTmp.RecordCount > 0 Then Ip_ID_Lp = RsTmp!lst_id
                Busca_Id_Combo CboListaPrecios, Ip_ID_Lp
               
                If CboVendedores.ListCount > 0 Then Busca_Id_Combo CboVendedores, Val(!ven_id)
                LlenaGiros
            End With
        End If
        
        
        
        
        TxtRut.Locked = True
        TxtRut.TabStop = False
        CargaSucursales
'        TxtRz.SetFocus
    End If
End Sub
Private Sub CargaSucursales()
        Sql = "SELECT suc_id,suc_ciudad,suc_direccion,suc_contacto,gir_id,suc_activo " & _
              "FROM par_sucursales s " & _
              "WHERE rut_cliente='" & TxtRut & "'"
        Consulta RsTmp, Sql
        LLenar_Grilla RsTmp, Me, LvDetalle, False, True, True, False

End Sub


Private Sub LvDetalle_DblClick()
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    
    LBIdSuc = LvDetalle.SelectedItem
    TxtSucCiudad = LvDetalle.SelectedItem.SubItems(1)
    TxtSucDireccion = LvDetalle.SelectedItem.SubItems(2)
    TxtSucContacto = LvDetalle.SelectedItem.SubItems(3)
    Busca_Id_Combo CboGiros, Val(LvDetalle.SelectedItem.SubItems(4))
    CboSucActivo.ListIndex = IIf(LvDetalle.SelectedItem.SubItems(5) = "SI", 0, 1)
    TxtSucCiudad.SetFocus
End Sub



Private Sub Timer1_Timer()
    If Len(Me.TxtRut.Text) > 0 And _
       Len(Me.TxtRz.Text) > 0 And _
       Len(Me.TxtDirec) > 0 And _
       Len(Me.TxtCiudad) > 0 And _
       Len(Me.txtFono) > 0 And _
       Len(Me.TxtGiro) > 0 And _
       Len(Me.txtComuna) > 0 Then
        
            
             If NoGuardar = False Then Me.CmdGuarda.Enabled = True
       
    Else
        Me.CmdGuarda.Enabled = False
    End If
    
    For Each CTextos In Controls
        If (TypeOf CTextos Is TextBox) Then
            If Me.ActiveControl.Name = CTextos.Name Then 'Foco activo
                CTextos.BackColor = IIf(CTextos.Locked, ClrDesha, ClrCfoco)
            Else
                CTextos.BackColor = IIf(CTextos.Locked, ClrDesha, ClrSfoco)
            End If
        End If
    Next
    
End Sub


Private Sub Timer2_Timer()
    If SG_codigo = Empty Then
        Me.TxtRut.SetFocus
    Else
        TxtRz.SetFocus
    End If
    
   ' Timer1.Enabled = True
    Me.Timer2.Enabled = False
    
End Sub

Private Sub TxtCiudad_GotFocus()
    En_Foco TxtCiudad
End Sub

Private Sub TxtCiudad_KeyPress(KeyAscii As Integer)
KeyAscii = Asc(UCase(Chr(KeyAscii)))
 If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtCiudad_Validate(Cancel As Boolean)
TxtCiudad = Replace(TxtCiudad, "'", "")
End Sub

Private Sub TxtComuna_GotFocus()
    En_Foco txtComuna
   
End Sub

Private Sub txtComuna_KeyPress(KeyAscii As Integer)
KeyAscii = Asc(UCase(Chr(KeyAscii)))
 If KeyAscii = 39 Then KeyAscii = 0
End Sub





Private Sub TxtComuna_Validate(Cancel As Boolean)
txtComuna = Replace(txtComuna, "'", "")
End Sub

Private Sub txtContacto_GotFocus()
    En_Foco txtContacto
    
End Sub

Private Sub txtContacto_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
     If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub txtContacto_Validate(Cancel As Boolean)
txtContacto = Replace(txtContacto, "'", "")
End Sub

Private Sub txtContacto2_GotFocus()
    En_Foco txtContacto2
End Sub

Private Sub txtContacto2_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
     If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub txtContacto2_Validate(Cancel As Boolean)
txtContacto2 = Replace(txtContacto2, "'", "")
End Sub

Private Sub txtContacto3_GotFocus()
    En_Foco txtContacto3
End Sub
Private Sub txtContacto3_KeyPress(KeyAscii As Integer)
KeyAscii = Asc(UCase(Chr(KeyAscii)))
 If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub txtContacto3_Validate(Cancel As Boolean)
txtContacto3 = Replace(txtContacto3, "'", "")
End Sub

Private Sub TxtDesuentoCliente_GotFocus()
    En_Foco TxtDesuentoCliente
    
End Sub
Private Sub TxtDesuentoCliente_KeyPress(KeyAscii As Integer)
    KeyAscii = AceptaSoloNumeros(KeyAscii)
     If KeyAscii = 39 Then KeyAscii = 0
End Sub
Private Sub TxtDirec_GotFocus()
    En_Foco TxtDirec
End Sub
Private Sub TxtDirec_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
     If KeyAscii = 39 Then KeyAscii = 0
End Sub
Private Sub txtFax_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub TxtDirec_Validate(Cancel As Boolean)
TxtDirec = Replace(TxtDirec, "'", "")
End Sub

Private Sub TxtFantasia_GotFocus()
    En_Foco TxtFantasia
End Sub

Private Sub TxtFantasia_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
     If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtFantasia_Validate(Cancel As Boolean)
TxtFantasia = Replace(TxtFantasia, "'", "")
End Sub

Private Sub TxtFono_GotFocus()
    En_Foco txtFono
End Sub

Private Sub TxtFono_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
     If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtGiro_GotFocus()
    En_Foco TxtGiro
End Sub

Private Sub TxtGiro_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
     If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtGiro_Validate(Cancel As Boolean)
TxtGiro = Replace(TxtGiro, "'", "")
End Sub

Private Sub TxtGiroNombre_GotFocus()
    En_Foco TxtGiroNombre
End Sub

Private Sub TxtGiroNombre_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
     If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtMail_GotFocus()
    En_Foco TxtMail
End Sub
Private Sub TxtMail_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(LCase(Chr(KeyAscii)))
     If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtMail_Validate(Cancel As Boolean)
TxtMail = Replace(TxtMail, "'", "")
End Sub



Private Sub txtMail1_Validate(Cancel As Boolean)
txtMail1 = Replace(txtMail1, "'", "")
End Sub

Private Sub txtMail2_Validate(Cancel As Boolean)
txtMail2 = Replace(txtMail2, "'", "")
End Sub

Private Sub txtMail3_Validate(Cancel As Boolean)
txtMail3 = Replace(txtMail3, "'", "")
End Sub

Private Sub TxtMotivoBloqueo_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub TxtObs_GotFocus()
    En_Foco TxtObs
    
End Sub

Private Sub TXToBS_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
     If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtObs_Validate(Cancel As Boolean)
TxtObs = Replace(TxtObs, "'", "")
End Sub

Private Sub TxtRecargo_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
End Sub

Private Sub TxtRut_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
     If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtRut_Validate(Cancel As Boolean)
    If Len(TxtRut.Text) = 0 Then Exit Sub
    If TxtRut.Locked = True Then Exit Sub
    Dim VeriRut As Recordset
    
   
    Respuesta = VerificaRut(Me.TxtRut.Text, NuevoRut)
    If Respuesta = True Then
        Me.TxtRut.Text = NuevoRut
        'Dim VeriRut As Recordset
        'paso = Consulta(VeriRut, "SELECT * FROM maestro_Clientes WHERE Rut_cliente = '" & NuevoRut & "'")
        Consulta VeriRut, "SELECT * FROM maestro_clientes WHERE rut_cliente = '" & NuevoRut & "'"
        
        
        If VeriRut.RecordCount > 0 Then
          With VeriRut
            Me.TxtRz = "" & !nombre_rsocial
            TxtDirec = "" & !direccion
            TxtCiudad = "" & !ciudad
            txtComuna = "" & !comuna
            txtFono = "" & !fono
            TxtGiro = "" & !giro
            TxtMail = "" & !Email
            Me.TxtDesuentoCliente = 0 & !Descuento
            Me.TxtObs = "" & !comentario
            Me.InhabilitaCajas (True)
            NoGuardar = True
          End With
           If Bp_Nuevo Then MsgBox "Cliente ya existe", vbInformation
           If MsgBox("Desea incorporar este CLIENTE a la empresa ACTIVA ?", vbQuestion + vbYesNo) = vbNo Then Exit Sub
           On Error GoTo Fallo
           cn.BeginTrans
                cn.Execute "DELETE FROM par_asociacion_ruts " & _
                           "WHERE rut_emp='" & SP_Rut_Activo & "' AND rut_cli='" & TxtRut & "'"
                 Sql = "INSERT INTO par_asociacion_ruts (rut_emp,rut_cli) " & _
                        "VALUES('" & SP_Rut_Activo & "','" & TxtRut & "')"
                 cn.Execute Sql
                 cn.Execute "UPDATE maestro_clientes " & _
                            "SET habilitado='SI' " & _
                            "WHERE rut_cliente='" & TxtRut & "'"
            cn.CommitTrans
        Else
            Me.InhabilitaCajas (False)
            NoGuardar = False
        End If
        
    Else
        TxtRut.Text = ""
        NoGuardar = False
    End If
    Exit Sub
Fallo:
    MsgBox "Ocurrio un problema al actualizar..." & vbNewLine & Err.Number & vbNewLine & Err.Description, vbInformation
    cn.RollbackTrans
End Sub
Public Sub InhabilitaCajas(SI As Boolean)
    For Each CTxt In Controls
            If (TypeOf CTxt Is TextBox) Then
                If SI = True Then
                    If CTxt.Name <> "TxtRut" Then CTxt.Locked = True
                Else
                    If CTxt.Name <> "TxtRut" Then CTxt.Text = ""
                    CTxt.Locked = False
                    
                End If
            End If
    Next
End Sub

Private Sub TxtRz_GotFocus()
    En_Foco TxtRz
End Sub

Private Sub TxtRz_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
     If KeyAscii = 39 Then KeyAscii = 0
End Sub
''''''''''MsgBox ("Estos son los colores")
Private Sub Form_Resize()
Dim i As Integer
Dim Y As Integer
    With Me
        .Cls
        .AutoRedraw = True
        .DrawStyle = 6
        .DrawMode = 13
        .DrawWidth = 2
        .ScaleMode = 3
        .ScaleHeight = 512
    
    
        Y = 0
        For i = 255 To 0 Step -1
        
            AgregoCliente.Line (0, Y)-(Principal.Width, Y + 2), RGB(0 + 30, i + 50, i + 100), BF
        
            Y = Y + 2
        Next i
    End With
    
End Sub



Private Sub TxtRz_Validate(Cancel As Boolean)
TxtRz = Replace(TxtRz, "'", "")
End Sub

Private Sub TxtSucCiudad_GotFocus()
    En_Foco Me.ActiveControl
End Sub

Private Sub TxtSucCiudad_KeyPress(KeyAscii As Integer)
 KeyAscii = Asc(UCase(Chr(KeyAscii)))
  If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtSucContacto_GotFocus()
    En_Foco Me.ActiveControl
End Sub

Private Sub TxtSucContacto_KeyPress(KeyAscii As Integer)
 KeyAscii = Asc(UCase(Chr(KeyAscii)))
  If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtSucDireccion_GotFocus()
    En_Foco Me.ActiveControl
End Sub

Private Sub TxtSucDireccion_KeyPress(KeyAscii As Integer)
 KeyAscii = Asc(UCase(Chr(KeyAscii)))
  If KeyAscii = 39 Then KeyAscii = 0
End Sub
