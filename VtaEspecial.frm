VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Begin VB.Form VtaEspecial 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Venta que no afecta inventario"
   ClientHeight    =   5025
   ClientLeft      =   2760
   ClientTop       =   3750
   ClientWidth     =   6225
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5025
   ScaleWidth      =   6225
   ShowInTaskbar   =   0   'False
   Begin VB.Frame frmEspecial 
      Caption         =   "Detalle"
      Height          =   4215
      Left            =   240
      TabIndex        =   14
      Top             =   240
      Width           =   5775
      Begin VB.Frame Frame1 
         Caption         =   "Comisi�n"
         Height          =   615
         Left            =   240
         TabIndex        =   17
         Top             =   3120
         Width           =   3015
         Begin VB.CheckBox CheckEspecialComision 
            Caption         =   "Sin comisi�n"
            Height          =   195
            Left            =   1080
            TabIndex        =   1
            Top             =   240
            Value           =   1  'Checked
            Width           =   1695
         End
      End
      Begin VB.TextBox txtespecial 
         Appearance      =   0  'Flat
         Height          =   280
         Index           =   0
         Left            =   240
         LinkTimeout     =   45
         MaxLength       =   45
         TabIndex        =   0
         Tag             =   "T"
         Top             =   480
         Width           =   5295
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   375
         Left            =   360
         OleObjectBlob   =   "VtaEspecial.frx":0000
         TabIndex        =   16
         Top             =   3720
         Width           =   5175
      End
      Begin VB.TextBox txtespecial 
         Appearance      =   0  'Flat
         Height          =   280
         Index           =   4
         Left            =   240
         MaxLength       =   45
         TabIndex        =   8
         Tag             =   "T"
         Top             =   1440
         Width           =   5295
      End
      Begin VB.TextBox txtespecial 
         Appearance      =   0  'Flat
         Height          =   280
         Index           =   3
         Left            =   240
         MaxLength       =   45
         TabIndex        =   7
         Tag             =   "T"
         Top             =   1200
         Width           =   5295
      End
      Begin VB.TextBox txtespecial 
         Appearance      =   0  'Flat
         Height          =   280
         Index           =   2
         Left            =   240
         MaxLength       =   45
         TabIndex        =   6
         Tag             =   "T"
         Top             =   975
         Width           =   5295
      End
      Begin VB.TextBox txtespecial 
         Appearance      =   0  'Flat
         Height          =   280
         Index           =   1
         Left            =   240
         LinkTimeout     =   45
         MaxLength       =   45
         TabIndex        =   5
         Tag             =   "T"
         Top             =   720
         Width           =   5295
      End
      Begin VB.TextBox txtespecial 
         Appearance      =   0  'Flat
         Height          =   280
         Index           =   6
         Left            =   240
         MaxLength       =   45
         TabIndex        =   10
         Tag             =   "T"
         Top             =   1920
         Width           =   5295
      End
      Begin VB.TextBox txtespecial 
         Appearance      =   0  'Flat
         Height          =   280
         Index           =   7
         Left            =   240
         MaxLength       =   45
         TabIndex        =   11
         Tag             =   "T"
         Top             =   2160
         Width           =   5295
      End
      Begin VB.TextBox txtespecial 
         Appearance      =   0  'Flat
         Height          =   280
         Index           =   8
         Left            =   240
         MaxLength       =   45
         TabIndex        =   12
         Tag             =   "T"
         Top             =   2400
         Width           =   5295
      End
      Begin VB.TextBox txtespecial 
         Appearance      =   0  'Flat
         Height          =   280
         Index           =   9
         Left            =   240
         MaxLength       =   45
         TabIndex        =   13
         Tag             =   "T"
         Top             =   2640
         Width           =   5295
      End
      Begin VB.TextBox txtespecial 
         Appearance      =   0  'Flat
         Height          =   280
         Index           =   5
         Left            =   240
         MaxLength       =   45
         MultiLine       =   -1  'True
         TabIndex        =   9
         Tag             =   "T"
         Top             =   1680
         Width           =   5295
      End
      Begin VB.TextBox txtNeto 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   405
         Left            =   3585
         TabIndex        =   2
         Tag             =   "N"
         Top             =   3210
         Width           =   1935
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   255
         Index           =   1
         Left            =   4200
         OleObjectBlob   =   "VtaEspecial.frx":00B4
         TabIndex        =   15
         Top             =   3000
         Width           =   1335
      End
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   6480
      OleObjectBlob   =   "VtaEspecial.frx":0126
      Top             =   600
   End
   Begin VB.CommandButton CancelButton 
      Caption         =   "Cancelar"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   4440
      TabIndex        =   4
      Top             =   4560
      Width           =   1575
   End
   Begin VB.CommandButton OKButton 
      Caption         =   "Aceptar"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2760
      TabIndex        =   3
      Top             =   4560
      Width           =   1575
   End
End
Attribute VB_Name = "VtaEspecial"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit

Private Sub CancelButton_Click()
    Unload Me
End Sub


Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
            SendKeys "{tab}"
            KeyAscii = 0
    Else
        If KeyAscii = 8 Or KeyAscii = 45 Then Exit Sub
        If KeyAscii = 46 Then KeyAscii = 44
        KeyAscii = Asc(UCase$(Chr(KeyAscii))) 'Convertimos a mayuscula cualquier caractar
        Dim txtS      As Control '                  ingresado
        Dim Decimales As Integer
        For Each txtS In Controls
            If (TypeOf txtS Is TextBox) Then
                If Me.ActiveControl.Name = txtS.Name Then

                    If Mid(txtS.Tag, 1, 1) = "N" Then 'Tag de textbox si es N = numerico
                        '                        If Not IsNumeric(KeyAscii) Then
                        If KeyAscii = 44 Then
                            If Len(txtS.Tag) > 1 Then
                                Decimales = Mid(txtS.Tag, 2, 1)
                                Replace txtS, ",", "."
                                If Decimales > 0 Then If InStrRev(txtS, ".") > 0 Or InStrRev(txtS, ",") > 0 Then KeyAscii = 0
                            End If
                        Else
                            KeyAscii = AceptaSoloNumeros(KeyAscii)
                        End If
                        'If KeyAscii = 46 Then If InStrRev(txtS, ".") > 0 Or InStrRev(txtS, ",") > 0 Then KeyAscii = 0
                    End If
                End If
            End If
        Next
    End If
End Sub


Private Sub Form_Load()
    Aplicar_skin Me
End Sub

Private Sub OKButton_Click()
    Dim i As Integer
    If Len(txtespecial(0)) = 0 Or Val(Me.TxtNeto) <= 0 Then
        MsgBox "Debe completar los datos", vbInformation + vbOKOnly
        Exit Sub
    End If
    VentaEspecial = True
    DM_Comision_Boton_Especial = IIf(CheckEspecialComision.Value = 1, False, True)
    With VtaDirecta
        For i = 0 To 9
            .txtespecial(i) = txtespecial(i)
            If Len(txtespecial(i)) = 0 Then .txtespecial(i) = " "
        Next
        .TxtNeto = TxtNeto
        .SkTotalMateriales = TxtNeto
     '   .TxtSubTotalMateriales = txtNeto
        
     '   .SkBrutoMateriales = Round(AgregarIVA(Val(txtNeto), 19), 0)
      '  .SkIvaMateriales = .SkBrutoMateriales - .SktotalMateriales
                
    End With

    Unload Me
End Sub
Private Sub txtespecial_KeyPress(Index As Integer, KeyAscii As Integer)
 '   KeyAscii = Asc(UCase(Chr(KeyAscii)))
    If Len(txtespecial(Index)) = 45 Or KeyAscii = 13 Then SendKeys "{tab}"
End Sub


Private Sub TxtNeto_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
End Sub
