VERSION 5.00
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "MSDATGRD.OCX"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Begin VB.Form MaestroProductos 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Maestro_PRODUCTOS"
   ClientHeight    =   8535
   ClientLeft      =   1095
   ClientTop       =   375
   ClientWidth     =   9960
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8535
   ScaleWidth      =   9960
   Begin VB.CommandButton CmdAgregar 
      Caption         =   "Agregar"
      Height          =   375
      Left            =   5520
      TabIndex        =   22
      Top             =   2880
      Width           =   1335
   End
   Begin VB.ComboBox CboMarca 
      DataField       =   "MARCA"
      Height          =   315
      ItemData        =   "MaestroProductos.frx":0000
      Left            =   2040
      List            =   "MaestroProductos.frx":000A
      Style           =   2  'Dropdown List
      TabIndex        =   21
      Top             =   360
      Width           =   3255
   End
   Begin MSDataGridLib.DataGrid DataGrid1 
      Bindings        =   "MaestroProductos.frx":0023
      Height          =   4215
      Left            =   240
      TabIndex        =   20
      Top             =   3480
      Width           =   9255
      _ExtentX        =   16325
      _ExtentY        =   7435
      _Version        =   393216
      HeadLines       =   1
      RowHeight       =   15
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ColumnCount     =   2
      BeginProperty Column00 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   13322
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   13322
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         BeginProperty Column00 
         EndProperty
         BeginProperty Column01 
         EndProperty
      EndProperty
   End
   Begin VB.PictureBox picButtons 
      Align           =   2  'Align Bottom
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   300
      Left            =   0
      ScaleHeight     =   300
      ScaleWidth      =   9960
      TabIndex        =   16
      Top             =   7905
      Width           =   9960
      Begin VB.CommandButton cmdClose 
         Caption         =   "&Cerrar"
         Height          =   300
         Left            =   4445
         TabIndex        =   19
         Top             =   0
         Width           =   1095
      End
      Begin VB.CommandButton cmdDelete 
         Caption         =   "&Eliminar"
         Height          =   300
         Left            =   3060
         TabIndex        =   18
         Top             =   0
         Width           =   1095
      End
      Begin VB.CommandButton cmdUpdate 
         Caption         =   "A&ctualizar"
         Height          =   300
         Left            =   1675
         TabIndex        =   17
         Top             =   0
         Width           =   1095
      End
   End
   Begin VB.TextBox txtFields 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      DataField       =   "Stock_Critico"
      Height          =   285
      Index           =   9
      Left            =   2040
      TabIndex        =   15
      Top             =   2940
      Width           =   3375
   End
   Begin VB.TextBox txtFields 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      DataField       =   "Stock_Actual"
      Height          =   285
      Index           =   8
      Left            =   2040
      TabIndex        =   13
      Top             =   2620
      Width           =   3375
   End
   Begin VB.TextBox txtFields 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      Height          =   285
      Index           =   6
      Left            =   2040
      TabIndex        =   10
      Top             =   1980
      Width           =   3375
   End
   Begin VB.TextBox txtFields 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      DataField       =   "Porciento_Utilidad"
      Height          =   285
      Index           =   5
      Left            =   2040
      TabIndex        =   8
      Top             =   1660
      Width           =   3375
   End
   Begin VB.TextBox txtFields 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      DataField       =   "Precio_Compra"
      Height          =   285
      Index           =   4
      Left            =   2040
      TabIndex        =   6
      ToolTipText     =   "El precio debe ser mayor a 0"
      Top             =   1340
      Width           =   3375
   End
   Begin VB.TextBox txtFields 
      DataField       =   "Descripcion"
      Height          =   285
      Index           =   3
      Left            =   2040
      TabIndex        =   4
      Top             =   1020
      Width           =   3375
   End
   Begin VB.TextBox txtFields 
      DataField       =   "Codigo"
      Height          =   285
      Index           =   2
      Left            =   2040
      TabIndex        =   2
      Top             =   720
      Width           =   3375
   End
   Begin MSAdodcLib.Adodc datPrimaryRS 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      Top             =   8205
      Width           =   9960
      _ExtentX        =   17568
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   "PROVIDER=MSDASQL;dsn=Citroen;uid=;pwd=;"
      OLEDBString     =   "PROVIDER=MSDASQL;dsn=Citroen;uid=;pwd=;"
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   $"MaestroProductos.frx":003E
      Caption         =   " "
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.Label LbMargen 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   2040
      TabIndex        =   23
      Top             =   2280
      Width           =   3375
   End
   Begin VB.Label lblLabels 
      Caption         =   "Stock_Critico:"
      Height          =   255
      Index           =   9
      Left            =   120
      TabIndex        =   14
      Top             =   2940
      Width           =   1815
   End
   Begin VB.Label lblLabels 
      Caption         =   "Stock_Actual:"
      Height          =   255
      Index           =   8
      Left            =   120
      TabIndex        =   12
      Top             =   2620
      Width           =   1815
   End
   Begin VB.Label lblLabels 
      Caption         =   "Margen:"
      Height          =   255
      Index           =   7
      Left            =   120
      TabIndex        =   11
      Top             =   2300
      Width           =   1815
   End
   Begin VB.Label lblLabels 
      Caption         =   "Precio_Venta:"
      Height          =   255
      Index           =   6
      Left            =   120
      TabIndex        =   9
      Top             =   1980
      Width           =   1815
   End
   Begin VB.Label lblLabels 
      Caption         =   "Porciento_Utilidad:"
      Height          =   255
      Index           =   5
      Left            =   120
      TabIndex        =   7
      Top             =   1660
      Width           =   1815
   End
   Begin VB.Label lblLabels 
      Caption         =   "Precio_Compra:"
      Height          =   255
      Index           =   4
      Left            =   120
      TabIndex        =   5
      Top             =   1340
      Width           =   1815
   End
   Begin VB.Label lblLabels 
      Caption         =   "Descripcion:"
      Height          =   255
      Index           =   3
      Left            =   120
      TabIndex        =   3
      Top             =   1020
      Width           =   1815
   End
   Begin VB.Label lblLabels 
      Caption         =   "Codigo:"
      Height          =   255
      Index           =   2
      Left            =   120
      TabIndex        =   1
      Top             =   700
      Width           =   1815
   End
   Begin VB.Label lblLabels 
      Caption         =   "MARCA:"
      Height          =   255
      Index           =   1
      Left            =   120
      TabIndex        =   0
      Top             =   380
      Width           =   1815
   End
End
Attribute VB_Name = "MaestroProductos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub CmdAgregar_Click()

    If CboMarca.ListIndex < 0 Then ' Verifica que este seleccionada una marca
        MsgBox "Debe seleccionar marca", vbExclamation + vbOKOnly
        CboMarca.SetFocus '
        Exit Sub
    End If
    If txtFields(2).Text = "" Then
        MsgBox "Debe ingresar un c�digo", vbExclamation + vbOKOnly
        txtFields(2).SetFocus '
        Exit Sub
    End If
    If txtFields(3).Text = "" Then
        MsgBox "Debe ingresar una descripci�n", vbExclamation + vbOKOnly
        txtFields(2).SetFocus '
        Exit Sub
    End If
  
    
    
    



    With Me.datPrimaryRS.Recordset
        .AddNew
        .Fields(0) = RutEmpresa
        .Fields(1) = CboMarca.Text
        .Fields(2) = txtFields(2).Text
        .Fields(3) = txtFields(3).Text
        .Fields(4) = txtFields(4).Text
        .Fields(5) = txtFields(5).Text
        .Fields(6) = txtFields(6).Text
        .Fields(7) = LbMargen.Caption
        .Fields(8) = txtFields(8).Text
        .Fields(9) = txtFields(9).Text
        .Update
    
    End With


End Sub

Private Sub Form_Unload(Cancel As Integer)
  Screen.MousePointer = vbDefault
End Sub

Private Sub datPrimaryRS_Error(ByVal ErrorNumber As Long, Description As String, ByVal Scode As Long, ByVal Source As String, ByVal HelpFile As String, ByVal HelpContext As Long, fCancelDisplay As Boolean)
  'Aqu� es donde puede colocar el c�digo de control de errores
  'Si desea pasar por alto los errores, marque como comentario la siguiente l�nea
  'Si desea detectarlos, agregue c�digo aqu� para controlarlos
  MsgBox "Data error event hit err:" & Description
End Sub

Private Sub datPrimaryRS_MoveComplete(ByVal adReason As ADODB.EventReasonEnum, ByVal pError As ADODB.Error, adStatus As ADODB.EventStatusEnum, ByVal pRecordset As ADODB.Recordset)
  'Esto mostrar� la posici�n de registro actual para este Recordset
  datPrimaryRS.Caption = "Record: " & CStr(datPrimaryRS.Recordset.AbsolutePosition)
End Sub

Private Sub datPrimaryRS_WillChangeRecord(ByVal adReason As ADODB.EventReasonEnum, ByVal cRecords As Long, adStatus As ADODB.EventStatusEnum, ByVal pRecordset As ADODB.Recordset)
  'Aqu� se coloca el c�digo de validaci�n
  'Se llama a este evento cuando ocurre la siguiente acci�n
  Dim bCancel As Boolean

  Select Case adReason
  Case adRsnAddNew
  Case adRsnClose
  Case adRsnDelete
  Case adRsnFirstChange
  Case adRsnMove
  Case adRsnRequery
  Case adRsnResynch
  Case adRsnUndoAddNew
  Case adRsnUndoDelete
  Case adRsnUndoUpdate
  Case adRsnUpdate
  End Select

  If bCancel Then adStatus = adStatusCancel
End Sub

Private Sub cmdAdd_Click()
  On Error GoTo AddErr
  datPrimaryRS.Recordset.AddNew

  Exit Sub
AddErr:
  MsgBox Err.Description
End Sub

Private Sub cmdDelete_Click()
  On Error GoTo DeleteErr
  With datPrimaryRS.Recordset
    .Delete
    .MoveNext
    If .EOF Then .MoveLast
  End With
  Exit Sub
DeleteErr:
  MsgBox Err.Description
End Sub

Private Sub cmdUpdate_Click()
  On Error GoTo UpdateErr

  datPrimaryRS.Recordset.UpdateBatch adAffectAll
  Exit Sub
UpdateErr:
  MsgBox Err.Description
End Sub

Private Sub cmdClose_Click()
  Unload Me
End Sub

Private Sub txtFields_Change(Index As Integer)
    
    
    Dim Porcentaje As Double
    
    
    
    
   
    If Index = 5 Then
        Porcentaje = Val(txtFields(4)) / 100 * Val(txtFields(5))
        txtFields(6) = Val(txtFields(4)) + Porcentaje
        LbMargen.Caption = Val(txtFields(6)) - Val(txtFields(4))
    End If
    
End Sub

Private Sub txtFields_KeyPress(Index As Integer, KeyAscii As Integer)
    Dim letra As String
    
    If Index = 4 Or Index = 5 Or Index = 6 Or Index = 8 Or Index = 9 Then
    
        If IsNumeric(Chr(KeyAscii)) Then
            'pasa
        Else
            If KeyAscii = 8 Then Exit Sub ' acepta retorceso
            
            If KeyAscii = 13 Then
                SendKeys "{TAB}" 'envia un tab
                Exit Sub '8=retroceso y 13 =enter
            End If
            
            KeyAscii = 0 'No acepta el caracter ingresado
            
        End If
    Else
        letra = UCase(Chr(KeyAscii)) 'a mayuscula el caractere ingresado
        KeyAscii = Asc(letra) 'recupero el codigo ascci del caractar ya transofrmado
    End If
End Sub

Private Sub txtFields_Validate(Index As Integer, Cancel As Boolean)
    If Index = 6 Then
    
        LbMargen.Caption = Val(txtFields(6)) - Val(txtFields(4))
        
        txtFields(5) = Val(LbMargen.Caption) * 100 / Val(txtFields(4).Text)
    
    End If
    If Index = 4 Then
        If Val(txtFields(4).Text) = 0 Then txtFields(4) = 1
        
    
    
    End If
    
    
    
    
    
    
End Sub
