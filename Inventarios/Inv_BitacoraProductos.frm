VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Begin VB.Form Inv_BitacoraProductos 
   Caption         =   "Bitacora"
   ClientHeight    =   4905
   ClientLeft      =   2220
   ClientTop       =   2220
   ClientWidth     =   11685
   LinkTopic       =   "Form1"
   ScaleHeight     =   4905
   ScaleWidth      =   11685
   Begin VB.Frame FrmSub 
      Caption         =   "Productos m�s vendidos al cliente"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3810
      Left            =   345
      TabIndex        =   1
      Top             =   240
      Width           =   10980
      Begin VB.CommandButton cmdSub 
         Caption         =   "Command6"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   570
         Index           =   0
         Left            =   810
         TabIndex        =   2
         Top             =   -180
         Visible         =   0   'False
         Width           =   9585
      End
   End
   Begin VB.Timer Timer1 
      Interval        =   10
      Left            =   480
      Top             =   30
   End
   Begin VB.CommandButton cmdSalir 
      Cancel          =   -1  'True
      Caption         =   "&Retornar"
      Height          =   465
      Left            =   10200
      TabIndex        =   0
      ToolTipText     =   "Salir sin hacer cambios"
      Top             =   4200
      Width           =   1050
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   0
      OleObjectBlob   =   "Inv_BitacoraProductos.frx":0000
      Top             =   0
   End
End
Attribute VB_Name = "Inv_BitacoraProductos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdSalir_Click()
    SG_codigo = Empty
    Unload Me
End Sub

Private Sub cmdSub_Click(Index As Integer)
    SG_codigo = cmdSub(Index).Tag
    Unload Me
End Sub

Private Sub Form_Load()
    SG_codigo = Empty
    If cmdSub.Count > 2 Then
        For i = cmdSub.Count - 1 To 1 Step -1
            Unload cmdSub(i)
        Next
    ElseIf cmdSub.Count = 2 Then
        Unload cmdSub(1)
    End If
   ' Sql = "SELECT cte_id men_id,CONCAT(ban_nombre,'  Nro ',cte_numero) men_nombre,cte_saldo_inicial men_tooltips " & _
            "FROM ban_cta_cte c " & _
            "INNER JOIN par_bancos b USING(ban_id) " & _
            "WHERE cte_activo='SI' AND rut_emp='" & SP_Rut_Activo & "'"
            
            
            
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        With RsTmp
            i = 1
            .MoveFirst
            Do While Not .EOF
                 Load cmdSub(i)
                 cmdSub(i).Caption = !Descripcion
                 cmdSub(i).Height = cmdSub(0).Height
                 cmdSub(i).Top = cmdSub(i - 1).Top + cmdSub(0).Height + 100
                 'cmdSub(i).ToolTipText = !men_tooltips
                 cmdSub(i).Visible = True
                 cmdSub(i).Tag = !Codigo
                 i = i + 1
                .MoveNext
            Loop
        End With
    End If
    
    
    
    Aplicar_skin Me
    Centrar Me
    
    
    
    
End Sub

Private Sub Timer1_Timer()
    On Error Resume Next
    cmdSub(1).SetFocus
    Timer1.Enabled = False
End Sub
