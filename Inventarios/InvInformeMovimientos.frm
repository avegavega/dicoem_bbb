VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{DA729E34-689F-49EA-A856-B57046630B73}#1.0#0"; "progressbar-xp.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form InvInformeMovimientos 
   Caption         =   "Movimientos de bodegas"
   ClientHeight    =   9540
   ClientLeft      =   2190
   ClientTop       =   585
   ClientWidth     =   14055
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   9540
   ScaleWidth      =   14055
   Begin VB.CommandButton CmdElimina 
      Caption         =   "F4 - Eliminar"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   4695
      TabIndex        =   13
      ToolTipText     =   "Visualizar compra"
      Top             =   8895
      Width           =   2175
   End
   Begin VB.Frame FraProgreso 
      Caption         =   "Progreso exportación"
      Height          =   795
      Left            =   1725
      TabIndex        =   11
      Top             =   3855
      Visible         =   0   'False
      Width           =   11430
      Begin Proyecto2.XP_ProgressBar BarraProgreso 
         Height          =   330
         Left            =   150
         TabIndex        =   12
         Top             =   315
         Width           =   11130
         _ExtentX        =   19632
         _ExtentY        =   582
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BrushStyle      =   0
         Color           =   16777088
         Scrolling       =   1
         ShowText        =   -1  'True
      End
   End
   Begin VB.Timer Timer1 
      Interval        =   10
      Left            =   495
      Top             =   30
   End
   Begin VB.Frame Frame1 
      Caption         =   "Filtro"
      Height          =   855
      Left            =   4185
      TabIndex        =   6
      Top             =   135
      Width           =   8250
      Begin VB.CommandButton CmdBusca 
         Caption         =   "Buscar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   6750
         TabIndex        =   10
         ToolTipText     =   "Busca texto ingresado"
         Top             =   345
         Width           =   1095
      End
      Begin VB.CheckBox ChkFecha 
         Height          =   225
         Left            =   1275
         TabIndex        =   7
         Top             =   390
         Width           =   225
      End
      Begin MSComCtl2.DTPicker DtDesde 
         CausesValidation=   0   'False
         Height          =   285
         Left            =   1560
         TabIndex        =   8
         Top             =   360
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   503
         _Version        =   393216
         Format          =   101515265
         CurrentDate     =   41001
      End
      Begin MSComCtl2.DTPicker DtHasta 
         Height          =   285
         Left            =   2760
         TabIndex        =   9
         Top             =   360
         Width           =   1200
         _ExtentX        =   2117
         _ExtentY        =   503
         _Version        =   393216
         Format          =   101515265
         CurrentDate     =   41001
      End
   End
   Begin VB.CommandButton CmdNueva 
      Caption         =   "F1 - Nueva"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   270
      TabIndex        =   5
      ToolTipText     =   "Nueva compra"
      Top             =   8895
      Width           =   2175
   End
   Begin VB.CommandButton CmdSeleccionar 
      Caption         =   "F2 - Ver"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   2490
      TabIndex        =   4
      ToolTipText     =   "Visualizar compra"
      Top             =   8895
      Width           =   2175
   End
   Begin VB.CommandButton CmdSalir 
      Cancel          =   -1  'True
      Caption         =   "Esc  -Retornar"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   9060
      TabIndex        =   3
      ToolTipText     =   "Salir"
      Top             =   8895
      Width           =   2055
   End
   Begin VB.CommandButton cmdExportar 
      Caption         =   "F3- Exportar Lista a Excel"
      Height          =   495
      Left            =   6945
      TabIndex        =   2
      ToolTipText     =   "Exportar"
      Top             =   8895
      Width           =   2055
   End
   Begin VB.Frame frmOts 
      Caption         =   "Listado historico"
      Height          =   7515
      Left            =   255
      TabIndex        =   0
      Top             =   1200
      Width           =   13500
      Begin MSComctlLib.ListView LvDetalle 
         Height          =   6990
         Left            =   195
         TabIndex        =   1
         Top             =   330
         Width           =   13170
         _ExtentX        =   23230
         _ExtentY        =   12330
         View            =   3
         LabelWrap       =   0   'False
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   11
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "N100"
            Text            =   "Nro"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "F1000"
            Text            =   "Fecha"
            Object.Width           =   1852
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "T1000"
            Text            =   "Bod. Origen"
            Object.Width           =   4762
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Object.Tag             =   "T2500"
            Text            =   "Bod Destino"
            Object.Width           =   4762
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Object.Tag             =   "T1000"
            Text            =   "Guia"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Object.Tag             =   "T3000"
            Text            =   "Obs."
            Object.Width           =   5292
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   6
            Object.Tag             =   "T1500"
            Text            =   "Usuario"
            Object.Width           =   4410
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   7
            Object.Tag             =   "N109"
            Text            =   "origen_id"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   8
            Object.Tag             =   "N109"
            Text            =   "destino_id"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   9
            Object.Tag             =   "N109"
            Text            =   "NROGUIA"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   10
            Object.Tag             =   "N109"
            Text            =   "DOC_ID"
            Object.Width           =   2540
         EndProperty
      End
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   0
      OleObjectBlob   =   "InvInformeMovimientos.frx":0000
      Top             =   0
   End
End
Attribute VB_Name = "InvInformeMovimientos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub CmdBusca_Click()
    CargaM
End Sub

Private Sub CmdElimina_Click()
    Dim Ip_Or As Integer, Ip_Ds As Integer, Rp_Mov As Recordset
     Dim Dp_Promedio As Double
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    If MsgBox("Esta seguro(a) de eliminar guia " & vbNewLine & LvDetalle.SelectedItem.SubItems(4), vbQuestion + vbOKCancel) = vbCancel Then Exit Sub
    
    Ip_Or = LvDetalle.SelectedItem.SubItems(7)
    Ip_Ds = LvDetalle.SelectedItem.SubItems(8)
    'Ahora eliminar guia, esto debe sacar los productos que se fueron al destino y volverlos al origen
    '18 Enero 2014
    
    Sql = "SELECT pro_codigo,mpd_cantidad " & _
            "FROM inv_movimientos_bodega_detalle " & _
            "WHERE mvp_id=" & LvDetalle.SelectedItem
            
    Consulta Rp_Mov, Sql
    If Rp_Mov.RecordCount > 0 Then
        Rp_Mov.MoveFirst
        
        On Error GoTo ErrroElimina
        cn.BeginTrans
        
     'For i = 1 To LvDetalle.ListItems.Count
        Do While Not Rp_Mov.EOF
        
            Sql = "SELECT IFNULL(ROUND(kar_nuevo_saldo_valor/kar_nuevo_saldo),pro_precio_neto) promedio " & _
                    "FROM inv_kardex " & _
                    "WHERE rut_emp='" & SP_Rut_Activo & "' AND pro_codigo='" & Rp_Mov!pro_codigo & "' AND bod_id=" & Ip_Ds & "  " & _
                    "ORDER BY kar_id DESC " & _
                    "LIMIT 1"
            Consulta RsTmp2, Sql
            Dp_Promedio = 0
            If RsTmp2.RecordCount > 0 Then
                Dp_Promedio = RsTmp2!promedio
            End If
        
        
            Kardex Fql(Date), "SALIDA", 0, 0, Ip_Ds, Rp_Mov!pro_codigo, Rp_Mov!mpd_cantidad, "ELIM." & LvDetalle.SelectedItem.SubItems(4) & ".MOV. SALIDA DE " & LvDetalle.SelectedItem.SubItems(3) & " A " & LvDetalle.SelectedItem.SubItems(2), Dp_Promedio, Dp_Promedio * Rp_Mov!mpd_cantidad, , , , , , , , , 0
        
            Rp_Mov.MoveNext
        Loop
        Rp_Mov.MoveFirst
        Do While Not Rp_Mov.EOF
           ' If LvDetalle.SelectedItem.SubItems(3) = "SI" Then
                Sql = "SELECT IFNULL(ROUND(kar_nuevo_saldo_valor/kar_nuevo_saldo),pro_precio_neto) promedio " & _
                        "FROM inv_kardex " & _
                        "WHERE rut_emp='" & SP_Rut_Activo & "' AND pro_codigo='" & Rp_Mov!pro_codigo & "' AND bod_id=" & Ip_Or & "  " & _
                        "ORDER BY kar_id DESC " & _
                        "LIMIT 1"
                Consulta RsTmp, Sql
                'Dp_Promedio = 0
                If RsTmp.RecordCount > 0 Then
                    Dp_Promedio = RsTmp!promedio
                End If
            
            
            
                'Aqui hacemos que el kardex le de una entrada a la bodega seleccionada
                Kardex Fql(Date), "ENTRADA", 0, 0, Ip_Or, Rp_Mov!pro_codigo, Rp_Mov!mpd_cantidad, "ELIM." & LvDetalle.SelectedItem.SubItems(4) & "MOV. ENTRADA A " & LvDetalle.SelectedItem.SubItems(2) & " DE " & LvDetalle.SelectedItem.SubItems(3), Dp_Promedio, Dp_Promedio * Rp_Mov!mpd_cantidad, , , , , , , , , 0
                            
           ' End If
            Rp_Mov.MoveNext
        Loop
        
        cn.Execute "DELETE FROM ven_doc_venta " & _
                    "WHERE no_documento=" & LvDetalle.SelectedItem.SubItems(9) & " AND doc_id=" & LvDetalle.SelectedItem.SubItems(10) & " AND rut_emp='" & SP_Rut_Activo & "'"
        cn.Execute "DELETE FROM ven_detalle " & _
                    "WHERE no_documento=" & LvDetalle.SelectedItem.SubItems(9) & " AND doc_id=" & LvDetalle.SelectedItem.SubItems(10) & " AND rut_emp='" & SP_Rut_Activo & "'"
                    
        cn.Execute "DELETE FROM inv_movimientos_bodegas " & _
                        "WHERE mvp_id=" & LvDetalle.SelectedItem
                        
        cn.Execute "DELETE FROM inv_movimientos_bodega_detalle " & _
                        "WHERE mvp_id=" & LvDetalle.SelectedItem
                    
        cn.CommitTrans
        MsgBox "Guia eliminada correctamente..."
    End If
    CargaM
    Exit Sub
ErrroElimina:
    cn.RollbackTrans
    MsgBox "Problema al eliminar guia..." & vbnelwine & Err.Number & " " & Err.Description
    
End Sub

Private Sub cmdExportar_Click()
    Dim tit(2) As String
    If LvDetalle.ListItems.Count = 0 Then Exit Sub
    FraProgreso.Visible = True
    tit(0) = Me.Caption & " " & DtFecha & " - " & Time
    tit(1) = ""
    tit(2) = ""
    ExportarNuevo LvDetalle, tit, Me, BarraProgreso
    FraProgreso.Visible = False
End Sub

Private Sub CmdNueva_Click()
    DG_ID_Unico = 0
    InvMovimientosArticulos.Show 1
    CargaM
End Sub

Private Sub CmdSalir_Click()
    Unload Me
End Sub
Private Sub CmdSeleccionar_Click()
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    DG_ID_Unico = LvDetalle.SelectedItem
    InvMovimientosArticulos.Show 1
End Sub

Private Sub Form_KeyUp(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF1 Then CmdNueva_Click
    If KeyCode = vbKeyF2 Then CmdSeleccionar_Click
    If KeyCode = vbKeyF3 Then cmdExportar_Click
End Sub

Private Sub Form_Load()
    Centrar Me, False
    Aplicar_skin Me
    DtDesde = Date
    DtHasta = Date
    CargaM
End Sub

'ordena columnas
Private Sub LvDetalle_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    ordListView ColumnHeader, Me, LvDetalle
End Sub
Private Sub CargaM()
    Dim Sp_Fechas As String
    Sp_Fechas = Empty
    If ChkFecha.Value = 1 Then
        Sp_Fechas = " AND b.mvp_fecha BETWEEN '" & Fql(DtDesde) & "' AND '" & Fql(DtHasta) & "' "
    End If
    
    Sql = "SELECT mvp_id nro,mvp_fecha,b1.bod_nombre orgien,b2.bod_nombre destino,CONCAT('GUIA:',CAST(no_documento AS CHAR)),  mvp_obs,mvp_usuario,mvp_id_origen,mvp_id_destino,no_documento,doc_id " & _
            "FROM inv_movimientos_bodegas b " & _
            "INNER JOIN par_bodegas b1 ON b.mvp_id_origen=b1.bod_id " & _
            "INNER JOIN par_bodegas b2 ON b.mvp_id_destino=b2.bod_id " & _
            "WHERE b.rut_emp='" & SP_Rut_Activo & "'" & Sp_Fechas & " " & _
            "ORDER BY mvp_id DESC"
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvDetalle, False, True, True, False
    For i = 1 To LvDetalle.ListItems.Count
        LvDetalle.ListItems(i).Bold = True
    Next
End Sub

Private Sub LvDetalle_DblClick()
    CmdSeleccionar_Click
End Sub

Private Sub Timer1_Timer()
    On Error Resume Next
    LvDetalle.SetFocus
    Timer1.Enabled = False
End Sub
