VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Object = "{DA729E34-689F-49EA-A856-B57046630B73}#1.0#0"; "progressbar-xp.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form busca_productos_OC 
   Caption         =   "Generar OC a Proveedores"
   ClientHeight    =   10950
   ClientLeft      =   3285
   ClientTop       =   450
   ClientWidth     =   17190
   LinkTopic       =   "Form1"
   ScaleHeight     =   10950
   ScaleWidth      =   17190
   Begin MSComDlg.CommonDialog Dialogo 
      Left            =   17025
      Top             =   5850
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.CommandButton CmdSalir 
      Cancel          =   -1  'True
      Caption         =   "&Retornar"
      Height          =   375
      Left            =   16245
      TabIndex        =   24
      Top             =   10500
      Width           =   1095
   End
   Begin VB.CommandButton CmdGenerar 
      Appearance      =   0  'Flat
      Caption         =   "Generar OC"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   465
      Left            =   45
      TabIndex        =   25
      Top             =   10365
      Width           =   2580
   End
   Begin VB.Frame Frame3 
      Caption         =   "Productos OC"
      Height          =   4530
      Left            =   240
      TabIndex        =   34
      Top             =   6375
      Width           =   16680
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   210
         Left            =   11760
         OleObjectBlob   =   "busca_productos_OC.frx":0000
         TabIndex        =   41
         Top             =   4170
         Width           =   1920
      End
      Begin VB.TextBox TxtTotalGomas 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   13740
         Locked          =   -1  'True
         TabIndex        =   40
         Text            =   "0"
         Top             =   4140
         Width           =   1620
      End
      Begin VB.CommandButton Command2 
         Caption         =   "Exportar a Excel"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   5160
         TabIndex        =   39
         ToolTipText     =   "Se Agregan los productos marcados"
         Top             =   4170
         Width           =   1590
      End
      Begin VB.CommandButton CmdEliminadeOC 
         Caption         =   "Eliminar de OC"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   2520
         TabIndex        =   38
         ToolTipText     =   "Se Agregan los productos marcados"
         Top             =   4155
         Width           =   1395
      End
      Begin MSComctlLib.ListView LvOC 
         Height          =   3750
         Left            =   195
         TabIndex        =   35
         Top             =   345
         Width           =   16215
         _ExtentX        =   28601
         _ExtentY        =   6615
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         Checkboxes      =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   15
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "T1000"
            Text            =   "Codigo"
            Object.Width           =   1940
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "T1500"
            Text            =   "Marca"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "T3000"
            Text            =   "Descripcion"
            Object.Width           =   10936
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   3
            Object.Tag             =   "N100"
            Text            =   "Precio Venta"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Object.Tag             =   "N109"
            Text            =   "Stock"
            Object.Width           =   1411
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Object.Tag             =   "T2000"
            Text            =   "Proveedor"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   6
            Object.Tag             =   "T1000"
            Text            =   "Codigo Proveedor"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   7
            Object.Tag             =   "2000"
            Text            =   "Codigo Empresa"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   8
            Object.Tag             =   "N109"
            Text            =   "Stk Minimo"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   9
            Object.Tag             =   "N109"
            Text            =   "Pedir"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   10
            Object.Tag             =   "N109"
            Text            =   "Id Enlace"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(12) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   11
            Object.Tag             =   "N109"
            Text            =   "Orden Enlace"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(13) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   12
            Object.Tag             =   "N109"
            Text            =   "Stock Enlace"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(14) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   13
            Object.Tag             =   "N100"
            Text            =   "Precio Unitario"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(15) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   14
            Key             =   "totaloc"
            Object.Tag             =   "N100"
            Text            =   "Total"
            Object.Width           =   2540
         EndProperty
      End
      Begin ACTIVESKINLibCtl.SkinLabel SKCantidadOC 
         Height          =   285
         Left            =   7080
         OleObjectBlob   =   "busca_productos_OC.frx":0072
         TabIndex        =   37
         Top             =   4170
         Width           =   3480
      End
   End
   Begin VB.Frame FrmEnlace 
      Caption         =   "Similares"
      Height          =   2925
      Left            =   7845
      TabIndex        =   28
      Top             =   2970
      Visible         =   0   'False
      Width           =   3285
      Begin VB.CommandButton CmdQuitar 
         Caption         =   "<-----"
         Height          =   255
         Left            =   195
         TabIndex        =   32
         ToolTipText     =   "Quitar"
         Top             =   2625
         Width           =   645
      End
      Begin VB.CommandButton CierraFrm 
         Caption         =   "x"
         Height          =   195
         Left            =   2940
         TabIndex        =   30
         Top             =   165
         Width           =   210
      End
      Begin VB.CommandButton CmdEnlace 
         Caption         =   "Enlace"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1500
         TabIndex        =   29
         Top             =   2595
         Width           =   1665
      End
      Begin MSComctlLib.ListView LvEnlace 
         Height          =   2190
         Left            =   120
         TabIndex        =   31
         Top             =   390
         Width           =   3090
         _ExtentX        =   5450
         _ExtentY        =   3863
         View            =   3
         LabelWrap       =   0   'False
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   3
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "N109"
            Text            =   "Orden"
            Object.Width           =   2646
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "N109"
            Text            =   "Codigo Empresa"
            Object.Width           =   3528
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "N109"
            Text            =   "Codigo Interno"
            Object.Width           =   2540
         EndProperty
      End
   End
   Begin VB.CheckBox Check1 
      Caption         =   "Check1"
      Height          =   195
      Left            =   555
      TabIndex        =   26
      Top             =   2040
      Width           =   240
   End
   Begin VB.Timer Timer1 
      Interval        =   30
      Left            =   300
      Top             =   5535
   End
   Begin VB.Frame FraProgreso 
      Caption         =   "Progreso exportaci�n"
      Height          =   795
      Left            =   1755
      TabIndex        =   0
      Top             =   4230
      Visible         =   0   'False
      Width           =   11430
      Begin Proyecto2.XP_ProgressBar BarraProgreso 
         Height          =   330
         Left            =   150
         TabIndex        =   1
         Top             =   315
         Width           =   11130
         _ExtentX        =   19632
         _ExtentY        =   582
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BrushStyle      =   0
         Color           =   16777088
         Scrolling       =   1
         ShowText        =   -1  'True
      End
   End
   Begin MSComctlLib.ListView LvDetalle 
      Height          =   3810
      Left            =   525
      TabIndex        =   2
      Top             =   1995
      Width           =   16215
      _ExtentX        =   28601
      _ExtentY        =   6720
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   0
      NumItems        =   13
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Object.Tag             =   "T1000"
         Text            =   "Codigo"
         Object.Width           =   1940
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Object.Tag             =   "T1500"
         Text            =   "Marca"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Object.Tag             =   "T3000"
         Text            =   "Descripcion"
         Object.Width           =   10936
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   3
         Object.Tag             =   "N100"
         Text            =   "Precio Venta"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Object.Tag             =   "N109"
         Text            =   "Stock"
         Object.Width           =   1411
      EndProperty
      BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   5
         Object.Tag             =   "T2000"
         Text            =   "Proveedor"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   6
         Object.Tag             =   "T1000"
         Text            =   "Codigo Proveedor"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   7
         Object.Tag             =   "2000"
         Text            =   "Codigo Empresa"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   8
         Object.Tag             =   "N109"
         Text            =   "Stk Minimo"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   9
         Object.Tag             =   "N109"
         Text            =   "Pedir"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   10
         Object.Tag             =   "N109"
         Text            =   "Id Enlace"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(12) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   11
         Object.Tag             =   "N109"
         Text            =   "Orden Enlace"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(13) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   12
         Object.Tag             =   "N109"
         Text            =   "Stock Enlace"
         Object.Width           =   2540
      EndProperty
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   195
      OleObjectBlob   =   "busca_productos_OC.frx":00F4
      Top             =   6495
   End
   Begin VB.Frame Frame2 
      Caption         =   "Buscando Producto"
      Height          =   6015
      Left            =   315
      TabIndex        =   3
      Top             =   285
      Width           =   16560
      Begin VB.CommandButton CmdADD_OC 
         Caption         =   "Agrega a OC"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   5295
         TabIndex        =   36
         ToolTipText     =   "Se Agregan los productos marcados"
         Top             =   5520
         Width           =   1395
      End
      Begin VB.CommandButton CmdAgregarSimilar 
         Caption         =   "----->"
         Height          =   300
         Left            =   2055
         TabIndex        =   33
         Top             =   1380
         Width           =   645
      End
      Begin VB.CommandButton CmdMarcarSimilares 
         Caption         =   "Similares"
         Height          =   330
         Left            =   180
         TabIndex        =   27
         Top             =   1365
         Width           =   1800
      End
      Begin VB.Frame Frame1 
         Caption         =   "Opciones de busqueda"
         Height          =   1500
         Left            =   6465
         TabIndex        =   13
         Top             =   180
         Width           =   9975
         Begin VB.OptionButton Option2 
            Caption         =   "Que contenga el texto dentro de la descripcion"
            Height          =   270
            Left            =   3720
            TabIndex        =   22
            Top             =   390
            Value           =   -1  'True
            Width           =   3630
         End
         Begin VB.OptionButton OptBusca 
            Caption         =   "Que la descripcion comience con el texto"
            Height          =   300
            Left            =   285
            TabIndex        =   21
            Top             =   375
            Width           =   3315
         End
         Begin VB.OptionButton Option1 
            Caption         =   "Filtro por codigo Barra/int (empresa)"
            Height          =   195
            Left            =   285
            TabIndex        =   20
            Top             =   795
            Width           =   3465
         End
         Begin VB.ComboBox CboLista 
            Height          =   315
            ItemData        =   "busca_productos_OC.frx":0328
            Left            =   7545
            List            =   "busca_productos_OC.frx":033E
            Style           =   2  'Dropdown List
            TabIndex        =   19
            Top             =   630
            Width           =   1590
         End
         Begin VB.OptionButton Option3 
            Caption         =   "Filtro por codigo Proveedor"
            Height          =   195
            Left            =   3765
            TabIndex        =   17
            Top             =   825
            Width           =   3240
         End
         Begin VB.TextBox txtRutProveedor 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   4665
            TabIndex        =   16
            Top             =   1095
            Width           =   1125
         End
         Begin VB.CommandButton cmdRutProveedor 
            Caption         =   "Proveedor"
            Enabled         =   0   'False
            Height          =   270
            Left            =   3735
            TabIndex        =   15
            Top             =   1125
            Width           =   900
         End
         Begin VB.TextBox txtNombreProveedor 
            BackColor       =   &H8000000F&
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   5820
            Locked          =   -1  'True
            TabIndex        =   14
            Top             =   1095
            Width           =   3930
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
            Height          =   210
            Left            =   7545
            OleObjectBlob   =   "busca_productos_OC.frx":0363
            TabIndex        =   18
            Top             =   420
            Width           =   1500
         End
      End
      Begin VB.CommandButton CmdTodos 
         Caption         =   "Todos"
         Height          =   390
         Left            =   3435
         TabIndex        =   12
         Top             =   750
         Width           =   1215
      End
      Begin VB.TextBox TxtBusqueda 
         Height          =   375
         Left            =   135
         TabIndex        =   11
         Top             =   735
         Width           =   3300
      End
      Begin VB.CommandButton CmdSeleccionar 
         Caption         =   "&Seleccionar"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   240
         TabIndex        =   10
         Top             =   5520
         Width           =   1305
      End
      Begin VB.CommandButton CmdCrear 
         Caption         =   "Crear Producto"
         Height          =   375
         Left            =   1560
         TabIndex        =   9
         Top             =   5520
         Width           =   1335
      End
      Begin VB.CommandButton CmdEditar 
         Caption         =   "Editar/Modificar"
         Height          =   375
         Left            =   2895
         TabIndex        =   8
         Top             =   5520
         Width           =   1260
      End
      Begin VB.CommandButton Command1 
         Caption         =   "Exportar Lista "
         Height          =   375
         Left            =   4155
         TabIndex        =   7
         Top             =   5520
         Width           =   1110
      End
      Begin VB.ComboBox CboCodigosProveedor 
         Height          =   315
         Left            =   11595
         Style           =   2  'Dropdown List
         TabIndex        =   6
         Top             =   5535
         Width           =   4890
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkResultados 
         Height          =   285
         Left            =   6765
         OleObjectBlob   =   "busca_productos_OC.frx":03DF
         TabIndex        =   4
         Top             =   5625
         Width           =   2055
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel5 
         Height          =   240
         Left            =   10125
         OleObjectBlob   =   "busca_productos_OC.frx":0461
         TabIndex        =   5
         Top             =   5610
         Width           =   1410
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   375
         Left            =   120
         OleObjectBlob   =   "busca_productos_OC.frx":04E1
         TabIndex        =   23
         Top             =   345
         Width           =   2655
      End
   End
End
Attribute VB_Name = "busca_productos_OC"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim RsProductos As Recordset
Dim Ip_Paginas As Integer

Dim P_Fecha As String * 12
Dim P_Documento As String * 40
Dim P_Numero As String * 11
Dim P_Vence As String * 10
Dim P_Venta As String * 12
Dim P_Pago As String * 12
Dim P_Saldo As String * 12
Dim Cy As Double, Cx As Double, Dp As Double, S_Item As String * 4
Dim Im_Paginas As Integer





Private Sub CargaTodosConCodigosProveedor()
        Sp_Like = ""

        If OptBusca.Value Then
            Sp_Like = " AND descripcion LIKE '" & TxtBusqueda & "%' "
        End If
        If Option2.Value Then
            Sp_Like = " AND  descripcion LIKE '%" & TxtBusqueda & "%' "
        End If
        If Option1.Value Then
            Sp_Like = " AND pro_codigo_interno LIKE '" & TxtBusqueda & "%'"
        End If
        
        If Option3.Value Then
            '08-01-2016 _
            filtro por codigo proveedor
            Sp_Like = " AND (SELECT cpv_codigo_proveedor " & _
                        "FROM par_codigos_proveedor o " & _
                        "WHERE  o.pro_codigo = p.codigo AND o.rut_emp = '" & SP_Rut_Activo & "' " & _
                        "LIMIT 1) LIKE '" & TxtBusqueda & "%' "
        End If

      Sql = "SELECT codigo,mar_nombre,descripcion,precio_venta, " & _
                        "(SELECT sto_stock stock " & _
                        "FROM  pro_stock k " & _
                        "WHERE rut_emp='" & SP_Rut_Activo & "' AND k.pro_codigo=p.codigo AND bod_id= 1 " & _
                        "GROUP BY pro_codigo) stk, " & _
                    "(SELECT nombre_empresa " & _
                        "FROM par_codigos_proveedor o " & _
                        "JOIN maestro_proveedores s ON o.rut_proveedor=s.rut_proveedor " & _
                    "WHERE   o.pro_codigo = p.codigo AND o.rut_emp = '" & SP_Rut_Activo & "' " & _
                    "LIMIT 1) nombreproveedor, " & _
                    "(SELECT cpv_codigo_proveedor " & _
                        "FROM par_codigos_proveedor o " & _
                        "WHERE  o.pro_codigo = p.codigo AND o.rut_emp = '" & SP_Rut_Activo & "' " & _
                        "LIMIT 1) codproveedor,pro_codigo_interno,stock_critico,0,(SELECT sim_id FROM par_productos_similares_detalle WHERE pro_codigo=p.codigo)  " & _
              "FROM maestro_productos p " & _
              "JOIN par_marcas m ON p.mar_id=m.mar_id  " & _
              "WHERE pro_activo='SI' AND p.rut_emp='" & SP_Rut_Activo & "'" & Sp_Like & Sp_Like2 & _
              " ORDER BY descripcion "

        CargaLista

End Sub





Private Sub Check1_Click()
    If Check1.Value = 0 Then
        For i = 1 To LvDetalle.ListItems.Count
            LvDetalle.ListItems(i).Checked = True
        Next
    Else
        For i = 1 To LvDetalle.ListItems.Count
            LvDetalle.ListItems(i).Checked = False
        Next
    End If
End Sub

Private Sub CierraFrm_Click()
    FrmEnlace.Visible = False
End Sub

Private Sub CmdADD_OC_Click()
    For G = 1 To LvDetalle.ListItems.Count
        If LvDetalle.ListItems(G).Checked Then
             LvOC.ListItems.Add , , LvDetalle.ListItems(G)
             LvOC.ListItems(LvOC.ListItems.Count).SubItems(1) = LvDetalle.ListItems(G).SubItems(1)
             LvOC.ListItems(LvOC.ListItems.Count).SubItems(2) = LvDetalle.ListItems(G).SubItems(2)
             LvOC.ListItems(LvOC.ListItems.Count).SubItems(3) = LvDetalle.ListItems(G).SubItems(3)
             LvOC.ListItems(LvOC.ListItems.Count).SubItems(4) = LvDetalle.ListItems(G).SubItems(4)
             LvOC.ListItems(LvOC.ListItems.Count).SubItems(5) = LvDetalle.ListItems(G).SubItems(5)
             LvOC.ListItems(LvOC.ListItems.Count).SubItems(6) = LvDetalle.ListItems(G).SubItems(6)
             LvOC.ListItems(LvOC.ListItems.Count).SubItems(7) = LvDetalle.ListItems(G).SubItems(7)
         
             LvOC.ListItems(LvOC.ListItems.Count).SubItems(8) = LvDetalle.ListItems(G).SubItems(8)
             LvOC.ListItems(LvOC.ListItems.Count).SubItems(9) = LvDetalle.ListItems(G).SubItems(9)
             LvOC.ListItems(LvOC.ListItems.Count).SubItems(10) = LvDetalle.ListItems(G).SubItems(10)
             LvOC.ListItems(LvOC.ListItems.Count).SubItems(11) = LvDetalle.ListItems(G).SubItems(11)
             LvOC.ListItems(LvOC.ListItems.Count).SubItems(12) = LvDetalle.ListItems(G).SubItems(12)
        
        End If
    Next
    Me.SKCantidadOC = "Articulos seleccionados:" & LvOC.ListItems.Count
    PrecioOC
End Sub

Private Sub CmdAgregarSimilar_Click()
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    For i = 1 To LvEnlace.ListItems.Count
        If Val(LvDetalle.SelectedItem) = LvEnlace.ListItems(i).SubItems(2) Then
            MsgBox "Ya est� en la lista de similares...", vbExclamation
            Exit Sub
        End If
    Next
    LvEnlace.ListItems.Add , , LvEnlace.ListItems.Count + 1
    LvEnlace.ListItems(LvEnlace.ListItems.Count).SubItems(1) = LvDetalle.SelectedItem.SubItems(7)
    LvEnlace.ListItems(LvEnlace.ListItems.Count).SubItems(2) = LvDetalle.SelectedItem
    
    
End Sub

Private Sub CmdCrear_Click()
    SG_codigo = Empty
    AgregarProducto.Bm_Nuevo = True
    If SG_Es_la_Flor = "SI" Then
                AgregarProductoFlor.Show 1
    Else
        AgregarProducto.Show 1
    End If
    Sql = "SELECT codigo,marca,descripcion " & _
          "FROM maestro_productos " & _
          "WHERE  rut_emp='" & SP_Rut_Activo & "' " & _
          "ORDER BY descripcion"
    CargaLista
End Sub

Private Sub CmdEditar_Click()
            If LvDetalle.SelectedItem Is Nothing Then Exit Sub
            SG_codigo = LvDetalle.SelectedItem.Text
            AgregarProducto.Bm_Nuevo = False
            If SG_Es_la_Flor = "SI" Then
                AgregarProductoFlor.Show 1
            Else
                AgregarProducto.Show 1
            End If
         '   CmdSeleccionar_Click
End Sub

Private Sub CmdFiltra_Click()
    Sm_filtro = " AND pro_codigo_interno LIKE '%" & TxtBusca & "%'"
End Sub





Private Sub CmdEliminadeOC_Click()
    If LvOC.SelectedItem Is Nothing Then Exit Sub
    LvOC.ListItems.Remove LvOC.SelectedItem.Index
    Me.SKCantidadOC = "Articulos seleccionados:" & LvOC.ListItems.Count
    
End Sub

Private Sub CmdEnlace_Click()
    Dim Sp_Codigos As String
    Dim Lp_IdE As Long
    Sp_Codigos = ""
    

    If LvEnlace.ListItems.Count > 0 Then
        For i = 1 To LvEnlace.ListItems.Count
            Sp_Codigos = Sp_Codigos & LvEnlace.ListItems(i).SubItems(2) & ","
        Next
        Sp_Codigos = Mid(Sp_Codigos, 1, Len(Sp_Codigos) - 1)
        
        Sql = "DELETE FROM par_productos_similares_detalle " & _
                "WHERE pro_codigo IN(" & Sp_Codigos & ")"
                
        cn.Execute Sql
        
    
    End If
    
    
    Lp_IdE = UltimoNro("par_productos_similares", "sim_id")
    Sql = "INSERT INTO par_productos_similares (sim_id,rut_emp) " & _
            "VALUES(" & Lp_IdE & ",'" & SP_Rut_Activo & "')"
    
    cn.Execute Sql
    
    'Grabar enlace
    Sql = "INSERT INTO par_productos_similares_detalle (pro_codigo,sid_orden,sim_id) VALUES"
    For i = 1 To LvEnlace.ListItems.Count
        Sql = Sql & "(" & LvEnlace.ListItems(i).SubItems(2) & "," & i & "," & Lp_IdE & "),"
    
    
    Next
    Sql = Mid(Sql, 1, Len(Sql) - 1)
    cn.Execute Sql
    
    Me.FrmEnlace.Visible = False
End Sub

Private Sub CmdGenerar_Click()

    If Len(Me.txtRutProveedor) = 0 Then
        MsgBox "Seleccione cliente...", vbInformation
        Exit Sub
    End If

    Dialogo.CancelError = True
    On Error GoTo CancelaImpesion
    Dialogo.ShowPrinter
    

    
    Printer.Orientation = vbPRORPortrait
    Printer.ScaleMode = vbCentimeters
    
    ImprimeOC
    
    Exit Sub
CancelaImpesion:
    'NO QUISO IMPRIMIR

End Sub
Private Sub GuardaOC()

''9 Nov,
'' Graba datos de oc
'' AVV
'Se retoma 18 Enero
'
'Sql = "INSERT INTO com_doc_compra (id,doc_id,no_documento,rut,nombre_proveedor,neto,iva,total,iva_retenido,total_impuestos,fecha,tipo_movimiento,mes_contable,ano_contable,doc_fecha_vencimiento,rut_emp,com_bruto_neto,bod_id,com_exe_otros_imp," & _
                      "pago,com_folio,com_folio_texto,com_plazo,com_solicitado_por,com_autorizado_por,com_nc_utilizada,com_id_ref_de_nota_debito,caj_id,com_pla_id,com_retirado_por,com_tasa_iva,com_iva_sin_credito) " & _
          "VALUES(" & Lp_Id & "," & CboTipoDoc.ItemData(CboTipoDoc.ListIndex) & "," & TxtNDoc & ",'" & txtRutProveedor & "','" & TxtRsocial & "'," & CDbl(TxtNeto) & "," & CDbl(TxtIva) & "," & CDbl(txtTotal) & "," & _
           "0,0,'" & Fql(DtFecha.Value) & "','INGRESO COMPRA'," & IG_Mes_Contable & "," & IG_Ano_Contable & ",'" & Fql(DtFecha + 30) & _
           "','" & SP_Rut_Activo & "','" & IIf(Option1.Value, "NETO", "BRUTO") & "'," & CboBodega.ItemData(CboBodega.ListIndex) & "," & CDbl(TxtExento) & ",'" & CboFpago.Text & "'," & Sp_FolioNro & ",'" & Sp_Folio & "'," & CboPlazos.ItemData(CboPlazos.ListIndex) & ",'" & _
           TxtSolicitadoPor & "','" & TxtAutorizadoPor & "','" & Sp_NC_Utilizada & "'," & Sp_NroIdRef & "," & lacajita & "," & Val(Right(CboPlazos.Text, 5)) & ",'" & Me.TxtRetiradoPor & "','" & SkTasaIVA & "'," & CDbl(Me.txtIvaSinCredito) & ")"
  '  cn.Execute Sql
    'Items detalle
'    With compra_detalle.LvDetalle
'        Dim Lp_Id_Detalle As Long
'
'
'        For i = 1 To compra_detalle.LvDetalle.ListItems.Count
'             Sql = "INSERT INTO com_doc_compra_detalle (cmd_id,id,pro_codigo,cmd_cantidad,cmd_unitario_neto,cmd_unitario_bruto, " & _
'                    "cmd_total_neto,cmd_total_bruto,pla_id,cen_id,gas_id,are_id,cmd_detalle,cmd_costo_real_neto,cmd_exento,cmd_nro_linea) VALUES "
'
'            Lp_Id_Detalle = UltimoNro("com_doc_compra_detalle", "cmd_id")
'
'            Ip_CenId = .ListItems(i).SubItems(9)
'            Ip_PlaId = .ListItems(i).SubItems(8)
'            Ip_GasId = .ListItems(i).SubItems(10)
'            Ip_AreId = .ListItems(i).SubItems(11)
'            If Option1.Value Then
'                Lp_Neto = CxP(CDbl(.ListItems(i).SubItems(17)) / CDbl(.ListItems(i).SubItems(5)))
'                Lp_NetoTotal = CxP(CDbl(.ListItems(i).SubItems(17)))
'                Lp_Bruto = "0"
'                Lp_BrutoTotal = "0"
'            Else
'                'Aunque ingresamos valores Brutos y descuentos, igual guardaremos el valor neto o costo real
'                '21 Diciembre 2011
'                Lp_Neto = CxP(CDbl(.ListItems(i).SubItems(17)) / CDbl(.ListItems(i).SubItems(5)))
'                Lp_NetoTotal = CxP(CDbl(.ListItems(i).SubItems(17)))
'                'Lp_Neto = CxP(.ListItems(i).SubItems(19))
'                'L'p_NetoTotal = CDbl(.ListItems(i).SubItems(19)) * CDbl(.ListItems(i).SubItems(5))
'                'Lp_Neto = "0"
'                'Lp_NetoTotal = "0"
'                Lp_Bruto = CxP(CDbl(.ListItems(i).SubItems(4)))
'                Lp_BrutoTotal = CxP(CDbl(.ListItems(i).SubItems(7)))
'            End If
'            Lp_Exento = CxP(CDbl(.ListItems(i).SubItems(21)))
'            Sql = Sql & "(" & Lp_Id_Detalle & "," & Lp_Id & ",'" & .ListItems(i).SubItems(1) & "'," & CxP(CDbl(.ListItems(i).SubItems(5))) & "," & Lp_Neto & "," & _
'            Lp_Bruto & "," & Lp_NetoTotal & "," & Lp_BrutoTotal & "," & Ip_PlaId & "," & Ip_CenId & "," & Ip_GasId & "," & Ip_AreId & ",'" & _
'            .ListItems(i).SubItems(2) & "'," & CxP(CDbl(.ListItems(i).SubItems(20))) & "," & CxP(CDbl(.ListItems(i).SubItems(21))) & "," & i & "),"
'
'            '25 Octubre 2014
'            'Grabaremos la penderacion de los impuestos linea a linea, (harina, carnes, etc)
'            If compra_detalle.LvImpuestosInd.ListItems.Count > 0 Then
'                Sql4 = ""
'                For p = 1 To compra_detalle.LvImpuestosInd.ListItems.Count
'                    If compra_detalle.LvImpuestosInd.ListItems(p) = i Then
'                        Sql4 = Sql4 & "(" & Lp_Id_Detalle & "," & compra_detalle.LvImpuestosInd.ListItems(p).SubItems(1) & "," & _
'                        IG_id_Empresa & "," & compra_detalle.LvImpuestosInd.ListItems(p).SubItems(2) & "),"
'                    End If
'                Next
'                If Len(Sql4) > 0 Then
'                    Sql4 = "INSERT INTO com_doc_compra_detalle_impuestos (cmd_id,imp_id,emp_id,cdi_valor) VALUES " & Mid(Sql4, 1, Len(Sql4) - 1)
'                    cn.Execute Sql4
'                End If
'
'            End If
'
'
'
'
'
'
'
'
'        Next
End Sub




Private Sub ImprimeOC()
    Dim Sp_Texto As String
    Dim Sp_Lh As String
    Dim Lp_Contador As Long
    Dim Ip_LineasPorPaginas As Integer
    
    On Error GoTo ErrorP
    Ip_LineasPorPaginas = 55
    Cx = 2
    Cy = 1
    For i = 1 To 147
        Sp_Lh = Sp_Lh & "="
    Next
    
 
        
        Lp_Contador = 1
        Im_Paginas = 1
        Ip_Paginas = Int(LvDetalle.ListItems.Count / Ip_LineasPorPaginas)
        If (LvOC.ListItems.Count Mod Ip_LineasPorPaginas) > 0 Then
            Ip_Paginas = Ip_Paginas + 1
        End If
        ResumenEncabezadoLaser
      '  GoTo fin
        Cy = Cy + Dp
        
        For i = 1 To Me.LvOC.ListItems.Count
            S_Item = i
            P_Fecha = LvOC.ListItems(i).SubItems(6)
            P_Documento = LvOC.ListItems(i).SubItems(2)
            P_Numero = LvOC.ListItems(i).SubItems(9)
            RSet P_Vence = LvOC.ListItems(i).SubItems(13)
            RSet P_Venta = LvOC.ListItems(i).SubItems(14)
            'RSet P_Pago = LvDetalle.ListItems(i).SubItems(9)
            'RSet P_Saldo = LvDetalle.ListItems(i).SubItems(10)
            EnviaLineasC False
            Cy = Cy + Dp
            Lp_Contador = Lp_Contador + 1
            If Lp_Contador = Ip_LineasPorPaginas Then
                Coloca Cx, Cy, "                                 continua en la siguiente pagina  --->", False, 8
                Cy = Cy + Dp
                Coloca Cx, Cy, Mid(Sp_Lh, 1, 80), False, 8
                Printer.NewPage
                Im_Paginas = Im_Paginas + 1
                ResumenEncabezadoLaser
                Cy = Cy + Dp
                Lp_Contador = 1
            End If
        Next
       ' PieDeCartola
        
fin:
       ' Coloca Cx, Cy, "                      FIN " & Sp_Lh, False, 8
        Cy = Cy + Dp
        Coloca Cx, Cy, Mid(Sp_Lh, 80), False, 8
        
        Cy = Cy + Dp
        Coloca Cx, Cy, Space(70) & " TOTAL OC$: " & TxtTotalGomas, True, 10
        
        
         Cy = Cy + Dp
        Coloca Cx, Cy, Mid(Sp_Lh, 80), False, 8
        Printer.NewPage
        Printer.EndDoc
        'FIN DEL DOCUMETNO"
    Exit Sub
ErrorP:
    MsgBox "Problema al imprimir..." & vbNewLine & Err.Number & vbNewLine & Err.Description & vbNewLine & Err.Source
  

End Sub



Private Sub CmdMarcarSimilares_Click()
    LvEnlace.ListItems.Clear
    c = 0
    For i = 1 To LvDetalle.ListItems.Count
        If LvDetalle.ListItems(i).Checked Then
            c = c + 1
            LvEnlace.ListItems.Add , , c
            LvEnlace.ListItems(LvEnlace.ListItems.Count).SubItems(1) = LvDetalle.ListItems(i).SubItems(7)
            LvEnlace.ListItems(LvEnlace.ListItems.Count).SubItems(2) = LvDetalle.ListItems(i)
            
        End If
    
    Next
    If LvEnlace.ListItems.Count > 0 Then Me.FrmEnlace.Visible = True


End Sub

Private Sub CmdQuitar_Click()
    If LvEnlace.SelectedItem Is Nothing Then Exit Sub
    LvEnlace.ListItems.Remove LvEnlace.SelectedItem.Index
    
End Sub

Private Sub cmdRutProveedor_Click()
    AccionProveedor = 0
    BuscaProveedor.Show 1
    txtRutProveedor = RutBuscado

    txtRutProveedor_Validate True
    txtRutProveedor.SetFocus
End Sub

Private Sub cmdSalir_Click()
    SG_codigo = Empty
    Unload Me
End Sub

Private Sub CmdSeleccionar_Click()
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    SG_codigo = LvDetalle.SelectedItem.Text
        If Len(LvDetalle.SelectedItem.SubItems(7)) > 0 Then
        SG_codigo2 = LvDetalle.SelectedItem.SubItems(7)
    Else
        SG_codigo2 = SG_codigo
    End If
    Unload Me
End Sub

Private Sub CmdSube_Click()
    If LvEnlace.SelectedItem Is Nothing Then Exit Sub
    
    
End Sub

Private Sub CmdTodos_Click()
    Sql = "SELECT codigo,mar_nombre,descripcion,precio_venta " & _
          "FROM maestro_productos p " & _
             "JOIN par_marcas m ON p.mar_id=m.mar_id  " & _
          "WHERE pro_activo='SI' AND p.rut_emp='" & SP_Rut_Activo & "' " & _
          "ORDER BY descripcion"
    CargaLista
End Sub

Private Sub Command1_Click()
    Dim tit(2) As String
    If LvDetalle.ListItems.Count = 0 Then Exit Sub
    FraProgreso.Visible = True
    tit(0) = Me.Caption & " " & DtFecha & " - " & Time
    tit(1) = ""
    tit(2) = ""
    ExportarNuevo LvDetalle, tit, Me, BarraProgreso
    FraProgreso.Visible = False
End Sub





Private Sub Command2_Click()
    Dim tit(2) As String
    If LvOC.ListItems.Count = 0 Then Exit Sub
    FraProgreso.Visible = True
    tit(0) = Me.Caption & " " & DtFecha & " - " & Time
    tit(1) = ""
    tit(2) = ""
    
    

    
    
    ExportarNuevo LvOC, tit, Me, BarraProgreso
    FraProgreso.Visible = False
End Sub

Private Sub PrecioOC()
    Dim Sp_Codigos As String
    
    Sp_Codigos = ""
    If LvOC.ListItems.Count = 0 Then Exit Sub
    
    For i = 1 To LvOC.ListItems.Count
        
        
        Sp_Codigos = Sp_Codigos & LvOC.ListItems(i) & ","
    
    
    
    Next
    
    Sql = "SELECT pro_codigo,AVG(pro_ultimo_precio_compra) precio " & _
                "FROM pro_stock s " & _
                "WHERE s.rut_emp='" & SP_Rut_Activo & "' AND s.pro_codigo IN(" & Mid(Sp_Codigos, 1, Len(Sp_Codigos) - 1) & ") AND bod_id=1 " & _
                "GROUP BY pro_codigo "
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        RsTmp.MoveFirst
        Do While Not RsTmp.EOF
            For i = 1 To LvOC.ListItems.Count
                If RsTmp!pro_codigo = LvOC.ListItems(i) Then
                    LvOC.ListItems(i).SubItems(13) = 0
                    LvOC.ListItems(i).SubItems(13) = "" & RsTmp!Precio
                    LvOC.ListItems(i).SubItems(14) = CDbl(LvOC.ListItems(i).SubItems(9)) * RsTmp!Precio
                    LvOC.ListItems(i).SubItems(13) = NumFormat(LvOC.ListItems(i).SubItems(13))
                    LvOC.ListItems(i).SubItems(14) = NumFormat(LvOC.ListItems(i).SubItems(14))
                    Exit For
                End If
            
            
            Next
            RsTmp.MoveNext
        Loop

    End If
    TxtTotalGomas = NumFormat(TotalizaColumna(LvOC, "totaloc"))

End Sub

Private Sub Command3_Click()
    PrecioOC
End Sub

Private Sub Form_Load()
    Centrar Me
    Aplicar_skin Me, App.Path
    CboLista.ListIndex = 0
    
    DTInicio = Date - 30
    DtHasta = Date
   
    
    If SP_Rut_Activo = "76.553.302-3" Then
    '    Me.Check1.Value = 1
        'SOLO KYR
        OptBusca.Value = True
        CboLista.ListIndex = CboLista.ListCount - 1
        
    End If
        
    
    
    Sql = "SELECT codigo,mar_nombre,descripcion,precio_venta,'','','',pro_codigo_interno /*, " & _
      "(SELECT sto_stock stock " & _
                        "FROM  pro_stock k " & _
                        "WHERE rut_emp='" & SP_Rut_Activo & "' AND k.pro_codigo=p.codigo AND bod_id= 1 " & _
                        "GROUP BY pro_codigo) stk, " & _
                    "(SELECT nombre_empresa " & _
                        "FROM par_codigos_proveedor o " & _
                        "JOIN maestro_proveedores s ON o.rut_proveedor=s.rut_proveedor " & _
                    "WHERE   o.pro_codigo = p.codigo AND o.rut_emp = '" & SP_Rut_Activo & "' " & _
                    "LIMIT 1) nombreproveedor, " & _
                    "(SELECT cpv_codigo_proveedor " & _
                        "FROM par_codigos_proveedor o " & _
                        "WHERE  o.pro_codigo = p.codigo AND o.rut_emp = '" & SP_Rut_Activo & "' " & _
                        "LIMIT 1) codproveedor */ " & _
          "FROM maestro_productos p " & _
          "JOIN par_marcas m ON p.mar_id=m.mar_id  " & _
          "WHERE pro_activo='SI' AND p.rut_emp='" & SP_Rut_Activo & "' " & _
          "ORDER BY descripcion " & _
          "LIMIT 20"
    
    
    CargaLista
    
        'Solo para alcalde
    '21 Agosto 2015
    If SP_Rut_Activo = "76.169.962-8" Then
        OptBusca.Value = True
    End If
End Sub

Private Sub CargaLista()
    Dim Lp_Pedir As Long
    Consulta RsProductos, Sql
 

        LLenar_Grilla RsProductos, Me, LvDetalle, True, True, True, False
        
        For i = 1 To LvDetalle.ListItems.Count
            Lp_Pedir = Val(LvDetalle.ListItems(i).SubItems(8)) - Val(LvDetalle.ListItems(i).SubItems(4))
            If Lp_Pedir > 0 Then
                LvDetalle.ListItems(i).SubItems(9) = Lp_Pedir
                LvDetalle.ListItems(i).Checked = True
            Else
                LvDetalle.ListItems(i).SubItems(9) = 0
            End If
        Next
        
        For i = 1 To LvDetalle.ListItems.Count
            If LvDetalle.ListItems(i).Checked And Val(LvDetalle.ListItems(i).SubItems(11)) = 1 Then
            
            
                Sql = "SELECT SUM(sto_stock ) stock " & _
                            "FROM  pro_stock k " & _
                            "WHERE rut_emp = '76.178.895-7' " & _
                            "AND k.pro_codigo IN(SELECT pro_codigo FROM par_productos_similares_detalle WHERE sim_id= " & _
                    "(SELECT sim_id FROM par_productos_similares_detalle WHERE sid_orden=1 AND pro_codigo= " & LvDetalle.ListItems(i) & " LIMIT 1)) " & _
                            "AND bod_id = 1"
            
                Consulta RsTmp, Sql
                If RsTmp.RecordCount > 0 Then
                    LvDetalle.ListItems(i).SubItems(12) = RsTmp!stock
                    If RsTmp!stock < Val(LvDetalle.ListItems(i).SubItems(8)) Then
                        LvDetalle.ListItems(i).SubItems(9) = LvDetalle.ListItems(i).SubItems(8) - LvDetalle.ListItems(i).SubItems(12)
                    Else
                        LvDetalle.ListItems(i).SubItems(9) = 0
                        LvDetalle.ListItems(i).Checked = False
                    End If
                   
                End If
                
            Else
                
                If LvDetalle.ListItems(i).Checked And Val(LvDetalle.ListItems(i).SubItems(10)) = 0 Then
                
                Else
                    
                    LvDetalle.ListItems(i).SubItems(9) = 0
                    LvDetalle.ListItems(i).Checked = False
                End If
                
            End If
        
        Next
        
        
        

    Me.SkResultados = LvDetalle.ListItems.Count & " Encontrados..."
End Sub



Private Sub LvDetalle_Click()
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    Me.CboCodigosProveedor.Clear
    Sql = "SELECT CONCAT(cpv_codigo_proveedor,' - ',nombre_empresa) cod_proveedor  " & _
                "FROM par_codigos_proveedor c " & _
                "JOIN maestro_proveedores m ON c.rut_proveedor=m.rut_proveedor " & _
                "WHERE pro_codigo='" & LvDetalle.SelectedItem & "' AND c.rut_emp='" & SP_Rut_Activo & "'"
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        RsTmp.MoveFirst
        Do While Not RsTmp.EOF
            Me.CboCodigosProveedor.AddItem RsTmp!cod_proveedor
        
            RsTmp.MoveNext
        Loop
        'LLenar_Grilla RsTmp, Me, LvProveedor, False, True, True, False
    End If
End Sub

Private Sub LvDetalle_DblClick()

    Dim Lp_Cant As Variant
    
    Lp_Cant = Val(InputBox("Ingresar Cantidad", "Ingrese cantidad", LvDetalle.SelectedItem.SubItems(9)))
    If Lp_Cant > 0 Then
        LvDetalle.SelectedItem.SubItems(9) = Lp_Cant
    End If

End Sub

Private Sub LvDetalle_KeyDown(KeyCode As Integer, Shift As Integer)
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    If KeyCode = 13 Then CmdSeleccionar_Click
        
End Sub
'ordena columnas
Private Sub LvDetalle_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    ordListView ColumnHeader, Me, LvDetalle
End Sub

Private Sub LvOC_DblClick()
    If LvOC.SelectedItem Is Nothing Then Exit Sub
    
    newcantidad = InputBox("Ingrese la cantidad a pedir..", "Modificando cantidad", LvOC.SelectedItem.SubItems(9))
    If Val(newcantidad) = 0 Then
        MsgBox "Cantidad ingresada no v�lida...", vbExclamation
    Else
        LvOC.SelectedItem.SubItems(9) = Val(newcantidad)
        LvOC.SelectedItem.SubItems(14) = CDbl(LvOC.SelectedItem.SubItems(9)) * CDbl(LvOC.SelectedItem.SubItems(13))
        LvOC.SelectedItem.SubItems(13) = NumFormat(LvOC.SelectedItem.SubItems(13))
        LvOC.SelectedItem.SubItems(14) = NumFormat(LvOC.SelectedItem.SubItems(14))
        TxtTotalGomas = NumFormat(TotalizaColumna(LvOC, "totaloc"))
    End If

    
    
End Sub

Private Sub OptBusca_Click()
    TxtBusqueda_Change
            Me.cmdRutProveedor.Enabled = False
        txtRutProveedor.Enabled = False
   ' DeshabilitaMMA
End Sub

Private Sub DeshabilitaMMA()
    CboMarcas.Enabled = False
    CboModelos.Enabled = False
    TxtAnoDesde.Enabled = False
    CmdAgregaAplicacion.Enabled = False

End Sub

Private Sub Option1_Click()
    TxtBusqueda_Change
    Me.cmdRutProveedor.Enabled = False
    txtRutProveedor.Enabled = False
End Sub

Private Sub Option2_Click()
    TxtBusqueda_Change
    Me.cmdRutProveedor.Enabled = False
    txtRutProveedor.Enabled = False
   ' DeshabilitaMMA
End Sub

Private Sub Option3_Click()
    If Option3.Value Then
        Me.cmdRutProveedor.Enabled = True
        txtRutProveedor.Enabled = True
    End If
End Sub

Private Sub Timer1_Timer()
    TxtBusqueda.SetFocus
    Timer1.Enabled = False
End Sub





Private Sub TxtBusqueda_Change()
    Dim Sp_Like As String, Sp_Like2 As String, Sp_Limit As String, Sp_LikeNomProv As String, Sp_LikeCodProv As String
    Sp_Like = ""
    Sp_Like2 = ""
    Sp_LikeNomProv = ""
    Sp_LikeCodProv = ""
    sp_likeinmo = ""

        sm_FiltroFecha = ""
    
   
    '31-10-2015
    'HAREMOS LA QUERY DE BUSQUEDA MAS RAPIDA LIMITANDO LA CANTIDAD DE RESULTADOS
    If CboLista.Text = "TODOS" Then
        Sp_Limit = ""
    Else
        Sp_Limit = " LIMIT " & CboLista.Text
    End If
    
    
    If Len(Me.TxtBusqueda.Text) = 0 Then
        'Me.AdoProducto.Recordset.Filter = 0
    Else
    
        If Len(TxtBusqueda) > 0 Then
            If OptBusca.Value Then
                Sp_Like = " AND descripcion LIKE '" & TxtBusqueda & "%' "
            End If
            If Option2.Value Then
                Sp_Like = " AND  descripcion LIKE '%" & TxtBusqueda & "%' "
            End If
            If Option1.Value Then
                Sp_Like = " AND pro_codigo_interno LIKE '" & TxtBusqueda & "%'"
            End If
        End If
        
      
        
        
        
        Sp_LikeNomProv = "(SELECT nombre_empresa " & _
                        "FROM par_codigos_proveedor o " & _
                        "JOIN maestro_proveedores s ON o.rut_proveedor=s.rut_proveedor " & _
                    "WHERE o.pro_codigo = p.codigo AND o.rut_emp = '" & SP_Rut_Activo & "' " & _
                    "LIMIT 1) nombreproveedor"
        Sp_LikeCodProv = "(SELECT cpv_codigo_proveedor " & _
                        "FROM par_codigos_proveedor o " & _
                        "WHERE o.pro_codigo = p.codigo AND o.rut_emp = '" & SP_Rut_Activo & "' " & _
                        "LIMIT 1) codproveedor"
        
        
        If Option3.Value Then
            '08-01-2016 _
            filtro por codigo proveedor
            Sp_RutP = ""
            If Len(txtRutProveedor) > 0 Then
                Sp_RutP = "AND o.rut_proveedor='" & txtRutProveedor & "' "
            End If


            Sp_Like = " AND (SELECT cpv_codigo_proveedor " & _
                        "FROM par_codigos_proveedor o " & _
                        "WHERE  cpv_codigo_proveedor LIKE '%" & TxtBusqueda & "%' AND o.pro_codigo = p.codigo AND o.rut_emp = '" & SP_Rut_Activo & "' " & Sp_RutP & _
                        "LIMIT 1) LIKE '" & TxtBusqueda & "%' "
            Sp_LikeNomProv = "(SELECT nombre_empresa " & _
                        "FROM par_codigos_proveedor o " & _
                        "JOIN maestro_proveedores s ON o.rut_proveedor=s.rut_proveedor " & _
                    "WHERE cpv_codigo_proveedor LIKE '%" & TxtBusqueda & "%' AND  o.pro_codigo = p.codigo AND o.rut_emp = '" & SP_Rut_Activo & "' " & Sp_RutP & _
                    "LIMIT 1) nombreproveedor"
            Sp_LikeCodProv = "(SELECT cpv_codigo_proveedor " & _
                        "FROM par_codigos_proveedor o " & _
                        "WHERE cpv_codigo_proveedor LIKE '%" & TxtBusqueda & "%' AND o.pro_codigo = p.codigo AND o.rut_emp = '" & SP_Rut_Activo & "' " & Sp_RutP & _
                        "LIMIT 1) codproveedor"
'
        End If
        
        
        If Check1.Value = 1 Then
            
            Sp_Like2 = " AND codigo IN (SELECT pro_codigo FROM inv_relaciona_codigo_marca_modelo WHERE " & _
                    "mob_id=" & CboModelos.ItemData(CboModelos.ListIndex)
            If Val(TxtAnoDesde) = 0 Then
                Sp_Like2 = Sp_Like2 & ")" 'no tomatoms en cuenta el a�o para la busqueda
            Else
                Sp_Like2 = Sp_Like2 & " AND (" & TxtAnoDesde & " BETWEEN bcm_desde AND bcm_hasta)) "
            End If
        End If
        Sql = "SELECT codigo,mar_nombre,descripcion,precio_venta, " & _
                        "(SELECT sto_stock stock " & _
                        "FROM  pro_stock k " & _
                        "WHERE rut_emp='" & SP_Rut_Activo & "' AND k.pro_codigo=p.codigo AND bod_id= 1 " & _
                        "GROUP BY pro_codigo) stk, " & _
                     Sp_LikeNomProv & "," & Sp_LikeCodProv & _
                    ",pro_codigo_interno,stock_critico,0,(SELECT sim_id FROM par_productos_similares_detalle WHERE pro_codigo=p.codigo LIMIT 1), " & _
                    "(SELECT sid_orden FROM par_productos_similares_detalle WHERE pro_codigo=p.codigo LIMIT 1) orden " & _
              "FROM maestro_productos p " & _
              "JOIN par_marcas m ON p.mar_id=m.mar_id  " & _
              "WHERE pro_activo='SI' AND p.rut_emp='" & SP_Rut_Activo & "'" & Sp_Like & Sp_Like2 & sp_likeinmo & " " & _
              " ORDER BY descripcion " & Sp_Limit

        CargaLista
    End If
End Sub

Private Sub TxtBusqueda_GotFocus()
    En_Foco TxtBusqueda
End Sub
Private Sub CargaListilla()
     If Option3.Value Then
            '08-01-2016 _
            filtro por codigo proveedor
            Sp_RutP = ""
            If Len(txtRutProveedor) > 0 Then
                Sp_RutP = "AND o.rut_proveedor='" & txtRutProveedor & "' "
            End If
            
            
            Sp_Like = " AND (SELECT cpv_codigo_proveedor " & _
                        "FROM par_codigos_proveedor o " & _
                        "WHERE  cpv_codigo_proveedor LIKE '%" & TxtBusqueda & "%' AND o.pro_codigo = p.codigo AND o.rut_emp = '" & SP_Rut_Activo & "' " & Sp_RutP & _
                        "LIMIT 1) LIKE '" & TxtBusqueda & "%' "
            Sp_LikeNomProv = "(SELECT nombre_empresa " & _
                        "FROM par_codigos_proveedor o " & _
                        "JOIN maestro_proveedores s ON o.rut_proveedor=s.rut_proveedor " & _
                    "WHERE cpv_codigo_proveedor LIKE '%" & TxtBusqueda & "%' AND  o.pro_codigo = p.codigo AND o.rut_emp = '" & SP_Rut_Activo & "' " & Sp_RutP & _
                    "LIMIT 1) nombreproveedor"
            Sp_LikeCodProv = "(SELECT cpv_codigo_proveedor " & _
                        "FROM par_codigos_proveedor o " & _
                        "WHERE cpv_codigo_proveedor LIKE '%" & TxtBusqueda & "%' AND o.pro_codigo = p.codigo AND o.rut_emp = '" & SP_Rut_Activo & "' " & Sp_RutP & _
                        "LIMIT 1) codproveedor"
                        
        End If
        
          Sql = "SELECT codigo,mar_nombre,descripcion,precio_venta, " & _
                        "(SELECT sto_stock stock " & _
                        "FROM  pro_stock k " & _
                        "WHERE rut_emp='" & SP_Rut_Activo & "' AND k.pro_codigo=p.codigo AND bod_id= 1 " & _
                        "GROUP BY pro_codigo) stk, " & _
                     Sp_LikeNomProv & "," & Sp_LikeCodProv & _
                    ",pro_codigo_interno,stock_critico,0,(SELECT sim_id FROM par_productos_similares_detalle WHERE pro_codigo=p.codigo LIMIT 1), " & _
                    "(SELECT sid_orden FROM par_productos_similares_detalle WHERE pro_codigo=p.codigo LIMIT 1) orden " & _
              "FROM maestro_productos p " & _
              "JOIN par_marcas m ON p.mar_id=m.mar_id  " & _
              "WHERE pro_activo='SI' AND p.rut_emp='" & SP_Rut_Activo & "'" & Sp_Like & Sp_Like2 & sp_likeinmo & " " & _
              " ORDER BY descripcion " & Sp_Limit

        CargaLista
End Sub


Private Sub TxtBusqueda_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 13 Then LvDetalle.SetFocus
End Sub

Private Sub TxtBusqueda_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii))) 'Transformo a mayuscula el caracter ingresado
     If KeyAscii = 39 Then KeyAscii = 0
End Sub


Private Sub TxtRutProveedor_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then CargaListilla
End Sub

Private Sub txtRutProveedor_Validate(Cancel As Boolean)
    If Len(txtRutProveedor) = 0 Then
        txtNombreProveedor = ""
        Exit Sub
    End If
    Sql = "SELECT nombre_empresa " & _
        "FROM maestro_proveedores " & _
        "WHERE rut_proveedor='" & txtRutProveedor & "'"
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        txtNombreProveedor = RsTmp!nombre_empresa
    End If
End Sub
Function Buscar(LV As ListView, _
                Cadena As String, _
                nCol As Integer, _
                bFraseCompleta As Boolean) As Long
   
   
    Dim i As Long
    Dim iStart As Long
    Dim oItem As ListItem
    
    
    'If Lv.SelectedItem Is Nothing Then
    '   i = 1
    If Not LV.SelectedItem Is Nothing Then
       iStart = LV.SelectedItem.Index + 1
    Else
       iStart = 1
    End If
    
    With LV
    For i = 1 To LV.ListItems.Count
        
        Set oItem = LV.ListItems(i)
        
        Dim sItem As String
        
        If nCol = 0 Then
            sItem = LV.ListItems(i)
        Else
            sItem = oItem.SubItems(nCol)
        End If
        If bFraseCompleta = False Then
           Dim nPos As Integer
           
           nPos = InStr(LCase(sItem), LCase(Cadena))
           If nPos <> 0 Then
              Buscar = oItem.Index
              oItem.EnsureVisible
              Exit For
           End If
        ElseIf bFraseCompleta = True Then
             If LCase(sItem) = LCase(Cadena) Then
              Buscar = oItem.Index
              oItem.EnsureVisible
              Exit For
             End If
        End If
    Next
    End With
    
    Set LV.SelectedItem = Nothing
    
End Function

Private Sub ResumenEncabezadoLaser()
    Dim Sp_Lh As String, Sp_Tit As String
    Dim Sp_Cetras As String
    
    
    For i = 1 To 100
        Sp_Lh = Sp_Lh & "_"
    Next
    Printer.FontName = "Courier New"
    

    Dp = 0.35
    Cx = 2
    Cy = 1.5
    Coloca Cx + 15, Cy, "P�gina:" & Im_Paginas & " de " & Ip_Paginas, False, 8
    Cy = Cy + Dp
    Coloca Cx + 15, Cy, "Fecha :" & Date, False, 8
    
    Cx = 2
    Cy = 2
   
    
   ' Cy = Cy + Dp
    'Datos de la empresa
    Sql = "SELECT direccion,giro,comuna,ciudad " & _
            "FROM sis_empresas " & _
            "WHERE rut='" & SP_Rut_Activo & "'"
    Consulta RsTmp, Sql
    
    
    '2-Abril-2015
    Coloca Cx, Cy, Principal.SkEmpresa, True, 10 'Nombre empresa
    Cy = Cy + Dp
    Coloca Cx, Cy, Principal.SkRut, False, 10 'rut
    Cy = Cy + Dp
    Coloca Cx, Cy, RsTmp!direccion & " - " & RsTmp!comuna & " - " & RsTmp!ciudad, False, 10 'direccion
    Cy = Cy + Dp
    Coloca Cx, Cy, RsTmp!giro, False, 10 'giro
    Cy = Cy + Dp
    Coloca Cx, Cy, Sp_Lh, False, 8
    Cy = Cy + Dp
    Coloca Cx, Cy, "                " & "ORDEN DE COMPRA A", True, 14 ' Titulo
    Cy = Cy + Dp
    Coloca Cx, Cy, Sp_Lh, False, 8
    
 
    'Ahora datos del PROVEEDOR
    Sql = "SELECT nombre_empresa,direccion,comuna,ciudad,fono,email " & _
            "FROM maestro_proveedores " & _
            "WHERE rut_proveedor='" & Me.txtRutProveedor & "'"
    Consulta RsTmp, Sql
     
     
    Cy = Cy + (Dp * 2)
    Coloca Cx, Cy, "R.U.T.   :" & txtRutProveedor, True, 10
    Cy = Cy + Dp
    Coloca Cx, Cy, "NOMBRE   :" & Me.txtNombreProveedor, False, 10 'Nombre empresa
    Cy = Cy + Dp
    Coloca Cx, Cy, "DIRECCION:" & RsTmp!direccion & "    ", False, 10
    Cy = Cy + Dp
    Coloca Cx, Cy, " CIUDAD  :" & RsTmp!ciudad & "   FONO:" & RsTmp!fono & "       email:" & RsTmp!Email, False, 10 'rut
    Cy = Cy + Dp + Dp
    Coloca Cx, Cy, Sp_Lh, False, 8
  
    S_Item = "Item"
    P_Fecha = "Cod.Prov."
    P_Documento = "Descripcion"
    P_Numero = "Cant"
    RSet P_Vence = "Precio U."
    RSet P_Venta = "SubTotal"
    'RSet P_Pago = "Abono"
    'RSet P_Saldo = "Saldo"
    Cy = Cy + Dp
    EnviaLineasC True
    Cy = Cy + Dp
     Printer.FontName = "Courier New"
    Coloca Cx, Cy, Sp_Lh, False, 8
     
    
    
    
    
End Sub

Private Function EnviaLineasC(Negrita As Boolean)
        Dim avance As Double
        avance = 1.5
        Printer.FontSize = 10
        Printer.FontName = "Arial Narrow"
        Coloca Cx, Cy, S_Item, Negrita, 10
        Coloca Cx + 1, Cy, P_Fecha, Negrita, 10
        Coloca Cx + 3.5, Cy, P_Documento, Negrita, 10
        Coloca Cx + 13, Cy, P_Numero, Negrita, 10
        Coloca (Cx + 15.5) - Printer.TextWidth(P_Vence), Cy, P_Vence, Negrita, 10
        Coloca (Cx + 17) - Printer.TextWidth(P_Venta), Cy, P_Venta, Negrita, 10
        'Coloca (Cx + 14.2) - Printer.TextWidth(P_Pago), Cy, P_Pago, Negrita, 10
        'Coloca (Cx + 17) - Printer.TextWidth(P_Saldo), Cy, P_Saldo, Negrita, 10
       
       
End Function

