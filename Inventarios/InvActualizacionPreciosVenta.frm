VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{DA729E34-689F-49EA-A856-B57046630B73}#1.0#0"; "progressbar-xp.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form InvActualizacionPreciosVenta 
   Caption         =   "Actualizador de Precios de Venta"
   ClientHeight    =   9030
   ClientLeft      =   3210
   ClientTop       =   4500
   ClientWidth     =   16095
   LinkTopic       =   "Form1"
   ScaleHeight     =   9030
   ScaleWidth      =   16095
   Begin VB.CommandButton CmdF8 
      Caption         =   "F8 - Ficha Producto"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   225
      TabIndex        =   24
      Top             =   1590
      Width           =   2235
   End
   Begin VB.Frame FraProgreso 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      ForeColor       =   &H80000008&
      Height          =   795
      Left            =   1590
      TabIndex        =   21
      Top             =   5880
      Visible         =   0   'False
      Width           =   11430
      Begin Proyecto2.XP_ProgressBar BarraProgreso 
         Height          =   495
         Left            =   90
         TabIndex        =   22
         Top             =   210
         Width           =   11190
         _ExtentX        =   19738
         _ExtentY        =   873
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BrushStyle      =   0
         Color           =   16777088
         Scrolling       =   1
         ShowText        =   -1  'True
      End
   End
   Begin VB.CommandButton CmdExportar 
      Caption         =   "&Exportar"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   5460
      TabIndex        =   20
      ToolTipText     =   "Exportar planilla a Excel"
      Top             =   8265
      Width           =   1140
   End
   Begin VB.Frame Frame3 
      Caption         =   "Lista de precios"
      Height          =   1095
      Left            =   210
      TabIndex        =   18
      Top             =   270
      Width           =   3555
      Begin VB.ComboBox CboListaPrecios 
         Appearance      =   0  'Flat
         Height          =   315
         ItemData        =   "InvActualizacionPreciosVenta.frx":0000
         Left            =   225
         List            =   "InvActualizacionPreciosVenta.frx":0002
         Style           =   2  'Dropdown List
         TabIndex        =   19
         Top             =   465
         Width           =   3090
      End
   End
   Begin VB.CommandButton CmdOk 
      Caption         =   "&Aplicar Cambio de Precios"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   465
      Left            =   10605
      TabIndex        =   17
      Top             =   7455
      Width           =   5250
   End
   Begin VB.CommandButton cmdSalir 
      Caption         =   "&Retornar"
      Height          =   465
      Left            =   14625
      TabIndex        =   16
      Top             =   8460
      Width           =   1185
   End
   Begin VB.Frame FraInformacionExtra 
      Caption         =   "Cantidad Registros"
      Height          =   720
      Left            =   7830
      TabIndex        =   11
      Top             =   7305
      Width           =   1680
      Begin VB.TextBox txtCntidadregistros 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   450
         Locked          =   -1  'True
         TabIndex        =   12
         Text            =   "0"
         Top             =   315
         Width           =   945
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Opciones de cambio de precios"
      Height          =   1200
      Left            =   225
      TabIndex        =   8
      Top             =   7575
      Width           =   6645
      Begin VB.CheckBox ChkRedondea 
         Caption         =   "Redondear a 10"
         Height          =   255
         Left            =   3540
         TabIndex        =   15
         Top             =   375
         Width           =   1635
      End
      Begin VB.CommandButton CmdCalcular 
         Caption         =   "Calcular"
         Height          =   390
         Left            =   5250
         TabIndex        =   14
         Top             =   285
         Width           =   1125
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   210
         Left            =   135
         OleObjectBlob   =   "InvActualizacionPreciosVenta.frx":0004
         TabIndex        =   13
         Top             =   885
         Width           =   6285
      End
      Begin VB.TextBox TxtFactor 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   2325
         TabIndex        =   10
         ToolTipText     =   "Ej: 10    o  -10"
         Top             =   315
         Width           =   690
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   225
         Left            =   240
         OleObjectBlob   =   "InvActualizacionPreciosVenta.frx":00EC
         TabIndex        =   9
         Top             =   360
         Width           =   4155
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Busqueda"
      Height          =   1305
      Left            =   3750
      TabIndex        =   0
      Top             =   270
      Width           =   12045
      Begin VB.OptionButton OptBarra 
         Caption         =   "Codigo Barra/Int"
         Height          =   420
         Left            =   1695
         TabIndex        =   23
         Top             =   690
         Width           =   1740
      End
      Begin VB.TextBox TxtBusca 
         Height          =   375
         Left            =   120
         TabIndex        =   6
         Top             =   240
         Width           =   3390
      End
      Begin VB.OptionButton OpDescripcion 
         Caption         =   "Descripcion"
         Height          =   255
         Left            =   120
         TabIndex        =   5
         Top             =   720
         Value           =   -1  'True
         Width           =   1215
      End
      Begin VB.OptionButton OpCodigo 
         Caption         =   "Codigo"
         Height          =   255
         Left            =   120
         TabIndex        =   4
         Top             =   975
         Width           =   975
      End
      Begin VB.CommandButton CmdBuscaProd 
         Caption         =   "&Buscar"
         Height          =   375
         Left            =   10935
         TabIndex        =   3
         Top             =   600
         Width           =   975
      End
      Begin VB.Frame FraMarca 
         Caption         =   "Categoria"
         Height          =   1095
         Left            =   3600
         TabIndex        =   2
         Top             =   0
         Width           =   3750
         Begin VB.ComboBox CboTipoProducto 
            Height          =   315
            Left            =   90
            Style           =   2  'Dropdown List
            TabIndex        =   25
            Top             =   630
            Width           =   3585
         End
      End
      Begin VB.Frame FraFamilia 
         Caption         =   "Sub-Categoria"
         Height          =   1095
         Left            =   7350
         TabIndex        =   1
         Top             =   0
         Width           =   3450
         Begin VB.ComboBox CboMarca 
            Height          =   315
            Left            =   150
            Style           =   2  'Dropdown List
            TabIndex        =   26
            Top             =   615
            Width           =   3255
         End
      End
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   -60
      OleObjectBlob   =   "InvActualizacionPreciosVenta.frx":019E
      Top             =   150
   End
   Begin VB.Timer Timer1 
      Interval        =   1
      Left            =   480
      Top             =   180
   End
   Begin MSComctlLib.ListView LvDetalle 
      Height          =   5250
      Left            =   195
      TabIndex        =   7
      Top             =   1995
      Width           =   15630
      _ExtentX        =   27570
      _ExtentY        =   9260
      View            =   3
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   11
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Object.Tag             =   "N109"
         Text            =   "Codigo"
         Object.Width           =   1411
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Object.Tag             =   "T2000"
         Text            =   "Codigo Barra/Int"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Object.Tag             =   "T2000"
         Text            =   "Descripcion"
         Object.Width           =   7056
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Object.Tag             =   "T2000"
         Text            =   "Categoria"
         Object.Width           =   4233
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Object.Tag             =   "T4000"
         Text            =   "Sub-Categoria"
         Object.Width           =   4233
      EndProperty
      BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   5
         Object.Tag             =   "N100"
         Text            =   "Ultimo Pre. Compra"
         Object.Width           =   2558
      EndProperty
      BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   6
         Object.Tag             =   "N102"
         Text            =   "Factor %"
         Object.Width           =   1676
      EndProperty
      BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   7
         Object.Tag             =   "N100"
         Text            =   "Pre. Venta"
         Object.Width           =   2117
      EndProperty
      BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   8
         Object.Tag             =   "N102"
         Text            =   "Utilidad"
         Object.Width           =   2117
      EndProperty
      BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   9
         Object.Tag             =   "N109"
         Text            =   "Id"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   10
         Object.Tag             =   "N109"
         Text            =   "Precio vent actual"
         Object.Width           =   0
      EndProperty
   End
End
Attribute VB_Name = "InvActualizacionPreciosVenta"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim RsProductos As Recordset, s_Sql As String
Dim Sm_filtro As String
Dim Sm_FiltroMarca As String
Dim Sm_FiltroTipo  As String
Dim Lp_Id As Long
Dim Bm_Cargado As Boolean
Dim Bm_Actualizando As Boolean
Dim Bm_GrillaCargada As Boolean

Private Sub CboListaPrecios_Click()
    If CboListaPrecios.ListIndex = -1 Then Exit Sub
    Lp_Id = CboListaPrecios.ItemData(CboListaPrecios.ListIndex)
    If Lp_Id = 0 Then
        s_Sql = "SELECT codigo,pro_codigo_interno,descripcion,tip_nombre,mar_nombre,precio_compra,IFNULL((precio_venta-precio_compra)/precio_compra*100,1) factor,IFNULL(precio_venta,0),precio_venta-precio_compra margen,p.id,precio_venta pactual " & _
                "FROM maestro_productos p,par_tipos_productos t,par_marcas m /*,par_bodegas b */ " & _
                "WHERE pro_activo='SI' AND pro_codigo_interno LIKE '%EM%' AND p.rut_emp='" & SP_Rut_Activo & "' AND  p.tip_id=t.tip_id AND p.mar_id=m.mar_id /* AND p.bod_id=b.bod_id */ " & Sm_filtro & Sm_FiltroTipo & Sm_FiltroMarca
    Else
     '
          s_Sql = "SELECT codigo,pro_codigo_interno,descripcion,tip_nombre,mar_nombre,precio_compra,IFNULL((" & _
               "(SELECT lsd_precio FROM par_lista_precios_detalle WHERE lst_id=" & Lp_Id & " AND id=p.id)" & _
          "-precio_compra)/precio_compra*100,0) factor," & _
          "IFNULL((SELECT lsd_precio FROM par_lista_precios_detalle WHERE lst_id=" & Lp_Id & " AND id=p.id),0)," & _
          "IFNULL((SELECT lsd_precio FROM par_lista_precios_detalle WHERE lst_id=" & Lp_Id & " AND id=p.id),0)-precio_compra margen,p.id,precio_venta pactual " & _
                "FROM maestro_productos p,par_tipos_productos t,par_marcas m /*,par_bodegas b */ " & _
                "WHERE pro_activo='SI'  AND pro_codigo_interno LIKE '%EM%' AND p.rut_emp='" & SP_Rut_Activo & "' AND  p.tip_id=t.tip_id AND p.mar_id=m.mar_id /* AND p.bod_id=b.bod_id */ " & Sm_filtro & Sm_FiltroTipo & Sm_FiltroMarca
     
     
     
     
     's_Sql = "SELECT codigo,descripcion,tip_nombre,mar_nombre,precio_compra,IFNULL((lsd_precio-precio_compra)/precio_compra*100,0) factor,IFNULL(lsd_precio,0),lsd_precio-precio_compra margen,p.id,precio_venta pactual,pro_codigo_interno " & _
                "FROM maestro_productos p " & _
                "INNER JOIN par_tipos_productos t USING(tip_id) " & _
                "INNER JOIN par_marcas m USING(mar_id) " & _
                "/* INNER JOIN par_bodegas b USING(bod_id) */ " & _
                "rigth JOIN par_lista_precios l ON p.rut_emp=l.rut_emp " & _
                "right JOIN par_lista_precios_detalle d ON l.lst_id=d.lst_id AND p.id=d.id " & _
                "WHERE d.lst_id=" & Lp_Id & " AND p.rut_emp='" & SP_Rut_Activo & "' " & Sm_filtro & Sm_FiltroTipo & Sm_FiltroMarca
         
    End If
    If Bm_Cargado Then Grilla
End Sub

Private Sub CboMarca_Click()
    If CboMarca.Text = "TODOS" Then
        Sm_FiltroMarca = Empty
    Else
        Sm_FiltroMarca = " AND p.mar_id=" & CboMarca.ItemData(CboMarca.ListIndex) & " "
    End If
    
    If Bm_Cargado Then Grilla
End Sub

Private Sub CboTipoProducto_Click()
''''    If CboTipoProducto.Text = "TODOS" Then
''''        Sm_FiltroTipo = Empty
''''    Else
''''        Sm_FiltroTipo = " AND p.tip_id=" & CboTipoProducto.ItemData(CboTipoProducto.ListIndex) & " "
''''    End If
''''    If Bm_Cargado Then Grilla

    Bm_GrillaCargada = True
        If Not Bm_GrillaCargada Then Exit Sub
    If CboTipoProducto.Text = "TODOS" Then
        Sm_FiltroTipo = Empty
        CboMarca.Clear
        CboMarca.AddItem "TODOS"
    Else
         LLenarCombo CboMarca, "mar_nombre", "mar_id", "par_marcas", "tip_id=" & CboTipoProducto.ItemData(CboTipoProducto.ListIndex) & " AND mar_activo='SI' AND  rut_emp='" & SP_Rut_Activo & "'", "mar_nombre"
         Sm_FiltroMarca = ""
        Sm_FiltroTipo = " AND p.tip_id=" & CboTipoProducto.ItemData(CboTipoProducto.ListIndex) & " "

    End If
    Grilla
End Sub
Private Sub CmdBuscaProd_Click()
    Sm_filtro = Empty
    If Len(TxtBusca) > 0 Then
        If Me.OpDescripcion.Value Then
            Sm_filtro = "AND descripcion LIKE '%" & TxtBusca & "%' "
        End If
        If Me.OpCodigo Then
            Sm_filtro = "AND codigo LIKE '%" & TxtBusca & "%' "
        End If
        If OptBarra Then
            Sm_filtro = " AND pro_codigo_interno LIKE '" & TxtBusca & "%'"
        End If
    End If
    Grilla
    LvDetalle.SetFocus
End Sub

Private Sub CmdCalcular_Click()
    If LvDetalle.ListItems.Count = 0 Then Exit Sub
    If Val(TxtFactor) = 0 Then Exit Sub
    For i = 1 To LvDetalle.ListItems.Count
        LvDetalle.ListItems(i).SubItems(6) = Format(CDbl(LvDetalle.ListItems(i).SubItems(6)) + (CDbl(LvDetalle.ListItems(i).SubItems(6)) / 100 * TxtFactor), "#,#0")
        If Me.ChkRedondea.Value = 1 Then LvDetalle.ListItems(i).SubItems(6) = Int(CDbl(LvDetalle.ListItems(i).SubItems(6)) / 10) * 10
        If CDbl(LvDetalle.ListItems(i).SubItems(6)) > 0 Then
            LvDetalle.ListItems(i).SubItems(5) = Format((CDbl(LvDetalle.ListItems(i).SubItems(6)) - CDbl(LvDetalle.ListItems(i).SubItems(4))) / (CDbl(LvDetalle.ListItems(i).SubItems(6))) * 100, "#.##")
            LvDetalle.ListItems(i).SubItems(7) = Format(CDbl(LvDetalle.ListItems(i).SubItems(6)) - CDbl(LvDetalle.ListItems(i).SubItems(4)), "#,#")
        End If
    Next
    
End Sub



Private Sub CmdExportar_Click()
 Dim tit(2) As String
    If LvDetalle.ListItems.Count = 0 Then Exit Sub
    FraProgreso.Visible = True
    tit(0) = Me.Caption & " " & DtFecha & " - " & Time
    tit(1) = ""
    tit(2) = ""
    ExportarNuevo LvDetalle, tit, Me, BarraProgreso
    FraProgreso.Visible = False
End Sub

Private Sub CmdF8_Click()
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    
    par_productos.OpCodigo.Value = True
    par_productos.TxtBusca = LvDetalle.SelectedItem
    par_productos.Show 1
            
End Sub

Private Sub CmdOk_Click()
    Dim A_Sql As String, Lp_Act As Long, D_Sql As String
    If CboListaPrecios.ListIndex = -1 Then Exit Sub
    Lp_Id = CboListaPrecios.ItemData(CboListaPrecios.ListIndex)
    If MsgBox("�Esta seguro de aplicar cambio de precios...?", vbYesNo + vbQuestion) = vbNo Then Exit Sub
    FraProgreso.Visible = True
    DoEvents
    cn.BeginTrans
        Lp_Act = UltimoNro("inv_actualizacion_precios", "act_id")
        D_Sql = ""
        
        
        BarraProgreso.Min = 1
        If LvDetalle.ListItems.Count = 1 Then BarraProgreso.Max = 10 Else BarraProgreso.Max = LvDetalle.ListItems.Count
        For i = 1 To LvDetalle.ListItems.Count
            BarraProgreso.Value = i
            If Lp_Id = 0 Then
                A_Sql = "UPDATE maestro_productos SET porciento_utilidad=" & CxP(CDbl(LvDetalle.ListItems(i).SubItems(5))) & _
                                                     ",precio_venta=" & CDbl(LvDetalle.ListItems(i).SubItems(6)) & _
                                                     ",margen=" & CDbl(LvDetalle.ListItems(i).SubItems(7)) & _
                        "  WHERE id=" & LvDetalle.ListItems(i).SubItems(8)
            Else
                A_Sql = "DELETE FROM par_lista_precios_detalle " & _
                            "  WHERE lst_id=" & Lp_Id & " AND id= '" & LvDetalle.ListItems(i).SubItems(8) & "'"
                cn.Execute A_Sql

            
                A_Sql = "INSERT INTO par_lista_precios_detalle(lst_id,id,lsd_precio) " & _
                               "VALUES(" & Lp_Id & "," & LvDetalle.ListItems(i).SubItems(8) & "," & CDbl(LvDetalle.ListItems(i).SubItems(6)) & ")"
            
            '    A_Sql = "INSERT INTO par_lista_precios_detalle SET lsd_precio=" & CDbl(LvDetalle.ListItems(i).SubItems(6)) & " " & _
                        "  WHERE lst_id=" & Lp_Id & " AND id=" & LvDetalle.ListItems(i).SubItems(8)
            
            End If
            cn.Execute A_Sql
            
            D_Sql = D_Sql & "(" & Lp_Act & "," & LvDetalle.ListItems(i).SubItems(8) & "," & CDbl(LvDetalle.ListItems(i).SubItems(9)) & "," & CDbl(LvDetalle.ListItems(i).SubItems(6)) & "),"
            
        Next
        D_Sql = Mid(D_Sql, 1, Len(D_Sql) - 1)
        
        
        Sql = "INSERT INTO inv_actualizacion_precios (act_id,usu_login,act_fecha,act_hora,rut_emp,lst_id) VALUES(" & _
               Lp_Act & ",'" & LogUsuario & "','" & Fql(Date) & "','" & Time & "','" & SP_Rut_Activo & "'," & Lp_Id & ")"
        cn.Execute Sql
        
        Sql = "INSERT INTO inv_actualizacion_precios_detalle (act_id,id,acd_precio_anterior,acd_precio_nuevo) VALUES " & D_Sql
        cn.Execute Sql
        
    
    
    cn.CommitTrans
    MsgBox "Actualizacion realizada con exito...", vbInformation
    FraProgreso.Visible = False
    Exit Sub
ErrorAct:
    FraProgreso.Visible = False
    cn.RollbackTrans
    MsgBox "Ocurrio un error al actualizar..." & vbNewLine & "No se realiz� ningun cambio" & vbNewLine & Err.Number & vbNewLine & Err.Description
End Sub

Private Sub CmdSalir_Click()
    Unload Me
End Sub





Private Sub Form_KeyUp(KeyCode As Integer, Shift As Integer)
      
    
    If KeyCode = vbKeyF8 Then
        CmdF8_Click
    End If
   
End Sub

Private Sub Form_Load()
    Centrar Me
    Bm_Cargado = False
    s_Sql = "SELECT codigo,pro_codigo_interno,descripcion,tip_nombre,mar_nombre,precio_compra,IFNULL((precio_venta-precio_compra)/precio_compra*100,0) factor,IFNULL(precio_venta,0),precio_venta-precio_compra margen,p.id,precio_venta pactual,0,0 " & _
                "FROM maestro_productos p,par_tipos_productos t,par_marcas m,par_bodegas b " & _
                "WHERE pro_activo='SI' AND pro_codigo_interno LIKE '%EM%'  AND p.rut_emp='" & SP_Rut_Activo & "' AND  p.tip_id=t.tip_id AND p.mar_id=m.mar_id AND p.bod_id=b.bod_id " & Sm_filtro & Sm_FiltroTipo & Sm_FiltroMarca
                
    LLenarCombo CboTipoProducto, "tip_nombre", "tip_id", "par_tipos_productos", "tip_activo='SI' AND  rut_emp='" & SP_Rut_Activo & "'", "tip_nombre"
    CboTipoProducto.AddItem "TODOS"
    CboTipoProducto.ListIndex = CboTipoProducto.ListCount - 1
    LLenarCombo CboMarca, "mar_nombre", "mar_id", "par_marcas", "mar_activo='SI' AND  rut_emp='" & SP_Rut_Activo & "'", "mar_nombre"
    CboMarca.AddItem "TODOS"
    CboMarca.ListIndex = CboMarca.ListCount - 1
    Sm_FiltroMarca = Empty
    Sm_FiltroTipo = Empty
    Aplicar_skin Me
    'Grilla
    
  '  CboSucActivo.ListIndex = 0
    CboListaPrecios.Enabled = False
    LLenarCombo Me.CboListaPrecios, "lst_nombre", "lst_id", "par_lista_precios", "lst_activo='SI' AND rut_emp='" & SP_Rut_Activo & "'"
   '' CboListaPrecios.AddItem "SIN LISTA DE PRECIOS"
    CboListaPrecios.ItemData(CboListaPrecios.ListCount - 1) = 0
    CboListaPrecios.Enabled = True
    CboListaPrecios.ListIndex = CboListaPrecios.ListCount - 1
    Bm_Cargado = True
    Grilla
    If SP_Rut_Activo = "76.178.895-7" Then
        OptBarra.Value = True
    End If
End Sub



Private Sub LvDetalle_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    ordListView ColumnHeader, Me, LvDetalle
End Sub

Private Sub LvDetalle_DblClick()
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    Bm_Actualizando = True
    NuevoPrecio
   
    
End Sub
Private Sub NuevoPrecio()
    SG_codigo2 = Empty
    sis_InputBox.Caption = "Nuevo Precio"
    sis_InputBox.FramBox = "Ingrese valor"
    sis_InputBox.texto.PasswordChar = ""
    sis_InputBox.Sm_TipoDato = "N"
    sis_InputBox.texto = LvDetalle.SelectedItem.SubItems(6)
    sis_InputBox.Show 1
    
    If Val(SG_codigo2) > 0 Then
        LvDetalle.SelectedItem.SubItems(7) = NumFormat(SG_codigo2)
        
        If CDbl(LvDetalle.SelectedItem.SubItems(5)) > 0 And (CDbl(LvDetalle.SelectedItem.SubItems(7)) - CDbl(LvDetalle.SelectedItem.SubItems(5))) > 0 Then
            LvDetalle.SelectedItem.SubItems(6) = Format((CDbl(LvDetalle.SelectedItem.SubItems(7)) - CDbl(LvDetalle.SelectedItem.SubItems(5))) / (CDbl(LvDetalle.SelectedItem.SubItems(5))) * 100, "#,##0")
            LvDetalle.SelectedItem.SubItems(8) = Format(CDbl(LvDetalle.SelectedItem.SubItems(7)) - CDbl(LvDetalle.SelectedItem.SubItems(5)), "#,##0")
        Else
            LvDetalle.SelectedItem.SubItems(6) = 1
            LvDetalle.SelectedItem.SubItems(8) = NumFormat(CDbl(LvDetalle.SelectedItem.SubItems(7)))
        End If
    End If
    
End Sub
''Private Sub NuevoPrecio()
''    SG_codigo2 = Empty
''    sis_InputBox.Caption = "Nuevo Precio"
''    sis_InputBox.FramBox = "Ingrese valor"
''    sis_InputBox.texto.PasswordChar = ""
''    sis_InputBox.Sm_TipoDato = "N"
''    sis_InputBox.texto = LvDetalle.SelectedItem.SubItems(6)
''    sis_InputBox.Show 1
''
''    If Val(SG_codigo2) > 0 Then
''        LvDetalle.SelectedItem.SubItems(6) = NumFormat(SG_codigo2)
''
''        If CDbl(LvDetalle.SelectedItem.SubItems(4)) > 0 And (CDbl(LvDetalle.SelectedItem.SubItems(6)) - CDbl(LvDetalle.SelectedItem.SubItems(4))) > 0 Then
''            LvDetalle.SelectedItem.SubItems(5) = Format((CDbl(LvDetalle.SelectedItem.SubItems(6)) - CDbl(LvDetalle.SelectedItem.SubItems(4))) / (CDbl(LvDetalle.SelectedItem.SubItems(4))) * 100, "#,##0")
''            LvDetalle.SelectedItem.SubItems(7) = Format(CDbl(LvDetalle.SelectedItem.SubItems(6)) - CDbl(LvDetalle.SelectedItem.SubItems(4)), "#,##0")
''        Else
''            LvDetalle.SelectedItem.SubItems(5) = 1
''            LvDetalle.SelectedItem.SubItems(7) = NumFormat(CDbl(LvDetalle.SelectedItem.SubItems(6)))
''        End If
''    End If
''
''End Sub



Private Sub LvDetalle_KeyPress(KeyAscii As Integer)
             If KeyAscii = 13 Then
            'Bm_Actualizando = True
            NuevoPrecio
        End If
        KeyAscii = 0
End Sub

Private Sub LvDetalle_KeyUp(KeyCode As Integer, Shift As Integer)
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    
    If KeyCode = vbKeyF8 Then
            
        CmdF8_Click
    End If
End Sub

Private Sub Timer1_Timer()
    On Error Resume Next
    LvDetalle.SetFocus
    Timer1.Enabled = False
End Sub
Private Sub Grilla()
    
    Consulta RsProductos, s_Sql & Sm_filtro & Sm_FiltroMarca & Sm_FiltroTipo
    LLenar_Grilla RsProductos, Me, LvDetalle, False, True, True, False
    Me.txtCntidadregistros = RsProductos.RecordCount
    
End Sub

Private Sub TxtBusca_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
    If KeyAscii = 13 Then
        CmdBuscaProd_Click
    End If
    If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtBusca_Validate(Cancel As Boolean)
TxtBusca = Replace(TxtBusca, "'", "")
End Sub

Private Sub TxtFactor_GotFocus()
    En_Foco TxtFactor
End Sub
Private Sub TxtFactor_KeyPress(KeyAscii As Integer)
    If KeyAscii = 45 Then
        '
    Else
        If Not IsNumeric(Chr(KeyAscii)) Then KeyAscii = 0
    End If
    'KeyAscii = SoloNumeros(KeyAscii)
End Sub
