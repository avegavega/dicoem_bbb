VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{28D47522-CF84-11D1-834C-00A0249F0C28}#1.0#0"; "gif89.dll"
Object = "{DA729E34-689F-49EA-A856-B57046630B73}#1.0#0"; "progressbar-xp.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form InvInventarioValorizado 
   Caption         =   "Informe Inventario Valorizado"
   ClientHeight    =   9315
   ClientLeft      =   4965
   ClientTop       =   2370
   ClientWidth     =   15450
   LinkTopic       =   "Form1"
   ScaleHeight     =   9315
   ScaleWidth      =   15450
   Begin VB.Frame FrmLoad 
      Height          =   1905
      Left            =   5475
      TabIndex        =   22
      Top             =   5895
      Visible         =   0   'False
      Width           =   2805
      Begin GIF89LibCtl.Gif89a Gif89a1 
         Height          =   1725
         Left            =   210
         OleObjectBlob   =   "InvInventarioValorizado.frx":0000
         TabIndex        =   23
         Top             =   285
         Width           =   2445
      End
   End
   Begin Proyecto2.XP_ProgressBar pbarCarga 
      Height          =   615
      Left            =   4530
      TabIndex        =   18
      Top             =   3105
      Visible         =   0   'False
      Width           =   7590
      _ExtentX        =   13388
      _ExtentY        =   1085
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BrushStyle      =   0
      Color           =   11034163
      Scrolling       =   7
   End
   Begin VB.Timer Timer2 
      Enabled         =   0   'False
      Interval        =   50
      Left            =   -60
      Top             =   4140
   End
   Begin VB.CommandButton CmdIrKardex 
      Caption         =   "Consultar Kardex"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   450
      Left            =   375
      TabIndex        =   17
      Top             =   8760
      Width           =   1950
   End
   Begin VB.CommandButton CmdSalir 
      Cancel          =   -1  'True
      Caption         =   "&Retornar"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   390
      Left            =   13155
      TabIndex        =   12
      ToolTipText     =   "Exportar planilla a Excel"
      Top             =   8760
      Width           =   1665
   End
   Begin VB.Frame FrmArticulos 
      Caption         =   "Articulos de la nueva planilla"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   8250
      Left            =   420
      TabIndex        =   0
      Top             =   420
      Width           =   14835
      Begin VB.CommandButton CmdUltimaCompra 
         Caption         =   "Ult. Compra"
         Height          =   390
         Left            =   13425
         TabIndex        =   24
         Top             =   525
         Width           =   1080
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkCantidad 
         Height          =   330
         Left            =   2520
         OleObjectBlob   =   "InvInventarioValorizado.frx":0086
         TabIndex        =   21
         Top             =   7800
         Width           =   2985
      End
      Begin VB.CommandButton CmdValorizado 
         Caption         =   "Valorizado"
         Height          =   390
         Left            =   12420
         TabIndex        =   20
         Top             =   525
         Width           =   975
      End
      Begin VB.CommandButton CmdBuscaProductos 
         Caption         =   "Cargo Productos"
         Height          =   390
         Left            =   11085
         TabIndex        =   19
         Top             =   525
         Width           =   1320
      End
      Begin VB.ComboBox CboBodega 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         ItemData        =   "InvInventarioValorizado.frx":0116
         Left            =   6510
         List            =   "InvInventarioValorizado.frx":0123
         Style           =   2  'Dropdown List
         TabIndex        =   15
         ToolTipText     =   "Seleccione Bodega"
         Top             =   585
         Width           =   3105
      End
      Begin VB.TextBox TxtTotal 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   11505
         Locked          =   -1  'True
         TabIndex        =   13
         TabStop         =   0   'False
         Tag             =   "N"
         Text            =   "0"
         Top             =   7830
         Width           =   1845
      End
      Begin VB.CommandButton cmdKardex 
         Caption         =   "Consultar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   450
         Left            =   15405
         TabIndex        =   11
         Top             =   690
         Width           =   1545
      End
      Begin VB.ComboBox CboTipo 
         Height          =   315
         Left            =   690
         Style           =   2  'Dropdown List
         TabIndex        =   5
         Top             =   585
         Width           =   3210
      End
      Begin VB.ComboBox CboMarca 
         Height          =   315
         Left            =   3885
         Style           =   2  'Dropdown List
         TabIndex        =   4
         Top             =   585
         Width           =   2625
      End
      Begin VB.Frame FraProgreso 
         Height          =   795
         Left            =   1710
         TabIndex        =   2
         Top             =   3915
         Visible         =   0   'False
         Width           =   11430
         Begin Proyecto2.XP_ProgressBar BarraProgreso 
            Height          =   495
            Left            =   90
            TabIndex        =   3
            Top             =   195
            Width           =   11190
            _ExtentX        =   19738
            _ExtentY        =   873
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BrushStyle      =   0
            Color           =   16777088
            Scrolling       =   1
            ShowText        =   -1  'True
         End
      End
      Begin VB.CommandButton CmdExportar 
         Caption         =   "&Exportar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   390
         Left            =   195
         TabIndex        =   1
         ToolTipText     =   "Exportar planilla a Excel"
         Top             =   7815
         Width           =   1665
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   240
         Left            =   555
         OleObjectBlob   =   "InvInventarioValorizado.frx":0148
         TabIndex        =   6
         Top             =   375
         Width           =   1125
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   195
         Index           =   0
         Left            =   3750
         OleObjectBlob   =   "InvInventarioValorizado.frx":01C0
         TabIndex        =   7
         Top             =   375
         Width           =   615
      End
      Begin MSComctlLib.ListView LvProductos 
         Height          =   6705
         Left            =   180
         TabIndex        =   8
         Top             =   1095
         Width           =   14535
         _ExtentX        =   25638
         _ExtentY        =   11827
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   0   'False
         HideSelection   =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   8
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "T1500"
            Text            =   "Codigo"
            Object.Width           =   2646
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "T2000"
            Text            =   "Tipo Producto"
            Object.Width           =   3175
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "T2000"
            Text            =   "Marca"
            Object.Width           =   3175
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Object.Tag             =   "T5000"
            Text            =   "Descripcion"
            Object.Width           =   7937
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   4
            Object.Tag             =   "N102"
            Text            =   "Stock"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   5
            Object.Tag             =   "N102"
            Text            =   "Costo Promedio"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   6
            Key             =   "total"
            Object.Tag             =   "N100"
            Text            =   "Valorizado"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   7
            Object.Tag             =   "F1000"
            Text            =   "Ult. Compra"
            Object.Width           =   2540
         EndProperty
      End
      Begin MSComCtl2.DTPicker DtHasta 
         Height          =   285
         Left            =   9615
         TabIndex        =   9
         Top             =   600
         Width           =   1305
         _ExtentX        =   2302
         _ExtentY        =   503
         _Version        =   393216
         Format          =   94765057
         CurrentDate     =   40628
      End
      Begin ACTIVESKINLibCtl.SkinLabel skFin 
         Height          =   240
         Left            =   9600
         OleObjectBlob   =   "InvInventarioValorizado.frx":0228
         TabIndex        =   10
         Top             =   390
         Width           =   1290
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   270
         Left            =   8895
         OleObjectBlob   =   "InvInventarioValorizado.frx":02A0
         TabIndex        =   14
         Top             =   7860
         Width           =   2580
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   195
         Index           =   1
         Left            =   7110
         OleObjectBlob   =   "InvInventarioValorizado.frx":0317
         TabIndex        =   16
         Top             =   375
         Width           =   615
      End
   End
   Begin VB.Timer Timer1 
      Interval        =   10
      Left            =   660
      Top             =   7665
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   60
      OleObjectBlob   =   "InvInventarioValorizado.frx":0381
      Top             =   7740
   End
End
Attribute VB_Name = "InvInventarioValorizado"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim Sm_FiltroTipo As String, Sm_FiltroMarca As String, Sm_FiltroHasta
Dim Bm_CargoDatos As Boolean
Dim Ct_X As Integer




Private Sub CboMarca_Click()
'    If Not Bm_CargoDatos Then Exit Sub
   If CboMarca.ListIndex = -1 Then Exit Sub
    If CboMarca.Text = "TODOS" Then
        Sm_FiltroMarca = Empty
    Else
        Sm_FiltroMarca = " AND p.mar_id=" & CboMarca.ItemData(CboMarca.ListIndex) & " "
    End If
    'CargaDatos
End Sub

Private Sub CboTipo_Click()
'    If Not Bm_CargoDatos Then Exit Sub
    If CboTipo.ListIndex = -1 Then Exit Sub
    If CboTipo.Text = "TODOS" Then
        Sm_FiltroTipo = Empty
    Else
        Sm_FiltroTipo = " AND p.tip_id=" & CboTipo.ItemData(CboTipo.ListIndex) & " "
    End If
    'CargaDatos
End Sub



Private Sub CmdBuscaProductos_Click()
            '18 Abril 2015
       
    DoEvents
    FrmLoad.Visible = True
    Bm_CargoDatos = True
    Sql = "SELECT codigo,tip_nombre, mar_nombre,descripcion,0,0 " & _
                "FROM maestro_productos p " & _
                "JOIN par_marcas m ON p.mar_id=m.mar_id " & _
                "JOIN par_tipos_productos t ON p.tip_id=t.tip_id " & _
                "WHERE  pro_inventariable='SI' AND  " & Sp_FiltroBodega & " p.rut_emp='" & SP_Rut_Activo & "' " & Sm_FiltroMarca & Sm_FiltroTipo & "  " & _
                      Sm_FiltroMarca & Sm_FiltroTipo & " " & _
                "ORDER BY codigo DESC "
        
        
    On Error Resume Next
 '   Consulta RsTmp, Sql
    DoEvents
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvProductos, False, True, True, False
    SkCantidad = "Cantidad de productos:" & RsTmp.RecordCount
    TxtTotal = 0
    FrmLoad.Visible = False
End Sub

Private Sub CmdExportar_Click()
    Dim tit(2) As String
    FraProgreso.Visible = True
    tit(0) = "Inventario Valorizado " & DtFecha & " - " & Time
    tit(1) = "" '"Nro Planilla " & txtNroPlanilla & " - Bodega:" & CboBodega.Text & " - " & Me.TxtDescripcion
    tit(2) = ""
    ExportarNuevo Me.LvProductos, tit, Me, BarraProgreso
    FraProgreso.Visible = False
End Sub

Private Sub CmdIrKardex_Click()
    inv_KardexGeneral.Show 1
End Sub

Private Sub cmdKardex_Click()
    Bm_CargoDatos = True
    Sm_FiltroHasta = " AND kar_fecha <= '" & Format(DtHasta, "YYYY-MM-DD") & "' "
    CargaDatos
     pbarCarga.Visible = False
End Sub

Private Sub CmdSalir_Click()
    Unload Me
End Sub

Private Sub CmdUltimaCompra_Click()
    DoEvents
    FrmLoad.Visible = True
    
    For i = 1 To LvProductos.ListItems.Count
        Sql = "SELECT fecha lafecha " & _
                "FROM com_doc_compra c " & _
                "JOIN com_doc_compra_detalle d ON c.id=d.id " & _
                "WHERE d.pro_codigo=" & LvProductos.ListItems(i) & " AND fecha<='" & Fql(DtHasta) & "' " & _
                "ORDER BY fecha DESC " & _
                "LIMIT 1"
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
            LvProductos.ListItems(i).SubItems(7) = "" & RsTmp!LaFecha
        End If
    Next
    FrmLoad.Visible = False
End Sub

Private Sub CmdValorizado_Click()
    Dim Lp_Total As Long
     DoEvents
    FrmLoad.Visible = True
    
    
    FraProgreso.Visible = True
    TxtTotal = 0
    Lp_Total = 0
    DoEvents
    Me.BarraProgreso.Min = 1
    Me.BarraProgreso.Max = LvProductos.ListItems.Count
    Sm_FiltroHasta = " AND kar_fecha <= '" & Format(DtHasta, "YYYY-MM-DD") & "' "
    For i = 1 To LvProductos.ListItems.Count
        BarraProgreso.Value = i
    '    Sql = "SELECT kar_nuevo_saldo,pro_precio_neto,(pro_precio_neto * kar_nuevo_saldo)/kar_nuevo_saldo,pro_precio_neto * kar_nuevo_saldo valor " & _
                "FROM inv_kardex tbl " & _
                "WHERE pro_codigo=" & LvProductos.ListItems(i) & " AND tbl.rut_emp='" & SP_Rut_Activo & "'  AND  kar_fecha = (SELECT Max(kar_fecha) FROM inv_kardex WHERE pro_codigo = tbl.pro_codigo) AND " & _
                    "kar_id = (SELECT Max(kar_id) FROM inv_kardex WHERE rut_emp='" & SP_Rut_Activo & "' AND pro_codigo = tbl.pro_codigo) " & Sm_FiltroHasta & _
                "GROUP BY pro_codigo " & _
                "ORDER BY kar_fecha DESC "
                
                
        Sql = "SELECT  kar_nuevo_saldo,    pro_precio_neto," & _
                    "(pro_precio_neto * kar_nuevo_saldo " & _
                    ")/ kar_nuevo_saldo, " & _
                    "pro_precio_neto * kar_nuevo_saldo valor " & _
                "FROM    inv_kardex tbl " & _
                "WHERE   pro_codigo = " & LvProductos.ListItems(i) & " AND tbl.rut_emp = '" & SP_Rut_Activo & "' " & Sm_FiltroHasta & " " & _
                "ORDER BY   kar_id DESC " & _
                "LIMIT 1"
    
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
            LvProductos.ListItems(i).SubItems(4) = "" & RsTmp!kar_nuevo_saldo
            LvProductos.ListItems(i).SubItems(5) = NumFormat(RsTmp!pro_precio_neto)
            LvProductos.ListItems(i).SubItems(6) = "" & NumFormat(RsTmp!Valor)
            Lp_Total = Lp_Total + CDbl(LvProductos.ListItems(i).SubItems(6))
        End If
    Next
    TxtTotal = NumFormat(Lp_Total)
    FrmLoad.Visible = False
    FraProgreso.Visible = False
End Sub

Private Sub Form_Load()
    Bm_CargoDatos = False
    Aplicar_skin Me
    DtHasta = Date
    Centrar Me
    Filtro = Empty
     LLenarCombo CboTipo, "tip_nombre", "tip_id", "par_tipos_productos", "tip_activo='SI' AND rut_emp='" & SP_Rut_Activo & "'"
    CboTipo.AddItem "TODOS"
    LLenarCombo CboMarca, "mar_nombre", "mar_id", "par_marcas", "mar_activo='SI' AND  rut_emp='" & SP_Rut_Activo & "'"
    CboMarca.AddItem "TODOS"
    Sm_FiltroHasta = " AND tbl.kar_fecha <= '" & Format(DtHasta, "YYYY-MM-DD") & "' "
    Sm_FiltroTipo = Empty
    Sm_FiltroMarca = Empty
    LLenarCombo CboBodega, "bod_nombre", "bod_id", "par_bodegas", "bod_activo='SI' AND rut_emp='" & SP_Rut_Activo & "'", "bod_id"
    CboBodega.AddItem "TODAS"
    CboBodega.ListIndex = CboBodega.ListCount - 1
    Ct_X = 1
End Sub
Private Sub CargaDatos()
    Dim Sp_FiltroBodega As String
   
    'Sql = "SELECT codigo,tip_nombre,m.mar_nombre,p.descripcion, " & _
          "(SELECT ABS(round(kar_nuevo_saldo_valor / kar_nuevo_saldo,2)) " & _
            "FROM inv_kardex i WHERE i.pro_codigo=p.codigo " & _
            "ORDER BY kar_id DESC LIMIT 1) costo_promedio, " & _
            "SUM(s.sto_stock) stock,SUM(s.sto_stock)* " & _
            "(SELECT ABS(round(kar_nuevo_saldo_valor / kar_nuevo_saldo,2)) " & _
            "FROM inv_kardex i WHERE i.pro_codigo=p.codigo " & _
            "ORDER BY kar_id DESC LIMIT 1) valorizado " & _
            "FROM maestro_productos p " & _
            "INNER JOIN pro_stock s ON p.codigo=s.pro_codigo " & _
            "INNER JOIN par_tipos_productos t USING(tip_id) " & _
            "INNER JOIN par_marcas m USING(mar_id) " & _
            "WHERE p.rut_emp='" & SP_Rut_Activo & "' " & Sm_FiltroMarca & Sm_FiltroTipo & " " & _
            "GROUP BY p.codigo"
    If CboBodega.Text = "TODAS" Then
        Sp_FiltroBodega = ""
        
   '     Sql = "SELECT codigo,tip_nombre,mar_nombre,descripcion, " & _
            "0,0,0 " & _
            "FROM maestro_productos p " & _
            "INNER JOIN par_marcas m USING(mar_id) " & _
            "INNER JOIN par_tipos_productos t USING(tip_id) " & _
            "WHERE pro_inventariable='SI' AND t.rut_emp='" & SP_Rut_Activo & "' AND m.rut_emp='" & SP_Rut_Activo & "' AND  p.rut_emp='" & SP_Rut_Activo & "' " & Sm_FiltroMarca & Sm_FiltroTipo & " " & _
            "ORDER BY p.descripcion"
    '   Sql = "SELECT codigo,tip_nombre,mar_nombre,descripcion, " & _
            "(SELECT kar_nuevo_saldo FROM inv_kardex i WHERE " & Sp_FiltroBodega & "  rut_emp='" & SP_Rut_Activo & "' AND  i.pro_codigo=p.codigo " & Sm_FiltroHasta & " ORDER BY kar_id DESC LIMIT 1) saldo, " & _
            " /*(SELECT ABS(round(kar_nuevo_saldo_valor / kar_nuevo_saldo,2)) FROM inv_kardex i WHERE " & Sp_FiltroBodega & "  rut_emp='" & SP_Rut_Activo & "' AND  i.pro_codigo=p.codigo " & Sm_FiltroHasta & " ORDER BY kar_id DESC LIMIT 1) costo_promedio, */ " & _
            "round((SELECT pro_precio_neto*    kar_nuevo_saldo  FROM inv_kardex i WHERE " & Sp_FiltroBodega & " rut_emp='" & SP_Rut_Activo & "' AND  i.pro_codigo=p.codigo " & Sm_FiltroHasta & " ORDER BY kar_id DESC LIMIT 1)/(SELECT kar_nuevo_saldo FROM inv_kardex i WHERE " & Sp_FiltroBodega & "  rut_emp='" & SP_Rut_Activo & "' AND  i.pro_codigo=p.codigo " & Sm_FiltroHasta & " ORDER BY kar_id DESC LIMIT 1),0) costo_promedio, " & _
            "(SELECT pro_precio_neto*    kar_nuevo_saldo  FROM inv_kardex i WHERE " & Sp_FiltroBodega & " rut_emp='" & SP_Rut_Activo & "' AND  i.pro_codigo=p.codigo " & Sm_FiltroHasta & " ORDER BY kar_id DESC LIMIT 1) valirzado " & _
            "FROM maestro_productos p " & _
            "INNER JOIN par_marcas m USING(mar_id) " & _
            "INNER JOIN par_tipos_productos t USING(tip_id) " & _
            "WHERE pro_inventariable='SI' AND t.rut_emp='" & SP_Rut_Activo & "' AND m.rut_emp='" & SP_Rut_Activo & "' AND  p.rut_emp='" & SP_Rut_Activo & "' " & Sm_FiltroMarca & Sm_FiltroTipo & " " & _
            "ORDER BY p.descripcion"
    
       Sql = "SELECT pro_codigo,tip_nombre, mar_nombre,descripcion,kar_nuevo_saldo,(pro_precio_neto * kar_nuevo_saldo)/kar_nuevo_saldo,pro_precio_neto * kar_nuevo_saldo " & _
                "FROM inv_kardex tbl " & _
                "JOIN maestro_productos p ON pro_codigo=p.codigo " & _
                "JOIN par_marcas m ON p.mar_id=m.mar_id " & _
                "JOIN par_tipos_productos t ON p.tip_id=t.tip_id " & _
                "WHERE pro_inventariable='SI' AND p.rut_emp='" & SP_Rut_Activo & "' " & Sm_FiltroMarca & Sm_FiltroTipo & " AND p.rut_emp='" & SP_Rut_Activo & "' AND  kar_fecha = (SELECT Max(kar_fecha) FROM inv_kardex WHERE pro_codigo = tbl.pro_codigo) AND " & _
                    "kar_id = (SELECT Max(kar_id) FROM inv_kardex WHERE rut_emp='" & SP_Rut_Activo & "' AND pro_codigo = tbl.pro_codigo) " & Sm_FiltroMarca & Sm_FiltroTipo & " " & Sm_FiltroHasta & _
                "GROUP BY pro_codigo " & _
                "ORDER BY kar_fecha DESC "
                
            
            
    Else
        Sp_FiltroBodega = " tbl.bod_id=" & CboBodega.ItemData(CboBodega.ListIndex) & " AND "
   
      '  Sql = "SELECT codigo,tip_nombre,mar_nombre,descripcion, " & _
            "(SELECT kar_nuevo_saldo FROM inv_kardex i WHERE " & Sp_FiltroBodega & "  rut_emp='" & SP_Rut_Activo & "' AND  i.pro_codigo=p.codigo " & Sm_FiltroHasta & " ORDER BY kar_id DESC LIMIT 1) saldo, " & _
             "round((SELECT pro_precio_neto*    kar_nuevo_saldo  FROM inv_kardex i WHERE " & Sp_FiltroBodega & " rut_emp='" & SP_Rut_Activo & "' AND  i.pro_codigo=p.codigo " & Sm_FiltroHasta & " ORDER BY kar_id DESC LIMIT 1)/(SELECT kar_nuevo_saldo FROM inv_kardex i WHERE " & Sp_FiltroBodega & "  rut_emp='" & SP_Rut_Activo & "' AND  i.pro_codigo=p.codigo " & Sm_FiltroHasta & " ORDER BY kar_id DESC LIMIT 1),0) costo_promedio, " & _
            "(SELECT pro_precio_neto*    kar_nuevo_saldo  FROM inv_kardex i WHERE " & Sp_FiltroBodega & " rut_emp='" & SP_Rut_Activo & "' AND  i.pro_codigo=p.codigo " & Sm_FiltroHasta & " ORDER BY kar_id DESC LIMIT 1) valirzado " & _
            "FROM maestro_productos p " & _
            "INNER JOIN par_marcas m USING(mar_id) " & _
            "INNER JOIN par_tipos_productos t USING(tip_id) " & _
            "WHERE pro_inventariable='SI' AND t.rut_emp='" & SP_Rut_Activo & "' AND m.rut_emp='" & SP_Rut_Activo & "' AND  p.rut_emp='" & SP_Rut_Activo & "' " & Sm_FiltroMarca & Sm_FiltroTipo & " " & _
            "ORDER BY p.descripcion"
    
        '18 Abril 2015
       Sql = "SELECT pro_codigo,tip_nombre, mar_nombre,descripcion,kar_nuevo_saldo,(pro_precio_neto * kar_nuevo_saldo)/kar_nuevo_saldo,pro_precio_neto * kar_nuevo_saldo " & _
                "FROM inv_kardex tbl " & _
                "JOIN maestro_productos p ON pro_codigo=p.codigo " & _
                "JOIN par_marcas m ON p.mar_id=m.mar_id " & _
                "JOIN par_tipos_productos t ON p.tip_id=t.tip_id " & _
                "WHERE  pro_inventariable='SI' AND  " & Sp_FiltroBodega & " p.rut_emp='" & SP_Rut_Activo & "' " & Sm_FiltroMarca & Sm_FiltroTipo & " AND p.rut_emp='" & SP_Rut_Activo & "' AND  kar_fecha = (SELECT Max(kar_fecha) FROM inv_kardex WHERE pro_codigo = tbl.pro_codigo) AND " & _
                    "kar_id = (SELECT Max(kar_id) FROM inv_kardex WHERE rut_emp='" & SP_Rut_Activo & "' AND pro_codigo = tbl.pro_codigo) " & Sm_FiltroMarca & Sm_FiltroTipo & " " & Sm_FiltroHasta & " " & _
                "GROUP BY pro_codigo " & _
                "ORDER BY kar_fecha DESC "
        
        
     End If
    pbarCarga.Visible = True
    DoEvents
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvProductos, False, True, True, False
    If CboBodega.Text = "TODAS" Then
        Dim Lp_Csaldo As Double
        Dim Lp_CostoPromedio As Double
        Dim Lp_Valorizado As Double
        Lp_Saldo = 0
        For X = 1 To LvProductos.ListItems.Count
            For i = 1 To CboBodega.ListCount - 1
                'Columna Saldo
                Sp_FiltroBodega = "i.bod_id=" & CboBodega.ItemData(i - 1) & " AND "
                Sql = "SELECT kar_nuevo_saldo FROM inv_kardex i " & _
                        "WHERE " & Sp_FiltroBodega & "  rut_emp='" & SP_Rut_Activo & "' AND  i.pro_codigo='" & LvProductos.ListItems(X) & "' " & Sm_FiltroHasta & _
                        " ORDER BY kar_id DESC " & _
                        "LIMIT 1"
                Consulta RsTmp, Sql
                If RsTmp.RecordCount > 0 Then
                    Lp_Saldo = Lp_Saldo + RsTmp!kar_nuevo_saldo
                End If
               
                
                'Columna Valorizado
                Sql = "SELECT pro_precio_neto*    kar_nuevo_saldo  valorizado " & _
                        "FROM inv_kardex i " & _
                        "WHERE " & Sp_FiltroBodega & " rut_emp='" & SP_Rut_Activo & "' AND  i.pro_codigo='" & LvProductos.ListItems(X) & "' " & Sm_FiltroHasta & _
                        " ORDER BY kar_id DESC " & _
                        "LIMIT 1"
                'Sql =
                Consulta RsTmp, Sql
                If RsTmp.RecordCount > 0 Then
                    Lp_Valorizado = Lp_Valorizado + RsTmp!valorizado
                    
                End If
            Next
            LvProductos.ListItems(X).SubItems(4) = NumFormat(Lp_Saldo)
            If Lp_Saldo <> 0 Then
                LvProductos.ListItems(X).SubItems(5) = NumFormat(Lp_Valorizado / Lp_Saldo)
                  LvProductos.ListItems(X).SubItems(6) = NumFormat(Lp_Valorizado)
            Else
                LvProductos.ListItems(X).SubItems(6) = 0
                LvProductos.ListItems(X).SubItems(5) = NumFormat(Lp_Valorizado)
            End If
          
            Lp_Saldo = 0
            Lp_CostoPromedio = 0
            Lp_Valorizado = 0
        Next

    End If
    
    TxtTotal = NumFormat(TotalizaColumna(LvProductos, "total"))
   If 1 = 2 Then
        If LvProductos.ListItems.Count > 0 Then
            LvProductos.ListItems.Add , , ""
            LvProductos.ListItems(LvProductos.ListItems.Count).SubItems(3) = "TOTAL VALORIZADO"
            LvProductos.ListItems(LvProductos.ListItems.Count).SubItems(6) = TxtTotal
            LvProductos.ListItems(LvProductos.ListItems.Count).ListSubItems(6).ForeColor = vbWhite
            LvProductos.ListItems(LvProductos.ListItems.Count).ListSubItems(3).Bold = True
        End If
    End If
    Bm_CargoDatos = True
    pbarCarga.Visible = False
End Sub

Private Sub LvProductos_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
  ordListView ColumnHeader, Me, LvProductos
End Sub

Private Sub Timer1_Timer()
    LvProductos.SetFocus
    Timer1.Enabled = False
End Sub

Private Sub Timer2_Timer()
    Ct_X = Ct_X + 2
    pbarCarga.Value = Ct_X
    If Ct_X > 97 Then
        Ct_X = 1
    End If
    
End Sub
