VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form inv_DescuentosPorVolumen 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Descuentos por Volumen"
   ClientHeight    =   7710
   ClientLeft      =   4605
   ClientTop       =   2190
   ClientWidth     =   8790
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7710
   ScaleWidth      =   8790
   ShowInTaskbar   =   0   'False
   Begin VB.Timer Timer1 
      Interval        =   20
      Left            =   30
      Top             =   0
   End
   Begin VB.CommandButton CmdSalir 
      Cancel          =   -1  'True
      Caption         =   "&Retornar"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   6390
      TabIndex        =   1
      Top             =   7080
      Width           =   1215
   End
   Begin VB.Frame FrmProducto 
      Caption         =   "Matriz de descuentos por volumen"
      Height          =   6585
      Left            =   405
      TabIndex        =   0
      Top             =   330
      Width           =   7740
      Begin TabDlg.SSTab SstTipo 
         Height          =   5745
         Left            =   405
         TabIndex        =   3
         Top             =   645
         Width           =   6780
         _ExtentX        =   11959
         _ExtentY        =   10134
         _Version        =   393216
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   520
         TabCaption(0)   =   "Individual"
         TabPicture(0)   =   "inv_DescuentosPorVolumen.frx":0000
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "SkinLabel1(6)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "SkinLabel1(5)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "SkinLabel1(3)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "LvDetalle"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "SkinLabel1(4)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "SkinLabel1(2)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "SkinLabel1(1)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "SkinLabel1(0)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "TxtPrecioU"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "TxtCodBarra"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "TxtCodSistema"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "TxtNombre"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "CmdOk"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "TxtPU"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).Control(14)=   "TxtHasta"
         Tab(0).Control(14).Enabled=   0   'False
         Tab(0).Control(15)=   "TxtDesde"
         Tab(0).Control(15).Enabled=   0   'False
         Tab(0).ControlCount=   16
         TabCaption(1)   =   "Descuento Por Tipo"
         TabPicture(1)   =   "inv_DescuentosPorVolumen.frx":001C
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "SkinLabel1(10)"
         Tab(1).Control(1)=   "SkinLabel1(8)"
         Tab(1).Control(2)=   "CboTipo"
         Tab(1).Control(3)=   "TxtTipoDesde"
         Tab(1).Control(4)=   "TxtTipoDscto"
         Tab(1).Control(5)=   "CmdTipoAplicar"
         Tab(1).ControlCount=   6
         Begin VB.CommandButton CmdTipoAplicar 
            Caption         =   "Aplicar"
            Height          =   570
            Left            =   -73275
            TabIndex        =   23
            Top             =   2400
            Width           =   3150
         End
         Begin VB.TextBox TxtTipoDscto 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFF80&
            Height          =   285
            Left            =   -71640
            TabIndex        =   22
            Top             =   1965
            Width           =   1935
         End
         Begin VB.TextBox TxtTipoDesde 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFF80&
            Height          =   285
            Left            =   -73755
            TabIndex        =   21
            Top             =   1965
            Width           =   1920
         End
         Begin VB.ComboBox CboTipo 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            ItemData        =   "inv_DescuentosPorVolumen.frx":0038
            Left            =   -74040
            List            =   "inv_DescuentosPorVolumen.frx":003A
            Style           =   2  'Dropdown List
            TabIndex        =   20
            Top             =   1035
            Width           =   4290
         End
         Begin VB.TextBox TxtDesde 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFF80&
            Height          =   285
            Left            =   75
            TabIndex        =   12
            Top             =   2055
            Width           =   1920
         End
         Begin VB.TextBox TxtHasta 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFF80&
            Height          =   285
            Left            =   2010
            TabIndex        =   13
            Top             =   2055
            Width           =   1935
         End
         Begin VB.TextBox TxtPU 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFF80&
            Height          =   285
            Left            =   3960
            TabIndex        =   14
            Top             =   2055
            Width           =   1830
         End
         Begin VB.CommandButton CmdOk 
            Caption         =   "Ok"
            Height          =   270
            Left            =   5820
            TabIndex        =   16
            Top             =   2055
            Width           =   705
         End
         Begin VB.TextBox TxtNombre 
            BackColor       =   &H0080C0FF&
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   135
            Locked          =   -1  'True
            TabIndex        =   7
            Top             =   780
            Width           =   6285
         End
         Begin VB.TextBox TxtCodSistema 
            BackColor       =   &H0080C0FF&
            Height          =   285
            Left            =   1080
            Locked          =   -1  'True
            TabIndex        =   6
            Top             =   1155
            Width           =   1020
         End
         Begin VB.TextBox TxtCodBarra 
            BackColor       =   &H0080C0FF&
            Height          =   285
            Left            =   3225
            Locked          =   -1  'True
            TabIndex        =   5
            Top             =   1155
            Width           =   1515
         End
         Begin VB.TextBox TxtPrecioU 
            Alignment       =   1  'Right Justify
            BackColor       =   &H0080C0FF&
            Height          =   300
            Left            =   5505
            Locked          =   -1  'True
            TabIndex        =   4
            Top             =   1155
            Width           =   915
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
            Height          =   315
            Index           =   0
            Left            =   120
            OleObjectBlob   =   "inv_DescuentosPorVolumen.frx":003C
            TabIndex        =   8
            Top             =   1185
            Width           =   1005
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
            Height          =   315
            Index           =   1
            Left            =   2175
            OleObjectBlob   =   "inv_DescuentosPorVolumen.frx":00B2
            TabIndex        =   9
            Top             =   1185
            Width           =   1080
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
            Height          =   315
            Index           =   2
            Left            =   135
            OleObjectBlob   =   "inv_DescuentosPorVolumen.frx":012A
            TabIndex        =   10
            Top             =   555
            Width           =   1860
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
            Height          =   240
            Index           =   4
            Left            =   4785
            OleObjectBlob   =   "inv_DescuentosPorVolumen.frx":01AE
            TabIndex        =   11
            Top             =   1200
            Width           =   1080
         End
         Begin MSComctlLib.ListView LvDetalle 
            Height          =   2895
            Left            =   60
            TabIndex        =   15
            Top             =   2385
            Width           =   6510
            _ExtentX        =   11483
            _ExtentY        =   5106
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            FullRowSelect   =   -1  'True
            GridLines       =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   0
            NumItems        =   4
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Text            =   "Id"
               Object.Width           =   0
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Object.Tag             =   "N103"
               Text            =   "Desde"
               Object.Width           =   3387
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   2
               Object.Tag             =   "N103"
               Text            =   "Hasta"
               Object.Width           =   3387
            EndProperty
            BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   3
               Object.Tag             =   "N102"
               Text            =   "Precio Unitario"
               Object.Width           =   3228
            EndProperty
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
            Height          =   180
            Index           =   3
            Left            =   420
            OleObjectBlob   =   "inv_DescuentosPorVolumen.frx":021E
            TabIndex        =   17
            Top             =   1845
            Width           =   1545
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
            Height          =   315
            Index           =   5
            Left            =   1950
            OleObjectBlob   =   "inv_DescuentosPorVolumen.frx":0292
            TabIndex        =   18
            Top             =   1860
            Width           =   1965
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
            Height          =   180
            Index           =   6
            Left            =   4170
            OleObjectBlob   =   "inv_DescuentosPorVolumen.frx":0306
            TabIndex        =   19
            Top             =   1875
            Width           =   1560
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
            Height          =   180
            Index           =   8
            Left            =   -73410
            OleObjectBlob   =   "inv_DescuentosPorVolumen.frx":0382
            TabIndex        =   24
            Top             =   1755
            Width           =   1545
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
            Height          =   315
            Index           =   10
            Left            =   -71700
            OleObjectBlob   =   "inv_DescuentosPorVolumen.frx":03F6
            TabIndex        =   25
            Top             =   1770
            Width           =   1965
         End
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   315
         Index           =   7
         Left            =   -180
         OleObjectBlob   =   "inv_DescuentosPorVolumen.frx":046A
         TabIndex        =   2
         Top             =   9390
         Width           =   2925
      End
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   0
      OleObjectBlob   =   "inv_DescuentosPorVolumen.frx":0506
      Top             =   2040
   End
End
Attribute VB_Name = "inv_DescuentosPorVolumen"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub CboTipo_Click()
    TxtTipoDesde = 0
    TxtTipoDscto = 0
    If CboTipo.ListIndex > -1 Then
        Sql = "SELECT dti_desde, dti_descuento " & _
                "FROM par_descuento_por_tipo " & _
                "WHERE tip_id=" & CboTipo.ItemData(CboTipo.ListIndex)
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
            TxtTipoDesde = RsTmp!dti_desde
            TxtTipoDscto = RsTmp!dti_descuento
        End If
                
        
    
    
    
    End If
End Sub

Private Sub CmdOk_Click()
    If Val(TxtDesde) = 0 Or Val(TxtHasta) = 0 Or Val(TxtPU) = 0 Then
        MsgBox "Valors deben ser mayor a 0...", vbInformation
        TxtDesde.SetFocus
        Exit Sub
    End If
    
    Sql = "INSERT INTO par_productos_matriz_descuento (rut_emp,pro_codigo,mpr_desde,mpr_hasta,mpr_precio_unitario) " & _
    "VALUES('" & SP_Rut_Activo & "'," & TxtCodSistema & "," & CxP(Val(TxtDesde)) & "," & CxP(Val(TxtHasta)) & "," & CxP(Val(TxtPU)) & ")"
    cn.Execute Sql
    CargaMatriz
    
End Sub
Public Sub CargaMatriz()
    Sql = "SELECT mpr_id,mpr_desde,mpr_hasta,mpr_precio_unitario " & _
            "FROM par_productos_matriz_descuento " & _
            "WHERE rut_emp='" & SP_Rut_Activo & "' AND pro_codigo=" & TxtCodSistema & " " & _
            "ORDER BY mpr_desde"
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvDetalle, False, True, True, False
    On Error Resume Next
    TxtDesde.SetFocus
            
End Sub


Private Sub cmdSalir_Click()
    Unload Me
End Sub

Private Sub CmdTipoAplicar_Click()
    If CboTipo.ListIndex = -1 Then
        MsgBox "Debe seleccionar tipo antes de continuar...", vbInformation
        CboTipo.SetFocus
        Exit Sub
    End If
    
    If Val(TxtTipoDesde) = 0 Then
        MsgBox "Debe ingresar cantidad desde ...", vbInformation
        TxtTipoDesde.SetFocus
        Exit Sub
    End If
    If Val(TxtTipoDscto) = 0 Then
        MsgBox "Debe ingresar % descuento...", vbInformation
        TxtTipoDscto.SetFocus
        Exit Sub
    End If
    
    'Ya validamos, ahora a registrar el descuento
    '
    Sql = "DELETE FROM par_descuento_por_tipo " & _
            "WHERE tip_id=" & CboTipo.ItemData(CboTipo.ListIndex)
    cn.Execute Sql
    Sql = "INSERT INTO par_descuento_por_tipo (tip_id,dti_desde,dti_descuento) " & _
            "VALUES(" & CboTipo.ItemData(CboTipo.ListIndex) & "," & Val(TxtTipoDesde) & "," & Val(TxtTipoDscto) & ")"
    cn.Execute Sql
    
    MsgBox "Valores guardados...", vbInformation
    
    

End Sub

Private Sub Form_Load()
    Aplicar_skin Me
    Centrar Me, False
    LLenarCombo CboTipo, "tip_nombre", "tip_id", "par_tipos_productos", "tip_activo='SI' AND  rut_emp='" & SP_Rut_Activo & "'", "tip_nombre"
End Sub


Private Sub LvDetalle_DblClick()
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    TxtDesde = CxP(LvDetalle.SelectedItem.SubItems(1))
    TxtHasta = CxP(LvDetalle.SelectedItem.SubItems(2))
    TxtPU = CxP(LvDetalle.SelectedItem.SubItems(3))
    
    cn.Execute "DELETE FROM par_productos_matriz_descuento " & _
                "WHERE mpr_id=" & LvDetalle.SelectedItem
    
    Me.CargaMatriz
    TxtDesde.SetFocus
End Sub



Private Sub Timer1_Timer()
    TxtDesde.SetFocus
    Timer1.Enabled = False
End Sub

Private Sub TxtDesde_GotFocus()
    En_Foco TxtDesde
End Sub

Private Sub TxtDesde_KeyPress(KeyAscii As Integer)
    KeyAscii = AceptaSoloNumeros(KeyAscii)
End Sub

Private Sub TxtDesde_Validate(Cancel As Boolean)
    If Val(TxtDesde) = 0 Then
        TxtDesde = "0"
    End If
End Sub
Private Sub TxtHasta_GotFocus()
    En_Foco TxtHasta
End Sub

Private Sub TxtHasta_KeyPress(KeyAscii As Integer)
    KeyAscii = AceptaSoloNumeros(KeyAscii)
End Sub

Private Sub TxtHasta_Validate(Cancel As Boolean)
    If Val(TxtHasta) = 0 Then TxtHasta = 0
End Sub
Private Sub TxtPU_GotFocus()
    En_Foco TxtPU
End Sub

Private Sub TxtPU_KeyPress(KeyAscii As Integer)
    KeyAscii = AceptaSoloNumeros(KeyAscii)
End Sub

Private Sub TxtPU_Validate(Cancel As Boolean)
    If Val(TxtPU) = 0 Then TxtPU = 0
End Sub
Private Sub TxtTipoDesde_GotFocus()
    En_Foco TxtTipoDesde
End Sub

Private Sub TxtTipoDesde_KeyPress(KeyAscii As Integer)
    KeyAscii = AceptaSoloNumeros(KeyAscii)
End Sub

Private Sub TxtTipoDesde_Validate(Cancel As Boolean)
    If Val(TxtTipoDesde) = 0 Then TxtTipoDesde = "0"
End Sub
Private Sub TxtTipoDscto_GotFocus()
    En_Foco TxtTipoDscto
End Sub

Private Sub TxtTipoDscto_KeyPress(KeyAscii As Integer)
    KeyAscii = AceptaSoloNumeros(KeyAscii)
End Sub

Private Sub TxtTipoDscto_Validate(Cancel As Boolean)
    If Val(TxtTipoDscto) = 0 Then TxtTipoDscto = 0
End Sub
