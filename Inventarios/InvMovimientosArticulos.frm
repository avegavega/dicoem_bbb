VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{DA729E34-689F-49EA-A856-B57046630B73}#1.0#0"; "progressbar-xp.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form InvMovimientosArticulos 
   Caption         =   "Movimientos de Art�culos"
   ClientHeight    =   7830
   ClientLeft      =   5490
   ClientTop       =   1815
   ClientWidth     =   10455
   BeginProperty Font 
      Name            =   "MS Sans Serif"
      Size            =   9.75
      Charset         =   0
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   ScaleHeight     =   7830
   ScaleWidth      =   10455
   Begin VB.Frame FraProgreso 
      Caption         =   "Progreso exportaci�n"
      Height          =   795
      Left            =   255
      TabIndex        =   27
      Top             =   3975
      Visible         =   0   'False
      Width           =   10080
      Begin Proyecto2.XP_ProgressBar BarraProgreso 
         Height          =   330
         Left            =   270
         TabIndex        =   28
         Top             =   375
         Width           =   9570
         _ExtentX        =   16880
         _ExtentY        =   582
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BrushStyle      =   0
         Color           =   16777088
         Scrolling       =   1
         ShowText        =   -1  'True
      End
   End
   Begin VB.CommandButton CmdGuardar 
      Caption         =   "&Guardar"
      Height          =   465
      Left            =   8340
      TabIndex        =   50
      Top             =   6525
      Width           =   1485
   End
   Begin MSComctlLib.ListView LvDetalle 
      Height          =   2730
      Left            =   1035
      TabIndex        =   49
      Top             =   3615
      Width           =   8730
      _ExtentX        =   15399
      _ExtentY        =   4815
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   4
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Object.Tag             =   "T1000"
         Text            =   "Codigo"
         Object.Width           =   2337
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Object.Tag             =   "T3000"
         Text            =   "Nombre"
         Object.Width           =   10583
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   2
         Object.Tag             =   "N102"
         Text            =   "Cantidad"
         Object.Width           =   1773
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Object.Tag             =   "T1000"
         Text            =   "Mantiene Stock"
         Object.Width           =   0
      EndProperty
   End
   Begin MSComDlg.CommonDialog Dialogo 
      Left            =   495
      Top             =   7680
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.Frame FrameCliente 
      Caption         =   "Datos del cliente"
      Height          =   1230
      Left            =   465
      TabIndex        =   29
      Top             =   8325
      Width           =   12810
      Begin VB.TextBox TxtDscto 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   10440
         TabIndex        =   37
         Top             =   495
         Width           =   615
      End
      Begin VB.TextBox TxtGiro 
         Height          =   285
         Left            =   7680
         Locked          =   -1  'True
         TabIndex        =   36
         TabStop         =   0   'False
         Top             =   210
         Width           =   1935
      End
      Begin VB.TextBox TxtCiudad 
         Height          =   285
         Left            =   10440
         Locked          =   -1  'True
         TabIndex        =   35
         TabStop         =   0   'False
         Text            =   " "
         Top             =   210
         Width           =   2205
      End
      Begin VB.TextBox TxtComuna 
         Height          =   285
         Left            =   7680
         Locked          =   -1  'True
         TabIndex        =   34
         TabStop         =   0   'False
         Text            =   " "
         Top             =   495
         Width           =   1935
      End
      Begin VB.TextBox TxtDireccion 
         Height          =   285
         Left            =   3960
         Locked          =   -1  'True
         TabIndex        =   33
         TabStop         =   0   'False
         Top             =   510
         Width           =   2775
      End
      Begin VB.TextBox TxtRazonSocial 
         BackColor       =   &H00E0E0E0&
         Height          =   285
         Left            =   3960
         Locked          =   -1  'True
         TabIndex        =   32
         TabStop         =   0   'False
         Top             =   210
         Width           =   2775
      End
      Begin VB.TextBox TxtRut 
         BackColor       =   &H0080FF80&
         Height          =   285
         Left            =   630
         TabIndex        =   31
         ToolTipText     =   "Ingrese el RUT sin puntos ni guion."
         Top             =   225
         Width           =   1575
      End
      Begin VB.TextBox TxtListaPrecio 
         BackColor       =   &H00E0E0E0&
         Height          =   285
         Left            =   135
         Locked          =   -1  'True
         TabIndex        =   30
         TabStop         =   0   'False
         Top             =   795
         Width           =   2775
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   255
         Index           =   4
         Left            =   2370
         OleObjectBlob   =   "InvMovimientosArticulos.frx":0000
         TabIndex        =   38
         Top             =   540
         Width           =   1605
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   255
         Index           =   0
         Left            =   120
         OleObjectBlob   =   "InvMovimientosArticulos.frx":0072
         TabIndex        =   39
         Top             =   240
         Width           =   495
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel16 
         Height          =   255
         Left            =   2235
         OleObjectBlob   =   "InvMovimientosArticulos.frx":00D6
         TabIndex        =   40
         Top             =   240
         Width           =   1695
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel18 
         Height          =   255
         Left            =   6795
         OleObjectBlob   =   "InvMovimientosArticulos.frx":0156
         TabIndex        =   41
         Top             =   240
         Width           =   855
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel19 
         Height          =   255
         Left            =   6780
         OleObjectBlob   =   "InvMovimientosArticulos.frx":01BC
         TabIndex        =   42
         Top             =   570
         Width           =   855
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel20 
         Height          =   255
         Left            =   9810
         OleObjectBlob   =   "InvMovimientosArticulos.frx":0226
         TabIndex        =   43
         Top             =   240
         Width           =   735
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel21 
         Height          =   255
         Left            =   9855
         OleObjectBlob   =   "InvMovimientosArticulos.frx":0290
         TabIndex        =   44
         Top             =   540
         Width           =   1395
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Movimientos entre bodegas"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1215
      Left            =   495
      TabIndex        =   16
      Top             =   240
      Width           =   9570
      Begin VB.ComboBox CboGuia 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         ItemData        =   "InvMovimientosArticulos.frx":031A
         Left            =   1440
         List            =   "InvMovimientosArticulos.frx":031C
         Style           =   2  'Dropdown List
         TabIndex        =   46
         ToolTipText     =   "Seleccione Documento de  venta. Ventas con Retenci�n, cuando el contribuyente recibe factura de terceros "
         Top             =   585
         Width           =   2565
      End
      Begin VB.TextBox TxtNroGuia 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   4005
         TabIndex        =   45
         ToolTipText     =   "Nro de Documento"
         Top             =   585
         Width           =   1320
      End
      Begin VB.TextBox TxtNro 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   7620
         Locked          =   -1  'True
         TabIndex        =   26
         TabStop         =   0   'False
         Text            =   "(automatico)"
         Top             =   570
         Width           =   1800
      End
      Begin VB.TextBox TxtUsuario 
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   5520
         Locked          =   -1  'True
         TabIndex        =   19
         TabStop         =   0   'False
         Top             =   570
         Width           =   2085
      End
      Begin MSComCtl2.DTPicker DtFecha 
         Height          =   330
         Left            =   135
         TabIndex        =   0
         Top             =   585
         Width           =   1305
         _ExtentX        =   2302
         _ExtentY        =   582
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   91815937
         CurrentDate     =   41104
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel6 
         Height          =   210
         Left            =   135
         OleObjectBlob   =   "InvMovimientosArticulos.frx":031E
         TabIndex        =   17
         Top             =   405
         Width           =   1305
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel7 
         Height          =   210
         Left            =   6045
         OleObjectBlob   =   "InvMovimientosArticulos.frx":0386
         TabIndex        =   18
         Top             =   345
         Width           =   1305
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   210
         Left            =   7140
         OleObjectBlob   =   "InvMovimientosArticulos.frx":03F2
         TabIndex        =   25
         Top             =   390
         Width           =   1905
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
         Height          =   210
         Index           =   2
         Left            =   4560
         OleObjectBlob   =   "InvMovimientosArticulos.frx":0474
         TabIndex        =   47
         Top             =   390
         Width           =   735
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
         Height          =   210
         Index           =   3
         Left            =   1455
         OleObjectBlob   =   "InvMovimientosArticulos.frx":04DE
         TabIndex        =   48
         Top             =   405
         Width           =   990
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Bodegas"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1005
      Left            =   495
      TabIndex        =   13
      Top             =   1545
      Width           =   9570
      Begin VB.ComboBox CboDestino 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         ItemData        =   "InvMovimientosArticulos.frx":054E
         Left            =   5010
         List            =   "InvMovimientosArticulos.frx":0550
         Style           =   2  'Dropdown List
         TabIndex        =   2
         Top             =   540
         Width           =   4230
      End
      Begin VB.ComboBox CboOrigen 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         ItemData        =   "InvMovimientosArticulos.frx":0552
         Left            =   495
         List            =   "InvMovimientosArticulos.frx":0554
         Style           =   2  'Dropdown List
         TabIndex        =   1
         Top             =   540
         Width           =   4530
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   255
         Index           =   1
         Left            =   480
         OleObjectBlob   =   "InvMovimientosArticulos.frx":0556
         TabIndex        =   14
         Top             =   360
         Width           =   2580
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
         Height          =   255
         Index           =   0
         Left            =   5025
         OleObjectBlob   =   "InvMovimientosArticulos.frx":05C0
         TabIndex        =   15
         Top             =   315
         Width           =   2580
      End
   End
   Begin VB.Timer Timer1 
      Interval        =   20
      Left            =   -30
      Top             =   7260
   End
   Begin VB.CommandButton cmdSalir 
      Cancel          =   -1  'True
      Caption         =   "Esc - &Retornar"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   465
      Left            =   9015
      TabIndex        =   12
      Top             =   7260
      Width           =   1185
   End
   Begin VB.Frame FrmMantenedor 
      Caption         =   "Art�culos"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4455
      Left            =   540
      TabIndex        =   8
      Top             =   2670
      Width           =   9510
      Begin VB.TextBox TxtObs 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   435
         Left            =   555
         TabIndex        =   7
         Top             =   3915
         Width           =   7260
      End
      Begin VB.TextBox TxtCantidad 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   7875
         TabIndex        =   4
         Top             =   585
         Width           =   1005
      End
      Begin VB.TextBox TxtStock 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   5625
         Locked          =   -1  'True
         TabIndex        =   21
         ToolTipText     =   "Stock Actual"
         Top             =   150
         Visible         =   0   'False
         Width           =   2205
      End
      Begin VB.CommandButton CmdBuscaProducto 
         Caption         =   "F1"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   1590
         TabIndex        =   20
         ToolTipText     =   "Busqueda de productos"
         Top             =   615
         Width           =   300
      End
      Begin VB.TextBox TxtCodigo 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   510
         TabIndex        =   3
         Top             =   600
         Width           =   1095
      End
      Begin VB.ComboBox CboActivo 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         ItemData        =   "InvMovimientosArticulos.frx":062C
         Left            =   8370
         List            =   "InvMovimientosArticulos.frx":0633
         Style           =   2  'Dropdown List
         TabIndex        =   5
         ToolTipText     =   $"InvMovimientosArticulos.frx":063B
         Top             =   45
         Visible         =   0   'False
         Width           =   1605
      End
      Begin VB.TextBox TxtNombre 
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1875
         Locked          =   -1  'True
         TabIndex        =   9
         TabStop         =   0   'False
         Tag             =   "T"
         Top             =   585
         Width           =   6000
      End
      Begin VB.CommandButton CmdOk 
         Caption         =   "Ok"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   8880
         TabIndex        =   6
         Top             =   585
         Width           =   330
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   255
         Index           =   3
         Left            =   1875
         OleObjectBlob   =   "InvMovimientosArticulos.frx":06D8
         TabIndex        =   10
         Top             =   375
         Width           =   1095
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel5 
         Height          =   255
         Index           =   2
         Left            =   8040
         OleObjectBlob   =   "InvMovimientosArticulos.frx":0742
         TabIndex        =   11
         Top             =   30
         Visible         =   0   'False
         Width           =   1485
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   255
         Index           =   0
         Left            =   525
         OleObjectBlob   =   "InvMovimientosArticulos.frx":07BC
         TabIndex        =   22
         Top             =   360
         Width           =   1095
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   255
         Index           =   1
         Left            =   7845
         OleObjectBlob   =   "InvMovimientosArticulos.frx":0826
         TabIndex        =   23
         Top             =   360
         Width           =   1095
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   255
         Index           =   2
         Left            =   555
         OleObjectBlob   =   "InvMovimientosArticulos.frx":0894
         TabIndex        =   24
         Top             =   3705
         Width           =   3765
      End
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   60
      OleObjectBlob   =   "InvMovimientosArticulos.frx":0926
      Top             =   6015
   End
End
Attribute VB_Name = "InvMovimientosArticulos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub CboActivo_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 13 Then Me.CmdOk.SetFocus
End Sub


Private Sub CboGuia_Click()
    
    If CboGuia.ListIndex = -1 Then Exit Sub
    TxtNroGuia = AutoIncremento("LEE", CboGuia.ItemData(CboGuia.ListIndex), , IG_id_Sucursal_Empresa)
End Sub

Private Sub CmdBuscaProducto_Click()
    SG_codigo = Empty
    BuscaProducto.Show 1
    If Len(SG_codigo) = 0 Then Exit Sub
    TxtCodigo = SG_codigo
    TxtCodigo_Validate True
End Sub

Private Sub CmdGuardar_Click()
    If CboOrigen.ListIndex = -1 Then
        MsgBox "Falta Origen...", vbInformation
        CboOrigen.SetFocus
        Exit Sub
    End If
    If CboDestino.ListIndex = -1 Then
        MsgBox "Falta Destino...", vbInformation
        CboDestino.SetFocus
        Exit Sub
    End If
    If LvDetalle.ListItems.Count = 0 Then
        MsgBox "Faltan productos...", vbInformation
        TxtCodigo.SetFocus
        Exit Sub
    End If
    If Len(TxtObs) = 0 Then
        MsgBox "Falta observacion...", vbInformation
        TxtObs.SetFocus
        Exit Sub
    End If
    If CboDestino.ItemData(CboDestino.ListIndex) = CboOrigen.ItemData(CboOrigen.ListIndex) Then
        MsgBox "Bodegas deben ser distintas...", vbInformation
        CboDestino.SetFocus
        Exit Sub
    End If
    
      If CboGuia.ListIndex = -1 Then
        MsgBox "Seleccione guia...", vbInformation
        CboGuia.SetFocus
        Exit Sub
    End If
    If Val(TxtNroGuia) = 0 Then
        MsgBox "Falta nro de guia"
        TxtNroGuia.SetFocus
        Exit Sub
    End If
    
    If DG_ID_Unico > 0 Then GoTo porAquiSeImprime
    
    'Tambien debemos revisar que el correlativo
    'de la guia no este ocupado
    Sql = "SELECT id " & _
            "FROM ven_doc_venta " & _
            "WHERE no_documento=" & TxtNroGuia & " " & _
            "AND doc_id=" & CboGuia.ItemData(CboGuia.ListIndex)
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        MsgBox "Este documento ya ha sido utilizado...", vbInformation
        TxtNroGuia.SetFocus
        Exit Sub
    End If
    
    
    
    
    
    'Teniendo las validaciones procedemos a grabar el registro 18 Julio 2012
    CmdGuardar.Enabled = False
    On Error GoTo errorGraba
    cn.BeginTrans
        AutoIncremento "GUARDA", CboGuia.ItemData(CboGuia.ListIndex), TxtNroGuia, IG_id_Sucursal_Empresa
        Sql = "INSERT INTO ven_doc_venta (doc_id,no_documento,fecha,rut_emp,rut_cliente,usu_nombre) " & _
                "VALUES(" & CboGuia.ItemData(CboGuia.ListIndex) & "," & Val(TxtNroGuia) & ",'" & Fql(DtFecha) & "','" & SP_Rut_Activo & "','" & TxtRut & "','" & LogUsuario & "')"
        cn.Execute Sql
        Sql = "INSERT INTO ven_detalle (doc_id,no_documento,codigo,unidades,rut_emp) VALUES"
        sql2 = ""
        For i = 1 To LvDetalle.ListItems.Count
            sql2 = sql2 & "(" & CboGuia.ItemData(CboGuia.ListIndex) & "," & TxtNroGuia & "," & LvDetalle.ListItems(i) & "," & CDbl(LvDetalle.ListItems(i).SubItems(2)) & ",'" & SP_Rut_Activo & "'),"
        
        Next
        sql2 = Mid(sql2, 1, Len(sql2) - 1)
        cn.Execute Sql & sql2
    
    
    
    
        txtNro = UltimoNro("inv_movimientos_bodegas", "mvp_id")
        Sql = "INSERT INTO inv_movimientos_bodegas (mvp_id,mvp_fecha,mvp_usuario,mvp_obs,mvp_id_origen,mvp_id_destino,rut_emp,doc_id,no_documento) VALUES( " & _
            txtNro & ",'" & Fql(DtFecha) & "','" & txtUsuario & "','" & TxtObs & "'," & Me.CboOrigen.ItemData(CboOrigen.ListIndex) & "," & CboDestino.ItemData(CboDestino.ListIndex) & ",'" & SP_Rut_Activo & "'," & CboGuia.ItemData(CboGuia.ListIndex) & "," & TxtNroGuia & ")"
    
        sql2 = "INSERT INTO inv_movimientos_bodega_detalle(mvp_id,pro_codigo,mpd_cantidad,mpd_mantiene_stock) VALUES"
        For i = 1 To LvDetalle.ListItems.Count
            sql2 = sql2 & "(" & txtNro & ",'" & LvDetalle.ListItems(i) & "'," & CDbl(LvDetalle.ListItems(i).SubItems(2)) & ",'" & LvDetalle.ListItems(i).SubItems(3) & "'),"
        
        Next
        sql2 = Mid(sql2, 1, Len(sql2) - 1)
        cn.Execute Sql
        cn.Execute sql2
        Dim Dp_Promedio As Double
        Dp_Promedio = 0
                             
        
        
        
        For i = 1 To LvDetalle.ListItems.Count
        'LVDetalle.ListItems (i)
            Sql = "SELECT IFNULL(ROUND(kar_nuevo_saldo_valor/kar_nuevo_saldo),pro_precio_neto) promedio " & _
                    "FROM inv_kardex " & _
                    "WHERE rut_emp='" & SP_Rut_Activo & "' AND pro_codigo='" & LvDetalle.ListItems(i) & "' AND bod_id=" & CboOrigen.ItemData(CboOrigen.ListIndex) & "  " & _
                    "ORDER BY kar_id DESC " & _
                    "LIMIT 1"
            Consulta RsTmp, Sql
            Dp_Promedio = 0
            If RsTmp.RecordCount > 0 Then
                Dp_Promedio = RsTmp!promedio
            End If
        
        
            Kardex Fql(DtFecha), "SALIDA", 0, txtNro, CboOrigen.ItemData(CboOrigen.ListIndex), LvDetalle.ListItems(i), CDbl(LvDetalle.ListItems(i).SubItems(2)), "MOV. SALIDA DE " & CboOrigen.Text & " A " & CboDestino.Text, Dp_Promedio, Dp_Promedio * Val(LvDetalle.ListItems(i).SubItems(2)), , , , , , , , , 0
        Next
        
        For i = 1 To LvDetalle.ListItems.Count
            If LvDetalle.ListItems(i).SubItems(3) = "SI" Then
                Sql = "SELECT IFNULL(ROUND(kar_nuevo_saldo_valor/kar_nuevo_saldo),pro_precio_neto) promedio " & _
                        "FROM inv_kardex " & _
                        "WHERE rut_emp='" & SP_Rut_Activo & "' AND pro_codigo='" & LvDetalle.ListItems(i) & "' AND bod_id=" & CboDestino.ItemData(CboDestino.ListIndex) & "  " & _
                        "ORDER BY kar_id DESC " & _
                        "LIMIT 1"
                Consulta RsTmp, Sql
                'Dp_Promedio = 0
                If RsTmp.RecordCount > 0 Then
                    Dp_Promedio = RsTmp!promedio
                End If
            
            
            
                'Aqui hacemos que el kardex le de una entrada a la bodega seleccionada
                Kardex Fql(DtFecha), "ENTRADA", 0, txtNro, CboDestino.ItemData(CboDestino.ListIndex), LvDetalle.ListItems(i), CDbl(LvDetalle.ListItems(i).SubItems(2)), "MOV. ENTRADA A " & CboDestino.Text & " DE " & CboOrigen.Text, Dp_Promedio, Dp_Promedio * Val(LvDetalle.ListItems(i).SubItems(2)), , , , , , , , , 0
                            
            End If
        Next
        
        
    cn.CommitTrans
    
    
     'AHORA IMPRIMIMOS LA GUIA
     
porAquiSeImprime:

    Dialogo.CancelError = True
    On Error GoTo CancelaImpesionNV
    Dialogo.ShowPrinter
    CargaEmpresa
    ImprimeGuiaFertiquimica
   
    
    If MsgBox("�Exportar a Excel?...", vbInformation + vbYesNo) = vbNo Then
        
    Else
        'Exportar a Excel Movimiento
        Dim tit(2) As String
        If LvDetalle.ListItems.Count = 0 Then Exit Sub
        FraProgreso.Visible = True
        tit(0) = "MOVIMIENTO DE ARTICULOS NRO " & txtNro & "   Fecha:" & DtFecha.Value
        tit(1) = "BODEGA ORIGEN:" & CboOrigen.Text & "     -    DESTINO:" & CboDestino.Text
        tit(2) = TxtObs
        ExportarNuevo LvDetalle, tit, Me, BarraProgreso
        FraProgreso.Visible = False
    End If
    Unload Me
    Exit Sub
errorGraba:
    MsgBox "Error al intentar grabar..." & vbNewLine & Err.Description & vbNewLine & "Nro " & Err.Number
    cn.RollbackTrans
    Exit Sub
CancelaImpesionNV:
 'no imprime guia
 Unload Me
End Sub

Private Sub CmdOk_Click()
    Dim Ip_pos As Integer
    If Len(TxtCodigo) = 0 Then
        MsgBox "Falta codigo...", vbInformation
        TxtCodigo.SetFocus
        Exit Sub
    End If
    If Val(TxtCantidad) = 0 Then
        MsgBox "Ingrese Cantidad...", vbInformation
        TxtCantidad.SetFocus
        Exit Sub
    End If
    If Len(TxtNombre) = 0 Then
        MsgBox "Faltan datos...", vbInformation
        TxtCodigo.SetFocus
        Exit Sub
    End If
    
    LvDetalle.ListItems.Add , , TxtCodigo
    Ip_pos = LvDetalle.ListItems.Count
    LvDetalle.ListItems(Ip_pos).SubItems(1) = TxtNombre
    LvDetalle.ListItems(Ip_pos).SubItems(2) = TxtCantidad
    LvDetalle.ListItems(Ip_pos).SubItems(3) = CboActivo
    TxtCodigo = ""
    TxtNombre = ""
    TxtCantidad = "0"
    TxtCodigo.SetFocus
End Sub

Private Sub cmdSalir_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    Centrar Me
    Aplicar_skin Me
    DtFecha = Date
    txtUsuario = Principal.TxtNombre
    LLenarCombo CboGuia, "doc_nombre", "doc_id", "sis_documentos", "doc_activo='SI' AND doc_documento='VENTA' AND doc_nombre LIKE '%GUIA%'", "doc_id"
    
    If SP_Rut_Activo = "76.370.578-1" Or SP_Rut_Activo = "76.361.539-1" Or SP_Rut_Activo = "7.242.181-7" Then
        LLenarCombo CboOrigen, "bod_nombre", "bod_id", "par_bodegas", "bod_activo='SI' AND rut_emp='" & SP_Rut_Activo & "'"
        LLenarCombo CboDestino, "bod_nombre", "bod_id", "par_bodegas", "bod_activo='SI' AND rut_emp='" & SP_Rut_Activo & "'"
    
    Else
        LLenarCombo CboOrigen, "bod_nombre", "bod_id", "par_bodegas", "bod_activo='SI' AND rut_emp='" & SP_Rut_Activo & "' AND sue_id=" & IG_id_Sucursal_Empresa
        LLenarCombo CboDestino, "bod_nombre", "bod_id", "par_bodegas", "bod_activo='SI' AND rut_emp='" & SP_Rut_Activo & "' AND sue_id<>" & IG_id_Sucursal_Empresa
    End If
    CboOrigen.ListIndex = 0
    CboActivo.ListIndex = 0
    If DG_ID_Unico > 0 Then
        Frame1.Enabled = False
        Frame2.Enabled = False
        Me.FrmMantenedor.Enabled = False
        
        'Cargamos movimiento seleccionado... 19 - 07- 2012
        Sql = "SELECT * " & _
             "FROM inv_movimientos_bodegas " & _
             "WHERE mvp_id=" & DG_ID_Unico
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
            With RsTmp
                DtFecha = !mvp_fecha
                usuario = !mvp_usuario
             '   Busca_Id_Combo CboOrigen, Val(!mvp_id_origen)
             '   Busca_Id_Combo CboDestino, Val(!mvp_id_destino)
                LLenarCombo CboOrigen, "bod_nombre", "bod_id", "par_bodegas", "bod_activo='SI' AND rut_emp='" & SP_Rut_Activo & "' AND bod_id=" & !mvp_id_origen
                CboOrigen.ListIndex = 0
                LLenarCombo CboDestino, "bod_nombre", "bod_id", "par_bodegas", "bod_activo='SI' AND rut_emp='" & SP_Rut_Activo & "' AND bod_id=" & !mvp_id_destino
                CboDestino.ListIndex = 0
                txtNro = !mvp_id
                TxtObs = !mvp_obs
                Busca_Id_Combo CboGuia, Val(!doc_id)
                TxtNroGuia = !no_documento
                
            End With
            Sql = "SELECT d.pro_codigo,p.descripcion,d.mpd_cantidad,d.mpd_mantiene_stock " & _
                    " FROM inv_movimientos_bodega_detalle d " & _
                    " INNER JOIN maestro_productos p ON d.pro_codigo=p.codigo AND rut_emp='" & SP_Rut_Activo & "' " & _
                    "WHERE mvp_id=" & DG_ID_Unico
            Consulta RsTmp, Sql
            LLenar_Grilla RsTmp, Me, LvDetalle, False, True, True, False
            CmdGuardar.Caption = "IMPRIMIR"
        End If
        'Me.CmdGuardar.Enabled = False
    End If
    
End Sub
Private Sub CargaEmpresa()
    
    Sql = "SELECT * " & _
            "FROM sis_empresas " & _
            "WHERE rut='" & SP_Rut_Activo & "'"
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        With RsTmp
            TxtRut = SP_Rut_Activo
            Me.TxtRazonSocial = !nombre_empresa
            TxtGiro = !giro
        End With
    End If
    If CboDestino.ListIndex = -1 Then Exit Sub
    
    Sql = "SELECT sue_direccion,sue_ciudad,sue_comuna " & _
            "FROM sis_empresas_sucursales " & _
            "WHERE sue_id=(SELECT sue_id " & _
                            "FROM par_bodegas " & _
                            "WHERE bod_id=" & CboDestino.ItemData(CboDestino.ListIndex) & ")"
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        TxtDireccion = RsTmp!sue_direccion
        TxtCiudad = RsTmp!sue_ciudad
        txtComuna = RsTmp!sue_comuna
        
    End If
End Sub
Private Sub LvDetalle_DblClick()
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    If DG_ID_Unico > 0 Then Exit Sub
    TxtCodigo = LvDetalle.SelectedItem
    TxtNombre = LvDetalle.SelectedItem.SubItems(1)
    TxtCantidad = LvDetalle.SelectedItem.SubItems(2)
    CboActivo.ListIndex = IIf(LvDetalle.SelectedItem.SubItems(3) = "SI", 0, 1)
    LvDetalle.ListItems.Remove LvDetalle.SelectedItem.Index
    TxtCantidad.SetFocus
End Sub

Private Sub Timer1_Timer()
    On Error Resume Next
    DtFecha.SetFocus
    Timer1.Enabled = False
End Sub
Private Sub txtCantidad_GotFocus()
    En_Foco TxtCodigo
    TxtStock.Visible = True
End Sub

Private Sub txtCantidad_KeyPress(KeyAscii As Integer)
    KeyAscii = AceptaSoloNumeros(KeyAscii)
End Sub

Private Sub TxtCantidad_LostFocus()
    TxtStock.Visible = False
End Sub

Private Sub TxtCodigo_GotFocus()
    En_Foco TxtCodigo
End Sub

Private Sub TxtCodigo_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 112 Then
        CmdBuscaProducto_Click
    End If
End Sub

Private Sub TxtCodigo_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

'Private Sub TxtCodigo_KeyPress(KeyAscii As Integer)
'    KeyAscii = Asc(UCase(Chr(KeyAscii)))
'    If KeyAscii = 13 Then
'        TxtCantidad.SetFocus
'    End If
'End Sub

Private Sub TxtCodigo_Validate(Cancel As Boolean)
    If Len(TxtCodigo) = 0 Then Exit Sub
    If CboOrigen.ListIndex = -1 Then
        MsgBox "Antes debe seleccionar Origen...", vbInformation
        CboOrigen.SetFocus
        Exit Sub
    End If
    Sql = "SELECT p.descripcion,sto_stock " & _
            "FROM maestro_productos p " & _
            "LEFT JOIN pro_stock s ON p.codigo=s.pro_codigo AND p.rut_emp=s.rut_emp " & _
            "WHERE p.rut_emp='" & SP_Rut_Activo & "' AND p.codigo='" & TxtCodigo & "' AND s.bod_id=" & CboOrigen.ItemData(CboOrigen.ListIndex)
   Consulta RsTmp, Sql
   If RsTmp.RecordCount > 0 Then
        TxtNombre = RsTmp!Descripcion
        TxtStock = "Stock Actual:" & RsTmp!sto_stock
    Else
        Sql = "SELECT p.descripcion, 0 sto_stock " & _
        "FROM maestro_productos p " & _
        "WHERE p.rut_emp='" & SP_Rut_Activo & "' AND p.codigo='" & TxtCodigo & "'"
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
            TxtNombre = RsTmp!Descripcion
            TxtStock = "Stock Actual:" & RsTmp!sto_stock
        End If
    End If
End Sub


Private Sub TxtNroGuia_GotFocus()
    En_Foco TxtNroGuia
End Sub

Private Sub TxtNroGuia_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
    If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtObs_GotFocus()
    En_Foco TxtObs
End Sub

Private Sub TXToBS_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub ImprimeGuiaFertiquimica()
    Dim Cx As Double, Cy As Double, Sp_Fecha As String, Ip_Mes As Integer, Dp_S As Double, Sp_Letras As String
    Dim Sp_Neto As String * 12, Sp_IVA As String * 12, Sp_Total As String * 12
    Dim Sp_GuiasFacturadas As String
    
    Dim p_Codigo As String * 5
    Dim p_Cantidad As String * 7
    Dim p_UM As String * 4
    Dim p_Detalle As String * 40
    Dim p_Unitario As String * 10
    Dim p_Total As String * 11
    Dim p_Mes As String * 10
    Dim p_CiudadF As String * 20
    
    On Error GoTo ProblemaImpresora
    Printer.FontName = "Courier New"
    Printer.FontSize = 10
    Printer.ScaleMode = 7
        
  'Aqui leemos el X y el Y iniciales de la tabla de sucursales
    Sql = "SELECT sue_cx,sue_cy " & _
            "FROM sis_empresas_sucursales " & _
            "WHERE sue_id=" & IG_id_Sucursal_Empresa
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        Cx = Val(RsTmp!sue_cx) + 0.2
        Cy = Val(RsTmp!sue_cy) + 0.2 'vertical)
    Else
        Cx = 1.1 'horizontal
        Cy = 3 'vertical
    
    End If
    Dp_S = 0.1
    
    Printer.CurrentX = Cx
    Printer.CurrentY = Cy
    Sp_Fecha = Me.DtFecha
    Ip_Mes = Val(Mid(Sp_Fecha, 4, 2))
    
    Printer.CurrentY = Printer.CurrentY + 1.9
    Printer.CurrentX = Cx + 5.2
    p_Mes = UCase(MonthName(Ip_Mes))
    RSet p_CiudadF = CboOrigen.Text
    Printer.Print p_CiudadF & "  " & Mid(Sp_Fecha, 1, 2) & Space(6) & p_Mes & Space(10) & Mid(Sp_Fecha, 7, 4)

    Printer.CurrentX = Cx - 0.4
  '  Printer.CurrentY = Printer.CurrentY + Dp_S
    pos = Printer.CurrentY
    Printer.Print TxtRazonSocial
    
    Printer.CurrentX = Cx + 12
    Printer.CurrentY = pos
    pos = Printer.CurrentY
    Printer.Print Me.TxtRut
    
   
    Printer.CurrentX = Cx - 0.4
   ' Printer.CurrentY = Printer.CurrentY + Dp_S
    pos = Printer.CurrentY
    posd = Printer.CurrentY
    Printer.Print TxtDireccion
    
    Printer.CurrentY = pos
    Printer.CurrentX = Printer.CurrentX + 10
    'Printer.Print Me.LbTelefono
    
    

    Printer.CurrentY = pos
    Printer.CurrentX = Cx + 12
    Printer.Print txtComuna
    
    
    
    pos = Printer.CurrentY
    Printer.CurrentY = pos
    Printer.CurrentX = Cx - 0.6
    Printer.Print TxtGiro
    
    Printer.CurrentY = pos
    Printer.CurrentX = Cx + 12
    Printer.Print TxtNroDocumento
    
    Printer.CurrentX = Cx + 12
    Printer.Print "Origen :" & CboOrigen.Text
    Printer.CurrentX = Cx + 12
    Printer.Print "Destino:" & CboDestino.Text
    
    
    Printer.CurrentY = Printer.CurrentY - (Dp_S * 2)
    pos = Printer.CurrentY
    
    Printer.CurrentX = Cx + 8.5
    Printer.CurrentY = Printer.CurrentY + 0.2
       
    Printer.CurrentY = Printer.CurrentY + 1
    
    
    Printer.CurrentY = Printer.CurrentY + 1
    
    For i = 1 To LvDetalle.ListItems.Count
        '6 - 3 - 7 - 8
        Printer.CurrentX = Printer.CurrentX + 0.2 'INTERLINIA ARTICULOS
       ' Printer.CurrentX = Cx - 2
        Printer.CurrentX = Cx - 1
        p_Codigo = LvDetalle.ListItems(i)
        RSet p_Cantidad = LvDetalle.ListItems(i).SubItems(2)
        p_UM = " " 'LvMateriales.ListItems(i).SubItems(21)
        p_Detalle = LvDetalle.ListItems(i).SubItems(1)
        RSet p_Unitario = "" ' Round(CDbl(LvDetalle.ListItems(i).SubItems(3)))
        RSet p_Total = "" ' Round(CDbl(LvDetalle.ListItems(i).SubItems(4)))
        
        Printer.Print p_Codigo & " " & p_Cantidad & " " & p_UM & " " & p_Detalle & "    " & p_Unitario & " " & p_Total
    Next
    
    Printer.CurrentX = Cx - 1
    Printer.CurrentY = Cy + 14.5
    pos = Printer.CurrentY
        
    Printer.CurrentX = Cx + 1
    Printer.CurrentY = pos - 2.2
   ' Printer.Print TxtComentario
    
  '  pos = Printer.CurrentY
  
    Printer.CurrentX = Cx + 1
        
    'Printer.CurrentY = pos
    Printer.CurrentY = pos - 1.4
    
    RSet Sp_Neto = txtTotalGuia
    RSet Sp_IVA = SkIvaMateriales
    RSet Sp_Total = SkBrutoMateriales
    pos = pos - 1.6
    'pos = pos - 1.4
    Printer.CurrentY = pos - 0.5
    Printer.CurrentX = Cx + 11.9
    Printer.Print "Total Neto $" & Sp_Neto
    
    Printer.CurrentY = Printer.CurrentY + 1
    
    Printer.CurrentX = Cx + 0.5
    Printer.Print TxtComentario
     
   
    
    Printer.NewPage
    Printer.EndDoc
    Exit Sub
ProblemaImpresora:
    MsgBox "Ocurrio un error al intentar imprimir..." & vbNewLine & Err.Description & vbNewLine & Err.Number

End Sub

