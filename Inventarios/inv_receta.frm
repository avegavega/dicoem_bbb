VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form inv_receta_listado 
   Caption         =   "Listado F�rmulas"
   ClientHeight    =   7530
   ClientLeft      =   3765
   ClientTop       =   2430
   ClientWidth     =   10455
   LinkTopic       =   "Form1"
   ScaleHeight     =   7530
   ScaleWidth      =   10455
   Begin VB.Frame Frame2 
      Caption         =   "Busqueda"
      Height          =   1095
      Left            =   270
      TabIndex        =   4
      Top             =   150
      Width           =   9990
      Begin VB.TextBox TxtBusca 
         Height          =   375
         Left            =   120
         TabIndex        =   13
         Top             =   240
         Width           =   2385
      End
      Begin VB.OptionButton OpDescripcion 
         Caption         =   "Descripcion"
         Height          =   255
         Left            =   120
         TabIndex        =   12
         Top             =   720
         Value           =   -1  'True
         Width           =   1230
      End
      Begin VB.OptionButton OpCodigo 
         Caption         =   "Codigo"
         Height          =   255
         Left            =   1500
         TabIndex        =   11
         Top             =   735
         Width           =   975
      End
      Begin VB.CommandButton CmdBuscaProd 
         Caption         =   "&Buscar"
         Height          =   375
         Left            =   7770
         TabIndex        =   10
         Top             =   465
         Width           =   975
      End
      Begin VB.CommandButton CmdTodosPro 
         Caption         =   "Todos"
         Height          =   375
         Left            =   8850
         TabIndex        =   9
         Top             =   480
         Width           =   975
      End
      Begin VB.Frame FraMarca 
         Caption         =   "Marca"
         Height          =   1095
         Left            =   2625
         TabIndex        =   7
         Top             =   0
         Width           =   2115
         Begin VB.ComboBox CboMarca 
            Height          =   315
            Left            =   90
            Style           =   2  'Dropdown List
            TabIndex        =   8
            Top             =   480
            Width           =   1815
         End
      End
      Begin VB.Frame FraFamilia 
         Caption         =   "Tipo Producto"
         Height          =   1095
         Left            =   4710
         TabIndex        =   5
         Top             =   0
         Width           =   2055
         Begin VB.ComboBox CboTipoProducto 
            Height          =   315
            Left            =   120
            Style           =   2  'Dropdown List
            TabIndex        =   6
            Top             =   480
            Width           =   1815
         End
      End
   End
   Begin VB.CommandButton CmdNueva 
      Caption         =   "F1 - Nueva Formula"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   240
      TabIndex        =   3
      ToolTipText     =   "Nueva Venta"
      Top             =   6630
      Width           =   2250
   End
   Begin VB.CommandButton CmdSeleccionar 
      Caption         =   "F2 - Ver"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   2610
      TabIndex        =   2
      ToolTipText     =   "Visualizar Venta"
      Top             =   6645
      Width           =   2115
   End
   Begin VB.CommandButton cmdSalir 
      Caption         =   "Retornar"
      Height          =   555
      Left            =   8580
      TabIndex        =   0
      Top             =   6705
      Width           =   1680
   End
   Begin VB.Timer Timer1 
      Interval        =   10
      Left            =   510
      Top             =   30
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   0
      OleObjectBlob   =   "inv_receta.frx":0000
      Top             =   0
   End
   Begin MSComctlLib.ListView LVDetalle 
      Height          =   5175
      Left            =   255
      TabIndex        =   1
      Top             =   1335
      Width           =   9975
      _ExtentX        =   17595
      _ExtentY        =   9128
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   0   'False
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   0
      NumItems        =   6
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Object.Tag             =   "N109"
         Text            =   "Nro "
         Object.Width           =   882
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Object.Tag             =   "N109"
         Text            =   "Id"
         Object.Width           =   882
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Object.Tag             =   "N109"
         Text            =   "Codigo"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Text            =   "Producto"
         Object.Width           =   6174
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Object.Tag             =   "F1000"
         Text            =   "Fecha Creacion"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   5
         Object.Tag             =   "F1000"
         Text            =   "Fecha Modificacion"
         Object.Width           =   2540
      EndProperty
   End
End
Attribute VB_Name = "inv_receta_listado"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub CmdNueva_Click()
    DG_ID_Unico = 0
    inv_recetas.Show 1
    CargaRecetas
End Sub

Private Sub cmdSalir_Click()
    Unload Me
End Sub

Private Sub CmdSeleccionar_Click()
    For i = 1 To LvDetalle.ListItems.Count
        If LvDetalle.ListItems(i).Checked Then
            DG_ID_Unico = LvDetalle.ListItems(i).SubItems(1)
            inv_recetas.Show 1
            CargaRecetas
        End If
    Next i
End Sub

Private Sub Form_Load()
    Aplicar_skin Me
    Centrar Me
    LLenarCombo CboTipoProducto, "tip_nombre", "tip_id", "par_tipos_productos", "tip_activo='SI' AND  rut_emp='" & SP_Rut_Activo & "'", "tip_nombre"
    CboTipoProducto.AddItem "TODOS"
    CboTipoProducto.ListIndex = CboTipoProducto.ListCount - 1
    LLenarCombo CboMarca, "mar_nombre", "mar_id", "par_marcas", "mar_activo='SI' AND  rut_emp='" & SP_Rut_Activo & "'", "mar_nombre"
    CboMarca.AddItem "TODOS"
    CboMarca.ListIndex = CboMarca.ListCount - 1
    CargaRecetas
End Sub

Private Sub Timer1_Timer()
    On Error Resume Next
    LvDetalle.SetFocus
    Timer1.Enabled = False
End Sub
Private Sub CargaRecetas()
    Sql = "SELECT  @rownum:=@rownum+1 AS rownum,rec_id,pro_codigo,descripcion,rec_fecha_creacion,rec_fecha_modificacion " & _
            "FROM inv_receta r " & _
            "JOIN maestro_productos p ON r.pro_codigo=p.codigo " & _
            "WHERE rec_activo='SI' " & _
            "ORDER BY @rownum :=@rownum + 1"
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvDetalle, True, False, False, True
End Sub



Private Sub TxtBusca_Validate(Cancel As Boolean)
TxtBusca = Replace(TxtBusca, "'", "")
End Sub
