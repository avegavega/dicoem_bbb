VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form Inv_Kardex 
   BackColor       =   &H00008000&
   Caption         =   "Kardex"
   ClientHeight    =   7470
   ClientLeft      =   -390
   ClientTop       =   1065
   ClientWidth     =   15420
   LinkTopic       =   "Form1"
   ScaleHeight     =   7470
   ScaleWidth      =   15420
   Begin VB.Timer Timer1 
      Interval        =   10
      Left            =   285
      Top             =   6630
   End
   Begin VB.CommandButton CmdSalir 
      Cancel          =   -1  'True
      Caption         =   "&Retornar"
      Height          =   375
      Left            =   13080
      TabIndex        =   3
      Top             =   6990
      Width           =   1695
   End
   Begin VB.CommandButton cmdExportar 
      Caption         =   "Exportar a Excel"
      Height          =   375
      Left            =   150
      TabIndex        =   2
      Top             =   6990
      Width           =   1695
   End
   Begin VB.Frame FraProgreso 
      Height          =   540
      Left            =   2685
      TabIndex        =   4
      Top             =   6855
      Visible         =   0   'False
      Width           =   8835
      Begin VB.PictureBox Picture1 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         ForeColor       =   &H80000008&
         Height          =   300
         Left            =   8010
         ScaleHeight     =   270
         ScaleWidth      =   705
         TabIndex        =   5
         Top             =   135
         Width           =   735
         Begin ACTIVESKINLibCtl.SkinLabel SkProgreso 
            Height          =   225
            Left            =   -15
            OleObjectBlob   =   "invKardex.frx":0000
            TabIndex        =   6
            Top             =   15
            Width           =   735
         End
      End
      Begin MSComctlLib.ProgressBar BarraProgreso 
         Height          =   270
         Left            =   165
         TabIndex        =   7
         Top             =   165
         Width           =   7905
         _ExtentX        =   13944
         _ExtentY        =   476
         _Version        =   393216
         Appearance      =   0
         Min             =   1e-4
      End
   End
   Begin VB.Frame Frame3 
      Caption         =   "Kardex"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   6525
      Left            =   60
      TabIndex        =   0
      Top             =   90
      Width           =   16500
      Begin VB.Frame Frame1 
         Caption         =   "ARTICULO"
         Height          =   570
         Left            =   105
         TabIndex        =   8
         Top             =   315
         Width           =   16215
         Begin ACTIVESKINLibCtl.SkinLabel SkArticulo 
            Height          =   330
            Left            =   6615
            OleObjectBlob   =   "invKardex.frx":0062
            TabIndex        =   9
            Top             =   150
            Width           =   7845
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
            Height          =   330
            Left            =   2010
            OleObjectBlob   =   "invKardex.frx":00D5
            TabIndex        =   10
            Top             =   150
            Width           =   1110
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkCodigo 
            Height          =   330
            Left            =   3165
            OleObjectBlob   =   "invKardex.frx":013A
            TabIndex        =   11
            Top             =   150
            Width           =   1260
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
            Height          =   330
            Left            =   4710
            OleObjectBlob   =   "invKardex.frx":019F
            TabIndex        =   12
            Top             =   150
            Width           =   1860
         End
      End
      Begin MSComctlLib.ListView LvDetalle 
         Height          =   5490
         Left            =   105
         TabIndex        =   1
         Top             =   945
         Width           =   16200
         _ExtentX        =   28575
         _ExtentY        =   9684
         View            =   3
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   15
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "N109"
            Text            =   "Nro"
            Object.Width           =   1058
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "T1000"
            Text            =   "Fecha"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   2
            Object.Tag             =   "N102"
            Text            =   "Unitario"
            Object.Width           =   1940
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   3
            Object.Tag             =   "N102"
            Text            =   "Entrada"
            Object.Width           =   1676
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   4
            Object.Tag             =   "N102"
            Text            =   "Salida"
            Object.Width           =   1676
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   5
            Object.Tag             =   "N102"
            Text            =   "Saldo"
            Object.Width           =   1676
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   6
            Object.Tag             =   "N100"
            Text            =   "Entrada $"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   7
            Object.Tag             =   "N100"
            Text            =   "Salida $"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   8
            Object.Tag             =   "N100"
            Text            =   "Saldo $"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   9
            Object.Tag             =   "N100"
            Text            =   "Costo Promedio"
            Object.Width           =   2293
         EndProperty
         BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   10
            Object.Tag             =   "T2000"
            Text            =   "Descripcion"
            Object.Width           =   4586
         EndProperty
         BeginProperty ColumnHeader(12) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   11
            Object.Tag             =   "T1500"
            Text            =   "Usuario"
            Object.Width           =   1940
         EndProperty
         BeginProperty ColumnHeader(13) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   12
            Object.Tag             =   "T2000"
            Text            =   "rsocial"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(14) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   13
            Object.Tag             =   "T1000"
            Text            =   "hora"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(15) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   14
            Object.Tag             =   "T2000"
            Text            =   "Bodega"
            Object.Width           =   3528
         EndProperty
      End
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   120
      OleObjectBlob   =   "invKardex.frx":020E
      Top             =   7425
   End
End
Attribute VB_Name = "Inv_Kardex"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public Lm_BodID As Integer
Dim b_BuscaKardex As Boolean

Private Sub CmdExportar_Click()
    Dim tit(2) As String
    FraProgreso.Visible = True
    tit(0) = Me.Caption
    tit(1) = "FECHA INFORME: " & Date & " - " & Time
    tit(2) = "" ' "                                        DESDE:" & DtInicio.Value & "   HASTA:" & Dtfin.Value
    Exportar LvDetalle, tit, Me, BarraProgreso, SkProgreso
    BarraProgreso.Value = 1
    SkProgreso = "0%"
    FraProgreso.Visible = False
End Sub

Private Sub CargaK()
    Sql = "SELECT kar_id nro,date_format(kar_fecha,'%d-%m-%Y') fecha," & _
                "pro_precio_neto unitario," & _
                "IF(kar_movimiento='ENTRADA'  OR kar_movimiento='NND',kar_cantidad,0) entrada," & _
                "IF(kar_movimiento='SALIDA' OR kar_movimiento='NNC',kar_cantidad,0) salida," & _
                "kar_nuevo_saldo saldo," & _
                "IF(kar_movimiento='ENTRADA' OR kar_movimiento='NND',kar_cantidad_valor,0) entrada_valor," & _
                "IF(kar_movimiento='SALIDA' OR kar_movimiento='NNC' OR kar_movimiento='NN',kar_cantidad_valor,0)  salida_valor," & _
                " IF(kar_nuevo_saldo<0,(kar_nuevo_saldo_valor*-1),kar_nuevo_saldo_valor)  saldo_valor," & _
                "ABS(round(kar_nuevo_saldo_valor/kar_nuevo_saldo,2)) costo_promedio," & _
                "kar_descripcion," & _
                "kar_usuario," & _
                "rsocial," & _
                "kar_hora,bod_nombre " & _
            "FROM inv_kardex k,maestro_productos m,par_bodegas b " & _
            "WHERE k.bod_id=" & Lm_BodID & " AND k.bod_id=b.bod_id AND  k.rut_emp='" & SP_Rut_Activo & "' AND m.rut_emp='" & SP_Rut_Activo & "' AND kar_mostrar='SI' AND k.pro_codigo=m.codigo AND k.pro_codigo='" & SG_codigo2 & "' " & G_Filtro_Fecha & _
            " ORDER BY kar_id DESC"
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvDetalle, False, True, True, False
    
    If RsTmp.RecordCount > 0 Then
        'unidaes
        Dim elColor As Variant, elColorEntrada
        elColor = &HC000&
        elColorEntrada = &H808000
        For L = 3 To 5
            For i = 1 To LvDetalle.ListItems.Count
                If Val(LvDetalle.ListItems(i).SubItems(3)) = 0 Then
                    If Val(LvDetalle.ListItems(i).SubItems(5)) < 0 Then elColor = &HFF& Else elColor = &HC000&
                Else
                    If Val(LvDetalle.ListItems(i).SubItems(5)) < 0 Then elColor = &HFF& Else elColor = &H8000&
                End If
                LvDetalle.ListItems(i).ListSubItems(L).ForeColor = elColor
                If L = 5 Then LvDetalle.ListItems(i).ListSubItems(L).Bold = True
            Next i
        Next L
        'valores
        For L = 6 To 8
            For i = 1 To LvDetalle.ListItems.Count
                If Val(LvDetalle.ListItems(i).SubItems(6)) = 0 Then
                    LvDetalle.ListItems(i).ListSubItems(L).ForeColor = &HC0C000
                Else
                    LvDetalle.ListItems(i).ListSubItems(L).ForeColor = &H808000
                End If
                If CDbl(LvDetalle.ListItems(i).SubItems(5)) < 0 Then
                    LvDetalle.ListItems(i).SubItems(8) = Format(CDbl(LvDetalle.ListItems(i).SubItems(8)) * -1, "#,0")
                End If
                If L = 8 Then LvDetalle.ListItems(i).ListSubItems(L).Bold = True
            Next i
        Next L
    End If
    
End Sub

Private Sub CmdSalir_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    Centrar Me
    Aplicar_skin Me
    CargaK
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Lm_BodID = 0
End Sub

Private Sub Timer1_Timer()
    LvDetalle.SetFocus
    Timer1.Enabled = False
End Sub
