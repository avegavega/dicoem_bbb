VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{DA729E34-689F-49EA-A856-B57046630B73}#1.0#0"; "progressbar-xp.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form invPlanillaInventario 
   Caption         =   "Planillas de inventario"
   ClientHeight    =   10710
   ClientLeft      =   3060
   ClientTop       =   450
   ClientWidth     =   14610
   LinkTopic       =   "Form1"
   ScaleHeight     =   10710
   ScaleWidth      =   14610
   Begin VB.Frame FrmPlanillas 
      Caption         =   "Planillas"
      Height          =   570
      Left            =   390
      TabIndex        =   9
      Top             =   1335
      Width           =   13920
      Begin VB.CommandButton cmdNuevaPlanilla 
         Caption         =   "Nueva planilla"
         Height          =   320
         Left            =   10275
         TabIndex        =   24
         Top             =   180
         Width           =   1170
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel7 
         Height          =   225
         Left            =   165
         OleObjectBlob   =   "invPlanillaInventario.frx":0000
         TabIndex        =   21
         Top             =   1770
         Width           =   2100
      End
      Begin VB.CommandButton cmdVerPlanillas 
         Caption         =   "Ver planillas"
         Height          =   320
         Left            =   11505
         TabIndex        =   11
         Top             =   180
         Width           =   1170
      End
      Begin MSComctlLib.ListView LvPlanillas 
         Height          =   1140
         Left            =   165
         TabIndex        =   12
         Top             =   600
         Width           =   12390
         _ExtentX        =   21855
         _ExtentY        =   2011
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   8
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "T3000"
            Text            =   "Fecha"
            Object.Width           =   2293
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "T1000"
            Text            =   "Hora "
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "N100"
            Text            =   "Nro Planilla"
            Object.Width           =   1940
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Object.Tag             =   "T2500"
            Text            =   "Descripcion Planilla"
            Object.Width           =   5644
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Object.Tag             =   "T1000"
            Text            =   "Bodega"
            Object.Width           =   3528
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   5
            Object.Tag             =   "T2000"
            Text            =   "Estado"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   6
            Object.Tag             =   "T2000"
            Text            =   "Usuario"
            Object.Width           =   2646
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   7
            Object.Tag             =   "N100"
            Text            =   "bod_id"
            Object.Width           =   0
         EndProperty
      End
   End
   Begin Proyecto2.XP_ProgressBar ProgBarGuarda 
      Height          =   240
      Left            =   2055
      TabIndex        =   27
      Top             =   7530
      Visible         =   0   'False
      Width           =   10620
      _ExtentX        =   18733
      _ExtentY        =   423
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BrushStyle      =   0
      Color           =   16777088
      Scrolling       =   3
      ShowText        =   -1  'True
   End
   Begin VB.CommandButton CmdGuardar 
      Caption         =   "Crear Planilla"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   480
      Left            =   420
      TabIndex        =   38
      Top             =   9870
      Width           =   1665
   End
   Begin VB.Timer tmrLoading 
      Enabled         =   0   'False
      Interval        =   10
      Left            =   945
      Top             =   7980
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   300
      Left            =   540
      Picture         =   "invPlanillaInventario.frx":009E
      ScaleHeight     =   300
      ScaleWidth      =   13425
      TabIndex        =   28
      Top             =   10575
      Width           =   13425
   End
   Begin VB.TextBox Text1 
      BackColor       =   &H00FFFFC0&
      ForeColor       =   &H00FF0000&
      Height          =   345
      Left            =   4800
      TabIndex        =   25
      Text            =   "Doble Click en Cantidad para ingresar Valor"
      Top             =   10095
      Width           =   3195
   End
   Begin VB.CommandButton cmdSalir 
      Caption         =   "&Retornar"
      Height          =   405
      Left            =   13020
      TabIndex        =   7
      Top             =   10230
      Width           =   1350
   End
   Begin VB.Frame Frame2 
      Caption         =   "Craer Nueva Planilla"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1080
      Left            =   375
      TabIndex        =   6
      Top             =   120
      Width           =   13965
      Begin VB.ComboBox CboBodega 
         Height          =   315
         Left            =   7725
         Style           =   2  'Dropdown List
         TabIndex        =   2
         Top             =   630
         Width           =   2805
      End
      Begin VB.TextBox txtNroPlanilla 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         Height          =   315
         Left            =   10530
         Locked          =   -1  'True
         TabIndex        =   17
         Text            =   "0"
         Top             =   630
         Width           =   1845
      End
      Begin VB.TextBox TxtDescripcion 
         Height          =   315
         Left            =   2340
         TabIndex        =   1
         Top             =   630
         Width           =   5400
      End
      Begin MSComCtl2.DTPicker DtFecha 
         Height          =   315
         Left            =   960
         TabIndex        =   0
         Top             =   630
         Width           =   1365
         _ExtentX        =   2408
         _ExtentY        =   556
         _Version        =   393216
         Format          =   273416193
         CurrentDate     =   40628
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
         Height          =   240
         Left            =   840
         OleObjectBlob   =   "invPlanillaInventario.frx":4BDB
         TabIndex        =   8
         Top             =   435
         Width           =   570
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   240
         Left            =   2325
         OleObjectBlob   =   "invPlanillaInventario.frx":4C43
         TabIndex        =   10
         Top             =   435
         Width           =   1770
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel5 
         Height          =   240
         Left            =   11310
         OleObjectBlob   =   "invPlanillaInventario.frx":4CCF
         TabIndex        =   18
         Top             =   435
         Width           =   1035
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel6 
         Height          =   240
         Left            =   7665
         OleObjectBlob   =   "invPlanillaInventario.frx":4D45
         TabIndex        =   20
         Top             =   435
         Width           =   615
      End
   End
   Begin VB.Frame FrmArticulos 
      Caption         =   "Articulos de la nueva planilla"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   8115
      Left            =   420
      TabIndex        =   5
      Top             =   1995
      Width           =   13950
      Begin VB.TextBox TxtBusquedaCodigoSistema 
         Height          =   315
         Left            =   9990
         TabIndex        =   58
         ToolTipText     =   "Despues de ingresar presione enter."
         Top             =   7635
         Width           =   2520
      End
      Begin VB.TextBox TxtBSis 
         Height          =   315
         Left            =   10830
         TabIndex        =   55
         Top             =   1065
         Width           =   1830
      End
      Begin VB.CommandButton Command3 
         Caption         =   "<-"
         Height          =   240
         Left            =   12360
         TabIndex        =   54
         ToolTipText     =   "Limpiar"
         Top             =   1080
         Width           =   330
      End
      Begin VB.CommandButton CmdbuscarCodSistema 
         Caption         =   "Buscar"
         Height          =   240
         Left            =   12810
         TabIndex        =   53
         Top             =   1050
         Width           =   960
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkCantidadProductos 
         Height          =   225
         Left            =   1440
         OleObjectBlob   =   "invPlanillaInventario.frx":4DAF
         TabIndex        =   52
         Top             =   1095
         Width           =   3915
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkScan 
         Height          =   240
         Left            =   1215
         OleObjectBlob   =   "invPlanillaInventario.frx":4E29
         TabIndex        =   50
         Top             =   7650
         Width           =   2685
      End
      Begin VB.TextBox TxtBusquedaCodigo 
         Height          =   315
         Left            =   3840
         TabIndex        =   49
         ToolTipText     =   "Despues de ingresar presione enter."
         Top             =   7605
         Width           =   3525
      End
      Begin VB.CommandButton CmdBuscaStock 
         Caption         =   "B"
         Height          =   270
         Left            =   7740
         TabIndex        =   48
         Top             =   990
         Width           =   450
      End
      Begin VB.TextBox TxtFiltroStock 
         Height          =   285
         Left            =   6855
         TabIndex        =   46
         Text            =   "<0"
         Top             =   990
         Width           =   870
      End
      Begin VB.CheckBox Check1 
         Height          =   195
         Left            =   180
         TabIndex        =   45
         ToolTipText     =   "Todo"
         Top             =   4605
         Width           =   210
      End
      Begin VB.Frame FraProgreso 
         Height          =   795
         Left            =   1995
         TabIndex        =   15
         Top             =   6495
         Visible         =   0   'False
         Width           =   11430
         Begin Proyecto2.XP_ProgressBar BarraProgreso 
            Height          =   495
            Left            =   90
            TabIndex        =   16
            Top             =   195
            Width           =   11190
            _ExtentX        =   19738
            _ExtentY        =   873
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BrushStyle      =   0
            Color           =   16777088
            Scrolling       =   1
            ShowText        =   -1  'True
         End
      End
      Begin VB.TextBox TxtTemp 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00000000&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H0000FFFF&
         Height          =   285
         Left            =   10650
         TabIndex        =   26
         Text            =   "0"
         Top             =   120
         Visible         =   0   'False
         Width           =   1530
      End
      Begin MSComctlLib.ListView LvProductos 
         Height          =   2820
         Left            =   135
         TabIndex        =   51
         Top             =   4800
         Width           =   13650
         _ExtentX        =   24077
         _ExtentY        =   4974
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   0   'False
         HideSelection   =   -1  'True
         Checkboxes      =   -1  'True
         FullRowSelect   =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   6
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "T1500"
            Text            =   "        Codigo"
            Object.Width           =   2646
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "T2000"
            Text            =   "Tipo Producto"
            Object.Width           =   3528
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "T2000"
            Text            =   "Marca"
            Object.Width           =   3528
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Object.Tag             =   "T5000"
            Text            =   "Descripcion"
            Object.Width           =   8819
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   4
            Object.Tag             =   "T1500"
            Text            =   "Cantidad"
            Object.Width           =   2646
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Object.Tag             =   "T1000"
            Text            =   "Cod. Empresa"
            Object.Width           =   2540
         EndProperty
      End
      Begin VB.CommandButton CmdAgrega 
         Caption         =   "Agrega Seleccionados"
         Height          =   345
         Left            =   5610
         TabIndex        =   44
         Top             =   4425
         Width           =   2265
      End
      Begin VB.CommandButton cmdBCI 
         Caption         =   "Buscar"
         Height          =   240
         Left            =   12825
         TabIndex        =   42
         Top             =   525
         Width           =   960
      End
      Begin VB.CommandButton Command1 
         Caption         =   "<-"
         Height          =   240
         Left            =   12375
         TabIndex        =   39
         ToolTipText     =   "Limpiar"
         Top             =   600
         Width           =   330
      End
      Begin VB.TextBox TxtCodInterno 
         Height          =   315
         Left            =   10875
         TabIndex        =   40
         Top             =   585
         Width           =   1830
      End
      Begin VB.CommandButton cmdBuscar 
         Caption         =   "Buscar"
         Height          =   270
         Left            =   2265
         TabIndex        =   37
         Top             =   615
         Width           =   2880
      End
      Begin VB.CommandButton CmdBuscaSimple 
         Caption         =   "Simple"
         Height          =   270
         Left            =   4200
         TabIndex        =   35
         Top             =   615
         Visible         =   0   'False
         Width           =   1200
      End
      Begin VB.CommandButton cmdLimpiaBusqueda 
         Caption         =   "<-"
         Height          =   240
         Left            =   4800
         TabIndex        =   34
         ToolTipText     =   "Limpiar"
         Top             =   330
         Width           =   330
      End
      Begin VB.TextBox txtBusqueda 
         Height          =   315
         Left            =   2280
         TabIndex        =   33
         Top             =   300
         Width           =   2895
      End
      Begin VB.Frame FraLoading 
         Height          =   1275
         Left            =   7515
         TabIndex        =   29
         Top             =   6015
         Width           =   4815
         Begin Proyecto2.XP_ProgressBar ProgLoading 
            Height          =   195
            Left            =   765
            TabIndex        =   31
            Top             =   735
            Width           =   3315
            _ExtentX        =   5847
            _ExtentY        =   344
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BrushStyle      =   0
            Color           =   11034163
            Scrolling       =   1
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel8 
            Height          =   375
            Left            =   30
            OleObjectBlob   =   "invPlanillaInventario.frx":4ECD
            TabIndex        =   30
            Top             =   285
            Width           =   4725
         End
      End
      Begin VB.CommandButton CmdQuitar 
         Caption         =   "quitar"
         Height          =   270
         Left            =   180
         TabIndex        =   23
         ToolTipText     =   "Quitar el articulos seleccionado"
         Top             =   7620
         Width           =   705
      End
      Begin VB.CommandButton CmdExportar 
         Caption         =   "&Exportar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   12210
         TabIndex        =   22
         ToolTipText     =   "Exportar planilla a Excel"
         Top             =   135
         Width           =   1665
      End
      Begin VB.CheckBox ChkTodo 
         Height          =   195
         Left            =   150
         TabIndex        =   19
         ToolTipText     =   "Todo"
         Top             =   1200
         Width           =   210
      End
      Begin VB.ComboBox CboMarca 
         Height          =   315
         Left            =   6840
         Style           =   2  'Dropdown List
         TabIndex        =   4
         Top             =   600
         Width           =   3210
      End
      Begin VB.ComboBox CboTipo 
         Height          =   315
         Left            =   6840
         Style           =   2  'Dropdown List
         TabIndex        =   3
         Top             =   285
         Width           =   3210
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   240
         Index           =   0
         Left            =   5670
         OleObjectBlob   =   "invPlanillaInventario.frx":4F3B
         TabIndex        =   13
         Top             =   330
         Width           =   1125
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   240
         Left            =   6180
         OleObjectBlob   =   "invPlanillaInventario.frx":4FB3
         TabIndex        =   14
         Top             =   660
         Width           =   615
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   240
         Index           =   1
         Left            =   675
         OleObjectBlob   =   "invPlanillaInventario.frx":501B
         TabIndex        =   32
         Top             =   330
         Width           =   1545
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   240
         Index           =   2
         Left            =   660
         OleObjectBlob   =   "invPlanillaInventario.frx":50A5
         TabIndex        =   36
         Top             =   645
         Visible         =   0   'False
         Width           =   1455
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   240
         Index           =   3
         Left            =   10860
         OleObjectBlob   =   "invPlanillaInventario.frx":5113
         TabIndex        =   41
         Top             =   405
         Width           =   2280
      End
      Begin MSComctlLib.ListView LvProductosAntes 
         Height          =   2925
         Left            =   105
         TabIndex        =   43
         Top             =   1380
         Width           =   13650
         _ExtentX        =   24077
         _ExtentY        =   5159
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   0   'False
         HideSelection   =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   6
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "T1500"
            Text            =   "        Codigo"
            Object.Width           =   2646
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "T2000"
            Text            =   "Tipo Producto"
            Object.Width           =   3528
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "T2000"
            Text            =   "Marca"
            Object.Width           =   3528
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Object.Tag             =   "T5000"
            Text            =   "Descripcion"
            Object.Width           =   8819
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   4
            Object.Tag             =   "T1500"
            Text            =   "Cantidad"
            Object.Width           =   2646
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Object.Tag             =   "T1000"
            Text            =   "Cod. Empresa"
            Object.Width           =   2540
         EndProperty
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel9 
         Height          =   240
         Left            =   5970
         OleObjectBlob   =   "invPlanillaInventario.frx":51A3
         TabIndex        =   47
         Top             =   1035
         Width           =   825
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   240
         Index           =   4
         Left            =   10845
         OleObjectBlob   =   "invPlanillaInventario.frx":5219
         TabIndex        =   56
         Top             =   885
         Width           =   2280
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkScan2 
         Height          =   240
         Left            =   7500
         OleObjectBlob   =   "invPlanillaInventario.frx":5293
         TabIndex        =   57
         Top             =   7680
         Width           =   2400
      End
   End
   Begin VB.Timer Timer1 
      Interval        =   10
      Left            =   105
      Top             =   8070
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   30
      OleObjectBlob   =   "invPlanillaInventario.frx":5321
      Top             =   7185
   End
End
Attribute VB_Name = "invPlanillaInventario"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim Sm_FiltroBusca As String
Dim Sm_FiltroBuscaCI As String
Dim Sm_FiltroStock As String
Dim Lp_Contador As Long
Dim Lm_AltoChico As Long
Dim Lm_AltoGrande As Long
Dim Lm_AltoPlanillas As Long
Dim Lm_MinPlanilla As Long
Dim Lm_Top1 As Long
Dim Lm_Top2 As Long
Dim Sm_FiltroTipo As String, Sm_FiltroMarca As String, Sm_Filtrobodega As String
Dim Lp_IdP As Double, Im_Id_Bodega As Double, Sm_Descripcion As String
Private WithEvents cSubLV As cSubclassListView
Attribute cSubLV.VB_VarHelpID = -1

Private Sub GuardaPlanilla()
 Dim Bp_Productos As Boolean
    Dim Lp_NroInv As Long
    Dim Dp_Cantidad As String
    Bp_Productos = False
    If Len(Me.TxtDescripcion) = 0 Then
        MsgBox "Debe ingresar alguna descripcion para la planilla ", vbInformation
        TxtDescripcion.SetFocus
        Exit Sub
    End If
    For i = 1 To LvProductos.ListItems.Count
        If LvProductos.ListItems(i).Checked Then
            Bp_Productos = True
            Exit For
        End If
    Next
    If Not Bp_Productos And Val(txtNroPlanilla) = 0 Then
        MsgBox "Debe seleccionar producto(s)...", vbInformation
        LvProductos.SetFocus
        Exit Sub
    End If
    If CboBodega.ListIndex = -1 Then
        MsgBox "Debe seleccionar Bodega...", vbInformation
        CboBodega.SetFocus
        Exit Sub
    End If
    On Error GoTo FalloGrabacion
    
    DoEvents
    Me.ProgBarGuarda.Visible = True
    
    cn.BeginTrans
            If 1 = 2 Then
          '  If Val(txtNroPlanilla) > 0 Then
                '31 Dic 2013
               ' Sql = "DELETE FROM " & _
                      "inv_toma_inventario, " & _
                      "inv_toma_inventario_detalle " & _
                      "USING inv_toma_inventario " & _
                      "LEFT JOIN inv_toma_inventario_detalle ON (inv_toma_inventario.tmi_id=inv_toma_inventario_detalle.tmi_id) " & _
                      "WHERE inv_toma_inventario.rut_emp='" & SP_Rut_Activo & "' AND inv_toma_inventario.tmi_id=" & txtNroPlanilla
                'Consulta RsTmp, Sql
                'Agregamos transaccion y cambiamos delete indiviudal por tabla
                
                cn.Execute "DELETE FROM inv_toma_inventario " & _
                        "WHERE tmi_id=" & txtNroPlanilla & " AND rut_emp='" & SP_Rut_Activo & "'"
                 Dim LosFiltros As String
                    LosFiltros = ""
                 
                 For i = 1 To Me.LvProductos.ListItems.Count
                    LosFiltros = LosFiltros & LvProductos.ListItems(i) & ","
                 Next
                 If Len(LosFiltros) > 0 Then
                    LosFiltros = " AND pro_codigo IN(" & Mid(LosFiltros, 1, Len(LosFiltros) - 1) & ")"
                End If
                Sql = "DELETE FROM inv_toma_inventario_detalle  " & _
                        "WHERE tmi_id=" & txtNroPlanilla & LosFiltros
                cn.Execute Sql
                Lp_NroInv = txtNroPlanilla
            Else
                Lp_NroInv = UltimoNro("inv_toma_inventario", "tmi_id")
            End If
            'Guardar Planilla
            Sql = "INSERT INTO inv_toma_inventario (tmi_id,tmi_descripcion,tmi_fecha,tmi_hora,usu_nombre,bod_id,rut_emp) " & _
                  "VALUES(" & Lp_NroInv & ",'" & TxtDescripcion & "','" & Format(DtFecha.Value, "YYYY-MM-DD") & "','" & Time & "','" & LogUsuario & "'," & CboBodega.ItemData(CboBodega.ListIndex) & ",'" & SP_Rut_Activo & "')"
            Consulta RsTmp, Sql
            Sql = "INSERT INTO inv_toma_inventario_detalle (tmi_id,pro_codigo,tmi_cantidad) " & _
                 "VALUES "
                 
            ProgBarGuarda.Value = 0
            ProgBarGuarda.Max = LvProductos.ListItems.Count
            For i = 1 To LvProductos.ListItems.Count
                ProgBarGuarda.Value = i
                LvProductos.ListItems(i).SubItems(4) = Replace(LvProductos.ListItems(i).SubItems(4), ",", ".")
                Dp_Cantidad = IIf(Val(LvProductos.ListItems(i).SubItems(4)) > 0, LvProductos.ListItems(i).SubItems(4), 0)
                If txtNroPlanilla = 0 Then
                    If LvProductos.ListItems(i).Checked Then Sql = Sql & "(" & Lp_NroInv & ",'" & LvProductos.ListItems(i) & "'," & Dp_Cantidad & "),"
                Else
                    Sql = Sql & "(" & Lp_NroInv & ",'" & LvProductos.ListItems(i) & "'," & Dp_Cantidad & "),"
                End If
            Next
            Sql = Mid(Sql, 1, Len(Sql) - 1)
            cn.Execute Sql
            txtNroPlanilla = Lp_NroInv
            
            'Cuando creamos una planilla
            'Debemos asegurarnos q los producos se encuentran en la tabla 'pro_stock'
            '26 - Dic 2012
            
            For i = 1 To LvProductos.ListItems.Count
                ProgBarGuarda.Value = i
                Sql = "SELECT Id " & _
                        "FROM pro_stock " & _
                        "WHERE pro_codigo='" & LvProductos.ListItems(i) & "' AND bod_id=" & Me.CboBodega.ItemData(CboBodega.ListIndex) & " AND rut_emp='" & SP_Rut_Activo & "' "
                Consulta RsTmp, Sql
                If RsTmp.RecordCount = 0 Then
                    ' No esta , agregalo
                    Sql = "INSERT INTO pro_stock (pro_codigo,bod_id,rut_emp) " & _
                                "VALUES('" & LvProductos.ListItems(i) & "'," & CboBodega.ItemData(CboBodega.ListIndex) & ",'" & SP_Rut_Activo & "')"
                    cn.Execute Sql
                    'aaaa
                End If
            Next
            
    cn.CommitTrans
    
    
    
    CargaPlanillas
    Lp_IdP = Lp_NroInv
    Im_Id_Bodega = CboBodega.ItemData(CboBodega.ListIndex)
    Sm_Descripcion = TxtDescripcion
    
    'Cuando creamos una planilla
    'Debemos asegurarnos q los producos se encuentran en la tabla 'pro_stock'
    
    
    
    
    
    CargaProductosPlanilla
    CmdGuardar.Enabled = False
    Me.ProgBarGuarda.Visible = False
    MsgBox "Planilla guardada correctamente...", vbInformation
    Exit Sub
    
FalloGrabacion:
    cn.RollbackTrans
     MsgBox "Problema al intentar guardar!!!" & vbNewLine & Err.Number & " - " & Err.Description, vbInformation + vbOKOnly
        
    
End Sub

Private Sub CboMarca_Click()
   If CboMarca.ListIndex = -1 Then Exit Sub
   ' GuardaPlanilla
    If CboMarca.Text = "TODOS" Then
        Sm_FiltroMarca = Empty
    Else
        Sm_FiltroMarca = " AND m.mar_id=" & CboMarca.ItemData(CboMarca.ListIndex) & " "
    End If
    CargaProductos
End Sub

Private Sub CboTipo_Click()
    If CboTipo.ListIndex = -1 Then Exit Sub
  '  GuardaPlanilla
    If CboTipo.Text = "TODOS" Then
        Sm_FiltroTipo = Empty
    Else
        Sm_FiltroTipo = " AND m.tip_id=" & CboTipo.ItemData(CboTipo.ListIndex) & " "
    End If
    CargaProductos
End Sub

Private Sub Check1_Click()
     If LvProductos.ListItems.Count = 0 Then Exit Sub
    For i = 1 To LvProductos.ListItems.Count
        If LvProductos.ListItems(i).Checked Then
            LvProductos.ListItems(i).Checked = False
        Else
            LvProductos.ListItems(i).Checked = True
        End If
    Next
End Sub

Private Sub ChkTodo_Click()
    If LvProductosAntes.ListItems.Count = 0 Then Exit Sub
    For i = 1 To LvProductosAntes.ListItems.Count
        If LvProductosAntes.ListItems(i).Checked Then
            LvProductosAntes.ListItems(i).Checked = False
        Else
            LvProductosAntes.ListItems(i).Checked = True
        End If
    Next
End Sub

Private Sub CmdAgrega_Click()
    Dim bp_Esta As Boolean
    Dim Sp_Encontrados As String
    For i = 1 To LvProductosAntes.ListItems.Count
        If LvProductosAntes.ListItems(i).Checked Then
            If LvProductos.ListItems.Count > 0 Then
                    For X = 1 To LvProductos.ListItems.Count
                        If LvProductosAntes.ListItems(i) = LvProductos.ListItems(X) Then
                            'MsgBox LvProductosAntes.ListItems(i).SubItems(3) & vbNewLine & "Ya se encuentra en la planilla...", vbInformation
                            Sp_Encontrados = Sp_Encontrados & LvProductosAntes.ListItems(i) & " - "
                           bp_Esta = True
                        End If
                    Next
                    If Not bp_Esta Then
                        LvProductos.ListItems.Add , , LvProductosAntes.ListItems(i)
                        For p = 1 To LvProductosAntes.ColumnHeaders.Count - 1
                            LvProductos.ListItems(LvProductos.ListItems.Count).SubItems(p) = LvProductosAntes.ListItems(i).SubItems(p)
                        Next
                    End If
                    
            Else
                LvProductos.ListItems.Add , , LvProductosAntes.ListItems(i)
                For p = 1 To LvProductosAntes.ColumnHeaders.Count - 1
                    LvProductos.ListItems(LvProductos.ListItems.Count).SubItems(p) = LvProductosAntes.ListItems(i).SubItems(p)
                Next
            End If
        End If
    Next
    If bp_Esta Then MsgBox "Los codigos " & vbNewLine & Sp_Encontrados & vbNewLine & "Ya se encontraban....", vbInformation
End Sub

Private Sub cmdBCI_Click()
     If Len(TxtCodInterno) > 2 Then
        Sm_FiltroBuscaCI = " AND m.pro_codigo_interno LIKE '" & Me.TxtCodInterno & "%'"
        CargaProductos
    Else
        Sm_FiltroBuscaCI = ""
    End If
End Sub

Private Sub CmdBuscar_Click()
     If Len(TxtBusqueda) > 2 Then
        Sm_FiltroBusca = " AND m.descripcion LIKE '%" & TxtBusqueda & "%'"
        CargaProductos
    Else
        Sm_FiltroBusca = ""
    End If
End Sub

Private Sub CmdbuscarCodSistema_Click()
        If Len(TxtBSis) > 0 Then
        Sm_FiltroBuscaCI = " AND m.codigo=" & Me.TxtBSis & " "
        CargaProductos
    Else
        Sm_FiltroBuscaCI = ""
    End If
End Sub

Private Sub CmdBuscaSimple_Click()
    Dim itmFound As ListItem
    
    If Len(TxtBusqueda) = 0 Then Exit Sub
     Set itmFound = LvProductos.FindItem(TxtBusqueda, lvwSubItem, , lvwWholeWord)
    If itmFound Is Nothing Then
        MsgBox "No se ha encontrado ninguna coincidencia"
        'Exit Sub
     Else
        itmFound.Selected = True
        itmFound.EnsureVisible
      
       LvProductos.SetFocus
        
    End If
End Sub

Private Sub CmdBuscaStock_Click()

     If Len(TxtFiltroStock) <> 0 Then
        Sm_FiltroStock = " AND (SELECT SUM(sto_stock) FROM pro_stock WHERE pro_stock.pro_codigo= m.codigo) " & Me.TxtFiltroStock & " "
        CargaProductos
    Else
        Sm_FiltroStock = ""
    End If
End Sub

Private Sub CmdExportar_Click()
    Dim tit(2) As String
    If Val(txtNroPlanilla) = 0 Then
        'cSubLV.SowToolTipText "Info", "No hay planilla Activa para ingresar cantidades", TTIconInfo, TTBalloon, False, , , 5000, 0
        MsgBox "No hay planilla activa...", vbInformation
        Exit Sub
    End If
    FraProgreso.Visible = True
    tit(0) = "Planilla para tomar Inventario " & DtFecha & " - " & Time
    tit(1) = "Nro Planilla " & txtNroPlanilla & " - Bodega:" & CboBodega.Text & " - " & Me.TxtDescripcion
    tit(2) = ""
    ExportarNuevo Me.LvProductos, tit, Me, BarraProgreso
    FraProgreso.Visible = False
End Sub

Private Sub CmdGuardar_Click()
    GuardaPlanilla
    

End Sub

Private Sub cmdLimpiaBusqueda_Click()
    TxtBusqueda = ""
    TxtBusqueda.SetFocus
    Sm_FiltroBusca = ""
    CargaProductos
End Sub

Private Sub cmdNuevaPlanilla_Click()
    TxtDescripcion = Empty
    CboBodega.ListIndex = -1
    txtNroPlanilla = 0
    Sm_FiltroTipo = Empty
    Sm_FiltroMarca = Empty
    CargaProductos
    CmdGuardar.Enabled = True
    CmdQuitar.Enabled = True
End Sub

Private Sub CmdQuitar_Click()
    For i = LvProductos.ListItems.Count To 1 Step -1
        If LvProductos.ListItems(i).Checked Then
            If MsgBox("Seguro de quitar el articulo" & vbNewLine & _
             "Codigo:" & LvProductos.ListItems(i) & vbNewLine & "Descripcion:" & _
             LvProductos.ListItems(i).SubItems(1), vbYesNo + vbQuestion, "Quitar Articulo") = vbNo Then Exit Sub
             
             LvProductos.ListItems.Remove LvProductos.ListItems(i).Index
        End If
    Next
End Sub

Private Sub cmdSalir_Click()
    Unload Me
End Sub

Private Sub cmdVerPlanillas_Click()
    'If FrmArticulos.Height = Lm_AltoChico Then
    If cmdVerPlanillas.Caption = "ocultar planillas" Then
        cmdVerPlanillas.Caption = "ver planillas"
        FrmArticulos.Height = Lm_AltoGrande
        
        Me.FrmPlanillas.Height = Lm_MinPlanilla
        LvProductos.Height = Lm_AltoGrande - 1200
        'CmdGuardar.Top = Lm_AltoGrande - 450
        FrmArticulos.Top = Lm_Top2
        CmdAgrega.Visible = True
        LvProductos.Top = LvProductos.Top + LvProductos.Height + 1200
        
    Else
        cmdVerPlanillas.Caption = "ocultar planillas"
        FrmArticulos.Top = Lm_Top1
        FrmArticulos.Height = Lm_AltoGrande - 2400
        LvProductos.Height = Lm_AltoGrande - 4200
       ' CmdGuardar.Top = Lm_AltoChico - 450
       LvProductos.Top = LvProductosAntes.Top
        Me.FrmPlanillas.Height = Lm_AltoPlanillas
        CmdAgrega.Visible = False
        
    End If
    TxtBusquedaCodigo.Top = FrmArticulos.Height - 350
    TxtBusquedaCodigoSistema.Top = FrmArticulos.Height - 350
    SkScan.Top = TxtBusquedaCodigo.Top
    SkScan2.Top = SkScan.Top
End Sub
Private Sub Combo1_Click()
    Dim SpathSkin As String
    
    SpathSkin = App.Path & "\skins\"

    Select Case Combo1.ListIndex
        Case 0: Call setColumnHeader(SpathSkin & Combo1.Text, vbBlack, vbBlack, True, False, Me.BackColor, vbBlack)
        Case 1: Call setColumnHeader(SpathSkin & Combo1.Text, vbBlack, vbBlack, True, False, &HC0FFC0, vbBlue)
        Case 2: Call setColumnHeader(SpathSkin & Combo1.Text, vbWhite, vbYellow, True, False, RGB(150, 190, 217))
        Case 3: Call setColumnHeader(SpathSkin & Combo1.Text, vbWhite, vbBlack, True, False, &HC0FFFF, vbBlack)
    End Select
End Sub



Private Sub TxtBSis_GotFocus()
    En_Foco TxtBSis
End Sub



Private Sub Command1_Click()
    TxtCodInterno = ""
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF4 Then
        TxtBusquedaCodigo.SetFocus
    End If
    If KeyCode = vbKeyF3 Then
        TxtBusquedaCodigoSistema.SetFocus
    End If
End Sub

Private Sub Form_Load()
    Centrar Me
 '   Set cSubLV = New cSubclassListView
  '  cSubLV.SubClassListView LvProductos
    
    Sm_FiltroTipo = Empty
    Sm_FiltroMarca = Empty
    Sm_Filtrobodega = Empty
    Lm_AltoChico = 4470
    Lm_AltoGrande = 8115
    Lm_AltoPlanillas = 2000
    Lm_MinPlanilla = 450
    Lm_Top1 = 3555
    Lm_Top2 = 1935
    
  '  For i = 1 To 4
  '      Combo1.AddItem "Skin" & i
  '  Next
    
  '  Combo1.ListIndex = 2
  '  Combo1.Visible = False
    
    
    FrmArticulos.Height = Lm_AltoGrande
    DtFecha.Value = Date
    LLenarCombo CboTipo, "tip_nombre", "tip_id", "par_tipos_productos", "tip_activo='SI' AND rut_emp='" & SP_Rut_Activo & "'"
    CboTipo.AddItem "TODOS"
    LLenarCombo CboMarca, "mar_nombre", "mar_id", "par_marcas", "mar_activo='SI' AND  rut_emp='" & SP_Rut_Activo & "'"
    CboMarca.AddItem "TODOS"
    LLenarCombo CboBodega, "bod_nombre", "bod_id", "par_bodegas", "bod_activo='SI' AND rut_emp='" & SP_Rut_Activo & "'"
    
    Aplicar_skin Me
    CargaProductos
    CargaPlanillas
End Sub
Private Sub CargaProductos()
    Dim Sp_Planilla As String
    FraLoading.Visible = True
    tmrLoading.Enabled = True
    DoEvents
    ProgLoading.Min = 1
    ProgLoading.Max = 100
    Lp_Contador = 1
    Sp_Planilla = ""
    
    If Val(txtNroPlanilla) > 0 Then
        Sp_Planilla = " AND codigo IN((SELECT pro_codigo " & _
                            "FROM inv_toma_inventario_detalle " & _
                            "WHERE tmi_id=" & Val(txtNroPlanilla) & "))"
        
    End If

    Sql = "SELECT codigo,tip_nombre,mar_nombre,descripcion," & _
                "(SELECT tmi_cantidad " & _
                "FROM inv_toma_inventario_detalle " & _
                "WHERE tmi_id =" & Val(Me.txtNroPlanilla) & " AND pro_codigo = Codigo) cantidad,pro_codigo_interno " & _
          "FROM maestro_productos m " & _
          "INNER JOIN par_tipos_productos USING(tip_id) " & _
          "INNER JOIN par_marcas USING(mar_id) " & _
          "WHERE pro_activo='SI' AND m.pro_inventariable='SI' AND m.rut_emp='" & SP_Rut_Activo & "' " & Sm_FiltroTipo & Sm_FiltroMarca & Sp_Planilla & Sm_FiltroBusca & Sm_FiltroBuscaCI & Sm_FiltroStock
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvProductosAntes, True, True, True, False
    SkCantidadProductos = "REGISTROS: " & LvProductosAntes.ListItems.Count
    FraLoading.Visible = False
    tmrLoading.Enabled = False
End Sub

Private Sub CargaPlanillas()
    Sql = "SELECT i.tmi_fecha,i.tmi_hora,i.tmi_id,tmi_descripcion,bod_nombre,tmi_estado,usu_nombre,i.bod_id " & _
          "FROM inv_toma_inventario i " & _
          "INNER JOIN par_bodegas USING(bod_id) " & _
          "WHERE  i.rut_emp='" & SP_Rut_Activo & "' AND  tmi_estado='ACTIVO' " & _
          "ORDER BY tmi_id DESC"
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvPlanillas, False, True, True, False
         
End Sub


Private Sub Form_Unload(Cancel As Integer)
    Set cSubLV = Nothing
End Sub


Private Sub LvPlanillas_DblClick()
    
    If LvPlanillas.SelectedItem Is Nothing Then Exit Sub
    Lp_IdP = LvPlanillas.SelectedItem.SubItems(2)
    Im_Id_Bodega = LvPlanillas.SelectedItem.SubItems(7)
    Sm_Descripcion = LvPlanillas.SelectedItem.SubItems(3)
    CargaProductosPlanilla
    CmdGuardar.Enabled = False
    CmdQuitar.Enabled = False
End Sub

Private Sub CargaProductosPlanilla()
    Sql = "SELECT codigo,tip_nombre,mar_nombre,descripcion,IF(tmi_cantidad=0,'________',CAST(tmi_cantidad AS CHAR)) cant,pro_codigo_interno " & _
          "FROM inv_toma_inventario_detalle i,maestro_productos m " & _
          "INNER JOIN par_tipos_productos USING(tip_id) " & _
          "INNER JOIN par_marcas USING(mar_id) " & _
          "WHERE  m.rut_emp='" & SP_Rut_Activo & "' AND i.pro_codigo=m.codigo AND i.tmi_id=" & Lp_IdP
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvProductos, True, True, True, False
    txtNroPlanilla = Lp_IdP
    TxtDescripcion = LvPlanillas.SelectedItem.SubItems(3)
    Busca_Id_Combo CboBodega, Im_Id_Bodega

End Sub



Private Sub LvProductos_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If LvProductos.SelectedItem Is Nothing Then Exit Sub
    If KeyCode = 13 Then
            If Val(txtNroPlanilla) = 0 Then Exit Sub
            TxtTemp.Visible = True
            TxtTemp = LvProductos.SelectedItem.SubItems(4)
            TxtTemp.Tag = LvProductos.SelectedItem
            TxtTemp.Top = LvProductos.Top + LvProductos.SelectedItem.Top
            TxtTemp.SetFocus
    End If
End Sub
'ORDENA COLUMNAS
Private Sub LvProductos_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    ordListView ColumnHeader, Me, LvProductos
End Sub



Private Sub Timer1_Timer()
    On Error Resume Next
    DtFecha.SetFocus
    Timer1.Enabled = False
End Sub

Private Sub tmrLoading_Timer()
    
    
    ProgLoading.Value = Lp_Contador
    Lp_Contador = Lp_Contador + 1
    If Lp_Contador = 100 Then Lp_Contador = 1
End Sub

Private Sub TxtBSis_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
End Sub

Private Sub TxtBusqueda_Change()
    Exit Sub
    If Len(TxtBusqueda) > 2 Then
        Sm_FiltroBusca = " AND m.descripcion LIKE '%" & TxtBusqueda & "%'"
        CargaProductos
    Else
        Sm_FiltroBusca = ""
    End If
    
End Sub

Private Sub TxtBusqueda_GotFocus()
    En_Foco TxtBusqueda
End Sub

Private Sub TxtBusqueda_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
    If KeyAscii = 39 Then KeyAscii = 0
End Sub



Private Sub TxtBusquedaCodigo_GotFocus()
    En_Foco TxtBusquedaCodigo
End Sub

Private Sub TxtBusquedaCodigo_KeyPress(KeyAscii As Integer)
    'KeyAscii = SoloNumeros(KeyAscii)
    If KeyAscii = 13 Then
            SkScan = KeyAscii
            If Len(TxtBusquedaCodigo) = 0 Then Exit Sub
            Dim ret As Long
   
             ret = Buscar(LvProductos, TxtBusquedaCodigo, 0, True)
            
             If ret <> 0 Then
                LvProductos.ListItems(ret).Selected = True
                LvProductos.SetFocus
                LvProductos_KeyDown 13, 1
             Else
             
                    ret = Buscar(LvProductos, TxtBusquedaCodigo, 5, True)
            
                    If ret <> 0 Then
                       LvProductos.ListItems(ret).Selected = True
                       LvProductos.SetFocus
                       LvProductos_KeyDown 13, 1
                    Else
                
                    
             
                            MsgBox "Dato no encontrado", vbExclamation
                    End If
             End If
    
    
    End If
End Sub






Private Sub TxtBusquedaCodigoSistema_GotFocus()
    En_Foco TxtBusquedaCodigoSistema
End Sub

Private Sub TxtBusquedaCodigoSistema_KeyPress(KeyAscii As Integer)
    'KeyAscii = SoloNumeros(KeyAscii)
    If KeyAscii = 13 Then
            SkScan = KeyAscii
            If Len(TxtBusquedaCodigoSistema) = 0 Then Exit Sub
            Dim ret As Long
   
             ret = Buscar(LvProductos, TxtBusquedaCodigoSistema, 0, True)
            
             If ret <> 0 Then
                LvProductos.ListItems(ret).Selected = True
                LvProductos.SetFocus
                LvProductos_KeyDown 13, 1
             Else
             
                    ret = Buscar(LvProductos, TxtBusquedaCodigoSistema, 5, True)
            
                    If ret <> 0 Then
                       LvProductos.ListItems(ret).Selected = True
                       LvProductos.SetFocus
                       LvProductos_KeyDown 13, 1
                    Else
                
                    
             
                            MsgBox "Dato no encontrado", vbExclamation
                    End If
             End If
    
    
    End If
End Sub

Private Sub txtDescripcion_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
    If KeyAscii = 39 Then KeyAscii = 0
End Sub
'ordena columnas
Private Sub LvProductosAntes_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    ordListView ColumnHeader, Me, LvProductosAntes
End Sub

Private Sub cSubLV_AfterEdit( _
    ByVal Columna As Integer, _
    Cancel As Boolean, _
    Value As Variant)
    
    Dim lBcolor As Long
    Dim lFColor As Long
    
    Select Case Combo1.ListIndex
        Case 0: lBcolor = &HC0FFFF: lFColor = vbBlack
        Case 1: lBcolor = &HC0FFC0: lFColor = vbBlack
        Case 2: lBcolor = RGB(214, 228, 243): lFColor = vbBlack
        Case 3: lBcolor = &HC0FFC0: lFColor = vbBlack
    End Select
    
    Select Case Columna
        Case 0
           If Not IsNumeric(Value) Then
              Cancel = True
              cSubLV.SowToolTipText "Dato no v�lido", "El valor debe ser un n�mero", TTIconWarning, TTBalloon, False, lFColor, lBcolor, 5000, 0
           End If
        Case 1

           If UCase(Value) <> "ABC" And UCase(Value) <> "XYZ" Then
              Cancel = True
              cSubLV.SowToolTipText "Error", "El valor debe ser ABC o XYZ", TTIconError, TTBalloon, False, lFColor, lBcolor, 5000, 0
              
           End If
        Case 3
           If Not IsDate(Value) Then
              Cancel = True
              cSubLV.SowToolTipText "Error !!!!!!!", "El valor no es una fecha v�lida", TTIconError, TTBalloon, False, lFColor, lBcolor, 5000, 0
           End If
        Case 4
           If Not IsNumeric(Value) Then
                Cancel = True
              cSubLV.SowToolTipText "Error !!!!!!!", "Debe ingresar un valor numerico", TTIconWarning, TTBalloon, False, lFColor, lBcolor, 5000, 0
           End If
           'If Value = "" Then
           '   Cancel = True
           '   cSubLV.SowToolTipText "Error !!!!!!!", "El valor de este campo no puede ser un valor nulo o vacio", TTIconWarning, TTBalloon, False, lFColor, lBcolor, 5000, 0
           'End If
    End Select
    
End Sub

Private Sub cSubLV_beforeEdit(ByVal Columna As Integer, Cancel As Boolean)

    
    If Columna = 2 Or Columna = 0 Or Columna = 3 Or Columna = 1 Then
       Cancel = True
       cSubLV.SowToolTipText "Info", "Esta columna es de solo lectura y no se puede editar", TTIconInfo, TTBalloon, False, , , 5000, 0
    End If
    If Val(txtNroPlanilla) = 0 Then
        cSubLV.SowToolTipText "Info", "No hay planilla Activa para ingresar cantidades", TTIconInfo, TTBalloon, False, , , 5000, 0
        Cancel = True
    End If
End Sub
Sub setColumnHeader( _
    SpathSkin As String, _
    lColorNormal As Long, _
    lColorUp As Long, _
    Optional bIconAlingmentRight As Boolean = False, _
    Optional bTextBold As Boolean = False, _
    Optional BackColortextBox As Long = vbWhite, _
    Optional ForeColorTxtEdit As Long = vbBlack)
    
  '  Exit Sub
    With cSubLV
        .SkinPicture = LoadPicture(SpathSkin & ".bmp")
        .TextNormalColor = lColorNormal
        .TextResalteColor = lColorUp
        .IconAlingmentRight = bIconAlingmentRight
        .HedersFontBlod = bTextBold
        
        .SetPropertyTextBoxEdit BackColortextBox, ForeColorTxtEdit
    End With
End Sub

Private Sub TxtDescripcion_Validate(Cancel As Boolean)
TxtDescripcion = Replace(TxtDescripcion, "'", "")
End Sub

Private Sub TxtTemp_GotFocus()
    On Error Resume Next
    En_Foco Me.ActiveControl
End Sub

Private Sub TxtTemp_KeyPress(KeyAscii As Integer)
    Dim Ip_Letra As Integer
    Ip_Letra = KeyAscii
    If KeyAscii = 27 Then
        TxtTemp.Visible = False
        LvProductos.SetFocus
        Exit Sub
    End If
    KeyAscii = SoloNum(KeyAscii)
    
    If KeyAscii = 13 Then
        If Val(CxP(TxtTemp)) = 0 Then TxtTemp = 0 '  Exit Sub
        
        LvProductos.SelectedItem.Selected = True
        LvProductos.SetFocus
        Me.LvProductos.SelectedItem.SubItems(4) = CxP(TxtTemp)
        TxtTemp = 0
        TxtTemp.Visible = False
        
        Sql = "UPDATE inv_toma_inventario_detalle SET tmi_cantidad=" & LvProductos.SelectedItem.SubItems(4) & " " & _
                "WHERE pro_codigo='" & LvProductos.SelectedItem & "' AND tmi_id=" & txtNroPlanilla
        cn.Execute Sql
    End If

End Sub

Private Sub TxtTemp_LostFocus()
    TxtTemp.Visible = False
End Sub
Public Function SoloNum(CodAscii As Integer)
    If IsNumeric(Chr(CodAscii)) Or CodAscii = 8 Or CodAscii = 44 Then  'pas
    Else
        If CodAscii = 13 Then
            'Sigue
            'SendKeys "{TAB}" 'envia un tab
        ElseIf CodAscii = 46 Then 'pase
        Else
            CodAscii = 0 'No acepta el caracter ingresado
        End If
    End If
    SoloNum = CodAscii
End Function

Function Buscar(LV As ListView, _
                Cadena As String, _
                nCol As Integer, _
                bFraseCompleta As Boolean) As Long
   
   
    Dim i As Long
    Dim iStart As Long
    Dim oItem As ListItem
    
    
    'If Lv.SelectedItem Is Nothing Then
    '   i = 1
    If Not LV.SelectedItem Is Nothing Then
       iStart = LV.SelectedItem.Index + 1
    Else
       iStart = 1
    End If
    
    With LV
    For i = 1 To LV.ListItems.Count
        
        Set oItem = LV.ListItems(i)
        
        Dim sItem As String
        
        If nCol = 0 Then
            sItem = LV.ListItems(i)
        Else
            sItem = oItem.SubItems(nCol)
        End If
        If bFraseCompleta = False Then
           Dim nPos As Integer
           
           nPos = InStr(LCase(sItem), LCase(Cadena))
           If nPos <> 0 Then
              Buscar = oItem.Index
              oItem.EnsureVisible
              Exit For
           End If
        ElseIf bFraseCompleta = True Then
             If LCase(sItem) = LCase(Cadena) Then
              Buscar = oItem.Index
              oItem.EnsureVisible
              Exit For
             End If
        End If
    Next
    End With
    
    Set LV.SelectedItem = Nothing
    
End Function
