VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{28D47522-CF84-11D1-834C-00A0249F0C28}#1.0#0"; "gif89.dll"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{DA729E34-689F-49EA-A856-B57046630B73}#1.0#0"; "progressbar-xp.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form par_productos 
   Caption         =   "Mantenedor de Productos"
   ClientHeight    =   10695
   ClientLeft      =   1125
   ClientTop       =   660
   ClientWidth     =   18825
   LinkTopic       =   "Form1"
   ScaleHeight     =   10695
   ScaleWidth      =   18825
   Begin VB.Timer Timer2 
      Interval        =   500
      Left            =   0
      Top             =   0
   End
   Begin VB.Frame Frame3 
      Caption         =   "Bitacora"
      Height          =   4050
      Left            =   255
      TabIndex        =   10
      Top             =   6360
      Width           =   18060
      Begin TabDlg.SSTab SSTab1 
         Height          =   3570
         Left            =   240
         TabIndex        =   11
         Top             =   345
         Width           =   17595
         _ExtentX        =   31036
         _ExtentY        =   6297
         _Version        =   393216
         Tabs            =   4
         TabsPerRow      =   4
         TabHeight       =   520
         TabCaption(0)   =   "Arriendos/Contratos"
         TabPicture(0)   =   "par_productos.frx":0000
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "LvArriendos"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "SkinLabel2"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "cmdExportaArr"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).ControlCount=   3
         TabCaption(1)   =   "Mantenciones Internas"
         TabPicture(1)   =   "par_productos.frx":001C
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "ListView2"
         Tab(1).ControlCount=   1
         TabCaption(2)   =   "Traslados"
         TabPicture(2)   =   "par_productos.frx":0038
         Tab(2).ControlEnabled=   0   'False
         Tab(2).Control(0)=   "LvTraslados"
         Tab(2).Control(1)=   "Text1"
         Tab(2).Control(2)=   "TxtTraGuia"
         Tab(2).Control(3)=   "TxtTraUsuario"
         Tab(2).Control(4)=   "DtFechaTraslado"
         Tab(2).Control(5)=   "CboOrigen"
         Tab(2).Control(6)=   "CboDestino"
         Tab(2).Control(7)=   "CmdRegistrarTraslado"
         Tab(2).ControlCount=   8
         TabCaption(3)   =   "Listas de Precios"
         TabPicture(3)   =   "par_productos.frx":0054
         Tab(3).ControlEnabled=   0   'False
         Tab(3).Control(0)=   "CmdOkLista"
         Tab(3).Control(1)=   "TxtIdLsd"
         Tab(3).Control(2)=   "TxtNombreLista"
         Tab(3).Control(3)=   "TxtPrecioLista"
         Tab(3).Control(4)=   "CmdMantenedorListadosPrecios"
         Tab(3).Control(5)=   "LvListasPrecios"
         Tab(3).ControlCount=   6
         Begin VB.CommandButton cmdExportaArr 
            Caption         =   "Exportar"
            Height          =   255
            Left            =   2850
            TabIndex        =   82
            Top             =   405
            Width           =   1110
         End
         Begin VB.CommandButton CmdOkLista 
            Caption         =   "Ok"
            Height          =   300
            Left            =   -58410
            TabIndex        =   78
            Top             =   765
            Width           =   585
         End
         Begin VB.TextBox TxtIdLsd 
            BackColor       =   &H80000016&
            Height          =   300
            Left            =   -65745
            Locked          =   -1  'True
            MaxLength       =   50
            TabIndex        =   77
            ToolTipText     =   "Codigo �nico de precio (no modificable)"
            Top             =   765
            Width           =   915
         End
         Begin VB.TextBox TxtNombreLista 
            Height          =   315
            Left            =   -64830
            Locked          =   -1  'True
            TabIndex        =   76
            ToolTipText     =   "Nombre de la lista de precios"
            Top             =   765
            Width           =   4000
         End
         Begin VB.TextBox TxtPrecioLista 
            Height          =   315
            Left            =   -60825
            TabIndex        =   75
            ToolTipText     =   "Precio de esta lista"
            Top             =   765
            Width           =   2400
         End
         Begin VB.CommandButton CmdMantenedorListadosPrecios 
            Caption         =   "Listas de Precios"
            Height          =   630
            Left            =   -74595
            TabIndex        =   74
            Top             =   2175
            Width           =   3045
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
            Height          =   210
            Left            =   285
            OleObjectBlob   =   "par_productos.frx":0070
            TabIndex        =   69
            Top             =   480
            Width           =   3270
         End
         Begin VB.CommandButton CmdRegistrarTraslado 
            Caption         =   "Registrar"
            Height          =   285
            Left            =   -59745
            TabIndex        =   54
            Top             =   615
            Width           =   1830
         End
         Begin VB.ComboBox CboDestino 
            Height          =   315
            Left            =   -62160
            Style           =   2  'Dropdown List
            TabIndex        =   53
            Top             =   630
            Width           =   2400
         End
         Begin VB.ComboBox CboOrigen 
            Height          =   315
            Left            =   -64545
            Style           =   2  'Dropdown List
            TabIndex        =   52
            Top             =   630
            Width           =   2400
         End
         Begin MSComCtl2.DTPicker DtFechaTraslado 
            Height          =   300
            Left            =   -66555
            TabIndex        =   51
            Top             =   630
            Width           =   2000
            _ExtentX        =   3519
            _ExtentY        =   529
            _Version        =   393216
            Format          =   95289345
            CurrentDate     =   42793
         End
         Begin VB.TextBox TxtTraUsuario 
            Height          =   315
            Left            =   -70350
            Locked          =   -1  'True
            TabIndex        =   50
            Top             =   630
            Width           =   3800
         End
         Begin VB.TextBox TxtTraGuia 
            Height          =   315
            Left            =   -72735
            TabIndex        =   49
            Top             =   630
            Width           =   2400
         End
         Begin VB.TextBox Text1 
            BackColor       =   &H80000016&
            Height          =   300
            Left            =   -74760
            Locked          =   -1  'True
            MaxLength       =   50
            TabIndex        =   48
            ToolTipText     =   "Aqui se ingresa el codigo unico"
            Top             =   630
            Width           =   2025
         End
         Begin MSComctlLib.ListView LvArriendos 
            Height          =   2760
            Left            =   270
            TabIndex        =   44
            Top             =   675
            Width           =   16995
            _ExtentX        =   29977
            _ExtentY        =   4868
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            FullRowSelect   =   -1  'True
            GridLines       =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483646
            BackColor       =   -2147483643
            Appearance      =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   14.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            NumItems        =   10
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Object.Tag             =   "N109"
               Text            =   "Id Interno"
               Object.Width           =   0
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Object.Tag             =   "T1000"
               Text            =   "Nro Contrato"
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   2
               Object.Tag             =   "F1000"
               Text            =   "Fecha "
               Object.Width           =   3528
            EndProperty
            BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   3
               Object.Tag             =   "T1000"
               Text            =   "Rut"
               Object.Width           =   3528
            EndProperty
            BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   4
               Object.Tag             =   "T3000"
               Text            =   "Nombre"
               Object.Width           =   12347
            EndProperty
            BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   5
               Object.Tag             =   "T3000"
               Text            =   "Obra"
               Object.Width           =   8819
            EndProperty
            BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   6
               Object.Tag             =   "N109"
               Text            =   "Nro Recepcion"
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   7
               Object.Tag             =   "T2000"
               Text            =   "Nombre Recepcion"
               Object.Width           =   4410
            EndProperty
            BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   8
               Object.Tag             =   "F1000"
               Text            =   "Fecha Recepcion"
               Object.Width           =   3175
            EndProperty
            BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   9
               Text            =   "SUCURSAL"
               Object.Width           =   2540
            EndProperty
         End
         Begin MSComctlLib.ListView ListView2 
            Height          =   1980
            Left            =   -73485
            TabIndex        =   45
            Top             =   540
            Width           =   15540
            _ExtentX        =   27411
            _ExtentY        =   3493
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            FullRowSelect   =   -1  'True
            GridLines       =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483646
            BackColor       =   -2147483643
            Appearance      =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   14.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            NumItems        =   9
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Object.Tag             =   "N109"
               Text            =   "Id Interno"
               Object.Width           =   0
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Object.Tag             =   "T1000"
               Text            =   "Codigo"
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   2
               Object.Tag             =   "T3000"
               Text            =   "Descripci�n"
               Object.Width           =   5292
            EndProperty
            BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   3
               Object.Tag             =   "N102"
               Text            =   "Cantidad"
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   4
               Object.Tag             =   "N100"
               Text            =   "Precio"
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   5
               Object.Tag             =   "N100"
               Text            =   "Total"
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   6
               Object.Tag             =   "N109"
               Text            =   "Stock"
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   7
               Object.Tag             =   "T1000"
               Text            =   "Inventario"
               Object.Width           =   0
            EndProperty
            BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   8
               Text            =   "Precio Costo"
               Object.Width           =   0
            EndProperty
         End
         Begin MSComctlLib.ListView LvTraslados 
            Height          =   2010
            Left            =   -74760
            TabIndex        =   47
            Top             =   990
            Width           =   16905
            _ExtentX        =   29819
            _ExtentY        =   3545
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            FullRowSelect   =   -1  'True
            GridLines       =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483646
            BackColor       =   -2147483643
            Appearance      =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   14.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            NumItems        =   6
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Object.Tag             =   "N109"
               Text            =   "Id Traslado"
               Object.Width           =   3528
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Object.Tag             =   "N109"
               Text            =   "Numero de Guia"
               Object.Width           =   4233
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   2
               Object.Tag             =   "T3000"
               Text            =   "Nombre de usuario"
               Object.Width           =   6703
            EndProperty
            BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   3
               Object.Tag             =   "F1000"
               Text            =   "Fecha Traslado"
               Object.Width           =   3528
            EndProperty
            BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   4
               Object.Tag             =   "T1000"
               Text            =   "Sucursal Origen"
               Object.Width           =   4233
            EndProperty
            BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   5
               Object.Tag             =   "T1000"
               Text            =   "Sucursal Destino"
               Object.Width           =   4233
            EndProperty
         End
         Begin MSComctlLib.ListView LvListasPrecios 
            Height          =   1695
            Left            =   -65730
            TabIndex        =   73
            Top             =   1095
            Width           =   7950
            _ExtentX        =   14023
            _ExtentY        =   2990
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            FullRowSelect   =   -1  'True
            GridLines       =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483646
            BackColor       =   -2147483643
            Appearance      =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   14.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            NumItems        =   4
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Object.Tag             =   "N109"
               Text            =   "Id Lista"
               Object.Width           =   1614
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Object.Tag             =   "T1000"
               Text            =   "Descripcion"
               Object.Width           =   7056
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   2
               Object.Tag             =   "N109"
               Text            =   "Precio"
               Object.Width           =   4233
            EndProperty
            BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   3
               Object.Tag             =   "N109"
               Text            =   "id lista lista"
               Object.Width           =   0
            EndProperty
         End
      End
   End
   Begin MSComctlLib.ListView LvMaquinasUbicacion 
      Height          =   4740
      Left            =   1260
      TabIndex        =   84
      Top             =   10680
      Width           =   21825
      _ExtentX        =   38497
      _ExtentY        =   8361
      View            =   3
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   13
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Object.Tag             =   "N109"
         Object.Width           =   529
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Object.Tag             =   "T1000"
         Text            =   "Codigo Empresa"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Object.Tag             =   "T2000"
         Text            =   "Categoria"
         Object.Width           =   3528
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Object.Tag             =   "T2000"
         Text            =   "Sub-Categoria"
         Object.Width           =   3528
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Object.Tag             =   "T4000"
         Text            =   "Descripcion"
         Object.Width           =   7056
      EndProperty
      BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   5
         Object.Tag             =   "T2000"
         Text            =   "Obs"
         Object.Width           =   3528
      EndProperty
      BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   6
         Object.Tag             =   "T1000"
         Text            =   "Serie"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   7
         Object.Tag             =   "T1500"
         Text            =   "Estado"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   8
         Object.Tag             =   "T1000"
         Text            =   "Nro Contrato"
         Object.Width           =   3528
      EndProperty
      BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   9
         Object.Tag             =   "F1000"
         Text            =   "Fecha Contrato"
         Object.Width           =   3528
      EndProperty
      BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   10
         Object.Tag             =   "T3000"
         Text            =   "Cliente"
         Object.Width           =   5292
      EndProperty
      BeginProperty ColumnHeader(12) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   11
         Object.Tag             =   "T3000"
         Text            =   "Obra"
         Object.Width           =   5292
      EndProperty
      BeginProperty ColumnHeader(13) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   12
         Object.Tag             =   "T1000"
         Text            =   "Sucursal Contrato"
         Object.Width           =   3175
      EndProperty
   End
   Begin VB.CommandButton ExportarUbicacion 
      Caption         =   "Exportar con ubicacion actual"
      Height          =   300
      Left            =   6030
      TabIndex        =   83
      Top             =   6000
      Width           =   2655
   End
   Begin VB.Frame FrmLoad 
      BorderStyle     =   0  'None
      Height          =   1950
      Left            =   7245
      TabIndex        =   56
      Top             =   2325
      Visible         =   0   'False
      Width           =   2805
      Begin ACTIVESKINLibCtl.SkinLabel SkLoad 
         Height          =   270
         Left            =   210
         OleObjectBlob   =   "par_productos.frx":010C
         TabIndex        =   79
         Top             =   120
         Width           =   2415
      End
      Begin GIF89LibCtl.Gif89a Gif89a1 
         Height          =   1320
         Left            =   180
         OleObjectBlob   =   "par_productos.frx":016A
         TabIndex        =   57
         Top             =   420
         Width           =   2445
      End
   End
   Begin VB.Frame FraProgreso 
      Caption         =   "Progreso exportaci�n"
      Height          =   795
      Left            =   600
      TabIndex        =   71
      Top             =   4950
      Visible         =   0   'False
      Width           =   10200
      Begin Proyecto2.XP_ProgressBar BarraProgreso 
         Height          =   330
         Left            =   150
         TabIndex        =   72
         Top             =   315
         Width           =   9900
         _ExtentX        =   17463
         _ExtentY        =   582
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BrushStyle      =   0
         Color           =   16777088
         Scrolling       =   1
         ShowText        =   -1  'True
      End
   End
   Begin VB.CommandButton CmdExportar 
      Caption         =   "Excel"
      Height          =   300
      Left            =   8955
      TabIndex        =   70
      Top             =   6000
      Width           =   1875
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkCantidadProductos 
      Height          =   225
      Left            =   285
      OleObjectBlob   =   "par_productos.frx":01F0
      TabIndex        =   55
      Top             =   6015
      Width           =   3405
   End
   Begin VB.Frame Frame7 
      Caption         =   "Ingreso de Productos"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   6315
      Left            =   10935
      TabIndex        =   14
      Top             =   -105
      Width           =   6870
      Begin VB.TextBox TxtSerieMotor 
         Height          =   315
         Left            =   4545
         Locked          =   -1  'True
         TabIndex        =   85
         Top             =   2100
         Width           =   2130
      End
      Begin VB.TextBox Txt_PrecioVenta 
         Height          =   285
         Left            =   3705
         TabIndex        =   68
         Text            =   "0"
         Top             =   1800
         Width           =   1110
      End
      Begin ACTIVESKINLibCtl.SkinLabel Skn_Precio_Venta 
         Height          =   255
         Left            =   2670
         OleObjectBlob   =   "par_productos.frx":026A
         TabIndex        =   67
         Top             =   1815
         Width           =   1050
      End
      Begin VB.TextBox TxtSerie 
         Height          =   315
         Left            =   1470
         Locked          =   -1  'True
         TabIndex        =   62
         Top             =   2085
         Width           =   1830
      End
      Begin VB.CommandButton CmdEditar 
         Caption         =   "Editar"
         Height          =   240
         Left            =   5520
         TabIndex        =   61
         Top             =   195
         Width           =   1185
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkCreando 
         Height          =   240
         Left            =   3705
         OleObjectBlob   =   "par_productos.frx":02E0
         TabIndex        =   59
         Top             =   495
         Visible         =   0   'False
         Width           =   2085
      End
      Begin VB.TextBox TxtPrecioVta 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   5085
         Locked          =   -1  'True
         TabIndex        =   58
         Text            =   "0"
         Top             =   1815
         Visible         =   0   'False
         Width           =   975
      End
      Begin VB.CommandButton CmdNuevo 
         Caption         =   "Nuevo"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   285
         TabIndex        =   43
         Top             =   5880
         Width           =   2205
      End
      Begin VB.CommandButton CmdGrabar 
         Caption         =   "Guardar"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   4605
         TabIndex        =   42
         Top             =   5895
         Width           =   2205
      End
      Begin VB.Frame Frame6 
         Caption         =   "�Cuando ingreso?"
         Height          =   720
         Left            =   345
         TabIndex        =   38
         Top             =   3135
         Width           =   6270
         Begin VB.TextBox TxtDetalleIngreso 
            Height          =   345
            Left            =   1815
            Locked          =   -1  'True
            TabIndex        =   41
            Text            =   "FACTURA 3232389 - CASE"
            Top             =   300
            Width           =   4200
         End
         Begin MSComCtl2.DTPicker DtFechaIngreso 
            Height          =   360
            Left            =   435
            TabIndex        =   40
            Top             =   300
            Width           =   1380
            _ExtentX        =   2434
            _ExtentY        =   635
            _Version        =   393216
            Enabled         =   0   'False
            Format          =   95289345
            CurrentDate     =   42779
         End
      End
      Begin VB.Frame Frame4 
         Caption         =   "Estado actual"
         Height          =   1620
         Left            =   345
         TabIndex        =   37
         Top             =   3915
         Width           =   6360
         Begin MSComCtl2.DTPicker DtFechaBaja 
            Height          =   330
            Left            =   2490
            TabIndex        =   88
            Top             =   870
            Width           =   1185
            _ExtentX        =   2090
            _ExtentY        =   582
            _Version        =   393216
            Format          =   95289345
            CurrentDate     =   43524
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkMotivoBaja 
            Height          =   285
            Left            =   165
            OleObjectBlob   =   "par_productos.frx":035C
            TabIndex        =   81
            Top             =   1275
            Visible         =   0   'False
            Width           =   645
         End
         Begin VB.TextBox TxtMotivoBaja 
            Height          =   345
            Left            =   855
            Locked          =   -1  'True
            TabIndex        =   80
            Top             =   1215
            Visible         =   0   'False
            Width           =   5325
         End
         Begin VB.ComboBox CboBodegaActual 
            Height          =   315
            ItemData        =   "par_productos.frx":03C6
            Left            =   3885
            List            =   "par_productos.frx":03D0
            Style           =   2  'Dropdown List
            TabIndex        =   65
            Top             =   855
            Width           =   2355
         End
         Begin VB.ComboBox CboEstado 
            Height          =   315
            ItemData        =   "par_productos.frx":03EE
            Left            =   120
            List            =   "par_productos.frx":03F8
            Locked          =   -1  'True
            Style           =   2  'Dropdown List
            TabIndex        =   46
            Top             =   870
            Width           =   2355
         End
         Begin VB.TextBox txtProEstado 
            Height          =   345
            Left            =   105
            Locked          =   -1  'True
            TabIndex        =   39
            Text            =   "EN BODEGA TEMUCO"
            Top             =   270
            Visible         =   0   'False
            Width           =   6045
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
            Height          =   255
            Index           =   2
            Left            =   120
            OleObjectBlob   =   "par_productos.frx":0416
            TabIndex        =   64
            Top             =   690
            Width           =   1695
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkBodegaInicio 
            Height          =   255
            Left            =   3930
            OleObjectBlob   =   "par_productos.frx":0480
            TabIndex        =   66
            Top             =   615
            Width           =   1695
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkFechaBaja 
            Height          =   255
            Left            =   2475
            OleObjectBlob   =   "par_productos.frx":04F0
            TabIndex        =   87
            Top             =   675
            Width           =   1695
         End
      End
      Begin VB.TextBox TxtCodigoInterno 
         Height          =   285
         Left            =   1470
         Locked          =   -1  'True
         TabIndex        =   26
         Top             =   2775
         Width           =   2655
      End
      Begin VB.ComboBox CboHabilitado 
         Height          =   315
         ItemData        =   "par_productos.frx":0562
         Left            =   3150
         List            =   "par_productos.frx":056C
         Locked          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   25
         Top             =   2400
         Width           =   765
      End
      Begin VB.TextBox TxtPrecioCostoSFlete 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   1485
         Locked          =   -1  'True
         TabIndex        =   24
         Text            =   "0"
         Top             =   1785
         Width           =   975
      End
      Begin VB.CommandButton CmdFamilias 
         Caption         =   "Categoria"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   4725
         TabIndex        =   23
         ToolTipText     =   "Ingrese al Mantenedor de Tipos de Art�culos"
         Top             =   825
         Width           =   1290
      End
      Begin VB.ComboBox CboInventariable 
         Height          =   315
         ItemData        =   "par_productos.frx":0578
         Left            =   1485
         List            =   "par_productos.frx":0582
         Locked          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   22
         Top             =   2415
         Width           =   765
      End
      Begin VB.Frame Frame8 
         Caption         =   "Impuestos del Producto"
         Enabled         =   0   'False
         Height          =   3210
         Left            =   7095
         TabIndex        =   20
         Top             =   1035
         Width           =   4545
         Begin MSComctlLib.ListView ListView1 
            Height          =   2670
            Left            =   255
            TabIndex        =   21
            Top             =   390
            Width           =   4110
            _ExtentX        =   7250
            _ExtentY        =   4710
            View            =   3
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            FullRowSelect   =   -1  'True
            GridLines       =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   0
            NumItems        =   3
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Text            =   "Id"
               Object.Width           =   1058
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Text            =   "Descripcion"
               Object.Width           =   4260
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   2
               Object.Tag             =   "N102"
               Text            =   "Factor"
               Object.Width           =   1720
            EndProperty
         End
      End
      Begin VB.TextBox TxtCodigo 
         BackColor       =   &H80000016&
         Height          =   285
         Left            =   1485
         MaxLength       =   50
         TabIndex        =   19
         ToolTipText     =   "Aqui se ingresa el codigo unico"
         Top             =   510
         Width           =   2055
      End
      Begin VB.TextBox TxtDescripcion 
         Height          =   315
         Left            =   1485
         Locked          =   -1  'True
         TabIndex        =   18
         Top             =   1455
         Width           =   5220
      End
      Begin VB.ComboBox CboFtipo 
         Height          =   315
         Left            =   1485
         Locked          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   17
         Top             =   825
         Width           =   3255
      End
      Begin VB.ComboBox CboFMarca 
         Height          =   315
         ItemData        =   "par_productos.frx":058E
         Left            =   1485
         List            =   "par_productos.frx":0590
         Locked          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   16
         Top             =   1155
         Width           =   3255
      End
      Begin VB.CommandButton CmdMarcas 
         Caption         =   "Sub Categoria"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   4740
         TabIndex        =   15
         ToolTipText     =   "Ingrese al Mantenedor de Marcas de Art�culos"
         Top             =   1155
         Width           =   1290
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel16 
         Height          =   240
         Index           =   0
         Left            =   390
         OleObjectBlob   =   "par_productos.frx":0592
         TabIndex        =   27
         Top             =   840
         Width           =   960
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel11 
         Height          =   225
         Left            =   2910
         OleObjectBlob   =   "par_productos.frx":0602
         TabIndex        =   28
         Top             =   2670
         Width           =   300
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
         Height          =   315
         Left            =   75
         OleObjectBlob   =   "par_productos.frx":0660
         TabIndex        =   29
         Top             =   1185
         Width           =   1335
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   255
         Left            =   120
         OleObjectBlob   =   "par_productos.frx":06D8
         TabIndex        =   30
         Top             =   510
         Width           =   1290
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   255
         Index           =   0
         Left            =   180
         OleObjectBlob   =   "par_productos.frx":0752
         TabIndex        =   31
         Top             =   1485
         Width           =   1215
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel12 
         Height          =   195
         Left            =   240
         OleObjectBlob   =   "par_productos.frx":07C6
         TabIndex        =   32
         Top             =   2430
         Width           =   1095
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel13 
         Height          =   255
         Left            =   210
         OleObjectBlob   =   "par_productos.frx":083E
         TabIndex        =   33
         Top             =   1800
         Width           =   1170
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel15 
         Height          =   195
         Left            =   1935
         OleObjectBlob   =   "par_productos.frx":08B4
         TabIndex        =   34
         Top             =   2430
         Width           =   1095
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   255
         Index           =   3
         Left            =   -330
         OleObjectBlob   =   "par_productos.frx":0926
         TabIndex        =   35
         Top             =   2775
         Width           =   1695
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkCreando2 
         Height          =   240
         Left            =   4215
         OleObjectBlob   =   "par_productos.frx":09A0
         TabIndex        =   60
         Top             =   2790
         Visible         =   0   'False
         Width           =   2085
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   255
         Index           =   1
         Left            =   165
         OleObjectBlob   =   "par_productos.frx":0A1C
         TabIndex        =   63
         Top             =   2115
         Width           =   1215
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   255
         Index           =   4
         Left            =   3465
         OleObjectBlob   =   "par_productos.frx":0A84
         TabIndex        =   86
         Top             =   2130
         Width           =   990
      End
      Begin VB.Label Label4 
         Alignment       =   2  'Center
         Caption         =   "Para poder grabar el registro debe completar todos los campos"
         Height          =   780
         Left            =   5415
         TabIndex        =   36
         Top             =   2715
         Width           =   1935
      End
   End
   Begin VB.Timer Timer1 
      Interval        =   100
      Left            =   18510
      Top             =   6195
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   18555
      OleObjectBlob   =   "par_productos.frx":0AF8
      Top             =   7275
   End
   Begin VB.Frame Frame2 
      Caption         =   "Busqueda"
      Height          =   1110
      Left            =   255
      TabIndex        =   1
      Top             =   -15
      Width           =   10590
      Begin VB.Frame Frame5 
         Caption         =   "Bodega"
         Height          =   810
         Left            =   8655
         TabIndex        =   12
         Top             =   180
         Width           =   1905
         Begin VB.ComboBox CboBodega 
            Height          =   315
            Left            =   45
            Style           =   2  'Dropdown List
            TabIndex        =   13
            Top             =   390
            Width           =   1800
         End
      End
      Begin VB.TextBox TxtBusca 
         Height          =   375
         Left            =   120
         TabIndex        =   9
         Top             =   240
         Width           =   2925
      End
      Begin VB.OptionButton OpDescripcion 
         Caption         =   "Descripcion"
         Height          =   255
         Left            =   135
         TabIndex        =   8
         Top             =   705
         Value           =   -1  'True
         Width           =   1185
      End
      Begin VB.OptionButton OpCodigo 
         Caption         =   "Codigo"
         Height          =   255
         Left            =   1290
         TabIndex        =   7
         Top             =   720
         Width           =   855
      End
      Begin VB.CommandButton CmdTodosPro 
         Caption         =   "Todos"
         Height          =   375
         Left            =   2145
         TabIndex        =   6
         Top             =   660
         Width           =   900
      End
      Begin VB.Frame FraMarca 
         Caption         =   "Sub-Categoria"
         Height          =   810
         Left            =   5775
         TabIndex        =   4
         Top             =   180
         Width           =   2835
         Begin VB.ComboBox CboMarca 
            Height          =   315
            Left            =   90
            Style           =   2  'Dropdown List
            TabIndex        =   5
            Top             =   375
            Width           =   2700
         End
      End
      Begin VB.Frame FraFamilia 
         Caption         =   "Categoria"
         Height          =   810
         Left            =   3135
         TabIndex        =   2
         Top             =   180
         Width           =   2655
         Begin VB.ComboBox CboTipoProducto 
            Height          =   315
            Left            =   60
            Style           =   2  'Dropdown List
            TabIndex        =   3
            Top             =   390
            Width           =   2550
         End
      End
   End
   Begin MSComctlLib.ListView LvDetalle 
      Height          =   4740
      Left            =   240
      TabIndex        =   0
      Top             =   1215
      Width           =   10575
      _ExtentX        =   18653
      _ExtentY        =   8361
      View            =   3
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   19
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Object.Tag             =   "N109"
         Object.Width           =   529
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Object.Tag             =   "T1000"
         Text            =   "Codigo Empresa"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Object.Tag             =   "T2000"
         Text            =   "Categoria"
         Object.Width           =   3528
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Object.Tag             =   "T2000"
         Text            =   "Sub-Categoria"
         Object.Width           =   3528
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Object.Tag             =   "T4000"
         Text            =   "Descripcion"
         Object.Width           =   7056
      EndProperty
      BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   5
         Object.Tag             =   "N102"
         Text            =   "Pre. Compra"
         Object.Width           =   1676
      EndProperty
      BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   6
         Object.Tag             =   "N102"
         Text            =   "Margen"
         Object.Width           =   1676
      EndProperty
      BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   7
         Object.Tag             =   "N102"
         Text            =   "Pre. Venta"
         Object.Width           =   2117
      EndProperty
      BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   8
         Object.Tag             =   "N102"
         Text            =   "Utilidad"
         Object.Width           =   2117
      EndProperty
      BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   9
         Object.Tag             =   "N102"
         Text            =   "Stock Bodega"
         Object.Width           =   2117
      EndProperty
      BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   10
         Object.Tag             =   "N102"
         Text            =   "En Arriendo"
         Object.Width           =   2117
      EndProperty
      BeginProperty ColumnHeader(12) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   11
         Object.Tag             =   "N102"
         Text            =   "Stock_Total"
         Object.Width           =   2117
      EndProperty
      BeginProperty ColumnHeader(13) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   12
         Object.Tag             =   "T2000"
         Text            =   "Bodega"
         Object.Width           =   4586
      EndProperty
      BeginProperty ColumnHeader(14) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   13
         Object.Tag             =   "T1500"
         Text            =   "Ubicacion Bodega"
         Object.Width           =   3528
      EndProperty
      BeginProperty ColumnHeader(15) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   14
         Object.Tag             =   "T2000"
         Text            =   "Obs"
         Object.Width           =   3528
      EndProperty
      BeginProperty ColumnHeader(16) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   15
         Object.Tag             =   "T1000"
         Text            =   "Inventariable"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(17) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   16
         Object.Tag             =   "T1000"
         Text            =   "Serie"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(18) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   17
         Object.Tag             =   "N109"
         Text            =   "id"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(19) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   18
         Text            =   "Estado Actual"
         Object.Width           =   2540
      EndProperty
   End
End
Attribute VB_Name = "par_productos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim RsProductos As Recordset
Dim s_Sql As String
Dim sql2 As String
Dim Sm_filtro As String
Dim Sm_FiltroMarca As String
Dim Sm_FiltroTipo  As String
Dim Sm_Filtrobodega As String
Dim Bm_GrillaCargada As Boolean
Dim Sp_CodigoProveedor As String
Dim Sp_SoloRut As String
Dim Sm_CodInternos As String * 2
Dim Sm_FiltroCodInterno As String
Dim Sm_TipoF As String
Dim Sm_Sucursal As String



Private Sub CboBodega_Click()
    If CboBodega.Text = "TODAS" Then
        Sm_Sucursal = ""
    Else
        Sm_Sucursal = " AND p.bod_id=" & CboBodega.ItemData(CboBodega.ListIndex) & " "
    End If
    Grilla
End Sub

Private Sub CboEstado_Click()
    If CboEstado.Text = "DADO DE BAJA" Then
        SkMotivoBaja.Visible = True
        TxtMotivoBaja.Visible = True
        TxtMotivoBaja.Locked = False
        TxtMotivoBaja.Enabled = True
        SkFechaBaja.Visible = True
        DtFechaBaja.Visible = True
    Else
        SkMotivoBaja.Visible = False
        TxtMotivoBaja.Visible = False
        SkFechaBaja.Visible = False
        DtFechaBaja.Visible = False
    End If
End Sub

Private Sub CboMarca_Click()
    If Not Bm_GrillaCargada Then Exit Sub
    If CboMarca.Text = "TODOS" Then
        Sm_FiltroMarca = Empty
    Else
        Sm_FiltroMarca = " AND p.mar_id=" & CboMarca.ItemData(CboMarca.ListIndex) & " "
    End If
    
    Grilla
End Sub

Private Sub CboTipoProducto_Click()
    If Not Bm_GrillaCargada Then Exit Sub
    If CboTipoProducto.Text = "TODOS" Then
        Sm_FiltroTipo = Empty
        CboMarca.Clear
        CboMarca.AddItem "TODOS"
    Else
         LLenarCombo CboMarca, "mar_nombre", "mar_id", "par_marcas", "tip_id=" & CboTipoProducto.ItemData(CboTipoProducto.ListIndex) & " AND mar_activo='SI' AND  rut_emp='" & SP_Rut_Activo & "'", "mar_nombre"
         Sm_FiltroMarca = ""
        Sm_FiltroTipo = " AND p.tip_id=" & CboTipoProducto.ItemData(CboTipoProducto.ListIndex) & " "
        
    End If
    Grilla
End Sub

Private Sub BuscarTexto()
    Sm_filtro = Empty
    Sm_FiltroCodInterno = Empty
    If Len(TxtBusca) > 0 Then
        If Me.OpDescripcion.Value Then
            Sm_filtro = "AND descripcion LIKE '%" & TxtBusca & "%' "
        End If
        If Me.OpCodigo Then
        '    Sm_filtro = "AND codigo LIKE '%" & TxtBusca & "%' "
            If Sm_CodInternos = "SI" Then
                Sm_FiltroCodInterno = " AND pro_codigo_interno LIKE '%" & TxtBusca & "%' "
            End If
            If SP_Rut_Activo = "76.115.474-5" Then
                '05 agosto 2019
                'Redic
                  Sm_FiltroCodInterno = "AND pro_codigo_interno LIKE '%" & TxtBusca & "%' "
                 
                
            End If
            
            
        End If
        Grilla
    Else
        Grilla
    End If
End Sub

Private Sub CmdEditar_Click()
    CboFtipo.Locked = False
    CboFMarca.Locked = False
    Me.TxtDescripcion.Locked = False
    TxtPrecioCostoSFlete.Locked = False
    TxtPrecioVta.Locked = False
    CboInventariable.Locked = False
    Me.CboHabilitado.Locked = False
    TxtCodigoInterno.Locked = False
    DtFechaIngreso.Enabled = True
    TxtDetalleIngreso.Locked = False
    txtProEstado.Locked = False
    CboEstado.Locked = False
    CmdGrabar.Enabled = True
    TxtSerie.Locked = False
    TxtSerieMotor.Locked = False
  '  Me.CboBodegaActual.Visible = False
  '  Me.SkBodegaInicio.Visible = False
    Me.Txt_PrecioVenta.Locked = False
    CboEstado.Enabled = True
    
End Sub
Private Sub Inhabilitar()
    TxtSerieMotor.Locked = True
    CboFtipo.Locked = True
    CboFMarca.Locked = True
    Me.TxtDescripcion.Locked = True
    TxtPrecioCostoSFlete.Locked = True
    TxtPrecioVta.Locked = True
    CboInventariable.Locked = True
    Me.CboHabilitado.Locked = True
    TxtCodigoInterno.Locked = True
    DtFechaIngreso.Enabled = True
    TxtDetalleIngreso.Locked = True
    'txtProEstado.Locked = True
    CboEstado.Locked = True
    CmdGrabar.Enabled = False
    TxtSerie.Locked = True
  '  Me.CboBodegaActual.Visible = False
   ' Me.SkBodegaInicio.Visible = False
    Me.Txt_PrecioVenta.Locked = True
    Me.CboBodegaActual.Visible = False
    Me.SkBodegaInicio.Visible = False
End Sub



Private Sub cmdExportaArr_Click()
    Dim tit(2) As String
    FraProgreso.Visible = True
        tit(0) = "Arrindos de producto:" & LvDetalle.SelectedItem.SubItems(1)
        tit(1) = "Nombre maquina:" & LvDetalle.SelectedItem.SubItems(4)
        tit(2) = "Fecha informe: " & Date & " " & Now
        ExportarNuevo Me.LvArriendos, tit, Me, BarraProgreso
    FraProgreso.Visible = False
End Sub

Private Sub CmdExportar_Click()
    Dim tit(2) As String
    Dim Sp_Codigos As String
    If LvDetalle.ListItems.Count = 0 Then Exit Sub
    
    
    'Comprobaremos cantidad de columnas
    If LvDetalle.ColumnHeaders.Count = 18 Then
        FrmLoad.Visible = True
        SkLoad = "Cargando Precios..."
        DoEvents
        
        'Agregaremos columnas de listas de precios
        Sql = "SELECT lst_id,lst_nombre " & _
                "FROM par_lista_precios " & _
                "WHERE lst_activo='SI'"
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
            RsTmp.MoveFirst
            Do While Not RsTmp.EOF
                
                LvDetalle.ColumnHeaders.Add , , RsTmp!lst_nombre
                SkLoad = "Cargando Precios..." & RsTmp!lst_nombre
                Sp_Codigos = ""
                For i = 1 To LvDetalle.ListItems.Count
                    Sp_Codigos = Sp_Codigos & LvDetalle.ListItems(i).SubItems(17) & ","
                    
                
                Next
                If Len(Sp_Codigos) > 0 Then
                    Sp_Codigos = Mid(Sp_Codigos, 1, Len(Sp_Codigos) - 1)
                    Sql = "SELECT id,lsd_precio " & _
                            "FROM par_lista_precios_detalle " & _
                            "WHERE lst_id=" & RsTmp!lst_id & " AND id IN(" & Sp_Codigos & ")"
                    Consulta RsTmp2, Sql
                    If RsTmp2.RecordCount > 0 Then
                        RsTmp2.MoveFirst
                        Do While Not RsTmp2.EOF
                            For i = 1 To LvDetalle.ListItems.Count
                                If LvDetalle.ListItems(i).SubItems(17) = RsTmp2!ID Then
                                    LvDetalle.ListItems(i).SubItems(LvDetalle.ColumnHeaders.Count - 1) = "" & RsTmp2!lsd_precio
                                End If
                            Next
                            
                            RsTmp2.MoveNext
                        Loop
                    
                    End If
                
                
                
                
                End If
                
                RsTmp.MoveNext
            Loop
        End If
        FrmLoad.Visible = False
        
    
    End If
    
    FraProgreso.Visible = True
    tit(0) = Me.Caption & " " & DtFecha & " - " & Time
    tit(1) = "BODEGA:" & CboBodega.Text
    tit(2) = "CATEGORIA:" & Me.CboTipoProducto.Text & " SUB-CATEGORIA:" & CboMarca.Text
    ExportarNuevo LvDetalle, tit, Me, BarraProgreso
    FraProgreso.Visible = False
End Sub

Private Sub CmdFamilias_Click()
      'Aqui llamamos a un mantenedor
    Sql = "SELECT mav_id,mav_nombre,mav_activo,mav_tabla,mav_consulta,mav_caption,mav_caption_frm,man_editable, " & _
                        "man_dep_id,man_dep_nombre,man_dep_tabla,man_dep_activo,man_dep_titulo " & _
                  "FROM sis_mantenedor_simple " & _
                  "WHERE man_activo='SI' AND man_id=8"
            Consulta RsTmp2, Sql
            If RsTmp2.RecordCount > 0 Then
                With Mantenedor_Dependencia
                    .S_Id = RsTmp2!mav_id
                    .S_Nombre = RsTmp2!mav_nombre
                    .S_Activo = RsTmp2!mav_activo
                    .S_tabla = RsTmp2!mav_tabla
                    .S_Consulta = RsTmp2!mav_consulta
                    .B_Editable = IIf(RsTmp2!man_editable = "SI", True, False)
                    .S_id_dep = RsTmp2!man_dep_id
                    .S_nombre_dep = RsTmp2!man_dep_nombre
                    .S_Activo_dep = RsTmp2!man_dep_activo
                    .S_tabla_dep = RsTmp2!man_dep_tabla
                    .S_titulo_dep = RsTmp2!man_dep_titulo
                    .CmdBusca.Visible = True
                    .Caption = "SubCategoria"
'                    .FrmMantenedor.Caption = RsTmp2!mav_caption_frm

                    .Show 1
                End With
            End If
    LLenarCombo CboTipoProducto, "tip_nombre", "tip_id", "par_tipos_productos", "tip_activo='SI' AND rut_emp='" & SP_Rut_Activo & "'", "tip_nombre"
End Sub


Private Sub CmdGrabar_Click()
    Dim Lp_Id_Producto As Long
    Dim Mensaje As String
    If CboFMarca.ListIndex = -1 Then
        MsgBox "Seleccione Categoria...", vbInformation + vbOKOnly
        CboFMarca.SetFocus
        Exit Sub
    End If
    
    If CboFtipo.ListIndex = -1 Then
        MsgBox "Seleccione Sub-Categoria...", vbInformation + vbOKOnly
        CboFtipo.SetFocus
        Exit Sub
    End If
    
    If Len(TxtDescripcion) = 0 Then
        MsgBox "Ingrese descripcion...", vbInformation + vbOKOnly
        TxtDescripcion.SetFocus
        Exit Sub
    End If
    If CboEstado.ListIndex = -1 Then CboEstado.ListIndex = 0
    
   ' If Val(TxtPrecioVta) = 0 Then
    '    MsgBox "Ingrese precio de Venta...", vbInformation + vbOKOnly
    ''    TxtPrecioVta.SetFocus
      '  Exit Sub
    
  '  End If
    sp_Actualiza = ""
    If SkCreando.Visible Then
        'Nuevo
            
            Lp_Id_Producto = UltimoNro("maestro_productos", "id")
            
             sp_Actualiza = "INSERT INTO maestro_productos SET id=" & Lp_Id_Producto & ", codigo='" & TxtCodigo & "', marca='" & CboMarca.Text & "'," & "descripcion='" & TxtDescripcion & "'," & _
                                                "precio_compra=" & CxP(TxtPrecioCostoSFlete) & "," & _
                                                "precio_venta=" & CxP(Me.Txt_PrecioVenta) & "," & _
                                                "porciento_utilidad=0," & _
                                                "/* precio_venta=" & CxP(TxtPrecioVta) & ",*/" & _
                                                "stock_actual=1," & _
                                                "stock_critico=1," & _
                                                "ubicacion_bodega='" & txtUbicacionBodega & "'," & _
                                                "comentario='" & TxtComentario & "', " & _
                                                "bod_id=" & Me.CboBodegaActual.ItemData(CboBodegaActual.ListIndex) & ", " & _
                                                "mar_id=" & CboFMarca.ItemData(CboFMarca.ListIndex) & "," & _
                                                "pro_inventariable='" & Me.CboInventariable.Text & "'," & _
                                                "rut_emp='" & SP_Rut_Activo & "'," & _
                                                "tip_id=" & CboFtipo.ItemData(CboFtipo.ListIndex) & ", " & _
                                                "ume_id=1," & _
                                                "pro_precio_sin_flete=0," & _
                                                "pro_precio_flete=0," & _
                                                "pro_activo='" & CboHabilitado.Text & "', " & _
                                                "pro_codigo_interno='" & TxtCodigoInterno & "', " & _
                                                "pro_tipo='F'," & _
                                                "pro_estado='" & Me.CboEstado & "',pro_serie_motor='" & TxtSerieMotor & "'," & _
                                                "serie='" & TxtSerie & "'," & _
                                                "pro_fecha_ingreso='" & Fql(Me.DtFechaIngreso) & "'," & _
                                                "pro_info_adicional='" & Me.TxtDetalleIngreso & "'," & _
                                                "est_id=" & CboEstado.ItemData(CboEstado.ListIndex) & ",pro_usuario_creacion='" & LogUsuario & "'"
                                                Mensaje = "Guardado " & vbNewLine & "Nuevo producto"
                                                SkCreando.Visible = False
                                                SkCreando2.Visible = False
                                                Me.CboBodegaActual.Visible = False
                                                Me.SkBodegaInicio.Visible = False
            cn.Execute sp_Actualiza
             sp_Actualiza = ""
             'Agregar precios para listas de precios
             Sql = "INSERT INTO par_lista_precios_detalle (lst_id,id,lsd_precio) " & _
                    "SELECT lst_id," & Lp_Id_Producto & "," & Txt_PrecioVenta & " " & _
                        "FROM par_lista_precios"
            cn.Execute Sql
            
            ListaPrecios2 Lp_Id_Producto
            MsgBox "Recuerde asiganar los precios por cada sucursal...", vbInformation
            Me.LvListasPrecios.SetFocus
             
                    '"WHERE codigo='" & TxtCodigo & "' AND rut_emp='" & SP_Rut_Activo & "'"
    
    Else
            If CboEstado.ListIndex = -1 Then
                MsgBox "Seleccione estado...", vbInformation
                CboEstado.SetFocus
                Exit Sub
            End If
    

            sp_Actualiza = "UPDATE maestro_productos SET marca='" & CboMarca.Text & "'," & "descripcion='" & TxtDescripcion & "'," & _
                                                "precio_compra=" & CxP(TxtPrecioCostoSFlete) & "," & _
                                                "precio_venta=" & CxP(Me.Txt_PrecioVenta) & "," & _
                                                "porciento_utilidad=0," & _
                                                "/* precio_venta=" & CxP(TxtPrecioVta) & ",*/" & _
                                                "stock_actual=1," & _
                                                "stock_critico=1," & _
                                                "ubicacion_bodega='" & txtUbicacionBodega & "'," & _
                                                "comentario='" & TxtComentario & "', " & _
                                                "bod_id=1," & _
                                                "mar_id=" & CboFMarca.ItemData(CboFMarca.ListIndex) & "," & _
                                                "pro_inventariable='" & Me.CboInventariable.Text & "'," & _
                                                "rut_emp='" & SP_Rut_Activo & "'," & _
                                                "tip_id=" & CboFtipo.ItemData(CboFtipo.ListIndex) & ", " & _
                                                "ume_id=1," & _
                                                "pro_precio_sin_flete=0," & _
                                                "pro_precio_flete=0," & _
                                                "pro_activo='" & CboHabilitado.Text & "', " & _
                                                "pro_codigo_interno='" & TxtCodigoInterno & "', " & _
                                                "pro_tipo='F',pro_serie_motor='" & TxtSerieMotor & "', " & _
                                                "serie='" & TxtSerie & "'," & _
                                                "pro_estado='" & Me.CboEstado.Text & "',pro_dado_de_baja_motivo='" & TxtMotivoBaja & "'," & _
                                                "est_id=" & CboEstado.ItemData(CboEstado.ListIndex) & ", " & _
                                                "pro_fecha_baja='" & Fql(DtFechaBaja) & "' " & _
                    "WHERE codigo='" & TxtCodigo & "' AND rut_emp='" & SP_Rut_Activo & "'"
                    Mensaje = "Guardado " & vbNewLine & "Editado codigo: " & TxtCodigo
            Debug.Print sp_Actualiza
            cn.Execute sp_Actualiza
            sp_Actualiza = ""
    End If
            

    MsgBox Mensaje, vbInformation
    Me.CmdGrabar.Enabled = False
    Inhabilitar
End Sub

Private Sub CmdMantenedorListadosPrecios_Click()
          'Aqui llamamos a un mantenedor
    With Mantenedor_Simple
        .S_Id = "lst_id"
        .S_Nombre = "lst_nombre"
        .S_Activo = "lst_activo"
        .S_tabla = "par_lista_precios"
        .S_RutEmpresa = "SI"
        .S_Consulta = "SELECT " & .S_Id & "," & .S_Nombre & "," & .S_Activo & " " & _
                      "FROM " & .S_tabla & " " & _
                      "WHERE  rut_emp='" & SP_Rut_Activo & "' "
        .Caption = "Listas de Precios Disponibles"
        .FrmMantenedor.Caption = "Listas de precios "
        .B_Editable = True
        .Show 1
    End With
    ListaPrecios
End Sub

Private Sub CmdMarcas_Click()
    With Mantenedor_Simple
        .S_Id = "mar_id"
        .S_Nombre = "mar_nombre"
        .S_Activo = "mar_activo"
        .S_tabla = "par_marcas"
        .S_RutEmpresa = "SI"
        .S_Consulta = "SELECT " & .S_Id & "," & .S_Nombre & "," & .S_Activo & " " & _
                      "FROM " & .S_tabla & " " & _
                      "WHERE  rut_emp='" & SP_Rut_Activo & "' "
        .Caption = "Mantenedor Marcas de Articulos"
        .FrmMantenedor.Caption = "Categorias "
        .B_Editable = True
        .Show 1
    End With
    LLenarCombo CboMarca, "mar_nombre", "mar_id", "par_marcas", "mar_activo='SI' AND rut_emp='" & SP_Rut_Activo & "'", "mar_nombre"

End Sub



Private Sub CmdNuevo_Click()
    SkCreando.Visible = True
    SkCreando2.Visible = True
    Me.CboHabilitado.ListIndex = 0
    Me.CboInventariable.ListIndex = 0
    Busca_Id_Combo CboEstado, 1
    Me.CboBodegaActual.Visible = True
    CboBodegaActual.ListIndex = 0
    Me.SkBodegaInicio.Visible = True
    Sql = "SELECT MAX(codigo)+1 nuevocodigo " & _
            "FROM maestro_productos " & _
            "WHERE rut_emp='" & SP_Rut_Activo & "'"
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        TxtCodigo = RsTmp!nuevocodigo
        TxtCodigoInterno = Mid(TxtCodigoInterno, 1, 2) & Trim(Str(RsTmp!nuevocodigo))
    End If
    Me.LvListasPrecios.ListItems.Clear
    CmdGrabar.Enabled = True
    
    sis_InputBox.texto = TxtCodigo
    sis_InputBox.Caption = "Nuevo codigo"
    sis_InputBox.FramBox.Caption = "Confirmar nuevo codigo"
    sis_InputBox.Sm_TipoDato = "N"
    sis_InputBox.texto.PasswordChar = ""
    sis_InputBox.Show 1
    
    If Val(SG_codigo2) > 0 Then
        'buscar el codigo
        Sql = "SELECT codigo " & _
                "FROM maestro_productos " & _
                "WHERE codigo=" & Val(SG_codigo2)
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
            MsgBox "El codigo que ingreso ya existe...", vbInformation
            Inhabilitar
            Exit Sub
        End If
        TxtCodigo = SG_codigo2
    End If
    
    
End Sub

Private Sub CmdOkLista_Click()
    If Val(TxtIdLsd) = 0 Then Exit Sub
    If Val(TxtPrecioLista) = 0 Then
        TxtPrecioLista = "0"
    End If
    If Val(LvListasPrecios.SelectedItem) = 0 Then
    
        Sql = "INSERT INTO "
    Else
    
        Sql = "UPDATE par_lista_precios_detalle " & _
                "SET lsd_precio=" & Val(TxtPrecioLista) & " " & _
                "WHERE lsd_id=" & Me.TxtIdLsd
        cn.Execute Sql
    End If
    Me.LvListasPrecios.SelectedItem.SubItems(2) = TxtPrecioLista
    TxtIdLsd = ""
    Me.TxtNombreLista = ""
    TxtPrecioLista = 0
End Sub

Private Sub CmdRegistrarTraslado_Click()

    If CboOrigen.ListIndex = -1 Then
        MsgBox "Debe seleccionar Origen...", vbInformation
        CboOrigen.SetFocus
        Exit Sub
    End If
    If CboDestino.ListIndex = -1 Then
        MsgBox "Debe seleccionar Destino...", vbInformation
        CboDestino.SetFocus
        Exit Sub
    End If
    If CboDestino.ListIndex = CboOrigen.ListIndex Then
        MsgBox "Seleccione Destino distino al origen...", vbInformation
        CboDestino.SetFocus
        Exit Sub
    End If
    
    On Error GoTo ErrTraslado
    cn.BeginTrans
        
        Sql = "INSERT INTO inv_traslados(tra_nro_guia,tra_usuario,tra_fecha,bod_origen,bod_destino,pro_codigo) " & _
                "VALUES(" & Me.TxtTraGuia & ",'" & Me.TxtTraUsuario & "','" & Fql(Me.DtFechaTraslado) & "'," & Me.CboOrigen.ItemData(CboOrigen.ListIndex) & "," & Me.CboDestino.ItemData(CboDestino.ListIndex) & "," & Me.LvDetalle.SelectedItem & ")"
        cn.Execute Sql
        
        Sql = "UPDATE maestro_productos SET bod_id=" & CboDestino.ItemData(CboDestino.ListIndex) & " " & _
                "WHERE codigo=" & TxtCodigo
        cn.Execute Sql
    
    cn.CommitTrans
    CargaDatosProducto
    MsgBox LvDetalle.SelectedItem.SubItems(4) & vbNewLine & "Traslado Registrado...", vbInformation
    
    Exit Sub
ErrTraslado:
    MsgBox "Problema al registrar traslado..." & vbNewLine & Err.Number & vbNewLine & Err.Description, vbInformation
    cn.RollbackTrans
End Sub



Private Sub ExportarUbicacion_Click()
        Dim tit(2) As String
        FrmLoad.Visible = True
        DoEvents
        Me.LvMaquinasUbicacion.ListItems.Clear
        For i = 1 To LvDetalle.ListItems.Count
            LvMaquinasUbicacion.ListItems.Add , , LvDetalle.ListItems(i)
            For n = 1 To 4
                LvMaquinasUbicacion.ListItems(LvMaquinasUbicacion.ListItems.Count).SubItems(n) = LvDetalle.ListItems(i).SubItems(n)
            Next
            LvMaquinasUbicacion.ListItems(LvMaquinasUbicacion.ListItems.Count).SubItems(5) = LvDetalle.ListItems(i).SubItems(14)
            LvMaquinasUbicacion.ListItems(LvMaquinasUbicacion.ListItems.Count).SubItems(6) = LvDetalle.ListItems(i).SubItems(16)
            LvMaquinasUbicacion.ListItems(LvMaquinasUbicacion.ListItems.Count).SubItems(7) = LvDetalle.ListItems(i).SubItems(18)
        Next
        For i = 1 To LvMaquinasUbicacion.ListItems.Count
            Sql = "SELECT arr_id " & _
                "FROM ven_arr_detalle " & _
                "WHERE det_arr_estado='En Arriendo' AND det_arr_codigo=" & LvMaquinasUbicacion.ListItems(i) & " " & _
                "ORDER BY arr_id DESC " & _
                "LIMIT 1"
            Consulta RsTmp, Sql
            If RsTmp.RecordCount > 0 Then
                Sql = "SELECT  arr_fecha_contrato,arr_nombre_cliente,arr_direccion_obra,sue_ciudad " & _
                        "FROM ven_arriendo v " & _
                        "JOIN sis_empresas_sucursales s ON v.sue_id=s.sue_id " & _
                        "WHERE arr_estado<>'NULO' AND arr_id=" & RsTmp!arr_id
                
                Consulta RsTmp2, Sql
                If RsTmp2.RecordCount > 0 Then
                    LvMaquinasUbicacion.ListItems(i).SubItems(8) = RsTmp!arr_id
                    LvMaquinasUbicacion.ListItems(i).SubItems(9) = RsTmp2!arr_fecha_contrato
                    LvMaquinasUbicacion.ListItems(i).SubItems(10) = RsTmp2!arr_nombre_cliente
                    LvMaquinasUbicacion.ListItems(i).SubItems(11) = RsTmp2!arr_direccion_obra
                    LvMaquinasUbicacion.ListItems(i).SubItems(12) = RsTmp2!sue_ciudad
                End If
                
                
                
            End If
        Next
        If LvMaquinasUbicacion.ListItems.Count > 0 Then
                FraProgreso.Visible = True
                tit(0) = "Categoria    :" & CboTipoProducto.Text
                tit(1) = "SubCategoria :" & CboMarca.Text
                tit(2) = "Fecha informe: " & Date & " " & Now
                ExportarNuevo LvMaquinasUbicacion, tit, Me, BarraProgreso
                FraProgreso.Visible = False
        End If
    FrmLoad.Visible = False
End Sub

Private Sub Form_Load()
    Aplicar_skin Me
    Centrar Me
    TxtTraUsuario = LogUsuario
    
End Sub
Private Sub CargandoDatos()
    FrmLoad.Visible = True
    DoEvents

    DtFechaIngreso = Date
    DtFechaTraslado = Date
    LLenarCombo CboTipoProducto, "tip_nombre", "tip_id", "par_tipos_productos", "tip_activo='SI' AND  rut_emp='" & SP_Rut_Activo & "'", "tip_nombre"
    LLenarCombo CboFtipo, "tip_nombre", "tip_id", "par_tipos_productos", "tip_activo='SI' AND  rut_emp='" & SP_Rut_Activo & "'", "tip_nombre"
    CboTipoProducto.AddItem "TODOS"
    CboTipoProducto.ListIndex = CboTipoProducto.ListCount - 1
    LLenarCombo CboFMarca, "mar_nombre", "mar_id", "par_marcas", "mar_activo='SI' AND  rut_emp='" & SP_Rut_Activo & "'", "mar_nombre"
   ' LLenarCombo CboMarca, "mar_nombre", "mar_id", "par_marcas", "mar_activo='SI' AND  rut_emp='" & SP_Rut_Activo & "'", "mar_nombre"
    CboMarca.AddItem "TODOS"
    CboMarca.ListIndex = CboMarca.ListCount - 1
    
    LLenarCombo CboBodega, "bod_nombre", "bod_id", "par_bodegas", "bod_activo='SI' AND rut_emp='" & SP_Rut_Activo & "'", "bod_nombre"
    LLenarCombo CboBodegaActual, "bod_nombre", "bod_id", "par_bodegas", "bod_activo='SI' AND rut_emp='" & SP_Rut_Activo & "'", "bod_nombre"
    
    LLenarCombo CboOrigen, "bod_nombre", "bod_id", "par_bodegas", "bod_activo='SI' AND rut_emp='" & SP_Rut_Activo & "'", "bod_nombre"
    LLenarCombo CboDestino, "bod_nombre", "bod_id", "par_bodegas", "bod_activo='SI' AND rut_emp='" & SP_Rut_Activo & "'", "bod_nombre"
    
    LLenarCombo CboEstado, "est_nombre", "est_id", "par_estados_maquinas", "est_activo='SI' AND rut_emp='" & SP_Rut_Activo & "'", "est_nombre"
    
    CboBodega.AddItem "TODAS"
    CboBodega.ListIndex = CboBodega.ListCount - 1
    Sm_CodInternos = "SI"
        
    Sm_FiltroMarca = Empty
    Sm_FiltroTipo = Empty
    Sm_Filtrobodega = Empty
    ''Skin2 Me, , 4
    Grilla

    FrmLoad.Visible = False


End Sub





Private Sub Grilla()
    s_sql2 = ""
    FrmLoad.Visible = True
    DoEvents
        'bod_id= 1, ya que las bodegas principales, son 1
    
 '   s_Sql = "SELECT codigo,pro_codigo_interno,tip_nombre,mar_nombre,descripcion," & _
             "IFNULL((SELECT AVG(pro_ultimo_precio_compra) FROM pro_stock s WHERE s.rut_emp='" & SP_Rut_Activo & "' AND s.pro_codigo=p.codigo AND bod_id=1),precio_compra) precio_compra," & _
             "porciento_utilidad,precio_venta,precio_venta-IFNULL((" & _
                    "SELECT AVG(pro_ultimo_precio_compra) " & _
                    "FROM pro_stock s " & _
                    "WHERE s.rut_emp = '" & SP_Rut_Activo & "' " & _
                    "AND s.pro_codigo = p.codigo)," & _
                    "precio_compra) margen," & _
              "IF(pro_inventariable='SI',IFNULL((SELECT SUM(sto_stock) FROM pro_stock s WHERE " & Sm_Filtrobodega & " s.rut_emp='" & SP_Rut_Activo & "' AND s.pro_codigo=p.codigo),0),0) stock_bodega," & _
              "IFNULL((SELECT SUM(arr_cantidad) " & _
                "FROM inv_productos_arrendados WHERE arr_devuelto='NO' AND pro_codigo=p.codigo AND emp_id=" & IG_id_Empresa & "),0) arrendados,0, " & _
              "stock_critico, bod_nombre,ubicacion_bodega,comentario,pro_inventariable " & Sp_CodigoProveedor & " " & _
          "FROM maestro_productos p,par_tipos_productos t,par_marcas m,par_bodegas b " & _
          "WHERE p.rut_emp='" & SP_Rut_Activo & "' AND  p.tip_id=t.tip_id AND p.mar_id=m.mar_id AND p.bod_id=b.bod_id " & Sm_filtro & Sm_FiltroTipo & Sm_FiltroMarca & Sm_TipoF & _
          Sp_SoloRut
    
  '  If Sm_CodInternos = "SI" And Bm_GrillaCargada And OpCodigo Then
    
        s_Sql = " SELECT codigo,pro_codigo_interno,tip_nombre,mar_nombre,descripcion," & _
             "IFNULL((SELECT AVG(pro_ultimo_precio_compra) FROM pro_stock s WHERE s.rut_emp='" & SP_Rut_Activo & "' AND s.pro_codigo=p.codigo),0) precio_compra" & _
             "porciento_utilidad,precio_venta,margen," & _
              "IF(pro_inventariable='SI',IFNULL((SELECT SUM(sto_stock) FROM pro_stock s WHERE " & Sm_Filtrobodega & " s.rut_emp='" & SP_Rut_Activo & "' AND s.pro_codigo=p.codigo),0),0) stock_bodega," & _
              "IFNULL((SELECT SUM(arr_cantidad) " & _
                "FROM inv_productos_arrendados WHERE arr_devuelto='NO' AND pro_codigo=p.codigo AND emp_id=" & IG_id_Empresa & "),0) arrendados,0, " & _
              "stock_critico, bod_nombre,ubicacion_bodega,comentario,pro_inventariable,serie,p.id,p.pro_estado " & Sp_CodigoProveedor & " " & _
          "FROM maestro_productos p,par_tipos_productos t,par_marcas m,par_bodegas b " & _
          "WHERE p.rut_emp='" & SP_Rut_Activo & "' AND  p.tip_id=t.tip_id AND p.mar_id=m.mar_id AND p.bod_id=b.bod_id " & Sm_filtro & Sm_FiltroCodInterno & Sm_FiltroTipo & Sm_FiltroMarca & Sm_TipoF & Sm_Sucursal & _
          Sp_SoloRut & "ORDER BY p.codigo ASC"
          
    
    
    
   ' End If
    
    
    Consulta RsProductos, s_Sql
    
    
    
    
    LLenar_Grilla RsProductos, Me, LvDetalle, False, True, True, False
    txtCntidadregistros = RsProductos.RecordCount
    For i = 1 To LvDetalle.ListItems.Count
        LvDetalle.ListItems(i).SubItems(8) = CDbl(LvDetalle.ListItems(i).SubItems(8)) - CDbl(LvDetalle.ListItems(i).SubItems(9))
        LvDetalle.ListItems(i).SubItems(10) = CDbl(LvDetalle.ListItems(i).SubItems(9)) + CDbl(LvDetalle.ListItems(i).SubItems(8))
    Next
    Bm_GrillaCargada = True
    Me.SkCantidadProductos = LvDetalle.ListItems.Count
    FrmLoad.Visible = False
End Sub



Private Sub LvArriendos_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    ordListView ColumnHeader, Me, LvArriendos
End Sub

Private Sub LvDetalle_Click()
    CargaDatosProducto
    Inhabilitar
    CmdGrabar.Enabled = False
End Sub
Private Sub CargaDatosProducto()
    SkCreando.Visible = False
    SkCreando2.Visible = False
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    FrmLoad.Visible = True
    DoEvents
    Me.SkCantidadProductos = "Registro " & LvDetalle.SelectedItem.Index & " de " & LvDetalle.ListItems.Count
 '   Me.CboBodegaActual.Visible = False
 '   Me.SkBodegaInicio.Visible = False

    Sql = "SELECT * " & _
            "FROM maestro_productos " & _
            "WHERE codigo=" & LvDetalle.SelectedItem
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        TxtCodigo = RsTmp!Codigo
        TxtDescripcion = RsTmp!Descripcion
        TxtPrecioCostoSFlete = RsTmp!precio_compra
        Txt_PrecioVenta = RsTmp!precio_venta
        TxtMotivoBaja = "" & RsTmp!pro_dado_de_baja_motivo
        TxtSerieMotor = "" & RsTmp!pro_serie_motor
        If Not IsNull(RsTmp!pro_fecha_ingreso) Then Me.DtFechaIngreso = RsTmp!pro_fecha_ingreso
        Me.TxtDetalleIngreso = RsTmp!pro_info_adicional
    '    TxtPrecioVta = RsTmp!precio_venta
      '  txtProEstado = RsTmp!pro_estado
      '  If txtProEstado = "DISPONIBLE" Then
            
      '  End If
        
      
      
        If RsTmp!pro_inventariable = "SI" Then
            CboInventariable.ListIndex = 0
        Else
            CboInventariable.ListIndex = 1
        End If
        If RsTmp!pro_activo = "SI" Then
            CboHabilitado.ListIndex = 0
        Else
            CboHabilitado.ListIndex = 1
        End If
        TxtSerie = "" & RsTmp!serie
        TxtCodigoInterno = RsTmp!pro_codigo_interno
        Busca_Id_Combo CboFtipo, Val(RsTmp!tip_id)
        Busca_Id_Combo CboFMarca, Val(RsTmp!mar_id)
        Busca_Id_Combo CboOrigen, Val(RsTmp!bod_id)
        Busca_Id_Combo CboBodegaActual, Val(RsTmp!bod_id)
        Busca_Id_Combo CboEstado, Val(RsTmp!est_id)

        
        For i = 0 To CboBodega.ListCount - 1
            If CboBodega.ItemData(i) = RsTmp!bod_id Then
                'txtProEstado = UCase(RsTmp!pro_estado) & " EN BODEGA " & UCase(CboBodega.List(i))
                txtProEstado = UCase(CboEstado.Text) & " EN BODEGA " & UCase(CboBodega.List(i))
            End If
        Next
        Frame7.Caption = LvDetalle.SelectedItem.SubItems(1)
    End If
    
    
    Sql = "SELECT a.arr_id,a.arr_id,arr_fecha_contrato,arr_rut_cliente,arr_nombre_cliente,arr_direccion_obra," & _
                "det_arr_num_recepcion,det_arr_nombre_receptor,det_arr_fecha_recepcion, " & _
                " IF(a.sue_id=1,'TEMUCO','VALDIVIA') sucursal " & _
            "FROM ven_arriendo a " & _
            "JOIN ven_arr_detalle d ON a.arr_id=d.arr_id " & _
            "WHERE arr_estado<>'NULO' /* AND a.sue_id=" & LogSueID & " AND d.sue_id=" & LogSueID & " */ AND  d.det_arr_codigo=" & LvDetalle.SelectedItem & " " & _
            "ORDER BY a.arr_fecha_contrato DESC"
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, Me.LvArriendos, False, True, True, False
    
    
    Sql = "SELECT tra_id,tra_nro_guia,tra_usuario,tra_fecha," & _
            "(SELECT bod_nombre FROM par_bodegas t WHERE t.bod_id=t.bod_origen) origen, " & _
            "(SELECT bod_nombre FROM par_bodegas t WHERE t.bod_id=t.bod_destino) destino " & _
            "FROM inv_traslados t " & _
            "WHERE pro_codigo=" & LvDetalle.SelectedItem
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, Me.LvTraslados, False, True, True, False
    
    
    ListaPrecios
    
    
    
    FrmLoad.Visible = False
    
End Sub
Private Sub ListaPrecios()
    Sql = "SELECT d.lsd_id,lst_nombre,lsd_precio " & _
                "FROM par_lista_precios l " & _
                "LEFT JOIN par_lista_precios_detalle d ON l.lst_id=d.lst_id " & _
                "WHERE  lst_activo='SI' AND id=" & LvDetalle.SelectedItem.SubItems(17)
    
    
                
    
  
    
    Sql = "SELECT  (SELECT lsd_id FROM par_lista_precios_detalle d WHERE d.lst_id=l.lst_id AND id=" & LvDetalle.SelectedItem.SubItems(17) & ")," & _
                    "lst_nombre, " & _
                    "(SELECT lsd_precio FROM par_lista_precios_detalle d WHERE d.lst_id=l.lst_id AND id=" & LvDetalle.SelectedItem.SubItems(17) & ") " & _
                    "FROM    par_lista_precios l " & _
                    "WHERE   lst_activo = 'SI'"
      Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, Me.LvListasPrecios, False, True, True, False
    
    
    'For p = 1 To LvListasPrecios.ListItems.Count
    '       Sql = "SELECT lsd_precio " & _
    '                "FROM par_lista_precios_detalle " & _
    '                "WHERE lsd_id=" & LvListasPrecios.SelectedItem
    '        Consulta RsTmp2, Sql
    '        If RsTmp2.RecordCount > 0 Then
    '
   '
    
    'Next

End Sub
Private Sub ListaPrecios2(Idd As Long)
    Sql = "SELECT d.lsd_id,lst_nombre,lsd_precio " & _
                "FROM par_lista_precios l " & _
                "JOIN par_lista_precios_detalle d ON l.lst_id=d.lst_id " & _
                "WHERE  lst_activo='SI' AND id=" & Idd
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, Me.LvListasPrecios, False, True, True, False
End Sub


Private Sub LvDetalle_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    ordListView ColumnHeader, Me, LvDetalle
End Sub
Private Sub LvDetalle_KeyUp(KeyCode As Integer, Shift As Integer)
'    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
'    CargaDatosProducto
End Sub

Private Sub LvListasPrecios_DblClick()
    TxtIdLsd = LvListasPrecios.SelectedItem
    Me.TxtNombreLista = LvListasPrecios.SelectedItem.SubItems(1)
    TxtPrecioLista = LvListasPrecios.SelectedItem.SubItems(2)
    TxtPrecioLista.SetFocus
End Sub

Private Sub Timer1_Timer()
    On Error Resume Next
    LvDetalle.SetFocus
    CargandoDatos
    Timer1.Enabled = False
End Sub

Private Sub Txt_PrecioVenta_KeyPress(KeyAscii As Integer)
    
    KeyAscii = SoloNumeros(KeyAscii)
End Sub

Private Sub TxtBusca_Change()
    BuscarTexto
End Sub

Private Sub TxtBusca_GotFocus()
    En_Foco TxtBusca
End Sub
Private Sub TxtBusca_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub
Private Sub TxtPrecioLista_GotFocus()
    En_Foco TxtPrecioLista
End Sub

Private Sub TxtPrecioLista_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
End Sub

Private Sub TxtSerie_GotFocus()
    En_Foco TxtSerie
End Sub

Private Sub TxtSerie_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub TxtTraGuia_GotFocus()
    En_Foco TxtTraGuia
End Sub

Private Sub TxtTraGuia_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
End Sub
