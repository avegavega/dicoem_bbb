VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{DA729E34-689F-49EA-A856-B57046630B73}#1.0#0"; "progressbar-xp.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form InvAjusteInventario 
   Caption         =   "Ajuste de Inventario"
   ClientHeight    =   8355
   ClientLeft      =   1200
   ClientTop       =   2490
   ClientWidth     =   15195
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   ScaleHeight     =   8355
   ScaleWidth      =   15195
   Begin VB.ComboBox Combo1 
      Height          =   315
      Left            =   285
      Style           =   2  'Dropdown List
      TabIndex        =   5
      Top             =   8520
      Width           =   2415
   End
   Begin VB.CommandButton cmdSalir 
      Caption         =   "&Retornar"
      Height          =   405
      Left            =   13605
      TabIndex        =   4
      Top             =   7875
      Width           =   1350
   End
   Begin VB.Frame Frame2 
      Caption         =   "Productos"
      Height          =   5400
      Left            =   300
      TabIndex        =   1
      Top             =   2430
      Width           =   14715
      Begin VB.CommandButton CmdExportar 
         Caption         =   "&Exportar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   12765
         TabIndex        =   8
         ToolTipText     =   "Exportar planilla a Excel"
         Top             =   210
         Width           =   1665
      End
      Begin VB.Frame FraProgreso 
         Caption         =   "Espere por favor...."
         Height          =   795
         Left            =   900
         TabIndex        =   6
         Top             =   3885
         Visible         =   0   'False
         Width           =   11430
         Begin Proyecto2.XP_ProgressBar BarraProgreso 
            Height          =   495
            Left            =   90
            TabIndex        =   7
            Top             =   195
            Width           =   11190
            _ExtentX        =   19738
            _ExtentY        =   873
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BrushStyle      =   0
            Color           =   16777088
            Scrolling       =   1
            ShowText        =   -1  'True
         End
      End
      Begin VB.CommandButton CmdGuardar 
         Caption         =   "&Ajustar Planilla"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   405
         Left            =   135
         TabIndex        =   3
         Top             =   4920
         Width           =   1665
      End
      Begin MSComctlLib.ListView LvProductos 
         Height          =   4365
         Left            =   135
         TabIndex        =   11
         Top             =   525
         Width           =   14400
         _ExtentX        =   25400
         _ExtentY        =   7699
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   0   'False
         HideSelection   =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   8
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "T1500"
            Text            =   "Codigo"
            Object.Width           =   2646
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "T2000"
            Text            =   "Tipo Producto"
            Object.Width           =   3351
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "T2000"
            Text            =   "Marca"
            Object.Width           =   3351
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Object.Tag             =   "T5000"
            Text            =   "Descripcion"
            Object.Width           =   7056
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   4
            Object.Tag             =   "N102"
            Text            =   "Stock Sistema"
            Object.Width           =   2646
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   5
            Object.Tag             =   "N102"
            Text            =   "Cant.Contada"
            Object.Width           =   2646
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   6
            Object.Tag             =   "N102"
            Text            =   "Diferencia"
            Object.Width           =   2646
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   7
            Object.Tag             =   "N109"
            Text            =   "Id Unicvo"
            Object.Width           =   0
         EndProperty
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Planillas disponibles para ajustar"
      Height          =   2130
      Left            =   285
      TabIndex        =   0
      Top             =   195
      Width           =   14745
      Begin VB.OptionButton Option2 
         Caption         =   "Historicas"
         Height          =   240
         Left            =   12690
         TabIndex        =   10
         Top             =   225
         Width           =   1830
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Pendientes"
         Height          =   255
         Left            =   11040
         TabIndex        =   9
         Top             =   210
         Value           =   -1  'True
         Width           =   1365
      End
      Begin MSComctlLib.ListView LvPlanillas 
         Height          =   1530
         Left            =   165
         TabIndex        =   2
         Top             =   480
         Width           =   14475
         _ExtentX        =   25532
         _ExtentY        =   2699
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   8
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "T3000"
            Text            =   "Fecha"
            Object.Width           =   2293
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "T1000"
            Text            =   "Hora "
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "N100"
            Text            =   "Nro Planilla"
            Object.Width           =   1940
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Object.Tag             =   "T2500"
            Text            =   "Descripcion Planilla"
            Object.Width           =   6174
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Object.Tag             =   "T1000"
            Text            =   "Bodega"
            Object.Width           =   3528
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   5
            Object.Tag             =   "T2000"
            Text            =   "Estado"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   6
            Object.Tag             =   "T2000"
            Text            =   "Usuario"
            Object.Width           =   4057
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   7
            Object.Tag             =   "N100"
            Text            =   "bod_id"
            Object.Width           =   0
         EndProperty
      End
   End
   Begin VB.Timer Timer1 
      Left            =   540
      Top             =   7695
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   -30
      OleObjectBlob   =   "InvAjusteInventario.frx":0000
      Top             =   7725
   End
End
Attribute VB_Name = "InvAjusteInventario"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private WithEvents cSubLV As cSubclassListView
Attribute cSubLV.VB_VarHelpID = -1
Private WithEvents cSubLV2 As cSubclassListView
Attribute cSubLV2.VB_VarHelpID = -1
Dim Sm_EstadoPlanilla As String
Private Sub cmdExportar_Click()
     Dim tit(2) As String
    If LvProductos.ListItems.Count = 0 Then
        MsgBox "No hay productos para exportar ...", vbInformation
        Exit Sub
    End If
    FraProgreso.Visible = True
    tit(0) = "Ajuste de Inventario " & DtFecha & " - " & Time
    tit(1) = "Nro Planilla " & LvPlanillas.SelectedItem.SubItems(2) & " - Bodega:" & LvPlanillas.SelectedItem.SubItems(4) & " - " & LvPlanillas.SelectedItem.SubItems(3)
    tit(2) = ""
    ExportarNuevo Me.LvProductos, tit, Me, BarraProgreso
    FraProgreso.Visible = False
End Sub

Private Sub CmdGuardar_Click()
        Dim Lp_IdP As Double
    Dim Sp_Codigo As String
    Dim Dp_Cantidad As Double
    Dim Dp_CantContada As Double
    Dim Ip_Bodega As Integer
    Dim Dp_Promedio As Double
    Dim Dp_CantSegunSistema As Double
    
    If LvPlanillas.SelectedItem Is Nothing Then Exit Sub
        FraProgreso.Visible = True
    
     ' LbProgreso = Format(V / Progreso.Max * 100, "###") & "%"
     BarraProgreso.Max = LvProductos.ListItems.Count
    'BarraProgreso.Value = V
    Lp_IdP = LvPlanillas.SelectedItem.SubItems(2)
    Ip_Bodega = LvPlanillas.SelectedItem.SubItems(7)
    If MsgBox("Esta acci�n ajustara el inventario del sistema con el Stock Inventariado fisicamente" & vbNewLine & _
              "Continuar...", vbQuestion + vbYesNo) = vbNo Then Exit Sub
    On Error GoTo errorInventario
    DoEvents
    cn.BeginTrans
        'Actualiza planilla de inventario
        cant = 1
        For i = 1 To LvProductos.ListItems.Count
            cant = cant + 1
            BarraProgreso.Value = V

            Sql = "UPDATE inv_toma_inventario_detalle i " & _
                    "SET tmi_diferencia=" & CxP(CDbl(LvProductos.ListItems(i).SubItems(6))) & ",tdi_estado='OK',tdi_actual=" & CxP(CDbl(LvProductos.ListItems(i).SubItems(4))) & ",tmi_cantidad=" & CxP(CDbl(LvProductos.ListItems(i).SubItems(5))) & " " & _
                    "WHERE i.tmi_id =" & Lp_IdP & " AND tdi_id=" & LvProductos.ListItems(i).SubItems(7)
            cn.Execute Sql
        Next
    
                
              
            
          '  Sql = "UPDATE pro_stock k,inv_toma_inventario n,inv_toma_inventario_detalle i " & _
              "SET tmi_diferencia=tmi_cantidad-sto_stock,tdi_estado='OK',tdi_actual=sto_stock " & _
              "WHERE k.rut_emp='" & SP_Rut_Activo & "' AND n.rut_emp='" & SP_Rut_Activo & "' AND    k.bod_id = n.bod_id AND k.pro_codigo = i.pro_codigo AND n.tmi_id = i.tmi_id AND i.tmi_id =" & Lp_IdP
       ' Consulta RsTmp, Sql
        'Ajusta el stock sistema con el stock fisico
        'Sql = "UPDATE pro_stock k,inv_toma_inventario n,inv_toma_inventario_detalle i " & _
              "SET sto_stock=tmi_cantidad,tmi_estado='AJUSTADO' " & _
              "WHERE k.bod_id = n.bod_id AND k.pro_codigo = i.pro_codigo AND n.tmi_id = i.tmi_id AND i.tmi_id =" & Lp_IdP
        Sql = "UPDATE pro_stock k,inv_toma_inventario n,inv_toma_inventario_detalle i " & _
              "SET tmi_estado='AJUSTADO' " & _
              "WHERE  k.rut_emp='" & SP_Rut_Activo & "' AND n.rut_emp='" & SP_Rut_Activo & "' AND   k.bod_id = n.bod_id AND k.pro_codigo = i.pro_codigo AND n.tmi_id = i.tmi_id AND i.tmi_id =" & Lp_IdP
        cn.Execute Sql
        BarraProgreso.Value = 1
        BarraProgreso.Max = LvProductos.ListItems.Count
       
        For i = 1 To LvProductos.ListItems.Count
            
            BarraProgreso.Value = i
            Sp_Codigo = LvProductos.ListItems(i)
            Dp_Cantidad = CDbl(LvProductos.ListItems(i).SubItems(6))
            Dp_CantContada = LvProductos.ListItems(i).SubItems(5)
            Dp_CantSegunSistema = CDbl(LvProductos.ListItems(i).SubItems(4))
            'If Dp_Cantidad <> 0 Then
                
                Sql = "SELECT IFNULL(ROUND(kar_nuevo_saldo_valor/kar_nuevo_saldo),pro_precio_neto) promedio " & _
                        "FROM inv_kardex " & _
                        "WHERE rut_emp='" & SP_Rut_Activo & "' AND pro_codigo='" & Sp_Codigo & "' " & _
                        "ORDER BY kar_id DESC " & _
                        "LIMIT 1"
                Consulta RsTmp, Sql
                Dp_Promedio = 0
                If RsTmp.RecordCount > 0 Then
                    Dp_Promedio = RsTmp!promedio
                End If
                'Si la diferencia es negativa hacemos una resta en el kardex
                
                
                '7 Octubre,2013
                'Rehacer kardex para ajuste de inventario
                    Dim Dp_SAnterior As Double
                    Dim Dp_Diferencia As Double
                    Dim Dp_PrecioNeto As Double
                    Sql = "SELECT kar_nuevo_saldo,pro_precio_neto " & _
                            "FROM inv_kardex " & _
                            "WHERE pro_codigo='" & Sp_Codigo & "' AND rut_emp='" & SP_Rut_Activo & "' " & _
                            "ORDER BY kar_id DESC " & _
                            "LIMIT 1"
                            
                            
                    Consulta RsTmp, Sql
                    If RsTmp.RecordCount > 0 Then
                        Dp_SAnterior = RsTmp!kar_nuevo_saldo
                        Dp_Diferencia = Dp_CantContada - Dp_SAnterior
                        Dp_PrecioNeto = RsTmp!pro_precio_neto
                    
                    End If
                'Hacer el insert directo sin la funcion Kardex
                
                
                
                Sql = "INSERT INTO inv_kardex (kar_fecha,kar_movimiento,kar_numero,bod_id,pro_codigo," & _
                    "kar_saldo_anterior,kar_cantidad,kar_nuevo_saldo,kar_usuario,pro_precio_neto,kar_descripcion," & _
                    "kar_saldo_anterior_valor,kar_cantidad_valor,kar_nuevo_saldo_valor,rut_emp) VALUES("
                    
                If Dp_Diferencia < 0 Then
                
                    'Kardex Format(Date, "YYYY-MM-DD"), "SALIDA", 0, Lp_IdP, Ip_Bodega, _
                    Sp_Codigo, Abs(Dp_Cantidad), "AJUSTE PLANILLA " & Str(Lp_IdP), Dp_Promedio, Dp_Promedio * Abs(Dp_Cantidad)
                    
                    cn.Execute Sql & "'" & Fql(Date) & "','SALIDA'," & Lp_IdP & "," & Ip_Bodega & ",'" & Sp_Codigo & "'," & _
                    CxP(Dp_SAnterior) & "," & CxP(Abs(Dp_Diferencia)) & "," & CxP(Dp_CantContada) & ",'" & LogUsuario & "'," & CxP(Dp_PrecioNeto) & ",'" & _
                    "AJUSTE PLANILLA " & Str(Lp_IdP) & "'," & Round(CxP(Val(Dp_PrecioNeto) * Dp_SAnterior), 0) & "," & _
                    Round(CxP(Abs(Val(Dp_Diferencia)) * CxP(Val(Dp_PrecioNeto))), 0) & "," & CxP(Dp_PrecioNeto * Dp_CantContada) & ",'" & SP_Rut_Activo & "')"
                    
                    
                    
                    
                    
                    
                ElseIf Dp_Diferencia >= 0 Then
                    'Si la diferencia es positiva hacemos un ingreso en el kardex
              '      Kardex Format(Date, "YYYY-MM-DD"), "ENTRADA", 0, Lp_IdP, Ip_Bodega, _
                    Sp_Codigo, Dp_Cantidad, "AJUSTE PLANILLA " & Str(Lp_IdP), Dp_Promedio, Dp_Promedio * Dp_Cantidad
                    
                        cn.Execute Sql & "'" & Fql(Date) & "','ENTRADA'," & Lp_IdP & "," & Ip_Bodega & ",'" & Sp_Codigo & "'," & _
                    CxP(Dp_SAnterior) & "," & CxP(Abs(Dp_Diferencia)) & "," & CxP(Dp_CantContada) & ",'" & LogUsuario & "'," & CxP(Dp_PrecioNeto) & ",'" & _
                    "AJUSTE PLANILLA " & Str(Lp_IdP) & "'," & CxP(Dp_PrecioNeto * Dp_SAnterior) & "," & _
                    CxP(Abs(Val(Dp_Diferencia)) * CxP(Val(Dp_PrecioNeto))) & "," & CxP(Dp_PrecioNeto * Dp_CantContada) & ",'" & SP_Rut_Activo & "')"
                End If
                cn.Execute "UPDATE pro_stock SET sto_stock=" & CxP(Dp_CantContada) & " " & _
                                "WHERE bod_id=" & Ip_Bodega & " AND rut_emp='" & SP_Rut_Activo & "' AND pro_codigo='" & Sp_Codigo & "'"
                        
            'End If
        Next
    cn.CommitTrans
    
    FraProgreso.Visible = False
    MsgBox "Planilla ajustada, ahora con el kardex"
    CargaPlanillas
    LvProductos.ListItems.Clear
    Exit Sub
errorInventario:
    MsgBox Sp_Codigo
    FraProgreso.Visible = False
    MsgBox "Problema al ajustar inventario..." & vbNewLine & Err.Description & vbNewLine & Err.Number, vbExclamation
    cn.RollbackTrans
End Sub

Private Sub CmdSalir_Click()
    Unload Me
End Sub


Private Sub CargaPlanillas()
    Sql = "SELECT i.tmi_fecha,i.tmi_hora,i.tmi_id,tmi_descripcion,bod_nombre,tmi_estado,usu_nombre,i.bod_id " & _
          "FROM inv_toma_inventario i " & _
          "INNER JOIN par_bodegas USING(bod_id) " & _
          "WHERE i.rut_emp='" & SP_Rut_Activo & "' AND " & Sm_EstadoPlanilla & _
          "ORDER BY tmi_id DESC"
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvPlanillas, False, True, True, False
         
End Sub
'ORDENA COLUMNAS
Private Sub LvProductos_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    ordListView ColumnHeader, Me, LvProductos
End Sub


Private Sub Form_Load()
    Centrar Me
    Aplicar_skin Me
    Sm_EstadoPlanilla = "tmi_estado='ACTIVO' "
  '  Set cSubLV = New cSubclassListView
  '  cSubLV.SubClassListView LvProductos
    
 '   Set cSubLV2 = New cSubclassListView
 '   cSubLV2.SubClassListView LvPlanillas
    
  '  For i = 1 To 4: Combo1.AddItem "Skin" & i:   Next
    'Combo1.ListIndex = 2
    'Combo1.Visible = False
    CargaPlanillas
End Sub





Private Sub LvPlanillas_Click()
    LvPlanillas_DblClick
End Sub

Private Sub LvPlanillas_DblClick()
    Dim Lp_IdP As Long
    If LvPlanillas.SelectedItem Is Nothing Then Exit Sub
    If Option1.Value Then CargaProdcutos
    If Option2.Value Then CargaProdcutosHistorica

End Sub

Private Sub CargaProdcutos()
    Lp_IdP = LvPlanillas.SelectedItem.SubItems(2)
    Sql = "SELECT  codigo, tip_nombre, mar_nombre, descripcion,sto_stock sistema," & _
                  "tmi_cantidad contado,tmi_cantidad-sto_stock diferencia,i.tdi_id " & _
          "FROM pro_stock k,inv_toma_inventario n,inv_toma_inventario_detalle i,    maestro_productos m " & _
          "INNER JOIN par_tipos_productos USING(tip_id) " & _
          "INNER JOIN par_marcas USING(mar_id) " & _
          "WHERE k.rut_emp='" & SP_Rut_Activo & "' AND n.rut_emp='" & SP_Rut_Activo & "' AND m.rut_emp='" & SP_Rut_Activo & "' AND n.tmi_id = i.tmi_id And k.bod_id = n.bod_id And k.pro_codigo = i.pro_codigo AND " & _
          "i.pro_codigo = m.Codigo AND i.tmi_id = " & Lp_IdP
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvProductos, False, True, True, False
End Sub
Private Sub CargaProdcutosHistorica()
    Lp_IdP = LvPlanillas.SelectedItem.SubItems(2)
    Sql = "SELECT  codigo, tip_nombre, mar_nombre, descripcion,tdi_actual sistema," & _
                  "tmi_cantidad contado,tmi_diferencia " & _
          "FROM inv_toma_inventario n,inv_toma_inventario_detalle i,    maestro_productos m " & _
          "INNER JOIN par_tipos_productos USING(tip_id) " & _
          "INNER JOIN par_marcas USING(mar_id) " & _
          "WHERE n.rut_emp='" & SP_Rut_Activo & "' AND m.rut_emp='" & SP_Rut_Activo & "' AND n.tmi_id = i.tmi_id  AND " & _
          "i.pro_codigo = m.Codigo AND i.tmi_id = " & Lp_IdP
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvProductos, False, True, True, False
End Sub


Private Sub Option1_Click()
    LvProductos.ListItems.Clear
    Sm_EstadoPlanilla = "tmi_estado='ACTIVO' "
    CmdGuardar.Enabled = True
    CargaPlanillas
End Sub

Private Sub Option2_Click()
    LvProductos.ListItems.Clear
    Sm_EstadoPlanilla = "tmi_estado='AJUSTADO' "
    CmdGuardar.Enabled = False
    CargaPlanillas
End Sub

Private Sub Timer1_Timer()
    LvPlanillas.SetFocus
    Timer1.Enabled = False
End Sub
Private Sub Combo1_Click()
    Dim SpathSkin As String
    
    SpathSkin = App.Path & "\skins\"

    Select Case Combo1.ListIndex
        Case 0: Call setColumnHeader(SpathSkin & Combo1.Text, vbBlack, vbBlack, True, False, Me.BackColor, vbBlack)
        Case 1: Call setColumnHeader(SpathSkin & Combo1.Text, vbBlack, vbBlack, True, False, &HC0FFC0, vbBlue)
        Case 2: Call setColumnHeader(SpathSkin & Combo1.Text, vbWhite, vbYellow, True, False, RGB(150, 190, 217))
        Case 3: Call setColumnHeader(SpathSkin & Combo1.Text, vbWhite, vbBlack, True, False, &HC0FFFF, vbBlack)
    End Select
End Sub
Private Sub cSubLV_AfterEdit( _
    ByVal Columna As Integer, _
    Cancel As Boolean, _
    Value As Variant)
    
    Dim lBcolor As Long
    Dim lFColor As Long
    
    Select Case Combo1.ListIndex
        Case 0: lBcolor = &HC0FFFF: lFColor = vbBlack
        Case 1: lBcolor = &HC0FFC0: lFColor = vbBlack
        Case 2: lBcolor = RGB(214, 228, 243): lFColor = vbBlack
        Case 3: lBcolor = &HC0FFC0: lFColor = vbBlack
    End Select
    
    Select Case Columna
        Case 0
           If Not IsNumeric(Value) Then
              Cancel = True
              cSubLV.SowToolTipText "Dato no v�lido", "El valor debe ser un n�mero", TTIconWarning, TTBalloon, False, lFColor, lBcolor, 5000, 0
           End If
        Case 1

           If UCase(Value) <> "ABC" And UCase(Value) <> "XYZ" Then
              Cancel = True
              cSubLV.SowToolTipText "Error", "El valor debe ser ABC o XYZ", TTIconError, TTBalloon, False, lFColor, lBcolor, 5000, 0
              
           End If
        Case 3
           If Not IsDate(Value) Then
              Cancel = True
              cSubLV.SowToolTipText "Error !!!!!!!", "El valor no es una fecha v�lida", TTIconError, TTBalloon, False, lFColor, lBcolor, 5000, 0
           End If
        Case 4
           If Not IsNumeric(Value) Then
                Cancel = True
              cSubLV.SowToolTipText "Error !!!!!!!", "Debe ingresar un valor numerico", TTIconWarning, TTBalloon, False, lFColor, lBcolor, 5000, 0
           End If
           'If Value = "" Then
           '   Cancel = True
           '   cSubLV.SowToolTipText "Error !!!!!!!", "El valor de este campo no puede ser un valor nulo o vacio", TTIconWarning, TTBalloon, False, lFColor, lBcolor, 5000, 0
           'End If
    End Select
    
End Sub

Private Sub cSubLV_beforeEdit(ByVal Columna As Integer, Cancel As Boolean)

    
    If Columna = 2 Or Columna = 0 Or Columna = 3 Or Columna = 1 Then
       Cancel = True
       cSubLV.SowToolTipText "Info", "Esta columna es de solo lectura y no se puede editar", TTIconInfo, TTBalloon, False, , , 5000, 0
    End If
   ' If Val(txtNroPlanilla) = 0 Then
   '     cSubLV.SowToolTipText "Info", "No hay planilla Activa para ingresar cantidades", TTIconInfo, TTBalloon, False, , , 5000, 0
   '     Cancel = True
   ' End If
End Sub
Private Sub cSubLV2_beforeEdit(ByVal Columna As Integer, Cancel As Boolean)

    
    '�If Columna = 2 Or Columna = 0 Or Columna = 3 Or Columna = 1 Then
       Cancel = True
    '   cSubLV2.SowToolTipText "Info", "Esta columna es de solo lectura y no se puede editar", TTIconInfo, TTBalloon, False, , , 5000, 0
   ' End If
   ' If Val(txtNroPlanilla) = 0 Then
   '     cSubLV.SowToolTipText "Info", "No hay planilla Activa para ingresar cantidades", TTIconInfo, TTBalloon, False, , , 5000, 0
   '     Cancel = True
    '�End If
End Sub

Sub setColumnHeader( _
    SpathSkin As String, _
    lColorNormal As Long, _
    lColorUp As Long, _
    Optional bIconAlingmentRight As Boolean = False, _
    Optional bTextBold As Boolean = False, _
    Optional BackColortextBox As Long = vbWhite, _
    Optional ForeColorTxtEdit As Long = vbBlack)
    
  '  Exit Sub
  '  With cSubLV
  '      .SkinPicture = LoadPicture(SpathSkin & ".bmp")
  '      .TextNormalColor = lColorNormal
  '      .TextResalteColor = lColorUp
  '      .IconAlingmentRight = bIconAlingmentRight
  '      .HedersFontBlod = bTextBold
  '
  '      .SetPropertyTextBoxEdit BackColortextBox, ForeColorTxtEdit
  '  End With
     With cSubLV2
        .SkinPicture = LoadPicture(SpathSkin & ".bmp")
        .TextNormalColor = lColorNormal
        .TextResalteColor = lColorUp
        .IconAlingmentRight = bIconAlingmentRight
        .HedersFontBlod = bTextBold
        
        .SetPropertyTextBoxEdit BackColortextBox, ForeColorTxtEdit
    End With
End Sub


