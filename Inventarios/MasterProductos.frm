VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{28D47522-CF84-11D1-834C-00A0249F0C28}#1.0#0"; "gif89.dll"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form MasterProductos 
   Appearance      =   0  'Flat
   BackColor       =   &H80000005&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Maestro de Productos y Servicios"
   ClientHeight    =   9825
   ClientLeft      =   5145
   ClientTop       =   1200
   ClientWidth     =   16515
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   9825
   ScaleWidth      =   16515
   Begin VB.Timer Timer1 
      Interval        =   1000
      Left            =   930
      Top             =   270
   End
   Begin VB.CommandButton Command4 
      Caption         =   "Productos sin"
      Height          =   255
      Left            =   7185
      TabIndex        =   76
      Top             =   1305
      Width           =   2250
   End
   Begin VB.CommandButton CmdTodosPro 
      Caption         =   "Todos"
      Height          =   450
      Left            =   1755
      TabIndex        =   71
      Top             =   210
      Width           =   1350
   End
   Begin VB.CommandButton CmdBuscaProd 
      Caption         =   "&Buscar"
      Height          =   450
      Left            =   3180
      TabIndex        =   70
      Top             =   210
      Width           =   1350
   End
   Begin VB.CommandButton CmdOkFiltro 
      Caption         =   "Ok"
      Height          =   225
      Left            =   11985
      TabIndex        =   55
      Top             =   1305
      Width           =   390
   End
   Begin VB.Frame FrmLoad 
      Height          =   1680
      Left            =   4230
      TabIndex        =   53
      Top             =   4200
      Visible         =   0   'False
      Width           =   2805
      Begin GIF89LibCtl.Gif89a Gif89a1 
         Height          =   1275
         Left            =   210
         OleObjectBlob   =   "MasterProductos.frx":0000
         TabIndex        =   54
         Top             =   285
         Width           =   2445
      End
   End
   Begin VB.TextBox TxtStock 
      Height          =   285
      Left            =   11265
      TabIndex        =   52
      Top             =   1290
      Width           =   675
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
      Height          =   225
      Left            =   10365
      OleObjectBlob   =   "MasterProductos.frx":0086
      TabIndex        =   51
      Top             =   1305
      Width           =   900
   End
   Begin VB.Frame Frame5 
      Caption         =   "Historial"
      Height          =   2430
      Left            =   210
      TabIndex        =   44
      Top             =   7785
      Width           =   16035
      Begin VB.CommandButton CmdSalir 
         Cancel          =   -1  'True
         Caption         =   "&Retornar"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   14385
         TabIndex        =   56
         Top             =   1875
         Width           =   1215
      End
      Begin TabDlg.SSTab SSTab1 
         Height          =   2085
         Left            =   105
         TabIndex        =   45
         Top             =   240
         Width           =   15690
         _ExtentX        =   27675
         _ExtentY        =   3678
         _Version        =   393216
         TabHeight       =   520
         WordWrap        =   0   'False
         TabCaption(0)   =   "Compras"
         TabPicture(0)   =   "MasterProductos.frx":00F2
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "SkinLabel3(3)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "SkinLabel3(2)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "SkinLabel3(1)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "SkinLabel4(0)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "SkinLabel3(0)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "LvDocumentos"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "TxtPventaBruto"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "txtPVentaNeto"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "TxtPCompraNeto"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "TxtPcompraBruto"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "txtDifBruto"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "TxtDifNeto"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "SkXcien"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).ControlCount=   13
         TabCaption(1)   =   "Ventas"
         TabPicture(1)   =   "MasterProductos.frx":010E
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "LvVentas"
         Tab(1).ControlCount=   1
         TabCaption(2)   =   "Ventas por Mes"
         TabPicture(2)   =   "MasterProductos.frx":012A
         Tab(2).ControlEnabled=   0   'False
         Tab(2).Control(0)=   "LvVtaMensual"
         Tab(2).ControlCount=   1
         Begin ACTIVESKINLibCtl.SkinLabel SkXcien 
            Height          =   240
            Left            =   14610
            OleObjectBlob   =   "MasterProductos.frx":0146
            TabIndex        =   68
            Top             =   1020
            Width           =   780
         End
         Begin VB.TextBox TxtDifNeto 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   13485
            Locked          =   -1  'True
            TabIndex        =   66
            Top             =   915
            Width           =   1050
         End
         Begin VB.TextBox txtDifBruto 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   13485
            Locked          =   -1  'True
            TabIndex        =   65
            Top             =   1185
            Width           =   1050
         End
         Begin VB.TextBox TxtPcompraBruto 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   11505
            Locked          =   -1  'True
            TabIndex        =   62
            Top             =   1200
            Width           =   1050
         End
         Begin VB.TextBox TxtPCompraNeto 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   11505
            Locked          =   -1  'True
            TabIndex        =   61
            Top             =   915
            Width           =   1050
         End
         Begin VB.TextBox txtPVentaNeto 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   12555
            Locked          =   -1  'True
            TabIndex        =   59
            Top             =   915
            Width           =   900
         End
         Begin VB.TextBox TxtPventaBruto 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   12555
            Locked          =   -1  'True
            TabIndex        =   57
            Top             =   1185
            Width           =   915
         End
         Begin MSComctlLib.ListView LvDocumentos 
            Height          =   1515
            Left            =   105
            TabIndex        =   46
            Top             =   420
            Width           =   10695
            _ExtentX        =   18865
            _ExtentY        =   2672
            View            =   3
            LabelWrap       =   0   'False
            HideSelection   =   0   'False
            FullRowSelect   =   -1  'True
            GridLines       =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   7
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Object.Tag             =   "F1200"
               Text            =   "Fecha"
               Object.Width           =   1940
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Object.Tag             =   "T1100"
               Text            =   "Nombre "
               Object.Width           =   3528
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   2
               Object.Tag             =   "T100"
               Text            =   "Documento"
               Object.Width           =   3528
            EndProperty
            BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   3
               Object.Tag             =   "T100"
               Text            =   "Nro Documento"
               Object.Width           =   2117
            EndProperty
            BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   4
               Object.Tag             =   "N102"
               Text            =   "Precio.U."
               Object.Width           =   2646
            EndProperty
            BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   5
               Key             =   "cant"
               Object.Tag             =   "N102"
               Text            =   "Cantidad"
               Object.Width           =   2646
            EndProperty
            BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   6
               Key             =   "total"
               Object.Tag             =   "N100"
               Text            =   "Total"
               Object.Width           =   2646
            EndProperty
         End
         Begin MSComctlLib.ListView LvVentas 
            Height          =   1425
            Left            =   -74895
            TabIndex        =   47
            Top             =   510
            Width           =   13875
            _ExtentX        =   24474
            _ExtentY        =   2514
            View            =   3
            LabelWrap       =   0   'False
            HideSelection   =   0   'False
            FullRowSelect   =   -1  'True
            GridLines       =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   7
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Object.Tag             =   "F1200"
               Text            =   "Fecha"
               Object.Width           =   1940
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Object.Tag             =   "T1100"
               Text            =   "Nombre "
               Object.Width           =   3528
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   2
               Object.Tag             =   "T100"
               Text            =   "Documento"
               Object.Width           =   3528
            EndProperty
            BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   3
               Object.Tag             =   "T100"
               Text            =   "Nro Documento"
               Object.Width           =   2117
            EndProperty
            BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   4
               Object.Tag             =   "N102"
               Text            =   "Precio.U."
               Object.Width           =   2646
            EndProperty
            BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   5
               Key             =   "cant"
               Object.Tag             =   "N102"
               Text            =   "Cantidad"
               Object.Width           =   2646
            EndProperty
            BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   6
               Key             =   "total"
               Object.Tag             =   "N100"
               Text            =   "Total"
               Object.Width           =   2646
            EndProperty
         End
         Begin MSComctlLib.ListView LvVtaMensual 
            Height          =   1560
            Left            =   -74790
            TabIndex        =   48
            ToolTipText     =   "Solo incluye las ventas, no resta las NC"
            Top             =   420
            Width           =   10350
            _ExtentX        =   18256
            _ExtentY        =   2752
            View            =   3
            LabelWrap       =   0   'False
            HideSelection   =   0   'False
            FullRowSelect   =   -1  'True
            GridLines       =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   2
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Object.Tag             =   "T1000"
               Text            =   "Periodo"
               Object.Width           =   2646
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Object.Tag             =   "N109"
               Text            =   "Unidades"
               Object.Width           =   3528
            EndProperty
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
            Height          =   240
            Index           =   0
            Left            =   10830
            OleObjectBlob   =   "MasterProductos.frx":01A4
            TabIndex        =   58
            Top             =   885
            Width           =   630
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
            Height          =   240
            Index           =   0
            Left            =   10800
            OleObjectBlob   =   "MasterProductos.frx":0210
            TabIndex        =   60
            Top             =   1185
            Width           =   660
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
            Height          =   240
            Index           =   1
            Left            =   12525
            OleObjectBlob   =   "MasterProductos.frx":027C
            TabIndex        =   63
            Top             =   720
            Width           =   945
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
            Height          =   240
            Index           =   2
            Left            =   11655
            OleObjectBlob   =   "MasterProductos.frx":02E8
            TabIndex        =   64
            Top             =   705
            Width           =   915
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
            Height          =   240
            Index           =   3
            Left            =   13740
            OleObjectBlob   =   "MasterProductos.frx":0358
            TabIndex        =   67
            Top             =   690
            Width           =   915
         End
      End
   End
   Begin VB.ComboBox CboTipo 
      Height          =   315
      ItemData        =   "MasterProductos.frx":03CA
      Left            =   13230
      List            =   "MasterProductos.frx":03D7
      Style           =   2  'Dropdown List
      TabIndex        =   43
      Top             =   1260
      Width           =   1815
   End
   Begin MSComDlg.CommonDialog Dialogo 
      Left            =   300
      Top             =   165
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.PictureBox PicTipo 
      BackColor       =   &H00FFFF80&
      Height          =   3960
      Left            =   8820
      ScaleHeight     =   3900
      ScaleWidth      =   4275
      TabIndex        =   33
      Top             =   1590
      Visible         =   0   'False
      Width           =   4335
      Begin VB.CommandButton Command2 
         Caption         =   "Por defecto"
         Height          =   210
         Left            =   3255
         TabIndex        =   38
         Top             =   3660
         Width           =   975
      End
      Begin VB.CommandButton CmdAplicaVistaCol 
         Caption         =   "Aplicar"
         Height          =   225
         Left            =   60
         TabIndex        =   37
         Top             =   3660
         Width           =   960
      End
      Begin VB.CommandButton cmdCierraTipos 
         Caption         =   "x"
         Height          =   195
         Left            =   4005
         TabIndex        =   34
         Top             =   15
         Width           =   270
      End
      Begin MSComctlLib.ListView LvColumnasVisibles 
         Height          =   3330
         Left            =   60
         TabIndex        =   35
         Top             =   285
         Width           =   4185
         _ExtentX        =   7382
         _ExtentY        =   5874
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         Checkboxes      =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   3
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Width           =   1058
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Nombre"
            Object.Width           =   5292
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "N109"
            Text            =   "Ancho Por defecto"
            Object.Width           =   0
         EndProperty
      End
      Begin VB.Label Label3 
         BackStyle       =   0  'Transparent
         Caption         =   "Seleccione columnas visibles"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000E&
         Height          =   195
         Left            =   390
         TabIndex        =   36
         Top             =   15
         Width           =   3165
      End
      Begin VB.Shape Shape2 
         BackColor       =   &H80000002&
         BackStyle       =   1  'Opaque
         FillColor       =   &H00FF0000&
         Height          =   240
         Left            =   0
         Top             =   15
         Width           =   4305
      End
   End
   Begin VB.PictureBox FrmDetalles 
      BackColor       =   &H00C0C000&
      Height          =   1170
      Left            =   525
      ScaleHeight     =   1110
      ScaleWidth      =   8175
      TabIndex        =   23
      Top             =   1575
      Visible         =   0   'False
      Width           =   8235
      Begin VB.CommandButton Command3 
         Caption         =   "+"
         Height          =   210
         Left            =   7530
         TabIndex        =   42
         Top             =   15
         Width           =   270
      End
      Begin VB.CommandButton CmdMinimizaProveedor 
         Caption         =   "__"
         Height          =   210
         Left            =   7170
         TabIndex        =   41
         Top             =   15
         Width           =   270
      End
      Begin VB.CommandButton CmdTodosLosProveedores 
         Caption         =   "Ver Todos los codigos de proveedor"
         Height          =   285
         Left            =   4365
         TabIndex        =   39
         Top             =   750
         Width           =   3390
      End
      Begin VB.TextBox txtRutProveedor 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1020
         TabIndex        =   29
         Top             =   360
         Width           =   1125
      End
      Begin VB.CommandButton cmdRutProveedor 
         Caption         =   "Proveedor"
         Height          =   270
         Left            =   90
         TabIndex        =   28
         Top             =   390
         Width           =   900
      End
      Begin VB.TextBox txtNombreProveedor 
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2130
         Locked          =   -1  'True
         TabIndex        =   27
         Top             =   360
         Width           =   5610
      End
      Begin VB.CommandButton VerConCodigoProveedor 
         Caption         =   "Ver del proveedor seleccionado"
         Height          =   285
         Left            =   1005
         TabIndex        =   26
         Top             =   750
         Width           =   3195
      End
      Begin VB.CommandButton CmdCierraDetalleCheques 
         Caption         =   "x"
         Height          =   255
         Left            =   7875
         TabIndex        =   24
         ToolTipText     =   "Cierra detalle de cheques"
         Top             =   -15
         Width           =   255
      End
      Begin VB.Label Label1 
         BackStyle       =   0  'Transparent
         Caption         =   "Seleccion de proveedor"
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   345
         TabIndex        =   25
         Top             =   30
         Width           =   1815
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H00FF0000&
         BackStyle       =   1  'Opaque
         Height          =   255
         Left            =   0
         Top             =   0
         Width           =   8220
      End
   End
   Begin VB.Frame Frame3 
      Caption         =   "Listado productos"
      Height          =   6165
      Left            =   180
      TabIndex        =   16
      Top             =   1605
      Width           =   16065
      Begin VB.CommandButton CmdGeneraArchivoPlano 
         Caption         =   "Generar Archivo Plano"
         Height          =   315
         Left            =   13905
         TabIndex        =   77
         Top             =   5595
         Width           =   1965
      End
      Begin VB.CommandButton CmdStockCritico 
         Caption         =   "Stock Critico"
         Height          =   315
         Left            =   10515
         TabIndex        =   75
         Top             =   5565
         Width           =   2145
      End
      Begin VB.CommandButton CmdDesmarcaOferta 
         Caption         =   "desmarcar en Oferta"
         Height          =   315
         Left            =   6705
         TabIndex        =   74
         Top             =   5580
         Width           =   1680
      End
      Begin VB.CommandButton CmdMarcaOferta 
         Caption         =   "Marcar en Oferta"
         Height          =   315
         Left            =   5145
         TabIndex        =   73
         Top             =   5580
         Width           =   1530
      End
      Begin VB.CommandButton CmdKyRSinInformacion 
         Caption         =   "Sin Informacion"
         Height          =   330
         Left            =   3690
         TabIndex        =   72
         ToolTipText     =   "Productos sin aplicaciones"
         Top             =   5580
         Width           =   1425
      End
      Begin VB.TextBox txtCntidadregistros 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   8850
         Locked          =   -1  'True
         TabIndex        =   49
         Text            =   "0"
         Top             =   5760
         Width           =   1365
      End
      Begin VB.CommandButton CmdMatrix 
         Caption         =   "Descuentos por volumen"
         Height          =   330
         Left            =   1665
         TabIndex        =   40
         Top             =   5580
         Width           =   2010
      End
      Begin VB.Frame FrmCargando 
         Caption         =   "Cargando"
         Height          =   810
         Left            =   4065
         TabIndex        =   30
         Top             =   1485
         Visible         =   0   'False
         Width           =   7275
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
            Height          =   330
            Left            =   2160
            OleObjectBlob   =   "MasterProductos.frx":0401
            TabIndex        =   31
            Top             =   300
            Width           =   4200
         End
      End
      Begin VB.Frame FraProgreso 
         Caption         =   "Progreso"
         Height          =   735
         Left            =   4755
         TabIndex        =   19
         Top             =   3930
         Visible         =   0   'False
         Width           =   6750
         Begin VB.PictureBox Picture1 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            ForeColor       =   &H80000008&
            Height          =   375
            Left            =   5595
            ScaleHeight     =   345
            ScaleWidth      =   705
            TabIndex        =   20
            Top             =   240
            Width           =   735
            Begin ACTIVESKINLibCtl.SkinLabel SkProgreso 
               Height          =   255
               Left            =   -15
               OleObjectBlob   =   "MasterProductos.frx":0487
               TabIndex        =   21
               Top             =   45
               Width           =   735
            End
         End
         Begin MSComctlLib.ProgressBar BarraProgreso 
            Height          =   375
            Left            =   150
            TabIndex        =   22
            Top             =   255
            Width           =   5415
            _ExtentX        =   9551
            _ExtentY        =   661
            _Version        =   393216
            Appearance      =   0
            Min             =   1e-4
         End
      End
      Begin VB.CommandButton CmdEliminarMarcados 
         Caption         =   "Eliminar Art�culos"
         Height          =   330
         Left            =   150
         TabIndex        =   18
         ToolTipText     =   "Eliminar Articulos Marcados"
         Top             =   5580
         Width           =   1485
      End
      Begin MSComctlLib.ListView LvDetalle 
         Height          =   5250
         Left            =   120
         TabIndex        =   17
         Top             =   255
         Width           =   15825
         _ExtentX        =   27914
         _ExtentY        =   9260
         View            =   3
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   21
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "T1000"
            Text            =   "Codigo"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "T1000"
            Text            =   "Cod Interno"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "T2000"
            Text            =   "Tipo Producto"
            Object.Width           =   3528
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Object.Tag             =   "T2000"
            Text            =   "Marca"
            Object.Width           =   3528
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Object.Tag             =   "T4000"
            Text            =   "Descripcion"
            Object.Width           =   7056
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   5
            Object.Tag             =   "N102"
            Text            =   "Pre. Compra"
            Object.Width           =   1676
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   6
            Object.Tag             =   "N102"
            Text            =   "Margen"
            Object.Width           =   1676
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   7
            Object.Tag             =   "N102"
            Text            =   "Pre. Venta"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   8
            Object.Tag             =   "N102"
            Text            =   "Utilidad"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   9
            Object.Tag             =   "N102"
            Text            =   "Stock Bodega"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   10
            Object.Tag             =   "N102"
            Text            =   "En Arriendo"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(12) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   11
            Object.Tag             =   "N102"
            Text            =   "Stock_Total"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(13) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   12
            Object.Tag             =   "N102"
            Text            =   "Stock Minimo"
            Object.Width           =   2293
         EndProperty
         BeginProperty ColumnHeader(14) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   13
            Object.Tag             =   "T2000"
            Text            =   "Bodega"
            Object.Width           =   4586
         EndProperty
         BeginProperty ColumnHeader(15) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   14
            Object.Tag             =   "T1500"
            Text            =   "Ubicacion Bodega"
            Object.Width           =   3528
         EndProperty
         BeginProperty ColumnHeader(16) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   15
            Object.Tag             =   "T2000"
            Text            =   "Obs"
            Object.Width           =   3528
         EndProperty
         BeginProperty ColumnHeader(17) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   16
            Object.Tag             =   "T1000"
            Text            =   "Inventariable"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(18) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   17
            Object.Tag             =   "T1500"
            Text            =   "Codigo Empresa"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(19) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   18
            Object.Tag             =   "T1000"
            Text            =   "Codigo Proveedor"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(20) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   19
            Object.Tag             =   "T1000"
            Text            =   "Noriega"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(21) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   20
            Object.Tag             =   "T1000"
            Text            =   "Oferta"
            Object.Width           =   2540
         EndProperty
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkiCantidadRegistros 
         Height          =   255
         Left            =   8745
         OleObjectBlob   =   "MasterProductos.frx":04E9
         TabIndex        =   50
         Top             =   5535
         Width           =   1410
      End
   End
   Begin VB.CommandButton CmdCodigoProveedor 
      Caption         =   "Ver c�digos proveedor"
      Height          =   255
      Left            =   150
      TabIndex        =   15
      Top             =   1275
      Width           =   1935
   End
   Begin VB.Frame Frame1 
      Caption         =   "Bodega"
      Height          =   885
      Left            =   14370
      TabIndex        =   13
      Top             =   270
      Width           =   1905
      Begin VB.ComboBox CboBodega 
         Height          =   315
         Left            =   60
         Style           =   2  'Dropdown List
         TabIndex        =   14
         Top             =   480
         Width           =   1800
      End
   End
   Begin VB.CommandButton CmdExporta 
      Caption         =   "&Exportar"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3000
      TabIndex        =   3
      ToolTipText     =   "Exportar a excel"
      Top             =   870
      Width           =   1215
   End
   Begin VB.CommandButton cmdNuevoKardex 
      Caption         =   "&Kardex"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1920
      TabIndex        =   2
      Top             =   870
      Width           =   1050
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   2250
      OleObjectBlob   =   "MasterProductos.frx":056B
      Top             =   8100
   End
   Begin VB.Frame Frame2 
      Caption         =   "Busqueda"
      Height          =   1110
      Left            =   4635
      TabIndex        =   9
      Top             =   135
      Width           =   11685
      Begin VB.OptionButton OptCodProveedor 
         Caption         =   "Codigo Proveedor"
         Height          =   255
         Left            =   2955
         TabIndex        =   69
         Top             =   675
         Width           =   1890
      End
      Begin VB.Frame FraFamilia 
         Caption         =   "Tipo Producto"
         Height          =   885
         Left            =   7680
         TabIndex        =   12
         Top             =   135
         Width           =   2010
         Begin VB.ComboBox CboTipoProducto 
            Height          =   315
            Left            =   120
            Style           =   2  'Dropdown List
            TabIndex        =   8
            Top             =   480
            Width           =   1815
         End
      End
      Begin VB.Frame FraMarca 
         Caption         =   "Marca"
         Height          =   885
         Left            =   5010
         TabIndex        =   11
         Top             =   135
         Width           =   2640
         Begin VB.ComboBox CboMarca 
            Height          =   315
            Left            =   90
            Style           =   2  'Dropdown List
            TabIndex        =   7
            Top             =   480
            Width           =   2505
         End
      End
      Begin VB.OptionButton OpCodigo 
         Caption         =   "Codigo"
         Height          =   255
         Left            =   1470
         TabIndex        =   6
         Top             =   675
         Width           =   1530
      End
      Begin VB.OptionButton OpDescripcion 
         Caption         =   "Descripcion"
         Height          =   255
         Left            =   135
         TabIndex        =   5
         Top             =   705
         Value           =   -1  'True
         Width           =   1185
      End
      Begin VB.TextBox TxtBusca 
         Height          =   375
         Left            =   120
         TabIndex        =   4
         Top             =   240
         Width           =   4845
      End
   End
   Begin VB.CommandButton CmdEditar 
      Caption         =   "&Editar"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1020
      TabIndex        =   1
      Top             =   870
      Width           =   900
   End
   Begin VB.CommandButton CmdNuevo 
      Caption         =   "&Nuevo"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   45
      TabIndex        =   0
      Top             =   870
      Width           =   945
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Ver Columnas"
      Height          =   255
      Left            =   2160
      TabIndex        =   32
      Top             =   1275
      Width           =   1935
   End
   Begin VB.Label Label2 
      Caption         =   "Label2"
      DataField       =   "CODIGO"
      DataSource      =   "AdoHistorial"
      Height          =   135
      Left            =   4755
      TabIndex        =   10
      Top             =   8085
      Visible         =   0   'False
      Width           =   855
   End
End
Attribute VB_Name = "MasterProductos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim RsProductos As Recordset
Dim s_Sql As String
Dim sql2 As String
Dim Sm_filtro As String
Dim Sm_FiltroMarca As String
Dim Sm_FiltroTipo  As String
Dim Sm_Filtrobodega As String
Dim Bm_GrillaCargada As Boolean
Dim Sp_CodigoProveedor As String
Dim Sp_SoloRut As String
Dim Sm_CodInternos As String * 2
Dim Sm_FiltroCodInterno As String
Dim Sm_TipoF As String
Dim Sm_FiltroStock As String
Dim Sm_FichaNetoBruto As String * 2
Dim Sm_FichaTuma As String * 2
Dim Sm_Stock_Critico As String
Dim Sm_EsHora As String * 2
Dim Form_Ficha As Form
Private Sub CboBodega_Click()
    If Not Bm_GrillaCargada Then Exit Sub
    If CboBodega.Text = "TODAS" Then
        Sm_Filtrobodega = Empty
    Else
        Sm_Filtrobodega = " s.bod_id=" & CboBodega.ItemData(CboBodega.ListIndex) & " AND "
    End If
    Grilla
End Sub

Private Sub CboMarca_Click()
    If Not Bm_GrillaCargada Then Exit Sub
    If CboMarca.Text = "TODOS" Then
        Sm_FiltroMarca = Empty
    Else
        Sm_FiltroMarca = " AND p.mar_id=" & CboMarca.ItemData(CboMarca.ListIndex) & " "
    End If
    
    Grilla
End Sub

Private Sub CboTipo_Click()
    If CboTipo.ListIndex = -1 Then Exit Sub
    If CboTipo.ListIndex = 0 Then
        Sm_TipoF = " AND pro_tipo='F' "
    ElseIf CboTipo.ListIndex = 1 Then
        Sm_TipoF = " AND pro_tipo='M' "
    Else
        Sm_TipoF = ""
    End If
    Grilla
End Sub

Private Sub CboTipoProducto_Click()
    If Not Bm_GrillaCargada Then Exit Sub
    If CboTipoProducto.Text = "TODOS" Then
        Sm_FiltroTipo = Empty
    Else
        Sm_FiltroTipo = " AND p.tip_id=" & CboTipoProducto.ItemData(CboTipoProducto.ListIndex) & " "
    End If
    Grilla
End Sub

Private Sub CmdAplicaVistaCol_Click()
    For i = 1 To LvColumnasVisibles.ListItems.Count
        If LvColumnasVisibles.ListItems(i).Checked Then
            LvDetalle.ColumnHeaders(i).Width = LvColumnasVisibles.ListItems(i).SubItems(2)
        Else
            LvDetalle.ColumnHeaders(i).Width = 0
        End If
        
    
    Next

    PicTipo.Visible = False
End Sub

Private Sub CmdBuscaProd_Click()
    Sm_filtro = Empty
    If Len(TxtBusca) > 0 Then
        If Me.OpDescripcion.Value Then
            Sm_filtro = "AND descripcion LIKE '%" & TxtBusca & "%' "
        End If
        If Me.OpCodigo Then
            Sm_FiltroCodInterno = ""
            Sm_filtro = "AND codigo LIKE '%" & TxtBusca & "%' "
            If Sm_CodInternos = "SI" Then
                Sm_FiltroCodInterno = " AND pro_codigo_interno ='" & TxtBusca & "' "
            End If
        End If
        If Me.OptCodProveedor Then
            Sm_filtro = " AND (SELECT COUNT(*) FROM par_codigos_proveedor pr WHERE  cpv_codigo_proveedor LIKE '" & Me.TxtBusca & "%' AND  pr.pro_codigo=codigo)>0 "
        End If
        Grilla
    End If
End Sub

Private Sub CmdCierraDetalleCheques_Click()
    Sp_CodigoProveedor = ""
    FrmDetalles.Visible = False
End Sub

Private Sub cmdCierraTipos_Click()
    PicTipo.Visible = False
End Sub

Private Sub CmdCodigoProveedor_Click()
    FrmDetalles.Visible = True
End Sub

Private Sub CmdDesmarcaOferta_Click()
    For i = 1 To LvDetalle.ListItems.Count
        If LvDetalle.ListItems(i).Checked Then
           ' Update
           '3 Junio 2019 poner productos en oferta kyr
            Sql = "UPDATE maestro_productos SET pro_en_oferta='NO' " & _
                "WHERE codigo=" & LvDetalle.ListItems(i)
            cn.Execute Sql
            LvDetalle.ListItems(i).SubItems(20) = "NO"
        End If
    Next
    MsgBox "Productos seleccionados han sido desmarcados de Oferta", vbInformation
End Sub

Private Sub CmdEditar_Click()
    Dim Lp_Cx As Long
    For Lp_Cx = 1 To LvDetalle.ListItems.Count
        If LvDetalle.ListItems(Lp_Cx).Checked Then
            SG_codigo = LvDetalle.ListItems(Lp_Cx)
            
            
            If Len(SG_Formulario_Ficha_Producto) > 0 Then
        'traemos el formulario de la bd 'parametro 27000
                Set Form_Ficha = Forms.Add(SG_Formulario_Ficha_Producto)
                SG_codigo = ""
                Form_Ficha.Bm_Nuevo = False
                Form_Ficha.Show 1
                If SG_codigo3 = "GRABO" Then
                     GoTo sale
                Else
                    Exit Sub
                End If
                    
            End If
            
            
            
            If Sm_FichaNetoBruto = "SI" Then
                inv_ficha_bs.Bm_Nuevo = False
                inv_ficha_bs.Show 1
        
 '       AgregaProductoBarraStuder.Bm_Nuevo = True
 '       AgregaProductoBarraStuder.Show 1
                GoTo sale
            End If
            
            
            '10 mayo 2019
            If Sm_FichaTuma = "SI" Then
              '  SG_codigo = ""
              '  SG_codigo = Empty
                inv_ficha_articulos.Bm_Nuevo = False
                inv_ficha_articulos.Show 1
    
                GoTo sale
            End If
            
          
            If SG_Es_la_Flor = "SI" Then
                If SP_Rut_Activo = "76.178.895-7" Then
                    AgregaProductoTG.Bm_Nuevo = False
                    AgregaProductoTG.Show 1
                Else
                    AgregarProductoFlor.Show 1
                End If
            Else
                AgregarProducto.Bm_Nuevo = False
                AgregarProducto.Show 1
            End If
            
        End If
    Next
sale:
    Grilla
End Sub

Private Sub CmdEliminarMarcados_Click()
        Dim Lp_CantidadEliminados As Long
        Lp_CantidadEliminados = 0
        cn.BeginTrans
        For i = 1 To LvDetalle.ListItems.Count
            If LvDetalle.ListItems(i).Checked Then
                SG_codigo = LvDetalle.ListItems(i)
                Sql = "SELECT codigo " & _
                        "FROM ven_detalle " & _
                        "WHERE codigo = '" & SG_codigo & "' AND rut_emp='" & SP_Rut_Activo & "' " & _
                        "UNION " & _
                        "SELECT pro_codigo " & _
                        "FROM com_doc_compra_detalle d " & _
                        "INNER JOIN com_doc_compra c ON d.id=c.id " & _
                        "WHERE pro_codigo = '" & SG_codigo & "' AND rut_emp='" & SP_Rut_Activo & "' "
                Consulta RsTmp, Sql
                
                If RsTmp.RecordCount > 0 Then
                
                    If SP_Rut_Activo = "76.978.874-3" Or SP_Rut_Activo = "76.169.962-8" Or SP_Rut_Activo = "76.504.041-8" Then
                            If MsgBox("Este art�culo ha sido utilizado en compras y/o ventas ..." & vbNewLine & _
                                   "no es posible eliminarlo ya que se perderian tambien los registros anteriores." & _
                                   vbNewLine & " �Continuar sabiendo esto?...", vbQuestion + vbOKCancel) = vbOK Then
                                    
                                    cn.Execute "DELETE FROM maestro_productos WHERE codigo='" & SG_codigo & "' AND rut_emp='" & SP_Rut_Activo & "'"
                                    Lp_CantidadEliminados = Lp_CantidadEliminados + 1
                            End If
                    Else
                             MsgBox "Este art�culo ha sido utilizado en compras y/o ventas ..." & vbNewLine & _
                                   "no es posible eliminarlo ya que se perderian tambien los registros anteriores."
                    End If
                Else
                    'Eliminar el codigo
                    cn.Execute "DELETE FROM maestro_productos WHERE codigo='" & SG_codigo & "' AND rut_emp='" & SP_Rut_Activo & "'"
                    Lp_CantidadEliminados = Lp_CantidadEliminados + 1
                    
                End If
            
            End If
        Next
        
        If Lp_CantidadEliminados > 0 Then
            If MsgBox("Se eliminar�n " & Lp_CantidadEliminados & " art�culos... " & vbNewLine & _
                   " �Continuar? ... ", vbQuestion + vbOKCancel) = vbOK Then
                cn.CommitTrans
            Else
                cn.RollbackTrans
                
            End If
        Else
            cn.RollbackTrans
        End If
        Grilla
End Sub

Private Sub CmdExporta_Click()
    Dim tit(2) As String
    FraProgreso.Visible = True
    tit(0) = Me.Caption
    tit(1) = "FECHA INFORME: " & Date & " - " & Time
    tit(2) = "" ' "                                        DESDE:" & DtInicio.Value & "   HASTA:" & Dtfin.Value
    Exportar LvDetalle, tit, Me, BarraProgreso, SkProgreso
    BarraProgreso.Value = 1
    SkProgreso = "0%"
    FraProgreso.Visible = False
End Sub

Private Sub CmdFamilias_Click()
    With Mantenedor_Dependencia
        .S_Id = "p.tip_id"
        .S_Nombre = "tip_nombre"
        .S_Activo = "tip_activo"
        .S_tabla = "par_tipos_productos p"
        .S_RutEmpresa = "SI"
        .S_Consulta = "SELECT tip_id,pla_nombre,tip_nombre,tip_activo " & _
                      "FROM par_tipos_productos p " & _
                      "INNER JOIN con_plan_de_cuentas c ON p.pla_id=c.pla_id " & _
                      "WHERE  rut_emp='" & SP_Rut_Activo & "' "
        .Caption = "Mantenedor Tipos de productos"
        .FrmMantenedor.Caption = "Tipos de productos "
        .S_id_dep = "c.pla_id"
        .S_nombre_dep = "pla_nombre"
        .S_tabla_dep = "con_plan_de_cuentas c"
        .S_Activo_dep = "pla_activo"
        .S_titulo_dep = "Cuenta contable"
        .B_Editable = True
        .Show 1
    End With
    
End Sub



Private Sub CmdMarcas_Click()
    With Mantenedor_Simple
        .S_Id = "mar_id"
        .S_Nombre = "mar_nombre"
        .S_Activo = "mar_activo"
        .S_tabla = "par_marcas"
        .S_RutEmpresa = "SI"
        .S_Consulta = "SELECT " & .S_Id & "," & .S_Nombre & "," & .S_Activo & " " & _
                      "FROM " & .S_tabla & " " & _
                      "WHERE  rut_emp='" & SP_Rut_Activo & "' "
        .Caption = "Mantenedor Marcas de Articulos"
        .FrmMantenedor.Caption = "Marcas "
        .B_Editable = True
        .Show 1
    End With
End Sub

Private Sub CmdGeneraArchivoPlano_Click()
    Dim Sp_NomArchivo As String
    Dim Ip_Cantidad As Integer
    If LvDetalle.ListItems.Count = 0 Then Exit Sub
    
    Ip_Cantidad = 0
    
    For i = 1 To LvDetalle.ListItems.Count
        If LvDetalle.ListItems(i).Checked Then Ip_Cantidad = Ip_Cantidad + 1
    Next
    
    If Ip_Cantidad = 0 Then Exit Sub
    
    On Error GoTo sale
    Dialogo.ShowSave
    
    Sp_NomArchivo = Dialogo.FileName
    X = FreeFile
    Open Sp_NomArchivo For Output As X
        Print #X, Chr(34) & "CODIGO" & Chr(34) & "," & Chr(34) & "NOMBRE" & Chr(34)
        For i = 1 To LvDetalle.ListItems.Count
            If LvDetalle.ListItems(i).Checked Then Print #X, Chr(34) & LvDetalle.ListItems(i) & Chr(34) & "," & Chr(34) & LvDetalle.ListItems(i).SubItems(4) & Chr(34)
        Next
    Close #X
    
    MsgBox Sp_NomArchivo & vbNewLine & "Generado correctamente..."
    
    
    Exit Sub
sale:
    
End Sub

Private Sub CmdKyRSinInformacion_Click()
    Sql = "SELECT codigo " & _
        "FROM maestro_productos p " & _
        "WHERE pro_activo='SI' AND " & _
            "(select count(*) from inv_relaciona_codigo_marca_modelo x WHERE p.codigo=x.pro_codigo)=0"
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        RsTmp.MoveFirst
        Sm_filtro = ""
        Do While Not RsTmp.EOF
            Sm_filtro = Sm_filtro & RsTmp!Codigo & ","
            RsTmp.MoveNext
        Loop
        If Len(Sm_filtro) > 0 Then
            Sm_filtro = " AND codigo IN(" & Mid(Sm_filtro, 1, Len(Sm_filtro) - 1) & ")"
            Grilla
        End If
        
    End If
End Sub

Private Sub CmdMarcaOferta_Click()
    For i = 1 To LvDetalle.ListItems.Count
        If LvDetalle.ListItems(i).Checked Then
           ' Update
           '3 Junio 2019 poner productos en oferta kyr
            Sql = "UPDATE maestro_productos SET pro_en_oferta='SI' " & _
                "WHERE codigo=" & LvDetalle.ListItems(i)
            cn.Execute Sql
            LvDetalle.ListItems(i).SubItems(20) = "SI"
           
        End If
    Next
    MsgBox "Los productos seleccionados, fueron marcados en oferta!", vbInformation
End Sub

Private Sub CmdMatrix_Click()
    Dim Sp_Codigos As String
    Sp_Codigos = ""
    For i = 1 To LvDetalle.ListItems.Count
        If LvDetalle.ListItems(i).Checked Then
            
            inv_DescuentosPorVolumen.TxtCodSistema = LvDetalle.ListItems(i)
            inv_DescuentosPorVolumen.TxtNombre = LvDetalle.ListItems(i).SubItems(4)
            inv_DescuentosPorVolumen.TxtCodBarra = LvDetalle.ListItems(i).SubItems(17)
            inv_DescuentosPorVolumen.TxtPrecioU = LvDetalle.ListItems(i).SubItems(7)
            inv_DescuentosPorVolumen.CargaMatriz
            inv_DescuentosPorVolumen.Show 1
        End If
    Next
    Exit Sub
    If Len(Sp_Codigos) > 0 Then
        Sp_Codigos = Mid(Sp_Codigos, 1, Len(Sp_Codigos) - 1)
        inv_DescuentosPorVolumen.TxtNombre = Sp_Codigos
        inv_DescuentosPorVolumen.CargaMatriz
        inv_DescuentosPorVolumen.Show 1
    End If
End Sub

Private Sub CmdMinimizaProveedor_Click()
    FrmDetalles.Height = 300
End Sub

Private Sub CmdNuevo_Click()
    SG_codigo = Empty
    If Len(SG_Formulario_Ficha_Producto) > 0 Then
        'traemos el formulario de la bd 'parametro 27000
        Set Form_Ficha = Forms.Add(SG_Formulario_Ficha_Producto)
        SG_codigo = ""
        Form_Ficha.Bm_Nuevo = True
        Form_Ficha.Show 1
        If SG_codigo3 = "GRABO" Then
             GoTo sale
        Else
            Exit Sub
        End If
            
    End If
    
    
    If Sm_FichaNetoBruto = "SI" Then
        SG_codigo = Empty
        SG_codigo = ""
        inv_ficha_bs.Bm_Nuevo = True
        inv_ficha_bs.Show 1
        
 '       AgregaProductoBarraStuder.Bm_Nuevo = True
 '       AgregaProductoBarraStuder.Show 1
        GoTo sale
    End If
    If Sm_FichaTuma = "SI" Then
         SG_codigo = Empty
        SG_codigo = ""
        inv_ficha_articulos.Bm_Nuevo = True
        inv_ficha_articulos.Show 1
    
        GoTo sale
    End If
    
 '   Sql = "SELECT emp_de_repuestos " & _
            "FROM sis_empresas " & _
            "WHERE rut='" & SP_Rut_Activo & "'"
 '   Consulta RsTmp, Sql
 '   If RsTmp!emp_de_repuestos = "SI" Then
        
 '       inv_ficha_respuestos.Show 1
 '   Else

         
        If SG_Es_la_Flor = "SI" Then
                    
                    If SP_Rut_Activo = "76.178.895-7" Then
                        'ficha escpecial totalgomas
                        AgregaProductoTG.Bm_Nuevo = True
                        AgregaProductoTG.Show 1
                    Else
                        AgregarProductoFlor.Bm_Nuevo = True
                        AgregarProductoFlor.Show 1
                    End If
                    
        Else
            AgregarProducto.Bm_Nuevo = True
            AgregarProducto.Show 1
        End If
        
sale:
  '  End If
    Grilla
End Sub

Private Sub cmdNuevoKardex_Click()
    'With Me.GridProductos
    '    .Col = 2
    '    Inv_Kardex.TxtCodigo = .Text
        inv_KardexGeneral.Show 1
   ' End With
End Sub

Private Sub CmdOkFiltro_Click()
    If Val(TxtStock) = 0 Then
        Sm_FiltroStock = ""
    Else
    
        Sm_FiltroStock = " AND (SELECT SUM(sto_stock) " & _
                        "FROM pro_stock s " & _
                        "WHERE s.rut_emp = '" & SP_Rut_Activo & "' AND s.pro_codigo = p.codigo)<" & Val(TxtStock)
    End If
    Grilla
End Sub

Private Sub cmdRutProveedor_Click()
    AccionProveedor = 0
    BuscaProveedor.Show 1
    TxtRutProveedor = RutBuscado

    TxtRutProveedor_Validate True
End Sub

Private Sub CmdSalir_Click()
    Unload Me
End Sub

Private Sub CmdStockCritico_Click()
    Sm_Stock_Critico = " HAVING stock_bodega < stock_critico "
    Grilla
End Sub

Private Sub CmdTodosLosProveedores_Click()
    Dim Ip_Mayor As Integer
    Sp_CodigoProveedor = ""
    Sp_SoloRut = ""
    FrmCargando.Visible = True
    FraProgreso.Visible = True
    DoEvents
    BarraProgreso.Value = 1
    BarraProgreso.Max = LvDetalle.ListItems.Count
    Ip_Mayor = 1
    Sql = "SELECT COUNT(pro_codigo) codigo " & _
            "FROM par_codigos_proveedor " & _
            "GROUP BY rut_emp,pro_codigo " & _
            "HAVING Codigo > 1 "
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        RsTmp.MoveFirst
        Do While Not RsTmp.EOF
            If RsTmp!Codigo > Ip_Mayor Then Ip_Mayor = RsTmp!Codigo
            RsTmp.MoveNext
        Loop
    End If
    'Cantidad de columnas normla de la grillal de productos
    If LvDetalle.ColumnHeaders.Count > 18 Then
        For i = LvDetalle.ColumnHeaders.Count To 1 Step -1
            LvDetalle.ColumnHeaders.Remove LvDetalle.ColumnHeaders.Count
            If LvDetalle.ColumnHeaders.Count = 18 Then Exit For
        Next
    End If
    If Ip_Mayor > 1 Then
            For i = 2 To Ip_Mayor
                LvDetalle.ColumnHeaders.Add , , "Prov " & i, 1000, 1
            Next
    End If
    For i = 1 To LvDetalle.ListItems.Count
            BarraProgreso.Value = i
            SkProgreso = i
  '  SELECT cpv_codigo_proveedor FROM par_codigos_proveedor o WHERE o.pro_codigo=p.codigo
            Col = 17
            Sql = "SELECT rut_proveedor,cpv_codigo_proveedor " & _
                    "FROM par_codigos_proveedor " & _
                    "WHERE pro_codigo='" & LvDetalle.ListItems(i) & "' AND rut_emp='" & SP_Rut_Activo & "'"
            Consulta RsTmp, Sql
            If RsTmp.RecordCount > 0 Then
                RsTmp.MoveFirst
                Do While Not RsTmp.EOF
                    LvDetalle.ListItems(i).SubItems(Col) = RsTmp!rut_proveedor & " : " & RsTmp!cpv_codigo_proveedor
            
                    RsTmp.MoveNext
                    Col = Col + 1
                Loop
            End If
    Next
    FraProgreso.Visible = False
    FrmCargando.Visible = False
End Sub
Private Sub CmdTodosPro_Click()
    Me.TxtBusca.Text = ""
    Sm_filtro = Empty
    Grilla
End Sub
Private Sub Grilla()
    s_sql2 = ""
    FrmLoad.Visible = True
    DoEvents
    'bod_id= 1, ya que las bodegas principales, son 1
    If SP_Rut_Activo = "76.978.874-3" Then
        'tumas 28 mayo 2019
        s_Sql = "SELECT codigo,pro_codigo_interno,tip_nombre,mar_nombre,descripcion," & _
             " precio_compra," & _
             "porciento_utilidad,precio_venta,precio_venta-" & _
                    "(precio_compra*1.19) margen," & _
              "IF(pro_inventariable='SI',IFNULL((SELECT SUM(sto_stock) FROM pro_stock s WHERE " & Sm_Filtrobodega & " s.rut_emp='" & SP_Rut_Activo & "' AND s.pro_codigo=p.codigo limit 1),0),0) stock_bodega," & _
              "IFNULL((SELECT SUM(arr_cantidad) " & _
                "FROM inv_productos_arrendados WHERE arr_devuelto='NO' AND pro_codigo=p.codigo AND emp_id=" & IG_id_Empresa & "),0) arrendados,0, " & _
              "stock_critico, bod_nombre,ubicacion_bodega,comentario,pro_inventariable,pro_codigo_interno " & Sp_CodigoProveedor & ",pro_noriega,pro_en_oferta " & _
          "FROM maestro_productos p,par_tipos_productos t,par_marcas m,par_bodegas b " & _
          "WHERE  p.rut_emp='" & SP_Rut_Activo & "' AND  p.tip_id=t.tip_id AND p.mar_id=m.mar_id AND p.bod_id=b.bod_id " & Sm_filtro & Sm_FiltroTipo & Sm_FiltroMarca & Sm_TipoF & _
          Sp_SoloRut & " " & Sm_FiltroStock & Sm_Stock_Critico
    
            If Sm_CodInternos = "SI" And Bm_GrillaCargada And OpCodigo Then
            
                s_sql2 = " UNION SELECT codigo,pro_codigo_interno,tip_nombre,mar_nombre,descripcion," & _
                     " precio_compra," & _
                     "porciento_utilidad,precio_venta,precio_venta-(precio_compra*1.19) margen," & _
                      "IF(pro_inventariable='SI',IFNULL((SELECT SUM(sto_stock) FROM pro_stock s WHERE " & Sm_Filtrobodega & " s.rut_emp='" & SP_Rut_Activo & "' AND s.pro_codigo=p.codigo),0),0) stock_bodega," & _
                      "IFNULL((SELECT SUM(arr_cantidad) " & _
                        "FROM inv_productos_arrendados WHERE arr_devuelto='NO' AND pro_codigo=p.codigo AND emp_id=" & IG_id_Empresa & "),0) arrendados,0, " & _
                      "stock_critico, bod_nombre,ubicacion_bodega,comentario,pro_inventariable,pro_codigo_interno " & Sp_CodigoProveedor & ",pro_noriega,pro_en_oferta " & _
                  "FROM maestro_productos p,par_tipos_productos t,par_marcas m,par_bodegas b " & _
                  "WHERE p.rut_emp='" & SP_Rut_Activo & "' AND  p.tip_id=t.tip_id AND p.mar_id=m.mar_id AND p.bod_id=b.bod_id " & Sm_FiltroCodInterno & Sm_FiltroTipo & Sm_FiltroMarca & Sm_TipoF & _
                  Sp_SoloRut & " " & Sm_FiltroStock & Sm_Stock_Critico
            End If
    Else
        s_Sql = "SELECT codigo,pro_codigo_interno,tip_nombre,mar_nombre,descripcion," & _
             "IFNULL((SELECT AVG(pro_ultimo_precio_compra) FROM pro_stock s WHERE s.rut_emp='" & SP_Rut_Activo & "' AND s.pro_codigo=p.codigo AND bod_id=1),precio_compra) precio_compra," & _
             "porciento_utilidad,precio_venta,precio_venta-IFNULL((" & _
                    "SELECT AVG(pro_ultimo_precio_compra) " & _
                    "FROM pro_stock s " & _
                    "WHERE s.rut_emp = '" & SP_Rut_Activo & "' " & _
                    "AND s.pro_codigo = p.codigo)," & _
                    "precio_compra) margen," & _
              "IF(pro_inventariable='SI',IFNULL((SELECT SUM(sto_stock) FROM pro_stock s WHERE " & Sm_Filtrobodega & " s.rut_emp='" & SP_Rut_Activo & "' AND s.pro_codigo=p.codigo),0),0) stock_bodega," & _
              "IFNULL((SELECT SUM(arr_cantidad) " & _
                "FROM inv_productos_arrendados WHERE arr_devuelto='NO' AND pro_codigo=p.codigo AND emp_id=" & IG_id_Empresa & "),0) arrendados,0, " & _
              "stock_critico, bod_nombre,ubicacion_bodega,comentario,pro_inventariable,pro_codigo_interno " & Sp_CodigoProveedor & ",pro_noriega,pro_en_oferta " & _
          "FROM maestro_productos p,par_tipos_productos t,par_marcas m,par_bodegas b " & _
          "WHERE p.pro_maquina='NO' AND p.rut_emp='" & SP_Rut_Activo & "' AND  p.tip_id=t.tip_id AND p.mar_id=m.mar_id AND p.bod_id=b.bod_id " & Sm_filtro & Sm_FiltroTipo & Sm_FiltroMarca & Sm_TipoF & _
          Sp_SoloRut & " " & Sm_FiltroStock & Sm_Stock_Critico
            If Sm_CodInternos = "SI" And Bm_GrillaCargada And OpCodigo Then
                s_sql2 = " UNION SELECT codigo,pro_codigo_interno,tip_nombre,mar_nombre,descripcion," & _
                     "IFNULL((SELECT AVG(pro_ultimo_precio_compra) FROM pro_stock s WHERE s.rut_emp='" & SP_Rut_Activo & "' AND s.pro_codigo=p.codigo),0) precio_compra," & _
                     "porciento_utilidad,precio_venta,margen," & _
                      "IF(pro_inventariable='SI',IFNULL((SELECT SUM(sto_stock) FROM pro_stock s WHERE " & Sm_Filtrobodega & " s.rut_emp='" & SP_Rut_Activo & "' AND s.pro_codigo=p.codigo),0),0) stock_bodega," & _
                      "IFNULL((SELECT SUM(arr_cantidad) " & _
                        "FROM inv_productos_arrendados WHERE arr_devuelto='NO' AND pro_codigo=p.codigo AND emp_id=" & IG_id_Empresa & "),0) arrendados,0, " & _
                      "stock_critico, bod_nombre,ubicacion_bodega,comentario,pro_inventariable,pro_codigo_interno " & Sp_CodigoProveedor & ",pro_noriega,pro_en_oferta " & _
                  "FROM maestro_productos p,par_tipos_productos t,par_marcas m,par_bodegas b " & _
                  "WHERE  p.pro_maquina='NO' AND  p.rut_emp='" & SP_Rut_Activo & "' AND  p.tip_id=t.tip_id AND p.mar_id=m.mar_id AND p.bod_id=b.bod_id " & Sm_FiltroCodInterno & Sm_FiltroTipo & Sm_FiltroMarca & Sm_TipoF & _
                  Sp_SoloRut & " " & Sm_FiltroStock & Sm_Stock_Critico
            End If
    End If
    Consulta RsProductos, s_Sql & s_sql2
    LLenar_Grilla RsProductos, Me, LvDetalle, True, True, True, False
    txtCntidadregistros = RsProductos.RecordCount
    For i = 1 To LvDetalle.ListItems.Count
        LvDetalle.ListItems(i).SubItems(9) = CDbl(LvDetalle.ListItems(i).SubItems(9)) - CDbl(LvDetalle.ListItems(i).SubItems(10))
        LvDetalle.ListItems(i).SubItems(11) = CDbl(LvDetalle.ListItems(i).SubItems(10)) + CDbl(LvDetalle.ListItems(i).SubItems(9))
        If SG_PriorizaCodigoInterno = "SI" Then
            LvDetalle.ListItems(i).ListSubItems(1).Bold = True
        End If
        
    Next
    FrmLoad.Visible = False
End Sub

Private Sub Command1_Click()
    PicTipo.Visible = True
End Sub

Private Sub Command2_Click()
    For i = 1 To LvColumnasVisibles.ListItems.Count
        
        LvDetalle.ColumnHeaders(i).Width = LvColumnasVisibles.ListItems(i).SubItems(2)
        
        If LvDetalle.ColumnHeaders(i).Width > 0 Then
            LvColumnasVisibles.ListItems(i).Checked = True
        End If
    Next
    PicTipo.Visible = False
    
End Sub



Private Sub Command3_Click()
    FrmDetalles.Height = 1170
End Sub




Private Sub Form_Load()
    Sm_EsHora = "NO"
    Centrar Me
    FrmLoad.Visible = True
    DoEvents
    Bm_GrillaCargada = False
    Sp_CodigoProveedor = ",'' cpr "
    Sm_Stock_Critico = ""
    Sp_SoloRut = ""
   ' BuscaProducto.AdoProducto.Refresh
    LLenarCombo CboTipoProducto, "tip_nombre", "tip_id", "par_tipos_productos", "tip_activo='SI' AND  rut_emp='" & SP_Rut_Activo & "'", "tip_nombre"
    CboTipoProducto.AddItem "TODOS"
    CboTipoProducto.ListIndex = CboTipoProducto.ListCount - 1
    LLenarCombo CboMarca, "mar_nombre", "mar_id", "par_marcas", "mar_activo='SI' AND  rut_emp='" & SP_Rut_Activo & "'", "mar_nombre"
    CboMarca.AddItem "TODOS"
    CboMarca.ListIndex = CboMarca.ListCount - 1
    
    LLenarCombo CboBodega, "bod_nombre", "bod_id", "par_bodegas", "bod_activo='SI' AND rut_emp='" & SP_Rut_Activo & "'", "bod_nombre"
    CboBodega.AddItem "TODAS"
    CboBodega.ListIndex = CboBodega.ListCount - 1
    
    
    Sql = "SELECT emp_utiliza_codigos_internos_productos codinterno " & _
        "FROM sis_empresas " & _
        "WHERE rut='" & SP_Rut_Activo & "'"
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        Sm_CodInternos = RsTmp!codinterno
    End If
    
    
    If SG_Codigos_Alfanumericos = "NO" Then
        LvDetalle.ColumnHeaders(1).Tag = "N109"
    End If
     
    If SP_Rut_Activo = "76.553.302-3" Then
        CmdKyRSinInformacion.Visible = True
    End If
    
    If SG_PriorizaCodigoInterno = "SI" Then
        LvDetalle.ColumnHeaders(2).Width = 1300
    End If
    Sm_FiltroMarca = Empty
    Sm_FiltroTipo = Empty
    Sm_Filtrobodega = Empty
    Skin2 Me, , 5
    Sm_EsHora = "SI"
End Sub
Private Sub CargaColumnasVisibles()
    LvColumnasVisibles.ListItems.Clear
    For i = 1 To LvDetalle.ColumnHeaders.Count
        LvColumnasVisibles.ListItems.Add , , i
        LvColumnasVisibles.ListItems(LvColumnasVisibles.ListItems.Count).SubItems(1) = LvDetalle.ColumnHeaders(i).Text
        LvColumnasVisibles.ListItems(LvColumnasVisibles.ListItems.Count).SubItems(2) = LvDetalle.ColumnHeaders(i).Width
        If LvDetalle.ColumnHeaders(i).Width > 0 Then
            LvColumnasVisibles.ListItems(LvColumnasVisibles.ListItems.Count).Checked = True
        End If
    Next
End Sub


Private Sub LvDetalle_Click()
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    VerHistorialProducto LvDetalle.SelectedItem
End Sub

Private Sub LvDetalle_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    ordListView ColumnHeader, Me, LvDetalle
End Sub

Private Sub Timer1_Timer()
    
    If Sm_EsHora = "NO" Then Exit Sub
    Timer1.Enabled = False
    DoEvents
    FrmLoad.Visible = True

    Grilla
    Bm_GrillaCargada = True
    
    If SP_Rut_Activo <> "76.244.196-9" Then
        LvDetalle.ColumnHeaders(10).Width = 0
    
    End If
    
    'Omitir columnas para mario acu�a
    '30-06-2017
    'AVV
    If SP_Rut_Activo = "12.072.366-9" Then
        LvDetalle.ColumnHeaders(3).Width = 0
        LvDetalle.ColumnHeaders(18).Width = 0
        LvDetalle.ColumnHeaders(17).Width = 0
        LvDetalle.ColumnHeaders(13).Width = 0
        LvDetalle.ColumnHeaders(10).Width = 0
    End If
    
    If SP_Rut_Activo <> "76.169.962-8" Then
         LvDetalle.ColumnHeaders(18).Width = 0
    End If
    Sql = "SELECT par_id,par_valor " & _
            "FROM tabla_parametros " & _
            "WHERE par_id IN(15000,16000)"
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
            RsTmp.MoveFirst
            Do While Not RsTmp.EOF
                If RsTmp!par_id = 15000 And RsTmp!par_valor = "SI" Then
                        Sm_FichaNetoBruto = "SI"
                End If
                If RsTmp!par_id = 16000 And RsTmp!par_valor = "SI" Then
                        Sm_FichaTuma = "SI"
                End If
                    
                    
            
                RsTmp.MoveNext
            Loop
            
    End If
    
    CargaColumnasVisibles
    FrmLoad.Visible = False
End Sub

Private Sub TxtBusca_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii))) 'Transformo a mayuscula el caracter ingresado
    If KeyAscii = 39 Then KeyAscii = 0
    If KeyAscii = 13 Then CmdBuscaProd_Click
End Sub
Private Sub TxtRutProveedor_Validate(Cancel As Boolean)
    If Len(TxtRutProveedor) = 0 Then Exit Sub
    Sql = "SELECT nombre_empresa " & _
        "FROM maestro_proveedores " & _
        "WHERE rut_proveedor='" & TxtRutProveedor & "'"
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        txtNombreProveedor = RsTmp!nombre_empresa
    End If
End Sub

Private Sub TxtStock_GotFocus()
    En_Foco TxtStock
End Sub

Private Sub TxtStock_KeyPress(KeyAscii As Integer)
    KeyAscii = AceptaSoloNumeros(KeyAscii)
End Sub

Private Sub VerConCodigoProveedor_Click()
    Sp_CodigoProveedor = ",'' cdpr"
    Sp_SoloRut = ""
    If Len(TxtRutProveedor) = 0 Then
        MsgBox "Debe ingresar RUT de Proveedor", vbInformation
    
        Exit Sub
        
    End If
    FrmCargando.Visible = True
    DoEvents
     Sp_CodigoProveedor = ",(SELECT cpv_codigo_proveedor FROM par_codigos_proveedor o WHERE o.pro_codigo=p.codigo AND o.rut_proveedor='" & TxtRutProveedor & "' AND o.rut_emp='" & SP_Rut_Activo & "' LIMIT 1) codproveedor "
  '   Sp_Solo_Proveedor = " AND rut='" & txtRutProveedor & "' "
   '  = ",(SELECT )"
    If Len(Sm_Stock_Critico) > 0 Then
        Sm_Stock_Critico = "HAVING LENGTH(codproveedor)>0 AND  stock_bodega < stock_critico "
    Else
        Sp_SoloRut = "HAVING LENGTH(codproveedor)>0"
    End If
    Grilla
    FrmCargando.Visible = False
End Sub

Private Sub VerHistorialProducto(HCodigo As String)
    'Primero, historico Ventas
    '14 Marzo 2016
    Me.TxtPventaBruto = NumFormat(CDbl(LvDetalle.SelectedItem.SubItems(7)))
    Me.txtPVentaNeto = NumFormat(CDbl(TxtPventaBruto) / 1.19)
    
    
    
    Me.TxtPCompraNeto = NumFormat(CDbl(LvDetalle.SelectedItem.SubItems(5)))
    Me.TxtPcompraBruto = NumFormat(CDbl(Me.TxtPCompraNeto) * 1.19)
    
    Me.txtDifBruto = NumFormat(CDbl(TxtPventaBruto) - CDbl(TxtPcompraBruto))
    Me.TxtDifNeto = NumFormat(CDbl(txtPVentaNeto) - CDbl(TxtPCompraNeto))
    SkXcien = ""
    If CDbl(TxtPCompraNeto) > 0 And CDbl(TxtDifNeto) > 0 Then
        SkXcien = Int((CDbl(TxtDifNeto) / CDbl(TxtPCompraNeto)) * 100) & "%"
    End If
    Sql = "SELECT m.fecha,nombre_cliente,doc_nombre,m.no_documento,c.precio_final,c.unidades,c.subtotal " & _
              "FROM ven_doc_venta m " & _
              "INNER JOIN sis_documentos d USING(doc_id),ven_detalle c " & _
              "WHERE ven_informa_venta = 'SI' AND c.rut_emp='" & SP_Rut_Activo & "' AND doc_nota_de_venta='NO' AND m.rut_emp='" & SP_Rut_Activo & "' AND c.doc_id=m.doc_id AND c.no_documento=m.no_documento AND codigo ='" & HCodigo & "' " & _
              "ORDER BY m.fecha"
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, Me.LvVentas, False, True, True, False
    
    Sql = "SELECT concat(year(fecha),'-',month(fecha)),SUM(unidades) cantidad " & _
                "FROM    ven_detalle c " & _
                "WHERE c.rut_emp = '" & SP_Rut_Activo & "' AND codigo = " & HCodigo & " " & _
                "GROUP BY    year (c.fecha),month(fecha) " & _
                "ORDER BY    year (c.fecha),month(fecha)"
    Consulta RsTmp, Sql
        LLenar_Grilla RsTmp, Me, Me.LvVtaMensual, False, True, True, False
    
    
    'Ahora las compras ' P R O V E D O R E S
    Sql = "SELECT m.fecha,nombre_empresa,doc_nombre,no_documento,c.cmd_unitario_neto,c.cmd_cantidad,c.cmd_total_neto " & _
              "FROM com_doc_compra m " & _
              "INNER JOIN com_doc_compra_detalle c ON m.id = c.id " & _
              "INNER JOIN sis_documentos d USING(doc_id) " & _
              "JOIN maestro_proveedores p ON m.rut=p.rut_proveedor " & _
              "WHERE d.doc_orden_de_compra='NO' AND m.rut_emp='" & SP_Rut_Activo & "' AND pro_codigo ='" & HCodigo & "' " & _
              "ORDER BY m.fecha"
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, Me.LvDocumentos, False, True, True, False

End Sub


