VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{DA729E34-689F-49EA-A856-B57046630B73}#1.0#0"; "progressbar-xp.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form inv_KardexGeneral 
   Caption         =   "Kardex General"
   ClientHeight    =   8520
   ClientLeft      =   3630
   ClientTop       =   1425
   ClientWidth     =   14475
   LinkTopic       =   "Form1"
   ScaleHeight     =   8520
   ScaleWidth      =   14475
   Begin VB.Frame FraCarga 
      Caption         =   "Consultando"
      Height          =   795
      Left            =   705
      TabIndex        =   23
      Top             =   3570
      Visible         =   0   'False
      Width           =   11430
      Begin Proyecto2.XP_ProgressBar XpProgB 
         Height          =   225
         Left            =   120
         TabIndex        =   24
         Top             =   330
         Width           =   11190
         _ExtentX        =   19738
         _ExtentY        =   397
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BrushStyle      =   0
         Color           =   11034163
      End
   End
   Begin VB.CommandButton CmdSalir 
      Cancel          =   -1  'True
      Caption         =   "&Retornar"
      Height          =   375
      Left            =   13035
      TabIndex        =   21
      Top             =   7860
      Width           =   1200
   End
   Begin VB.Timer Timer1 
      Interval        =   10
      Left            =   12450
      Top             =   4290
   End
   Begin VB.Frame Frame4 
      Caption         =   "Filtro por descripcion"
      Height          =   1740
      Left            =   150
      TabIndex        =   19
      Top             =   60
      Width           =   4455
      Begin VB.TextBox TxtCodigoBarra 
         Height          =   285
         Left            =   510
         TabIndex        =   27
         Top             =   1425
         Width           =   3480
      End
      Begin VB.TextBox txtBuscaCodigo 
         Height          =   285
         Left            =   540
         TabIndex        =   26
         Top             =   930
         Width           =   3480
      End
      Begin VB.TextBox txtBusqueda 
         Height          =   285
         Left            =   525
         TabIndex        =   1
         Top             =   420
         Width           =   3480
      End
      Begin VB.CommandButton CmdX 
         Caption         =   "X"
         Height          =   585
         Left            =   4065
         TabIndex        =   2
         Top             =   465
         Width           =   270
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   225
         Left            =   510
         OleObjectBlob   =   "inv_KardexGeneral.frx":0000
         TabIndex        =   20
         Top             =   225
         Width           =   1935
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   225
         Index           =   0
         Left            =   525
         OleObjectBlob   =   "inv_KardexGeneral.frx":0084
         TabIndex        =   25
         Top             =   735
         Width           =   1935
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   225
         Index           =   1
         Left            =   495
         OleObjectBlob   =   "inv_KardexGeneral.frx":00FE
         TabIndex        =   28
         Top             =   1230
         Width           =   1935
      End
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   12525
      OleObjectBlob   =   "inv_KardexGeneral.frx":0184
      Top             =   7440
   End
   Begin VB.Frame FrameK 
      Caption         =   "Kardex General"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   6375
      Left            =   180
      TabIndex        =   12
      Top             =   1890
      Width           =   14175
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   240
         Left            =   360
         OleObjectBlob   =   "inv_KardexGeneral.frx":03B8
         TabIndex        =   18
         Top             =   5625
         Width           =   6360
      End
      Begin VB.Frame FraProgreso 
         Height          =   735
         Left            =   1935
         TabIndex        =   14
         Top             =   5670
         Visible         =   0   'False
         Width           =   8880
         Begin VB.PictureBox Picture1 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            ForeColor       =   &H80000008&
            Height          =   375
            Left            =   8085
            ScaleHeight     =   345
            ScaleWidth      =   705
            TabIndex        =   15
            Top             =   255
            Width           =   735
            Begin ACTIVESKINLibCtl.SkinLabel SkProgreso 
               Height          =   240
               Left            =   -15
               OleObjectBlob   =   "inv_KardexGeneral.frx":0470
               TabIndex        =   16
               Top             =   60
               Width           =   735
            End
         End
         Begin MSComctlLib.ProgressBar BarraProgreso 
            Height          =   375
            Left            =   165
            TabIndex        =   17
            Top             =   225
            Width           =   7905
            _ExtentX        =   13944
            _ExtentY        =   661
            _Version        =   393216
            Appearance      =   1
         End
      End
      Begin VB.CommandButton cmdExportar 
         Caption         =   "Exportar a Exel"
         Height          =   375
         Left            =   300
         TabIndex        =   8
         Top             =   5865
         Width           =   1695
      End
      Begin MSComctlLib.ListView LvDetalle 
         Height          =   5325
         Left            =   330
         TabIndex        =   13
         Top             =   300
         Width           =   13635
         _ExtentX        =   24051
         _ExtentY        =   9393
         View            =   3
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   7
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "T1100"
            Text            =   "Codigo"
            Object.Width           =   2293
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "T2500"
            Text            =   "Descripcion Producto"
            Object.Width           =   7937
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   2
            Object.Tag             =   "N102"
            Text            =   "Entrada"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   3
            Object.Tag             =   "N102"
            Text            =   "Salida"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   4
            Object.Tag             =   "N102"
            Text            =   "Saldo Actual"
            Object.Width           =   3528
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   5
            Object.Tag             =   "N100"
            Text            =   "Entrada $"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   6
            Object.Tag             =   "N100"
            Text            =   "Salida $"
            Object.Width           =   0
         EndProperty
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Fecha"
      Height          =   1110
      Left            =   7170
      TabIndex        =   9
      Top             =   60
      Width           =   2550
      Begin ACTIVESKINLibCtl.SkinLabel skIni 
         Height          =   240
         Left            =   165
         OleObjectBlob   =   "inv_KardexGeneral.frx":04D2
         TabIndex        =   10
         Top             =   315
         Width           =   795
      End
      Begin MSComCtl2.DTPicker DTInicio 
         Height          =   300
         Left            =   1005
         TabIndex        =   5
         Top             =   285
         Width           =   1305
         _ExtentX        =   2302
         _ExtentY        =   529
         _Version        =   393216
         Format          =   85590017
         CurrentDate     =   40628
      End
      Begin MSComCtl2.DTPicker DtHasta 
         Height          =   285
         Left            =   1005
         TabIndex        =   6
         Top             =   570
         Width           =   1305
         _ExtentX        =   2302
         _ExtentY        =   503
         _Version        =   393216
         Format          =   85590017
         CurrentDate     =   40628
      End
      Begin ACTIVESKINLibCtl.SkinLabel skFin 
         Height          =   240
         Left            =   165
         OleObjectBlob   =   "inv_KardexGeneral.frx":053A
         TabIndex        =   11
         Top             =   585
         Width           =   795
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Informacion"
      Height          =   1275
      Left            =   4560
      TabIndex        =   0
      Top             =   60
      Width           =   9780
      Begin VB.ComboBox CboBodega 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         ItemData        =   "inv_KardexGeneral.frx":05A2
         Left            =   5235
         List            =   "inv_KardexGeneral.frx":05AF
         Style           =   2  'Dropdown List
         TabIndex        =   22
         ToolTipText     =   "Seleccione Bodega"
         Top             =   495
         Width           =   2790
      End
      Begin VB.OptionButton Option2 
         Caption         =   "Seleccionar fechas"
         Height          =   240
         Left            =   420
         TabIndex        =   4
         Top             =   690
         Value           =   -1  'True
         Width           =   2070
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Todas las fechas"
         Height          =   240
         Left            =   420
         TabIndex        =   3
         Top             =   375
         Width           =   2070
      End
      Begin VB.CommandButton cmdKardex 
         Caption         =   "Consultar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   450
         Left            =   8175
         TabIndex        =   7
         Top             =   375
         Width           =   1545
      End
   End
End
Attribute VB_Name = "inv_KardexGeneral"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim l_Alto_original As Long

Private Sub CmdExportar_Click()
    Me.Height = l_Alto_original + 500
    FrameK.Height = FrameK.Height + 500
    Dim tit(2) As String
    FraProgreso.Visible = True
    tit(0) = "KARDEX PRODUCTO"
    tit(1) = TxtCodigo
    tit(2) = "FECHA INFORME: " & Date & " - " & Time      '                                   DESDE:" & DtInicio.Value & "   HASTA:" & Dtfin.Value
    Exportar LvDetalle, tit, Me, BarraProgreso, SkProgreso
    BarraProgreso.Value = 1
    SkProgreso = "0%"
    FraProgreso.Visible = False
End Sub

Private Sub cmdKardex_Click()
    Dim s_Like As String
    G_Filtro_Fecha = Empty
    s_Like = Empty
    If Option2.Value Then
        G_Filtro_Fecha = " AND kar_fecha BETWEEN '" & Format(DTInicio.Value, "YYYY-MM-DD") & "' AND '" & Format(DtHasta.Value, "YYYY-MM-DD") & "' "
    End If
    If Len(TxtBusqueda) > 0 Then
        s_Like = "  AND (descripcion LIKE '%" & TxtBusqueda & "%' OR codigo='" & txtBuscaCodigo & "')"
    ElseIf Len(txtBuscaCodigo) > 0 Then
        s_Like = "  AND (codigo='" & txtBuscaCodigo & "')"
    End If
    
    If Len(TxtCodigoBarra) > 0 Then
        s_Like = " AND pro_codigo_interno='" & TxtCodigoBarra & "' "
    
    End If
    
   ' Sql = "SELECT pro_codigo, descripcion," & _
          "SUM(IF(kar_movimiento='ENTRADA',kar_cantidad,0)) entrada," & _
          "SUM(IF(kar_movimiento='SALIDA',kar_cantidad,0)) salida," & _
          "(SUM(IF(kar_movimiento='ENTRADA' ,kar_cantidad,0))-SUM(IF(kar_movimiento='SALIDA',kar_cantidad,0))) saldo," & _
          "SUM(IF(kar_movimiento='ENTRADA' OR kar_movimiento='NND',kar_cantidad_valor,0))   entrada_valor," & _
          "SUM(IF(kar_movimiento='SALIDA' OR kar_movimiento='NNC' OR kar_movimiento='NN' ,kar_cantidad_valor,0))  salida_valor " & _
           "FROM inv_kardex k,maestro_productos m " & _
           "WHERE k.rut_emp='" & SP_Rut_Activo & "' AND m.rut_emp='" & SP_Rut_Activo & "' AND m.pro_inventariable='SI' AND k.pro_codigo = m.Codigo " & G_Filtro_Fecha & s_Like & _
           "GROUP BY pro_codigo " & _
           "ORDER BY descripcion "
   Sql = "SELECT  pro_codigo cod, descripcion,0,0," & _
            "(SELECT kar_nuevo_saldo FROM inv_kardex x  WHERE x.rut_emp='" & SP_Rut_Activo & "' AND x.pro_codigo=k.pro_codigo AND bod_id=" & CboBodega.ItemData(CboBodega.ListIndex) & " ORDER BY kar_id DESC LIMIT 1 ) saldo " & _
            ", 0,0 " & _
            "FROM inv_kardex k " & _
            "JOIN maestro_productos m ON k.pro_codigo=codigo " & _
            "WHERE m.rut_emp='" & SP_Rut_Activo & "' AND k.rut_emp='" & SP_Rut_Activo & "' AND k.bod_id=" & CboBodega.ItemData(CboBodega.ListIndex) & " " & s_Like & "  " & _
            "GROUP BY pro_codigo"
   ' Sql = "SELECT pro_codigo, descripcion,0,0,kar_nuevo_saldo " & _
          "/*SUM(IF(kar_movimiento='ENTRADA',kar_cantidad,0)) entrada," & _
          "SUM(IF(kar_movimiento='SALIDA',kar_cantidad,0)) salida*/" & _
          "(SUM(IF(kar_movimiento='ENTRADA' ,kar_cantidad,0))-SUM(IF(kar_movimiento='SALIDA',kar_cantidad,0))) saldo," & _
          ",0,0 /*SUM(IF(kar_movimiento='ENTRADA' OR kar_movimiento='NND',kar_cantidad_valor,0))   entrada_valor," & _
          "SUM(IF(kar_movimiento='SALIDA' OR kar_movimiento='NNC' OR kar_movimiento='NN' ,kar_cantidad_valor,0))  salida_valor */ " & _
           "FROM inv_kardex k,maestro_productos m " & _
           "WHERE k.rut_emp='" & SP_Rut_Activo & "' AND m.rut_emp='" & SP_Rut_Activo & "' AND m.pro_inventariable='SI' AND k.pro_codigo = m.Codigo " & G_Filtro_Fecha & s_Like & _
           "GROUP BY pro_codigo " & _
           "ORDER BY descripcion "
           
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvDetalle, False, True, True, False
    
    If RsTmp.RecordCount > 0 Then
        'unidaes
        Dim elColor As Variant
        elColor = &HC000&
        DoEvents
        FraCarga.Visible = True
        XpProgB.Value = 1
        XpProgB.Max = LvDetalle.ListItems.Count
        For L = 2 To 4
            For i = 1 To LvDetalle.ListItems.Count
                If Val(LvDetalle.ListItems(i).SubItems(4)) < 0 Then elColor = &HFF& Else elColor = &HC000&
                LvDetalle.ListItems(i).ListSubItems(L).ForeColor = elColor
                If L = 4 Then LvDetalle.ListItems(i).ListSubItems(L).Bold = True
                XpProgB.Value = i
            Next i
        Next L
        'valores
        For L = 5 To 6
            For i = 1 To LvDetalle.ListItems.Count
                LvDetalle.ListItems(i).ListSubItems(L).ForeColor = &HC0C000
                If L = 6 Then LvDetalle.ListItems(i).ListSubItems(L).Bold = True
                XpProgB.Value = i
            Next i
        Next L
    End If
    FraCarga.Visible = False
End Sub

Private Sub CmdSalir_Click()
    Unload Me
End Sub

Private Sub CmdX_Click()
    TxtBusqueda = ""
    txtBuscaCodigo = ""
    'cmdKardex_Click
End Sub

Private Sub Form_Load()
    Centrar Me
    Aplicar_skin Me
    'Skin2 Me, , 5
    DTInicio.Value = Date - 30
    DtHasta.Value = Date
    l_Alto_original = Me.Height
    LLenarCombo CboBodega, "bod_nombre", "bod_id", "par_bodegas", "bod_activo='SI' AND rut_emp='" & SP_Rut_Activo & "'", "bod_id"
    CboBodega.ListIndex = 0
    
End Sub


Private Sub LvDetalle_DblClick()
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    SG_codigo2 = LvDetalle.SelectedItem.Text
    Inv_Kardex.Lm_BodID = CboBodega.ItemData(CboBodega.ListIndex)
    Inv_Kardex.Caption = "KARDEX INDIVIDUAL -BODEGA " & CboBodega.Text  ' & LvDetalle.SelectedItem.Text & " " & LvDetalle.SelectedItem.SubItems(1)
    Inv_Kardex.SkCodigo = LvDetalle.SelectedItem.Text
    Inv_Kardex.SkArticulo = LvDetalle.SelectedItem.SubItems(1)
    

    Inv_Kardex.Show 1
    
    
End Sub

Private Sub Option1_Click()
    skIni.Enabled = False
    skFin.Enabled = False
    DTInicio.Enabled = False
    DtHasta.Enabled = False
End Sub

Private Sub Option2_Click()
    skIni.Enabled = True
    skFin.Enabled = True
    DTInicio.Enabled = True
    DtHasta.Enabled = True
End Sub

Private Sub Timer1_Timer()
    cmdKardex.SetFocus
    Timer1.Enabled = False
    
End Sub

Private Sub txtBuscaCodigo_GotFocus()
    En_Foco txtBuscaCodigo
End Sub

Private Sub txtBuscaCodigo_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
    If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub txtBuscaCodigo_Validate(Cancel As Boolean)
txtBuscaCodigo = Replace(txtBuscaCodigo, "'", "")
End Sub

Private Sub TxtBusqueda_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
    If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtBusqueda_Validate(Cancel As Boolean)
TxtBusqueda = Replace(TxtBusqueda, "'", "")
End Sub


