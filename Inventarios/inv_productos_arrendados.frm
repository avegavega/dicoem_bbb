VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{DA729E34-689F-49EA-A856-B57046630B73}#1.0#0"; "progressbar-xp.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form inv_productos_arrendados 
   Caption         =   "Articulos en arriendo"
   ClientHeight    =   7410
   ClientLeft      =   570
   ClientTop       =   2130
   ClientWidth     =   14835
   LinkTopic       =   "Form1"
   ScaleHeight     =   7410
   ScaleWidth      =   14835
   Begin VB.Frame FraProgreso 
      Caption         =   "Progreso exportaci�n"
      Height          =   795
      Left            =   2280
      TabIndex        =   4
      Top             =   4575
      Visible         =   0   'False
      Width           =   11430
      Begin Proyecto2.XP_ProgressBar BarraProgreso 
         Height          =   330
         Left            =   150
         TabIndex        =   5
         Top             =   315
         Width           =   11130
         _ExtentX        =   19632
         _ExtentY        =   582
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BrushStyle      =   0
         Color           =   16777088
         Scrolling       =   1
         ShowText        =   -1  'True
      End
   End
   Begin VB.CommandButton Command1 
      Cancel          =   -1  'True
      Caption         =   "Retornar"
      Height          =   375
      Left            =   12915
      TabIndex        =   2
      Top             =   6900
      Width           =   1680
   End
   Begin VB.Timer Timer1 
      Interval        =   40
      Left            =   1005
      Top             =   165
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   210
      OleObjectBlob   =   "inv_productos_arrendados.frx":0000
      Top             =   165
   End
   Begin VB.Frame Frame1 
      Caption         =   "Productos"
      Height          =   6555
      Left            =   270
      TabIndex        =   0
      Top             =   285
      Width           =   14370
      Begin VB.CheckBox Check 
         Caption         =   "Check1"
         Height          =   195
         Left            =   150
         TabIndex        =   12
         Top             =   1245
         Width           =   195
      End
      Begin VB.CommandButton cmdDevolver 
         Caption         =   "Devolver productos marcados"
         Height          =   315
         Left            =   255
         TabIndex        =   11
         ToolTipText     =   "Opci�n que retorna los art�culos a Bodega."
         Top             =   6120
         Width           =   2445
      End
      Begin VB.Frame Frame2 
         Caption         =   "Filtrar cliente"
         Height          =   915
         Left            =   7440
         TabIndex        =   6
         Top             =   225
         Width           =   6735
         Begin VB.CommandButton CmdBuscaCliente 
            Caption         =   "F1 - Buscar"
            Height          =   255
            Left            =   615
            TabIndex        =   9
            Top             =   585
            Width           =   1125
         End
         Begin VB.TextBox TxtRazonSocial 
            BackColor       =   &H00E0E0E0&
            Height          =   285
            Left            =   2190
            Locked          =   -1  'True
            TabIndex        =   8
            TabStop         =   0   'False
            Top             =   300
            Width           =   4275
         End
         Begin VB.TextBox TxtRut 
            BackColor       =   &H0080FF80&
            Height          =   285
            Left            =   615
            TabIndex        =   7
            ToolTipText     =   "Ingrese el RUT sin puntos ni guion."
            Top             =   300
            Width           =   1575
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
            Height          =   255
            Index           =   0
            Left            =   105
            OleObjectBlob   =   "inv_productos_arrendados.frx":0234
            TabIndex        =   10
            Top             =   315
            Width           =   495
         End
      End
      Begin VB.CommandButton Command2 
         Caption         =   "Exportar a Excel"
         Height          =   315
         Left            =   2805
         TabIndex        =   3
         Top             =   6120
         Width           =   2805
      End
      Begin MSComctlLib.ListView LVDetalle 
         Height          =   4875
         Left            =   120
         TabIndex        =   1
         Top             =   1185
         Width           =   14070
         _ExtentX        =   24818
         _ExtentY        =   8599
         View            =   3
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   11
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "N109"
            Object.Width           =   529
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "N109"
            Text            =   "Codidgo"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Descripcion"
            Object.Width           =   6174
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Object.Tag             =   "N102"
            Text            =   "Cantidad"
            Object.Width           =   1720
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Object.Tag             =   "F1000"
            Text            =   "Fecha"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Object.Tag             =   "T1500"
            Text            =   "Rut Cliente"
            Object.Width           =   2293
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   6
            Object.Tag             =   "T3000"
            Text            =   "Nombre Cliente"
            Object.Width           =   6174
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   7
            Object.Tag             =   "N109"
            Text            =   "EMP_ID"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   8
            Object.Tag             =   "T2000"
            Text            =   "Documento"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   9
            Object.Tag             =   "N109"
            Text            =   "Numero"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   10
            Object.Tag             =   "N109"
            Text            =   "ven id"
            Object.Width           =   0
         EndProperty
      End
   End
End
Attribute VB_Name = "inv_productos_arrendados"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim Sm_filtro As String

Private Sub Check_Click()
    For i = 1 To LvDetalle.ListItems.Count
        If LvDetalle.ListItems(i).Checked Then LvDetalle.ListItems(i).Checked = False Else LvDetalle.ListItems(i).Checked = True
        
    Next
End Sub

Private Sub CmdBuscaCliente_Click()
    ClienteEncontrado = False
    BuscaCliente.Show 1
    TxtRut = SG_codigo
    TxtRut.SetFocus
    BuscaClientes
End Sub
Private Sub BuscaClientes()
    If Len(TxtRut) = 0 Then Exit Sub
    Sql = "SELECT rut_cliente,nombre_rsocial " & _
            "FROM maestro_clientes " & _
            "WHERE rut_cliente='" & TxtRut & "'"
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        TxtRazonSocial = RsTmp!nombre_rsocial
        Sm_filtro = " AND rut_cliente='" & TxtRut & "' "
        CargaCliente
    Else
        
        MsgBox "Cliente no enconctrado..", vbInformation
        TxtRut.SetFocus
        Exit Sub
    End If
    
            
End Sub

Private Sub cmdDevolver_Click()
    Dim bp_Devuelve As Boolean
    bp_Devuelve = False
    For i = 1 To LvDetalle.ListItems.Count
        If LvDetalle.ListItems(i).Checked Then
            bp_Devuelve = True
            Exit For
        End If
    Next
    If Not bp_Devuelve Then Exit Sub
    If MsgBox("Esta seguro(a) de devolver estos articulos al stock?..", vbQuestion + vbOKCancel) = vbCancel Then Exit Sub
    
    'aqui se confirma la devolucion de los marcados
    On Error GoTo errorDevuelta
    cn.BeginTrans
        For i = 1 To LvDetalle.ListItems.Count
            If LvDetalle.ListItems(i).Checked Then cn.Execute "UPDATE inv_productos_arrendados " & _
                            "SET arr_devuelto='SI' " & _
                            "WHERE arr_id=" & LvDetalle.ListItems(i)
        Next
    cn.CommitTrans
    CargaCliente
    Exit Sub
errorDevuelta:
    cn.RollbackTrans
    MsgBox "Error al intentar devolver articulos .." & vbNewLine & Err.Number & vbNewLine & Err.Description
    
End Sub

Private Sub Command1_Click()
    Unload Me
End Sub

Private Sub Command2_Click()
    Dim tit(2) As String
    If LvDetalle.ListItems.Count = 0 Then Exit Sub
    FraProgreso.Visible = True
    tit(0) = Me.Caption & " " & DtFecha & " - " & Time
    tit(1) = ""
    tit(2) = ""
    ExportarNuevo LvDetalle, tit, Me, BarraProgreso
    FraProgreso.Visible = False
End Sub

Private Sub Form_Load()
    Centrar Me
    Aplicar_skin Me
    Sm_filtro = ""
    CargaCliente
End Sub
Private Sub CargaCliente()
    Sql = "SELECT arr_id,pro_codigo,descripcion,arr_cantidad,fecha,rut_cliente,nombre_cliente,emp_id,doc_nombre,no_documento " & _
            "FROM vi_productos_en_arriendo " & _
            "WHERE emp_id=" & IG_id_Empresa & Sm_filtro & " " & _
            "ORDER BY doc_nombre,no_documento"
            
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvDetalle, True, True, True, False

End Sub

Private Sub Timer1_Timer()
    On Error Resume Next
    LvDetalle.SetFocus
    Timer1.Enabled = False
End Sub



Private Sub TxtRut_Validate(Cancel As Boolean)
    BuscaClientes
End Sub
