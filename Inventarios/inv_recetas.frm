VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form inv_recetas 
   Caption         =   "Formula"
   ClientHeight    =   8220
   ClientLeft      =   8355
   ClientTop       =   1080
   ClientWidth     =   8940
   LinkTopic       =   "Form1"
   ScaleHeight     =   8220
   ScaleWidth      =   8940
   Begin VB.Frame Frame2 
      Caption         =   "Ficha Formula"
      Height          =   6705
      Left            =   540
      TabIndex        =   1
      Top             =   750
      Width           =   8055
      Begin VB.CommandButton CmdGuardar 
         Caption         =   "Guarda Formula"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   465
         Left            =   315
         TabIndex        =   16
         Top             =   5940
         Width           =   1740
      End
      Begin VB.TextBox TxtDescrp 
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1185
         Locked          =   -1  'True
         TabIndex        =   13
         Top             =   675
         Width           =   5520
      End
      Begin VB.TextBox TxtMarca 
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   3345
         Locked          =   -1  'True
         TabIndex        =   12
         Top             =   330
         Width           =   3375
      End
      Begin VB.CommandButton CmdBuscaProducto 
         Caption         =   "Codigo"
         Height          =   270
         Left            =   255
         TabIndex        =   11
         Top             =   390
         Width           =   900
      End
      Begin VB.TextBox TxtCodigo 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1185
         TabIndex        =   10
         ToolTipText     =   "Para ingresar un Gasto digite 0 (cero)"
         Top             =   345
         Width           =   1095
      End
      Begin VB.Frame Frame1 
         Caption         =   "Materias Primas"
         Height          =   4440
         Left            =   300
         TabIndex        =   2
         Top             =   1440
         Width           =   7245
         Begin VB.CommandButton CmdOk 
            Caption         =   "Ok"
            Height          =   315
            Left            =   6540
            TabIndex        =   17
            Top             =   660
            Width           =   525
         End
         Begin VB.TextBox TxtCodigoMP 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   135
            TabIndex        =   6
            ToolTipText     =   "Para ingresar un Gasto digite 0 (cero)"
            Top             =   675
            Width           =   1095
         End
         Begin VB.CommandButton CmdCod 
            Caption         =   "Codigo"
            Height          =   270
            Left            =   120
            TabIndex        =   5
            Top             =   405
            Width           =   1110
         End
         Begin VB.TextBox TxtMP 
            BackColor       =   &H8000000F&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   1245
            Locked          =   -1  'True
            TabIndex        =   4
            Top             =   675
            Width           =   4230
         End
         Begin VB.TextBox txtCantidad 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   5445
            TabIndex        =   3
            ToolTipText     =   "Para ingresar un Gasto digite 0 (cero)"
            Top             =   675
            Width           =   1095
         End
         Begin MSComctlLib.ListView LVDetalle 
            Height          =   3210
            Left            =   120
            TabIndex        =   7
            Top             =   990
            Width           =   6885
            _ExtentX        =   12144
            _ExtentY        =   5662
            View            =   3
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            FullRowSelect   =   -1  'True
            GridLines       =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   0
            NumItems        =   3
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Object.Tag             =   "N109"
               Text            =   "Codigo MP"
               Object.Width           =   1931
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Object.Tag             =   "T1000"
               Text            =   "Nombre Materia Prima"
               Object.Width           =   7461
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   2
               Object.Tag             =   "N102"
               Text            =   "Cantidad"
               Object.Width           =   1940
            EndProperty
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
            Height          =   210
            Left            =   1260
            OleObjectBlob   =   "inv_recetas.frx":0000
            TabIndex        =   8
            Top             =   420
            Width           =   840
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
            Height          =   225
            Left            =   5490
            OleObjectBlob   =   "inv_recetas.frx":0086
            TabIndex        =   9
            Top             =   435
            Width           =   840
         End
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
         Height          =   210
         Left            =   2415
         OleObjectBlob   =   "inv_recetas.frx":00F4
         TabIndex        =   14
         Top             =   405
         Width           =   840
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel5 
         Height          =   210
         Left            =   300
         OleObjectBlob   =   "inv_recetas.frx":015C
         TabIndex        =   15
         Top             =   690
         Width           =   840
      End
   End
   Begin VB.CommandButton CmdRetornar 
      Caption         =   "Retornar"
      Height          =   450
      Left            =   7110
      TabIndex        =   0
      Top             =   7620
      Width           =   1485
   End
   Begin VB.Timer Timer1 
      Interval        =   10
      Left            =   510
      Top             =   30
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   0
      OleObjectBlob   =   "inv_recetas.frx":01CA
      Top             =   0
   End
End
Attribute VB_Name = "inv_recetas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub CmdBuscaProducto_Click()
    BuscaProducto.Show 1
    If Len(SG_codigo) = 0 Then Exit Sub
    TxtCodigo = SG_codigo
    TxtCodigo_Validate True
End Sub

Private Sub CmdCod_Click()
    BuscaProducto.Show 1
    If Len(SG_codigo) = 0 Then Exit Sub
    TxtCodigoMP = SG_codigo
    TxtCodigoMP_Validate True
End Sub

Private Sub CmdGuardar_Click()
    Dim Lp_Receta_ID As Long
    If Len(TxtCodigo) = 0 Then
        MsgBox "Debe ingresar codigo PRODUCTO...", vbInformation
        TxtCodigo.SetFocus
        Exit Sub
    End If

    If LvDetalle.ListItems.Count = 0 Then
        MsgBox "Debe ingresar Materia prima para la formula...", vbInformation
        TxtCodigoMP.SetFocus
        Exit Sub
    End If
        
    On Error GoTo ProblemaGrabar
    cn.BeginTrans
        If DG_ID_Unico = 0 Then
            Lp_Receta_ID = UltimoNro("inv_receta", "rec_id")
            Sql = "INSERT INTO inv_receta (rec_id,pro_codigo,rut_emp,rec_fecha_creacion) " & _
                    "VALUES(" & Lp_Receta_ID & "," & TxtCodigo & ",'" & SP_Rut_Activo & "','" & Fql(Date) & "')"
            cn.Execute Sql
            
            Sql = "INSERT INTO inv_receta_detalle (rec_id,pro_codigo,red_cantidad) VALUES"
            For i = 1 To LvDetalle.ListItems.Count
                Sql = Sql & "(" & Lp_Receta_ID & "," & LvDetalle.ListItems(i) & "," & CxP(LvDetalle.ListItems(i).SubItems(2)) & "),"
            Next
            Sql = Mid(Sql, 1, Len(Sql) - 1)
            cn.Execute Sql
            
        Else
            Lp_Receta_ID = DG_ID_Unico
            Sql = "UPDATE inv_receta SET rec_fecha_modificacion='" & Fql(Date) & "'"
            cn.Execute "DELETE FROM inv_receta_detalle " & _
                        "WHERE rec_id=" & DG_ID_Unico
            
            Sql = "INSERT INTO inv_receta_detalle (rec_id,pro_codigo,red_cantidad) VALUES"
            For i = 1 To LvDetalle.ListItems.Count
                Sql = Sql & "(" & Lp_Receta_ID & "," & LvDetalle.ListItems(i) & "," & CxP(LvDetalle.ListItems(i).SubItems(2)) & "),"
            Next
            Sql = Mid(Sql, 1, Len(Sql) - 1)
            cn.Execute Sql
            
            
        
        End If
    cn.CommitTrans
    Unload Me
    Exit Sub
ProblemaGrabar:
    MsgBox "Problema al intentar grabar..." & vbNewLine & Err.Description, vbExclamation
    cn.RollbackTrans
End Sub

Private Sub CmdOk_Click()
    
    If Len(TxtCodigo) = 0 Then
        MsgBox "Debe ingresar codigo PRODUCTO...", vbInformation
        TxtCodigo.SetFocus
        Exit Sub
    End If
    
    If Len(TxtCodigoMP) = 0 Then
        MsgBox "Debe ingresar codigo Materia Prima...", vbInformation
        TxtCodigoMP.SetFocus
        Exit Sub
    End If
    
    If CDbl(TxtCantidad) = 0 Then
        MsgBox "Debe ingresar Cantidad...", vbInformation
        TxtCantidad.SetFocus
        Exit Sub
    End If
    
    '16 Mayo, Validaciones listas
    LvDetalle.ListItems.Add , , TxtCodigoMP
    LvDetalle.ListItems(LvDetalle.ListItems.Count).SubItems(1) = TxtMP
    LvDetalle.ListItems(LvDetalle.ListItems.Count).SubItems(2) = CxP(TxtCantidad)
    
    TxtCodigoMP = ""
    TxtMP = ""
    TxtCantidad = 0
    
    TxtCodigoMP.SetFocus
End Sub

Private Sub CmdRetornar_Click()
    Unload Me
End Sub



Private Sub Form_Load()
    Aplicar_skin Me
    Centrar Me
    If DG_ID_Unico > 0 Then
        Sql = "SELECT pro_codigo,mar_nombre,descripcion " & _
                "FROM inv_receta r " & _
                "JOIN maestro_productos p ON r.pro_codigo=p.codigo " & _
                "JOIN par_marcas m ON p.mar_id=m.mar_id " & _
                "WHERE rec_id=" & DG_ID_Unico
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
            TxtCodigo = RsTmp!pro_codigo
            Me.TxtDescrp = RsTmp!Descripcion
            TxtMarca = RsTmp!mar_nombre
            TxtCodigo.Locked = True
            CmdBuscaProducto.Enabled = False
            Sql = "SELECT pro_codigo,descripcion,red_cantidad " & _
                    "FROM inv_receta_detalle d " & _
                    "JOIN maestro_productos m ON d.pro_codigo=m.codigo " & _
                    "WHERE rec_id=" & DG_ID_Unico
            Consulta RsTmp, Sql
            LLenar_Grilla RsTmp, Me, LvDetalle, False, True, True, False
        End If
        
        
        
    End If
End Sub

Private Sub LvDetalle_DblClick()
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    TxtCodigoMP = LvDetalle.SelectedItem
    TxtMP = LvDetalle.SelectedItem.SubItems(1)
    TxtCantidad = LvDetalle.SelectedItem.SubItems(2)
    
    LvDetalle.ListItems.Remove LvDetalle.SelectedItem.Index
End Sub

Private Sub Timer1_Timer()
    On Error Resume Next
    TxtCodigo.SetFocus
    Timer1.Enabled = False
End Sub

Private Sub txtCantidad_GotFocus()
    En_Foco TxtCantidad
End Sub

Private Sub txtCantidad_KeyPress(KeyAscii As Integer)
    KeyAscii = AceptaSoloNumeros(KeyAscii)
End Sub

Private Sub txtCantidad_Validate(Cancel As Boolean)
    If Val(TxtCantidad) = 0 Then TxtCantidad = "0"
    
End Sub

Private Sub TxtCodigo_GotFocus()
    En_Foco TxtCodigo
End Sub

Private Sub TxtCodigo_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 And Len(TxtCodigo.Text) > 0 Then SendKeys "{Tab}"
 '   KeyAscii = Asc(UCase(Chr(KeyAscii)))
    KeyAscii = AceptaSoloNumeros(KeyAscii)
    If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtCodigo_Validate(Cancel As Boolean)
    If Len(TxtCodigo) = 0 Then Exit Sub
    Sql = "SELECT descripcion,marca,precio_compra,stock_actual,tip_nombre familia " & _
          "FROM maestro_productos m,par_tipos_productos t " & _
          "WHERE  m.rut_emp='" & SP_Rut_Activo & "' AND  m.tip_id=t.tip_id AND codigo='" & TxtCodigo & "'"
    Call Consulta(RsTmp, Sql)
    If RsTmp.RecordCount > 0 Then
                'Codigo ingresado ha sido encontrado
            With RsTmp
                Me.TxtDescrp = !Descripcion
                Me.TxtMarca.Text = "" & !MARCA
            End With
    Else
        TxtDescrp = Empty
        TxtMarca = Empty
    End If
End Sub





Private Sub TxtCodigoMP_GotFocus()
    En_Foco TxtCodigoMP
End Sub

Private Sub TxtCodigoMP_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 And Len(TxtCodigoMP.Text) > 0 Then SendKeys "{Tab}"
    'KeyAscii = Asc(UCase(Chr(KeyAscii)))
    KeyAscii = AceptaSoloNumeros(KeyAscii)
    If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtCodigoMP_Validate(Cancel As Boolean)
    If Len(TxtCodigoMP) = 0 Then Exit Sub
    Sql = "SELECT descripcion,marca,precio_compra,stock_actual,tip_nombre familia " & _
          "FROM maestro_productos m,par_tipos_productos t " & _
          "WHERE  m.rut_emp='" & SP_Rut_Activo & "' AND  m.tip_id=t.tip_id AND codigo='" & TxtCodigoMP & "'"
    Call Consulta(RsTmp, Sql)
    If RsTmp.RecordCount > 0 Then
                'Codigo ingresado ha sido encontrado
            With RsTmp
                TxtMP = !Descripcion
            End With
    Else
        TxtMP = Empty
        
    End If
End Sub

