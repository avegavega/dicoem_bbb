VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{DA729E34-689F-49EA-A856-B57046630B73}#1.0#0"; "progressbar-xp.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form inv_OT 
   Caption         =   "Orden de Trabajo para Produccion"
   ClientHeight    =   9855
   ClientLeft      =   5055
   ClientTop       =   600
   ClientWidth     =   9915
   LinkTopic       =   "Form1"
   ScaleHeight     =   9855
   ScaleWidth      =   9915
   Begin VB.Frame FraProgreso 
      Caption         =   "Progreso exportación"
      Height          =   810
      Left            =   1425
      TabIndex        =   24
      Top             =   5265
      Visible         =   0   'False
      Width           =   8100
      Begin Proyecto2.XP_ProgressBar BarraProgreso 
         Height          =   330
         Left            =   150
         TabIndex        =   25
         Top             =   315
         Width           =   11130
         _ExtentX        =   19632
         _ExtentY        =   582
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BrushStyle      =   0
         Color           =   16777088
         Scrolling       =   1
         ShowText        =   -1  'True
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Datos Orden de Trabajo"
      Height          =   1695
      Left            =   195
      TabIndex        =   15
      Top             =   135
      Width           =   9480
      Begin VB.ComboBox CboBodega 
         Appearance      =   0  'Flat
         Height          =   315
         ItemData        =   "inv_OT.frx":0000
         Left            =   1725
         List            =   "inv_OT.frx":0002
         Style           =   2  'Dropdown List
         TabIndex        =   17
         Top             =   555
         Width           =   5175
      End
      Begin VB.TextBox TxtObs 
         Height          =   435
         Left            =   450
         TabIndex        =   16
         Top             =   1155
         Width           =   6405
      End
      Begin MSComCtl2.DTPicker DtFecha 
         Height          =   330
         Left            =   435
         TabIndex        =   18
         Top             =   555
         Width           =   1305
         _ExtentX        =   2302
         _ExtentY        =   582
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   92078081
         CurrentDate     =   41104
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel6 
         Height          =   210
         Left            =   435
         OleObjectBlob   =   "inv_OT.frx":0004
         TabIndex        =   19
         Top             =   375
         Width           =   1305
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   255
         Index           =   1
         Left            =   1755
         OleObjectBlob   =   "inv_OT.frx":006C
         TabIndex        =   20
         Top             =   360
         Width           =   2580
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   255
         Index           =   2
         Left            =   465
         OleObjectBlob   =   "inv_OT.frx":00D6
         TabIndex        =   21
         Top             =   960
         Width           =   3765
      End
   End
   Begin VB.CommandButton cmdSalir 
      Cancel          =   -1  'True
      Caption         =   "Esc - &Retornar"
      Height          =   465
      Left            =   8475
      TabIndex        =   13
      Top             =   9300
      Width           =   1185
   End
   Begin VB.CommandButton CmdGuardar 
      Caption         =   "&Generar OT"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   210
      TabIndex        =   12
      Top             =   9255
      Width           =   1485
   End
   Begin VB.Frame Frame1 
      Caption         =   "Materiales a utilizar"
      Height          =   3465
      Left            =   180
      TabIndex        =   10
      Top             =   5715
      Width           =   9465
      Begin VB.CommandButton CmdExcelMateriales 
         Caption         =   "Exportar"
         Height          =   210
         Left            =   585
         TabIndex        =   22
         Top             =   3075
         Width           =   945
      End
      Begin MSComctlLib.ListView LvMP 
         Height          =   2610
         Left            =   570
         TabIndex        =   11
         Top             =   450
         Width           =   8685
         _ExtentX        =   15319
         _ExtentY        =   4604
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   4210688
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   4
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "T1000"
            Text            =   "Codigo"
            Object.Width           =   2337
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "T3000"
            Text            =   "Nombre"
            Object.Width           =   10583
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   2
            Object.Tag             =   "N102"
            Text            =   "Cantidad"
            Object.Width           =   1773
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Object.Tag             =   "T1000"
            Text            =   "Mantiene Stock"
            Object.Width           =   0
         EndProperty
      End
   End
   Begin MSComctlLib.ListView LvDetalle 
      Height          =   2445
      Left            =   645
      TabIndex        =   9
      Top             =   2805
      Width           =   8775
      _ExtentX        =   15478
      _ExtentY        =   4313
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   4
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Object.Tag             =   "T1000"
         Text            =   "Codigo"
         Object.Width           =   2337
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Object.Tag             =   "T3000"
         Text            =   "Nombre"
         Object.Width           =   10583
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   2
         Object.Tag             =   "N102"
         Text            =   "Cantidad"
         Object.Width           =   1773
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Object.Tag             =   "T1000"
         Text            =   "Mantiene Stock"
         Object.Width           =   0
      EndProperty
   End
   Begin VB.Frame FrmMantenedor 
      Caption         =   "Artículos"
      Height          =   3660
      Left            =   195
      TabIndex        =   0
      Top             =   1920
      Width           =   9480
      Begin VB.CommandButton CmdProductos 
         Caption         =   "Exportar"
         Height          =   210
         Left            =   450
         TabIndex        =   23
         Top             =   3345
         Width           =   945
      End
      Begin VB.CommandButton CmdOk 
         Caption         =   "Ok"
         Height          =   300
         Left            =   8880
         TabIndex        =   6
         Top             =   585
         Width           =   330
      End
      Begin VB.TextBox TxtNombre 
         BackColor       =   &H00E0E0E0&
         Height          =   315
         Left            =   1875
         Locked          =   -1  'True
         TabIndex        =   5
         TabStop         =   0   'False
         Tag             =   "T"
         Top             =   585
         Width           =   6000
      End
      Begin VB.TextBox TxtCodigoMP 
         Height          =   300
         Left            =   450
         TabIndex        =   4
         Top             =   585
         Width           =   1095
      End
      Begin VB.CommandButton CmdBuscaProducto 
         Caption         =   "F1"
         Height          =   255
         Left            =   1545
         TabIndex        =   3
         ToolTipText     =   "Busqueda de productos"
         Top             =   600
         Width           =   300
      End
      Begin VB.TextBox TxtStock 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   4890
         Locked          =   -1  'True
         TabIndex        =   2
         ToolTipText     =   "Stock Actual"
         Top             =   285
         Visible         =   0   'False
         Width           =   2205
      End
      Begin VB.TextBox TxtCantidad 
         Alignment       =   1  'Right Justify
         Height          =   315
         Left            =   7875
         TabIndex        =   1
         Top             =   585
         Width           =   1005
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   255
         Index           =   3
         Left            =   1875
         OleObjectBlob   =   "inv_OT.frx":0150
         TabIndex        =   7
         Top             =   375
         Width           =   1095
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   255
         Index           =   1
         Left            =   7845
         OleObjectBlob   =   "inv_OT.frx":01BA
         TabIndex        =   8
         Top             =   360
         Width           =   1095
      End
   End
   Begin VB.Timer Timer1 
      Interval        =   10
      Left            =   480
      Top             =   30
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   0
      OleObjectBlob   =   "inv_OT.frx":0228
      Top             =   0
   End
   Begin MSComctlLib.ListView LvInsumos 
      Height          =   2610
      Left            =   9930
      TabIndex        =   14
      Top             =   3375
      Visible         =   0   'False
      Width           =   7545
      _ExtentX        =   13309
      _ExtentY        =   4604
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   4210688
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   4
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Object.Tag             =   "T1000"
         Text            =   "Codigo"
         Object.Width           =   2337
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Object.Tag             =   "T3000"
         Text            =   "Nombre"
         Object.Width           =   10583
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   2
         Object.Tag             =   "N102"
         Text            =   "Cantidad"
         Object.Width           =   1773
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Object.Tag             =   "T1000"
         Text            =   "Mantiene Stock"
         Object.Width           =   0
      EndProperty
   End
End
Attribute VB_Name = "inv_OT"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False


Private Sub CmdBuscaProducto_Click()
    BuscaProducto.Show 1
    If Len(SG_codigo) = 0 Then Exit Sub
    TxtCodigoMP = SG_codigo
    TxtCodigoMP_Validate True
End Sub



Private Sub CmdExcelMateriales_Click()
    Dim tit(2) As String
    If LvDetalle.ListItems.Count = 0 Then Exit Sub
    FraProgreso.Visible = True
    tit(0) = "INSUMOS " & DtFecha & " - " & Time
    tit(1) = TxtObs
    tit(2) = ""
    ExportarNuevo LvMP, tit, Me, BarraProgreso
    FraProgreso.Visible = False
End Sub

Private Sub CmdGuardar_Click()
    Dim Lp_Otr_ID As Long
    If CboBodega.ListIndex = -1 Then Exit Sub
    If Len(TxtObs) = 0 Then
        MsgBox "Ingrese descripcion OT...", vbInformation
        TxtObs.SetFocus
        Exit Sub
    End If
    If LvDetalle.ListItems.Count = 0 Then
        MsgBox "Debe ingresar articulos para OT...", vbInformation
        TxtCodigoMP.SetFocus
        Exit Sub
    
    End If
    
    On Error GoTo errorGraba
    cn.BeginTrans
        Lp_Otr_ID = UltimoNro("inv_ot", "otr_id")
        Sql = "INSERT INTO inv_ot (otr_id,otr_fecha,otr_usuario,bod_id,rut_emp,otr_obs) " & _
                "VALUES(" & Lp_Otr_ID & ",'" & Fql(DtFecha) & "','" & LogUsuario & "'," & CboBodega.ItemData(CboBodega.ListIndex) & ",'" & SP_Rut_Activo & "','" & TxtObs & "')"
        cn.Execute Sql
        
        
        
        Sql = "INSERT INTO inv_ot_detalle(otr_id,pro_codigo,otd_cantidad) VALUES"
        For i = 1 To LvDetalle.ListItems.Count
            Sql = Sql & "(" & Lp_Otr_ID & "," & LvDetalle.ListItems(i) & "," & LvDetalle.ListItems(i).SubItems(2) & "),"
            
            
        Next
        cn.Execute Mid(Sql, 1, Len(Sql) - 1)
        
        
        
        Sql = "INSERT INTO inv_ot_detalle_insumos(otr_id,pro_codigo,otd_cantidad) VALUES"
        For i = 1 To LvMP.ListItems.Count
            Sql = Sql & "(" & Lp_Otr_ID & "," & LvMP.ListItems(i) & "," & LvMP.ListItems(i).SubItems(2) & "),"
            
            
        Next
        cn.Execute Mid(Sql, 1, Len(Sql) - 1)
    
    cn.CommitTrans
    MsgBox "OT Generada ...", vbInformation
    Unload Me
    Exit Sub
    
errorGraba:
    cn.RollbackTrans
    MsgBox "No se pudo completar la OT..." & vbNewLine & Err.Description, vbInformation
    
End Sub



Private Sub CmdOk_Click()
    

    
    If Len(TxtCodigoMP) = 0 Then
        MsgBox "Debe ingresar codigo Producto...", vbInformation
        TxtCodigoMP.SetFocus
        Exit Sub
    End If
    
    If CDbl(TxtCantidad) = 0 Then
        MsgBox "Debe ingresar Cantidad...", vbInformation
        TxtCantidad.SetFocus
        Exit Sub
    End If
    
    '16 Mayo, Validaciones listas
    LvDetalle.ListItems.Add , , TxtCodigoMP
    LvDetalle.ListItems(LvDetalle.ListItems.Count).SubItems(1) = TxtNombre
    LvDetalle.ListItems(LvDetalle.ListItems.Count).SubItems(2) = CxP(TxtCantidad)
    
    TxtCodigoMP = ""
    TxtNombre = ""
    TxtCantidad = 0
    
    TxtCodigoMP.SetFocus
    Materiales
End Sub

Private Sub CmdProductos_Click()
    Dim tit(2) As String
    If LvDetalle.ListItems.Count = 0 Then Exit Sub
    FraProgreso.Visible = True
    tit(0) = "Productos " & DtFecha & " - " & Time
    tit(1) = TxtObs
    tit(2) = ""
    ExportarNuevo LvDetalle, tit, Me, BarraProgreso
    FraProgreso.Visible = False
End Sub

Private Sub CmdSalir_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    Skin2 Me, , 4
    DtFecha = Date
    LLenarCombo CboBodega, "bod_nombre", "bod_id", "par_bodegas", "bod_activo='SI' AND rut_emp='" & SP_Rut_Activo & "'", "bod_id"
    CboBodega.ListIndex = CboBodega.ListCount - 1
    
    
    
    If DG_ID_Unico > 0 Then
        Sql = "SELECT otr_fecha,bod_id,otr_obs " & _
                "FROM inv_ot " & _
                "WHERE otr_id=" & DG_ID_Unico
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
            DtFecha = RsTmp!otr_fecha
            Busca_Id_Combo CboBodega, Val(RsTmp!bod_id)
            TxtObs = RsTmp!otr_obs
        End If
        
        Sql = "SELECT pro_codigo,descripcion,otd_cantidad " & _
                "FROM inv_ot_detalle d " & _
                "JOIN maestro_productos p ON d.pro_codigo=p.codigo " & _
                "WHERE otr_id=" & DG_ID_Unico
        Consulta RsTmp, Sql
        LLenar_Grilla RsTmp, Me, LvDetalle, False, True, True, False
    
        Sql = "SELECT pro_codigo,descripcion,otd_cantidad " & _
                "FROM inv_ot_detalle_insumos d " & _
                "JOIN maestro_productos p ON d.pro_codigo=p.codigo " & _
                "WHERE otr_id=" & DG_ID_Unico
        Consulta RsTmp, Sql
        LLenar_Grilla RsTmp, Me, LvMP, False, True, True, False
    
    
    
    End If
End Sub


Private Sub Timer1_Timer()
    On Error Resume Next
    DtFecha.SetFocus
    Timer1.Enabled = False
End Sub

Private Sub txtCantidad_GotFocus()
    En_Foco TxtCantidad
End Sub

Private Sub txtCantidad_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
End Sub

Private Sub TxtCodigoMP_GotFocus()
    En_Foco TxtCodigoMP
End Sub

Private Sub TxtCodigoMP_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 And Len(TxtCodigoMP.Text) > 0 Then SendKeys "{Tab}"
    'KeyAscii = Asc(UCase(Chr(KeyAscii)))
    KeyAscii = AceptaSoloNumeros(KeyAscii)
    If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtCodigoMP_Validate(Cancel As Boolean)
    If Len(TxtCodigoMP) = 0 Then Exit Sub
    Sql = "SELECT descripcion,marca,precio_compra,stock_actual,tip_nombre familia " & _
          "FROM maestro_productos m,par_tipos_productos t " & _
          "WHERE  m.rut_emp='" & SP_Rut_Activo & "' AND  m.tip_id=t.tip_id AND codigo='" & TxtCodigoMP & "'"
    Call Consulta(RsTmp, Sql)
    If RsTmp.RecordCount > 0 Then
                'Codigo ingresado ha sido encontrado
            With RsTmp
                TxtNombre = !Descripcion
            End With
    Else
        TxtNombre = Empty
        
    End If
End Sub

Private Sub Materiales()
    Dim Sp_Codigos As String
    Dim Bp_Encontro As Boolean
    Bp_Encontro = True
    
    LvInsumos.ListItems.Clear
    
    For i = 1 To LvDetalle.ListItems.Count
        Sql = "SELECT d.pro_codigo,descripcion, SUM(red_cantidad) * " & CDbl(LvDetalle.ListItems(i).SubItems(2)) & " usar " & _
            "FROM inv_receta_detalle d " & _
            "JOIN maestro_productos p ON d.pro_codigo='" & p.Codigo & "'" & _
            "JOIN inv_receta r ON d.rec_id=r.rec_id " & _
            "WHERE r.pro_codigo = '" & LvDetalle.ListItems(i) & "'" & _
            " GROUP BY d.pro_codigo "
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
            RsTmp.MoveFirst
            Do While Not RsTmp.EOF
                LvInsumos.ListItems.Add , , RsTmp!pro_codigo
                LvInsumos.ListItems(LvInsumos.ListItems.Count).SubItems(1) = RsTmp!Descripcion
                LvInsumos.ListItems(LvInsumos.ListItems.Count).SubItems(2) = RsTmp!usar
            
                RsTmp.MoveNext
            Loop
        End If
    
    Next
    
    LvMP.ListItems.Clear
    
    For i = 1 To LvInsumos.ListItems.Count
        Bp_Encontro = False
        For p = 1 To LvMP.ListItems.Count
            If LvInsumos.ListItems(i) = LvMP.ListItems(p) Then
                Bp_Encontro = True
                LvMP.ListItems(p).SubItems(2) = CDbl(LvMP.ListItems(p).SubItems(2)) + CDbl(LvInsumos.ListItems(i).SubItems(2))
                Exit For
            End If
        Next p
        
        If Not Bp_Encontro Then
            LvMP.ListItems.Add , , LvInsumos.ListItems(i)
            LvMP.ListItems(LvMP.ListItems.Count).SubItems(1) = LvInsumos.ListItems(i).SubItems(1)
            LvMP.ListItems(LvMP.ListItems.Count).SubItems(2) = LvInsumos.ListItems(i).SubItems(2)
        End If
    Next i
End Sub



Private Sub LvDetalle_DblClick()
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    
End Sub


