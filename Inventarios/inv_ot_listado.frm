VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form inv_ot_listado 
   Caption         =   "Listado OT"
   ClientHeight    =   8700
   ClientLeft      =   4320
   ClientTop       =   3195
   ClientWidth     =   10695
   LinkTopic       =   "Form1"
   ScaleHeight     =   8700
   ScaleWidth      =   10695
   Begin VB.Frame Frame1 
      Caption         =   "Listado ordenes de Trabajo"
      Height          =   6105
      Left            =   300
      TabIndex        =   11
      Top             =   1575
      Width           =   10305
      Begin VB.CommandButton CmdEstado 
         Caption         =   "F3 - Finalizar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   645
         Left            =   4215
         TabIndex        =   15
         ToolTipText     =   "Cambiara estado a Finalizada"
         Top             =   5295
         Width           =   1785
      End
      Begin VB.CommandButton CmdNueva 
         Caption         =   "F1 - Nueva OT"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   645
         Left            =   225
         TabIndex        =   14
         ToolTipText     =   "Nueva OT"
         Top             =   5325
         Width           =   1860
      End
      Begin VB.CommandButton CmdSeleccionar 
         Caption         =   "F2 - Ver"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   645
         Left            =   2220
         TabIndex        =   13
         ToolTipText     =   "Visualizar OT"
         Top             =   5310
         Width           =   1785
      End
      Begin MSComctlLib.ListView LVDetalle 
         Height          =   4815
         Left            =   210
         TabIndex        =   12
         Top             =   375
         Width           =   9975
         _ExtentX        =   17595
         _ExtentY        =   8493
         View            =   3
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   8
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "N109"
            Text            =   "Nro "
            Object.Width           =   882
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "N109"
            Text            =   "Nro OT"
            Object.Width           =   882
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "F1000"
            Text            =   "Fecha"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "Detalle"
            Object.Width           =   6174
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Object.Tag             =   "T1000"
            Text            =   "Usuario"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Object.Tag             =   "T1000"
            Text            =   "Bodega"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   6
            Object.Tag             =   "T1000"
            Text            =   "Estado"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   7
            Text            =   "Bod_Id"
            Object.Width           =   2540
         EndProperty
      End
   End
   Begin VB.Timer Timer1 
      Interval        =   10
      Left            =   510
      Top             =   30
   End
   Begin VB.CommandButton cmdSalir 
      Caption         =   "Retornar"
      Height          =   555
      Left            =   8850
      TabIndex        =   8
      Top             =   7950
      Width           =   1680
   End
   Begin VB.Frame Frame2 
      Caption         =   "Busqueda"
      Height          =   1095
      Left            =   270
      TabIndex        =   0
      Top             =   345
      Width           =   9990
      Begin VB.Frame FraFamilia 
         Caption         =   "Estado"
         Height          =   1095
         Left            =   4710
         TabIndex        =   9
         Top             =   0
         Width           =   2055
         Begin VB.ComboBox CboEstado 
            Height          =   315
            ItemData        =   "inv_ot_listado.frx":0000
            Left            =   120
            List            =   "inv_ot_listado.frx":000D
            Style           =   2  'Dropdown List
            TabIndex        =   10
            Top             =   480
            Width           =   1815
         End
      End
      Begin VB.Frame FraMarca 
         Caption         =   "Bodega"
         Height          =   1095
         Left            =   2625
         TabIndex        =   6
         Top             =   0
         Width           =   2055
         Begin VB.ComboBox CboBodega 
            Height          =   315
            Left            =   90
            Style           =   2  'Dropdown List
            TabIndex        =   7
            Top             =   480
            Width           =   1815
         End
      End
      Begin VB.CommandButton CmdTodosPro 
         Caption         =   "Todos"
         Height          =   375
         Left            =   8850
         TabIndex        =   5
         Top             =   480
         Width           =   975
      End
      Begin VB.CommandButton CmdBuscaProd 
         Caption         =   "&Buscar"
         Height          =   375
         Left            =   7770
         TabIndex        =   4
         Top             =   465
         Width           =   975
      End
      Begin VB.OptionButton OpCodigo 
         Caption         =   "Codigo"
         Height          =   255
         Left            =   1500
         TabIndex        =   3
         Top             =   735
         Width           =   975
      End
      Begin VB.OptionButton OpDescripcion 
         Caption         =   "Descripcion"
         Height          =   255
         Left            =   120
         TabIndex        =   2
         Top             =   720
         Value           =   -1  'True
         Width           =   1230
      End
      Begin VB.TextBox TxtBusca 
         Height          =   375
         Left            =   120
         TabIndex        =   1
         Top             =   240
         Width           =   2385
      End
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   0
      OleObjectBlob   =   "inv_ot_listado.frx":002F
      Top             =   0
   End
End
Attribute VB_Name = "inv_ot_listado"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim Sm_Estado As String

Private Sub CmdBuscaProd_Click()
    If CboEstado.ListIndex = -1 Then
        Sm_Estado = ""
    ElseIf CboEstado.ListIndex = 1 Then
        Sm_Estado = " AND otr_estado='FINALIZADA'"
    ElseIf CboEstado.ListIndex = 0 Then
        Sm_Estado = " AND otr_estado='EJECUCION'"
    Else
        Sm_Estado = ""
    End If
    CargaOT
    
    
End Sub

Private Sub CmdEstado_Click()
    If LVDetalle.SelectedItem Is Nothing Then Exit Sub
    
    If LVDetalle.SelectedItem.SubItems(6) = "EJECUCION" Then
        If MsgBox("Al cambiar estado a finalizado se realizar�n los siguientes procesos:" & vbNewLine & vbNewLine & "1- Se descontaran del inventario los insumos utilizados" & vbNewLine & "2- Pasaran a stock los productos finales." & vbNewLine & vbNewLine & " �Continuar?.. ", vbQuestion + vbOKCancel) = vbOK Then
            
                ActualizaStockOT LVDetalle.SelectedItem.SubItems(1), 0, LVDetalle.SelectedItem.SubItems(3), LVDetalle.SelectedItem.SubItems(7)
                Sql = "UPDATE inv_ot SET otr_estado='FINALIZADA' " & _
                                "WHERE otr_id=" & LVDetalle.SelectedItem.SubItems(1)
                cn.Execute Sql
                CargaOT
        End If
    Else
        MsgBox "OT ya est� finalizada..."
    End If
    
    
End Sub

Private Sub CmdNueva_Click()
    DG_ID_Unico = 0
    inv_OT.Show 1
    CargaOT
End Sub

Private Sub cmdSalir_Click()
    Unload Me
End Sub

Private Sub CmdSeleccionar_Click()
    If LVDetalle.SelectedItem Is Nothing Then Exit Sub
    
    DG_ID_Unico = LVDetalle.SelectedItem.SubItems(1)
    inv_OT.Show 1
    CargaOT
End Sub



Private Sub Form_Load()
    Skin2 Me, , 5
    Centrar Me
    Sm_Estado = " AND otr_estado='EJECUCION'"
    CargaOT
End Sub
Private Sub CargaOT()
    Sql = "SELECT 1,otr_id,otr_fecha,otr_obs,otr_usuario,bod_nombre,otr_estado,o.bod_id " & _
            "FROM inv_ot o " & _
            "JOIN par_bodegas b ON o.bod_id=b.bod_id " & _
            "WHERE 1=1 " & Sm_Estado
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LVDetalle, False, True, True, False
End Sub

Private Sub Timer1_Timer()
    On Error Resume Next
    LVDetalle.SetFocus
    Timer1.Enabled = False
End Sub



Private Sub TxtBusca_GotFocus()
    En_Foco TxtBusca
End Sub

Private Sub TxtBusca_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub


Private Sub ActualizaStockOT(NroDoc As Long, IdDoc As Integer, NombreDoc As String, BodegaId As Integer)
        Dim Dp_Promedio As Double
        Dim Rp_Stock As Recordset
        
        'PRODUCTOS
        Sql = "SELECT pro_codigo codigo,otd_cantidad unidades,'SI' pro_inventariable,0 doc_id," & NroDoc & " id " & _
                "FROM inv_ot_detalle d " & _
                "WHERE otr_id=" & NroDoc
        Consulta Rp_Stock, Sql
        If Rp_Stock.RecordCount > 0 Then
            Rp_Stock.MoveFirst
            Do While Not Rp_Stock.EOF
                If Rp_Stock!pro_inventariable = "SI" Then 'inventariable
                              Dp_Promedio = 0 '
                              Dp_Promedio = CostoAVG(Str(Rp_Stock!Codigo), 1) '
                               Sql = "SELECT pro_precio_neto promedio " & _
                                        "FROM inv_kardex k " & _
                                        "WHERE pro_codigo='" & Rp_Stock!Codigo & "' AND rut_emp='" & SP_Rut_Activo & "' AND bod_id=" & BodegaId & " " & _
                                        "ORDER BY kar_id DESC " & _
                                         "LIMIT 1 "
                              Consulta RsTmp, Sql
                              If RsTmp.RecordCount > 0 Then Dp_Promedio = RsTmp!promedio '
                             Kardex Fql(Date), "ENTRADA", _
                                Rp_Stock!doc_id, Val(NroDoc), BodegaId, Rp_Stock!Codigo, _
                                Rp_Stock!Unidades, "OT " & NombreDoc & " " & NroDoc, _
                                0, 0, SP_Rut_Activo, "DECOHOGAR", , , Rp_Stock!doc_id, , , , Rp_Stock!Id
                End If
                Rp_Stock.MoveNext
            Loop
        End If
       
       'INSUMOS
        Sql = "SELECT pro_codigo codigo,otd_cantidad unidades,'SI' pro_inventariable,0 doc_id," & NroDoc & " id " & _
                "FROM inv_ot_detalle_insumos d " & _
                "WHERE otr_id=" & NroDoc
       
       Consulta Rp_Stock, Sql
        If Rp_Stock.RecordCount > 0 Then
            Rp_Stock.MoveFirst
            Do While Not Rp_Stock.EOF
                If Rp_Stock!pro_inventariable = "SI" Then 'inventariable
                              Dp_Promedio = 0 '
                              Dp_Promedio = CostoAVG(Str(Rp_Stock!Codigo), 1) '
                               Sql = "SELECT pro_precio_neto promedio " & _
                                        "FROM inv_kardex k " & _
                                        "WHERE pro_codigo='" & Rp_Stock!Codigo & "' AND rut_emp='" & SP_Rut_Activo & "' AND bod_id=" & BodegaId & " " & _
                                        "ORDER BY kar_id DESC " & _
                                         "LIMIT 1 "
                              Consulta RsTmp, Sql
                              If RsTmp.RecordCount > 0 Then Dp_Promedio = RsTmp!promedio '
                             Kardex Fql(Date), "SALIDA", _
                                Rp_Stock!doc_id, Val(NroDoc), BodegaId, Rp_Stock!Codigo, _
                                Rp_Stock!Unidades, "OT " & NombreDoc & " " & NroDoc, _
                                0, 0, SP_Rut_Activo, "DECOHOGAR", , , Rp_Stock!doc_id, , , , Rp_Stock!Id
                End If
                Rp_Stock.MoveNext
            Loop
        End If
       
End Sub
