VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{28D47522-CF84-11D1-834C-00A0249F0C28}#1.0#0"; "gif89.dll"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form inv_ficha_bs 
   Caption         =   "Ficha Producto NIT"
   ClientHeight    =   10515
   ClientLeft      =   7035
   ClientTop       =   1950
   ClientWidth     =   15615
   LinkTopic       =   "Form1"
   ScaleHeight     =   10515
   ScaleWidth      =   15615
   Begin VB.CommandButton CmxX 
      Caption         =   "X"
      Height          =   300
      Left            =   14325
      TabIndex        =   145
      Top             =   1140
      Visible         =   0   'False
      Width           =   270
   End
   Begin MSComctlLib.ListView LvModelos 
      Height          =   4425
      Left            =   8655
      TabIndex        =   25
      Top             =   1110
      Visible         =   0   'False
      Width           =   5970
      _ExtentX        =   10530
      _ExtentY        =   7805
      View            =   3
      LabelWrap       =   0   'False
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   2
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Object.Tag             =   "N109"
         Text            =   "Id"
         Object.Width           =   882
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Object.Tag             =   "T1100"
         Text            =   "Modelo"
         Object.Width           =   10583
      EndProperty
   End
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Interval        =   100
      Left            =   30
      Top             =   6134
   End
   Begin VB.Timer Timer2 
      Enabled         =   0   'False
      Interval        =   50
      Left            =   105
      Top             =   6899
   End
   Begin VB.Frame FrameAyuda 
      Caption         =   "Ayuda"
      Height          =   495
      Left            =   240
      TabIndex        =   131
      Top             =   9209
      Width           =   6780
      Begin ACTIVESKINLibCtl.SkinLabel skAyuda 
         Height          =   300
         Left            =   510
         OleObjectBlob   =   "inv_ficha_bs.frx":0000
         TabIndex        =   132
         Top             =   165
         Width           =   5925
      End
   End
   Begin VB.TextBox TxtFlete 
      Alignment       =   1  'Right Justify
      Height          =   285
      Left            =   7530
      TabIndex        =   130
      Text            =   "0"
      Top             =   8744
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.CommandButton CmdBodega 
      Caption         =   "Bodega"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   18165
      TabIndex        =   129
      Top             =   8459
      Visible         =   0   'False
      Width           =   810
   End
   Begin VB.ComboBox CboBodega 
      Height          =   315
      ItemData        =   "inv_ficha_bs.frx":0057
      Left            =   15765
      List            =   "inv_ficha_bs.frx":0059
      Style           =   2  'Dropdown List
      TabIndex        =   128
      Top             =   8459
      Visible         =   0   'False
      Width           =   2415
   End
   Begin VB.TextBox TxtPrecioCompra 
      Alignment       =   1  'Right Justify
      Height          =   285
      Left            =   7530
      Locked          =   -1  'True
      TabIndex        =   127
      TabStop         =   0   'False
      Text            =   "0"
      Top             =   9029
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.TextBox TxtStockActual 
      Alignment       =   1  'Right Justify
      Height          =   285
      Left            =   15765
      Locked          =   -1  'True
      TabIndex        =   126
      TabStop         =   0   'False
      Text            =   "0"
      Top             =   7874
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.TextBox TxtStockCritico 
      Alignment       =   1  'Right Justify
      Height          =   285
      Left            =   15765
      TabIndex        =   125
      Text            =   "0"
      Top             =   0
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.TextBox TxtComentario 
      Height          =   735
      Left            =   10005
      MultiLine       =   -1  'True
      TabIndex        =   124
      Top             =   9089
      Visible         =   0   'False
      Width           =   3855
   End
   Begin VB.Frame Frame3 
      Caption         =   "Informacion adicional"
      Height          =   4305
      Left            =   255
      TabIndex        =   114
      Top             =   4844
      Width           =   6840
      Begin VB.Frame Frame4 
         Caption         =   "Codigo proveedor"
         Height          =   1860
         Left            =   150
         TabIndex        =   120
         Top             =   195
         Width           =   6660
         Begin VB.TextBox txtProveedor 
            BackColor       =   &H8000000F&
            Height          =   285
            Left            =   180
            Locked          =   -1  'True
            TabIndex        =   121
            TabStop         =   0   'False
            Top             =   510
            Width           =   4350
         End
         Begin VB.TextBox TxtCodigoProveedor 
            Height          =   285
            Left            =   4545
            MaxLength       =   50
            TabIndex        =   14
            ToolTipText     =   "Aqui se ingresa el codigo unico"
            Top             =   510
            Width           =   1605
         End
         Begin VB.CommandButton CmdOkProveedor 
            Caption         =   "Ok"
            Height          =   255
            Left            =   6180
            TabIndex        =   15
            Top             =   510
            Width           =   330
         End
         Begin VB.CommandButton cmdBuscaProveedor 
            Caption         =   "Proveedor"
            Height          =   195
            Left            =   150
            TabIndex        =   13
            Top             =   300
            Width           =   1650
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel17 
            Height          =   240
            Left            =   4545
            OleObjectBlob   =   "inv_ficha_bs.frx":005B
            TabIndex        =   122
            Top             =   330
            Width           =   1095
         End
         Begin MSComctlLib.ListView LvProveedor 
            Height          =   945
            Left            =   180
            TabIndex        =   123
            Top             =   795
            Width           =   6330
            _ExtentX        =   11165
            _ExtentY        =   1667
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            FullRowSelect   =   -1  'True
            GridLines       =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   0
            NumItems        =   4
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Object.Tag             =   "N109"
               Text            =   "Id"
               Object.Width           =   0
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Object.Tag             =   "T1000"
               Text            =   "RutProveedor"
               Object.Width           =   0
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   2
               Text            =   "Nombre"
               Object.Width           =   7673
            EndProperty
            BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   3
               Object.Tag             =   "T1500"
               Text            =   "Codigo"
               Object.Width           =   2840
            EndProperty
         End
      End
      Begin VB.Frame Frame6 
         Caption         =   "Otros datos (No obligados)"
         Height          =   2145
         Left            =   45
         TabIndex        =   115
         Top             =   2100
         Width           =   6675
         Begin VB.TextBox TxtCodigoOriginal 
            Height          =   285
            Left            =   45
            TabIndex        =   16
            Top             =   480
            Width           =   2040
         End
         Begin VB.ComboBox CboMarca2 
            Height          =   315
            ItemData        =   "inv_ficha_bs.frx":00C5
            Left            =   4485
            List            =   "inv_ficha_bs.frx":00C7
            Style           =   2  'Dropdown List
            TabIndex        =   19
            Top             =   450
            Width           =   2175
         End
         Begin VB.ComboBox CboProcedencia 
            Height          =   315
            ItemData        =   "inv_ficha_bs.frx":00C9
            Left            =   2490
            List            =   "inv_ficha_bs.frx":00CB
            Style           =   2  'Dropdown List
            TabIndex        =   18
            Top             =   450
            Width           =   1995
         End
         Begin VB.CommandButton cmdProcedencia 
            Caption         =   "Procedencia"
            Height          =   225
            Left            =   2505
            TabIndex        =   117
            Top             =   225
            Width           =   1950
         End
         Begin VB.CommandButton cmdMarca2 
            Caption         =   "Marcas"
            Height          =   240
            Left            =   4500
            TabIndex        =   116
            Top             =   210
            Width           =   2130
         End
         Begin VB.CommandButton CmdOkOriginal 
            Caption         =   "Ok"
            Height          =   270
            Left            =   2085
            TabIndex        =   17
            Top             =   480
            Width           =   390
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
            Height          =   255
            Index           =   5
            Left            =   165
            OleObjectBlob   =   "inv_ficha_bs.frx":00CD
            TabIndex        =   118
            Top             =   195
            Width           =   900
         End
         Begin MSComctlLib.ListView LvOriginales 
            Height          =   1260
            Left            =   60
            TabIndex        =   119
            Top             =   825
            Width           =   6570
            _ExtentX        =   11589
            _ExtentY        =   2223
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            FullRowSelect   =   -1  'True
            GridLines       =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   0
            NumItems        =   3
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Object.Tag             =   "N109"
               Text            =   "Id"
               Object.Width           =   0
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Object.Tag             =   "T1000"
               Text            =   "Codigo Original"
               Object.Width           =   10583
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   2
               Object.Tag             =   "T1000"
               Text            =   "activo"
               Object.Width           =   0
            EndProperty
         End
      End
   End
   Begin VB.CommandButton CmdGuardar 
      Caption         =   "Guardar Registro"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2415
      TabIndex        =   113
      Top             =   9704
      Width           =   2445
   End
   Begin VB.Frame FraModelos 
      Caption         =   "Vehiculos compatibles con este producto"
      Height          =   3540
      Left            =   7335
      TabIndex        =   103
      Top             =   329
      Width           =   7965
      Begin VB.CommandButton CmdAgregaAplicacion 
         Caption         =   "OK"
         Height          =   285
         Left            =   7155
         TabIndex        =   24
         Top             =   510
         Width           =   465
      End
      Begin VB.TextBox TxtAnoHasta 
         Alignment       =   1  'Right Justify
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "0,00%"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   13322
            SubFormatType   =   5
         EndProperty
         Height          =   300
         Left            =   6465
         TabIndex        =   23
         Text            =   "0"
         Top             =   510
         Width           =   675
      End
      Begin VB.TextBox TxtAnoDesde 
         Alignment       =   1  'Right Justify
         Height          =   300
         Left            =   5730
         TabIndex        =   22
         Text            =   "0"
         Top             =   510
         Width           =   675
      End
      Begin VB.ComboBox CboMarcas 
         Height          =   315
         Left            =   150
         Style           =   2  'Dropdown List
         TabIndex        =   20
         Top             =   540
         Width           =   1500
      End
      Begin VB.ComboBox CboModelos 
         Height          =   315
         Left            =   1665
         TabIndex        =   21
         Text            =   "CboModelos"
         Top             =   540
         Width           =   4065
      End
      Begin VB.TextBox TxtCodigoObtAplicacion 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   5040
         TabIndex        =   107
         Text            =   "0"
         Top             =   3225
         Width           =   1125
      End
      Begin VB.CommandButton CmdObtener 
         Caption         =   "Obtener"
         Height          =   225
         Left            =   6225
         TabIndex        =   106
         Top             =   3240
         Width           =   1365
      End
      Begin VB.CommandButton CmdEliminaAplicacion 
         Caption         =   "x"
         Height          =   255
         Left            =   180
         TabIndex        =   105
         Top             =   3225
         Width           =   315
      End
      Begin VB.TextBox TxtBuscaModelo 
         Height          =   300
         Left            =   1680
         TabIndex        =   104
         Top             =   195
         Visible         =   0   'False
         Width           =   4020
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel18 
         Height          =   240
         Index           =   0
         Left            =   6060
         OleObjectBlob   =   "inv_ficha_bs.frx":0143
         TabIndex        =   108
         Top             =   135
         Width           =   585
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel18 
         Height          =   240
         Index           =   1
         Left            =   5730
         OleObjectBlob   =   "inv_ficha_bs.frx":01A9
         TabIndex        =   109
         Top             =   330
         Width           =   570
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel18 
         Height          =   240
         Index           =   2
         Left            =   6420
         OleObjectBlob   =   "inv_ficha_bs.frx":0211
         TabIndex        =   110
         Top             =   315
         Width           =   840
      End
      Begin MSComctlLib.ListView LvAplicacion 
         Height          =   2340
         Left            =   165
         TabIndex        =   111
         Top             =   870
         Width           =   7500
         _ExtentX        =   13229
         _ExtentY        =   4128
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   7
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "N109"
            Text            =   "Id"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "T1000"
            Text            =   "Marca"
            Object.Width           =   2646
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "T3000"
            Text            =   "Modelo"
            Object.Width           =   7170
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Object.Tag             =   "N109"
            Text            =   "Desde"
            Object.Width           =   1182
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Object.Tag             =   "N109"
            Text            =   "Hasta"
            Object.Width           =   1182
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Object.Tag             =   "N109"
            Text            =   "Marca Id"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   6
            Object.Tag             =   "N109"
            Text            =   "modelo"
            Object.Width           =   0
         EndProperty
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel21 
         Height          =   255
         Left            =   2565
         OleObjectBlob   =   "inv_ficha_bs.frx":0279
         TabIndex        =   112
         Top             =   3225
         Width           =   2415
      End
   End
   Begin VB.Frame Frame5 
      Caption         =   "Historial"
      Height          =   4095
      Left            =   7365
      TabIndex        =   96
      Top             =   4064
      Width           =   7935
      Begin TabDlg.SSTab SSTab1 
         Height          =   3555
         Left            =   105
         TabIndex        =   97
         Top             =   315
         Width           =   7725
         _ExtentX        =   13626
         _ExtentY        =   6271
         _Version        =   393216
         TabHeight       =   520
         TabCaption(0)   =   "Compras"
         TabPicture(0)   =   "inv_ficha_bs.frx":0313
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "LvDocumentos"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).ControlCount=   1
         TabCaption(1)   =   "Ventas"
         TabPicture(1)   =   "inv_ficha_bs.frx":032F
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "Text1"
         Tab(1).Control(1)=   "LvVentas"
         Tab(1).Control(2)=   "SkinLabel20(1)"
         Tab(1).ControlCount=   3
         TabCaption(2)   =   "Ventas por Mes"
         TabPicture(2)   =   "inv_ficha_bs.frx":034B
         Tab(2).ControlEnabled=   0   'False
         Tab(2).Control(0)=   "LvVtaMensual"
         Tab(2).ControlCount=   1
         Begin VB.TextBox Text1 
            Height          =   285
            Left            =   -72960
            Locked          =   -1  'True
            TabIndex        =   98
            Top             =   3225
            Width           =   1665
         End
         Begin MSComctlLib.ListView LvDocumentos 
            Height          =   3030
            Left            =   60
            TabIndex        =   99
            Top             =   420
            Width           =   7560
            _ExtentX        =   13335
            _ExtentY        =   5345
            View            =   3
            LabelWrap       =   0   'False
            HideSelection   =   0   'False
            FullRowSelect   =   -1  'True
            GridLines       =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   7
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Object.Tag             =   "F1200"
               Text            =   "Fecha"
               Object.Width           =   1940
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Object.Tag             =   "T1100"
               Text            =   "Nombre "
               Object.Width           =   3528
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   2
               Object.Tag             =   "T100"
               Text            =   "Documento"
               Object.Width           =   3528
            EndProperty
            BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   3
               Object.Tag             =   "T100"
               Text            =   "Nro Documento"
               Object.Width           =   2117
            EndProperty
            BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   4
               Object.Tag             =   "N102"
               Text            =   "Precio.U."
               Object.Width           =   2646
            EndProperty
            BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   5
               Key             =   "cant"
               Object.Tag             =   "N102"
               Text            =   "Cantidad"
               Object.Width           =   2646
            EndProperty
            BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   6
               Key             =   "total"
               Object.Tag             =   "N100"
               Text            =   "Total"
               Object.Width           =   2646
            EndProperty
         End
         Begin MSComctlLib.ListView LvVentas 
            Height          =   2880
            Left            =   -74925
            TabIndex        =   100
            Top             =   360
            Width           =   7560
            _ExtentX        =   13335
            _ExtentY        =   5080
            View            =   3
            LabelWrap       =   0   'False
            HideSelection   =   0   'False
            FullRowSelect   =   -1  'True
            GridLines       =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   7
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Object.Tag             =   "F1200"
               Text            =   "Fecha"
               Object.Width           =   1940
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Object.Tag             =   "T1100"
               Text            =   "Nombre "
               Object.Width           =   3528
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   2
               Object.Tag             =   "T100"
               Text            =   "Documento"
               Object.Width           =   3528
            EndProperty
            BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   3
               Object.Tag             =   "T100"
               Text            =   "Nro Documento"
               Object.Width           =   2117
            EndProperty
            BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   4
               Object.Tag             =   "N102"
               Text            =   "Precio.U."
               Object.Width           =   2646
            EndProperty
            BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   5
               Key             =   "cant"
               Object.Tag             =   "N102"
               Text            =   "Cantidad"
               Object.Width           =   2646
            EndProperty
            BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   6
               Key             =   "total"
               Object.Tag             =   "N100"
               Text            =   "Total"
               Object.Width           =   2646
            EndProperty
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel20 
            Height          =   225
            Index           =   1
            Left            =   -74760
            OleObjectBlob   =   "inv_ficha_bs.frx":0367
            TabIndex        =   101
            Top             =   3255
            Width           =   1695
         End
         Begin MSComctlLib.ListView LvVtaMensual 
            Height          =   2475
            Left            =   -73635
            TabIndex        =   102
            ToolTipText     =   "Solo incluye las ventas, no resta las NC"
            Top             =   570
            Width           =   4875
            _ExtentX        =   8599
            _ExtentY        =   4366
            View            =   3
            LabelWrap       =   0   'False
            HideSelection   =   0   'False
            FullRowSelect   =   -1  'True
            GridLines       =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   2
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Object.Tag             =   "T1000"
               Text            =   "Periodo"
               Object.Width           =   2646
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Object.Tag             =   "N109"
               Text            =   "Unidades"
               Object.Width           =   3528
            EndProperty
         End
      End
   End
   Begin VB.TextBox TxtSeCreo 
      Height          =   285
      Left            =   9600
      Locked          =   -1  'True
      TabIndex        =   94
      Top             =   8219
      Width           =   3765
   End
   Begin VB.Frame Frame1 
      Caption         =   "Ingreso de Productos"
      Height          =   4815
      Left            =   255
      TabIndex        =   53
      Top             =   29
      Width           =   6870
      Begin VB.TextBox TxtPrecioVentaNeto 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   1905
         TabIndex        =   143
         Text            =   "0"
         Top             =   2595
         Width           =   975
      End
      Begin VB.TextBox TxtPcompraCIVA 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   4350
         TabIndex        =   141
         Text            =   "0"
         Top             =   2055
         Width           =   1050
      End
      Begin VB.Frame FrmIvaAnt 
         Caption         =   "Impuestos ILA"
         Height          =   795
         Left            =   4560
         TabIndex        =   63
         Top             =   2970
         Width           =   2295
         Begin VB.ComboBox CboILA 
            Height          =   315
            ItemData        =   "inv_ficha_bs.frx":03E7
            Left            =   90
            List            =   "inv_ficha_bs.frx":03F1
            Style           =   2  'Dropdown List
            TabIndex        =   64
            Top             =   330
            Width           =   2100
         End
      End
      Begin VB.Frame FrmFijaStock 
         Caption         =   "Actualizar Stock"
         Height          =   1020
         Left            =   4560
         TabIndex        =   60
         Top             =   3720
         Visible         =   0   'False
         Width           =   2175
         Begin VB.CommandButton CmdFijarStock 
            Caption         =   "Fijar Stock"
            Height          =   345
            Left            =   150
            TabIndex        =   62
            Top             =   390
            Width           =   1155
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkNewStock 
            Height          =   330
            Left            =   1485
            OleObjectBlob   =   "inv_ficha_bs.frx":0414
            TabIndex        =   61
            Top             =   390
            Width           =   510
         End
      End
      Begin VB.TextBox TxtStockMinimo 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   1875
         TabIndex        =   12
         Text            =   "0"
         Top             =   4395
         Width           =   1125
      End
      Begin VB.TextBox TxtCodigoInterno 
         Height          =   285
         Left            =   1875
         TabIndex        =   11
         Top             =   4095
         Width           =   2655
      End
      Begin VB.ComboBox CboTipo 
         Height          =   315
         ItemData        =   "inv_ficha_bs.frx":0474
         Left            =   1890
         List            =   "inv_ficha_bs.frx":047E
         Style           =   2  'Dropdown List
         TabIndex        =   10
         Top             =   3750
         Width           =   2445
      End
      Begin VB.TextBox txtUbicacionBodega 
         Height          =   285
         Left            =   1890
         TabIndex        =   7
         Top             =   3135
         Width           =   2430
      End
      Begin VB.ComboBox CboHabilitado 
         Height          =   315
         ItemData        =   "inv_ficha_bs.frx":04A1
         Left            =   3570
         List            =   "inv_ficha_bs.frx":04AB
         Style           =   2  'Dropdown List
         TabIndex        =   9
         Top             =   3420
         Width           =   765
      End
      Begin VB.TextBox TxtPrecioCostoSFlete 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   1905
         TabIndex        =   5
         Text            =   "0"
         Top             =   2025
         Width           =   975
      End
      Begin VB.CommandButton CmdUme 
         Caption         =   "UM"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   6240
         TabIndex        =   59
         ToolTipText     =   "Unidades de medida"
         Top             =   1440
         Width           =   450
      End
      Begin VB.ComboBox CboUme 
         Height          =   315
         ItemData        =   "inv_ficha_bs.frx":04B7
         Left            =   5160
         List            =   "inv_ficha_bs.frx":04B9
         Style           =   2  'Dropdown List
         TabIndex        =   4
         Top             =   1455
         Width           =   1095
      End
      Begin VB.CommandButton CmdFamilias 
         Caption         =   "Tipos"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   5160
         TabIndex        =   58
         ToolTipText     =   "Ingrese al Mantenedor de Tipos de Art�culos"
         Top             =   810
         Width           =   1095
      End
      Begin VB.ComboBox CboInventariable 
         Height          =   315
         ItemData        =   "inv_ficha_bs.frx":04BB
         Left            =   1905
         List            =   "inv_ficha_bs.frx":04C5
         Style           =   2  'Dropdown List
         TabIndex        =   8
         Top             =   3435
         Width           =   765
      End
      Begin VB.Frame Frame2 
         Caption         =   "Impuestos del Producto"
         Enabled         =   0   'False
         Height          =   3210
         Left            =   7095
         TabIndex        =   56
         Top             =   1035
         Width           =   4545
         Begin MSComctlLib.ListView LVDetalle 
            Height          =   2670
            Left            =   255
            TabIndex        =   57
            Top             =   390
            Width           =   4110
            _ExtentX        =   7250
            _ExtentY        =   4710
            View            =   3
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            FullRowSelect   =   -1  'True
            GridLines       =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   0
            NumItems        =   3
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Text            =   "Id"
               Object.Width           =   1058
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Text            =   "Descripcion"
               Object.Width           =   4260
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   2
               Object.Tag             =   "N102"
               Text            =   "Factor"
               Object.Width           =   1720
            EndProperty
         End
      End
      Begin VB.TextBox TxtCodigo 
         Height          =   285
         Left            =   1905
         MaxLength       =   50
         TabIndex        =   0
         ToolTipText     =   "Aqui se ingresa el codigo unico"
         Top             =   510
         Width           =   2055
      End
      Begin VB.TextBox TxtDescripcion 
         Height          =   600
         Left            =   1905
         TabIndex        =   3
         Top             =   1455
         Width           =   3240
      End
      Begin VB.TextBox TxtPorcentaje 
         Alignment       =   1  'Right Justify
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "0,00%"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   13322
            SubFormatType   =   5
         EndProperty
         Height          =   285
         Left            =   1905
         TabIndex        =   6
         Text            =   "0"
         Top             =   2295
         Width           =   975
      End
      Begin VB.TextBox TxtPrecioVta 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   4350
         TabIndex        =   55
         Text            =   "0"
         Top             =   2610
         Width           =   1035
      End
      Begin VB.ComboBox CboTipoProducto 
         Height          =   315
         Left            =   1905
         Style           =   2  'Dropdown List
         TabIndex        =   1
         Top             =   795
         Width           =   3255
      End
      Begin VB.ComboBox CboMarca 
         Height          =   315
         ItemData        =   "inv_ficha_bs.frx":04D1
         Left            =   1920
         List            =   "inv_ficha_bs.frx":04D3
         Style           =   2  'Dropdown List
         TabIndex        =   2
         Top             =   1125
         Width           =   3255
      End
      Begin VB.CommandButton CmdMarcas 
         Caption         =   "Marcas"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   5160
         TabIndex        =   54
         ToolTipText     =   "Ingrese al Mantenedor de Marcas de Art�culos"
         Top             =   1125
         Width           =   1095
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel19 
         Height          =   180
         Left            =   1290
         OleObjectBlob   =   "inv_ficha_bs.frx":04D5
         TabIndex        =   65
         Top             =   3795
         Width           =   435
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel16 
         Height          =   240
         Index           =   0
         Left            =   825
         OleObjectBlob   =   "inv_ficha_bs.frx":053B
         TabIndex        =   66
         Top             =   1140
         Width           =   960
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel11 
         Height          =   225
         Left            =   -165
         OleObjectBlob   =   "inv_ficha_bs.frx":05A3
         TabIndex        =   67
         Top             =   1755
         Width           =   1440
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel7 
         Height          =   255
         Left            =   345
         OleObjectBlob   =   "inv_ficha_bs.frx":0601
         TabIndex        =   68
         Top             =   2880
         Width           =   1455
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel6 
         Height          =   255
         Left            =   3195
         OleObjectBlob   =   "inv_ficha_bs.frx":067D
         TabIndex        =   69
         Top             =   2640
         Width           =   1125
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel5 
         Height          =   255
         Left            =   600
         OleObjectBlob   =   "inv_ficha_bs.frx":06F1
         TabIndex        =   70
         Top             =   2340
         Width           =   1215
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
         Height          =   315
         Left            =   495
         OleObjectBlob   =   "inv_ficha_bs.frx":0763
         TabIndex        =   71
         Top             =   840
         Width           =   1335
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   255
         Left            =   375
         OleObjectBlob   =   "inv_ficha_bs.frx":07E1
         TabIndex        =   72
         Top             =   510
         Width           =   1455
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   255
         Index           =   0
         Left            =   615
         OleObjectBlob   =   "inv_ficha_bs.frx":084B
         TabIndex        =   73
         Top             =   1455
         Width           =   1215
      End
      Begin ACTIVESKINLibCtl.SkinLabel LbMargen 
         Height          =   255
         Left            =   1905
         OleObjectBlob   =   "inv_ficha_bs.frx":08BF
         TabIndex        =   74
         Top             =   2895
         Width           =   975
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel12 
         Height          =   195
         Left            =   660
         OleObjectBlob   =   "inv_ficha_bs.frx":091F
         TabIndex        =   75
         Top             =   3450
         Width           =   1095
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel13 
         Height          =   255
         Left            =   60
         OleObjectBlob   =   "inv_ficha_bs.frx":0997
         TabIndex        =   76
         Top             =   2070
         Width           =   1770
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel15 
         Height          =   195
         Left            =   2355
         OleObjectBlob   =   "inv_ficha_bs.frx":0A17
         TabIndex        =   77
         Top             =   3450
         Width           =   1095
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   255
         Index           =   2
         Left            =   600
         OleObjectBlob   =   "inv_ficha_bs.frx":0A89
         TabIndex        =   78
         Top             =   3135
         Width           =   1215
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   255
         Index           =   3
         Left            =   90
         OleObjectBlob   =   "inv_ficha_bs.frx":0AF9
         TabIndex        =   79
         Top             =   4095
         Width           =   1695
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   255
         Index           =   4
         Left            =   75
         OleObjectBlob   =   "inv_ficha_bs.frx":0B83
         TabIndex        =   80
         Top             =   4395
         Width           =   1695
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel25 
         Height          =   255
         Left            =   2895
         OleObjectBlob   =   "inv_ficha_bs.frx":0BF9
         TabIndex        =   142
         Top             =   2085
         Width           =   1455
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel26 
         Height          =   255
         Left            =   435
         OleObjectBlob   =   "inv_ficha_bs.frx":0C79
         TabIndex        =   144
         Top             =   2640
         Width           =   1380
      End
      Begin VB.Label Label4 
         Alignment       =   2  'Center
         Caption         =   "Para poder grabar el registro debe completar todos los campos"
         Height          =   780
         Left            =   5310
         TabIndex        =   82
         Top             =   2160
         Width           =   1935
      End
      Begin VB.Label LbCodigoActual 
         Caption         =   "Label5"
         Height          =   255
         Left            =   4800
         TabIndex        =   81
         Top             =   60
         Width           =   1335
      End
   End
   Begin VB.TextBox TxtFechaUltimoAjuste 
      BackColor       =   &H000000FF&
      ForeColor       =   &H000000FF&
      Height          =   285
      Left            =   9600
      Locked          =   -1  'True
      TabIndex        =   52
      Top             =   8519
      Width           =   3765
   End
   Begin VB.CommandButton CmdVerFotos 
      Caption         =   "Imagenes"
      Height          =   345
      Left            =   240
      TabIndex        =   51
      Top             =   9719
      Width           =   1995
   End
   Begin VB.PictureBox PicImagenes 
      Height          =   9705
      Left            =   15750
      ScaleHeight     =   9645
      ScaleWidth      =   15540
      TabIndex        =   29
      Top             =   179
      Visible         =   0   'False
      Width           =   15600
      Begin VB.PictureBox PicSinFoto 
         Height          =   5010
         Left            =   15150
         Picture         =   "inv_ficha_bs.frx":0CEB
         ScaleHeight     =   4950
         ScaleWidth      =   5175
         TabIndex        =   50
         Top             =   2835
         Width           =   5235
      End
      Begin VB.PictureBox PicFoto 
         Height          =   3765
         Index           =   0
         Left            =   435
         ScaleHeight     =   3705
         ScaleWidth      =   4095
         TabIndex        =   48
         Top             =   585
         Width           =   4155
         Begin VB.CommandButton cmdDescargarImagen 
            Caption         =   "Descargar"
            Height          =   225
            Index           =   0
            Left            =   45
            TabIndex        =   49
            Top             =   3465
            Width           =   1305
         End
      End
      Begin VB.PictureBox PicFoto 
         Height          =   3765
         Index           =   1
         Left            =   5070
         ScaleHeight     =   3705
         ScaleWidth      =   4095
         TabIndex        =   45
         Top             =   570
         Width           =   4155
         Begin VB.CommandButton CmdEliminaFoto 
            Caption         =   "x"
            Height          =   210
            Index           =   1
            Left            =   3705
            TabIndex        =   47
            ToolTipText     =   "Elimina Foto"
            Top             =   3480
            Width           =   360
         End
         Begin VB.CommandButton cmdDescargarImagen 
            Caption         =   "Descargar"
            Height          =   225
            Index           =   1
            Left            =   30
            TabIndex        =   46
            Top             =   3435
            Width           =   1305
         End
      End
      Begin VB.PictureBox PicFoto 
         Height          =   3765
         Index           =   2
         Left            =   9675
         ScaleHeight     =   3705
         ScaleWidth      =   4095
         TabIndex        =   42
         Top             =   555
         Width           =   4155
         Begin VB.CommandButton CmdEliminaFoto 
            Caption         =   "x"
            Height          =   210
            Index           =   2
            Left            =   3705
            TabIndex        =   44
            ToolTipText     =   "Elimina Foto"
            Top             =   3480
            Width           =   360
         End
         Begin VB.CommandButton cmdDescargarImagen 
            Caption         =   "Descargar"
            Height          =   225
            Index           =   2
            Left            =   75
            TabIndex        =   43
            Top             =   3435
            Width           =   1305
         End
      End
      Begin VB.PictureBox PicFoto 
         Height          =   3765
         Index           =   3
         Left            =   465
         ScaleHeight     =   3705
         ScaleWidth      =   4095
         TabIndex        =   39
         Top             =   4755
         Width           =   4155
         Begin VB.CommandButton CmdEliminaFoto 
            Caption         =   "x"
            Height          =   210
            Index           =   3
            Left            =   3660
            TabIndex        =   41
            ToolTipText     =   "Elimina Foto"
            Top             =   3435
            Width           =   360
         End
         Begin VB.CommandButton cmdDescargarImagen 
            Caption         =   "Descargar"
            Height          =   225
            Index           =   3
            Left            =   45
            TabIndex        =   40
            Top             =   3450
            Width           =   1305
         End
      End
      Begin VB.PictureBox PicFoto 
         Height          =   3765
         Index           =   4
         Left            =   5115
         ScaleHeight     =   3705
         ScaleWidth      =   4095
         TabIndex        =   36
         Top             =   4740
         Width           =   4155
         Begin VB.CommandButton CmdEliminaFoto 
            Caption         =   "x"
            Height          =   210
            Index           =   4
            Left            =   3660
            TabIndex        =   38
            ToolTipText     =   "Elimina Foto"
            Top             =   3465
            Width           =   360
         End
         Begin VB.CommandButton cmdDescargarImagen 
            Caption         =   "Descargar"
            Height          =   225
            Index           =   4
            Left            =   45
            TabIndex        =   37
            Top             =   3435
            Width           =   1305
         End
      End
      Begin VB.PictureBox PicFoto 
         Height          =   3765
         Index           =   5
         Left            =   9720
         ScaleHeight     =   3705
         ScaleWidth      =   4095
         TabIndex        =   33
         Top             =   4725
         Width           =   4155
         Begin VB.CommandButton CmdEliminaFoto 
            Caption         =   "x"
            Height          =   210
            Index           =   5
            Left            =   3645
            TabIndex        =   35
            ToolTipText     =   "Elimina Foto"
            Top             =   3465
            Width           =   360
         End
         Begin VB.CommandButton cmdDescargarImagen 
            Caption         =   "Descargar"
            Height          =   225
            Index           =   5
            Left            =   15
            TabIndex        =   34
            Top             =   3465
            Width           =   1305
         End
      End
      Begin VB.CommandButton CmdEliminaFoto 
         Caption         =   "x"
         Height          =   210
         Index           =   0
         Left            =   4170
         TabIndex        =   32
         ToolTipText     =   "Elimina Foto"
         Top             =   4080
         Width           =   360
      End
      Begin VB.CommandButton cmdVolverFicha 
         Caption         =   "Ver Ficha"
         Height          =   405
         Left            =   465
         TabIndex        =   30
         Top             =   9060
         Width           =   2670
      End
      Begin MSComDlg.CommonDialog Dialogo 
         Left            =   105
         Top             =   990
         _ExtentX        =   847
         _ExtentY        =   847
         _Version        =   393216
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel24 
         Height          =   195
         Left            =   435
         OleObjectBlob   =   "inv_ficha_bs.frx":16062D
         TabIndex        =   31
         Top             =   135
         Width           =   7020
      End
   End
   Begin VB.Frame FrmLoad 
      Height          =   1680
      Left            =   11025
      TabIndex        =   27
      Top             =   4379
      Visible         =   0   'False
      Width           =   2805
      Begin GIF89LibCtl.Gif89a Gif89a1 
         Height          =   1275
         Left            =   210
         OleObjectBlob   =   "inv_ficha_bs.frx":160705
         TabIndex        =   28
         Top             =   285
         Width           =   2445
      End
   End
   Begin VB.CommandButton CmdCancelar 
      Cancel          =   -1  'True
      Caption         =   "&Retornar"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   390
      Left            =   4935
      TabIndex        =   26
      ToolTipText     =   "Salir sin hacer cambios"
      Top             =   9689
      Width           =   2070
   End
   Begin TabDlg.SSTab SSTab2 
      Height          =   5430
      Left            =   15870
      TabIndex        =   83
      Top             =   1874
      Visible         =   0   'False
      Width           =   7140
      _ExtentX        =   12594
      _ExtentY        =   9578
      _Version        =   393216
      Tabs            =   2
      TabsPerRow      =   2
      TabHeight       =   520
      TabCaption(0)   =   "Ficha"
      TabPicture(0)   =   "inv_ficha_bs.frx":16078B
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).ControlCount=   0
      TabCaption(1)   =   "Detalle"
      TabPicture(1)   =   "inv_ficha_bs.frx":1607A7
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "SkinLabel23"
      Tab(1).Control(1)=   "TxtFichaElTotal"
      Tab(1).Control(1).Enabled=   0   'False
      Tab(1).Control(2)=   "SkinLabel22"
      Tab(1).Control(3)=   "CmdFichaOk"
      Tab(1).Control(4)=   "TxtFichaTotal"
      Tab(1).Control(4).Enabled=   0   'False
      Tab(1).Control(5)=   "TxtFichaPrecio"
      Tab(1).Control(5).Enabled=   0   'False
      Tab(1).Control(6)=   "TxtFichaCantidad"
      Tab(1).Control(7)=   "TxtFichaDescripcion"
      Tab(1).Control(7).Enabled=   0   'False
      Tab(1).Control(8)=   "TxtFichaCodigo"
      Tab(1).Control(9)=   "LvFicha"
      Tab(1).ControlCount=   10
      Begin VB.TextBox TxtFichaCodigo 
         Alignment       =   1  'Right Justify
         Height          =   300
         Left            =   -74880
         TabIndex        =   92
         ToolTipText     =   "Presione F1 para buscar insumos"
         Top             =   750
         Width           =   795
      End
      Begin VB.TextBox TxtFichaDescripcion 
         BackColor       =   &H8000000F&
         Height          =   300
         Left            =   -74085
         Locked          =   -1  'True
         TabIndex        =   91
         TabStop         =   0   'False
         Top             =   750
         Width           =   3375
      End
      Begin VB.TextBox TxtFichaCantidad 
         Alignment       =   1  'Right Justify
         Height          =   300
         Left            =   -70710
         TabIndex        =   90
         Text            =   "0"
         Top             =   750
         Width           =   675
      End
      Begin VB.TextBox TxtFichaPrecio 
         Alignment       =   1  'Right Justify
         Height          =   300
         Left            =   -70035
         Locked          =   -1  'True
         TabIndex        =   89
         TabStop         =   0   'False
         Text            =   "0"
         Top             =   750
         Width           =   885
      End
      Begin VB.TextBox TxtFichaTotal 
         Alignment       =   1  'Right Justify
         Height          =   300
         Left            =   -69150
         Locked          =   -1  'True
         TabIndex        =   88
         TabStop         =   0   'False
         Text            =   "0"
         Top             =   750
         Width           =   885
      End
      Begin VB.CommandButton CmdFichaOk 
         Caption         =   "Ok"
         Height          =   315
         Left            =   -68265
         TabIndex        =   87
         Top             =   720
         Width           =   330
      End
      Begin VB.TextBox TxtFichaElTotal 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   -69690
         Locked          =   -1  'True
         TabIndex        =   85
         TabStop         =   0   'False
         Top             =   4950
         Width           =   1500
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel23 
         Height          =   195
         Left            =   -71025
         OleObjectBlob   =   "inv_ficha_bs.frx":1607C3
         TabIndex        =   84
         Top             =   5040
         Width           =   1275
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel22 
         Height          =   240
         Left            =   -74775
         OleObjectBlob   =   "inv_ficha_bs.frx":16083B
         TabIndex        =   86
         Top             =   4980
         Width           =   2145
      End
      Begin MSComctlLib.ListView LvFicha 
         Height          =   3825
         Left            =   -74880
         TabIndex        =   93
         Top             =   1080
         Width           =   6945
         _ExtentX        =   12250
         _ExtentY        =   6747
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   6
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "N109"
            Text            =   "Id"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "N109"
            Text            =   "Codigo"
            Object.Width           =   1411
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "T3000"
            Text            =   "Descripcion"
            Object.Width           =   5962
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   3
            Object.Tag             =   "N109"
            Text            =   "Cant."
            Object.Width           =   1323
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   4
            Object.Tag             =   "N109"
            Text            =   "Precio"
            Object.Width           =   1561
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   5
            Key             =   "total"
            Object.Tag             =   "N109"
            Text            =   "Total"
            Object.Width           =   1561
         EndProperty
      End
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel20 
      Height          =   225
      Index           =   0
      Left            =   8460
      OleObjectBlob   =   "inv_ficha_bs.frx":1608D7
      TabIndex        =   95
      Top             =   8264
      Width           =   1050
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   0
      OleObjectBlob   =   "inv_ficha_bs.frx":160945
      Top             =   5519
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkiUbicaci�n 
      Height          =   255
      Left            =   14475
      OleObjectBlob   =   "inv_ficha_bs.frx":160B79
      TabIndex        =   133
      Top             =   8489
      Visible         =   0   'False
      Width           =   1215
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel10 
      Height          =   375
      Left            =   8835
      OleObjectBlob   =   "inv_ficha_bs.frx":160BE3
      TabIndex        =   134
      Top             =   9059
      Visible         =   0   'False
      Width           =   1095
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel9 
      Height          =   255
      Left            =   14235
      OleObjectBlob   =   "inv_ficha_bs.frx":160C55
      TabIndex        =   135
      Top             =   7919
      Visible         =   0   'False
      Width           =   1455
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel8 
      Height          =   255
      Left            =   14235
      OleObjectBlob   =   "inv_ficha_bs.frx":160CCB
      TabIndex        =   136
      Top             =   8204
      Visible         =   0   'False
      Width           =   1455
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
      Height          =   255
      Left            =   5685
      OleObjectBlob   =   "inv_ficha_bs.frx":160D41
      TabIndex        =   137
      Top             =   9074
      Visible         =   0   'False
      Width           =   1770
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
      Height          =   255
      Index           =   1
      Left            =   1230
      OleObjectBlob   =   "inv_ficha_bs.frx":160DCB
      TabIndex        =   138
      Top             =   1439
      Visible         =   0   'False
      Width           =   1215
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel14 
      Height          =   255
      Left            =   5670
      OleObjectBlob   =   "inv_ficha_bs.frx":160E33
      TabIndex        =   139
      Top             =   8789
      Visible         =   0   'False
      Width           =   1770
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel20 
      Height          =   225
      Index           =   2
      Left            =   8460
      OleObjectBlob   =   "inv_ficha_bs.frx":160E9B
      TabIndex        =   140
      Top             =   8564
      Width           =   1050
   End
End
Attribute VB_Name = "inv_ficha_bs"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public Bm_Nuevo As Boolean
Dim Sm_Img_Cargadas As String * 2
Dim Sm_Fijar_Stock_sin_Clave As String * 2
Private Sub CboInventariable_GotFocus()
    FrameAyuda.Caption = "INVENTARIABLE"
    skAyuda = "Si no afecta inventario seleccione NO "
End Sub
Private Sub CboInventariable_LostFocus()
    FrameAyuda = ""
    skAyuda = Empty
End Sub
Private Sub CboMarca_GotFocus()
    FrameAyuda = "Marca o Proveedor(overlock,singer,etc)"
    skAyuda = "Aqui se ingresa la marca o el proveedor del producto "
End Sub
Private Sub CboMarca_LostFocus()
    skAyuda = Empty
    FrameAyuda = ""
End Sub

Private Sub CboMarcas_Click()
    If CboMarcas.ListIndex = -1 Or CboMarcas.ListCount = 0 Then Exit Sub
    LLenarCombo CboModelos, "mob_nombre", "mob_id", "par_buscador_modelos", "mob_activo='SI' AND mab_id=" & CboMarcas.ItemData(CboMarcas.ListIndex), "mob_nombre"
    BuscaModelos
    TxtBuscaModelo = ""
    TxtBuscaModelo.Visible = True
    CmxX.Visible = True
    On Error Resume Next
    TxtBuscaModelo.SetFocus
   ' SendKeys "{Tab}"
End Sub

Private Sub CboMarcas_KeyPress(KeyAscii As Integer)
    'If KeyAscii = vbKeyTab Then
    '    CboMarcas_Validate True
    'End If
End Sub



Private Sub CboTipoProducto_GotFocus()
    FrameAyuda = "Tipo (sacos,g�neros,hilos,etc)"
    skAyuda = "Aqui se ingresa el tipo de producto "
End Sub



Private Sub CboTipoProducto_LostFocus()
    skAyuda = Empty
    FrameAyuda = ""
End Sub

Private Sub CmdAgregaAplicacion_Click()
    
    If CboMarcas.ListIndex = -1 Then
        MsgBox "Seleccione Marca  antes de agregar...", vbInformation
        CboMarcas.SetFocus
        Exit Sub
    End If
    If CboModelos.ListIndex = -1 Then
        MsgBox "Seleccione Modelo antes de agregar...", vbInformation
        CboModelos.SetFocus
        Exit Sub
    End If
    'If Val(TxtAnoDesde) = 0 Then
    '    MsgBox "Seleccione A�os desde y Hasta antes de agregar...", vbInformation
    '    TxtAnoDesde.SetFocus
    ''    Exit Sub
    'End If
    If TxtAnoDesde = "-" Then
        TxtAnoDesde = 0
    End If
    
    If TxtAnoHasta = "-" Then
        TxtAnoHasta = 0
    End If
    
    
    If Val(FraModelos.Tag) = 0 Then
    
            Sql = "INSERT INTO inv_relaciona_codigo_marca_modelo (pro_codigo,mab_id,mob_id,bcm_desde,bcm_hasta,rut_emp) " & _
                "VALUES(" & TxtCodigo & "," & CboMarcas.ItemData(CboMarcas.ListIndex) & "," & CboModelos.ItemData(CboModelos.ListIndex) & "," & TxtAnoDesde & "," & TxtAnoHasta & ",'" & SP_Rut_Activo & "')"
    Else
            Sql = "UPDATE inv_relaciona_codigo_marca_modelo " & _
                    "SET mab_id=" & CboMarcas.ItemData(CboMarcas.ListIndex) & ",mob_id=" & CboModelos.ItemData(CboModelos.ListIndex) & ",bcm_desde=" & TxtAnoDesde & ",bcm_hasta=" & TxtAnoHasta & " " & _
                "WHERE bcm_id=" & FraModelos.Tag
    
            
    
    End If
    cn.Execute Sql
    
    CargaCodigoMarcaModelo

    TxtAnoDesde = 0
    TxtAnoHasta = 0
    CboModelos.ListIndex = -1
    CboMarcas.ListIndex = -1
    FraModelos.Tag = 0
End Sub
Private Sub CargaCodigoMarcaModelo()
    Sql = "SELECT bcm_id,mab_nombre,mob_nombre,IF(bcm_desde=0,'-',bcm_desde),IF(bcm_hasta=0,'-',bcm_hasta) bcm_hasta,x.mab_id,x.mob_id " & _
            "FROM inv_relaciona_codigo_marca_modelo x " & _
            "JOIN par_buscador_modelos m ON x.mob_id=m.mob_id " & _
            "JOIN par_buscador_marcas r ON x.mab_id=r.mab_id " & _
            "WHERE pro_codigo=" & TxtCodigo & " AND x.rut_emp='" & SP_Rut_Activo & "'"
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvAplicacion, False, True, True, False
    

End Sub


Private Sub CmdBodega_Click()
     With Mantenedor_Simple
        .S_Id = "bod_id"
        .S_Nombre = "bod_nombre"
        .S_Activo = "bod_activo"
        .S_tabla = "par_bodegas"
        .S_Consulta = "SELECT bod_id,bod_nombre,bod_activo FROM par_bodegas WHERE rut_emp='" & SP_Rut_Activo & "' ORDER BY bod_nombre"
        .S_RutEmpresa = "SI"
        .Caption = "Mantenedor de bodegas"
        .FrmMantenedor.Caption = "Bodegas"
        .B_Editable = True
        .Show 1
    End With
    LLenarCombo CboBodega, "bod_nombre", "bod_id", "par_bodegas", "bod_activo='SI' AND rut_emp='" & SP_Rut_Activo & "'", "bod_nombre"
End Sub

Private Sub cmdBuscaProveedor_Click()
    AccionProveedor = 0
    BuscaProveedor.Show 1
    txtProveedor.Tag = RutBuscado
    
    If Len(txtProveedor.Tag) > 0 Then
        Sql = "SELECT nombre_empresa " & _
            "FROM maestro_proveedores " & _
            "WHERE rut_proveedor='" & txtProveedor.Tag & "'"
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
            txtProveedor = RsTmp!nombre_empresa
        End If
    End If
    TxtCodigoProveedor.SetFocus
End Sub

Private Sub CmdCancelar_Click()
    If AccionProducto = 1 Then
        MasterProductos.CmdNuevo.Enabled = True
        MasterProductos.CmdEditar.Enabled = True
    End If
    Unload Me
End Sub

Private Sub cmdDescargarImagen_Click(Index As Integer)
    Dim Imagen As IPictureDisp
    Dim ejecutarShell As Variant
    Set Imagen = PicFoto(Index).Image
  
'graba la imagen
    SavePicture Imagen, App.Path & "\temp\imagen.jpg"
    
    

On Error GoTo Error
ejecutarShell = Shell("rundll32.exe url.dll,FileProtocolHandler " & (App.Path & "\temp\imagen.jpg"), 1)
Exit Sub
Error: MsgBox Err.Description, vbExclamation, "Error al procesar la informaci�n"

End Sub

Private Sub CmdEliminaAplicacion_Click()
    If Me.LvAplicacion.SelectedItem Is Nothing Then Exit Sub
    If MsgBox("Esta seguro de eliminar aplicacion..? " & vbNewLine & _
        Me.LvAplicacion.SelectedItem.SubItems(1) & " - " & Me.LvAplicacion.SelectedItem.SubItems(2) & " - " & _
        Me.LvAplicacion.SelectedItem.SubItems(1) & " - " & Me.LvAplicacion.SelectedItem.SubItems(2), vbOKCancel + vbQuestion) = vbOK Then
    
            cn.Execute "DELETE FROM inv_relaciona_codigo_marca_modelo  " & _
                        "WHERE bcm_id=" & Me.LvAplicacion.SelectedItem
            
            CargaCodigoMarcaModelo
    End If
    
End Sub

Private Sub CmdEliminaFoto_Click(Index As Integer)
    PicFoto(Index).Picture = PicSinFoto.Picture
    FrmLoad.Visible = True
    DoEvents
    PicFoto(Index).ScaleMode = 3
    PicFoto(Index).AutoRedraw = True
    PicFoto(Index).PaintPicture PicFoto(Index).Picture, _
        0, 0, PicFoto(Index).ScaleWidth, PicFoto(Index).ScaleHeight

    Sql = "UPDATE par_productos_imagenes SET img_imagen='' " & _
            "WHERE pro_codigo='" & TxtCodigo & "' AND img_orden=" & Index + 1
       
    cn.Execute Sql
    FrmLoad.Visible = False
    
End Sub

Private Sub CmdFamilias_Click()
      'Aqui llamamos a un mantenedor
    Sql = "SELECT mav_id,mav_nombre,mav_activo,mav_tabla,mav_consulta,mav_caption,mav_caption_frm,man_editable, " & _
                        "man_dep_id,man_dep_nombre,man_dep_tabla,man_dep_activo,man_dep_titulo " & _
                  "FROM sis_mantenedor_simple " & _
                  "WHERE man_activo='SI' AND man_id=8"
            Consulta RsTmp2, Sql
            If RsTmp2.RecordCount > 0 Then
                With Mantenedor_Dependencia
                    .S_Id = RsTmp2!mav_id
                    .S_Nombre = RsTmp2!mav_nombre
                    .S_Activo = RsTmp2!mav_activo
                    .S_tabla = RsTmp2!mav_tabla
                    .S_Consulta = RsTmp2!mav_consulta
                    .B_Editable = IIf(RsTmp2!man_editable = "SI", True, False)
                    .S_id_dep = RsTmp2!man_dep_id
                    .S_nombre_dep = RsTmp2!man_dep_nombre
                    .S_Activo_dep = RsTmp2!man_dep_activo
                    .S_tabla_dep = RsTmp2!man_dep_tabla
                    .S_titulo_dep = RsTmp2!man_dep_titulo
                    .CmdBusca.Visible = True
'                    .Caption = RsTmp2!mav_caption
'                    .FrmMantenedor.Caption = RsTmp2!mav_caption_frm

                    .Show 1
                End With
            End If
    LLenarCombo CboTipoProducto, "tip_nombre", "tip_id", "par_tipos_productos", "tip_activo='SI' AND rut_emp='" & SP_Rut_Activo & "'", "tip_nombre"
End Sub

Private Sub CmdFichaOk_Click()
    If Len(TxtFichaCodigo) = 0 Then
        MsgBox "Falta codigo de producto...", vbInformation
        TxtFichaCodigo.SetFocus
        Exit Sub
    End If
    
    If Val(TxtFichaCantidad) = 0 Then
        MsgBox "Falta cantidad...", vbInformation
        TxtFichaCantidad.SetFocus
        Exit Sub
    End If
    
    If Len(TxtFichaDescripcion) = 0 Then
        MsgBox "Faltan datos...", vbInformation
        TxtFichaCodigo.SetFocus
        Exit Sub
    End If
    
    For i = 1 To LvFicha.ListItems.Count
        If TxtFichaCodigo = LvFicha.ListItems(i).SubItems(1) Then
            MsgBox "Este producto ya se encuent� en la lista...", vbInformation
            TxtFichaCodigo.SetFocus
            Exit Sub
        End If
    Next
    
    LvFicha.ListItems.Add , , ""
    LvFicha.ListItems(LvFicha.ListItems.Count).SubItems(1) = TxtFichaCodigo
    LvFicha.ListItems(LvFicha.ListItems.Count).SubItems(2) = TxtFichaDescripcion
    LvFicha.ListItems(LvFicha.ListItems.Count).SubItems(3) = TxtFichaCantidad
    LvFicha.ListItems(LvFicha.ListItems.Count).SubItems(4) = TxtFichaPrecio
    LvFicha.ListItems(LvFicha.ListItems.Count).SubItems(5) = TxtFichaTotal
    
    
    TxtFichaCodigo = ""
    TxtFichaDescripcion = ""
    TxtFichaCantidad = ""
    TxtFichaPrecio = ""
    TxtFichaTotal = ""
    
    TxtFichaElTotal = NumFormat(TotalizaColumna(LvFicha, "total"))
    TxtFichaCodigo.SetFocus
End Sub

Private Sub CmdFijarStock_Click()

    Dim Bp_Autorizado As Boolean
    Dim Sp_Dif As String
    Dim Sp_Ant As String
    Dim Sp_Con As String
    
    
                If Sm_Fijar_Stock_sin_Clave <> "SI" Then 'Esto depende la clave del id 7100 permitira acutalizar stock sin clave
                        SG_codigo2 = Empty
                        sis_InputBox.FramBox = "<Ingrese contrase�a para autorizar>"
                        sis_InputBox.Sm_TipoDato = "T"
                        sis_InputBox.Show 1
                        Sp_Llave = UCase(SG_codigo2)
                        If Len(Sp_Llave) = 0 Then Exit Sub
                        Sql = "SELECT usu_id,usu_nombre,usu_login " & _
                              "FROM sis_usuarios " & _
                              "WHERE per_id=1 AND usu_pwd = MD5('" & Sp_Llave & "')"
                        Consulta RsTmp3, Sql
                        If RsTmp3.RecordCount > 0 Then
                        SG_codigo2 = ""
                            
                        Else
                            MsgBox "Usuario no autorizado !!!", vbExclamation
                            
                            Exit Sub
                        End If
                End If
 '7 Octubre,2013
                Dim Dp_NuevoStock As Variant
 
                Dp_NuevoStock = InputBox("Ingrese el Stock Real de este producto...", "Fijar stock", "0")
                If Not IsNumeric(Dp_NuevoStock) Then Exit Sub
                 Dp_NuevoStock = CxP(Dp_NuevoStock)
                 
                 If Bm_Nuevo Then
                    Me.SkNewStock = Dp_NuevoStock
                    Exit Sub
                End If
                'Rehacer kardex para ajuste de inventario
                    Dp_CantContada = Dp_NuevoStock
                    Dim Dp_SAnterior As Double
                    Dim Dp_Diferencia As Double
                    Dim Dp_PrecioNeto As Double
                    Sql = "SELECT kar_nuevo_saldo,pro_precio_neto " & _
                            "FROM inv_kardex " & _
                            "WHERE pro_codigo='" & TxtCodigo & "' AND rut_emp='" & SP_Rut_Activo & "' " & _
                            "ORDER BY kar_id DESC " & _
                            "LIMIT 1"
                            
                            
                    Consulta RsTmp, Sql
                    If RsTmp.RecordCount > 0 Then
                        Dp_SAnterior = CxP(RsTmp!kar_nuevo_saldo)
                        Sp_Ant = CxP(RsTmp!kar_nuevo_saldo)
                        'Si el saldo que esta ingresando es igual al saldo que teiene el sistema
                        'no hacer nada
                        If Dp_CantContada = Sp_Ant Then
                                MsgBox "El producto tiene el mismo stock que esta ingresando...", vbInformation
                                Exit Sub
                        End If
                        Dp_Diferencia = Val(Dp_CantContada) - Val(Sp_Ant)
                        Sp_Dif = CxP(Dp_Diferencia)
                        Dp_PrecioNeto = RsTmp!pro_precio_neto
                    
                    End If
                'Hacer el insert directo sin la funcion Kardex
                Lp_IdP = UltimoNro("inv_kardex", "kar_id")
                
                Ip_Bodega = 1
                Sql = "SELECT bod_id " & _
                        "FROM par_bodegas " & _
                        "WHERE rut_emp='" & SP_Rut_Activo & "'"
                Consulta RsTmp, Sql
                If RsTmp.RecordCount > 0 Then
                    Ip_Bodega = RsTmp!bod_id
                End If
                
                
                Sql = "INSERT INTO inv_kardex (kar_fecha,kar_movimiento,kar_numero,bod_id,pro_codigo," & _
                    "kar_saldo_anterior,kar_cantidad,kar_nuevo_saldo,kar_usuario,pro_precio_neto,kar_descripcion," & _
                    "kar_saldo_anterior_valor,kar_cantidad_valor,kar_nuevo_saldo_valor,rut_emp) VALUES("
                    
                If Dp_Diferencia < 0 Then
                
                    'Kardex Format(Date, "YYYY-MM-DD"), "SALIDA", 0, Lp_IdP, Ip_Bodega, _
                    Sp_Codigo, Abs(Dp_Cantidad), "AJUSTE PLANILLA " & Str(Lp_IdP), Dp_Promedio, Dp_Promedio * Abs(Dp_Cantidad)
                    
                    cn.Execute Sql & "'" & Fql(Date) & "','SALIDA'," & Lp_IdP & "," & Ip_Bodega & ",'" & TxtCodigo & "'," & _
                    Sp_Ant & "," & Sp_Dif & "," & CxP(Dp_CantContada) & ",'" & LogUsuario & "'," & CxP(Dp_PrecioNeto) & ",'" & _
                    "AJUSTE INDIVIDUAL " & Str(Lp_IdP) & "'," & Round(CxP(Val(Dp_PrecioNeto) * Val(Sp_Ant)), 0) & "," & _
                    Round(CxP(Abs(Val(Sp_Dif)) * CxP(Val(Dp_PrecioNeto))), 0) & "," & CxP(Dp_PrecioNeto * Val(Dp_CantContada)) & ",'" & SP_Rut_Activo & "')"
                    
                    
                    
                    
                    
                    
                ElseIf Dp_Diferencia >= 0 Then
                    'Si la diferencia es positiva hacemos un ingreso en el kardex
              '      Kardex Format(Date, "YYYY-MM-DD"), "ENTRADA", 0, Lp_IdP, Ip_Bodega, _
                    Sp_Codigo, Dp_Cantidad, "AJUSTE PLANILLA " & Str(Lp_IdP), Dp_Promedio, Dp_Promedio * Dp_Cantidad
                    
                        cn.Execute Sql & "'" & Fql(Date) & "','ENTRADA'," & Lp_IdP & "," & Ip_Bodega & ",'" & TxtCodigo & "'," & _
                    Sp_Ant & "," & Sp_Dif & "," & CxP(Dp_CantContada) & ",'" & LogUsuario & "'," & CxP(Dp_PrecioNeto) & ",'" & _
                    "AJUSTE INDIVIDUAL " & Str(Lp_IdP) & "'," & CxP(Dp_PrecioNeto * Val(Sp_Ant)) & "," & _
                    CxP(Abs(Val(Sp_Dif)) * CxP(Val(Dp_PrecioNeto))) & "," & CxP(Dp_PrecioNeto * Val(Dp_CantContada)) & ",'" & SP_Rut_Activo & "')"
                End If
                cn.Execute "UPDATE pro_stock SET sto_stock=" & CxP(Dp_CantContada) & " " & _
                                "WHERE bod_id=" & Ip_Bodega & " AND rut_emp='" & SP_Rut_Activo & "' AND pro_codigo='" & TxtCodigo & "'"
                                
                                
                MsgBox "Nuevo stock actualizadoo...", vbInformation
                        
End Sub
Private Sub NuevoStock()

    Dim Bp_Autorizado As Boolean
    Dim Sp_Dif As String
    Dim Sp_Ant As String
    Dim Sp_Con As String
    
    'SG_codigo2 = Empty
    'sis_InputBox.FramBox = "<Ingrese contrase�a para autorizar>"
    'sis_InputBox.Sm_TipoDato = "T"
    'sis_InputBox.Show 1
    'Sp_Llave = UCase(SG_codigo2)
    'If Len(Sp_Llave) = 0 Then Exit Sub
    'Sql = "SELECT usu_id,usu_nombre,usu_login " & _
          "FROM sis_usuarios " & _
          "WHERE per_id=1 AND usu_pwd = MD5('" & Sp_Llave & "')"
    'Consulta RsTmp3, Sql
    'If RsTmp3.RecordCount > 0 Then
    'SG_codigo2 = ""
        
    'Else
     '   MsgBox "Usuario no autorizado !!!", vbExclamation
        
     '   Exit Sub
    'End If
 '7 Octubre,2013
           '     Dim Dp_NuevoStock As Variant
 
            '    Dp_NuevoStock = InputBox("Ingrese el Stock Real de este producto...", "Fijar stock", "0")
             '   If Not IsNumeric(Dp_NuevoStock) Then Exit Sub
                 Dp_NuevoStock = Val(SkNewStock)
                'Rehacer kardex para ajuste de inventario
                    Dp_CantContada = Dp_NuevoStock
                    Dim Dp_SAnterior As Double
                    Dim Dp_Diferencia As Double
                    Dim Dp_PrecioNeto As Double
                    Sql = "SELECT kar_nuevo_saldo,pro_precio_neto " & _
                            "FROM inv_kardex " & _
                            "WHERE pro_codigo='" & TxtCodigo & "' AND rut_emp='" & SP_Rut_Activo & "' " & _
                            "ORDER BY kar_id DESC " & _
                            "LIMIT 1"
                            
                            
                    Consulta RsTmp, Sql
                    If RsTmp.RecordCount > 0 Then
                        Dp_SAnterior = CxP(RsTmp!kar_nuevo_saldo)
                        Sp_Ant = CxP(RsTmp!kar_nuevo_saldo)
                        'Si el saldo que esta ingresando es igual al saldo que teiene el sistema
                        'no hacer nada
                        If Dp_CantContada = Sp_Ant Then
                                MsgBox "El producto tiene el mismo stock que esta ingresando...", vbInformation
                                Exit Sub
                        End If
                        Dp_Diferencia = Val(Dp_CantContada) - Val(Sp_Ant)
                        Sp_Dif = CxP(Dp_Diferencia)
                        Dp_PrecioNeto = RsTmp!pro_precio_neto
                    
                    End If
                'Hacer el insert directo sin la funcion Kardex
                Lp_IdP = UltimoNro("inv_kardex", "kar_id")
                
                Ip_Bodega = 1
                Sql = "SELECT bod_id " & _
                        "FROM par_bodegas " & _
                        "WHERE rut_emp='" & SP_Rut_Activo & "'"
                Consulta RsTmp, Sql
                If RsTmp.RecordCount > 0 Then
                    Ip_Bodega = RsTmp!bod_id
                End If
                
                
                Sql = "INSERT INTO inv_kardex (kar_fecha,kar_movimiento,kar_numero,bod_id,pro_codigo," & _
                    "kar_saldo_anterior,kar_cantidad,kar_nuevo_saldo,kar_usuario,pro_precio_neto,kar_descripcion," & _
                    "kar_saldo_anterior_valor,kar_cantidad_valor,kar_nuevo_saldo_valor,rut_emp) VALUES("
                    
                If Dp_Diferencia < 0 Then
                
                    'Kardex Format(Date, "YYYY-MM-DD"), "SALIDA", 0, Lp_IdP, Ip_Bodega, _
                    Sp_Codigo, Abs(Dp_Cantidad), "AJUSTE PLANILLA " & Str(Lp_IdP), Dp_Promedio, Dp_Promedio * Abs(Dp_Cantidad)
                    
                    cn.Execute Sql & "'" & Fql(Date) & "','SALIDA'," & Lp_IdP & "," & Ip_Bodega & ",'" & TxtCodigo & "'," & _
                    Sp_Ant & "," & Sp_Dif & "," & CxP(Dp_CantContada) & ",'" & LogUsuario & "'," & CxP(Dp_PrecioNeto) & ",'" & _
                    "AJUSTE INDIVIDUAL " & Str(Lp_IdP) & "'," & Round(CxP(Val(Dp_PrecioNeto) * Val(Sp_Ant)), 0) & "," & _
                    Round(CxP(Abs(Val(Sp_Dif)) * CxP(Val(Dp_PrecioNeto))), 0) & "," & CxP(Dp_PrecioNeto * Val(Dp_CantContada)) & ",'" & SP_Rut_Activo & "')"
                    
                    
                    
                    
                    
                    
                ElseIf Dp_Diferencia >= 0 Then
                    'Si la diferencia es positiva hacemos un ingreso en el kardex
              '      Kardex Format(Date, "YYYY-MM-DD"), "ENTRADA", 0, Lp_IdP, Ip_Bodega, _
                    Sp_Codigo, Dp_Cantidad, "AJUSTE PLANILLA " & Str(Lp_IdP), Dp_Promedio, Dp_Promedio * Dp_Cantidad
                    
                        cn.Execute Sql & "'" & Fql(Date) & "','ENTRADA'," & Lp_IdP & "," & Ip_Bodega & ",'" & TxtCodigo & "'," & _
                    Sp_Ant & "," & Sp_Dif & "," & CxP(Dp_CantContada) & ",'" & LogUsuario & "'," & CxP(Dp_PrecioNeto) & ",'" & _
                    "AJUSTE INDIVIDUAL " & Str(Lp_IdP) & "'," & CxP(Dp_PrecioNeto * Val(Sp_Ant)) & "," & _
                    CxP(Abs(Val(Sp_Dif)) * CxP(Val(Dp_PrecioNeto))) & "," & CxP(Dp_PrecioNeto * Val(Dp_CantContada)) & ",'" & SP_Rut_Activo & "')"
                End If
                cn.Execute "UPDATE pro_stock SET sto_stock=" & CxP(Dp_CantContada) & " " & _
                                "WHERE bod_id=" & Ip_Bodega & " AND rut_emp='" & SP_Rut_Activo & "' AND pro_codigo='" & TxtCodigo & "'"
                                
                                
      '          MsgBox "Nuevo stock actualizadoo...", vbInformation
End Sub
Private Sub CmdGuardar_Click()
    Dim Filtro As String
    Dim sp_Actualiza As String
    If Len(TxtComentario) = 0 Then TxtComentario = "-"
    If Len(TxtUbicacion) = 0 Then TxtUbicacion = "-"
    'accion producto 7 = "Venta directa"'
    'accion producto 6 = "Maestro Productos"'
    'accion producto 8 = "OT
    'accion producto 4 = "busca producto
    If CboInventariable.Text = "NO" Then
        If MsgBox("Esta seguro dejar como" & vbNewLine & "NO INVENTARIABLE..?", vbOKCancel) = vbCancel Then
            CboInventariable.SetFocus
            Exit Sub
        End If
        
    End If
    Filtro = "Codigo = '" & Me.TxtCodigo.Text & "'"
    If Val(Me.TxtPorcentaje) = 0 Then TxtPorcentaje = 0
    
    If CboUme.ListIndex = -1 Then
        MsgBox "Seleccione Unidad de Medida", vbInformation
        CboUme.SetFocus
        Exit Sub
    End If
    If CboMarca.ListIndex = -1 Then
        MsgBox "Seleccione Marca", vbInformation
        CboMarca.SetFocus
        Exit Sub
    End If
    If CboBodega.ListIndex = -1 Then
        MsgBox "Seleccione Bodega...", vbInformation
        CboBodega.SetFocus
        Exit Sub
    End If
    On Error GoTo ErrorGrabando
    cn.BeginTrans
    
     xcienimpueto = 0
    codigoimpuesto = 0
    
    If CboILA.ListIndex > -1 Then
        If CboILA.Text <> "NINGUNO" Then
            'BUSCAR EL %
            Sql = "SELECT imp_adicional " & _
                    "FROM par_impuestos " & _
                    "WHERE imp_id=" & CboILA.ItemData(CboILA.ListIndex)
            Consulta RsTmp, Sql
            If RsTmp.RecordCount > 0 Then
                xcienimpueto = RsTmp!imp_adicional
                codigoimpuesto = CboILA.ItemData(CboILA.ListIndex)
            End If
            
        End If
    End If
    
    If CboProcedencia.ListIndex < 0 Then
        pcd_id = 0
    Else
        pcd_id = CboProcedencia.ItemData(CboProcedencia.ListIndex)
    End If
    
    If CboMarca2.ListIndex < 0 Then
        ma2_id = 0
    Else
        ma2_id = CboMarca2.ItemData(CboMarca2.ListIndex)
    End If
    
    If Me.Bm_Nuevo Then
            Sql = "SELECT codigo " & _
                  "FROM maestro_productos " & _
                  "WHERE  rut_emp='" & SP_Rut_Activo & "' AND  " & Filtro
            Call Consulta(RsTmp, Sql)
            If RsTmp.RecordCount > 0 Then
                MsgBox "El codigo ingresado no esta disponible" & Chr(13) & "El sistema no permite la duplicacion de codigos", vbInformation
                Me.TxtCodigo.SetFocus
                cn.RollbackTrans
                Exit Sub
            End If
            
            If Len(Trim(TxtCodigoInterno)) > 0 Then
                Sql = "SELECT pro_codigo_interno " & _
                        "FROM maestro_productos " & _
                        "WHERE pro_codigo_interno='" & TxtCodigoInterno & "' AND rut_emp='" & SP_Rut_Activo & "'"
                Consulta RsTmp, Sql
                If RsTmp.RecordCount > 0 Then
                    MsgBox "Codigo barra/int ya existe...", vbInformation
                    TxtCodigoInterno.SetFocus
                    cn.RollbackTrans
                    Exit Sub
                End If
                    
            
            
            End If
            
            
            
            
            
            'Sql = "INSERT INTO maestro_productos (" & _
                  "marca,codigo,descripcion,precio_compra,/*porciento_utilidad,*/ precio_venta,margen," & _
                  "stock_actual,stock_critico,ubicacion_bodega,comentario,bod_id,mar_id,tip_id,pro_inventariable," & _
                  "rut_emp,ume_id,pro_precio_sin_flete,pro_precio_flete,pro_activo,pro_codigo_interno) " & _
                  "VALUES ('" & CboMarca.Text & "','" & TxtCodigo & "','" & _
                  TxtDescripcion & "'," & TxtPrecioCompra & "," & Replace(TxtPorcentaje, ",", ".") & "," & _
                  TxtPrecioVta & "," & LbMargen & "," & CxP(TxtStockActual) & "," & TxtStockCritico & ",'" & _
                   txtUbicacionBodega & "','" & "" & TxtComentario & "'," & CboBodega.ItemData(CboBodega.ListIndex) & _
                   "," & CboMarca.ItemData(CboMarca.ListIndex) & "," & CboTipoProducto.ItemData(CboTipoProducto.ListIndex) & ",'" & Me.CboInventariable.Text & _
                   "','" & SP_Rut_Activo & "'," & CboUme.ItemData(CboUme.ListIndex) & "," & CDbl(TxtPrecioCostoSFlete) & "," & _
                   CDbl(TxtFlete) & ",'" & CboHabilitado.Text & "','" & TxtCodigoInterno & "')"
                '   cn.Execute Sql
                   
            Sql = "INSERT INTO maestro_productos (" & _
                  "marca,codigo,descripcion,precio_compra,porciento_utilidad, precio_venta,margen," & _
                  "stock_actual,stock_critico,ubicacion_bodega,comentario,bod_id,mar_id,tip_id,pro_inventariable," & _
                  "rut_emp,ume_id,pro_precio_sin_flete,pro_precio_flete,pro_activo,pro_codigo_interno,pro_iva_anticipado," & _
                  "pro_iva_anticipado_codigo,pro_tipo,pro_codigo_original,pcd_id,ma2_id,pro_precio_costo_bruto,pro_precio_venta_neto) " & _
                  "VALUES ('" & CboMarca.Text & "','" & TxtCodigo & "','" & _
                  TxtDescripcion & "'," & TxtPrecioCostoSFlete & "," & CDbl(TxtPorcentaje) & "," & _
                  CxP(TxtPrecioVta) & "," & CxP(LbMargen) & "," & CxP(TxtStockActual) & "," & TxtStockMinimo & ",'" & _
                   txtUbicacionBodega & "','" & "" & TxtComentario & "'," & CboBodega.ItemData(CboBodega.ListIndex) & _
                   "," & CboMarca.ItemData(CboMarca.ListIndex) & "," & CboTipoProducto.ItemData(CboTipoProducto.ListIndex) & ",'" & Me.CboInventariable.Text & _
                   "','" & SP_Rut_Activo & "'," & CboUme.ItemData(CboUme.ListIndex) & "," & TxtPrecioCostoSFlete & "," & _
                   CDbl(TxtFlete) & ",'" & CboHabilitado.Text & "','" & TxtCodigoInterno & "'," & CxP(xcienimpueto) & "," & codigoimpuesto & ",'" & IIf(CboTipo.ListIndex = 0, "F", "M") & "','" & TxtCodigoOriginal & "'," & pcd_id & "," & ma2_id & "," & CDbl(Me.TxtPcompraCIVA) & "," & CDbl(TxtPrecioVentaNeto) & ")"
                   cn.Execute Sql
            
            'Aqui creamos el Kardex inicial del producto
            '2 Abril 2011
            Kardex Format(Date, "YYYY-MM-DD"), "ENTRADA", 0, 0, CboBodega.ItemData(CboBodega.ListIndex), _
            TxtCodigo, TxtStockActual, "KARDEX INICIAL", PxC(TxtPrecioCostoSFlete), TxtPrecioCostoSFlete * TxtStockActual, , , , , , , , , 0
            
            If Val(SkNewStock) > 0 Then
                NuevoStock
            End If
         Else 'ACTUALIZAR PRODUCTO
            If Len(Trim(TxtCodigoInterno)) > 0 Then
                Sql = "SELECT pro_codigo_interno " & _
                        "FROM maestro_productos " & _
                        "WHERE codigo<>" & TxtCodigo & " AND pro_codigo_interno='" & TxtCodigoInterno & "' AND rut_emp='" & SP_Rut_Activo & "'"
                Consulta RsTmp, Sql
                If RsTmp.RecordCount > 0 Then
                    MsgBox "Codigo barra/int ya existe en otro producto...", vbInformation
                    TxtCodigoInterno.SetFocus
                    cn.RollbackTrans
                    Exit Sub
                End If
                    
            
            
            End If
         
         
            sp_Actualiza = "UPDATE maestro_productos SET marca='" & CboMarca.Text & "'," & "descripcion='" & TxtDescripcion & "'," & _
                                                "precio_compra=" & CxP(TxtPrecioCostoSFlete) & "," & _
                                                "porciento_utilidad=" & Replace(TxtPorcentaje, ",", ".") & "," & _
                                                "precio_venta=" & CxP(TxtPrecioVta) & "," & _
                                                "stock_actual=" & CxP(TxtStockActual) & "," & _
                                                "stock_critico=" & TxtStockMinimo & "," & _
                                                "ubicacion_bodega='" & txtUbicacionBodega & "'," & _
                                                "comentario='" & TxtComentario & "', " & _
                                                "bod_id=" & CboBodega.ItemData(CboBodega.ListIndex) & "," & _
                                                "mar_id=" & CboMarca.ItemData(CboMarca.ListIndex) & "," & _
                                                "pro_inventariable='" & Me.CboInventariable.Text & "'," & _
                                                "rut_emp='" & SP_Rut_Activo & "'," & _
                                                "tip_id=" & CboTipoProducto.ItemData(CboTipoProducto.ListIndex) & ", " & _
                                                "ume_id=" & CboUme.ItemData(CboUme.ListIndex) & "," & _
                                                "pro_precio_sin_flete=" & CxP(TxtPrecioCostoSFlete) & "," & _
                                                "pro_precio_flete=" & CDbl(TxtFlete) & "," & _
                                                "pro_activo='" & CboHabilitado.Text & "', " & _
                                                "pro_codigo_interno='" & TxtCodigoInterno & "', " & _
                                                "pro_iva_anticipado=" & CxP(xcienimpueto) & "," & _
                                                "pro_iva_anticipado_codigo=" & codigoimpuesto & ", " & _
                                                "pro_tipo='" & IIf(CboTipo.ListIndex = 0, "F", "M") & "', " & _
                                                "pro_codigo_original='" & TxtCodigoOriginal & "'," & _
                                                "pcd_id=" & pcd_id & ",ma2_id=" & ma2_id & ", " & _
                                                "pro_precio_costo_bruto=" & CDbl(Me.TxtPcompraCIVA) & ",pro_precio_venta_neto=" & CDbl(TxtPrecioVentaNeto) & " " & _
                    "WHERE codigo='" & TxtCodigo & "' AND rut_emp='" & SP_Rut_Activo & "'"
            Debug.Print sp_Actualiza
            
            
            cn.Execute sp_Actualiza
            
            sp_Actualiza = "UPDATE pro_stock SET pro_ultimo_precio_compra=" & CxP(TxtPrecioCostoSFlete) & " " & _
                           "WHERE rut_emp='" & SP_Rut_Activo & "' AND pro_codigo='" & TxtCodigo & "'"
            cn.Execute sp_Actualiza
            
            
                    
            
            
            If CDbl(Me.TxtPrecioVta.Tag) <> CDbl(Me.TxtPrecioVta) Or CDbl(TxtPrecioCostoSFlete.Tag) <> CDbl(TxtPrecioCostoSFlete) Then
                'Registrar cambio de precios
                sp_Actualiza = "INSERT INTO inv_cambios_de_precios (reg_fecha,reg_hora,usuario,pro_codigo,reg_precio_compra_anterior," & _
                                    "reg_precio_compra_nuevo,reg_precio_venta_anterior,reg_precio_venta_nuevo) " & _
                                    "VALUES('" & Fql(Date) & "','" & Time & "','" & LogUsuario & "'," & TxtCodigo & "," & _
                                    CDbl(TxtPrecioCostoSFlete.Tag) & "," & CDbl(TxtPrecioCostoSFlete) & "," & _
                                    CDbl(TxtPrecioVta.Tag) & "," & CDbl(TxtPrecioVta) & ")"

                cn.Execute sp_Actualiza
            
            End If
            
        End If
        
        'codigos origianles
        Sql = "DELETE FROM par_codigos_originales " & _
                    "WHERE pro_codigo=" & TxtCodigo
        cn.Execute Sql
        
        If LvOriginales.ListItems.Count > 0 Then
            Sql = "INSERT INTO par_codigos_originales (pro_codigo,pro_original) VALUES "
            For i = 1 To Me.LvOriginales.ListItems.Count
                Sql = Sql & "(" & TxtCodigo & ",'" & LvOriginales.ListItems(i).SubItems(1) & "'),"
            
            Next
            Sql = Mid(Sql, 1, Len(Sql) - 1)
            cn.Execute Sql
        End If
        
        'Comprobar codigos de proveedor
        cn.Execute "DELETE FROM par_codigos_proveedor " & _
                    "WHERE pro_codigo='" & TxtCodigo & "' AND rut_emp='" & SP_Rut_Activo & "'"
                    
        If LvProveedor.ListItems.Count > 0 Then
            Sql = "INSERT INTO par_codigos_proveedor (rut_proveedor,cpv_codigo_proveedor,rut_emp,pro_codigo) " & _
                    "VALUES "
            For i = 1 To LvProveedor.ListItems.Count
                sql2 = "('" & LvProveedor.ListItems(i).SubItems(1) & "','" & LvProveedor.ListItems(i).SubItems(3) & "','" & SP_Rut_Activo & "'," & TxtCodigo & "),"
                Sql = Sql & sql2
            Next
            Sql = Mid(Sql, 1, Len(Sql) - 1)
            cn.Execute Sql
        End If
        
        
        
        SG_codigo2 = TxtCodigoInterno
        
        
        If LvFicha.ListItems.Count > 0 Then
            Sql = "INSERT INTO par_ficha_producto (pro_codigo,fch_cantidad,fch_precio,fch_total,pro_codigo_producto) VALUES "
            
            For i = 1 To LvFicha.ListItems.Count
                Sql = Sql & "(" & LvFicha.ListItems(i).SubItems(1) & "," & LvFicha.ListItems(i).SubItems(3) & "," & LvFicha.ListItems(i).SubItems(4) & "," & LvFicha.ListItems(i).SubItems(4) & "," & TxtCodigo & "),"
            
            Next
            cn.Execute Mid(Sql, 1, Len(Sql) - 1)
            
        End If
        
        cn.CommitTrans
        SG_codigo3 = "GRABO"
        Unload Me
        Exit Sub
ErrorGrabando:
    MsgBox "Ocurrio un error al intentar grabar el registro..." & Chr(13) & Err.Description, vbExclamation
    
    cn.RollbackTrans
End Sub


Private Sub cmdMarca2_Click()
'Aqui llamamos a un mantenedor
    With Mantenedor_Simple
        .S_Id = "ma2_id"
        .S_Nombre = "ma2_nombre"
        .S_Activo = "ma2_activo"
        .S_tabla = "par_marcas2"
        .S_RutEmpresa = "NO"
        .S_Consulta = "SELECT " & .S_Id & "," & .S_Nombre & "," & .S_Activo & " " & _
                      "FROM " & .S_tabla & " " & _
                      "WHERE 2=2"
        .Caption = "Mantenedor Marca adicional"
        .FrmMantenedor.Caption = "Marcas "
        .B_Editable = True
        .Show 1
    End With
            LLenarCombo CboMarca2, "ma2_nombre", "ma2_id", "par_marcas2", "ma2_activo='SI'", "ma2_nombre"

End Sub

Private Sub CmdMarcas_Click()
    With Mantenedor_Simple
        .S_Id = "mar_id"
        .S_Nombre = "mar_nombre"
        .S_Activo = "mar_activo"
        .S_tabla = "par_marcas"
        .S_RutEmpresa = "SI"
        .S_Consulta = "SELECT " & .S_Id & "," & .S_Nombre & "," & .S_Activo & " " & _
                      "FROM " & .S_tabla & " " & _
                      "WHERE  rut_emp='" & SP_Rut_Activo & "' "
        .Caption = "Mantenedor Marcas de Articulos"
        .FrmMantenedor.Caption = "Marcas "
        .B_Editable = True
        .Show 1
    End With
    LLenarCombo CboMarca, "mar_nombre", "mar_id", "par_marcas", "mar_activo='SI' AND rut_emp='" & SP_Rut_Activo & "'", "mar_nombre"

End Sub

Private Sub CmdObtener_Click()
    If MsgBox("Seguro obtener apliacion del codigo " & TxtCodigoObtAplicacion & ")", vbOKCancel + vbQuestion) = vbCancel Then Exit Sub
    
    Sql = "INSERT INTO inv_relaciona_codigo_marca_modelo(pro_codigo,mab_id,mob_id,bcm_desde,bcm_hasta,rut_emp) " & _
            "SELECT " & TxtCodigo & ",mab_id,mob_id,bcm_desde,bcm_hasta,rut_emp " & _
                "FROM inv_relaciona_codigo_marca_modelo " & _
                "WHERE pro_codigo=" & TxtCodigoObtAplicacion
    cn.Execute Sql
    
    CargaCodigoMarcaModelo
                
End Sub

Private Sub CmdOkOriginal_Click()
    If Len(Me.TxtCodigoOriginal) = 0 Then
        MsgBox "Falta ingresar datos.."
        TxtCodigoOriginal.SetFocus
        Exit Sub
    End If
    LvOriginales.ListItems.Add , , 1
    LvOriginales.ListItems(LvOriginales.ListItems.Count).SubItems(1) = TxtCodigoOriginal
    TxtCodigoOriginal = ""
    TxtCodigoOriginal.SetFocus
    
    
End Sub

Private Sub CmdOkProveedor_Click()
    If Len(txtProveedor) = 0 Then
        MsgBox "Seleccione proveedor...", vbInformation
        Me.cmdBuscaProveedor.SetFocus
        Exit Sub
    End If
    If Len(TxtCodigoProveedor) = 0 Then
        MsgBox "Ingrese codigo proveedor...", vbInformation
        TxtCodigoProveedor.SetFocus
        Exit Sub
    End If
    
    LvProveedor.ListItems.Add , , 0
    LvProveedor.ListItems(LvProveedor.ListItems.Count).SubItems(1) = txtProveedor.Tag
    LvProveedor.ListItems(LvProveedor.ListItems.Count).SubItems(2) = txtProveedor
    LvProveedor.ListItems(LvProveedor.ListItems.Count).SubItems(3) = TxtCodigoProveedor
    
    txtProveedor.Tag = ""
    txtProveedor = ""
    TxtCodigoProveedor = ""
    
    
    
End Sub

Private Sub cmdProcedencia_Click()
   'Aqui llamamos a un mantenedor
    With Mantenedor_Simple
        .S_Id = "pcd_id"
        .S_Nombre = "pcd_nombre"
        .S_Activo = "pcd_activo"
        .S_tabla = "par_procedencias"
        .S_RutEmpresa = "NO"
        .S_Consulta = "SELECT " & .S_Id & "," & .S_Nombre & "," & .S_Activo & " " & _
                      "FROM " & .S_tabla & " " & _
                      "WHERE 2=2 "
        .Caption = "Procedencias"
        .FrmMantenedor.Caption = "Procedencias "
        .B_Editable = True
        .Show 1
    End With
          LLenarCombo CboProcedencia, "pcd_nombre", "pcd_id", "par_procedencias", "pcd_activo='SI'", "pcd_nombre"
End Sub

Private Sub CmdUme_Click()
    With Mantenedor_Simple
        .S_Id = "ume_id"
        .S_Nombre = "ume_nombre"
        .S_Activo = "ume_activo"
        .S_tabla = "sis_unidad_medida"
        .S_RutEmpresa = "SI"
        .S_Consulta = "SELECT " & .S_Id & "," & .S_Nombre & "," & .S_Activo & " " & _
                      "FROM " & .S_tabla & " " & _
                      "WHERE  rut_emp='" & SP_Rut_Activo & "' "
        .Caption = "Mantenedor Unidades de medida"
        .FrmMantenedor.Caption = "Unidades de medida "
        .B_Editable = True
        .Show 1
    End With
    LLenarCombo CboUme, "ume_nombre", "ume_id", "sis_unidad_medida", "ume_activo='SI' AND rut_emp='" & SP_Rut_Activo & "'", "ume_nombre"

End Sub



Private Sub Command1_Click()
    Sql = "SELECT CONCAT(substr(MONTHNAME(fecha),1,3),'-',year(fecha)),SUM(unidades) " & _
            "From ven_detalle " & _
        "WHERE codigo='" & TxtCodigo & "' " & _
        "GROUP BY MONTH(fecha),codigo "
    
    Consulta RsTmp, Sql
    
    Set Grafico.DataSource = RsTmp
    
    
End Sub

Private Sub CmdVerFotos_Click()
    PicImagenes.Visible = True
    If Sm_Img_Cargadas = "NO" Then
        FrmLoad.Visible = True
        DoEvents
      '  CargaImagenes
        Sm_Img_Cargadas = "SI"
    
        FrmLoad.Visible = False
    End If
End Sub


Private Sub cmdVolverFicha_Click()
    PicImagenes.Visible = False
End Sub

Private Sub CmxX_Click()
    LvModelos.Visible = False
    CmxX.Visible = False
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        On Error Resume Next
        SendKeys "{Tab}"
    End If
End Sub


Private Sub VerHistorialProducto()


    
    
    'Primero, historico Ventas
    '14 Marzo 2016
    Sql = "SELECT m.fecha,nombre_cliente,doc_nombre,m.no_documento,c.precio_final,c.unidades,c.subtotal " & _
              "FROM ven_doc_venta m " & _
              "INNER JOIN sis_documentos d USING(doc_id),ven_detalle c " & _
              "WHERE ven_informa_venta = 'SI' AND c.rut_emp='" & SP_Rut_Activo & "' AND doc_nota_de_venta='NO' AND m.rut_emp='" & SP_Rut_Activo & "' AND c.doc_id=m.doc_id AND c.no_documento=m.no_documento AND codigo ='" & TxtCodigo & "' " & _
              "ORDER BY m.fecha"
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, Me.LvVentas, False, True, True, False
    
    
    
    'Ahora la cantidad vendedia por cada mes
    'Sql = "SELECT CONCAT(CAST(month(c.fecha) AS CHAR),'-' ,CAST(year(c.fecha) AS CHAR)) periodo,SUM(unidades) cantidad " & _
                "FROM ven_doc_venta m " & _
                "INNER JOIN sis_documentos d USING (doc_id), " & _
                "ven_detalle c WHERE " & _
                    "ven_informa_venta = 'SI' AND c.rut_emp = '" & SP_Rut_Activo & "' " & _
                "AND doc_nota_de_credito='NO' AND doc_nota_de_venta = 'NO' AND m.rut_emp = '" & SP_Rut_Activo & "' " & _
                "AND c.doc_id = m.doc_id AND c.no_documento = m.no_documento " & _
                "AND codigo = '" & TxtCodigo & "' GROUP BY MONTH(c.fecha) " & _
                "ORDER BY    cantidad DESC,c.fecha "
                
    Sql = "SELECT concat(year(fecha),'-',month(fecha)),SUM(unidades) cantidad " & _
                "FROM    ven_detalle c " & _
                "WHERE c.rut_emp = '" & SP_Rut_Activo & "' AND codigo = " & TxtCodigo & " " & _
                "GROUP BY    year (c.fecha),month(fecha) " & _
                "ORDER BY    year (c.fecha),month(fecha)"
    Consulta RsTmp, Sql
        LLenar_Grilla RsTmp, Me, Me.LvVtaMensual, False, True, True, False
    
    
    'Ahora las compras ' P R O V E D O R E S
    Sql = "SELECT m.fecha,nombre_empresa,doc_nombre,no_documento,c.cmd_unitario_neto,c.cmd_cantidad,c.cmd_total_neto " & _
              "FROM com_doc_compra m " & _
              "INNER JOIN com_doc_compra_detalle c ON m.id = c.id " & _
              "INNER JOIN sis_documentos d USING(doc_id) " & _
              "JOIN maestro_proveedores p ON m.rut=p.rut_proveedor " & _
              "WHERE d.doc_orden_de_compra='NO' AND m.rut_emp='" & SP_Rut_Activo & "' AND pro_codigo ='" & TxtCodigo & "' " & _
              "ORDER BY m.fecha"
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, Me.LvDocumentos, False, True, True, False

End Sub
Private Sub CargaDatosProductos()
    Dim Sp_Repuestos As String * 2
    On Error GoTo MalMal
    PicImagenes.Left = 105
    FrmLoad.Visible = True
    Sm_Img_Cargadas = "NO"
    DoEvents
    CboHabilitado.ListIndex = 0
    LLenarCombo CboTipoProducto, "tip_nombre", "tip_id", "par_tipos_productos", "tip_activo='SI' AND rut_emp='" & SP_Rut_Activo & "'", "tip_nombre"
    LLenarCombo CboMarca, "mar_nombre", "mar_id", "par_marcas", "mar_activo='SI' AND rut_emp='" & SP_Rut_Activo & "'", "mar_nombre"
    LLenarCombo CboBodega, "bod_nombre", "bod_id", "par_bodegas", "bod_activo='SI' AND rut_emp='" & SP_Rut_Activo & "'", "bod_nombre"
    LLenarCombo CboUme, "ume_nombre", "ume_id", "sis_unidad_medida", "ume_activo='SI' AND rut_emp='" & SP_Rut_Activo & "'", "ume_nombre"
    
    
    'If ComprobarTabla("par_procedencias") = 1 Then
            LLenarCombo CboProcedencia, "pcd_nombre", "pcd_id", "par_procedencias", "pcd_activo='SI'", "pcd_nombre"
    'End If
    
    'If ComprobarTabla("par_marcas2") = 1 Then
            LLenarCombo CboMarca2, "ma2_nombre", "ma2_id", "par_marcas2", "ma2_activo='SI'", "ma2_nombre"
    'End If
    
     CboBodega.ListIndex = 0
    CboMarca.ListIndex = 0
    CboInventariable.ListIndex = 0
    'Skin2 Me, , 5
 
    LLenarCombo CboMarcas, "mab_nombre", "mab_id", "par_buscador_marcas", "mab_activo='SI'", "mab_nombre"
    
    If SP_Rut_Activo = "77.328.790-2" Or SP_Rut_Activo = "76.430.524-8" Then
        'molineras super mercados
        LLenarCombo CboILA, "imp_nombre", "imp_id", "par_impuestos", "imp_activo='SI'", "imp_nombre"
        CboILA.AddItem "NINGUNO"
        CboILA.ListIndex = CboILA.ListCount - 1
    End If
    CboTipo.ListIndex = 0
    Sp_Repuestos = "NO"
    Sql = "SELECT emp_de_repuestos,emp_iva_anticipado_ventas iva_ant " & _
            "FROM sis_empresas " & _
            "WHERE rut='" & SP_Rut_Activo & "'"
    Consulta RsTmp, Sql
    Me.Width = 7775
    If RsTmp!emp_de_repuestos = "SI" Or SP_Rut_Activo = "78.967.170-2" Then
        Sp_Repuestos = "SI"
        Me.CmdCancelar.Left = 13500
        Me.Width = 15540
    End If
    If Len(SG_codigo) = 0 Then
        'Asumimos que es nuevo producto
        Me.Bm_Nuevo = True
         If SG_Codigos_Alfanumericos = "NO" Then 'SON NUEMRICOS
            SubNuevoCodigo
        Else
            If SP_Rut_Activo = "76.063.757-2" Or SP_Rut_Activo = "10.966.790-0" Then
                SubNuevoCodigo
            End If
            
        End If
    Else
        'Aqui editamos el producto
        Bm_Nuevo = False
        Sql = "SELECT codigo,tip_id,mar_id,descripcion," & _
                    "ROUND(IFNULL((SELECT AVG(pro_ultimo_precio_compra) FROM pro_stock s WHERE s.rut_emp='" & SP_Rut_Activo & "' AND s.pro_codigo=maestro_productos.codigo),0),2) precio_compra," & _
                    "/*IFNULL((SELECT pro_ultimo_precio_compra FROM pro_stock s WHERE s.rut_emp='" & SP_Rut_Activo & "' AND s.pro_codigo=maestro_productos.codigo LIMIT 1),2) precio_compra,*/" & _
                    "porciento_utilidad,precio_venta,margen," & _
                    "IFNULL((SELECT SUM(sto_stock) FROM pro_stock s WHERE rut_emp='" & SP_Rut_Activo & "' AND s.pro_codigo=maestro_productos.codigo),0) stock," & _
                     "stock_critico,bod_id,ubicacion_bodega,comentario,pro_inventariable,ume_id,pro_precio_sin_flete,pro_precio_flete,pro_activo,pro_codigo_interno,pro_iva_anticipado_codigo,pro_tipo,pro_codigo_original,pcd_id,ma2_id,pro_precio_costo_bruto costo_bruto,pro_precio_venta_neto venta_neto " & _
              "FROM maestro_productos  " & _
              "WHERE  rut_emp='" & SP_Rut_Activo & "' AND codigo='" & SG_codigo & "'"
        Consulta RsTmp, Sql
        If RsTmp.RecordCount = 0 Then Exit Sub
        
        With RsTmp
            TxtCodigo = !Codigo
            Busca_Id_Combo CboTipoProducto, !tip_id
            Busca_Id_Combo CboMarca, !mar_id
            TxtDescripcion = !Descripcion
            
            Me.TxtPorcentaje = Val(0 & !porciento_utilidad)
            Me.TxtPrecioVta = !precio_venta
            TxtPrecioVta.Tag = TxtPrecioVta
            LbMargen = "" & Int(!Margen)
            TxtStockActual = !stock
            TxtCodigoInterno = "" & !pro_codigo_interno
            TxtStockCritico = !stock_critico
            TxtStockMinimo = !stock_critico
            Busca_Id_Combo CboBodega, !bod_id
            txtUbicacionBodega = !ubicacion_bodega
            TxtComentario = "" & !comentario
            TxtPrecioCostoSFlete = CxP(!precio_compra) '!pro_precio_sin_flete
            TxtPrecioCostoSFlete.Tag = CDbl(TxtPrecioCostoSFlete)
            Me.TxtPcompraCIVA = "" & !costo_bruto
            Me.TxtPrecioVentaNeto = "" & !venta_neto
            
         '   lvmargen = CDbl(TxtPrecioVentaNeto) - CDbl(TxtPrecioCostoSFlete)
            
            TxtFlete = CxP(!pro_precio_flete)
            CboTipo.ListIndex = IIf(!pro_tipo = "F", 0, 1)
            TxtPrecioCompra = !precio_compra
            If CDbl(TxtPrecioCompra) = 0 Then Me.TxtPrecioCompra = "1"
            Me.CboInventariable.ListIndex = IIf(!pro_inventariable = "SI", 0, 1)
            CboHabilitado.ListIndex = IIf(!pro_activo = "SI", 0, 1)
            Busca_Id_Combo CboUme, !ume_id
            
            If Val("" & !pro_iva_anticipado_codigo) > 0 Then
                Busca_Id_Combo CboILA, !pro_iva_anticipado_codigo
            End If
            Busca_Id_Combo CboProcedencia, Val("" & !pcd_id)
            Busca_Id_Combo CboMarca2, Val("" & !ma2_id)
            TxtCodigoOriginal = "" & !pro_codigo_original
            
        End With
        Sql = "SELECT cpv_id,c.rut_proveedor,nombre_empresa,cpv_codigo_proveedor  " & _
                "FROM par_codigos_proveedor c " & _
                "JOIN maestro_proveedores m ON c.rut_proveedor=m.rut_proveedor " & _
                "WHERE pro_codigo='" & TxtCodigo & "' AND c.rut_emp='" & SP_Rut_Activo & "'"
        Consulta RsTmp, Sql
        LLenar_Grilla RsTmp, Me, LvProveedor, False, True, True, False
        TxtCodigo.Locked = True
        
        If Sp_Repuestos = "SI" Then
               CargaCodigoMarcaModelo
        End If
        On Error Resume Next
        
        
        Sql = "SELECT kar_fecha,kar_descripcion " & _
                "FROM inv_kardex " & _
                "WHERE rut_emp='" & SP_Rut_Activo & "' AND pro_codigo=" & TxtCodigo & " " & _
                "LIMIT 1"
                
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
            Me.TxtSeCreo = RsTmp!kar_fecha & " " & RsTmp!kar_descripcion
        
        End If
        Sql = " SELECT tmi_fecha " & _
                    "FROM inv_toma_inventario_detalle d " & _
                    "JOIN inv_toma_inventario i ON d.tmi_id=i.tmi_id " & _
                    "WHERE tmi_estado='AJUSTADO' AND pro_codigo = " & TxtCodigo & " " & _
                    "ORDER BY tdi_id DESC " & _
                    "LIMIT 1"
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
            TxtFechaUltimoAjuste = RsTmp!tmi_fecha
        End If

        CboTipoProducto.SetFocus
   End If
   
   If Sp_Repuestos = "SI" Then VerHistorialProducto
   
'   Sql = "SELECT fch_id,pro_codigo,descripcion,fch_cantidad,fch_precio,fch_total " & _
'            "FROM par_ficha_producto f " & _
'            "JOIN maestro_productos m ON pro_codigo=codigo " & _
'            "WHERE pro_codigo_producto=" & TxtCodigo
'    Consulta RsTmp, Sql
'    LLenar_Grilla RsTmp, Me, LvFicha, False, True, True, False
'    TxtFichaElTotal = NumFormat(TotalizaColumna(LvFicha, "total"))
'
   If (SP_Rut_Activo = "76.169.962-8" Or SP_Rut_Activo = "76.553.302-3" Or SP_Rut_Activo = "76.039.757-1" Or _
   SP_Rut_Activo = "76.504.041-8" Or SP_Rut_Activo = "12.072.366-9" Or _
   SP_Rut_Activo = "78.967.170-2" Or SP_Rut_Activo = "76.603.639-2" Or SP_Rut_Activo = "76.881.179-2") And LogPerfil = 1 Then
   
        FrmFijaStock.Visible = True
   End If
   
   Sql = "SELECT par_id,par_valor " & _
            "FROM tabla_parametros " & _
            "WHERE par_id IN(7000,7100) AND par_activo='SI'"
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        Do While Not RsTmp.EOF
            If RsTmp!par_id = 7000 Then
                If RsTmp!par_valor = "SI" Then
                    FrmFijaStock.Visible = True
                End If
            End If
            If RsTmp!par_id = 7100 Then
                If RsTmp!par_valor = "SI" Then
                    Sm_Fijar_Stock_sin_Clave = "SI"
                End If
            End If
            RsTmp.MoveNext
        Loop
    End If
   
   
    Sql = "SELECT ori_id,pro_original,ori_activo " & _
            "FROM par_codigos_originales " & _
            "WHERE pro_codigo=" & TxtCodigo
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, Me.LvOriginales, False, True, True, False
   
   
   
   For i = 0 To 5
    PicFoto(i).Picture = PicSinFoto.Picture
    PicFoto(i).ScaleMode = 3
    PicFoto(i).AutoRedraw = True
    PicFoto(i).PaintPicture PicFoto(i).Picture, _
        0, 0, PicFoto(i).ScaleWidth, PicFoto(i).ScaleHeight
   Next
   
   FrmLoad.Visible = False
   Exit Sub
MalMal:
   MsgBox "Problema a cargar ficha.." & vbNewLine & Err.Description & vbNewLine & Err.Number
End Sub
Private Sub SubNuevoCodigo()
    Sql = "SELECT ifnull(MAX(codigo)+1 ,1) nuevocodigo " & _
                  "FROM maestro_productos " & _
                  "WHERE rut_emp='" & SP_Rut_Activo & "'"
            Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        TxtCodigo = RsTmp!nuevocodigo
    Else
        TxtCodigo = 1
    End If
End Sub
Private Sub Form_Load()
    SG_codigo3 = "SINGRABAR"
   On Error GoTo problema
   Skin2 Me, , 5
   Centrar Me
  FrmLoad.Visible = True
  DoEvents
   CargaDatosProductos
problema:
End Sub




Private Sub CargaImpuestos()
    Sql = "SELECT imp_id,imp_nombre,imp_adicional " & _
          "FROM par_impuestos " & _
          "WHERE imp_activo='SI'"
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvDetalle, True, True, True, False
    
    
    
End Sub


Private Sub LvAplicacion_DblClick()
    If LvAplicacion.SelectedItem Is Nothing Then Exit Sub
    
        Busca_Id_Combo CboMarcas, Val(LvAplicacion.SelectedItem.SubItems(5))
        LLenarCombo CboModelos, "mob_nombre", "mob_id", "par_buscador_modelos", "mob_activo='SI' AND mab_id=" & CboMarcas.ItemData(CboMarcas.ListIndex), "mob_nombre"
        Busca_Id_Combo CboModelos, Val(LvAplicacion.SelectedItem.SubItems(6))
        TxtAnoDesde = LvAplicacion.SelectedItem.SubItems(3)
        TxtAnoHasta = LvAplicacion.SelectedItem.SubItems(4)
        Me.FraModelos.Tag = LvAplicacion.SelectedItem
        
        CboModelos.SetFocus
End Sub

Private Sub LvDocumentos_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    ordListView ColumnHeader, Me, LvDocumentos
End Sub




Private Sub LvFicha_DblClick()
    If LvFicha.SelectedItem Is Nothing Then Exit Sub
    
    TxtFichaCodigo = LvFicha.SelectedItem.SubItems(1)
    TxtFichaDescripcion = LvFicha.SelectedItem.SubItems(2)
    TxtFichaCantidad = LvFicha.SelectedItem.SubItems(3)
    TxtFichaPrecio = LvFicha.SelectedItem.SubItems(4)
    TxtFichaTotal = LvFicha.SelectedItem.SubItems(5)
    
    LvFicha.ListItems.Remove LvFicha.SelectedItem.Index
    
    TxtFichaElTotal = NumFormat(TotalizaColumna(LvFicha, "total"))
    TxtFichaCodigo.SetFocus
End Sub


Private Sub LvModelos_DblClick()
    TxtBuscaModelo = LvModelos.SelectedItem.SubItems(1)
    Busca_Id_Combo CboModelos, Val(LvModelos.SelectedItem)
    LvModelos.Visible = False
    TxtBuscaModelo.Visible = False
    TxtAnoDesde.SetFocus
End Sub

Private Sub LvModelos_KeyDown(KeyCode As Integer, Shift As Integer)
    If LvModelos.SelectedItem Is Nothing Then Exit Sub
    If KeyCode = 13 Then
            TxtBuscaModelo = LvModelos.SelectedItem.SubItems(1)
            Busca_Id_Combo CboModelos, Val(LvModelos.SelectedItem)
            LvModelos.Visible = False
            TxtBuscaModelo.Visible = False
            TxtAnoDesde.SetFocus
    End If
End Sub



Private Sub LvOriginales_DblClick()
    If LvOriginales.SelectedItem Is Nothing Then Exit Sub
    TxtCodigoOriginal = LvOriginales.SelectedItem.SubItems(1)
    LvOriginales.ListItems.Remove LvOriginales.SelectedItem.Index
    
End Sub

Private Sub LvProveedor_DblClick()
    If LvProveedor.SelectedItem Is Nothing Then Exit Sub
    txtProveedor.Tag = LvProveedor.SelectedItem.SubItems(1)
    txtProveedor = LvProveedor.SelectedItem.SubItems(2)
    TxtCodigoProveedor = LvProveedor.SelectedItem.SubItems(3)
    LvProveedor.ListItems.Remove LvProveedor.SelectedItem.Index
End Sub

Private Sub LvVentas_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    ordListView ColumnHeader, Me, LvVentas
End Sub

Private Sub LvVtaMensual_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    ordListView ColumnHeader, Me, Me.LvVtaMensual
End Sub











Private Sub Timer1_Timer()
    On Error GoTo Herror
    If Len(Me.TxtCodigo.Text) > 0 And _
        Me.CboTipoProducto.ListIndex > -1 And _
        Len(Me.TxtDescripcion.Text) > 0 And _
        Val(Me.TxtPrecioCompra.Text) > 0 And _
        Val(Me.TxtPrecioVta.Text) > 0 And _
        Me.TxtStockActual.Text <> "" And _
        CboInventariable.ListIndex > -1 And _
        Me.TxtStockCritico.Text <> "" Then
        Me.CmdGuardar.Enabled = True
    Else
        Me.CmdGuardar.ToolTipText = "Para poder grabar debe completar todos los campos"
        Me.CmdGuardar.Enabled = False
    End If
    For Each TXTsx In Controls
        If (TypeOf TXTsx Is TextBox) Then
            If Me.ActiveControl.Name = TXTsx.Name Then 'Foco activo
                TXTsx.BackColor = IIf(TXTsx.Locked, ClrDesha, ClrCfoco)
            Else
                TXTsx.BackColor = IIf(TXTsx.Locked, ClrDesha, ClrSfoco)
            End If
        End If
    Next
    Exit Sub
    
Herror:
'error
End Sub

Private Sub Timer2_Timer()
    On Error GoTo problema
    If Bm_Nuevo Then
        TxtCodigo.SetFocus
    Else
        CboTipoProducto.SetFocus
    End If
    Timer2.Enabled = False
    Timer1.Enabled = False
    
problema:
    'MsgBox "Aqui esta el problema " & vbNewLine & Err.Description
    Timer2.Enabled = False
End Sub

Private Sub TxtAnoDesde_GotFocus()
    En_Foco TxtAnoDesde
End Sub

Private Sub TxtAnoDesde_KeyPress(KeyAscii As Integer)
    KeyAscii = AceptaSoloNumeros(KeyAscii)
End Sub

Private Sub TxtAnoHasta_GotFocus()
    En_Foco TxtAnoHasta
End Sub

Private Sub TxtAnoHasta_KeyPress(KeyAscii As Integer)
      KeyAscii = AceptaSoloNumeros(KeyAscii)
End Sub

Private Sub TxtAnoHasta_Validate(Cancel As Boolean)
    If Val(TxtAnoHasta) = 0 Then TxtAnoHasta = "0"
End Sub

Private Sub TxtBuscaModelo_Change()
    BuscaModelos
End Sub
Private Sub BuscaModelos()
    Dim Sp_FiltroModelo As String
    If CboMarcas.ListCount = 0 Then Exit Sub
    Sp_FiltroModelo = ""
    If Len(TxtBuscaModelo) = 0 Then
        Sp_FiltroModelo = ""
    Else
        Sp_FiltroModelo = " AND mob_nombre LIKE '%" & TxtBuscaModelo & "%'"
    End If
    LvModelos.Visible = True
    Sql = "SELECT mob_id,mob_nombre " & _
            "FROM par_buscador_modelos " & _
            "WHERE mab_id=" & CboMarcas.ItemData(CboMarcas.ListIndex) & " " & Sp_FiltroModelo
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvModelos, False, True, True, False
   ' TxtBuscaModelo.Visible = True
   ' TxtBuscaModelo.SetFocus
    'TxtBuscaModelo.SetFocus
End Sub

Private Sub TxtBuscaModelo_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyTab Then
        LvModelos.SetFocus
    End If
    
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
    
End Sub

Private Sub TxtCodigo_GotFocus()
    FrameAyuda.Caption = "Codigo"
    skAyuda = "Aqui se ingresa codigo unico, puede ser alfanumerico "
End Sub

Private Sub TxtCodigo_KeyPress(KeyAscii As Integer)
    Dim letra As String    'Aqui controlamos las teclas
    letra = UCase(Chr(KeyAscii)) 'a mayuscula el caractere ingresado
    KeyAscii = Asc(letra) 'recupero el codigo ascci del caractar ya transofrmado
     If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtCodigo_LostFocus()
    skAyuda = Empty
    FrameAyuda.Caption = ""
End Sub

Private Sub TxtCodigo_Validate(Cancel As Boolean)
   ' If AccionProducto <> 2 Then
   '    If Len(TxtCodigo) = 0 Then Exit Sub
   If Not TxtCodigo.Locked And Len(TxtCodigo) > 0 Then
        Sql = "SELECT codigo FROM maestro_productos WHERE  rut_emp='" & SP_Rut_Activo & "' AND codigo='" & TxtCodigo & "'"
        Call Consulta(RsTmp, Sql)
        If RsTmp.RecordCount > 0 Then
            MsgBox "Codigo ya existe ", vbOKOnly + vbInformation
            Exit Sub
        End If
    End If
End Sub

Private Sub TxtCodigoInterno_Validate(Cancel As Boolean)
                If Len(Trim(TxtCodigoInterno)) > 0 Then
                    Sql = "SELECT pro_codigo_interno " & _
                            "FROM maestro_productos " & _
                            "WHERE pro_codigo_interno='" & TxtCodigoInterno & "' AND rut_emp='" & SP_Rut_Activo & "'"
                    Consulta RsTmp, Sql
                    If RsTmp.RecordCount > 0 Then
                        MsgBox "Codigo barra/int ya existe...", vbExclamation
                        TxtCodigoInterno.SetFocus
                    End If
                End If
End Sub

Private Sub TxtCodigoObtAplicacion_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
End Sub

Private Sub TxtCodigoOriginal_GotFocus()
    En_Foco TxtCodigoOriginal
End Sub

Private Sub TxtCodigoOriginal_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub TxtCodigoProveedor_GotFocus()
    En_Foco TxtCodigoProveedor
End Sub

Private Sub TxtCodigoProveedor_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
    If KeyAscii = 13 Then CmdOkProveedor.SetFocus
     If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtDescripcion_GotFocus()
    En_Foco TxtDescripcion
    FrameAyuda.Caption = "Descripci�n"
    skAyuda = "Aqui se ingresa la descripci�n del producto "
End Sub

Private Sub txtDescripcion_KeyPress(KeyAscii As Integer)
    Dim letra As String    'Aqui controlamos las teclas
    letra = UCase(Chr(KeyAscii)) 'a mayuscula el caractere ingresado
    KeyAscii = Asc(letra) 'recupero el codigo ascci del caractar ya transofrmado
    If KeyAscii = 39 Then KeyAscii = 0
End Sub



Private Sub TxtDescripcion_LostFocus()
    skAyuda = Empty
    FrameAyuda.Caption = ""
End Sub

Private Sub TxtDescripcion_Validate(Cancel As Boolean)
    TxtDescripcion = Replace(TxtDescripcion, "'", " ")
    
End Sub


Private Sub TxtFichaCantidad_GotFocus()
    En_Foco TxtFichaCantidad
End Sub


Private Sub TxtFichaCantidad_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
End Sub


Private Sub TxtFichaCantidad_Validate(Cancel As Boolean)
    If Val(TxtFichaCantidad) = 0 Then
    
    '
    Else
        TxtFichaTotal = Val(TxtFichaCantidad) * Val(TxtFichaPrecio)
    End If
    
End Sub


Private Sub TxtFichaCodigo_GotFocus()
    En_Foco TxtFichaCodigo
End Sub

Private Sub TxtFichaCodigo_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
End Sub

Private Sub TxtFichaCodigo_KeyUp(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF1 Then
        SG_codigo = ""
        BuscaProducto.Sm_Filtro_Tipo = " AND pro_tipo='M' "
        BuscaProducto.Show 1
        If Len(SG_codigo) > 0 Then
            TxtFichaCodigo = SG_codigo
        End If
    End If
End Sub

Private Sub TxtFichaCodigo_Validate(Cancel As Boolean)
    If Len(TxtFichaCodigo) = 0 Then Exit Sub
    
    Sql = "SELECT descripcion,precio_compra " & _
            "FROM maestro_productos " & _
            "WHERE codigo=" & TxtFichaCodigo & " AND pro_tipo='M' AND rut_emp='" & SP_Rut_Activo & "'"
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        TxtFichaDescripcion = RsTmp!Descripcion
        TxtFichaPrecio = RsTmp!precio_compra
        
        
    End If
End Sub

Private Sub TxtFlete_Change()
    TxtPrecioCompra = Val(TxtPrecioCostoSFlete) + Val(TxtFlete)
End Sub

Private Sub TxtFlete_GotFocus()
    En_Foco TxtFlete
End Sub

Private Sub TxtFlete_KeyPress(KeyAscii As Integer)
    KeyAscii = AceptaSoloNumeros(KeyAscii)
End Sub

Private Sub TxtFlete_Validate(Cancel As Boolean)
    If Val(TxtFlete) = 0 Then TxtFlete = "0"
End Sub

Private Sub TxtPorcentaje_GotFocus()
    En_Foco TxtPorcentaje
    
End Sub

Private Sub TxtPorcentaje_KeyPress(KeyAscii As Integer)
    KeyAscii = AceptaSoloNumeros(KeyAscii)
End Sub

Private Sub TxtPorcentaje_Validate(Cancel As Boolean)
    If Val(Me.TxtPorcentaje.Text) > 0 Then
        If Val(TxtPrecioCompra.Text) = 0 Then Me.TxtPrecioCompra.Text = 1
        Me.TxtPrecioVta.Text = Round(Val(Me.TxtPcompraCIVA) + (Val(Me.TxtPcompraCIVA) / 100 * Val(Me.TxtPorcentaje.Text)), 0)
        Me.LbMargen.Caption = Val(Me.TxtPrecioVta) - Val(Me.TxtPcompraCIVA)
        TxtPrecioVentaNeto = Round(TxtPrecioVta / Val("1." & DG_IVA), 0)
    Else
        TxtPorcentaje = 0
    End If
End Sub

Private Sub TxtPrecioCompra_GotFocus()
    En_Foco TxtPrecioCompra
End Sub

Private Sub TxtPrecioCompra_KeyPress(KeyAscii As Integer)
    KeyAscii = AceptaSoloNumeros(KeyAscii)
End Sub

Private Sub TxtPrecioCompra_Validate(Cancel As Boolean)
    If Val(TxtPrecioCompra.Text) = 0 Then Me.TxtPrecioCompra.Text = 1
    
    
    
    If Val(Me.TxtPrecioVta.Text) > 1 And _
        Val(Me.TxtPrecioVta) > Val(Me.TxtPrecioCompra) Then
            
        Me.LbMargen.Caption = Val(Me.TxtPrecioVta) - Val(Me.TxtPrecioCompra)
        Me.TxtPorcentaje.Text = Format(Val(Me.LbMargen.Caption) * 100 / Val(Me.TxtPrecioCompra.Text), "###.##")
        Me.TxtPorcentaje.Text = Replace(Me.TxtPorcentaje.Text, ",", ".")
    End If
End Sub

Private Sub TxtPrecioCostoSFlete_Change()
    TxtPrecioCompra = Val(TxtPrecioCostoSFlete) + Val(TxtFlete)
End Sub

Private Sub TxtPrecioCostoSFlete_GotFocus()
    En_Foco TxtPrecioCostoSFlete
End Sub

Private Sub TxtPrecioCostoSFlete_KeyPress(KeyAscii As Integer)
    KeyAscii = AceptaSoloNumeros(KeyAscii)
End Sub

Private Sub TxtPrecioCostoSFlete_Validate(Cancel As Boolean)
        If Val(TxtPrecioCostoSFlete) = 0 Then Me.TxtPrecioCostoSFlete = "0"
        If Val(TxtPrecioCostoSFlete) > 0 Then
            Me.TxtPcompraCIVA = Round(CDbl(TxtPrecioCostoSFlete) * Val("1." & DG_IVA), 0)
        End If
End Sub

Private Sub TxtPrecioVta_GotFocus()
    En_Foco TxtPrecioVta
End Sub

Private Sub TxtPrecioVta_KeyPress(KeyAscii As Integer)
    KeyAscii = AceptaSoloNumeros(KeyAscii)
End Sub

Private Sub TxtPrecioVta_Validate(Cancel As Boolean)
    If Val(TxtPrecioCompra.Text) = 0 Then Me.TxtPrecioCompra.Text = 1
    Me.LbMargen.Caption = Val(Me.TxtPrecioVta) - Val(Me.TxtPcompraCIVA)
    Me.TxtPorcentaje.Text = Round(Val(Me.LbMargen.Caption) * 100 / Val(Me.TxtPcompraCIVA.Text), 0)
    Me.TxtPorcentaje.Text = Replace(Me.TxtPorcentaje.Text, ",", ".")
    TxtPrecioVentaNeto = Round(CDbl(TxtPrecioVta) / Val("1." & DG_IVA), 0)
End Sub

Private Sub TxtStockActual_GotFocus()
    En_Foco TxtStockActual
End Sub

Private Sub TxtStockActual_KeyPress(KeyAscii As Integer)
    KeyAscii = AceptaSoloNumeros(KeyAscii)
End Sub

Private Sub TxtStockCritico_GotFocus()
    En_Foco TxtStockCritico
End Sub

Private Sub TxtStockCritico_KeyPress(KeyAscii As Integer)
    KeyAscii = AceptaSoloNumeros(KeyAscii)
End Sub

Private Sub txtUbicacion_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase$(Chr(KeyAscii)))
End Sub
Private Sub TxtStockMinimo_GotFocus()
       En_Foco TxtStockMinimo
End Sub

Private Sub TxtStockMinimo_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
End Sub

Private Sub TxtStockMinimo_Validate(Cancel As Boolean)
    If Val(TxtStockMinimo) = 0 Then TxtStockMinimo = 0
End Sub

Private Sub txtUbicacionBodega_GotFocus()
    En_Foco txtUbicacionBodega
    FrameAyuda.Caption = "UBICACION BODEGA :"
    skAyuda = " Se refiere al lugar o estante f�sico donde se encuentra el producto"
End Sub

Private Sub txtUbicacionBodega_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
    If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub txtUbicacionBodega_LostFocus()
    skAyuda.Caption = Empty
    FrameAyuda = ""
End Sub

Private Sub txtUbicacionBodega_Validate(Cancel As Boolean)
txtUbicacionBodega = Replace(txtUbicacionBodega, "'", " ")
End Sub
Function ComprobarTabla(Tabla As String) As Integer
    Dim Sp_Sql As String
    Sp_Sql = "SELECT COUNT(*) AS cuantos " & _
                "FROM information_schema.tables " & _
                "WHERE /*table_schema = '" & SG_BaseDato & "'   " & _
                "AND */ table_name = '" & Tabla & "'"
    Consulta RsTmp, Sp_Sql
    If RsTmp.RecordCount > 0 Then
        If RsTmp!cuantos = 0 Then
            MsgBox "Falta tabla:" & Tabla
            ComprobarTabla = 0
        Else
            ComprobarTabla = 1
        End If
    End If
    
End Function


