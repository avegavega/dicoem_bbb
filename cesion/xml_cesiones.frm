VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{DA729E34-689F-49EA-A856-B57046630B73}#1.0#0"; "progressbar-xp.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form xml_cesiones 
   Caption         =   "Acuse de Recibo / XML / Cesiones"
   ClientHeight    =   10110
   ClientLeft      =   2115
   ClientTop       =   795
   ClientWidth     =   15960
   LinkTopic       =   "Form1"
   ScaleHeight     =   10110
   ScaleWidth      =   15960
   Begin VB.Frame FraProgreso 
      Caption         =   "Recopilando  y comparando informacion"
      Height          =   795
      Left            =   2565
      TabIndex        =   30
      Top             =   6660
      Visible         =   0   'False
      Width           =   11430
      Begin Proyecto2.XP_ProgressBar BarraProgreso 
         Height          =   330
         Left            =   150
         TabIndex        =   31
         Top             =   315
         Width           =   11130
         _ExtentX        =   19632
         _ExtentY        =   582
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BrushStyle      =   0
         Color           =   16777088
         Scrolling       =   1
         ShowText        =   -1  'True
      End
   End
   Begin VB.Frame FraProcesandoDTE 
      Height          =   1080
      Left            =   1605
      TabIndex        =   18
      Top             =   4230
      Visible         =   0   'False
      Width           =   13020
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel13 
         Height          =   510
         Left            =   2130
         OleObjectBlob   =   "xml_cesiones.frx":0000
         TabIndex        =   19
         Top             =   450
         Width           =   10275
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Tareas"
      Height          =   9660
      Index           =   1
      Left            =   285
      TabIndex        =   0
      Top             =   180
      Width           =   15285
      Begin TabDlg.SSTab SSTab1 
         Height          =   8940
         Left            =   315
         TabIndex        =   1
         Top             =   450
         Width           =   14700
         _ExtentX        =   25929
         _ExtentY        =   15769
         _Version        =   393216
         TabHeight       =   520
         TabCaption(0)   =   "Acuse de recibos"
         TabPicture(0)   =   "xml_cesiones.frx":00C8
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "CmdBuscaCompra"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "Frame4(0)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "frmOts"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).ControlCount=   3
         TabCaption(1)   =   "Obtener  XML Firmado"
         TabPicture(1)   =   "xml_cesiones.frx":00E4
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "FrmDocFirmado(0)"
         Tab(1).ControlCount=   1
         TabCaption(2)   =   "Cesion electronica"
         TabPicture(2)   =   "xml_cesiones.frx":0100
         Tab(2).ControlEnabled=   0   'False
         Tab(2).ControlCount=   0
         Begin VB.Frame frmOts 
            Caption         =   "Listado historico"
            Height          =   6210
            Left            =   555
            TabIndex        =   28
            Top             =   2325
            Width           =   13380
            Begin MSComctlLib.ListView LvDetalle 
               Height          =   5715
               Left            =   135
               TabIndex        =   29
               Top             =   285
               Width           =   13020
               _ExtentX        =   22966
               _ExtentY        =   10081
               View            =   3
               LabelWrap       =   0   'False
               HideSelection   =   0   'False
               FullRowSelect   =   -1  'True
               GridLines       =   -1  'True
               _Version        =   393217
               ForeColor       =   -2147483640
               BackColor       =   -2147483643
               BorderStyle     =   1
               Appearance      =   1
               NumItems        =   12
               BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                  Object.Tag             =   "N109"
                  Text            =   "Id Unico"
                  Object.Width           =   0
               EndProperty
               BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                  SubItemIndex    =   1
                  Object.Tag             =   "T1200"
                  Text            =   "Folio"
                  Object.Width           =   2117
               EndProperty
               BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                  SubItemIndex    =   2
                  Object.Tag             =   "F1000"
                  Text            =   "Fecha"
                  Object.Width           =   1940
               EndProperty
               BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                  SubItemIndex    =   3
                  Object.Tag             =   "T3000"
                  Text            =   "Nombre Proveedor"
                  Object.Width           =   5292
               EndProperty
               BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                  SubItemIndex    =   4
                  Object.Tag             =   "T2500"
                  Text            =   "Tipo Documento"
                  Object.Width           =   4939
               EndProperty
               BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                  Alignment       =   1
                  SubItemIndex    =   5
                  Object.Tag             =   "T800"
                  Text            =   "Nro Doc"
                  Object.Width           =   1940
               EndProperty
               BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                  SubItemIndex    =   6
                  Object.Tag             =   "T1500"
                  Text            =   "Movimiento"
                  Object.Width           =   3528
               EndProperty
               BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                  Alignment       =   1
                  SubItemIndex    =   7
                  Key             =   "total"
                  Object.Tag             =   "N100"
                  Text            =   "Total"
                  Object.Width           =   2646
               EndProperty
               BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                  SubItemIndex    =   8
                  Object.Tag             =   "N109"
                  Text            =   "Doc_id"
                  Object.Width           =   0
               EndProperty
               BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                  SubItemIndex    =   9
                  Object.Tag             =   "T1000"
                  Text            =   "rut_prvoeedodr"
                  Object.Width           =   0
               EndProperty
               BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                  Alignment       =   1
                  SubItemIndex    =   10
                  Key             =   "retencion"
                  Object.Tag             =   "N100"
                  Text            =   "Impuesto"
                  Object.Width           =   2540
               EndProperty
               BeginProperty ColumnHeader(12) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                  Alignment       =   1
                  SubItemIndex    =   11
                  Key             =   "totalb"
                  Object.Tag             =   "N100"
                  Text            =   "Total"
                  Object.Width           =   2540
               EndProperty
            End
         End
         Begin VB.Frame Frame4 
            Caption         =   "Mes y A�o"
            Height          =   1395
            Index           =   0
            Left            =   825
            TabIndex        =   20
            Top             =   630
            Width           =   2970
            Begin VB.CheckBox ChkFecha 
               Height          =   225
               Left            =   210
               TabIndex        =   23
               Top             =   1035
               Width           =   225
            End
            Begin VB.ComboBox ComAno 
               Height          =   315
               Left            =   1710
               Style           =   2  'Dropdown List
               TabIndex        =   22
               Top             =   435
               Width           =   1215
            End
            Begin VB.ComboBox comMes 
               Height          =   315
               ItemData        =   "xml_cesiones.frx":011C
               Left            =   135
               List            =   "xml_cesiones.frx":011E
               Style           =   2  'Dropdown List
               TabIndex        =   21
               Top             =   450
               Width           =   1575
            End
            Begin MSComCtl2.DTPicker DtDesde 
               CausesValidation=   0   'False
               Height          =   285
               Left            =   480
               TabIndex        =   24
               Top             =   1005
               Width           =   1215
               _ExtentX        =   2143
               _ExtentY        =   503
               _Version        =   393216
               Enabled         =   0   'False
               Format          =   96141313
               CurrentDate     =   41001
            End
            Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
               Height          =   255
               Left            =   120
               OleObjectBlob   =   "xml_cesiones.frx":0120
               TabIndex        =   25
               Top             =   255
               Width           =   375
            End
            Begin ACTIVESKINLibCtl.SkinLabel SkinLabel5 
               Height          =   255
               Left            =   1725
               OleObjectBlob   =   "xml_cesiones.frx":0184
               TabIndex        =   26
               Top             =   255
               Width           =   375
            End
            Begin MSComCtl2.DTPicker DtHasta 
               Height          =   285
               Left            =   1680
               TabIndex        =   27
               Top             =   1005
               Width           =   1200
               _ExtentX        =   2117
               _ExtentY        =   503
               _Version        =   393216
               Enabled         =   0   'False
               Format          =   96141313
               CurrentDate     =   41001
            End
         End
         Begin VB.Frame FrmDocFirmado 
            Caption         =   "Documento de Venta"
            Height          =   4470
            Index           =   0
            Left            =   -70740
            TabIndex        =   3
            Top             =   2025
            Width           =   7605
            Begin VB.ComboBox CboDocVenta 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   12
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   420
               Left            =   1695
               Style           =   2  'Dropdown List
               TabIndex        =   16
               Top             =   1095
               Width           =   5145
            End
            Begin VB.CommandButton CmdConsultaDEV 
               Caption         =   "Consultar"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   12
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   570
               Left            =   2325
               TabIndex        =   14
               Top             =   3630
               Width           =   2505
            End
            Begin VB.TextBox TxtNroDocVenta 
               Alignment       =   1  'Right Justify
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   12
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   360
               Left            =   885
               TabIndex        =   13
               Text            =   "0"
               Top             =   2100
               Width           =   1650
            End
            Begin VB.Frame FraUbicacion 
               Caption         =   "Ubicaci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   12
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   810
               Index           =   0
               Left            =   4365
               TabIndex        =   10
               Top             =   1725
               Width           =   2685
               Begin VB.OptionButton OptLocal 
                  Caption         =   "Local"
                  Enabled         =   0   'False
                  Height          =   270
                  Left            =   45
                  TabIndex        =   12
                  Top             =   315
                  Width           =   1035
               End
               Begin VB.OptionButton OptNube 
                  Caption         =   "Nube"
                  Height          =   270
                  Left            =   1680
                  TabIndex        =   11
                  Top             =   315
                  Value           =   -1  'True
                  Width           =   855
               End
            End
            Begin VB.Frame FrmCopia 
               Caption         =   "Copia"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   12
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   720
               Index           =   2
               Left            =   240
               TabIndex        =   7
               Top             =   2760
               Width           =   2535
               Begin VB.OptionButton Option3 
                  Caption         =   "Original"
                  Enabled         =   0   'False
                  Height          =   195
                  Left            =   405
                  TabIndex        =   9
                  Top             =   330
                  Value           =   -1  'True
                  Width           =   915
               End
               Begin VB.OptionButton Option4 
                  Caption         =   "Cedible"
                  Enabled         =   0   'False
                  Height          =   360
                  Left            =   1530
                  TabIndex        =   8
                  Top             =   270
                  Width           =   945
               End
            End
            Begin VB.Frame FraAmbiente 
               Caption         =   "Ambiente"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   12
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   720
               Index           =   1
               Left            =   4305
               TabIndex        =   4
               Top             =   2745
               Width           =   2535
               Begin VB.OptionButton Option5 
                  Caption         =   "Certf"
                  Enabled         =   0   'False
                  Height          =   360
                  Left            =   1485
                  TabIndex        =   6
                  Top             =   345
                  Width           =   945
               End
               Begin VB.OptionButton Option6 
                  Caption         =   "Producti"
                  Height          =   195
                  Left            =   180
                  TabIndex        =   5
                  Top             =   420
                  Value           =   -1  'True
                  Width           =   1080
               End
            End
            Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
               Height          =   405
               Index           =   0
               Left            =   1845
               OleObjectBlob   =   "xml_cesiones.frx":01E8
               TabIndex        =   15
               Top             =   720
               Width           =   1935
            End
            Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
               Height          =   285
               Index           =   1
               Left            =   870
               OleObjectBlob   =   "xml_cesiones.frx":0258
               TabIndex        =   17
               Top             =   1800
               Width           =   1935
            End
         End
         Begin VB.CommandButton CmdBuscaCompra 
            Caption         =   "Buscar Compras"
            Height          =   720
            Left            =   4290
            TabIndex        =   2
            Top             =   900
            Width           =   2205
         End
      End
   End
   Begin VB.Timer Timer1 
      Left            =   135
      Top             =   1485
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   510
      OleObjectBlob   =   "xml_cesiones.frx":02C0
      Top             =   600
   End
End
Attribute VB_Name = "xml_cesiones"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
 Option Explicit

      Private Declare Function ShellExecute Lib "shell32.dll" Alias _
      "ShellExecuteA" (ByVal hWnd As Long, ByVal lpszOp As _
      String, ByVal lpszFile As String, ByVal lpszParams As String, _
      ByVal lpszDir As String, ByVal FsShowCmd As Long) As Long
      
      Dim i As Integer
      Dim Sp_Archivo2 As String

Private Sub ChkFecha_Click()
    On Error Resume Next
    If ChkFecha.Value = 1 Then
        comMes.Enabled = False
        ComAno.Enabled = False
        DtDesde.Enabled = True
        DtHasta.Enabled = True
        DtDesde.SetFocus
    Else
        comMes.SetFocus '
        DtDesde.Enabled = False
        DtHasta.Enabled = False
        comMes.Enabled = True
        ComAno.Enabled = True
    End If
End Sub

Private Sub CmdBuscaCompra_Click()
Dim Bp_Cedible As Boolean
    Dim Sp_TipoImpreso As String
    Dim Bp_Encontrado As Boolean
    Bp_Encontrado = True
    Dim Sp_Fila As String
    Dim pss As Long
    Dim psi As Long
    Dim Sp_File As String
    Dim Sp_campo As String
    Dim Ic As Integer
    Dim pr As Integer
    Dim Sp_Condicion As String
    'Modulo: Obtener compras
    'fecha : 14 sep 2016
    
    Dim resp As Boolean
    
    
    If ChkFecha.Value = 1 Then
        Sp_Condicion = Sp_Condicion & " AND v.fecha BETWEEN '" & Fql(DtDesde.Value) & "' AND '" & Fql(DtHasta.Value) & "' "
    Else
        Sp_Condicion = Sp_Condicion & " AND MONTH(v.fecha)=" & comMes.ItemData(comMes.ListIndex) & " AND YEAR(v.fecha)=" & IIf(ComAno.ListCount = 0, Year(Date), ComAno.Text)
    End If
    
        FraProcesandoDTE.Visible = True
        DoEvents
        'Consulta electronica
        Dim obj As DTECloud.Integracion
        Set obj = New DTECloud.Integracion
        Dim X As Integer
'
        obj.UrlServicioFacturacion = SG_Url_Factura_Electronica ' "http://wsdte.dyndns.biz/WSDTE/Service.asmx?WSDL"
        obj.HabilitarDescarga = True
        obj.CarpetaDestino = "C:\FACTURAELECTRONICA\PDF\" & Replace(SP_Rut_Activo, ".", "")
      
        obj.Password = "1234"
       
        On Error GoTo ErrorConexion
        
        'Dim xml As String = String.Empty
       ' If obj.ConsultaEmitidosPlus(Replace(SP_Rut_Activo, ".", ""), Fql(DtDesde), Fql(DtHasta), True, 5) Then
        'Sp_Archivo2 = obj.ConsultaRecepcionadosF_XML(Replace(SP_Rut_Activo, ".", ""), Fql(DtDesde), Fql(DtHasta), True)
        If ChkFecha.Value = 1 Then
            Sp_Archivo2 = obj.ConsultaRecepcionadosF1(Replace(SP_Rut_Activo, ".", ""), Fql(DtDesde), Fql(DtHasta), True)
        Else
            Sp_Archivo2 = obj.ConsultaRecepcionadosF1(Replace(SP_Rut_Activo, ".", ""), Fql(ComAno.Text & "-" & comMes & "-01"), Fql(UltimoDiaMes("01-" & comMes.Text & "-" & ComAno.Text)), True)
        End If
       ' Sp_Archivo2 = obj.ConsultaRecepcionadosF2(Replace(SP_Rut_Activo, ".", ""), Fql(DtDesde), Fql(DtHasta), True, 10)
            Sp_Archivo2 = obj.StringRecepcionados
            pss = 1
            If Len(Sp_Archivo2) > 0 Then
                X = FreeFile
                Sp_File = "C:\FACTURAELECTRONICA\obtcompra.xml"
                Open Sp_File For Output As X
                psi = 1
                    Do While Bp_Encontrado
                        
                        pss = InStr(pss, Sp_Archivo2, "|", vbTextCompare)
                        If pss > 0 Then
                            Sp_Fila = Mid(Sp_Archivo2, psi, (pss - psi))
                            Print #X, Sp_Fila
                            psi = pss + 1
                            pss = psi
                        Else
                            Bp_Encontrado = False
                        End If
                    Loop
                        
                
                Close #X
                LvDetalle.ListItems.Clear
                X = FreeFile
                Open Sp_File For Input As X
                    Do While Not EOF(X)
                        Line Input #X, Sp_Fila
                        If Len(Sp_Fila) > 0 Then
                            Bp_Encontrado = True
                            psi = 1
                            pss = 1
                            LvDetalle.ListItems.Add , , ""
                            Ic = 1
                            Do While Bp_Encontrado
                                pss = InStr(pss, Sp_Fila, ";", vbTextCompare)
                                If pss > 0 Then
                                    Sp_campo = Mid(Sp_Fila, psi, pss - psi)
                                    Select Case Ic
                                        Case 1
                                            pr = 5 'folio
                                        Case 2
                                            pr = 8 'doc_id_sii
                                        Case 3
                                            pr = 9 'fut
                                        Case 4
                                            pr = 2 'fecha
                                        Case 5
                                            pr = 7 'total
                                        Case 6
                                            pr = 10 '
                                        Case 7
                                            pr = 10
                                        Case 8
                                            pr = 10 '
                                        Case 9
                                            pr = 10
                                        Case 10
                                            pr = 10 '
                                        Case 11
                                            pr = 10
                                    End Select
                                    LvDetalle.ListItems(LvDetalle.ListItems.Count).SubItems(pr) = Sp_campo
                                    psi = pss + 1
                                    pss = psi
                                    Ic = Ic + 1
                                Else
                                    Bp_Encontrado = False
                                End If
                            
                            Loop
                        End If
                    
                    
                    Loop
                Close #X
                
                Me.FraProgreso.Visible = True
                DoEvents
                For i = 1 To LvDetalle.ListItems.Count
                   
                    Respuesta = VerificaRut(LvDetalle.ListItems(i).SubItems(9), NuevoRut)
                    Sql = "SELECT (SELECT nombre_empresa FROM maestro_proveedores WHERE rut_proveedor='" & NuevoRut & "' LIMIT 1) nombre," & _
                                    "(SELECT doc_nombre FROM sis_documentos WHERE doc_cod_sii=" & LvDetalle.ListItems(i).SubItems(8) & " LIMIT 1) documento "
                    Consulta RsTmp, Sql
                    If RsTmp.RecordCount > 0 Then
                        LvDetalle.ListItems(i).SubItems(3) = "" & RsTmp!nombre
                        LvDetalle.ListItems(i).SubItems(4) = "" & RsTmp!Documento
                    
                    End If
                
                
                Next
                
                
                FraProgreso.Visible = False
                
               

            
            End If
        'End If
        
       ' MsgBox Sp_Archivo2
    
         FraProcesandoDTE.Visible = False
    Exit Sub

ErrorConexion:
    MsgBox "Error:" & Err.Description & vbNewLine & "Nro:" & Err.Number & " " & Err.Source
    
End Sub





Private Sub CmdConsultaDEV_Click()
    Dim Bp_Cedible As Boolean
    Dim Sp_TipoImpreso As String
    Dim Sp_Archivo2 As String
    Bp_Cedible = False
    Dim Bp_ambiente As Boolean
    
    If Option6 Then Bp_ambiente = True Else Bp_ambiente = False
    Sp_TipoImpreso = 6
    'Modulo: ver pdfs, ya sea local o en la nube
    'fecha : 11 octubre 2014
    Dim resp As Boolean
    If CboDocVenta.ListIndex = -1 Then
        MsgBox "Seleccione documento de venta...", vbInformation
        CboDocVenta.SetFocus
        Exit Sub
    End If
    
    If Val(Me.TxtNroDocVenta) = 0 Then
        MsgBox "Ingrese Folio...", vbInformation
        TxtNroDocVenta.SetFocus
        Exit Sub
    End If
        FraProcesandoDTE.Visible = True
        DoEvents
    If Option4 Then Bp_Cedible = True
    
        'Consulta electronica
        Dim obj As DTECloud.Integracion
        Set obj = New DTECloud.Integracion
     
        obj.UrlServicioFacturacion = SG_Url_Factura_Electronica ' "http://wsdte.dyndns.biz/WSDTE/Service.asmx?WSDL"
        obj.HabilitarDescarga = True
        obj.CarpetaDestino = "C:\FACTURAELECTRONICA\PDF\" & Replace(SP_Rut_Activo, ".", "")
      
        obj.Password = "1234"
        obj.Productivo = Bp_ambiente
         Sp_TipoImpreso = "1"
        '////////////////////////////
        If SG_LlevaIVAAnticipado = "SI" Then
            Sp_TipoImpreso = 6
            'obj.TipoImpresionRDL = "6"
        End If
        obj.TipoImpresionRDL = Sp_TipoImpreso
        'resp = obj.DescargarPDF(rutempresa.Text, rutempresa.Text, Combo1.Text, folio.Text, "E", False, False)
        On Error GoTo ErrorConexion
        resp = obj.DescargarPDF(Replace(SP_Rut_Activo, ".", ""), Replace(SP_Rut_Activo, ".", ""), Trim(CboDocVenta.ItemData(CboDocVenta.ListIndex)), Trim(Me.TxtNroDocVenta), "E", Bp_Cedible, Bp_ambiente)
     
        Archivo = obj.URLPDF
        If Len(obj.URLXMLFirmado) > 0 Then
            ShellExecute Me.hWnd, "open", obj.URLXMLFirmado, "", "", 4
        End If
     
        
        If Len(Archivo) = 0 Then
            MsgBox "No se encotr� el documento...", vbInformation
        End If
        FraProcesandoDTE.Visible = False
    '    VerDteLocal
        'End If
 
   
    Exit Sub

ErrorConexion:
    MsgBox "Error:" & Err.Description & vbNewLine & "Nro:" & Err.Number & " " & Err.Source
    
    

End Sub

Private Sub Form_Load()
    Skin2 Me, , 2
    DtDesde = Date
    DtHasta = Date
    For i = 1 To 12
        comMes.AddItem UCase(MonthName(i, False))
        comMes.ItemData(comMes.ListCount - 1) = i
    Next
    Busca_Id_Combo comMes, Val(IG_Mes_Contable)
    LLenaYears ComAno, 2010
     LLenarCombo CboDocVenta, "doc_nombre", "doc_cod_sii", "sis_documentos", "doc_activo='SI' AND doc_dte='SI' AND /*doc_documento='VENTA' AND*/ doc_honorarios='NO'"
    CboDocVenta.ListIndex = 0
End Sub

Private Sub Timer1_Timer()
    On Error Resume Next
    comMes.SetFocus
    Timer1.Enabled = False
End Sub

Private Sub TxtNroDocVenta_GotFocus()
    En_Foco TxtNroDocVenta
End Sub

Private Sub TxtNroDocVenta_KeyPress(KeyAscii As Integer)
    KeyAscii = AceptaSoloNumeros(KeyAscii)
End Sub
