VERSION 5.00
Object = "{0ECD9B60-23AA-11D0-B351-00A0C9055D8E}#6.0#0"; "MSHFLXGD.OCX"
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Begin VB.Form HistoricoBoletas 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Registro de Boletas de Venta Directa"
   ClientHeight    =   7920
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   11280
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7920
   ScaleWidth      =   11280
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton CmdSalir 
      Caption         =   "&Salir"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   9600
      TabIndex        =   12
      Top             =   7200
      Width           =   1215
   End
   Begin VB.CommandButton CmdSeleccionar 
      Caption         =   "Seleccionar"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   120
      TabIndex        =   11
      Top             =   7080
      Width           =   1935
   End
   Begin VB.Frame Frame2 
      Caption         =   "Ordenar Por"
      Height          =   1215
      Left            =   7680
      TabIndex        =   7
      Top             =   600
      Width           =   3255
      Begin VB.ComboBox CboDireccion 
         Height          =   315
         ItemData        =   "HistoricoBoletas.frx":0000
         Left            =   1320
         List            =   "HistoricoBoletas.frx":000A
         Style           =   2  'Dropdown List
         TabIndex        =   9
         Top             =   720
         Width           =   1695
      End
      Begin VB.ComboBox CboOrden 
         Height          =   315
         Left            =   480
         Style           =   2  'Dropdown List
         TabIndex        =   8
         Top             =   240
         Width           =   2415
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   255
         Left            =   240
         OleObjectBlob   =   "HistoricoBoletas.frx":0027
         TabIndex        =   10
         Top             =   720
         Width           =   855
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Filtro"
      Height          =   1215
      Left            =   0
      TabIndex        =   1
      Top             =   600
      Width           =   6855
      Begin VB.CommandButton CmdTodos 
         Caption         =   "Todos"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   5640
         TabIndex        =   5
         Top             =   480
         Width           =   1095
      End
      Begin VB.CommandButton CmdBusca 
         Caption         =   "Buscar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   4320
         TabIndex        =   4
         Top             =   480
         Width           =   1095
      End
      Begin VB.OptionButton OpCliente 
         Caption         =   "N� Boleta"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   600
         TabIndex        =   3
         Top             =   840
         Value           =   -1  'True
         Width           =   1695
      End
      Begin VB.TextBox TxtBusqueda 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   600
         TabIndex        =   2
         Top             =   380
         Width           =   3615
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   255
         Left            =   720
         OleObjectBlob   =   "HistoricoBoletas.frx":0097
         TabIndex        =   6
         Top             =   120
         Width           =   2295
      End
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
      Height          =   375
      Left            =   720
      OleObjectBlob   =   "HistoricoBoletas.frx":00FA
      TabIndex        =   0
      Top             =   0
      Width           =   8415
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
      Height          =   255
      Left            =   2280
      OleObjectBlob   =   "HistoricoBoletas.frx":017E
      TabIndex        =   13
      Top             =   7200
      Width           =   6615
   End
   Begin MSHierarchicalFlexGridLib.MSHFlexGrid MshOt 
      Height          =   5055
      Left            =   0
      TabIndex        =   14
      Top             =   1920
      Width           =   10815
      _ExtentX        =   19076
      _ExtentY        =   8916
      _Version        =   393216
      FixedCols       =   0
      HighLight       =   2
      SelectionMode   =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _NumberOfBands  =   1
      _Band(0).Cols   =   2
   End
End
Attribute VB_Name = "HistoricoBoletas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim AdoOrden As ADODB.Recordset

Private Sub CboDireccion_Click()
    If CboOrden.ListIndex = -1 Then Exit Sub
    MshOt.Col = CboOrden.ListIndex
    MshOt.Sort = Me.CboDireccion.ListIndex + 1
End Sub

Private Sub CboOrden_Click()
   ' If Me.CboOrden.ListIndex = 1 Then
   '     paso = FiltraRSFecha("x")
   ' Else
    
        
        MshOt.Col = CboOrden.ListIndex
        MshOt.Sort = Me.CboDireccion.ListIndex + 1
   ' End If
End Sub
Private Sub CmdBusca_Click()
        
    If Len(Me.TxtBusqueda.Text) = 0 Then Exit Sub
    
    paso = FiltraRS(Me.TxtBusqueda.Text)
    
    Exit Sub
'    Filtro = "No_Boleta = " & Val(Me.TxtBusqueda.Text)
    'Filtro = IIf(Me.OpCliente.Value = True, "Nombre_Cliente", "Patente") & " LIKE '" & Me.TxtBusqueda.Text & "*'"
    AdoOrden.Filter = 0
    AdoOrden.Filter = Filtro
    
        
                          
                
            
            
    
End Sub

Private Sub CmdSalir_Click()
    Unload Me
End Sub

Private Sub CmdSeleccionar_Click()
    MshOt_DblClick
End Sub



Private Sub CmdTodos_Click()
        paso = CargarOTS("No_Boleta")
End Sub

Private Sub Form_Load()
    Aplicar_skin Me
    Me.CboDireccion.ListIndex = 0
    paso = CargarOTS("No_Boleta")
        
    MshOt.Row = 0
    For i = 0 To Me.MshOt.Cols - 1
        MshOt.Col = i
        Me.CboOrden.AddItem MshOt.Text
    Next
End Sub



Private Sub MshOt_DblClick()
    
    With MshOt
        If .Row = 0 Then Exit Sub
        .Col = 0
        Nfac = Val(.Text)
        Nbol = Val(.Text)
        EstadoOT = "VistaVD"
    End With
    
    VDdoc = "Boleta"
    EstadoOT = "VistaVD"
    
    
    OrdenTrabajo.Caption = "Boleta emitida N�" & Nfac
  
    OrdenTrabajo.Show 1
                
    paso = CargarOTS("No_Boleta") 'Renueva la lista
    
End Sub


Private Sub TxtBusqueda_KeyPress(KeyAscii As Integer)
    KeyAscii = AceptaSoloNumeros(KeyAscii)
    'KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Function CargarOTS(Campo)
            Dim cn As ADODB.Connection 'variable para la base de datos y el recordset
            
            Connection_String = "PROVIDER=MSDASQL;dsn=citroen;uid=;pwd=;" '   ' le agrega el path de la base de datos al Connectionstring
            'Sql = "select  From OT Where No_Orden = " & NumeroOrden
            Sql = "Select * from Boleta Where Tipo_Movimiento = 'VD' Order by " & Campo & " desc"
            Set cn = New ADODB.Connection '  ' Nuevo objeto Connection
            cn.CursorLocation = adUseClient
            cn.Open Connection_String '  'Abre la conexi�n
            Set AdoOrden = New ADODB.Recordset '  'Nuevo recordset
            AdoOrden.Open Sql, cn, adOpenStatic, adLockOptimistic '  ' abre el recordset
            
            With MshOt
                .FixedCols = 0
                Set .DataSource = AdoOrden
                .Col = 0
                For i = 1 To .Rows - 1
                    .Row = i
                    .CellFontBold = True
                    .CellBackColor = &HFFFF80
                    .CellFontSize = .CellFontSize + 2
                Next
                Dim ElNeto As Double
                For i = 1 To .Rows - 1
                    .Col = 1
                    .Row = i
                    .Text = Format(.Text, "DD/MM/YYYY")
                    .Col = 5
                    
                    ElNeto = Val(.Text)
                    .Text = Format(Replace(.Text, ".", ""), "###,##")
                    
                    .Col = 6
                    .Text = Format(Round(AgregarIVA(ElNeto, 19), 0), "###,##")
                  
                    
                Next
                .Row = 0
                .Col = 6
                .Text = "Precio Total"
                .FixedCols = 1
                .ColWidth(2) = 1000
                .ColWidth(3) = 1000
                .ColWidth(4) = 1000
                .ColWidth(5) = 1500
                .ColAlignment(6) = 6
            End With
End Function
Function FiltraRS(Nboletas As Double)
           Dim cn As ADODB.Connection 'variable para la base de datos y el recordset
            
            Connection_String = "PROVIDER=MSDASQL;dsn=citroen;uid=;pwd=;" '   ' le agrega el path de la base de datos al Connectionstring
            'Sql = "select  From OT Where No_Orden = " & NumeroOrden
            Sql = "Select * from Boleta Where Tipo_Movimiento = 'VD' and No_Boleta = " & Nboletas
            Set cn = New ADODB.Connection '  ' Nuevo objeto Connection
            cn.CursorLocation = adUseClient
            cn.Open Connection_String '  'Abre la conexi�n
            Set AdoOrden = New ADODB.Recordset '  'Nuevo recordset
            AdoOrden.Open Sql, cn, adOpenStatic, adLockOptimistic '  ' abre el recordset
            
            With MshOt
                .FixedCols = 0
                Set .DataSource = AdoOrden
                .Col = 0
                For i = 1 To .Rows - 1
                    .Row = i
                    .CellFontBold = True
                    .CellBackColor = &HFFFF80
                    .CellFontSize = .CellFontSize + 2
                Next
                
                For i = 1 To .Rows - 1
                    .Col = 1
                    .Row = i
                    .Text = Format(.Text, "DD/MM/YYYY")
                    .Col = 5
                    
                    masiva = .Text
                    .Text = Format(masiva, "###,##")
                    .Col = 6
                    .Text = Format(Str(AgregarIVA(Val(masiva), 19)), "###,##")
                Next
                .Row = 0
                .Col = 6
                .Text = "Precio Total"
                .FixedCols = 1
                .ColWidth(2) = 0
                .ColWidth(3) = 0
                .ColWidth(4) = 1
                .ColWidth(5) = 1500
                .ColAlignment(6) = 6
            End With
End Function

Function FiltraRSFecha(Nombre As String)
            Dim cn As ADODB.Connection 'variable para la base de datos y el recordset
            
            Connection_String = "PROVIDER=MSDASQL;dsn=citroen;uid=;pwd=;" '   ' le agrega el path de la base de datos al Connectionstring
            'Sql = "select  From OT Where No_Orden = " & NumeroOrden
            Sql = "Select * from Boleta Where Tipo_Movimiento = 'VD' Order by Fecha desc"
            Set cn = New ADODB.Connection '  ' Nuevo objeto Connection
            cn.CursorLocation = adUseClient
            cn.Open Connection_String '  'Abre la conexi�n
            Set AdoOrden = New ADODB.Recordset '  'Nuevo recordset
            AdoOrden.Open Sql, cn, adOpenStatic, adLockOptimistic '  ' abre el recordset
            MsgBox "ahlo"
            With MshOt
                .ClearStructure
                MsgBox "reacinedo"
                .FixedCols = 0
                Set .DataSource = AdoOrden
                .Col = 0
                For i = 1 To .Rows - 1
                    .Row = i
                    .CellFontBold = True
                    .CellBackColor = &HFFFF80
                    .CellFontSize = .CellFontSize + 2
                Next
                MsgBox "hacineod mas"
                .FixedCols = 1
                .ColWidth(2) = 0
                .ColWidth(3) = 0
                .ColWidth(4) = 1
                .ColWidth(5) = 1500
               
            End With
End Function


