VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form BuscaProducto 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Buscar Producto"
   ClientHeight    =   6690
   ClientLeft      =   4005
   ClientTop       =   1860
   ClientWidth     =   13425
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6690
   ScaleWidth      =   13425
   Begin VB.Timer Timer1 
      Interval        =   30
      Left            =   225
      Top             =   5670
   End
   Begin VB.CommandButton CmdSalir 
      Cancel          =   -1  'True
      Caption         =   "&Retornar"
      Height          =   375
      Left            =   12165
      TabIndex        =   6
      Top             =   6225
      Width           =   1095
   End
   Begin VB.Frame Frame2 
      Caption         =   "Buscando Producto"
      Height          =   5895
      Left            =   240
      TabIndex        =   0
      Top             =   240
      Width           =   13065
      Begin VB.ComboBox CboTipo 
         Height          =   315
         ItemData        =   "BuscaProducto.frx":0000
         Left            =   11115
         List            =   "BuscaProducto.frx":000D
         Style           =   2  'Dropdown List
         TabIndex        =   14
         Top             =   5490
         Width           =   1815
      End
      Begin VB.CommandButton CmdEditar 
         Caption         =   "Editar"
         Height          =   360
         Left            =   4350
         TabIndex        =   13
         Top             =   5430
         Width           =   1425
      End
      Begin VB.Frame Frame1 
         Caption         =   "Opciones de busqueda"
         Height          =   780
         Left            =   4725
         TabIndex        =   10
         Top             =   405
         Width           =   8205
         Begin VB.OptionButton Option2 
            Caption         =   "Que contenga el texto dentro de la descripcion"
            Height          =   300
            Left            =   4485
            TabIndex        =   12
            Top             =   360
            Value           =   -1  'True
            Width           =   3630
         End
         Begin VB.OptionButton OptBusca 
            Caption         =   "Que la descripcion comience con el texto"
            Height          =   300
            Left            =   210
            TabIndex        =   11
            Top             =   345
            Width           =   3600
         End
      End
      Begin VB.CommandButton cmdCarga 
         Caption         =   "Consultar Stock"
         Height          =   285
         Left            =   8985
         TabIndex        =   9
         Top             =   1230
         Width           =   3945
      End
      Begin VB.CommandButton CmdTodos 
         Caption         =   "Todos"
         Height          =   390
         Left            =   3435
         TabIndex        =   4
         Top             =   735
         Width           =   1215
      End
      Begin VB.TextBox TxtBusqueda 
         Height          =   375
         Left            =   135
         TabIndex        =   1
         Top             =   735
         Width           =   3300
      End
      Begin VB.CommandButton CmdSeleccionar 
         Caption         =   "&Seleccionar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   225
         TabIndex        =   3
         Top             =   5430
         Width           =   1815
      End
      Begin VB.CommandButton CmdCrear 
         Caption         =   "Crear Nuevo Producto"
         Height          =   375
         Left            =   2265
         TabIndex        =   5
         Top             =   5430
         Width           =   1815
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   375
         Left            =   120
         OleObjectBlob   =   "BuscaProducto.frx":0037
         TabIndex        =   7
         Top             =   345
         Width           =   2655
      End
      Begin MSComctlLib.ListView LvDetalle 
         Height          =   4095
         Left            =   120
         TabIndex        =   2
         Top             =   1245
         Width           =   8790
         _ExtentX        =   15505
         _ExtentY        =   7223
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   5
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "T1000"
            Text            =   "Codigo"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "T1500"
            Text            =   "Codigo"
            Object.Width           =   3351
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "T3000"
            Text            =   "Descripcion"
            Object.Width           =   8819
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   3
            Object.Tag             =   "N100"
            Text            =   "Precio Venta"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Object.Tag             =   "T1000"
            Text            =   "Codigo Empresa"
            Object.Width           =   0
         EndProperty
      End
      Begin MSComctlLib.ListView LvSuc 
         Height          =   3750
         Left            =   9000
         TabIndex        =   8
         Top             =   1575
         Width           =   3930
         _ExtentX        =   6932
         _ExtentY        =   6615
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   3
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "T1000"
            Text            =   "Codigo"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "T1500"
            Text            =   "Sucursal"
            Object.Width           =   4233
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   2
            Object.Tag             =   "N109"
            Text            =   "Stock"
            Object.Width           =   1764
         EndProperty
      End
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   120
      OleObjectBlob   =   "BuscaProducto.frx":00B4
      Top             =   6630
   End
End
Attribute VB_Name = "BuscaProducto"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public Sm_Filtro_Tipo As String
Dim Sm_Tipo As String
Dim RsProductos As Recordset


Private Sub CboTipo_Click()
    If CboTipo.ListIndex = -1 Then
        Sm_Tipo = ""
    ElseIf CboTipo.ListIndex = 0 Then
        Sm_Tipo = " AND pro_tipo ='F'"
    ElseIf CboTipo.ListIndex = 1 Then
        Sm_Tipo = " AND pro_tipo ='M'"
    Else
        Sm_Tipo = ""
    End If
    
        
End Sub

Private Sub CmdCarga_Click()
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    
    Sql = "SELECT bod_id, bod_nombre," & _
            "IFNULL((SELECT sto_stock FROM pro_stock s WHERE s.bod_id=b.bod_id AND pro_codigo='" & LvDetalle.SelectedItem & "'),0) stock " & _
            "FROM par_bodegas b " & _
            "WHERE bod_activo='SI' "
    Consulta RsTmp3, Sql
    LLenar_Grilla RsTmp3, Me, LvSuc, False, True, True, False
    'sql = "SELECT (SELECT SUM(sto_stock) FROM pro_stock k WHERE ) " & _
            FROM maestro_productos m " & _
            "WHERE
            
End Sub

Private Sub CmdCrear_Click()
    SG_codigo = Empty
   
    If SG_Es_la_Flor = "SI" Then
                AgregarProductoFlor.Bm_Nuevo = True
                AgregarProductoFlor.Show 1
    Else
         AgregarProducto.Bm_Nuevo = True
         
        AgregarProducto.Show 1
    End If
    Sql = "SELECT codigo,marca,descripcion " & _
          "FROM maestro_productos " & _
          "WHERE  rut_emp='" & SP_Rut_Activo & "' " & _
          "ORDER BY descripcion"
    CargaLista
End Sub

Private Sub CmdEditar_Click()
            If LvDetalle.SelectedItem Is Nothing Then Exit Sub
            SG_codigo = LvDetalle.SelectedItem.Text
            AgregarProducto.Bm_Nuevo = False
            If SG_Es_la_Flor = "SI" Then
                AgregarProductoFlor.Show 1
            Else
                AgregarProducto.Show 1
            End If
            CmdSeleccionar_Click
End Sub

Private Sub CmdSalir_Click()
    SG_codigo = Empty
    Unload Me
End Sub

Private Sub CmdSeleccionar_Click()
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    SG_codigo = LvDetalle.SelectedItem.Text
    Unload Me
End Sub

Private Sub CmdTodos_Click()
    Sql = "SELECT codigo,mar_nombre,descripcion,precio_venta " & _
          "FROM maestro_productos p " & _
             "JOIN par_marcas m ON p.mar_id=m.mar_id  " & _
          "WHERE pro_activo='SI' AND p.rut_emp='" & SP_Rut_Activo & "' " & _
          "ORDER BY descripcion"
    CargaLista
End Sub

Private Sub Form_Load()
    

    Centrar Me
    Aplicar_skin Me, App.Path
    
    
    Sql = "SELECT codigo,pro_codigo_interno,descripcion,precio_venta,pro_codigo_interno " & _
          "FROM maestro_productos p " & _
          "JOIN par_marcas m ON p.mar_id=m.mar_id  " & _
          "WHERE pro_tipo='F' AND pro_activo='SI' " & Sm_Filtro_Tipo & " AND p.rut_emp='" & SP_Rut_Activo & "' " & _
          "ORDER BY descripcion LIMIT 100"
    
    
    CargaLista
    
    CargaSucs
    
    'Solo para alcalde
    '21 Agosto 2015
    If SP_Rut_Activo = "76.169.962-8" Then
        OptBusca.Value = True
    End If
    
End Sub
Private Sub CargaSucs()
    Sql = "SELECT sue_id, CONCAT(sue_ciudad,'-',sue_direccion) sucursal,0 " & _
            "FROM sis_empresas_sucursales " & _
            "WHERE emp_id=" & IG_id_Empresa
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvSuc, False, True, True, False

End Sub
Private Sub CargaLista()
    Consulta RsProductos, Sql
    LLenar_Grilla RsProductos, Me, LvDetalle, False, True, True, False
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Sm_Filtro_Tipo = ""
End Sub

Private Sub LvDetalle_DblClick()
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    'SG_codigo = LvDetalle.SelectedItem.Text
    SG_codigo = LvDetalle.SelectedItem.SubItems(4)
    Unload Me
End Sub

Private Sub LvDetalle_KeyDown(KeyCode As Integer, Shift As Integer)
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    If KeyCode = 13 Then CmdSeleccionar_Click
        
End Sub
'ordena columnas
Private Sub LvDetalle_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    ordListView ColumnHeader, Me, LvDetalle
End Sub

Private Sub OptBusca_Click()
    TxtBusqueda_Change
End Sub

Private Sub Option2_Click()
TxtBusqueda_Change
End Sub

Private Sub Timer1_Timer()
    On Error Resume Next
    TxtBusqueda.SetFocus
    Timer1.Enabled = False
End Sub
Private Sub TxtBusqueda_Change()
    Dim Sp_Like As String
    If Len(Me.TxtBusqueda.Text) < 3 Then
        'Me.AdoProducto.Recordset.Filter = 0
    Else
        If OptBusca.Value Then
            Sp_Like = " LIKE '" & TxtBusqueda & "%' "
        Else
            
            Sp_Like = " LIKE '%" & TxtBusqueda & "%' "
        End If
        
        If SP_Rut_Activo = "76.370.578-1" Then
            'Para talentos debemos verificar con que lista de precios trabajar
            If Val(Principal.SkSucursal.Tag) > 0 Then
                    PVENTA = "(SELECT lsd_precio FROM par_lista_precios_detalle t WHERE p.id=t.id LIMIT 1)"
                    Sql = "SELECT codigo,pro_codigo_interno,descripcion," & PVENTA & ",pro_codigo_interno " & _
                        "FROM maestro_productos p " & _
                        "JOIN par_marcas m ON p.mar_id=m.mar_id  " & _
                        "WHERE pro_activo='SI' " & Sm_Filtro_Tipo & " AND p.rut_emp='" & SP_Rut_Activo & "' AND  descripcion " & Sp_Like & _
                        " ORDER BY descripcion LIMIT 20"
            
            
            
            
            Else
            
            
                Sql = "SELECT codigo,pro_codigo_interno,descripcion,precio_venta,pro_codigo_interno " & _
                "FROM maestro_productos p " & _
                "JOIN par_marcas m ON p.mar_id=m.mar_id  " & _
                "WHERE pro_activo='SI'  AND p.rut_emp='" & SP_Rut_Activo & "' AND  descripcion " & Sp_Like & _
                " ORDER BY descripcion LIMIT 50"
            End If
        Else
            Sql = "SELECT codigo,pro_codigo_interno,descripcion,precio_venta,pro_codigo_interno " & _
              "FROM maestro_productos p " & _
              "JOIN par_marcas m ON p.mar_id=m.mar_id  " & _
              "WHERE pro_activo='SI' " & Sm_Filtro_Tipo & " AND p.rut_emp='" & SP_Rut_Activo & "' AND  descripcion " & Sp_Like & Sm_Tipo & _
              " ORDER BY descripcion LIMIT 50"

        
        End If

        CargaLista
    End If
End Sub

Private Sub TxtBusqueda_GotFocus()
    En_Foco TxtBusqueda
End Sub

Private Sub TxtBusqueda_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 13 Then LvDetalle.SetFocus
End Sub

Private Sub TxtBusqueda_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii))) 'Transformo a mayuscula el caracter ingresado
     If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtBusqueda_Validate(Cancel As Boolean)
TxtBusqueda = Replace(TxtBusqueda, "'", "")
End Sub
